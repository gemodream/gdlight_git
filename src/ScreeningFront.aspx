﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="ScreeningFront.aspx.cs" Inherits="Corpt.ScreeningFront" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
        
</ajaxToolkit:ToolkitScriptManager>
    <script src="Style/select2.min.js"></script>
	<link href="Style/select2.min.css" rel="stylesheet" />
	
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: #555555;
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .modalProgress {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .centerProgress {
            z-index: 1000;
            margin: 20% 40%;
            padding: 10px;
            width: 70px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
            text-align: center;
            color: black;
            font-size: 9px;
        }

        .centerProgress img {
            height: 50px;
            width: 50px;
        }

        .modal-header {
            background-color: #f0f0f0;
            text-align: left;
            padding: 7px 14px !important;
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
        }

        .modal-title {
            margin: -7px -13px 0px 0 !important;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.5em;
            height: 20px;
        }

        .modal-footer {
            background-color: #f0f0f0;
            padding: 17px 5px !important;
            text-align: right;
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=0);
            opacity: 0;
        }

        .modal-dialog {
            vertical-align: middle;
            background-color: white;
            border-radius: 5px;
            background-color: white;
            border: 2px solid gray;
            box-shadow: 10px 10px 5px rgba(0,0,0,0.6);
        }

        .modal-body {
            padding: 5px 5px 0px 5px;
            position: relative;
            overflow: hidden;
            max-height: 730px !important;
        }

        .RadioButtonList input {
            vertical-align: top;
        }

        .DropDownListBoxStyle {
            font-size: 15px;
            font-family: Tahoma,Arial,sans-serif;
            padding: 1px 1px 1px 1px;
        }

        .radioButtonList input[type="radio"] {
            width: auto;
            float: left;
            width: 20px;
            height: 20px;
            margin-top: 7px;
            margin-left: 7px;
            margin-left: 2px;
            position: absolute;
            z-index: 1;
            font-size: 15px;
            padding-bottom:9px;
        }

        .checkboxSingle input[type="checkbox"] {
            width: auto;
            float: left;
            width: 20px;
            height: 20px;
            margin-top: 5px;
            margin-left: 7px;
            margin-left: 10px;
            position: absolute;
            z-index: 1;
            font-size: 15px;
        }

         .checkboxSingle2 input[type="checkbox"] {
            width: auto;
            float: left;
            width: 20px;
            height: 20px;
            margin-top: 10px;
            margin-left: 7px;
            margin-left: 10px;
            position: absolute;
            z-index: 1;
            font-size: 15px;
        }

        .radioButtonList label {
            color: white;
            font-weight: bold;
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            *background-color: #2f96b4;
            background-repeat: repeat-x;
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            border-left-color: #2f96b4;
            border-right-color: #2f96b4;
            border-top-color: #2f96b4;
            border-bottom-color: #1f6377;
            padding-left: 22px;
            width: max-content;
            line-height: 25px;
           
        }

        .checkboxSingle label, .checkboxSingle2 label {
            color: white;
            font-weight: bold;
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            *background-color: #2f96b4;
            background-repeat: repeat-x;
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            border-left-color: #2f96b4;
            border-right-color: #2f96b4;
            border-top-color: #2f96b4;
            border-bottom-color: #1f6377;
            width: max-content;
            line-height: 25px;
            padding-bottom: 6px;
            padding-top: 2px;
            height: 20px;
            margin-bottom: 2px;
            margin-top: 0px;
            padding-left: 32px;
        }

        .DropDownListBoxStyle option:hover {
            font-size: 15px;
            font-family: Tahoma,Arial,sans-serif;
            padding: 1px 1px 1px 1px;
            font-weight: bold;
            background-color: #ddd;
        }

        .auto-style1 {
            height: 30px;
        }

        .auto-style3 {
            width: 205px;
        }

        ul, li, body {
            margin: 0;
            padding: 0;
        }


        .tab {
            border: 1px solid black;
            padding: 2px 10px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }

        .selectedtab {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            text-decoration: none;
        }

        .tabcontents {
            border: 1px solid black;
            padding: 10px;
            width: 760px;
            height: auto;
            background-color: white;
        }

        .filedSetBorder {
            border: 1px groove black !important;
            padding: 10px 10px 10px 10px;
            border-radius: 8px;
            margin: 0px 5px 5px 5px;
            float: left;
            vertical-align: top;
        }

        .filedSetBorder legend {
            padding-left: 7px;
            padding-right: 7px;
            margin-bottom: 0px;
            border-bottom: 0px !important;
            font-family: verdana,tahoma,helvetica;
            width: max-content;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-size: 12px;
            font-family: Tahoma,Arial,sans-serif;
        }

        #master_content {
            padding-top: 20px;
        }

        #master_contentfooter {
            padding-left: 202px !important;
        }
        .ddlListStyle{
			width:250px;
			height:30px;
		}
        .RadioButtonList input {
            vertical-align: top;
        }
    </style>


    <script type="text/javascript">
		
		function pageLoad() {
			$('.ddlListStyle').select2({ theme: "classic" });
		}
        ///-----------------Upload Control Validation--------------------------///
        var ifIgnoreError = false;
        function UpLoadStarted(sender, e) {
            var fileName = e.get_fileName();
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'png' || fileExtension == 'PNG') {
                //file is good -- go ahead
                document.getElementById('<%=btnUploadTakeIn.ClientID%>').disabled = true;
                document.getElementById('<%=btnUploadGiveOut.ClientID%>').disabled = true;
            }
            else {
                //stop upload
                ifIgnoreError = true;
                sender._stopLoad();
            }
        }
        function UploadError(sender, e) {
            if (ifIgnoreError) {
                alert("Wrong file type, Only JPG/PNG file extension allowed");
            }
            else {
                alert(e.get_message());
            }
        }
        function UploadCompleteTakeIn(sender, e) {
            document.getElementById('<%=btnUploadTakeIn.ClientID%>').disabled = false;
        }

        function UploadCompleteGiveOut(sender, e) {
            document.getElementById('<%=btnUploadGiveOut.ClientID%>').disabled = false;
        }


        function FilterShapes_TakeIn(caller) {
            var textbox = caller;
            filter = textbox.value.toLowerCase();
            var dropDownArray = document.getElementById('<%=lstCustomerlookupReq.ClientID%>');
            var list = dropDownArray;
            for (var c = 0; c < list.options.length; c++) {
                if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
                    list.options.item(c).style.display = "block";
                }
                else {
                    list.options.item(c).style.display = "none";
                }
            }
        }

        ///------------------------DropDownListBox Extender--------------------//
		function FilterDestination(caller) {
			var textbox = caller;
			filter = textbox.value.toLowerCase();
			var dropDownArray = document.getElementById('<%=lstDestination.ClientID%>');
			//var dropDownArray = document.getElementById('DropDownListBox');
			var list = dropDownArray;
			for (var c = 0; c < list.options.length; c++) {
				if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
					list.options.item(c).style.display = "block";
				}
				else {
					list.options.item(c).style.display = "none";
				}
			}
        }
        $(function () {
            $("#<%=nbrCopiesBox.ClientID %>").keyup(function () {
              if ($(this).val().length == 1) {
                  $("#<%=txtRequest.ClientID %>").focus();
              }
          });
        });
        $(function () {
            $("#<%=txtScanPackageBarcodeShipReceiving.ClientID %>").keyup(function () {
              if ($(this).val().length == 1) {
                    $("#<%=txtRequest.ClientID %>").focus();
                }
            });
        });
        $(function () {
            $("#<%=txtGiveOutScanPackageBarCodeShip.ClientID %>").keyup(function () {
              if ($(this).val().length == 1) {
                    $("#<%=txtViewOrder.ClientID %>").focus();
                }
            });
        } );
       <%-- $(function () {
            $("#<%=txtViewOrder.ClientID %>").keypress(function () {
                alert("Wow; Its Work!.")
            });
        });--%>
	</script>
	

    <div class="demoarea">
        <div style="font-size: smaller; height: 25px;">
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="demoheading">
            Front
        </div>


        <asp:UpdatePanel runat="server" ID="MainPanel" UpdateMode="Conditional" ChildrenAsTrigger="false">
            <Triggers>
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$btnPrintLabelTakeIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel6$btnPrintLabelViewOrder" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$btnPrintReceiptTakeIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel6$btnPrintReceiptViewOrder" />
            </Triggers>
            <ContentTemplate>

            <table>
              <tr>
               <td style="vertical-align:top;">

                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="900px" Font-Size="12px" ActiveTabIndex="1" 
					AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged" >
                    
                    <ajaxToolkit:TabPanel ID="TabPanel1" HeaderText="Take In" runat="server"  >
                        <ContentTemplate>
                            <asp:Panel runat="server" CssClass="form-inline" ID="CustomerSearchPanel" Width="1000px" DefaultButton="btnRequestSearch"  >
                                <table style="float: left; margin-right: 10px; width: 500px;">
                                    <tr>
                                        <td style="width: 142px; ">
                                            <asp:Label ID="lblRequest" runat="server" Text="Request ID" Width="100px" Style="font-size: medium; font-weight: bold; float: left" Height="20px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtRequest" autocomplete="off" Height="20px" MaxLength="10" OnTextChanged="txtRequest_TextChanged"></asp:TextBox>
                                            <asp:HiddenField ID="hndDeliveryMethodCodeTakeIn" runat="server" />
                                            <asp:HiddenField ID="hdnAcceptCustomerID" runat="server" />
                                            <asp:HiddenField ID="hdnAcceptCustomerName" runat="server" />
                                            <asp:HiddenField ID="hdnAcceptMessenger" runat="server" />
                                            <asp:HiddenField ID="hdnAcceptCarrier" runat="server" />
                                            <asp:HiddenField ID="hdnAcceptTrackingNumber" runat="server" />
                                            <asp:Button ID="btnRequestSearch" runat="server" Text="Load" ToolTip="Get Request" CssClass="btn btn-info btn-large" OnClick="btnRequestSearch_Click" ValidationGroup="vgRequest"  />
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers" TargetControlID="txtRequest" Enabled="False"></ajaxToolkit:FilteredTextBoxExtender>

											<asp:RequiredFieldValidator runat="server" ID="OrderReq2" ControlToValidate="txtRequest"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
															ValidationGroup="vgRequest" />
														<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender2" TargetControlID="OrderReq2"
															HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
                                        </td>

                                        <td colspan="0">
                                            <asp:CheckBox ID="acceptanceBox" runat="server" CssClass="checkbox" Text="Bulk Acceptance" Width="80px" AutoPostBack="True" OnCheckedChanged="acceptanceBox_CheckedChanged" />
                                            <asp:CheckBox ID="saveShipInfoBox" runat="server" CssClass="checkbox" Text="Save Shipping Info" Width="80px" AutoPostBack="True" OnCheckedChanged="saveShipInfoBox_CheckedChanged" />
                                            <asp:Label ID="lblRequestError" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table>
                                    <tr>
                                        <td style="width:586px;">
                                                <table border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCustomerlookup" runat="server" Text="Customer" Width="140px" Style="float: left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtCustomerLookupTakeIn" Width="365px" Height="20px" Enabled="False"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                                            <asp:HiddenField ID="hdnContactId" runat="server" />
                                                            <asp:HiddenField ID="hdnMemoPicturePath" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblRetailer" runat="server" Text="Retailer" Width="125px" Style="float: left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtRetailerName" Width="365px" Height="20px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblServiceType" runat="server" Text="Service Type" Width="125px" Style="float: left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtServiceTypeTakeIn" Width="365px" Height="20px" Enabled="False"></asp:TextBox>
                                                             <asp:HiddenField ID="hdnServiceTypeId" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCategory" runat="server" Text="Category" Width="125px" Style="float: left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtCategoryTakeIn" Width="365px" Height="20px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblBNCategory" runat="server" Text="BN Category" Width="125px" Style="float: left" Visible="False"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtBNCategory" Width="365px" Height="20px" Enabled="False" Visible="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblBNDestination" runat="server" Text="BN Destination" Width="125px" Style="float: left" Visible="False"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: -5px; padding-right: 5px">
                                                            <asp:TextBox runat="server" ID="txtBNDestination" Width="365px" Height="20px" Enabled="False" Visible="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            <div id="MsgDetails" runat="server">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Messenger Details</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMessengerTakeIn" runat="server" Text="Messenger" Width="125px" Style="float: left"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtMessengerTakeIn" Width="276px" autocomplete="off" Height="20px" Enabled="False" Visible="False" PlaceHolder="Messenger Lookup" style="padding-left:0px;" ></asp:TextBox>
                                                            <asp:DropDownList ID="ddlMessengersList" runat="server" AutoPostBack="True" Height="26px" OnSelectedIndexChanged="OnMessengerListIndexChanged" ToolTip="Messengers List">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField runat="server" ID="hdnMessengerTakeIn"></asp:HiddenField>
                                                            <asp:HiddenField runat="server" ID="hdnMessengerPath2Photo" />
                                                            <asp:HiddenField runat="server" ID="hdnMessengerPath2Signature" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSwithToCourier" runat="server" Text="Switch To Shipping" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnSwitchToCourier_Click" Width="160px" />
                                                        </td>
                                                    </tr>

                                                </table>

                                                <table id="tblTakeIn" runat="server">
                                                    <tr runat="server">
                                                        <td colspan="2" style="padding-left: 128px;" runat="server">
                                                            <table border="0">
                                                                <tr>
                                                                    <td style="border: 1px solid lightgray;">
                                                                        <asp:Image ID="imgStoredSignature" runat="server" Style="float: right; width: 150px; height: 100px;" />
                                                                    </td>

                                                                    <td style="width: 2px;"></td>

                                                                    <td style="border: 1px solid lightgray;">
                                                                        <asp:Image ID="imgStoredPhoto" runat="server" Style="float: right; width: 150px; height: 100px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="text-align: center;">
                                                                        <asp:Button runat="server" ID="signatureBtn" Text ="Signature" CssClass="btn btn-large btn-info" Style="float: left;" OnClick="ValidateSignature_Click" Visible="False" />
                                                                    </td>
                                                                    <td style="width: 2px;"></td>
                                                                    <td style="text-align: center;">
                                                                        <asp:Button runat="server" ID="photoBtn" Text ="Photo" CssClass="btn btn-large btn-info" Style="float: left;" OnClick="ValidatePhoto_Click" Visible="False" />

                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </fieldset>
                                            </div>
                                            <div id="ShippingDetails" runat="server">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Shipping Details</legend>
                                                <table id="tblShipReceiving" runat="server" >
                                                    
                                                    <tr runat="server">
                                                        <td runat="server">
                                                            <asp:Label ID="Label11" runat="server" Text="Shipping Courier" Width="127px" Style="float: left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: 7px; padding-right: 5px" runat="server">
                                                            <asp:TextBox runat="server" ID="txtCarriersShipReceiving" Width="270px" Height="20px" Enabled="False" style="padding-left:0px;" Visible="False"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlShippingCourier" runat="server" AutoPostBack="True" 
                                                            Height="26px" OnSelectedIndexChanged="CourierListSelectedChanged" ToolTip="Couriers List">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hdnCarriersShipReceiving" runat="server" Value="0" />
                                                        </td>
                                                        <td runat="server">
                                                            <asp:Button ID="btnSwithToMessenger" runat="server" Text="Switch To Messenger" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnSwitchToMessenger_Click" Width="160px" />
                                                        </td>
                                                    </tr>

                                                    <tr runat="server">
                                                        <td colspan="2" style="padding-top: 5px; padding-bottom: 10px; text-align: right;" runat="server">
                                                            <asp:TextBox ID="txtScanPackageBarcodeShipReceiving" runat="server" Height="20px" Width="350px" Style="margin-right: 5px; padding-left: 0px;" AutoPostBack="True"></asp:TextBox>
                                                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" Enabled="True" TargetControlID="txtScanPackageBarcodeShipReceiving" WatermarkText="Scan Package Bar-Code Here">
                                                            </ajaxToolkit:TextBoxWatermarkExtender>
                                                        </td>
                                                        <td runat="server">
                                                            <asp:HiddenField ID="hdnCourierInfo" runat="server" />
                                                            <asp:HiddenField ID="hdnTrackingInfo" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr runat="server">
                                                        <td style="height: 20px;" colspan="2" runat="server">
                                                            <asp:Label ID="lblVendorTakeIn" runat="server" Style="float: left; font-weight: bold;"></asp:Label>
                                                            <asp:HiddenField ID="hdnVendorTakeIn" runat="server" />
                                                           
                                                        </td>
                                                        <td runat="server">
                                                            <asp:Button ID="btnShippingOverwrite" runat="server" Text="Overwrite Info" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnOverwriteShipInfo_Click" Width="160px" Visible="False" />
                                                        </td>
                                                    </tr>

                                                </table>

                                            </fieldset>
                                            </div>
                                        </td>

                                        <td style="width: 250px; height: 100px; text-align: center;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table >
                                                            <tr style="margin-left: 20px">
                                                                <div style="margin-left: 30px">
                                                                <td colspan=3 style="margin-left: 30px">
                                                                    <span>Number Of Copies: </span>
                                                                    <asp:TextBox runat="server" ID="nbrCopiesBox" autocomplete="off" Height="20px" Width="70px" MaxLength="6" AutoPostBack="True"></asp:TextBox>
                                                                </td>
                                                                    </div>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button1" runat="server" Text="1" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button2" runat="server" Text="2" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button3" runat="server" Text="3" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button5" runat="server" Text="4" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button6" runat="server" Text="5" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button7" runat="server" Text="6" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button8" runat="server" Text="7" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button9" runat="server" Text="8" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button10" runat="server" Text="9" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button11" runat="server" Text="C" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesClear_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button12" runat="server" Text="0" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button13" runat="server" Text="ADD" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopies_Click" Height="60px" Width="60px" Visible="False" />
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <asp:Label ID="lblOrderNumbeTakeIn" runat="server" Font-Bold="True" Style="font-size: 30px; text-align: center; width: 200px; white-space: pre-wrap; word-wrap: break-word;"></asp:Label>
                                                        </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>

                                </table>

                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel5" Width="800px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Order</legend>
                                    <table width="400px" style="float: left; margin-right: 0px;" border="0">
                                        <tr>
                                            <div id="QtyNumber" runat="server" >
                                            <td>
                                                <asp:Label ID="lblNumberOfItems" runat="server" Text="Number of Items" Style="float: left" Width="150px"></asp:Label>
                                            </td>
                                            <td style="margin-top:6px; margin-bottom:6px; ">
                                                <asp:TextBox runat="server" ID="txtNoOfItemsTakeIn" Width="100px" MaxLength="5" Height="20px" ></asp:TextBox>
                                                <asp:HiddenField ID="hdntxtNoOfItemsTakeIn" runat="server"></asp:HiddenField>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers" TargetControlID="txtNoOfItemsTakeIn" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td Width="250px">
                                                <asp:RadioButtonList ID="rdlNoOfItems" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" Height="24px">
                                                    <asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                         </div>
                                            <div id="TotalW" runat="server" visible="False">
                                                <td>
                                                    <asp:Label ID="lblTotalW" runat="server" Text="Total Weight" Style="float: left" Width="150px"></asp:Label>
                                                </td>
                                                <td style="width: 360px; margin-top: 6px; margin-bottom:10px; ">
                                                    <asp:TextBox runat="server" ID="txtTotalW" Width="100px" Enabled="False" MaxLength="5" Height="20px"></asp:TextBox>
                                                </td>
                                                <td Width="250px" height="30" >
                                               </td>
                                         </div>
                                            <td rowspan="5" style="vertical-align: top;">
                                                <fieldset class="filedSetBorder" style="padding-top: 2px;">
                                                    <legend class="label">Memo/PO/SKU/Style</legend>
                                                    <table style="float: right; padding-left: 10px;" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOrderMemo" runat="server" Style="float: left" Text="Order Memo" Width="70px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMemoTakeIn" runat="server" Width="115px" Height="20px" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblPONumber" runat="server" Style="float: left" Text="PO Number" Width="70px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPONumber" runat="server" Width="115px" Height="20px" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblSKU" runat="server" Style="float: left" Text="SKU" Width="70px"></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSKU" runat="server" Width="115px" Height="20px" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblStyle" runat="server" Style="float: left" Text="Style" Width="70px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtStyle" runat="server" Width="115px" Height="20px" Enabled="False"></asp:TextBox>

                                                            </td>

                                                        </tr>


                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Button ID="btnServiceTimeTakeIn" runat="server" Text="Service Time" Width="150px" class="btn btn-info"
                                                    Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 3px;"></asp:Button>

                                            </td>
                                            <td colspan="2" class="auto-style1">

                                                <asp:TextBox runat="server" ID="txtServiceTimeTakeIn" Width="310px" Enabled="False" Height="20px"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdnServiceTimeTakeIn" Value="4"></asp:HiddenField>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSpecialInstructions" runat="server" Text="Special Instructions" Width="120px" Style="float: left"></asp:Label>
                                                <asp:TextBox runat="server" ID="txtSpecialInstructions" Width="200px" Height="65px" TextMode="MultiLine" MaxLength="2000" Enabled="False"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBatchMemo" runat="server" Text="Batch Memos" Width="120px" Style="float: left"></asp:Label>
                                                <asp:TextBox runat="server" ID="txtBatchMemos" Width="200px" Height="65px" TextMode="MultiLine" MaxLength="2000" Enabled="False"></asp:TextBox>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Total Qty" Width="120px" Style="float: left"></asp:Label>

                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtTotalQty" runat="server" Width="40px" CssClass="bagtext" MaxLength="5" Enabled="False"></asp:TextBox></td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    &nbsp;
                                </fieldset>
                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel6" Width="780px">
                                <table style="width: 100%">

                                    <tr class="checkboxSingle">
                                        <td style="padding-left: 7px;" colspan="2">
                                            <asp:CheckBox ID="chkPickedUpByOurMessengerTakeIn" runat="server" Text="Picked Up By GSI Messenger" OnCheckedChanged="chkPickedUpByOurMessengerTakeIn_CheckedChanged" AutoPostBack="True" />
                                            <asp:HiddenField ID="hdnOrderCode" runat="server" />
                                            <asp:HiddenField ID="hdnRequestID" runat="server" />

                                            <div style="float: right;">
                                                <asp:Button ID="btnPrintReceiptTakeIn" runat="server" Text="Print Documents" CssClass="btn btn-large btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintReceiptTakeIn_Click" Enabled="False" Visible="False" />
                                                <asp:Button ID="btnPrintLabelTakeIn" runat="server" Text="Print Label" CssClass="btn btn-large btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintLabelTakeIn_Click" Visible="False" Enabled="False" />
                                                <asp:Button ID="btnSubmitTakeIn" runat="server" Text="Submit" CssClass="btn btn-large btn-info" Style="float: right;" OnClick="btnSubmitTakeIn_Click" Width="110px" />
                                                <asp:Button ID="btnSubmitTakeInAll" runat="server" Text="Save Shipping Info" CssClass="btn btn-large btn-info" Style="float: right;" OnClick="btnSubmitTakeInAll_Click" Width="180px" Visible="False"/>
                                                <asp:Button ID="btnClearTakeIn" runat="server" Text="Clear" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" OnClick="btnClearTakeIn_Click" Width="110px" />
                                                <asp:Button ID="btnSendEmail" runat="server" Text="Resend Email" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" OnClick="btnSendEmail_Click" Width="150px" Enabled="False" />
                                                <asp:Button ID="btnAcceptRequest" runat="server" Text="Accept" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" OnClick="btnAcceptRequest_Click" Width="110px" Enabled="False" />
                                            </div>

                                        </td>

                                    </tr>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel6" HeaderText="Take Out Order" runat="server" Visible="true">
                        <ContentTemplate>
							 <asp:Panel runat="server" CssClass="form-inline" ID="Panel1" Width="630px" DefaultButton="imgViewOrder">
                            <table>
                                <tr>
                                    <td colspan="2">

                                        <fieldset class="filedSetBorder" style="float: none; width: max-content;">
                                            <legend class="label">Order</legend>
                                            <table style="float: left; margin-right: 15px; width: 500px;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label41" runat="server" Text="Order Number" Width="100px" Style="float: left" Height="20px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtViewOrder" autocomplete="off" Height="20px" MaxLength="10" OnTextChanged="txtViewOrder_Changed"></asp:TextBox>
                                                        <asp:HiddenField runat="server" ID="hdnViewOrder"></asp:HiddenField>
                                                        <asp:HiddenField runat="server" ID="hdnViewRequestID"></asp:HiddenField>
                                                        <asp:HiddenField runat="server" ID="hdnBulkGiveOutMessenger" />
                                                        <asp:HiddenField runat="server" ID="hdnBulkGiveOutCarrier" />
                                                        <asp:HiddenField runat="server" ID="hdnBulkGiveOutTrackingNumber" />
                                                        <asp:HiddenField runat="server" ID="hdnBulkGiveOutCustomercode" />
                                                        <asp:HiddenField runat="server" ID="hdnBulkGiveOutCustomerName" />
                                                        <asp:Button ID="imgViewOrder" runat="server" Text="TAKE OUT" OnClick="btnViewOrder_Click" CssClass="btn btn-info btn-large" ValidationGroup="vgViewOrder" />
                                                        <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers" TargetControlID="txtViewOrder" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                                   <asp:RequiredFieldValidator runat="server" ID="OrderReq1" ControlToValidate="txtViewOrder"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
															ValidationGroup="vgViewOrder" />
														<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender1" TargetControlID="OrderReq1"
															HighlightCssClass="validatorCalloutHighlight" Enabled="True" />--%>
														</td>

                                                    <td colspan="0">
                                                        <div>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                        <asp:CheckBox ID="BulkGiveOutCheckBox" runat="server" CssClass="checkbox" Text="Bulk Take Out" Width="140px" AutoPostBack="true" OnCheckedChanged="BulkGiveOutCheckBox_CheckedChanged" />
                                                                        </td>
                                                                    </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="BulkGiveOutCheckBoxNoPrint" runat="server" CssClass="checkbox" Text="GiveOut NoPrint" Width="140px" AutoPostBack="true" OnCheckedChanged="BulkGiveOutCheckBoxNoPrint_CheckedChanged" />
                                                                    </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        <asp:Label ID="lblViewOrderSearch" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <%--<asp:TextBox runat="server" ID="txtViewTrackingNumber" autocomplete="off" Height="20px" MaxLength="10" OnTextChanged="txtViewOrder_Changed" Visible="true" Text="Track"</asp:TextBox>--%>
                                                        <asp:Label Text="--------Criteria to Choose--------" runat="server" Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="ddlCriteria" runat="server" AutoPostBack="True" CssClass="text-style"
                                                            ToolTip ="Criteria List" Width="250px" Height="30px" style="margin-top: 8px; margin-left: 0px;" Visible="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="imgViewTakeOutOrder" runat="server" Text="TAKE OUT OLD" OnClick="btnViewOrder_Click" CssClass="btn btn-info btn-large" visible="false"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr runat="server" id="trViewOrderSummary">
                                    <td colspan="2" runat="server">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Order Summary</legend>
                                            <table style="float: left; width: 550px;" border="0">
                                                <tr>
                                                    <td colspan="1">
                                                        <asp:Label ID="Label42" runat="server" Text="Customer:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderCustomer" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label43" runat="server" Text="No Goods:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblNoGoods" runat="server"></asp:Label>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <asp:Label ID="Label55" runat="server" Text="Vendor:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderVendorName" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label53" runat="server" Text="Memo:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderMemo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label47" runat="server" Text="Number Of Items (Not Inspected):" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderNumOfItemsNotInspected" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label46" runat="server" Text="Create Date:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderCreateDate" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label49" runat="server" Text="Total Weight (Not Inspected):" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderTotalWeightNotInspected" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label52" runat="server" Text="Request ID:" Font-Bold="True" Visible="true"></asp:Label>
                                                        <asp:Label ID="lblViewRequestID" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label44" runat="server" Text="Number Of Items (Inspected):" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderNumOfItemsInspected" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label59" runat="server" Text="Order Status:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderStatus" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label48" runat="server" Text="Total Weight (Inspected):" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderTotalWeightInspected" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label51" runat="server" Text="Service Type:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewOrderServiceType" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr runat="server" id="trViewOrderOperation" visible="False">
                                    <td style="vertical-align: top;" id="tdTakeIn" runat="server">
                                        <fieldset class="filedSetBorder" style="width: 340px; height: 270px;"> 
                                            <legend class="label">
                                                <asp:Label ID="lblTakeInViewOrderLabel" runat="server" Text="Take In" Font-Bold="True"></asp:Label></legend>
                                            <table border="0" id="tbTakeIn" runat="server">
                                                <tr runat="server">
                                                    <td runat="server" class="auto-style4">
                                                        <asp:Label ID="lblTakeInSubmittedbytext" runat="server" Text="Submitted By:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblTakeInSubmittedBy" runat="server" Font-Bold="False"></asp:Label>
                                                        <asp:Label ID="lblTakeInPickedUpByOurMessenger" runat="server" Text="Picked Up By GSI Messenger" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="trTakeInImages">
                                                    <td runat="server">
                                                        <asp:Image ID="imgTakeInMessanger" runat="server" Style="float: right; width: 150px; height: 100px;" />
                                                        <asp:Image ID="imgTakeInSignature" runat="server" Style="float: right; width: 150px; height: 100px;" />
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="trViewTakeInOrderCarrier">
                                                    <td runat="server">
                                                        <asp:Label ID="Label50" runat="server" Text="Carrier:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInOrderCarrier" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInOrderCarrierTrackingNumber">
                                                    <td runat="server">
                                                        <asp:Label ID="Label54" runat="server" Text="Carrier Tracking Number:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInOrderCarrierTrackingNumber" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="Label56" runat="server" Text="Accepted By:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInAcceptedBy" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInSignatureTimeStamp">
                                                    <td colspan="2" runat="server">
                                                        <asp:Label ID="Label60" runat="server" Text="Signature TimeStamp:" Font-Bold="True" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInSignatureTimeStamp" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="trViewTakeInRecieptUploadedTimeStamp">
                                                    <td colspan="2" runat="server">
                                                        <asp:Label ID="Label63" runat="server" Text="Receipt Uploaded TimeStamp:" Font-Bold="True" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInRecieptUploadedTimeStamp" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInRecieptDownload">
                                                    <td runat="server">
                                                        <asp:LinkButton ID="lnkRecieptTakeInDownloadfrm" runat="server" href="#" target="_blank" Text="View Signed Receipt" Style="font-weight: bold;" Visible="False"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>

                                        </fieldset>
                                    </td>
                                    
                                    
                                    <td style="vertical-align: top;" runat="server" id="tdGiveOut1">
                                        <div id="GiveOutMessenger" runat="server">
                                        <fieldset class="filedSetBorder" style="width: 340px; height: 270px;">
                                            <legend class="label">
                                                <asp:Label ID="lblGiveOutViewOrderLabel" runat="server" Text="Give Out" Font-Bold="True"></asp:Label></legend>
                                            <table border="0" id="tbGiveOutOrder" runat="server">
                                                <tr runat="server">
                                                    <td id="tdGiveOutMessenger" runat="server">
                                                        <table runat="server">
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    <asp:Label ID="lblViewGiveOutSubmittedBytext" runat="server" Text="Accepted By:" Font-Bold="True"></asp:Label>
                                                                    <asp:TextBox runat="server" ID="txtGiveOutMessengersList" Width="276px" autocomplete="off" Height="20px" Enabled="False" PlaceHolder="Messenger Lookup" style="padding-left:0px;" ></asp:TextBox>
                                                                    <asp:DropDownList ID="ddlGiveOutMessengersList" runat="server" AutoPostBack="True" Height="26px" Width="150px" 
                                                                        OnSelectedIndexChanged="OnGiveOutMessengerListIndexChanged" ToolTip="Messengers List">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField runat="server" ID="hdnMessengerTakeOut"></asp:HiddenField>
                                                                    <asp:HiddenField runat="server" ID="hdnTakeOutMessengerPath2Signature" />
                                                                    <asp:HiddenField runat="server" ID="hdnTakeOutMessengerPath2Photo" />
                                                                    <asp:Label ID="lblViewGiveOutSubmittedBy" Width="30px" runat="server" Style="float: left"></asp:Label>
                                                                    <asp:Button ID="Button27" runat="server" Text="To Shipping" CssClass="btn btn-large btn-info" Style="float: right" Font-Size="8pt" OnClick="btnGiveOutSwitchToCourier_Click" Width="100px" />
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    <asp:Image ID="imgGiveOutSignature" runat="server" Style="float: left; width: 120px; height: 90px;" />
                                                                    <asp:Image ID="imgGiveOutPhoto" runat="server" Style="float: right; width: 120px; height:90px;" />
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                    <td style="text-align: center;" runat="server">
                                                                        <asp:Button runat="server" ID="giveOutSignatureBtn" Text ="Signature" 
                                                                            CssClass="btn btn-large btn-info" Style="float: left;" OnClick="ValidateGiveOutSignature_Click" />
                                                                        <asp:Button runat="server" ID="giveOutPhotoBtn" Text ="Photo" 
                                                                            CssClass="btn btn-large btn-info" Style="float: right;" OnClick="ValidateGiveOutPhoto_Click" />
                                                                    </td>
                                                                    <td style="width: 2px;" runat="server"></td>
                                                                    <td style="text-align: center;" runat="server">
                                                                        
                                                                    </td>
                                                                </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                        </fieldset>
                                        </div>
                                        <div id="GiveOutShipping" runat="server">
                                            <fieldset class="filedSetBorder" style="width: 340px; height: 270px;">
                                                <legend class="label">
                                                <asp:Label ID="lblGiveOutShipping" runat="server" Text="Give Out" Font-Bold="True"></asp:Label></legend>
                                                <table id="tblGiveOutShipping" runat="server">
                                                    <tr runat="server">
                                                        <td runat="server">
                                                            <asp:Label ID="Label111" runat="server" Text="Shipping Carrier" Width="127px" Style="float:left"></asp:Label>
                                                        </td>
                                                        <td style="padding-top: 7px; padding-right: 5px;" runat="server">
                                                            <asp:TextBox runat="server" ID="txtGiveOutCarriersShip" Width="150px" Height="20px" Enabled="False" style="padding-left:0px;" Visible="False"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlGiveOutShippingCourier" runat="server" AutoPostBack="True" Height="26px" 
                                                                OnSelectedIndexChanged="GiveOutCourierListSelectedChanged" ToolTip="Courier List"></asp:DropDownList>
                                                            <asp:HiddenField ID="hdnGiveOutCarriersShip" runat="server" Value="0" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server">
                                                        <td colspan="2" style="padding-top: 10px; text-align: right" runat="server" class="auto-style4">
                                                            <asp:TextBox ID="txtGiveOutScanPackageBarCodeShip" runat="server" Height="20px" Width="350px" Style="margin-right: 5px; padding-left: 0px;" AutoPostBack="True"></asp:TextBox>
                                                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWaterMarkExtender11" runat="server" Enabled="True" TargetControlID="txtGiveOutScanPackageBarCodeShip"
                                                                WatermarkText="Scan Package Bar-Code here">
                                                            </ajaxToolkit:TextBoxWatermarkExtender>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server">
                                                        <td runat="server">
                                                            <asp:Button ID="btnGiveOutSwitchToMessenger" runat="server" Text="Switch to Messenger" CssClass="btn btn-large btn-info" 
                                                                Style="float: right; margin-right=5px;" Font-Size="8pt" OnClick="btnGiveOutSwitchToMessenger_Click" Width="160px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </td>
                                    <td runat="server">
                                        <table id="tbNumberOfCopies" runat="server" visible="false">
                                                <tr runat="server">
                                                    <td runat="server">
                                                        <table >
                                                            <tr style="margin-left: 20px">
                                                                <div style="margin-left: 30px">
                                                                <td colspan=3 style="margin-left: 30px">
                                                                    <span>Number Of Copies: </span>
                                                                    <asp:TextBox runat="server" ID="NmbrOfCopiesBox" autocomplete="off" Height="20px" Width="70px" MaxLength="6" AutoPostBack="true"></asp:TextBox>
                                                                </td>
                                                                    </div>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button14" runat="server" Text="1" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button15" runat="server" Text="2" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button16" runat="server" Text="3" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button17" runat="server" Text="4" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button18" runat="server" Text="5" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button19" runat="server" Text="6" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button20" runat="server" Text="7" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button21" runat="server" Text="8" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button22" runat="server" Text="9" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Button23" runat="server" Text="C" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesClearView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button24" runat="server" Text="0" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" />
                                                                    </td>
                                                                <td>
                                                                    <asp:Button ID="Button25" runat="server" Text="ADD" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="nbrCopiesView_Click" Height="60px" Width="60px" Visible="false" />
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td runat="server">
                                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Style="font-size: 30px; text-align: center; width: 200px; white-space: pre-wrap; word-wrap: break-word;"></asp:Label>
                                                        </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr align="center" runat="server">
                                    <td colspan="2" style="text-align:;" align="center" runat="server">
                                        <asp:Button ID="btnUploadSignedOrderReciept" runat="server" Text="Upload Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-left: 2px;" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnPrintReceiptViewOrder" runat="server" Text="Reprint Receipt" CssClass="btn btn-sm btn-info" Style="float:none; margin-right: 2px;" OnClick="btnPrintReceiptViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnPrintLabelViewOrder" runat="server" Text="Reprint Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnPrintLabelViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnClearViewOrder" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnClearViewOrder_Click" Visible="False" />
                                        <asp:Button ID="btnPrintReceiptTakeOut" runat="server" Text="Submit TakeOut" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnPrintReceiptViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnUsePhotoTakeOut" runat="server" Text="Use Photo" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnUseTakeOutPhotoViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnSavePhotoTakeOut" runat="server" Text="Save Photo" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnSaveTakeOutPhotoViewOrder_Click" Enabled="false" Visible="false" />
                                        
                                        <asp:Button ID="btnReprintReceiptTakeOut" runat="server" Text="Reprint TakeOut" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnPrintReceiptViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnCreateTakeOutList" runat="server" Text="Bulk Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnCreateTakeOutList_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnBulkGiveOut" runat="server" Text="Bulk Give Out" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnBulkGiveOut_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnPickupMail" runat="server" Text="Pickup Mail" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnCreateTakeOutList_Click" Enabled="false" Visible="false" />
                                        <asp:HiddenField ID="hdnTakeOutPhoto" runat="server" />
                                    </td>
                                </tr>
                            </table>
								 </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel3" HeaderText="Submission & Front" runat="server">
                        <ContentTemplate>
                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel10" Width="1000px"  >
								<table>
									<tr>
										<td style="width: 586px;">
											<table border="0">
												<tr>
													<td rowspan="4" colspan="2" style="vertical-align: top;">
														<table>
															<tr>
																<td>
																	<asp:Label ID="Label25" runat="server" Text="Customer" Width="140px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionCustomerlst" runat="server" AutoPostBack="True" DataTextField="CustomerName" 
																		DataValueField="CustomerID" CssClass="ddlListStyle" OnSelectedIndexChanged="OnSubmissionCustomerLookupSelectedChanged" ToolTip="Customers List"></asp:DropDownList>

																</td>
																<td>
																	<span id="spCustomerlst" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionCustomerlst" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:Label ID="Label70" runat="server" Text="Customer Service" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionCustomerService" runat="server" DataTextField="ServiceName" AutoPostBack="true"
																		DataValueField="ServiceID" CssClass="ddlListStyle" ToolTip="Customer Contact"></asp:DropDownList>

																</td>
																<td>
																	<span id="spCustomerServices" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionCustomerService" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:Label ID="Label27" runat="server" Text="Retailer" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionRetailer" runat="server" AutoPostBack="True" DataTextField="RetailerName" OnTextChanged="InputBoxChanged"
																		DataValueField="RetailerId" CssClass="ddlListStyle" ToolTip="Customers List" OnSelectedIndexChanged="ddlRetailer_SelectedIndexChanged"></asp:DropDownList>
																</td>
																<td>
																	<span id="spRetailer" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionRetailer" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:Label ID="Label28" runat="server" Text="Service Type" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionServiceType" runat="server" AutoPostBack="True" OnTextChanged="InputBoxChanged" DataTextField="ServiceTypeName" DataValueField="ServiceTypeId"
																		CssClass="ddlListStyle" ToolTip="Service Type" OnSelectedIndexChanged="ddlServiceType_SelectedIndexChanged"></asp:DropDownList>

																</td>
																<td>
																	<span id="spServiceType" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionServiceType" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:Label ID="Label29" runat="server" Text="Category" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionCategory" runat="server" AutoPostBack="True" DataTextField="CategoryName" OnTextChanged="InputBoxChanged"
																		DataValueField="CategoryId" CssClass="ddlListStyle" ToolTip="Category" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"></asp:DropDownList>

																</td>
																<td>
																	<span id="spCategory" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionCategory" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>

															<tr id="JWeight" runat="server" visible="False"><td runat="server">
																	<asp:Label ID="Label73" runat="server" Text="Weight" Width="125px" Style="float: left"></asp:Label>

																</td>
<td runat="server">
																	<asp:TextBox ID="txtSubmissionWeightloose" GroupName="sub_fields" runat="server" CssClass="form-control form-control-height text-style"
																		placeholder="Total Weight" Width="236px" Height="24px" Style="padding-top: 2px; padding-bottom: 2px; text-align: center" TabIndex="13" AutoPostBack="True"
																		OnTextChanged="InputFieldChanged" MaxLength="6" ToolTip="Please enter items Weight" />

																	<span id="spWeight" style="color: Red" visible="False" runat="server">*</span>

																</td>
</tr>

														</table>
												</tr>
												<tr>
													<td rowspan="4" colspan="2" style="vertical-align: top;">
														<table>
															<tr id="trJewelryType" runat="server"><td runat="server">
																	<asp:Label ID="Label33" runat="server" Text="Jewelry Type" Width="125px" Style="float: left"></asp:Label>

																</td>
																	<td style="padding-top: -5px; padding-right: 5px" runat="server">
																	<asp:DropDownList ID="ddlSubmissionJewelryType" runat="server"  DataTextField="JewelryTypeName" 
																		DataValueField="JewelryTypeId" CssClass="ddlListStyle" ToolTip="Jewelry Type"></asp:DropDownList>

																</td>
																	<td runat="server">
																	<span id="spJewType" style="color: Red" runat="server"></span>
																		<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionJewelryType" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
																</tr>

															<tr>
																<td>
																	<asp:Label ID="Label36" runat="server" Text="Customer Contact" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px">
																	<asp:DropDownList ID="ddlSubmissionCustomerContact" runat="server"  DataTextField="CustomerContactName" 
																		DataValueField="CustomerContactId" CssClass="ddlListStyle" ToolTip="Customer Contact"></asp:DropDownList>

																	<asp:HiddenField runat="server" ID="hdnSubmissionCustomerContact" Value="4"></asp:HiddenField>

																</td>
																<td>
																	<span id="spCustomerContact" style="color: Red" runat="server"></span>
																	<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionCustomerContact" ValidationGroup="vgpSubmissionFront">
																	</asp:RequiredFieldValidator>
																</td>
															</tr>
															<tr id="trBNCategory" runat="server" visible="False"><td runat="server">
																	<asp:Label ID="Label71" runat="server" Text="BN Category" Width="125px" Style="float: left"></asp:Label>
																</td>
																	<td style="padding-top: -5px; padding-right: 5px" runat="server">
																	<asp:DropDownList ID="ddlSubmissionBNCategory" runat="server" AutoPostBack="True" DataTextField="CategoryName" 
																		DataValueField="CategoryId" CssClass="ddlListStyle" ToolTip="BN Category" OnSelectedIndexChanged="ddlSubmissionBNCategory_SelectedIndexChanged"></asp:DropDownList>
																</td>
																	<td runat="server">
																	<span id="spBNCategory" style="color: Red" runat="server" visible="False">*</span>
																	
																</td>
																</tr>
															<tr id="trDestination" runat="server" visible="False"><td runat="server">
																	<asp:Label ID="Label72" runat="server" Text="Destination" Width="125px" Style="float: left"></asp:Label>

																</td>
																<td style="padding-top: -5px; padding-right: 5px" runat="server">
																	<asp:DropDownList ID="ddlSubmissionDestination" runat="server"  DataTextField="DestinationName" 
																		DataValueField="DestinationId" CssClass="ddlListStyle" ToolTip="Destination"></asp:DropDownList>
																</td>
																	<td runat="server">
																		<span id="spSubmissionDestination" style="color: Red" runat="server" visible="False">*</span>

																	</td>
																</tr>
															<tr id="asnTr" runat="server" visible="False">
																<td runat="server">
																	<div class="form-group" style="margin-bottom: 8px; margin-top: 0px;">
																		<asp:Label ID="Label6" runat="server" Text="ASN" Width="125px" Style="float: left"></asp:Label>
																	</div>
																</td>
																<td runat="server">
																<asp:TextBox ID="txtASN"  runat="server"  placeholder="Enter ASN" MaxLength="250" Style="width: 235px;height:20px;" />

															</td>
															</tr>

														</table>
													</td>
												</tr>
											</table>
											<asp:Panel runat="server" CssClass="form-inline" ID="Panel11" Width="800px">
												<fieldset class="filedSetBorder">
													<legend class="label">Order</legend>
													<table width="400px" style="float: left; margin-top: -10px; margin-bottom: -5px;" border="0">
														<tr>
															<td>
																<table>
																	<tr id="JQuantity" runat="server">
																		<td>
																			<asp:Label ID="Label45" runat="server" Text="Number of Items" Style="float: left" Width="165px"></asp:Label>

																		</td>
																		<td style="margin-top: 6px; margin-bottom: 6px;" colspan="2">
																			<table>
																				<tr>
																					<td>
																						<asp:TextBox runat="server" ID="txtSubmissionNoofItems" Width="100px" MaxLength="5" Height="20px" OnTextChanged="InputFieldChanged" ></asp:TextBox>
																						<ajaxToolkit:FilteredTextBoxExtender  runat="server"  FilterType="Numbers" TargetControlID="txtSubmissionNoofItems" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
																						
																					</td>
																					<td>
																						<span id="spQuantity" style="color: Red" runat="server"></span>
																						<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionNoofItems" ValidationGroup="vgpSubmissionFront">
																						</asp:RequiredFieldValidator>
																					</td>
																				</tr>
																			</table>
																		</td>
																		<td width="250px">
																			<asp:RadioButtonList ID="rblSubmissionNoofItems" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" Height="24px"><asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
																				<asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
																				<asp:ListItem Text="N/A" Value="2"></asp:ListItem>
																			</asp:RadioButtonList>

																		</td>

																	</tr>
																	<tr>

																		<td>
																			<asp:Label ID="Label57" runat="server" Text="Total Weight" Style="float: left" Width="150px"></asp:Label>

																		</td>
																		<td style="width: 360px; margin-top: 6px; margin-bottom: 10px;">
																			<asp:TextBox runat="server" ID="txtSubmissionWeight" Width="50px" MaxLength="5" Height="20px"></asp:TextBox>
																			<ajaxToolkit:FilteredTextBoxExtender  runat="server" ValidChars="." FilterType="Numbers ,Custom" TargetControlID="txtSubmissionWeight" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
																		</td>
																		<td>
																			<asp:DropDownList ID="ddlSubmissionWeightType" runat="server" Width="50px" ToolTip="Weight" Style="height: 30px; margin-bottom: 2px;" 
																				DataTextField="MeasureUnitName" DataValueField="MeasureUnitID">
																		</asp:DropDownList>

																		</td>
																		<td width="250px" height="30">
																			<asp:RadioButtonList ID="rblSubmissionWeight" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" Height="24px"><asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
																				<asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
																				<asp:ListItem Text="N/A" Value="2"></asp:ListItem>
																				</asp:RadioButtonList>

																		</td>

																	</tr>
																</table>
															</td>

															<td rowspan="5" style="vertical-align: top;">
																<fieldset class="filedSetBorder" style="padding-top: 2px;">
																	<legend class="label">Memo/PO/SKU/Style</legend>
																	<table style="float: right; padding-left: 10px;" border="0">
																		<tr>
																			<td>
																				<asp:Label ID="Label61" runat="server" Style="float: left" Text="Order Memo" Width="70px"></asp:Label>

																			</td>
																			<td>
																				<asp:TextBox ID="txtSubmissionOrderMemo" runat="server" Width="115px" Height="20px" MaxLength="100" OnTextChanged="InputFieldChanged" ></asp:TextBox>

																			</td>
																			<td>
																				<span id="spMemo" style="color: Red" runat="server"></span>
																				<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionOrderMemo" ValidationGroup="vgpSubmissionFront">
																				</asp:RequiredFieldValidator>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<asp:Label ID="Label62" runat="server" Style="float: left" Text="PO Number" Width="70px"></asp:Label>

																			</td>
																			<td>
																				<asp:TextBox ID="txtSubmissionPONumber" runat="server" Width="115px" Height="20px" MaxLength="100" OnTextChanged="InputFieldChanged" ></asp:TextBox>

																			</td>
																			<td>
																				<span id="spPONumber" style="color: Red" runat="server"></span>
																				<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionPONumber" ValidationGroup="vgpSubmissionFront">
																				</asp:RequiredFieldValidator>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<asp:Label ID="Label64" runat="server" Style="float: left" Text="SKU" Width="70px"></asp:Label>


																			</td>
																			<td>
																				<asp:TextBox ID="txtSubmissionSKU" runat="server" Width="115px" Height="20px" MaxLength="50" OnTextChanged="InputFieldChanged"></asp:TextBox>

																				<asp:DropDownList ID="ddlSubmissionSKU" CssClass="ddlListStyle" DataTextField="CustomerProgramName" OnTextChanged="InputBoxChanged" AutoPostBack="True"
																					DataValueField="CpId" runat="server" Width="130px" Height="30px" Visible="False"
																					OnSelectedIndexChanged="dllSubmissionSKU_SelectedIndexChanged"></asp:DropDownList>


																			</td>
																			<td>
																				<span id="spSKU" style="color: Red" runat="server"></span>
																				<asp:RequiredFieldValidator ID="rfvtxtSubmissionSKU" runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionSKU" ValidationGroup="vgpSubmissionFront">
																				</asp:RequiredFieldValidator>
																				<asp:RequiredFieldValidator  ID="rfvddlSubmissionSKU" runat="server" Enabled="false" Display="Dynamic" ErrorMessage="*" InitialValue="0"  SetFocusOnError="true" ControlToValidate="ddlSubmissionSKU">
																				</asp:RequiredFieldValidator>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<asp:Label ID="Label65" runat="server" Style="float: left" Text="Style" Width="70px"></asp:Label>

																			</td>
																			<td>
																				<asp:TextBox ID="txtSubmissionStyle" runat="server" Width="115px" Height="20px" MaxLength="50" OnTextChanged="InputFieldChanged" ></asp:TextBox>

																			</td>
																			<td>
																				<span id="spStyle" style="color: Red" runat="server"></span>
																				<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionStyle" ValidationGroup="vgpSubmissionFront">
																				</asp:RequiredFieldValidator>
																			</td>
																		</tr>


																	</table>
																</fieldset>
																<span>Number Of Copies: </span>
																<asp:TextBox runat="server" ID="txtSubmissionNoOfCopies" autocomplete="off" Height="20px" Width="70px" MaxLength="6" Text="1" AutoPostBack="True"></asp:TextBox>

															</td>
														</tr>

														<tr>
															<td>
																<table>
																	<tr>
																		<td class="auto-style1">
																			<asp:Button ID="btnSubmissionServiceTime" runat="server" CausesValidation="false" Text="Service Time" Width="150px" class="btn btn-info"
																				Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 3px;"></asp:Button>

																		</td>
																		<td colspan="2" class="auto-style1">
																			<asp:TextBox runat="server" ID="txtSubmissionServiceTime" Width="310px" Enabled="False" Height="20px" Style="float: none;"></asp:TextBox>
																			<asp:HiddenField runat="server" ID="hdnSubmissionServiceTime" Value="4"></asp:HiddenField>
																			<span id="spServiceTime" style="color: Red" runat="server"></span>
																			<asp:RequiredFieldValidator runat="server" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="true" ControlToValidate="txtSubmissionServiceTime" ValidationGroup="vgpSubmissionFront">
																			</asp:RequiredFieldValidator>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<asp:Label ID="Label66" runat="server" Text="Special Instructions" Width="120px" Style="float: left"></asp:Label>

																		</td>
																		<td>
																			<asp:Label ID="Label40" runat="server" Text="Batch Memos" Width="80px" Style="float: right; margin-left: 28px;"></asp:Label>

																		</td>
																		<td style="display: flex;">
																			<asp:TextBox runat="server" ID="txtBatchMemo" Width="155px" MaxLength="100" Style="margin-right: 10px; float: right;"></asp:TextBox>

																			<asp:Button ID="btnAddMemo" runat="server" Text="Add" CssClass="btn btn-info" Style="float: none; margin-right: 0px;" CausesValidation="false" OnClick="btnAddMemo_Click" />

																		</td>
																	</tr>
																	<tr>

																		<td>
																			<asp:TextBox runat="server" ID="txtSubmissionSpecialInstruction" Width="200px" Height="55px" TextMode="MultiLine" MaxLength="2000"></asp:TextBox>

																		</td>

																		<td>
																			<asp:Label ID="Label69" runat="server" Text="Batch Memos </br>Numbers" Width="80px" Style="float: right"></asp:Label>

																		</td>
																		<td>
																			<asp:ListBox runat="server" ID="lstBatchMemos" Width="225px" Height="65px" Style="float: right;"></asp:ListBox>

																		</td>
																	</tr>
																	<tr>
																		<td colspan="3">
																			<table>
																				<tr>
																					<td style="width: 80px;">
																						<asp:Label ID="Label75" runat="server" Text="Vendor Memo"></asp:Label>
																					</td>
																					<td>
																						<%--<asp:FileUpload runat="server" ID="fuVendorMemo" accept="application/pdf" ToolTip="Please select file in PDF format"></asp:FileUpload>--%>
																						<ajaxToolkit:AsyncFileUpload ID="fuVendorMemo"  runat="server"  OnUploadedComplete="UploadMemoFileToAzure" 
																							ToolTip="Please select file in PDF format" Width="200px"   ></ajaxToolkit:AsyncFileUpload>
																					</td>
																					<td>
																						<asp:Button ID="btnMemo" runat="server" Text="Upload" OnClick="UploadMemoFileToAzure_Click" CausesValidation="false" CssClass="btn btn-info" />
																						<asp:TextBox ID="txtVendorMemoPDF" runat="server" style="visibility:hidden;width:0px;"  />
																						<asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="*" ControlToValidate="txtVendorMemoPDF" ValidationGroup="vgpSubmissionFront">
																						</asp:RequiredFieldValidator>
																						<span id="spVendorMemo" style="color: Red" runat="server"></span>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</fieldset>
											</asp:Panel>

											<div id="dvSubmissionMessanger" runat="server">
												<fieldset class="filedSetBorder">
													<legend class="label">Messenger Details</legend>
													<table>
														<tr>
															<td>
																<table>
																	<tr>
																		<td>
																			<asp:Label ID="Label37" runat="server" Text="Messenger" Width="125px" Style="float: left"></asp:Label>

																		</td>
																	</tr>
																</table>
															</td>
															<td>
																<asp:TextBox runat="server" ID="TextBox9" Width="276px" autocomplete="off" Height="20px" Enabled="False" Visible="False" PlaceHolder="Messenger Lookup" Style="padding-left: 0px;"></asp:TextBox>

																<asp:DropDownList ID="ddlSubmissionMessengerList" runat="server" CssClass="ddlListStyle" AutoPostBack="True" Height="26px"
																	OnTextChanged="InputBoxChanged" OnSelectedIndexChanged="OnSubmissionMessengerListIndexChanged" ToolTip="Messengers List"></asp:DropDownList>


															</td>
															<td>
																<span id="spMessenger" style="color: Red" runat="server"></span>
																<asp:RequiredFieldValidator runat="server" ID="rfvSubmissionMessengerList" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionMessengerList" ValidationGroup="vgpSubmissionFront">
																</asp:RequiredFieldValidator>

															</td>
															<td>
																<asp:Button ID="btnSwitchToSubmissionCourier" runat="server" Text="Switch To Shipping" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnSwitchToSubmissionCourier_Click" Width="160px" CausesValidation="false" />

															</td>
														</tr>

													</table>

													<table id="Table1" runat="server"><tr runat="server"><td colspan="2" style="padding-left: 128px;" runat="server">
																<table border="0">
																	<tr>
																		<td style="border: 1px solid lightgray;">
																			<asp:Image ID="imgSubmissionStoredSignature" runat="server" Style="float: right; width: 150px; height: 100px;" />

																		</td>

																		<td style="width: 2px;"></td>

																		<td style="border: 1px solid lightgray;">
																			<asp:Image ID="imgSubmissionStoredPhoto" runat="server" Style="float: right; width: 150px; height: 100px;" />

																		</td>
																	</tr>

																	<tr>
																		<td style="text-align: center;">
																			<asp:Button runat="server" ID="btnValidateSubmissionSignature" Text="Signature" CssClass="btn btn-large btn-info" Style="float: left; margin-top: 5px;" OnClick="ValidateSubmissionSignature_Click" />

																		</td>
																		<td style="width: 2px;"></td>
																		<td style="text-align: center;">
																			<asp:Button runat="server" ID="btnValidateSubmissionPhoto" Text="Photo" CssClass="btn btn-large btn-info" Style="float: left; margin-top: 5px;" OnClick="ValidateSubmissionPhoto_Click" />


																		</td>
																	</tr>

																</table>
															</td>
</tr>
</table>

												</fieldset>
											</div>

											<div id="dvSubmissionShipping" runat="server" visible="False">
												<fieldset class="filedSetBorder">
													<legend class="label">Shipping Details</legend>
													<table id="Table2" runat="server"><tr runat="server"><td runat="server">
																<asp:Label ID="Label38" runat="server" Text="Shipping Courier" Width="127px" Style="float: left"></asp:Label>

															</td>
<td style="padding-top: 7px; padding-right: 5px" runat="server">
																<asp:TextBox runat="server" ID="TextBox10" Width="270px" Height="20px" Enabled="False" Style="padding-left: 0px;" Visible="False"></asp:TextBox>

																<asp:DropDownList ID="ddlSubmissionCourierList" runat="server"  CssClass="ddlListStyle"
																	Height="26px"  ToolTip="Couriers List"></asp:DropDownList>

																<asp:HiddenField ID="hdnSubmissionCourier" runat="server" Value="0" />

															</td>
<td runat="server">
																<span id="spCourier" style="color: Red" runat="server"></span>
															<asp:RequiredFieldValidator runat="server" ID="rfvSubmissionCourierList" Display="Dynamic" ErrorMessage="*" InitialValue="0" SetFocusOnError="true" ControlToValidate="ddlSubmissionCourierList" ValidationGroup="vgpSubmissionFront">
																</asp:RequiredFieldValidator>

															</td>
<td runat="server">
																<asp:Button ID="btnSwitchToSubmission" runat="server" Text="Switch To Messenger" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnSwitchToSubmissionMessenger_Click" Width="160px" CausesValidation="false" />

															</td>
</tr>
<tr runat="server"><td colspan="2" style="padding-top: 5px; padding-bottom: 10px; text-align: right;" runat="server">
																<asp:TextBox ID="txtSubmissionBarcode" runat="server" Height="20px" Width="350px" Style="margin-right: 5px; padding-left: 0px;" ></asp:TextBox>

																<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" Enabled="True" TargetControlID="txtSubmissionBarcode" WatermarkText="Scan Package Bar-Code Here"></ajaxToolkit:TextBoxWatermarkExtender>

															</td>
<td runat="server">

															</td>
</tr>
<tr runat="server"><td style="height: 20px;" colspan="2" runat="server">
																<asp:Label ID="Label39" runat="server" Style="float: left; font-weight: bold;"></asp:Label>



															</td>
<td runat="server">
																<asp:Button ID="Button32" runat="server" Text="Overwrite Info" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" Font-Size="8pt" OnClick="btnOverwriteShipInfo_Click" Width="160px" Visible="False" />

															</td>
</tr>
</table>


												</fieldset>
											</div>

											
											<div style="padding-top: 40px; padding-left: 50px; text-align: center; display: inline-grid; line-height: 30px;">
												<asp:Label ID="lblServiceCode" runat="server" Font-Size="70px" Font-Bold="True"></asp:Label>
<br />
												<asp:Label ID="lblOrderCodelbl" runat="server" Text="OrderCode" Font-Size="16px" Visible="False"></asp:Label>

												<asp:Label ID="lblOrderCode" runat="server" Text="3030042" Font-Size="30px" Font-Bold="True" Visible="False"></asp:Label>

											</div>
										</td>
									</tr>
								</table>
                            </asp:Panel>


                          

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel12" Width="780px">
                                <table style="width: 100%">

                                    <tr class="checkboxSingle2">
                                        <td style="padding-left: 7px;" colspan="2">
											<div style="float: left;">
												<asp:CheckBox ID="chkSubmissionPickedUpByGSIMessenger" runat="server" Text="Picked Up By GSI Messenger" OnCheckedChanged="chkSubmissionPickedUpByGSIMessenger_CheckedChanged" AutoPostBack="True" Style="margin-top: 10px" />
												<asp:HiddenField ID="hdnID" runat="server" Value="0" />
												<asp:Button ID="btnSubmissionPrintLabel" runat="server" Text="Print Label" CssClass="btn btn-large btn-info" Style="float: none; margin-right: 5px;" OnClick="btnSubmissionPrintLabel_Click" CausesValidation="false" />

												<asp:Button ID="btnSubmissionSubmit" runat="server" Text="Submit" CssClass="btn btn-large btn-info" Style="float: right;" OnClick="btnSubmissionSubmit_Click" Width="110px" ValidationGroup="vgpSubmissionFront" />

												<asp:Button ID="btnSubmissionClear" runat="server" Text="Clear" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" OnClick="btnSubmissionClear_Click" Width="110px" CausesValidation="false" />

												<asp:HiddenField ID="hdnSubmissionRequestID" runat="server"></asp:HiddenField>
												<asp:HiddenField ID="hdnGDServiceTypeId" runat="server"></asp:HiddenField>
												<asp:HiddenField ID="hdnSubmissionOrderCode" runat="server"></asp:HiddenField>
											</div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                    </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" HeaderText="" runat="server" Enabled="false" Visible="false">
                        <ContentTemplate>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel2" Width="630px" DefaultButton="btnItemGiveOut" Enabled="False">
                                <table>
                                    <tr>
                                        <td style="padding-left: 5px;">
                                            <asp:Label runat="server" ID="Label26" Text="Customer Name"></asp:Label>
                                        </td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtCustomerGiveOut" runat="server" Width="300px" Enabled="False" Height="20px" style="margin-left:8px;"></asp:TextBox>
                                            <asp:HiddenField ID="hfCustomerGiveOut" runat="server" />
                                            
                                        </td>
                                    </tr>
                                </table>

                                <fieldset class="filedSetBorder">
                                    <legend class="label">Messenger Detail</legend>
                                    <table style="width: 650px;" border="0">

                                        <tr>
                                            <td style="width:160px;">
                                                <asp:Button ID="btnMessengerLookupGiveOut" runat="server" class="btn btn-info" Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" Text="Messenger" Width="150px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMessengerLookupGiveOut" runat="server" autocomplete="off" Enabled="False" Height="20px" PlaceHoler="Messenger Lookup" Width="300px" ></asp:TextBox>
                                            </td>
                                        
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td >
                                                <table border="0">
                                                    <tr>
                                                        
                                                        <td style="border: 1px solid lightgray; float: none; padding-left: 2px;">
                                                            <asp:Image ID="imgStoredSignGiveOut" runat="server" BorderStyle="None" Style="float: right; width: 150px!important; height: 100px!important;" />
                                                        </td>
                                                        <td style="border: 1px solid lightgray; float: none; padding-left: 2px;">
                                                            <asp:Image ID="imgStoredPhotoGiveOut" runat="server" Style="float: right; width: 150px!important; height: 100px!important;" />
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td style="text-align: center;">
                                                            <asp:Label ID="Label8" runat="server" Style="float: none; padding: 0 10px 0 10px;" Text="Stored Signature" Width="100px"></asp:Label>
                                                        </td>
                                                       

                                                        <td style="text-align: center;">
                                                            <asp:Label ID="Label12" runat="server" Style="float: none; width: 50px; padding: 0 10px 0 12px;" Text="Photo" Width="50px"></asp:Label>
                                                        </td>
                                                     
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>


                                    </table>
                                </fieldset>
                                <br />

                                <fieldset class="filedSetBorder" style="width: auto;">
                                    <legend class="label">Shipping Detail</legend>
                                    <table style="width: 650px;" border="0">
                                       
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCarriersShipOut" runat="server" Text="Carrier" CssClass="btn btn-sm btn-info" Width="100px"
                                                    Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 5px; margin-bottom: 4px;"></asp:Button>

                                                <asp:TextBox runat="server" ID="txtCarriersShipOut" Width="200px" autocomplete="off" Height="20px" Enabled="False"></asp:TextBox>
                                                <asp:HiddenField ID="hdnCarriersShipOut" runat="server" Value="0" />
                                            </td>
                                            <td style="padding-right: 5px">
                                                <asp:Button ID="btnDepartureSettingShipOut" runat="server" Text="Departure Setting" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right; margin-right: 5px; margin-top: 4px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px; padding-bottom: 10px;">
                                                <asp:TextBox ID="txtBarcodeShipOut" runat="server" Width="312px" placeholder="" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkText="Scan Package Bar-Code Here" TargetControlID="txtBarcodeShipOut" Enabled="True"></ajaxToolkit:TextBoxWatermarkExtender>
                                               
                                            </td>
                                            <td style="padding-left: 42px;">
                                                Account Number
                                                <asp:TextBox ID="txtAccountNumberGiveOut" runat="server" Width="150px" Height="20px"></asp:TextBox>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblVendorShipOut" runat="server" Style="float: left; font-size: small;"></asp:Label>
                                                <asp:HiddenField ID="hfVendorShipOut" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>

                                <br />
                                <table style="width:660px" border="0">

                                    <tr>
                                        <td colspan="2">

                                            <table style="width: 676px;">
                                                <tr>
                                                    <td style="padding-bottom: 7px; width: 80px;">
                                                        <asp:Label ID="Label5" runat="server" Text="Order Number"></asp:Label>
                                                    </td>
                                                    <td style="width: 170px;">
                                                        <asp:TextBox ID="txtItemNoGiveOut" runat="server" Width="150px" Height="20px" autocomplete="off" ></asp:TextBox>
														<asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtItemNoGiveOut"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
															ValidationGroup="vgItemNoGiveOut" />
														<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="OrderReqE" TargetControlID="OrderReq"
															HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
														<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers" TargetControlID="txtItemNoGiveOut" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:Button ID="btnItemGiveOut" runat="server" Text="Load" CssClass="btn btn-info btn-large" OnClick="btnItemGiveOut_Click" ValidationGroup="vgItemNoGiveOut" />
                                                    </td>
                                                    <td class="checkboxSingle" style="float: right;">
                                                       <fieldset class="filedSetBorder">
                                                           <asp:RadioButton ID="rdbGiveOut" runat="server" Text="GiveOut" GroupName="Out" CssClass="radioButtonList" AutoPostBack="True" OnCheckedChanged="rdbGiveOut_CheckedChanged" />
                                                           <asp:RadioButton ID="rdbShipOut" runat="server" Text="ShipOut" GroupName="Out" CssClass="radioButtonList" AutoPostBack="True" OnCheckedChanged="rdbShipOut_CheckedChanged" />
                                                       </fieldset>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Panel runat="server" ID="Panel3" Width="675px" Height="200px" ScrollBars="Auto" BorderStyle="Inset" BorderWidth="1px">
                                                            <asp:TreeView ID="trvOrderTreeGiveOut" runat="server" ShowCheckBoxes="All"
                                                                AfterClientCheck="CheckChildNodes();" OnClick="OnTreeClick(event)"
                                                                Font-Names="Tahoma" Font-Size="Small" ForeColor="Black"
                                                                Font-Bold="False" ExpandDepth="2">
                                                                <SelectedNodeStyle BackColor="#D7FFFF" />
                                                            </asp:TreeView>
                                                        </asp:Panel>

                                                    </td>
                                                </tr>

                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 0px; vertical-align: bottom; width: 680px;">
                                            <table border="0" style="width: 100%;">
                                                
                                                <tr>
                                                    <td class="checkboxSingle" style="">
                                                        <asp:CheckBox ID="chkTakenOutByOurMessengerGiveOut" runat="server" Text="Taken Out By GSI Messenger" Width="173px" OnCheckedChanged="chkTakenOutByOurMessengerGiveOut_CheckedChanged" AutoPostBack="True" />
                                                    </td>
                                                    <td class="checkboxSingle" >
                                                        <asp:Label ID="lblMsgGiveOut" runat="server" ForeColor="Blue" Font-Bold="True" Width="274px" style="margin-left:47px;"></asp:Label>
                                                    </td>
                                                    <td style="width: 85px;">

                                                        <asp:Button ID="btnClearGiveOut" runat="server" Text="Clear" CssClass="btn btn-large btn-info" Style="float: right; margin-right: 5px;" OnClick="btnClearGiveOut_Click" Width="85px" />
                                                    </td>

                                                    <td style="width: 85px;">
                                                        <asp:Button ID="btnSubmitGiveOut" runat="server" Text="Submit" CssClass="btn btn-large btn-info" Style="" OnClick="btnSubmitGiveOut_Click" Enabled="False" Width="85px" />

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel5" HeaderText="Create Request" runat="server" Visible="false">
                        <ContentTemplate>
							<asp:Panel runat="server" CssClass="form-inline" ID="Panel9"  DefaultButton="btnCreateRequest">
                            <table>
                                <tr>
                                    <td>
                                        <table class="form-group" style="margin-top: 1vmin; margin-bottom: 1vmin;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label13" runat="server" Text="Customer Lookup:" Width="125px" Style="float: left; padding-left: 6px;"></asp:Label>
                                                </td>
                                                <td style="padding-top: -5px; padding-right: 5px" >
                                                    <asp:TextBox runat="server" ID="txtCustomerLookupReq" OnTextChanged="txtCustomerLookupReq_TextChanged" autocomplete="off" Height="20px" Width="320px"></asp:TextBox>
                                                    <ajaxToolkit:DropDownExtender ID="DropDownExtender1" runat="server" TargetControlID="txtCustomerLookupReq" DropDownControlID="DropDownListPanelDestination" Enabled="True" DynamicServicePath="" />
                                                    <asp:Panel runat="server" ID="DropDownListPanelDestination" Style="visibility: hidden;">
                                                        <asp:ListBox runat="server" ID="lstCustomerlookupReq"
                                                            DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                                                            OnSelectedIndexChanged="OnCustomerReqSelectedChanged" Rows="20" Width="335px" CssClass="DropDownListBoxStyle"></asp:ListBox>
                                                        <br />
                                                        <asp:DropDownList ID="ddlCustomerListReq" runat="server" AutoPostBack="True" DataTextField="CustomerName"
                                                            DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnCustomerReqSelectedChanged" ToolTip="Customers List" Visible="False">
                                                        </asp:DropDownList>
                                                    </asp:Panel>
                                                    
                                                    <asp:HiddenField ID="hfCustomerCodeReq" runat="server" Value="0" />
                                                    <asp:HiddenField ID="hfCustomerIDReq" runat="server" Value="0" />
                                                    <asp:HiddenField ID="hfRequestID" runat="server" Value="0" />
                                                </td>

                                                <td class="auto-style3">
                                                    <asp:HiddenField ID="hfDeliveryMethodCode" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtDeliveryMethod" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Delivery Method" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="CreateRequest" ControlToValidate="txtDeliveryMethod" Text="*" Display="Dynamic" ErrorMessage="Please enter Delivery Method!" />
                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnDeliveryMethod" runat="server" Text="Delivery Method" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                        <input type="button" runat="server" id="hdnbtnDeparture" style="display: none" />
                                                    </div>
                                                    
                                                </td>

                                            </tr>

                                            <tr>

                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtMemoNum" style="float: left; margin-top: 4px;">
                                                            Memo:</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMemoNum" runat="server" CssClass="form-control form-control-height text-style" Style="margin-top: 7px; margin-bottom: 7px;"
                                                        placeholder="Enter Memo" MaxLength="100" Width="170px" CausesValidation="true" autocomplete="off" Height="20px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="CreateRequest" ControlToValidate="txtMemoNum" Text="*" Display="Dynamic" ErrorMessage="Please enter memo number!" />
                                                     <asp:Label ID="lblValidateMemo" runat="server" Text="**" ForeColor="Red" Visible="false"></asp:Label>
                                                </td>

                                                <td class="auto-style3">
                                                    <asp:HiddenField ID="hfShippingCourierCode" runat="server" Value="0" />                                                    
                                                    <asp:TextBox runat="server" ID="txtShippingCourier" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Shipping Courier" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:Label ID="lblValidateShippingCourier" runat="server" Text="**" ForeColor="Red" Visible="false"></asp:Label>
                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnShippingCourier" runat="server" Text="Shipping Courier" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr>

                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtPONum" style="float: left; margin-top: 4px;">
                                                            PO Number:</label>
                                                    </div

                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPONum" runat="server" CssClass="form-control form-control-height text-style"
                                                        PlaceHolder="Enter PO Number" MaxLength="100" Width="170px" autocomplete="off" Height="20px"></asp:TextBox>
                                                </td>

                                                <td class="auto-style3">

                                                    <asp:HiddenField ID="hfRetailerID" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtRetailer" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Retailer" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="CreateRequest" ControlToValidate="txtRetailer" Text="*" Display="Dynamic" ErrorMessage="Please select retailer!" />

                                                </td>


                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnRetailer" runat="server" Text="Retailer" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtStyleReq" style="float: left; margin-top: 4px;">
                                                            Style:</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtStyleReq" runat="server" CssClass="form-control form-control-height text-style"
                                                        placeholder="Enter Style" Width="170px" autocomplete="off" Height="20px" MaxLength="50"></asp:TextBox>
                                                    <asp:Label ID="lblValidateStyleReq" runat="server" Text="**" ForeColor="Red" Visible="false"></asp:Label>
                                                </td>

                                                <td class="auto-style3">

                                                    <asp:HiddenField ID="hfServiceTypeCode" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtServiceType" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Service Type" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="CreateRequest" ControlToValidate="txtServiceType" Text="*" Display="Dynamic" ErrorMessage="Please select service type!" />
                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnServiceType" runat="server" Text="Service Type" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtSkuReq" style="float: left; margin-top: 4px;">
                                                            SKU:</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSkuReq" runat="server" CssClass="form-control form-control-height text-style"
                                                        placeholder="Enter SKU" MaxLength="50" Width="170px" autocomplete="off" Height="20px"></asp:TextBox>
                                                    <asp:Label ID="lblValidateSKUReq" runat="server" Text="**" ForeColor="Red" Visible="false"></asp:Label>
                                                </td>

                                                <td class="auto-style3">

                                                    <asp:HiddenField ID="hfCategoryCode" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtCategory" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Category" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:HiddenField ID="hfServiceTimeName" runat="server" Value="" />
                                                    <asp:HiddenField ID="hfServiceTimeCode" runat="server" Value="0" />
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="CreateRequest" ControlToValidate="txtCategory" Text="*" Display="Dynamic" ErrorMessage="Please select Category!" />
                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnCategory" runat="server" Text="Category" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr >
                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtTotalQty" style="float: left; margin-top: 4px;">Total Quantity</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalQtyReq" runat="server" CssClass="form-control form-control-height text-style" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                                                        placeholder="Enter No. of Items" Width="170px" autocomplete="off" Height="20px" MaxLength="5"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="CreateRequest" ControlToValidate="txtTotalQtyReq" Text="*" Display="Dynamic" ErrorMessage="Please enter quantity!" />
                                                </td>

                                                <td class="auto-style3">
                                                    <asp:HiddenField ID="Loose" runat="server" Value="0" />
                                                    <asp:HiddenField ID="hfJewelryTypeCode" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtJewelryType" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Select Jewelry Type" Style="margin-top: 3px;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="CreateRequest" ControlToValidate="txtJewelryType" Text="*" Display="Dynamic" ErrorMessage="Please select Jewelry Type!" />
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <asp:Button ID="btnJewelryType" runat="server" Text="Jewelry Type" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr>
                                                <td rowspan="2">
                                                    <label class="control-label" for="txtSpecialInstruction" style="float: left; margin-top: 4px;">
                                                        Special Instructions :</label>
                                                </td>
                                                <td rowspan="2">
                                                    <asp:TextBox ID="txtSpecialInstruction" runat="server" CssClass="form-control form-control-height text-style"
                                                        MaxLength="150" TextMode="MultiLine" Height="100px" Width="300px" Style="margin-right: 30px;"></asp:TextBox>
                                                </td>


                                            </tr>

                                            <tr style="height:120px;">
                                                <td class="auto-style3" style="vertical-align:top!important;">
                                                    <asp:HiddenField ID="hdnPersonID" runat="server" Value="0" />
                                                    <asp:TextBox runat="server" ID="txtCustomerContact" Width="170px" autocomplete="off" Height="20px" onkeydown="return false" onpaste="return false" PlaceHolder="Contact Lookup" Style="margin-top: 3px;"></asp:TextBox>

                                                </td>
                                                <td valign="top" style="vertical-align: top!important;">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnCustomerContact" runat="server" Text="Customer Contact" Width="150px" class="btn btn-info btn-large"
                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                    </div>
                                                </td>
                                            </tr>
                                           
                                            <tr style="visibility: collapse;">
                                              
                                                <td >
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtAccountNumber" style="float: left; ">
                                                            Shipping Courier Account#:</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="form-control form-control-height text-style" Style="margin-top: 4px;"
                                                        placeholder="Enter Account#" Width="170px" autocomplete="off" Height="20px" MaxLength="100"></asp:TextBox>
                                                </td>

                                            </tr>
                                          
                                            <tr style="visibility: collapse;">
                                                <td>
                                                    <div class="form-group">
                                                        <label class="control-label" for="txtDestination" style="float: left; margin-top: 4px;">
                                                            Shipping Courier Destination:</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDestination" runat="server" CssClass="form-control form-control-height text-style"
                                                        Width="170px" autocomplete="off" Height="20px" MaxLength="50" onkeydown="return false" onpaste="return false"></asp:TextBox>
                                                    <asp:HiddenField ID="hfDestination" runat="server" Value="0" />
                                                </td>
                                            </tr>
                                           

                                        </table>
                                        
                                        <table style="float: right;">
                                            <tr>
                                                
                                                <td class="auto-style3">
                                                    <div class="form-group" style="text-align:right;">
                                                        <asp:Button ID="btnCreateRequest" runat="server" class="btn btn-info btn-large" Style="width: 150px; font-size: 17.5px;" Text="Submit" CausesValidation="false" Onclick="btnCreateRequest_Click"/>

                                                    </div>
                                                </td>

                                                <td class="auto-style3">
                                                    <div class="form-group" style="margin-bottom: 2px; text-align: left;padding-left:8px;">
                                                        <asp:Button ID="btnClear" runat="server" class="btn btn-info btn-large" Style="width: 150px; font-size: 17.5px;" Text="Clear" CausesValidation="False" OnClick="btnClear_Click" />
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                            </table>
								</asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                </ajaxToolkit:TabContainer>
               
               </td>
                <td style="text-align: left; vertical-align: top; width: 600px; font-family: Tahoma,Arial,sans-serif; padding-left: 15px; padding-top: 15px;">
                        <table>
                            <tr>
                                <td>
									<asp:ValidationSummary
										DisplayMode="BulletList"
										EnableClientScript="true"
										ValidationGroup="CreateRequest"
										runat="server" Width="260px" />
                        <asp:Label ID="lblCreateRequestMessege" runat="server" Style="color: red; padding-right: 60px;"></asp:Label>
                                </td>
                                </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="lnkView" runat="server" Text="View PDF" ToolTip="Get Request" CssClass="btn btn-info btn-large" OnClick="ViewPdf" OnClientClick="window.document.forms[0].target='_blank';"></asp:Button>
                                    <asp:Button ID="takeOutLnkView" runat="server" Text="View Receipt" ToolTip="View Receipt" CssClass="btn btn-info btn-large" OnClick="ViewTakeOutReceipt" OnClientClick="window.document.forms[0].target='_blank';"></asp:Button>
                                    <iframe src="#" id="iframePDFViewer" width="650" height="650"  runat="server" visible="false"></iframe>
                        <%--<asp:Literal ID="ltEmbed" runat="server" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">
                                    <asp:ListBox runat="server" ID="BulkGiveOutList" Rows="10" DataValueField="ID" OnSelectedIndexChanged="OnBulkGiveOutListChanged" AutoPostBack="True"
                                        DataTextField="RequestId" Visible ="false" />
                                    <asp:Label ID="lblBulkGiveOutTotal" runat="server" Style="color: red; padding-right: 20px;" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">
                                    <asp:ListBox runat="server" ID="lstRequests" Rows="10" DataValueField="ID" OnSelectedIndexChanged="OnRequestChanged" AutoPostBack="True"
                                        DataTextField="RequestId" Visible ="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="savedCarrierTxt" runat="server" Height="20px" Width="100px" Style="margin-right: 5px; padding-left: 0px;" Visible="false" Enabled="false"></asp:TextBox>
                                    <asp:TextBox ID="savedTrackingNumberTxt" runat="server" Height="20px" Width="350px" Style="margin-right: 5px; padding-left: 0px;" Visible="false" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                  
                </tr>
                </table>

                <asp:Panel runat="server" ID="pnlUploadSignedOrderReciept" CssClass="modal-dialog" style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel1">
                                <asp:Button Style="float: right" ID="Button4" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" />
                                <asp:Label ID="Label58" runat="server" Text="Upload / Download Signed Receipt"
                                    Style="float: none; padding-top: 5px; font-size: 15px; line-height: 25px;"></asp:Label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Take In</legend>
                                            <table border="1" cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label76" runat="server" Text="Select Scanned File (JPG/PNG Only) :-" Style="float: none; padding-top: 5px;"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadSignedOrderRecieptTakeIn" runat="server" Width="175px" OnUploadedComplete="AsyncFileUploadSignedOrderRecieptTakeIn_UploadedComplete"
                                                            ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" OnClientUploadComplete="UploadCompleteTakeIn" />

                                                    </td>
                                                    <td>
                                                        <asp:Button Style="float: right" ID="btnUploadTakeIn" runat="server" Text="Upload" CssClass="btn btn-info" CausesValidation="false" OnClick="btnUploadSignedOrderRecieptTakeIn_Click" Enabled="false" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblRecieptTakeIn" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Blue"></asp:Label></td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Give Out
                                            </legend>
                                            <table border="1" cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label68" runat="server" Text="Select Scanned File (JPG/PNG Only) :-" Style="float: none; padding-top: 5px;"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadSignedOrderRecieptGiveOut" runat="server" Width="175px" OnUploadedComplete="AsyncFileUploadSignedOrderRecieptGiveOut_UploadedComplete"
                                                            ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" OnClientUploadComplete="UploadCompleteGiveOut" />

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label67" runat="server" Text="" ForeColor="Blue"></asp:Label>
                                                        <asp:Button Style="float: right" ID="btnUploadGiveOut" runat="server" Text="Upload" CssClass="btn btn-info" CausesValidation="false" OnClick="btnUploadSignedOrderRecieptGiveOut_Click" Enabled="false" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblRecieptGiveOut" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Blue"></asp:Label>
                                                    </td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender17" runat="server" PopupControlID="pnlUploadSignedOrderReciept" TargetControlID="TabContainer1$TabPanel6$btnUploadSignedOrderReciept" CancelControlID="ctl00_SampleContent_ModalPopupExtender17_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <%--Error Message popup for Font All Tabs Start--%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" style="width: 410px; display: none; border: solid 2px lightgray;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />

                            <b>Information</b>

                        </div>
                    </asp:Panel>

                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                    </div>

                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />

                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                    PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup" Style="width: 430px; display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black; padding-top: 10px">
                        Do you want to save the order changes?
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClientClick="this.disabled=true;" UseSubmitBehavior="false" OnClick="OnQuesSaveYesClick" Text="Yes"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" Style="display: none"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close" Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog"
                    ID="SaveQPopupExtender" PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn">
                </ajaxToolkit:ModalPopupExtender>
                
                <%--signatures compare popup --%>                
		        <asp:Panel runat="server" ID="CompareSignaturesPanel" CssClass="modalPopup" Height="540px" Width="1280px" Style="display: none; padding-top:5px;">
                    <asp:HiddenField runat="server" ID="HiddenUrl123" ></asp:HiddenField>
                    <div style="text-align: center; vertical-align: middle; padding-top:50px;" >
                        <table width="100%">
                            <tr>
                                <td style="text-align: center; vertical-align: middle;"><asp:Image ID="SignaturePicture1333" runat="server" BorderWidth="1px" Width="600px"></asp:Image></td>
                                <td style="text-align: center; vertical-align: middle;"><asp:Image ID="SignaturePicture333" runat="server" BorderWidth="1px" Width="600px"></asp:Image></td>
                            </tr>
                            <tr><td><div style="padding-top:35px"> </div></td></tr>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;"><asp:Button ID="SignatureOkButton123" runat="server" Text="OK" CssClass="btn btn-large btn-info" Width="120px" /></td>
                                <td style="text-align: center; vertical-align: middle;"><asp:Button ID="SignatureFailedButton123" runat="server" Text="Failed" OnClick="SignatureFailed_Click" CssClass="btn btn-large btn-info" Width="120px" /></td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenSignaturesCompareBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenSignaturesCompareBtn" PopupControlID="CompareSignaturesPanel" ID="SignaturesComparePopupExtender" 
                    OkControlID="SignatureOkButton123" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlServiceTimeTakeIn" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label4" Text="Service Time" Font-Bold="True"></asp:Label>

                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2" class="radioButtonList">
                                <asp:RadioButtonList ID="rdlServiceTimeTakeIn" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdbServiceTimeTakeIn_IndexChanged"
                                    CellPadding="3" CellSpacing="0" RepeatColumns="3" Font-Size="X-Small" DataValueField="ServiceTimeID"
                                    DataTextField="ServiceTimeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="pnlServiceTimeTakeIn"
                    TargetControlID="TabContainer1$TabPanel1$btnServiceTimeTakeIn" CancelControlID="ctl00_SampleContent_ModalPopupExtender4_backgroundElement"
                    Enabled="True" DropShadow="True">
                </ajaxToolkit:ModalPopupExtender>

                 <asp:Panel runat="server" ID="pnlSubmissionServiceTime" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label74" Text="Service Time" Font-Bold="True"></asp:Label>

                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <%--<td colspan="2" class="radioButtonList">--%>
                                <asp:RadioButtonList ID="rdlSubmissionServiceTime" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlSubmissionServiceTime_IndexChanged"
                                    CellPadding="3" CellSpacing="0" RepeatColumns="3" Font-Size="X-Small" DataValueField="ServiceTimeID"
                                    DataTextField="ServiceTimeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender7" runat="server" PopupControlID="pnlSubmissionServiceTime"
                    TargetControlID="TabContainer1$TabPanel3$btnSubmissionServiceTime" CancelControlID="ctl00_SampleContent_ModalPopupExtender7_backgroundElement"
                    Enabled="True" DropShadow="True">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="pnlMessengerLookupGiveOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label22" Text="Messenger" Font-Bold="true"> </asp:Label>
                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField runat="server" ID="hdnMessengerLookupGiveOut"></asp:HiddenField>
                                <asp:RadioButtonList ID="rblMessengerLookupGiveOut" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rblMessengerLookupGiveOut_Changed"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="PersonId"
                                    DataTextField="DisplayName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="Label24" runat="server" Text="No Messenger available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender9" runat="server" PopupControlID="pnlMessengerLookupGiveOut" TargetControlID="TabContainer1$TabPanel2$btnMessengerLookupGiveOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender9_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlCarriersShipOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label34" Text="Messenger" Font-Bold="true"> </asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlCarriersShipOut" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlCarriersShipOut_Changed"
                                    CellPadding="2" CellSpacing="0" RepeatColumns="4" Font-Size="X-Small" DataValueField="CarrierID"
                                    DataTextField="CarrierName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="Label35" runat="server" Text="No Messenger available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender14" runat="server" PopupControlID="pnlCarriersShipOut" TargetControlID="TabContainer1$TabPanel2$btnCarriersShipOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender14_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>


                <%--Create Request Pop-ups start--%>
                <asp:Panel runat="server" ID="plnCustomerContact" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label14" Text="Customer Contact" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlCustomerContact" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlCustomerContact_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="PersonId"
                                    DataTextField="FullName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblCustomerContactMsg" runat="server" Text="No Contact available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="plnCustomerContact" TargetControlID="TabContainer1$TabPanel5$btnCustomerContact" CancelControlID="ctl00_SampleContent_ModalPopupExtender1_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="plnDeliveryMethod" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label15" Text="Delivery Method" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlDeliveryMethod" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlDeliveryMethod_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="DeliveryMethodCode"
                                    DataTextField="DeliveryMethodName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblDeliveryMethodMsg" runat="server" Text="No delivery method available." Visible="false"></asp:Label>
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="plnDeliveryMethod" TargetControlID="TabContainer1$TabPanel5$btnDeliveryMethod" CancelControlID="ctl00_SampleContent_ModalPopupExtender2_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="plnJewelryType" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label17" Text="Jewelry Type" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlJewelryType" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlJewelryType_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="JewelryTypeCode"
                                    DataTextField="JewelryTypeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblJewelryTypeMsg" runat="server" Text="No jewelry type available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender5" runat="server" PopupControlID="plnJewelryType" TargetControlID="TabContainer1$TabPanel5$btnJewelryType" CancelControlID="ctl00_SampleContent_ModalPopupExtender5_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="plnShippingCourier" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label19" Text="Shipping Courier" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlShippingCourier" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlShippingCourier_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="CarrierCode"
                                    DataTextField="CarrierName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblShippingCourierMsg" runat="server" Text="No shipping courier available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender6" runat="server" PopupControlID="plnShippingCourier" TargetControlID="TabContainer1$TabPanel5$btnShippingCourier" CancelControlID="ctl00_SampleContent_ModalPopupExtender6_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="plnRetailer" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label23" Text="Retailer" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlRetailer" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlRetailer_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="RetailerID"
                                    DataTextField="RetailerName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblRetailerMsg" runat="server" Text="No retailer available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender11" runat="server" PopupControlID="plnRetailer" TargetControlID="TabContainer1$TabPanel5$btnRetailer" CancelControlID="ctl00_SampleContent_ModalPopupExtender11_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="pnlServiceType" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label16" Text="Service Type" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlServiceType" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlServiceType_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="ServiceTypeCode"
                                    DataTextField="ServiceTypeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblServiceTypeMsg" runat="server" Text="No service type available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender12" runat="server" PopupControlID="pnlServiceType" TargetControlID="TabContainer1$TabPanel5$btnServiceType" CancelControlID="ctl00_SampleContent_ModalPopupExtender12_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="plnCategory" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label18" Text="Category" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlCategory" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 10px" AutoPostBack="True" OnSelectedIndexChanged="rdlCategory_SelectedIndexChanged"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="CategoryCode"
                                    DataTextField="CategoryName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblCategoryMsg" runat="server" Text="No category available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender18" runat="server" PopupControlID="plnCategory" TargetControlID="TabContainer1$TabPanel5$btnCategory" CancelControlID="ctl00_SampleContent_ModalPopupExtender18_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="pnlDepartureSetting" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel3">
                                <asp:Label ID="Label30" runat="server" Text="Departure Setting" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="btnModelCancelDeparture" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" />
                            </div>
                        </div>
                        <div class="modal-body" style="overflow: inherit;">
                            <table>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label31" Text="" Font-Bold="true"> </asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label32" Text="Select Destination" Font-Bold="true"> </asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtShippingDestination" OnTextChanged="txtShippingDestination_TextChanged"
                                            autocomplete="off" Height="20px" Width="320px"></asp:TextBox>
                                         <ajaxToolkit:DropDownExtender ID="DropDownExtender2" runat="server" TargetControlID="txtShippingDestination" DropDownControlID="DropDownListPanel" Enabled="True" DynamicServicePath="" />
                                        <asp:Panel runat="server" ID="DropDownListPanel" Style="visibility: hidden;">
                                            <asp:ListBox runat="server" ID="lstDestination" DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                                                OnSelectedIndexChanged="OnCustomerReqSelectedChangedPopup" Rows="20" Width="335px" CssClass="DropDownListBoxStyle"></asp:ListBox>
                                            <br />
                                            <asp:DropDownList ID="ddlDestination" runat="server" AutoPostBack="True" DataTextField="CustomerName"
                                                DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnCustomerReqSelectedChangedPopup" ToolTip="Customers List" Visible="False">
                                            </asp:DropDownList>
                                        </asp:Panel>

                                    </td>

                                </tr>
                                <tr>
                                    <td style="float: right;">
                                        <asp:Button ID="btnSetDepartureSetting" runat="server" Text="Set" CssClass="btn btn-info" CausesValidation="false"
                                            OnClick="btnSetDepartureSetting_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                            <div class="modal-title" id="departureFooter">
                                <asp:Button ID="btnAddDestination" runat="server" Text="Add New Destination" class="btn btn-info" OnClick="btnAddDestination_OnClick"
                                    Style="float: left; margin-top: 5px;" CausesValidation="false" />
                                <input type="button" runat="server" id="hdnbtnAddDestination" style="display: none" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modelDepartureSetting" runat="server" PopupControlID="pnlDepartureSetting" TargetControlID="TabContainer1$TabPanel5$hdnbtnDeparture"
                    CancelControlID="btnModelCancelDeparture" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlDepartureSetting" TargetControlID="TabContainer1$TabPanel2$btnDepartureSettingShipOut"
                    CancelControlID="btnModelCancelDeparture" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
               

                <asp:Panel runat="server" ID="pnlAddCustomer" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" style="height:25px;">
                                <asp:Label ID="Label3" runat="server" Text="Add Destination" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="btnCancelCutomerContact" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" />
                            </div>
                        </div>
                        <div class="modal-body" style="background-color: white; overflow: hidden;">
                            <table style="height: 320px;">
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table style="width: 600px;">
                                            <tr>
                                                <td style="vertical-align: top; width: 400px;" rowspan="2">
                                                    <fieldset class="filedSetBorder" style="height: 250px;">
                                                        <legend class="label">Destination</legend>
                                                        <table style="width: 600px;">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>Delivery Method</td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="TextBox1" MaxLength="250" Text="Shipping" onpaste="return false" onkeydown="return false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 100px;">Company Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyName" runat="server" ControlToValidate="CompanyName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Company Name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompanyName" MaxLength="250" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Short Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyShortName" runat="server" ControlToValidate="ShortName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Short Name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="ShortName" MaxLength="250" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Address1
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddr1" runat="server" ControlToValidate="CompanyAddress1"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Address1 is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompanyAddress1" MaxLength="250" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Address2
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompanyAddress2" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>City
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyCity" runat="server" ControlToValidate="CompanyCity"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="City is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompanyCity" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="2px">
                                                                                    <tr>
                                                                                        <td style="width: 100px;">
                                                                                            <asp:Label ID="Label7" runat="server" Text="Country   "></asp:Label>
                                                                                            <asp:RequiredFieldValidator ID="CustomerCountryValidator" runat="server" ControlToValidate="ddlCustomerCountry"
                                                                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ForeColor="Red" ToolTip="Customer Country is required"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="padding-top: 2px; text-align: right; padding-right: 3px;">
                                                                                            <asp:DropDownList ID="ddlCustomerCountry" runat="server" Width="175px" AutoPostBack="true" Height="23px"
                                                                                                DataTextField="CountryCode" DataValueField="CountryID" Style="margin-right: -3px;"
                                                                                                OnSelectedIndexChanged="ddlCustomerCountry_SelectedIndexChanged">
                                                                                            </asp:DropDownList>

                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="Label10" runat="server" Text="State"></asp:Label>
                                                                                            <asp:RequiredFieldValidator ID="CompUsStateValidator" runat="server" ControlToValidate="ddlCustomerState"
                                                                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ForeColor="Red" ToolTip="Customer State is required"></asp:RequiredFieldValidator>

                                                                                        </td>
                                                                                        <td style="text-align: right; padding-right: 3px;">

                                                                                            <asp:DropDownList ID="ddlCustomerState" runat="server" Width="175px" Height="23px" Style="margin-right: -3px;"
                                                                                                DataTextField="StateCode" DataValueField="StateId">
                                                                                            </asp:DropDownList>

                                                                                        </td>

                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>


                                                                    </table>

                                                                </td>
                                                                <td style="vertical-align: top;">
                                                                    <table>
                                                                        <tr style="visibility: collapse;">
                                                                            <td width="95px">
                                                                                <asp:Label ID="Label20" Text="ID" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CustomerCode" Enabled="False"></asp:TextBox>
                                                                                <asp:TextBox runat="server" ID="CustomerId" Style="display: none"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: collapse;">
                                                                            <td>Start Date
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CustomerStartDate" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: collapse;">
                                                                            <td style="height: 0px; width: 101px;">
                                                                                <asp:Label ID="Label21" runat="server" Text="BusinessType" Visible="false"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlCustomerBusinessType" runat="server" DataTextField="BusinessTypeName" Width="173px" Height="26px"
                                                                                    DataValueField="BusinessTypeId" Enabled="false" Visible="false">
                                                                                </asp:DropDownList>

                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>Zip
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidatorZip" runat="server" ControlToValidate="CompanyZip1"
                                                                              ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ForeColor="Red" ToolTip="Zip is required"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Panel ID="Panel4" runat="server" CssClass="form-inline" Style="float: right;">
                                                                                    <asp:TextBox runat="server" ID="CompanyZip1" Width="110px" MaxLength="13" TargetControlID="CompanyZip1"></asp:TextBox>
                                                                                    <asp:TextBox runat="server" ID="CompanyZip2" Width="32px" MaxLength="5"></asp:TextBox>

                                                                                </asp:Panel>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Phone
																		<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ForeColor="Red" ControlToValidate="CompanyPhoneCode"
                                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Country Code is required"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Panel ID="Panel7" runat="server" CssClass="form-inline" Style="float: right;">

                                                                                    <asp:TextBox runat="server" ID="CompanyPhoneCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom" TargetControlID="CompanyPhoneCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                                    <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="CompanyPhoneCode" Display="Dynamic" />
                                                                                    <asp:TextBox runat="server" ID="CompanyPhone" Width="112px" MaxLength="20"></asp:TextBox>
                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers" TargetControlID="CompanyPhone"></ajaxToolkit:FilteredTextBoxExtender>

                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>Fax
																		<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ForeColor="Red" ControlToValidate="CompanyFaxCode"
                                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Country Code is required"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Panel ID="Panel8" runat="server" CssClass="form-inline" Style="float: right;">

                                                                                    <asp:TextBox runat="server" ID="CompanyFaxCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom" TargetControlID="CompanyFaxCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                                    <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="CompanyFaxCode" Display="Dynamic" />
                                                                                    <asp:TextBox runat="server" ID="CompanyFax" Width="112px" MaxLength="20"></asp:TextBox>
                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers" TargetControlID="CompanyFax"></ajaxToolkit:FilteredTextBoxExtender>

                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Email
																		
                                                                            </td>
                                                                            <td>

                                                                                <asp:TextBox runat="server" ID="CompanyEmail" MaxLength="50" TextMode="Email"></asp:TextBox>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Insurance</td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtInsurance" MaxLength="50"></asp:TextBox></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>Receiver</td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtReceiver" MaxLength="50"></asp:TextBox></td>

                                                                        </tr>


                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr style="height:60px;">
                                                                <td style="text-align: right; padding-top: 10px;" colspan="2">


                                                                    <asp:Label runat="server" ID="lblAddCustMessageLabel" ForeColor="Blue"></asp:Label>
                                                                    <asp:Button runat="server" ID="CustomerUpdateBtn" OnClick="OnUpdateCompanyClick" ValidationGroup="ValGrpCompany"
                                                                        Style="margin-right: -5px; margin-top: 0px" Text="Add Destination" CssClass="btn btn-info" />

                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </fieldset>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div class="modal-footer" style="visibility: hidden;">
                            <div class="modal-title" id="AddCustomerFooter" runat="server" style="color: red; font-weight: 100; text-align: left; height: max-content; font-size: 14px; height: 10px;">
                                <input type="button" runat="server" id="hdnbtnAddNewDestination" style="display: none" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <ajaxToolkit:ModalPopupExtender ID="modelAddCustomer" runat="server" PopupControlID="pnlAddCustomer" TargetControlID="hdnbtnAddNewDestination"
                    CancelControlID="btnCancelCutomerContact" Enabled="True" DropShadow="true" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>

            
                </div>
				<%--<%--signatures compare popup --%>                
				<%--<asp:Panel runat="server" ID="Panel10" CssClass="modalPopup" Height="850px" Width="600px" Style="display: none">
                	<asp:HiddenField runat="server" ID="HiddenUrl1" ></asp:HiddenField>
                	<div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Image ID="SignaturePicture1" runat="server" BorderWidth="1px" ></asp:Image>
                        </p>
                        <p style="text-align: center;">
                            <asp:Image ID="SignaturePicture2" runat="server" BorderWidth="1px" ></asp:Image>
                        </p>
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="SignatureOkButton" runat="server" Text="OK" />
                            <asp:Button ID="SignatureFailedButton" runat="server" Text="Failed" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenSignaturesCompareBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenSignaturesCompareBtn" PopupControlID="Panel10" ID="SignaturesComparePopupExtender" 
                    OkControlID="SignatureOkButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>   --%>
            </ContentTemplate>


        </asp:UpdatePanel>

    </div>
</asp:Content>
