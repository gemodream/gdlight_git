<%@ Page language="c#" Codebehind="OutsideServices.aspx.cs" AutoEventWireup="True" Inherits="Corpt.OutsideServices" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OutsideServices</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:dropdownlist style="Z-INDEX: 102; POSITION: absolute; TOP: 48px; LEFT: 104px" id="lstService"
				Runat="server" CssClass="inputStyleCC5" Width="160px">
				<asp:ListItem Value="AVI polish">AVI polish</asp:ListItem>
				<asp:ListItem Value="PhotoScribe Laser">PhotoScribe Laser</asp:ListItem>
				<asp:ListItem Value="Photoscribe Engraving">Photoscribe Engraving</asp:ListItem>
				<asp:ListItem Value="AVI Recut">AVI Recut</asp:ListItem>
			</asp:dropdownlist>
			<asp:button style="Z-INDEX: 113; POSITION: absolute; TOP: 672px; LEFT: 272px" id="cmdSave" runat="server"
				CssClass="buttonStyle" Width="88px" Text="Save"></asp:button>
			<asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 240px; LEFT: 48px" id="Label4" runat="server"
				CssClass="text">Item Number</asp:label>
			<asp:listbox style="Z-INDEX: 108; POSITION: absolute; TOP: 264px; LEFT: 144px" id="Listbox1"
				runat="server" CssClass="text" Width="112px" AutoPostBack="True" Height="432px"></asp:listbox>
			<asp:TextBox style="Z-INDEX: 111; POSITION: absolute; TOP: 240px; LEFT: 144px" id="txtItemNumberDO"
				runat="server" CssClass="inputStyleCC" AutoPostBack="True"></asp:TextBox>
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 240px; LEFT: 48px" id="Label3" runat="server"
				CssClass="text">Item Number</asp:label>
			<asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 128px; LEFT: 304px" id="cmdOpenBatch"
				runat="server" CssClass="buttonStyle" Width="88px" Text="Open Batch"></asp:button>
			<asp:listbox style="Z-INDEX: 107; POSITION: absolute; TOP: 264px; LEFT: 144px" id="lstItemList"
				runat="server" CssClass="text" Width="112px" AutoPostBack="True" Height="432px"></asp:listbox>
			<asp:TextBox style="Z-INDEX: 106; POSITION: absolute; TOP: 128px; LEFT: 144px" id="txtBatchNumber"
				runat="server" CssClass="inputStyleCC" AutoPostBack="True"></asp:TextBox>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 128px; LEFT: 48px" id="Label2" runat="server"
				CssClass="text">Batch Number</asp:label>
			<asp:label style="Z-INDEX: 100; POSITION: absolute; TOP: 48px; LEFT: 40px" id="Label1" runat="server"
				CssClass="text">Service</asp:label>
			<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 48px; LEFT: 336px" id="lblInfo" runat="server"
				CssClass="text"></asp:label>
			<asp:RadioButtonList style="Z-INDEX: 112; POSITION: absolute; TOP: 168px; LEFT: 152px" id="rblMode" runat="server"
				Width="136px">
				<asp:ListItem Value="0">Going Out</asp:ListItem>
				<asp:ListItem Value="1">Receiving</asp:ListItem>
			</asp:RadioButtonList>
		</form>
	</body>
</HTML>
