﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningTester.aspx.cs" Inherits="Corpt.ScreeningTester" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

            input[type="checkbox"], label {
                margin-top: 0;
                margin-bottom: 0;
                line-height: normal;
            }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }
    </style>

    <script type="text/javascript">
		<%--$().ready(function () {
			$('#<%=txtOrderCode.ClientID%>, #<%=txtFailedStonePerItems.ClientID%>, #<%=txtFiledStones.ClientID%>, #<%=txtItemwithFailStone.ClientID%>, #<%=txtSuspectedStone.ClientID%>, #<%=txtSyntheticStone.ClientID%>').keypress(function (event) {
				return isOnlyNumber(event, this);
			});
		});
		function isOnlyNumber(evt, element) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode < 48 || charCode > 57)
				return false;
			return true;
		}--%>
    </script>

    <asp:UpdatePanel runat="server" ID="MainPanel">
        <ContentTemplate>
            <div class="demoarea">
                <div style="font-size: smaller; height: 25px;">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                        <ProgressTemplate>
                            <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                            <b>Please, wait....</b>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="demoarea">


                    <table>
                        <tr>
                            <td>
                                <div class="demoheading">
                                    Tester View
						            <br />
                                </div>
                            </td>

                            <td colspan="2">
                                <div class="demoheading">
                                    <asp:Label runat="server" ID="lblInvalidLabel" ForeColor="Red" Style="padding-left: 10px" Width="400px"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; height: 30px;">
                                <asp:TextBox ID="txtOrderCode" runat="server" MaxLength="7" placeholder="Order code" Style="width: 115px;" OnFocus="this.select();"
                                    autocomplete="off" onkeypress="return event.charCode >= 48 && event.charCode <= 57" ValidationGroup="BatchGroup"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderCode"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
                                    ValidationGroup="BatchGroup" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                    HighlightCssClass="validatorCalloutHighlight" />
                                <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderCode"
                                    Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>six or seven numeric characters</strong>"
                                    ValidationGroup="BatchGroup" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                    HighlightCssClass="validatorCalloutHighlight" />
								<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers" TargetControlID="txtOrderCode"></ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                            <td style="vertical-align: top; height: 30px; width: 160px;">
                                <asp:Button ID="btnLoadBatch" runat="server" Text="Load" CssClass="btn btn-info btn-large" Style="margin-bottom: 0px; margin-left: 10px;" OnClick="btnLoadBatch_Click" />
                            </td>
                            <td rowspan="2" style="vertical-align: top;">
                                <table id="tblDetail" cellpadding="3" runat="server" style="width: 600px;" visible="false">
                                    <tr>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lblBatch" runat="server" Text="Data For" Font-Bold="True" Font-Size="16px" CssClass="label"></asp:Label>
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info btn-large" Style="margin-right: 10px;" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-info btn-large" Text="Clear" Style="margin-right: 10px;" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnPrintLabel" runat="server" Text="Print Label" CssClass="btn btn-info btn-large" OnClick="btnPrintLabel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="Total number of items"></asp:Label>
                                        </td>
                                        <td style="vertical-align: middle; height: 40px;">
                                            <asp:Label ID="lblNoofItems" runat="server" Text="0" Style="padding-bottom: 10px;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text="Screening Instrument"></asp:Label>
                                        </td>
                                        <td style="vertical-align: middle; height: 40px;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTestingInstrument" runat="server" Enabled="false"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnTestingInstrumentCode" runat="server" Value="0" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnScreeningInstrument" runat="server" CssClass="btn btn-info btn-large"
                                                            Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" Text="Screening Instrument" />

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="Number of synthetic stones"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSyntheticStone" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text="Number of suspected stones"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSuspectedStone" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="Number of items with failed stones"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtItemwithFailStone" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label8" runat="server" Text="Number of failed stones"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFiledStones" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text="Number of failed stones per items"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFailedStonePerItems" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <asp:Label ID="Label7" runat="server" Text="Comment"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtComment" runat="server" Height="70px" TextMode="MultiLine" Width="200px" MaxLength="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align: right;"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <asp:ListBox ID="lstOrderBatchlist" runat="server" Height="200px" Width="130px"
                                    DataTextField="FullBatchNumber" DataValueField="BatchId" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="lstOrderBatchlist_SelectedIndexChanged"></asp:ListBox>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <%-- Screening Instrument Dialog --%>
                <asp:Panel runat="server" ID="pnlScreeningInstrument" CssClass="modal-dialog" Width="460px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label1" Text="Screening Instrument"></asp:Label>
                            </td>
                            <td><%-- --%>
                                <asp:DropDownList ID="ddlScreeningInstrument" runat="server" DataTextField="InstrumentName" OnSelectedIndexChanged="ddlScreeningInstrument_SelectedIndexChanged" AutoPostBack="true"
                                    DataValueField="InstrumentCode" Width="150px" Height="26px">
                                </asp:DropDownList>

                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repScreeningInstrument" runat="server" OnItemCommand="repScreeningInstrument_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnCustomerBusinessType" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="InstrumentCode"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InstrumentCode")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlScreeningInstrument" TargetControlID="btnScreeningInstrument" CancelControlID="ctl00_SampleContent_ModalPopupExtender3_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray; margin-top: 25%;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
                        id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif;">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
                </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrintLabel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
