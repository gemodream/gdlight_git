﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ItemHistory.aspx.cs" Inherits="Corpt.ItemHistory" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Item History</div>
        <div class="navbar nav-tabs form-inline">
        <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadButton" Style="font-size: small">
            <asp:Label ID="Label1" runat="server" Text="Order/Batch/Item number"></asp:Label>
            <asp:TextBox runat="server" ID="ItemField"></asp:TextBox>
            <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item History" ImageUrl="~/Images/ajaxImages/search.png"
                OnClick="OnLoadClick" />
            From
            <asp:TextBox runat="server" ID="DateFrom" OnTextChanged="OnChangedDateFrom"
                AutoPostBack="True">
            </asp:TextBox>
            To
            <asp:TextBox runat="server" ID="DateTo" OnTextChanged="OnChangedDateTo"
                AutoPostBack="True">
            </asp:TextBox>
            Author
            <asp:DropDownList ID="AuthorList" runat="server" DataTextField="Name" DataValueField="AuthorOfficeIdAuthorId"
                Height="26px" Width="150px">
            </asp:DropDownList>
            <br/>
            <ajaxToolkit:CalendarExtender ID="CalendarFrom" runat="server" ClearTime="True" TargetControlID="DateFrom">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CalendarTo" runat="server" ClearTime="True" TargetControlID="DateTo">
            </ajaxToolkit:CalendarExtender>
        </asp:Panel>
        </div>
        <div>
            <asp:DataGrid runat="server" ID="HistoryGrid" AutoGenerateColumns="False" CellPadding="3"
                CellSpacing="3" ForeColor="Black" AllowSorting="true" OnSortCommand="sort_table">
                <Columns>
                    <asp:BoundColumn DataField="NewNumber" HeaderText="New #" SortExpression="NewNumber"></asp:BoundColumn>
                    <asp:BoundColumn DataField="OldNumber" HeaderText="Old #" SortExpression="OldNumber"></asp:BoundColumn>
                    <asp:BoundColumn DataField="StartDate" HeaderText="Start Date" SortExpression="StartDate"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PartName" HeaderText="Part Name" SortExpression="PartName"></asp:BoundColumn>
                    <asp:BoundColumn DataField="MeasureName" HeaderText="Measure Name" SortExpression="MeasureName"></asp:BoundColumn>
                    <asp:BoundColumn DataField="RecheckNumber" HeaderText="R/c"></asp:BoundColumn>
                    <asp:BoundColumn DataField="OldValue" HeaderText="Old Value" />
                    <asp:BoundColumn DataField="NewValue" HeaderText="New Value" />
                    <asp:BoundColumn DataField="Person" HeaderText="User" SortExpression="Person" />
                </Columns>
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>
