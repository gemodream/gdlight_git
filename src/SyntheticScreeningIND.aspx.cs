﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;

namespace Corpt
{
    public partial class SyntheticScreeningIND : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid(0);
                InsertBlankRecord();
                gvTitle.InnerText = "Screening Result - Top 50";
                txtNotes.Value = "";
                LoadRetailer();
                LoadDestination();
                LoadCertifiedBy();
            }

            this.Form.DefaultFocus = txtGsiOrder.ClientID;
            this.Form.DefaultButton = cmdLoadBatch.UniqueID;
        }
        public void LoadRetailer()
		{
            DataTable dtRetailers = SyntheticScreeningUtils.GetAllRetailers(this);
            ddlDestinationSearch.DataSource = dtRetailers;
            ddlDestinationSearch.DataTextField = "RetailerName";
            ddlDestinationSearch.DataValueField = "RetailerID";
            ddlDestinationSearch.DataBind();
            ListItem liRetailer = new ListItem();
            liRetailer.Text = "";
            liRetailer.Value = "0";
            ddlDestinationSearch.Items.Insert(0, liRetailer);
            ddlDestinationSearch.ClearSelection();
        }
        
        public void LoadDestination()
        {
            DataTable dtDestination = SyntheticScreeningUtils.GetAllRetailers(this);
            ddlDestination.DataSource = dtDestination;
            ddlDestination.DataTextField = "RetailerName";
            ddlDestination.DataValueField = "RetailerID";
            ddlDestination.DataBind();
            ListItem liDestination = new ListItem();
            liDestination.Text = "";
            liDestination.Value = "0";
            ddlDestination.Items.Insert(0, liDestination);
            ddlDestination.ClearSelection();
        }

        public void LoadCertifiedBy()
        {
            DataTable dtCertifiedBy = SyntheticScreeningUtils.GetAllSyntheticCertifiedBy(this);
            ddlCertifiedBy.DataSource = dtCertifiedBy;
            ddlCertifiedBy.DataTextField = "CertifiedBy";
            ddlCertifiedBy.DataValueField = "CertifiedID";
            ddlCertifiedBy.DataBind();
            ListItem liCertifiedBy = new ListItem();
            liCertifiedBy.Text = "";
            liCertifiedBy.Value = "0";
            ddlCertifiedBy.Items.Insert(0, liCertifiedBy);
            ddlCertifiedBy.ClearSelection();
        }



        protected void OnOrderCodeLoadClick(object sender, EventArgs e)
        {
            hdnScreeningID.Value = "0";
            DataSet dsOrderDetail = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", "", txtGsiOrder.Text, this);
            if (dsOrderDetail.Tables[0].Rows.Count == 1)
            {
                lblRequestID.Text = dsOrderDetail.Tables[0].Rows[0]["RequestID"].ToString().Trim();
                lblTotalQTY.Text = dsOrderDetail.Tables[0].Rows[0]["TotalQty"].ToString().Trim();
                lblCustomerCode.Text = dsOrderDetail.Tables[0].Rows[0]["CustomerCode"].ToString().Trim();
                ddlDestination.SelectedItem.Text = dsOrderDetail.Tables[0].Rows[0]["RetailerName"].ToString().Trim();
                lblMemoNum.Text = dsOrderDetail.Tables[0].Rows[0]["MemoNum"].ToString().Trim();
                txtPONum.Text = dsOrderDetail.Tables[0].Rows[0]["PONum"].ToString().Trim();
                txtSKU.Text = dsOrderDetail.Tables[0].Rows[0]["SKUName"].ToString().Trim();
                lblStyle.Text = dsOrderDetail.Tables[0].Rows[0]["StyleName"].ToString().Trim();

                txtNotes.Value = dsOrderDetail.Tables[0].Rows[0]["SpecialInstructions"].ToString().Trim();
                hdnScreeningID.Value = dsOrderDetail.Tables[3].Rows[0]["ScreeningID"].ToString().Trim();

                if (hdnScreeningID.Value != "0")
                {
                    DataSet ds = GSIAppQueryUtils.GetScreeningDetailsByOrderIND(int.Parse(txtGsiOrder.Text.Trim()), this);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string[] testValues = ds.Tables[0].Rows[0]["Test"].ToString().Replace("&nbsp;", string.Empty).Split(',');
                        if (testValues.Contains(chkQCHK.Value)) chkQCHK.Checked = true; else chkQCHK.Checked = false;
                        if (testValues.Contains(chkDiamondView.Value)) chkDiamondView.Checked = true; else chkDiamondView.Checked = false;
                        if (testValues.Contains(chkYehuda.Value)) chkYehuda.Checked = true; else chkYehuda.Checked = false;
                        if (testValues.Contains(chkRaman.Value)) chkRaman.Checked = true; else chkRaman.Checked = false;
                        if (testValues.Contains(chkFTIR.Value)) chkFTIR.Checked = true; else chkFTIR.Checked = false;

                        ddlCertifiedBy.SelectedItem.Text = ds.Tables[0].Rows[0]["CertifiedBy"].ToString().Replace("&nbsp;", string.Empty);

                        txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString().Replace("&nbsp;", string.Empty);

                        txtFTQuantity.Value = ds.Tables[0].Rows[0]["FTQuantity"].ToString().Replace("&nbsp;", "0");
                        txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
                        txtQtyFail.Value = ds.Tables[0].Rows[0]["QtyFail"].ToString();

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            gvItems.DataSource = ds.Tables[1];
                            gvItems.DataBind();
                            Session["FailItems"] = ds.Tables[1];
                        }

                        btnSave.Text = "Update";
                    }
                }

            }
            else
            {
                hdnScreeningID.Value = "0";
                DataSet dsOrderDetailWithOutRequest = SyntheticScreeningUtils.GetSyntheticEntriesWithOutRequest(txtGsiOrder.Text, this);
                if (dsOrderDetailWithOutRequest.Tables[0].Rows.Count == 1)
                {
                    lblRequestID.Text = "";
                    lblTotalQTY.Text = dsOrderDetailWithOutRequest.Tables[0].Rows[0]["TotalQty"].ToString().Trim();
                    lblCustomerCode.Text = dsOrderDetailWithOutRequest.Tables[0].Rows[0]["CustomerCode"].ToString().Trim();
                    ddlDestination.SelectedItem.Text = "";
                    lblMemoNum.Text = dsOrderDetailWithOutRequest.Tables[0].Rows[0]["MemoNum"].ToString().Trim();
                    txtNotes.Value = dsOrderDetailWithOutRequest.Tables[0].Rows[0]["SpecialInstructions"].ToString().Trim();
                    hdnScreeningID.Value = dsOrderDetailWithOutRequest.Tables[1].Rows[0]["ScreeningID"].ToString().Trim();
                    txtPONum.Text = "";
                    txtSKU.Text = "";
                    lblStyle.Text = "";

                    if (hdnScreeningID.Value != "0")
                    {
                        DataSet ds = GSIAppQueryUtils.GetScreeningDetailsByOrderIND(int.Parse(txtGsiOrder.Text.Trim()), this);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string[] testValues = ds.Tables[0].Rows[0]["Test"].ToString().Replace("&nbsp;", string.Empty).Split(',');
                            if (testValues.Contains(chkQCHK.Value)) chkQCHK.Checked = true; else chkQCHK.Checked = false;
                            if (testValues.Contains(chkDiamondView.Value)) chkDiamondView.Checked = true; else chkDiamondView.Checked = false;
                            if (testValues.Contains(chkYehuda.Value)) chkYehuda.Checked = true; else chkYehuda.Checked = false;
                            if (testValues.Contains(chkRaman.Value)) chkRaman.Checked = true; else chkRaman.Checked = false;
                            if (testValues.Contains(chkFTIR.Value)) chkFTIR.Checked = true; else chkFTIR.Checked = false;

                            ddlCertifiedBy.SelectedItem.Text = ds.Tables[0].Rows[0]["CertifiedBy"].ToString().Replace("&nbsp;", string.Empty);

                            txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString().Replace("&nbsp;", string.Empty);

                            txtFTQuantity.Value = ds.Tables[0].Rows[0]["FTQuantity"].ToString().Replace("&nbsp;", "0");
                            txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
                            txtQtyFail.Value = ds.Tables[0].Rows[0]["QtyFail"].ToString();

                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                gvItems.DataSource = ds.Tables[1];
                                gvItems.DataBind();
                                Session["FailItems"] = ds.Tables[1];
                            }

                            btnSave.Text = "Update";
                        }
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Order does not exist');", true);
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string xmlItems = "";
            SyntheticScreeningINDModel synScrData = new SyntheticScreeningINDModel();
            synScrData.ScreeningID = Int64.Parse(hdnScreeningID.Value);
            synScrData.GSIOrder = int.Parse(txtGsiOrder.Text);
            synScrData.CustomerCode = lblCustomerCode.Text;
            synScrData.PONum = txtPONum.Text;
            synScrData.TotalQty = int.Parse(lblTotalQTY.Text);
            synScrData.QtyPass = int.Parse(txtQtyPass.Value);
            synScrData.QtyFail = int.Parse(txtQtyFail.Value);
            synScrData.FTQuantity = txtFTQuantity.Value.Trim() == string.Empty ? 0 : int.Parse(txtFTQuantity.Value);
            synScrData.SKUName = txtSKU.Text;
            synScrData.Notes = txtNotes.Value;
            synScrData.Destination = ddlDestination.SelectedItem.Text;
            string strTestData = "";
            if (chkQCHK.Checked == true) strTestData = strTestData + chkQCHK.Value + ",";
            if (chkDiamondView.Checked == true) strTestData = strTestData + chkDiamondView.Value + ",";
            if (chkYehuda.Checked == true) strTestData = strTestData + chkYehuda.Value + ",";
            if (chkRaman.Checked == true) strTestData = strTestData + chkRaman.Value + ",";
            if (chkFTIR.Checked == true) strTestData = strTestData + chkFTIR.Value + ",";
            synScrData.Test = strTestData.TrimEnd(',');
            synScrData.CertifiedBy = ddlCertifiedBy.SelectedItem.Text;

            DataSet ds = new DataSet();
            ds.DataSetName = "DocumentElement";
            ds.Tables.Add(((DataTable)Session["FailItems"]).Copy());
            ds.Tables[0].TableName = "SyntheticFailItems";
            if (ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Rows[0]["GSIItemNumber"].ToString() == "")
            {
                xmlItems = "";
            }
            else
            {
                xmlItems = ConvertDatatableToXML(ds);
            }
            string status = "";
            status = GSIAppQueryUtils.SaveScreeningDataIND(synScrData, xmlItems, this);
            if (status != "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Order " + synScrData.GSIOrder + " not save.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Order saved successfully.');", true);
                BindGrid(0);
                ClearText();
            }

            //if (IsOrderExist(int.Parse(txtGsiOrder.Text)) == true && lblRequestID.Text == string.Empty)
            //{
            //    ScriptManager.RegisterStartupScript(this.GetType(), "alert", "alert('Request is not exist for this Order.');", true);
            //}
            //else
            //{

            //}
        }

        // By using this method we can convert datatable to xml
        public string ConvertDatatableToXML(DataSet ds)
        {
            MemoryStream str = new MemoryStream();
            ds.Tables[0].WriteXml(str, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }

        private void BindGrid(int orderCode)
        {
            List<SyntheticScreeningINDModel> lstScreening = new List<SyntheticScreeningINDModel>();
            DataSet dsScr = new DataSet();
            lstScreening = GSIAppQueryUtils.GetScreeningByOrderIND(orderCode, this);
            dsScr.Tables.Add(ToDataTable<SyntheticScreeningINDModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }

        private void BindGrid(string strDestination)
        {
            List<SyntheticScreeningINDModel> lstScreening = new List<SyntheticScreeningINDModel>();
            DataSet dsScr = new DataSet();
            lstScreening = GSIAppQueryUtils.GetScreeningDetailsByDestinationIND(strDestination, this);
            dsScr.Tables.Add(ToDataTable<SyntheticScreeningINDModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }

        private void BindGrid(string strCreateFromDate, string strCreateToDate)
        {
            List<SyntheticScreeningINDModel> lstScreening = new List<SyntheticScreeningINDModel>();
            DataSet dsScr = new DataSet();
            DateTime dtFromDate = DateTime.Parse(strCreateFromDate, CultureInfo.CreateSpecificCulture("en-US"));
            DateTime dtToDate = DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
            lstScreening = GSIAppQueryUtils.GetScreeningByCreateDateIND(dtFromDate, dtToDate, this);
            dsScr.Tables.Add(ToDataTable<SyntheticScreeningINDModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }

        private bool IsOrderExist(int orderCode)
        {
            bool isOrderExist = false;
            if (GSIAppQueryUtils.GetScreeningByOrderIND(orderCode, this).Count > 0)
                isOrderExist = true;
            return isOrderExist;
        }

        private void GetOrderDetails(int orderCode)
        {
            List<SyntheticScreeningINDModel> lstScreening = new List<SyntheticScreeningINDModel>();
            DataSet dsScr = new DataSet();
            lstScreening = GSIAppQueryUtils.GetScreeningByOrderIND(orderCode, this);

            dsScr.Tables.Add(ToDataTable<SyntheticScreeningINDModel>(lstScreening));
            Session["ScreeningData"] = dsScr;

            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }

        private void ClearText()
        {
            lblRequestID.Text = string.Empty;
            txtGsiOrder.Text = string.Empty;
            lblCustomerCode.Text = string.Empty;
            txtPONum.Text = string.Empty;
            lblTotalQTY.Text = string.Empty;
            txtFTQuantity.Value = string.Empty;
            txtQtyPass.Value = string.Empty;
            txtQtyFail.Value = string.Empty;
            txtSKU.Text = string.Empty;
            txtNotes.Value = string.Empty;
            ddlDestination.SelectedIndex = 0;
            lblMemoNum.Text = string.Empty;
            lblStyle.Text = string.Empty;
            chkQCHK.Checked = false;
            chkDiamondView.Checked = false;
            chkRaman.Checked = false;
            chkYehuda.Checked = false;
            chkFTIR.Checked = false;
            btnSave.Text = "Add New";
            grdScreening.SelectedIndex = -1;
            ddlCertifiedBy.SelectedIndex = 0;            
            txtItemQTYFail.Value = string.Empty;
            txtGSIItemNumber.Value = string.Empty;
            InsertBlankRecord();
        }

        private void ClearFilterText()
        {
            gsiOrderFilter.Value = string.Empty;
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            ddlDestinationSearch.SelectedIndex = 0;
            BindGrid(0);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            dataTable.Columns.Remove("Notes");
            return dataTable;
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //--Remove work column
                //foreach (var hiddenColumn in GetHiddenColumns())
                //{
                //    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
                //}

                if (dataTable.Columns.Contains("ScreeningID")) dataTable.Columns.Remove("ScreeningID");

                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }


            //-- Save File
            //var dir = GetExportDirectory(p);

            //var filename = fname + ".xlsx";
            //var sw = File.Create(dir + filename);
            //workbook.Write(sw);
            //sw.Close();
            //if (!skipDownload) DownloadExcelFile(filename, p);

            var filename = fname + ".xlsx";
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p); 

            Memory.Flush();
        }

        public static void DownloadExcelFile(String filename, Page p)
        {
            var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            //p.Response.AddHeader("Refresh", "0.1");
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            p.Response.End();
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            if (!forHeader)
            {
                //style.WrapText = true;
            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Center;
            return style;
        }

        protected void grdScreening_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //ClearText();
            grdScreening.PageIndex = e.NewPageIndex;
            if (txtCreateDateSearch.Value.Trim() != string.Empty && txtToCreateDateSearch.Value.Trim() != string.Empty)
                BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim());
            else if (ddlDestinationSearch.SelectedIndex != 0)
                BindGrid(ddlDestinationSearch.SelectedItem.Value);
            else
                BindGrid(int.Parse(gsiOrderFilter.Value == string.Empty ? "0" : gsiOrderFilter.Value));

        }

        protected void grdScreening_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = GSIAppQueryUtils.GetScreeningDetailsByOrderIND(int.Parse(grdScreening.SelectedRow.Cells[3].Text.Trim()), this);

            txtGsiOrder.Text = ds.Tables[0].Rows[0]["GSIOrder"].ToString();
            hdnScreeningID.Value = ds.Tables[0].Rows[0]["ScreeningID"].ToString();
            lblRequestID.Text = ds.Tables[0].Rows[0]["RequestID"].ToString();            
            lblCustomerCode.Text = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
            txtPONum.Text = ds.Tables[0].Rows[0]["PONum"].ToString();
            txtSKU.Text = ds.Tables[0].Rows[0]["SKUName"].ToString();
            lblStyle.Text = ds.Tables[0].Rows[0]["StyleName"].ToString();
            lblMemoNum.Text = ds.Tables[0].Rows[0]["MemoNum"].ToString();
            ddlDestination.SelectedItem.Text = ds.Tables[0].Rows[0]["Destination"].ToString();


            string[] testValues = ds.Tables[0].Rows[0]["Test"].ToString().Replace("&nbsp;", string.Empty).Split(',');
            if (testValues.Contains(chkQCHK.Value)) chkQCHK.Checked = true; else chkQCHK.Checked = false;
            if (testValues.Contains(chkDiamondView.Value)) chkDiamondView.Checked = true; else chkDiamondView.Checked = false;
            if (testValues.Contains(chkYehuda.Value)) chkYehuda.Checked = true; else chkYehuda.Checked = false;
            if (testValues.Contains(chkRaman.Value)) chkRaman.Checked = true; else chkRaman.Checked = false;
            if (testValues.Contains(chkFTIR.Value)) chkFTIR.Checked = true; else chkFTIR.Checked = false;

            ddlCertifiedBy.SelectedItem.Text = ds.Tables[0].Rows[0]["CertifiedBy"].ToString().Replace("&nbsp;", string.Empty);

            txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString().Replace("&nbsp;", string.Empty);
            lblTotalQTY.Text = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            txtFTQuantity.Value = ds.Tables[0].Rows[0]["FTQuantity"].ToString().Replace("&nbsp;", "0");
            txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
            txtQtyFail.Value = ds.Tables[0].Rows[0]["QtyFail"].ToString();

            if (ds.Tables[1].Rows.Count > 0)
            {
                gvItems.DataSource = ds.Tables[1];
                gvItems.DataBind();
                Session["FailItems"] = ds.Tables[1];
            }

            btnSave.Text = "Update";
        }

        protected void btnClear_ServerClick(object sender, EventArgs e)
        {
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            ClearText();
            ClearFilterText();
            LoadRetailer();
            LoadDestination();
            LoadCertifiedBy();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            ddlDestinationSearch.SelectedIndex = 0;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            if (gsiOrderFilter.Value.Trim() == "")
            {
                BindGrid(0);
                gvTitle.InnerText = "Screening Result - Top 50";
            }
            else
            {
                BindGrid(int.Parse(gsiOrderFilter.Value));
                gvTitle.InnerText = "Screening Result - For orders starts with " + gsiOrderFilter.Value;
            }

        }

        protected void btnCreateDateSearch_Click(object sender, EventArgs e)
        {
            if (txtCreateDateSearch.Value.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "alert('Please enter From Date');", true);
                return;
            }
            if (txtToCreateDateSearch.Value.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "alert('Please enter To Date');", true);
                return;
            }
            ClearText();
            ddlDestinationSearch.SelectedIndex = 0;
            gsiOrderFilter.Value = string.Empty;
            ddlDestinationSearch.SelectedIndex = 0;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim());
            gvTitle.InnerText = "Screening Result - For orders created between " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
        }

        protected void btnDestSearch_Click(object sender, EventArgs e)
        {
            ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            gsiOrderFilter.Value = string.Empty;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            BindGrid(ddlDestinationSearch.SelectedItem.Text.Trim());
            gvTitle.InnerText = "Screening Result - For orders having destination " + ddlDestinationSearch.SelectedItem.Text;
        }


        protected void btnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            DataSet dsScr = new DataSet();
            dsScr = (DataSet)Session["ScreeningData"];
            Export(dsScr, "SyntheticScreening", false, this);
        }

        private void InsertBlankRecord()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add("GSIItemNumber");
            dt.Columns.Add("ItemQTYFail");
            dr = dt.NewRow();
            dr["GSIItemNumber"] = string.Empty;
            dr["ItemQTYFail"] = string.Empty;
            dt.Rows.Add(dr);
            //Store the DataTable in Session
            Session["FailItems"] = dt;
            gvItems.DataSource = dt;
            gvItems.DataBind();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (Session["FailItems"] != null)
            {
                DataTable dt = (DataTable)Session["FailItems"];
                DataRow drCurrentRow = null;
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Label lblItem = (Label)gvItems.Rows[rowIndex].Cells[1].FindControl("lblGSIItemNumber");
                        Label lblQTYFail = (Label)gvItems.Rows[rowIndex].Cells[2].FindControl("lblItemQTYFail");
                        drCurrentRow = dt.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;
                        dt.Rows[i - 1]["lblGSIItemNumber"] = lblItem.Text;
                        dt.Rows[i - 1]["lblItemQTYFail"] = lblQTYFail.Text;
                        rowIndex++;
                    }
                }
            }
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt = (DataTable)Session["FailItems"];

            if (txtGSIItemNumber.Value.Trim() == string.Empty || txtItemQTYFail.Value.Trim() == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please enter valid GSI item number or quantity fail.');", true);
                return;
            }

            if (txtGSIItemNumber.Value.Trim().Length < 6)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please enter valid GSI item number.');", true);
                return;
            }

            Utils.DissectItemNumber(txtGSIItemNumber.Value.Trim(), out string order, out var _, out var _);

            if (order != txtGsiOrder.Text.Trim())
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please enter valid GSI item number.');", true);
                return;
            }

            bool exists = dt.AsEnumerable().Where(c => c.Field<string>("GSIItemNumber").Equals(txtGSIItemNumber.Value)).Count() > 0;
            if (exists)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('GSI item number already exist.');", true);
                return;
            }

            if (dt.Rows.Count == 1 && (dt.Rows[0]["GSIItemNumber"].ToString() == string.Empty && dt.Rows[0]["ItemQTYFail"].ToString() == string.Empty))
            {
                dt.Rows[0]["GSIItemNumber"] = txtGSIItemNumber.Value;
                dt.Rows[0]["ItemQTYFail"] = txtItemQTYFail.Value;
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["GSIItemNumber"] = txtGSIItemNumber.Value;
                dr["ItemQTYFail"] = txtItemQTYFail.Value;
                dt.Rows.Add(dr);
            }
            //Store the DataTable in Session
            Session["FailItems"] = dt;
            gvItems.DataSource = dt;
            gvItems.DataBind();
        }

        protected void gvItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = Session["FailItems"] as DataTable;
            dt.Rows[index].Delete();
            dt.AcceptChanges();
            Session["FailItems"] = dt;
            gvItems.DataSource = dt;
            gvItems.DataBind();
            if (dt.Rows.Count == 0)
                InsertBlankRecord();

        }

     

    }
}