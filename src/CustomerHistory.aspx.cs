﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace Corpt
{
    public partial class CustomerHistory : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Customer History";
            if (IsPostBack) return;
            LoadOrderStates();
            InitPeriod();
            //-- Load Customers
            LoadCustomers();
            if (!string.IsNullOrEmpty(Request.Params["CustomerCode"]))
            {
                CustomerLike.Text = Request.Params["CustomerCode"];
                OnCustomerLikeClick(null, null);
            } else
            {
                CustomerLike.Focus();
            }
        }
        private void LoadOrderStates()
        {
            OrderStateList.DataSource = QueryUtils.GetOrderStateCodes(this);
            OrderStateList.DataBind();
            OrderStateList.SelectedIndex = 0;
        }

        private void InitPeriod()
        {
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var dateFrom = PaginatorUtils.ConvertDateToString(today.AddYears(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;

        }
        #region Customer search
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
            lstCustomerList.SelectedIndex = 0;
        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            HideTreeHistory();
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;

            
        }
        private CustomerModel GetCustomerFromView(string customerId)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
                            new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            HideTreeHistory();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (filtered.Count == 1) lstCustomerList.SelectedValue = filtered[0].CustomerId;
            /*
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
            */ 
        }

        #endregion

        #region LookupBtn
        protected void OnLookupClick(object sender, EventArgs e)
        {
            HideTreeHistory();
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;
            var customer = GetCustomerFromView(lstCustomerList.SelectedValue);
            if (customer == null) return;
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var customerItems = QueryCustomerUtils.GetCustomerItemsHistory(customer, dateFrom, dateTo, OrderStateList.SelectedValue, this);
            /*
            List<CustomerItemModel> customerItems;

            var items = QueryCustomerUtils.GetCustomerHistory(customer, this, dateFrom, dateTo, out customerItems);
            if (items == null) return;
            */
            //ShowTreeHistory(items);
            customerItems.Sort((m1, m2) =>  String.Compare(m1.FullItemNumber, m2.FullItemNumber, StringComparison.Ordinal));
            var groups = customerItems.GroupBy(m => m.StateName).ToList();
            var totals = "";
            foreach (var group in groups)
            {

                totals += (totals.Length == 0 ? "" : ", ") + group.Key + " " + customerItems.FindAll(m => m.StateName == group.Key).Count;
            }
             
            GridItems.DataSource = customerItems;
            GridItems.DataBind();
            ItemsTotals.Text = "Totals " + customerItems.Count +  " (" + totals + ")";

        }

        #endregion
        #region Tree History
        private void HideTreeHistory()
        {
            treeHistory.Visible = false;
            GridItems.DataSource = null;
            GridItems.DataBind();
            ItemsTotals.Text = "";
        }
        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items)
        {
            CustomerLikeButton.Enabled = false;
            treeHistory.Nodes.Clear();
            treeHistory.Visible = true;
            var data = new List<TreeViewModel>();
            foreach(var item in items)
            {
                data.Add(
                    new TreeViewModel
                        {
                            Id = item.Id, 
                            ParentId = item.ParentId, 
                            DisplayName = item.Title
                        });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            rootNode.SelectAction = TreeNodeSelectAction.None;

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.None);
            rootNode.Expand();
            
            treeHistory.Nodes.Add(rootNode);
            
            CustomerLikeButton.Enabled = true;
        }   

        protected void OnTreeSelectedChanged(object sender, EventArgs e)
        {

        }
        #endregion

    }
}