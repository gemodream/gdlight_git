using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;

namespace Corpt
{
	/// <summary>
	/// Summary description for LeoListII.
	/// </summary>
	public partial class LeoListII : System.Web.UI.Page
	{
		protected SortedList slOrders;
		protected System.Web.UI.WebControls.DataGrid Datagrid1;
		protected System.Web.UI.WebControls.DataGrid dgDebug0;
		protected System.Web.UI.WebControls.DataGrid dgDebug1;
		protected SortedList slBatches;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");

			if(!IsPostBack)
			{
				cmdClear_Click(sender,e);
			}
			SqlConnection connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
#if DEBUG
			lblDebug.Text=connection.DataSource+":"+connection.Database;
#endif
			if(Utils.NullValue(Session["LeoListII.slOrders"])=="")
			{
				slOrders = new SortedList();
				Session["LeoListII.slOrders"] = slOrders;
			}
			else
			{
				slOrders = Session["LeoListII.slOrders"] as SortedList;
			}

		}

		protected void cmdAdd_Click(object sender, System.EventArgs e)
		{
			if(Regex.IsMatch(txtOrderNumber.Text.Trim(), @"^\d{5}$|^\d{6}$|^\d{7}$"))
			{
				Session["dsMovedItems"] = null;
				//plce order into queue.
				try
				{
					slOrders.Add(txtOrderNumber.Text.Trim(),txtOrderNumber.Text.Trim());
				}
				catch(Exception ex)
				{
					lblInfo.Text="This order is already on the list";
				}
			}
			else
			{
				lblInfo.Text="Wrong format. Please chech the order number and try again.";
			}

			lstOrderNumber.Items.Clear();
			foreach(DictionaryEntry di in slOrders)
			{
				lstOrderNumber.Items.Add(di.Value.ToString());
			}
			txtOrderNumber.Text="";			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgPass.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPass_ItemCommand);
			this.dgFail.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgFail_ItemCommand);

		}
		#endregion

		protected void txtOrderNumber_TextChanged(object sender, System.EventArgs e)
		{
			cmdAdd_Click(sender,e);
		}

		protected void cmdClear_Click(object sender, System.EventArgs e)
		{
			lblMovedItems.Visible=false;
			lblMovedItems.Enabled=false;
			
			Session["LeoListII.slOrders"] = null;
			Session["dtPass"]=null;
			Session["dtFail"]=null;
			Session["dtCombinedShortReportWithCheckBox"]=null;

			lstOrderNumber.Items.Clear();
			lblExistingReports.Text="";
//			lstBatches.Items.Clear();
//			lstItems.Items.Clear();

			lblKm.Text = "";
			Session["KMMessage"]=null;

			txtOrderNumber.Text="";
			lblInfo.Text="";
			ShowData();			
		}

		protected void cmdShowShortReport_Click(object sender, System.EventArgs e)
		{
			lblMovedItems.Visible=false;
			lblMovedItems.Enabled=false;

			string strKMMessage = "";
			Session["KMMessage"]=null;

			//collect batches for the orders;
			DataSet dsBatches = new DataSet();
			foreach(DictionaryEntry di in slOrders)
			{
				string strGroupCode = di.Value.ToString();

				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("spGetBatchByCode");
				command.CommandType=CommandType.StoredProcedure;
				command.Connection=conn;
	
				command.Parameters.Add("@CustomerCode",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EGroupState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BGroupState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BDate",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EDate",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BatchCode",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@GroupCode",SqlDbType.Int).Value=Int32.Parse(strGroupCode);
				command.Parameters.Add("@AuthorID",SqlDbType.Int).Value=Int32.Parse(Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeID",SqlDbType.Int).Value=Int32.Parse(Session["AuthorOfficeID"].ToString());

				conn.Open();
				SqlDataAdapter da = new SqlDataAdapter(command);
				command.CommandTimeout=0;
				DataTable dt = new DataTable();
				da.Fill(dt);
				if(dt.Rows.Count<1)
				{
					lblInfo.Text="Failed to load batch for order "+strGroupCode;
					return;
				}
                dsBatches.Tables.Add(dt.Copy());
			}
			
			//order list here is a SortedList slOrders;
			//check for already ordered reports here.
			string strExistingReportsXmlFileName = "";
			bool blHaveExistingReportsOrdered = false;
			blHaveExistingReportsOrdered = HasExistingReports(slOrders, out strExistingReportsXmlFileName);
			if(blHaveExistingReportsOrdered)
			{
				lblExistingReports.Text="<br />One or more orders here has some reports already ordered. <br />Please review the order and make sure you are not overriding already printed reports. Be aware of billing.";
			}
			else
			{
				lblExistingReports.Text="";
			}

			//I have Data about ordered reports
//			lblDebug.Text=strExistingReportsXmlFileName;
			DataSet dsOrderedReports = new DataSet();
			dsOrderedReports.ReadXml(strExistingReportsXmlFileName,XmlReadMode.ReadSchema);

			DataTable dtOrderedReports = new DataTable(); // will be empty or will have ordered reports list
			if(dsOrderedReports.Tables.Count>0)
			{
				dtOrderedReports = dsOrderedReports.Tables[0].Copy();
				dtOrderedReports.Rows.Clear();
				dtOrderedReports.AcceptChanges();

				foreach(DataTable dt in dsOrderedReports.Tables)
				{

					//debug
////////					DataGrid dg = new DataGrid();
////////					dg.DataSource=dt;
////////					dg.DataBind();
////////					pnlDebug.Controls.Add(dg);
					///debug
					
					foreach(DataRow dr in dt.Rows)
					{
						dtOrderedReports.Rows.Add(dr.ItemArray);
					}
				}
				dtOrderedReports.AcceptChanges();
			}

			//end collecting data about ordered reports

			if(dsBatches.Tables.Count>0)
			{
				DataTable dtBatchList = new DataTable();
				dtBatchList= dsBatches.Tables[0].Copy();
				dtBatchList.Rows.Clear();
				dtBatchList.AcceptChanges();

				ArrayList al = new ArrayList();


				foreach(DataTable dt in dsBatches.Tables)
				{
					foreach(DataRow dr in dt.Rows)
					{
						Batch b = new Batch(dr);
						al.Add(b);						
						dtBatchList.Rows.Add(dr.ItemArray);
					}
				}
				dtBatchList.AcceptChanges();

				if(al.Count>0)
				{
					//create short report table base
					DataTable dtCombinedShortReport = new DataTable();
					Batch b = al[0] as Batch;

					SqlCommand commandCheck = new SqlCommand("spGetItemStructureByBatchID");
					commandCheck.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					commandCheck.CommandType=CommandType.StoredProcedure;				
					commandCheck.Parameters.Add(new SqlParameter("@BatchID", b.BatchID));
					SqlDataAdapter da = new SqlDataAdapter(commandCheck);
					DataTable dtItemStructure = new DataTable();
					da.Fill(dtItemStructure);

					Session["BatchShortReportII.dtItemStructure"] = dtItemStructure;
					dtCombinedShortReport  = Utils.BatchShortReport(b.BatchID.ToString(),this);
					Session["BatchShortReportII.dtItemStructure"] = null;

					//clean out the KM message
					strKMMessage = "";
					Session["KMMessage"]=null;

					if(dtCombinedShortReport.Rows.Count<1)
					{
						lblInfo.Text+="Failed to load short report for batch "+b.FullBatchNumber+". Terminating abnormally.";
						return;
					}
					else
					{
						dtCombinedShortReport.Rows.Clear();
						dtCombinedShortReport.AcceptChanges();
						foreach(Batch b2 in al)
						{
							DataTable dtTemp = new DataTable();
							dtTemp = Utils.BatchShortReport(b2.BatchID.ToString(),this);
							strKMMessage += Session["KMMessage"].ToString();
							if(dtTemp.Rows.Count<1)
							{
								lblInfo.Text+="Failed to load short report for batch "+b2.FullBatchNumber+". Terminating abnormally.";
								return;
							}
							else
							{
								try
								{
									foreach(DataRow dr in dtTemp.Rows)
									{
										dtCombinedShortReport.Rows.Add(dr.ItemArray);									
									}
								}
								catch(Exception ex)
								{
									//faied to add new short report to combined short report.
									lblInfo.Text+="Failed to combine short reports. Wrong number of columns in one or more reports."+ex.Message+" "+b2.FullBatchNumber+" </ br>Termintating abnormally.";
									return;
								}
							}
						}

						//if all is well i have here the full short report table.
//						dgDebug2.DataSource=dtCombinedShortReport;
						Session["dtCombinedShortReport"]=dtCombinedShortReport;
//						dgDebug2.DataBind();

						//now lets deal with the pass/fail cprules.
						DataTable dtCpRulesResult = new DataTable();
						SqlCommand commandCP = new SqlCommand("sp_RulesTracking24");
						commandCP.Connection=new SqlConnection(Session["MyIP_ConnectionString"].ToString());
						commandCP.CommandType=CommandType.StoredProcedure;

						commandCP.Parameters.Add(new SqlParameter("@BatchFullNumber", (al[0] as Batch).FullBatchNumber+"00"));
						commandCP.Parameters.Add(new SqlParameter("@ShowRules", "0"));
						
						SqlDataAdapter daCP = new SqlDataAdapter(commandCP);
						daCP.Fill(dtCpRulesResult);

						dtCpRulesResult.Rows.Clear();
						dtCpRulesResult.AcceptChanges();
                        						
						foreach(Batch b3 in al)
						{
							DataTable dtTemp = new DataTable();
							commandCP.Parameters["@BatchFullNumber"].Value=b3.FullBatchNumber+"00";
							daCP.Fill(dtTemp);
							if(dtTemp.Rows.Count<1)
							{
								lblInfo.Text+="Failed to load CP Result for batch "+b.FullBatchNumber+". Terminating abnormally.";
								return;
							}
							else
							{
								foreach(DataRow dr in dtTemp.Rows)
								{
									dtCpRulesResult.Rows.Add(dr.ItemArray);
								}
							}
						}

						//reformat OldItemNumber
						foreach(DataRow drCp in dtCpRulesResult.Rows)
						{
							drCp["OldItemNumber"]=drCp["OldItemNumber"].ToString().Replace(".","").Trim();
						}
                        
						//here we have the pass/failed table for cprules.
						Session["dtCpRulesResult"]=dtCpRulesResult;
//						dgDebug3.DataSource=dtCpRulesResult;
//						dgDebug3.DataBind();


						//figure out wich column in short reort has item numbers
						DataTable dtShortReportStructure = new DataTable();

						SqlCommand commandDoc = new SqlCommand("spGetDocumentValue");
						commandDoc.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
						commandDoc.CommandType=CommandType.StoredProcedure;

						string documentID = "";
						Utils.IsShortReport((al[0] as Batch).BatchID.ToString(),this, out documentID);

						commandDoc.Parameters.Add("@DocumentID",documentID);
						commandDoc.Parameters.Add("@AuthorID", this.Session["ID"]);
						commandDoc.Parameters.Add("@AuthorOfficeID", this.Session["AuthorOfficeID"]);

						SqlDataAdapter daDoc = new SqlDataAdapter(commandDoc);
						
						daDoc.Fill(dtShortReportStructure);
//						dgDebug4.DataSource=dtShortReportStructure;
//						dgDebug4.DataBind();

						string strItemNumberColumnName = "";
						foreach(DataRow drCP in dtShortReportStructure.Rows)
						{
							if(drCP["Value"].ToString().ToLower().Trim()=="[item container.item #]")
							{
								strItemNumberColumnName=drCP["Title"].ToString();
							}
						}
					
						if(strItemNumberColumnName=="")
						{
							lblInfo.Text+="Failed to find Item Number column. Terminating.";
						}
						else
						{
							//i have the column from short report.
							//lblInfo.Text=strItemNumberColumnName;
							Session["strItemNumberColumnName"]=strItemNumberColumnName;
						}

						//dtCombinedShortReport	
						DataColumn dcFailCP = new DataColumn("Fail CP");
						dtCombinedShortReport.Columns.Add(dcFailCP);
						dtCombinedShortReport.AcceptChanges();
						
						foreach(DataRow drCombinedShortReport in dtCombinedShortReport.Rows)
						{
							foreach(DataRow drCpRulesFail in dtCpRulesResult.Rows)
							{
								if(drCpRulesFail["OldItemNumber"].ToString().Trim().ToUpper()==drCombinedShortReport[strItemNumberColumnName].ToString().Trim())
								{
									if(drCpRulesFail["Status"].ToString()=="Failed")
									{
										drCombinedShortReport["Fail CP"]="Failed CPRules";
										drCombinedShortReport["Result"]="<span class=\"text_highlitedyellow\">Fail</span>";
									}
								}
							}
						}					
	
						dtCombinedShortReport.AcceptChanges();						

						//i have here combined table with fail/pass info from short report and cprules.
						//prepare extra column for checkbox and share

//////////						dgDebug.DataSource=dtOrderedReports;
//////////						dgDebug.DataBind();
	
						DataTable dtCombinedShortReportWithCheckBox = new DataTable();
						dtCombinedShortReportWithCheckBox = dtCombinedShortReport.Copy();						
						dtCombinedShortReportWithCheckBox.Columns.Add(new DataColumn("LineChecked",System.Type.GetType("System.Boolean")));
						dtCombinedShortReportWithCheckBox.AcceptChanges();

						//here will be the place to update dtCombinedShortReportWithCheckBox with ordered documents - place info into dr["Result"]

						UncheckOrderedReports(dtCombinedShortReportWithCheckBox, dtOrderedReports, strItemNumberColumnName);
						
						foreach(DataRow dr in dtCombinedShortReportWithCheckBox.Rows)
						{
							if(Utils.NullValue(dr["Result"])!="")
							{
								dr["LineChecked"]=false;
							}
							else
							{
								dr["LineChecked"]=true;
							}
						}

						//uncheck  all the already ordered items - not realised now
						//UncheckAllOrderedReports(dtCombinedShortReportWithCheckBox,

						Session["dtCombinedShortReportWithCheckBox"]=dtCombinedShortReportWithCheckBox;						
						//--debug
//						dgCombined.DataSource=dtCombinedShortReportWithCheckBox;
//						dgCombined.DataBind();
						//
	

						/////////remove separate tables here
////////////////						DataTable dtToPrint = new DataTable();
////////////////						DataTable dtFail = new DataTable();
////////////////						
////////////////						dtToPrint = dtCombinedShortReport.Copy();
////////////////						dtFail = dtCombinedShortReport.Copy();
////////////////
////////////////						dtToPrint.Rows.Clear();
////////////////						dtFail.Rows.Clear();
////////////////
////////////////						foreach(DataRow drR in dtCombinedShortReport.Rows)
////////////////						{
////////////////							if((Utils.NullValue(drR["Result"])=="")&&(Utils.NullValue(drR["Fail CP"])==""))
////////////////							{
////////////////								dtToPrint.Rows.Add(drR.ItemArray);								
////////////////							}
////////////////							else
////////////////							{
////////////////								dtFail.Rows.Add(drR.ItemArray);
////////////////							}
////////////////						}
////////////////						dtToPrint.AcceptChanges();
////////////////						dtFail.AcceptChanges();
////////////////
////////////////						dgPass.DataSource=dtToPrint;
////////////////						Session["dtPass"]=dtToPrint;
////////////////						//dgPass.DataBind();
////////////////
////////////////						dgFail.DataSource=dtFail;
////////////////						Session["dtFail"]=dtFail;
////////////////						//dgFail.DataBind();

                        ShowData();						
					}
				}

//				dgDebug.DataSource=dtBatchList;
//				dgDebug.DataBind();
//				dgDebug2.DataBind();
			}
			else
			{
				lblInfo.Text="No Batches Loaded.";
				return;
			}
			lblKm.Text=strKMMessage;
			
			if(Utils.NullValue(Session["dsMovedItems"])!="")
			{
				lblMovedItems.Visible=true;
				lblMovedItems.Enabled=true;
			}
			else
			{
				lblMovedItems.Visible=false;
				lblMovedItems.Enabled=false;
			}
		}

		private void ShowDataCheckBoxed()
		{
			if(Utils.NullValue(Session["dtCombinedShortReportWithCheckBox"])!="")
			{
				DataTable dtCombinedShortReportWithCheckBox = new DataTable();
				dtCombinedShortReportWithCheckBox = Session["dtCombinedShortReportWithCheckBox"] as DataTable;

				//LeoReportToCpRulesRedirect.aspx 
				foreach(DataRow dr in dtCombinedShortReportWithCheckBox.Rows)
				{
					if(Utils.NullValue(dr["Result"])!="")
					{
						//http://testserver/Corpt/CPResult.aspx?ItemNumber=19393.001.01&Flag=OneItem
						string strDottedItem = "";
						string strOrigItem = dr[Session["strItemNumberColumnName"].ToString()].ToString();
						strDottedItem = Utils.GetItemNumberWithDots(strOrigItem);
						dr["Result"]=dr["Result"].ToString().Replace("Fail","<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber="+strDottedItem+ "&Flag=OneItem\" target=\"_blank\">Fail</a>");
					}
				}
				
				dgCombined.DataSource=dtCombinedShortReportWithCheckBox;
				dgCombined.DataBind();

				dgCombined.Visible=true;
			}
			else
			{
				dgCombined.Visible=false;
			}
		}

		private void ShowData()
		{
			ShowDataCheckBoxed();

			///remove separate pass/fail tables
//////////////			DataTable dtPass = new DataTable();
//////////////			DataTable dtFail = new DataTable();
//////////////
//////////////			if(Utils.NullValue(Session["dtPass"])!=""&&Utils.NullValue(Session["dtFail"])!="")
//////////////			{
//////////////
//////////////				dtPass = Session["dtPass"] as DataTable;
//////////////				dtFail = Session["dtFail"] as DataTable;
//////////////
//////////////				DataTable dtPassDisplay = new DataTable();
//////////////				//			dtPassDisplay.Columns.Add("Remove");
//////////////				foreach(DataColumn dc in dtPass.Columns)
//////////////				{
//////////////					dtPassDisplay.Columns.Add(dc.ColumnName);
//////////////				}
//////////////
//////////////				foreach(DataRow dr in dtPass.Rows)
//////////////				{
//////////////					DataRow drAdd = dtPassDisplay.NewRow();
//////////////					foreach(DataColumn dc in dtPass.Columns)
//////////////					{
//////////////						drAdd[dc.ColumnName]=dr[dc.ColumnName].ToString();
//////////////					}
//////////////					dtPassDisplay.Rows.Add(drAdd);
//////////////				}
//////////////					
//////////////				foreach(DataColumn dc in dtPassDisplay.Columns)
//////////////				{	
//////////////				
//////////////				}
//////////////				dgPass.DataSource=dtPassDisplay;
//////////////				dgPass.DataBind();
//////////////
//////////////				DataTable dtFailDisplay = new DataTable();
//////////////				//			dtFailDisplay.Columns.Add("Remove");
//////////////				foreach(DataColumn dc in dtFail.Columns)
//////////////				{
//////////////					dtFailDisplay.Columns.Add(dc.ColumnName);
//////////////				}
//////////////
//////////////				foreach(DataRow dr in dtFail.Rows)
//////////////				{
//////////////					DataRow drAdd = dtFailDisplay.NewRow();
//////////////					foreach(DataColumn dc in dtPass.Columns)
//////////////					{
//////////////						drAdd[dc.ColumnName]=dr[dc.ColumnName].ToString();
//////////////					}
//////////////					dtFailDisplay.Rows.Add(drAdd);
//////////////				}
//////////////			
//////////////				dgFail.DataSource=dtFailDisplay;
//////////////				dgFail.DataBind();
//////////////
//////////////				dgPass.Visible=true;
//////////////				dgFail.Visible=true;
//////////////			}
//////////////			else
//////////////			{
//////////////				dgPass.Visible=false;
//////////////				dgFail.Visible=false;
//////////////			}
		}

		protected void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if(lstOrderNumber.SelectedIndex!=-1)
			{
				lstOrderNumber.Items.RemoveAt(lstOrderNumber.SelectedIndex);
			}
		}

		private void dgPass_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			// <span class="text_highlitedyellow">Fail</span> 
			//lblInfo.Text="e.CommandArgument:"+e.CommandArgument+"</br >"+"e.CommandName:"+e.CommandName+"</ br>"+"e.CommandSource:"+e.CommandSource;
			DataGridItem dii = e.Item;

			DataTable dtPass = new DataTable();
			DataTable dtFail = new DataTable();

			dtPass = Session["dtPass"] as DataTable;
			dtFail = Session["dtFail"] as DataTable;

//			System.Web.UI.WebControls.DataGridItem diSender;
//			diSender = ((source as System.Web.UI.WebControls.Button).Parent as System.Web.UI.WebControls.TableCell).Parent as System.Web.UI.WebControls.DataGridItem;
			//diSender.DataSetIndex
				
//			lblInfo.Text=dii.DataSetIndex.ToString();
			
			dtFail.Rows.Add(dtPass.Rows[dii.DataSetIndex].ItemArray);
			dtPass.Rows.RemoveAt(dii.DataSetIndex);

			ShowData();
		}

		private void dgFail_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataGridItem dii = e.Item;

			DataTable dtPass = new DataTable();
			DataTable dtFail = new DataTable();

			dtPass = Session["dtPass"] as DataTable;
			dtFail = Session["dtFail"] as DataTable;
            
			dtPass.Rows.Add(dtFail.Rows[dii.DataSetIndex].ItemArray);
			dtFail.Rows.RemoveAt(dii.DataSetIndex);

			ShowData();
		}

		protected void cmdOrderReports_Click(object sender, System.EventArgs e)
		{

			if(Utils.NullValue(Session["dtCombinedShortReportWithCheckBox"])=="")
			{
				lblInfo.Text="Nothing to order from. Please load short report first.";
				return;
			}
            //prepare new DataPass table here

			DataTable dtCombinedShortReportWithCheckBox = new DataTable();
			DataTable dtPass = new DataTable();
			DataTable dtSend = new DataTable();

			dtCombinedShortReportWithCheckBox = Session["dtCombinedShortReportWithCheckBox"] as DataTable;
            dtPass = dtCombinedShortReportWithCheckBox.Copy();
			dtPass.Rows.Clear();
			dtPass.AcceptChanges();

			foreach(DataRow dr in dtCombinedShortReportWithCheckBox.Rows)
			{
				if((Utils.NullValue(dr["LineChecked"])!="")&&(bool.Parse(dr["LineChecked"].ToString())))
				{
					dtPass.Rows.Add(dr.ItemArray);
				}
			}
			dtPass.AcceptChanges();
			dtPass.Columns.Remove("LineChecked");
			dtPass.AcceptChanges();

			Session["dtPass"]=dtPass;
			
			if(Utils.NullValue(Session["dtPass"])=="")
			{
				lblInfo.Text="Nothing to order from. Please load short report first.";
				return;
			}

			dtPass = Session["dtPass"] as DataTable;
			
			if(dtPass.Rows.Count<1)
			{
				lblInfo.Text="Nothing to order - there are no PASS items listed.";
				return;
			}

			dtSend.Columns.Add(new DataColumn("ReportNumber"));
			dtSend.AcceptChanges();
			
			string strItemNumberColumnName = Utils.NullValue(Session["strItemNumberColumnName"]);
			
			if(strItemNumberColumnName!="")
			{
				//do
				foreach(DataRow drPass in dtPass.Rows)
				{
					DataRow drSend = dtSend.NewRow();
					drSend["ReportNumber"]=drPass[strItemNumberColumnName].ToString().Trim();
					dtSend.Rows.Add(drSend);
				}

//				dgDebug2.DataSource=dtSend;
//				dgDebug2.DataBind();

				DataSet dtItemSet = new DataSet("ItemSet");			
				DataTable dtItems = new DataTable("Items");
				dtItemSet.Tables.Add(dtItems);

				dtItems.Columns.Add(new DataColumn("SRP"));
				dtItems.Columns.Add(new DataColumn("SIDD_PRICE"));
				dtItems.Columns.Add(new DataColumn("NEW_SKU"));
				dtItems.Columns.Add(new DataColumn("XREF"));
				dtItems.Columns.Add(new DataColumn("REPORT_TYPE"));
				dtItems.Columns.Add(new DataColumn("ITEM"));
				dtItems.Columns.Add(new DataColumn("GroupCode"));
				dtItems.Columns.Add(new DataColumn("BatchCode"));
				dtItems.Columns.Add(new DataColumn("ItemCode"));

				dtItems.AcceptChanges();

				foreach(DataRow dr in dtSend.Rows)
				{
					DataRow drDest = dtItems.NewRow();
					drDest["SRP"]=DBNull.Value;
					drDest["SIDD_PRICE"]=DBNull.Value;
					drDest["NEW_SKU"]=DBNull.Value;
					drDest["XREF"]=DBNull.Value;
					drDest["REPORT_TYPE"]=DBNull.Value;
					drDest["ITEM"]=dr["ReportNumber"].ToString();

					Utils.DissectItemNumber(dr["ReportNumber"].ToString(), out int intGroupCode, out int intBatchCode, out int intItemCode);
					drDest["GroupCode"]= intGroupCode;
					drDest["BatchCode"]= intBatchCode;
					drDest["ItemCode"]= intItemCode;
				
					dtItems.Rows.Add(drDest);
				}
                
//				dgDebug.DataSource=dtItemSet;
//				dgDebug.DataBind();

				SqlCommand command = new SqlCommand("spGetCPDocsFromItemList");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter("@ItemsList",dtItemSet.GetXml()));
				command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
				command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeId"].ToString()));

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataSet dsCo = new DataSet();
				da.Fill(dsCo);

				DataSet dsOrderDocs = new DataSet("SID_Docs");
				DataTable dtOrderDocs = new DataTable("Operation");
				dsOrderDocs.Tables.Add(dtOrderDocs);

				dtOrderDocs.Columns.Add(new DataColumn("AuthorId"));
				dtOrderDocs.Columns.Add(new DataColumn("AuthorOfficeId"));
				dtOrderDocs.Columns.Add(new DataColumn("CurrentOfficeId"));
				dtOrderDocs.Columns.Add(new DataColumn("OperationTypeOfficeID"));
				dtOrderDocs.Columns.Add(new DataColumn("OperationTypeID"));
				dtOrderDocs.Columns.Add(new DataColumn("BatchID"));
				dtOrderDocs.Columns.Add(new DataColumn("ItemCode"));
				dtOrderDocs.AcceptChanges();

//				dgDebug0.DataSource=dsCo.Tables[0];
//				dgDebug0.DataBind();
//
//				dgDebug1.DataSource = dsCo.Tables[1];
//				dgDebug1.DataBind();

                //DataRow[] drsSelected = dsCo.Tables[1].Select("OperationChar = 'R' or OperationChar = 'T' or OperationChar = 'D'");
				DataRow[] drsSelected = dsCo.Tables[1].Select();

				foreach(DataRow dr in drsSelected)
				{
					DataRow drToOrder = dtOrderDocs.NewRow();
					drToOrder["AuthorId"]= Session["ID"].ToString();
					drToOrder["AuthorOfficeId"]=Session["AuthorOfficeId"].ToString();
					drToOrder["CurrentOfficeId"]=dr["OperationTypeOfficeID"].ToString();
					drToOrder["OperationTypeOfficeID"]=dr["OperationTypeOfficeID"].ToString();
					drToOrder["OperationTypeID"]=dr["OperationTypeID"].ToString();
					drToOrder["BatchID"]=dr["NewBatchID"].ToString();
					drToOrder["ItemCode"]=dr["NewItemCode"].ToString();
					dtOrderDocs.Rows.Add(drToOrder);
					dtOrderDocs.AcceptChanges();
				}
				
//				dgDebug4.DataSource=dtOrderDocs;
//				dgDebug4.DataBind();

				try
				{
					OrderReports(dsOrderDocs);
					PushTheQueue(dtSend);
					lblInfo.Text="<h3>Reports ordered.</h3>";
				}
				catch(Exception ex)
				{
					lblInfo.Text="Failed to order reports: "+ex.Message;
				}
			}
			else
			{
				lblInfo.Text+="Could not find item/report number column name. Terminating ubnormally.";
			}

		}

		private void PushTheQueue(DataTable dtOrder)
		{
			DataSet ds = new DataSet("dsItems");
			ds.Tables.Add(dtOrder.Copy());
			ds.Tables[0].TableName="dtItems";

			SqlCommand command = new SqlCommand("sp_EnqueueLeo");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@ItemNumbers",ds.GetXml()));
			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
		}

		protected void ItemCheckBoxClicked(object sender, System.EventArgs e)
		{
			System.Web.UI.WebControls.DataGridItem diSender;
			diSender = ((sender as System.Web.UI.WebControls.CheckBox).Parent as System.Web.UI.WebControls.TableCell).Parent as System.Web.UI.WebControls.DataGridItem;

			DataTable dtCombinedShortReportWithCheckBox = new DataTable();
            dtCombinedShortReportWithCheckBox =	Session["dtCombinedShortReportWithCheckBox"] as DataTable;

			dtCombinedShortReportWithCheckBox.Rows[diSender.DataSetIndex]["LineChecked"]=(sender as System.Web.UI.WebControls.CheckBox).Checked;
			dtCombinedShortReportWithCheckBox.AcceptChanges();
			Session["dtCombinedShortReportWithCheckBox"]=dtCombinedShortReportWithCheckBox;
			
			ShowDataCheckBoxed();
//
//			dtCombinedReport.Rows[diSender.DataSetIndex]["LineChecked"]=(sender as System.Web.UI.WebControls.CheckBox).Checked;
//			grdShortReport.DataSource=dtCombinedReport;
//			Session["combinedReport"]=dtCombinedReport;
//			grdShortReport.DataBind();
			
			
			//lblInfo.Text=diSender.DataSetIndex.ToString();
		}

		private void OrderReports(DataSet dsOrder)
		{
			SqlCommand command = new SqlCommand("sp_SetItemsDocsFromList");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@ItemsDocs",dsOrder.GetXml()));
			//execute command here and hope for the best:
			try
			{
				SqlDataAdapter da= new SqlDataAdapter(command);
				DataSet ds = new DataSet();
				da.Fill(ds);
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(ex.Message);
				lblInfo.Text = ex.Message;
			}
			//			Exception up = new Exception("Something went wrong");
			//			throw up; //just because i can:)
		}

		private bool HasExistingReports(SortedList slOrderList, out string strExistingReportsXmlFileName)
		{
			bool blHasReportsOrdered = false;
			DataSet ds = new DataSet();
//			Session["TempDir"]

			DirectoryInfo diri = new DirectoryInfo(Session["TempDir"].ToString()+DateTime.Now.Ticks.ToString());
			if(!diri.Exists)
			{
				diri.Create();
			}

			foreach(DictionaryEntry di in slOrderList)
			{
				SqlCommand command = new SqlCommand("sasha_SelectPrintingQueue3");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType=CommandType.StoredProcedure;

				string strGroupCode = di.Value.ToString();
/*
				dbo].[sasha_SelectPrintingQueue3] 5674,0,0,''

(
@GroupCode int,
@BatchCode int,
@ItemCode int,
@ReportStatus varchar(1),
@ShowLabel bit = 0,
@ShowLaserGold bit = 0,
@ShowGMXorderFiles bit = 0,
@ShowOldNumbers bit = 0
)
*/
				command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode", "0"));
				command.Parameters.Add(new SqlParameter("@ItemCode", "0"));
				command.Parameters.Add(new SqlParameter("@ReportStatus", "A"));
				command.Parameters.Add(new SqlParameter("@ShowLabel", "0"));
				command.Parameters.Add(new SqlParameter("@ShowLaserGold", "0"));
				command.Parameters.Add(new SqlParameter("@ShowGMXorderFiles", "1"));
				command.Parameters.Add(new SqlParameter("@ShowOldNumbers", "0"));

                
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtReportList = new DataTable();
				da.Fill(dtReportList);

				ds.Tables.Add(dtReportList.Copy());

				if(dtReportList.Rows.Count==0)
				{
					//nothing ordered
				}
				
				if(dtReportList.Rows.Count>0)
				{
					//stuff ordered.
					blHasReportsOrdered = true;
				}

//				foreach(DataRow drReport in dtReportList.Rows)
//				{
//					//not nessessary foreach, might be usefull later when we want more details;
//						blHasReportsOrdered = true;
//				}
			}
			
			strExistingReportsXmlFileName = diri.FullName+"\\"+DateTime.Now.Ticks.ToString()+".xml";
			ds.WriteXml(strExistingReportsXmlFileName, XmlWriteMode.WriteSchema);

			return blHasReportsOrdered;
		}

		private void UncheckOrderedReports(DataTable dtCombinedShortReportWithCheckBox, DataTable dtOrderedReports, string strItemNumberColumnName)
		{
			if((dtOrderedReports.Rows.Count>0)&&(strItemNumberColumnName!=""))
			{
				foreach(DataRow dr in dtCombinedShortReportWithCheckBox.Rows)
				{
					string strItemNumber = dr[strItemNumberColumnName].ToString().Trim();
					foreach(DataRow drOrdered in dtOrderedReports.Rows)
					{
					    string strOrderedReportNumber = drOrdered["OldItemCode"].ToString();//NB  Regex.Match(drOrdered["OldItemCode"].ToString(), @"^\d{10}$|^\d{11}$").Value;
                        if (strOrderedReportNumber.IndexOf(strItemNumber) != -1)//== strItemNumber)
						{
							if(!dr["Result"].ToString().EndsWith("Reports Already Ordered"))
							{
								dr["Result"]=dr["Result"] + "<br/>" +
                                    "<span class=\"text_highlitedyellow\">Reports Already Ordered</span>";
							}
						}
					}
				}
			}
		}

	}
}
