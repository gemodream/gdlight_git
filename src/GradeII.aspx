<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="GradeII.aspx.cs" AutoEventWireup="True" Inherits="Corpt.GradeII" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Grade II</div>
        <div>
        </div>
        
            <table>
              <tr>
                    <td style="vertical-align: top">
                        <!-- 'Batch Number' textbox -->
                        <asp:Panel ID="Panel1" runat="server" defaultbutton="cmdLoadBatch" CssClass="form-inline">
                            <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                placeholder="Batch number" style="font-weight: bold; width: 100px; margin-top: 10px" />
							<!--    AutoPostBack="True"/> -->
                            <!-- 'Load' button -->
                            <asp:Button ID="cmdLoadBatch" class="btn btn-primary" runat="server" Text="Load"
                                OnClick="OnLoadClick" style="margin-left: 10px"></asp:Button>
                       </asp:Panel>
						<asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
                            Display="None" 
                            ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." 
                            validationgroup="BatchGroup" />
					    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                            HighlightCssClass="validatorCalloutHighlight" />
						<asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
                            Display="None" ValidationExpression="(.{1,100})" 
                           ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>" 
                           validationgroup="BatchGroup" />
						<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                            HighlightCssClass="validatorCalloutHighlight" /> 
					</td>
                    <td style="text-align: left; padding-right: 40px; vertical-align: top">
                    <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                        <asp:CheckBox ID="IgnoreCpFlag" runat="server" CssClass="radio-inline" Text="Ignore Cp" Checked="True" />
                        <asp:TextBox type="text" ID="UniqKeyFld" runat="server" AutoPostBack="True" Visible="False" />
                        <!-- 'Save' button -->
                        <asp:Button ID="cmdSave" CssClass="btn btn-default" runat="server" Text="Save" style="text-align: right; margin-top: 10px"
                            OnClick="OnSaveClick"></asp:Button>
                    </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 0px; vertical-align: top">
                        <asp:ListBox ID="lstItemList" runat="server" Width="192px" CssClass="text" style="vertical-align: top"
                            Height="350px" AutoPostBack="True" Rows="10" OnSelectedIndexChanged="OnItemListSelectedChanged">
                        </asp:ListBox>
                        <br />
                        <asp:Label runat="server" ID="CpPathToPicture"></asp:Label><br />
                        <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image><br />
                        <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server"></asp:Label>
                    </td>
                    <td style="padding-left: 10px;padding-top: 0px;padding-bottom: 0px;">
                        <div style="border: thin dotted #C0C0C0; overflow: auto; height: 520px; font-family: Cambria; font-size: small; width: 550px">
                            <asp:Panel ID="pnlRight" runat="server" Width="510px" Height="520px"
                                Wrap="False" viewstatemode="Enabled" HorizontalAlign="Left" >
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
            </table>
    </div>

</asp:Content>
