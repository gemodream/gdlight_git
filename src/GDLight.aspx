﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="GDLight.aspx.cs" Inherits="Corpt.GDLight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div class="demoarea">
        <div class="demoheading">GDLight</div>
        <table>
            <tr>
                <td style="width: 150px;"><asp:Button runat="server" class="btn btn-info" ID="cmdNewOrder" Text="New Order" OnClick="NewOrderClick" Style="width: 100%"/></td>
                <td style="width: 150px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdItemize" Text="Itemize" OnClick="ItemizeClick" Style="width: 100%"/></td>
                <td style="width: 150px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdAutoMeasure" Text="AutoMeasure" OnClick="AutoMeasureClick" Enabled="False" Style="width: 100%"/></td>
            </tr>
            <tr>
                <td style="padding-top: 5px;"><asp:Button runat="server" class="btn btn-info" ID="cmdColor" Text="Color" OnClick="ColorClick" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdClarity" Text="Clarity" OnClick="ClarityClick" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdMeasure" Text="Measure" OnClick="MeasureClick" Style="width: 100%"/></td>
            </tr>
            <tr>
                <td style="padding-top: 5px;"><asp:Button runat="server" class="btn btn-info" ID="cmdGrade2" Text="(Grade II)" OnClick="Grade2Click" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="ReItemiznButton" Text="ReItemizn" OnClick="OnReItemiznClick" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdReAss" Text="ReAssembly" OnClick="ReAssemblyClick" Enabled="False" Style="width: 100%"/></td>
            </tr>
            <tr>
                <td style="padding-top: 5px;"><asp:Button runat="server" class="btn btn-info" ID="cmdPrintCertified" Text="Print Certified" OnClick="PrintCertifiedClick" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdPrintItemized" Text="Print Itemized" OnClick="PrintItemizedClick" Style="width: 100%"/></td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="cmdNumberGenerator" Text="Number Generator" OnClick="NumberGeneratorClick" Style="width: 100%"/></td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" class="btn btn-info" ID="cmdBulkUpdate" Text="Bulk Update" OnClick="BulkUpdateClick" Enabled="False" Style="width: 100%"/>
                </td>
                <td style="padding-top: 5px;padding-left: 10px;">
                    <asp:Button runat="server" class="btn btn-info" ID="cmdEndSession" Text="End Session" OnClick="EndSessionClick"  Style="width: 100%"/>
                </td>
                <td style="padding-top: 5px;padding-left: 10px;"><asp:Button runat="server" class="btn btn-info" ID="UpdateOrderButton" Text="Order Update" OnClick="OnUpdateOrderClick" Style="width: 100%"/></td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" class="btn btn-info" ID="btnAccountRepresentative" Text="Account Representative" Style="width: 100%" OnClick="btnAccountRepresentative_Click"/>
                </td>
            </tr>
            
        </table>
       
    </div>
</asp:Content>
