﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Corpt.Models;

namespace Corpt
{
    public partial class StNumberReport1 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var id = int.Parse(Session["ID"].ToString());
            if (id != 10 && id != 12 && id != 17 && id != 19 && id != 24)
            {
                Response.Redirect("Middle.aspx");
            }

            if (!IsPostBack)
            {
                ReFillCustomerList();
                CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
                
            }

        }
        private void ReFillCustomerList()
        {
            lstCustomerList.Items.Clear();
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand { CommandText = "spGetCustomers", Connection = conn, CommandType = CommandType.StoredProcedure };

            command.Parameters.AddWithValue("@AuthorID", Session["ID"].ToString());
            command.Parameters.AddWithValue("@AuthorOfficeID", Session["AuthorOfficeID"].ToString());
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                lstCustomerList.DataSource = reader;
                lstCustomerList.DataBind();
            }
            conn.Close();
        }
        private void ClearMemos()
        {
            lstMemos.SelectedValue = null;
            cblBatches.Items.Clear();
            cblBatches.ToolTip = "Batch numbers";
            lstLastBatchList.Items.Clear();
            lstLastBatchList.ToolTip = "Selected Batch numbers";

        }
        private void ClearBatches()
        {
            cblBatches.Items.Clear();
            cblBatches.ToolTip = "Batch numbers";
            lstLastBatchList.Items.Clear();
            lstLastBatchList.ToolTip = "Selected Batch numbers";
            
        }
        private void ClearSelectedBatches()
        {
            lstLastBatchList.Items.Clear();
            lstLastBatchList.ToolTip = "Selected Batch numbers";
        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            ClearMemos();
            //-- Get Memos
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);

            var command = new SqlCommand
            {
                CommandText = "sp_GetMemoNumbersByCustomerByDateRange",
                Connection = conn,
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.Add("@CustomerID", SqlDbType.Int);
            command.Parameters["@CustomerID"].Value = int.Parse(lstCustomerList.SelectedValue.Split('_')[1]);

            if (CalendarExtenderFrom.SelectedDate != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = CalendarExtenderFrom.SelectedDate;
            }

            if (CalendarExtenderTo.SelectedDate != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = ((DateTime)CalendarExtenderTo.SelectedDate).AddDays(1);
            }

            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);

            var dw = new DataView(dt, "", "MemoNumber", DataViewRowState.CurrentRows);

            lstMemos.DataSource = dw;
            lstMemos.DataBind();
            if (lstMemos.SelectedValue != null)
            {
                OnMemoSelectedChanged(null, null);                
            }


        }

        protected void OnMemoSelectedChanged(object sender, EventArgs e)
        {
            ClearBatches();
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);

            var command = new SqlCommand
            {
                CommandText = "sp_GetBatchesByCustomerByDateRangeByMemoNumber", 
                Connection = conn, CommandType = CommandType.StoredProcedure
            };

            command.Parameters.Add("@CustomerID", SqlDbType.Int);
            command.Parameters["@CustomerID"].Value = int.Parse(lstCustomerList.SelectedValue.Split('_')[1]);
            command.Parameters.Add("@MemoNumber", SqlDbType.VarChar).Size = 255;
            command.Parameters["@MemoNumber"].Value = lstMemos.SelectedValue;

            if (CalendarExtenderFrom.SelectedDate != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = CalendarExtenderFrom.SelectedDate;
            }
            if (CalendarExtenderTo.SelectedDate != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = ((DateTime)CalendarExtenderTo.SelectedDate).AddDays(1);
            }

            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);

            var batches = (from DataRow dr in dt.Rows select new BatchModel(dr)).ToList();


            var batchList = new DataTable();
            batchList.Columns.Add(new DataColumn("BatchID"));
            batchList.Columns.Add(new DataColumn("BatchNumber"));
            batchList.Columns.Add(new DataColumn("GroupCode"));
            batchList.Columns.Add(new DataColumn("BatchCode"));
            batchList.Columns.Add(new DataColumn("GroupID"));
            batchList.Columns.Add(new DataColumn("CPID"));
            
            foreach (DataRow dr in dt.Copy().Rows)
            {
                var ddr = batchList.NewRow();
                ddr["BatchID"] = dr["BatchID"];

                var strGroupCode = (int.Parse(dr["GroupCode"].ToString()) + 100000).ToString().Substring(1); // Utils.FillToFiveChars(dr["GroupCode"].ToString());
                var strBatchCode = (int.Parse(dr["BatchCode"].ToString()) + 1000).ToString().Substring(1);   // Utils.FillToThreeChars(dr["BatchCode"].ToString());
                ddr["BatchNumber"] = strGroupCode + strBatchCode;

                ddr["GroupCode"] = dr["GroupCode"];
                ddr["BatchCode"] = dr["BatchCode"];
                ddr["GroupID"] = dr["GroupID"];
                ddr["CPID"] = dr["CPID"];
                batchList.Rows.Add(ddr);
            }
            cblBatches.DataSource = batchList;
            cblBatches.DataBind();
            cblBatches.ToolTip = string.Format("Branch numbers ({0} rows)", cblBatches.Items.Count);
            Session["batchList"] = batchList.Copy();

        }

        protected void CblBatchesSelectedIndexChanged(object sender, EventArgs e)
        {
            ClearSelectedBatches();
            var finalBatchList = new DataTable();
            finalBatchList.Columns.Add(new DataColumn("BatchID"));
            finalBatchList.Columns.Add(new DataColumn("BatchNumber"));
            finalBatchList.Columns.Add(new DataColumn("GroupCode"));
            finalBatchList.Columns.Add(new DataColumn("BatchCode"));
            finalBatchList.Columns.Add(new DataColumn("GroupID"));
            finalBatchList.Columns.Add(new DataColumn("CPID"));

            var selectedShortReport = Utils.BatchShortReport(cblBatches.SelectedValue, this);
            foreach (DataRow dr in (Session["batchList"] as DataTable).Rows)
            {
                if (Utils.CompareColumnList(selectedShortReport, Utils.BatchShortReport(dr["BatchID"].ToString(), this)))
                {
                    finalBatchList.Rows.Add(dr.ItemArray);
                }
            }
            lstLastBatchList.DataSource = finalBatchList;
            lstLastBatchList.DataBind();
            lstLastBatchList.ToolTip = string.Format("Selected Batch numbers ({0} rows)", lstLastBatchList.Items.Count);


        }
    }
}