﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CustomerProgram.aspx.cs" Inherits="Corpt.CustomerProgram" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black;/* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel {
	        padding-bottom:2px;
	        color:#5377A9;
	        font-family:Arial, Sans-Serif;
	        font-weight:bold;
	        font-size:1.0em;
        }
    </style>
    <script type="text/javascript">
        $(window).resize(function () {
            showHeight();
        });
        $(document).ready(function () {
            showHeight();
        });
        function showHeight() {

            var h = parseInt(window.innerHeight) - 230;
            var div1 = document.getElementById('<%= ItemTypeGrpDiv.ClientID %>');
            div1.style.height = h + 'px';

            var div2 = document.getElementById('<%= ItemTypeDiv.ClientID %>');
            div2.style.height = h + 'px';

            var fld = document.getElementById('<%= ForHeightFld.ClientID %>');
            fld.value = h;

        };
    </script>
    
    <div class="demoarea">
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

        <div class="demoheading">Customer Program</div>
        <asp:UpdatePanel runat="server" ID="SearchPanel" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="VendorAsCustomer" 
                    EventName="CheckedChanged" />
            </Triggers>
            <ContentTemplate>
                
                <asp:Panel runat="server" DefaultButton="NoActionButton" ID="FindPanel">
                    <asp:HiddenField runat="server" ID="ForHeightFld" ></asp:HiddenField>
                    <table>
                        <tr>
                            <td>
                                Customer:
                            </td>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="CustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Height="26px" />
                            </td>
                            <td style="padding-top: 5px">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="CustomerLikeButton">
                                    <asp:TextBox runat="server" ID="CustomerLike" Width="100px" Height="18px"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" Style="display: none" />
                                </asp:Panel>
                            </td>
                            <td style="padding-left: 10px">
                                Vendor:
                            </td>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="VendorList" runat="server" AutoPostBack="True" CssClass=""
                                    DataTextField="CustomerName" DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnVendorSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Enabled="False" />
                            </td>
                            <td style="padding-top: 5px">
                                <asp:Panel ID="Panel4" runat="server" DefaultButton="VendorLikeButton">
                                    <asp:TextBox ID="VendorLike" runat="server" Width="100px" Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="VendorLikeButton" runat="server" ImageUrl="~/Images/ajaxImages/search.png"
                                        OnClick="OnVendorSearchClick" ToolTip="Filtering the list of customers" Style="display: none" />
                                </asp:Panel>
                            </td>
                            <td style="vertical-align: middle">
                                <asp:CheckBox ID="VendorAsCustomer" runat="server" Checked="True" Text="the same as Customer"
                                    CssClass="checkbox" Width="250px" OnCheckedChanged="OnVendorAsCustomerChanging"
                                    AutoPostBack="True" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Programs:
                            </td>
                            <td>
                                <asp:DropDownList ID="CpList" runat="server" AutoPostBack="True" CssClass="" DataTextField="CustomerProgramName"
                                    DataValueField="CpId" OnSelectedIndexChanged="OnCpSelectedChanged" ToolTip="Customer Program List"
                                    Width="300px" />
                            </td>
                            <td>
                                <asp:Button runat="server" ID="NewSkuBtn" Text="New SKU" OnClick="OnNewSkuClick"
                                    Enabled="False" CssClass="btn btn-small btn-info" />
                                <asp:Button runat="server" ID="HiddenNewSkuBtn" Style="display: none"/>
                            </td>
                            <td style="padding-left: 10px">Batch:</td>
                            <td colspan="2">
                                <asp:Panel runat="server" ID="BatchPanel" DefaultButton="SearchCpByBatchBtn" CssClass="form-inline">
                                    <asp:TextBox runat="server" ID="BatchNumberFld" MaxLength="15" 
										ontextchanged="BatchNumberFld_TextChanged" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                                        ValidChars="0123456789-" TargetControlID="BatchNumberFld" />
                                    <asp:Button runat="server" ID="SearchCpByBatchBtn" OnClick="OnSearchCpByBatchClick"
                                        Style="display: none" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <asp:Panel runat="server" ID="CpPanel" BorderStyle="Solid" BorderColor="Silver" BorderWidth="1px"
                    DefaultButton="NoActionButton" Style="margin: 5px; padding: 6px" Visible="False">
                    <asp:Button runat="server" ID="NoActionButton" Style="display: none" />
                    <table>
                        <tr>
                            <td style="vertical-align: top">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label4" runat="server" Text="Program Name"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label1" runat="server" Text="Style"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label2" runat="server" Text="Cust. ID"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label3" runat="server" Text="SRP"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label5" runat="server" Text="Path to picture"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="ProgramNameFld"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="CpStyle"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="CustId" Width="100px"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="Srp"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="SrpExtender" runat="server"
                                                ValidChars="0123456789." TargetControlID="Srp" Enabled="True" />
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px;">
                                            <asp:Panel runat="server" ID="PathToPicturePanel" CssClass="form-inline" DefaultButton="ApplyPath2PictureBtn">
                                                <asp:TextBox runat="server" ID="Path2PictureFld" Width="200px"></asp:TextBox>
                                                <asp:ImageButton runat="server" ID="ApplyPath2PictureBtn" OnClick="OnApplyPath2PictureClick"
                                                    ImageUrl="~/Images/ajaxImages/apply.png" ToolTip="Apply new Path to Picture" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="vertical-align: top; padding-left: 5px">
                                            <asp:Panel ID="Panel6" runat="server" CssClass="form-inline">
                                                <asp:Image ID="itGroupIcon" runat="server" ToolTip="Item Type Group" Width="16px" />
                                                <asp:Label runat="server" ID="itGroupName"></asp:Label>
                                                <asp:Image ID="itIcon" runat="server" ToolTip="Item Type" Width="16px" />
                                                <asp:Label runat="server" ID="itName"></asp:Label>
                                                <asp:TextBox runat="server" ID="itId" Style="display: none"></asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top;text-align: center">
                                            <asp:Button runat="server" ID="SaveButton" OnClick="OnSaveClick" CssClass="btn btn-small btn-info"
                                               Enabled="False" Text="Save" ToolTip="Save Customer Program"/>
                                            <asp:Button runat="server" ID="SaveAsButton" OnClick="OnSaveAsClick" CssClass="btn btn-small btn-info"
                                                Enabled="False" Text="Save as" />
                                            <asp:Button runat="server" ID="SaveAsHiddenBtn" Style="display: none"
                                                Enabled="False" Text="Save as" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="vertical-align: top">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px; text-align: left;width: 100px">
                                            <asp:Label runat="server" ID="CpPathToPicture"></asp:Label><br />
                                            <asp:Image ID="itemPicture" runat="server" Width="90px" Visible="False"></asp:Image><br />
                                            <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Width="90px"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top;font-size: smaller;padding-left: 10px">
                                            <div style="overflow-y: auto; overflow-x: hidden; height: 120px; border: silver solid 1px">
                                                <asp:TreeView ID="CpReportsTree" runat="server" Width="200px">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                </asp:TreeView>
                                            </div>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:ImageButton runat="server" ID="RefreshCpDocumentsTree" OnClick="OnRefreshCpDocumentsClick"
                                                ImageUrl="~/Images/ajaxImages/refresh24.png" ToolTip="Refresh Cp Reports" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False" 
                    ActiveTabIndex="2" Width="100%" Height="100%" Visible="False">
                    <ajaxToolkit:TabPanel runat="server" HeaderText="Description" OnDemandMode="Once" ID="TabDescription" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="DescriptionPanel">
                                <asp:Panel runat="server" ID="PanelDescr1" CssClass="collapsePanel">
                                    <table style="font-family: sans-serif">
                                        <tr>
                                            <td >
                                                <asp:Label ID="Label8" runat="server" Text="Parts" CssClass="label"></asp:Label>
                                            </td>
                                            <td style="padding-left: 20px">
                                                <asp:Label ID="Label9" runat="server" Text="Characteristics" CssClass="label"></asp:Label>
                                            </td>
                                            <td></td>
                                            <td style="padding-left: 20px">
                                                <asp:Label runat="server" Text="Description" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; border-top: dotted silver 1px;font-family: sans-serif">
                                                <asp:TreeView ID="DescriptionPartsTreeView" runat="server" OnSelectedNodeChanged="OnDescripPartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                </asp:TreeView>
                                            </td>
                                            <td rowspan="3" style="vertical-align: top; padding-left: 20px;">
                                                <asp:ListBox runat="server" ID="DescripMeasuresField" DataTextField="MeasureTitle" DataValueField="MeasureId"
                                                    Rows="22" />
                                            </td>
                                            <td style="padding-left: 5px">
                                                <asp:ImageButton ID="CopyT0CommentBtn" runat="server" ToolTip="Copy Characteristic to Description"
                                                    ImageUrl="~/Images/ajaxImages/rightArrowBl.png" OnClick="OnCopyCharToCommentClick" />
                                            </td>
                                            <td style="vertical-align: top; padding-left: 20px;">
                                                <asp:TextBox runat="server" ID="CpComments" TextMode="MultiLine" Width="350px" Height="150px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td style="padding-left: 20px;padding-top: 10px">
                                                <asp:Label ID="Label7" runat="server" Text="Comments" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td style="padding-left: 5px;vertical-align: top">
                                                <asp:ImageButton ID="CopyToDescripBtn" runat="server" ToolTip="Copy Characteristic to Comments"
                                                    ImageUrl="~/Images/ajaxImages/rightArrowBl.png" OnClick="OnCopyCharToDescripClick" />
                                            </td>
                                            <td style="padding-left: 20px">
                                                <asp:TextBox runat="server" ID="CpDescrip" Width="350px" Height="150px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Operations" OnDemandMode="Once" ID="TabOperations" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="OperationsPanel">
                                <asp:Panel runat="server" ID="PanelOper1" CssClass="collapsePanel">
                                    <asp:TreeView ID="TreeOpers" runat="server" ShowCheckBoxes="All" ExpandDepth="1"
                                        AfterClientCheck="CheckChildNodes();" OnTreeNodeCheckChanged="TreeOperationCheckChanged"
                                        OnSelectedNodeChanged="OnTreeOperationSelectedChanged" 
                                        onclick="OnTreeClick(event)">
                                        <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                        <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                        <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                    </asp:TreeView>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Requirements" OnDemandMode="Once" ID="TabRequirements" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="RequirementsPanel">
                                <asp:Panel ID="PanelRequir1" runat="server" CssClass="collapsePanel">
                                    <table>
                                        <tr>
                                            <td style="vertical-align: top; padding-left: 20px">
                                                <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                                    <asp:Button runat="server" ID="AddDocumentBtn" Text="New Document" CssClass="btn btn-info btn-small"
                                                        OnClick="OnAddDocumentClick" />
                                                    <asp:Button runat="server" ID="DelDocumentBtn" Text="Delete Document" CssClass="btn btn-info btn-small"
                                                        OnClick="OnDelDocumentClick" />
                                                    <asp:HiddenField runat="server" ID="CpDocIdHidden" />
                                                </asp:Panel>
                                            </td>
                                            <td colspan="2" style="vertical-align: top;">
                                                <asp:RadioButtonList ID="CpDocsList" runat="server" RepeatDirection="Horizontal"
                                                    DataTextField="Title" DataValueField="CpDocId" BorderStyle="None" Style="margin-left: 5px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="OnCpDocsListChanged" CellPadding="1"
                                                    CellSpacing="1" RepeatColumns="6" Font-Size="X-Small">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr style="padding: 3px">
                                            <td style="vertical-align: top; padding-left: 20px">
                                                <asp:Label ID="Label6" runat="server" Text="What to do" CssClass="label"></asp:Label>
                                                <asp:DataGrid runat="server" ID="MeasureGroupGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                    DataKeyField="UniqueKey" OnItemDataBound="OnMeasureGroupDataBound">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="MeasureGroupId" HeaderText="MeasureGroupId" Visible="False" />
                                                        <asp:BoundColumn DataField="MeasureGroupName" HeaderText="Property Name"/>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>Rechecks</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Panel ID="Panel3" runat="server" DefaultButton="ApplyRechecksButton" >
                                                                    <asp:ImageButton runat="server" ID="ApplyRechecksButton" Style="display: none" OnClick="OnDocRechecksEnter"
                                                                        ImageUrl="" />
                                                                    <asp:TextBox runat="server" ID="RechecksField" ></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
                                                                        TargetControlID="RechecksField" FilterType="Numbers" />
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                </asp:DataGrid>
                                                                                                
                                                <asp:TextBox runat="server" ID="DocDescripField" TextMode="MultiLine" Width="250px"
                                                    ToolTip="Document description" Height="100px"></asp:TextBox><br/>

                                                <asp:CheckBox ID="IsReturnDoc" runat="server" Text="Return Item to Customer"
                                                    CssClass="checkbox" Width="200px" OnCheckedChanged="OnVendorAsCustomerChanging"
                                                    AutoPostBack="True" />
                                            </td>
                                            <td style="vertical-align: top; padding-left: 10px;font-family: sans-serif">
                                                <table>
                                                    <tr >
                                                        <td colspan="2" style="padding-top: 0px">
                                                            <asp:Label runat="server" ID="Label16" Text="Attached Reports" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td style="text-align: right;padding-top: 0px">
                                                            <asp:ImageButton runat="server" ID="AddPrintDocToDocBtn" OnClick="AddPrintDocToDocClick"
                                                                ImageUrl="~/Images/ajaxImages/attachEdit.png" ToolTip="Attach Print Doc" />
                                                            <asp:ImageButton runat="server" ID="DelDocPrintDocBtn" OnClick="OnDelDocPrintDocClick"
                                                                ImageUrl="~/Images/ajaxImages/delete.png" ToolTip="Detach selected Print Doc" />
                                                            <a href="DefineDocument.aspx" target="_blank" id="ViewLink" runat="server" >View</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" >
                                                            <asp:ListBox runat="server" ID="DocAttachedReportsList" Rows="5" Width="250px" DataTextField="OperationTypeName"
                                                                DataValueField="Key" OnSelectedIndexChanged="OnDocAttachPrintDocSelection" AutoPostBack="True"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:TreeView ID="DocPartTree" runat="server" OnSelectedNodeChanged="OnDocPartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                </asp:TreeView>
                                            </td>
                                            <td style="vertical-align: top; padding-left: 20px; width: 650px" rowspan="2">
                                                <asp:Label runat="server" ID="RuleCountLabel" CssClass="label" ></asp:Label>
                                                <asp:DataGrid runat="server" ID="RulesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                    DataKeyField="UniqueKey" OnItemDataBound="OnRuleItemDataBound">
                                                    <Columns>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="RuleResetBtn" runat="server" Text="Reset" OnClick="OnRuleResetBtnClick"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MeasureClass" HeaderText="Measure Class" Visible="False"></asp:BoundColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>Min Value</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Panel runat="server" ID="RuleMinValuePanel" CssClass="form-inline">
                                                                    <asp:TextBox runat="server" ID="RuleMinNumericFld" Width="135px"></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMinExtender" runat="server" FilterType="Custom"
                                                                        ValidChars="0123456789." TargetControlID="RuleMinNumericFld" />
                                                                    <asp:DropDownList runat="server" ID="RuleMinEnumFld" DataTextField="MeasureValueName"
                                                                        DataValueField="MeasureValueId" Width="150px"/>
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>Max Value</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Panel runat="server" ID="RuleMaxValuePanel" CssClass="form-inline">
                                                                    <asp:TextBox runat="server" ID="RuleMaxNumericFld"  Width="135px"></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMaxExtender" runat="server" FilterType="Custom"
                                                                        ValidChars="0123456789." TargetControlID="RuleMaxNumericFld" />
                                                                    <asp:DropDownList runat="server" ID="RuleMaxEnumFld" DataTextField="MeasureValueName"
                                                                        DataValueField="MeasureValueId" Width="150px"/>
                                                                    
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>
                                                                Is Default
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div style="text-align: center;">
                                                                    <asp:CheckBox runat="server" Text='' ID="CheckBoxIsDefault"></asp:CheckBox>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>
                                                                Not Visible
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div style="text-align: center;">
                                                                    <asp:CheckBox runat="server" Text='' ID="CheckBoxNotVisibleInCcm" />
                                                                </div>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Pricing" OnDemandMode="Once" ID="TabPricing" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="PricingPanel" DefaultButton="PriceDefaultBtn">
                                <asp:Button runat="server" ID="PriceDefaultBtn" Style="display: none"/>
                                <asp:Panel runat="server" ID="PanelPrice1" CssClass="collapsePanel">
                                    <table style="font-family: sans-serif">
                                        <tr style="font-weight: bold;">
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                <asp:Label ID="Label11" runat="server" Text="Item Structure Tree" CssClass="label"></asp:Label>
                                                
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px">
                                                <asp:Label ID="Label12" runat="server" Text="Item Type Props" CssClass="label"></asp:Label>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                <asp:Label runat="server" Text="Pass" CssClass="label"></asp:Label>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                
                                                <asp:Label ID="Label10" runat="server" Text="Fail" CssClass="label"></asp:Label>
                                            </td>
                                           <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                <asp:Label ID="Label18" runat="server" Text="Currency" CssClass="label"></asp:Label>
                                            </td>				 
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; padding-top: 5px;font-family: sans-serif">
                                                <asp:TreeView ID="PricePartTree" runat="server" OnSelectedNodeChanged="OnPricePartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                </asp:TreeView>
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px">
                                                <asp:ListBox runat="server" ID="PartMeasureList" DataTextField="MeasureName" DataValueField="MeasureId"
                                                    Rows="10" style="width: 200px"/>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px; border: dotted silver 1px">
                                                <table>
                                                    <tr>
                                                        <td rowspan="2">
                                                            <asp:RadioButtonList runat="server" ID="IsFixedFld" Width="100px" OnSelectedIndexChanged="OnPricingModeChanges" AutoPostBack="True">
                                                                <asp:ListItem Value="1" Text="Fixed" />
                                                                <asp:ListItem Value="0" Text="Dynamic" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            Price
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:TextBox runat="server" ID="PassFixedPrice" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender1" runat="server"
                                                                ValidChars="0123456789." TargetControlID="PassFixedPrice" Enabled="True" />
                                                        </td>
                                                        <td style="vertical-align: top; padding-left: 10px; text-align: left;margin-right: 10px">
                                                            Discount (%)
                                                            <asp:TextBox runat="server" ID="PassDiscount" Width="60px" ></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender2" runat="server"
                                                                ValidChars="0123456789." TargetControlID="PassDiscount" Enabled="True" />

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-right: 10px">
                                                            Delta Fix
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="DeltaFix" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender3" runat="server"
                                                                ValidChars="0123456789." TargetControlID="DeltaFix" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="vertical-align: top">
                                                            <asp:ListBox runat="server" ID="PricePartMeasureList" DataTextField="PartNameMeasureName"
                                                                DataValueField="MeasureCode" Width="280px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label14" runat="server" Text="Price Ranges" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td style="text-align: right;padding-left: 10px">
                                                            <asp:Button runat="server" ID="AddPriceRangeBtn" Text="Add" OnClick="OnAddPriceRangeBtnClick"
                                                                CssClass="btn btn-small btn-default" />
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding-top: 6px;width: 220px">
                                                            <asp:DataGrid runat="server" ID="PriceRangeGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                                OnItemDataBound="OnPriceRangeDataBound" OnItemCommand="OnRemovePriceRangeCommand" 
                                                                DataKeyField="Rownum">
                                                                <Columns>
                                                                    <asp:ButtonColumn Text="Del"/>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            From</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="FromField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FromExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="FromField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            To</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="ToField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ToExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="ToField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Price</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="PriceField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="PriceExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="PriceField" Enabled="True" />

                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                            </asp:DataGrid>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px; margin-left: 5px;border: dotted silver 1px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Fixed Price
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="FailFixedFld" Width="60px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender4" runat="server"
                                                                ValidChars="0123456789." TargetControlID="FailFixedFld" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Discount (%)
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="FailDiscountFld" Width="60px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender5" runat="server"
                                                                ValidChars="0123456789." TargetControlID="FailDiscountFld" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                             <td style="vertical-align: top;">
                                               <asp:DropDownList ID="ddlCurrency" runat="server" DataTextField="Currency" DataValueField="CurrencyID" ToolTip="Currency Name" Width="150px" style="margin-bottom:10px" />
                                               <br />
                                               <asp:Button runat="server" ID="btnChangeCurrency" CssClass="btn btn-small btn-default" Text="Set" OnClick="btnChangeCurrency_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td style="padding-left: 20px;padding-top: 10px">
                                                <table>
                                                    <tr style="padding-top: 10px">
                                                        <td colspan="2">
                                                            <asp:Label ID="Label13" runat="server" Text="Additional Services" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td  style="text-align: right;padding-left: 10px">
                                                            <asp:Button runat="server" ID="AddServiceBtn" OnClick="OnAddServiceBtnClick" CssClass="btn btn-small btn-default"
                                                                Text="Add" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="width: 280px">
                                                            <asp:DataGrid runat="server" ID="AddServicePriceGrid" AutoGenerateColumns="False"
                                                                CssClass="table table-condensed" OnItemDataBound="OnAddServicePriceDataBound"
                                                                DataKeyField="Rownum" OnItemCommand="OnDelAddServiceCommand">
                                                                <Columns>
                                                                    <asp:ButtonColumn Text="Del" />
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Service Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList runat="server" ID="AddServiceField" CssClass="" DataValueField="ServiceId"
                                                                                DataTextField="ServiceName" OnSelectedIndexChanged="OnAddServiceChanged" style="width: 150px">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Price</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="ServicePriceField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ServPriceExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="ServicePriceField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>    

                </ajaxToolkit:TabContainer>
                
                <asp:Button ID="btnModalPopUp" runat="server" Style="display: none" />
                
                <%--  On Attach Print Doc to Document --%>
                <asp:Panel runat="server" ID="AttachToDocPanel" CssClass="modalPopup"  Style="display: none">
                    <asp:Panel ID="AttachToDocDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div>
                            <p>Attach Print Doc to Document:</p>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="form-inline" Style="padding-top: 10px">
                        Print Docs:
                        <asp:DropDownList runat="server" ID="AvailPrintDocsForDocument" DataValueField="Key" DataTextField="OperationTypeName"/>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="AttachToDocOkBtn" runat="server" OnClick="OnAttachToDocOkClick" Text="Add"/>
                            <asp:Button ID="AttachToDocCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenBtnDoc" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBtnDoc" PopupControlID="AttachToDocPanel" ID="AttachToDocPopupExtender"
                PopupDragHandleControlID="AttachToDocDragHandle" OkControlID="AttachToDocCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- New Sku parameters dialog --%>
                <asp:Panel runat="server" ID="ChoiceItemTypePanel" CssClass="modalPopup" Width="910px" Style="display: none;border: solid 2px #5377A9;" >
                    <asp:Panel ID="ItemTypePanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 30px">
                        <div style="text-align: left;color:#5377A9;font-weight: bold ">
                            <b>New SKU Parameters</b>
                        </div>
                    </asp:Panel>
                    <asp:Panel CssClass="form-inline" DefaultButton="NewProgramNameBtn" runat="server" Style="padding-top: 5px">
                        <strong>New Program Name*:</strong>
                        <asp:TextBox runat="server" ID="NewProgramNameFld"></asp:TextBox>
                        <asp:Button runat="server" ID="NewProgramNameBtn" Style="display: none"  OnClick="OnNewProgramNameClick"/>
                        <asp:Label runat="server" ID="ErrNewCpNameLabel" ForeColor="Red"></asp:Label>
                    </asp:Panel>
                    <table>
                        <tr>
                            <td><asp:Label Text="Item Type Groups" CssClass="label" runat="server"/></td>
                            <td style="padding-left: 2px"><asp:Label ID="Label15" Text="Item Types" CssClass="label" runat="server"/></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <div runat="server" id="ItemTypeGrpDiv" style="height: 300px;overflow-y: auto">
                                    <asp:RadioButtonList ID="ItemGroupsList" runat="server" DataValueField="ItemTypeGroupId"
                                        DataTextField="Title" Width="200px" RepeatDirection="Vertical" BorderStyle="None"
                                        Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnItemGroupsListChanged"
                                        CellPadding="1" CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                            <td style="vertical-align: top;padding-left: 2px">
                                <div style="overflow-y: auto;height: 300px;width: 600px" runat="server" id="ItemTypeDiv">
                                    <asp:RadioButtonList ID="ItemTypesList" runat="server" DataValueField="ItemTypeId" Width="700px"
                                        DataTextField="Title" RepeatDirection="Vertical" BorderStyle="None" Style="margin-left: 5px"
                                        AutoPostBack="True" OnSelectedIndexChanged="OnItemTypesListChanged" CellPadding="1"
                                        CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="ChoiceItemTypeOkButton" runat="server" OnClick="OnNewSkuDialogOkClick" Text="Ok" />
                            <asp:Button ID="ChoiceItemTypeCancelButton" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="NewSkuPopupExtender" runat="server" TargetControlID="HiddenNewSkuBtn"
                    PopupControlID="ChoiceItemTypePanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="ItemTypePanelDragHandle"
                    OkControlID="ChoiceItemTypeCancelButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Save As Dialog--%>
                <asp:Panel runat="server" ID="SaveAsPanel" CssClass="modalPopup"  Style="display: none;width: 430px">
                    <asp:Panel ID="SaveAsDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div><p>Save Customer Program as</p>
                        </div>
                    </asp:Panel><br/>
                    <asp:Panel ID="Panel5" runat="server" DefaultButton="SaveAsNameBtn">
                        <table>
                            <tr >
                                <td>Save as</td>
                                <td><asp:TextBox runat="server" ID="SaveAsNameFld" Width="280px"></asp:TextBox></td>
                                <td><asp:Button runat="server" ID="SaveAsNameBtn" OnClick="OnSaveAsNameEnter" Style="display: none"/></td>
                            </tr>
                            <tr>
                                <td colspan="3"><asp:Label runat="server" ID="SaveAsErrLabel" ForeColor="DarkRed"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    Customer:
                                </td>
                                <td style="padding-top: -5px; padding-right: 5px">
                                    <asp:DropDownList ID="SaCustomerList" runat="server" DataTextField="CustomerName"
                                        AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnSaCustomerSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Height="26px" />
                                </td>
                                <td style="padding-top: 5px">
                                    <asp:Panel ID="Panel7" runat="server" DefaultButton="SaCustomerLikeButton">
                                        <asp:TextBox runat="server" ID="SaCustomerLike" Width="55px" Height="18px"></asp:TextBox>
                                        <asp:ImageButton ID="SaCustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                            ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnSaCustomerSearchClick" Style="display: none" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>Vendor:</td>
                                <td style="padding-top: -5px; padding-right: 5px">
                                    <asp:DropDownList ID="SaVendorList" runat="server" AutoPostBack="True" CssClass=""
                                        DataTextField="CustomerName" DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnSaVendorSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Enabled="False" />
                                </td>
                                <td style="padding-top: 5px">
                                    <asp:Panel ID="Panel8" runat="server" DefaultButton="SaVendorLikeButton">
                                        <asp:TextBox ID="SaVendorLike" runat="server" Width="55px" Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="SaVendorLikeButton" runat="server" ImageUrl="~/Images/ajaxImages/search.png"
                                            OnClick="OnSaVendorSearchClick" ToolTip="Filtering the list of customers" Style="display: none" />
                                    </asp:Panel>
                                </td>

                            </tr>
                            <tr>
                                <td></td>
                                <td style="vertical-align: middle">
                                    <asp:CheckBox ID="SaVendorAsCustomer" runat="server" Checked="True" Text="the same as Customer"
                                        CssClass="checkbox" Width="250px" OnCheckedChanged="OnSaVendorAsCustomerChanging"
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="SaveAsOkBtn" runat="server" OnClick="OnSaveAsOkClick" Text="Save"/>
                            <asp:Button ID="SaveAsCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenSaveAsBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="SaveAsHiddenBtn" PopupControlID="SaveAsPanel" ID="SaveAsPopupExtender"
                PopupDragHandleControlID="SaveAsDragHandle" OkControlID="SaveAsCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <b>Save Customer Program</b>
                        </div>
                    </asp:Panel>
                    <div><asp:CheckBox runat="server" ID="ApplyAllCp" Text="Apply changes to all Customer Program copies?"/></div><br/>
                    <div style="color: black;padding-top: 10px">Are you sure you want to save this Customer Program?</div>
                    <div ><asp:HiddenField runat="server" ID="SaveMode"/></div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender"
                PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesSaveNoBtn" >
                </ajaxToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
