﻿using System;
using System.Net;
using System.Net.Mail;
using Corpt.Constants;
using Corpt.Utilities;

namespace Corpt
{
    public partial class EmailTest : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }
            SendButton.Attributes.Add("onclick", "return confirm('Are you sure you want to send?');");
            SettingFld.SelectedValue = "VvUser";
            OnSettingModeChanges(null, null);
        }

        protected void OnSendClick(object sender, EventArgs e)
        {
            try
            {
                var fromAddr = FromAddress.Text;
                var toAddr = ToAddress.Text;    
                var password = FromPassword.Text;

                var msg = new MailMessage
                    {Subject = Subject.Text, From = new MailAddress(fromAddr), Body = BodyContent.InnerHtml};
                msg.To.Add(new MailAddress(toAddr));
               // msg.Attachments.Add(new Attachment(AttachFile.Text));
                var smtp = new SmtpClient
                {
                    Host = Host.Text, 
                    Port = Convert.ToInt32(Port.Text), 
                    UseDefaultCredentials = UseDefault.Checked, 
                    EnableSsl = EnableSsl.Checked
                };
                var nc = new NetworkCredential(fromAddr, password);
                smtp.Credentials = nc;
                smtp.Send(msg);
                sendResult.Text = "OK";
            } catch (Exception ex)
            {
                sendResult.Text = ex.Message;
            }

        }

        protected void OnSettingModeChanges(object sender, EventArgs e)
        {

            var settings = SettingFld.SelectedValue;
            if (settings == "VvUser")
            {
                Host.Text = "" + Session[SessionConstants.GlobalVvEmailHost];
                Port.Text = "" + Session[SessionConstants.GlobalVvEmailPort];
                EnableSsl.Checked = ("" + Session[SessionConstants.GlobalVvEmailSsl]) =="Y";
                FromAddress.Text = "" + Session[SessionConstants.GlobalVvEmailFromAddr];
                FromPassword.Text = "" + Session[SessionConstants.GlobalVvEmailFromPassw];
                Subject.Text = "GSI#";
                BodyContent.InnerHtml = ExcelUtils.EMailVvBody;
                //Body.Text = ExcelUtils.EMailVvBody;
            } else
            {
                Host.Text = "" + Session[SessionConstants.GlobalEmailHost];
                Port.Text = "" + Session[SessionConstants.GlobalEmailPort];
                EnableSsl.Checked = ("" + Session[SessionConstants.GlobalEmailSsl]) == "Y";
                FromAddress.Text = "" + Session[SessionConstants.GlobalEmailFromAddr];
                FromPassword.Text = "" + Session[SessionConstants.GlobalEmailFromPassw];
                Subject.Text = "Report from GSI";
                BodyContent.InnerHtml = "See attached files.<br/><i>Gemological Science International.</i>";
                //Body.Text = "See attached files.<br/><i>Gemological Science International.</i>";
            }
        }
    }
}