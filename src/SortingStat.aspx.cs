﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class SortingStat : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            calFrom.Focus();
            CalendarExtenderFrom.SelectedDate = DateTime.Now.Date;
            CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
            
        }
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
            OnLoadClick(null, null);
        }

        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
            OnLoadClick(null, null);
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            var data = QueryUtils.GetSortingStats(CalendarExtenderFrom.SelectedDate, CalendarExtenderTo.SelectedDate,
                                                  this);

            SetViewState(data, SessionConstants.SortingStatList);
            UpdateDataView();
        }
        private void UpdateDataView()
        {
            grdOrders.DataSource = null;
            grdOrders.DataBind();
            RowsCount.Text = "";
            var data = GetViewState(SessionConstants.SortingStatList) as List<SortingStatModel>;
            if (data == null || data.Count == 0) return;
            //-- create table
            var table = new DataTable("Orders");
            table.Columns.Add("Order Date", typeof (DateTime));
            table.Columns.Add("Company Name", typeof(string));
            table.Columns.Add("Customer Code", typeof(string));
            table.Columns.Add("Order Code", typeof(string));
            table.Columns.Add("Batch Code", typeof(string));
            table.Columns.Add("Customer Program Name", typeof(string));
            table.Columns.Add("Total Weight", typeof(string));
            table.Columns.Add("Failed Col/Clar", typeof(string));
            table.Columns.Add("Failed Pol/Sym", typeof(string));
            table.Columns.Add("Failed CutGrade", typeof(string));
            table.Columns.Add("Total Pass", typeof(string));
            table.Columns.Add("Total Failed", typeof(string));
            table.Columns.Add("Check", typeof(string));
            foreach(var item in data)
            {
                table.Rows.Add(new object[]
                {
                    item.OrderDate,
                    item.CompanyName,
                    item.CustomerCode,
                    item.OrderCode,
                    item.BatchCode,
                    item.CpName,
                    item.TotalWeight.ToString("0.0000"),
                    item.FailedColClar.ToString("0.0000"),
                    item.FailedPolSym.ToString("0.0000"),
                    item.FailedCutGrade.ToString("0.0000"),
                    item.TotalPass.ToString("0.0000"),
                    item.TotalFailed.ToString("0.0000"),
                    item.CheckColumn.ToString("0.0000")
                });
            }
            table.AcceptChanges();
            var dv = new DataView { Table = table };
            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "Order Date";
                SetViewState(new SortingModel { ColumnName = "Order Date", Direction = true }, SessionConstants.CurrentSortModel);
            }
            RowsCount.Text = table.Rows.Count + " rows";
            grdOrders.DataSource = dv;
            grdOrders.DataBind();

        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            UpdateDataView();

        }
    }
}