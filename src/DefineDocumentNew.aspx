﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="DefineDocumentNew.aspx.cs" Inherits="Corpt.DefineDocumentNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%-- IvanB start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<%-- IvanB end --%>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black;/* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel {
	        padding-bottom:2px;
	        color:#5377A9;
	        font-family:Arial, Sans-Serif;
	        font-weight:bold;
	        font-size:1.0em;
        }
    </style>
    <script type="text/javascript">
        <%--IvanB start--%>

        <%-- init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used --%>
        function pageLoad() {
            $("#<%= DocumentList.ClientID %>").select2({});
            $("#<%= CorelFilesList.ClientID %>").select2({});
        }
        <%--IvanB end--%>
    </script>
    <%--Nimesh code Start--%>
    <asp:HiddenField ID="focusedControlId" runat="server" />
     <script>
         document.addEventListener('DOMContentLoaded', function () {
             document.addEventListener('focusin', function (event) {
                 var focusedControl = event.target;
                 var focusedControlId = focusedControl.id || '';

                 // Check if the focused control is within the DataGrid
                 var dataGrid = document.getElementById('<%= ValuesGrid.ClientID %>');
                 if (dataGrid && dataGrid.contains(focusedControl)) {
                     document.getElementById('<%= focusedControlId.ClientID %>').value = focusedControlId;
                 }
             });
         });
     </script>
    <%--Nimesh code End--%>
    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        
        <div class="demoheading"><asp:Label ID="PageTitleLabel" runat="server" Font-Size="80%"></asp:Label></div>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <ContentTemplate>
                <div class="navbar nav-tabs" style="padding-bottom: 1px;padding-top: 1px;margin-bottom: 1px">
                    <asp:Button runat="server" ID="SaveAsBtn" Text="Save As" Enabled="False" CssClass="btn btn-small btn-info" OnClick="OnSaveAsClick" />
                    <asp:Button runat="server" ID="SaveBtn" Text="Save" CssClass="btn btn-small btn-info" OnClick="OnSaveClick" Enabled="False" OnClientClick = 'return confirm("Are you sure you want to Save?");'/>
                    <asp:Button runat="server" ID="AttachBtn" Text="Attach" CssClass="btn btn-small btn-info" OnClick="OnAttachClick" Enabled="False" OnClientClick = 'return confirm("Are you sure you want to Attach?");'/>
                    <asp:Button runat="server" ID="BackToSku" Text="Back to SKU" CssClass="btn btn-small btn-info" OnClick="OnBackToSkuClick" />
                </div>
                <table>
                    <tr>
                        <td style="text-align: center"><asp:Label runat="server" ID="CpPathToPicture"></asp:Label></td>
                        <td style="padding-left: 15px">
                            <asp:Label ID="DocsLabel" runat="server" Text="Documents" CssClass="label"></asp:Label>
                            <asp:RequiredFieldValidator ID="DocumentListValidator" runat="server" ControlToValidate="DocumentList"
                                ErrorMessage="" Text="*" ValidationGroup="DocumentValidatorGrp" ToolTip="Document is required"></asp:RequiredFieldValidator>
                        </td>
                        <div>
                            <td style="padding-left: 15px"><asp:Label ID="CorelLabel" runat="server" Text="Corel Files" CssClass="label"></asp:Label>
                                <asp:CheckBox ID="AllCDRFiles" runat="server" Checked="False" Text="All CDR Files"
                                    CssClass="checkbox" Width="130px" OnCheckedChanged="OnAllCDRFiles_Changed"
                                    AutoPostBack="True" />
                            </td>
                        </div>
                        <td style="padding-left: 15px"><asp:Label ID="PartLabel" runat="server" Text="Parts" CssClass="label"></asp:Label></td>
                        <td style="padding-left: 15px"><asp:Label ID="MeasureLabel" runat="server" Text="Measures" CssClass="label"></asp:Label></td>
                        <td style="padding-left: 15px">
                            <asp:Label ID="TitleLabel" runat="server" Text="Titles" CssClass="label"></asp:Label>
                            <asp:DropDownList runat="server" ID="LanguageList" OnSelectedIndexChanged="OnLanguageChanged" DataTextField="Name" DataValueField="Id" Width="100px"
                            style="margin-left: 10px"/>
                        </td>
                        <td style="padding-left: 15px"><asp:Label ID="DetailLabel" runat="server" Text="Formats" CssClass="label"></asp:Label></td>
                        <td style="width: 15px"></td>
                        <td style="text-align: left"><asp:Label ID="Label2" runat="server" Text="Separator" CssClass="label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <div>
                                <asp:Image ID="itemPicture" runat="server" Width="150px" Visible="False"></asp:Image><br />
                                <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Width="150px"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="ErrorText" runat="server" Visible="false" ></asp:TextBox>
                            </div>
                        </td>
                        <td style="padding-left: 15px; vertical-align: top">
                            <asp:DropDownList runat="server" ID="DocumentList" OnSelectedIndexChanged="OnDocumentChanged"
                                DataTextField="DocumentName" DataValueField="DocumentId" AutoPostBack="True" />
                            <br />
                            <asp:RequiredFieldValidator ID="CorelFileValidator" runat="server" ControlToValidate="CorelFilesList"
                                ErrorMessage="" Text="*" ValidationGroup="DocumentValidatorGrp" ToolTip="Document is required"></asp:RequiredFieldValidator>
                            <br />
                            <asp:Label runat="server" ID="RepDirLabel" Width="200px"></asp:Label><br />
                            <asp:Button runat="server" ID="DownloadBtn" Text="Download" Visible="False" OnClick="OnDownloadClick"
                                CssClass="btn btn-small btn-default" />
                        </td>
                        <td style="vertical-align: top; padding-left: 15px">
                            <asp:ListBox runat="server" ID="CorelFilesList" DataTextField="DisplayName" DataValueField="Name"
                                Rows="13" OnSelectedIndexChanged="CorelFilesList_SelectedIndexChanged" />
                        </td>
                        <td style="vertical-align: top;padding-left: 15px">
                            <asp:TreeView ID="PartTree" runat="server" OnSelectedNodeChanged="OnPartsTreeChanged">
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                            </asp:TreeView>
                        </td>
                        <td style="vertical-align: top; padding-left: 15px">
                            <asp:ListBox runat="server" ID="MeasureList" DataTextField="MeasureTitle" DataValueField="MeasureId" Rows="15" Width="160px"/>
                        </td>
                        <td style="padding-left: 15px;vertical-align: top">
                            <asp:ListBox runat="server" ID="TitleList" Rows="15" DataTextField="TitleName" DataValueField="TitleCode" Width="160px"/>
                        </td>
                        <td style="padding-left: 20px;">
                                                <asp:Panel ID="NewCorelFilePanel" runat="server">
                                                    <asp:Label runat="server" ID="NewCorelFileField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                                                    <div >
						                                <asp:ImageButton ID="NewCorelFileButton" runat="server" Width="300px" ToolTip="New Report Picture button" OnClick="OnNewPictureClick"></asp:ImageButton><br />
                                                    </div>
                                                    <%--<asp:Image id="NewCorelFilePicture" ImageUrl="~/Images/ajaxImages/studswithss.pdf" runat="server" Width="300px" BorderWidth="1px"></asp:Image>--%>
                                                </asp:Panel>
                                            </td>
                        <td style="padding-left: 15px;vertical-align: top" runat="server" ID="ImportExport">
                            <table>
                                <tr>
                                    <td>Import</td>
                                    <td><asp:DropDownList runat="server" ID="ImportList" DataTextField="Name" DataValueField="Id" Width="120px"/></td>
                                </tr>
                                <tr>
                                    <td>Export</td>
                                    <td><asp:DropDownList runat="server" ID="ExportList" DataTextField="Name" DataValueField="Id" Width="120px"/></td>
                                </tr>
                                <tr>
                                    <td>File Format</td>
                                    <td><asp:DropDownList runat="server" ID="FormatList" DataTextField="Name" DataValueField="Id" Width="120px"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top">Use</td>
                                    <td >
                                        <asp:CheckBox runat="server" CssClass="radio" Text="Date" ID="UseDateCb" style="text-align: left; padding-left: 0;"/><br/>
                                        <asp:CheckBox runat="server" CssClass="radio" Text="VV Number" ID="UseVvNumberCb" style="text-align: left; padding-left: 0;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top">
                                        Bar Code
                                        <asp:RequiredFieldValidator ID="BarCodeTextValidator" runat="server" ControlToValidate="FixedTextFld"
                                            ErrorMessage="" Text="*" ValidationGroup="DocumentValidatorGrp" ToolTip="Bar Code is required"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList runat="server" ID="BarCodeOptionList" OnSelectedIndexChanged="OnBarCodeOptionChanged" AutoPostBack="True">
                                            <asp:ListItem Value="1">Report Number</asp:ListItem>
                                            <asp:ListItem Value="2">Fixed Text</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><asp:TextBox runat="server" ID="FixedTextFld" Width="110px"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 15px"></td>
                        <td style="vertical-align: top;">
                            <asp:RadioButtonList runat="server" ID="SeparatorList" RepeatDirection="Vertical"
                                Style="vertical-align: top; margin-left: 10px" DataTextField="DisplayName" DataValueField="Value" >
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 5px;padding-top: 5px;height: 400px; overflow:auto; width: 90%">
                    <asp:DataGrid runat="server" ID="ValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                                OnItemDataBound="OnValuesDataBound"  
                                                                DataKeyField="DocumentValueId" >
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="8%">
                                <HeaderTemplate></HeaderTemplate>
                                <ItemTemplate >
                                    <asp:Panel runat="server" CssClass="form-inline" Width="100%">
                                        <asp:CheckBox runat="server" ID="SelectCheckBox" Width="16px" OnCheckedChanged="OnCheckRowChanged"
                                            CssClass="buttonStyle" AutoPostBack="True" ToolTip="Edit Line"></asp:CheckBox>
                                        <asp:ImageButton ID="MoveUpBtn" runat="server" ToolTip="Move Up" ImageUrl="~/Images/ajaxImages/up.png"
                                            OnClick="OnMoveUpRowClick" />
                                        <asp:ImageButton ID="MoveDownBtn" runat="server" ToolTip="Move Down" ImageUrl="~/Images/ajaxImages/down.png"
                                            OnClick="OnMoveDownRowClick" />
                                        <asp:ImageButton ID="DeleteBtn" runat="server" ToolTip="Delete Line" ImageUrl="~/Images/ajaxImages/delete.png"
                                            OnClick="OnDelClick" />
                                        <asp:ImageButton ID="InsertBtn" runat="server" ToolTip="Insert Line" ImageUrl="~/Images/ajaxImages/insert.png"
                                            OnClick="OnInsertClick" />
                                    </asp:Panel>
                                </ItemTemplate>
                                <ItemStyle Width="8%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-Width="15%">
                                <HeaderTemplate>
                                    Title</HeaderTemplate>
                                <ItemTemplate >
                                    <asp:TextBox runat="server" ID="TitleField" TextMode="MultiLine" Width="95%"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="15%" />
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn  ItemStyle-Width="70%">
                                <HeaderTemplate>
                                    Value</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="ValueField" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="70%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-Width="5%">
                                <HeaderTemplate>
                                    Unit</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="UnitField" TextMode="MultiLine" Width="90%"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                    </asp:DataGrid>
                </div>
                <div>
                    <asp:Button runat="server" ID="AddLineBtn" Text="Add Line" CssClass="btn btn-small btn-info" OnClick="OnAddLineClick"/>
                </div>
<%--                Save As Panel--%>
                <asp:Panel runat="server" ID="SaveAsPanel"  CssClass="modalPopup" Width="250px" Style="display: none">
                    <asp:Panel ID="SaveAsPanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Gray; color: darkblue">
                        <div style="text-align: center"><b>Save with Name</b></div>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="form-inline" Style="padding-top: 5px;" >
                        Report Name:
                        <asp:TextBox runat="server" ID="NewReportName" Width="95%"></asp:TextBox>
                    </asp:Panel>
                    
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="SaveAsOkButton" runat="server" OnClick="OnSaveAsOkClick" Text="Ok" />
                            <asp:Button ID="SaveAsCancelButton" runat="server" Text="Cancel" />
                        </p>
                    </div>
                    <div><asp:Label runat="server" ID="SaveAsMessageLbl" ForeColor="DarkRed"></asp:Label></div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="SaveAsPopupExtender" runat="server" TargetControlID="SaveAsBtn"
                    PopupControlID="SaveAsPanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="SaveAsPanelDragHandle"
                    OkControlID="SaveAsCancelButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <asp:Label runat="server" ID="InfoTitle"></asp:Label>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>

				<%--new picture popup --%>
                <asp:Panel runat="server" ID="Panel3" CssClass="modalPopup" Width="600px" Style="display: none; position:center">
                    <asp:HiddenField runat="server" ID="HiddenUrl" ></asp:HiddenField>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Image ID="NewCorelFilePicture" runat="server" BorderWidth="1px" ></asp:Image>
                        </p>
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="NewPictureOkButton" runat="server" Text="OK" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenNewPictureBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenNewPictureBtn" PopupControlID="Panel3" ID="NewPicturePopupExtender" 
                    OkControlID="NewPictureOkButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>
