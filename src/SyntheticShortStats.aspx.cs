﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using Corpt.TreeModel;

namespace Corpt
{
    public partial class SyntheticShortStats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid(0);
                InsertBlankRecord();
                LoadOfficesTree(); //Office IDs
                grdClosedScreening.Visible = false;
                //gvTitle.InnerText = "Screening Result - Top 50";
                //txtNotes.Value = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SyntheticShortStatsModel synScrData = new SyntheticShortStatsModel();
            synScrData.GSIOrder = int.Parse(txtGsiOrder.Value);
            synScrData.CustomerCode = int.Parse(txtVendorNum.Value);
            synScrData.TotalQty = int.Parse(txtTotalQTY.Value);
            synScrData.QtyPass = int.Parse(txtQtyPass.Value);
            synScrData.QtySynth = int.Parse(txtQtySynth.Value);
            synScrData.QtySusp = int.Parse(txtQtySusp.Value);
            synScrData.Memo = txtMemoNum.Value;
            synScrData.Notes = txtNotes.Value;
            synScrData.Destination = txtRetailer.Value;
            txtRetailer.Value = txtRetailer.Value.ToUpper();
            string status = GSIAppQueryUtils.SaveShortScreeningData(synScrData, this);
            if (status != "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order " + synScrData.GSIOrder + " not save.');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order saved successfully.');", true);
                BindGrid(0);
                ClearText();
            }
        }
        
        // By using this method we can convert datatable to xml
        protected void btnAddItem_Click(object sender, EventArgs e)
        {
        }

        public System.Web.UI.WebControls.SortDirection SortDirection { get; set; }
        protected void sort_table(object sender, GridViewSortEventArgs e)
        {
            List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            var sortField = e.SortExpression;
            DataSet dsScr = (DataSet)Session["ScreeningData"];
            string direction = null;
            if (System.Web.HttpContext.Current.Session["sortExpression"] != null)
            {
                string[] sortData = Session["sortExpression"].ToString().Trim().Split(' ');
                direction = sortData[1];
                if (e.SortExpression == sortData[0])
                {
                    if (sortData[1] == "ASC")
                    {
                        dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "DESC";
                        Session["sortExpression"] = e.SortExpression + " " + "DESC";
                    }
                    else
                    {
                        dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                        Session["sortExpression"] = e.SortExpression + " " + "ASC";
                    }
                }
                else
                {
                    dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                    Session["sortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                Session["sortExpression"] = e.SortExpression + " " + "ASC";
            }

            //lstScreening = (from DataRow row in dsScr.Tables[0].Rows select new SyntheticScreeningModel(row)).ToList();
            //SyntheticScreeningModel abc = new SyntheticScreeningModel();
            //lstScreening.Sort((x, y) => x.sortField.CompareTo(y.sortField));
            //lstScreening = lstScreening.Sort(x => x.sortField);
            lstScreening = (from DataRow row in dsScr.Tables[0].Rows select new SyntheticShortStatsModel(row)).ToList();

            //grdScreening.DataSource = lstScreening;
            Session["ScreeningData"] = dsScr;
            if (e.SortExpression == "TotalQty" || e.SortExpression == "GSIOrder" || e.SortExpression == "QtyPass" || e.SortExpression == "QtySynth" || e.SortExpression == "QtySusp")
            {
                if (dsScr.Tables[0].Columns.Contains("ForIntSort"))
                {
                    dsScr.Tables[0].Columns.Remove("ForIntSort");
                }
                string convertStr = @"Convert(" + e.SortExpression + @", 'System.Int32')";
                dsScr.Tables[0].Columns.Add("ForIntSort", typeof(int), convertStr);
                if (direction == "ASC")
                    dsScr.Tables[0].DefaultView.Sort = "ForIntSort DESC";
                else
                    dsScr.Tables[0].DefaultView.Sort = "ForIntSort ASC";
            }
            else if (e.SortExpression == "CreatedDate" || e.SortExpression == "ReadyDate" || e.SortExpression == "LastModifiedDate" || e.SortExpression == "PickupDate")
            {
                if (dsScr.Tables[0].Columns.Contains("ForDateSort"))
                {
                    dsScr.Tables[0].Columns.Remove("ForDateSort");
                }
                dsScr.Tables[0].Columns.Add("ForDateSort", typeof(DateTime), e.SortExpression);
                if (direction == "ASC")
                    dsScr.Tables[0].DefaultView.Sort = "ForDateSort DESC";
                else
                    dsScr.Tables[0].DefaultView.Sort = "ForDateSort ASC";
            }
            grdScreening.DataSource = dsScr.Tables[0].DefaultView;
            grdScreening.DataBind();

        }//sort_table

        protected void OnKeyPress(object sender, EventArgs e)
        {
            //int totalQty = Convert.ToInt32(txtTotalQTY);
            //int bagQty = Convert.ToInt32(txtBagQty);
            //int bagNum = totalQty / bagQty;
            txtBagNum.Value = (Convert.ToInt32(txtTotalQTY)/Convert.ToInt32(txtBagQty)).ToString();
        }
        public string ConvertDatatableToXML(DataSet ds)
        {
            MemoryStream str = new MemoryStream();
            ds.Tables[0].WriteXml(str, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }

        private void BindGrid(int orderCode)
        {
        	grdClosedScreening.Visible = false;
            string office = GetCheckedOffices();
            List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            DataSet dsScr = new DataSet();
            lstScreening = GSIAppQueryUtils.GetSyntheticShortInit(orderCode, office, this);
            foreach (var list in lstScreening)
            {
                if (list.Status.Contains("incomplete"))
                    list.PickupDate = null;
                else
                    list.PickupDate = list.LastModifiedDate;
                if (list.Destination != null && list.Destination != "")
                    list.Destination = list.Destination.ToUpper();
            }
            dsScr.Tables.Add(ToDataTable<SyntheticShortStatsModel>(lstScreening));
            totalRecNumber.Value = lstScreening.Count.ToString() + @" Items Found";
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }

        private void BindGrid(string strDestination)
        {
        	grdClosedScreening.Visible = false;
            var office = GetCheckedOffices();
            List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            DataSet dsScr = new DataSet();
            lstScreening = GSIAppQueryUtils.GetScreeningShortDetailsByDestination(strDestination, office, this);
            foreach (var list in lstScreening)
            {
                if (list.Status.Contains("incomplete"))
                    list.PickupDate = null;
                else
                    list.PickupDate = list.LastModifiedDate;
                if (list.Destination != null && list.Destination != "")
                    list.Destination = list.Destination.ToUpper();
            }
            dsScr.Tables.Add(ToDataTable<SyntheticShortStatsModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }
/* alex
        private void BindGrid(string strCreateFromDate, string strCreateToDate, string GSIOrder)
        {
            var office = GetCheckedOffices();
            List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            DataSet dsScr = new DataSet();
            if (GSIOrder == "")
                GSIOrder = null;
            if (strCreateFromDate == "")
                strCreateFromDate = null;
            if (strCreateToDate == "")
                strCreateToDate = null;
            lstScreening = GSIAppQueryUtils.GetShortStatsAll(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            foreach (var list in lstScreening)
            {
                if (list.Status.Contains("incomplete"))
                    list.PickupDate = null;
                else
                    list.PickupDate = list.LastModifiedDate;
                if (list.Destination != null && list.Destination != "")
                    list.Destination = list.Destination.ToUpper();
            }
            totalRecNumber.Value = lstScreening.Count.ToString() + @" Items Found";
            dsScr.Tables.Add(ToDataTable<SyntheticShortStatsModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
        }
        alex */
        
        private void BindGrid(string strCreateFromDate, string strCreateToDate, string GSIOrder)
        {
            grdClosedScreening.Visible = false;
            grdScreening.Visible = true;
            var office = GetCheckedOffices();
            if (office == null)
                office = "1";
            List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            DataSet dsScr = new DataSet();
            if (GSIOrder == "")
                GSIOrder = null;
            if (strCreateFromDate == "")
                strCreateFromDate = null;
            if (strCreateToDate == "")
                strCreateToDate = null;
            //lstScreening = GSIAppQueryUtils.GetShortStatsAll(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            //DataTable dt = GSIAppQueryUtils.GetShortStatsAll(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            DataSet ds = GSIAppQueryUtils.GetShortStatsAll(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            lstScreening = (from DataRow row in ds.Tables[0].Rows select new SyntheticShortStatsModel(row)).ToList();
            foreach (var list in lstScreening)
            {
                if (list.Status.Contains("incomplete"))
                    list.PickupDate = null;
                else
                    list.PickupDate = list.LastModifiedDate;
                string newDestination = GSIAppQueryUtils.GetCustomerEntriesDestination(list.GSIOrder.ToString(), this);
                if (newDestination != null)
                {
                    if (newDestination == "old system")
                    {
                        if (list.Destination != null && list.Destination != "")
                            list.Destination = list.Destination.ToUpper();
                    }
                    else
                    {
                        list.Destination = newDestination;
                    }
                }
                else
                {
                    if (list.Destination != null && list.Destination != "")
                        list.Destination = list.Destination.ToUpper();
                }
            }
            
            totalRecNumber.Value = lstScreening.Count.ToString() + @" Items Found";
            dsScr.Tables.Add(ToDataTable<SyntheticShortStatsModel>(lstScreening));
            //dsScr.Tables.Add(ToDataTable(lstScreening));
            Session["ScreeningData"] = dsScr;
            bool done = QueryUtils.TmpAddTblCount(ds.Tables.Count, this);
            if (ds.Tables.Count > 1)
            {
                dsScr.Tables[0].TableName = "Full Report";
                ds.Tables[1].TableName = "QC";
                dsScr.Tables.Add(ds.Tables[1].Copy());
                var lstQCScreening = (from DataRow row in ds.Tables[1].Rows select new SyntheticShortStatsQCModel(row)).ToList();
                if (lstQCScreening.Count > 0)
                {
                    //var destList = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
                    dsScr.Tables.Add(ToDataTable<SyntheticShortStatsQCModel>(lstQCScreening));
                    int count = dsScr.Tables.Count - 1;
                    dsScr.Tables[count].TableName = "QC1";
                }
                if (ds.Tables.Count > 2)//add qc+screening
                {
                    ds.Tables[2].TableName = "QC+Screening";
                    dsScr.Tables.Add(ds.Tables[2].Copy());
                    ds.Tables[3].TableName = "LG";
                    dsScr.Tables.Add(ds.Tables[3].Copy());
                    if (ds.Tables.Count > 4)
                    {
                        ds.Tables[4].TableName = "Surat";
                        dsScr.Tables.Add(ds.Tables[4].Copy());
                    }
                }
            }
            done = QueryUtils.TmpAddTblCount(dsScr.Tables.Count, this);
            BuildListByDestination(dsScr, ds);
            grdScreening.DataSource = lstScreening;
            grdScreening.DataBind();
            btnExportToExcelSorted_Click(null, null);
        }

        private void BindGrid(string strCreateFromDate, string strCreateToDate, string GSIOrder, bool closed)
        {
            grdClosedScreening.Visible = true;
            grdScreening.Visible = false;
            var office = GetCheckedOffices();
            if (office == null)
                office = "1";
            //List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
            List<SyntheticClosedStatsModel> lstScreening = new List<SyntheticClosedStatsModel>();
            DataSet dsScr = new DataSet();
            if (GSIOrder == "")
                GSIOrder = null;
            if (strCreateFromDate == "")
                strCreateFromDate = null;
            if (strCreateToDate == "")
                strCreateToDate = null;
            //lstScreening = GSIAppQueryUtils.GetShortStatsAll(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            DataTable dt = GSIAppQueryUtils.GetShortStatsClosed(strCreateFromDate, strCreateToDate, GSIOrder, office, this);
            lstScreening = (from DataRow row in dt.Rows select new SyntheticClosedStatsModel(row)).ToList();
            foreach (var list in lstScreening)
            {
                if (list.Status.Contains("incomplete"))
                    list.PickupDate = null;
                //else
                //    list.PickupDate = DateTime.Today;
                if (list.Destination != null && list.Destination != "")
                    list.Destination = list.Destination.ToUpper();
            }

            totalRecNumber.Value = lstScreening.Count.ToString() + @" Items Found";
            dsScr.Tables.Add(ToDataTable<SyntheticClosedStatsModel>(lstScreening));
            Session["ScreeningData"] = dsScr;
            grdClosedScreening.DataSource = lstScreening;
            grdClosedScreening.DataBind();
            var newList = lstScreening.OrderBy(x => x.Destination).ToList();
            DataSet dsByDestination = new DataSet();

        }
        private bool IsOrderExist(int orderCode)
        {
            bool isOrderExist = false;
            if (GSIAppQueryUtils.GetScreeningByOrder(orderCode, this).Count > 0)
                isOrderExist = true;
            return isOrderExist;
        }
        
        private void BuildListByDestination(DataSet dsSort, DataSet ds)
        {
            DataTable dtDestination = GSIAppQueryUtils.GetDestinationsList(this);
            //DataSet dsSort = new DataSet();
            if (ds.Tables[0] != null)
            {
                //DataSet dsByDestination = new DataSet();
                var lstBlueNileScreening = (from DataRow row in ds.Tables[0].Rows select new SyntheticBlueNileStatsModel(row)).ToList();
                foreach (var item in lstBlueNileScreening)
                {
                    item.Destination = item.Destination.Replace("\r\n", string.Empty);
                }
                var lstMiscScreening = (from DataRow row in ds.Tables[0].Rows select new SyntheticMiscStatsModel(row)).ToList();
                foreach (var item in lstMiscScreening)
                {
                    item.Destination = item.Destination.Replace("\r\n", string.Empty);
                }
                foreach (DataRow row in dtDestination.Rows)
                {
                    string destination = row["destination"].ToString().ToLower().Trim();
                    if (destination == null || destination == "")
                        continue;
                    if (destination == "blue nile" || destination == "helzerg")
                    {
                        var destItem = lstBlueNileScreening.FindAll(m => m.Destination.ToLower().Trim() == destination);
                        if (destItem.Count > 0)
                        {
                            //var destList = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
                            dsSort.Tables.Add(ToDataTable<SyntheticBlueNileStatsModel>(destItem));
                            int count = dsSort.Tables.Count-1;
                            dsSort.Tables[count].TableName = destination;
                        }
                    }
                    else
                    {
                        var destItem = lstMiscScreening.FindAll(m => m.Destination.ToLower().Trim() == destination);
                        if (destItem.Count > 0)
                        {
                            //var destList = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
                            dsSort.Tables.Add(ToDataTable<SyntheticMiscStatsModel>(destItem));
                            int count = dsSort.Tables.Count-1;
                            dsSort.Tables[count].TableName = destination;
                        }
                    }
                }
                if (dsSort.Tables.Count > 0)
                {
                    for (int i=0; i<= dsSort.Tables.Count-1; i++)
                    {
                        if (dsSort.Tables[i].TableName != "blue nile" && dsSort.Tables[i].TableName != "helzberg" && dsSort.Tables[i].TableName != "Full Report" 
                            && dsSort.Tables[i].TableName != "Surat")
                            dsSort.Tables[i].Columns.Remove("Destination");
                    }
                }
                Session["SortScreeningData"] = dsSort;
            }
        }
        private void ClearText()
        {
            txtGsiOrder.Value = string.Empty;
            txtVendorNum.Value = string.Empty;
            txtMemoNum.Value = string.Empty;
            txtTotalQTY.Value = string.Empty;
            txtQtySynth.Value = string.Empty;
            txtQtySusp.Value = string.Empty;
            txtQtyPass.Value = string.Empty;
            txtNotes.Value = string.Empty;
            btnSave.Text = "Add New";
            grdScreening.SelectedIndex = -1;
            InsertBlankRecord();
        }
        private void LoadOfficesTree()
        {
            var countries = QueryUtils.GetCountryOffices(this);
            OfficeTree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Offices" } };
            foreach (var country in countries)
            {
                if (country.Country == "USA" || country.Country == "India" || country.Country == "China")
                {
                    data.Add(new TreeViewModel { ParentId = "0", Id = country.Country, DisplayName = country.Country });
                    foreach (var office in country.Offices)
                    {
                        data.Add(new TreeViewModel { ParentId = country.Country, Id = office.OfficeId, DisplayName = office.DisplayOffice });
                    }
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            OfficeTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }

        private void ClearFilterText()
        {
            gsiOrderFilter.Value = string.Empty;
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            txtGsiOrder.Value = string.Empty;
            txtMemoNum.Value = string.Empty;
            txtTotalQTY.Value = string.Empty;
            txtQtyPass.Value = string.Empty;
            txtQtySynth.Value = string.Empty;
            txtQtySusp.Value = string.Empty;
            txtVendorNum.Value = string.Empty;
            txtNotes.Value = string.Empty;
            txtRetailer.Value = string.Empty; 
            BindGrid(0);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //dataTable.Columns.Remove("Notes");
            return dataTable;
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);
                    sheet.SetColumnWidth(i, 20 * 256);
                }
                
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }


            //-- Save File
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";
            //var sw = File.Create(dir + filename);
            //workbook.Write(sw);

            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p); //, Memory);

            Memory.Flush();
            //sw.Close();
        }

        public static void ExportSorted(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            foreach (DataTable dataTable in dataSet.Tables)
            {
                string sheetName = dataTable.TableName;
                if (sheetName.Contains(@"/"))
                    sheetName = sheetName.Replace(@"/", " ");
                var sheet = workbook.CreateSheet(sheetName);
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);
                    sheet.SetColumnWidth(i, 20 * 256);
                }

                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }


            //-- Save File
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";
            //var sw = File.Create(dir + filename);
            //workbook.Write(sw);

            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p); //, Memory);

            Memory.Flush();
            //sw.Close();
        }//ExportSorted
        private string GetCheckedOffices()
        {
            int i = 0;
            string office = null;
            foreach (TreeNode node in OfficeTree.CheckedNodes)
            {
                if (node.Depth != 2)
                    continue;//-- 0 - all, 1 - country, 2 - office
                else
                {
                    office = node.Value;
                    i++;
                }
            }
            if (i > 1)
                return "all";
            else
                return office;
        }
        public static void DownloadExcelFile(String filename, Page p)
        {
            var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            p.Response.End();
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Center;
            return style;
        }

        protected void grdScreening_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdScreening.PageIndex = e.NewPageIndex;
            if ((txtCreateDateSearch.Value.Trim() != string.Empty && txtToCreateDateSearch.Value.Trim() != string.Empty) ||
                gsiOrderFilter.Value.Trim() != string.Empty)
                BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim());
        }
        
        protected void grdScreening_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            string order = grdScreening.SelectedRow.Cells[3].Text.Trim();
            ds = GSIAppQueryUtils.GetShortStatsDetails(null, null, order, this);
            txtGsiOrder.Value = ds.Tables[0].Rows[0]["GSIOrder"].ToString();
            txtVendorNum.Value = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
            txtMemoNum.Value = ds.Tables[0].Rows[0]["Memo"].ToString();
            txtRetailer.Value = ds.Tables[0].Rows[0]["Destination"].ToString();
            txtRetailer.Value = txtRetailer.Value.ToUpper();
            txtTotalQTY.Value = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            txtQtySusp.Value = ds.Tables[0].Rows[0]["QtySusp"].ToString();
            txtQtySynth.Value = ds.Tables[0].Rows[0]["QtySynth"].ToString();
            txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
            txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString();
            btnSave.Text = "Update";
        }
        
        protected void grdScreening_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Deleting feature is temporary disabled.');", true);
        }

        protected void btnClear_ServerClick(object sender, EventArgs e)
        {
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            //ClearText();
            ClearFilterText();
        }

        protected void btnShort_ServerClick(object sender, EventArgs e)
        {
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            ClearFilterText();
        }
        protected void btnLoad_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            string order = txtGsiOrder.Value.ToString();
            ds = GSIAppQueryUtils.GetShortStatsDetails(null, null, order, this);
            if (ds.Tables[0].Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No Items for this order for screening.');", true);
                return;
            }
            txtGsiOrder.Value = ds.Tables[0].Rows[0]["GSIOrder"].ToString();
            txtVendorNum.Value = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
            txtMemoNum.Value = ds.Tables[0].Rows[0]["Memo"].ToString();
            txtRetailer.Value = ds.Tables[0].Rows[0]["Destination"].ToString();
            txtRetailer.Value = txtRetailer.Value.ToUpper();
            txtTotalQTY.Value = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
            txtQtySynth.Value = ds.Tables[0].Rows[0]["QtySynth"].ToString();
            txtQtySusp.Value = ds.Tables[0].Rows[0]["QtySusp"].ToString();
            txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            if (gsiOrderFilter.Value.Trim() == "")
            {
                BindGrid(0);
            }
            else
            {
                BindGrid(int.Parse(gsiOrderFilter.Value));
                gvTitle.InnerText = "Screening Result - For orders starts with " + gsiOrderFilter.Value;
            }

        }

        protected void btnCreateDateSearch_Click(object sender, EventArgs e)
        {
            if (ClosedOnly.Checked)
            {
                if (txtToCreateDateSearch.Value.Trim() != "" && txtCreateDateSearch.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter From Date');", true);
                    return;
                }
                grdScreening.PageIndex = 0;
                grdScreening.SelectedIndex = -1;
                string fromDate = DateTime.Today.ToString();
                BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim(), true);
                if (gsiOrderFilter.Value.Trim() == "")
                    gvTitle.InnerText = "Screening Result - For orders created between " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
                else
                    gvTitle.InnerText = "Screening Result - For order " + gsiOrderFilter.Value;
            }
            else
            {
                if (txtCreateDateSearch.Value.Trim() == "" && txtToCreateDateSearch.Value.Trim() == "" && gsiOrderFilter.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Order, From and To Date');", true);
                    return;
                }
                /*
                if (txtCreateDateSearch.Value.Trim() != "" && txtToCreateDateSearch.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter To Date');", true);
                    return;
                }
                */
                if (txtToCreateDateSearch.Value.Trim() != "" && txtCreateDateSearch.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter From Date');", true);
                    return;
                }
                grdScreening.PageIndex = 0;
                grdScreening.SelectedIndex = -1;
                BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim());
                if (gsiOrderFilter.Value.Trim() == "")
                    gvTitle.InnerText = "Screening Result - For orders created between " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
                else
                    gvTitle.InnerText = "Screening Result - For order " + gsiOrderFilter.Value;
            }
        }

        protected void btnYesterdaySearch_Click(object sender, EventArgs e)
        {
            if (ClosedOnly.Checked)
            {
                if (txtToCreateDateSearch.Value.Trim() != "" && txtCreateDateSearch.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter From Date');", true);
                    return;
                }
                grdScreening.PageIndex = 0;
                grdScreening.SelectedIndex = -1;
                
                //BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim(), true);
                if (gsiOrderFilter.Value.Trim() == "")
                    gvTitle.InnerText = "Screening Result - For orders created between " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
                else
                    gvTitle.InnerText = "Screening Result - For order " + gsiOrderFilter.Value;
            }
            else
            {
                //if (txtCreateDateSearch.Value.Trim() == "" && txtToCreateDateSearch.Value.Trim() == "" && gsiOrderFilter.Value.Trim() == "")
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Order, From and To Date');", true);
                //    return;
                //}
                /*
                if (txtCreateDateSearch.Value.Trim() != "" && txtToCreateDateSearch.Value.Trim() == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter To Date');", true);
                    return;
                }
                */
                //if (txtToCreateDateSearch.Value.Trim() != "" && txtCreateDateSearch.Value.Trim() == "")
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter From Date');", true);
                //    return;
                //}
                var fromDate = DateTime.Today.DayOfWeek.ToString();
                string dateToRun = null;
                if (fromDate == "Monday")
                {
                    var yesterday = DateTime.Now.AddDays(-3);
                    dateToRun = yesterday.ToString("MM/dd/yyyy");
                    //dateToRun = DateTime.Now.AddDays(-3).ToString("mm/dd/yyyy");
                }
                else
                    dateToRun = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                BindGrid(dateToRun, dateToRun, gsiOrderFilter.Value.Trim());
                
                grdScreening.PageIndex = 0;
                grdScreening.SelectedIndex = -1;
                //BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim());
                if (gsiOrderFilter.Value.Trim() == "")
                    gvTitle.InnerText = "Screening Result - For orders created for " + dateToRun;
                else
                    gvTitle.InnerText = "Screening Result - For order " + gsiOrderFilter.Value;
            }
        }
        protected void btnDestSearch_Click(object sender, EventArgs e)
        {
            ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            gsiOrderFilter.Value = string.Empty;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            //bool validated = GSIAppQueryUtils.ValidateRetailer(retailerSearch.Value.Trim(), this);
            //if (!validated)
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Right Retailer Code');", true);
            //    return;
            //}
            retailerSearch.Value = retailerSearch.Value.ToUpper();
            BindGrid(retailerSearch.Value.Trim());
            gvTitle.InnerText = "Screening Result - For orders having destination " + retailerSearch.Value;
        }
        
        protected void btnCLosedSearch_Click(object sender, EventArgs e)
        {
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            string fromDate = DateTime.Today.ToString();
            BindGrid(fromDate, fromDate, gsiOrderFilter.Value.Trim(), true);
            if (gsiOrderFilter.Value.Trim() == "")
                gvTitle.InnerText = "Screening Result - For orders created between " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
            else
                gvTitle.InnerText = "Screening Result - For order " + gsiOrderFilter.Value;
        }

        protected void btnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            DataSet dsScr = new DataSet();
            dsScr = (DataSet)Session["ScreeningData"];
            Export(dsScr, "SyntheticScreening_" + DateTime.Now.Month.ToString() + @"_" + DateTime.Now.Day.ToString() + @"_" + DateTime.Now.Year.ToString(), false, this);
        }

        protected void btnExportToExcelSorted_Click(object sender, ImageClickEventArgs e)
        {
            DataSet dsScr = new DataSet();
            dsScr = (DataSet)Session["SortScreeningData"];
            ExportSorted(dsScr, "SyntheticScreeningSorted_" + DateTime.Now.Month.ToString() + @"_" + DateTime.Now.Day.ToString() + @"_" + DateTime.Now.Year.ToString(), false, this);
        }

        private void InsertBlankRecord()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add("GSIItemNumber");
            dt.Columns.Add("ItemQTYFail");
            dr = dt.NewRow();
            dr["GSIItemNumber"] = string.Empty;
            dr["ItemQTYFail"] = string.Empty;
            dt.Rows.Add(dr);
            //Store the DataTable in Session
            Session["FailItems"] = dt;
        }
    }
}