using System;
using System.Collections;
using System.Data;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class SorterSeparator
	{
		public SorterSeparator()
		{
			// 
			// TODO: Add constructor logic here
			//
			al = new ArrayList();
		}

		public ArrayList al;

		public static ArrayList SortSeparate(DataSet ds)
		{
			SorterSeparator sr = new SorterSeparator();
			sr.Separate(ds);
			return sr.al;
		}

		public void Separate(DataSet ds)
		{
			if(ds.Tables.Count<1)
			{
				return;
			}

			DataSet dataset = ds.Copy();
			ArrayList tAl = new ArrayList();

			tAl.Add(dataset.Tables[0]);
			dataset.Tables.RemoveAt(0);

			for(int i = dataset.Tables.Count-1; i>-1 ;i--)
			{
				if(Utils.CompareColumnList(tAl[0] as DataTable, dataset.Tables[i]))
				{
					tAl.Insert(1,dataset.Tables[i]);
					//tAl.Add(dataset.Tables[i]);
					dataset.Tables.RemoveAt(i);
				}
			}
			al.Add(tAl);
			Separate(dataset);
		}
	}
}
