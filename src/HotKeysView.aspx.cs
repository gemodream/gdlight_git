﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Utilities;

namespace Corpt
{
    public partial class HotKeysView : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Hot Keys";
            if (!IsPostBack) SetGridData();
        }

        #region Excel Button
        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            var table = GetViewState(SessionConstants.ShortReportActive) as DataTable;
            ExcelUtils.ExportThrouPoi(table, "HotKeys", this);

        }
        #endregion

        #region DataGrid
        private void SetGridData()
        {
            DuplicateLabel.Text = HotKeyConstants.GetDuplicateKeys();
            var mDataView = new DataView(GetDataTable()) { Sort = "Access,Measure,HotKey" };

            var result = mDataView.Table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < mDataView.Count; i++)
            {
                result.Rows.Add(mDataView[i].Row.ItemArray);
            }
            result.AcceptChanges();
            SetViewState(result, SessionConstants.ShortReportActive);
            GridView.DataSource = mDataView;
            GridView.DataBind();
            ExcelButton.Visible = result.Rows.Count > 0;

        }
        private DataTable GetDataTable()
        {
            var table = new DataTable();
            table.Columns.Add("Access", typeof(string));
            table.Columns.Add("Measure", typeof(string));
            table.Columns.Add("MeasureValue", typeof(string));
            table.Columns.Add("HotKey", typeof(string));
            table.Columns.Add("MeasureId", typeof (int));
            table.Columns.Add("MeasureValueId", typeof (int));
            var allKeys = QueryUtils.GetMeasureHotKeys(this);
            foreach (var key in allKeys)
            {
                table.Rows.Add(new object[] { key.Access.AccessName, key.Measure.MeasureName, key.EnumValueTitle, key.HotKey.ToUpper(), key.Measure.MeasureId, key.EnumValueId });
            }
//            foreach (var item in HotKeyConstants.GetHotKeys("4"))
//            {
//                table.Rows.Add(new object[] { item.Access.AccessName, item.Measure.MeasureName, item.EnumValueTitle, item.HotKey.ToUpper(), item.Measure.MeasureId, item.EnumValueId });
//            }
//            foreach (var item in HotKeyConstants.GetHotKeys("5"))
//            {
//                table.Rows.Add(new object[] { item.Access.AccessName, item.Measure.MeasureName, item.EnumValueTitle, item.HotKey.ToUpper(), item.Measure.MeasureId, item.EnumValueId });
//            }
            table.AcceptChanges();
            return table;

        }

        protected void OnDataBound(object sender, EventArgs e)
        {
            if (GridView.Rows.Count > 0)
            {
                GridViewRow row = GridView.Rows[0];
                row.Cells[0].Font.Bold = true;
            }
            for (int i = GridView.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = GridView.Rows[i];
                row.Cells[0].Font.Bold = true;
                GridViewRow previousRow = GridView.Rows[i - 1];
                for (int j = 0; j < 2; j++)
                {
                    if (row.Cells[j].Text == previousRow.Cells[j].Text)
                    {
                        if (previousRow.Cells[j].RowSpan == 0)
                        {
                            if (row.Cells[j].RowSpan == 0)
                            {
                                previousRow.Cells[j].RowSpan += 2;
                            }
                            else
                            {
                                previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
                            }
                            row.Cells[j].Visible = false;
                        }
                    }
                }
            }
            
        }

        protected void OnSortingEvent(object sender, GridViewSortEventArgs e)
        {

        }
        #endregion
    }
}