﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="PrintLabels.aspx.cs" Inherits="Corpt.PrintLabels" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading"><asp:Label runat="server" ID="TitleLabel"></asp:Label></div>
        <asp:Panel runat="server" DefaultButton="cmdLoadBatch">
            <!-- 'Batch Number' textbox -->
            Batch Number: 
            <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="9" runat="server" name="txtOrderNumber"
                placeholder="Batch number" Style="font-weight: bold; width: 100px; margin-top: 10px"/>
            <!-- 'Load' button -->
            <asp:Button ID="cmdLoadBatch" class="btn btn-info" runat="server" Text="Load"
                OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
            <asp:Button runat="server" ID="DownloadButton" OnClick="OnDownloadClick" Text="Download" CssClass="btn btn-info"/>
            <br />
            <asp:Label runat="server" ID="InfoLabel" ForeColor="Red"></asp:Label>
        </asp:Panel>
        <div style="margin-top: 10px;font-size: small">
            <asp:DataGrid runat="server" ID="dgrCertLabels" AutoGenerateColumns="False" Visible="False" CellPadding="5">
                <Columns>
                    <asp:BoundColumn DataField="BarCode" HeaderText="Bar Code" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="CombinedNumber" HeaderText="Combined Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Column1" HeaderText="Column 1"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Column2" HeaderText="Column 2"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Column3" HeaderText="Column 3"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Column4" HeaderText="Column 4"></asp:BoundColumn>
                </Columns>
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
            </asp:DataGrid>
        </div>
    </div>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
        Display="None" 
        ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." 
        ValidationGroup="BatchGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
        Display="None" ValidationExpression="^\d{8}$|^\d{9}$" 
        ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>" 
        ValidationGroup="BatchGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
</asp:Content>
