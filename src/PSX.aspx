﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="PSX.aspx.cs" Inherits="Corpt.PSX" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
    <div class="demoheading">Photospex</div>
   <asp:Panel  CssClass="form-inline" runat="server" DefaultButton="LookupButton">
        <!-- Date From -->
        <label style="margin-right: 5px">
            Find scans from</label>
        <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
            AutoPostBack="True" />
        <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
            AlternateText="Click to show calendar" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
            PopupButtonID="Image1" />
        <!-- Date To -->
        <label style="margin-left: 15px; margin-right: 5px; text-align: right">
            To</label>
        <asp:TextBox runat="server" ID="calTo" Width="100px" OnTextChanged="OnChangedDateTo"
            AutoPostBack="True" />
        <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
            AlternateText="Click to show calendar" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
            PopupButtonID="Image2" />
        <label style="padding-left: 10px">where can number like </label>
        <asp:TextBox runat="server" ID="ScanNumberFld" Text="*"></asp:TextBox>
        &nbsp;and it its at least
        <asp:DropDownList ID="XFactorList" runat="server" CssClass="dropdown" Width="55px">
            <asp:ListItem Value="0" Selected="True">Any</asp:ListItem>
            <asp:ListItem Value="1">1X</asp:ListItem>
            <asp:ListItem Value="2">2X</asp:ListItem>
            <asp:ListItem Value="3">3X</asp:ListItem>
        </asp:DropDownList>
        &nbsp;x-factor.
        <asp:Button ID="LookupButton" class="btn btn-info" runat="server" Text="Lookup" OnClick="OnLookupClick"
            Style="margin-left: 15px"></asp:Button>
    </asp:Panel>
    <br/>
    <asp:Label runat="server" ID="RowsCountLabel" ForeColor="#5377A9" Font-Bold="True" Font-Size="12" Font-Names="Cambria"></asp:Label>
    <div style="font-size: small">
        <asp:DataGrid runat="server" ID="ScanGrid" AutoGenerateColumns="True"  AllowSorting="True" OnSortCommand="OnSortCommand" CellPadding="5">
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
        </asp:DataGrid>
    </div>
    </div>
</asp:Content>

