﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScreeningFrontEntries.aspx.cs" Inherits="Corpt.ScreeningFrontEntries" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--alex<title>GSI Report</title>alex -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function print() {
            var iframe = document.getElementById('textfile');
            iframe.contentWindow.print();
        }
        function print12() {
            var w = window.open("test13.pdf");
            w.print();
        }
        function print1() {
            var fileName = document.getElementById("txtMemoNum").value + ".pdf";
            var w = window.open(fileName);
            w.print();
        }
        function print3() {
            var fileName = getElementById("txtMemoNum");
            var w = window.open(fileName);
            w.print();
        }
</script>
    <style>
        .bg {
            background-image: url("Images/background_land.png");
            height: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .fullscreen {
            position: absolute;
            left: 0;
            top: 0;
            width: 99%;
            height: 99%;
        }
        .hidden { display: none; }

        /*Screen of Mobile for 480 Resolution*/
        @media only screen and (max-width:480px) {
            p {
                font-size: 11px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 5px;
                border-radius: 5px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:9px; 
                padding-top: 1px; 
                width:75px
            }

            .header {
                text-align: center; 
                font-size: 11px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 1px; 
                font-size: 14px;
            }

            .btnFont {
                font-size: 8px;
            }

            .lblStyle {
                height: 32px;
                width: 648px; 
            }
        }

        /*Screen of Mobile for 640 Resolution*/
        @media only screen and (min-width : 481px) and (max-width:640px) {
            p {
                font-size: 12px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 30px;
            }

            .text {
                -moz-border-radius: 6px;
                border-radius: 6px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:12px; 
                padding-top: 1px; 
                width:100px
            }

            .header {
                text-align: center; 
                font-size: 12px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 18px;
            }

            .btnFont {
                font-size: 11px;
            }

            .lblStyle {
                height: 13px;
                width: 258px; 
            }
        }

        @media only screen and (min-device-width: 641px) and (max-device-width: 700px) {
            p {
                font-size: 12.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 65px;
            }

            .text {
                -moz-border-radius: 6px;
                border-radius: 6px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:13px; 
                padding-top: 1px; 
                width:109px
            }

            .header {
                text-align: center; 
                font-size: 11px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 20px;
            }

            .btnFont {
                font-size: 12px;
            }

            .lblStyle {
                height: 14px;
                width: 283px; 
            }      
        }

        @media only screen and (min-width : 701px) and (max-width:800px) {
            p {
                font-size: 13.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 7px;
                border-radius: 7px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:15px; 
                padding-top: 1px; 
                width:125px
            }

            .header {
                text-align: center; 
                font-size: 16px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 23px;
            }

            .btnFont {
                font-size: 14px;
            }

            .lblStyle {
                height: 16px;
                width: 323px; 
            }
        }

        /*Screen of Laptop for 1024 Resolution*/
        @media only screen and (min-width : 801px) and (max-width:1024px) {
             p {
                font-size: 15px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 75px;
            }

            .text {
                -moz-border-radius: 10px;
                border-radius: 10px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 2px; 
                font-size:19px; 
                padding-top: 2px; 
                width:150px
            }

            .header {
                text-align: center; 
                font-size: 17px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 3px; 
                font-size: 29px;
            }

            .btnFont {
                font-size: 18px;
            }

            .lblStyle {
                height: 20px;
                width: 413px; 
            }
        }

        /*Screen of Laptop for 1366 Resolution*/
        @media only screen and (min-width: 1025px) and (max-width: 1366px) {
             p {
                font-size: 20.4px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 13px;
                border-radius: 13px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 2.5px; 
                font-size:25px; 
                padding-top: 2.5px; 
                width:213px
            }

            .header {
                text-align: center; 
                font-size: 22px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 4px; 
                font-size: 39px;
            }

            .btnFont {
                font-size: 24px;
            }

            .lblStyle {
                height: 27px;
                width: 551px; 
            }

        }

         /*Screen of Desktop for 1605 Resolution*/
        @media only screen and (min-width: 1367px) and (max-width: 1605px) {
            p {
                font-size: 23.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 150px;
            }

            .text {
                -moz-border-radius: 15px;
                border-radius: 15px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 3px; 
                font-size:30px; 
                padding-top: 3px; 
                width:250px
            }

            .header {
                text-align: center; 
                font-size: 26px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 5px; 
                font-size: 46px;
            }

            .btnFont {
                font-size: 28px;
            }

            .lblStyle {
                height: 32px;
                width: 648px; 
            }

        }

        /*Screen of 1920 Resolution*/
        @media only screen and (min-width: 1606px) {
            p {
                font-size: 29px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 175px;
            }

            .text {
                -moz-border-radius: 18px;
                border-radius: 18px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 4px; 
                font-size:36px; 
                padding-top: 4px; 
                width:300px
            }

            .header {
                text-align: center; 
                font-size: 31px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 6px; 
                font-size: 55px;
            }

            .btnFont {
                font-size: 34px;
            }

            .lblStyle {
                height: 38px;
                width: 777px; 
            }
        }


        .auto-style2 {
            left: 20px;
            top: -126px;
        }


    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container-fluid">
                <div class="auto-style2" >

                    <!--alex <div class="col-sm-8 col-md-8 col-lg-8 headTop"> -->
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <%--<p>
                            Simply key in below your Grading Report Number (It's a good idea to record this number in a place                                
                                you can easily access it in the event of report loss.)
                        </p>--%>
                        
                        <!-- alex<p style="text-align: center; padding-top:3%;"> alex -->
                        <p style="text-align: center;">
                            <span class="spanStyle" style="background-color: lightblue; color: #0a517d;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Enter Request &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span>
                        </p>
                            
                        
                        <p style="text-align: center; padding-top: 2%;">
                            <asp:Label ID="Label7" text="REQUEST ID" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="RequestIDBox" class="text" runat="server" OnClick="OnLoadClick" OnTextChanged="OnLoadClick"
                                placeholder="Enter REQUEST ID" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 2%;">
                            <td>
                                <asp:Label ID="Label1" text="MEMO" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtMemoNum" class="text" runat="server" placeholder="Enter Memo" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            </td>
                            <td>
                                <asp:Label ID="Label9" Text="GSI ORDER" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtGSIOrder" class="text" runat="server" placeholder="GSI ORDER" autocomplete="off" OnClick="OnOrderClick" OnTextChanged="txtGSIOrder_TextChanged"></asp:TextBox>
                            </td>
                            </p>
                        <p style="text-align: center; padding-top: 2%;">
                            <td>
                                <asp:Label ID="LabelCustCode" text="Cust Code" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtCustomerCode" class="text" style="width:70px;" runat="server" OnClick="OnCustCodeClick" OnTextChanged="OnCustCodeClick"
                                    placeholder="CustomerCode" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:HiddenField ID="txtCustomerID" runat="server" />
                                <asp:HiddenField ID="txpPersonID" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="Label10" text="Cust Name" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtCustomerName" class="text" runat="server" placeholder="CustomerName" autocomplete="off"></asp:TextBox>
                            </td>
                            <td>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:Label ID="Label11" text="Messenger" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:DropDownList ID="messengerListBox" runat="server" placeholder="Messenger Name" Font-Bold="true"
                                Style="padding-top: 2px; padding-bottom: 2px; width:200px;">
                            </asp:DropDownList>
                            </td>
                        </p>
                        <p style="text-align: center; padding-top: 2%;">
                            <td>
                                <asp:Label ID="Label2" text="PO NUMBER" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtPONum" class="text" runat="server" placeholder="Enter PO Number" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            </td>
                            <td>
                                <asp:Label ID="Label3" text="SKU" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtSku" class="text" runat="server" placeholder="Enter SKU" autocomplete="off"></asp:TextBox>
                            </td>
                            
                        </p>
                        
                        <p style="text-align: center; padding-top: 2%;">
                            <!--
                            <td>
                                <asp:Label ID="Label6" Text="Retailer" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtRetailer" class="text" runat="server" placeholder="Enter Retailer" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            </td>
                            -->
                            <td>
                                <asp:Label ID="Label13" Text="STYLE" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtStyle" class="text" runat="server" placeholder="Enter Style" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            </td>
                            <td>
                                <asp:Label ID="Label8" Text="Retailer" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <!--alex <asp:TextBox ID="txtSpecialInstructions" class="text" runat="server" placeholder="Enter Special Instructions" autocomplete="off"></asp:TextBox> -->
                                <asp:DropDownList ID="specialInstructionsListBox" runat="server" Font-Bold="true" AutoPostBack="true"
                                Style="padding-top: 2px; padding-bottom: 2px;" OnSelectedIndexChanged="specialInstructionsListBox_SelectedIndexChanged">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>FM</asp:ListItem>
                                <asp:ListItem>STERLING</asp:ListItem>
                                <asp:ListItem>JARED</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList> 
                            </td>
                        </p>
                        <p style="text-align: center; padding-top: 2%;">
                            <td>
                                <asp:Label ID="Label4" text="Total Quantity" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtQuantity" class="text" runat="server" placeholder="Qty" style="width: 100px;" autocomplete="off"></asp:TextBox>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>

                            </td>
                            <!--
                            <td>
                                <asp:Label ID="Label5" text="Quantity per Bag" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                                <asp:TextBox ID="txtBagQty" class="text" runat="server" placeholder="Enter Bag Quantity" autocomplete="off"></asp:TextBox>
                             </td>
                            -->
                        </p>
                        <p style="text-align: center; padding-top: 2%;">
                            
                                <asp:Label ID="Label12" Text="Batches" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                                <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <textarea id="Textarea1" runat="server" rows="3"
                                placeholder="Enter Batches" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                            
                        </p>
                        <p style="text-align: center; padding-top: 2%; ">
                            <asp:Button ID="btnSearch" runat="server" CssClass="hidden" OnClick="btnSearch1_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist';" Text="SEARCH" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSavefront_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 150px;  font-size: 28px;" Text="CREATE ORDER" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <asp:Button ID="btnPrint" runat="server"  class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 160px;  font-size: 28px;" Text="PRINT LABEL" OnClick="btnPrint_Click" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <asp:Button ID="btnPrint1" runat="server"  class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 160px;  font-size: 28px;" Text="PRINT RECEIPT" OnClick="btnPrint1_Click" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <asp:Button ID="btnClearAll" runat="server"  class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 100px;  font-size: 20px; " Text="CLEAR" OnClick="btnClear_Click" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"> </span>
                            <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 150px; font-size: 20px;" Text="Back to Main" />
                            
                        </p>

                       <%-- <p>
                            If you’re having technical difficulties registering or accessing your GSI report,<br />
                            please contact us at info@gemscience.net or call: 212-207-4140 or (toll free): 800-720-2018
                        </p>--%>
                        <p style="text-align: center;">
                            <asp:Label ID="lblError" runat="server" class="lblStyle" ForeColor="Red" Font-Bold="True" ></asp:Label>
                        </p>
                    </div>

                   <!-- alex <div class="col-sm-2 col-md-2 col-lg-2 "></div> -->

                </div>
            </div>
        </div>
    </form>
</body>
</html>
