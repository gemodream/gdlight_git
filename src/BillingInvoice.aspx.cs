﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class BillingInvoice : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Regular Billing";
        }

        #region Search by Order Code
        protected void OnSearchClick(object sender, ImageClickEventArgs e)
        {
            LoadOrderData();
        }
        private void ResetOrderData()
        {
            TreeHistory.Nodes.Clear();

            MigratedItemsGrid.DataSource = null;
            MigratedItemsGrid.DataBind();
            InfoGrid.DataSource = null;
            InfoGrid.DataBind();
        }
        private void LoadOrderData()
        {
            ResetOrderData();
            var order = OrderField.Text.Trim();
            var items = new List<CustomerHistoryModel>();
            if (!string.IsNullOrEmpty(order))
            {
                items = QueryItemiznUtils.GetOrderHistory(OrderField.Text, true, this);
            }

            if (items.Count == 0)
            {
                SetViewState(null, SessionConstants.OrderHistoryList);
                PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", order), true);

            }

            SetViewState(items, SessionConstants.OrderHistoryList);
            SetViewState(new UpdateOrderModel { OrderCode = order }, SessionConstants.UpdateOrder);

            ShowTreeHistory(items);

            //-- Migrated information
            var migratedItems = QueryUtils.GetMigratedItems(OrderField.Text.Trim(), this);
            if (migratedItems.Count > 0)
            {
                MigratedItemsGrid.DataSource = migratedItems;
                MigratedItemsGrid.DataBind();
            }

            //-- Billing Info
            var infoItems = QueryUtils.GetBillingInfo(order, this);
            if (infoItems.Count > 0)
            {
                InfoGrid.DataSource = infoItems;
                InfoGrid.DataBind();
            }
        }
        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items)
        {
            TreeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            TreeHistory.Visible = customerHistoryModels.Length > 0;
            BillingButton.Visible = TreeHistory.Visible;
            if (customerHistoryModels.Length == 0) return;
            var data = new List<TreeViewModel>();
            foreach (var item in customerHistoryModels)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        DisplayName = item.Title
                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = TreeNodeSelectAction.None };

            TreeUtils.FillNode(rootNode, root, true, TreeNodeSelectAction.None);
            rootNode.Expand();

            TreeHistory.Nodes.Add(rootNode);
        }
        #endregion

        #region Billing
        protected void OnBillingClick(object sender, EventArgs e)
        {
            var batchIds = GetCheckedBatch();
            if (batchIds.Count == 0)
            {
                PopupInfoDialog("No batches selected.", true);
                return;
            }


        }
        private List<CustomerHistoryModel> GetOrderHistoryFromView()
        {
            return GetViewState(SessionConstants.OrderHistoryList) as List<CustomerHistoryModel> ??
                   new List<CustomerHistoryModel>();
        }

        private List<string> GetCheckedBatch()
        {
            var items = GetOrderHistoryFromView();
            var batchIds = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                var item = items.Find(m => m.Id == node.Value);
                if (item == null) continue;
                if (batchIds.Contains(item.BatchId)) continue;
                batchIds.Add(item.BatchId);
            }
            return batchIds;
        }
        private IEnumerable<string> GetCheckedItems()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                checkItems.Add(node.Value.Split('_')[1]); // 1283_61887.001.01 -> 61887.001.01
            }
            return checkItems;
        }

        #endregion
        
        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    }
}