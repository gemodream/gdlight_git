﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class Tracking2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Tracking II";
            if (!IsPostBack)
            {
                PrepareForm();
                CustomerLike.Focus();
            }

        }

        #region Prepare Form
        private void PrepareForm()
        {
            LoadCustomerList();
            LoadLstAction();
            LoadLstForm();
            CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
            //CreateCurrentGrid();
        }
        private void CreateCurrentGrid()
        {
            grdBatches.Columns.Add(new BoundColumn {DataField = "CompanyName", HeaderText = "Customer Name"});
            grdBatches.Columns.Add(new BoundColumn {DataField = "GroupCode", HeaderText = "Order" });
            grdBatches.Columns.Add(new BoundColumn { DataField = "CreateDate", HeaderText = "Order Came at" });
            grdBatches.Columns.Add(new BoundColumn { DataField = "AgeInHrs", HeaderText = "Age in hours" });
            grdBatches.AllowSorting = true;
            
        }
        private void LoadCustomerList()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this, true);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
        }

        private void LoadLstAction()
        {
            lstAction.Items.Clear();
            lstAction.DataSource = QueryUtils.GetEventsList(this);
            lstAction.DataTextField = "EventName";
            lstAction.DataValueField = "EventId";
            lstAction.DataBind();
        }

        private void LoadLstForm()
        {
            lstForm.Items.Clear();
            lstForm.DataSource = QueryUtils.GetFormsList(this);
            lstForm.DataTextField = "ViewAccessName";
            lstForm.DataValueField = "ViewAccessId";
            lstForm.DataBind();

        }
        #endregion

        #region Common Filter
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();

        }
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
        }
        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;

        }
        #endregion

        #region Batches Current
        protected void OnLoadClick(object sender, EventArgs e)
        {
            var filterModel = new TrackingFilterModel
            {
                CustomerId = string.IsNullOrEmpty(lstCustomerList.SelectedValue) ? 0 : Int32.Parse(lstCustomerList.SelectedValue),
                DateFrom = CalendarExtenderFrom.SelectedDate,
                DateTo = CalendarExtenderTo.SelectedDate,
                AgeCurrent = !chkHideCurrent.Checked ? 0 :
                        (string.IsNullOrEmpty(AgeCurrent.Text) ? 0 : Int32.Parse(AgeCurrent.Text))
            };
            var batches = QueryUtils.GetAgedBatches(filterModel, this);
            SetViewState(batches, SessionConstants.BatchesCurrentList);
            ApplyCurrentSort();
            lblCurrentRows.Text = batches.Count == 0 ? "" : batches.Count + " rows";
        }
        protected void OnSortBatchesCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            ApplyCurrentSort();
        }
        void ApplyCurrentSort()
        {
            var batches = GetViewState(SessionConstants.BatchesCurrentList) as List<TrackingModel> ??
                          new List<TrackingModel>();
            var table = new DataTable("BatchesCurrent");
            table.Columns.Add(new DataColumn { ColumnName = "Customer", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Order", DataType = typeof(int) });
            table.Columns.Add(new DataColumn { ColumnName = "CreateDate", DataType = typeof(DateTime) });
            table.Columns.Add(new DataColumn { ColumnName = "Age", DataType = typeof(int) });

            foreach (var trackingModel in batches)
            {
                table.Rows.Add(new object[]
                {
                    trackingModel.CompanyName, 
                    Int32.Parse(trackingModel.GroupCode),
                    trackingModel.CreateDate, 
                    trackingModel.AgeInHrs
                });
            }
            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "CreateDate";
                SetViewState(new SortingModel { ColumnName = "CreateDate", Direction = true }, SessionConstants.CurrentSortModel);
            }

            // Rebind data 
            grdBatches.DataSource = dv;
            grdBatches.DataBind();
        }
        #endregion
       
        #region Batches History
        protected void OnSortHistoriesCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.HistorySortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.HistorySortModel);
            ApplyHistorySort();

        }
        void ApplyHistorySort()
        {
            var batches = GetViewState(SessionConstants.BatchedHistoryList) as List<TrackingHistoryModel> ??
                          new List<TrackingHistoryModel>();
            var table = new DataTable("BatchesHistory");
            table.Columns.Add(new DataColumn { ColumnName = "Customer", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Batch", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Action", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Form", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "TimeStamp", DataType = typeof(DateTime) });
            table.Columns.Add(new DataColumn { ColumnName = "Age", DataType = typeof(int) });

            foreach (var trackingModel in batches)
            {
                table.Rows.Add(new object[]
                {
                    trackingModel.CompanyName, 
                    trackingModel.BatchDisplay,
                    trackingModel.EventName,
                    trackingModel.ViewAccessName,
                    trackingModel.CreateDate, 
                    trackingModel.AgeInHrs
                });
            }
            var dv = new DataView { Table = table };
            
            var sortModel = GetViewState(SessionConstants.HistorySortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "TimeStamp";
                SetViewState(new SortingModel { ColumnName = "TimeStamp", Direction = true }, SessionConstants.HistorySortModel);
            }

            dgHistory.DataSource = dv;
            dgHistory.DataBind();
        }
        protected void OnLoadHistoryClick(object sender, EventArgs e)
        {
            HistoryErrMessage.InnerHtml = "";
            var filterModel = new TrackingFilterModel
            {
                CustomerId = string.IsNullOrEmpty(lstCustomerList.SelectedValue) ? 0 : Int32.Parse(lstCustomerList.SelectedValue),
                EventId = Int32.Parse(lstAction.SelectedValue),
                ViewAccessId = Int32.Parse(lstForm.SelectedValue),
                DateFrom = CalendarExtenderFrom.SelectedDate,
                DateTo = CalendarExtenderTo.SelectedDate,
                ShowCheckedOut = !chkHideCheckedOut.Checked
            };
            try
            {
                var histories = QueryUtils.GetAgesHistoryBatches(filterModel, this);
                SetViewState(histories, SessionConstants.BatchedHistoryList);
                lblHistoryRows.Text = histories.Count == 0 ? "" : histories.Count + " rows";
                ApplyHistorySort();
            } catch (Exception x)
            {
                HistoryErrMessage.InnerHtml = x.Message;
            }
        }
        #endregion

        protected void OnActiveTabChanged(object sender, EventArgs e)
        {
            if (TabContainer.ActiveTabIndex == 0)
            {
                dgHistory.Visible = false;
                ApplyCurrentSort();
                grdBatches.Visible = true;
            }
            if (TabContainer.ActiveTabIndex == 1)
            {
                grdBatches.Visible = false;
                ApplyHistorySort();
                dgHistory.Visible = true;
            }
        }
    }
}