﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="FindItemByMeasureValue.aspx.cs" Inherits="Corpt.FindItemByMeasureValue" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" />
    <div class="demoarea">
        <div class="demoheading">Find by Measure Value</div>
        <table>
            <tr>
                <td><b>Measure</b></td>
                <td style="padding-left: 20px"><b>Value</b></td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <asp:ListBox runat="server" ID="lstMeasures" Rows="10" DataValueField="MeasureId" OnSelectedIndexChanged="OnMeasureChanged" AutoPostBack="True"
                        DataTextField="MeasureName" />
                </td>
                <td style="vertical-align: top;padding-left: 20px;width: 100%">
                    <asp:Panel runat="server" CssClass="form-horizontal" DefaultButton="FindButton">
                        <asp:TextBox runat="server" ID="ValueField" Width="200px"></asp:TextBox>
                        <asp:Button runat="server" Text="Find" CssClass="btn btn-info" ID="FindButton" OnClick="OnFindClick"/>
                    </asp:Panel><br/>
                    <div style="font-size: small">
                        <asp:Label runat="server" ID="RowsLabel" ForeColor="Black" Font-Bold="True"></asp:Label>
                        <asp:DataGrid runat="server" AutoGenerateColumns="False" ID="ItemGrid" CellPadding="5">
                            <Columns>
                                <asp:BoundColumn DataField="MeasureValue" HeaderText="Measure Value"></asp:BoundColumn>
                                <asp:BoundColumn DataField="FullItemNumber" HeaderText="Item Number" />
                                <asp:BoundColumn DataField="JumpFullOrder" HeaderText="Full Order" />
                                <asp:BoundColumn DataField="JumpColor" HeaderText="Color" />
                                <asp:BoundColumn DataField="JumpClarity" HeaderText="Clarity" />
                                <asp:BoundColumn DataField="JumpMeasure" HeaderText="Measure" />
                                <asp:BoundColumn DataField="JumpBulkUpdate" HeaderText="Bulk Update" />
                                <asp:BoundColumn DataField="JumpGradeII" HeaderText="Grade II" />
                            </Columns>
                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
