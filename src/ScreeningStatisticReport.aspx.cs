﻿using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Globalization;

namespace Corpt
{
    public partial class ScreeningStatisticReport : System.Web.UI.Page
	{
		#region PageLoad
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");

			this.Form.DefaultFocus = txtSearch.ClientID;
			this.Form.DefaultButton = btnSubmit.UniqueID;

		}
		#endregion

		#region Button Event
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
            DataSet ds = new DataSet();

            Clear();

            if (rbRequestNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(txtSearch.Text, "", "", this);
            }
            else if (rbMemoNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", txtSearch.Text, "", this);
            }
            else if (rbOrderNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", "", txtSearch.Text, this);
            }

            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                lblMsg.Text = "Records not found.";
                tblDetails.Visible = false;
                return;
            }
            
			lblMsg.Text = "";
			tblDetails.Visible = true;
            tblBatchInfo.Visible = true;
            lblRequest.InnerText = ds.Tables[0].Rows[0]["RequestID"].ToString();
            lblMemo.InnerText = ds.Tables[0].Rows[0]["MemoNum"].ToString();
            lblPO.InnerText = ds.Tables[0].Rows[0]["PONum"].ToString();
            lblSKU.InnerText = ds.Tables[0].Rows[0]["SKUName"].ToString();
            lblStyle.InnerText = ds.Tables[0].Rows[0]["StyleName"].ToString();
            lblServiceType.InnerText = ds.Tables[0].Rows[0]["ServiceTypeName"].ToString();
            lblRetailer.InnerText = ds.Tables[0].Rows[0]["RetailerName"].ToString();
            lblCategory.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString();
            lblServiceTime.InnerText = ds.Tables[0].Rows[0]["ServiceTimeName"].ToString();
            lblDeliveryMethod.InnerText = ds.Tables[0].Rows[0]["DeliveryMethodName"].ToString();
            lblCarrier.InnerText = ds.Tables[0].Rows[0]["CarrierName"].ToString();
            lblOrder.InnerText = ds.Tables[0].Rows[0]["GSIOrder"].ToString();
			lblJewelryType.InnerText = ds.Tables[0].Rows[0]["JewelryTypeName"].ToString();

			lblQty.InnerText =  ds.Tables[0].Rows[0]["TotalQty"].ToString();
            lblComments.InnerText = ds.Tables[0].Rows[0]["Comment"].ToString();
            hdnStatus.Value = GetOrderStatus( Convert.ToInt32(lblRequest.InnerText), lblMemo.InnerText).ToString();

            if (ds.Tables[1].Rows.Count != 0)
            {
                divResult.Visible = true;
                if (Convert.ToInt16(ds.Tables[1].Rows[0]["StatusID"].ToString()) >= 14)
                { 
					lblScreeningPass.InnerText = ds.Tables[1].Rows[0]["TotalPassScreening"].ToString();
					lblScreeningFail.InnerText = ds.Tables[1].Rows[0]["TotalFailScreening"].ToString();
					lblTestPass.InnerText = ds.Tables[1].Rows[0]["TotalPass"].ToString();
					lblTestSysthetic.InnerText = ds.Tables[1].Rows[0]["TotalSynthetic"].ToString();
					lblTestSuspect.InnerText = ds.Tables[1].Rows[0]["TotalSuspect"].ToString();
				}
            }
            else
            {
                divResult.Visible = false;
            }

            if(ds.Tables[2].Rows.Count!=0)
            {
                tblBatchInfo.Visible = true;
                gvBatch.DataSource = ds.Tables[2];
                gvBatch.DataBind();
            }
            else
            {
                tblBatchInfo.Visible = false;
                gvBatch.DataSource = null;
                gvBatch.DataBind();
            }
            BindDataList();
        }

        protected void btnClear_Click(object sender, EventArgs e)
		{
            Clear();
        }

        public void Clear()
        {
            //txtSearch.Text = "";
            lblRequest.InnerText = "";
            lblMemo.InnerText = "";
            lblPO.InnerText = "";
            lblSKU.InnerText = "";
            lblStyle.InnerText = "";
            lblServiceType.InnerText = "";
            lblRetailer.InnerText = "";
            lblCategory.InnerText = "";
            lblServiceTime.InnerText = "";
            lblDeliveryMethod.InnerText = "";
            lblCarrier.InnerText = "";
            lblOrder.InnerText = "";
            lblQty.InnerText = "Total Quantity:";
            lblComments.InnerText = "";
            lblPass.InnerText = "";
            lblFail.InnerText = "";
            hdnStatus.Value = "";
            lblScreeningPass.InnerText = "";
            lblScreeningFail.InnerText = "";
            lblTestPass.InnerText = "";
            lblTestSysthetic.InnerText = "";
            lblTestSuspect.InnerText = "";
            dtlProgress.DataSource = null;
            dtlProgress.DataBind();
            tblDetails.Visible = false;
            tblBatchInfo.Visible = false;
            gvBatch.DataSource = null;
            gvBatch.DataBind();
        }
		#endregion

		#region BindData
		protected void dtlProgress_ItemDataBound(object sender, DataListItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{

				int Status = hdnStatus.Value != "" ? Convert.ToInt32(hdnStatus.Value) : 0;
				Label lblValue = (Label)e.Item.FindControl("lblValue");
				Label lblKey = (Label)e.Item.FindControl("lblKey");
				HtmlGenericControl tdStatus = (HtmlGenericControl)e.Item.FindControl("tdStatus");
				HtmlGenericControl firstline = (HtmlGenericControl)e.Item.FindControl("firstline");
				HtmlGenericControl lastline = (HtmlGenericControl)e.Item.FindControl("lastline");

				lblValue.Text = Regex.Replace(lblValue.Text, "([A-Z])", " $1").Trim();

				if (lblKey.Text == "1")
				{
					firstline.Style.Add("border-color", "white");
				}

				if (lblKey.Text == Status.ToString())
				{
					lastline.Style.Add("border-color", "#C0C0C0");
				}

				if (Convert.ToInt16(lblKey.Text) > Status)
				{
					tdStatus.Style.Add("background-color", "#C0C0C0");
					firstline.Style.Add("border-color", "#C0C0C0");
					lastline.Style.Add("border-color", "#C0C0C0");
					lblValue.CssClass = "btn btn-default";
				}
				else
				{
					lblValue.CssClass = "btn btn-success";
				}
				if (lblKey.Text == "16")
				{
					lastline.Style.Add("border-color", "white");
				}

			}
		}
		public void BindDataList()
        {
            DataSet SyntheticOrderStatus = SyntheticScreeningUtils.GetSyntheticOrderStatus(1, this);
            dtlProgress.DataSource = SyntheticOrderStatus.Tables[0];
            dtlProgress.DataBind();
        }

        public int GetOrderStatus(int RequestID, string Memo)
        {
            int Status = 0;
            DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderStatusByOrderCode(RequestID, Memo, this);
            if (ds.Tables[0].Rows.Count != 0)
            { 
                Status = Convert.ToInt16(ds.Tables[0].Rows[0]["Status"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["Status"].ToString());
            }
            return Status;
        }

        protected void CheckedChanged(object sender, EventArgs e)
        {
            txtSearch.Text = "";
        }
		#endregion

		#region Print PDF

		protected void btnPrint_Click(object sender, EventArgs e)
        {
            PrintPDFReceipt();
        }

        public void PrintPDFReceipt()
        {
            DataSet ds = new DataSet();


            if (rbRequestNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(txtSearch.Text, "", "", this);
            }
            else if (rbMemoNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", txtSearch.Text, "", this);
            }
            else if (rbOrderNo.Checked == true)
            {
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", "", txtSearch.Text, this);
            }

            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                lblMsg.Text = "Records not found.";
                tblDetails.Visible = false;
                return;
            }

            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();

            dt = ds.Tables[0];

            string requestID = dt.Rows[0]["RequestID"].ToString();

            // Create a Document object
            var document = new Document(PageSize.A4, 25, 25, 25, 25);

            // Create a new PdfWrite object, writing the output to a MemoryStream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            // Open the Document for writing
            document.Open();

            // First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
            var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
            var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
            var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
            var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
            var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
            var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);

            // Finally, add an image in the upper right corner
            var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/logo.gif"));
            logo.SetAbsolutePosition(50, 760);
            logo.ScalePercent(60);
            document.Add(logo);

            // Add the Receipt title
            var rptHeader = new Paragraph("Gemological Science International", headerFont);

            rptHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(rptHeader);

            var repSubHeader = new Paragraph("www.gemscience.net", bodyFont);
            repSubHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(repSubHeader);
            document.Add(Chunk.NEWLINE);

            // Add the Receipt title
            var rptTitle = new Paragraph("Batch Receipt", titleFont);
            rptTitle.SpacingBefore = 10;
            document.Add(rptTitle);

            // Now add the "Your order details are below." message
            document.Add(new Paragraph("Your order details are below.", bodyFont));
            document.Add(Chunk.NEWLINE);
            document.Add(Chunk.NEWLINE);

            Barcode128 bc = new Barcode128();
            bc.TextAlignment = Element.ALIGN_CENTER;
            bc.Code = requestID;
            bc.StartStopText = false;
            bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
            bc.Extended = true;

            PdfContentByte cb = writer.DirectContent;
            iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
            patImage.ScaleToFit(250, 50);
            patImage.SetAbsolutePosition(450, 700);
            document.Add(patImage);

            // Add the "Order Information" subtitle
            document.Add(new Paragraph("Order Information", subTitleFont));

            PdfPTable mtable = new PdfPTable(2);
            mtable.WidthPercentage = 100;
            mtable.SetWidths(new int[] { 300, 300 });

            mtable.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            // Create the Order Information table - see http://www.mikesdotnetting.com/Article/86/iTextSharp-Introducing-Tables for more info
            var orderInfoTable = new PdfPTable(2);
            orderInfoTable.HorizontalAlignment = 0;
            orderInfoTable.SpacingBefore = 10;
            orderInfoTable.SpacingAfter = 7;
            orderInfoTable.DefaultCell.Border = 0;
            orderInfoTable.TotalWidth = 300;
            orderInfoTable.LockedWidth = true;
            orderInfoTable.SetWidths(new int[] { 100, 200 });

            orderInfoTable.AddCell(new Phrase("RequestID:", boldTableFont));
            orderInfoTable.AddCell(requestID);

            orderInfoTable.AddCell(new Phrase("Memo:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["MemoNum"].ToString());

            orderInfoTable.AddCell(new Phrase("PO Number:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["PONum"].ToString());

            orderInfoTable.AddCell(new Phrase("Style:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["StyleName"].ToString());

            orderInfoTable.AddCell(new Phrase("SKU:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["SKUName"].ToString());

            orderInfoTable.AddCell(new Phrase("Total Quantity:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["TotalQty"].ToString());

            orderInfoTable.AddCell(new Phrase("Retailer:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["RetailerName"].ToString());

            orderInfoTable.AddCell(new Phrase("Category:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["CategoryName"].ToString());

            orderInfoTable.AddCell(new Phrase("Service Type:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["ServiceTypeName"].ToString());

            orderInfoTable.AddCell(new Phrase("Service Time:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["ServiceTimeName"].ToString());

			orderInfoTable.AddCell(new Phrase("Jewelry Type:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["JewelryTypeName"].ToString());

			orderInfoTable.AddCell(new Phrase("Order Number:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["GSIOrder"].ToString());

            orderInfoTable.AddCell(new Phrase("Order Created On:", boldTableFont));

            string OrderCreateDate = dt.Rows[0]["CreateDate"].ToString() == "01/01/1900 12:00:00 AM" ? "" : Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
            orderInfoTable.AddCell(OrderCreateDate);

            string batchTotal = "";
            if (ds.Tables.Count == 3)
            {
                batchTotal = ds.Tables[2].Rows.Count == 0 ? "" : ds.Tables[2].Rows.Count.ToString();
            }

            orderInfoTable.AddCell(new Phrase("Total Batches:", boldTableFont));
            orderInfoTable.AddCell(batchTotal);

            mtable.AddCell(orderInfoTable);

            // Create the Order Details table
            orderInfoTable = new PdfPTable(4);
            orderInfoTable.TotalWidth = 250;
            orderInfoTable.LockedWidth = true;
            //relative col widths in proportions - 1/3 and 2/3
            float[] widths = new float[] { 45, 45, 80, 80 };
            orderInfoTable.SetWidths(widths);

            orderInfoTable.HorizontalAlignment = 1;
            orderInfoTable.SpacingBefore = 10;
            orderInfoTable.SpacingAfter = 10;

            orderInfoTable.AddCell(new Phrase("Batch #", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Quantity", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Screener", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Tester", boldTableFont));

            if (ds.Tables.Count == 3)
            {
                if (ds.Tables[2].Rows.Count != 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        orderInfoTable.AddCell(row["BatchCode"].ToString());
                        orderInfoTable.AddCell(row["Quantity"].ToString());
                        orderInfoTable.AddCell(row["Screener"].ToString());
                        orderInfoTable.AddCell(row["Tester"].ToString());
                    }
                }
            }

            //orderInfoTable.AddCell("1");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("2");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("3");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("4");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("5");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("6");
            //orderInfoTable.AddCell("50");

            mtable.AddCell(orderInfoTable);

            document.Add(mtable);

            var screenerInfoTable = new PdfPTable(4);
            screenerInfoTable.HorizontalAlignment = 0;
            screenerInfoTable.SpacingBefore = 10;
            screenerInfoTable.SpacingAfter = 10;
            screenerInfoTable.TotalWidth = 460f;
            screenerInfoTable.LockedWidth = true;
            float[] tableWidth = new float[] { 175f, 55f, 175f, 55f };

            screenerInfoTable.SetWidths(tableWidth);

            PdfPCell cellScreener = new PdfPCell(new Phrase("Screener", subTitleFont));
            cellScreener.Colspan = 2;
            screenerInfoTable.AddCell(cellScreener);
            PdfPCell cellTester = new PdfPCell(new Phrase("Tester", subTitleFont));
            cellTester.Colspan = 2;
            screenerInfoTable.AddCell(cellTester);
            
            string TotalPassScreening = "";
            string TotalSynthetic = "";
            string TotalFailScreening = "";
            string TotalSuspect = "";
            string TotalPass = "";

            if (ds.Tables.Count >= 2)
            {
                dtResult = ds.Tables[1];
                if (dtResult.Rows.Count != 0)
                {
                    //Screener = dtResult.Rows[0]["Screener"].ToString();
                    //Tester = dtResult.Rows[0]["Tester"].ToString();
                    TotalPassScreening = dtResult.Rows[0]["TotalPassScreening"].ToString();
                    TotalSynthetic = dtResult.Rows[0]["TotalSynthetic"].ToString();
                    TotalFailScreening = dtResult.Rows[0]["TotalFailScreening"].ToString();
                    TotalSuspect = dtResult.Rows[0]["TotalSuspect"].ToString();
                    TotalPass = dtResult.Rows[0]["TotalPass"].ToString();
                }
            }

            screenerInfoTable.AddCell(new Phrase("Total Pass Screening Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalPassScreening, bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Synthetic Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalSynthetic, bodyFont));

            screenerInfoTable.AddCell(new Phrase("Total Fail Screening Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalFailScreening, bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Suspect Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalSuspect, bodyFont));

            screenerInfoTable.AddCell(new Phrase("", bodyFont));
            screenerInfoTable.AddCell(new Phrase("", bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Pass Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalPass, bodyFont));

            document.Add(screenerInfoTable);

            // Add ending message
            var endingMessage = new Paragraph("For assistance please email to support@gemscience.net or call GSI office in your country.\nYou can find the phone information by following the link www.gemscience.net/contact/.", endingMessageFont);
            //endingMessage.
            document.Add(endingMessage);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", requestID));
            Response.BinaryWrite(output.ToArray());
        }
		#endregion
	}

}