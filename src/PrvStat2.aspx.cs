﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class PrvStat2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            BuildCpTree();
            CpNameLike.Focus();
        }


        private IEnumerable<ColorClarityStatModel> GetData(string cp)
        {
            var items = QueryUtils.GetLeslieList(cp, this);
            /*
            var vendorYearMonths = from item in items
                                  orderby item.Vendor , item.Year , item.Month
                                  group item by new {item.Vendor, item.Year, item.Month};
            var totals = new List<ColorClarityStatModel>();
            //-- Calc quentity Vendor + Year + Month
            foreach(var total in vendorYearMonths)
            {
                totals.Add(new ColorClarityStatModel
                {
                    Level = 1, 
                    MeasureName = "Total per month", 
                    Vendor = total.Key.Vendor, 
                    Year = total.Key.Year, 
                    Month = total.Key.Month, 
                    Qnty = total.Sum(m=>m.Qnty)
                });
            }

            //-- Calc Vendor + Year
            var vendorYears = from item in items
                orderby item.Vendor , item.Year
                group item by new {item.Vendor, item.Year};
            foreach (var total in vendorYears)
            {
                totals.Add(new ColorClarityStatModel
                {
                    Level = 2,
                    MeasureName = "Total per year",
                    Vendor = total.Key.Vendor,
                    Year = total.Key.Year,
                    Month = total.Max(m => m.Month),
                    Qnty = total.Sum(m => m.Qnty)
                });
            }

            //-- Calc Vendor + Year
            var vendors = from item in items
                              orderby item.Vendor
                              group item by new { item.Vendor };
            foreach (var total in vendors)
            {
                totals.Add(new ColorClarityStatModel
                {
                    Level = 3,
                    MeasureName = "Total vendor",
                    Vendor = total.Key.Vendor,
                    Year = total.Max(m => m.Year),
                    Month = total.Last().Month,
                    Qnty = total.Sum(m => m.Qnty)
                });
            }

            items.AddRange(totals); */
            return items;
        }

        private DataTable GetDataTable(string cp)
        {
            var table = new DataTable();
            table.Columns.Add("Vendor", typeof(string));
            table.Columns.Add("Year", typeof(Int32));
            table.Columns.Add("Month", typeof(Int32));
            table.Columns.Add("MeasureName", typeof(string));
            table.Columns.Add("MeasureValue", typeof(string));
            table.Columns.Add("Qnty", typeof(Int32));
            table.Columns.Add("Level", typeof(Int32));
            foreach(var item in GetData(cp))
            {
                table.Rows.Add(new object[]
                    {item.Vendor, item.Year, item.Month, item.MeasureName, item.MeasureValue, item.Qnty, item.Level});
            }
            table.AcceptChanges();
            return table;
        }
        protected void OnSortingEvent(object sender, GridViewSortEventArgs e)
        {
        }


        protected void OnDataBound(object sender, EventArgs e)
        {
            if (GridView.Rows.Count > 0)
            {
                GridViewRow row = GridView.Rows[0];
                row.Cells[0].Font.Bold = true;
            }
            for (int i = GridView.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = GridView.Rows[i];
                row.Cells[0].Font.Bold = true;
                if (row.Cells[3].Text.IndexOf("Total", StringComparison.Ordinal) != -1)
                {
                    row.Cells[3].Font.Bold = true;
                    row.Cells[5].Font.Bold = true;
                }
                GridViewRow previousRow = GridView.Rows[i - 1];
                for (int j = 0; j < row.Cells.Count-2; j++)
                {
                    if (row.Cells[j].Text == previousRow.Cells[j].Text)
                    {
                        if (previousRow.Cells[j].RowSpan == 0)
                        {
                            if (row.Cells[j].RowSpan == 0)
                            {
                                previousRow.Cells[j].RowSpan += 2;
                            }
                            else
                            {
                                previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
                            }
                            row.Cells[j].Visible = false;
                        }
                    }
                }
            }
        }
  
        #region Customer Programs View
        protected void OnCpSelected(object sender, EventArgs e)
        {
            if (ProgramsView.SelectedNode == null || ProgramsView.SelectedNode.Parent == null)
            {
                ResetDataGrid();
                return;
            }
            var mDataView = new DataView(GetDataTable(ProgramsView.SelectedNode.Value)) { Sort = "Vendor,Year,Month,Level" };
            
            var result = mDataView.Table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < mDataView.Count; i++)
            {
                result.Rows.Add(mDataView[i].Row.ItemArray);
            }
            result.Columns.Remove("Level");
            result.AcceptChanges();
            SetViewState(result, SessionConstants.ShortReportActive);
            GridView.DataSource = mDataView;
            GridView.DataBind();
            cmdExcel.Visible = result.Rows.Count > 0;

        }
        private void ResetDataGrid()
        {
            GridView.DataSource = null;
            GridView.DataBind();
            cmdExcel.Visible = false;
        }
        private void BuildCpTree()
        {
            ProgramsView.Nodes.Clear();
            ResetDataGrid();
            var cpName = CpNameLike.Text.Trim();
            if (string.IsNullOrEmpty(cpName))
            {
                return;
            }
            var cps = QueryUtils.GetLesliePrograms(cpName, this);
            var data = new List<TreeViewModel>();
            data.Add(new TreeViewModel { ParentId = "", Id = "root", DisplayName = "Select Program:" });
            data.AddRange(cps.Select(cp => new TreeViewModel {ParentId = "root", Id = cp, DisplayName = cp}));
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            ProgramsView.Nodes.Add(rootNode);

        }
        #endregion

        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            var table = GetViewState(SessionConstants.ShortReportActive) as DataTable;
            ExcelUtils.ExportThrouPoi(table, /*ProgramsView.SelectedNode.Value +*/ "Report", this);

        }

        protected void OnCpSearchClick(object sender, ImageClickEventArgs e)
        {
            BuildCpTree();
        }
    }
}