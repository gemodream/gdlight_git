﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ShortReportExt.aspx.cs" Inherits="Corpt.ShortReportExt" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%-- IvanB 15/03 start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<%-- IvanB 15/03 end --%>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">
		//$.ajaxSetup({ cache: false });

        $(window).resize(function () {
            showHeight();
           // showWeight();
        });
        $(document).ready(function () {
            showHeight();
          //  showWeight();
        });
        /*IvanB 15/03 start*/
        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {
            $(".filtered-select").select2({
                scrollAfterSelect: true,
                dropdownParent: $('#<%=EditItemPanel.ClientID%>')
            });
        }
        /*IvanB 15/03 end*/
        function gridviewScrollPercentFull() {
            var widthGrid = $('#gridContainer').width();
            var heightGrid = $('#gridContainer').height();
            $('#<%=grdShortReport.ClientID%>').gridviewScroll({
                width: widthGrid,
                height: heightGrid,
                freezesize: 1
            });
        }
        

        function showHeight() {
            
            var h = parseInt(window.innerHeight) - 230;
            var div1 = document.getElementById('<%= EditItemDiv.ClientID %>');
            div1.style.height = h + 'px';

            var div2 = document.getElementById('<%= ShowHideDiv.ClientID %>');
            div2.style.height = h + 'px';

            var div3 = document.getElementById('<%= MovedItemsDiv.ClientID %>');
            div3.style.height = h + 'px';

            var fld = document.getElementById('<%= ForHeightFld.ClientID %>');
            fld.value = h;

        };
        function LabelOldPrint() {
            var prtContent = document.getElementById('<%= LabelOldGrid.ClientID %>');
            prtContent.border = 0; //set no border here
            var Print = window.open('', '', 'left=10,top=10,width=1000,height=1000,toolbar=1,scrollbars=0,status=0,resizable=1');
            Print.document.write(prtContent.outerHTML);
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
        function LabelOld2Print() {
            var prtContent = document.getElementById('<%= LabelOld2Grid.ClientID %>');
            prtContent.border = 0; //set no border here
            var Print = window.open('', '', 'left=10,top=10,width=1000,height=1000,toolbar=1,scrollbars=0,status=0,resizable=1');
            Print.document.write(prtContent.outerHTML);
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
        function LabelTlkwPrint() {
            var prtContent = document.getElementById('<%= LabelTlkwGrid.ClientID %>');
            prtContent.border = 0; //set no border here
            var Print = window.open('', '', 'left=10,top=10,width=1000,height=1000,toolbar=0,scrollbars=0,status=0,resizable=1');
            Print.document.write(prtContent.outerHTML);
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
        function LabelTlkw2Print() {
            var prtContent = document.getElementById('<%= LabelTlkw2Grid.ClientID %>');
            prtContent.border = 0; //set no border here
            var Print = window.open('', '', 'left=10,top=10,width=1000,height=1000,toolbar=0,scrollbars=0,status=0,resizable=1');
            Print.document.write(prtContent.outerHTML);
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }             
        
    </script>
    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
         /*IvanB 15/03 start*/
        .select2-results ul
        {
	        margin: 0 2px 0 2px !important;
        }
        /*IvanB 15/03 end*/
    </style>
    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
            <ContentTemplate>
                <div class="demoheading">
                    <asp:Panel ID="Panel3" runat="server" CssClass="form-inline" >
                        Short Report
                        <asp:ImageButton ID="CollapseBtn" runat="server" ToolTip="Collapse top panel" ImageUrl="~/Images/ajaxImages/collapse24.png"
                            OnClick="OnCollapseBtnClick" Visible="True" Style="margin-left: 30px" />
                        <asp:ImageButton ID="ExpandBtn" runat="server" ToolTip="Expand top panel" ImageUrl="~/Images/ajaxImages/expand32.png"
                            OnClick="OnExpandBtnClick" Visible="False" Style="margin-left: 10px" />
                        <asp:Button runat="server" ID="ClearAllBtn" OnClick="OnClearAllClick" Text="Clear All" CssClass="btn btn-small btn-info"/>
                        <asp:Button runat="server" ID="RefreshBtn" OnClick="OnRefreshClick" Text="Refresh" CssClass="btn btn-small btn-info"/>
                        <asp:Label runat="server" ID="LastActionLbl" CssClass="lbl" Text="" Style="padding-left: 20px"></asp:Label>
                        <asp:Label ID="ErrorLabel" ForeColor="Red" runat="server" Font-Size="12px"></asp:Label>

                        <asp:HiddenField runat="server" ID="ForHeightFld" ></asp:HiddenField>
                    </asp:Panel>
                </div>
                <asp:Panel runat="server" ID="TopPanel" DefaultButton="HiddenFilterBtn" Style="border: silver solid 1px;padding: 5px;margin: 5px;">
                    <asp:Button runat="server" ID="HiddenFilterBtn" Style="display: none" />
                    <table>
                        <tr style="color: black;vertical-align: top">
                            <td>
                                <asp:Label ID="Label1" Text="Orders" runat="server" CssClass="label" />
                                <asp:Button ID="MovedItemsBtn" runat="server" Text="Moved Items" CssClass="btn btn-info btn-small"
                                    OnClick="OnMovedItemsClick" Style="margin-left: 5px; margin-right: 0px" Enabled="false"/>
                            </td>
                            <td style="padding-left: 10px">
                            </td>
                            <td>
                                <asp:Label ID="Label2" Text="Documents" runat="server" CssClass="label" />
                            </td>
                            
                            <td>
                                <asp:Label ID="Label3" Text="Item Numbers" runat="server" CssClass="label" />
                                <asp:Button ID="BuildBtn" runat="server" CssClass="btn btn-info btn-small" OnClick="OnBuildBtnClick" Style="padding-left: 10px"
                                    Text="Build" />
                                <asp:Button ID="BulkUpdateBtn" runat="server" CssClass="btn btn-info btn-small" OnClick="OnBulkUpdateBtnClick" Style="padding-left: 10px"
                                    Text="Bulk Update" />
                            </td>
                            <td style="width: 40px"></td>
                            <td style="width: 20px; padding-left: 10px; padding-right: 10px; text-align: left;font-size: 12px">
                                <asp:Label runat="server" ID="CpPathToPicture"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="FindOrderBtn" Width="140px" CssClass="form-inline">
                                    <asp:TextBox runat="server" ID="OrderFld" MaxLength="7" Width="80px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="OrderExtender" runat="server" ValidChars="0123456789"
                                        TargetControlID="OrderFld" Enabled="True" />
                                    <asp:ImageButton ID="FindOrderBtn" runat="server" ToolTip="Find and add order" ImageUrl="~/Images/ajaxImages/search.png"
                                        OnClick="OnFindOrderClick" /><br />
                                    <asp:ListBox runat="server" ID="OrderList" Rows="5" Width="100px" />
                                </asp:Panel>
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td style="vertical-align: top">
                                <asp:CheckBoxList runat="server" ID="DocumentList" OnSelectedIndexChanged="OnDocumentSelected"
                                    AutoPostBack="True" Width="200px"/>
                            </td>
                            <td style="vertical-align: top; font-family: sans-serif; ">
                                <asp:TreeView ID="ItemNumberTree1" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                    onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                    Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                    ShowLines="False" ShowExpandCollapse="true" OnSelectedNodeChanged="OnTreeChanged">
                                    <SelectedNodeStyle BackColor="#D7FFFF" />
                                </asp:TreeView>
                            </td>
                            <td></td>
                            <td style="vertical-align: top; padding-left: 10px; padding-right: 10px">
                                <asp:Panel runat="server" ID="CpPanel">
                                    <asp:Image ID="itemPicture" runat="server" Width="90px" Visible="False"></asp:Image><br />
                                    <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Font-Size="12px"></asp:Label>
                                    <div runat="server" id="CpDescDiv" style="font-size: small;width: 300px">
                                    </div>
                                    <div runat="server" id="CpCommDiv" style="font-size: small;width: 300px">
                                    </div>
                                    <div>
                                        <a href="CustomerProgramNew.aspx" target="_blank" id="CpRef" runat="server"></a>
                                    </div>
                                    <div>
                                        <a href="CustomerProgramNew.aspx" target="_blank" id="CpRules" runat="server"></a>
                                    </div>
                                    <asp:HiddenField runat="server" ID="DetailsBatchNumberFld"/>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                
                <asp:Panel ID="ButtonsPanel" runat="server" CssClass="form-inline" Style="margin: 5px;padding: 5px" Visible="False">
                    <asp:CheckBox CssClass="" runat="server" Text="Check All" ID="CheckAllCheckBox" AutoPostBack="True"
                        OnCheckedChanged="OnCheckAllClick" ></asp:CheckBox>
                    <asp:CheckBox CssClass="" runat="server" Text="Highlight OFF" ID="HighlightOffCheckBox" AutoPostBack="True"
                        OnCheckedChanged="OnHighlightOffClick" ></asp:CheckBox>
                    <asp:Button ID="HideColumnsBtn" runat="server" Text="Show/Hide colums" CssClass="btn btn-info btn-small"
                        OnClick="OnHideColumnsClick" Style="margin-left: 5px;margin-right: 15px"/>
                    
                    <asp:DropDownList runat="server" ID="AttLabelsList"  Width="100px" ToolTip="Attached LBL documents"/>
                    <asp:ImageButton ID="LookLblDocBtn" runat="server" ToolTip="Look selected Label Document" ImageUrl="~/Images/ajaxImages/barcode24.png"
                        OnClick="OnLookLblDocClick" />
                    <asp:Button ID="PrintLabelsBtn" runat="server" Text="Preview Labels" CssClass="btn btn-info btn-small" 
                        OnClick="OnPreviewLabelsClick" />
                    <asp:CheckBox CssClass="" runat="server" Text="Letter" ID="LetterBox" AutoPostBack="True"></asp:CheckBox>
                    <asp:Button ID="DownloadLabelBtn" runat="server" Text="Label to Excel " CssClass="btn btn-info btn-small" Style="margin-right: 15px"
                        OnClick="OnDownLoadLabelClick" ToolTip="For first checked Item Number"/>
                    <asp:ImageButton ID="ExcelButton" ToolTip="Short Report Export to Excel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                        OnClick="OnExcelClick" Enabled="true" />
                    <asp:Button ID="OrderButton" runat="server" Text="Order Reports" CssClass="btn btn-info btn-small"
                        OnClick="OnOrderReportsClick" Style="margin-left: 20px" Visible="False"/>
                    <br />
                    <asp:Label ID="Label5" runat="server" Text="Send email:" CssClass="label" Style="margin-left: 100px"></asp:Label>
                    <asp:DropDownList runat="server" ID="MailAddressList" DataValueField="Email" DataTextField="DisplayName" AutoPostBack="True" OnSelectedIndexChanged="OnChoiceMailFromContacts"/>
                    <asp:Label ID="Label6" runat="server" Text="Address To" CssClass="" Style="padding-left: 20px"></asp:Label>
                    <asp:TextBox runat="server" ID="AddressTo" Width="150px" placeholder="Email"></asp:TextBox>
                    <asp:ImageButton ID="GoEmailButton" ToolTip="Email Short Report" runat="server" ImageUrl="~/Images/ajaxImages/email_go.png"
                        OnClick="OnSendEmailClick" />
                    <asp:Label ID="SendtoNYLbl" runat="server" Text="Send email to NY:" CssClass="label" Style="margin-left: 5px"></asp:Label>
                    <asp:DropDownList runat="server" ID="NYMailList" Width="150px" DataValueField="Email" DataTextField="Text" AutoPostBack="True" OnSelectedIndexChanged="OnNYMailContacts">
                        <asp:ListItem Selected="True" Text="--Select--" Value="--Select--"></asp:ListItem>
                        <asp:ListItem Text="AlexandraS@gemscience.net" Value="AlexandraS@gemscience.net"></asp:ListItem>
                        <asp:ListItem Text="ZalesTeam@gemscience.net" Value="ZalesTeam@gemscience.net"></asp:ListItem>
                        <asp:ListItem Text="SterlingTeam@gemscience.net" Value="SterlingTeam@gemscience.net"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox runat="server" ID="NYAddress" Width="150px" placeholder="Email" Visible="false"></asp:TextBox>
                    <asp:Button ID="PrintNYButton" runat="server" Text="Print in NY" CssClass="btn btn-info btn-small" OnClick="OnSendNYEmailClick" Style="margin-left: 5px" />
                    <asp:Button ID="NYSpecInstr" runat="server" Text="Special Instructions" CssClass="btn btn-info btn-small" OnClick="OnSendNYEmailSpecInstrClick" Style="margin-left: 5px" />
                    <asp:HiddenField runat="server" ID="hdnSKU"/>
                    <asp:HiddenField runat="server" ID="hdnCustomerCode"/>
                    <asp:HiddenField runat="server" ID="hdnOrder" />
                </asp:Panel>
                <div id="gridContainer" style="font-size: small;" runat="server">
                    <asp:DataGrid ID="grdShortReport" runat="server" CellPadding="5" 
                        AllowSorting="False" OnItemDataBound="OnDataItemBinding"
                        OnSortCommand="OnSortCommand" OnItemCommand="OnEditItemCommand"  
                        DataKeyField="_ItemNumber" >
<%--                        <HeaderStyle CssClass="GridviewScrollHeader" />--%>
<%--                        <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>--%>
<%--                        <PagerStyle CssClass="GridviewScrollPager" />--%>
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" Font-Size="small"/>
                <ItemStyle ForeColor="#0D0D0D" BackColor="White" BorderColor="silver"></ItemStyle>
                        <Columns>
                            <asp:ButtonColumn Text="Edit"/>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" Text='' ID="Checkbox1"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderTemplate>Status</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="ItemStateImage" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:TemplateColumn>--%>
<%--                                <HeaderTemplate>Lbl doc</HeaderTemplate>--%>
<%--                                <ItemTemplate>--%>
<%--                                    <asp:ImageButton ID="LblDocViewBtn" ToolTip="Preview label" runat="server"--%>
<%--                                        ImageUrl="~/Images/ajaxImages/download16.png" OnClick="OnLblDocViewClick" Enabled="true" Style="text-align: center"/>--%>
<%--                                    <asp:DropDownList runat="server" ID="LabelDocsList" DataValueField="DocumentId" DataTextField="DocumentName"/>--%>
<%--                                </ItemTemplate>--%>
<%--                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn>
                                <HeaderTemplate>Print
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ShowPrintInfoBtn" ToolTip="Show printed info" runat="server"
                                        ImageUrl="~/Images/ajaxImages/information16.png" OnClick="OnShowPrintInfoClick" Enabled="true" Style="text-align: center"/>
                                    <asp:ImageButton ID="ShowPdfBtn" ToolTip="Show pdf" runat="server"
                                        ImageUrl="~/Images/ajaxImages/pdf16.png" OnClick="OnShowPdfClick" Enabled="true" Style="text-align: center"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>

                <%-- Show Printed Documents Dialog --%>
                <asp:Panel runat="server" ID="PrintedDocsPanel" CssClass="modalPopup" Style="display: none;width: 500px;border: solid 1px grey">
                    <asp:Panel ID="PrintedDocsDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div style="text-align: center;">
                            <p>
                                <asp:Label runat="server" ID="PrintedDocsTitle"></asp:Label>
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel5" runat="server" Style="padding-top: 10px; color: black">
                        <div >
                            <asp:DataGrid runat="server" ID="PrintedDocsGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                <Columns>
                                    <asp:BoundColumn DataField="OperationChar" HeaderText="Operation"/>
                                    <asp:BoundColumn DataField="ReportName" HeaderText="Report"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ReportDate" HeaderText="Date"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </div>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="PrintedDocsCloseBtn" runat="server" Text="Close" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="PrintedDocHiddenBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PrintedDocHiddenBtn" PopupControlID="PrintedDocsPanel" ID="PrintedDocsPopupExtender"
                PopupDragHandleControlID="PrintedDocsDragHandle" OkControlID="PrintedDocsCloseBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Show/Hide Columns Dialog --%>
                <asp:Panel runat="server" ID="HideColumnsPanel" CssClass="modalPopup"  Style="display: none;width: 600px;border: solid 2px #5377A9">
                    <asp:Panel ID="HideColumnsDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div style="text-align: center; color: #5377A9; font-weight: bold">
                            <p>
                                <asp:Label ID="ShowHideTitle" runat="server" />
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" Style="padding-top: 10px;color: black">
                            <div style="overflow-y: auto; overflow-x: hidden; height: 530px; width: 95%" id="ShowHideDiv" runat="server">
                                <asp:DataGrid runat="server" ID="ColumnsGrid" AutoGenerateColumns="False" CssClass="table table-condensed" DataKeyField="Title">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                Hide
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="text-align: center;">
                                                    <asp:CheckBox runat="server" Text='' ID="HideCheckBox" />
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Title" HeaderText="Title"></asp:BoundColumn>
                                         <asp:BoundColumn DataField="Value" HeaderText="Content"></asp:BoundColumn>
                                         <asp:BoundColumn DataField="DocumentName" HeaderText="Document"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                </asp:DataGrid>
                            </div>
                        
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="HideColumnsOkBtn" runat="server" OnClick="OnHideColumnsOkClick" Text="Apply"/>
                            <asp:Button ID="HideColumnsCancBtn" runat="server" Text="Close" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBtn" PopupControlID="HideColumnsPanel" ID="HideColumnsPopupExtender"
                PopupDragHandleControlID="HideColumnsDragHandle" OkControlID="HideColumnsCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;color: black"
                        id="MessageDiv" runat="server">
                    </div>
                    <asp:HiddenField runat="server" ID="EditDialogUp"/>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" OnClick="OnCloseInfoDialogClick"/>
                            <asp:Button ID="InfoPsevdoCloseButton" runat="server" Text="Ok" Style="display: none" Enabled="False"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OkControlID="InfoPsevdoCloseButton">
                </ajaxToolkit:ModalPopupExtender>
               <%--alex --%>
                <%-- Special instructions dialog Dialog --%>
                <asp:Panel runat="server" ID="SpecInstrPanel" CssClass="modalPopup" Style="width: 410px;display: none">
                    <asp:Panel ID="SpecInstrDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div style="text-align: center;color:#5377A9;font-weight: bold ">
                            <p><asp:Label ID="Label7" Text="Special Instructions" runat="server"/></p>
                        </div>
                    </asp:Panel>
                    <asp:TextBox style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;color: black;" 
                        id="SpecInstrText" runat="server"  TextMode="MultiLine" Rows="10" Width="95%">
                    </asp:TextBox>
                    <asp:HiddenField runat="server" ID="HiddenField1"/>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="SpecInstrOkBtn" runat="server" Text="Ok" OnClick="OnSpecInstrOkClick"/>
                            <asp:Button ID="Button2" runat="server" Text="Ok" Style="display: none" Enabled="False"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupSpecInstrButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupSpecInstrButton"
                    PopupControlID="SpecInstrPanel" ID="SpecInstrPopupExtender" PopupDragHandleControlID="SpecInstrDragHandle"
                    OkControlID="SpecInstrOkBtn">
                </ajaxToolkit:ModalPopupExtender>
               <%--alex --%>
                <%-- Edit Item Dialog --%>
                <asp:Panel runat="server" ID="EditItemPanel" CssClass="modalPopup"  Style="display: none;width: 510px;border: solid 2px #5377A9">
                    <asp:Panel ID="EditItemDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div style="text-align: center;color:#5377A9;font-weight: bold ">
                            <p><asp:Label ID="EditPanelTitle"  runat="server"/></p>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server" Style="padding-top: 10px;">
                        <div>
                            <asp:Label runat="server" Text="Calculated/Invisible measures" Style="color: black"></asp:Label>
                            <asp:DropDownList runat="server" ID="UnknownMeasureList" Width="300px" style="margin-left: 10px"/>
                            <asp:HiddenField runat="server" ID="EditingItemKeyFld"/>
                        </div>
                        <div style="overflow-y: auto;height:300px;width: 500px;color: black;border: silver solid 1px;margin-top: 5px" id="EditItemDiv" runat="server">
                            <asp:DataGrid runat="server" ID="ItemValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                    DataKeyField="UniqueKey" OnItemDataBound="OnEditItemDataBound" GridLines="None" ShowHeader="True">
                                <Columns>
                                    <asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureTitle" HeaderText="Measure"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureClass" HeaderText="Measure Class" Visible="False">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Value</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="ItemValuePanel" CssClass="form-inline" DefaultButton="EditHiddenBtn">
                                                <asp:Button runat="server" ID="EditHiddenBtn" Style="display: none" Enabled="False"/>
                                                <asp:TextBox runat="server" ID="ValueStringFld" Width="135px"></asp:TextBox>
                                                <asp:TextBox runat="server" ID="ValueNumericFld" Width="135px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="ValueNumericExtender" runat="server" FilterType="Custom"
                                                    ValidChars="0123456789." TargetControlID="ValueNumericFld" />
                                                <asp:DropDownList runat="server" ID="ValueEnumFld" DataTextField="ValueTitle"
                                                    DataValueField="MeasureValueId" Width="150px" />
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </div>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="EditItemOkBtn" runat="server" OnClick="OnEditItemOkClick" Text="Save" OnClientClick = 'return confirm("Are you sure you want to save changed measures?");'/>
                            <asp:Button ID="EditItemCancBtn" runat="server" Text="Cancel" />
                            <asp:Label ID="ItemErrorLbl" ForeColor="Red" runat="server" Font-Size="12px"></asp:Label>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenEditItemBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenEditItemBtn" PopupControlID="EditItemPanel" ID="EditItemPopupExtender"
					PopupDragHandleControlID="EditItemDragHandle" OkControlID="EditItemCancBtn" >
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <b>Save</b>
                        </div>
                    </asp:Panel>
                    <asp:Label style="color: black;padding-top: 10px" runat="server" ID="SaveQstLabel"></asp:Label>
                    <div ><asp:HiddenField runat="server" ID="SaveMode"/></div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender"
                PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesSaveNoBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Preview old Label Dialog --%>
                <asp:Panel runat="server" ID="LabelOldPanel" CssClass="modalPopup" Style="display: none;border: solid 2px #5377A9"  >
                    <asp:Panel ID="LabelOldDragHandle" runat="server" Style="cursor: move; background-color: white;">
                        <div style="color: #5377A9; font-weight: bold;text-align: center">
                            <asp:Label ID="LabelOldTitle" Text="Preview Labels" runat="server" />
                        </div>
                    </asp:Panel>
                    <div runat="server" id="LabelOldDiv" style="overflow-y: auto;overflow-x: hidden;height: 350px;width: 260px;text-align: left">
                        <asp:DataGrid runat="server" ID="LabelOldGrid" ShowHeader="False" AutoGenerateColumns="False" CssClass=""
                               DataKeyField="NewItemNumber" OnItemDataBound="OnPreviewLabelDataBound" GridLines="None" Style="text-align: left;max-width: 260px">
                            <Columns>
                                <asp:TemplateColumn >
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate >
                                        <asp:Panel runat="server" ID="LabelOldPrintPanel" Style="font-family: 'Arial Narrow';font-weight: bold; font-size: 10px; color: black; text-align: center;background-color: white;height: 87.5pt;margin: 0pt;vertical-align: top">
                                            <asp:Panel ID="Panel6" runat="server" Height="24px">
                                                <asp:Label runat="server" ID="Barcode" Style="font-family: 'Code 128'; font-size: 24px;font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel7" runat="server" Height="14px">
                                                <asp:Label runat="server" ID="BarcodeNum" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel8" runat="server" CssClass="form-inline"  Height="14px">
                                                <asp:Label runat="server" ID="IsPrinted"></asp:Label>
                                                <asp:Label runat="server" ID="Value1" Style="padding-left: 10px"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel9" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value2_3" ForeColor="black"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel10" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value4"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel11" runat="server" Height="12px">
                                                <asp:Label runat="server" ID="NewBatchNumber" Style="font-size: 10px"></asp:Label>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div >
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="PrintLblOldBtn" runat="server" CssClass="btn btn-small btn-default"
                                OnClick="OnLabelOldPrintClick" OnClientClick="LabelOldPrint()" Text="Print" />
                            <asp:Button ID="CloseLblOldBtn" runat="server" CssClass="btn btn-small btn-default"
                                Text="Close" />
                        </p>
                    </div>
                    
                </asp:Panel>
                <asp:Button ID="HiddenLblOldPrintBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblOldPrintBtn" PopupControlID="LabelOldPanel" ID="LabelOldPopupExtender"
					PopupDragHandleControlID="LabelOldDragHandle" OkControlID="CloseLblOldBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Preview old 2 Label Dialog --%>
                <asp:Panel runat="server" ID="LabelOld2Panel" CssClass="modalPopup" Style="display: none;border: solid 2px #5377A9" Width="600px"  >
                    <asp:Panel ID="LabelOld2DragHandle" runat="server" Style="cursor: move; background-color: white;">
                        <div style="color: #5377A9; font-weight: bold;text-align: center">
                            <asp:Label ID="LabelOld2Title" Text="Preview Labels" runat="server" />
                        </div>
                    </asp:Panel>
                    <div runat="server" id="LabelOld2Div" style="overflow-y: auto;overflow-x: hidden;height: 350px;width: 600px;text-align: left">
                        <asp:DataGrid runat="server" ID="LabelOld2Grid" ShowHeader="False" AutoGenerateColumns="False" CssClass=""
                               DataKeyField="UniqueKey" OnItemDataBound="OnPreviewLabelDataBound2" GridLines="None"  Width="550px">
                            <Columns>
                                <asp:TemplateColumn >
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate >
                                        <asp:Panel runat="server" ID="LabelOldPrintPanel_l" Style="font-family: 'Arial Narrow';
											font-weight: bold; font-size: 10px; color: black; text-align: center;background-color: white;
											height: 87.5pt;margin: 0pt;vertical-align: top">
                                            <asp:Panel ID="Panel6_l" runat="server" Height="24px">
                                                <asp:Label runat="server" ID="Barcode_l" Style="font-family: 'Code 128'; font-size: 24px;  font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel7_l" runat="server" Height="14px">
                                                <asp:Label runat="server" ID="BarcodeNum_l" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel8_l" runat="server" CssClass="form-inline"  Height="14px">
                                                <asp:Label runat="server" ID="IsPrinted_l"></asp:Label>
                                                <asp:Label runat="server" ID="Value1_l" Style="padding-left: 10px"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel9_l" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value2_3_l" ForeColor="black"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel10_l" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value4_l"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel11_l" runat="server" Height="12px">
                                                <asp:Label runat="server" ID="NewBatchNumber_l" Style="font-size: 10px"></asp:Label>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn >
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate >
                                        <asp:Panel runat="server" ID="LabelOldPrintPanel_r" Style="font-family: 'Arial Narrow';
											font-weight: bold; font-size: 10px; color: black; text-align: center;background-color: white;
											height: 87.5pt;margin: 0pt;vertical-align: top">
                                            <asp:Panel ID="Panel6" runat="server" Height="24px">
                                                <asp:Label runat="server" ID="Barcode_r" Style="font-family: 'Code 128'; font-size: 24px;font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel7_r" runat="server" Height="14px">
                                                <asp:Label runat="server" ID="BarcodeNum_r" CssClass=""></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel8_r" runat="server" CssClass="form-inline"  Height="14px">
                                                <asp:Label runat="server" ID="IsPrinted_r"></asp:Label>
                                                <asp:Label runat="server" ID="Value1_r" Style="padding-left: 10px"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel9_r" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value2_3_r" ForeColor="black"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel10_r" runat="server"  Height="14px">
                                                <asp:Label runat="server" ID="Value4_r"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel11_r" runat="server" Height="12px">
                                                <asp:Label runat="server" ID="NewBatchNumber_r" Style="font-size: 10px"></asp:Label>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div >
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="PrintLblOld2Btn" runat="server" CssClass="btn btn-small btn-default"
                                OnClick="OnLabelOld2PrintClick" OnClientClick="LabelOld2Print()" Text="Print" />
                            <asp:Button ID="CloseLblOld2Btn" runat="server" CssClass="btn btn-small btn-default"
                                Text="Close" />
                        </p>
                    </div>
                    
                </asp:Panel>
                <asp:Button ID="HiddenLblOld2PrintBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblOld2PrintBtn" PopupControlID="LabelOld2Panel" ID="LabelOld2PopupExtender"
					PopupDragHandleControlID="LabelOld2DragHandle" OkControlID="CloseLblOld2Btn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Preview TLKW Label Dialog --%>
                <asp:Panel runat="server" ID="LabelTlkwPanel" CssClass="modalPopup" Style="display: none;border: solid 2px #5377A9" Width="350px"  >
                    <asp:Panel ID="LabelTlkwDragHandle" runat="server" Style="cursor: move; background-color: white;">
                        <div style="color: #5377A9; font-weight: bold;text-align: center">
                            <asp:Label ID="LabelTlkwTitle" Text="Preview Labels" runat="server" />
                        </div>
                    </asp:Panel>
                    <div runat="server" id="LabelTlkwDiv" style="overflow-y: auto;overflow-x: auto;height: 384px;">
                        <asp:DataGrid runat="server" ID="LabelTlkwGrid" ShowHeader="False" AutoGenerateColumns="False" CssClass=""
                               DataKeyField="UniqueKey" OnItemDataBound="OnPreviewLabelDataBound" GridLines="None" Width="264px" >
                            <Columns>
                                <asp:TemplateColumn>
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Panel runat="server" ID="LabelTlkwPrintPanel" Style="font-family: 'Arial Narrow';font-weight: bold; font-size: 7px;color: black; padding-top: 0px; text-align: center;background-color: white;height: 82.5pt;margin: 0pt">
											<table style="line-height: 22px">
												<tr>
													<td>
													    <asp:Label runat="server" ID="TlkwBarcode" Style="font-family: 'Code 128'; font-size: 24px;font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""/>
													</td>
													<td>
													    <table style="line-height: 7px">
													        <tr><td><asp:Label runat="server" ID="TlkwDate"  ForeColor="Black" BackColor="White" Style="font-size: 5px;" CssClass=""/></td></tr>
                                                            <tr><td><asp:Label runat="server" ID="TlkwBarcodeNum" CssClass=""  Style="font-size: 7px;font-weight: bold"/></td></tr>
													    </table>
													</td>
												</tr>
											</table>
											<table style="text-align: left; line-height: 6px;font-family: 'Arial Narrow';font-weight: bold; font-size: 7px">
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle0" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue0" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle0" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue0" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle1" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue1" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle1" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue1" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle2" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue2" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle2" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue2" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle3" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue3" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle3" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue3" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle4" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue4" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle4" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue4" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle5" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue5" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle5" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue5" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle6" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue6" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle6" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue6" CssClass=""/></td>
												</tr>
												<tr>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments1" CssClass=""/></td>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments2" CssClass="" Style="font-style: italic"/></td>
												</tr>
											</table>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="PrintLblTlkwBtn" runat="server" CssClass="btn btn-small btn-default"
                                OnClick="OnLabelTlkwPrintClick" OnClientClick="LabelTlkwPrint()" Text="Print" />
                            <asp:Button ID="CloseLblTlkwBtn" runat="server" CssClass="btn btn-small btn-default"
                                Text="Close" />
                        </p>
                    </div>
                    
                </asp:Panel>
                <asp:Button ID="HiddenLblTlkwPrintBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblTlkwPrintBtn" PopupControlID="LabelTlkwPanel" ID="LabelTlkwPopupExtender"
					PopupDragHandleControlID="LabelTlkwDragHandle" OkControlID="CloseLblTlkwBtn" >
                </ajaxToolkit:ModalPopupExtender>

				<%-- Preview TLKW 2 Label Dialog --%>
                <asp:Panel runat="server" ID="LabelTlkw2Panel" CssClass="modalPopup" Style="display: none;border: solid 2px #5377A9" Width="600px"  >
                    <asp:Panel ID="LabelTlkw2DragHandle" runat="server" Style="cursor: move; background-color: white;">
                        <div style="color: #5377A9; font-weight: bold;text-align: center">
                            <asp:Label ID="LabelTlkw2Title" Text="Preview Labels" runat="server" />
                        </div>
                    </asp:Panel>
                    <div runat="server" id="LabelTlkw2Div" style="overflow-y: auto;overflow-x: auto;height: 384px;">
                        <asp:DataGrid runat="server" ID="LabelTlkw2Grid" ShowHeader="False" AutoGenerateColumns="False" CssClass=""
                               DataKeyField="UniqueKey" OnItemDataBound="OnPreviewLabelDataBound2" GridLines="None" Width="550px" >
                            <Columns>
                                <asp:TemplateColumn>
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Panel runat="server" ID="LabelTlkwPrintPanel_l" Style="font-family: 'Arial Narrow';font-weight: bold; font-size: 7px; 
                                            color: black; padding-top: 0px; text-align: center;background-color: white;height: 82.5pt;margin: 0pt">
											<table style="line-height: 22px">
												<tr>
													<td>
													    <asp:Label runat="server" ID="TlkwBarcode_l" Style="font-family: 'Code 128'; font-size: 24px;font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""/>
													</td>
													<td>
													    <table style="line-height: 7px">
													        <tr><td><asp:Label runat="server" ID="TlkwDate_l"  ForeColor="Black" BackColor="White" Style="font-size: 5px;" CssClass=""/></td></tr>
                                                            <tr><td><asp:Label runat="server" ID="TlkwBarcodeNum_l" CssClass=""  Style="font-size: 7px;font-weight: bold"/></td></tr>
													    </table>
													</td>
												</tr>
											</table>
											<table style="text-align: left; line-height: 6px;font-family: 'Arial Narrow';font-weight: bold; font-size: 7px">
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle0_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue0_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle0_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue0_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle1_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue1_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle1_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue1_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle2_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue2_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle2_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue2_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle3_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue3_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle3_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue3_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle4_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue4_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle4_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue4_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle5_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue5_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle5_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue5_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle6_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue6_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle6_l" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue6_l" CssClass=""/></td>
												</tr>
												<tr>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments1_l" CssClass=""/></td>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments2_l" CssClass="" Style="font-style: italic"/></td>
												</tr>
											</table>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
								<asp:TemplateColumn>
                                     <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Panel runat="server" ID="LabelTlkwPrintPanel_r" Style="font-family: 'Arial Narrow';font-weight: bold; font-size: 7px; color: black; padding-top: 0px; text-align: center;background-color: white;height: 82.5pt;margin: 0pt">
											<table style="line-height: 22px">
												<tr>
													<td>
													    <asp:Label runat="server" ID="TlkwBarcode_r" Style="font-family: 'Code 128'; font-size: 24px;font-weight: normal" ForeColor="Black" BackColor="White" CssClass=""/>
													</td>
													<td>
													    <table style="line-height: 7px">
													        <tr><td><asp:Label runat="server" ID="TlkwDate_r"  ForeColor="Black" BackColor="White" Style="font-size: 5px;" CssClass=""/></td></tr>
                                                            <tr><td><asp:Label runat="server" ID="TlkwBarcodeNum_r" CssClass=""  Style="font-size: 7px;font-weight: bold"/></td></tr>
													    </table>
													</td>
												</tr>
											</table>
											<table style="text-align: left; line-height: 6px;font-family: 'Arial Narrow';font-weight: bold; font-size: 7px">
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle0_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue0_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle0_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue0_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle1_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue1_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle1_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue1_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle2_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue2_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle2_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue2_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle3_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue3_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle3_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue3_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle4_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue4_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle4_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue4_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle5_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue5_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle5_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue5_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td><asp:Label runat="server" ID="TlkwLTitle6_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwLValue6_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRTitle6_r" CssClass=""/></td>
												    <td><asp:Label runat="server" ID="TlkwRValue6_r" CssClass=""/></td>
												</tr>
												<tr>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments1_r" CssClass=""/></td>
												    <td colspan="2"><asp:Label runat="server" ID="TlkwComments2_r" CssClass="" Style="font-style: italic"/></td>
												</tr>
											</table>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateColumn>							
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="PrintLblTlkw2Btn" runat="server" CssClass="btn btn-small btn-default"
                                OnClick="OnLabelTlkw2PrintClick" OnClientClick="LabelTlkw2Print()" Text="Print" />
                            <asp:Button ID="CloseLblTlkw2Btn" runat="server" CssClass="btn btn-small btn-default"
                                Text="Close" />
                        </p>
                    </div>
                    
                </asp:Panel>
                <asp:Button ID="HiddenLblTlkw2PrintBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblTlkw2PrintBtn" PopupControlID="LabelTlkw2Panel" ID="LabelTlkw2PopupExtender"
					PopupDragHandleControlID="LabelTlkw2DragHandle" OkControlID="CloseLblTlkw2Btn" >
                </ajaxToolkit:ModalPopupExtender>
                <%-- Show Label Define Document Dialog --%>
                <asp:Panel runat="server" ID="LblDocPanel" CssClass="modalPopup" Style="display: none; width: 500px; border: solid 1px grey">
                    <asp:Panel ID="LblDocDragHandle" runat="server" Style="cursor: move; background-color: white;">
                        <div style="color: #5377A9; font-weight: bold;text-align: center">
                            <asp:Label ID="LblDocTitle" Text="Lbl Document" runat="server" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel12" runat="server" Style="padding-top: 10px; color: black">
                        <div style="overflow-y: auto; overflow-x: auto;height: 300px;width: 99%">
                            <asp:DataGrid runat="server" ID="LblDocGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                <Columns>
                                    <asp:BoundColumn DataField="Title" HeaderText="Title"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Value" HeaderText="Value"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </div>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="LblDocCloseBtn" runat="server" Text="Close" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="LblDocHiddenBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="LblDocHiddenBtn"
                    PopupControlID="LblDocPanel" ID="LblDocPopupExtender" PopupDragHandleControlID="LblDocDragHandle"
                    OkControlID="LblDocCloseBtn">
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- PDF Viewer --%>
                <asp:Panel runat="server" ID="PdfPanel" CssClass="modalPopup" Style="width: 505px;display: none;border-color: silver">
                    <asp:Panel runat="server" ID="PdfPanelDragHandle" Style="cursor: move; background-color: whitesmoke; color: black; text-align: left;padding-bottom: 2px">
                        <div>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajaxImages/pdf24.png" />
                            <asp:Label ID="PdfName" runat="server"/>
                        </div>
                    </asp:Panel>
                    <div >
						<iframe src="PdfMapHandler.ashx?pdf=" width="500" height="400" runat="server" ID="PdfFrame"></iframe>
                    </div>
                    <asp:HiddenField runat="server" ID="PdfDialogUp"/>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="PdfViewCloseButton" runat="server" Text="Close"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PdfCloseButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PdfCloseButton"
                    PopupControlID="PdfPanel" ID="PdfPopupExtender" PopupDragHandleControlID="PdfPanelDragHandle"
                    OkControlID="PdfViewCloseButton">
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Bulk Update Dialog --%>
                <asp:Panel runat="server" ID="BulkItemPanel" CssClass="modalPopup"  Style="width: 650px;border: solid 2px #5377A9;display: none;" DefaultButton="BulkEnterHiddenBtn">
                    <asp:Button runat="server" ID="BulkEnterHiddenBtn" Style="display: none" OnClick="OnBulkEnter"/>
                    <asp:Panel ID="BulkItemDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: none 1px Gray; color: Black">
                        <div style="text-align: left;color:#5377A9;font-weight: bold ">
                            <p>Bulk Update</p>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="BulkContentPanel" runat="server" Style="padding-top: 10px;color: black">
                        <table>
                            <tr>
                                <td style="vertical-align: top">
                                    <div>
                                        <asp:Label runat="server" Text="Parts" CssClass="" Style="font-weight: bold;color: grey"></asp:Label><br />
                                        <asp:DropDownList runat="server" ID="PartsListField" OnSelectedIndexChanged="OnBulkPartChanged"
                                            AutoPostBack="True" DataTextField="PartName" DataValueField="PartId" />
                                    </div>
                                    <div>
                                        <asp:Label ID="Label4" runat="server" Text="Measures" CssClass="" Style="font-weight: bold;color: grey"></asp:Label><br />
                                        <asp:DropDownList runat="server" ID="MeasureListField" OnSelectedIndexChanged="OnBulkMeasureChanged"
                                            AutoPostBack="True" DataValueField="MeasureId" DataTextField="MeasureName" />
                                    </div>
                                    <div>
                                        <asp:Label runat="server" Text="New Value" CssClass="" Style="font-weight: bold;color: grey"></asp:Label><br />
                                        <asp:DropDownList runat="server" ID="MeasureEnumField" OnSelectedIndexChanged="OnBulkEnumMeasureChanged"
                                                    Visible="False" AutoPostBack="True" Width="100%" DataValueField="MeasureValueId"
                                                    DataTextField="ValueTitle" />
                                        <asp:Panel runat="server" DefaultButton="BulkNumHiddenBtn">
                                            <asp:TextBox ID="MeasureNumericField" runat="server" Visible="False" Width="90%"
                                                ToolTip="Numeric Value" BackColor="#CCFFCC" />
                                           <asp:Button runat="server" ID="BulkNumHiddenBtn" OnClick="OnBulkNumFieldChanged" style="display:none"/>
                                           <ajaxToolkit:FilteredTextBoxExtender ID="SrpExtender" runat="server" ValidChars="0123456789."
                                                TargetControlID="MeasureNumericField" Enabled="True" />
                                            
                                        </asp:Panel>
                                        <asp:Panel runat="server" DefaultButton="BulkTxtHiddenBtn">
                                            <asp:TextBox ID="MeasureTextField" runat="server" Visible="False" Width="90%" />
                                            <asp:Button runat="server" ID="BulkTxtHiddenBtn" OnClick="OnBulkTxtFieldChanged" Style="display: none"/>
                                        </asp:Panel>
                                            
                                    </div>
                                </td>
                                <td style="width: 30px"></td>
                                <td style="vertical-align: top">
                                    <asp:Label runat="server" Text="Entered Values" CssClass="" Style="font-weight: bold;color: grey"></asp:Label><br/>
                                    <div style="overflow-y: auto;height: 300px;overflow-x: hidden;width: 360px">
                                        <asp:DataGrid runat="server" ID="EnteredGrid" AutoGenerateColumns="False" CellPadding="2"
                                            CellSpacing="2" OnItemCommand="OnBulkDelCommand" DataKeyField="UniqueKey" Width="350px" >
                                            <Columns>
                                                <%--HeaderImageUrl="Images/ajaxImages/del.png" --%>
                                                <asp:ButtonColumn Text="Del" ButtonType="LinkButton" HeaderText="Del" />
                                                <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DisplayValue" HeaderText="Value"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                            <ItemStyle Font-Names="Cambria" Font-Size="Small" BorderColor="Silver" />
                                        </asp:DataGrid>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="BulkItemOkBtn" runat="server" OnClick="OnBulkUpdateSaveClick" Text="Save" OnClientClick = 'return confirm("Are you sure you want to save changed measures?");'/>
                            <asp:Button ID="BulkItemCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenBulkItemBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBulkItemBtn" PopupControlID="BulkItemPanel" ID="BulkItemPopupExtender"
					PopupDragHandleControlID="BulkItemDragHandle" OkControlID="BulkItemCancBtn" >
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Bulk Update Init: Select Item Numbers dialog  --%>
                <asp:Panel runat="server" ID="BulkInitPanel" CssClass="modalPopup" Style="width: 400px;border: solid 2px #5377A9;color: black;display: none">
					<asp:Panel runat="server" ID="BulkInitPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: none 1px Gray; color: Black">
                        <div style="text-align: left;color:#5377A9;font-weight: bold ">
                            <p>Select Item numbers for Bulk Update</p>
                        </div>
                    </asp:Panel>

                    <div >
                        <asp:RadioButtonList runat="server" ID="ItemTypesList" DataValueField="ItemTypeId" DataTextField="ItemTypeName" OnSelectedIndexChanged="OnItemTypeChanged" AutoPostBack="True"/>
                        <div style="overflow-y: auto; overflow-x: hidden;height: 400px">
                            <asp:TreeView ID="BulkUpdateItems" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True" OnSelectedNodeChanged="OnBulkUpdateTreeSelectedChanged"
                                ShowLines="False" ShowExpandCollapse="true">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                        </div>
                    </div>
                    
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
							<asp:Button ID="BulkInitNextButton" runat="server" Text="Next" OnClick="OnBulkInitNextClick"/>
                            <asp:Button ID="BulkInitCloseButton" runat="server" Text="Cancel"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="BulkInitHiddenButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="BulkInitHiddenButton"
                    PopupControlID="BulkInitPanel" ID="BulkInitPopupExtender" PopupDragHandleControlID="BulkInitPanelDragHandle"
                    OkControlID="BulkInitCloseButton">
                </ajaxToolkit:ModalPopupExtender>
                <%-- MovedItems Dialog --%>
                <asp:Panel runat="server" ID="MovedItemsPanel" CssClass="modalPopup"  Style="display: none;width: 600px;border: solid 2px #5377A9">
                    <asp:Panel ID="MovedItemsDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div style="text-align: center; color: #5377A9; font-weight: bold">
                            <p>
                                <asp:Label ID="MovedItemsTitle" runat="server" Text="Items moved from this orders"/>
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel13" runat="server" Style="padding-top: 10px;color: black">
                            <div style="overflow-y: auto; overflow-x: hidden; height: 530px; width: 95%" id="MovedItemsDiv" runat="server">
                                <asp:DataGrid runat="server" ID="MovedItemsGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                    <Columns>
										<asp:BoundColumn DataField="OldFullItemNumberWithDotes" HeaderText="Old Item Number"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NewFullItemNumberWithDotes" HeaderText="New Item Number"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                </asp:DataGrid>
                            </div>
                        
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="MovedItemsCancBtn" runat="server" Text="Close" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="Button1" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="Button1" PopupControlID="MovedItemsPanel" ID="MovedItemsBtnPopupExtender"
                PopupDragHandleControlID="MovedItemsDragHandle" OkControlID="MovedItemsCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

<%--        <ajaxToolkit:ResizableControlExtender ID="ResizableControlExtender1" runat="server"--%>
<%--            BehaviorID="ResizableControlBehavior1" --%>
<%--            TargetControlID="PdfPanel"--%>
<%--            ResizableCssClass="resizingImage"--%>
<%--            HandleCssClass="handleImage"--%>
<%--            MinimumWidth="505"--%>
<%--            MinimumHeight="400"--%>
<%--            MaximumWidth="1000"--%>
<%--            MaximumHeight="1000"--%>
<%--            HandleOffsetX="3"--%>
<%--            HandleOffsetY="3"/>--%>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ExcelButton"/>
                <asp:PostBackTrigger ControlID="GoEmailButton"/>
                <asp:PostBackTrigger ControlID="DownloadLabelBtn"/>
                <asp:PostBackTrigger ControlID="MovedItemsBtn"/>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
