﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="CustomerEdit1.aspx.cs" Inherits="Corpt.CustomerEdit1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
	<ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
    	.lblKYC {
    		border: 0px none white !important;
			box-shadow:none !important;
    	}
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: #555555;
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        body {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 13px;
            margin-top: -17px;
            background: white;
            width: max-content;
            margin-left: 10px;
            background-color: #999999;
            color: #ffffff;
            border-radius: 5px;
            padding-left: 10px;
            padding-right: 10px;
            margin-bottom: 5px;
        }

        #dvPermissionsPanel > div > span > label {
            color: white !important;
            font-weight: bold !important;
        }
   
		.modalProgress {
			position: fixed;
			z-index: 999;
			height: 100%;
			width: 100%;
			top: 0;
			left:0;
			background-color: Black;
			filter: alpha(opacity=60);
			opacity: 0.6;
			-moz-opacity: 0.8;
		}
		.centerProgress {
			z-index: 1000;
			margin:20% 40%;
			padding: 10px;
			width: 70px;
			background-color: White;
			border-radius: 10px;
			filter: alpha(opacity=100);
			opacity: 1;
			-moz-opacity: 1;
			text-align:center;
			color: black;
			font-size: 9px;
		}
		.centerProgress img {
			height: 50px;
			width: 50px;
		}
		.radioButtonList input[type="radio"],
		.radioButtonList input[type="checkbox"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 13px;
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.checkboxSingle input[type="checkbox"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 5px;
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.checkboxSingleCarier input[type="checkbox"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 5px;
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.CommunicationCheckbox input[type="checkbox"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 4px;
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.rblKYC input[type="radio"],
		.rblKYC input[type="checkbox"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 8px;
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.radioButtonList a,
		.CommunicationCheckbox a {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			padding-left: 30px;
			width: max-content;
			line-height: 25px;
			margin: 5px 5px 5px 5px;
			padding-top: 6px;
			padding-bottom: 6px;
			height: 37px;
			padding-right: 25px;
		}

		.radioButtonList label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			height: 30px;
			padding-left: 30px;
			width: max-content;
			line-height: 25px;
			margin: 5px 5px 5px 5px;
			padding-top: 6px;
			padding-bottom: 6px;
		}

		.checkboxSingle label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			line-height: 25px;
			padding-bottom: 6px;
			padding-top: 2px;
			height: 20px;
			margin-bottom: 2px;
			margin-top: 0px;
			padding-left: 32px;
		}

		.checkboxSingleCarier label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			width: max-content;
			line-height: 25px;
			padding-bottom: 6px;
			padding-top: 2px;
			height: 20px;
			margin-bottom: 8px;
			margin-top: 0px;
			padding-left: 32px;
		}

		.rblKYC label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			height: 20px;
			padding-left: 30px;
			width: max-content;
			line-height: 25px;
			margin: 2px 5px 2px 5px;
			padding-top: 3px;
			padding-bottom: 6px;
		}

		.nodeStyle {
			padding: 0px 6px 8px 0px;
		}

		.filedSetBorder {
			border: 1px groove black !important;
			padding: 5px 3px 5px 3px;
			border-radius: 8px;
			margin: 0px 5px 5px 5px;
			float: left;
			vertical-align: top;
		}

			.filedSetBorder legend {
				padding-left: 7px;
				padding-right: 7px;
				margin-bottom: 0px;
				border-bottom: 0px !important;
				font-family: verdana,tahoma,helvetica;
				width: max-content;
			}

		.DropDownListBoxStyle {
			font-size: 15px;
			font-family: Tahoma,Arial,sans-serif;
			padding: 1px 1px 1px 1px;
		}

			.DropDownListBoxStyle option:hover {
				font-size: 15px;
				font-family: Tahoma,Arial,sans-serif;
				padding: 1px 1px 1px 1px;
				font-weight: bold;
				background-color: #ddd;
			}

		/*KYC Form CSS*/
		.kYcForm tr > td  {
			
			padding-left: 7px;
		}

			.kYcForm tr > td > span {
				float: left;
				padding-right: 5px;
			}

			.kYcForm tr > td > input {
				float: right;
				padding-right: 3px;
			}


		.modal-header {
			background-color: #f0f0f0;
			text-align: left;
			padding: 0px 0px 10px 5px !important;
			box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
		}

		.modal-title {
			margin: -7px -13px 0px 0 !important;
			color: #5377A9;
			font-family: Arial, Sans-Serif;
			font-weight: bold;
			font-size: 1.5em;
			height: 20px;
			line-height: 1.5em;
		}

		.modal-footer {
			background-color: #f0f0f0;
			padding: 5px 5px !important;
			text-align: center;
			box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
		}

		.modalBackground {
			background-color: Black;
			filter: alpha(opacity=90);
			opacity: 0.8;
		}

		.modal-dialog {
			vertical-align: middle;
			background-color: white;
			border-radius: 5px;
			width: auto;
		}

		.modal-body {
			padding: 5px 5px 0px 5px;
			position: relative;
			overflow: auto;
			max-height: max-content;
		}


		/* Mark input boxes that gets an error on validation: */
		input.invalid {
			background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
			display: none;
		}

		button:hover {
			opacity: 0.8;
		}



		/* Make circles that indicate the steps of the form: */
		.step {
			height: 15px;
			width: 15px;
			margin: 0 2px;
			background-color: #bbbbbb;
			border: none;
			border-radius: 50%;
			display: inline-block;
			opacity: 0.5;
		}

			.step.active {
				opacity: 1;
			}

			/* Mark the steps that are finished and valid: */
			.step.finish {
				background-color: #4CAF50;
			}

		.mainpanelcss {
			border-color: black !important;
			border-width: 2px !important;
		}

		#ctl00_SampleContent_ModalPopupExtender13_foregroundElement {
			z-index: 100000 !important;
		}
	</style>
      

	<script type="text/javascript" src="Style/webcam.min.js"></script>
	<script src="Style/signature_pad.umd.js"></script>    	
	
    <script type="text/javascript">
		
		///------------------------DropDownListBox Extender--------------------//
		function FilterShapes(caller) {
			var textbox = caller;
			filter = textbox.value.toLowerCase();
			var dropDownArray = document.getElementById('<%=DropDownListBox.ClientID%>');
               var list = dropDownArray;
               for (var c = 0; c < list.options.length; c++) {
                   if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
                       list.options.item(c).style.display = "block";
                   }
                   else {
                       list.options.item(c).style.display = "none";
                   }
               }
		}
		///-----------------CheckBox Select Validation-------------------------------//
		
		function ToggleImg(id) {
			if (getElementById(id).style.display == 'block') {
				getElementById(id).style.display = 'none';
			}
			else {
				getElementById(id).style.display = 'block';
			}
        }
        //----------Valid Person Login---------------------//
        function IsValidLoginId(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (!(charCode >= 65 && charCode <= 90) && !(charCode >= 97 && charCode <= 122) && !(charCode >= 48 && charCode <= 57) && !(charCode == 46) && !(charCode == 95)) {
                return false;
            }
            return true;
        }
        //-----------------------End------------------------//
		///-----------------Upload Control Validation--------------------------///
		  var ifIgnoreError = false;
		function UpLoadStartedPDF(sender, e) {
            var fileName = e.get_fileName();
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
			if (fileExtension == 'pdf'|| fileExtension == 'PDF')
			{
                //file is good -- go ahead
            }
            else {
                //stop upload
                ifIgnoreError = true;
                sender._stopLoad();
            }
		}
        function UploadErrorPDF(sender, e) {
			if (ifIgnoreError) {
				alert("Wrong file type!!! Please upload file in PDF format.");
            }
            else {
              alert(e.get_message());
            }
        }        
        function GetWebCam() { 

            // Grab elements, create settings, etc.
            var video = document.getElementById('video');
            
            // Get access to the camera!
            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                // Not adding `{ audio: true }` since we only want video now
                navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    video.srcObject = stream;
                    video.play();
                });
            }

            // Trigger photo take
            document.getElementById("btnScreenshot").addEventListener("click", function () {
                // Elements for taking the snapshot
                var canvas = document.getElementById('canvas');
                var context = canvas.getContext('2d');
                var imgPhoto = document.getElementById('<%=imgPhoto.ClientID%>');

                context.drawImage(video, 0, 0, 200, 150);
				imgPhoto.src = canvas.toDataURL("image/png");
				document.getElementById('<%=hdfimgPhoto.ClientID%>').value = imgPhoto.src;
				//document.getElementById("ctl00_SampleContent_ModalPopupExtender14_foregroundElement").style.display = "none";
				$find('<%= ModalPopupExtender14.ClientID %>').hide();
            });
          
            return false;
        }

        function GetSignaturePad() { 
            var wrapper = document.getElementById("dvSignaturepad");            
            var canvas = wrapper.querySelector("canvas");
            var signaturePad = new SignaturePad(canvas);

            var clearButton = wrapper.querySelector("[data-action=clear]");            
            var undoButton = wrapper.querySelector("[data-action=undo]");
            var savePNGButton = wrapper.querySelector("[data-action=save-png]");
			var imgSignature = document.getElementById('<%=imgSignature.ClientID%>');
			

            clearButton.addEventListener("click", function (event) {
                signaturePad.clear();
            });

            undoButton.addEventListener("click", function (event) {
                var data = signaturePad.toData();
                if (data) {
                    data.pop(); // remove the last dot or line
                    signaturePad.fromData(data);
                }
            });

            savePNGButton.addEventListener("click", function (event) {
                if (signaturePad.isEmpty()) {
                    alert("Please provide a signature first.");
                } else {
                    var dataURL = signaturePad.toDataURL();                    
					imgSignature.src = dataURL; 
					document.getElementById('<%=hdfimgSignature.ClientID%>').value = imgSignature.src;
					//document.getElementById("ctl00_SampleContent_ModalPopupExtender15_foregroundElement").style.display = "none";
					$find('<%= ModalPopupExtender15.ClientID %>').hide();
                }
            });
            
            return false;
		}
		//-------------------CorpDoc/TaxID control onkeypress Event Start------------------------------/
		function SetCorpDoc_onkeypress(event) {
			var chkBoxList = document.getElementById('<%=cblDocProof.ClientID%>');
			var chkBoxCount = chkBoxList.getElementsByTagName("input");
			if (document.getElementById('<%=txtkycPAN.ClientID%>').value.length > 0) {
				chkBoxCount[0].checked = true;
			}
			else {
				chkBoxCount[0].checked = false;
			}
			if (document.getElementById('<%=txtkycGSTNo.ClientID%>').value.length > 0) {
				chkBoxCount[1].checked = true;
			}
			else {
				chkBoxCount[1].checked = false;
			}
		}
		
		function ClearCorpDoc() {
			document.getElementById('<%=txtkycPAN.ClientID%>').value = '';
		}
		function ClearGSTNo() {
			document.getElementById('<%=txtkycGSTNo.ClientID%>').value = '';
		}
	
		//-------------------CorpDoc/TaxID control onkeypress Event End------------------------------/
                      

    </script>
    <div class="demoarea">
		<div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="demoheading">
            Customer
        </div>
    
               
        <asp:UpdatePanel runat="server" ID="MainPanel" UpdateMode="Conditional" ChildrenAsTrigger="false">
            <Triggers>
                <asp:PostBackTrigger ControlID="lstCustomerList" />
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="CustomerLikeButton" ID="CustomerSearchPanel">
                    <table>
                        <tr>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Height="26px" Visible="false" />

                            </td>
                            <td style="padding-top: 5px">
                                <asp:TextBox runat="server" ID="CustomerLike" Width="350px" OnTextChanged="OnTextChanged" autocomplete="off" Height="20px"></asp:TextBox>
                                <ajaxToolkit:DropDownExtender runat="server" TargetControlID="CustomerLike" DropDownControlID="DropDownListPanel" ID="ddeCustomerLike" />

                                <asp:Panel runat="server" ID="DropDownListPanel" >
                                    <asp:ListBox runat="server" ID="DropDownListBox" AppendDataBoundItems="true"
                                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="true"
                                        OnSelectedIndexChanged="OnDropDownSelectedIndexChanged" Rows="20" Width="365px" CssClass="DropDownListBoxStyle"></asp:ListBox>
                                </asp:Panel>
                            </td>
                            <td style="padding-top: 10px">
								
                                <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
                            </td>
                            <td style="padding-left: 10px">
                                <a href="CustomerHistory.aspx" target="_blank" id="HistoryRef" runat="server"><b>Customer History</b></a>
                            </td>
                            <td>
                                <asp:Button runat="server" ID="NewCustomerButton" OnClick="OnNewCustomerClick"
                                    Style="margin-left: 10px; margin-top: 2px" Text="New Customer" CssClass="btn btn-info btn-small" />
                            </td>

                        </tr>
                    </table>
                </asp:Panel>
                <table>
                    <tr>
                        <td style="vertical-align: top;">
							<table>
								<tr>
									<td>
                            <asp:Panel runat="server" ID="CompanyDetailsPanel" BorderStyle="solid" BorderWidth="1px" Style="width: 1005px; padding: 5px 5px 5px 5px; margin-top: 10px;"
                                BorderColor="silver" DefaultButton="companyhiddenbtn" Width="100%" CssClass="mainpanelcss">
                                <asp:Button runat="server" ID="companyhiddenbtn" Style="display: none" Enabled="false" />
                                <div class="headingPanel">
                                    Company Details
                                </div>

                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align: top;" width="100%" rowspan="2">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Customer</legend>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>Company Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyName" runat="server" ControlToValidate="CompanyName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Company Name is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CompanyName" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Short Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyShortName" runat="server" ControlToValidate="ShortName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Short Name is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="ShortName" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address1
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddr1" runat="server" ControlToValidate="CompanyAddress1"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Address1 is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CompanyAddress1" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address2
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CompanyAddress2"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>City
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyCity" runat="server" ControlToValidate="CompanyCity"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="City is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CompanyCity"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellpadding="2px">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="btnCustomerCountry" runat="server" Text="Country" Width="82px" class="btn btn-info"
                                                                                        Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
                                                                                    <asp:RequiredFieldValidator ID="CustomerCountryValidator" runat="server" ControlToValidate="txtCustomerCountry"
                                                                                        ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Customer Country is required"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="padding-top: 2px">
                                                                                    <asp:TextBox ID="txtCustomerCountry" runat="server" Enabled="false"></asp:TextBox>

                                                                                </td>
                                                                            </tr>
                           
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="btnCustomerState" runat="server" Text="State" Width="82px" class="btn btn-info"
                                                                                        Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
																					<asp:RequiredFieldValidator ID="CompUsStateValidator" runat="server" ControlToValidate="txtCustomerState"
																					ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Customer State is required"></asp:RequiredFieldValidator>

                                                                                </td>
                                                                                <td>

                                                                                    <asp:TextBox runat="server" ID="txtCustomerState" Enabled="false"></asp:TextBox>

                                                                                </td>

                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                           
                                                        </td>
                                                        <td>
                                                            <table width="100%" cellpadding="2px">
                                                                <tr>
                                                                    <td width="95px">
                                                                        <asp:Label ID="Label2" Text="ID" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CustomerCode" Enabled="False"></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="CustomerId" Style="display: none"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Start Date
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CustomerStartDate" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 26px;width: 101px;">
                                                                        <asp:Button ID="btnCustomerBusinessType" runat="server" Text="Business Type" Width="90px" class="btn btn-info"
                                                                            Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; padding-right: 0px;"></asp:Button>
																		<asp:RequiredFieldValidator ID="RequiredFieldValidator1BusinessType" runat="server" ControlToValidate="txtCustomerBusinessType"
																		ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Business Type is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCustomerBusinessType" runat="server" Enabled="false"></asp:TextBox>

                                                                    </td>
                                                                </tr>
                                                            
                                                                <tr>
                                                                    <td>Zip
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidatorZip" runat="server" ControlToValidate="CompanyZip1"
                                                                              ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Zip is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                                                            <asp:TextBox runat="server" ID="CompanyZip1" Width="88px" MaxLength="13" TargetControlID="CompanyZip1"></asp:TextBox>
                                                                            <asp:TextBox runat="server" ID="CompanyZip2" Width="32px" MaxLength="5"></asp:TextBox>
                                                                            
                                                                        </asp:Panel>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone
                                                                    </td>
                                                                    <td>
                                                                        <asp:Panel ID="Panel3" runat="server" CssClass="form-inline">
                                                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="CompanyPhoneCode" ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Country Code is required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:TextBox runat="server" ID="CompanyPhoneCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>																			
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom" TargetControlID="CompanyPhoneCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="CompanyPhoneCode" Display="Dynamic" />                                                                            
                                                                            <asp:TextBox runat="server" ID="CompanyPhone" Width="80px" MaxLength="20"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers" TargetControlID="CompanyPhone"></ajaxToolkit:FilteredTextBoxExtender>

                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Fax
                                                                    </td>
                                                                    <td>
                                                                        <asp:Panel ID="Panel4" runat="server" CssClass="form-inline">
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="CompanyFaxCode" ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Country Code is required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:TextBox runat="server" ID="CompanyFaxCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
																			<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom" TargetControlID="CompanyFaxCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="CompanyFaxCode" Display="Dynamic" />
                                                                            <asp:TextBox runat="server" ID="CompanyFax" Width="80px" MaxLength="20"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers" TargetControlID="CompanyFax"></ajaxToolkit:FilteredTextBoxExtender>

                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="CompanyEmail" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                        <td valign="top">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Other Details</legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="center">
                                                            <asp:Button ID="btnCustomerPermissions" runat="server" Text="Permissions" Width="100px" class="btn btn-info"
                                                                Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <asp:Panel ID="dvCustomerPermissions" runat="server" Style="width: 300px; height: 70px;" ViewStateMode="Enabled">
                                                                </asp:Panel>
                                                            </fieldset>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="center">
                                                            <asp:Button ID="btnCompanyCommunication" runat="server" Text="Communication" Width="100px" class="btn btn-info"
                                                                Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <asp:Panel ID="dvCompanyCommunication" runat="server" Style="width: 300px; height: 20px;" ViewStateMode="Enabled">
                                                                </asp:Panel>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="center">
                                                            <asp:Button ID="btnGoodsMovement" runat="server" Text="Delivery Method" Width="100px" class="btn btn-info"
                                                                Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
															
                                                        </td>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <asp:Panel ID="dvGoodsMovement" runat="server" Style="width: 300px; height: 20px;" ViewStateMode="Enabled">
                                                                </asp:Panel>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>

                                        </td>




                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr style="float:right;">
													<td>
													<asp:Label runat="server" ID="MessageLabel" ForeColor="Red" Height="12px" style="float:left;padding-right: 5px;padding-left: 7px;"></asp:Label>
														</td>
												</tr>
                                                <tr>
                                                    <td align="right">
                                                        <div>

                                                            
															
															<input id="btnPopupAddDeliveryMethod" runat="server" type="button" value="" style="visibility:hidden;" />
															<input id="hdnbtnKYC" runat="server" type="button" value="" style="visibility:hidden;" />
															<asp:Button ID="btnKYC" runat="server" Text=" KYC " Width="110px" class="btn btn-info"
																OnClientClick="showTab(0);" Style="margin-right: 2px; margin-top: 5px;" OnClick="btnKYC_OnClick"></asp:Button>
															 <asp:Button runat="server" ID="btnPopupAddDeliveryMethodClick" OnClick="btnPopupAddDeliveryMethod_OnClick"
                                                                Style="margin-right: 2px; margin-top: 5px;padding-left:5px!important;padding-right:5px !important;" Text="Add Delivery Method" CssClass="btn btn-info" CausesValidation="False" />
															
                                                            <asp:Button runat="server" ID="CustomerUpdateBtn" OnClick="OnUpdateCompanyClick"
                                                                Style="margin-right: 2px; margin-top: 5px;padding-left:5px!important;padding-right:5px !important;" Text="Update Customer" CssClass="btn btn-info" />
                                                        </div>
													
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
								</td>
							</tr>
								<tr>
									<td style="padding-top: 7px">

                            <asp:Panel runat="server" ID="CarrierDetailsPanel" Width="1006px" BorderStyle="Solid" BorderWidth="1px" BorderColor="Silver" DefaultButton="CarrierHiddenBtn" Style="padding: 5px 5px 5px 5px; margin-top: 5px;"
								CssClass="mainpanelcss">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="headingPanel">
                                                Carrier Details
                                                                       
                                            </div>

                                            <asp:Button runat="server" ID="CarrierHiddenBtn" Style="display: none" Enabled="False" />
											<asp:ValidationSummary ID="SumGrpCarrier" runat="server" ValidationGroup="ValGrpCarrier" />
                                            <table style="width: 100%;">

                                                <tr>
                                                    <td style="padding-left: 10px; padding-top: 0px;">
                                                        <asp:Button ID="btnCarrier" runat="server" Text="Carrier" Width="82px" class="btn btn-info"
                                                            Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
														<asp:RequiredFieldValidator ID="CarrierValidator" runat="server" ControlToValidate="txtCarrierList"
													ErrorMessage="" Text="*" ValidationGroup="ValGrpCarrier" ToolTip="Carrier Require"></asp:RequiredFieldValidator>

                                                    </td>
                                                    <td style="padding-left: 0px; padding-top: 0px;">
                                                        <asp:TextBox ID="txtCarrierList" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="padding-left: 10px; padding-top: 0px;">LowerLimit

                                                    </td>
                                                    <td style="padding-left: 10px; padding-top: 0px;">
                                                        <asp:TextBox ID="LowerLimit" runat="server" MaxLength="8" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="padding-left: 10px; vertical-align: bottom;padding-bottom: 5px;">
                                                        <div class="checkboxSingleCarier" style="height: 26px; float: left;">
															
                                                            <asp:CheckBox runat="server" ID="UseAccountFlag" Style="padding-left: 7px; margin: 0px 0px 4px 0 !important; font-size: smaller; width: max-content;"
                                                                Font-Size="10px" Text="We use their account to ship" OnCheckedChanged="OnUseAccountChecked"
                                                                AutoPostBack="True" />
                                                        </div>
                                                    </td>
                                                    <td>
														<asp:Label ID="UpdateCarrierLabel" runat="server" ForeColor="Blue" Width="250px" style="text-align: right;"></asp:Label>
													</td>

                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle; padding-top: 0px; text-align: center; width: 102px;">Account #
                                                            <asp:RequiredFieldValidator ID="AccountValidator" runat="server" ControlToValidate="txtCarrierAccount"
                                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpCarrier" ToolTip="Account is required"></asp:RequiredFieldValidator></td>


                                                    <td style="padding-top: 0px">
                                                        <asp:TextBox runat="server" ID="txtCarrierAccount" Width="150px" MaxLength="250"></asp:TextBox>

                                                    </td>
                                                    <td style="padding-left: 10px; padding-top: 0px;">UpperLimit</td>
                                                    <td style="padding-top: 0px; padding-left: 10px;">
                                                        <asp:TextBox runat="server" ID="UpperLimit" Width="150px" MaxLength="8"></asp:TextBox>
                                                    </td>
                                                    <td align="right" colspan="2">
                                                      <div align="right" style="margin-right: 2px; margin-bottom: 3px;">
														    <asp:Button runat="server" ID="btnPopupAddShippingCourier" CausesValidation="false"
                                                                Style="margin-right: 0px; margin-top: 0px;" Text="Add Shipping Currier" CssClass="btn btn-info" />
                                                            <asp:Button runat="server" ID="CarrierUpdateBtn" Style="margin-left: 0px; margin-bottom: 0px"
                                                                OnClick="OnUpdateCarrierClick" Text="Update Carrier" CssClass="btn btn-info" />
                                                            <asp:Button runat="server" ID="CarrierDeleteBtn" Style="margin-left: 0px; margin-bottom: 0px"
                                                                OnClick="OnDeleteCarrierClick" Text="Delete Carrier" CssClass="btn btn-info" />
                                                        </div></td>
                                                    
                                                </tr>
                                            </table>

                                        </td>

                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
								</tr>
								<tr>
									<td style="padding-top: 7px">

                            <asp:Panel runat="server" ID="PersonDetailsPanel" Width="1007px" BorderStyle="Solid" BorderWidth="1px" BorderColor="Silver" 
								DefaultButton="PersonHiddenBtn" Style="padding: 5px 5px 5px 5px; margin-top: 5px;" CssClass="mainpanelcss">

                                <asp:Button runat="server" ID="PersonHiddenBtn" Style="display: none" Enabled="False" />
                                <asp:ValidationSummary ID="SumGrpPerson" runat="server" ValidationGroup="ValGrpPerson" />
                                <div class="headingPanel">
                                    Person Details
                                    
                                    
                                </div>
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Person</legend>
                                                <table>
                                                    <tr>
                                                        <td style="vertical-align: top">
                                                            <table cellpadding="2px">
                                                                <tr>
                                                                    <td style="width: 85px;">First Name
                                                        <asp:RequiredFieldValidator ID="ValFirstName" runat="server" ControlToValidate="PersonFirstName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person First Name is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonFirstName" MaxLength="250"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Last Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="PersonLastName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person Last Name is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonLastName" MaxLength="250"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Birth Date</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonBirthDate" OnTextChanged="OnChangedBirthDate" AutoPostBack="True">
                                                                        </asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExt" runat="server" ClearTime="True"
                                                                            TargetControlID="PersonBirthDate">
                                                                        </ajaxToolkit:CalendarExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address1</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonAddress1"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address2</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonAddress2"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>City</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonCity"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>

                                                                    <td colspan="2">
                                                                        <asp:Panel ID="PersonStatePanel" runat="server" CssClass="form-inline" DefaultButton="PersonCountryBtn">
                                                                            <table width="100%" border="0" cellpadding="2px">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnPersonCountry" runat="server" Text="Country" Width="72px" class="btn btn-info"
                                                                                            Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
																						<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPersonCountry"
																						ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person Country is required"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td style="padding-top: 2px" width="30px">
                                                                                        <asp:TextBox ID="txtPersonCountry" runat="server" Enabled="false"></asp:TextBox>

                                                                                    </td>

                                                                                </tr>
                                                                          
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnPersonState" runat="server" Text="State" Width="72px" class="btn btn-info"
                                                                                            Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
																						<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPersonState"
																							ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person State is required "></asp:RequiredFieldValidator>
                                                                                        
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtPersonState" runat="server" Enabled="false"></asp:TextBox>

                                                                                        </td>
                                                                                </tr>
                                                                            </table>
                                                                            <asp:Button runat="server" ID="PersonCountryBtn" Style="display: none" />
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2">
                                                                        <div class="checkboxSingle">
                                                                            <asp:CheckBox runat="server" ID="AsCompanyAddress" Text="Same as Company Address" Style="margin: 0px 0px 4px 0 !important;"
                                                                                OnCheckedChanged="OnAsCompanyChecked" AutoPostBack="True" />
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                        <td style="vertical-align: top">
                                                           
                                                            <table cellpadding="2px">
                                                              
                                                                <tr>
                                                                    <td>Start Date</td>
                                                                    <td width="155">
                                                                        <asp:TextBox runat="server" ID="PersonStartDate" Enabled="False" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>

                                                                        <asp:Button ID="btnPersonPosition" runat="server" Text="Position" Width="100px" class="btn btn-info"
                                                                            Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPosition" runat="server" ControlToValidate="txtPersonPosition"
                                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Position is required"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPersonPosition" runat="server" Enabled="false"></asp:TextBox>

                                                                    </td>
                                                                </tr>
																   <tr>
                                                                    <td>Zip</td>
                                                                    <td>
                                                                        <asp:Panel ID="PersonZipPanel" runat="server" CssClass="form-inline">
                                                                            <asp:TextBox runat="server" ID="PersonZip1" Width="88px" MaxLength="13"></asp:TextBox>
                                                                            <asp:TextBox runat="server" ID="PersonZip2" Width="32px" MaxLength="5"></asp:TextBox>
                                                                            
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
																     <tr>
                                                                    <td>Cell</td>
                                                                    <td>
                                                                        <asp:Panel ID="PersonCellPanel" runat="server" CssClass="form-inline">
                                                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="PersonCellCode" ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Country Code is required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:TextBox runat="server" ID="PersonCellCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
																			<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom" TargetControlID="PersonCellCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="PersonCellCode" Display="Dynamic" />
                                                                            <asp:TextBox runat="server" ID="PersonCell" Width="80px" MaxLength="20"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers" TargetControlID="PersonCell"></ajaxToolkit:FilteredTextBoxExtender>

                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone</td>
                                                                    <td>
                                                                        <asp:Panel ID="PersonPhonePanel" runat="server" CssClass="form-inline">
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="PersonPhoneCode" ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Country Code is required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:TextBox runat="server" ID="PersonPhoneCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
																			<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom" TargetControlID="PersonPhoneCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="PersonPhoneCode" Display="Dynamic" />
                                                                            <asp:TextBox runat="server" ID="PersonPhone" Width="80px" MaxLength="20"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers" TargetControlID="PersonPhone"></ajaxToolkit:FilteredTextBoxExtender>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Ext</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonPhoneExt" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers" TargetControlID="PersonPhoneExt"></ajaxToolkit:FilteredTextBoxExtender>

                                                                    </td>
                                                                </tr>
                                                           
                                                                <tr>
                                                                    <td>Fax</td>
                                                                    <td>
                                                                        <asp:Panel ID="PersonFaxPanel" runat="server" CssClass="form-inline">
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="PersonFaxCode" ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Country Code is required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:TextBox runat="server" ID="PersonFaxCode" Width="30px" MaxLength="4" Enabled="false"></asp:TextBox>
																			<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom" TargetControlID="PersonFaxCode" ValidChars="+0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[+][0-9]{1,3}$" ErrorMessage="Enter Country Code as +XXX" ControlToValidate="PersonFaxCode" Display="Dynamic" />
                                                                            <asp:TextBox runat="server" ID="PersonFax" Width="80px" MaxLength="20"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers" TargetControlID="PersonFax"></ajaxToolkit:FilteredTextBoxExtender>

                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="PersonEmail" MaxLength="50"></asp:TextBox>

                                                                    </td>
                                                                </tr>
																  <tr style="visibility:hidden;">
                                                                    <td>ID</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtPersonCode" Enabled="False"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnPersonID" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </fieldset>
                                           
                                        </td>
                                        <td>

                                            <td valign="top" align="left">
                                                <table>
                                                    <tr>
                                                        <td colspan="3">
                                                            <fieldset class="filedSetBorder">
                                                                <legend class="label">Other Details</legend>
                                                                <table>
																	 <tr>
                                                                        <td valign="center">
                                                                            <asp:Button ID="btnPersonService" runat="server" Text="Services" Width="110px" class="btn btn-info"
                                                                                Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
                                                                        </td>
                                                                        <td>
                                                                            <fieldset class="filedSetBorder">
                                                                                <asp:Panel ID="dvPersonService" runat="server" Style="width: 300px; height: 40px;" ScrollBars="Auto" ViewStateMode="Enabled">
                                                                                </asp:Panel>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="center">
                                                                            <asp:Button ID="btnPersonPermission" runat="server" Text="Permissions" Width="110px" class="btn btn-info"
                                                                                Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
                                                                        </td>
                                                                        <td>
                                                                            <fieldset class="filedSetBorder">
                                                                                <asp:Panel ID="dvPersonPermission" runat="server" Style="width: 300px; height: 40px;" ScrollBars="Auto" ViewStateMode="Enabled">
                                                                                </asp:Panel>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="center">
                                                                            <asp:Button ID="btnPersonMethod" runat="server" Text="Communication" Width="110px" class="btn btn-info"
                                                                                Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"></asp:Button>
                                                                        </td>
                                                                        <td>
                                                                            <fieldset class="filedSetBorder">
                                                                                <asp:Panel ID="dvPersonMethod" runat="server" Style="width: 300px; height: 20px;" ViewStateMode="Enabled">
                                                                                </asp:Panel>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
																	
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <legend class="label">Web</legend>
                                                                <table style="margin-top: 10px;">
                                                                    <tr>
                                                                        <td>Login </td>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorWebLogin" runat="server" ControlToValidate="WebLogin" ErrorMessage="" Text="*" ToolTip="Web login is required" ValidationGroup="ValGrpPerson"></asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[0-9A-Za-z._]+$" ErrorMessage="" Text="*" ToolTip="Enter Valid Login" ControlToValidate="WebLogin" ValidationGroup="ValGrpPerson" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="WebLogin" runat="server" Style="width: 90px;" MaxLength="50" onkeypress="return IsValidLoginId(event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Password </td>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ControlToValidate="WebPassword" ErrorMessage="" Text="*" ToolTip="Web password is required" ValidationGroup="ValGrpPerson"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="WebPassword" runat="server" Style="width: 90px;"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Re-type
																			<br />
                                                                            Password </td>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRePassword" runat="server" ControlToValidate="RePassword" ErrorMessage="" Text="*" ToolTip="Re-type password is required" ValidationGroup="ValGrpPerson"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="RePassword" runat="server" Style="width: 90px;"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <legend class="label">Signature</legend>
																<asp:HiddenField runat="server" ID="hdfimgSignature" />
																
																<img id="imgSignature" runat="server" src="" style="margin-left: 3px; margin-right: 3px; margin-top: 0px; position: relative; width: 100px; height: 75px;" />
																
																<div style="text-align: center;">
																	<asp:Button ID="btnCaptureSignature" runat="server" Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;"
																		Text="Capture Sign" OnClientClick="GetSignaturePad();" CssClass="btn btn-info" />
                        </div>
                                                                
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset class="filedSetBorder">
                                                                <legend class="label">Picture</legend>
																<asp:HiddenField runat="server" ID="hdfimgPhoto" />
																<img id="imgPhoto" runat="server" src="" style="margin-left: 3px; margin-right: 3px; margin-top: 0px; position: relative; width: 100px; height: 75px;" />
																<div style="text-align: center;">
																	<asp:Button ID="btnCapturePhoto" runat="server" Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;" Text="Capture Photo" OnClientClick="GetWebCam();" CssClass="btn btn-info" />
																</div>
                                                               
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </td>


                                    </tr>
									 <tr>
										 <td colspan="3">
											 <table style="width:100%;">
												 <tr>
													 <td>
											
												  <table >

													  <tr>
														  <td>
                                                
												
                                                
												</td><td>
												 												
												<asp:Button ID="btnAuthorizationForm" runat="server" Text="Authorization Form"  class="btn btn-info" OnClick="btnAuthorizationForm_OnClick" />
													 &nbsp;&nbsp;&nbsp;&nbsp;<input id="hdnbtnAuthorizationForm" runat="server" type="button" value="" style="visibility:hidden;" />

												     </td><td>
											</td><td>
												<%--<asp:Button ID="btnUploadCompanyLatterhead" runat="server" Text="Upload" class="btn btn-info"
                                              Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88;" ></asp:Button>--%>
												  </td>
													  </tr>
												  </table>
                                           <%-- </fieldset>--%>
														 </td>
											  <td colspan="3" align="right" style="vertical-align: bottom; padding-left: 0px;">
                                                              <asp:Label ID="UpdatePersonLabel" runat="server" ForeColor="Red"></asp:Label>
                                                              <asp:Button ID="PersonUpdateBtn" runat="server" CssClass="btn btn-info" OnClick="OnUpdatePersonClick" Style="margin-left: 0px;" Text="Update Person" />


                                                            <asp:Button ID="PersonDeleteBtn" runat="server" CssClass="btn btn-info" OnClick="OnDeletePersonClick" Style="margin-left: 0px;" Text="Delete Person" />
                                                        <%--<td align="right" style="vertical-align: bottom; padding-left: 0px; width: 100%;">

                                                          

                                                        </td>--%>
														 </td>
												 </tr>
											 </table>
											 </td>
                                                    </tr>
                                </table>
                           </asp:Panel>
                        </td>
								</tr>
							</table>
                        </td>
                        <td style="vertical-align: top; padding-left: 10px">
                          
                            <asp:Panel runat="server" ID="PersonListPanel">
                                <div>
                                    <asp:Button runat="server" ID="NewPersonButton" OnClick="OnAddPersonClick" Style="margin-left: 0px; margin-bottom: 5px"
                                        Text="New Person" CssClass="btn btn-info" />
                                    <asp:Label runat="server" ID="PersonCountLabel" Style="padding-left: 20px"></asp:Label>
                                </div>
                                <div>
                                    <asp:DataGrid runat="server" ID="PersonsGrid" AutoGenerateColumns="False" CellPadding="3" CellSpacing="5"
                                        DataKeyField="PersonId" OnItemDataBound="OnPersonItemDataBound" OnItemCommand="OnViewPersonCommand">
                                        <Columns>
                                            <asp:ButtonColumn Text="View" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/search16.png" ItemStyle-CssClass="btn btn-info" ItemStyle-ForeColor="White" ItemStyle-BorderWidth="1" />
                                            <asp:BoundColumn DataField="PersonId" HeaderText="PersonId" Visible="False" />
                                            <asp:BoundColumn DataField="FullName" HeaderText="Name" />
                                            <asp:BoundColumn DataField="DisplayPosition" HeaderText="Position" />
                                            <asp:BoundColumn DataField="DisplayPhone" HeaderText="Phone" />
                                            <asp:BoundColumn DataField="DisplayFax" HeaderText="Fax" />
                                            <asp:BoundColumn DataField="DisplayCell" HeaderText="Cell" />
                                            <asp:BoundColumn DataField="DisplayEmail" HeaderText="Email" />
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" BackColor="White" />
                                    </asp:DataGrid>
                                </div>



                                <br />
                                <div>
                                    <asp:Button runat="server" ID="NewCarrierButton" OnClick="OnAddCarrierClick" Style="margin-left: 0px; margin-bottom: 5px"
                                        Text="New Carrier" CssClass="btn btn-info" />
                                </div>
                                <div>
                                    <asp:DataGrid runat="server" ID="CarrierGrid" AutoGenerateColumns="False" CellPadding="3"
                                        DataKeyField="CarrierID" OnItemCommand="OnViewCarrierCommand">
                                        <Columns>
                                            <asp:ButtonColumn Text="View" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/search16.png" ItemStyle-CssClass="btn btn-info" ItemStyle-ForeColor="White" ItemStyle-BorderWidth="1" />
                                            <asp:BoundColumn DataField="CarrierID" HeaderText="CarrierID" Visible="False" />
                                            <asp:BoundColumn DataField="CarrierName" HeaderText="Name" />
                                            <asp:BoundColumn DataField="Account" HeaderText="Account" />
                                            <asp:BoundColumn DataField="LowerLimit" HeaderText="LowerLimit" />
                                            <asp:BoundColumn DataField="UpperLimit" HeaderText="UpperLimit" />
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" BackColor="White" />
                                    </asp:DataGrid>
                                </div>
                            </asp:Panel>
                           
                        </td>
                    </tr>
                </table>
				
                <%-- Customer Country Dialog --%>
                <asp:Panel runat="server" ID="pnlCustomerCountry" CssClass="modal-dialog" Width="800px" 
					Style="overflow-y:scroll;max-height:-webkit-fill-available;background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label11" Text="Country"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCustomerCountry" runat="server" DataTextField="CountryCode" DataValueField="CountryID"
                                    Width="150px" Height="25px" OnSelectedIndexChanged="ddlCustomerCountry_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Repeater ID="repCustomerCountry" runat="server" OnItemCommand="repCustomerCountry_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnCustomerCountry" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px; width: auto;"
                                            CommandName="CountryID"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CountryID")+","+ DataBinder.Eval(Container.DataItem,"PhoneCode")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "CountryCode")%>' 
											CausesValidation="false"/>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="pnlCustomerCountry" TargetControlID="btnCustomerCountry" CancelControlID="ctl00_SampleContent_ModalPopupExtender1_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Customer State Dialog --%>
                <asp:Panel runat="server" ID="pnlCustomerState" CssClass="modal-dialog" Width="800px" 
					Style="background-color: white;overflow-y:scroll;max-height:-webkit-fill-available;min-height:max-content; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label134" Text="State"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCustomerState" runat="server" DataTextField="StateCode" DataValueField="StateId"
                                    Width="150px" Height="25px" AutoPostBack="True" OnSelectedIndexChanged="CompanyUsState_SelectedIndexChanged">
                                </asp:DropDownList>
                                
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repCustomerState" runat="server" OnItemCommand="repCustomerState_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnCustomerState" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="StateId"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StateId")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "StateCode")%>' 
											CausesValidation="false"/>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="pnlCustomerState" TargetControlID="btnCustomerState" CancelControlID="ctl00_SampleContent_ModalPopupExtender2_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Customer Business Type Dialog --%>
                <asp:Panel runat="server" ID="pnlCustomerBusinessType" CssClass="modal-dialog" Width="460px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label4" Text="Business Type"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCustomerBusinessType" runat="server" DataTextField="BusinessTypeName"
                                    DataValueField="BusinessTypeId" Width="150px" Height="26px">
                                </asp:DropDownList>
                                
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repCustomerBusinessType" runat="server" OnItemCommand="repCustomerBusinessType_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnCustomerBusinessType" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="BusinessTypeId"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BusinessTypeId")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "BusinessTypeName")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlCustomerBusinessType" TargetControlID="btnCustomerBusinessType" CancelControlID="ctl00_SampleContent_ModalPopupExtender3_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Person Country Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonCountry" CssClass="modal-dialog" Width="800px" 
					Style="overflow-y:scroll;max-height:-webkit-fill-available;background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label3" Text="Country"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPersonCountry" runat="server" DataTextField="CountryCode" DataValueField="CountryID"
                                    Width="150px" Height="25px" OnSelectedIndexChanged="ddlPersonCountry_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repPersonCountry" runat="server" OnItemCommand="repPersonCountry_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnPersonCountry" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="CountryID"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CountryID")+","+ DataBinder.Eval(Container.DataItem,"PhoneCode")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "CountryCode")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="pnlPersonCountry" TargetControlID="btnPersonCountry" CancelControlID="ctl00_SampleContent_ModalPopupExtender4_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>
                <%-- Person State Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonState" CssClass="modal-dialog" Width="800px" 
					Style="background-color: white;overflow-y:scroll;max-height:-webkit-fill-available;min-height:max-content; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label5" Text="Country"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPersonState" runat="server" DataTextField="StateCode" DataValueField="StateId"
                                    Width="150px" Height="25px" OnSelectedIndexChanged="ddlPersonState_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repPersonState" runat="server" OnItemCommand="repPersonState_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnPersonState" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="StateId"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StateId")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "StateCode")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender5" runat="server" PopupControlID="pnlPersonState" TargetControlID="btnPersonState" CancelControlID="ctl00_SampleContent_ModalPopupExtender5_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Person Position Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonPosition" CssClass="modal-dialog" Width="460px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label6" Text="Position"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPersonPosition" runat="server" DataTextField="Name" DataValueField="Id"
                                    Height="26px" Width="150px" OnSelectedIndexChanged="ddlPersonPosition_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repPersonPosition" runat="server" OnItemCommand="repPersonPosition_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnPersonState" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="Id"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "Name")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender6" runat="server" PopupControlID="pnlPersonPosition" TargetControlID="btnPersonPosition" CancelControlID="ctl00_SampleContent_ModalPopupExtender6_backgroundElement"
                    Enabled="True" DynamicServicePath="">
                </ajaxToolkit:ModalPopupExtender>
                <%-- Person Position Dialog --%>
                <asp:Panel runat="server" ID="pnlCarrier" CssClass="modal-dialog" Width="460px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label7" Text="Carrier"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCarrierList" runat="server" DataTextField="Name" DataValueField="Id"
                                    Height="23px" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlCarrierList_SelectedIndexChanged">
                                </asp:DropDownList>
                                
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:Repeater ID="repCarrier" runat="server" OnItemCommand="repCarrier_ItemCommand">
                                    <ItemTemplate>
                                        <asp:Button ID="btnCarrier" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                            CommandName="Id"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                            Text='<%# DataBinder.Eval(Container.DataItem, "Name")%>'
                                            CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender7" runat="server" PopupControlID="pnlCarrier" TargetControlID="btnCarrier" CancelControlID="ctl00_SampleContent_ModalPopupExtender7_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                <%--Customer Permission Dialog --%>
                <asp:Panel runat="server" ID="pnlCustomerPermission" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label8" Text="Permissions" Font-Bold="true"></asp:Label>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:CheckBoxList ID="CompanyPermissionsList" runat="server" RepeatDirection="Horizontal"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="3" DataTextField="Name"
                                    DataValueField="Id" CssClass="radioButtonList">
                                </asp:CheckBoxList>
                                <asp:Button ID="btnCompanyPermissionsBack" runat="server" Text="Set" CssClass="btn btn-info btn-large" Style="margin-left: 5px; margin-top: 5px; float: right;" OnClick="btnSetCompanyPermissions_Click" />
                            </td>

                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender8" runat="server" PopupControlID="pnlCustomerPermission" TargetControlID="btnCustomerPermissions" CancelControlID="ctl00_SampleContent_ModalPopupExtender8_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <%--Customer Communication Dialog --%>
                <asp:Panel runat="server" ID="pnlCompanyCommunication" CssClass="modal-dialog" Width="200px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label9" Text="Communication" Font-Bold="true"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:TreeView ID="CompanyMethods" runat="server" ShowCheckBoxes="All" Width="80px"
                                    ExpandDepth="0" AfterClientCheck="CheckChildNodes();" PopulateNodesFromClient="true"
                                    ShowLines="false" ShowExpandCollapse="false" OnTreeNodeCheckChanged="OnCompanyMethodsCheckChanged"
                                    onclick="OnTreeClick(event)" CssClass="CommunicationCheckbox">
                                    <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                    <NodeStyle CssClass="nodeStyle" Height="30px" />
                                    <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                </asp:TreeView>
                                <asp:Button ID="btnCompanyMethodsBack" runat="server" Text="Set" CssClass="btn btn-info btn-large" Style="margin-left: 5px; margin-top: 5px; float: right;" OnClick="btnSetCompanyMethods_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender9" runat="server" PopupControlID="pnlCompanyCommunication" TargetControlID="btnCompanyCommunication" CancelControlID="ctl00_SampleContent_ModalPopupExtender9_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <%--Customer GoodsMovement Dialog --%>
                <asp:Panel runat="server" ID="pnlGoodsMovement" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label10" Text="Delivery Method" Font-Bold="true"> </asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMovements" runat="server" ControlToValidate="HiddenMovements"
                                    ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="One of the delivery method methods must be selected">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:TextBox runat="server" Style="display: none" ID="HiddenMovements"></asp:TextBox>
                                <asp:RadioButtonList ID="rblMovementsList" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnCheckedMovement"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="3" Font-Size="X-Small" DataValueField="DeliveryMethodCode"
                                    DataTextField="DeliveryMethodName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender10" runat="server" PopupControlID="pnlGoodsMovement" TargetControlID="btnGoodsMovement" CancelControlID="ctl00_SampleContent_ModalPopupExtender10_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                <%--Person Permission Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonPermission" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label12" Text="Permissions" Font-Bold="true"></asp:Label>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBoxList ID="cblPersonPermissionsList" runat="server" RepeatDirection="Horizontal"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="3" DataTextField="Name"
                                    DataValueField="Id" CssClass="radioButtonList">
                                </asp:CheckBoxList>
                                <asp:Button ID="btnPersonPermissionsBack" runat="server" Text="Set" CssClass="btn btn-info btn-large" Style="margin-left: 5px; margin-top: 5px; float: right;" OnClick="btnSetPersonPermissions_Click" />
                            </td>

                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender11" runat="server" PopupControlID="pnlPersonPermission" TargetControlID="btnPersonPermission" CancelControlID="ctl00_SampleContent_ModalPopupExtender11_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
				
                <%--Person Communication Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonMethods" CssClass="modal-dialog" Width="200px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label13" Text="Communication" Font-Bold="true"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2"><%--width:100px;--%>
                                <asp:TreeView ID="PersonMethods" runat="server" ShowCheckBoxes="All" Width="110px"
                                    ExpandDepth="0" AfterClientCheck="CheckChildNodes();" PopulateNodesFromClient="true"
                                    ShowLines="false" ShowExpandCollapse="false" OnTreeNodeCheckChanged="OnPersonMethodsCheckChanged"
                                    onclick="OnTreeClick(event)" CssClass="CommunicationCheckbox">
                                    <NodeStyle CssClass="nodeStyle" Height="10px" />
                                    <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                    <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                </asp:TreeView>
                                <asp:Button ID="btnPersonMethodsBack" runat="server" Text="Set" CssClass="btn btn-info btn-large" Style="margin-left: 5px; margin-top: 5px; float: right;" OnClick="btnSetPersonMethods_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender12" runat="server" PopupControlID="pnlPersonMethods" TargetControlID="btnPersonMethod" CancelControlID="ctl00_SampleContent_ModalPopupExtender12_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
				  <%--Person Service Dialog --%>
                <asp:Panel runat="server" ID="pnlPersonService" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label55" Text="Services" Font-Bold="true"></asp:Label>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBoxList ID="cblPersonServicesList" runat="server" RepeatDirection="Horizontal"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="3" DataTextField="ServiceName"
                                    DataValueField="ServiceID" CssClass="radioButtonList">
                                </asp:CheckBoxList>
                                <asp:Button ID="btnSetPersonService" runat="server" Text="Set" CssClass="btn btn-info btn-large" Style="margin-left: 5px; margin-top: 5px; float: right;" OnClick="btnSetPersonService_Click" />
                            </td>

                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="mpePersonService" runat="server" PopupControlID="pnlPersonService" TargetControlID="btnPersonService" CancelControlID="ctl00_SampleContent_mpePersonService_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
				 	<%--Delivery Method Dialog --%>
				<asp:Panel runat="server" ID="pnlPopupAddDeliveryMethod" CssClass="modal-dialog" 
					Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">

					<div class="modal-content">
						<div class="modal-header">
						<asp:Button Style="float: right" ID="Button1" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" 
							OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender17_foregroundElement').style.display = 'none';" />
								<asp:Label ID="Label45" runat="server" Text="Delivery Method" Style="float: none; padding-top: 5px;" Font-Bold="true" Font-Size="15px"></asp:Label>
						</div>
						<div class="modal-body">
							<table border="0" style="width: 470px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label47" runat="server" Text="Delivery Method"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtDeliveryMethodName" runat="server" Width="250px" placeholder="Enter Delivery Method"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnDeliveryMethodId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtDeliveryMethodName" ValidationGroup="vgDeliveryMethod"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddDeliveryMethod" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddDeliveryMethod_Click" ValidationGroup="vgDeliveryMethod" />
                                                    <asp:Button ID="btnClearDeliveryMethod" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearDeliveryMethod_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblDeliveryMethodMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvDeliveryMethod" runat="server" DataKeyNames="DeliveryMethodId" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingDeliveryMethod" Width="470px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditDeliveryMethod" CssClass="btn btn-info" style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeliveryMethod Name" SortExpression="DeliveryMethodName">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeliveryMethodName" runat="server" Text='<%#Eval("DeliveryMethodName")%>' CausesValidation="false" ></asp:Label>
                                                                    <asp:HiddenField ID="hdnDeliveryMethodID" runat="server" Value='<%#Eval("DeliveryMethodId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        </table>
							</div>
						<div class="modal-footer"><br /></div>
					</div>
				</asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modelAddDeliveryMethod" runat="server" PopupControlID="pnlPopupAddDeliveryMethod" 
					TargetControlID="btnPopupAddDeliveryMethod" CancelControlID="ctl00_SampleContent_modelAddDeliveryMethod_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

				 <%--Shipping Courier Dialog --%>
				<asp:Panel runat="server" ID="pnlAddShippingCourier" CssClass="modal-dialog" 
					Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">

					<div class="modal-content">
						<div class="modal-header">
															<asp:Button Style="float: right" ID="Button2" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender14_foregroundElement').style.display = 'none';" />
								<asp:Label ID="Label46" runat="server" Text="Shipping Courier" Style="float: none; padding-top: 5px;" Font-Bold="true" Font-Size="15px"></asp:Label>
						</div>
						<div class="modal-body">

							<table border="0" style="width: 425px;">
								<tr>

									<td style="text-align: left; padding-top: 15px;">
										<div>
											<asp:Label ID="Label21" runat="server" Text="Shipping Courier"></asp:Label>
										</div>
										<asp:TextBox ID="txtShippingCourierName" runat="server" Width="200px" placeholder="Enter Shipping Courier"></asp:TextBox>
										<asp:HiddenField ID="hdnShippingCourierId" runat="server" Value="0"></asp:HiddenField>

										<asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Required"
											ControlToValidate="txtShippingCourierName" ValidationGroup="vgShippingCourier"></asp:RequiredFieldValidator>

										<asp:Button ID="btnAddShippingCourier" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddShippingCourier_Click" ValidationGroup="vgShippingCourier" />
										<asp:Button ID="btnClearShippingCourier" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearShippingCourier_Click" />


									</td>
								</tr>

								<tr>
									<td style="text-align: left; height: 15px;" colspan="1">
										<asp:Label ID="lblShippingCourierMsg" runat="server" ForeColor="Blue"></asp:Label>
									</td>
								</tr>
								<tr>
									<td>
										<asp:GridView ID="gvShippingCourier" runat="server" DataKeyNames="CarrierID" AutoGenerateColumns="False"
											AllowSorting="True" OnSorting="OnSortingCarrierName" Width="420px" CellPadding="4" ForeColor="#333333">
											<AlternatingRowStyle BackColor="White" />

											<Columns>
												<asp:TemplateField ItemStyle-Width="50px">
													<ItemTemplate>
														<asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditShippingCourier" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="ShippingCourier Name" SortExpression="CarrierName">
													<ItemTemplate>
														<asp:label ID="lblShippingCourierName" runat="server" Text='<%#Eval("CarrierName")%>' CausesValidation="false" ></asp:label>
														<asp:HiddenField ID="hdnShippingCourierID" runat="server" Value='<%#Eval("CarrierID")%>' />
													</ItemTemplate>
												</asp:TemplateField>


											</Columns>

											<EditRowStyle BackColor="#2461BF" />
											<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
											<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
											<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
											<RowStyle BackColor="#EFF3FB" />
											<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
											<SortedAscendingCellStyle BackColor="#F5F7FB" />
											<SortedAscendingHeaderStyle BackColor="#6D95E1" />
											<SortedDescendingCellStyle BackColor="#E9EBEF" />
											<SortedDescendingHeaderStyle BackColor="#4870BE" />
										</asp:GridView>
									</td>
								</tr>
							</table>

						</div>
						<div class="modal-footer"><br /></div>
					</div>
				</asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modelAddShippingCourier" runat="server" PopupControlID="pnlAddShippingCourier" 
					TargetControlID="btnPopupAddShippingCourier" CancelControlID="ctl00_SampleContent_modelAddShippingCourier_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

				

                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none; border: solid 2px lightgray;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>

                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                    PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton">
                </ajaxToolkit:ModalPopupExtender>
				
				<%----------------------Image Caprure  Start----------------------%>
				<asp:Panel runat="server" ID="pnlCapture" CssClass="modal-dialog"
					Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
					<div class="modal-content">
						<div class="modal-header" style="padding: 7px 14px !important;">
							
							<div class="modal-title" id="exampleModalLabel2">
								<asp:Button Style="float: right;" ID="Button5" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender14_foregroundElement').style.display = 'none';" />
								<asp:Label ID="Label22" runat="server" Text="Image Capture" Style="float: none; padding-top: 5px;"></asp:Label>
							</div>
						</div>
						<div class="modal-body">

							
							<div id="webcam">
								<video id="video" width="400" height="300" autoplay></video>
								<canvas id="canvas" width="200" height="150" style="display: none;"></canvas>
							</div>
							<div align="center">
								<button id="btnScreenshot" type="button" class="button save">Take Screenshot</button>
							</div>
							

						</div>
						<div class="modal-footer">
							<br />
						</div>
						</div>
				</asp:Panel>
			
				<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender14" runat="server" PopupControlID="pnlCapture" TargetControlID="btnCapturePhoto" CancelControlID="ctl00_SampleContent_ModalPopupExtender14_backgroundElement"
					Enabled="True" DynamicServicePath="" DropShadow="true"  >
				</ajaxToolkit:ModalPopupExtender>

				<%----------------------Signature Caprure  Start----------------------%>
				<asp:Panel runat="server" ID="pnlSignature" CssClass="modal-dialog"
					Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
					<div class="modal-content">
						<div class="modal-header" style="padding: 7px 14px !important;">
							<div class="modal-title" id="exampleModalLabel1">
								<asp:Button Style="float: right;" ID="Button6" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender15_foregroundElement').style.display = 'none';" />
								<asp:Label ID="Label23" runat="server" Text="Signature Capture" Style="float: none; padding-top: 5px;"></asp:Label>
							</div>
						</div>
						<div class="modal-body">
							<%--<div style="text-align: center; margin-bottom: 5px;">Signature</div>--%>

							<div id="dvSignaturepad" class="m-signature-pad">
								<div class="m-signature-pad--body">
									<canvas style="background-color: white;" width="400" height="300"></canvas>
								</div>

								<div class="signature-pad--footer">
									<div class="signature-pad--actions">
										<div align="center">
											<button type="button" class="button clear" data-action="clear">Clear</button>
											<button type="button" class="button" data-action="undo">Undo</button>
											<button id="btnSave" type="button" class="button save" data-action="save-png">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<br />
						</div>
					</div>
				</asp:Panel>

				<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender15" runat="server" PopupControlID="pnlSignature" TargetControlID="btnCaptureSignature" CancelControlID="ctl00_SampleContent_ModalPopupExtender15_backgroundElement"
					Enabled="True" DynamicServicePath="" DropShadow="true">
				</ajaxToolkit:ModalPopupExtender>

			<%----------------------Upload Company Latterhead ----------------------%>
				<asp:Panel runat="server" ID="pnlCompanyLatterhead" CssClass="modal-dialog"
					Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
					<div class="modal-content">
						<div class="modal-header" style="padding: 7px 14px !important;">
							<div class="modal-title" id="exampleModalLabel1">
								<asp:Button Style="float: right;" ID="Button7" runat="server" Text="X" class="btn btn-danger" CausesValidation="false"  />
								<asp:Label ID="Label24" runat="server" Text="Authorization Form" Style="float: none; padding-top: 5px;"></asp:Label>
							</div>
						</div>
						<div class="modal-body" >
							<table cellpadding="10">
								<tr>
									<td>
										<asp:Label ID="Label75" runat="server" Text="Sample Messanger Authorization Form" Style="float: none; padding-top: 5px;"></asp:Label>
									</td>
									<td>
										<asp:LinkButton ID="LinkButton1" runat="server" href="Document/Messenger.docx" target="_blank" Text="Donwload" CssClass="btn btn-info"></asp:LinkButton>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label76" runat="server" Text="Select File :-" Style="float: none; padding-top: 5px;"></asp:Label>
									</td>
									<td> 
										
										<ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadAuthForm" runat="server" Width="200px" OnUploadedComplete="AsyncFileUploadAuthForm_UploadedComplete"
											ClientIDMode="AutoID" UploadingBackColor="Yellow"  OnClientUploadStarted="UpLoadStartedPDF" OnClientUploadError="UploadErrorPDF"  />
										<asp:Label ID="Label52" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Red" >(Select only PDF File)</asp:Label><br />
										<asp:Label ID="lblAuthFormMsg" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Blue"></asp:Label>
										
										</td>
									<td>
										<asp:Button Style="float: right" ID="btnPersonAuthForm" runat="server" Text="Upload" CssClass="btn btn-info"  CausesValidation="false" OnClick="btnPersonAuthForm_Click"  />
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label77" runat="server" Text="View Authorization Form" Style="float: none; padding-top: 5px;" ></asp:Label>
									</td>
									<td>
										
										<asp:Button ID="btnAuthFormView" runat="server"  Text="View" CssClass="btn btn-info" OnClick="btnAuthFormView_Click"  />
										<asp:HyperLink ID="lnkAuthorizationFormDownload" runat="server"  Text="Download"  Target="_blank" Visible="false" ></asp:HyperLink>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<asp:Literal ID="ltlAuthorizationForm" runat="server"  Visible="false" />
									</td>
								</tr>
							</table>
						</div>
						<div class="modal-footer">
							<br />
						</div>
					</div>
				</asp:Panel>

				<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender16" runat="server" PopupControlID="pnlCompanyLatterhead" TargetControlID="hdnbtnAuthorizationForm" CancelControlID="ctl00_SampleContent_ModalPopupExtender16_backgroundElement"
					Enabled="True" DynamicServicePath="" DropShadow="true">
				</ajaxToolkit:ModalPopupExtender>
		
				<%----------------------KYC Form Start----------------------%>
				    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender13" runat="server" PopupControlID="pnlKYC" TargetControlID="hdnbtnKYC" 
                    Enabled="True" DynamicServicePath="" DropShadow="true"  >
                </ajaxToolkit:ModalPopupExtender>
		
				<asp:Panel ID="pnlKYC" runat="server" CssClass="modal-dialog" Style="width: 900px; display: none; border: solid 2px lightgray;overflow: auto;height:auto;" >
					<div class="modal-content">
						<div class="modal-header" style="padding: 7px 14px !important;">
							<div class="modal-title" id="exampleModalLabel">
								<asp:Button Style="float: right" ID="btnClosex" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="javascript:currentTab=0;" />
								<asp:Label ID="lblModelHeader" runat="server" Text="KYC FORM" Style="float: none; padding-top: 5px;"></asp:Label>
							</div>
						</div>
						<div class="modal-body">
									 <div class="tab">
									<table class="kYcForm" border="0" cellspacing="3">
								<tr>
									<td align="center" colspan="4" style="padding:15px;">
										<asp:Label ID="Label14" runat="server" Text="KNOW YOUR CUSTOMER/SUPPLIER FORM" Font-Bold="False" Font-Size="18px" style="float:none;"></asp:Label>
									</td>
								</tr>
								<tr>
									<td colspan="4" align="center">
										<asp:Label ID="Label15" runat="server" Text="Customer Information Form :" Font-Bold="False" style="margin-top: 7px;"></asp:Label>
									<asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*" ControlToValidate="rdlCustomerInfo" SetFocusOnError="True" ValidationGroup="ValGrpKYC" Display="Static" ToolTip="Select Customer Domestic or International"></asp:RequiredFieldValidator>
										<asp:RadioButtonList ID="rdlCustomerInfo" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Font-Bold="True" CssClass="rblKYC">
											<asp:ListItem Value="0" Text="Domestic"></asp:ListItem>
											<asp:ListItem Value="1" Text="International"></asp:ListItem>
										</asp:RadioButtonList>
									</td>
								</tr>
									<tr>
										<td colspan="4" style="border-top:1px black solid;">
											<br />
										</td>
									</tr>	
								<tr>
									<td style="color:red;">* Mandatory  
									</td>
									<td colspan="2">Kindly attach visiting
										card with this form
									</td>
									<td colspan="1" style="float: right;">
										<asp:Label ID="Label17" runat="server" Text="Date: " style="margin-top: 5px;"></asp:Label>
										<asp:TextBox ID="txtkycDate" runat="server" TextMode="Date" ClientIDMode="Static"  ></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<asp:Label ID="Label72" runat="server" Text="Customer Type : " style="margin-top: 7px;"></asp:Label>
										<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ControlToValidate="rdlExistingCustomer" SetFocusOnError="True" ValidationGroup="ValGrpKYC" Display="Static" ToolTip="Select Customer New or Existing"></asp:RequiredFieldValidator>
										<asp:RadioButtonList ID="rdlExistingCustomer" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rblKYC" ControlToValidate="rdlExistingBuyer">
											<asp:ListItem Value="0" Text="Existing Customer"></asp:ListItem>
											<asp:ListItem Value="1" Text="New Customer"></asp:ListItem>
										</asp:RadioButtonList>
										
									</td>
									<td colspan="2">
										<asp:Label ID="Label73" runat="server" Text="Type Of Address : " style="margin-top: 7px;"></asp:Label>
									<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="rdlTypeOfAddress" SetFocusOnError="True" ValidationGroup="ValGrpKYC" Display="Static" ToolTip="Select Type OF Office"></asp:RequiredFieldValidator>
										<asp:RadioButtonList ID="rdlTypeOfAddress" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rblKYC">
											<asp:ListItem Value="0" Text="Office"></asp:ListItem>
											<asp:ListItem Value="1" Text="Recidence"></asp:ListItem>
											<asp:ListItem Value="2" Text="Shop"></asp:ListItem>
										</asp:RadioButtonList>
									</td>
								</tr>
										
								<tr>
									<td colspan="3">
										<asp:Label ID="Label16" runat="server" Text="Company Name:"></asp:Label>
										<asp:TextBox ID="txtkycCompany" runat="server" Width="450px" MaxLength="250" CssClass="lblKYC" Enabled="false" ></asp:TextBox>
									</td>
									<td rowspan="10" style="vertical-align:top;" align="center" >
										<div style="height:215px;width:210px;border:1px solid;">
										<asp:Literal ID="ltlKycPhoto" runat="server"  />
											</div>
										<asp:Label ID="Label54" runat="server" Text="" Style="float: none; padding-top: 5px;" >Select Customer KYC file</asp:Label><br />
										<ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadImgKycPhoto" runat="server" Width="172px" OnUploadedComplete="AsyncFileUploadImgKycPhoto_UploadedComplete"
											ClientIDMode="AutoID" UploadingBackColor="Yellow"  OnClientUploadStarted="UpLoadStartedPDF" OnClientUploadError="UploadErrorPDF"  />
										<asp:Label ID="Label53" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Red" >(Select only PDF file)</asp:Label><br />
										<asp:HyperLink ID="ImgKycPDFDownload" runat="server"  Text="Download"  Target="_blank" ></asp:HyperLink>
										<asp:HiddenField id="hdnKycPhoto" runat="server" />
									</td>
								</tr>
						
								<tr>
							
									<td colspan="3">
										<asp:Label ID="Label20" runat="server" Text="Address 1:"></asp:Label>
										<asp:TextBox ID="txtkycAddress1" runat="server" Width="450px" MaxLength="250" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td></td>
								</tr>
								<tr>
							
									<td colspan="3">
												<asp:Label ID="Label1" runat="server" Text="Address 2:"></asp:Label>
										<asp:TextBox ID="txtkycAddress2" runat="server" Width="450px" MaxLength="250" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label25" runat="server" Text="City:"></asp:Label>
										<asp:TextBox ID="txtkycCity" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td>
										<asp:Label ID="Label26" runat="server" Text="Zip:"></asp:Label>
										<asp:TextBox ID="txtkycZip" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td>
										<asp:Label ID="Label27" runat="server" Text="State:"></asp:Label>
										<asp:TextBox ID="txtkycState" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
								
								</tr>
								<tr>
										
									<td>
										<asp:Label ID="Label29" runat="server" Text="Tel(1):"></asp:Label>
										<asp:TextBox ID="txtkycTel1" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td>
										<asp:Label ID="Label30" runat="server" Text="Tel(2):"></asp:Label>
										<asp:TextBox ID="txtkycTel2" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td style="visibility:hidden;">
										<asp:Label ID="Label28" runat="server" Text="City Code:"></asp:Label>
										<asp:TextBox ID="txtkycCityCode" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td></td>
								</tr>
								<tr>
									
									<td>
										<asp:Label ID="Label32" runat="server" Text="FAX:"></asp:Label>
										<asp:TextBox ID="txtkycFAX" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td>
										<asp:Label ID="Label33" runat="server" Text="Email:"></asp:Label>
										<asp:TextBox ID="txtkycEmail" runat="server" Width="130px" MaxLength="50" CssClass="lblKYC" Enabled="false"></asp:TextBox>
									</td>
									<td></td>
									<td></td>
								</tr>
										<tr>
											<td colspan="2">
												<asp:Label ID="Label19" runat="server" Text="Owner / Partner’s Name:"></asp:Label>
											<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ControlToValidate="txtkycOwnerName" SetFocusOnError="True" ValidationGroup="ValGrpKYC" Display="Static" ToolTip="Enter Owners / Partner Name"></asp:RequiredFieldValidator>
											<asp:TextBox ID="txtkycOwnerName" runat="server" Width="245px" MaxLength="250" ></asp:TextBox>
											</td>
											<td >
										<asp:Label ID="Label18" runat="server" Text="Year Of Estd."></asp:Label>
										<asp:TextBox ID="txtkycYOE" runat="server" MaxLength="4" TextMode="Number" onkeypress="if (this.value.length >= 4) { return false; }" Width="100px" ></asp:TextBox>
									</td>
										</tr>
								<tr>
									<td>
										<asp:Label ID="Label31" runat="server" Text="Mobile:"></asp:Label>
										<asp:TextBox ID="txtkycMobile" runat="server" Width="130px" MaxLength="50" TextMode="Phone" ></asp:TextBox>
									</td>
									<td colspan="2">
										<asp:Label ID="Label34" runat="server" Text="Website"></asp:Label>
										<asp:TextBox ID="txtkycWebsite" runat="server" Width="375px" MaxLength="50" TextMode="Url"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<asp:Label ID="Label35" runat="server" Text="Your Representative if any"></asp:Label>
										<asp:TextBox ID="txtkycRepresentative" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
									</td>
									<td colspan="1">
										<asp:Label ID="Label36" runat="server" Text="Designation"></asp:Label>
										<asp:TextBox ID="txtkycDesignation" runat="server" Width="110px" MaxLength="50"></asp:TextBox>
									</td>
									<td></td>
								</tr>
										
										<tr>
											<td colspan="3">
												<br />
													<br />
												<br />
											</td>
										</tr>
							</table>
										 </div>
							  
								   <div class="tab">
								   <table class="kYcForm" border="0" cellspacing="3">
									   <tr>
										   <td colspan="4" style="text-align: center">
											   (Please Fill Up Representative Authority Form)
										   </td>
									   </tr>
									   <tr>
										   <td colspan="4">
											   <asp:Label ID="Label37" runat="server" Text="Member of"></asp:Label>
									
											   <asp:TextBox ID="txtkycMemberOf" runat="server" Width="720px" MaxLength="50"></asp:TextBox>
										   </td>
									   </tr>
									   <tr>
										  <td>
											   <asp:Label ID="Label38" runat="server" Text="Reference: 1"></asp:Label>
											  
										
											   <asp:TextBox ID="txtkycRef1" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
										   </td>
										   <td>
											    <asp:Label ID="Label40" runat="server" Text="Contact Person"></asp:Label>
										 
											   <asp:TextBox ID="txtkycContactPerson1" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
										   </td>
									   </tr>
									     <tr>
										  <td >
											   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label39" runat="server" Text="Reference: 2"></asp:Label>
										  
											   <asp:TextBox ID="txtkycRef2" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
										   </td>
											  <td>
											    <asp:Label ID="Label41" runat="server" Text="Contact Person"></asp:Label>
										  
											   <asp:TextBox ID="txtkycContactPerson2" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
										   </td>
									   </tr>
									   <tr>
										  <td>
											   <asp:Label ID="Label42" runat="server" Text="Corp Doc"></asp:Label>
											 
											   <asp:TextBox ID="txtkycPAN" runat="server" Width="150px" MaxLength="50" onkeyup="SetCorpDoc_onkeypress(event);" onkeydown="if(event.keyCode === 32) return false;"></asp:TextBox>
										   </td>
										   	  <td>
											   <asp:Label ID="Label43" runat="server" Text="Tax ID (GST No.)"></asp:Label>
												
											   <asp:TextBox ID="txtkycGSTNo" runat="server" Width="150px" MaxLength="50" onkeyup="SetCorpDoc_onkeypress(event);" onkeydown="if(event.keyCode === 32) return false;"></asp:TextBox>
										   </td>
										    <td>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><asp:Label ID="Label44" runat="server" Text="Tax ID w.e.f" style="padding-left: 10px;padding-right: 0px;"></asp:Label></td>
														<td><asp:TextBox ID="txtkycGSTNowef" runat="server" Width="150px" MaxLength="50" ></asp:TextBox></td>
													</tr>
												</table>
											   
											   
										   </td>
									   </tr>
						
									   <tr>
										  <td colspan="4">
											   <asp:Label ID="Label48" runat="server" Text="Proof of Identification with Photograph* (Kindly enclose Minimum Two Details (1 for Company & 1 for Owner)):"></asp:Label>
											  
											  </td>
										   </tr>
									   <tr>
										   <td colspan="4">
											   <table>
												   <tr>
													   <td style="width:300px;">
														   <asp:CheckBoxList ID="cblDocProof" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="300px"
															   Font-Bold="True" Font-Size="X-Large" CssClass="rblKYC" ClientIDMode="Static">
															   <asp:ListItem Value="0" Text="Corp Doc *" onclick="ClearCorpDoc();"></asp:ListItem>
															   <asp:ListItem Value="1" Text="Tax ID" onclick="ClearGSTNo();"></asp:ListItem>
															   <asp:ListItem Value="2" Text="Other" ></asp:ListItem>
														   </asp:CheckBoxList>
													   </td>
													   <td>
														   <%--<asp:CustomValidator ID="cvCheckBoxList" runat="server" ErrorMessage="* (Select Minimum Two Doc Proof)"
															   ValidationGroup="ValGrpKYC2" SetFocusOnError="true" 
															    OnServerValidate="DocProofCheckBoxListServerValidate" ToolTip="Select Minimum Two Doc Proof" Display="Dynamic" />--%>
													   </td>
												   </tr>
											   </table>
										   </td>
									   </tr>
									   <tr>
										   <td colspan="4">
											   <table>
												   <tr>
													   <td>
														   <asp:Label ID="Label49" runat="server" Text="Company Type"></asp:Label>
														   <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*" ControlToValidate="rdlCompanyType"
															   SetFocusOnError="True" ValidationGroup="ValGrpKYC2" ToolTip="Select Company Type"></asp:RequiredFieldValidator>
													   </td>
													   <td valign="top" colspan="2">
														   <asp:RadioButtonList ID="rdlCompanyType" runat="server" RepeatDirection="Horizontal" Width="150px" CssClass="rblKYC">
															   <asp:ListItem Value="0" Text="Individual"></asp:ListItem>
															   <asp:ListItem Value="1" Text="Proprietor"></asp:ListItem>
															   <asp:ListItem Value="2" Text="Partnership"></asp:ListItem>
															   <asp:ListItem Value="3" Text="Private Ltd."></asp:ListItem>
															   <asp:ListItem Value="4" Text="Public Ltd."></asp:ListItem>
														   </asp:RadioButtonList>
													   </td>
												   </tr>
									   <tr>
										   <td>
											  <asp:Label ID="Label50" runat="server" Text="Business Type"></asp:Label>
												
										   </td>
										    <td valign="top" colspan="2">
											   <asp:RadioButtonList ID="rdlBusinessType" runat="server" RepeatDirection="Horizontal" Width="170px" CssClass="rblKYC">
												   <asp:ListItem Value="0" Text="Wholesaler"></asp:ListItem>
												   <asp:ListItem Value="1" Text="Jewelery Manufacturer"></asp:ListItem>
												   <asp:ListItem Value="2" Text="Jewellery Retailer"></asp:ListItem>
												   <asp:ListItem Value="3" Text="End User"></asp:ListItem>
											   </asp:RadioButtonList>
										   </td>
										   
										   
									   </tr>
									   <tr>
										    <td>
											  <asp:Label ID="Label51" runat="server" Text="Distribution %"></asp:Label>
												
										   </td>
										    <td valign="top" colspan="2">
											  <asp:RadioButtonList ID="rdlDistribution" runat="server" RepeatDirection="Horizontal" Width="170px" CssClass="rblKYC">
												   <asp:ListItem Value="0" Text="Wholesaler"></asp:ListItem>
												   <asp:ListItem Value="1" Text="Jewelery Manufacturer"></asp:ListItem>
												   <asp:ListItem Value="2" Text="Retailer / Jeweller"></asp:ListItem>
												   <asp:ListItem Value="3" Text="End User"></asp:ListItem>
											   </asp:RadioButtonList>
										   </td>
									   </tr>

											   </table>
									   </td>
										   
									   </tr>
									   <tr>
										   <td>
											   <asp:Label ID="Label71" runat="server" Text="Company Brief :"></asp:Label>
										   </td>
									   </tr>
									   <tr>
										   <td colspan="4">
											   <asp:TextBox ID="txtkycCompanyBrief" runat="server" Width="865px" TextMode="MultiLine" Height="50px" onkeypress="if (this.value.length >= 500) { return false; }"></asp:TextBox>
										   </td>
									   </tr>
							
									   <tr>
										  <td colspan="4">
											Note : Information recorded here could be based on customers’ submission, primary internal research and secondary sources 
											  regarded as authentic. Please state the source for each piece of information and provide documentary support where meaningful.
											  The attempt here is to establish that the customer is an established member of the industry and as a committed member of the 
											  industry is unlikely to jeopardize their reputation through involvement in terrorist financing or money laundering.
										  </td>
										  </tr>
									   <tr>
										   <td colspan="4">
											   * Company documents such as PAN card, filing receipt, company registration , etc.
										   </td>
									   </tr>
								  </table>
									 
								 </div>
							
							 <div style="overflow:auto;">
								 <div style="float: right;">
									 <asp:Label ID="lblkycMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
									 <asp:Button runat="server" ID="btnNextKYC" CssClass="btn btn-info" Text="Next" OnClick="btnNextKYC_OnClick" ValidationGroup="ValGrpKYC"  /> 
									 <asp:Button runat="server" ID="btnPrevKYC" CssClass="btn btn-info" Text="Prev" OnClick="btnPrevKYC_OnClick" Visible="false"  />
									 <asp:Button ID="btnKYCSubmit" runat="server" Text="Submit" class="btn btn-info" OnClick="btnKYCSubmit_Click" Visible="false" OnClientClick="javascript:showTab(1);" />
								 </div>
							 </div>
							<!-- Circles which indicates the steps of the form: -->
							<div style="text-align: center; margin-top: 0px;">
								<span class="step"></span>
								<span class="step"></span>
								<%--<span class="step"></span>--%>
							</div>
			
							<script type="text/javascript">
								var currentTab = 0; // Current tab is set to be the first tab (0)
								//showTab(currentTab); // Display the current tab
								function showTab(n) {
									var x = document.getElementsByClassName("tab");
									x[n].style.display = "block";
									if (n == 0) {
										document.getElementById("prevBtn").style.display = "none";
									} else {
										document.getElementById("prevBtn").style.display = "inline";
									}
									if (n == (x.length - 1)) {
										document.getElementById("ctl00_SampleContent_btnKYCSubmit").style.display = "inline";
										document.getElementById("nextBtn").style.display = "none";
									} else {
										document.getElementById("nextBtn").innerHTML = "Next";
										document.getElementById("ctl00_SampleContent_btnKYCSubmit").style.display = "none";
										document.getElementById("nextBtn").style.display = "inline";
									}

									//... and run a function that will display the correct step indicator:
									fixStepIndicator(n)
								}
								function nextPrev(n) {
									// This function will figure out which tab to display
									var x = document.getElementsByClassName("tab");
									// Exit the function if any field in the current tab is invalid:

									// Hide the current tab:
									x[currentTab].style.display = "none";
									// Increase or decrease the current tab by 1:
									currentTab = currentTab + n;
									// if you have reached the end of the form...
									if (currentTab >= x.length) {
										// ... the form gets submitted:
										//document.getElementById("regForm").submit();
										//return false;
										currentTab = 0;
										//alert(currentTab);
									}
									// Otherwise, display the correct tab:
									showTab(currentTab);
								}
								function fixStepIndicator(n) {
									// This function removes the "active" class of all steps...
									var i, x = document.getElementsByClassName("step");
									for (i = 0; i < x.length; i++) {
										x[i].className = x[i].className.replace(" active", "");
									}
									//... and adds the "active" class on the current step:
									x[n].className += " active";
								}
							</script>

						</div>
						<div class="modal-footer">
							<br />
						</div>
					</div>
				</asp:Panel>
				<%----------------------KYC Form End----------------------%>
             </ContentTemplate>
        </asp:UpdatePanel>
            </div>
			
   
</asp:Content>
