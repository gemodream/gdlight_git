<%@ Page language="c#" Codebehind="DeliveryLogLookup.aspx.cs" AutoEventWireup="True" Inherits="Corpt.DeliveryLogLookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeliveryLogLookup</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:textbox style="Z-INDEX: 103; POSITION: absolute; TOP: 40px; LEFT: 24px" id="txtBatchNumber"
				runat="server" name="txtItemNumber" CssClass="inputStyleCC" ontextchanged="txtBatchNumber_TextChanged"></asp:textbox>
			<asp:hyperlink style="Z-INDEX: 105; POSITION: absolute; TOP: 40px; LEFT: 296px" id="HyperLink1"
				runat="server" CssClass="text" NavigateUrl="Middle.aspx">Back</asp:hyperlink>
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 192px" id="cmdLookup"
				runat="server" CssClass="buttonStyle" Text="Lookup" Width="80px" onclick="cmdLookup_Click"></asp:button>
			<asp:datagrid style="Z-INDEX: 102; POSITION: absolute; TOP: 80px; LEFT: 24px" id="dgCurrentBatchDisplay"
				runat="server" CssClass="text"></asp:datagrid>
			<asp:Label style="Z-INDEX: 104; POSITION: absolute; TOP: 16px; LEFT: 24px" id="lblInfo" runat="server"
				CssClass="text"></asp:Label>
		</form>
	</body>
</HTML>
