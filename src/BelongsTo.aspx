﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="BelongsTo.aspx.cs" Inherits="Corpt.BelongsTo" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" />
    <div class="demoheading">Report - Customer</div>
    <asp:Panel runat="server" DefaultButton="LoadButton" ID="SourceLoadPanel" CssClass="form-inline">
        <asp:Label runat="server" Text="Report Number"></asp:Label>
        <asp:TextBox ID="NumberField" runat="server" ToolTip="Item number or Char + Item Number"></asp:TextBox>
        <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Search Customer Name by Report Number"
            ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnLoadClick" />
        <asp:Label runat="server" ID="CustomerNameFld"></asp:Label>
    </asp:Panel>
    <asp:Label runat="server" ID="InfoLabel" ForeColor="Red"></asp:Label>
</asp:Content>
