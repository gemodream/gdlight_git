﻿namespace Corpt.Constants
{
    public class SessionConstants
    {
        //-- Sorting DataGrid
        public const string CurrentSortModel = "CurrentSortModel";
        public const string MovedItems  = "dsMovedItems";
        public const string BatchList   = "BatchList";

        //-- Item View
        public const string ShortReportModel = "shortReportModel";
        //-- Short Report II
        public const string ShortReportModelList = "shortReportModelList";
        public const string ShortReportActiveGroup = "shortReportActiveGroup";
        public const string ShortReportActive = "combinedReport";
        public const string ShortReportPrettyTable = "shortReportPrettyTable";
        //-- For sorting short report II
        public const string ShortReportSortExpression = "ShortReportII_SortExpression";
        public const string ShortReportSortExpressionDir = "ShortReportII_SortExpressionDir";
        
        //-- For Export to Excel and Download
        public const string DownLoadCmd = "DownLoadCmd";
        public const string DownLoadCmdAutoFull = "DownLoadAutoFull";
        public const string DownLoadCmdGrp = "DownLoadGrp";
        public const string DownLoadFileName = "DownLoadFileName";
        public const string DownLoadPretty = "DownLoadPretty";

        //-- Grade II
        public const string GradeItemNumbers = "GradeItemNumbers";
        public const string GradeMeasureParts = "GradeMeasureParts";
        public const string GradeMeasureValues = "GradeMeasureValues";
        public const string GradeMeasureValuesCp = "GradeMeasureValuesCp";
        public const string GradeMeasureControls = "GradeMeasureControls";
        public const string GradeMeasureEnums = "GDLight_EnumMeasures";

        //-- Open Orders
        public const string OpenOrdersList = "OpenOrdersList";

        //-- CP OverView
        public const string CustomersList = "CustomersList";
        public const string CustomersListSel = "CustomersListSel";
        public const string CustomerProgramList = "CustomerProgramList";
        public const string CustomerCpByStyle = "CustomerCpByStyle";
        public const string CustomerCpBySku = "CustomerCpBySku";

        public const string CpRulesByCp = "CpRulesByCp";
        public const string CpRulesByBatch = "CpRulesByBatch";
        public const string CpRulesByStyle = "CpRulesByStyle";
        public const string CpRulesBySku = "CpRulesBySku";

        //-- Tracking II
        public const string HistorySortModel = "HistorySortModel";
        public const string BatchesCurrentList = "BatchedCurrentList";
        public const string BatchedHistoryList = "BatchesHistoryList";

        //-- Tracking
        public const string TrackingAll = "TrackingAll";
        public const string TrackingMemo = "TrackingMemo";
        public const string TrackingCp = "TrackingCp";
        public const string TrackingOrder = "TrackingOrder";

        //-- Tracking Details
        public const string TrackDetailsFilter = "TrackDetailsFilter";
        public const string TrackDetailsList = "TrackDetailsList";
        public const string TrackDetailsModeOrder = "Order";
        public const string TrackDetailsModeMemo = "Memos";
        public const string TrackDetailsModeCp = "CP";
        public const string TrackBatchHistory = "TrackBatchHistory";

        //-- Testing GetFullOrder
        public const string TestFullOrderOld = "TestFullOrderOld";
        public const string TestFullOrderNew = "TestFullOrderNew";

        //-- New Order
        public const string NewOrderMemoList = "NewOrderMemoList";

        //-- Itemize
        public const string ItemizeOrderInfo = "ItemizeOrderInfo";
        public const string ItemizeMemoList = "ItemizeMemoList";
        public const string ItemizeLotList = "ItemizeLotList";
        public const string ItemizeCpList = "ItemizeCpList";
        public const string ItemizeNewBatches = "ItemizeNewBatches";
        public const string ItemizeDragedItems = "ItemizeDragedItems";

        //-- Global.asax, Directories
        public const string GlobalConnectionString = "ConnectionString";
        public const string GlobalConnectionStringMb = "ConnectionStringMB";
        public const string GlobalConnectionStringVv = "VVConnectionString";
        public const string GlobalConnectionStringStats = "StatsConnectionString";
        
        public const string GlobalOfficepPGroup = "OfficeIPGroup";
        public const string GlobalDocumentRoot = "DocumentRoot";
        public const string GlobalGifDocumentRoot = "GIFDocumentRoot";
        
        public const string GlobalPdfRoot = "PDFRoot";
        public const string GlobalVirtualVaultId = "VirtualVaultID";
        public const string GlobalTempDir = "TempDir";
        
        public const string GlobalReportTemplate = "ReportTemplate";
        public const string GlobalCertLabel = "CertLabel";
        public const string GlobalItemizedLabel = "ItemizedLabel";
        
        public const string GlobalSarinDir = "SARINDir";
        public const string Global2Ftp = "2FTP";
        public const string GlobalXmlMemo = "XML_Memo";
        public const string GlobalRepDir = "RepDir";

        public const string GlobalShapeRoot = "ShapeRoot";
        public const string GlobalShapePngRoot = "ShapePngRoot";
        public const string GlobalPictureRoot = "PictureRoot";

		public const string GlobalUnItemizedOrderAlertHours = "UnItemizedOrderAlertHours";
        public const string GlobalSpLog = "SpLog";

        public const string GlobalPathToPlotting = "PathToPlotting";
        public const string GlobalPlotDir = "PlotDir";
        public const string GlobalTempPlotDir = "tmpPlottingDir";

		// -- Global.asax, AZURE
		public const string AzureAccount = "AzureAccountName";
		public const string AccountKey = "AzureAccountKey";
		public const string AzureContainerName = "AzureContainerName";
		public const string WebPictureShapeRoot = "WebPictureShapeRoot";
		public const string WebShapePngRoot = "WebShapePngRoot";
		public const string WebTempFolder = "WebTempFolder";
		
		//-- Global.asax, Send Email parameters for ShortReport
		public const string GlobalEmailHost = "EMailHost";
        public const string GlobalEmailPort = "EMailPort";
        public const string GlobalEmailFromAddr = "EMailFromAddr";
        public const string GlobalEmailFromPassw = "EMailFromPassw";
        public const string GlobalEmailSsl = "EMailSSL";

        //-- Global.asax, Send Email parameters for ShortReport
        public const string GlobalVvEmailHost = "VVEMailHost";
        public const string GlobalVvEmailPort = "VVEMailPort";
        public const string GlobalVvEmailFromAddr = "VVEMailFromAddr";
        public const string GlobalVvEmailFromPassw = "VVEMailFromPassw";
        public const string GlobalVvEmailSsl = "VVEMailSSL";
        public static string[] GlobalAll =
            {
                GlobalConnectionString, GlobalConnectionStringMb, GlobalConnectionStringVv,
                GlobalOfficepPGroup, GlobalDocumentRoot, GlobalGifDocumentRoot, 
                GlobalPdfRoot, GlobalVirtualVaultId, GlobalTempDir, 
                GlobalReportTemplate,  GlobalCertLabel, GlobalItemizedLabel,
                GlobalSarinDir, Global2Ftp, GlobalXmlMemo, GlobalRepDir,
                GlobalShapeRoot, GlobalShapePngRoot, GlobalPictureRoot, 
                GlobalUnItemizedOrderAlertHours, GlobalSpLog,
                GlobalPathToPlotting, GlobalPlotDir,
                GlobalEmailHost, GlobalEmailPort, GlobalEmailFromAddr, GlobalEmailFromPassw, GlobalEmailSsl,
                GlobalVvEmailHost, GlobalVvEmailPort, GlobalVvEmailFromAddr, GlobalVvEmailFromPassw, GlobalVvEmailSsl
            };
        

        //-- Bulk Update 2
        public const string BulkBatches = "BulkBatches";
        public const string BulkMeasures = "BulkMeasures";
        public const string BulkParts = "BulkParts";
        public const string BulkPartsMulti = "BulkPartsMulti";
        public const string BulkEnumMeasures = "BulkEnumMeasures";
        public const string BulkEnteredValues = "BulkEnteredValues";
        public const string BulkFirstBatch = "BulkFirstBatch";
        //-- Leo Report 2
        public const string LeoShortReports = "LeoShortReports";
        public const string LeoDataTable = "LeoDataTable";
        public const string LeoItemNumbersForOrder = "LeoItemNumbersForOrder";
        public const string LeoHistoryOrders = "LeoOrdersHistory";
        public const string LeoMovedItems = "LeoMovedItems";

        //-- Reassembly (CopyData)
        public static string ReassemblySourceItem = "ReassemblySourceItem";
        public static string ReassemblyTargetItem = "ReassemblyTargetItem";

        //-- Login & Change Password
        public static string LoginModel = "LoginModel";

        //-- PSX form
        public static string ScanData = "ScanData";

        //-- Customer/Vendor form
        public static string CustomerVendorCp = "CustomerVendorCp";
        public static string CpList = "CpList";

        //-- Sorting Stat form
        public static string SortingStatList = "SortingStatList";

        //-- Number Generator form
        public static string NumberGeneratorList = "NumberGeneratorList";

        //-- Print Labels (Certified, Itemized)
        public static string PrintBatchModel = "PrintBatchModel";
        public static string PrintLabelsList = "PrintLabelsList";

        //-- DeliveryLog
        public static string TrkEventList = "TrkEventList";

        //-- Grade: Color, Clarity, Measure
        public static string GradeEditedItemList = "GradeTuchedItemList";
        public static string GradeMeasureList = "GradeMeasureList";
        public static string GradeMeasureValuesMap = "GradeMeasureValuesMap";
        public static string GradeMeasureHotKeys = "GradeMeasureHotKeys";
        public static string ExpressGradingMeasures = "ExpressGradingMeasures";
        //-- Show/Hide side menu
        public static string SideMenuState = "SideMenuState";
        public static string SideMenuShow = "SideMenuShow";
        public static string SideMenuHide = "SideMenuHide";

        //-- Pre-Filling Measure Values
        public static string PreFillingValues = "PreFillingValues";

        //-- Edit Customer
        public static string CustomerEditData = "CustomerEditData";
        public static string PersonEditData = "PersonEditData";
        public static string CustomerEditDataExt = "CustomerEditDataExt";

        //-- Customer History
        public static string CustomerHistoryData = "CustomerHistoryData";

        //-- Customer Program
        public static string CpCommonItemTypeGroups = "CpCommonItemTypeGroups";
        public static string CpCommonItemTypes = "CpCommonItemTypes";
        public static string CpCommonAdditionalServices = "CpCommonAdditionalServices";
        public static string CpCommonShortMeasures = "CpCommonShortMeasures";
        public static string CpCommonEnumMeasures = "CpCommonEnumMeasures";
        public static string SamplePriceList = "SamplePriceList";
        public static string SamplePriceListRanges = "SamplePriceListRanges";
        public static string SamplePricePartMeasures = "SamplePricePartMeasures";
        public static string CpLastFocusIsComment = "CpLastFocusIsComment";
        public static string CpEditModel = "CpEditModel";
        public static string FractionRangesModel = "FractionRangesModel";
        public static string FractionRangesGroupModel = "FractionRangesGroupModel";

        //-- New User Activation
        public static string VvUsers = "VvUsers";

        //-- Define Document
        public static string DdInitData = "DdInitData";

        //-- Order Update
        public static string OrderHistoryList = "OrderHistoryList";
        public static string UpdateOrder = "UpdateOrder";

        //-- Billing
        public static string BillingInfo = "BillingInfo";

        //-- Short Report extended
        public static string ShortReportExtMeasuresForEdit = "ShortReportExtMeasuresForEdit";
        public static string ShortReportExtLabelsForPrint = "ShortReportExtLabelsForPrint";
        public static string MeasureValueBatch = "MeasureValueBatch";
        //-- Stats
        public static string DocumentTypeList = "DocumentTypeList";

        //--Currency
        public static string CurrencyList = "CurrencyList";

        //-- AccountRep
        public static string GroupCode = "GroupCode";
        public static string BatchCode = "BatchCode";
        public static string ItemCode = "ItemCode";
        public static string StateTargetModels = "StateTargetModels";
        public static string AdditionalServicesRows = "AdditionalServiceData";
        public static string AdditionalServicesModels = "AdditionalServiceModels";
        public static string AdditionalServicesOrders = "AdditionalServiceOrders";
        public static string MigratedItems = "MigratedItems";
        public static string BillingCurrentGroupModels = "BillingCurrentGroupModel";
        public static string BatchPictureImageRecords = "BatchImageRecords";
        public static string ItemShapeImageRecords = "ItemImageRecords";
        public static string NewOrderTreeLoaded = "NewOrderTreeLoaded";
        public static string IsBulkBilling = "IsBulkBilling";
        public static string BeginDate = "BeginDate";
        public static string EndDate = "EndDate";
        public static string BulkBillingPricesModels = "BulkBillingPricesGridViewRows";
        public static string BulkBillingPrices = "BulkBillingPrices";
        public static string BulkBillingQuantitiesData = "BulkBillingQuantitiesData";
        public static string AlreadyBilledOrders = "AlreadyBilledOrders";
        public static string CustomerInvoices = "CustomerInvoices";
        public static string OrdersInvoice = "OrderInvoices";
        public static string InvoiceItems = "InvoiceItems";
        public static string TimeZoneOffsetInMinutes = "TimeZoneOffsetInMinutes";
        public static string DisplayTimeZoneMode = "DisplayTimeZoneMode";
        public static string OrdersTreeViewDataSet = "OrdersTreeViewDataSet";
        public static string OfficeTimeZones = "OfficeTimeZones";
        public static string IsSearchByOrderList = "IsSearchByOrderList";
        public static string TreeViewTotalNodesLimit = "TreeViewTotalNodesLimit";
        public static string SelectedItemsServiceTypesBillingSet = "SelectedItemsServiceTypesBillingSet";
        public static string BatchLabelDialogModel = "BatchLabelDialogModel";

        //Screening Utilities

        public static string AllTypes = "All types";
        public static string LGshort = "LG";
        public static string LGfull = "Lab Grown";
        public static string Natural = "Natural";
        public static string ShowAll = "Show all";
        public static string NonBranded = " Non-branded";
    }
}