﻿namespace Corpt.Constants
{
    public class PageConstants
    {
        public const string PageShowMovedItems = "ShowMovedItems.aspx";
        public const string PageShortReportIICutgradeizer = "ShortReportIICutgradeizer.aspx";
        public const string StNumberReport = "StNumberReport.aspx";
        public const string PageItemView = "ItemView.aspx";
        //-- link constants
         public const string LinkOnPageItemViewDetail = PageItemView + "?BatchId={0}" + "&ItemCode={1}" + "&ItemNumber={2}";
        //-- work columns name
        public const string ColNameLinkOnItemView = "_ItemNumber";
        public const string ColNameLeoOrdered = "Ordered";
        public const string ColNameLeoPrinted = "Printed";
        //-- layuot direction for short report
        public const string DirHorizontal = "H";
        public const string DirVertical = "V";
        
        public const string RevNumber = "(Rev# 430)"; 
        public const string AppTitle = "GSI: ";
	    public const string BatchNumberPattern = @".{1,100}"; // @"^\d{7,9}|\-\d{1,2}$";
    	public const string ItemNumberPattern = @".{1,100}"; // @"^\d{7,9}|\-\d{1,2}$";

    }
}