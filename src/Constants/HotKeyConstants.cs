﻿using System;
using System.Collections.Generic;
using System.Linq;
using Corpt.Models;

namespace Corpt.Constants
{
    public  static class HotKeyConstants
    {
        
        static readonly MeasureModel MeasureClarityGrade = new MeasureModel { MeasureId = 29,  MeasureName = "Clarity Grade",MeasureClass = MeasureModel.MeasureClassEnum};
        static readonly MeasureModel MeasurePolish       = new MeasureModel { MeasureId = 18,  MeasureName = "Polish",       MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureSymmetry     = new MeasureModel { MeasureId = 19,  MeasureName = "Symmetry",     MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureGirdleType   = new MeasureModel { MeasureId = 21,  MeasureName = "GirdleType",   MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureGirdleFrom   = new MeasureModel { MeasureId = 92,  MeasureName = "GirdleFrom",   MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureGirdleTo     = new MeasureModel { MeasureId = 93,  MeasureName = "GirdleTo",     MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureFluorescence = new MeasureModel { MeasureId = 91,  MeasureName = "Fluorescence Color", MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureOpeningCavty = new MeasureModel { MeasureId = 103, MeasureName = "Openings/Cavity",  MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureBlackPique   = new MeasureModel { MeasureId = 102, MeasureName = "Black Pique",      MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureIndentedClar = new MeasureModel { MeasureId = 104, MeasureName = "Indented Natural", MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureBrokenStone  = new MeasureModel { MeasureId = 105, MeasureName = "Broken Stone", MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureFf = new MeasureModel { MeasureId = 51, MeasureName = "FF", MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureHeartsArrow  = new MeasureModel { MeasureId = 97, MeasureName = "Hearts and Arrows", MeasureClass = MeasureModel.MeasureClassEnum };
        static readonly MeasureModel MeasureColorGrade   = new MeasureModel { MeasureId = 27, MeasureName = "Color Grade", MeasureClass = MeasureModel.MeasureClassEnum };

        static readonly MeasureAccessModel Acl = MeasureAccessModel.AccessClarity;
        static readonly MeasureAccessModel Ac = MeasureAccessModel.AccessColor;

        static readonly List<MeasureHotKeyModel> HotKeys = new List<MeasureHotKeyModel>
                {
                    //-- Color Grade (Color)
                    new MeasureHotKeyModel { HotKey = "DD",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 12,	 EnumValueTitle = "D"},
                    new MeasureHotKeyModel { HotKey = "EE",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 13,	 EnumValueTitle = "E"},
                    new MeasureHotKeyModel { HotKey = "FF",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 14,	 EnumValueTitle = "F"},
                    new MeasureHotKeyModel { HotKey = "GG",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 15,	 EnumValueTitle = "G"},
                    new MeasureHotKeyModel { HotKey = "HH",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 16,	 EnumValueTitle = "H"},
                    new MeasureHotKeyModel { HotKey = "II",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 17,	 EnumValueTitle = "I"},
                    new MeasureHotKeyModel { HotKey = "JJ",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 18,	 EnumValueTitle = "J"},
                    new MeasureHotKeyModel { HotKey = "KK",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 19,	 EnumValueTitle = "K"},
                    new MeasureHotKeyModel { HotKey = "LL",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 20,	 EnumValueTitle = "L"},
                    new MeasureHotKeyModel { HotKey = "MM",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 21,	 EnumValueTitle = "M"},
                    new MeasureHotKeyModel { HotKey = "NOO",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 22,	 EnumValueTitle = "N-O"},	
                    new MeasureHotKeyModel { HotKey = "OPP",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 23,	 EnumValueTitle = "O-P"},	
                    new MeasureHotKeyModel { HotKey = "PQQ",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 24,	 EnumValueTitle = "P-Q"},	
                    new MeasureHotKeyModel { HotKey = "QRR",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 25,	 EnumValueTitle = "Q-R"},	
                    new MeasureHotKeyModel { HotKey = "RSS",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 26,	 EnumValueTitle = "R-S"},	
                    new MeasureHotKeyModel { HotKey = "STT",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 27,	 EnumValueTitle = "S-T"},	
                    new MeasureHotKeyModel { HotKey = "TUU",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 28,	 EnumValueTitle = "T-U"},	
                    new MeasureHotKeyModel { HotKey = "UVV",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 29,	 EnumValueTitle = "U-V"},	
                    new MeasureHotKeyModel { HotKey = "VMM",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 30,	 EnumValueTitle = "V-W"},	
                    new MeasureHotKeyModel { HotKey = "WXX",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 31,	 EnumValueTitle = "W-X"},	
                    new MeasureHotKeyModel { HotKey = "XYY",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 32,	 EnumValueTitle = "X-Y"},	
                    new MeasureHotKeyModel { HotKey = "YZZ",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 33,	 EnumValueTitle = "Y-Z"},	
                    new MeasureHotKeyModel { HotKey = "EFF",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 34,	 EnumValueTitle = "E-F"},	
                    new MeasureHotKeyModel { HotKey = "FGG",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 163, EnumValueTitle = "F-G"},
                    new MeasureHotKeyModel { HotKey = "GHH",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 164, EnumValueTitle = "G-H"},	
                    new MeasureHotKeyModel { HotKey = "HII",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 165, EnumValueTitle = "H-I"},	
                    new MeasureHotKeyModel { HotKey = "IJJ",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 166, EnumValueTitle = "I-J"},	
                    new MeasureHotKeyModel { HotKey = "JKK",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 167, EnumValueTitle = "J-K"},	
                    new MeasureHotKeyModel { HotKey = "KLL",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 168, EnumValueTitle = "K-L"},	
                    new MeasureHotKeyModel { HotKey = "LMM",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 169, EnumValueTitle = "L-M"},	
                    new MeasureHotKeyModel { HotKey = "MNN",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 170, EnumValueTitle = "M-N"},	
                    new MeasureHotKeyModel { HotKey = "NN",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 206, EnumValueTitle = "N"},
                    new MeasureHotKeyModel { HotKey = "DEE",    Access = Ac, Measure = MeasureColorGrade, EnumValueId = 5590, EnumValueTitle = "D-E"},
                    new MeasureHotKeyModel { HotKey = "ZN",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 5591, EnumValueTitle = "N/A"},	
                    new MeasureHotKeyModel { HotKey = "ZZ",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 5631, EnumValueTitle = "Reject"},
                    new MeasureHotKeyModel { HotKey = "ZT",     Access = Ac, Measure = MeasureColorGrade, EnumValueId = 5817, EnumValueTitle = "Mismatch"},	

                    //-- Hearts and Arrows (Clarity)
                    new MeasureHotKeyModel { HotKey = "ha",  Access = Acl, Measure = MeasureHeartsArrow, EnumValueId = 5522,     EnumValueTitle = "Hearts and Arrows"},
                    new MeasureHotKeyModel { HotKey = "hn",  Access = Acl, Measure = MeasureHeartsArrow, EnumValueId = 5521,     EnumValueTitle = ""},

                    //-- FF (Clarity)
                    new MeasureHotKeyModel { HotKey = "ff",  Access = Acl, Measure = MeasureFf, EnumValueId = 50,     EnumValueTitle = "Fracture Filled"},
                    new MeasureHotKeyModel { HotKey = "fg",  Access = Acl, Measure = MeasureFf, EnumValueId = 51,     EnumValueTitle = ""},

                    //-- Broken Stone (Clarity)
                    new MeasureHotKeyModel { HotKey = "zb1",  Access = Acl, Measure = MeasureBrokenStone, EnumValueId = 5555,     EnumValueTitle = ""},
                    new MeasureHotKeyModel { HotKey = "zb2",  Access = Acl, Measure = MeasureBrokenStone, EnumValueId = 5659,     EnumValueTitle = "Chipped"},
                    new MeasureHotKeyModel { HotKey = "zb3",  Access = Acl, Measure = MeasureBrokenStone, EnumValueId = 5658,     EnumValueTitle = "Broken"},
                    new MeasureHotKeyModel { HotKey = "zb4",  Access = Acl, Measure = MeasureBrokenStone, EnumValueId = 5554,     EnumValueTitle = "Severely Broken"},

                    //-- Indented Natural (Clarity)
                    new MeasureHotKeyModel { HotKey = "zii",  Access = Acl, Measure = MeasureIndentedClar, EnumValueId = 5552,     EnumValueTitle = "Indented Natural"},
                    new MeasureHotKeyModel { HotKey = "zig",  Access = Acl, Measure = MeasureIndentedClar, EnumValueId = 6140,     EnumValueTitle = "Indented Natural GR"},
                    new MeasureHotKeyModel { HotKey = "ziq",  Access = Acl, Measure = MeasureIndentedClar, EnumValueId = 5553,     EnumValueTitle = ""},

                    //-- Black Pique (Clarity)
                    new MeasureHotKeyModel { HotKey = "zpp",  Access = Acl, Measure = MeasureBlackPique, EnumValueId = 5548,     EnumValueTitle = "Black Pique"},
                    new MeasureHotKeyModel { HotKey = "zpq",  Access = Acl, Measure = MeasureBlackPique, EnumValueId = 5549,     EnumValueTitle = ""},

                    //-- Openings/Cavity (Clarity)
                    new MeasureHotKeyModel { HotKey = "zoo",  Access = Acl, Measure = MeasureOpeningCavty, EnumValueId = 5550,     EnumValueTitle = "Openings/Cavity"},
                    new MeasureHotKeyModel { HotKey = "zog",  Access = Acl, Measure = MeasureOpeningCavty, EnumValueId = 6141,     EnumValueTitle = "Open GR"},
                    new MeasureHotKeyModel { HotKey = "zoq",  Access = Acl, Measure = MeasureOpeningCavty, EnumValueId = 5551,     EnumValueTitle = ""},

                    //-- Clarity Grade (Clarity)
                    new MeasureHotKeyModel { HotKey = "1",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 1,     EnumValueTitle = "FL"},
                    new MeasureHotKeyModel { HotKey = "2",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 2,     EnumValueTitle = "IF"},
                    new MeasureHotKeyModel { HotKey = "3",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 3,     EnumValueTitle = "VVS1"},
                    new MeasureHotKeyModel { HotKey = "4",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 4,     EnumValueTitle = "VVS2"},
                    new MeasureHotKeyModel { HotKey = "5",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5,     EnumValueTitle = "VS1"},
                    new MeasureHotKeyModel { HotKey = "6",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 6,     EnumValueTitle = "VS2"},
                    new MeasureHotKeyModel { HotKey = "7",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 7,     EnumValueTitle = "SI1"},
                    new MeasureHotKeyModel { HotKey = "8",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 8,     EnumValueTitle = "SI2"},
                    new MeasureHotKeyModel { HotKey = "9",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 9,     EnumValueTitle = "I1"},
                    new MeasureHotKeyModel { HotKey = "0",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 10,    EnumValueTitle = "I2"},
                    new MeasureHotKeyModel { HotKey = "Q",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 11,    EnumValueTitle = "I3"},
                    new MeasureHotKeyModel { HotKey = "W",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 185,   EnumValueTitle = "VS1-VS2"},
                    new MeasureHotKeyModel { HotKey = "E",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 186,   EnumValueTitle = "VS2-SI1"},
                    new MeasureHotKeyModel { HotKey = "R",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 187,   EnumValueTitle = "SI1-SI2"},
                    new MeasureHotKeyModel { HotKey = "T",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 188,   EnumValueTitle = "SI2-I1"},
                    new MeasureHotKeyModel { HotKey = "Y",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 189,   EnumValueTitle = "I1-I2"},
                    new MeasureHotKeyModel { HotKey = "U",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 190,   EnumValueTitle = "I2-I3"},
                    new MeasureHotKeyModel { HotKey = "I",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5796,  EnumValueTitle = "VVS1-VVS2"},
                    new MeasureHotKeyModel { HotKey = "O",  Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5765,  EnumValueTitle = "VVS2-VS1"},
                    new MeasureHotKeyModel { HotKey = "ZT", Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5818,  EnumValueTitle = "Mismatch"},
                    new MeasureHotKeyModel { HotKey = "ZZ", Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5630,  EnumValueTitle = "Reject"},
                    new MeasureHotKeyModel { HotKey = "ZN", Access = Acl, Measure = MeasureClarityGrade, EnumValueId = 5583,  EnumValueTitle = "N/A"},
                    
                    //-- Polish (Clarity)
                    new MeasureHotKeyModel { HotKey = "pe", Access = Acl, Measure = MeasurePolish, EnumValueId = 37,  EnumValueTitle = "Excellent"},
                    new MeasureHotKeyModel { HotKey = "pv", Access = Acl, Measure = MeasurePolish, EnumValueId = 38,  EnumValueTitle = "Very Good"},
                    new MeasureHotKeyModel { HotKey = "pg", Access = Acl, Measure = MeasurePolish, EnumValueId = 193, EnumValueTitle = "Good"},
                    new MeasureHotKeyModel { HotKey = "pf", Access = Acl, Measure = MeasurePolish, EnumValueId = 194, EnumValueTitle = "Fair"},
                    new MeasureHotKeyModel { HotKey = "pp", Access = Acl, Measure = MeasurePolish, EnumValueId = 195, EnumValueTitle = "Poor"},

                    //-- Symmetry (Clarity)
                    new MeasureHotKeyModel { HotKey = "se", Access = Acl, Measure = MeasureSymmetry, EnumValueId = 40,   EnumValueTitle = "Excellent"},
                    new MeasureHotKeyModel { HotKey = "sv", Access = Acl, Measure = MeasureSymmetry, EnumValueId = 41,   EnumValueTitle = "Very Good"},
                    new MeasureHotKeyModel { HotKey = "sg", Access = Acl, Measure = MeasureSymmetry, EnumValueId = 5486, EnumValueTitle = "Good"},
                    new MeasureHotKeyModel { HotKey = "sf", Access = Acl, Measure = MeasureSymmetry, EnumValueId = 5487, EnumValueTitle = "Fair"},
                    new MeasureHotKeyModel { HotKey = "sp", Access = Acl, Measure = MeasureSymmetry, EnumValueId = 5488, EnumValueTitle = "Poor"},

                    //-- GirdleType (Clarity)
                    new MeasureHotKeyModel { HotKey = "gtp",    Access = Acl, Measure = MeasureGirdleType, EnumValueId = 196,  EnumValueTitle = "Polished"},
                    new MeasureHotKeyModel { HotKey = "gtf",    Access = Acl, Measure = MeasureGirdleType, EnumValueId = 197,  EnumValueTitle = "Faceted"},
                    new MeasureHotKeyModel { HotKey = "gtb",    Access = Acl, Measure = MeasureGirdleType, EnumValueId = 77,   EnumValueTitle = "Bruted"},
                    new MeasureHotKeyModel { HotKey = "gtsp",   Access = Acl, Measure = MeasureGirdleType, EnumValueId = 78,   EnumValueTitle = "Semi polished"},
                    new MeasureHotKeyModel { HotKey = "stsf",   Access = Acl, Measure = MeasureGirdleType, EnumValueId = 79,   EnumValueTitle = "Semi faceted"},

                    //-- GirdleFrom (Clarity)
                    new MeasureHotKeyModel { HotKey = "gf1",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5475,  EnumValueTitle = "Extremely Thin"},
                    new MeasureHotKeyModel { HotKey = "gf2",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5476,  EnumValueTitle = "Very Thin"},
                    new MeasureHotKeyModel { HotKey = "gf3",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5477,  EnumValueTitle = "Thin"},
                    new MeasureHotKeyModel { HotKey = "gf4",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5478,  EnumValueTitle = "Medium"},
                    new MeasureHotKeyModel { HotKey = "gf5",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5479,  EnumValueTitle = "Slightly Thick"},
                    new MeasureHotKeyModel { HotKey = "gf6",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5480,  EnumValueTitle = "Thick"},
                    new MeasureHotKeyModel { HotKey = "gf7",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5481,  EnumValueTitle = "Very Thick"},
                    new MeasureHotKeyModel { HotKey = "gf8",   Access = Acl, Measure = MeasureGirdleFrom, EnumValueId = 5482,  EnumValueTitle = "Extremely Thick"},

                    //-- GirdleTo (Clarity)
                    new MeasureHotKeyModel { HotKey = "gt1",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5511,  EnumValueTitle = "Extremely Thin"},
                    new MeasureHotKeyModel { HotKey = "gt2",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5512,  EnumValueTitle = "Very Thin"},
                    new MeasureHotKeyModel { HotKey = "gt3",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5513,  EnumValueTitle = "Thin"},
                    new MeasureHotKeyModel { HotKey = "gt4",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5514,  EnumValueTitle = "Medium"},
                    new MeasureHotKeyModel { HotKey = "gt5",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5515,  EnumValueTitle = "Slightly Thick"},
                    new MeasureHotKeyModel { HotKey = "gt6",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5516,  EnumValueTitle = "Thick"},
                    new MeasureHotKeyModel { HotKey = "gt7",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5517,  EnumValueTitle = "Very Thick"},
                    new MeasureHotKeyModel { HotKey = "gt8",   Access = Acl, Measure = MeasureGirdleTo, EnumValueId = 5518,  EnumValueTitle = "Extremely Thick"},

                    //-- Fluorescence (Color)
                    new MeasureHotKeyModel { HotKey = "FLB",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5426,  EnumValueTitle = "Blue"},
                    new MeasureHotKeyModel { HotKey = "FLY",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5427,  EnumValueTitle = "Yellow"},
                    new MeasureHotKeyModel { HotKey = "FLR",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5428,  EnumValueTitle = "Red"},
                    new MeasureHotKeyModel { HotKey = "FLO",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5429,  EnumValueTitle = "Orange"},
                    new MeasureHotKeyModel { HotKey = "FLW",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5430,  EnumValueTitle = "White"},
                    new MeasureHotKeyModel { HotKey = "FLG",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5431,  EnumValueTitle = "Green"},
                    new MeasureHotKeyModel { HotKey = "FLZ",   Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5570,  EnumValueTitle = ""},
                    new MeasureHotKeyModel { HotKey = "FLBY",  Access = Ac, Measure = MeasureFluorescence, EnumValueId = 5921,  EnumValueTitle = "Blue, yellow"}, 

                };

        public static List<MeasureHotKeyModel> GetHotKeys(string accessCode)
        {
            return string.IsNullOrEmpty(accessCode) ? HotKeys : HotKeys.FindAll(m => m.Access.AccessCode == Convert.ToInt32(accessCode));
        }
        public static string GetDuplicateKeys()
        {
            var keys = HotKeys;
            var duplicate = new List<MeasureHotKeyModel>();
            var groups = keys.GroupBy(m => m.HotKey + ";" + m.Access.AccessCode).ToList();
            foreach (var group in groups)
            {
                var hotKey = group.Key.Split(';')[0];
                var access = Convert.ToInt32(group.Key.Split(';')[1]);
                var byKey = keys.FindAll(m => m.Access.AccessCode == access && m.HotKey == hotKey);
                if (byKey.Count > 1) duplicate.AddRange(byKey);
            }
            return duplicate.Count == 0 ? "" : duplicate.Aggregate("Ambiguous key:" + "<br/>", (current, item) => current + (item.Help + "<br/>"));
        }

    }
}