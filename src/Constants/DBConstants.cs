﻿namespace Corpt.Constants
{
    public static class DbConstants
    {
        #region trkEvent

        public static int TrkEventOpened    = 1;
        public static int TrkEventUpdated   = 2;
        public static int TrkEventTouched   = 3;
        public static int TrkEventAddReport = 10;
        public static int TrkEventBilled    = 7;
        #endregion

        #region tblViewAccess

        public static int ViewAccessGdLightGradeId      = 20;
        public static int ViewAccessGdLightItemizing    = 19;
        public static int ViewAccessReitemizing         = 8;
        public static int ViewAccessRemeasure           = 7;
        public static int ViewAccessClarityCode         = 4;
        public static int ViewAccessColorCode           = 5;
        public static int ViewAccessMeasureCode         = 6;
        public static int ViewAccessAccountRep          = 8; //Representative account

        #endregion

        #region vwState

        public static int ItemInvalidStateId = 10;
        public static int ItemInvalidStateCode = 3;

        public static int ItemValidStateId = 11;
        public static int ItemValidStateCode = 2;

        #endregion

        #region tblMeasure
        public static int MeasureIdInternalComment = 26;
        public static string MeasureNameInternalComment = "Internal Comment";
        public static string  MeasureCodeInternalComment = "26";
        public static string MeasureIdShapeCut = "8";
        public static string MeasureCodeTotalWeight1 = "2";     //-- Total weight(ct)
        public static string MeasureCodeTotalWeight2 = "4";     //-- Measured Weight(ct)
        public static string MeasureCodeColorGrade = "27";      //-- Color Grade
        public static string MeasureCodeClarityGrade = "29";    //-- Clarity Grade
        public static string MeasureCodeColorDiamond = "32";    //-- Color diamond color
        public static string MeasureCodeVirtualNumber = "92";   //-- Virtual Vault Number
        public static string MeasureIdAppraisal = "10";         //-- Appraisal Value

        #endregion

        #region tblPartType

        public static string PartTypeCodeDiamond = "1";
        public static string PartTypeCodeItemContainer = "15";
        public static string PartTypeIdItemContainer = "15";
        public static string PartTypeCodeColorDiamond = "2";

        #endregion

        #region tblDocumentType

        public static string DocumentTypeCodeSrtReport = "10";
        public static string DocumentTypeCodeLblReport = "8";

        #endregion

        #region tblSyntheticCustomerEntries

        public static int GDServiceTypeIdDefault = 7;
        public static int GDServiceTypeIdLab = 1;
        public static int LabServiceTypeCode = 8;
        public static int InventoryScreeningTypeCode = 9;
        #endregion
        //IvanB 25/04 start
        #region tblServiceType
        public static string GDScreeningServiceTypeID = "7";
        public static string GDQCServiceTypeID = "12";
        public static string GDQCAndScreeningServiceTypeID = "13";
        #endregion
        //IvanB 25/04 end
    }
}