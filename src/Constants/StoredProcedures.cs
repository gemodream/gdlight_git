﻿namespace Corpt.Constants
{
    public class StoredProcedures
    {
        public const string WspvvGetItemsByBatchExistingNumbersOnly = "wspvvGetItemsByBatchExistingNumbersOnlyEx";
        public const string SpGetItemStructureByBatchId = "spGetItemStructureByBatchID";
        public const string SpGetItemDataFromOrderbatchItem = "sp_GetItemDataFromOrderbatchItem";
        public const string SpGetItemDataFromOrderbatchItemAndCp = "spGetItemDataFromOrderbatchItemAndCP";
        public const string SpRulesTracking24 = "sp_RulesTracking24";
        public const string SpGetOldNumberByBatchIdItemCode = "sp_GetOldNumberByBatchIDItemCode";
        public const string SpGetDocumentTypeCodeByBatchId = "spGetDocumentTypeCodeByBatchID";
        public const string SpGetKmlDffByItemNumber = "spGetKMLDFFByItemNumber";
        public const string SpGetDocumentValue = "spGetDocumentValue";
        public const string SpGetBatchByCode = "sp_GetBatchByCode";
        public const string SpGetBatch = "spGetBatch";
        public const string SpGetBatchesByCustomerByDateRangeByMemoNumber =
            "sp_GetBatchesByCustomerByDateRangeByMemoNumber";
        public const string SpGetCustomer = "spGetCustomer";
        public const string SpGetCustomers = "spGetCustomers";
        public const string SpGetMemoNumbersByCustomerByDateRange = "sp_GetMemoNumbersByCustomerByDateRange";
        public const string SpBatchExists = "sp_BatchExists";
    }
}