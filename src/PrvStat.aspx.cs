using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.IO;


namespace Corpt
{
	/// <summary>
	/// Summary description for PrvStat.
	/// </summary>
	public partial class PrvStat : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
				
			SqlCommand command = new SqlCommand("select distinct customerprogramname from v0customerprogram where customerprogramname like 'lesli%'");
			
			command.Connection=conn;
			command.CommandType=CommandType.Text;
			
			SqlDataReader reader = command.ExecuteReader();
			if(reader.HasRows)
			{
				tblPrograms.Visible=true;
//				tblPrograms.DataSource=reader;
//				tblPrograms.DataBind();
				DataTable myTable = new DataTable("CPList");
				myTable.Columns.Add(new DataColumn("Select Program:"));
				int i =0;
				while(reader.Read())
				{
					DataRow myRow = myTable.NewRow();
					myRow["Select Program:"] = "<a href=\"PrvStat.aspx?CP="+ i++ +" &Monthly=1&CompanyName=1\">"+reader.GetSqlValue(reader.GetOrdinal("CustomerProgramName")).ToString()+"</a>";
					myTable.Rows.Add(myRow);
				}
			    tblPrograms.DataSource=myTable;
				Session["StatCPList"]=myTable;
				tblPrograms.DataBind();
//				tblPrograms.Visible=true;
			}
			conn.Close();

			if(Utils.NullValue(Request.QueryString["CP"])!="")
			{
				lblProgram.Text=Regex.Replace((Session["StatCPList"] as DataTable).Rows[Int32.Parse(Utils.NullValue(Request.QueryString["CP"]))][0].ToString(),@"<[^>]+>","");

				conn.Open();
				command = new SqlCommand("sp_StatColorClarity2");
			
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
				command.Parameters.Add("@cpName", Regex.Replace((Session["StatCPList"] as DataTable).Rows[Int32.Parse(Utils.NullValue(Request.QueryString["CP"]))][0].ToString(),@"<[^>]+>",""));
				command.Parameters.Add("@monthly", 1);
				command.Parameters.Add("@companyName",1);
			
				command.CommandTimeout = 0;

				reader = command.ExecuteReader();
                
				if(reader.HasRows)
				{
					DataTable statTable = new DataTable("Stat");
					statTable.Columns.Add(new DataColumn("# of Items"));
					statTable.Columns.Add(new DataColumn("Color/Clarity"));
					statTable.Columns.Add(new DataColumn("Value"));
					statTable.Columns.Add(new DataColumn("Month"));
					statTable.Columns.Add(new DataColumn("Year"));
					statTable.Columns.Add(new DataColumn("Vendor"));

					String numberOfItems = "";
					String color_clarity = ""; String prevColor = "";
					String svalue = "";
					String currMonth = ""; String prevMonth = "";
					String currYear = "";  
					String currVendor = ""; String prevVendor = "";

					int totalMonth = 0;
					int totalColor = 0;
					int totalVendor = 0;



					while(reader.Read())
					{
						numberOfItems = reader.GetSqlValue(reader.GetOrdinal("items")).ToString();
						color_clarity = reader.GetSqlValue(reader.GetOrdinal("Measuretitle")).ToString();
						svalue = reader.GetSqlValue(reader.GetOrdinal("mactname")).ToString();
						currMonth = reader.GetSqlValue(reader.GetOrdinal("m")).ToString();
						currYear = reader.GetSqlValue(reader.GetOrdinal("y")).ToString();
						currVendor = reader.GetSqlValue(reader.GetOrdinal("companyName")).ToString();

						if(prevMonth==""&prevColor==""&prevVendor=="")
						{
							prevMonth=currMonth;
							prevColor=color_clarity ;
							prevVendor=currVendor;
						}
                        
						if(prevMonth!=currMonth)
						{
							DataRow statRow2 = statTable.NewRow();
							statRow2["# of Items"]= "";
							statRow2["Color/Clarity"]="Total(Month):";
							statRow2["Value"]=totalMonth.ToString();
							statRow2["Month"]="";
							statRow2["Year"]="";
							statRow2["Vendor"]="";
							statTable.Rows.Add(statRow2);
                            
							totalMonth=0;
							prevMonth=currMonth;
						}

						if(prevColor!=color_clarity)
						{
							DataRow statRow2 = statTable.NewRow();
							statRow2["# of Items"]= "";
							statRow2["Color/Clarity"]="Total:(Col/Clr)";
							statRow2["Value"]=totalColor.ToString();
							statRow2["Month"]="";
							statRow2["Year"]="";
							statRow2["Vendor"]="";
							statTable.Rows.Add(statRow2);
                            
							totalColor=0;
							prevColor=color_clarity ;
						}

						if(prevVendor!=currVendor)
						{
							DataRow statRow2 = statTable.NewRow();
							statRow2["# of Items"]= "";
							statRow2["Color/Clarity"]="Total:(Vendor)";
							statRow2["Value"]=(totalVendor/2).ToString();
							statRow2["Month"]="";
							statRow2["Year"]="";
							statRow2["Vendor"]="";
							statTable.Rows.Add(statRow2);
                            
							totalVendor=0;
							prevVendor=currVendor;
						}

						totalMonth+=Int32.Parse(numberOfItems);
						totalColor+=Int32.Parse(numberOfItems);
						totalVendor+=Int32.Parse(numberOfItems);

						DataRow statRow = statTable.NewRow();
						statRow["# of Items"]=numberOfItems;
						statRow["Color/Clarity"]=color_clarity;
						statRow["Value"]=svalue;
						statRow["Month"]=currMonth;
						statRow["Year"]=currYear;
						statRow["Vendor"]=currVendor;
						statTable.Rows.Add(statRow);
					}
					
					#region Add Last Summs
					if(true)
					{
						DataRow statRow2 = statTable.NewRow();
						statRow2["# of Items"]= "";
						statRow2["Color/Clarity"]="Total(Month):";
						statRow2["Value"]=totalMonth.ToString();
						statRow2["Month"]="";
						statRow2["Year"]="";
						statRow2["Vendor"]="";
						statTable.Rows.Add(statRow2);
                            
						totalMonth=0;
						prevMonth=currMonth;
					}

					if(true)
					{
						DataRow statRow2 = statTable.NewRow();
						statRow2["# of Items"]= "";
						statRow2["Color/Clarity"]="Total:(Col/Clr)";
						statRow2["Value"]=totalColor.ToString();
						statRow2["Month"]="";
						statRow2["Year"]="";
						statRow2["Vendor"]="";
						statTable.Rows.Add(statRow2);
                            
						totalColor=0;
						prevColor=color_clarity ;
					}

					if(true)
					{
						DataRow statRow2 = statTable.NewRow();
						statRow2["# of Items"]= "";
						statRow2["Color/Clarity"]="Total:(Vendor)";
						statRow2["Value"]=(totalVendor/2).ToString();
						statRow2["Month"]="";
						statRow2["Year"]="";
						statRow2["Vendor"]="";
						statTable.Rows.Add(statRow2);
                            
						totalVendor=0;
						prevVendor=currVendor;
					}
					#endregion

					tblStat.Visible=true;

					Session["Stat"]=statTable;

					tblStat.DataSource=statTable;
					tblStat.DataBind();

//					tblStat.DataSource=reader;
//					tblStat.DataBind();
				}
				conn.Close();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdExcel_Click(object sender, System.EventArgs e)
		{
			if(!Directory.Exists(Session["TempDir"].ToString() + Session.SessionID + @"\"))
			{
				Directory.CreateDirectory(Session["TempDir"].ToString() + Session.SessionID + @"\");
			}
			if(Session["Stat"]!=null)
			{
				DataSet dataset = new DataSet();
				dataset.Tables.Add((Session["Stat"] as DataTable));
				Utils.SaveExcell(dataset,this);
				if(File.Exists(Session["TempDir"].ToString() + Session.SessionID + @"\" + "Report.xls"))
				{
					Response.Redirect("\\Corpt\\Temp\\" + Session.SessionID + @"\" + "Report.xls");
				}				
			}
		}
	}
}
