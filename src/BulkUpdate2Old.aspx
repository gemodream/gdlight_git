﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="BulkUpdate2Old.aspx.cs" Inherits="Corpt.BulkUpdate2Old" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">

        function IsOneDecimalPoint(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
            var parts = evt.srcElement.value.split('.');
            if (parts.length > 1 && charCode == 46)
                return false;
            return true;
        }
    </script>
    <div class="demoheading">Bulk Update II</div>
    <div class="navbar nav-tabs">
        <asp:Button runat="server" ID="SaveEnumButton" Text="Save" OnClick="OnSaveMeasureClick" CssClass="btn btn-info" />
        <asp:Button ID="ClearButton" runat="server" Text="Clear" CssClass="btn btn-info" OnClick="OnClearClick" />
    </div>

    <table>
        <tr>
            <td><asp:Label ID="Label2" runat="server" Text="Order"></asp:Label></td>
            <td colspan="4">
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="LoadButton">
                    <asp:TextBox runat="server" ID="OrderField"></asp:TextBox>
                    <asp:Label ID="Label1" runat="server" Text="Batch"></asp:Label>
                    <asp:TextBox runat="server" ID="BatchField"></asp:TextBox>
                    <asp:Button runat="server" ID="LoadButton" Text="Load" OnClick="OnLoadClick" CssClass="btn btn-primary"/>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3">
                <asp:RadioButtonList ID="ModeField" runat="server" CssClass="radio" Width="100%"
                    RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="OnModeChanged">
                    <asp:ListItem Value="cpEnabled" Selected="True">Follow Customer Program</asp:ListItem>
                    <asp:ListItem Value="cpDisabled">Ignore Customer Program</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="vertical-align: top;font-size: small; font-family: Calibri" colspan="4">
                <asp:Label ID="Label4" runat="server" Text="Batches with the same structure as the batch above"></asp:Label><br/>
                <asp:CheckBox class="checkbox" runat="server" Text="Check All" ID="checkAll" AutoPostBack="True" Width="100px"
                    OnCheckedChanged="OnCheckAllClick"></asp:CheckBox><br/>
                <asp:Panel ToolTip="Similar Batches" ID="Panel2" runat="server" Height="105"
                    CssClass="checkbox" Style="width: 100%; height: 105px; vertical-align: top"
                    BorderColor="Silver" BorderStyle="Dotted" BorderWidth="1px" ScrollBars="Vertical"
                    Wrap="True" >
                    
                    <asp:CheckBoxList ID="BatchList" runat="server" RepeatDirection="Horizontal" Width="95%"
                        BorderStyle="None" AutoPostBack="True" OnSelectedIndexChanged="OnBatchListChanged"
                        CellPadding="1" CellSpacing="1" RepeatColumns="10" Style="margin-left: 5px;" DataTextField="BatchCode"
                        DataValueField="BatchId">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td style="vertical-align: top"><asp:Label ID="Label3" runat="server" Text="Part Type"></asp:Label></td>
            <td><asp:DropDownList runat="server" ID="PartsListField" OnSelectedIndexChanged="OnPartChanged" AutoPostBack="True"/></td>
            <td><asp:Label ID="Label5" runat="server" Text="Measure"></asp:Label></td>
            <td><asp:DropDownList runat="server" ID="MeasureListField" OnSelectedIndexChanged="OnMeasureChanged" AutoPostBack="True" /></td>
        </tr>
        <tr>
            <td><asp:Label ID="ValueLabel" runat="server" Text="Value" Style="vertical-align: middle" Visible="False"></asp:Label></td>
            <td >
                <asp:DropDownList runat="server" ID="MeasureEnumField" OnSelectedIndexChanged="OnEnumMeasureChanged"
                    Visible="False" AutoPostBack="True" Width="100%"/>
                <asp:TextBox ID="MeasureNumericField" runat="server" 
                    onkeypress="return IsOneDecimalPoint(event);" AutoPostBack="True"
                    Visible="False"  Width="100%" ToolTip="Numeric Value" BackColor="#CCFFCC"/>
                <asp:TextBox ID="MeasureTextField" runat="server" Visible="False"  Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="4"><asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small;color: Red"></asp:Label></td>
        </tr>

    </table>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="OrderField"
        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order Code is required." />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="OrderField"
        Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>Five, six or seven numeric characters</strong>" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />

    <asp:RequiredFieldValidator runat="server" ID="BatchReq" ControlToValidate="BatchField"
        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Code is required." />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqE" TargetControlID="BatchReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="BatchRegExpr" ControlToValidate="BatchField"
        Display="None" ValidationExpression="^\d{3}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch code in the format:<br /><strong>Three numeric characters</strong>" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqExpr" TargetControlID="BatchRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server"
        FilterType="Custom" ValidChars="01234567890." TargetControlID="MeasureNumericField">
    </ajaxToolkit:FilteredTextBoxExtender>
</asp:Content>
