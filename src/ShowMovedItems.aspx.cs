using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;

namespace Corpt
{
	/// <summary>
	/// Summary description for ShowMovedItems.
	/// </summary>
	public partial class ShowMovedItems : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
            if (Session[SessionConstants.MovedItems] != null)
			{
				DisplayMovedItems();
			}
		}

		private void DisplayMovedItems()
		{
            if (Session[SessionConstants.MovedItems] is List<MovedItemModel>)
            {
                var itemsList = Session[SessionConstants.MovedItems] as List<MovedItemModel>;
                var dgDisplay = new DataGrid { DataSource = FormatTable(itemsList) };
                Controls.Add(dgDisplay);
                dgDisplay.DataBind();
            } else 
            {
                var dsMoved = Session[SessionConstants.MovedItems] as DataSet;
                if (dsMoved == null) return;
                foreach (DataTable dtMoved in dsMoved.Tables)
                {
                    var dtPretty = FormatTable(dtMoved);

                    var dgDisplay = new DataGrid { DataSource = dtPretty };
                    dgDisplay.DataBind();
                    Controls.Add(dgDisplay);
                }
            }
		}
        private DataTable FormatTable(IEnumerable<MovedItemModel> itemsList)
        {
            var dt = new DataTable();
            dt.Columns.Add("Old Item Number");
            dt.Columns.Add("New Item Number");
            dt.AcceptChanges();
            foreach (var movedItemModel in itemsList)
            {
                var row = dt.NewRow();
                row["Old Item Number"] = movedItemModel.FullItemNumber;
                row["New Item Number"] = movedItemModel.NewFullItemNumber;
                dt.Rows.Add(row);
            }
            dt.AcceptChanges();
            return dt;
        }
        //TODO - should be deleted
		private DataTable FormatTable(DataTable dtMoved)
		{
			var dtFormatted = new DataTable();
			dtFormatted.Columns.Add("Old Item Number");
			dtFormatted.Columns.Add("New Item Number");
			dtFormatted.AcceptChanges();

			foreach(DataRow dr in dtMoved.Rows)
			{
				var drFormatted = dtFormatted.NewRow();
				drFormatted["Old Item Number"]=Utils.FullItemNumber(dr["orderCode"].ToString(), dr["BatchCode"].ToString(), dr["ItemCode"].ToString());
				drFormatted["New Item Number"]=Utils.FullItemNumber(dr["newGroupCode"].ToString(), dr["newBatchCode"].ToString(), dr["newItemCode"].ToString());
				dtFormatted.Rows.Add(drFormatted);
			}
			dtFormatted.AcceptChanges();
			return dtFormatted;
		}
	}
}
