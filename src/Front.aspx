﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Front.aspx.cs" Inherits="Corpt.Front" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: #555555;
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel {
	        padding-bottom:2px;
	        color:#5377A9;
	        font-family:Arial, Sans-Serif;
	        font-weight:bold;
	        font-size:1.0em;
        }
    </style>
    <div class="demoarea">
        <div class="demoheading">
            Customer</div>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <Triggers>
                <asp:PostBackTrigger ControlID="lstCustomerList" />
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="CustomerLikeButton" ID="CustomerSearchPanel">
                    <table>
                        <tr>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Height="26px"/>
                            </td>
                            <td style="padding-top: 10px">
                                <asp:TextBox runat="server" ID="CustomerLike" Width="100px"></asp:TextBox>
                            </td>
                            <td style="padding-top: 10px">
                                <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
                            </td>
                            <td style="padding-left: 10px">
                                <a href="CustomerHistory.aspx" target="_blank" id="HistoryRef" runat="server"><b>Customer History</b></a>
                            </td>
                            <td >
                                <asp:Button runat="server" ID="NewCustomerButton" OnClick="OnNewCustomerClick"
                                        Style="margin-left: 10px;margin-top: 2px" Text="New Customer" CssClass="btn btn-info btn-small" />
                            </td>
                            <td><asp:Label runat="server" ID="MessageLabel" ForeColor="Blue"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                <table>
                    <tr>
                        <td style="vertical-align: top; padding-top: 20px">
                            <asp:Panel runat="server" ID="CompanyDetailsPanel" BorderStyle="Solid" BorderWidth="1px" style="width: 1000px"
                                BorderColor="Silver" DefaultButton="CompanyHiddenBtn">
                                <asp:Button runat="server" ID="CompanyHiddenBtn" Style="display: none" Enabled="False"/>
                                <div class="headingPanel">
                                    Company Details<asp:Button runat="server" ID="CustomerUpdateBtn" OnClick="OnUpdateCompanyClick"
                                        Style="margin-left: 50px;margin-top: 2px" Text="Update" CssClass="btn btn-info btn-small" /></div>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;padding-left: 10px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Company Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyName" runat="server" ControlToValidate="CompanyName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Company Name is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" ID="CompanyName"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Short Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyShortName" runat="server" ControlToValidate="ShortName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Short Name is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" ID="ShortName"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Address1
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddr1" runat="server" ControlToValidate="CompanyAddress1"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Address1 is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" ID="CompanyAddress1" /></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Address2
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="CompanyAddress2"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        City
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyCity" runat="server" ControlToValidate="CompanyCity"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="City is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="CompanyCity"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Country, State
                                                        <asp:RequiredFieldValidator ID="CompUsStateValidator" runat="server" ControlToValidate="CompanyUsState"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="US State is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="CountryBtn">
                                                            <table>
                                                                <tr>
                                                                    <td style="padding-top: 2px">
                                                                        <asp:TextBox runat="server" ID="CompanyCountry" Width="60px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:DropDownList ID="CompanyUsState" runat="server" DataTextField="State" DataValueField="StateId"
                                                                            Width="70px" Height="25px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Button runat="server" ID="CountryBtn" OnClick="OnCompanyCountryChanging" Style="display: none" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Zip
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorZip" runat="server" ControlToValidate="CompanyZip1"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Zip is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="CompanyZip1" Width="60px"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="CompanyZip2" Width="60px"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Phone
                                                    </td>
                                                    <td>
                                                        <asp:Panel ID="Panel3" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="CompanyPhoneCode" Width="30px" MaxLength="4"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="CompanyPhone" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="CompanyPhone"
                                                                Mask="999 999 9999" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number" InputDirection="LeftToRight"
                                                                AcceptNegative="None" DisplayMoney="None" ErrorTooltipEnabled="False" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Email
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="CompanyEmail"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="vertical-align: top;padding-left: 20px;">
                                            <table>
                                                <tr>
                                                    <td >
                                                        <asp:Label ID="Label2" Text="ID" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="CustomerCode" Enabled="False"></asp:TextBox>
                                                        <asp:TextBox runat="server" ID="CustomerId" Style="display: none"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        Start Date
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="CustomerStartDate" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        Business Type
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1BusinessType" runat="server" ControlToValidate="BusinessType"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Business Type is required"></asp:RequiredFieldValidator>

                                                    </td>
                                                    <td >
                                                        <asp:DropDownList ID="BusinessType" runat="server" DataTextField="BusinessTypeName"
                                                            DataValueField="BusinessTypeId" Width="150px" Height="26px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        Industry
                                                        <br />
                                                        MemberShip
                                                    </td>
                                                    <td style="vertical-align: top; padding-top: 8px;">
                                                        <asp:CheckBoxList ID="IndustryMembers" runat="server" RepeatDirection="Horizontal"
                                                            BorderStyle="Dotted" BorderWidth="1" AutoPostBack="True" CellPadding="1" CellSpacing="2"
                                                            RepeatColumns="3" DataTextField="Name" DataValueField="Id">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Fax
                                                    </td>
                                                    <td>
                                                        <asp:Panel ID="Panel4" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="CompanyFaxCode" Width="30px" MaxLength="4"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="CompanyFax" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="CompanyFax"
                                                                Mask="999 999 9999" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number" InputDirection="LeftToRight"
                                                                AcceptNegative="None" DisplayMoney="None" ErrorTooltipEnabled="False" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="vertical-align: top; padding-left: 50px;">
                                                        <table style="border: dotted thin silver;">
                                                            <tr>
                                                                <td rowspan="2" style="vertical-align: top; padding-top: 10px; padding-bottom: 10px;padding-left: 10px">
                                                                    <asp:TreeView ID="CompanyMethods" runat="server" ShowCheckBoxes="All" Width="110px"
                                                                        ExpandDepth="0" AfterClientCheck="CheckChildNodes();" PopulateNodesFromClient="true"
                                                                        ShowLines="false" ShowExpandCollapse="false" OnTreeNodeCheckChanged="OnCompanyMethodsCheckChanged"
                                                                        onclick="OnTreeClick(event)">
                                                                        <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                                                        <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                                                        <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                                                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                                    </asp:TreeView>
                                                                </td>
                                                                <td style="vertical-align: middle; padding: 10px">
                                                                    <asp:ImageButton ID="CompanyDownButton" runat="server" ImageUrl="~/Images/ajaxImages/expand_blue.jpg"
                                                                        OnClick="OnCompanyDownClick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: bottom; padding: 10px">
                                                                    <asp:ImageButton ID="CompanyUpButton" runat="server" ImageUrl="~/Images/ajaxImages/collapse_blue.jpg"
                                                                        OnClick="OnCompanyUpClick" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="vertical-align: top;padding-left: 10px">
                                            <asp:Panel runat="server" ID="PermissionsPanel" CssClass="form-horizontal">
                                                <asp:Label ID="Label3" Text="Permissions" Style="text-align: left" runat="server"
                                                    Width="100%" Font-Bold="True"></asp:Label>
                                                <asp:CheckBoxList ID="CompanyPermissionsList" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="True" CellPadding="0" CellSpacing="0" RepeatColumns="1" DataTextField="Name"
                                                    DataValueField="Id">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 10px">
                                            <asp:Panel runat="server" ID="MovementsPanel" CssClass="form-horizontal">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMovements" runat="server" ControlToValidate="HiddenMovements"
                                                    ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="One of the Goods movement methods must be selected">
                                                </asp:RequiredFieldValidator>
                                                <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Goods Movement"></asp:Label>
                                                <asp:TextBox runat="server" Style="display: none" ID="HiddenMovements"></asp:TextBox>
                                                <asp:RadioButtonList ID="MovementsList" runat="server" RepeatDirection="Horizontal"
                                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnCheckedMovement"
                                                    CellPadding="1" CellSpacing="1" RepeatColumns="1" Font-Size="X-Small" DataValueField="Id"
                                                    DataTextField="Name">
                                                </asp:RadioButtonList>
                                                <asp:CheckBox runat="server" ID="UseAccountFlag" Style="padding-left: 7px; font-size: smaller"
                                                    Font-Size="10px" Text="We use their account to ship" OnCheckedChanged="OnUseAccountChecked"
                                                    AutoPostBack="True" />
                                                <table>
                                                    <tr>
                                                        <td style="padding-left: 10px">
                                                            Carrier
                                                            <asp:RequiredFieldValidator ID="CarrierValidator" runat="server" ControlToValidate="CarrierList"
                                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Carrier is required"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="CarrierList" runat="server" DataTextField="Name" DataValueField="Id"
                                                                Width="150px" Height="26px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 10px">
                                                            Account #
                                                            <asp:RequiredFieldValidator ID="AccountValidator" runat="server" ControlToValidate="AccountField"
                                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpCompany" ToolTip="Account is required"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="padding-top: 8px">
                                                            <asp:TextBox runat="server" ID="AccountField"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td style="vertical-align: top; padding-left: 10px" rowspan="2">
                            <asp:UpdatePanel runat="server" ID="PersonsUpdPanel">
                                <Triggers>
                                    
                                    <asp:AsyncPostBackTrigger ControlID="CustomerLikeButton" EventName="Click" />
                                    
                                </Triggers>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PersonListPanel">
                                        <div>
                                            <asp:Button runat="server" ID="NewPersonButton" OnClick="OnAddPersonClick" Style="margin-left: 0px;margin-bottom: 5px" 
                                                Text="New Person" CssClass="btn btn-info btn-small" />
                                            <asp:Label runat="server" ID="PersonCountLabel" Style="padding-left: 20px"></asp:Label>
                                        </div>
                                        <div>
                                            <asp:DataGrid runat="server" ID="PersonsGrid" AutoGenerateColumns="False" CellPadding="3"
                                                DataKeyField="PersonId" OnItemDataBound="OnPersonItemDataBound" OnItemCommand="OnViewPersonCommand">
                                                <Columns>
                                                    <asp:ButtonColumn Text="View" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/search16.png" />
                                                    <asp:BoundColumn DataField="PersonId" HeaderText="PersonId" Visible="False" />
                                                    <asp:BoundColumn DataField="FullName" HeaderText="Name" />
                                                    <asp:BoundColumn DataField="DisplayPosition" HeaderText="Position" />
                                                    <asp:BoundColumn DataField="DisplayPhone" HeaderText="Phone" />
                                                    <asp:BoundColumn DataField="DisplayFax" HeaderText="Fax" />
                                                    <asp:BoundColumn DataField="DisplayCell" HeaderText="Cell" />
                                                    <asp:BoundColumn DataField="DisplayEmail" HeaderText="Email" />
                                                </Columns>
                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" BackColor="White" />
                                            </asp:DataGrid>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
							<!-- Carrier Table -->
							<asp:UpdatePanel runat="server" ID="CarrierPanel">
                                <Triggers>
                                    
                                    <asp:AsyncPostBackTrigger ControlID="CustomerLikeButton" EventName="Click" />
                                    
                                </Triggers>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="CarierListPanel">
                                        <div>
                                            <asp:Button runat="server" ID="NewCarrierButton" OnClick="OnAddPersonClick" Style="margin-left: 0px;margin-bottom: 5px" 
                                                Text="New Carrier" CssClass="btn btn-info btn-small" />
                                            <asp:Label runat="server" ID="Label1" Style="padding-left: 20px"></asp:Label>
                                        </div>
                                        <div>
                                            <asp:DataGrid runat="server" ID="CarrierGrid" AutoGenerateColumns="False" CellPadding="3"
                                                DataKeyField="PersonId" OnItemDataBound="OnPersonItemDataBound" OnItemCommand="OnViewPersonCommand">
                                                <Columns>
                                                    <asp:ButtonColumn Text="View" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/search16.png" />
                                                    <asp:BoundColumn DataField="CarrierId" HeaderText="CarrierId" Visible="False" />
                                                    <asp:BoundColumn DataField="Account" HeaderText="Account" />
                                                    <asp:BoundColumn DataField="Lower Limit" HeaderText="Lower Limit" />
                                                    <asp:BoundColumn DataField="Upper Limit" HeaderText="Upper Limit" />
                                                </Columns>
                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" BackColor="White" />
                                            </asp:DataGrid>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px">
                            
                            <asp:Panel runat="server" ID="PersonDetailsPanel" Width="1000px" BorderStyle="Solid" BorderWidth="1px" BorderColor="Silver" DefaultButton="PersonHiddenBtn">
                                <asp:Button runat="server" ID="PersonHiddenBtn" Style="display: none" Enabled="False"/>
                                <asp:ValidationSummary ID="SumGrpPerson" runat="server" ValidationGroup="ValGrpPerson" />
                                <div class="headingPanel">
                                    Person Details
                                    <asp:Button runat="server" ID="PersonUpdateBtn" OnClick="OnUpdatePersonClick" Style="margin-left: 50px;
                                        margin-top: 2px" Text="Update" CssClass="btn btn-info btn-small" />
                                    <asp:Button runat="server" ID="PersonDeleteBtn" OnClick="OnDeletePersonClick" Style="margin-left: 10px;
                                        margin-top: 2px" Text="Delete" CssClass="btn btn-info btn-small" />
                                    <asp:Label runat="server" ID="UpdatePersonLabel" ForeColor="Blue"></asp:Label>
                                </div>
                                <table style="margin: 10px">
                                    <tr>
                                        <td style="vertical-align: top">
                                            <table>
                                                <tr>
                                                    <td>First Name
                                                        <asp:RequiredFieldValidator ID="ValFirstName" runat="server" ControlToValidate="PersonFirstName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person First Name is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="PersonFirstName"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="PersonLastName"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Person Last Name is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" ID="PersonLastName"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Birth Date</td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="PersonBirthDate" OnTextChanged="OnChangedBirthDate" AutoPostBack="True">
                                                        </asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExt" runat="server" ClearTime="True" 
                                                            TargetControlID="PersonBirthDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Address1</td>
                                                    <td><asp:TextBox runat="server" ID="PersonAddress1"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Address2</td>
                                                    <td><asp:TextBox runat="server" ID="PersonAddress2"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>City</td>
                                                    <td><asp:TextBox runat="server" ID="PersonCity"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Country, State</td>
                                                    <td>
                                                        <asp:Panel ID="PersonStatePanel" runat="server" CssClass="form-inline" DefaultButton="PersonCountryBtn">
                                                            <table>
                                                                <tr>
                                                                    <td style="padding-top: 2px">
                                                                        <asp:TextBox runat="server" ID="PersonCountry" Width="60px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:DropDownList ID="PersonUsState" runat="server" DataTextField="State" DataValueField="StateId"
                                                                            Width="70px" Height="25px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Button runat="server" ID="PersonCountryBtn" OnClick="OnPersonCountryChanging"
                                                                Style="display: none" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Zip</td>
                                                    <td>
                                                        <asp:Panel ID="PersonZipPanel" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="PersonZip1" Width="60px"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="PersonZip2" Width="60px"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: right">
                                                        <asp:CheckBox runat="server" ID="AsCompanyAddress" Text="Same as Company Address" 
                                                            OnCheckedChanged="OnAsCompanyChecked" AutoPostBack="True"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Cell</td>
                                                    <td>
                                                        <asp:Panel ID="PersonCellPanel" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="PersonCellCode" Width="30px" MaxLength="4"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="PersonCell" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="ExtPersonCell" runat="server" TargetControlID="PersonCell"
                                                                Mask="999 999 9999" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                                AcceptNegative="None" DisplayMoney="None" ErrorTooltipEnabled="False" 
                                                                ClearMaskOnLostFocus="False" ClearTextOnInvalid="False" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><asp:TextBox runat="server" ID="PersonEmail"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="vertical-align: top;padding-left: 20px">
                                            <table>
                                                <tr>
                                                    <td>ID</td>
                                                    <td><asp:TextBox runat="server" ID="PersonId" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Start Date</td>
                                                    <td><asp:TextBox runat="server" ID="PersonStartDate" Enabled="False"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Position
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPosition" runat="server" ControlToValidate="PersonPosition"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Position is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="PersonPosition" runat="server" DataTextField="Name" DataValueField="Id"
                                                            Height="26px" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td>Phone</td>
                                                    <td style="padding-top: 10px">
                                                        <asp:Panel ID="PersonPhonePanel" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="PersonPhoneCode" Width="30px" MaxLength="4"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="PersonPhone" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="PersonPhone"
                                                                Mask="999 999 9999" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number" InputDirection="LeftToRight"
                                                                AcceptNegative="None" DisplayMoney="None" ErrorTooltipEnabled="False" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Ext</td>
                                                    <td><asp:TextBox runat="server" ID="PersonPhoneExt"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Fax</td>
                                                    <td>
                                                        <asp:Panel ID="PersonFaxPanel" runat="server" CssClass="form-inline">
                                                            <asp:TextBox runat="server" ID="PersonFaxCode" Width="30px" MaxLength="4"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="PersonFax" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="PersonFax"
                                                                Mask="999 999 9999" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number" InputDirection="LeftToRight"
                                                                AcceptNegative="None" DisplayMoney="None" ErrorTooltipEnabled="False" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="2" style="vertical-align: top; padding-left: 50px;padding-top: 10px">
                                                        <table style="border: dotted thin silver;">
                                                            <tr>
                                                                <td rowspan="2" style="vertical-align: top; padding-top: 2px; padding-bottom: 10px;
                                                                    padding-left: 10px">
                                                                    <asp:TreeView ID="PersonMethods" runat="server" ShowCheckBoxes="All" Width="110px"
                                                                        ExpandDepth="0" AfterClientCheck="CheckChildNodes();" PopulateNodesFromClient="true"
                                                                        ShowLines="false" ShowExpandCollapse="false" OnTreeNodeCheckChanged="OnPersonMethodsCheckChanged"
                                                                        onclick="OnTreeClick(event)">
                                                                        <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                                                        <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                                                        <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                                                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                                    </asp:TreeView>
                                                                </td>
                                                                <td style="vertical-align: middle; padding: 10px">
                                                                    <asp:ImageButton ID="PersonDownButton" runat="server" ImageUrl="~/Images/ajaxImages/expand_blue.jpg"
                                                                        OnClick="OnPersonDownClick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: bottom; padding: 10px">
                                                                    <asp:ImageButton ID="PersonUpButton" runat="server" ImageUrl="~/Images/ajaxImages/collapse_blue.jpg"
                                                                        OnClick="OnPersonUpClick" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 20px">
                                            <table>
                                                <tr>
                                                    <td style="vertical-align: top; padding-left: 10px; margin-left: 5px">
                                                        <asp:Panel runat="server" ID="PersonPermissionsPanel" CssClass="form-horizontal">
                                                            <asp:Label ID="Label6" Text="Permissions" Style="text-align: left" runat="server"
                                                                Width="100%" Font-Bold="True"></asp:Label>
                                                            <asp:CheckBoxList ID="PersonPermissionsList" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" CellPadding="0" CellSpacing="0" RepeatColumns="1" DataTextField="Name"
                                                                DataValueField="Id">
                                                            </asp:CheckBoxList>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="vertical-align: top;padding-left: 20px">
                                            <table>
                                                <tr>
                                                    <td colspan="2"><b>Web</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Login
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorWebLogin" runat="server" ControlToValidate="WebLogin"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Web login is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" ID="WebLogin"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Password
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ControlToValidate="WebPassword"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Web password is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" TextMode="Password" ID="WebPassword" ></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Re-type <br/>Password
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRePassword" runat="server" ControlToValidate="RePassword"
                                                            ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Re-type password is required"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td><asp:TextBox runat="server" TextMode="Password" ID="RePassword"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none;border: solid 2px lightgray;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="ExtCompanyZip1" runat="server" FilterType="Custom"
                    ValidChars="0123456789" TargetControlID="CompanyZip1">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="ExtCompanyZip2" runat="server" FilterType="Custom"
                    ValidChars="0123456789" TargetControlID="CompanyZip2">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="ExtPersonZip1" runat="server" FilterType="Custom"
                    ValidChars="0123456789" TargetControlID="PersonZip1">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="ExtPersonZip2" runat="server" FilterType="Custom"
                    ValidChars="0123456789" TargetControlID="PersonZip2">
                </ajaxToolkit:FilteredTextBoxExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

