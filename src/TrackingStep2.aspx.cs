using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for TrackingStep2.
	/// </summary>
	public partial class TrackingStep2 : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			ShowInfo();
			Session["trP1"]=Request.Params["p1"];
			Session["trP2"]=Request.Params["p2"];
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowInfo()
		{
			string strMode = Utils.NullValue(Request.Params["p1"]);
			string strData = Utils.NullValue(Request.Params["p2"]);

///			lblInfo.Text=strMode+"&nbsp;:&nbsp;"+strData;

			if(strMode!="")
			{
				SqlCommand command = new SqlCommand("wspvvGetBatchList2WithDateRangeWithOrderMemo");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				lblDebug.Text=command.Connection.Database+"&nbsp;:&nbsp;"+command.Connection.DataSource;
				command.CommandType = CommandType.StoredProcedure;

////				Session["CustomerID"]=lstCustomerList.SelectedValue.Split('_')[1];
////				Session["CustomerOfficeID"]=lstCustomerList.SelectedValue.Split('_')[0];

				command.Parameters.Add(new SqlParameter("@CustomerID", (Session["CustomerID"] as string)));
				command.Parameters.Add(new SqlParameter("@CustomerOfficeID", (Session["CustomerOfficeID"] as string)));
				command.Parameters.Add(new SqlParameter("@VendorID", DBNull.Value));
				command.Parameters.Add(new SqlParameter("@VendorOfficeID", DBNull.Value));
				switch(strMode)
				{
					case "Order":
					{
						command.Parameters.Add(new SqlParameter("@OrderCode", int.Parse(strData)));
						lblInfo.Text="Order: "+strData;
						ShowTrackingInfo(int.Parse(strData));
						break;
					}
					case "CP":
					{
						//Customer Program
//////						Session["dtFrom"]=dtFrom;
//////						Session["dtTo"]=dtTo;
						command.Parameters.Add(new SqlParameter("@CustomerProgramName", strData));
						command.Parameters.Add(new SqlParameter("@DateFrom", DateTime.Parse(Session["dtFrom"].ToString())));
						command.Parameters.Add(new SqlParameter("@DateTo", DateTime.Parse(Session["dtTo"].ToString()).AddDays(1)));
						lblTrackingInfo.Text="";
						break;
					}
					case "Memos":
					{
						//Memo NUmber
						command.Parameters.Add(new SqlParameter("@MemoNumber", strData));
						command.Parameters.Add(new SqlParameter("@DateFrom", DateTime.Parse(Session["dtFrom"].ToString())));
						command.Parameters.Add(new SqlParameter("@DateTo", DateTime.Parse(Session["dtTo"].ToString()).AddDays(1)));
						lblTrackingInfo.Text="";
						break;
					}
				}
				
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtData = new DataTable();
				da.Fill(dtData);

//				dgDebug.DataSource=dtData;
//				dgDebug.DataBind();

				DataTable dtDisplay = new DataTable();

				dtDisplay = PrettyTable(dtData);
				
				dgDisplay.DataSource = dtDisplay;
				dgDisplay.DataBind();


				if(dtData.Rows.Count<1)
				{
					lblInfo.Text="Could not load the order.";
				}
				else
				{
					try
					{
						lblInfo.Text=DateTime.Parse(command.Parameters["@DateFrom"].Value.ToString()).ToShortDateString()+"&nbsp;&mdash;&nbsp;"+DateTime.Parse(command.Parameters["@DateTo"].Value.ToString()).ToShortDateString();
					}
					catch(Exception ex)
					{
						lblInfo.Text=ex.Message;
					}
				}
			}			
		}

		private DataTable PrettyTable(DataTable dtData)
		{
			DataTable dtDisplay = new DataTable();

			dtDisplay.Columns.Add("Order");
			dtDisplay.Columns.Add("Memo");
			dtDisplay.Columns.Add("Batch");
			dtDisplay.Columns.Add("Date");
			dtDisplay.Columns.Add("Customer Program");
			dtDisplay.AcceptChanges();

			foreach(DataRow dr in dtData.Rows)
			{
				DataRow drP = dtDisplay.NewRow();
				drP["Order"] = "<a href=\"TrackingStep2.aspx?p1=Order&p2="+HttpUtility.UrlEncode(dr["OrderCode"].ToString())+"\">"+dr["OrderCode"].ToString()+"</a>";
				drP["Memo"]= "<a href=\"TrackingStep2.aspx?p1=Memos&p2="+HttpUtility.UrlEncode(dr["MemoNumber"].ToString())+"\">"+dr["MemoNumber"].ToString()+"</a>";
				drP["Batch"]="<a href=\"TrackingStepOrderDetail.aspx?p2="+dr["BatchID"].ToString()+"\">"+dr["Order_Batch"].ToString()+"</a>";
				drP["Date"]=DateTime.Parse(dr["LastModifiedDate"].ToString()).ToShortDateString();
				drP["Customer Program"]="<a href=\"TrackingStep2.aspx?p1=CP&p2=" +HttpUtility.UrlEncode(dr["CustomerProgramName"].ToString()) + "\">"+ dr["CustomerProgramName"].ToString() +"</a>";

				dtDisplay.Rows.Add(drP);
			}

			return dtDisplay;
		}

		private void ShowTrackingInfo(int intOrderCode)
		{
			SqlCommand command = new SqlCommand("sp_GetTrackingInfo");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@OrderCode",intOrderCode));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dtTrackingInfo = new DataTable();

			da.Fill(dtTrackingInfo);

			if(dtTrackingInfo.Rows.Count>0)
			{
				lblTrackingInfo.Text="<b>Method of ddelivery: </b>Received from " + Utils.FormatCarrierString(dtTrackingInfo.Rows[0]["CarrierTrackingNumber"].ToString(),Int32.Parse(dtTrackingInfo.Rows[0]["carriercode"].ToString()));
			}
			else
			{
				lblTrackingInfo.Text="";
			}			
		}

		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Middle.aspx");
		}

		protected void cmdTrackingStart_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("TrackingStart.aspx");
		}
	}
}
