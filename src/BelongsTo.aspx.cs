﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using Corpt.Utilities;

namespace Corpt
{
    public partial class BelongsTo : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Report belongs to";
            if (IsPostBack) return;
            InfoLabel.Text = "";
            CustomerNameFld.Text = "";
        }

        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            InfoLabel.Text = "";
            CustomerNameFld.Text = "";
            
            var number = NumberField.Text.Trim();
            if(Regex.IsMatch(number, @"^[A-Z,a-z]\d{10}$|^[A-Z,a-z]\d{11}$"))
            {
                number = number.Substring(1);
            }
            if(Regex.IsMatch(number, @"^\d{10}$|^\d{11}$"))
            {
                var name = QueryUtils.GetCustomerByItemNumber(number, this);
                if (string.IsNullOrEmpty(name)) InfoLabel.Text = "Could not find item " + number;
                CustomerNameFld.Text = name;
            } else
            {
                InfoLabel.Text = "Could not find item " + number;
            }
        }
    }
}