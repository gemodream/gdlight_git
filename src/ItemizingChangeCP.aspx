﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ItemizingChangeCP.aspx.cs" Inherits="Corpt.ItemizingChangeCP" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
    <div class="demoheading">Itemizing Old/New Change CP</div>
    <div class="navbar nav-tabs">
        <asp:DropDownList runat="server" ID="CpListField" OnSelectedIndexChanged="OnCpChanged" AutoPostBack="True"/>
        <asp:Button runat="server" ID="CPCopyButton" Text="SELECT" OnClick="OnCPCopyClick" CssClass="btn btn-info"></asp:Button>
        <asp:Button runat="server" ID="CPClearButton" Text="CANCEL" OnClick="OnCPClearClick" CssClass="btn btn-info"></asp:Button>
    </div>
    <table border="1">
        <tr>
            <td>
                <asp:Label runat="server" style="width: 350px" ID="OldCP" Text="Old CP:" ForeColor="black"></asp:Label>
            </td>
            <td>
                <asp:Label runat="server" style="width: 350px;padding-left: 20px;" ID="NewCP" ForeColor="Black" Text="New CP:"></asp:Label> </td>
        </tr>
        <tr>
            <!-- Old customer id -->
            <td>
                <asp:Panel runat="server" ID="OldCustomerIDPanel">
                    <asp:Label runat="server" ID="OldCustomerIDLabel" Text="Old Customer ID" ForeColor="black" ToolTip="Old Customer ID"></asp:Label>
                    <asp:TextBox ID="OldCustomerIDBox" runat="server" ></asp:TextBox>
                </asp:Panel>
            </td>
            <!-- New Customer ID -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="NewCustomerIDPanel" runat="server">
                    <asp:Label runat="server" ID="NewCustomerIDLabel" Text="New Customer ID" ForeColor="black" ToolTip="New Customer ID"></asp:Label>
                    <asp:TextBox runat="server" ID="NewCustomerIDBox"></asp:TextBox>
                    <asp:HiddenField ID="NewOrder" runat="server" />
                    <asp:HiddenField ID="OldItem" runat="server" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <!-- Old Customer Style -->
            <td>
                <asp:Panel runat="server" ID="OldCustomerStylePanel">
                    <asp:Label runat="server" ID="OldCustomerStyleLabel" Text="Old Customer Style" ForeColor="black" ToolTip="Old Customer Style"></asp:Label>
                    <asp:TextBox ID="OldCustomerStyleBox" runat="server" ></asp:TextBox>
                </asp:Panel>
            </td>
            <!-- New Customer Style -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="NewCustomerStylePanel" runat="server">
                    <asp:Label runat="server" ID="NewCustomerStyleLabel" Text="New Customer Style" ForeColor="black" ToolTip="New Customer Style"></asp:Label>
                    <asp:TextBox runat="server" ID="NewCustomerStyleBox"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <!-- Old SRP -->
            <td>
                <asp:Panel runat="server" ID="OldSRPPanel">
                    <asp:Label runat="server" ID="OldSRPLabel" Text="Old SRP" ForeColor="black" ToolTip="Old SRP"></asp:Label>
                    <asp:TextBox ID="OldSRPBox" runat="server" ></asp:TextBox>
                </asp:Panel>
            </td>
            <!-- New SRP -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="NewSRPPanel" runat="server">
                    <asp:Label runat="server" ID="NewSRPLabel" Text="New SRP" ForeColor="black" ToolTip="New SRP"></asp:Label>
                    <asp:TextBox runat="server" ID="NewSRPBox"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <!-- Old Description -->
            <td>
                <asp:Panel runat="server" ID="OldDescPanel">
                    <asp:Label runat="server" ID="OldDescLabel" Text="Old Description" ForeColor="black" ToolTip="Old SRP"></asp:Label>
                    <asp:TextBox ID="OldDescBox" TextMode="multiline" Columns="50" Rows="5" runat="server" ></asp:TextBox>
                </asp:Panel>
            </td>
            <!-- New Description -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="NewDescPanel" runat="server">
                    <asp:Label runat="server" ID="NewDescLabel" Text="New Description" ForeColor="black" ToolTip="New SRP"></asp:Label>
                    <asp:TextBox runat="server" TextMode="multiline" Columns="50" Rows="5" ID="NewDescBox"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <!-- Old Comment -->
            <td style="height: 94px">
                <asp:Panel runat="server" ID="OldCommentPanel">
                    <asp:Label runat="server" ID="OldCommentLabel" Text="Old Comments" ForeColor="black" ToolTip="Old Comment"></asp:Label>
                    <asp:TextBox ID="OldCommentBox" TextMode="multiline" Columns="50" Rows="5" runat="server" ></asp:TextBox>
                </asp:Panel>
            </td>
            <!-- New Comment -->
            <td style="padding-left: 20px; height: 94px;">
                <asp:Panel ID="NewCommentPanel" runat="server">
                    <asp:Label runat="server" ID="NewCommentLabel" Text="New Comments" ForeColor="black" ToolTip="New Comment"></asp:Label>
                    <asp:TextBox runat="server" TextMode="multiline" Columns="50" Rows="5" ID="NewCommentBox"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <!-- Old Picture -->
            <td>
                <asp:Panel runat="server" ID="OldPicturePanel">
                    <asp:Label runat="server" ID="OldPicture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                    <asp:Image id="OldItemPicture"  runat="server" Width="200px" Visible="True" BorderWidth="1"></asp:Image>
                </asp:Panel>
            </td>
            <!-- New Picture -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="NewPicturePanel" runat="server">
                    <asp:Label runat="server" ID="NewPicture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                    <asp:Image id="NewItemPicture" ImageUrl="~/Images/ajaxImages/information24.png" runat="server" Width="200px" Visible="True" BorderWidth="1"></asp:Image>
                </asp:Panel>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
