<%@ Page Language="c#" CodeBehind="BulkItemDataEntry.aspx.cs" AutoEventWireup="True" 
Inherits="Corpt.BulkItemDataEntry" MasterPageFile="~/DefaultMaster.Master" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>



<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

  <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1"></ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript" src="style/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="style/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="style/bootstrap.js"></script>
    <script type="text/javascript" src="style/jquery-ui.js"></script>
    
    <style href="Style/bootstrap2.0/css/bootstrap.css" rel="stylesheet" type="text/css" ></style>
    <style href="style/jquery-ui.css" rel="stylesheet" type="text/css" ></style>

    <style type="text/css">
        .demoarea {
            padding: 10px;
            margin-left: 10px;
            margin-top: 10px;
        }

        .demoheading {
            padding-bottom: 5px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.5em;
        }


        .panelmargin {
            margin-left: 10px;
            margin-top: 2px;
            margin-bottom: 2px;
        }

        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            opacity: 0.8;
            min-height: 25%;
            width: 100%;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            border: none;
            position: absolute;
            display: none;
            background-color: White;
            z-index: 999;
            height: 100%;
            width: 100%;
            margin: 0;
        }
         
        input[type="file"]::-webkit-file-upload-button
        {
            vertical-align: middle;
            height: 100%;   
            border-radius:5px;
            background-color:white;   
            border:1px solid #cccccc;    
        }
        #master_content
        {
            padding-top:20px;
        }
       #master_contentplaceholder
       {
           padding :5px 5px 5px 5px;
       }
       .GridviewScrollHeader TD{
           padding: 0px 0px 0px 5px !important;
            font-family: sans-serif !important;
       }
       .GridviewScrollItem TD{
           padding: 0px 0px 0px 5px !important;
           font-family: sans-serif !important;
       }
    </style>

     <script type="text/javascript">                        
            
         function ShowProgress() {
             setTimeout(function () {
                 var modal = $('#divLoading');
                 modal.addClass("modal");
                 $('body').append(modal);
                 var loading = $(".loading");
                 loading.show();
                 var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                 var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                 loading.css({ top: top, left: left });
             }, 200);

         }

         $().ready(function () {
             
             $("#cmdSave").click(function () {
                 ShowProgress();
             });

             $("#btnBulkInsert").click(function () {
                 ShowProgress();
             });

             $("#btnDownload").click(function () {
                 if ($("#CustomerList").val().trim() == "")
                    { alert('Please select Customer and Program'); return false; }
                 else if ($("#CpList").val().trim() == "")
                    { alert('Please select Customer and Program'); return false; }

             });
             
             $("#btnUploadDataFile").click(function () {
                
                 if ($("#fuDataFile").val().trim() == "")
                 {
                     alert('Please select file to upload.'); return false;
                 }
                 else
                 {
                     Confirm();
                 }

             });

             $("#btnClear").click(function () {
                 $("#CustomerList").prop('selectedIndex', 0);
             });
            
         });

         function Confirm() {
             var confirm_value = document.createElement("INPUT");
             confirm_value.type = "hidden";
             confirm_value.name = "confirm_value";
             if (confirm("Do you want to upload excel data?")) {
                 confirm_value.value = "Yes";
             } else {
                 confirm_value.value = "No";
             }
             document.forms[0].appendChild(confirm_value);
         }

    </script>
        <div class="demoarea">
            <div class="demoheading" style="height: 30px;">Bulk Item Data Entry</div>

            <table style="width: 850px;" border="0">
                <tr>
                    <td style="text-align: left; padding-right: 0px; vertical-align: top; width: 200px;">
                        <table>
                            <tr style="height: 0px;">
                                <td>
                                    Select Excel File:  
                                </td>
                            </tr>
                            <tr style="height: 0px;">
                                <td>                     
                                    <asp:FileUpload ID="fuDataFile" runat="server" Style="width: 200px; font-size: 12px;" />
                                </td>
                            </tr>
                            <tr style="height: 0px;">
                                <td>
                                    <asp:Button runat="server" ID="btnUploadDataFile" Text="Get Excel Data" CssClass="btn btn-primary btn-sm" Style="vertical-align: top!important;" OnClick="btnUploadDataFile_Click" />
                                    <asp:Button runat="server" ID="btnClear" Text="Clear" CssClass="btn btn-primary btn-sm" Style="margin-left: 3px; vertical-align: top!important;" OnClick="btnClear_Click" />
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td style="text-align: left; padding-right: 0px; vertical-align: top; width: 550px;">
                    <fieldset">
    <%--<legend>Download Templet</legend>--%>
                    
                        <table>
                            <tr style="height: 30px;">
                               
                                <td style="padding-left: 5px">
                                    Customer: <asp:DropDownList ID="CustomerList" runat="server" DataTextField="CustomerName" CssClass="form-control input-sm"
                                        AutoPostBack="True" DataValueField="CustomerId" ToolTip="Customers List" Width="300px" OnSelectedIndexChanged="OnCustomerSelectedChanged" />
                                </td>
                                <td style="padding-left: 5px; width: auto">
                                    <asp:TextBox runat="server" ID="CustomerLike" Width="130px" CssClass="form-control input-sm" Style="display: inline-block;"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/search.png" Style="margin-left: 0px; display: inline-block; vertical-align: middle;" OnClick="OnCustomerSearchClick" />
                                </td>

                            </tr>

                            <tr style="height: 30px;">
                                
                                <td style="padding-left: 5px">
                                    Programs: <asp:DropDownList ID="CpList" runat="server" AutoPostBack="True" DataTextField="CustomerProgramName"
                                        CssClass="form-control input-sm" DataValueField="CpId" ToolTip="Customer Program List" Width="300px" OnSelectedIndexChanged="OnCpSelectedChanged" />
                                </td>
                                <td style="padding-left: 5px; vertical-align: top;">
                                    <asp:Button runat="server" ID="btnDownload" Text="Download Template" CssClass="btn btn-primary btn-sm" OnClick="OnDownloadTemplateClick" />
                                </td>


                            </tr>
                        </table>
                        </fieldset>
                    </td>

                </tr>
            </table>

            <table>

                <asp:UpdatePanel runat="server" ID="upPanel" UpdateMode="Conditional">
                    <ContentTemplate>
                <table>
                        <tr>
                            <td style="padding-top: 0px; vertical-align: top;width:100px;">
                                <asp:ListBox ID="lstItemList" runat="server" Width="192px" CssClass="text" Style="vertical-align: top;"
                                    Height="362px" AutoPostBack="false" Rows="10" ></asp:ListBox> <%--OnSelectedIndexChanged="OnItemListSelectedChanged"--%>

                                <asp:Label runat="server" ID="CpPathToPicture" Visible="False"></asp:Label>
                                <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image>
                                <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                                
                            </td>
                            <td style="padding-left: 10px; vertical-align: top;width:300px;display:none;">

                                <div style="border: thin solid #C0C0C0; overflow: auto; height: 360px; font-family: Cambria; font-size: 15px; width: 450px;">
                                    <asp:Panel ID="pnlRight" runat="server" CssClass="panelmargin" Wrap="False" ViewStateMode="Enabled" HorizontalAlign="Left">
                                    </asp:Panel>
                                </div>

                            </td>
                            <td style="padding-left: 10px; vertical-align: top; margin: 0; width: 200px;">
                                <div style="width:550px;height:362px;    overflow-y: scroll;    border: solid 1px #cccccc;">
                                <%--<asp:TextBox ID="txtItemStatus" runat="server" Width="300px" Height="362px" CssClass="text" Style="vertical-align: top; overflow-x: auto;" TextMode="MultiLine"/>--%>
                                    <asp:DataGrid ID="gvItemStatus" runat="server" CellPadding="5" AllowSorting="True" Width="550px">
                                        <HeaderStyle CssClass="GridviewScrollHeader" />
                                        <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
                                        <PagerStyle CssClass="GridviewScrollPager" />
                                    </asp:DataGrid>
                             </div>
                                <asp:ListBox ID="lstItemStatus" runat="server" Width="0px" Height="362px" CssClass="text" Style="vertical-align: top; overflow-x: auto;display:none;" Visible="false"></asp:ListBox>

                            </td>
                        </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMsg" ForeColor="Green" runat="server" ></asp:Label>
                        </td>
                    </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="lstItemList" />
                    </Triggers>
                </asp:UpdatePanel>

                <tr>
                    <td style="vertical-align: top; display: none;"></td>
                    <td style="text-align: left; padding-right: 40px; vertical-align: top;">
                        <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                            <asp:CheckBox ID="IgnoreCpFlag" runat="server" CssClass="radio-inline" Text="Ignore Cp" Checked="True" Visible="False"  />
                            <asp:TextBox type="text" ID="UniqKeyFld" runat="server" AutoPostBack="True" Visible="False" />
                            <!-- 'Save' button -->
                          
                            <asp:Button ID="btnBulkInsert" CssClass="btn btn-primary" runat="server" Text="Bulk Save" Style="text-align: right; margin-top: 10px; margin-left: 10px;vertical-align: text-bottom;" OnClick="btnBulkInsert_Click"></asp:Button>
                            <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back" Style="text-align: right; margin-top: 10px;  vertical-align: text-bottom; display:none;" OnClick="btnBack_Click" ></asp:Button>
                        </asp:Panel>
                    </td>

                </tr>

            </table>

        </div>

        <div id="divLoading" class="loading">
            <table style="text-align: center; vertical-align: middle; height: 100%; width: 100%; margin: 0; padding: 0;">
                <tr>
                    <td>
                        <b>Saving. Please wait.</b>
                        <br />
                        <br />
                        <img src="Images/loader.gif" />
                    </td>
                </tr>
            </table>

        </div>

   
</asp:Content>






