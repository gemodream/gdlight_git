﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningPacking.aspx.cs" Inherits="Corpt.ScreeningPacking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

            input[type="checkbox"], label {
                margin-top: 0;
                margin-bottom: 0;
                line-height: normal;
            }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }
    </style>

    <script type="text/javascript">
	<%--	$().ready(function () {
			$('#<%=txtOrderCode.ClientID%>, #<%=txtItemwithFailedStone.ClientID%>, #<%=txtTotalFaildStonePerOrder.ClientID%>, #<%=txtBlueBags.ClientID%>').keypress(function (event) {
				return isOnlyNumber(event, this);
			});
		});
		function isOnlyNumber(evt, element) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode < 48 || charCode > 57)
				return false;
			return true;
		}--%>
</script>
    <asp:UpdatePanel runat="server" ID="MainPanel">
        <ContentTemplate>

            <div class="demoarea">

                <div style="font-size: smaller; height: 25px;">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                        <ProgressTemplate>
                            <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                            <b>Please, wait....</b>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <table>
                    <tr>
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <div class="demoheading" style="width: 150px;">
                                            Packing View
                                        </div>
                                    </td>
                                    <td>
                                        <div class="demoheading">
                                            <asp:Label runat="server" ID="lblInvalidLabel" ForeColor="Red" Style="padding-left: 7px" Width="600px"></asp:Label>
                                            <br />
                                        </div>
                                    </td>

                                </tr>
                            </table>


                        </td>
                        <td></td>

                    </tr>
                    <tr>
                        <td style="vertical-align: top; height: 30px; width: 320px;">
                            <asp:TextBox ID="txtOrderCode" runat="server" MaxLength="7" placeholder="Order code" Style="width: 115px;" OnFocus="this.select();" autocomplete="off"
                                onkeypress="return event.charCode >= 48 && event.charCode <= 57" ValidationGroup="BatchGroup"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderCode"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderCode"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>six or seven numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:Button ID="btnLoad" runat="server" Text="Load" CssClass="btn btn-info btn-large"
                                Style="margin-bottom: 0px; margin-left: 10px;" OnClick="btnLoad_Click" />
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers" TargetControlID="txtOrderCode"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td></td>

                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <table id="tblDetail" runat="server" visible="false" cellpadding="3" style="width: 550px;">
                                

                                <tr>

                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Total number of items per order"></asp:Label>
                                    </td>
                                    <td style="vertical-align: middle; height: 40px;">
                                        <asp:Label ID="lblNoofItems" runat="server" Text="0" Style="padding-bottom: 10px;"></asp:Label>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Total number of pass items per order"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTotalPassItemsPerOrder" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label ID="Label1" runat="server" Style="margin-right: 10px;" Text="Number of items with failed stones per order"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTotalFailItemsPerOrder" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Total number of failed stones per order"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTotalFaildStonePerOrder" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>

                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Number of blue bags per order"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBlueBagsPerOrder" runat="server" Text="0" MaxLength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td style="vertical-align: top;">
                                        <asp:Label ID="Label8" runat="server" Text="Billing Info"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBillingInfo" runat="server" Height="70px" TextMode="MultiLine" Width="200px" MaxLength="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td style="vertical-align: top;">
                                        <asp:Label ID="Label7" runat="server" Text="Comment"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtComment" runat="server" Height="70px" TextMode="MultiLine" Width="200px" MaxLength="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info btn-large" OnClick="btnSave_Click" Text="Save" Visible="false" Style="margin-right: 10px;" />
                                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-info btn-large" Text="Clear" OnClick="btnClear_Click" Visible="false" />
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                        <td style="text-align: right;">&nbsp;</td>
                    </tr>
                </table>
            </div>
            <%-- Information Dialog --%>
            <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray; margin-top: 25%;">
                <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                    <div>
                        <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                        <b>Information</b>
                    </div>
                </asp:Panel>
                <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
                    id="MessageDiv" runat="server">
                </div>
                <div style="padding-top: 10px">
                    <p style="text-align: center; font-family: sans-serif;">
                        <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
                    </p>
                </div>
            </asp:Panel>
            <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>
