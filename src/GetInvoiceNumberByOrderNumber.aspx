﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="GetInvoiceNumberByOrderNumber.aspx.cs" Inherits="Corpt.GetInvoiceNumberByOrderNumber" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>

    <div class="demoarea">
        <div class="demoheading">Get Invoice Number</div>
        <asp:Panel runat="server" DefaultButton="LookupButton" CssClass="form-inline" >
            
            <!-- 'Order Number' textbox -->
            <asp:TextBox type="text" ID="txtOrderNumber" MaxLength="9" runat="server" name="txtOrderNumber" placeHolder="Order number" 
                Style="font-weight: bold; width: 100px;" AutoPostBack="True" />
            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderNumber"
                Display="None" 
                ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required." 
                ValidationGroup="OrderGroup" />
            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                HighlightCssClass="validatorCalloutHighlight" />
            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderNumber"
                Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" 
                ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>" 
                ValidationGroup="OrderGroup" />
            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                HighlightCssClass="validatorCalloutHighlight" />
            <asp:Button ID="LookupButton" class="btn btn-primary" runat="server" 
                Text="Lookup" OnClick="OnLookupClick">
            </asp:Button>
        </asp:Panel>
        <br/>
        <div id="gridContainer" style="font-size: small">
            <asp:DataGrid ID="dgResult" runat="server" Enabled="True" AutoGenerateColumns="False" CellPadding="5">
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                <Columns>
                    <asp:BoundColumn DataField="InvoiceNumber" HeaderText="Invoice Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="GroupCode" HeaderText="Group Code"></asp:BoundColumn>
                    <asp:BoundColumn DataField="BatchCode" HeaderText="Batch Code"></asp:BoundColumn>
                    <asp:BoundColumn DataField="BeginDate" HeaderText="Invoice Date"></asp:BoundColumn>
                    <asp:BoundColumn DataField="status" HeaderText="Status"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>
