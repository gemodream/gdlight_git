using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for MarkLaser.
	/// </summary>
	public partial class MarkLaser : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void txtItemNumber_TextChanged(object sender, System.EventArgs e)
		{	
			SqlCommand command = new SqlCommand("spRecordLaserLog");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@RecordText", txtItemNumber.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@UserID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@OperationType", 1));
				
			string strLaserName = txtItemNumber.Text.Trim();

            if(Regex.IsMatch(strLaserName, @"^\d{10}$|^\d{11}$"))
			{
                //spSetPartValue in sight

                Utils.DissectItemNumber(strLaserName, out string strGroupCode, out string strBatchCode, out string strItemCode);
				command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));
				command.Parameters.Add(new SqlParameter("@ItemCode", strItemCode));
			}
			else
			{
				
			}

			try
			{
				lblInfo.Text = strLaserName;
				command.Connection.Open();
				command.ExecuteNonQuery();
				command.Connection.Close();
				txtItemNumber.Text="";
				SetLaser(strLaserName);
			}
			catch(Exception ex)
			{
				lblInfo.Text = "Fail: "+ex.Message;
			}			
		}

		private void SetLaser(string strLaserName)
		{
            //Regex.IsMatch(strOrderNumber,@"\d{5,5}")
            if (Regex.IsMatch(strLaserName,@"^\d{10}$|^\d{11}$"))
			{
				//lblInfo.Text="";
				//spSetPartValue in sight

				var strGroupCode = Utils.ParseOrderCode(strLaserName).ToString();
				var strBatchCode = Utils.ParseBatchCode(strLaserName).ToString();
				var strItemCode = Utils.ParseItemCode(strLaserName).ToString();

				SqlCommand command = new SqlCommand("spGetItemDataFromOrderBatchItem");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType= CommandType.StoredProcedure;

				/*
				 *	DECLARE @GroupCode numeric(15,0)
					DECLARE @BatchCode numeric(15,0)
					DECLARE @ItemCode numeric(15,0)
					DECLARE @MeasureCode int
					DECLARE @PartID int
					DECLARE @OldNumbers int
					DECLARE @AuthorID numeric(8,0)
					DECLARE @AuthorOfficeID numeric(4,0)
				*/

				command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));
				command.Parameters.Add(new SqlParameter("@ItemCode", strItemCode));
				command.Parameters.Add(new SqlParameter("@MeasureCode", 147)); // li type
//				command.Parameters.Add(new SqlParameter("@PartID", DBNull.Value));
//				command.Parameters.Add(new SqlParameter("@OldNumbers", DBNull.Value));
				command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
				command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);
				da.Fill(dt);

				foreach(DataRow dr in dt.Rows)
				{
					SqlCommand spSetPartValue = new SqlCommand("spSetPartValue");
					spSetPartValue.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					spSetPartValue.CommandType = CommandType.StoredProcedure;

					/*
					 * @rId varchar(150) output,
						@ItemCode dnItemCode,
						@BatchID dnID, 
						@PartID dnSmallID, 
						@MeasureCode dnTinyID,
						@MeasureValue dnMeasureExt,
						@MeasureValueID dnSmallID,
						@StringValue dsName,
						@UseClosedRecheckSession int,
						@CurrentOfficeID dnTinyID,
						@AuthorID dnSmallID, 
						@AuthorOfficeID dnTinyID) 
*/
					spSetPartValue.Parameters.Add(new SqlParameter("@rId",SqlDbType.VarChar));
					spSetPartValue.Parameters["@rId"].Size=150;
					spSetPartValue.Parameters["@rId"].Direction = ParameterDirection.Output;
						
					string strNewItemCode=dr["NewItemNumber"].ToString().Trim().Substring(dr["NewItemNumber"].ToString().Trim().Length-2);					

                    spSetPartValue.Parameters.Add(new SqlParameter("@ItemCode",strNewItemCode));
					spSetPartValue.Parameters.Add(new SqlParameter("@BatchID", dr["BatchID"].ToString()));
					spSetPartValue.Parameters.Add(new SqlParameter("@PartID", dr["PartID"].ToString()));
					spSetPartValue.Parameters.Add(new SqlParameter("@MeasureCode", 67));
					spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
					spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
					spSetPartValue.Parameters.Add(new SqlParameter("@StringValue", strLaserName));
					spSetPartValue.Parameters.Add(new SqlParameter("@UseClosedRecheckSession", DBNull.Value));
					spSetPartValue.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));
					spSetPartValue.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
					spSetPartValue.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

					try
					{
						spSetPartValue.Connection.Open();
						spSetPartValue.ExecuteNonQuery();
						spSetPartValue.Connection.Close();
						lblInfo.Text = strLaserName;
					}
					catch(Exception ex)
					{
						lblInfo.Text= "UPATE FAILED!!!!: " + ex.Message;
					}
				}				
//				dgDebug.DataSource=dt;
//				dgDebug.DataBind();                
			}
			else
			{
				//not our ItemNumber
			}
		}
	}
}
