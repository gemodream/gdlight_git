﻿using System;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class ChangeUserPassword : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Change Password";
            if (IsPostBack) return;
            WrongInfoLabel.Text = "";
            LoginNameField.Text = "" + Session["LoginName"];
            OldPasswordField.Value = "";
            OldPasswordField.Focus();
        }

        protected void OnChangePasswordClick(object sender, EventArgs e)
        {
            Page.Validate("PwdGroup");
            if (!Page.IsValid) return;
            var loginModel = Session[SessionConstants.LoginModel] as LoginModel;
            if (loginModel == null) return;
            if (NewPasswordField.Value != NewPasswordField2.Value)
            {
                WrongInfoLabel.Text = "Check that you have filled the fields correctly and try again, please!";
                WrongInfoLabel.ForeColor = System.Drawing.Color.Red;
                return;
            }
            var result = QueryUtils.SetNewPassword(
                new LoginModel {Password = OldPasswordField.Value, NewPassword = NewPasswordField.Value, User = loginModel.User}, this);
            if (result == 1)
            {
                WrongInfoLabel.Text = "Password changed successfully";
                WrongInfoLabel.ForeColor = System.Drawing.Color.Green;
                loginModel.Password = NewPasswordField.Value;
                
            }
            if (result == -1)
            {
                WrongInfoLabel.Text = "Password was NOT changed. Please check that you have entered the old password correctly.";
                WrongInfoLabel.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}