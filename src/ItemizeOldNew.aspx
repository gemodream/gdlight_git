﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ItemizeOldNew.aspx.cs" Inherits="Corpt.ItemizeOldNew" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">
       
        
        
                
    </script>
    <div class="demoarea" style="">
    <div class="demoheading">Itemize Old/New</div>
    <div class="navbar nav-tabs">
        <table>
            <tr>
                <td><asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-info" OnClick="OnSaveClick" ToolTip="Add new Items"/></td>
                <td><asp:Button ID="ChangeCPButton" runat="server" Text="Change CP" CssClass="btn btn-info" OnClick="OnChangeCPButtonClick" OnClientClick="target ='_blank';"/></td>
                <td><asp:Button ID="PreloadButton" runat="server" Text="Preload" CssClass="btn btn-info" OnClick="OnPreloadButtonClick" /></td>
                <td><asp:Button ID="DeleteBatchButton" runat="server" Text="Delete Batch" CssClass="btn btn-info" OnClick="OnDeleteBatchButtonClick" /></td>
                <td><asp:Button ID="ClearButton" runat="server" Text="Clear" CssClass="btn btn-info" OnClick="OnClearButtonClick" /></td>
                <td><asp:Button ID="PrintButton" runat="server" Text="Print" CssClass="btn btn-info" OnClick="OnPrintButtonClick" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="padding-left: 10px">&nbsp;</td>
            </tr>
        </table>
        
        
        
        
        
    </div>
    <table>
        <tr>
            <td>
                <asp:Panel ID="NewMemo" runat="server" CssClass="form-inline">
                    <table style="width: 800px;">
                    <tr>
                        <td>    
                            <asp:Label ID="Memo" runat="server" Text="Memo Number"></asp:Label>
                            <asp:DropDownList runat="server" ID="MemoListField"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Item Number"></asp:Label>
                            <asp:TextBox runat="server" ID="ItemField" OnTextChanged="OnItemAddClick" 
                                style="width: 134px" ></asp:TextBox>
                            <asp:ImageButton ID="AddItemButton" runat="server" ToolTip="Add Item" ImageUrl="~/Images/ajaxImages/addbar.png"
                            OnClick="OnItemAddClick" Enabled="True" Height="37px" Width="106px"/>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="New Order"></asp:Label>
                            <asp:TextBox runat="server" ID="NewOrder" style="width: 100px" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Items to add"></asp:Label>
                            <asp:TextBox runat="server" ID="ItemsToAdd" style="width: 30px" OnTextChanged="OnItemCountClick" ><</asp:TextBox>
                            <asp:HiddenField ID="ItemsAvailable" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width=300px;">
                            <asp:ListBox runat="server" ID="OrderListBox" Rows="30" CssClass="control-list" Width="110px" Style="border: solid 2px;" />
                            <asp:ImageButton ID="RemoveItemButton" runat="server" ToolTip="Remove selected Item"
                            ImageUrl="~/Images/ajaxImages/deletebar.png" OnClick="OnItemDelClick" Style="vertical-align: top" Height="43px" Width="119px" />
                            <asp:ImageButton ID="GoItemButton" runat="server" ToolTip="Copy Items"
                            ImageUrl="~/Images/ajaxImages/rightArrow36.png" OnClick="OnItemsCopyClick" Style="horisontal-align: top; margin-left: 0px;" Width="33px" />
                            </td>
                        <td>
            
                <asp:Panel ID="OldItemsTreePanel" runat="server" Style="vertical-align: top; width: 360px; border: solid 2px;" Enabled="True">
                    <asp:TreeView ID="OldItemsTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="large" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="3" PopulateNodesFromClient="true" EnableClientScript="True"
                                ShowLines="False" ShowExpandCollapse="true" Width="360px">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                </asp:Panel>
            
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
            <!--
            <td colspan="3" style="font-weight: bold">
                                
            </td>
            -->
        </tr>
        
        <!--
        <tr>
            
            <td>
            </td> 
            <td style="height: 100px;vertical-align: top" >
                 <asp:Label runat="server" ID="Picture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label><br/>
                <asp:Image id="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image>
                <br/>
            </td>
            </tr>
        
        
       
        <tr>
            <td colspan="2">
                 <asp:Label runat="server"  Style="font-size: small" ID="ErrorPictureField" ForeColor="Red"></asp:Label>
            </td>
        </tr>
            -->
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </div>
</asp:Content>
