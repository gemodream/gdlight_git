<%@ Page language="c#" Codebehind="BulkUpdateII.aspx.cs" AutoEventWireup="True" Inherits="Corpt.BulkUpdateII" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BulkUpdateII</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body>
		<form style="Z-INDEX: 0" id="Form1" method="post" runat="server">
			<P>&nbsp;<A href="Middle.aspx"><IMG border="0" src="Images/H.gif" width="16"></A>
				<asp:button style="Z-INDEX: 0" id="cmdClear" runat="server" CssClass="buttonStyle" Text="Clear" onclick="cmdClear_Click"></asp:button><asp:radiobuttonlist style="Z-INDEX: 0" id="rblMeasureMode" runat="server" CssClass="text" RepeatDirection="Horizontal"
					AutoPostBack="True" onselectedindexchanged="rblMeasureMode_SelectedIndexChanged">
					<asp:ListItem Value="cpEnabled" Selected="True">Follow&#160;Customer&#160;Program</asp:ListItem>
					<asp:ListItem Value="cpDisabled">Ignore&#160;Customer&#160;Program</asp:ListItem>
				</asp:radiobuttonlist></P>
			<P><asp:label id="lblInfo" runat="server" CssClass="text" Font-Bold="True" BorderColor="White"
					BorderStyle="Solid"></asp:label></P>
			<P>Order:<asp:textbox id="order" runat="server" CssClass="inputStyleCC"></asp:textbox>&nbsp;Batch:<asp:textbox id="batch" runat="server" CssClass="inputStyleCC" Width="112px"></asp:textbox><asp:button id="cmdGet" runat="server" CssClass="buttonStyle" Text="Load Item Structure" style="Z-INDEX: 0" onclick="cmdGet_Click"></asp:button></P>
			<P><asp:dropdownlist id="lstPartName" runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="cmdLoadMeasures_Click"></asp:dropdownlist><asp:label id="lblDebug" runat="server" CssClass="text"></asp:label></P>
			<P>Batches with the same structure as the batch above:</P>
			<P><asp:button id="cmdSelectAll" runat="server" CssClass="buttonStyle" Text="Select All Batches" onclick="cmdSelectAll_Click"></asp:button><asp:button id="cmdDeselectAll" runat="server" CssClass="buttonStyle" Text="Deselect All Batches" onclick="cmdDeselectAll_Click"></asp:button></P>
			<P><asp:checkboxlist id="chklstBatches" runat="server" CssClass="text" RepeatDirection="Horizontal" RepeatColumns="4"
					BackColor="White"></asp:checkboxlist></P>
			<P><asp:button id="cmdLoadMeasures" runat="server" CssClass="buttonStyle" Text="Load Measures"
					Width="96px" onclick="cmdLoadMeasures_Click"></asp:button></P>
			<P><asp:dropdownlist id="lstMeasures" runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstMeasures_SelectedIndexChanged"></asp:dropdownlist></P>
			<P><asp:button id="cmdEdit" runat="server" CssClass="buttonStyle" Text="Edit" Width="96px" onclick="lstMeasures_SelectedIndexChanged"></asp:button></P>
			<asp:panel id="pnlEnum" runat="server" Visible="False" Enabled="False">
				<P>&nbsp;</P>
				<P>
					<asp:DropDownList id="lstEnumValues" runat="server" CssClass="inputStyleCC5"></asp:DropDownList>
					<asp:Button id="cmdSave" runat="server" Text="Save" CssClass="buttonStyle" onclick="cmdSave_Click"></asp:Button></P>
				<P>&nbsp;</P>
			</asp:panel><asp:panel id="pnlString" runat="server" Visible="False" Enabled="False">
				<P>&nbsp;</P>
				<P>
					<asp:textbox id="txtSetValue" runat="server" CssClass="inputStyleCC"></asp:textbox>
					<asp:Button id="cmdSave2" runat="server" Text="Save" CssClass="buttonStyle" onclick="cmdSave_Click"></asp:Button></P>
				<P>&nbsp;</P>
			</asp:panel>
			<P><asp:label id="lblDebug2" runat="server"></asp:label><asp:button style="Z-INDEX: 0" id="cmdPrefill" runat="server" CssClass="buttonStyle" Text="Pre-Fill Selected Measure in Selected Batches" onclick="cmdPrefill_Click"></asp:button></P>
			<P>
				<asp:button style="Z-INDEX: 0" id="cmdRandomizeMeasurements" runat="server" Text="Randomize Measurements"
					CssClass="buttonStyle" onclick="cmdRandomizeMeasurements_Click"></asp:button></P>
		</form>
	</body>
</HTML>
