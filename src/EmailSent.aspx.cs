using System;
using System.Data;
using System.IO;
using System.Web.Mail;

namespace Corpt
{
	/// <summary>
	/// Summary description for EmailSent.
	/// </summary>
	public partial class EmailSent : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Button Button1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(Utils.NullValue(Request.QueryString["Email"])!="")
			{
				try
				{
					SendShortReport(Utils.NullValue(Request.QueryString["Email"]), Session["ShortReport"] as DataSet);
					lblResult.Text="Email Sent.";
				}
				catch(Exception ex)
				{
					lblResult.Text="Unable to send email: " + ex.Message;
				}
			}
			else
			{
				lblResult.Text="Unable to send email. Email not specified";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void SendShortReport(String email, DataSet report)
		{
            const string from = "CustomerCare@gemscience.net";
            const string body = "See attached files.<br><i>Gemological Science International.</i>";
		    var subject = string.Format("Report from GSI - Batch {0}", Session["BatchNumberForEmail"]);
		    var dir = Session["TempDir"] + Session.SessionID + @"\";
            
            //-- Create xls file
            var srcFile = dir + "Report.xls";
		    var dstFile = dir + Session["BatchNumberForEmail"] + ".xls";
			Directory.CreateDirectory(Session["TempDir"] + Session.SessionID + @"\");
			Utils.SaveExcell(report,this);
            File.Move(srcFile, dstFile);

			var myEmail = new MailMessage();
		    myEmail.From = from;
		    myEmail.To = email;
            myEmail.Body = body;
            myEmail.Subject = subject;
            
            myEmail.BodyFormat = MailFormat.Html;
		    myEmail.BodyEncoding = System.Text.Encoding.UTF8;

			myEmail.Attachments.Add(new MailAttachment(dstFile, MailEncoding.Base64));

			SmtpMail.SmtpServer="testserver.gsi.local";
			SmtpMail.Send(myEmail);

			File.Delete(dstFile);
			Directory.Delete(dir);
		}
	}
}
