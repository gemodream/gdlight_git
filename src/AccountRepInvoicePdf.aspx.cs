﻿using System;
using System.IO;
using System.Web;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities;
using GemoDream.PdfGenerator.Invoice;
using GemoDream.QueueMessages;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.File;

namespace Corpt
{
    public partial class AccountRepInvoicePdf : CommonPage, ITemplateSupplier
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            PresentFileDownload(int.Parse(Request["requestId"]), int.Parse(Request["invoiceId"]));
        }

        private void PresentFileDownload(int requestId, int invoiceId)
        {
            string contentType = "application/pdf";
            Response.Clear();
            MemoryStream ms = GetPdf(requestId, invoiceId);
            Response.ContentType = contentType;
            Response.AddHeader("Content-disposition", "inline;filename=Invoice" + invoiceId + ".pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        private MemoryStream GetPdf(int requestId, int invoiceId)
        {
            QbInvoiceRequestModel qbInvoiceRequestModel = QueryAccountRep.GetQbInvoiceRequest(requestId, this);
            var generator = InvoicePdfDocumentGenerator.CreateInstanceFromInvoiceData(
                qbInvoiceRequestModel.InvoiceData, $"QB-Invoice-{invoiceId}");
            generator.Init(this);
            generator.Fill();
            MemoryStream memoryStream = new MemoryStream();
            ((InvoicePdfDocumentGenerator)generator).SaveToStream(memoryStream);
            return memoryStream;
        }
        public void GetTemplate(Action<Stream, string> convertToDocument)
        {
            DownloadFromAzureFileShare(
                accountName: Session["AzureAccountName"].ToString(), 
                accountKey: Session["AzureAccountKey"].ToString(), 
                shareName: (string)Session["AccRep.QB.InvoiceTemplate.ShareName"], 
                directoryName: (string)Session["AccRep.QB.InvoiceTemplate.DirectoryName"], 
                fileName: (string)Session["AccRep.QB.InvoiceTemplate.FileName"],
                convertToDocument: convertToDocument);
        }

        private void DownloadFromAzureFileShare(string accountName, string accountKey, string shareName,
            string directoryName, string fileName, Action<Stream, string> convertToDocument)
        {
            var storageCredentials = new StorageCredentials(accountName, accountKey);
            var storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
            var cloudFileClient = storageAccount.CreateCloudFileClient();

            var cloudFileShare = cloudFileClient.GetShareReference(shareName);
            if (!cloudFileShare.Exists())
            {
                throw new Exception($"Azure cloudFileShare \"{shareName}\" does not exist. AccountName={storageCredentials.AccountName}");
            }

            var cloudFileDirectory = cloudFileShare.GetRootDirectoryReference();
            if (!string.IsNullOrEmpty(directoryName))
            {
                cloudFileDirectory = cloudFileDirectory.GetDirectoryReference(directoryName);
            }
            if (!cloudFileDirectory.Exists())
            {
                throw new Exception($"Azure cloudFileDirectory \"{directoryName}\" not found");
            }

            CloudFile cloudFile = cloudFileDirectory.GetFileReference(fileName);
            if (!cloudFile.Exists())
            {
                throw new Exception($"Azure cloudFile \"{fileName}\" not found");
            }

            LogUtils.UpdateSpLog($"Take template from Azure \"{cloudFile.Uri}\"", Session);

            var memoryStream = new MemoryStream();
            cloudFile.DownloadToStream(memoryStream);
            convertToDocument(memoryStream, null);
        }

    }
}