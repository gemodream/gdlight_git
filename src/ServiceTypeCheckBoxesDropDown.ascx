﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/ServiceTypeCheckBoxesDropDown.ascx.cs" Inherits="Corpt.ServiceTypeCheckBoxesDropDown" %>

<style type="text/css">
    .unselectable {
        -moz-user-select: -moz-none; 
        -khtml-user-select: none; 
        -webkit-user-select: none; 
        -o-user-select: none; 
        user-select: none;
        background: white !important;
    }

    /*#CheckBoxListId input, #ServiceTypeCheckBoxList label {vertical-align: middle; cursor: pointer;}*/

    .select-box {
        background-image: url(Images/ajaxImages/DropDownArrow.png); 
        background-position: right center;
        background-repeat: no-repeat;
        cursor: pointer !important;
    }

    .multidropdown {
        position: absolute; 
        z-index: 1000; 
        margin: 0; 
        padding: 0;
        background: white;
    }

    .multidropdown hover {
        background-color: #F0F0FF;
    }
    
    .multidropdown input {
        margin: 0 0 0 0 !important;
        padding: 0 0 0 6px !important;
        vertical-align: middle;
        cursor: pointer;
    }

    .multidropdown label {
        margin: 0 0 0 0 !important;
        padding: 0 0 0 10px !important;
        vertical-align: middle;
        cursor: pointer;
    }
    
</style>

<script type="text/javascript" >

    (function initCheckBoxesDropDown () {
        $('body').on('click', '#<%= TextBoxId.ClientID %>', function (e) {
            var offset = $(this).position();
            var pTop = offset.top + 32;
            var pLeft = offset.left;

            // log("top", pTop, "left", pLeft);

            $("#<%= CheckBoxListId.ClientID %>").css("top", pTop).css("left", pLeft).toggle('fast', function() {
                if($(this).is(':hidden')) { 
                    __doPostBack('<%=CheckBoxListId.ClientID %>', "PopupHide");
                }
            }); 
            return false;
        });

        $('body').on('click', '#<%= CheckBoxListId.ClientID %>', function (e) {
            e.stopPropagation();
        });

        $('body').on('click', '#<%= CheckBoxListId.ClientID %>', function (e) {
            var inputs = $("#<%= CheckBoxListId.ClientID %> input[type=checkbox]");
            var labels = $("#<%= CheckBoxListId.ClientID %> label");
            var checked = [];
            var checkedNumber = 0;
            var i;
            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    checked[checkedNumber++] = labels[i];
                }
            }

            // Set TextBox Value
            var text;
            text = checked[0].innerText;
            if (checkedNumber > 1) {
                text += ", ... etc.";
            }
            var textBox = $('#<%= TextBoxId.ClientID %>')[0];
            textBox.value = text;
        });

        $(document).click(function (e) {
            var popupDisplay = $("#<%= CheckBoxListId.ClientID %>").css("display");
            if (popupDisplay === "table") {
                __doPostBack('<%=CheckBoxListId.ClientID %>', "PopupHide");
                $("#<%= CheckBoxListId.ClientID %>").hide();
            }
        });

    })();
</script>

<asp:TextBox ID="TextBoxId" runat="server" 
             CssClass="select-box unselectable" 
             Text="Any" 
             ReadOnly="True"
             unselectable="on"/>

<asp:CheckBoxList ID="CheckBoxListId" runat="server" 
                  AppendDataBoundItems="true" 
                  CssClass="multidropdown" style="display: none; text-align: left; border: 1px black solid;"
                  DataTextField="Text" 
                  DataValueField="Value"
                  TextAlign="Right"
                  DataTextFormatString="{0}"
                  OnDataBound="CheckBoxListId_OnDataBound">
</asp:CheckBoxList>

