﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="PrvStat2.aspx.cs" Inherits="Corpt.PrvStat2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server" >
     <div class="demoarea">
         <div class="demoheading">
            Color/Clarity Stat</div>
         <table>
             <tr>
                 <td>
                     <asp:Panel runat="server" DefaultButton="CpLikeButton" CssClass="form-inline"> Program Like:
                         <asp:TextBox runat="server" ID="CpNameLike" Text="leslie"></asp:TextBox>
                         <asp:ImageButton ID="CpLikeButton" runat="server" ToolTip="Filtering the list of customers"
                             ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCpSearchClick" />
                     </asp:Panel>
                 </td>
                 <td style="padding-left: 40px">
                     <asp:ImageButton ID="cmdExcel" ToolTip="Export to Excel" runat="server"
                         ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExcelClick" Visible="False"
                          />
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top">
                     <asp:TreeView runat="server" ID="ProgramsView" OnSelectedNodeChanged="OnCpSelected" >
                         <LeafNodeStyle Font-Names="Cambria" />
                         <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" />
                         <RootNodeStyle Font-Bold="True" Font-Names="Cambria"  />
                         <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                     </asp:TreeView>
                 </td>
                 <td style="vertical-align: top;padding-left: 40px">
                     <asp:GridView runat="server" ID="GridView" AutoGenerateColumns="False" OnSorting="OnSortingEvent"
                         OnDataBound="OnDataBound" EnableViewState="False" CssClass="table table-bordered"
                         DataKeyNames="Vendor,Year,Month,MeasureName" RowStyle-Height="8px">
                         <Columns>
                             <asp:BoundField DataField="Vendor" HeaderText="Vendor" SortExpression="Vendor" />
                             <asp:BoundField DataField="Year" HeaderText="Year" SortExpression="Year" />
                             <asp:BoundField DataField="Month" HeaderText="Month" SortExpression="Month" />
                             <asp:BoundField DataField="MeasureName" HeaderText="Color/Clarity" SortExpression="MeasureName" />
                             <asp:BoundField DataField="MeasureValue" HeaderText="Value" SortExpression="MeasureValue" />
                             <asp:BoundField DataField="Qnty" HeaderText="# of Items" SortExpression="Qnty" />
                             <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" Visible="False"/>
                         </Columns>
                         <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                     </asp:GridView>
                     
                 </td>
             </tr>
         </table>
     </div>
</asp:Content>
