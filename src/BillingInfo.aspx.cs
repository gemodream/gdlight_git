﻿using System;
using Corpt.Utilities;

namespace Corpt
{
    public partial class BillingInfo : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!IsPostBack) OrderField.Focus();
        }

        protected void OnLookupClick(object sender, EventArgs e)
        {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
            MessageField.Text = "";
            var infos = QueryUtils.GetBillingInfo(OrderField.Text, this);
            if (infos.Count == 0)
            {
                MessageField.Text = "Either the order number is not correct or the order was never billed through GemoDream.";
                InfoGrid.DataSource = null;
                InfoGrid.DataBind();
                return;
            }
            InfoGrid.DataSource = infos;
            InfoGrid.DataBind();

        }
    }
}