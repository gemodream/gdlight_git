﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Utilities;
using Corpt.Models;
using Corpt.TreeModel;
using System.Data;

namespace Corpt
{
    public partial class Appraisal : CommonPage
    {
        /* For testing batchNumber = 117124001 */
        // vy table for Batch mode
        private DataSet btch
        {
            get
            {
                if (Session["btch"] == null)
                { return null; }
                else
                {
                    return (DataSet)(Session["btch"]);
                }
            }
            set { Session["btch"] = value; }
        }


        private int BatchID
        {
            get
            {
                if (Session["BatchIDAppr"] == null)
                { return 0; }
                else
                {
                    return (int)(Session["BatchIDAppr"]);
                }
            }
            set { Session["BatchIDAppr"] = value; }
        }

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Appraisal";
            OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
            OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

            if (!IsPostBack)
            {
                CleanUp();
                // vy
                //SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
                txtBatchNumber.Focus();
            }
        }
        #endregion

        #region CleanUp (on PageLoad, on Load button click)
        private void CleanUp()
        {
            CleanItemListArea();
            CleanPartTreeArea();
            //HidePicturePanel();
            //HideShapePanel();
            CleanValueGridArea();
            CleanViewState();

            cmdSave.Visible = false;
            InvalidLabel.Text = "";

            //Sergey
            //Additional Controls
            currencyLabel.Visible = false;
            currencySelector.Text = "";
            currencySelector.Visible = false;
            currencyPrice.Text = "";
            currencyPrice.Visible = false;
            batchModeButton.Visible = false;

            ItemValuesGrid.Columns[checkBoxColumn].Visible = false;
            batchMetals.Visible = false;
            batchMetalTextBox.Visible = false;
            batchBumpItems.Visible = false;
            bulkUpdateColoredDiamond.Visible = false;
        }
        private void CleanItemListArea()
        {
            lstItemList.Items.Clear();
            ItemEditing.Value = "";
            lstItemList.Visible = false;
        }
        private void CleanPartTreeArea()
        {
            PartTree.Nodes.Clear();
            //PartEditing.Value = "";
        }
        private void CleanValueGridArea()
        {
            ItemValuesGrid.DataSource = null;
            ItemValuesGrid.DataBind();
            MeasurementPanel.Visible = false;
            DataForLabel.Visible = false;
        }
        private void CleanViewState()
        {
            //-- Item numbers
            SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);
            //VY

            //--Measure Parts
            SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);

            //-- Measure Descriptions
            SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

            //-- Item Measure Values
            SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);
            SetViewState(new List<PriceModel>(), ConstPricesForEdit);
        }
        #endregion

        #region Load Button
        protected void OnLoadClick(object sender, EventArgs e)
        {
            /*
            if (HasUnSavedChanges())
            {
                PopupSaveQDialog(ModeOnLoadClick);
                return;
            }
             */

            // VY0
            this.btch = null;
            LoadExecute();
            txtBatchNumber.Focus();
        }
        int ListMaxRow = 40;
        int ListMinRow = 10;
        private void LoadExecute()
        {
            CleanUp();
            if (txtBatchNumber.Text.Trim().Length == 0)
            {
                InvalidLabel.Text = "A Batch Number is required!";
                return;
            }
            //-- Get ItemNumbers by BatchNumber
            //var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page); // procedure spGetItemCPPictureByCode1Test
            DataTable dt = QueryUtils.GetItemsCpOldNew(txtBatchNumber.Text.Trim(), this.Page);
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
            if (itemList.Count == 0)
            {
                InvalidLabel.Text = "Items not found";
                return;
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);

            //-- Refresh batchNumber
            var itemNumber = itemList[0];
            if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
            {
                txtBatchNumber.Text = itemNumber.FullBatchNumber;
            }

            //-- Loading ItemList control
            lstItemList.Items.Clear();
            foreach (var singleItemModel in itemList)
            {
                lstItemList.Items.Add(singleItemModel.FullItemNumber);
            }
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                lstItemList.Rows = itemList.Count + 1;
            }
            lstItemList.Visible = true;

            //--Loading Parts by first ItemNumber and show on PartTree Control
            var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this); // procedure spGetPartsByItemTyp
			SetViewState(parts, SessionConstants.GradeMeasureParts);
            LoadPartsTree();

            // --Loading Measures by Cp or ItemTypeId  7 / 30 VY took away
            var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
            SetViewState(measures, SessionConstants.GradeMeasureValuesCp);

            lstItemList.SelectedIndex = 0;
            OnItemListSelectedChanged(null, null);

            // Sergey
            // Display additional controls after pressing Load Button
            LoadCurrencySelector();//Called in OnItemListSelectedChanged
            batchModeButton.Visible = true;
        }

        #endregion

        //Sergey
        #region Currency Selector
        private void LoadCurrencySelector()
        {
            currencyLabel.Visible = true;
            currencySelector.Visible = true;
            currencySelector.Text = "CAD Canadian Dollars";//Default value not working in .aspx for some reason
            currencyPrice.Visible = true;
            LoadCurrencyDisplay();
        }
        private void LoadCurrencyDisplay()
        {
            if (DataForLabel.Text != "Data For Batch")
            {
                DataGridItem firstRow = ItemValuesGrid.Items[0];
                var firstRowTextBox = firstRow.FindControl(ValueNumericFld) as TextBox;
                Decimal price = Decimal.Parse(firstRowTextBox.Text);
                string targetCurrency = currencySelector.Text.Split(new char[0])[0];
                price = CurrencyConverter.getRate(targetCurrency, "USD", price, this);
                if (price > 0)
                {
                    //currencyPrice.Text = Math.Round(price, 2).ToString();
                    currencyPrice.Text = ((int)Math.Round(price / 10) * 10).ToString();
                }
                else
                {
                    currencyPrice.Text = "";
                }
            }
            else//Batch Mode
            {
                string newCurrency = currencySelector.Text.Split(new char[0])[0];
                if (CurrencyConverter.getRate(newCurrency, oldCurrencySelector.Value, this) > 0)
                {
                    Decimal rate = CurrencyConverter.getRate(newCurrency, oldCurrencySelector.Value, this);
                    oldCurrencySelector.Value = newCurrency;
                    if (rate > 0)
                    {
                        foreach (DataGridItem item in ItemValuesGrid.Items)
                        {
                            var rowTextBox = item.FindControl(ValueNumericFld) as TextBox;
                            /*//Rounds to nearest 10s
							rowTextBox.Text = ((int)Math.Round((Decimal.Parse(rowTextBox.Text) * rate) / 10) * 10).ToString();//Update last text box
							item.Cells[18].Text = ((int)Math.Round((Decimal.Parse(item.Cells[18].Text) * rate) / 10) * 10).ToString();//Update saved prices
							*/
                            rowTextBox.Text = (Decimal.Parse(rowTextBox.Text) * rate).ToString("F2");//Update last text box
                            item.Cells[22].Text = (Decimal.Parse(item.Cells[22].Text) * rate).ToString("F2");//Update saved prices
                        }
                    }
                }
            }
        }
        protected void currencySelector_TextChanged(object sender, EventArgs e)
        {
            LoadCurrencyDisplay();
        }

        #endregion

        #region Data From View
        private List<MeasurePartModel> GetPartsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
        }
        private List<SingleItemModel> GetItemNumbersFromView()
        {
            return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
        }
        private SingleItemModel GetItemModelFromView(string itemNumber)
        {
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber);
        }
        private SingleItemModel GetSelectedItemModel()
        {
            var itemNumber = lstItemList.SelectedValue;
            if (string.IsNullOrEmpty(itemNumber)) return null;
            return GetItemModelFromView(itemNumber);
        }
        static string ConstPricesForEdit = "PricesForEdit";
        private List<PriceModel> GetPricesForEditFromView()
        {
            var data = GetViewState(ConstPricesForEdit) as List<PriceModel> ?? new List<PriceModel>();
            return data;
        }

        #endregion

        #region PartTree
        private void LoadPartsTree()
        {
            PartTree.Nodes.Clear();
            var parts = GetPartsFromView();
            if (parts.Count == 0)
            {
                return;
            }
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Select();

            OnPartsTreeChanged(null, null);

        }
        protected void OnPartsTreeChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region DataGrid
        protected void OnEditItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;
            var editModel = e.Item.DataItem as PriceModel;
            if (editModel == null) return;
            if (editModel.NeedSave)
            {
                MarkGridItemAsChanged(e.Item, true);
            }
            if (editModel.ManualAppraisal)
            {
            }
            var numericFld = e.Item.FindControl(ValueNumericFld) as TextBox;
            if (numericFld == null) return;
            numericFld.Text = editModel._Appraisal.ToString();
            numericFld.ToolTip = editModel._Appraisal.ToString();
            numericFld.Visible = true;
            string err = "";
            bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgAppraisal, this, out err);
            if (!access || err != "")
            {
                if (editModel.PartName == "Metal" || (editModel.PartName.ToLower().Contains("color") && editModel.PartName.ToLower().Contains("diamond")))
                    numericFld.Enabled = true;
                else
                    numericFld.Enabled = false;
            }
            else
                numericFld.Enabled = true;
            //numericFld.Enabled = editModel.IsContainer;//This line determines which textboxes are enabled
            //Enables the metal field, Sergey
            //if (editModel.PartName == "Metal" || (editModel.PartName.ToLower().Contains("color") && editModel.PartName.ToLower().Contains("diamond")))
            //if (editModel.PartName.Contains("Metal") || (editModel.PartName.ToLower().Contains("color") && editModel.PartName.ToLower().Contains("diamond")))
            //{
            //    numericFld.Enabled = true;
            //}
            if (editModel.PartName.ToLower().Contains("color") && editModel.PartName.ToLower().Contains("diamond"))
            {
                bulkUpdateColoredDiamond.Visible = true;
                bulkUpdateColoredDiamond.Enabled = true;
            }

        }
        private void GetNewValuesFromGrid()
        {
            var itemValues = GetPricesForEditFromView();
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[item.ItemIndex];
                var partTypeID = key.Split('_')[0];
                var partId = key.Split('_')[1];
                var itemNumber = key.Split('_')[2];
                if (partTypeID == "15")
                {
                    var itemValue = itemValues.Find(m => "" + m.PartId == partId && "" + m.ItemNumber == itemNumber);
                    if (itemValue == null) continue;
                    var newValue = "";
                    var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                    if (numericFld != null) newValue = numericFld.Text;
                    itemValue.PriceNew = newValue;
                }
                else continue;
            }
        }
        protected void OnNumericFldChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var item = (textBox.BindingContainer as DataGridItem);
            //Column indexes in DataGrid table
            //int oldPriceCol = 16;
            //int markupCol = 17;
            //int savedCol = 18;
            int oldPriceCol = 20;
            int markupCol = 21;
            int savedCol = 22;
			//goto a1;
            //Sergey
            if (IsContainerItem(item))//Text box changed was ItemContainer
            {
                var New_MSRP = float.Parse(textBox.Text);
                var priceSaved = float.Parse(item.Cells[savedCol].Text);// * float.Parse(item.Cells[17].Text));
                if (!priceSaved.Equals(New_MSRP)) MarkGridItemAsChanged(item, true);
				
			}
            else // if (item.Cells[5].Text.ToLower().Contains("color") && item.Cells[5].Text.ToLower().Contains("diamond"))
            {
                string cellName = item.Cells[5].Text;
                decimal sum = 0;
                foreach (DataGridItem items in ItemValuesGrid.Items)
                {
                    if (IsContainerItem(items)) continue;
                    
                    var numericFld = items.FindControl(ValueNumericFld) as TextBox;
                    //aaa
                        var itemValues = GetPricesForEditFromView();
                    if (items.Cells[5].Text == cellName)
                    {
                        foreach (var singleItem in itemValues)
                        {
                            if (singleItem.PartName == cellName)
                            {
                                singleItem.PriceNew = numericFld.Text;
                            }
                        }

                        SetViewState(itemValues, ConstPricesForEdit);
                    }
                    //For each row, either add the price from the old value or from the user, depending on if it is empty or not
                    //float number;
                    if (!string.IsNullOrEmpty(numericFld.Text)) //&& float.TryParse(numericFld.Text, out number)) //changed by Sasha
                    {
                        //if (number > 0.01)																	   //added
                        {
                            sum += Convert.ToDecimal(numericFld.Text);
                            items.Cells[oldPriceCol].Text = Convert.ToDecimal(numericFld.Text).ToString("F2");
                            items.Cells[savedCol].Text = Convert.ToDecimal(numericFld.Text).ToString("F2");
                        }
                    }
                    else if (!string.IsNullOrEmpty(items.Cells[oldPriceCol].Text) && items.Cells[oldPriceCol].Text != @"&nbsp;")
                    {
                        sum += Convert.ToDecimal(items.Cells[oldPriceCol].Text);
                    }
                }
                DataGridItem firstRow = ItemValuesGrid.Items[0];
                var firstRowTextBox = firstRow.FindControl(ValueNumericFld) as TextBox;
                firstRow.Cells[oldPriceCol].Text = sum.ToString("F2");  //Old comments: Change the Price/part column to reflect new calculated price	   //Commented 	by Sasha
                firstRow.Cells[markupCol].Text = QueryUtils.CalculateMarkup(Decimal.Parse(firstRow.Cells[oldPriceCol].Text), this.Page).ToString("F2");//Adjust Markup based on new Price/part value
                sum *= Convert.ToDecimal(firstRow.Cells[markupCol].Text);//Multiply by markup
                sum = (int)(sum / 10) * 10;
                firstRowTextBox.Text = sum.ToString("F2");//Change Calculated MSRP to proper value

                var New_MSRP = float.Parse(firstRowTextBox.Text);
                var priceSaved = float.Parse(firstRow.Cells[savedCol].Text);// * float.Parse(item.Cells[17].Text));
                if (!priceSaved.Equals(New_MSRP)) MarkGridItemAsChanged(firstRow, true);
                //float price = float.Parse(textBox.Text);
                //float icPrice = float.Parse(ItemValuesGrid.Items[0].Cells[18].Text);
                //string newPrice = (price + icPrice).ToString();
                //ItemValuesGrid.Items[0].Cells[18].Text = newPrice;
            }
            //else goto a1;//Text box changed was not ItemContainer
			//{
   //             decimal sum = 0;
   //             foreach (DataGridItem items in ItemValuesGrid.Items)
   //             {
   //                 if (IsContainerItem(items)) continue;
   //                 var numericFld = items.FindControl(ValueNumericFld) as TextBox;
			//		//For each row, either add the price from the old value or from the user, depending on if it is empty or not
			//		//float number;
			//		if (!string.IsNullOrEmpty(numericFld.Text)) //&& float.TryParse(numericFld.Text, out number)) //changed by Sasha
   //                 {
			//			//if (number > 0.01)																	   //added
			//			{
			//				sum += Convert.ToDecimal(numericFld.Text);
			//				items.Cells[oldPriceCol].Text = Convert.ToDecimal(numericFld.Text).ToString("F2");
			//			}
   //                 }
   //                 else if (!string.IsNullOrEmpty(items.Cells[oldPriceCol].Text))
   //                 {
   //                     sum += Convert.ToDecimal(items.Cells[oldPriceCol].Text);
   //                 }
   //             }
   //             DataGridItem firstRow = ItemValuesGrid.Items[0];
   //             var firstRowTextBox = firstRow.FindControl(ValueNumericFld) as TextBox;
   //             firstRow.Cells[oldPriceCol].Text = sum.ToString("F2");  //Old comments: Change the Price/part column to reflect new calculated price	   //Commented 	by Sasha
   //             firstRow.Cells[markupCol].Text = QueryUtils.CalculateMarkup(Decimal.Parse(firstRow.Cells[16].Text), this.Page).ToString("F2");//Adjust Markup based on new Price/part value
   //             sum *= Convert.ToDecimal(firstRow.Cells[markupCol].Text);//Multiply by markup
   //             firstRowTextBox.Text = sum.ToString("F2");//Change Calculated MSRP to proper value

   //             var New_MSRP = float.Parse(firstRowTextBox.Text);
   //             var priceSaved = float.Parse(firstRow.Cells[savedCol].Text);// * float.Parse(item.Cells[17].Text));
   //             if (!priceSaved.Equals(New_MSRP)) MarkGridItemAsChanged(firstRow, true);
   //         }
			//a1:
            //After making changes to Calculated MSRP, update the Currency Converter
            if (currencyPrice.Visible)
            {
                LoadCurrencyDisplay();
            }

            //var isChanged = !PriceModel.IsEquals(priceSaved.ToString(), textBox.Text);//textBox.ToolTip
            //MarkGridItemAsChanged(item, isChanged);
            //CalculateContainer();
        }

        private void CalculateContainer()
        {
            decimal sum = 0;
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                if (IsContainerItem(item)) continue;
                var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                if (numericFld != null)
                {
                    var value = numericFld.Text;
                    if (!string.IsNullOrEmpty(value)) sum += Convert.ToDecimal(value);
                }
            }
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                if (!IsContainerItem(item)) continue;
                var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                if (numericFld == null) break;
                numericFld.Text = sum.ToString();
                //-- refresh data from grid
                GetNewValuesFromGrid();
                var containerModel = GetPricesForEditFromView().Find(m => m.IsContainer);
                if (containerModel != null)
                {
                    MarkGridItemAsChanged(item, containerModel.NeedSave);
                }
                break;

            }
        }
        private int ColumnIsContainer = 0;
        //private int ColumnSaved = 18;
        private bool IsContainerItem(DataGridItem item)
        {
            return item.Cells[ColumnIsContainer].Text.ToLower() == "true";
        }
        private void MarkGridItemAsChanged(DataGridItem item, bool changed)
        {
            if (changed)
            {
                item.Style.Add("background-color", "LightGoldenrodYellow");
            }
            else
            {
                item.Style.Remove("background-color");
            }
        }
        #endregion

        #region ItemNumber List
        private void OnItemListSelected()
        {
            var itemNumber = lstItemList.SelectedValue;
            var singleItemModel = GetItemModelFromView(itemNumber);
            if (singleItemModel == null) return;
            ItemEditing.Value = itemNumber;
            LoadPricesForEditing(itemNumber);

            //Sergey: Load Currency Controls and Display
            LoadCurrencySelector();
            //LoadCurrencyDisplay();//Called in LoadCurrencySelector
        }
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
        {
            /*
            if (HasUnSavedChanges())
            {
                PopupSaveQDialog(ModeOnItemChanges);
                return;
            }
            */
            //Sergey: Get out of batch mode
            ItemValuesGrid.Columns[checkBoxColumn].Visible = false;
            batchMetals.Visible = false;
            batchMetalTextBox.Visible = false;
            batchBumpItems.Visible = false;

            OnItemListSelected();
        }
        private const string ValueNumericFld = "ValueNumericFld";

        private List<PriceModel> LoadPricesForBatchMode()
        {
            var itemNumber = lstItemList.SelectedValue;
            var singleItemModel = GetItemModelFromView(itemNumber);
            var itemModel = GetItemModelFromView(itemNumber);
            var parts = GetPartsFromView();
            var priceValues = QueryPrice.GetPriceValuesVY(itemModel, parts, this, btch, true, this.BatchID);
            foreach (var priceValue in priceValues)
            {
                if (priceValue.Old_MSRP != null && priceValue.Old_MSRP != "" && priceValue.Old_MSRP != "0.00")
                    priceValue._Appraisal = priceValue.Old_MSRP;
            }
            return priceValues;
        }

        private void LoadPricesForEditing(string itemNumber)
        {
            var itemModel = GetItemModelFromView(itemNumber);
            var parts = GetPartsFromView();
            // var priceValues = QueryPrice.GetPriceValues(itemModel, parts, this);
            //VY1

            //
            BatchID = itemModel.NewBatchId;
            var priceValues = QueryPrice.GetPriceValuesVY(itemModel, parts, this, btch, lstItemList.SelectedValue, this.BatchID); //procedure spGetBatchItemAppraisal_test or  AppraisalVY


			SetViewState(priceValues, ConstPricesForEdit);

            //Sergey
            //Save value in metal textbox temporarily
            int partNameCol = 5;
            int lwFlCol = 15;
            int textBoxCol = 19;
            int treatmentCol = 16;
            int partTypeIdCol = 3;
            TextBox metalTextBox;
            string metalTextBoxString = "";
            for (int i = 0; i < ItemValuesGrid.Columns.Count; i++)
            {
                var field = ItemValuesGrid.Columns[i];
                var bField = field as BoundColumn;
                if (bField != null)
                {
                    if (bField.DataField.ToLower().Trim() == "partname")
                    {
                        partNameCol = i;
                    }
                    if (bField.DataField.ToLower().Trim() == "treatment")
                        treatmentCol = i;
                    if (bField.DataField.ToLower().Trim() == "lwflourescence")
                        lwFlCol = i;
                    if (bField.DataField.ToLower().Trim() == "lowerprice")
                        textBoxCol = i;
                    if (bField.DataField.ToLower().Trim() == "parttypeid")
                        partTypeIdCol = i;

                }
            }
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                if (item.Cells[partNameCol].Text.Contains("Metal"))
                {
                    metalTextBox = item.Cells[textBoxCol].FindControl(ValueNumericFld) as TextBox;
                    metalTextBoxString = metalTextBox.Text;
                    break;
                }
            }

            ItemValuesGrid.DataSource = priceValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = true;
            string prevNumber = Utils.FillToFiveChars(itemModel.PrevOrderCode) + Utils.FillToThreeChars(itemModel.PrevBatchCode, itemModel.PrevOrderCode) + Utils.FillToTwoChars(itemModel.PrevItemCode);
            if (prevNumber != null)
                DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + prevNumber + @")";
            else
                DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            //DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            ShowHideItemDetails(true);
            cmdSave.Visible = true;

            //Sergey
            //Applies value in old metalTextBox to the new metalTextBox
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                if (item.Cells[partNameCol].Text.Contains("Metal"))
                {
                    var newMetalTextBox = item.Cells[textBoxCol].FindControl(ValueNumericFld) as TextBox;
                    newMetalTextBox.Text = metalTextBoxString;
                    OnNumericFldChanged(newMetalTextBox, null);
                    break;
                }
            }
            
            //foreach (DataGridItem item in ItemValuesGrid.Items)
            //{
            //    if (item.Cells[partNameCol].Text.ToLower().Contains("diamond") && !item.Cells[partNameCol].Text.ToLower().Contains("set"))
            //    {
            //        if ((item.Cells[treatmentCol].Text.ToLower().Contains("ff:") || item.Cells[treatmentCol].Text.ToLower().Contains("ld:") ||
            //            item.Cells[treatmentCol].Text.ToLower().Contains("km:") || item.Cells[treatmentCol].Text.ToLower().Contains("ec:") ||
            //            item.Cells[treatmentCol].Text.ToLower().Contains("ic:")) && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
            //            item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
            //        //if (item.Cells[treatmentCol].Text.ToLower() != "n/a" && item.Cells[treatmentCol].Text.ToLower() != "na" && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
            //        //    item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
            //        if (item.Cells[lwFlCol].Text.ToLower() != "none" && item.Cells[lwFlCol].Text.ToLower() != "faint" && item.Cells[lwFlCol].Text.ToLower() != "faint")
            //            item.Cells[lwFlCol].BackColor = System.Drawing.Color.Pink;
            //    }
            //    else if (item.Cells[partNameCol].Text.ToLower().Contains("cs") && !item.Cells[partNameCol].Text.ToLower().Contains("css"))
            //    {
            //        if ((item.Cells[treatmentCol].Text.ToLower().Contains("ff:") || item.Cells[treatmentCol].Text.ToLower().Contains("ld:") ||
            //            item.Cells[treatmentCol].Text.ToLower().Contains("km:") || item.Cells[treatmentCol].Text.ToLower().Contains("ec:") ||
            //            item.Cells[treatmentCol].Text.ToLower().Contains("ic:")) && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
            //            item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
            //        //if (item.Cells[treatmentCol].Text.ToLower() != "n/a" && item.Cells[treatmentCol].Text.ToLower() != "na" && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
            //        //    item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
            //    }
            //}
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                if (item.Cells[partTypeIdCol].Text.ToLower().Contains("1") || item.Cells[partTypeIdCol].Text.ToLower().Contains("2") ||
                    item.Cells[partTypeIdCol].Text.ToLower().Contains("10") || item.Cells[partTypeIdCol].Text.ToLower().Contains("11"))
                {
                    if ((item.Cells[treatmentCol].Text.ToLower().Contains("ff:") || item.Cells[treatmentCol].Text.ToLower().Contains("ld:") ||
                        item.Cells[treatmentCol].Text.ToLower().Contains("km:") || item.Cells[treatmentCol].Text.ToLower().Contains("ec:") ||
                        item.Cells[treatmentCol].Text.ToLower().Contains("ic:")) && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
                        item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
                    //if (item.Cells[treatmentCol].Text.ToLower() != "n/a" && item.Cells[treatmentCol].Text.ToLower() != "na" && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
                    //    item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
                    //if (item.Cells[lwFlCol].Text.ToLower() != "none" && item.Cells[lwFlCol].Text.ToLower() != "faint" && item.Cells[lwFlCol].Text.ToLower() != "faint")
                    //    item.Cells[lwFlCol].BackColor = System.Drawing.Color.Pink;
                }
                else if (item.Cells[partTypeIdCol].Text.ToLower().Contains("3") || item.Cells[partTypeIdCol].Text.ToLower().Contains("12"))
                {
                    if ((item.Cells[treatmentCol].Text.ToLower().Contains("ff:") || item.Cells[treatmentCol].Text.ToLower().Contains("ld:") ||
                        item.Cells[treatmentCol].Text.ToLower().Contains("km:") || item.Cells[treatmentCol].Text.ToLower().Contains("ec:") ||
                        item.Cells[treatmentCol].Text.ToLower().Contains("ic:")) && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
                        item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
                    //if (item.Cells[treatmentCol].Text.ToLower() != "n/a" && item.Cells[treatmentCol].Text.ToLower() != "na" && !item.Cells[treatmentCol].Text.ToLower().Contains("nbsp"))
                    //    item.Cells[treatmentCol].BackColor = System.Drawing.Color.Pink;
                }
                if (item.Cells[partNameCol].Text.ToLower().Contains("diamond") && !item.Cells[partNameCol].Text.ToLower().Contains("set"))
                {
                    if (item.Cells[lwFlCol].Text.ToLower() != "none" && item.Cells[lwFlCol].Text.ToLower() != "faint" && item.Cells[lwFlCol].Text.ToLower() != "faint")
                        item.Cells[lwFlCol].BackColor = System.Drawing.Color.Pink;
                }

            }
            DataTable dtSavedMSRP = QueryPrice.GetSavedMSRP(itemModel.OrderCode, itemModel.BatchCode, itemModel.ItemCode, this);
            if (dtSavedMSRP != null && dtSavedMSRP.Rows.Count > 0)
            {
                foreach (DataGridItem item in ItemValuesGrid.Items)
                {
                    string partName = item.Cells[partNameCol].Text;
                    DataRow dr = dtSavedMSRP.AsEnumerable()
                        .FirstOrDefault(r => r.Field<string>("PartName") == partName);
                   //.SingleOrDefault(r => r.Field<string>("PartName") == partName); alex 03222023
                    if (dr != null)
                        item.Cells[22].Text = dr["Price"].ToString();
                }
            }
        }
        void ShowHideItemDetails(bool show)
        {
            DataForLabel.Visible = show;
            MeasurementPanel.Visible = show;
        }
        #endregion

        #region Save
        protected void OnSaveClick(object sender, EventArgs e)
        {
            //-- no changes
            if (!HasUnSavedChanges())
            {
                PopupInfoDialog("No changes!", false);
                return;
            }
            var errMsg = SaveExecute();
            // initiate page reload
            this.btch = null;
            if (string.IsNullOrEmpty(errMsg))
            {
                //-- Reload Item Values
                //alex 05172022
                //OnItemListSelected();
                //alex 05172022
                InvalidLabel.Text = "Changes were updated successfully.";
                //PopupInfoDialog("Changes were updated successfully.", false);
				if (DataForLabel.Text == "Data For Batch")//Update saved values in batch mode
				{
                    //alex 05172022
                    OnItemListSelected();
                    //alex 05172022
                    OnBatchModeClick(null, null);
					//return "";
				}
			}
            else
            {
                InvalidLabel.Text = errMsg;
                //PopupInfoDialog(errMsg, true);
            }
            return;
        }
		private string SaveExecute()
		{
			/*
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            */


			//if (DataForLabel.Text == "Data For Batch")//Update saved values in batch mode
			//{
			//	OnBatchModeClick(null, null);
			//	//return "";
			//}
			//else
			{
				var itemModel = GetItemModelFromView(ItemEditing.Value);
                //var itemValues = GetPricesForEditFromView().FindAll(m => m.NeedSave);
                var itemValues = GetPricesForEditFromView().FindAll(m => m.IsContainer == true);
                //var itemValues = GetPricesForEditFromView();
                var errMsg = QueryPrice.SaveAppraisal(itemValues, this);
                if (errMsg != "")
                    return errMsg;
                var cdItemValues = GetPricesForEditFromView().FindAll(m => m.PartName.ToLower().Contains("color") && m.PartName.ToLower().Contains("diamond"));
                errMsg = QueryPrice.SaveColoredDiamondAppraisal(cdItemValues, this);
                var table = new DataTable("MSRP");
                table.Columns.Add("GroupCode");
                table.Columns.Add("BatchCode");
                table.Columns.Add("ItemCode");
                table.Columns.Add("PartName");
                table.Columns.Add("Price");
                for (int i=0; i<= ItemValuesGrid.Items.Count-1; i++)
                {
                    if (ItemValuesGrid.Items[i].Cells[22].Text != null)
                    {
                        string price = "";
                        if (ItemValuesGrid.Items[i].Cells[0].Text.ToLower() == "true")
                        {
                            price = itemValues[0].PriceNew;
                            ItemValuesGrid.Items[i].Cells[22].Text = price;
                        }
                        else
                            price = ItemValuesGrid.Items[i].Cells[22].Text;
                        string groupCode = itemModel.OrderCode;
                        string batchCode = itemModel.BatchCode;
                        string itemCode = itemModel.ItemCode;
                        string partName = ItemValuesGrid.Items[i].Cells[5].Text;
                        //string price = ItemValuesGrid.Items[i].Cells[22].Text;
                        table.Rows.Add(new object[] { groupCode, batchCode, itemCode, partName, price });
                    }

                }
                table.AcceptChanges();
                if (table.Rows.Count > 0)
                    QueryPrice.SaveMSRP(table, this);
				return errMsg;
			}
        }
        protected void OnBulkUpdateCDClick(object sender, EventArgs e)
        {
            var cdItemValues = GetPricesForEditFromView().FindAll(m => m.PartName.ToLower().Contains("color") && m.PartName.ToLower().Contains("diamond"));
            foreach (var item in lstItemList.Items)
            {
                string cdItem = item.ToString();
                cdItem = cdItem.Substring(cdItem.Length - 2);
                string errMsg = QueryPrice.SaveColoredDiamondBulkAppraisal(cdItemValues, this, cdItem);
                if (errMsg != "")
                {
                    InvalidLabel.Text = "Error during colored diamond bulk update";
                    return;
                }
            }
            InvalidLabel.Text = "Colored Diamond Bulk Update Successful";
            //PopupInfoDialog("Colored Diamond Bulk Update Successful", true);
                
        }
        #endregion

        private bool HasUnSavedChanges()
        {
            GetNewValuesFromGrid();
			var itemValues = GetPricesForEditFromView().FindAll(m => m.NeedSave);
			//var itemValues = GetPricesForEditFromView().FindAll(m => m.IsContainer == true);
            return itemValues.Count > 0;
        }

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Question Dialog
        static string ModeOnItemChanges = "item";
        static string ModeOnLoadClick = "load";
        private void PopupSaveQDialog(string src)
        {
            var cnt = GetPricesForEditFromView().FindAll(m => m.NeedSave).Count;
            SaveDlgMode.Value = src;
            QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
            SaveQPopupExtender.Show();
        }
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            var errMsg = SaveExecute();
            if (!string.IsNullOrEmpty(errMsg))
            {
                // Return on prev item
                txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
                lstItemList.SelectedValue = ItemEditing.Value;
                InvalidLabel.Text = errMsg;
                //PopupInfoDialog(errMsg, true);
                return;
            }

            if (SaveDlgMode.Value == ModeOnItemChanges)
            {
                OnItemListSelected();
            }
            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
        }
        protected void OnQuesSaveNoClick(object sender, EventArgs e)
        {
            if (SaveDlgMode.Value == ModeOnItemChanges)
            {
                OnItemListSelected();
            }
            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
        }
        protected void OnQuesSaveCancelClick(object sender, EventArgs e)
        {
            lstItemList.SelectedValue = ItemEditing.Value;
            txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
        }

        #endregion

        //Sergey: Batch Mode
        #region Batch Mode
        private int checkBoxColumn = 24;

        // vy  get  dataset save in  session 
        protected void OnBatchModeClick2(object sender, EventArgs e)
        {




            List<PriceModel> batchPriceValues = new List<PriceModel>();

            int checkBoxCount = 0;

            foreach (ListItem item in lstItemList.Items)
            {
                var itemNumber = item.Value;
                var singleItemModel = GetItemModelFromView(itemNumber);
                if (singleItemModel == null) continue;
                var itemModel = GetItemModelFromView(itemNumber);
                var parts = GetPartsFromView();
                var priceValues = QueryPrice.GetPriceValues(itemModel, parts, this);
                priceValues.First().PartName = priceValues.First().ItemNumber.ToString();

                var checkBoxItem = ItemValuesGrid.Items[checkBoxCount].FindControl("BatchCheckBoxes") as CheckBox;

                //Find Metal Column, if exists, and dynamically adjust each one ONLY if check box is checked
                if (checkBoxItem.Checked)
                {
                    foreach (PriceModel row in priceValues)
                    {
                        if (row.PartName == "Metal")
                        {
                            priceValues.First().Price = (Decimal.Parse(priceValues.First().Price) - Decimal.Parse(row.Price) + Decimal.Parse(batchMetalTextBox.Text)).ToString("F2");//Adjust price
                            priceValues.First().MarkUp = QueryUtils.CalculateMarkup(Decimal.Parse(priceValues.First().Price), this.Page).ToString("F2");//Adjust markup based on new price
                            priceValues.First()._Appraisal = (Decimal.Parse(priceValues.First().Price) * Decimal.Parse(priceValues.First().MarkUp)).ToString("F2");//Adjust appraisal based on now price and markup
                            break;
                        }
                    }
                }

                batchPriceValues.Add(priceValues.First());
                checkBoxCount++;
            }

            SetViewState(batchPriceValues, ConstPricesForEdit);
            ItemValuesGrid.DataSource = batchPriceValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = true;

            ///



            //ItemValuesGrid.DataSource = btch.BatchT1;
            //ItemValuesGrid.DataBind();
            //ItemValuesGrid.Enabled = true;
            //DataForLabel.Text = "Data For Batch";
            //ShowHideItemDetails(true);
            //lstItemList.SelectedIndex = -1;//Unselect everything in List

            //LoadCurrencyDisplay();


        }


        protected void OnBatchModeClick(object sender, EventArgs e)
        {
            //CleanUp();
            /*
			var itemNumber = lstItemList.SelectedValue;
			var singleItemModel = GetItemModelFromView(itemNumber);
			if (singleItemModel == null) return;
			ItemEditing.Value = itemNumber;
			LoadPricesForEditing(itemNumber);

			from LoadPricesForEditing:
			var itemModel = GetItemModelFromView(itemNumber);
            var parts = GetPartsFromView();
            var priceValues = QueryPrice.GetPriceValues(itemModel, parts, this);
			SetViewState(priceValues, ConstPricesForEdit);

			ItemValuesGrid.DataSource = priceValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = true;
            DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            ShowHideItemDetails(true);
            cmdSave.Visible = true;

			
			*/
            bulkUpdateColoredDiamond.Visible = false;
            currencyPrice.Text = "";
            currencyPrice.Visible = false;
            currencySelector.Text = "USD";
            oldCurrencySelector.Value = "USD";
            ItemValuesGrid.Columns[checkBoxColumn].Visible = true;
            batchMetals.Visible = true;
            batchMetalTextBox.Visible = true;
            batchBumpItems.Visible = true;


            List<PriceModel> batchPriceValues = LoadPricesForBatchMode();


            //foreach (ListItem item in lstItemList.Items)
            //{
            //    var itemNumber = item.Value;
            //    var singleItemModel = GetItemModelFromView(itemNumber);
            //    if (singleItemModel == null) continue;
            //    var itemModel = GetItemModelFromView(itemNumber);
            //    var parts = GetPartsFromView();
            //    var priceValues = QueryPrice.GetPriceValuesBVY(itemModel, parts, this, btch);
            //    priceValues.First().PartName = priceValues.First().ItemNumber.ToString();
            //    batchPriceValues.Add(priceValues.First());
            //}

            SetViewState(batchPriceValues, ConstPricesForEdit);
            ItemValuesGrid.DataSource = batchPriceValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = true;
            DataForLabel.Text = "Data For Batch";
            ShowHideItemDetails(true);
            lstItemList.SelectedIndex = -1;//Unselect everything in List

            LoadCurrencyDisplay();
        }
        /*
		protected void OnBatchModeClick(object sender, EventArgs e)
		{
			//CleanUp();
			
			currencyPrice.Text = "";
			currencyPrice.Visible = false;
			currencySelector.Text = "USD";
			oldCurrencySelector.Value = "USD";
			ItemValuesGrid.Columns[checkBoxColumn].Visible = true;
			batchMetals.Visible = true;
			batchMetalTextBox.Visible = true;
			batchBumpItems.Visible = true;

			List<PriceModel> batchPriceValues = new List<PriceModel>();

			//foreach (ListItem item in lstItemList.Items)
			//{
			//	var itemNumber = item.Value;
			//	var singleItemModel = GetItemModelFromView(itemNumber);
			//	if (singleItemModel == null) continue;
			//	var itemModel = GetItemModelFromView(itemNumber);
			//	var parts = GetPartsFromView();
			//	var priceValues = QueryPrice.GetPriceValues(itemModel, parts, this);
			//	priceValues.First().PartName = priceValues.First().ItemNumber.ToString();
			//	batchPriceValues.Add(priceValues.First());
			//}
			var parts = GetPartsFromView();
			var itemNumberForBatch = lstItemList.Items[0].Value;
			var firstItem = GetItemModelFromView(itemNumberForBatch);
			var BatchID = firstItem.BatchId;
			//firstItem.ItemCode = null;
			batchPriceValues = QueryPrice.GetPriceValues(firstItem, 0, parts, this);
			SetViewState(batchPriceValues, ConstPricesForEdit);
			ItemValuesGrid.DataSource = batchPriceValues;
			ItemValuesGrid.DataBind();

			ItemValuesGrid.Enabled = true;
			DataForLabel.Text = "Data For Batch";
			ShowHideItemDetails(true);
			lstItemList.SelectedIndex = -1;//Unselect everything in List
			
			LoadCurrencyDisplay();
		}
        */
        protected void OnBatchMetalClick(object sender, EventArgs e)
        {
            List<PriceModel> batchPriceValues = new List<PriceModel>();

            int checkBoxCount = 0;

            foreach (ListItem item in lstItemList.Items)
            {
                var itemNumber = item.Value;
                var singleItemModel = GetItemModelFromView(itemNumber);
                if (singleItemModel == null) continue;
                var itemModel = GetItemModelFromView(itemNumber);
                var parts = GetPartsFromView();
                var priceValues = QueryPrice.GetPriceValuesVY(itemModel, parts, this, btch, itemNumber, this.BatchID);
                //alex var priceValues = QueryPrice.GetPriceValues(itemModel, parts, this);
                priceValues.First().PartName = priceValues.First().ItemNumber.ToString();

                var checkBoxItem = ItemValuesGrid.Items[checkBoxCount].FindControl("BatchCheckBoxes") as CheckBox;

                //Find Metal Column, if exists, and dynamically adjust each one ONLY if check box is checked
                if (checkBoxItem.Checked)
                {
                    foreach (PriceModel row in priceValues)
                    {
                        if (row.PartName == "Metal")
                        {
                            priceValues.First().Price = (Decimal.Parse(priceValues.First().Price) - Decimal.Parse(row.Price) + Decimal.Parse(batchMetalTextBox.Text)).ToString("F2");//Adjust price
                            priceValues.First().MarkUp = QueryUtils.CalculateMarkup(Decimal.Parse(priceValues.First().Price), this.Page).ToString("F2");//Adjust markup based on new price
                            priceValues.First()._Appraisal = (Decimal.Parse(priceValues.First().Price) * Decimal.Parse(priceValues.First().MarkUp)).ToString("F2");//Adjust appraisal based on now price and markup
                            break;
                        }
                    }
                }

                batchPriceValues.Add(priceValues.First());
                checkBoxCount++;
            }

            SetViewState(batchPriceValues, ConstPricesForEdit);
            ItemValuesGrid.DataSource = batchPriceValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = true;
        }

        protected void OnBatchBumpClick(Object sender, EventArgs e)
        {
            Decimal bump = Decimal.Parse(batchBumpTextBox.Text) / 100;

            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                var checkBoxItem = item.FindControl("BatchCheckBoxes") as CheckBox;

                if (checkBoxItem.Checked)
                {
                    var price = item.FindControl("ValueNumericFld") as TextBox;
                    price.Text = (Decimal.Parse(price.Text) * (1 + bump)).ToString("F2");
                }
            }
        }

        protected void batchSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            var headcbox = sender as CheckBox;

            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                var cbox = item.FindControl("BatchCheckBoxes") as CheckBox;
                cbox.Checked = headcbox.Checked;
            }
        }
        #endregion
    }
}