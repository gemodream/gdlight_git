<%@ Page language="c#" Codebehind="Itemize.aspx.cs" AutoEventWireup="True" Inherits="Corpt.Itemize" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Itemize</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
		<script language="JavaScript" type="text/javascript">
var newwindow;
function poptastic(url)
{
	newwindow=window.open(url,'name','top=5,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
	if (window.focus) {newwindow.focus()}
}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<A href="GDLight.aspx"><IMG style="Z-INDEX: 109; POSITION: absolute; TOP: 0px; LEFT: 0px" border="0" src="Images/H.gif"
					width="16" height="16"></A>
			<asp:textbox style="Z-INDEX: 105; POSITION: absolute; TOP: 32px; LEFT: 136px" id="txtOrderNumber"
				runat="server" AutoPostBack="True" CssClass="inputStyleCC" ontextchanged="txtOrderNumber_TextChanged"></asp:textbox><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 32px; LEFT: 40px" id="Label1" runat="server"
				CssClass="text">Order Number</asp:label><asp:panel style="Z-INDEX: 107; POSITION: absolute; TOP: 144px; LEFT: 40px" id="pnlCP" runat="server"
				Height="194px" Width="496px" BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
				<TABLE border="0" cellSpacing="0" cellPadding="0">
					<TR>
						<TD>
							<asp:Label id="Label4" runat="server" CssClass="text">Memo Number</asp:Label></TD>
						<TD>
							<asp:DropDownList id="lstMemoNumber" runat="server" Width="152px" Font-Bold="True" BackColor="#EFF7FA"
								Font-Names="Verdana" Font-Size="10px" ForeColor="#1C4C8E"></asp:DropDownList>
							<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="text" ControlToValidate="lstMemoNumber"
								ErrorMessage="*"></asp:RequiredFieldValidator></TD>
						<TD>&nbsp;</TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="Label5" runat="server" CssClass="text">Number of items in the division</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtNumberOfItemsDivision" runat="server" CssClass="inputStyleCC"></asp:TextBox>
							<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" CssClass="text" ControlToValidate="txtNumberOfItemsDivision"
								ErrorMessage="*" ValidationExpression="\d*"></asp:RegularExpressionValidator>
							<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" CssClass="text" ControlToValidate="txtNumberOfItemsDivision"
								ErrorMessage="*"></asp:RequiredFieldValidator></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="Label6" runat="server" CssClass="text">Customer Program</asp:Label></TD>
						<TD>
							<asp:DropDownList id="lstCustomerProgram" runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstCustomerProgram_SelectedIndexChanged"></asp:DropDownList>
							<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" CssClass="text" ControlToValidate="lstCustomerProgram"
								ErrorMessage="*"></asp:RequiredFieldValidator></TD>
						<TD>
							<asp:Button id="cmdSelectProgram" runat="server" CssClass="buttonStyle" Text="." CausesValidation="False" onclick="cmdSelectProgram_Click"></asp:Button></TD>
					</TR>
					<TR>
						<TD>&nbsp;</TD>
						<TD>
							<asp:Image id="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image></TD>
						<TD></TD>
					</TR>
				</TABLE>
				<asp:Button id="cmdGo" runat="server" CssClass="buttonStyle" Text="GO" onclick="cmdGo_Click"></asp:Button>
				<IMG name="PleaseWaitPicture" alt="" src="images/5-0.gif" width="32" height="32">
			</asp:panel>
            <asp:label style="Z-INDEX: 108; POSITION: absolute; TOP: 32px; LEFT: 320px" id="lblCustName"
				runat="server" CssClass="text" Font-Bold="True"></asp:label>
        <!-- Message Panel: DataGrid, Dismiss button, Printer button -->
        <asp:Panel Style="z-index: 115; position: absolute; padding-bottom: 0px; padding-left: 0px;
            padding-right: 0px; top: 16px; padding-top: 0px; left: 16px" ID="pnlMessage"
            runat="server" Height="328px" Width="440px" Visible="False">
            <p class="text" align="center">
                Summary:
                <asp:DataGrid ID="DataGrid3" runat="server" CssClass="text">
                </asp:DataGrid></p>
            <asp:Button ID="cmdDismiss" runat="server" CssClass="buttonStyle" Text="Dismiss"
                CausesValidation="False" OnClick="cmdDismiss_Click"></asp:Button>
            <input class="buttonStyle" onclick="poptastic('PrintItemizedList.aspx')" value="Printer-Friendly"
                type="button">
            </asp:Panel>

        <asp:button style="Z-INDEX: 110; POSITION: absolute; TOP: 432px; LEFT: 520px" id="cmdClearAll"
				runat="server" CssClass="buttonStyle" Text="Clear" CausesValidation="False" onclick="cmdClearAll_Click"></asp:button><asp:panel style="Z-INDEX: 104; POSITION: absolute; TOP: 48px; LEFT: 16px" id="pnlInstruction"
				runat="server" CssClass="text" Height="56px" Width="576px">
				
			</asp:panel><asp:panel style="Z-INDEX: 103; POSITION: absolute; TOP: 360px; LEFT: 16px" id="pnlInstructions"
				runat="server" Height="232px" Width="496px">
<P class="text"><B>Instructions:</B>
<br/>
This form is for <B>simple itemizing</B> It lets you to create 
					necessary number of items by order, SKU and memo number.<BR>
					<BR>
					Please carefully read the instructions<BR>
					on the bottom of this page <B>before</B> using this form.</P>
					<OL class="text" type="1">
						<LI>
					Enter the order number. Hit Enter. If order number is entered correctly you 
					will see:
					<OL type="a">
						<LI>
						Customer name on the right of the order number;
						<LI>
						List of memo/rtv numbers in the order populated. If order has no memo/rtv 
						number the list would reflect that.
						<LI>
							List of SKUs populated.
						</LI>
					</OL>
					<LI>
					Select Memo/Rtv number from the list; Select SKU from the list; Enter desired 
					number of items.
					<LI>
						Click �GO� button (If you have forgotten to enter any information you will see 
						the field marked by the red asterisk. You have to fix the problem before you 
						can continue). PLEASE NOTE: Depending on the number of items the procedure 
						might take SEVERAL MINUTES to complete. <STRONG>Press GO only ONCE</STRONG>
					- BE PATIENT. You will see the conformation screen once the itemizing is done.
					<LI>
						From the conformation screen you can:
						<OL type="a">
					<LI>
					Hit �Clear� if you done with this order;
					<LI>
					Hit �Dismiss� if you want to dismiss the conformation screen and go back to the 
					itemizing screen (to add more items to the order);
					<LI>
						Hit �Print� if you want to see (and print) the printable summary screen.
					</LI></OL></LI></OL>
<P></P></asp:panel>
        <asp:Panel Style="z-index: 111; position: absolute; top: 16px; left: 608px" ID="lotNumbers"
            runat="server" Height="554px" Width="256px" BorderWidth="1px" BorderColor="#E0E0E0"
            BorderStyle="Solid">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <p class="text" align="center">
                            Lot Numbers</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td style="width: 150px">
                                    <asp:TextBox ID="txtLotNumber" runat="server" CssClass="inputStyleCC"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="cmdAddLot" runat="server" CssClass="buttonStyle" Text="Add" CausesValidation="False"
                                        OnClick="cmdAddLot_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">
                                    <asp:ListBox ID="lstLotNumbers" runat="server" CssClass="inputStyleCC" Width="152px"
                                        Height="464px" Font-Bold="True" BackColor="#EFF7FA" Font-Names="Verdana" Font-Size="10px"
                                        ForeColor="#1C4C8E"></asp:ListBox>
                                </td>
                                <td valign="top" align="left">
                                    <asp:Button ID="cmdDeleteLot" runat="server" CssClass="buttonStyle" Text="Delete"
                                        CausesValidation="False" OnClick="cmdDeleteLot_Click"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="Label7" runat="server" CssClass="text"># of itms:</asp:Label>
                        <asp:Label ID="lblNumberOfLotNumbers" runat="server" CssClass="text"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="cmdDone" runat="server" CssClass="buttonStyle" Text="Done" CausesValidation="False"
                            OnClick="cmdDone_Click"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:DataGrid Style="z-index: 112; position: absolute; top: 592px; left: 608px" ID="DataGrid1"
            runat="server" CssClass="text">
        </asp:DataGrid>
        <br/>
        <asp:Label Style="z-index: 113; position: absolute; top: 0px; left: 480px" ID="Label2"
            runat="server" CssClass="text" Font-Bold="True" Font-Size="Large"></asp:Label>
        <asp:Label Style="z-index: 114; position: absolute; top: 576px; left: 608px" ID="Label3"
            runat="server" CssClass="text"></asp:Label>
        <asp:RadioButtonList Style="z-index: 102; position: absolute; top: 112px; left: 184px"
            ID="modeSelector" runat="server" AutoPostBack="True" CssClass="text" Width="200px"
            RepeatDirection="Horizontal" OnSelectedIndexChanged="modeSelector_SelectedIndexChanged">
            <asp:ListItem Value="0">BLK</asp:ListItem>
            <asp:ListItem Value="1">Lot List</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Image Style="z-index: 101; position: absolute; top: -104px; left: 288px" ID="Image1"
            runat="server" ImageUrl="Images/newmessage.gif"></asp:Image>
        </form>
	</body>
</HTML>
