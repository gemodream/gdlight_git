﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/DefaultMaster.Master" CodeBehind="ScreeningStatisticReport.aspx.cs" Inherits="Corpt.ScreeningStatisticReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

	<ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
	</ajaxToolkit:ToolkitScriptManager>

	<style>
		.bootRBL input {
			display: inline;
			margin-right: 5px;
			margin-top: 0px;
		}

		.bootRBL label {
			display: inline;
			margin-right: 0.5em;
		}

		.subHeading {
			font-size: large;
			padding-bottom: 10px;
		}

		body {
			font-family: Tahoma, Arial, sans-serif
		}

		.vl {
			border-left: 6px solid green;
			height: 10px;
			margin-left: 7px;
		}

		.dot {
			height: 20px;
			width: 20px;
			background-color: green;
			font-size: 20px;
			color: white;
		}

		.filedSetBorder legend {
			padding-left: 7px;
			padding-right: 7px;
			margin-bottom: 0px;
			border-bottom: 0px !important;
			font-family: verdana,tahoma,helvetica;
			width: max-content;
		}

		.filedSetBorder {
			border: 1px groove black !important;
			padding: 10px 10px 10px 10px;
			border-radius: 8px;
			margin: 0px 5px 5px 5px;
			float: left;
			vertical-align: top;
		}
		.radioButtonList input[type="radio"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			margin-top: 13px !important;
			margin-top: 10px !important;
			margin-left: 7px ;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.radioButtonList label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			height: 30px;
			padding-left: 30px;
			width: max-content;
			line-height: 30px;
			/*margin: 5px 5px 5px 5px;*/
			padding-top: 5px;
			padding-bottom: 5px;
		}
	</style>
	<style>
		.lbl {
			width: 400px;
			display: inline-block;
			font-size: 12px;
		}

		select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
			display: inline-block;
			padding: 4px 6px;
			margin-bottom: 3px;
			font-size: 12px;
			line-height: 13px;
			color: black; /* #555555;*/
			vertical-align: middle;
			font-family: Tahoma,Arial,sans-serif;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
		}

		input, button, select, textarea {
			font-family: Tahoma,Arial,sans-serif;
			font-size: 14px;
		}

		input[type="checkbox"], label {
			margin-top: 0;
			margin-bottom: 0;
			line-height: normal;
		}

		body {
			font-family: Tahoma,Arial,sans-serif;
			/*font-size: 75%;*/
		}

		.headingPanel {
			padding-bottom: 2px;
			color: #5377A9;
			font-family: Arial, Sans-Serif;
			font-weight: bold;
			font-size: 1.0em;
		}

		.text_highlitedyellow {
			background-color: #FFFF00;
		}

		.text_nohighlitedyellow {
			background-color: white;
		}
	</style>
	<asp:UpdatePanel runat="server" ID="MainPanel">
			<ContentTemplate>
	 <div class="demoarea">
			 <div style="font-size: smaller;height:25px;">
								<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
									<ProgressTemplate>
										<img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
										<b>Please, wait....</b>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
		<div class="demoheading">Statistic Report</div>

		
				<div style="min-height: 500px;">

					<table>
						<tr>
							<td style="vertical-align: top;">
								<table style="margin-left: 20px;" >

									<tr>
										<td class="subHeading" style=""><br />Track By:</td>
										<td></td>
									</tr>
									<tr>
										<td style="padding-top:0px;">
											<asp:TextBox ID="txtSearch" runat="server" Style="width: 224px;height:25px;"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtSearch"
												Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Detail is required."
												ValidationGroup="BatchGroup" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
												HighlightCssClass="validatorCalloutHighlight" />
										</td>
										
									</tr>
									<tr>
										<td style="padding-top:10px;">
											<div class="radioButtonList">
												<asp:RadioButton ID="rbOrderNo" runat="server" GroupName="Search" Text="Order No." TextAlign="right" OnCheckedChanged="CheckedChanged" AutoPostBack="true" Checked="true" style="margin-right:10px;"  />
												<asp:RadioButton ID="rbRequestNo" runat="server" GroupName="Search" Text="Request No." TextAlign="right" OnCheckedChanged="CheckedChanged" AutoPostBack="true" style="margin-right:10px;" />
												<asp:RadioButton ID="rbMemoNo" runat="server" GroupName="Search" Text="Memo No." TextAlign="right" OnCheckedChanged="CheckedChanged" AutoPostBack="true" style="margin-right:10px;" />
											</div>
											<br />
										</td>
										
									</tr>

									

									<tr>

										<td colspan="1" style="padding-top:10px;">
											<asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-info btn-large" Style="margin-right: 10px;" OnClick="btnSubmit_Click" />
											<asp:Button ID="btnPrint" runat="server" Text="Print" class="btn btn-info btn-large" Style="margin-right: 10px;" OnClick="btnPrint_Click"  />
											<asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-info btn-large" OnClick="btnClear_Click" />
											<%--<asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-info btn-large" PostBackUrl="~/Default.aspx" />--%>
											<asp:Label ID="lblMsg" runat="server" Style="color: red; font-weight: bold;"></asp:Label>
										</td>

									</tr>
								</table>

								<table id="tblDetails" runat="server" visible="false" style="margin-left: 20px;">
									<tr>
										<td colspan="2">
											<div id="divDetails" runat="server" style="padding-left: 0px;">
												<fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; margin-top: 10px;">
													<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
														class="label">Order Information</legend>

													<table cellspacing="15">

														<tr>
															<td style="width: 120px;">Request No.:</td>
															<td style="width: 200px;">
																<label id="lblRequest" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td style="width: 120px;">Order No.:</td>
															<td style="width: 200px;">
																<label id="lblOrder" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>
														<tr>
															<td>Memo No.:</td>
															<td style="width: 200px;">
																<label id="lblMemo" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td>PO No.:</td>
															<td>
																<label id="lblPO" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>
														<tr>
															<td>Style:</td>
															<td>
																<label id="lblStyle" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td>SKU:</td>
															<td>
																<label id="lblSKU" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>

														<tr>
															<td>Delivery Method:</td>
															<td>
																<label id="lblDeliveryMethod" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td>Carrier:</td>
															<td>
																<label id="lblCarrier" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>

														<tr>
															<td>Retailer:</td>
															<td>
																<label id="lblRetailer" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td>Category:</td>
															<td>
																<label id="lblCategory" style="margin-bottom: 0px;" runat="server"></label>
															</td>

														</tr>
														<tr>
															<td>Service Type:</td>
															<td>
																<label id="lblServiceType" style="margin-bottom: 0px;" runat="server"></label>
															</td>

															<td>Service Time:</td>
															<td>
																<label id="lblServiceTime" style="margin-bottom: 0px;" runat="server"></label>
																<asp:HiddenField ID="hdnStatus" runat="server" />
															</td>

														</tr>
														<tr>
															<td>Jewelry Type:</td>
															<td>
																<label id="lblJewelryType" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>
														<tr>
															<td>Total Quantity:</td>
															<td>
																<label id="lblQty" style="margin-bottom: 0px;" runat="server"></label>
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<div>
																	<table>
																		<tr>
																			<td>
																				<label id="lblQty1" runat="server"></label>
																			</td>
																			<td>
																				<label id="lblPass" runat="server" style="padding-left: 15px;"></label>
																			</td>
																			<td>
																				<label id="lblFail" runat="server" style="padding-left: 15px;"></label>
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
														</tr>

														<tr>
															<td colspan="2">

																<div>
																	<table>
																		<tr>
																			<td style="font-weight: bold; padding-top: 10px;">Comments:</td>
																		</tr>

																		<tr>
																			<td>
																				<label id="lblComments" runat="server"></label>
																			</td>
																		</tr>
																	</table>
																</div>

															</td>
														</tr>

													</table>
												</fieldset>
											</div>

											<div id="divResult" runat="server" visible="false">
												<fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; margin-top: 10px;">
													<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
														class="label">Screening / Testing Information</legend>
													<table>

														<tr>
															<td colspan="2" style="font-weight: bold; padding-top: 10px; padding-bottom: 10px;">Screener</td>
															<td colspan="2" style="font-weight: bold; padding-top: 10px; padding-bottom: 10px; padding-left: 30px;">Tester</td>
														</tr>

														<tr>
															<td>Total Pass Screening Items/Stones:</td>
															<td>
																<label id="lblScreeningPass" runat="server"></label>
															</td>

															<td style="padding-left: 30px;">Total Synthetic Items/Stones:</td>
															<td>
																<label id="lblTestSysthetic" runat="server"></label>
															</td>
														</tr>
														<tr>
															<td>Total Fail Screening Items/Stones:</td>
															<td>
																<label id="lblScreeningFail" runat="server"></label>
															</td>

															<td style="padding-left: 30px;">Total Suspect Items/Stones:</td>
															<td>
																<label id="lblTestSuspect" runat="server"></label>
															</td>
														</tr>

														<tr>
															<td></td>
															<td></td>
															<td style="padding-left: 30px;">Total Pass Items/Stones:</td>
															<td>
																<label id="lblTestPass" runat="server"></label>
															</td>
													</table>
												</fieldset>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td style="vertical-align: top;">
								<table>
									<tr>
										<td style="vertical-align: top; padding-left: 70px; padding-top: 0px;">

											<asp:DataList ID="dtlProgress" runat="server" RepeatColumns="1" RepeatDirection="Vertical" ItemStyle-VerticalAlign="Bottom" BorderWidth="0" CellPadding="0" CellSpacing="0" OnItemDataBound="dtlProgress_ItemDataBound">
												<ItemTemplate>
													<table cellpadding="0" cellspacing="0" border="0">

														<tr>
															<td>
																<div class="vl" runat="server" id="firstline"></div>
																<div class="dot" runat="server" id="tdStatus">&#10004;</div>
																<div class="vl" runat="server" id="lastline"></div>
															</td>
															<td style="vertical-align: middle; padding-left: 10px;">
																<asp:Label ID="lblValue" runat="server" Text='<%#Eval("StatusName") %>' Width="150px" Style="word-wrap: break-word; padding: 6px;"></asp:Label>
																<asp:Label ID="lblKey" runat="server" Text='<%#Eval("StatusID") %>' Visible="false" Style="word-wrap: break-word;"></asp:Label>
															</td>
														</tr>

													</table>
												</ItemTemplate>
											</asp:DataList>
										</td>
										<td style="vertical-align: bottom; padding-left: 50px; padding-bottom: 20px;"></td>
									</tr>
								</table>

							</td>
						</tr>

					</table>

					<table id="tblBatchInfo" runat="server" visible="false" style="margin-left: 20px;">
						<tr>
							<td>
								<fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; width: 600px; margin-top: 10px;">
									<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 10px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
										class="label">Batch Information</legend>
									<table>
										<tr>
											<asp:GridView ID="gvBatch" runat="server" AutoGenerateColumns="false" Width="650px">
												<Columns>
													<asp:BoundField DataField="BatchCode" HeaderText="Batch Code" ItemStyle-Width="30" />
													<asp:BoundField DataField="Quantity" HeaderText="Qty" ItemStyle-Width="30" />
													<asp:BoundField DataField="TotalPassScreening" HeaderText="Screening Pass" ItemStyle-Width="30" />
													<asp:BoundField DataField="TotalFailScreening" HeaderText="Screening Fail" ItemStyle-Width="30" />
													<asp:BoundField DataField="TotalPass" HeaderText="Test Pass" ItemStyle-Width="30" />
													<asp:BoundField DataField="TotalSyntheticTesting" HeaderText="Test Synthetic" ItemStyle-Width="30" />
													<asp:BoundField DataField="TotalSuspectTesting" HeaderText="Test Suspect" ItemStyle-Width="30" />
													<asp:BoundField DataField="Screener" HeaderText="Screener" ItemStyle-Width="100" />
													<asp:BoundField DataField="Tester" HeaderText="Tester" ItemStyle-Width="100" />
												</Columns>


											</asp:GridView>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>

				</div>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
		</asp:UpdatePanel>

	</div>

</asp:Content>

