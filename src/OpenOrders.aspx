﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="OpenOrders.aspx.cs" Inherits="Corpt.OpenOrders" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
<%--    <script type="text/javascript">--%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScrollPercent();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScrollPercent();--%>
<%----%>
<%--        });--%>
<%--        function gridviewScrollPercent() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=grdOrders.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 1--%>
<%--            });--%>
<%--        }--%>
<%--    </script>--%>
   <div class="demoheading">Open Orders</div>
    <div class="form-inline" style="font-family: Arial; font-size: 12px; margin-bottom: 10px;">
        <!-- Customer -->
       
        <asp:Panel runat="server" DefaultButton="CustomerLikeButton"> Customer Like
            <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                width: 80px" placeHolder="Customer Like"></asp:TextBox>
            <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
        </asp:Panel>
        <asp:Panel runat="server" DefaultButton="cmdLoad">
            <asp:DropDownList ID="lstCustomerList" runat="server" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                Width="400px" Style="font-family: Arial; font-size: 12px" />
            <!-- Date From -->
            <label style="margin-left: 5px; margin-right: 5px; text-align: right">
                From</label>
            <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
                AutoPostBack="True" Style="margin-left: 3px; height: 21px" />
            <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <!-- Date To -->
            <label style="margin-left: 5px; margin-right: 5px; text-align: right">
                To</label>
            <asp:TextBox runat="server" ID="calTo" Width="100px" OnTextChanged="OnChangedDateTo"
                AutoPostBack="True" />
            <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Lookup" OnClick="OnLoadClick"
                Style="margin-left: 15px"></asp:Button>
        </asp:Panel>
        <ajaxToolkit:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="lstCustomerList"
            Enabled="True" PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains"
            QueryTimeout="2000">
        </ajaxToolkit:ListSearchExtender>
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
            PopupButtonID="Image1" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
            PopupButtonID="Image2" />
    </div>
    <!-- Info labels -->
    <div style="font-family: Arial; font-size: 12px; font-weight: bold">
        <asp:Label ID="lblNumberOfItems" runat="server" CssClass="control-label"></asp:Label><br />
    </div>

    <!-- DataGrid -->
    <div id="gridContainer" style="font-size: small">
        <asp:DataGrid ID="grdOrders" runat="server" Enabled="True" AllowSorting="true" OnSortCommand="OnSortCommand" CellPadding="5">
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
        </asp:DataGrid>
    </div>

</asp:Content>
