using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for ChangePassword.
	/// </summary>
	public partial class ChangePassword : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void PasswordChange_Click(object sender, System.EventArgs e)
		{
			if((ReportNumber.Text!="")||(NewPassword1.Text!="")||(NewPassword2.Text!="")||(NewPassword1.Text==NewPassword2.Text))
			{
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("sp_ChangePassword");
				command.CommandType=CommandType.StoredProcedure;
				command.Connection=conn;
				/*@Result integer out,
				@UserID integer,
				@OldPassword varchar(255),
				@NewPassword varchar(255)
				*/
				command.Parameters.Add("@Result",SqlDbType.Int);
				command.Parameters["@Result"].Direction=ParameterDirection.Output;

				command.Parameters.Add("@UserID",SqlDbType.Int);
				command.Parameters["@UserID"].Value=Int32.Parse(Session["ID"].ToString());

				command.Parameters.Add("@OldPassword",SqlDbType.VarChar,255);
				command.Parameters["@OldPassword"].Value=ReportNumber.Text.Trim();

				command.Parameters.Add("@NewPassword",SqlDbType.VarChar,255);
				command.Parameters["@NewPassword"].Value=NewPassword1.Text.Trim();

				conn.Open();

				command.ExecuteNonQuery();
				switch(Int32.Parse(command.Parameters["@Result"].Value.ToString()))
				{
					case 1: WrongInfoLabel.Text="Password changed succsessfully"; break;
					case -1: WrongInfoLabel.Text="Password was NOT changed. Please check that you have entered the old password correctly."; break;
				}
				conn.Close();
			}
			else
			{
				WrongInfoLabel.Text="Password was NOT changed. Please check that you have filled the fields correctly and try again";
			}
		}
	}
}
