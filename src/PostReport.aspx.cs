using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace Corpt
{
	/// <summary>
	/// Summary description for PostReport.
	/// </summary>
	public partial class PostReport : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				ReFillCustomerList();
				calTo.SelectedDate=DateTime.Now.Date;
				calFrom.SelectedDate=DateTime.Now.Subtract(new TimeSpan(14,0,0,0,0)).Date;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ReFillCustomerList()
		{
			lstCustomerList.Items.Clear();
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
				
			SqlCommand command = new SqlCommand("spGetCustomers");
			
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@AuthorID",Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			SqlDataReader reader = command.ExecuteReader();
			if(reader.HasRows)
			{
				lstCustomerList.DataSource=reader;
				lstCustomerList.DataTextField="CustomerName";
				lstCustomerList.DataValueField="CustomerOfficeID_CustomerID";
				lstCustomerList.DataBind();				
			}
			conn.Close();
		}

		private void lstOrderNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}

		protected void lstCustomerList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstCustomerList.SelectedIndex>-1)
			{
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

				SqlCommand command = new SqlCommand("sp_GetBatchesByCustomerByDateRange");
			
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;

				command.Parameters.Add("@CustomerID",SqlDbType.Int).Value=int.Parse(lstCustomerList.SelectedValue.Split('_')[1]);

				if(calFrom.SelectedDate > DateTime.Parse("01/01/0001"))
				{
					command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
					command.Parameters["@DateFrom"].Value=calFrom.SelectedDate;//.Subtract(TimeSpan.FromDays(1));
				}
				//if(calTo.SelectedDate!=DateTime.Now.Date && calTo.SelectedDate > DateTime.Parse("01/01/0001"))
				if(calTo.SelectedDate > DateTime.Parse("01/01/0001"))
				{
					command.Parameters.Add("@DateTo", SqlDbType.DateTime);
					command.Parameters["@DateTo"].Value=calTo.SelectedDate.AddDays(1);
				}


				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);

				da.Fill(dt);

				dt.Columns.Add(new DataColumn("CombinedBatchNumber"));

				foreach(DataRow dr in dt.Rows)
				{
					dr["CombinedBatchNumber"]= Utils.FillToFiveChars(dr["GroupCode"].ToString()) + Utils.FillToThreeChars(dr["BatchCode"].ToString(), dr["GroupCode"].ToString());
				}

				if(dt.Rows.Count>0)
				{
					DataView dw = new DataView(dt);
					dw.Sort = "CombinedBatchNumber";
					
					orderList.DataSource=dw;
					orderList.DataTextField="CombinedBatchNumber";
					orderList.DataValueField="BatchID";
					orderList.DataBind();
				}
			}
		}

		protected void cmdListProgram_Click(object sender, System.EventArgs e)
		{
			lstCustomerList_SelectedIndexChanged(sender, e);
		}

		protected void orderList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(orderList.SelectedIndex>-1)
			{
				lstFiles.Items.Clear();
				DirectoryInfo myDir = new DirectoryInfo(Session["GIFDocumentRoot"].ToString());
				foreach(FileInfo fi in myDir.GetFiles("?" + orderList.SelectedItem.Text + "??.??????????.jpg"))
				{
					lstFiles.Items.Add(fi.Name);
				}
			}
		}

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			orderList_SelectedIndexChanged(sender, e);
		}

		protected void cmdAddSelected_Click(object sender, System.EventArgs e)
		{
			if(lstFiles.Items.Count>0)
			{
				foreach(ListItem li in lstFiles.Items)
				{
					if(li.Selected)
					{
						if(Session["lstToPrint"]!=null)
						{
							DataTable dt = Session["lstToPrint"] as DataTable;
							DataRow dr = dt.NewRow();
							dr[0]=li.Text;
							dr[1]=li.Value;
							dt.Rows.Add(dr);
							Session["lstToPrint"] = Utils.DeDuper(dt);
						}
						else
						{
							DataTable dt = new DataTable();
							dt.Columns.Add(new DataColumn("Text"));
							dt.Columns.Add(new DataColumn("Value"));
							DataRow dr = dt.NewRow();
							dr[0]=li.Text;
							dr[1]=li.Value;
							dt.Rows.Add(dr);
							Session["lstToPrint"] = Utils.DeDuper(dt);
						}						
					}
				}
			}
			
			if(Session["lstToPrint"]!=null)
			{
				DataView dw = new DataView(Session["lstToPrint"] as DataTable);
				dw.Sort="Text";
				lstToPost.DataSource=dw;
				lstToPost.DataTextField="Text";
				lstToPost.DataValueField="Value";
				lstToPost.DataBind();
			}
		}

		protected void cmdAddAll_Click(object sender, System.EventArgs e)
		{			
			foreach(ListItem li in lstFiles.Items)
			{
				foreach(ListItem li2 in lstFiles.Items)
				{
				
					if(Session["lstToPrint"]!=null)
					{
						DataTable dt = Session["lstToPrint"] as DataTable;
						DataRow dr = dt.NewRow();
						dr[0]=li2.Text;
						dr[1]=li2.Value;
						dt.Rows.Add(dr);
						Session["lstToPrint"] = Utils.DeDuper(dt);
					}
					else
					{
						DataTable dt = new DataTable();
						dt.Columns.Add(new DataColumn("Text"));
						dt.Columns.Add(new DataColumn("Value"));
						DataRow dr = dt.NewRow();
						dr[0]=li2.Text;
						dr[1]=li2.Value;
						dt.Rows.Add(dr);
						Session["lstToPrint"] = Utils.DeDuper(dt);
					}
				}
			}
			if(Session["lstToPrint"]!=null)
			{
				DataView dw = new DataView(Session["lstToPrint"] as DataTable);
				dw.Sort="Text";
				lstToPost.DataSource=dw;
				lstToPost.DataTextField="Text";
				lstToPost.DataValueField="Value";
				lstToPost.DataBind();
			}
		}

		protected void cmdClear_Click(object sender, System.EventArgs e)
		{
			Session["lstToPrint"]=null;
			lstToPost.Items.Clear();
		}

		protected void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if((lstToPost.Items.Count>0)&&(Session["lstToPrint"]!=null))
			{
				DataTable dt = Session["lstToPrint"] as DataTable;
				DataView dw = new DataView(Session["lstToPrint"] as DataTable);
				dw.Sort="Text";

								
//				while(lstToPost.SelectedIndex>-1)
//				{
//					sl.Add(lstToPost.SelectedItem,lstToPost.SelectedItem);
//					lstToPost.Items.RemoveAt(lstToPost.SelectedIndex);
//				}

				if(lstToPost.SelectedIndex>-1)
				{
                    ArrayList sl = new ArrayList();
					
					foreach(ListItem li in lstToPost.Items)
					{
						if(li.Selected)
						{
							sl.Add(li);
						}
					}

					foreach(ListItem li in sl)
					{
					
						dt.Select("Text='"+li.Text+"'")[0].Delete();
					}
				
					lstToPost.DataSource=dw;
					lstToPost.DataBind();
				}
			}
		}

		protected void cmdPost_Click(object sender, System.EventArgs e)
		{
			//2FTP
			if(lstToPost.Items.Count>0)
			{
				foreach(ListItem li in lstToPost.Items)
				{
					File.Copy(Session["GIFDocumentRoot"].ToString()+li.Text,Session["2FTP"].ToString()+li.Text,true);
				}
				lstToPost.Items.Clear();
			}
		}
	}
}
