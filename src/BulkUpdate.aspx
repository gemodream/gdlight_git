<%@ Page language="c#" Codebehind="BulkUpdate.aspx.cs" AutoEventWireup="True" Inherits="Corpt.BulkUpdate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Itemize</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/main.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" type="text/javascript">
var newwindow;
function poptastic(url)
{
	newwindow=window.open(url,'name','top=5,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
	if (window.focus) {newwindow.focus()}
}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<A href="GDLight.aspx"><IMG style="Z-INDEX: 101; POSITION: absolute; TOP: 0px; LEFT: 0px" height="16" src="Images/H.gif"
					width="16" border="0"></A>
			<asp:textbox id="txtInt" style="Z-INDEX: 100; POSITION: absolute; TOP: 528px; LEFT: 392px" runat="server"
				CssClass="inputStyleCC"></asp:textbox>
			<asp:Button id="cmdGo" style="Z-INDEX: 102; POSITION: absolute; TOP: 656px; LEFT: 328px" runat="server"
				CssClass="buttonStyle" Text="GO" onclick="cmdGo_Click"></asp:Button>
			<asp:DropDownList id="lstEnum" style="Z-INDEX: 104; POSITION: absolute; TOP: 528px; LEFT: 24px" runat="server"
				CssClass="inputStyleCC5"></asp:DropDownList>
			<asp:textbox id="txtOrder" style="Z-INDEX: 105; POSITION: absolute; TOP: 560px; LEFT: 64px" runat="server"
				CssClass="inputStyleCC" MaxLength="5"></asp:textbox>
			<asp:textbox id="txtBatchTo" style="Z-INDEX: 106; POSITION: absolute; TOP: 592px; LEFT: 280px"
				runat="server" CssClass="inputStyleCC" MaxLength="5"></asp:textbox>
			<asp:textbox id="txtBatchFrom" style="Z-INDEX: 109; POSITION: absolute; TOP: 592px; LEFT: 96px"
				runat="server" CssClass="inputStyleCC" MaxLength="5"></asp:textbox>
			<asp:Label id="Label1" style="Z-INDEX: 110; POSITION: absolute; TOP: 560px; LEFT: 24px" runat="server"
				CssClass="text">Order</asp:Label>
			<asp:Label id="Label2" style="Z-INDEX: 111; POSITION: absolute; TOP: 592px; LEFT: 24px" runat="server"
				CssClass="text">Batch From</asp:Label>
			<asp:Label id="Label3" style="Z-INDEX: 112; POSITION: absolute; TOP: 592px; LEFT: 256px" runat="server"
				CssClass="text">to</asp:Label>
			<asp:RadioButtonList id="rblMeasure" style="Z-INDEX: 113; POSITION: absolute; TOP: 80px; LEFT: 24px"
				runat="server" CssClass="text" AutoPostBack="True" Width="240px" Height="296px" onselectedindexchanged="rblMeasure_SelectedIndexChanged">
				<asp:ListItem Value="96">Item SRP</asp:ListItem>
				<asp:ListItem Value="107" Selected="True">DWT</asp:ListItem>
				<asp:ListItem Value="27">Color Grade</asp:ListItem>
				<asp:ListItem Value="29">Clarity Grade</asp:ListItem>
				<asp:ListItem Value="2">Total Weight (c)</asp:ListItem>
				<asp:ListItem Value="10">Appraisal Value</asp:ListItem>
				<asp:ListItem Value="44">Metal</asp:ListItem>
				<asp:ListItem Value="20">Culet</asp:ListItem>
				<asp:ListItem Value="98">Weight in Fractions</asp:ListItem>
				<asp:ListItem Value="1">Total Weight (g)</asp:ListItem>
				<asp:ListItem Value="124">STnumber</asp:ListItem>
			</asp:RadioButtonList>
			<asp:Label id="Label5" style="Z-INDEX: 114; POSITION: absolute; TOP: 528px; LEFT: 344px" runat="server"
				CssClass="text">Weight</asp:Label>
			<asp:DataGrid id="grdItemStructure" style="Z-INDEX: 115; POSITION: absolute; TOP: 632px; LEFT: 24px"
				runat="server" CssClass="text"></asp:DataGrid>
			<asp:textbox id="order" style="Z-INDEX: 116; POSITION: absolute; TOP: 8px; LEFT: 24px" runat="server"
				CssClass="inputStyleCC"></asp:textbox>
			<asp:textbox id="Textbox2" style="Z-INDEX: 108; POSITION: absolute; TOP: 592px; LEFT: 96px" runat="server"
				CssClass="inputStyleCC" AutoPostBack="True" MaxLength="5"></asp:textbox>
			<asp:textbox id="batch" style="Z-INDEX: 117; POSITION: absolute; TOP: 8px; LEFT: 192px" runat="server"
				CssClass="inputStyleCC"></asp:textbox>
			<asp:textbox id="Textbox3" style="Z-INDEX: 107; POSITION: absolute; TOP: 592px; LEFT: 96px" runat="server"
				CssClass="inputStyleCC" AutoPostBack="True" MaxLength="5"></asp:textbox>
			<asp:Button id="cmdGet" style="Z-INDEX: 118; POSITION: absolute; TOP: 8px; LEFT: 360px" runat="server"
				CssClass="buttonStyle" Text="?" onclick="cmdGet_Click"></asp:Button>
			<asp:DropDownList id="lstPartName" style="Z-INDEX: 119; POSITION: absolute; TOP: 40px; LEFT: 24px"
				runat="server" CssClass="inputStyleCC5"></asp:DropDownList>
			<asp:DropDownList id="lstMeasure" style="Z-INDEX: 120; POSITION: absolute; TOP: 496px; LEFT: 24px"
				runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstMeasure_SelectedIndexChanged"></asp:DropDownList></form>
	</body>
</HTML>
