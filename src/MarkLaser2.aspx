﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="MarkLaser2.aspx.cs" Inherits="Corpt.MarkLaser2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoheading">Mark Laser</div>
    <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadButton">
        <asp:Label ID="Label2" runat="server" Text="Item Number" />
        <asp:TextBox runat="server" ID="ItemField"></asp:TextBox>
        <asp:Button runat="server" ID="LoadButton" Text="Update" OnClick="OnLoadClick" CssClass="btn btn-primary" />
        <asp:Label runat="server" ID="ResultLabel"></asp:Label>
    </asp:Panel>
    <asp:Label runat="server" ID="InfoLabel" ForeColor="Red"></asp:Label>

    <asp:RequiredFieldValidator runat="server" ID="ItemReq" ControlToValidate="ItemField"
        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Item Number is required." />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ItemReqE" TargetControlID="ItemReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="ItemRegExpr" ControlToValidate="ItemField"
        Display="None" ValidationExpression="^\d{10}$|^\d{11}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a item number in the format:<br /><strong>10 or 11 numeric characters</strong>" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ItemReqExpr" TargetControlID="ItemRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />

</asp:Content>
