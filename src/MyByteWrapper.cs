using System;

namespace Corpt
{
	/// <summary>
	/// Summary description for MyByteWrapper.
	/// </summary>
	public class MyByteWrapper
	{
		public MyByteWrapper()
		{
			//
			// TODO: Add constructor logic here
			//
			_myVal = 0;
		}

		public MyByteWrapper(byte b)
		{
			_myVal = b;
		}

		private byte _myVal;

		public byte myVal
		{
			set
			{
				_myVal = value;
			}
			get
			{
				return _myVal;
			}
		}
	}
}
