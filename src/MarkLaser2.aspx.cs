﻿using System;
using Corpt.Utilities;

namespace Corpt
{
    public partial class MarkLaser2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Mark Laser";
            if (IsPostBack) return;
            InfoLabel.Text = "";

        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            InfoLabel.Text = "";
            ResultLabel.Text = "";
            var itemNumber = ItemField.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber)) return;
            var msg = QueryUtils.InsertRecordLaserLog(itemNumber, this);
            if (!string.IsNullOrEmpty(msg))
            {
                InfoLabel.Text = msg;
                return;
            }
            msg = QueryUtils.SetLaserValue(itemNumber, this);
            if (!string.IsNullOrEmpty(msg))
            {
                InfoLabel.Text = msg;
            }else
            {
                ItemField.Text = "";
                ResultLabel.Text = string.Format("Laser inscribed {0}", itemNumber);
                InfoLabel.Text = "OK";
            }
        }
    }
}