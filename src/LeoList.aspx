<%@ Page language="c#" Codebehind="LeoList.aspx.cs" AutoEventWireup="True" Inherits="Corpt.LeoList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LeoList</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body onload="document.Form1.txtOrderNumber.focus();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:textbox style="Z-INDEX: 100; POSITION: absolute; TOP: 16px; LEFT: 16px" id="txtOrderNumber"
				runat="server" AutoPostBack="True" CssClass="inputStyleCC" ontextchanged="txtOrderNumber_TextChanged"></asp:textbox><asp:button style="Z-INDEX: 112; POSITION: absolute; TOP: 240px; LEFT: 184px" id="cmdEmailOnly"
				runat="server" CssClass="buttonStyle" Visible="False" Enabled="False" Text="EmailOnly(D)" Width="96px" onclick="cmdEmailOnly_Click"></asp:button><asp:button style="Z-INDEX: 110; POSITION: absolute; TOP: 168px; LEFT: 184px" id="cmdSendToLeo"
				runat="server" CssClass="buttonStyle" Text="Order Reports" Width="96px" Enabled="False" onclick="cmdSendToSasha_Click"></asp:button><asp:button style="Z-INDEX: 106; POSITION: absolute; TOP: 120px; LEFT: 184px" id="cmdShowShortReport"
				runat="server" CssClass="buttonStyle" Text="Short Report" Width="96px" onclick="cmdShowShortReport_Click"></asp:button><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 184px" id="cmdDelete"
				runat="server" CssClass="buttonStyle" Text="Delete" Width="96px" onclick="cmdDelete_Click"></asp:button><asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 56px; LEFT: 184px" id="cmdClear" runat="server"
				CssClass="buttonStyle" Text="Clear" Width="96px" onclick="cmdClear_Click"></asp:button><asp:datagrid style="Z-INDEX: 103; POSITION: absolute; TOP: 120px; LEFT: 304px" id="grdShortReport"
				runat="server" CssClass="text" AllowSorting="True">
				<HeaderStyle BackColor="#E0E0E0"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderTemplate>
							Some Header Here
						</HeaderTemplate>
						<ItemTemplate>
							<asp:CheckBox Runat="server" Text='' Checked='<%# DataBinder.Eval(Container.DataItem, "LineChecked") %>' AutoPostBack="True" OnCheckedChanged="ItemCheckBoxClicked">
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid><asp:listbox style="Z-INDEX: 102; POSITION: absolute; TOP: 48px; LEFT: 16px" id="lstOrderNumber"
				runat="server" Width="152px" Height="256px"></asp:listbox><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 16px; LEFT: 184px" id="cmdAdd" runat="server"
				CssClass="buttonStyle" Text="Add" Width="96px" onclick="cmdAdd_Click"></asp:button><asp:listbox style="Z-INDEX: 107; POSITION: absolute; TOP: 312px; LEFT: 16px" id="cblBatches"
				runat="server" Width="152px" Height="464px"></asp:listbox><asp:label style="Z-INDEX: 108; POSITION: absolute; TOP: 24px; LEFT: 304px" id="lblInfo" runat="server"></asp:label><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 144px; LEFT: 184px" id="cmdSaveExcel"
				runat="server" CssClass="buttonStyle" Text="Save Excel" Width="96px" onclick="cmdSaveExcel_Click"></asp:button>&nbsp;
			<asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 56px; LEFT: 312px" id="lblDebug" runat="server"></asp:label><asp:hyperlink style="Z-INDEX: 113; POSITION: absolute; TOP: 784px; LEFT: 16px" id="HyperLink1"
				runat="server" NavigateUrl="Middle.aspx">Back</asp:hyperlink></form>
	</body>
</HTML>
