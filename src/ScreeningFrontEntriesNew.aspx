﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningFrontEntriesNew.aspx.cs" Inherits="Corpt.ScreeningFrontEntriesNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        .GridPager a, .GridPager span {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }

        .divCol {
            float: left;
            width: 290px;
        }

        .divBottom {
            clear: both;
        }

        .auto-style1 {
            //float: right;
            width: 400px;
            //height: 521px;
            height: 800px;
        }

        .auto-style2 {
            font-size: 14px;
            color: #FFFFFF;
            vertical-align: middle;
            background-color: #0044CC;
            background-repeat: repeat-x;
        }

        .text-style {           
           padding-bottom: 3px;
           padding-top: 1px;
           padding-left: 3px;padding-right: 3px;
           margin-left: 10px;
           margin-bottom: 2px;
        }
    </style>



    <div class="demoarea" style="text-align: center;">
        <!--<h2 style="margin-left: 10px; margin-bottom: 10px; margin-top: 10px" class="demoheading"> 
        <h2 style="text-align: center; margin-bottom: 10px; margin-top: 10px" class="demoheading">
            Synthetic Front Entries
        </h2>-->
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">
                <!--
                <div class="divCol" style="width: 170px;">
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtGsiIOrder">
                            GSI Order #:</label>
                        <div>
                            <input id="hdnScreeningID" runat="server" type="hidden" />
                            <input id="txtGsiIOrder" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter GSI Order #" maxlength="7" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtVendorNum">
                            Vendor #:</label>
                        <div>
                            <input id="txtVendorNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter Vendor #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtPoNum">
                            PO #:</label>
                        <div>
                            <input id="txtPoNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter PO #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtSku">
                            SKU/Style:</label>
                        <div>
                            <input id="txtSku" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter SKU/Style" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label">
                            Test:</label>
                        <div>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkQCHK" runat="server" type="checkbox" style="margin-top: 0px;" value="QCHK" />QCHK++
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkYehuda" runat="server" type="checkbox" style="margin-top: 0px;" value="Yehuda" />Yehuda
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkDiamondView" runat="server" type="checkbox" style="margin-top: 0px;"
                                    value="DiamondView" />Diamond View
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkRaman" runat="server" type="checkbox" style="margin-top: 0px;" value="Raman" />Raman
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkFTIR" runat="server" type="checkbox" style="margin-top: 0px;" value="FTIR" />FTIR
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="width: 170px; margin-top: 50px;">
                        <div>
                            <label class="control-label">
                                Add Fail Items for Order#:</label>
                        </div>
                        <div style="width: 320px;">
                            <input id="txtGSIItemNumber" runat="server" maxlength="11" style="width: 40%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="GSI Item Number" />
                            <input id="txtItemQTYFail" runat="server" maxlength="3" style="width: 10%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="Quantity Fail" />
                            <asp:Button ID="btnAddItem" runat="server" CssClass="btn btn-primary" Text="Add Item"
                                Style="height: 25px; margin-bottom: 10px;" OnClick="btnAddItem_Click"></asp:Button>
                        </div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" Style="margin-top: 5px;"
                                        OnRowDeleting="gvItems_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Item Number" HeaderStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGSIItemNumber" runat="server" Text='<%# Eval("GSIItemNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QTY Fail" HeaderStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemQTYFail" runat="server" Text='<%# Eval("ItemQTYFail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="true" DeleteImageUrl="Images/delete.png" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                -->
                <!--<div class="auto-style1" style="margin-left: 55px;"> -->
                <div class="auto-style1" style="text-align: left;">
                    <h2 style="text-align: left; margin-bottom: 10px; margin-top: 10px" class="demoheading">Synthetic Front Entries
                    </h2>

                    <!--
                    <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtNotes">
                            Notes:</label>
                        <div>
                            <textarea id="txtNotes" runat="server" class="form-control form-control-height" rows="3"
                                placeholder="Enter Notes" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                        </div>
                    </div>
                    -->
                    <table class="form-group" style="margin-bottom: 7px;">
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" >
                                    <label class="control-label" for="RequestIDBox1" style="float: left;margin-top: 4px;">
                                        Request ID:</label>

                                    <asp:TextBox ID="RequestIDBox1" Style="width: 90px;" class="form-control form-control-height text-style" runat="server" OnClick="OnLoadClick" OnTextChanged="OnLoadClick"
                                        placeholder="Enter Request ID" autocomplete="off"></asp:TextBox>
                                    <!--
                                        <input id="RequestIDBox" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter Request ID" maxlength="5" style="width: 90px;" />
                                        <asp:Button ID="RequestBtn" runat="server"  class="btn btn-primary btnFont" Style="font:'Forgotten Futurist'; width: 10px;  font-size: 10px;" Text="RQ" OnClick="OnLoadClick" OnTextChanged="OnLoadClick" />
                                        -->

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtMemoNum" style="float: left;margin-top: 4px;">
                                        Memo:</label>

                                    <input id="txtMemoNum" runat="server" type="text" class="form-control form-control-height text-style"
                                        placeholder="Enter Memo" maxlength="25" style="width: 90px;" />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" >
                                    <label class="control-label" for="txtOrder1" style="float: left;margin-top: 4px;">
                                        GSI Order:</label>

                                    <asp:TextBox ID="txtOrder1" class="form-control form-control-height text-style" Style="width: 90px;" runat="server" placeholder="GSI ORDER" autocomplete="off" OnClick="OnOrderClick" OnTextChanged="txtOrder_TextChanged"></asp:TextBox>
                                    <!--
                                        <input id="txtOrder" runat="server" type="text" class="form-control form-control-height" OnClick="OnOrderClick" OnTextChanged="txtOrder_TextChanged"
                                            placeholder="Enter GSI order" maxlength="5" style="width: 90px;" />
                                        -->

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" >
                                    <label class="control-label" for="txtCustomerCode1" style="float: left;margin-top: 4px;">
                                        Customer Code:</label>

                                    <asp:TextBox ID="txtCustomerCode1" class="form-control form-control-height text-style" Style="width: 90px;" runat="server" OnClick="OnCustCodeClick" OnTextChanged="OnCustCodeClick"
                                        placeholder="CustomerCode" autocomplete="off"></asp:TextBox>
                                    <asp:DropDownList ID="CustomerCodeList" runat="server" AutoPostBack="True" DataValueField="CustomerCode" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Height="26px" />
                                    <asp:DropDownList ID="CustomerIdList" runat="server" AutoPostBack="True" DataValueField="CustomerId" CssClass="" ToolTip="Customer Id List" Width="300px" Height="26px" />
                                    </asp:DropDownList>
                                        <!--
                                        <input id="txtCustomerCode" runat="server" type="text" class="form-control form-control-height" OnClick="OnCustCodeClick" OnTextChanged="OnCustCodeClick"
                                            placeholder="Enter Customer Code" maxlength="5" style="width: 90px;" />
                                        -->

                                </div>
                                <asp:HiddenField ID="txtCustomerID" runat="server" />
                                <asp:HiddenField ID="txpPersonID" runat="server" />
                                <asp:HiddenField ID="txtRetailer" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" style="width:400px;margin-bottom: 8px;">
                                    <label class="control-label" for="txtCustomerName" style="float: left;margin-top: 2px;margin-bottom: 0px;">
                                        Customer Name:</label>

                                    <input id="txtCustomerName" runat="server" type="text" class="form-control form-control-height text-style"
                                        placeholder="Customer Name" maxlength="5" style="width: 90px;" />

                                    <asp:DropDownList ID="CustomerList2" runat="server" DataTextField="CustomerName"
                                        AutoPostBack="True" DataValueField="CustomerId" CssClass="text-style" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Height="26px" />
                                    </asp:DropDownList>
                                    
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtPONum1" style="float: left;margin-top: 4px;">
                                        PO Number:</label>

                                    <input id="txtPONum1" runat="server" type="text" class="form-control form-control-height text-style"
                                        placeholder="Enter PO Number" maxlength="5" style="width: 90px;" />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtSku1" style="float: left;margin-top: 4px;">
                                        SKU:</label>

                                    <input id="txtSku1" runat="server" type="text" class="form-control form-control-height text-style"
                                        placeholder="Enter SKU" maxlength="25" style="width: 90px;" />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtStyle" style="float: left;margin-top: 4px;">
                                        Style:</label>
                                    <div>
                                        <input id="txtStyle" runat="server" type="text" class="form-control form-control-height text-style"
                                            placeholder="Enter Style" maxlength="25" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtQuantity" style="float: left;margin-top: 4px;">
                                        Total Quantity:</label>
                                    <div>
                                        <input id="txtQuantity" runat="server" type="text" class="form-control form-control-height text-style"
                                            placeholder="Total Qty" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="txtStyle" style="float: left;margin-top: 4px;">
                                        Batches:</label>
                                    <div>
                                        <textarea id="Textarea1" runat="server" rows="3" class="text-style"
                                            placeholder="Enter Batches" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" style="margin-bottom: 8px;margin-top: 8px;">
                                    <label class="control-label" for="specialInstructionsListBox" style="float: left;margin-top: 4px;">
                                        Retailer:</label>
                                    <div>
                                        <asp:DropDownList ID="specialInstructionsListBox" runat="server" CssClass="form-control form-control-height selectpicker text-style"
                                            Style="padding-top: 2px; padding-bottom: 2px; text-align: center" OnSelectedIndexChanged="specialInstructionsListBox_SelectedIndexChanged">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>FM</asp:ListItem>
                                            <asp:ListItem>STERLING</asp:ListItem>
                                            <asp:ListItem>JARED</asp:ListItem>
                                            <asp:ListItem>Other</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" >
                                    <label class="control-label" for="messengerListBox" style="float: left;margin-top: 4px;">
                                        Messenger:</label>
                                    <div>
                                        <asp:DropDownList ID="messengerListBox" runat="server" CssClass="form-control form-control-height selectpicker text-style"
                                            Style="padding-top: 2px; padding-bottom: 2px;" placeholder="Messenger Name">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSavefront_Click" class="btn btn-primary btnFont" Style="font: 'Forgotten Futurist'; width: 160px; font-size: 15px;" Text="CREATE ORDER" />
                                </div>
                            </td>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <asp:Button ID="btnClearAll" runat="server" class="btn btn-primary btnFont" Style="font: 'Forgotten Futurist'; width: 160px; font-size: 15px;" Text="CLEAR" OnClick="btnClear_Click" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <asp:Button ID="btnPrint" runat="server" class="btn btn-primary btnFont" OnClick="btnPrint_Click" Style="font: 'Forgotten Futurist'; width: 160px; font-size: 15px;" Text="PRINT LABEL" />
                                </div>
                            </td>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <asp:Button ID="btnPrint1" runat="server" class="btn btn-primary btnFont" Style="font: 'Forgotten Futurist'; width: 160px; font-size: 15px;" Text="PRINT RECEIPT" OnClick="btnPrint1_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
