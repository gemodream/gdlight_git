using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace Corpt
{
	/// <summary>
	/// Summary description for ShortReportList.
	/// </summary>
	public partial class ShortReportList : System.Web.UI.Page
	{
	
		protected void Page_Init(object sender, System.EventArgs e)
		{
			if(myDiv.Controls.Count>0)
			{
				if(Session["btnList"]!=null)
				{
					AutoList();
				}
				foreach(Control c in myDiv.Controls)
				{
					if(c.GetType().ToString()=="System.Web.UI.WebControls.Button")
					{
						(c as Button).Click += new System.EventHandler(this.Buttons_Click);
					}
				}
			}
		}
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
//			if(Session["btnList"]!=null)
//			{
//				if((Session["btnList"] as ArrayList).Count>0)
//				{
//					foreach(object btn in (Session["btnList"] as ArrayList))
//					{
//						(btn as Button).Click += new System.EventHandler(this.Buttons_Click);
//					}
//				}
//			}
			if(Session["ID"]==null)
			{
				Response.Redirect("Login.aspx");
			}
			else
			{
				if(Utils.NullValue(Session["OrderCode"])!="")
				{
					if(!IsPostBack)
						ShowBatchList();
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion

		private void AutoList()
		{
			if(Session["ShortReports"]!=null)
			{
				ArrayList ass = new ArrayList();
				DataSet ShortReports = new DataSet();
				ass = SorterSeparator.SortSeparate(Session["ShortReports"] as DataSet);
				//Re-arrange checkboxes here
				//--------------------------
				ArrayList chkList = new ArrayList();
				ArrayList btnList = new ArrayList();

				int i = 0;
				int k = 0;
				foreach(object al in ass)
				{
					DataSet shortReports = new DataSet();
					foreach(object tb in al as ArrayList)
					{
						DataTable table = new DataTable();
						table = (tb as DataTable).Copy();
						table.TableName=(i++).ToString();
						shortReports.Tables.Add(table);
					}
					DataTable combinedTable = new DataTable();
					combinedTable=shortReports.Tables[0];
					for(int j=1; j<shortReports.Tables.Count;j++)
					{
						foreach(DataRow dr in shortReports.Tables[j].Rows)
						{
							combinedTable.ImportRow(dr);
						}
					}
					DataGrid dg = new DataGrid();
					dg.CssClass="text";
					dg.CellPadding=3;
					dg.BorderStyle=BorderStyle.None;
					dg.BackColor=Color.White;
					dg.AlternatingItemStyle.BackColor=Color.WhiteSmoke;
					dg.HeaderStyle.Font.Bold=true;
					dg.HeaderStyle.ForeColor=Color.White;
					dg.HeaderStyle.BackColor=Color.FromArgb(0x1C4C8E);
					dg.DataSource = combinedTable;
					dg.DataBind();
					myDiv.Controls.Add(dg);
					Label lb = new Label();
					lb.Text="<br>";
					Button btn = new Button();
					btn.Text="SaveExcell";
					btn.ID="btn"+(k++).ToString();
					btn.CssClass="buttonStyle";
					myDiv.Controls.Add(btn);
					myDiv.Controls.Add(lb);
					btnList.Add(btn);
					btn.Click += new EventHandler(this.Buttons_Click);
				}
				Session["btnList"]=btnList;
			}
		}

		private void ShowBatchList()
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			SqlCommand command = new SqlCommand("spGetBatchByCode");
			command.CommandType=CommandType.StoredProcedure;
			command.Connection=conn;
			/*	spGetBatchByCode 
							@AuthorID=10, 
							@AuthorOfficeID = 1,  
							@GroupCode = 8299, 
							@CustomerCode = null, 
							@BGroupState = null, 
							@EgroupState = null, 
							@Bstate = null, 
							@Estate = null, 
							@Bdate = null, 
							@Edate = null, 
							@BatchCode = null
			*/
	
			command.Parameters.Add("@CustomerCode",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@EGroupState",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@BGroupState",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@BState",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@EState",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@BDate",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@EDate",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value=DBNull.Value;
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value=Int32.Parse(Session["OrderCode"].ToString());
			command.Parameters.Add("@AuthorID",SqlDbType.Int).Value=Int32.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",SqlDbType.Int).Value=Int32.Parse(Session["AuthorOfficeID"].ToString());

			conn.Open();

			SqlDataReader reader = command.ExecuteReader();
			BatchList.Items.Clear();
			while(reader.Read())
			{
				ListItem li = new ListItem(Session["OrderCode"].ToString()+"."+reader.GetSqlValue(reader.GetOrdinal("BatchCode")).ToString(),reader.GetSqlValue(reader.GetOrdinal("BatchID")).ToString());
				BatchList.Items.Add(li);
			}
			conn.Close();

			DataSet ShortReports = new DataSet();
			foreach(ListItem li in BatchList.Items)
			{
				DataTable table = new DataTable();
				table = Utils.BatchShortReport(li.Value,this).Copy();
				table.TableName=li.Value;
				ShortReports.Tables.Add(table);
			}
			if(ShortReports.Tables.Count>0)
			{
				Session["ShortReports"]=ShortReports;
			}
		}

		public void Buttons_Click(object sender, EventArgs e)
		{
			try
			{
				int tableNumber = Int32.Parse((sender as Button).ID.ToString());				
				/*TODO:
				 *		create DataSet, populate it with tables from respectfull ass sublist,
				 *		save the DataSet as Session["DataSet"] and call the cmdDownloadExcel_Click( sender, e);
				 */
			}
			catch(Exception ex)
			{}
		}

		protected void cmdGo_Click(object sender, System.EventArgs e)
		{
			DataSet ShortReports = new DataSet();
			DataTable total = new DataTable();
			foreach(ListItem li in BatchList.Items)
			{
				if(li.Selected)
				{
					DataTable table = new DataTable();
					table = Utils.BatchShortReport(li.Value,this).Copy();
					table.TableName=li.Value;
					ShortReports.Tables.Add(table);
				}
			}

			//prepare and show combined table
			//-------------------------------
			DataTable CombinedTable = new DataTable();
			CombinedTable=ShortReports.Tables[0];
			for(int i=1; i<ShortReports.Tables.Count;i++)
			{
				if(Utils.CompareColumnList(CombinedTable,ShortReports.Tables[i]))
				{
					foreach(DataRow dr in ShortReports.Tables[i].Rows)
					{
						CombinedTable.ImportRow(dr);
					}
				}
			}
			DataSet ds = new DataSet();
			DataTable dt = new DataTable();
			dt = CombinedTable.Copy();
			dt.TableName="Short Report";
			ds.Tables.Add(dt);
			Session["DataSet"] = ds;
			ReportList.DataSource=CombinedTable;
			ReportList.DataBind();
			ReportList.Visible=true;
			cmdDownloadExcel.Visible=true;
		}

		protected void cmdDownloadExcel_Click(object sender, System.EventArgs e)
		{
			if(Directory.Exists(Session["TempDir"].ToString()+Session.SessionID + @"\"))
				Directory.Delete(Session["TempDir"].ToString()+Session.SessionID + @"\",true);
			Directory.CreateDirectory(Session["TempDir"].ToString()+Session.SessionID + @"\");
			Utils.SaveExcell(Session["DataSet"] as DataSet, this);
			Response.ContentType="application/vnd.ms-excel";
			Response.WriteFile(Session["TempDir"].ToString()+Session.SessionID + @"\Report.xls");
			Response.Flush();
			Response.Close();
		}

		protected void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem li in BatchList.Items)
			{
				li.Selected=true;
			}
		}

		protected void cmdDeselectAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem li in BatchList.Items)
			{
				li.Selected=false;
			}
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			AutoList();
		}

		protected void resultSelector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(resultSelector.SelectedValue=="Auto")
			{
				ShowBatchList();
				AutoList();
			}
			if(resultSelector.SelectedValue=="Manual")
			{
				ShowBatchList();
			}
		}
	}
}
