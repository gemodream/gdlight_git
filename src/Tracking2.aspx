﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="Tracking2.aspx.cs" Inherits="Corpt.Tracking2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">
        $(window).resize(function () {
            /* gridviewScrollBatch();
            gridviewScrollBatchHistory();*/
        });
        $(document).ready(function () {
            /*  gridviewScrollBatch();
            gridviewScrollBatchHistory();*/

        });
        function gridviewScrollBatch() {
            var widthGrid = $('#gridContainer').width();
            var heightGrid = $('#gridContainer').height();
            $('#<%=grdBatches.ClientID%>').gridviewScroll({
                width: widthGrid,
                height: heightGrid,
                freezesize: 1
            });
        }
        function gridviewScrollBatchHistory() {
            var widthGrid = $('#containerHistory').width();
            var heightGrid = $('#containerHistory').height();
            $('#<%=dgHistory.ClientID%>').gridviewScroll({
                width: widthGrid,
                height: heightGrid,
                freezesize: 1
            });
        }
        
    </script>
    <div class="demoarea">
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="demoheading">
            Tracking II</div>
        <!-- Filter Panel: Customer List, DateFrom, Date To, -->
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="form-inline">
                    <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="FilterPanelByCustomerAndRabgeDate"
                        class="form-inline">
                        <!-- Customers -->
                        <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                            width: 60px;"></asp:TextBox>
                        <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                            ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
                        <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                            DataValueField="CustomerId" Width="300px" Style="font-family: Arial; font-size: 12px"
                            ToolTip="Customers List" />
                        <!-- Date From -->
                        <label style="margin-right: 5px">
                            From</label>
                        <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
                            AutoPostBack="True" />
                        <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                            AlternateText="Click to show calendar" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                            PopupButtonID="Image1" />
                        <!-- Date To -->
                        <label style="margin-left: 15px; margin-right: 5px; text-align: right">
                            To</label>
                        <asp:TextBox runat="server" ID="calTo" Width="100px" OnTextChanged="OnChangedDateTo"
                            AutoPostBack="True" />
                        <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                            AlternateText="Click to show calendar" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                            PopupButtonID="Image2" />
                    </asp:Panel>
                </div>
                <div style="padding-top: 10px;">
                    <ajaxToolkit:TabContainer ID="TabContainer" runat="server" Width="850px" OnDemand="False"
                        ActiveTabIndex="1" AutoPostBack="True" OnActiveTabChanged="OnActiveTabChanged">
                        <ajaxToolkit:TabPanel runat="server" HeaderText="Current" ID="TabCurrent" OnDemandMode="None">
                            <ContentTemplate>
                                <!-- Filter -->
                                <asp:Panel ID="Panel1" DefaultButton="LookupCurrentButton" runat="server">
                                    <div class="form-inline">
                                        <label class="checkbox" style="width: 200px; font-family: Arial; font-size: small">
                                            <input type="checkbox" id="chkHideCurrent" checked="True" runat="server" />Hide
                                            Current Batches
                                        </label>
                                        Unitemized orders older than (hrs):
                                        <asp:TextBox ID="AgeCurrent" runat="server" Width="50px" /><ajaxToolkit:FilteredTextBoxExtender
                                            ID="AgeCurrentFilteredTextBoxExtender" runat="server" TargetControlID="AgeCurrent"
                                            FilterType="Numbers" />
                                        <asp:Button ID="LookupCurrentButton" class="btn btn-primary" runat="server" Text="Lookup"
                                            OnClick="OnLoadClick" Style="margin-left: 15px"></asp:Button></div>
                                </asp:Panel>
                                <asp:Label ID="lblCurrentRows" runat="server" Style="font-family: Arial; font-size: small;
                                    font-weight: bold" />
                                <div id="gridContainer">
                                    <asp:DataGrid ID="grdBatches" runat="server" Width="100%" Enabled="True" AllowSorting="true"
                                        OnSortCommand="OnSortBatchesCommand" AutoGenerateColumns="True">
                                        <HeaderStyle CssClass="GridviewScrollHeader" />
                                        <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
                                        <PagerStyle CssClass="GridviewScrollPager" />
                                    </asp:DataGrid></div>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel runat="server" HeaderText="History" ID="TabHistory" OnDemandMode="None">
                            <ContentTemplate>
                                <!-- Filter -->
                                <asp:Panel runat="server" ID="FilterPanelByActionAndForm" CssClass="form-inline">
                                    <label style="font-family: Arial; font-size: small">
                                        Action</label>
                                    <asp:DropDownList ID="lstAction" runat="server" Style="font-family: Arial; font-size: 12px"
                                        Width="200px" DataValueField="ActionId" DataTextField="ActionName" ToolTip="Actions List" />
                                    <label style="padding-left: 10px; font-family: Arial; font-size: small">
                                        Form</label>
                                    <asp:DropDownList ID="lstForm" runat="server" Style="font-family: Arial; font-size: 12px"
                                        Width="200px" DataValueField="ViewAccessId" DataTextField="ViewAccessName" ToolTip="Forms List" />
                                    <label class="checkbox" style="width: 200px; font-family: Arial; font-size: small">
                                        <input type="checkbox" id="chkHideCheckedOut" checked="true" runat="server" />&nbsp;&nbsp;Hide
                                        Checked Out Batches
                                    </label>
                                    <asp:Button ID="LoadHistoryBtn" class="btn btn-primary" runat="server" Text="Lookup"
                                        OnClick="OnLoadHistoryClick" Style="margin-left: 15px">
                                    </asp:Button>
                                    <div id="HistoryErrMessage" runat="server" style="color: brown"></div>
                                    </asp:Panel>

                                <asp:Label ID="lblHistoryRows" runat="server" Style="font-family: Arial; font-size: small;
                                    font-weight: bold"></asp:Label>
                                <div id="containerHistory">
                                    <asp:DataGrid ID="dgHistory" runat="server" Width="100%" AllowSorting="True" OnSortCommand="OnSortHistoriesCommand">
                                        <HeaderStyle CssClass="GridviewScrollHeader" />
                                        <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
                                        <PagerStyle CssClass="GridviewScrollPager" />
                                    </asp:DataGrid>
                                </div>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
