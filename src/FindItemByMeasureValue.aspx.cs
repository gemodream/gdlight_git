﻿using System;
using Corpt.Utilities;

namespace Corpt
{
    public partial class FindItemByMeasureValue : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Find by Measure Value";
            if (IsPostBack) return;
            
            var measures = QueryUtils.GetStringMeasures(this);
            var prefix = measures.Find(m => m.MeasureId == 112);
            lstMeasures.Rows = measures.Count + 1;
            lstMeasures.DataSource = measures;
            lstMeasures.DataBind();
            if (prefix != null)
            {
                lstMeasures.SelectedValue = "" + prefix.MeasureId;
            }
            ValueField.Focus();
        }

        protected void OnFindClick(object sender, EventArgs e)
        {
            if (ValueField.Text.Trim().Length == 0 || string.IsNullOrEmpty(lstMeasures.SelectedValue)) return;
            var items = QueryUtils.FindItemsByMeasureValue(lstMeasures.SelectedValue, ValueField.Text.Trim(), 1000, this);
            ItemGrid.DataSource = items;
            ItemGrid.DataBind();
            RowsLabel.Text = items.Count + " rows";
        }

        protected void OnMeasureChanged(object sender, EventArgs e)
        {
            ItemGrid.DataSource = null;
            ItemGrid.DataBind();
            RowsLabel.Text = "";
            ValueField.Text = "";

        }
    }
}