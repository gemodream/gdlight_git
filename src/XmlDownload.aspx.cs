﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
	public partial class XmlDownload : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

			if (Request.QueryString["Type"].ToString() == "Reciept")
			{
				RecieptXML();
			}
			else if (Request.QueryString["Type"].ToString() == "Label")
			{
				LabelXML();
			}
		}

		protected void RecieptXML()
		{
			

			string groupId = null;
			var gsiOrder = Request.QueryString["OrderCode"].ToString();
			DataTable orderDetails = FrontUtils.GetOrderSummery(gsiOrder, this);

			//var dir = GetExportDirectory(this);
			string filename = @"label_Front_External_" + gsiOrder + ".xml";
			var retailer = orderDetails.Rows[0]["SpecialInstruction"].ToString(); 

			//var totalQ = txtNoOfItemsTakeIn.Text.ToString();

			var memo = orderDetails.Rows[0]["Memo"].ToString();
			var custCode = orderDetails.Rows[0]["CustomerName"].ToString();
			string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
			DataSet dsGroupId = new DataSet();
			int authorId = Convert.ToInt32(this.Session["ID"]);
			int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
			int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
			dsGroupId = GSIAppQueryUtils.GetGroupId(gsiOrder, this);
			if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
			{
				groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
			}
			DataSet dsGroup = new DataSet();
			dsGroup = GSIAppQueryUtils.GetGroupWithCustomer(groupId, authorId, authorOfficeId, groupOfficeId, this);
			DataRow rGroup = null;
			if (dsGroup != null && dsGroup.Tables[0].Rows.Count != 0)
				rGroup = dsGroup.Tables[0].Rows[0];
			else
			{
				ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('No entries for Customer');", true);
				return;
			}
			string officeCode = rGroup["OfficeCode"].ToString();
			string customerName = rGroup["CustomerName"].ToString();
			string notInspectedQuantity = orderDetails.Rows[0]["NotInspectedQuantity"].ToString();
			string address = rGroup["Address1"].ToString() + " " + rGroup["Address2"].ToString();
			string city = rGroup["City"].ToString();
			string zip = rGroup["Zip1"].ToString();
			string stateName = rGroup["USStateName"].ToString();
			string country = rGroup["Country"].ToString();
			string phone = rGroup["CountryPhoneCode"].ToString() + " " + rGroup["Phone"].ToString();
			string fax = rGroup["CountryFaxCode"].ToString() + " " + rGroup["Fax"].ToString();
			DataSet dsAuthor = GSIAppQueryUtils.GetAuthorData(groupId, authorId, groupOfficeId, authorOfficeId, this);
			if (dsAuthor == null || dsAuthor.Tables[0].Rows.Count == 0)
			{
				ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('No entries for Author');", true);
				return;
			}
			string name = dsAuthor.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsAuthor.Tables[0].Rows[0]["LastName"];
			string sPersonID = dsGroup.Tables[0].Rows[0]["PersonID"].ToString();
			string sPersonCustomerID = dsGroup.Tables[0].Rows[0]["PersonCustomerID"].ToString();
			string sPersonCustomerOfficeID = dsGroup.Tables[0].Rows[0]["PersonCustomerOfficeID"].ToString();
			string totalQ = dsGroup.Tables[0].Rows[0]["NotInspectedQuantity"].ToString();
			string messenger = dsGroup.Tables[0].Rows[0]["NotInspectedQuantity"].ToString();
			var stream = new MemoryStream();
			//using (XmlWriter writer = XmlWriter.Create(dir + filename))
			using (XmlWriter writer = XmlWriter.Create(stream))
			{
				writer.WriteStartDocument();
				writer.WriteStartElement("label");
				writer.WriteElementString("memo", memo); //0
				writer.WriteElementString("groupCode", gsiOrder);//1
				writer.WriteElementString("customerCode", custCode); //2
				writer.WriteElementString("totalQ", totalQ);//3

				writer.WriteElementString("retailer", retailer);//4
				writer.WriteElementString("dateStamp", dateTime);//5
				writer.WriteElementString("serviceType", "Screening");//6
				writer.WriteElementString("customerName", customerName);//7
				writer.WriteElementString("notInspectedQuantity", notInspectedQuantity);//8
				writer.WriteElementString("address", address);//9
				writer.WriteElementString("city", city);//10
				writer.WriteElementString("zip", zip);//11
				writer.WriteElementString("stateName", stateName);//12
				writer.WriteElementString("country", country);//13
				writer.WriteElementString("phone", phone);//14
				writer.WriteElementString("fax", fax);//15
				writer.WriteElementString("messenger", messenger);//16
				writer.WriteElementString("name", name);//17
														//writer.WriteElementString("RequestID", RequestIDBox1.Text);
														/*var oldStyles = (List<SyntheticStyleModel>)Session["StyleNumbers"];
														string batchInfo = null;
														if (oldStyles.Count > 0)
														{
															int i = 1;
															foreach (var style in oldStyles)
															{
																var stN = style.styleNumber;
																batchInfo += i.ToString() + @"/" + stN + @"/" + style.style + @";";
																i++;
															}
															writer.WriteElementString("batchInfo", batchInfo.Substring(0, batchInfo.Length - 1));
														}
														else
														{
															batchInfo = @"1/" + txtNoOfItemsTakeIn.Text + @"/nostyle";
															writer.WriteElementString("batchInfo", batchInfo.Substring(0, batchInfo.Length - 1));
														}*/

				//writer.WriteElementString("bagQ", bagQ);

				//writer.WriteElementString("screening", screen);//7
				//writer.WriteElementString("test", test);//8
				writer.WriteEndElement();
				writer.WriteEndDocument();
				this.Session["Memory"] = stream;
				writer.Close();
				writer.Flush();
			}

			DownloadExcelFile(filename, this);
		}

		public void LabelXML()
		{
			string groupId = null;
			var gsiOrder = Request.QueryString["OrderCode"].ToString();
			DataTable orderDetails = FrontUtils.GetOrderSummery(gsiOrder, this);

			//var dir = GetExportDirectory(this);
			string filename = @"label_Front_Take_In_" + gsiOrder + ".xml";
			var retailer = orderDetails.Rows[0]["SpecialInstruction"].ToString();
			var groupCode = gsiOrder;
			//var totalQ = txtQuantity.Value;
			var memo = orderDetails.Rows[0]["Memo"].ToString();
			var custCode = orderDetails.Rows[0]["CustomerName"].ToString();
			string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
			var stream = new MemoryStream();
			//using (XmlWriter writer = XmlWriter.Create(dir + filename))
			using (XmlWriter writer = XmlWriter.Create(stream))
			{
				writer.WriteStartDocument();
				writer.WriteStartElement("label");
				writer.WriteElementString("memo", memo); //0
				writer.WriteElementString("groupCode", groupCode);//1
				writer.WriteElementString("customerCode", custCode); //2
				//writer.WriteElementString("totalQ", totalQ);//3

				writer.WriteElementString("retailer", retailer);//4
				writer.WriteElementString("dateStamp", dateTime);//5
				writer.WriteElementString("serviceType", "Screening");
				/*if (Textarea1.Value != null && Textarea1.Value != "")
				{
					string[] batchMemos = null;
					string memoStr = Textarea1.Value.Replace("\r", "");
					batchMemos = memoStr.Split('\n');
					string batchInfo = null;
					foreach (string batch in batchMemos)
					{
						batchInfo += batch + ";";
					}
					writer.WriteElementString("batchInfo", batchInfo.Substring(0, batchInfo.Length - 1));//6
				}
				else
					writer.WriteElementString("batchInfo", "no batches");//6*/
																		 //writer.WriteElementString("bagQ", bagQ);

				//writer.WriteElementString("screening", screen);//7
				//writer.WriteElementString("test", test);//8
				writer.WriteEndElement();
				writer.WriteEndDocument();

				this.Session["Memory"] = stream;
				writer.Close();
				writer.Flush();
			}
			DownloadExcelFile(filename, this);
			/*	stream.Position = 0;
				//FileStream fs = new FileStream(filePath, FileMode.Open, ileAccess.Read);
				//int length = Convert.ToInt32(fs.Length);
				//byte[] data = new byte[length];
				//fs.Read(data, 0, length);
				//fs.Close();
				using (var fs = new FileStream(filename, FileMode.CreateNew, FileAccess.ReadWrite))
				{
					stream.CopyTo(fs); // fileStream is not populated
									   //int lnth = (int)(fs.length);
					byte[] data = new byte[10000];
					fs.Read(data, 0, 10000);
					//BinaryReader r = new BinaryReader(fs);
					//byte[] postArray = r.ReadBytes((int)fs.Length);
					using (var client = new WebClient())
					{
						string webServiceUrl = @"http://localhost/dell/" + filename;
						//client.Headers.Add("Authorization", "Basic " + "YYY");
						var abc = client.UploadData(webServiceUrl, data);
						client.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
						using (var postStream = client.OpenWrite(webServiceUrl, "POST"))
						{
							postStream.Write(data, 0, data.Length);
						}
					}
				}*/
		}

		public void DownloadExcelFile(String filename, Page p)
		{

			//p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
			//string redirect = "<script>location.assign(window.location);</script>";
			//p.Response.Write(redirect);
			p.Response.Clear();
			//p.Response.Buffer = true; //alex
			MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
			//var dir = GetExportDirectory(p);
			//-- Download
			p.Response.ContentType = "text/plain";
			p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
			p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
			p.Response.BinaryWrite(msMemory.ToArray());
			p.Response.End();
			//p.Response.OutputStream.Flush();//alex
			//p.Response.OutputStream.Close();//alex
			//p.Response.TransmitFile(filename);
			//p.Response.TransmitFile(filename);
			//p.Response.Flush(); //alex
			//p.Response.Redirect("ScreeningFrontEntries.aspx");
			try
			{
				//SyntheticScreeningModel data = new SyntheticScreeningModel();
				//data.ScreeningID = 123;
				//data.SKUName = txtSku.Text;
				//data.CustomerName = txtCustomerName.Text;
				//Session["ScreeningData"] = data;
				// Context.Items.Add("abc", "xyz");
				//HttpContext.Current.ApplicationInstance.CompleteRequest();
				//Response.Redirect("ScreeningFrontEntries.aspx", true);
				//Response.Redirect(Request.RawUrl);
				//Context.Items.Add(txtGSIOrder, "abc");
				//Server.Transfer("ScreeningFrontEntries.aspx", false);
				//string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
				//Response.Redirect(navigate);
				//                p.Response.End();
			//	HttpContext.Current.Response.End();


			}
			catch (Exception ex)
			{
				// string abc = ex.Message;
				// txtGSIOrder.Text = "abc";

			}
		}
	}
}