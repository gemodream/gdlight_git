﻿
using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace Corpt
{
    public partial class ScreeningTester : System.Web.UI.Page
    {
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			if (!IsPostBack)
			{
				LoadScreeningInstrument();
			}

			this.Form.DefaultFocus = txtOrderCode.ClientID;
			this.Form.DefaultButton = btnLoadBatch.UniqueID;
		}
		public void LoadScreeningInstrument()
		{
			//Load Screening Instrument
			DataTable dtScreeningInstrument = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);
			ddlScreeningInstrument.DataTextField = "InstrumentName";
			ddlScreeningInstrument.DataValueField = "InstrumentCode";
			ddlScreeningInstrument.DataSource = dtScreeningInstrument;
			ddlScreeningInstrument.DataBind();

			repScreeningInstrument.DataSource = dtScreeningInstrument;
			repScreeningInstrument.DataBind();
			ddlScreeningInstrument.DataBind();
			ListItem liBlankInstrument = new ListItem();
			liBlankInstrument.Value = "0";
			liBlankInstrument.Text = "";
			ddlScreeningInstrument.Items.Insert(0, liBlankInstrument);
		}
		#endregion

		#region Other Event
		protected void btnLoadBatch_Click(object sender, EventArgs e)
		{
			//Get Order and memo details
			var orderModel = QueryUtils.GetOrderInfo(txtOrderCode.Text.Trim(), this);

			if (orderModel == null)
			{
				var message = string.Format("Order {0} not found.", txtOrderCode.Text.Trim());
				lblInvalidLabel.Text = message;
				lstOrderBatchlist.Visible = false;
				tblDetail.Visible = false;
				return;
			}

			var cpList = QueryUtils.GetOrderCp(orderModel, this);
			if (cpList.Count(cp => cp.CustomerProgramName == "TND") == 0)
			{
				lblInvalidLabel.Text = "Customer Program Name TND not found.";
				lstOrderBatchlist.Visible = false;
				tblDetail.Visible = false;
				return;
			}
			DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			if (ds.Tables[0].Rows.Count == 0)
			{
				lblInvalidLabel.Text = "Order is not associated with Synthetic Testing";
				return;
			}
			else
			{
				lstOrderBatchlist.DataSource = ds.Tables[1];
				lstOrderBatchlist.DataBind();
				if (ds.Tables[1].Rows.Count >= 1)
				{
					lstOrderBatchlist.SelectedIndex = 0;
					lblBatch.Text = "Data For #" + lstOrderBatchlist.SelectedItem.Text;
					lstOrderBatchlist.Visible = true;
					tblDetail.Visible = true;
					lblInvalidLabel.Text = "";
					lstOrderBatchlist_SelectedIndexChanged(null, null);
				}
				else
				{
					lblInvalidLabel.Text = "Items not found";
					lstOrderBatchlist.Visible = false;
					tblDetail.Visible = false;
				}
			}
		}

		protected void lstOrderBatchlist_SelectedIndexChanged(object sender, EventArgs e)
		{
			lblBatch.Text = "Data For #" + lstOrderBatchlist.SelectedItem.Text;
			DataSet dsBatches = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			DataTable dtBatch = dsBatches.Tables[1].Select("FullBatchNumber = '" + lstOrderBatchlist.SelectedItem.Text + "'").CopyToDataTable();
			if (dtBatch.Rows.Count >= 1)
			{
				int StatusID = Convert.ToInt16(dtBatch.Rows[0]["StatusID"].ToString());
				Clear();
                if (StatusID == (int)SynhteticOrderStatus.ScreeningComplete)
                {
                    lblInvalidLabel.Text = "";
                    tblDetail.Visible = true;
                    lblNoofItems.Text = dtBatch.Rows[0]["AllocateItemQty"].ToString();
                    txtTestingInstrument.Text = "";
                    hdnTestingInstrumentCode.Value = "0";

                }
                else if (StatusID == (int)SynhteticOrderStatus.TestingComplete)
                {
                    lblNoofItems.Text = dtBatch.Rows[0]["AllocateItemQty"].ToString();
                    hdnTestingInstrumentCode.Value = dtBatch.Rows[0]["TestingInstrumentID"].ToString();
                    txtSyntheticStone.Text = dtBatch.Rows[0]["TesterSyntheticStones"].ToString();
                    txtSuspectedStone.Text = dtBatch.Rows[0]["TesterSuspectStones"].ToString();
                    txtFiledStones.Text = dtBatch.Rows[0]["TesterFailedStones"].ToString();
                    txtItemwithFailStone.Text = dtBatch.Rows[0]["TesterFailItems"].ToString();
                    txtFailedStonePerItems.Text = dtBatch.Rows[0]["TesterFailedStonesPerItem"].ToString();
                    txtComment.Text = dtBatch.Rows[0]["TesterComments"].ToString();
                    txtTestingInstrument.Text = dtBatch.Rows[0]["TesterInstrumentName"].ToString();

                    lblInvalidLabel.Text = "";
                }
                else if (StatusID <= (int)SynhteticOrderStatus.ScreeningComplete)
                {
                    lblInvalidLabel.Text = "Batch screening is not completed.";
                    tblDetail.Visible = false;
                }
                else
                {
                    lblInvalidLabel.Text = "Batch packing is already completed.";
                    tblDetail.Visible = false;
                }
			}
		}


		protected void repScreeningInstrument_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (string.Compare(e.CommandName, "InstrumentCode", false) == 0)
			{
				txtTestingInstrument.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
				hdnTestingInstrumentCode.Value = ((System.Web.UI.WebControls.Button)e.CommandSource).CommandArgument;
				ddlScreeningInstrument.ClearSelection();
				ddlScreeningInstrument.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
			}
		}

		protected void ddlScreeningInstrument_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtTestingInstrument.Text = ddlScreeningInstrument.SelectedItem.Text;
			hdnTestingInstrumentCode.Value = ddlScreeningInstrument.SelectedItem.Value;
			ModalPopupExtender3.Hide();
		}
		#endregion

		#region Save
		protected void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (hdnTestingInstrumentCode.Value == "0")
				{
					lblInvalidLabel.Text = "Please Select Screening Instrument";
					return;
				}
				else if (txtSyntheticStone.Text.ToString().Trim() == "")
				{
                    txtSyntheticStone.Text = "0";
                }
				else if (txtSuspectedStone.Text.ToString().Trim() == "")
				{
                    txtSuspectedStone.Text = "0";
                }
				else if (txtFiledStones.Text.ToString().Trim() == "")
				{
                    txtFiledStones.Text = "0";
                }
                else if (txtItemwithFailStone.Text.ToString().Trim() == "")
                {
                    txtItemwithFailStone.Text = "0";
                }

                SyntheticBatchModel objBatch = new SyntheticBatchModel();
				objBatch.OrderCode = Utils.ParseOrderCode( lstOrderBatchlist.SelectedItem.Text );
				objBatch.BatchCode = Utils.ParseBatchCode( lstOrderBatchlist.SelectedItem.Text );
				objBatch.TesterID = Convert.ToInt32(Session["ID"]);
				objBatch.StatusID= (int)SynhteticOrderStatus.TestingComplete;
				objBatch.TestingInstrumentID = Convert.ToInt32(hdnTestingInstrumentCode.Value);
				objBatch.TesterPassItems = Convert.ToInt32(lblNoofItems.Text.ToString().Trim()) - Convert.ToInt32(txtItemwithFailStone.Text.ToString().Trim());
                objBatch.TesterFailedStones = Convert.ToInt32(txtFiledStones.Text.ToString().Trim());
				objBatch.TesterSyntheticStones = Convert.ToInt32(txtSyntheticStone.Text.ToString().Trim());
				objBatch.TesterSuspectStones = Convert.ToInt32(txtSuspectedStone.Text.ToString().Trim());
				objBatch.TesterFailItems = Convert.ToInt32(txtItemwithFailStone.Text.ToString().Trim());
				objBatch.TesterFailedStonesPerItem = txtFailedStonePerItems.Text.ToString().Trim();
				objBatch.TesterComments = txtComment.Text.ToString().Trim();

				objBatch.UpdateID = 3; // Update Tester Detail
				bool result = SyntheticScreeningUtils.SetSyntheticBatch(objBatch, this);

				///History Entry
				SyntheticOrderHistoryModel objSyntheticOrder = new SyntheticOrderHistoryModel();
				objSyntheticOrder.OrderCode = Utils.ParseOrderCode( lstOrderBatchlist.SelectedItem.Text );
				objSyntheticOrder.BatchCode = Utils.ParseBatchCode( lstOrderBatchlist.SelectedItem.Text );
				objSyntheticOrder.StatusId = (int)SynhteticOrderStatus.TestingComplete;
				objSyntheticOrder.ItemQty = Convert.ToInt32(lblNoofItems.Text.ToString().Trim());
				objSyntheticOrder.AssignedTo = Convert.ToInt32(this.Session["ID"].ToString());
				string historymsg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);

				if (result == true)
				{
					//Clear();
					lblInvalidLabel.Text = "";
					PopupInfoDialog("Testing save successfully", false);
				}
			}
			catch (Exception ex)
			{
				lblInvalidLabel.Text = ex.Message.ToString();
			}
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			Clear();
		}
		public void Clear()
		{
			hdnTestingInstrumentCode.Value = "0";
			txtFiledStones.Text = "0";
			txtSyntheticStone.Text = "0";
			txtSuspectedStone.Text = "0";
			txtItemwithFailStone.Text = "0";
			txtFailedStonePerItems.Text = "";
			txtComment.Text = "";
			txtTestingInstrument.Text = "";
			lblInvalidLabel.Text = "";
		}
		#endregion

		#region Popup Dialog

		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion

		#region Print Label
		protected void btnPrintLabel_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}
		public void PrintPDFReceipt()
		{
			DataSet ds = new DataSet();
			ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", "", txtOrderCode.Text, this);
			if (ds == null || ds.Tables[0].Rows.Count == 0)
			{
				lblInvalidLabel.Text = "Records not found.";
				return;
			}

			DataTable dt = new DataTable();
			DataTable dtResult = new DataTable();

			dt = ds.Tables[0];

			string requestID = dt.Rows[0]["RequestID"].ToString();

			// Create a Document object
			var document = new Document(PageSize.A4, 25, 25, 25, 25);

			// Create a new PdfWrite object, writing the output to a MemoryStream
			var output = new MemoryStream();
			var writer = PdfWriter.GetInstance(document, output);

			// Open the Document for writing
			document.Open();

			// First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
			var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
			var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
			var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
			var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
			var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
			var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);

			// Finally, add an image in the upper right corner
			var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/logo.gif"));
			logo.SetAbsolutePosition(50, 760);
			logo.ScalePercent(60);
			document.Add(logo);

			// Add the Receipt title
			var rptHeader = new Paragraph("Gemological Science International", headerFont);

			rptHeader.Alignment = Element.ALIGN_CENTER;
			document.Add(rptHeader);

			var repSubHeader = new Paragraph("www.gemscience.net", bodyFont);
			repSubHeader.Alignment = Element.ALIGN_CENTER;
			document.Add(repSubHeader);
			document.Add(Chunk.NEWLINE);

			// Add the Receipt title
			var rptTitle = new Paragraph("Batch Receipt", titleFont);
			rptTitle.SpacingBefore = 10;
			document.Add(rptTitle);

			// Now add the "Your order details are below." message
			document.Add(new Paragraph("Your order details are below.", bodyFont));
			document.Add(Chunk.NEWLINE);
			document.Add(Chunk.NEWLINE);

			Barcode128 bc = new Barcode128();
			bc.TextAlignment = Element.ALIGN_CENTER;
			bc.Code = requestID;
			bc.StartStopText = false;
			bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
			bc.Extended = true;

			PdfContentByte cb = writer.DirectContent;
			iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
			patImage.ScaleToFit(250, 50);
			patImage.SetAbsolutePosition(450, 700);
			document.Add(patImage);

			// Add the "Order Information" subtitle
			document.Add(new Paragraph("Order Information", subTitleFont));

			PdfPTable mtable = new PdfPTable(2);
			mtable.WidthPercentage = 100;
			mtable.SetWidths(new int[] { 300, 300 });

			mtable.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

			// Create the Order Information table - see http://www.mikesdotnetting.com/Article/86/iTextSharp-Introducing-Tables for more info
			var orderInfoTable = new PdfPTable(2);
			orderInfoTable.HorizontalAlignment = 0;
			orderInfoTable.SpacingBefore = 10;
			orderInfoTable.SpacingAfter = 7;
			orderInfoTable.DefaultCell.Border = 0;
			orderInfoTable.TotalWidth = 300;
			orderInfoTable.LockedWidth = true;
			orderInfoTable.SetWidths(new int[] { 100, 200 });

			orderInfoTable.AddCell(new Phrase("RequestID:", boldTableFont));
			orderInfoTable.AddCell(requestID);

			orderInfoTable.AddCell(new Phrase("Memo:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["MemoNum"].ToString());

			orderInfoTable.AddCell(new Phrase("PO Number:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["PONum"].ToString());

			orderInfoTable.AddCell(new Phrase("Style:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["StyleName"].ToString());

			orderInfoTable.AddCell(new Phrase("SKU:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["SKUName"].ToString());

			orderInfoTable.AddCell(new Phrase("Total Quantity:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["TotalQty"].ToString());

			orderInfoTable.AddCell(new Phrase("Retailer:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["RetailerName"].ToString());

			orderInfoTable.AddCell(new Phrase("Category:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["CategoryName"].ToString());

			orderInfoTable.AddCell(new Phrase("Service Type:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["ServiceTypeName"].ToString());

			orderInfoTable.AddCell(new Phrase("Service Time:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["ServiceTimeName"].ToString());

			orderInfoTable.AddCell(new Phrase("Jewelry Type:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["JewelryTypeName"].ToString());

			orderInfoTable.AddCell(new Phrase("Order Number:", boldTableFont));
			orderInfoTable.AddCell(dt.Rows[0]["GSIOrder"].ToString());

			orderInfoTable.AddCell(new Phrase("Order Created On:", boldTableFont));

			string OrderCreateDate = dt.Rows[0]["CreateDate"].ToString() == "01/01/1900 12:00:00 AM" ? "" : Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
			orderInfoTable.AddCell(OrderCreateDate);

			string batchTotal = "";
			if (ds.Tables.Count == 3)
			{
				batchTotal = ds.Tables[2].Rows.Count == 0 ? "" : ds.Tables[2].Rows.Count.ToString();
			}

			orderInfoTable.AddCell(new Phrase("Total Batches:", boldTableFont));
			orderInfoTable.AddCell(batchTotal);

			mtable.AddCell(orderInfoTable);

			// Create the Order Details table
			orderInfoTable = new PdfPTable(4);
			orderInfoTable.TotalWidth = 250;
			orderInfoTable.LockedWidth = true;
			//relative col widths in proportions - 1/3 and 2/3
			float[] widths = new float[] { 45, 45, 80, 80 };
			orderInfoTable.SetWidths(widths);

			orderInfoTable.HorizontalAlignment = 1;
			orderInfoTable.SpacingBefore = 10;
			orderInfoTable.SpacingAfter = 10;

			orderInfoTable.AddCell(new Phrase("Batch #", boldTableFont));
			orderInfoTable.AddCell(new Phrase("Quantity", boldTableFont));
			orderInfoTable.AddCell(new Phrase("Screener", boldTableFont));
			orderInfoTable.AddCell(new Phrase("Tester", boldTableFont));

			if (ds.Tables.Count == 3)
			{
				if (ds.Tables[2].Rows.Count != 0)
				{
					foreach (DataRow row in ds.Tables[2].Rows)
					{
						orderInfoTable.AddCell(row["BatchCode"].ToString());
						orderInfoTable.AddCell(row["Quantity"].ToString());
						orderInfoTable.AddCell(row["Screener"].ToString());
						orderInfoTable.AddCell(row["Tester"].ToString());
					}
				}
			}

			//orderInfoTable.AddCell("1");
			//orderInfoTable.AddCell("50");

			//orderInfoTable.AddCell("2");
			//orderInfoTable.AddCell("50");

			//orderInfoTable.AddCell("3");
			//orderInfoTable.AddCell("50");

			//orderInfoTable.AddCell("4");
			//orderInfoTable.AddCell("50");

			//orderInfoTable.AddCell("5");
			//orderInfoTable.AddCell("50");

			//orderInfoTable.AddCell("6");
			//orderInfoTable.AddCell("50");

			mtable.AddCell(orderInfoTable);

			document.Add(mtable);

			var screenerInfoTable = new PdfPTable(4);
			screenerInfoTable.HorizontalAlignment = 0;
			screenerInfoTable.SpacingBefore = 10;
			screenerInfoTable.SpacingAfter = 10;
			screenerInfoTable.TotalWidth = 460f;
			screenerInfoTable.LockedWidth = true;
			float[] tableWidth = new float[] { 175f, 55f, 175f, 55f };

			screenerInfoTable.SetWidths(tableWidth);

			PdfPCell cellScreener = new PdfPCell(new Phrase("Screener", subTitleFont));
			cellScreener.Colspan = 2;
			screenerInfoTable.AddCell(cellScreener);
			PdfPCell cellTester = new PdfPCell(new Phrase("Tester", subTitleFont));
			cellTester.Colspan = 2;
			screenerInfoTable.AddCell(cellTester);

			string TotalPassScreening = "";
			string TotalSynthetic = "";
			string TotalFailScreening = "";
			string TotalSuspect = "";
			string TotalPass = "";

			if (ds.Tables.Count >= 2)
			{
				dtResult = ds.Tables[1];
				if (dtResult.Rows.Count != 0)
				{
					//Screener = dtResult.Rows[0]["Screener"].ToString();
					//Tester = dtResult.Rows[0]["Tester"].ToString();
					TotalPassScreening = dtResult.Rows[0]["TotalPassScreening"].ToString();
					TotalSynthetic = dtResult.Rows[0]["TotalSynthetic"].ToString();
					TotalFailScreening = dtResult.Rows[0]["TotalFailScreening"].ToString();
					TotalSuspect = dtResult.Rows[0]["TotalSuspect"].ToString();
					TotalPass = dtResult.Rows[0]["TotalPass"].ToString();
				}
			}

			screenerInfoTable.AddCell(new Phrase("Total Pass Screening Items/Stones:", boldTableFont));
			screenerInfoTable.AddCell(new Phrase(TotalPassScreening, bodyFont));
			screenerInfoTable.AddCell(new Phrase("Total Synthetic Items/Stones:", boldTableFont));
			screenerInfoTable.AddCell(new Phrase(TotalSynthetic, bodyFont));

			screenerInfoTable.AddCell(new Phrase("Total Fail Screening Items/Stones:", boldTableFont));
			screenerInfoTable.AddCell(new Phrase(TotalFailScreening, bodyFont));
			screenerInfoTable.AddCell(new Phrase("Total Suspect Items/Stones:", boldTableFont));
			screenerInfoTable.AddCell(new Phrase(TotalSuspect, bodyFont));

			screenerInfoTable.AddCell(new Phrase("", bodyFont));
			screenerInfoTable.AddCell(new Phrase("", bodyFont));
			screenerInfoTable.AddCell(new Phrase("Total Pass Items/Stones:", boldTableFont));
			screenerInfoTable.AddCell(new Phrase(TotalPass, bodyFont));

			document.Add(screenerInfoTable);

			// Add ending message
			var endingMessage = new Paragraph("For assistance please email to support@gemscience.net or call GSI office in your country.\nYou can find the phone information by following the link www.gemscience.net/contact/.", endingMessageFont);
			//endingMessage.
			document.Add(endingMessage);

			document.Close();

			Response.ContentType = "application/pdf";
			Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", requestID));
			Response.BinaryWrite(output.ToArray());
		}
		#endregion
	}
}