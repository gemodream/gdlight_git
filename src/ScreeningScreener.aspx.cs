﻿using Corpt.Models;
using Corpt.Utilities;
using System;
using System.Data;
using System.Linq;

namespace Corpt
{
    public partial class ScreeningScreener : System.Web.UI.Page
    {
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["ID"] == null) Response.Redirect("Login.aspx");

			this.Form.DefaultFocus = txtOrderCode.ClientID;
			this.Form.DefaultButton = btnLoadBatch.UniqueID;
			//if (!IsPostBack)
			//{
			//	LoadScreeningInstrument();
			//}
		}

		//public void LoadScreeningInstrument()
		//{
		//	//Load Screening Instrument
		//	DataTable dtScreeningInstrument = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);
		//	ddlScreeningInstrument.DataTextField = "InstrumentName";
		//	ddlScreeningInstrument.DataValueField = "InstrumentCode";
		//	ddlScreeningInstrument.DataSource = dtScreeningInstrument;
		//	ddlScreeningInstrument.DataBind();

		//	repScreeningInstrument.DataSource = dtScreeningInstrument;
		//	repScreeningInstrument.DataBind();
		//	ddlScreeningInstrument.DataBind();
		//	ListItem liBlankInstrument = new ListItem();
		//	liBlankInstrument.Value = "0";
		//	liBlankInstrument.Text = "";
		//	ddlScreeningInstrument.Items.Insert(0, liBlankInstrument);
		//}
		#endregion

		#region Other Events
		protected void btnLoadBatch_Click(object sender, EventArgs e)
		{
			//Get Order and memo details
			var orderModel = QueryUtils.GetOrderInfo(txtOrderCode.Text.Trim(), this);

			if (orderModel == null)
			{
				var message = string.Format("Order {0} not found.", txtOrderCode.Text.Trim());
				lblInvalidLabel.Text = message;
				lstOrderBatchlist.Visible = false;
				tblDetail.Visible = false;
				return;
			}

			var cpList = QueryUtils.GetOrderCp(orderModel, this);
			if (cpList.Count(cp => cp.CustomerProgramName == "TND") == 0)
			{
				lblInvalidLabel.Text = "Customer Program Name TND not found.";
				lstOrderBatchlist.Visible = false;
				tblDetail.Visible = false;
				return;
			}
			DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			if (ds.Tables[0].Rows.Count == 0)
			{
				lblInvalidLabel.Text = "Order is not associated with Synthetic Screening";
				return;
			}
			else
			{
				lstOrderBatchlist.DataSource = ds.Tables[1];
				lstOrderBatchlist.DataBind();
				if (ds.Tables[1].Rows.Count >= 1)
				{
					lstOrderBatchlist.SelectedIndex = 0;
					lblBatch.Text = "Data For #" + lstOrderBatchlist.SelectedItem.Text;
					lstOrderBatchlist.Visible = true;
					tblDetail.Visible = true;
					lblInvalidLabel.Text = "";
					lstOrderBatchlist_SelectedIndexChanged(null, null);
				}
				else
				{
					lblInvalidLabel.Text = "Items not found";
					lstOrderBatchlist.Visible = false;
					tblDetail.Visible = false;
				}
			}
		}

		protected void lstOrderBatchlist_SelectedIndexChanged(object sender, EventArgs e)
		{
            lblBatch.Text = "Data For #" + lstOrderBatchlist.SelectedItem.Text;
			DataSet dsBatches = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			DataTable dtBatch = dsBatches.Tables[1].Select("FullBatchNumber = '" + lstOrderBatchlist.SelectedItem.Text + "'").CopyToDataTable();
			if (dtBatch.Rows.Count>=1)
			{
				int StatusID = Convert.ToInt16(dtBatch.Rows[0]["StatusID"].ToString());
				if (dtBatch.Rows[0]["ScreenerID"].ToString() != "")
				{
					Clear();
					int ScreenerID = Convert.ToInt16(dtBatch.Rows[0]["ScreenerID"].ToString());
					string ScreenerName = dtBatch.Rows[0]["ScreenerName"].ToString();
					//lblUser.Text = "(" + ScreenerName + " )";

					if (ScreenerID != Convert.ToInt32(Session["ID"]))
					{
						lblInvalidLabel.Text = "Screening not allowed, Batch allocated to " + ScreenerName;
						tblDetail.Visible = false;
						return;
					}
					
				}

                if (StatusID <= (int)SynhteticOrderStatus.BatchOpen)
                {
                    lblInvalidLabel.Text = "Batch is not assigned to user.";
                    tblDetail.Visible = false;
                    return;
                }
                else if(StatusID == (int) SynhteticOrderStatus.ScreeningStart)
				{
					lblInvalidLabel.Text = "";
					tblDetail.Visible = true;
					lblNoofItems.Text = dtBatch.Rows[0]["AllocateItemQty"].ToString();
					lblScreeningInstrument.Text= dtBatch.Rows[0]["ScreenerInstrumentName"].ToString();
					lblAssignedUser.Text= dtBatch.Rows[0]["ScreenerName"].ToString();
                    txtPassItems.Text = "0";
                    txtItemswithFailStone.Text = "0";
                    txtFailStone.Text = "0";
                    txtFailedStonePerItems.Text = "";
                    txtComment.Text = "";
                }
                else if (StatusID == (int)SynhteticOrderStatus.ScreeningComplete)
                {
                    lblInvalidLabel.Text = "";
                    tblDetail.Visible = true;
                    lblNoofItems.Text = dtBatch.Rows[0]["AllocateItemQty"].ToString();
                    lblScreeningInstrument.Text = dtBatch.Rows[0]["ScreenerInstrumentName"].ToString();
                    lblAssignedUser.Text = dtBatch.Rows[0]["ScreenerName"].ToString();

                    txtPassItems.Text = dtBatch.Rows[0]["ScreenerPassItems"].ToString();
                    txtItemswithFailStone.Text = dtBatch.Rows[0]["ScreenerFailItems"].ToString();
                    txtFailStone.Text = dtBatch.Rows[0]["ScreenerFailItems"].ToString();
                    txtFailedStonePerItems.Text = dtBatch.Rows[0]["ScreenerFailedStonesPerItem"].ToString();
                    txtComment.Text = dtBatch.Rows[0]["ScreenerComments"].ToString();
                }
				else 
				{
					lblInvalidLabel.Text = "Batch screening is already completed.";
					tblDetail.Visible = false;
				}
			}
				
		}
		
		//protected void repScreeningInstrument_ItemCommand(object source, RepeaterCommandEventArgs e)
		//{
		//	if (string.Compare(e.CommandName, "InstrumentCode", false) == 0)
		//	{
		//		txtScreeningInstrument.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
		//		hdnScreeningInstrumentCode.Value = ((System.Web.UI.WebControls.Button)e.CommandSource).CommandArgument;
		//		ddlScreeningInstrument.ClearSelection();
		//		ddlScreeningInstrument.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
		//	}
		//}

		//protected void ddlScreeningInstrument_SelectedIndexChanged(object sender, EventArgs e)
		//{
		//	txtScreeningInstrument.Text = ddlScreeningInstrument.SelectedItem.Text;
		//	hdnScreeningInstrumentCode.Value = ddlScreeningInstrument.SelectedItem.Value;
		//	ModalPopupExtender3.Hide();
		//}
		#endregion

		#region  Save Button
		protected void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				//if (hdnScreeningInstrumentCode.Value == "0")
				//{
				//	lblInvalidLabel.Text = "Please select screening instrument";
				//	return;
				//}
				//else
				if (txtPassItems.Text.ToString().Trim()=="")
				{
                    txtPassItems.Text = "0";
				}
				else if (txtItemswithFailStone.Text.ToString().Trim() == "")
				{
                    txtItemswithFailStone.Text = "0";
				}
				else if (txtFailStone.Text.ToString().Trim() == "")
				{
                    txtFailStone.Text = "0";
				}
				int PassItems = Convert.ToInt32(txtPassItems.Text.ToString().Trim() == "" ? "0" : txtPassItems.Text.ToString().Trim());
				int FailItems = Convert.ToInt32(txtItemswithFailStone.Text.ToString().Trim() == "" ? "0" : txtItemswithFailStone.Text.ToString());
				if (PassItems + FailItems != Convert.ToInt32(lblNoofItems.Text.ToString().Trim()))
				{
					lblInvalidLabel.Text = "Sum of pass & fail items should be same as batch total.";
					return;
				}
				SyntheticBatchModel objBatch = new SyntheticBatchModel();
				objBatch.OrderCode = Utils.ParseOrderCode( lstOrderBatchlist.SelectedItem.Text);
				objBatch.BatchCode = Utils.ParseBatchCode( lstOrderBatchlist.SelectedItem.Text);
				objBatch.StatusID = (int) SynhteticOrderStatus.ScreeningComplete;
				objBatch.ScreenerPassItems = Convert.ToInt32(txtPassItems.Text.ToString().Trim());
				objBatch.ScreenerFailItems = Convert.ToInt32(txtItemswithFailStone.Text.ToString().Trim());
				objBatch.ScreenerFailedStones = Convert.ToInt32(txtFailStone.Text.ToString().Trim());
				objBatch.ScreenerFailedStonesPerItem = txtFailedStonePerItems.Text.ToString().Trim();
				objBatch.ScreenerComments = txtComment.Text.ToString().Trim();
				objBatch.UpdateID = 2; // Update Screener Detail

				///History Entry
				SyntheticOrderHistoryModel objSyntheticOrder = new SyntheticOrderHistoryModel();
				objSyntheticOrder.OrderCode = Utils.ParseOrderCode( lstOrderBatchlist.SelectedItem.Text );
				objSyntheticOrder.BatchCode = Utils.ParseBatchCode( lstOrderBatchlist.SelectedItem.Text );
				objSyntheticOrder.StatusId = (int)SynhteticOrderStatus.ScreeningComplete;
				objSyntheticOrder.ItemQty = Convert.ToInt32(lblNoofItems.Text.ToString().Trim());
				objSyntheticOrder.AssignedTo = Convert.ToInt32(this.Session["ID"].ToString());
				string historymsg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);

				bool result = SyntheticScreeningUtils.SetSyntheticBatch(objBatch, this);
				if (result == true)
				{
					//Clear();
					lblInvalidLabel.Text = "";
					PopupInfoDialog("Screening save successfully", false);
					//ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Screening Save successfully');", true);
				}
			}
			catch (Exception ex)
			{
				lblInvalidLabel.Text = ex.Message.ToString();
			}
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			Clear();
		}
		public void Clear()
		{
			
			txtPassItems.Text = "0";
			txtItemswithFailStone.Text = "0";
			txtFailStone.Text = "0";
			txtFailedStonePerItems.Text = "";
			txtComment.Text = "";
			//lblAssignedUser.Text = "";
			//lblScreeningInstrument.Text = "";
			lblInvalidLabel.Text = "";
		}
		#endregion

		#region Popup Dialog

		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion

		
	}
}