﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ChangeUserPassword.aspx.cs" Inherits="Corpt.ChangeUserPassword" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoheading">Change Password</div>
    <div class="navbar nav-tabs">
        <asp:Button runat="server" ID="ChangePasswordBtn" Text="Save" OnClick="OnChangePasswordClick" CssClass="btn btn-info" />
        <asp:Label runat="server" ID="WrongInfoLabel" ForeColor="Red"></asp:Label>
    </div>
    <table>
        <tr>
            <td><asp:Label runat="server" Text="Login Name"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="LoginNameField" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Old Password"></asp:Label></td>
            <td><input id="OldPasswordField"  runat="server" type="password"/></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label2" runat="server" Text="New Password"></asp:Label></td>
            <td><input id="NewPasswordField" type="password"  runat="server"/></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Confirm New Password"></asp:Label></td>
            <td><input id="NewPasswordField2" type="password"  runat="server" /></td>
        </tr>
    </table>
        <asp:RequiredFieldValidator runat="server" ID="OldReq"
            ControlToValidate="OldPasswordField"
            Display="None"
            
        ErrorMessage="<b>Required Field Missing</b><br />A Old Password is required." 
        ValidationGroup="PwdGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OldReqE"
            TargetControlID="OldReq"
            HighlightCssClass="validatorCalloutHighlight"/>

        <asp:RequiredFieldValidator runat="server" ID="NewReq"
            ControlToValidate="NewPasswordField"
            Display="None"
            
        ErrorMessage="<b>Required Field Missing</b><br />A New Password is required." 
        ValidationGroup="PwdGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
            TargetControlID="NewReq"
            HighlightCssClass="validatorCalloutHighlight"/>

        <asp:RequiredFieldValidator runat="server" ID="NewReq2"
            ControlToValidate="NewPasswordField2"
            Display="None"
            
        ErrorMessage="<b>Required Field Missing</b><br />A Confirm New Password is required." 
        ValidationGroup="PwdGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
            TargetControlID="NewReq2"
            HighlightCssClass="validatorCalloutHighlight"/>

</asp:Content>
