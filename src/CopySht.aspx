<%@ Page language="c#" Codebehind="CopySht.aspx.cs" AutoEventWireup="false" Inherits="Corpt.CopySht" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CopySht</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/main.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iewc:TreeView id="sourceTree" style="Z-INDEX: 101; POSITION: absolute; TOP: 96px; LEFT: 128px"
				runat="server" Width="225px" Height="264px" AutoPostBack="True"></iewc:TreeView>
			<asp:Label id="lblDestCustomer" style="Z-INDEX: 119; POSITION: absolute; TOP: 56px; LEFT: 528px"
				runat="server" Width="352px" CssClass="text"></asp:Label>
			<asp:Label id="lblDest" style="Z-INDEX: 113; POSITION: absolute; TOP: 72px; LEFT: 528px" runat="server"
				CssClass="text"></asp:Label>
			<asp:button id="cmdHome" style="Z-INDEX: 111; POSITION: absolute; TOP: 0px; LEFT: 0px" runat="server"
				Height="16px" Width="16px" Text="H" CssClass="buttonStyle"></asp:button>
			<asp:Label id="Label2" style="Z-INDEX: 110; POSITION: absolute; TOP: 16px; LEFT: 520px" runat="server"
				CssClass="text">Target Item</asp:Label>
			<asp:Button id="cmdCopy" style="Z-INDEX: 107; POSITION: absolute; TOP: 248px; LEFT: 424px" runat="server"
				CssClass="buttonStyle" Text="COPY"></asp:Button>
			<asp:TextBox id="txtDest" style="Z-INDEX: 106; POSITION: absolute; TOP: 32px; LEFT: 520px" runat="server"
				CssClass="inputStyleCC"></asp:TextBox>
			<asp:Button id="cmdLoadDest" style="Z-INDEX: 105; POSITION: absolute; TOP: 32px; LEFT: 680px"
				runat="server" CssClass="buttonStyle" Text="Dest"></asp:Button>
			<iewc:TreeView id="destTree" style="Z-INDEX: 102; POSITION: absolute; TOP: 96px; LEFT: 528px" runat="server"
				Width="248px" Height="264px" AutoPostBack="True"></iewc:TreeView>
			<asp:Button id="cmdLoad" style="Z-INDEX: 103; POSITION: absolute; TOP: 32px; LEFT: 280px" runat="server"
				Text="Load" CssClass="buttonStyle"></asp:Button>
			<asp:TextBox id="txtItemNumber" style="Z-INDEX: 104; POSITION: absolute; TOP: 32px; LEFT: 120px"
				runat="server" CssClass="inputStyleCC"></asp:TextBox>
			<asp:Label id="lblCopy" style="Z-INDEX: 108; POSITION: absolute; TOP: 216px; LEFT: 416px" runat="server"
				CssClass="text"></asp:Label>
			<asp:Panel id="Panel1" style="Z-INDEX: 116; POSITION: absolute; TOP: 192px; LEFT: 272px" runat="server"
				Height="272px" Width="352px" CssClass="text" BackColor="#E0E0E0" Visible="False" BorderColor="Gray"
				BorderStyle="Solid" BorderWidth="2px">
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">
					<asp:Label id="lblMessage" runat="server" CssClass="text" Font-Size="Medium"></asp:Label></P>
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">
					<asp:Button id="cmdOK" runat="server" CssClass="buttonStyle" Text="OK"></asp:Button></P>
			</asp:Panel>
			<asp:Label id="Label1" style="Z-INDEX: 109; POSITION: absolute; TOP: 16px; LEFT: 120px" runat="server"
				CssClass="text">Source Item</asp:Label>
			<asp:Image id="Image1" style="Z-INDEX: 100; POSITION: absolute; TOP: 136px; LEFT: 368px" runat="server"
				ImageUrl="Images/left2right.jpg"></asp:Image>
			<asp:Label id="lblSource" style="Z-INDEX: 112; POSITION: absolute; TOP: 72px; LEFT: 128px"
				runat="server" CssClass="text"></asp:Label>
			<asp:DataGrid id="sourceDataGrid" style="Z-INDEX: 114; POSITION: absolute; TOP: 296px; LEFT: 120px"
				runat="server" CssClass="text">
				<HeaderStyle Font-Bold="True" BackColor="#E0E0E0"></HeaderStyle>
			</asp:DataGrid>
			<asp:DataGrid id="DestDataGrid" style="Z-INDEX: 115; POSITION: absolute; TOP: 296px; LEFT: 520px"
				runat="server" CssClass="text">
				<HeaderStyle Font-Bold="True" BackColor="#E0E0E0"></HeaderStyle>
			</asp:DataGrid>
			<asp:Label id="lblSourceCustomer" style="Z-INDEX: 118; POSITION: absolute; TOP: 56px; LEFT: 128px"
				runat="server" Width="352px" CssClass="text"></asp:Label>
		</form>
	</body>
</HTML>
