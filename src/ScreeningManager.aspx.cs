﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	public partial class ScreeningManager : System.Web.UI.Page
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");

			if (!IsPostBack)
			{
				
				LoadUserStatus();
				ClearForm();
				LoadPosition();
			}
			this.Form.DefaultFocus = txtSearch.ClientID;
			this.Form.DefaultButton = imgOrderSearch.UniqueID;

		}
		private void LoadPosition()
		{
			DataSet dsUserStatus = SyntheticScreeningUtils.GetSyntheticOrderStatus(1,this);
			drpPosition.DataTextField = "StatusName";
			drpPosition.DataValueField = "StatusID";
			drpPosition.DataSource = dsUserStatus.Tables[0];
			drpPosition.DataBind();
			ListItem liPosition = new ListItem();
			liPosition.Text = "";
			liPosition.Value = "0";
			drpPosition.Items.Insert(0, liPosition);
		}
		private void LoadUserStatus()
		{
			DataSet dsUserStatus = SyntheticScreeningUtils.GetSyntheticUserStatus(this);
			gdvUserStatus.DataSource = dsUserStatus.Tables[0];
			gdvUserStatus.DataBind();
			if(dsUserStatus.Tables[0].Rows.Count>=0)
			{
				dvUserEmptyRecord.Visible = false;
			}
			else
			{
				dvUserEmptyRecord.Visible = true;
			}

		}

		public void LoadOrder()
		{
			int OrderCode = 0;
			string CreateDate = string.Empty;
			string MemoNum = string.Empty;
			int CustomerCode = 0;
			int PositionCode = 0;
			lblInvalidLabel.Text = "";
			if (rblViewByPosition.Checked == true)
			{
				if (drpPosition.SelectedItem.Value == "0")
				{
					lblInvalidLabel.Text = "Please enter position";
					return;
				}
			}
			else if (txtSearch.Text.ToString().Trim() == "")
			{
				if (rblViewByOrder.Checked == true)
				{
					lblInvalidLabel.Text = "Please enter ordercode";
				}
				else if (rblViewByDate.Checked == true)
				{
					lblInvalidLabel.Text = "Please enter date";
				}
				else if (rblViewByMemo.Checked == true)
				{
					lblInvalidLabel.Text = "Please enter memo";
				}
				else if (rblViewByCustomer.Checked == true)
				{
					lblInvalidLabel.Text = "Please enter customercode";
				}
				return;
			}

			if (rblViewByOrder.Checked==true)
			{
				OrderCode = Convert.ToInt32(txtSearch.Text.ToString().Trim());
			}
			else if (rblViewByDate.Checked == true)
			{
				CreateDate = Convert.ToDateTime(txtSearch.Text.ToString().Trim()).ToString("yyyy-MM-dd");
			}
			else if (rblViewByMemo.Checked == true)
			{
				MemoNum = Convert.ToString(txtSearch.Text.ToString().Trim());
			}
			else if (rblViewByCustomer.Checked == true)
			{
				CustomerCode = Convert.ToInt32(txtSearch.Text.ToString().Trim());
			}
			else if (rblViewByPosition.Checked == true)
			{
				PositionCode = Convert.ToInt32(drpPosition.SelectedItem.Value.ToString().Trim());
			}
			DataSet dsOrders= SyntheticScreeningUtils.GetSyntheticOrderViewBy(OrderCode, CreateDate, MemoNum, CustomerCode, PositionCode, this);
			if (dsOrders.Tables[0].Rows.Count==0)
			{
				lblInvalidLabel.Text = "Order not found";
				return;
			}
			lstOrderIDs.DataTextField = "OrderCode";
			lstOrderIDs.DataValueField = "OrderCode";
			lstOrderIDs.DataSource = dsOrders.Tables[0];
			lstOrderIDs.DataBind();
			
			if (dsOrders.Tables[0].Rows.Count != 0)
			{
				lstOrderIDs_SelectedIndexChanged(null, null);
			}
		}
		
		public void LoadBatchStatus(int orderCode)
		{
			DataSet dsOrderBatchStatus = SyntheticScreeningUtils.GetSyntheticBatch(orderCode, this);
			gdvBatchStatus.DataSource = dsOrderBatchStatus;
			gdvBatchStatus.DataBind();

			if (dsOrderBatchStatus.Tables[0].Rows.Count >= 0)
			{
				dvBatchStatus.Visible = false;
			}
			else
			{
				dvBatchStatus.Visible = true;
			}
		}
		#endregion

		#region SelectIndex
		protected void lstOrderIDs_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lstOrderIDs.SelectedItem != null)
			{
				int orderCode = Convert.ToInt32(lstOrderIDs.SelectedItem.Value.ToString());
				DataSet dsOrders = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(orderCode, this);
				DataSet dsOrdersHistory = new DataSet();
				if (dsOrders != null)
				{
					txtMemo.Text = dsOrders.Tables[0].Rows[0]["MemoNum"].ToString();
					txtTotalQty.Text = dsOrders.Tables[0].Rows[0]["TotalQty"].ToString();
					txtStyle.Text = dsOrders.Tables[0].Rows[0]["StyleName"].ToString();
					txtOrderCode.Text = dsOrders.Tables[0].Rows[0]["OrderCode"].ToString(); ;
					txtRetailer.Text = dsOrders.Tables[0].Rows[0]["RetailerName"].ToString();
					txtCustomer.Text = dsOrders.Tables[0].Rows[0]["CustomerName"].ToString();
					txtPONumber.Text = dsOrders.Tables[0].Rows[0]["PONum"].ToString();
					txtSKU.Text = dsOrders.Tables[0].Rows[0]["SKUName"].ToString();

					LoadBatchStatus(orderCode);

					dsOrdersHistory = SyntheticScreeningUtils.GetSyntheticOrderHistoryByOrderCode(orderCode, this);

				}
				else
				{
					ClearForm();
				}

				if (dsOrdersHistory != null && dsOrdersHistory.Tables[0].Rows.Count != 0)
				{

					gdvSyntheticOrderHistory.DataSource = dsOrdersHistory.Tables[0];
					gdvSyntheticOrderHistory.DataBind();
					if (dsOrdersHistory.Tables[0].Rows.Count >= 0)
					{
						dvOrderHistory.Visible = false;
					}
					else
					{
						dvOrderHistory.Visible = true;
					}
				}

			}
			else
			{
				ClearForm();
			}
		}

		protected void rblViewBy_SelectedIndexChanged(object sender, EventArgs e)
		{
			lstOrderIDs.Items.Clear();
			txtSearch.Text = "";
			if (rblViewByOrder.Checked == true)
			{
				txtSearch.Visible = true;
				drpPosition.Visible = false;
				txtSearch.TextMode = TextBoxMode.SingleLine;
			}
			else if (rblViewByDate.Checked == true)
			{
				txtSearch.Visible = true;
				drpPosition.Visible = false;
				txtSearch.TextMode = TextBoxMode.Date;
			}
			else if (rblViewByMemo.Checked == true)
			{
				txtSearch.Visible = true;
				drpPosition.Visible = false;
				txtSearch.TextMode = TextBoxMode.SingleLine;
			}
			else if (rblViewByCustomer.Checked == true)
			{
				txtSearch.Visible = true;
				drpPosition.Visible = false;
				txtSearch.TextMode = TextBoxMode.SingleLine;
			}
			else if (rblViewByPosition.Checked == true)
			{
				txtSearch.Visible = false;
				drpPosition.Visible = true;
			}
		}
		#endregion

		#region PopupInfo
		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion

		#region Button Event
		protected void btnClear_Click(object sender, EventArgs e)
		{
			lblMsg.Text = "";
			ClearForm();
			int orderCode = Convert.ToInt32(lstOrderIDs.SelectedItem.Value.ToString());
			LoadBatchStatus(orderCode);
		}

		private void ClearForm()
		{
			txtMemo.Text = "";
			txtTotalQty.Text = "";
			txtStyle.Text = "";
			txtOrderCode.Text = "";
			txtRetailer.Text = "";
			txtCustomer.Text = "";
			lstOrderIDs.ClearSelection();
			gdvSyntheticOrderHistory.DataSource = null;
			gdvSyntheticOrderHistory.DataBind();
			gdvBatchStatus.DataSource = null;
			gdvBatchStatus.DataBind();
		}

		protected void imgOrderSearch_Click(object sender, EventArgs e)
		{
			LoadOrder();
		}
		#endregion
	}
}