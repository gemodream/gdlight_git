﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models.Stats;

namespace Corpt
{
    public partial class RemoteSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Remote Session";
            if (IsPostBack) return;
			/* Security to prevent user accessing directly via url after logging in */
			string err = "";
			bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgRemoteSession, this, out err);
			if (!access || err != "")
			{
				Response.Redirect("Middle.aspx");
			}
			// */
			UserList.DataSource = QueryUtils.GetAppUsers(this);
            UserList.DataBind();
            UserList.SelectedIndex = 0;

            SetFilterFromRequest();
            var p_page = Request.Params[StatsFilterModel.ParPage] ?? "";
            if (!string.IsNullOrEmpty(p_page))
            {
                var pageNo = Convert.ToInt32(p_page);
                PageNumberLoad(pageNo);
            }
        }

        protected void OnLookupClick(object sender, EventArgs e)
        {
            PageNumberLoad(1);
        }
        private void PageNumberLoad(int pageNo)
        {
            var filterModel = GetFilterFromPage();
            //if (!filterModel.Valid) return;
            filterModel.PageNo = pageNo;

            int pageCount = 0;
            int rowsCount = 0;
            var data = QueryUtils.GetRemoteSessions(filterModel, out pageCount, out rowsCount, this);
            GridView1.DataSource = data;
            GridView1.DataBind();
            if (pageCount > 0)
            {
                var myPages = PaginatorUtils.GenerateSessionPages(pageCount, pageNo, filterModel);
                var htmlPages = "";
                foreach (var page in myPages)
                {
                    htmlPages += page.PageUrl;
                }
                pagesTop.InnerHtml = htmlPages;
                pagesBotton.InnerHtml = htmlPages;
                var format = "Showing {0} to {1} of {2} entries";
                countLabel.Text = string.Format(format,
                    (pageNo - 1) * PaginatorUtils.SessionCountOnPage + 1,
                    pageNo * PaginatorUtils.SessionCountOnPage, rowsCount);
            }
            else
            {
                pagesTop.InnerHtml = "";
                pagesBotton.InnerHtml = "";
                countLabel.Text = "";
            }
        }

        #region Filter Panel
        private StatsFilterModel GetFilterFromPage()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime)dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.UserId = UserList.SelectedValue;
            return filterModel;
        }
        private void SetFilterFromRequest()
        {
            //-- DateFrom, DateTo
            calFrom.Text = Request.Params[StatsFilterModel.ParDateFrom] ?? "";
            calTo.Text = Request.Params[StatsFilterModel.ParDateTo] ?? "";

            //-- UserId
            UserList.SelectedValue = Request.Params[StatsFilterModel.ParUserId] ?? "";
        }
        #endregion
    }
}