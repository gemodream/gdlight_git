﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using OfficeOpenXml;

namespace Corpt
{
    public partial class Reassembly : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Reassembly";
            if(!IsPostBack)
            {
				/* Security to prevent user accessing directly via url after logging in */
				string err = "";
				bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgReassembly, this, out err);
				if (!access || err != "")
				{
					Response.Redirect("Middle.aspx");
				}
                // */
                CopyButton.Attributes.Add("onclick", "return confirm('Are you sure you want to copy?');");
                //alex <asp:Button runat="server" ID="CopyButton" Text="Copy" OnClick="OnCopyClick" CssClass="btn btn-info" />
                //string strconfirm = "<script>if(!window.confirm('Are you sure?')){window.location.href='Reassembly.aspx'}</script>";
                CopyButton.Enabled = false;
                CopyDiff.Enabled = false;
                Session["SrcItemParts"] = null;
                Session["TargetItemParts"] = null;
                MultiTargetBox.Enabled = false;
                lstMovedItems.Visible = false;
                BadItemsLbl.Visible = false;
                BadItemsLbl.Text = "Bad Items";
                ShowItemizing(false);
            }
        }
        // alex
        public void OnConfirm(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
                OnCopyDiff();
            else
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
        }

        #region Copy button

        protected void OnCopyDiff()
        {
            var srcModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            var trgModel = GetViewState(SessionConstants.ReassemblyTargetItem) as ItemCopyModel;
            if (srcModel == null || trgModel == null) return;
            var srcPartType = "";
            var trgPartType = "";
            if (SrcTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(SrcTreeView.SelectedNode.Value);
                srcModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = srcModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) srcPartType = partModel.PartTypeId;
            }
            if (TargetTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(TargetTreeView.SelectedNode.Value);
                trgModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = trgModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) trgPartType = partModel.PartTypeId;
            }
            var msg = "";
            if (string.IsNullOrEmpty(srcPartType))
            {
                msg = "Select Source Item Part!";
            }
            else if (string.IsNullOrEmpty(trgPartType))
            {
                msg = "Select Target Item Part!";
            }
            if (!string.IsNullOrEmpty(msg))
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;

            }
            msg = QueryUtils.CopyDiffItemPart(srcModel, trgModel, Convert.ToInt32(srcPartType), Convert.ToInt32(trgPartType), this);
            if (string.IsNullOrEmpty(msg))
            {
                msg = "Copy done.";
                CopyButton.Enabled = false;
                CopyDiff.Enabled = false;
            }
            System.Web.HttpContext.Current.Response.Write(
               "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
        }//OnCopyDiff
        
        protected void OnCopyClick(object sender, EventArgs e)
        {
            var srcModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            var trgModel = GetViewState(SessionConstants.ReassemblyTargetItem) as ItemCopyModel;
            if (srcModel == null || trgModel == null) return;
            var srcPartType = "";
            var trgPartType = "";
            if (SrcTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(SrcTreeView.SelectedNode.Value);
                srcModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = srcModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) srcPartType = partModel.PartTypeId;
            }  
            if(TargetTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(TargetTreeView.SelectedNode.Value);
                trgModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = trgModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) trgPartType = partModel.PartTypeId;
            }
            var msg = "";
            if (string.IsNullOrEmpty(srcPartType))
            {
                msg = "Select Source Item Part!";
            }
            else if (string.IsNullOrEmpty(trgPartType))
            {
                msg = "Select Target Item Part!";
            }
            else if (srcPartType != trgPartType)
            {
                msg = "Different part types. Copying can't be done.";
            }
            if (!string.IsNullOrEmpty(msg) )
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
                
            }
            msg = QueryUtils.CopyItemPart(srcModel, trgModel, this);
            if (string.IsNullOrEmpty(msg))
            {
                msg = "Copy done.";
                RemoveOldCert(srcModel, trgModel);
                
            }
            System.Web.HttpContext.Current.Response.Write(
               "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
        }
        
        protected void RemoveOldCert(ItemCopyModel srcModel, ItemCopyModel trgModel)
        {
            var srcSelNode = SrcTreeView.SelectedNode;
            if (srcSelNode == null) return;
            var srcPartId = srcSelNode.Value;
            var srcItemModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            if (srcItemModel == null) return;
            //var srcMeasures = itemModel.Measures.FindAll(m => m.PartId == srcPartId);
            var srcVVModel = srcItemModel.Measures.Find(m => m.MeasureCode == "92");
            string srcVV = srcVVModel.ResultValue;
            
            DataTable dataInputs = new DataTable("ExtLabel");
            dataInputs.Columns.Add("ItemNumber", typeof(String));
            dataInputs.Columns.Add("VV", typeof(String));
            DataRow row;
            row = dataInputs.NewRow();
            row[0] = srcItemModel.OldItemNumber;
            row[1] = srcVV;
            dataInputs.Rows.Add(row);
            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool sent = QueryUtils.SendMessageToStorage(ms, this, "MoveOldCerts");

            }
            dataInputs.Clear();
            
        }
        #endregion

        #region Source Item
        protected void OnSourceLoadClick(object sender, ImageClickEventArgs e)
        {
            Session["SrcItemParts"] = null;
            ResetSourceData();
            var itemNumber = SrcItemField.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ResetSourceData();
                return;
            }
			var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
			if (myResult.Trim() != "")
			{
				SrcItemField.Text = myResult;
				itemNumber = myResult;
			}
            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetSourceData();
                var msg = "Please enter a Source Item in the format: 10 or 11 numeric characters";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            var sourceModel = QueryUtils.GetItemByCode(SrcItemField.Text.Trim(), this);
            if (sourceModel == null)
            {
                ResetSourceData();
                var msg = "Source Item # " + itemNumber + " does not exist!";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                if (MultiFileUploader.HasFiles)
                    ErrMultiLbl.Text = "Alert: " + msg;
                return;

            }
            SetViewState(sourceModel, SessionConstants.ReassemblySourceItem);
            var itemName = itemNumber != sourceModel.OldItemNumber
                               ? itemNumber + "/" + sourceModel.OldItemNumber
                               : itemNumber;
            SrcItemName.Text = itemName;
            SrcCustomer.Text = sourceModel.CustomerName;
            
            var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
            sourceModel.ItemParts = itemParts;
            var treeRoot = GetRootTreeModel(itemParts);
            
            SrcTreeView.Nodes.Clear();
            var rootNode = new TreeNode(treeRoot.PartName, "" + treeRoot.PartId);
            FillNode(rootNode, treeRoot);
            SrcTreeView.Nodes.Add(rootNode);
            Session["SrcItemParts"] = itemParts;
        }
        protected void SrcNodeChanged(object sender, EventArgs e)
        {
            var selNode = SrcTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var itemModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            if (itemModel == null) return;
            var measures = itemModel.Measures.FindAll(m => m.PartId == partId);
            SrcGrid.DataSource = measures;
            SrcGrid.DataBind();
            if (MultiSourceBox.Text != "")//multi merge
            {
                ResetSrcMultiData(partId, itemModel.ItemTypeId.ToString(), selNode.Text);
                MultiMergeBtn.Enabled = true;
            }
            if (Session["BulkData"] != null)//if bulk load - update datagrid with new partid/itemtypeid
            {
                string partTypeId = "";
                var itemParts = QueryUtils.GetMeasureParts(itemModel.ItemTypeId, this);
                foreach (var part in itemParts)
                {
                    if (part.PartId == Convert.ToInt16(partId))
                    {
                        partTypeId = part.PartTypeId;
                        break;
                    }
                }
                HdnPartName.Value = selNode.Text;
                HdnPartId.Value = partId;
                HdnPartTypeId.Value = partTypeId;
                ResetBulkData(partId, partTypeId, itemModel.ItemTypeId, selNode.Text);
                ReverseBtn.Enabled = true;
                //ItemizeBtn.Enabled = true;
            }
        }
        
        private void ResetSourceData()
        {
            SrcCustomer.Text = "";
            SrcItemName.Text = "";
            SrcTreeView.Nodes.Clear();
            SrcGrid.DataSource = null;
            SrcGrid.DataBind();
        }
        #endregion

        #region Target Item
        protected void OnTargetLoadClick(object sender, ImageClickEventArgs e)
        {
            Session["TargetItemParts"] = null;
            ResetTargetData();
            var itemNumber = TargetItemField.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ResetTargetData();
                return;
            }
			var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
			if (myResult.Trim() != "")
			{
				TargetItemField.Text = myResult;
				itemNumber = myResult;
			}

            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetTargetData();
                const string msg = "Please enter a Target Item in the format: 10 or 11 numeric characters";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            var targetModel = QueryUtils.GetItemByCode(TargetItemField.Text.Trim(), this);
            if (targetModel == null)
            {
                ResetTargetData();
                var msg = "Target Item # " + itemNumber + " does not exist!";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;

            }
            SetViewState(targetModel, SessionConstants.ReassemblyTargetItem);

            var itemName = itemNumber != targetModel.OldItemNumber
                               ? itemNumber + "/" + targetModel.OldItemNumber
                               : itemNumber;
            TargetItemName.Text = itemName;

            TargetCustomer.Text = targetModel.CustomerName;

            var itemParts = QueryUtils.GetMeasureParts(targetModel.ItemTypeId, this);
            targetModel.ItemParts = itemParts;
            var treeRoot = GetRootTreeModel(itemParts);

            TargetTreeView.Nodes.Clear();
            var rootNode = new TreeNode(treeRoot.PartName, "" + treeRoot.PartId);
            FillNode(rootNode, treeRoot);
            TargetTreeView.Nodes.Add(rootNode);
            Session["TargetItemParts"] = itemParts;

        }
        protected void TargetNodeChanged(object sender, EventArgs e)
        {
            List<MeasurePartModel> srcItemParts = (List<MeasurePartModel>)Session["SrcItemParts"];
            List<MeasurePartModel> targetItemParts = (List<MeasurePartModel>)Session["TargetItemParts"];
            var srcSelNode = SrcTreeView.SelectedNode;
            if (srcSelNode == null)
                return;
            var srcPartId = srcSelNode.Value;
            var selNode = TargetTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            string srcPartTypeId = null, targetPartTypeId = null, srcPartName = null, targetPartName = null;
            foreach (var srcItem in srcItemParts)
            {
                if (srcItem.PartId == Convert.ToInt32(srcPartId)) 
                {
                    srcPartTypeId = srcItem.PartTypeId;
                    srcPartName = srcItem.PartName;
                    break;
                }
            }
            if (srcPartTypeId == null)
                return;
            foreach (var targetItem in targetItemParts)
            {
                if (targetItem.PartId == Convert.ToInt32(partId)) 
                {
                    targetPartTypeId = targetItem.PartTypeId;
                    targetPartName = targetItem.PartName;
                    break;
                }
            }
            if (targetPartTypeId == null)
                return;
            if (srcPartTypeId == targetPartTypeId)
            {
                CopyDiff.Enabled = false;
                CopyButton.Enabled = true;
            }
            else
            {
                CopyButton.Enabled = false;
                CopyDiff.Enabled = true;
                string msg = "You are trying to copy " + srcPartName + " to " + targetPartName;
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
            }
            var itemModel = GetViewState(SessionConstants.ReassemblyTargetItem) as ItemCopyModel;
            if (itemModel == null) return;
            var measures = itemModel.Measures.FindAll(m => m.PartId == partId);
            TargetGrid.DataSource = measures;
            TargetGrid.DataBind();
            if (MultiTargetBox.Text != "")//multi merge
            {
                ResetSrcMultiData(partId, srcPartTypeId,  srcPartName);
            }
        }

        private void ResetTargetData()
        {
            TargetCustomer.Text = "";
            TargetItemName.Text = "";
            TargetTreeView.Nodes.Clear();
            TargetGrid.DataSource = null;
            TargetGrid.DataBind();

        }
        #endregion

        #region Tree builder
        private static void FillNode(TreeNode node, ItemPartTreeModel root)
        {
            if (root.Childs.Count <= 0) return;
            foreach (var child in root.Childs)
            {
                var addNode = new TreeNode(child.PartName, "" + child.PartId);
                FillNode(addNode, child);
                node.ChildNodes.Add(addNode);
                node.Expanded = true;
            }
        }

        private static ItemPartTreeModel GetRootTreeModel(List<MeasurePartModel> parts)
        {
            var root = parts.Find(m => !m.HasParent);
            var rootNode = root == null ? null : new ItemPartTreeModel(root);
            if (rootNode != null)
            {
                GetTreeChild(parts, rootNode);
            }
            return rootNode;
        }
        private static void GetTreeChild(List<MeasurePartModel> parts, ItemPartTreeModel root)
        {
            var childs = parts.FindAll(m => m.ParentPartId == root.PartId);
            foreach (var subItem in childs.Select(child => new ItemPartTreeModel(child)))
            {
                subItem.Parent = root;
                root.Childs.Add(subItem);
                GetTreeChild(parts, subItem);
            }
        }
        #endregion

        #region Clear Button
        protected void OnClearClick(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            SrcItemField.Text = "";
            ResetSourceData();
            TargetItemField.Text = "";
            ResetTargetData();
            CopyButton.Enabled = false;
            CopyDiff.Enabled = false;
            Session["SrcItemParts"] = null;
            Session["TargetItemParts"] = null;

        }
        #endregion

        protected void RulesGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void OnRuleResetBtnClick(object sender, EventArgs e)
        {
            var grItem = ((LinkButton)sender).NamingContainer as DataGridItem;
            var addItems = new List<ReassemblyModel>();
            if (RulesGrid.Items.Count > 0)
            {
                for (int i = 0; i <= RulesGrid.Items.Count - 1; i++)
                {
                    if (i == grItem.ItemIndex)
                        continue;
                    string sourceItem = RulesGrid.Items[i].Cells[1].Text.ToString();
                    string sourcePart = RulesGrid.Items[i].Cells[2].Text.ToString();
                    string sourcePartId = RulesGrid.Items[i].Cells[3].Text.ToString();
                    string targetItem = RulesGrid.Items[i].Cells[4].Text.ToString();
                    string targetPart = RulesGrid.Items[i].Cells[5].Text.ToString();
                    string targetPartId = RulesGrid.Items[i].Cells[6].Text.ToString();
                    addItems.Add(new ReassemblyModel(sourceItem, sourcePart, sourcePartId, targetItem, targetPart, targetPartId));
                }
            }
            //else
            //{
            //    ErrMultiLbl.Text = "No items to add.";
            //}
            RulesGrid.DataSource = addItems;
            RulesGrid.DataBind();
            //xx
            //var grItem = ((LinkButton)sender).NamingContainer as DataGridItem;
            //var uniqueKey = "" + RulesGrid.DataKeys[grItem.ItemIndex];
            //var dataRule = GetDataFromRuleGrid();
            //var item = dataRule.Find(m => m.UniqueKey == uniqueKey);
            //if (item != null)
            //{
            //    dataRule.Remove(item);
            //    RulesGrid.DataSource = dataRule;
            //    RulesGrid.DataBind();
            //}
        }
        
        protected void OnLoadMultiTargetClick(object sender, ImageClickEventArgs e)
        {
            SetViewState(null, "MultiTargetItemPartName");
            ErrMultiLbl.Text = "";
            
            var itemNumber = MultiTargetBox.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ErrMultiLbl.Text = "Enter Source number";
                return;
            }
            bool duplicate = MultiFindDuplicates(itemNumber);
            if (duplicate)
            {
                ErrMultiLbl.Text = "Duplicate number";
                return;
            }
            var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
            if (myResult.Trim() != "")
            {
                MultiTargetBox.Text = myResult;
                itemNumber = myResult;
            }
            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetSourceData();
                var msg = "Please enter a Source Item in the format: 10 or 11 numeric characters";
                ErrMultiLbl.Text = msg;
                return;
            }
            var sourceModel = QueryUtils.GetItemByCode(itemNumber, this);
            if (sourceModel == null)
            {
                var msg = "Source Item # " + itemNumber + " does not exist!";
                ErrMultiLbl.Text = msg;
                return;

            }
            //SetViewState(sourceModel, SessionConstants.ReassemblyTargetItem);
            var itemName = itemNumber != sourceModel.OldItemNumber
                               ? itemNumber + "/" + sourceModel.OldItemNumber
                               : itemNumber;
            TargetItemName.Text = itemName;
            TargetCustomer.Text = sourceModel.CustomerName;

            var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
            sourceModel.ItemParts = itemParts;
            bool found = false;
            if (GetViewState("MultiSourceItemPartName") == null)
            {
                ErrMultiLbl.Text = "Source part is not set correctly";
                return;
            }
            string[] srcData = (GetViewState("MultiSourceItemPartName").ToString()).Split('+');
            foreach (var part in itemParts)
            {
                string srcPartiId = srcData[2];
                string srcPartTypeId = srcData[3];
                if (part.PartTypeId == srcPartTypeId)
                {
                    string partName = part.PartName;
                    string partId = part.PartId.ToString();
                    bool added = AddMultiTargetData(itemNumber, partName, partId, srcPartTypeId);
                    if (added)
                        found = true;
                    break;
                }
            }
            if (!found)
            {
                ErrMultiLbl.Text = "No part match with source.";
                TargetItemField.Text = itemNumber;
                OnTargetLoadClick(null, null);
                MultiTargetBox.Focus();
                return;
            }
            if (MultiSourceBox.Text != "")
            {
                OnAddNewMultiBtnClick(null, null);
            }
            TargetItemField.Text = itemNumber;
            OnTargetLoadClick(null, null);
            MultiSourceBox.Focus();
        }

        protected void OnLoadMultiSourceClick(object sender, ImageClickEventArgs e)
        {
            SetViewState(null, "MultiSourceItemPartName");
            ErrMultiLbl.Text = "";
            var itemNumber = MultiSourceBox.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ErrMultiLbl.Text = "Enter Source number";
                return;
            }
            bool duplicate = MultiFindDuplicates(itemNumber);
            if (duplicate)
            {
                ErrMultiLbl.Text = "Duplicate number";
                return;
            }
            var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
            if (myResult.Trim() != "")
            {
                MultiSourceBox.Text = myResult;
                itemNumber = myResult;
            }
            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetSourceData();
                var msg = "Please enter a Source Item in the format: 10 or 11 numeric characters";
                ErrMultiLbl.Text = msg;
                return;
            }
            var sourceModel = QueryUtils.GetItemByCode(MultiSourceBox.Text.Trim(), this);
            if (sourceModel == null)
            {
                var msg = "Source Item # " + itemNumber + " does not exist!";
                ErrMultiLbl.Text = msg;
                return;

            }
            //SetViewState(sourceModel, SessionConstants.ReassemblySourceItem);
            var itemName = itemNumber != sourceModel.OldItemNumber
                               ? itemNumber + "/" + sourceModel.OldItemNumber
                               : itemNumber;
            SrcItemName.Text = itemName;
            SrcCustomer.Text = sourceModel.CustomerName;
            string savedPartTypeId = (SavedPartNameBox.Text != "") ? HdnPartTypeId.Value : "1";
            var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
            sourceModel.ItemParts = itemParts;
            bool found = false;
            foreach (var part in itemParts)
            {
                if (part.PartTypeId == savedPartTypeId)
                {
                    string partName = part.PartName;
                    string partId = part.PartId.ToString();
                    string partTypeId = part.PartTypeId;
                    bool added = AddMultiSourceData(itemNumber, partName, partId, partTypeId);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                ErrMultiLbl.Text = "Choose the part.";
            }
            SrcItemField.Text = itemNumber;
            OnSourceLoadClick(null, null);
            if (ErrMultiLbl.Text != "" && ErrMultiLbl.Text.ToLower().Contains("alert"))
            {
                MultiSourceBox.Focus();
                OnClearClick(null, null);
            }
            MultiMergeBtn.Enabled = true;
            MultiTargetBox.Enabled = true;
            if (ItemizeOrderBox.Visible)
                ShowItemizing(false);
            MultiTargetBox.Focus();
        }
        private bool AddMultiSourceData(string itemNumber, string partName, string partId, string partTypeId)
        {
            SetViewState(itemNumber + "+" + partName +  "+" + partId + "+" + partTypeId, "MultiSourceItemPartName");
            ActivePartNameLbl.Text = partName;
            HdnPartId.Value = partId;
            HdnPartName.Value = partName;
            HdnPartTypeId.Value = partTypeId;
            return true; 
        }
        private bool AddMultiTargetData(string itemNumber, string partName, string partId, string partTypeId)
        {
            SetViewState(itemNumber + "+" + partName + "+" + partId + "+" + partTypeId, "MultiTargetItemPartName");
            return true;
        }
        protected void OnAddNewMultiBtnClick(object sender, EventArgs e)
        {
            ErrMultiLbl.Text = "";
            try
            {
                var addItems = new List<ReassemblyModel>();

                if (RulesGrid.Items.Count > 0)
                {
                    //DataTable myDataSet;
                    //myDataSet = (DataTable)RulesGrid.DataSource;
                    //foreach (DataRow dr in myDataSet.Rows)
                    //{
                    //    string sourceItem = dr["SourceItemFld"].ToString();
                    //    string sourcePart = dr["SourcePartNameFld"].ToString();
                    //    string targetItem = dr["TargetItemFld"].ToString();
                    //    string targetPart = dr["TargetPartNameFld"].ToString();
                    //    addItems.Add(new ReassemblyModel(sourceItem, sourcePart, targetItem, targetPart));
                    //}
                    for (int i=0; i<= RulesGrid.Items.Count-1; i++)
                    {
                        string sourceItem = RulesGrid.Items[i].Cells[1].Text.ToString();
                        string sourcePart = RulesGrid.Items[i].Cells[2].Text.ToString();
                        string sourcePartId = RulesGrid.Items[i].Cells[3].Text.ToString();
                        string targetItem = RulesGrid.Items[i].Cells[4].Text.ToString();
                        string targetPart = RulesGrid.Items[i].Cells[5].Text.ToString();
                        string targetPartId = RulesGrid.Items[i].Cells[6].Text.ToString();
                        addItems.Add(new ReassemblyModel(sourceItem, sourcePart, sourcePartId, targetItem, targetPart, targetPartId));
                    }
                }
                else
                {
                    ErrMultiLbl.Text = "New item added.";
                }
                string[] srcData = (GetViewState("MultiSourceItemPartName").ToString()).Split('+');
                string[] targetData = (GetViewState("MultiTargetItemPartName").ToString()).Split('+');
                addItems.Add(new ReassemblyModel(srcData, targetData));
                RulesGrid.DataSource = addItems;
                RulesGrid.DataBind();
                MultiSourceBox.Text = "";
                MultiTargetBox.Text = "";
                if (SavedPartNameBox.Text == "")
                {
                    HdnPartId.Value = "";
                    HdnPartName.Value = "";
                    HdnPartTypeId.Value = "";
                    ActivePartNameLbl.Text = "";
                }
                MultiTargetBox.Enabled = false;
                MultiSourceBox.Focus();
            }
            catch (Exception ex)
            {
                ErrMultiLbl.Text = "Error adding items to the list";
            }
        }

        protected void OnRuleItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }

        protected void OnMultiMergeBtnClick(object sender, EventArgs e)
        {
            ErrMultiLbl.Text = "";
            if (RulesGrid.Items.Count == 0)
            {
                ErrMultiLbl.Text = "No items to merge.";
                return;
            }
            for (int i = 0; i <= RulesGrid.Items.Count - 1; i++)
            {
                var srcItemNumber = RulesGrid.Items[i].Cells[1].Text;
                var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(srcItemNumber, this.Page);
                if (myResult.Trim() != "")
                    srcItemNumber = myResult;
                var sourceModel = QueryUtils.GetItemByCode(srcItemNumber, this);
                sourceModel.CopyPartId = Convert.ToInt16(RulesGrid.Items[i].Cells[3].Text);
                var targetItemNumber = RulesGrid.Items[i].Cells[4].Text;
                myResult = Utilities.QueryUtils.GetItemNumberBy7digit(targetItemNumber, this.Page);
                if (myResult.Trim() != "")
                    srcItemNumber = myResult;
                var targetModel = QueryUtils.GetItemByCode(targetItemNumber, this);
                targetModel.CopyPartId = Convert.ToInt16(RulesGrid.Items[i].Cells[6].Text);
                string msg = QueryUtils.CopyItemPart(sourceModel, targetModel, this);
            }
            ErrMultiLbl.Text = "Finished to merge.";
            MultiTargetBox.Enabled = false;
            MultiSourceBox.Text = "";
            MultiTargetBox.Text = "";
            HdnPartTypeId.Value = "";
            HdnPartName.Value = "";
            HdnPartId.Value = "";
            ItemizeBtn.Enabled = true;
        }

        protected void OnClearMultiMergeClick(object sender, EventArgs e)
        {
            RulesGrid.DataSource = null;
            RulesGrid.DataBind();
            MultiSourceBox.Text = "";
            MultiTargetBox.Text = "";
            MultiTargetBox.Enabled = false;
            MultiMergeBtn.Enabled = false;
            ItemizeBtn.Enabled = false;
            HdnPartId.Value = "";
            HdnPartName.Value = "";
            HdnPartTypeId.Value = "";
            ActivePartNameLbl.Text = "";
            ErrMultiLbl.Text = "";
            SetViewState(null, "MultiSourceItemPartName");
            SetViewState(null, "MultiTargetItemPartName");
            BadItemsLbl.Visible = false;
            BadItemsLbl.Text = "Bad Items";
            SavedPartNameBox.Text = "";
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = false;
            ShowItemizing(false);
            MultiSourceBox.Focus();
        }
        protected void ResetSrcMultiData(string partId, string newPartTypeId, string newPartName)
        {
            ErrMultiLbl.Text = "";
            if (GetViewState("MultiSourceItemPartName") != null)
            {
                string[] srcData = (GetViewState("MultiSourceItemPartName").ToString()).Split('+');
                var sourceModel = QueryUtils.GetItemByCode(srcData[0], this);
                int itemTypeId = sourceModel.ItemTypeId;
                var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
                foreach (var part in itemParts)
                {
                    if (part.PartId == Convert.ToInt16(partId))
                    {
                        string partName = part.PartName;
                        string partTypeId = part.PartTypeId;
                        bool added = AddMultiSourceData(MultiSourceBox.Text, partName, partId, partTypeId);
                        HdnPartName.Value = partName;
                        HdnPartId.Value = partId;
                        HdnPartTypeId.Value = partTypeId;
                        break;
                    }
                }
            }
            else
            {
                bool added = AddMultiSourceData(MultiSourceBox.Text, newPartName, partId, newPartTypeId);
                HdnPartName.Value = newPartName;
                HdnPartId.Value = partId;
                HdnPartTypeId.Value = newPartTypeId;
            }
            MultiTargetBox.Enabled = true;
            
        }
        protected void ResetTurgetMultiData(string partId)
        {
            ErrMultiLbl.Text = "";
            string[] srcData = (GetViewState("MultiTargetItemPartName").ToString()).Split('+');
            var sourceModel = QueryUtils.GetItemByCode(srcData[0], this);
            int itemTypeId = sourceModel.ItemTypeId;
            var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
            foreach (var part in itemParts)
            {
                if (part.PartId == Convert.ToInt16(partId))
                {
                    string partName = part.PartName;
                    string partTypeId = part.PartTypeId;
                    bool added = AddMultiTargetData(srcData[0], partName, partId, partTypeId);
                    break;
                }
            }
        }
        protected void ResetBulkData(string partId, string newPartTypeId, int origItemTypeId, string newPartName)
        {
            if (Session["BulkData"] == null)
            {
                ErrMultiLbl.Text = "No Bulk Data";
                return;
            }
            DataTable bulkDt = (DataTable)Session["BulkData"];
            foreach (DataRow bulkDr in bulkDt.Rows)
            {
                string srcData = bulkDr["SourceItemFld"].ToString();
                string targetData = bulkDr["TargetItemFld"].ToString();
                DataTable validateDt = QueryUtils.ReassemblyValidateItem(srcData, this.Page);
                if (validateDt == null)
                {
                    ErrMultiLbl.Text = "Error loading source";
                    return;
                }
                int itemTypeId = Convert.ToInt16(validateDt.Rows[0]["ItemTypeId"].ToString());
                //var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(srcData, this.Page);
                //if (myResult.Trim() != "")
                //    srcData = myResult;
                 
                //var sourceModel = QueryUtils.GetItemByCode(srcData, this);
                //int itemTypeId = sourceModel.ItemTypeId;
                if (itemTypeId != origItemTypeId)
                {
                    var itemParts = QueryUtils.GetMeasureParts(itemTypeId, this);
                    foreach (var part in itemParts)
                    {
                        if (part.PartTypeId == newPartTypeId)
                        {
                            bulkDr["SourcePartNameFld"] = part.PartName;
                            bulkDr["SourcePartIdFld"] = part.PartId;
                            break;
                        }
                    }
                }
                else
                {
                    bulkDr["SourcePartNameFld"] = newPartName;
                    bulkDr["SourcePartIdFld"] = partId;
                }
                //myResult = Utilities.QueryUtils.GetItemNumberBy7digit(targetData, this.Page);
                //if (myResult.Trim() != "")
                //    targetData = myResult;
                //var targetModel = QueryUtils.GetItemByCode(targetData, this);
                validateDt = QueryUtils.ReassemblyValidateItem(targetData, this.Page);
                if (validateDt == null)
                {
                    ErrMultiLbl.Text = "Error loading source";
                    return;
                }
                itemTypeId = Convert.ToInt16(validateDt.Rows[0]["ItemTypeId"].ToString());
                if (itemTypeId != origItemTypeId)
                {
                    var itemParts = QueryUtils.GetMeasureParts(itemTypeId, this);
                    foreach (var part in itemParts)
                    {
                        if (part.PartTypeId == newPartTypeId)
                        {
                            bulkDr["TargetPartNameFld"] = part.PartName;
                            bulkDr["TargetPartIdFld"] = part.PartId;
                            break;
                        }
                    }
                }
                else
                {
                    bulkDr["TargetPartNameFld"] = newPartName;
                    bulkDr["TargetPartIdFld"] = partId;
                }
            }
            MultiMergeBtn.Enabled = true;
            RulesGrid.DataSource = bulkDt.DefaultView;
            RulesGrid.DataBind();
        }
        protected bool MultiFindDuplicates(string itemNumber)
        {
            if (MultiSourceBox.Text.Trim() == MultiTargetBox.Text.Trim())
                return true;
            if (RulesGrid != null && RulesGrid.Items.Count > 0)
            {
                for (int i = 0; i <= RulesGrid.Items.Count - 1; i++)
                {
                    string sourceItem = RulesGrid.Items[i].Cells[1].Text.ToString();
                    string targetItem = RulesGrid.Items[i].Cells[4].Text.ToString();
                    if (itemNumber == sourceItem || itemNumber == targetItem)
                        return true;
                }
            }
            return false;
        }

        protected void SavePartNameBtn_Click(object sender, EventArgs e)
        {
            if (HdnPartName.Value != "")
                SavedPartNameBox.Text = HdnPartName.Value;
        }

        protected void btnSaveBulkFiles_Click(object sender, EventArgs e)
        {
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = false;
            BadItemsLbl.Visible = false;
            if (!MultiFileUploader.HasFiles)
            {
                ErrMultiLbl.Text = "Please select files to upload";
                return;
            }
            BadItemsLbl.Visible = true;
            lstMovedItems.Visible = true;
            lstMovedItems.Enabled = true;
            foreach (var file in MultiFileUploader.PostedFiles)
            {
                if (file.FileName.ToLower().Contains(@".xls"))
                {
                    try
                    {
                        var error = UploadDataFromExcel(file);
                        ErrMultiLbl.Text = error;
                        return;

                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        ErrMultiLbl.Text = "Files uploading failed.";
                        return;
                    }
                }
            }
            ShowItemizing(false);
            MultiMergeBtn.Enabled = false;
            ItemizeBtn.Enabled = true;
        }
        protected string UploadDataFromExcel(HttpPostedFile file)
        {
            try
            {
                int fileLen = file.ContentLength;
                byte[] input = new byte[fileLen - 1];
                string fileName = "";
                bool showError = false;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    input = binaryReader.ReadBytes(file.ContentLength);
                    fileName = file.FileName;
                    fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.:/]+", "", RegexOptions.Compiled);
                    DataTable dt = new DataTable();
                    DataTable newDt = new DataTable();
                    if (fileName.ToLower().Contains(@".xls"))
                    {
                        List<string> badItems = new List<string>();
                        using (ExcelPackage excelPackage = new ExcelPackage(file.InputStream))
                        {
                            dt = ExcelPackageToDataTable(excelPackage);
                        }
                        newDt = ValidateItems(dt);
                        //newDt = dt.Clone();
                        //validate items
                        //int rowCount = dt.Rows.Count;
                        //    int iterations = rowCount / 25;
                        //    int lastIter = rowCount % 25;
                        //    int k = 0;
                        //    int last = 25;
                        //    if (lastIter > 0)
                        //    {
                        //        iterations++;
                        //    }
                        //    int maxIter = iterations;
                        //    for (int j = 0; j <= iterations - 1; j++)
                        //    {
                        //        if (j != iterations - 1)
                        //        {
                        //            k = 25 * j;
                        //            if (j == maxIter-1)
                        //                last += lastIter;
                        //            else
                        //                last = 25 * (j + 1);
                        //            for (int i = k; i <= last - 1; i++)
                        //            {
                        //                DataRow dr = dt.Rows[i];
                        //                //DataRow workRow = newDt.NewRow();
                        //                string srcItem = dr["SourceItemFld"].ToString();
                        //                string targetItem = dr["TargetItemFld"].ToString();
                        //                var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(srcItem, this.Page);
                        //                if (myResult.Trim() == "")
                        //                {
                        //                    lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //                    continue;
                        //                }
                        //                var itemtModel = QueryUtils.GetItemByCode(srcItem, this);
                        //                if (itemtModel == null)
                        //                {
                        //                    lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //                    continue;
                        //                }
                        //                myResult = Utilities.QueryUtils.GetItemNumberBy7digit(targetItem, this.Page);
                        //                if (myResult.Trim() == "")
                        //                {
                        //                    lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //                    continue;
                        //                }
                        //                itemtModel = QueryUtils.GetItemByCode(targetItem, this);
                        //                if (itemtModel == null)
                        //                {
                        //                    lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //                    continue;
                        //                }
                        //                //workRow["SourceItemFld"] = srcItem;
                        //                //workRow["TargetItemFld"] = targetItem;
                        //                //newDt.Rows.Add(workRow);
                        //            }
                        //        }

                        //    }
                        //    for (int i=0; i<= dt.Rows.Count-1; i++)
                        //    {
                        //        DataRow dr = dt.Rows[i];
                        //        //DataRow workRow = newDt.NewRow();
                        //        string srcItem = dr["SourceItemFld"].ToString();
                        //        string targetItem = dr["TargetItemFld"].ToString();
                        //        var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(srcItem, this.Page);
                        //        if (myResult.Trim() == "")
                        //        {
                        //            lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //            continue;
                        //        }
                        //        var itemtModel = QueryUtils.GetItemByCode(srcItem, this);
                        //        if (itemtModel == null)
                        //        {
                        //            lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //            continue;
                        //        }
                        //        myResult = Utilities.QueryUtils.GetItemNumberBy7digit(targetItem, this.Page);
                        //        if (myResult.Trim() == "")
                        //        {
                        //            lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //            continue;
                        //        }
                        //        itemtModel = QueryUtils.GetItemByCode(targetItem, this);
                        //        if (itemtModel == null)
                        //        {
                        //            lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        //            continue;
                        //        }
                        //        //workRow["SourceItemFld"] = srcItem;
                        //        //workRow["TargetItemFld"] = targetItem;
                        //        //newDt.Rows.Add(workRow);
                        //    }
                        
                        if (!String.IsNullOrEmpty(ErrMultiLbl.Text))
                        {
                            showError = true;
                        }
                    }
                    if (newDt.Rows.Count == 0)
                        ErrMultiLbl.Text = "Alert: no items found";
                    else
                    {
                        string srcNumber = newDt.Rows[0]["SourceItemFld"].ToString();
                        SrcItemField.Text = srcNumber;
                        OnSourceLoadClick(null, null);
                        string targetNumber = newDt.Rows[0]["TargetItemFld"].ToString();
                        TargetItemField.Text = targetNumber;
                        OnTargetLoadClick(null, null);
                        Session["BulkData"] = newDt;
                        if (lstMovedItems.Items.Count == 0)
                            BadItemsLbl.Visible = false;
                        RulesGrid.DataSource = newDt.DefaultView;
                        RulesGrid.DataBind();
                    }
                    if (showError)
                    {
                        return "Error";
                    }
                    if (ErrMultiLbl.Text.ToLower().Contains("alert"))
                    {
                        RulesGrid.DataSource = null;
                        RulesGrid.DataBind();
                        return ErrMultiLbl.Text;
                    }
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "Failed";
            }
        }
        private DataTable ValidateItems(DataTable dt)
        {
            DataTable newDt = dt.Clone();
            //validate items
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string srcItem = dr["SourceItemFld"].ToString();
                    string targetItem = dr["TargetItemFld"].ToString();
                    DataTable validateDt = QueryUtils.ReassemblyValidateItem(srcItem, this.Page);
                    if (validateDt == null)
                    {
                        lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        continue;
                    }
                    validateDt = QueryUtils.ReassemblyValidateItem(targetItem, this.Page);
                    if (validateDt == null)
                    {
                        lstMovedItems.Items.Add(srcItem + "---" + targetItem);
                        continue;
                    }
                    newDt.ImportRow(dr);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return newDt;
        }
        public static DataTable ExcelPackageToDataTable(ExcelPackage excelPackage)
        {
            DataTable dt = new DataTable();
            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1];
            
            //check if the worksheet is completely empty
            if (worksheet.Dimension == null)
            {
                return dt;
            }
            dt.Columns.Add("SourceItemFld");
            dt.Columns.Add("SourcePartNameFld");
            dt.Columns.Add("SourcePartIdFld");
            dt.Columns.Add("TargetItemFld");
            dt.Columns.Add("TargetPartNameFld");
            dt.Columns.Add("TargetPartIdFld");
            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;

            //start adding the contents of the excel file to the datatable
            for (int row = start.Row+1; row <= end.Row; row++)
            { // Row by row...
                DataRow newRow = dt.NewRow();
                int col = 2;
                object cellValue = worksheet.Cells[row, col].Text;
                string value = cellValue.ToString();
                newRow["SourceItemFld"] = value;
                col = 5;
                cellValue = worksheet.Cells[row, col].Text;
                value = cellValue.ToString();
                newRow["TargetItemFld"] = value;
                //for (int col = start.Column; col <= end.Column; col++)
                //{ // ... Cell by cell...
                //    object cellValue = worksheet.Cells[row, col].Text; // This got me the actual value I needed.
                //    string value = cellValue.ToString();
                //    if (col == 2)
                //        newRow["SourceItemFld"] = value;
                //    else if (col == 5)
                //        newRow["TargetItemFld"] = value;
                //}
                newRow["SourcePartNameFld"] = "";
                newRow["SourcePartIdFld"] = "";
                newRow["TargetPartNameFld"] = "";
                newRow["TargetPartIdFld"] = "";
                dt.Rows.Add(newRow);
            }
            return dt;
            //for (int i = 1; i <= worksheet.Dimension.End.Row; i++)
            //{
            //    var row = worksheet.Cells[i, 1, i, worksheet.Dimension.End.Column];
            //    DataRow newRow = dt.NewRow();

            //    newRow["SourceItemFld"] = row[i, 0].Value.ToString();
            //    newRow["TargetItemFld"] = row[i,1].Value.ToString();
            //    newRow["SourcePartNameFld"] = "";
            //    newRow["SourcePartIdFld"] = "";
            //    newRow["TargetPartNameFld"] = "";
            //    newRow["TargetPartIdFld"] = "";

            //    //for (int i=0; i<= row.Columns-1; i++)
            //    //{
            //    //    newRow.ItemArray[0] = ro
            //    //}

            //    //loop all cells in the row
            //    //foreach (var cell in row)
            //    //{
            //    //    newRow[cell.Start.Column - 1] = cell.Text;
            //    //}


            //}


        }
        protected void ReverseBtn_OnClick(object sender, EventArgs e)
        {
            RulesGrid.DataSource = null;
            RulesGrid.DataBind();
            if (Session["BulkData"] == null)
                return;
            DataTable bulkDt = (DataTable)Session["BulkData"];
            DataTable newDt = bulkDt.Clone();
            foreach (DataRow bulkDr in bulkDt.Rows)
            {
                string srcData = bulkDr["SourceItemFld"].ToString();
                string targetData = bulkDr["TargetItemFld"].ToString();
                string srcPartName = bulkDr["SourcePartNameFld"].ToString();
                string targetPartName = bulkDr["TargetPartNameFld"].ToString();
                string srcPartId = bulkDr["SourcePartIdFld"].ToString();
                string targetPartId = bulkDr["TargetPartIdFld"].ToString();
                bulkDr["TargetItemFld"] = srcData;
                bulkDr["SourceItemFld"] = targetData;
                bulkDr["SourcePartNameFld"] = targetPartName;
                bulkDr["TargetPartNameFld"] = srcPartName;
                bulkDr["SourcePartIdFld"] = targetPartId;
                bulkDr["TargetPartIdFld"] = srcPartId;
                newDt.ImportRow(bulkDr);
            }
            Session["BulkData"] = newDt;
            RulesGrid.DataSource = newDt.DefaultView;
            RulesGrid.DataBind();
        }

        protected void OnItemizingProcessBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ItemizeOrderBox.Text.Trim()) || string.IsNullOrEmpty(ItemizeOrderBox.Text.Trim()) 
                || string.IsNullOrEmpty(ItemizeOrderBox.Text.Trim()) || string.IsNullOrEmpty(ItemizeMemoBox.Text.Trim()))
            {
                ErrMultiLbl.Text = "Missing data for itemizing";
                return;
            }
            if (RulesGrid.Items == null || RulesGrid.Items.Count == 0)
            {
                ErrMultiLbl.Text = "No items for itemizing";
                return;
            }
            List<ItemizedModel> batchList = new List<ItemizedModel>();
            ItemizedModel batch = new ItemizedModel();
            batch.Items = RulesGrid.Items.Count.ToString();
            batch.Memo = ItemizeMemoBox.Text.Trim();
            batch.Sku = ItemizeSkuBox.Text.Trim();
            batch.ItemsSet = "1";//j.ToString();
            batchList.Add(batch);
            OrderModel orderModel = QueryUtils.GetOrderInfo(ItemizeOrderBox.Text.Trim(), this);
            List<string> oldItemsList = new List<string>();
            for (int i = 0; i <= RulesGrid.Items.Count - 1; i++)
            {
                Utils.DissectItemNumber(RulesGrid.Items[i].Cells[4].Text, out string orderCode, out string batchCode, out string itemCode);
                string itemNumber = Convert.ToInt32(orderCode) + "." + Convert.ToInt16(batchCode) + "." + Convert.ToInt16(itemCode);
                oldItemsList.Add(itemNumber);
            }
            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            var memoList = QueryUtils.GetOrderMemos(orderModel, this);
            var cp = batch.Sku;
            var cpModel = cpList.Find(m => m.CustomerProgramName == cp);
            var memo = batch.Memo;
            if (string.IsNullOrEmpty(memo))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                return;
            }
            var memoModel = memoList.Find(m => m.MemoNumber == memo);
            if (memoModel == null) return;
            List<string> oldItem = new List<string>();
            var model = new ItemizeGoModel
            {
                Cp = cpModel,
                Memo = memoModel,
                NumberOfItems = Convert.ToInt16(batch.Items),
                Order = orderModel,
                LotNumbers = oldItemsList //new List<string>()
            };
            var newBatches = QueryUtils.ItemizeAddOldNewItemsByCount(model, this);
            if (newBatches != null && newBatches.Count > 0)
            {
                BadItemsLbl.Text = "New batches";
                lstMovedItems.Items.Clear();
                lstMovedItems.Visible = true;
                foreach (var newBatch in newBatches)
                {
                    lstMovedItems.Items.Add(newBatch.OrderCode + "." + newBatch.BatchCode);
                }
            }
            ItemizingProcessBtn.Enabled = false;
        }

        protected void OnItemizeBtnClick(object sender, EventArgs e)
        {
            ShowItemizing(true);
        }

        protected void ShowItemizing(bool show)
        {
            ItemizeOrderBox.Visible = show;
            ItemizeMemoBox.Visible = show;
            ItemizeSkuBox.Visible = show;
            ItemizeCountBox.Visible = show;
            ItemizeOrderLbl.Visible = show;
            ItemizeMemoLbl.Visible = show;
            ItemizeCountLbl.Visible = show;
            ItemizeSkuLbl.Visible = show;
            ItemizingProcessBtn.Visible = show;
            ItemizingProcessBtn.Enabled = show;
            ItemizeNewOrderBtn.Visible = show;
            ReverseBtn.Enabled = show;
        }
        protected void OnItemizeNewOrderBtnClick(object sender, ImageClickEventArgs e)
        {
            try
            {
                ItemizeCountBox.Text = RulesGrid.Items.Count.ToString();
                OrderModel orderModel = QueryUtils.GetOrderInfo(ItemizeOrderBox.Text.Trim(), this);
                var memoList = QueryUtils.GetOrderMemos(orderModel, this);
                ItemizeMemoBox.Text = memoList[0].MemoNumber;
                string oldItemNumber = RulesGrid.Items[0].Cells[4].Text;
                string batchNumber = oldItemNumber.Substring(0, oldItemNumber.Length - 2);
                Utils.DissectItemNumber(batchNumber, out string orderCode, out string batchCode, out string _);
                int order = Convert.ToInt32(orderCode);
                int batch = Convert.ToInt16(batchCode);
                string sku = QueryUtils.GetSkuByBatch(order, batch, this);
                ItemizeSkuBox.Text = sku;
            }
            catch (Exception ex)
            {
                ErrMultiLbl.Text = "Error getting info for itemizing";
            }
        }
    }
}