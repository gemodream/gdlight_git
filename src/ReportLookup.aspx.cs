using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for ReportLookup.
	/// </summary>
	public partial class ReportLookup : CommonPage
	{


		protected void Page_Load(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Report Lookup";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		//New report lookup routine - replace the slow one.

        #region Search By Item Number
        protected void OnSearchByItemNumberClick(object sender, ImageClickEventArgs e)
        {
            HasDirectoryRoot();
            ErrLabel1.Text = "";
            VirtualVault1.Text = "";
            if(!Regex.IsMatch(ItemNumberField.Text, @"^\d{10}$|^\d{11}$"))
            {
                ErrLabel1.Text = "Item number is incorrect.";
                VirtualVault1.Text = "";
                ItemNumberField.Focus();
                return;
                
            }
            var findModel = new ReportFindModel {ItemNumber = ItemNumberField.Text};
            findModel = QueryUtils.GetVirtualVaultByItemNumber(findModel, this);
            VirtualVault1.Text = findModel.VirtualVaultNumber;
            //FindReport(findModel);
            FindReport(findModel);
        }
        #endregion

        #region Search By Char + Item Number
        protected void OnSearchByCharItemNumberClick(object sender, ImageClickEventArgs e)
        {
//            HasDirectoryRoot();
//            ErrLabel2.Text = "";
//            VirtualVault2.Text = "";
//            if(!Regex.IsMatch(CharItemNumberField.Text.Trim(), @"^[A-Z,a-z]\d{10}$|^[A-Z,a-z]\d{11}$"))
//            {
//                ErrLabel2.Text = "Char + Item number is incorrect.";
//                CharItemNumberField.Focus();
//                return;
//            }
//            var findModel = new ReportFindModel
//            {
//                DocumentChar = CharItemNumberField.Text.Substring(0, 1).ToUpper(),
//                ItemNumber = CharItemNumberField.Text.Substring(1)
//            };
//            findModel = QueryUtils.GetVirtualVaultByItemNumber(findModel, this);
//            FindReport(findModel);

        }
        #endregion

        #region Search By GNumber (HEX)
        protected void OnSearchByGNumberClick(object sender, ImageClickEventArgs e)
        {
//            HasDirectoryRoot();
//            VirtualVault3.Text = "";
//            ItemNumber3.Text = "";
//            if (!Regex.IsMatch(GNumberField.Text.Trim(), "[a-f,A-F,0-9]+"))
//            {
//                ErrLabel3.Text = "GNumber is incorrect.";
//                return;
//            }
//            try
//            {
//                var number = int.Parse(GNumberField.Text.Trim(), System.Globalization.NumberStyles.AllowHexSpecifier);
//            } catch (Exception ex)
//            {
//                ErrLabel3.Text = "GNumber is incorrect." + ex.Message;
//                return;
//            }
//            var findModel = new ReportFindModel {HexGnumber = GNumberField.Text.Trim()};
//            findModel = QueryUtils.GetItemNumberByHexGnumber(findModel, this);
//            VirtualVault3.Text = findModel.VirtualVaultNumber;
//            ItemNumber3.Text = findModel.ItemNumber;
//            if(string.IsNullOrEmpty(findModel.ItemNumber))
//            {
//                ErrLabel3.Text = "Item number not found.";
//                return;
//            }
//            FindReport(findModel);
        }
        #endregion

        private string GetDirName()
        {
            var dirName = "" + Session[SessionConstants.GlobalDocumentRoot];
            if (dirName.EndsWith("\\") || dirName.EndsWith("/")) return dirName;
            return dirName + "/";
        }
        private bool HasDirectoryRoot()
        {
            FileGrid.DataSource = null;
            FileGrid.DataBind();
            InfoLabel.Text = "";
            
            try
            {
                var dir = new DirectoryInfo(Server.MapPath(GetDirName()));
                if (!dir.Exists)
                {
                    InfoLabel.Text = "Dir " + dir.FullName + " does not exist!";
                    return false;
                }
            } catch (Exception ex)
            {
                InfoLabel.Text = ex.Message;
                return false;
            }
            return true;

        }
        private void FindReport(ReportFindModel findModel)
        {

            if (!HasDirectoryRoot()) return;
            var filesList = new List<FileInfoModel>();
            var dirName = Server.MapPath(GetDirName());
            var dir = new DirectoryInfo(dirName);
            
            if (string.IsNullOrEmpty(findModel.DocumentChar))
            {
                //we have to find the correct char or if none found - use incorrect one to cause FileNotFound on the next stage
                string[] sDocumentType = { "H", "D", "S", "T", "C", "Q", "Z" };
                var al0 = new List<string>();
                
                //al0.Add("*.pdf");
                al0.Add("367_" + findModel.ItemNumber + " T.pdf");
                al0.Add("133_" + findModel.ItemNumber + " T.pdf");
                al0.Add(findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
                al0.Add(findModel.ItemNumber + ".pdf");

                foreach (var type in sDocumentType)
                {
                    al0.Add(type + findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
                    al0.Add(type + findModel.ItemNumber + ".pdf");
                }

                foreach (var obj in al0)
                {
                    var strTempFileName = "" + dirName + obj;
                    var fi = new FileInfo(strTempFileName);
                    if (fi.Exists)
                    {
                        filesList.Add(new FileInfoModel{FileName = fi.Name, LastWriteTimeUtc = fi.LastWriteTimeUtc, Exists = true});
                    } else
                    {
                        filesList.Add(new FileInfoModel { FileName = fi.Name, Exists = false});
                    }
                }

            }
            else
            {
                //-- Char + ItemNumber + VirtualNumber
                var files = dir.GetFiles(findModel.DocumentChar + findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
                foreach(var fi in files)
                {
                    if (!fi.Exists)
                    {
                        filesList.Add(new FileInfoModel { FileName = fi.Name, Exists = false });
                    } else
                    {
                        filesList.Add(new FileInfoModel { FileName = fi.Name, LastWriteTimeUtc = fi.LastWriteTimeUtc, Exists = true});
                    }
                }
            }

            foreach (var file in filesList.Where(file => file.Exists))
            {
                Response.Redirect(file.RedirectUrl);
                return;
            }
            FileGrid.DataSource = filesList;
            FileGrid.DataBind();
        }
    }
}
