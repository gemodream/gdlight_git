﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class ItemHistory : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Item History";
            if (IsPostBack) return;

			/* Security to prevent user accessing directly via url after logging in */
			string err = "";
			bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgItemHistory, this, out err);
			if (!access || err != "")
			{
				Response.Redirect("Middle.aspx");
			}
			// */
			//-- Load Authors
			AuthorList.DataSource = QueryCustomerUtils.GetAuthors(this);
            AuthorList.DataBind();
            CalendarTo.SelectedDate = DateTime.Now;
            DateTo.Text = String.Format("{0:m/d/yyyy}", CalendarTo.SelectedDate);
        }
        #region Load Button
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            var filter = new ItemHistoryFilterModel
            {
                DateFrom = CalendarFrom.SelectedDate,
                DateTo = CalendarTo.SelectedDate, 
            };
            if (!string.IsNullOrEmpty(AuthorList.SelectedValue))
            {
                var author = AuthorList.SelectedValue.Split('_');
                filter.AuthorId = author[0];
                filter.AuthorOfficeId = author[1];
            }
            var number = ItemField.Text.Trim();
            if (number.Length >= 5)
            {
                filter.Order = ""+Utils.ParseOrderCode(number);
            }
            if (number.Length >= 8)
            {
                filter.Batch = "" + Utils.ParseBatchCode(number);
            }
            if (number.Length >= 10)
            {
                filter.Item = "" + Utils.ParseItemCode(number);
            }

            HistoryGrid.DataSource = QueryUtils.GetItemHistory(filter, this);
            HistoryGrid.DataBind();
        }
        #endregion

        #region Date From, Date To
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (DateFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(DateFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarFrom.SelectedDate = date;

        }
        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (DateTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(DateTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarTo.SelectedDate = date;

        }
        #endregion

        protected void sort_table(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            List<ItemHistoryModel> lstScreening = new List<ItemHistoryModel>();
            var sortField = e.SortExpression;
            DataSet dsScr = (DataSet)Session["HistoryData"];
            string direction = null;
            if (System.Web.HttpContext.Current.Session["sortExpression"] != null)
            {
                string[] sortData = Session["sortExpression"].ToString().Trim().Split(' ');
                direction = sortData[1];
                if (e.SortExpression == sortData[0])
                {
                    if (sortData[1] == "ASC")
                    {
                        dsScr.Tables[1].DefaultView.Sort = e.SortExpression + " " + "DESC";
                        Session["sortExpression"] = e.SortExpression + " " + "DESC";
                    }
                    else
                    {
                        dsScr.Tables[1].DefaultView.Sort = e.SortExpression + " " + "ASC";
                        Session["sortExpression"] = e.SortExpression + " " + "ASC";
                    }
                }
                else
                {
                    dsScr.Tables[1].DefaultView.Sort = e.SortExpression + " " + "ASC";
                    Session["sortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                dsScr.Tables[1].DefaultView.Sort = e.SortExpression + " " + "ASC";
                Session["sortExpression"] = e.SortExpression + " " + "ASC";
            }

            //lstScreening = (from DataRow row in dsScr.Tables[0].Rows select new SyntheticScreeningModel(row)).ToList();
            //SyntheticScreeningModel abc = new SyntheticScreeningModel();
            //lstScreening.Sort((x, y) => x.sortField.CompareTo(y.sortField));
            //lstScreening = lstScreening.Sort(x => x.sortField);
            //var newLst = GSIAppQueryUtils.GetNewList(dsScr); 

            //grdScreening.DataSource = lstScreening;
            Session["HistoryData"] = dsScr;
            if (e.SortExpression == "PartName" || e.SortExpression == "MeasureName" || e.SortExpression == "OldNumber" || e.SortExpression == "NewNumber" || e.SortExpression == "User" || e.SortExpression == "OldNumber")
            {
                if (dsScr.Tables[1].Columns.Contains("ForIntSort"))
                {
                    dsScr.Tables[1].Columns.Remove("ForIntSort");
                }
                string convertStr = @"Convert(" + e.SortExpression + @", 'System.Int32')";
                //dsScr.Tables[1].Columns.Add("ForIntSort", typeof(int), convertStr);
                dsScr.Tables[1].Columns.Add("ForIntSort", typeof(string), e.SortExpression);
                if (direction == "ASC")
                    dsScr.Tables[1].DefaultView.Sort = "ForIntSort DESC";
                else
                    dsScr.Tables[1].DefaultView.Sort = "ForIntSort ASC";
            }
            else if (e.SortExpression == "StartDate")
            {
                if (dsScr.Tables[1].Columns.Contains("ForDateSort"))
                {
                    dsScr.Tables[1].Columns.Remove("ForDateSort");
                }
                dsScr.Tables[1].Columns.Add("ForDateSort", typeof(DateTime), e.SortExpression);
                if (direction == "ASC")
                    dsScr.Tables[1].DefaultView.Sort = "ForDateSort DESC";
                else
                    dsScr.Tables[1].DefaultView.Sort = "ForDateSort ASC";
            }
            HistoryGrid.DataSource = dsScr.Tables[1].DefaultView;
            HistoryGrid.DataBind();
        }
        
    }
}