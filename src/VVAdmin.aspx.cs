using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for VVAdmin.
	/// </summary>
	public partial class VVAdmin : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["ID"]==null)
				Response.Redirect("Login.aspx");
			if(false/*Session["ID"].ToString()!="10"*/)
				Response.Redirect("Middle.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdNewActivation_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("NewVVActivation.aspx");
		}

		protected void cmdExisitingUser_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("VVExistingUser.aspx");
		}
	}
}
