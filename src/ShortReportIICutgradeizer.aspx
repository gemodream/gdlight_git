<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="ShortReportIICutgradeizer.aspx.cs" AutoEventWireup="True"
    Inherits="Corpt.ShortReportIICutgradeizer" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
<style>
.text_highlitedyellow {background-color: #FFFF00}
</style>
<div class="demoarea">
    <div class="demoheading">Short Report II Cut gradeizer</div>
    <!-- dgResult DataGrid-->
    <div>
            <asp:ImageButton ID="ImageButton1" 
            ToolTip="Selected batch numbers to Excel" runat="server"
                ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExcelClick"
                OnClientClick="window.open('DownLoad.aspx?DownLoadCmd=DownLoadPretty');return false;"
            Enabled="True" />

        
    </div>
    <div style="font-family: Cambria; font-size: 12px; margin-top: 20px; overflow: auto; height: 500px" >
    <asp:DataGrid ID="dgResult" runat="server" Style="font-family: 'MS Sans Serif';
        font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
        ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
        CssClass="table" Font-Names="Cambria" Enabled="false"
        Visible="true" GridLines="Both" Font-Size="12px">
        <ItemStyle ForeColor="#555555" Font-Names="Cambria" Font-Size="13px"></ItemStyle>
        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="12px">
        </HeaderStyle>
    </asp:DataGrid>
    </div>
    <!-- Error message-->
    <div class="control-group error">
        <asp:Label class="control-label" id="lblInfo" runat="server" />
    </div>
    <!--lstFailedCutGrades + lstPrepped-->
    <div>
        <table>
            <tr>
                <td>
                    <div style="font-family: Cambria; font-size: small; overflow:auto; height: 200px;">
                    <asp:ListBox ID="lstFailedCutGrades" runat="server"  Width="200px"
                        Height="176px" Visible="False" Enabled="False" 
                        ToolTip="Failed Cut Grades List"></asp:ListBox>
                    </div>
                </td>
                <td>
                    <asp:ListBox ID="lstPrepped" runat="server" Width="160px" Height="176px" Visible="True"
                        Enabled="False"></asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <!-- dgCutgrade DataGrid-->
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; margin-top: 20px;overflow-x: auto; overflow-y: hidden;"  >
    <asp:DataGrid ID="dgCutgrade" runat="server" Style="font-family: 'MS Sans Serif';
        font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
        ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
        CssClass="table" Font-Names="Arial,Microsoft Sans Serif" Enabled="false"
        Visible="true" GridLines="Horizontal" Font-Size="14px">
        <ItemStyle ForeColor="#555555" Font-Names="Arial" Font-Size="14px"></ItemStyle>
        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="14px">
        </HeaderStyle>
    </asp:DataGrid>
    </div>
    <!-- dgBatchList DataGrid-->
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; margin-top: 20px;overflow-x: auto; overflow-y: hidden;"  >
    <asp:DataGrid ID="dgBatchList" runat="server" Style="font-family: 'MS Sans Serif';
        font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
        ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
        CssClass="table" Font-Names="Arial,Microsoft Sans Serif" Enabled="false"
        Visible="true" GridLines="Horizontal" Font-Size="14px">
        <ItemStyle ForeColor="#555555" Font-Names="Arial" Font-Size="14px"></ItemStyle>
        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="14px">
        </HeaderStyle>
    </asp:DataGrid>
    </div>
    <!-- dgPrettyReport DataGrid-->
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; margin-top: 20px;overflow-x: auto; overflow-y: hidden;"  >
    <asp:DataGrid ID="dgPrettyReport" runat="server" Style="font-family: 'MS Sans Serif';
        font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
        ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
        CssClass="table" Font-Names="Arial,Microsoft Sans Serif" GridLines="Horizontal" Font-Size="14px">
        <ItemStyle ForeColor="#555555" Font-Names="Arial" Font-Size="14px"></ItemStyle>
        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="14px">
        </HeaderStyle>
    </asp:DataGrid>
    </div>  
    <div>
        <!-- dgCutGradeDebug DataGrid-->
        <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; margin-top: 20px;overflow-x: auto; overflow-y: hidden;"  >
        <asp:DataGrid ID="dgCutGradeDebug" runat="server" Style="font-family: 'MS Sans Serif';
            font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
            ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD" Visible="True"
            CssClass="table" Font-Names="Arial,Microsoft Sans Serif" GridLines="Horizontal" Font-Size="14px">
            <ItemStyle ForeColor="#555555" Font-Names="Arial" Font-Size="14px"></ItemStyle>
            <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="14px">
            </HeaderStyle>
        </asp:DataGrid>
        </div>  
    </div>
</div>
</asp:Content>