﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Corpt
{
    public partial class testPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtInc = new DataTable();
            int[] colEnum = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            foreach (int i in colEnum)
            {
                dtInc.Columns.Add("Column" + i, typeof(Int32));
            }
            for (int i = 1; i < 20; i++)
            {
                dtInc.Rows.Add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            }
            dgResult.DataSource = dtInc;
            dgResult.DataBind();
        }

        protected void dgResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtInc = new DataTable();
            int[] colEnum = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            foreach (int i in colEnum)
            {
                dtInc.Columns.Add("Column" + i, typeof(Int32));
            }
            for (int i = 1; i < 20; i++)
            {
                dtInc.Rows.Add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            }
            DataGrid1.DataSource = dtInc;
            DataGrid1.DataBind();
            DataGrid1.Visible = true;
        }
    }
}