<%@ Page language="c#" Codebehind="TrackingStep2.aspx.cs" AutoEventWireup="True" Inherits="Corpt.TrackingStep2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TrackingStep2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:Button style="Z-INDEX: 0" id="cmdHome" runat="server" CssClass="ButtonStyle" Text="Home"
					DESIGNTIMEDRAGDROP="20" onclick="cmdHome_Click"></asp:Button>&nbsp;&nbsp;
				<asp:Button id="cmdTrackingStart" runat="server" CssClass="ButtonStyle" Text="Back to Customer Select" onclick="cmdTrackingStart_Click"></asp:Button></P>
			<P><asp:label style="Z-INDEX: 0" id="lblDebug" runat="server" CssClass="text">Label</asp:label></P>
			<P><asp:label id="lblInfo" CssClass="text" Runat="server"></asp:label></P>
			<P>
				<asp:Label style="Z-INDEX: 0" id="lblTrackingInfo" runat="server" CssClass="text"></asp:Label></P>
			<asp:datagrid id="dgDisplay" runat="server" CssClass="text"></asp:datagrid></form>
	</body>
</HTML>
