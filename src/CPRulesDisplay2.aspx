﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CPRulesDisplay2.aspx.cs" Inherits="Corpt.CPRulesDisplay2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">

    <div class="demoheading">CP Overview</div>
    <div style="font-family: Arial; font-size: 12px; padding-bottom: 20px;">
        <ajaxToolkit:TabContainer ID="TabContainer" runat="server" Width="850px" OnDemand="False"
            Height="200px" ActiveTabIndex="3" AutoPostBack="True" 
            onactivetabchanged="OnActiveTabChanged">
            <ajaxToolkit:TabPanel runat="server" HeaderText="By Customer" ID="TabByCustomer"
                OnDemandMode="Once">
                <ContentTemplate>
                    <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="SearchPanelByCustomerCp"
                        class="form-inline">
                        <!-- Customers -->
                        Customer Like<br />
                        <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px; width: 80px"></asp:TextBox>
                        <asp:ImageButton ID="CustomerLikeButton"  runat="server" ToolTip="Filtering the list of customers"
                            ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" 
                            />

                        <asp:DropDownList ID="lstCustomerList" runat="server" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                            DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                            Width="350px" Style="font-family: Arial; font-size: 12px" />
                        <asp:DropDownList ID="lstProgramList" runat="server" AutoPostBack="True" Style="font-family: Arial;
                            font-size: 12px" Width="200px" DataValueField="CpOfficeIdAndCpId" DataTextField="CustomerProgramName"
                            ToolTip="Customer program" OnSelectedIndexChanged="OnProgramSelectedChanged" />
                        <asp:Button ID="cmdLoad" class="btn btn-info" runat="server" Text="Lookup" OnClick="OnLoadByCustomerClick"
                            Style="margin-left: 15px"></asp:Button>
                    </asp:Panel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
           
            <!-- Tab By Batch -->
            <ajaxToolkit:TabPanel runat="server" HeaderText="By Batch" ID="TabByBatch"  OnDemandMode="Once">
                 <ContentTemplate>
                     <!-- Batch number -->
                     <asp:Panel class="form-inline"  runat="server" DefaultButton="cmdLoadByBatch" ID="SearchPanelByBatch">
                        <!-- 'Batch Number' textbox -->
                        <asp:TextBox type="text" ID="BatchNumber" MaxLength="9" runat="server" placeholder="Batch number"
                            Style="font-weight: bold; width: 100px; margin-top: 10px" />
                        <!-- 'Load' button -->
                        <asp:Button ID="cmdLoadByBatch" class="btn btn-info" runat="server" Text="Lookup"
                            OnClick="OnLoadByBatchClick" Style="margin-left: 10px; margin-top: 10px"></asp:Button>
                      </asp:Panel>
                        <br/>
                        <asp:RequiredFieldValidator runat="server" ID="BatchReq" ControlToValidate="BatchNumber"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqE" TargetControlID="BatchReq"
                            HighlightCssClass="validatorCalloutHighlight" />
                        <asp:RegularExpressionValidator runat="server" ID="BatchRegExpr" ControlToValidate="BatchNumber"
                            Display="None" ValidationExpression="^\d{8}$|^\d{9}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqExpr" TargetControlID="BatchRegExpr"
                            HighlightCssClass="validatorCalloutHighlight" />
                 <!--    </asp:Panel> -->
                 </ContentTemplate>
            </ajaxToolkit:TabPanel>
            
            <!-- Tab by Style -->
            <ajaxToolkit:TabPanel runat="server" HeaderText="By Style" ID="TabByStyle" OnDemandMode="Once">
                <ContentTemplate>
                     <asp:Panel class="form-inline" runat="server" DefaultButton="btnLoadByStyle" ID="SearchPanelByStyle">
                        <!-- 'Style ' textbox -->
                        <asp:TextBox type="text" ID="StyleTextBox" runat="server" placeholder="Style" 
                            Style="font-weight: bold; width: 120px; margin-top: 10px" />
						<asp:RequiredFieldValidator runat="server" ID="StyleReq" ControlToValidate="StyleTextBox"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Style is required." />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="StyleReqE" TargetControlID="StyleReq"
                            HighlightCssClass="validatorCalloutHighlight" />
                        <asp:Button ID="btnLoadByStyle" class="btn btn-info" runat="server" Text="Lookup" 
                            OnClick="OnLoadByStyleClick" Style="margin-left: 15px; margin-top: 5px"></asp:Button>
                         <asp:DropDownList runat="server" ID="ComboByStyle"  OnSelectedIndexChanged="OnComboByStyleSelectedChanged"
                            DataValueField="Id" DataTextField="DisplayValue" AutoPostBack="True" Visible="False" ToolTip="Style/Customer/SKU"
                            style="font-family: Arial; font-size: small; width: 500px; margin-top: 10px"/>
                         <asp:Button ID="AddCertBtn" class="btn btn-info" runat="server" Text="ADD STYLE CERT" 
                            OnClick="OnAddStyleCertClick" Style="margin-left: 15px; margin-top: 5px" Width="147px" Visible="False"></asp:Button>
                         <asp:Label ID="StyleCertInfoLbl" ForeColor="Red" runat="server"></asp:Label>
                     </asp:Panel>   
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
           
            <!-- Tab by SKU -->
            <ajaxToolkit:TabPanel runat="server" HeaderText="By SKU" ID="TabBySku" OnDemandMode="Always">
                <ContentTemplate>
                     <asp:Panel class="form-inline" runat="server" DefaultButton="btnLoadBySku" ID="SearchPanelBySku" Height="600px">
                        <!-- 'SKU ' textbox -->
                        <asp:TextBox type="text" ID="SkuTextBox" runat="server" placeholder="SKU" 
                            Style="font-weight: bold; width: 120px; margin-top: 10px" />
                        <asp:RequiredFieldValidator runat="server" ID="SkuReq" ControlToValidate="SkuTextBox"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A SKU is required." />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="SkuReqE" TargetControlID="SkuReq"
                            HighlightCssClass="validatorCalloutHighlight" />
                        <asp:Button ID="btnLoadBySku" class="btn btn-info" runat="server" Text="Lookup" 
                            OnClick="OnLoadBySkuClick" Style="margin-left: 15px; margin-top: 5px"></asp:Button>
                         
                         
                         <asp:HiddenField runat="server" ID="hdnItemTypeID" />
                         <table>
                             <tr>
                                 <td>
                                     <asp:TreeView ID="SKUTypesTreeView" runat="server" Visible="true" Enabled="false">
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                  </asp:TreeView>
                                 </td>
                                 <td>
                         <asp:DropDownList ID="SKUTypesList" runat="server" AutoPostBack="True" CssClass="" DataTextField="ItemTypeName"
                                    DataValueField="ItemTypeID" OnSelectedIndexChanged="OnSKUTypesListSelectedChanged" Visible="true" ToolTip="Customer Program List"
                                    Width="200px" />
                                 </td>
                                 <table>
                                         <tr>
                                             <td>
                                                <asp:DropDownList runat="server" ID="ComboBySku"  OnSelectedIndexChanged="OnComboBySkuSelectedChanged"
                                                DataValueField="Id" DataTextField="DisplayValue" AutoPostBack="True" Visible="False" ToolTip="SKU/Customer/Style"
                                                style="font-family: Arial; font-size: small; " Width="568px"/>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                <div runat="server" id="CPLink" class="auto-style1">
                                                    <a href="CustomerProgramNew.aspx" target="_blank" id="CpRef" runat="server"></a>
                                                </div>
                                            </td>
                                         </tr>
                                     </table>
                                 </tr>
                             </table>
                     </asp:Panel>   
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
       
        <!-- CpRule Details Panel -->
        <div style="padding-top: 20px">
            <asp:Panel runat="server" ID="RuleDetailPanel" Visible="False">
                <!-- DataGrid -->
                <div>
                    <asp:Label runat="server" ID="CpComments" ></asp:Label></div>
                <div>
                    <asp:Label runat="server" ID="CpDescription"></asp:Label></div>
                <table>
                    <tr>
                        <td style="vertical-align: top;">
                            <div id="containerByCust">
                                <asp:DataGrid runat="server" ID="grdByCust" CellSpacing="5" CellPadding="5">
                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                </asp:DataGrid>
                            </div>
                        </td>
                        <td style="vertical-align: top; width: 100px; text-align: left; font-family: Arial;
                            text-align: left; font-size: small; padding-left: 10px; color: black;">
                            <div>
                                <asp:Label runat="server" ID="CpCustomerId"></asp:Label></div>
                            <div>
                                <asp:Label runat="server" ID="CpCustomerName"></asp:Label></div>
                            <div>
                                <asp:Label runat="server" ID="CpSrp"></asp:Label></div>
                            <div>
                                <asp:Label runat="server" ID="CpName"></asp:Label></div>
                            <div>
                                <asp:Label runat="server" ID="CpStyle"></asp:Label></div>
                            <div>
                                <asp:Label runat="server" ID="CpPathToPicture"></asp:Label></div>
                            <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image><br />
                            <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server"></asp:Label>
                            <asp:Label ID="ErrMemoSkuField" ForeColor="Red" runat="server"></asp:Label>
                            <div style="font-size: small">
                                <table>
                                    <tr>
                                        <td>
                                            <strong>Documents:</strong>
                                            <br />
                                            <asp:ListBox runat="server" ID="DocList" DataValueField="DocName" DataTextField="DocName" />
                                        </td>
                                        <td>
                                            <div style="margin-left:50px;">
                                            <asp:Button ID="viewSku" runat="server" Text="View SKU" ToolTip="Get SKU" CssClass="btn btn-info btn-large" OnClick="ViewSkuPdf" OnClientClick="window.document.forms[0].target='_self';"></asp:Button>
                                                </div>
                                        </td>
                                        <td>
                                            <div class="form-group" style="margin-left:50px;border:5px solid brown" id="SkuUploadDiv" runat="server">
									            <label class="control-label" for="SkuFileUpload" style="font-weight: 600; float: left; margin-top: 4px;">SKU file:</label>
                                                <asp:FileUpload ID="SkuFileUploader" runat="server" Width="200px" TabIndex="17" ToolTip="Please select file" />
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="UploadSkuBtn" Text="Upload SKU" ToolTip="Upload SKU" CssClass="btn btn-info btn-large" OnClick="UploadSkuBtnClick" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="overflow:auto!important; -webkit-overflow-scrolling: touch!important;">
                                <iframe scrolling="yes" style="overflow:visible; overflow-x:scroll; overflow-y: scroll;" src="#" id="iframeMemoSkuPDFViewer" width="800" height="650"  runat="server" visible="false"></iframe>
                                <asp:Image ID="SkuImage" runat="server" Width="1000px" Height="300" Visible="False"></asp:Image><br />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </div>
</asp:Content>
