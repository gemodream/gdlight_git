﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="RetailersVendorInfo.aspx.cs" Inherits="Corpt.RetailersVendorInfo" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <style>
        /* List of company group*/
        .chkGroupCompanyList > tbody > tr > td
        {
            padding:0px;
        }
        .chkGroupCompanyList
        {
            margin-bottom:0px !important
        }
        .chkGroupCompanyList > tbody > tr > td > input[type="checkbox"],input[type="checkbox"]:disabled, input[type="checkbox"]:disabled + label{
            margin-bottom:8px !important;
            margin-left: 10px;
            display: inline !important;
        }
        .chkGroupCompanyList > tbody > tr > td > label, label:disabled{
            margin-bottom:8px !important;
            margin-left: 10px !important;
            display: inline !important;
        }
    </style>
    <script type="text/javascript">

        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {
            $("#<%= CpDropDown.ClientID %>").select2({});
            $("#<%= ddlProgramList.ClientID %>").select2({});
            $("#<%= ddlSkuProgram.ClientID %>").select2({});            
            
        }

<%--        function ClickDocCheckBox(el)
        {
            if (el.checked)
            {
                uncheckAll();
                el.checked = true;
                var label = el.parentElement.parentElement.children[1].textContent;
                $("#<%= hdnDocTitle.ClientID %>").val(label);
                event.preventDefault();
                //document.getElementById("myForm").submit();
            }
        }
        function uncheckAll() {
            $("#<%= CpDocsGrid.ClientID %>").find('input[type="checkbox"]').each(function () {
                $(this).prop("checked", false);
            });
        }--%>

    </script>

    <div class="demoarea">
         <div style="height: 25px">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                 <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                     <b>Please, wait....</b>
                 </ProgressTemplate>
             </asp:UpdateProgress>
         </div>
         
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <ContentTemplate>
                <div class="demoheading"><asp:Label ID="lblCustomerName" runat="server"></asp:Label></div>
<%--                <div>
                    <asp:Label ID="lblBrand" runat="server">Brand:</asp:Label><br />
                    <asp:Label ID="lblProgram" runat="server">Program:</asp:Label>
                   
                </div>--%>
                <asp:Panel ID="BranchesPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" id="lblGroupName"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" id="groupListTr">
                            <td>
                                <div style="border:silver thin solid;">
                                    <asp:CheckBoxList ID="cblGroupList" runat="server" CssClass="table table-condensed chkGroupCompanyList"
                                        RepeatLayout="Table" style="padding:5px; margin-right:10px;">
                                    </asp:CheckBoxList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="SkuFilterPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Sku Filter:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <table>
                            <tr> 
                                
                                <td>
                                    <asp:Label runat="server" for="ddlSkuNatural">Nature:</asp:Label>
                                </td>
                                <td>                                    
                                    <asp:DropDownList ID="ddlSkuNatural" runat="server" AutoPostBack="True" OnSelectedIndexChanged="OnSkuNaturalSelectedChanged" CssClass="text-style" 
                                        ToolTip="Sku Nature" Width="250px" Height="28px" Style="margin-top: 8px; margin-right: 10px;">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" for="ddlSkuBrand">Brand:</asp:Label>
                                </td>
                                <td>                                    
                                    <asp:DropDownList ID="ddlSkuBrand" runat="server" AutoPostBack="True" OnSelectedIndexChanged="OnBrandSelectedChanged" CssClass="text-style" 
                                        ToolTip="Brand" Width="250px" Height="28px" Style="margin-top: 8px;">
                                    </asp:DropDownList>
                                </td>   
                                <td>
                                    <asp:Label runat="server" for="ddlSkuProgram">Program:</asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSkuProgram" runat="server" AutoPostBack="True" OnSelectedIndexChanged="OnSkuProgramSelectedChanged" CssClass="text-style" 
                                        ToolTip="Sku Program" Width="250px" Height="28px" Style="margin-top: 8px; margin-right: 10px;">
                                    </asp:DropDownList>
                                </td>
                                                             
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" for="ddlSkuStructure">Structure:</asp:Label>
                                </td>
                                <td>                                    
                                    <asp:DropDownList ID="ddlSkuStructure" runat="server" AutoPostBack="True" OnSelectedIndexChanged="OnSkuStructureSelectedChanged" CssClass="text-style" 
                                        ToolTip="Sku Structure" Width="250px" Height="28px" Style="margin-top: 8px;">
                                    </asp:DropDownList>
                                </td>   
                                <td>
                                    <asp:Label runat="server" for="ddlSkuCategory">Category:</asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSkuCategory" runat="server" AutoPostBack="False" CssClass="text-style" 
                                        ToolTip="Sku Category" Width="250px" Height="28px" Style="margin-top: 8px; margin-right: 10px;">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" for="ddlSkuType">Type:</asp:Label>
                                </td>
                                <td>                                    
                                    <asp:DropDownList ID="ddlSkuType" runat="server" AutoPostBack="False" CssClass="text-style" 
                                        ToolTip="Sku Type" Width="250px" Height="28px" Style="margin-top: 8px;">
                                    </asp:DropDownList>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="CpPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Customer Programs:                   
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblCp" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>
                <table>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="CpProgramPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="True">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" for="CpDropDown">SKU:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CpDropDown" runat="server" DataTextField="CustomerProgramName"
                                                        AutoPostBack="True" DataValueField="CpId" CssClass="text-style" OnSelectedIndexChanged="OnCpDropDownSelectedChanged"
                                                        ToolTip="Customer Programs List" Width="250px" Height="30px" Style="margin-top: 8px; margin-right: 10px;">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" for="ddlProgramList">Program:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlProgramList" runat="server" AutoPostBack="True" CssClass="text-style" 
                                                OnSelectedIndexChanged="OnProgramDropDownSelectedChanged"
                                                    ToolTip="Programs List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="SaveProgram" OnClick="OnSaveProgramClick" CssClass="btn btn-small btn-info"
                                                Style="margin-left: 20px" Text="Save" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSaveError" Style="font-weight: bold;color:red;" Visible="false"></asp:Label>
                                            <asp:Label runat="server" ID="lblSaveSuccess" Style="font-weight: normal;color:green;" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td width="70%">
                            <asp:Panel ID="CpDescriptionPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                                Description: <asp:Label runat="server" ID="lblCpDescr" Width="100%" Wrap="True" Style="font-weight: normal;"></asp:Label>
                            </asp:Panel>
                        </td>
                        <td rowspan="2" width="30%">
                            <asp:Panel ID="CpImagePanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                                Picture:<br/>
                                <div>
                                    <asp:Image ID="itemPicture" runat="server" Width="90px" Visible="False"></asp:Image><br />
                                    <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Width="90px"></asp:Label>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="CpCommentPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                                Comment: <asp:Label runat="server" ID="lblCpComment" Width="100%" Wrap="True" Style="font-weight: normal;"></asp:Label>
                            </asp:Panel>                            
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="CpDocPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Documents/Reports:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <table>
                            <tr>
                                 <td style="vertical-align: top; padding-left: 20px;width: 380px;">
                                   <table>
                                       <tr>
                                           <td>
                                               <asp:Button runat="server" ID="MDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="MDX Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="FDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="FDX Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="SDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="SDX Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="GradingReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="Grading" Width="90px" Height="60px" />
                                          </td>
                                       </tr>
                                        <tr>
                                           <td>
                                               <asp:Button runat="server" ID="IDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="IDX Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="WTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="WhiteTag" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="PCReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="Price Card" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="CNSLTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="CNSLT Report" Width="90px" Height="60px" />
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <asp:Button runat="server" ID="HDSReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="HDS_Back" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="GEMTAGReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="GEM Tag" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="GOLDTAGReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Font-Size="XX-Small" Enabled="True" Text="GoldTag Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="GMXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="GMX Report" Width="90px" Height="60px" />
                                          </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <asp:Button runat="server" ID="CDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="CDX Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="LBLReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="LBL Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="TXTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="TXT Report" Width="90px" Height="60px" />
                                               <asp:Button runat="server" ID="SRTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="True" Text="SRT Report" Width="90px" Height="60px" />
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                               <td>
                                    <asp:GridView runat="server" ID="CpDocsGrid" AutoGenerateColumns="false" OnRowDataBound="OnDocsGrid_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>Document</HeaderTemplate>
                                                <ItemTemplate>                                                    
                                                    <asp:Label runat="server" ID="lblDocTitle" value=""></asp:Label>
                                                    <asp:HiddenField runat="server" ID="hdnDocId" Value="" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>Reports</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:ListBox runat="server" ID="DocAttachedReportsList" Rows="5" Width="250px" DataTextField="OperationTypeName"
                                                                            DataValueField="Key" OnSelectedIndexChanged="OnDocAttachPrintDocSelection" AutoPostBack="True"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>  
                                <td>
                                    <asp:Button runat="server" ID="RefreshDocs" OnClick="OnRefreshDocsClick" CssClass="btn btn-small btn-info"
                                                Style="margin-left: 20px" Text="Refresh" />
                                </td>
                                <td>
                                    <div>
                                        <asp:Panel ID="NewReportPicturePanel" runat="server" style="text-align:center;">
                                            <asp:Label runat="server" ID="NewReportPictureField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                                            <asp:ImageButton ID="NewReportPictureButton" runat="server" Width="250px" BorderWidth="1px" ToolTip="Report Picture button" OnClick="OnNewPictureClick" />
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                </asp:Panel>
                <asp:Panel ID="TreePanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Document Parts Tree/Measures
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <table>
                            <tr>
                                <td height="20%" style="vertical-align: top;">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlCpDocs" runat="server" DataTextField="Title"
                                                        AutoPostBack="True" DataValueField="CpDocId" CssClass="text-style" OnSelectedIndexChanged="OnCpDocsSelectedChanged"
                                                        ToolTip="Documents" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TreeView ID="DocPartTree" runat="server" ShowCheckBoxes="All" OnSelectedNodeChanged="OnDocPartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                                </asp:TreeView>
                                            </td>
                                        </tr>

                                    </table>
                                    
                                </td>
                                <td height="60%">
                                    <div>
                                        <asp:Label runat="server" ID="PartName" CssClass="label" style="white-space: normal"></asp:Label>
                                    </div>
                                    <div id="Rules" runat="server" style="width: 100%; height: 200px; overflow-y: scroll">
                                        <asp:DataGrid runat="server" ID="RulesGrid" CellSpacing="5" CellPadding="5">
                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        </asp:DataGrid>
                                    </div>
                                </td>

                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="CpPricingPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Pricing:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px; width: 100%">
                        <table>
                            <tr>
                                <td  style="vertical-align:top">
                                    <asp:Label runat="server" ID="lblPriceRange" CssClass="label" for="PriceRangeGrid">Price Range</asp:Label>
                                    <asp:DataGrid runat="server" ID="PriceRangeGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                        OnItemDataBound="OnPriceRangeDataBound" DataKeyField="Rownum">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    From</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblFrom" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    To</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTo" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    Price</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblPrice" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                    </asp:DataGrid>
                                </td>
                                <td style="vertical-align:top">
                                    <asp:Label ID="lblAddServ" runat="server" for="AddServicePriceGrid"  CssClass="label">Additional Services</asp:Label>
                                    <asp:DataGrid runat="server" ID="AddServicePriceGrid" AutoGenerateColumns="False"
                                        CssClass="table table-condensed" OnItemDataBound="OnAddServicePriceDataBound"
                                        DataKeyField="Rownum">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    Service Name</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblServiceName"></asp:Label>                                                  
                                                </ItemTemplate>
                                               <ItemStyle Width="200px" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    Price</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblServicePrice"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                        

                    </div>

                </asp:Panel>
                <asp:Panel ID="LinesheetsPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Linesheets:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblLinesheets" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="CertPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Certs:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblCert" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="CpItemsPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Top 10 Items:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblCpItems" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>

                <asp:Panel ID="TagsPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Tags:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblTags" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>               

                <asp:Panel ID="GoFPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; margin-top: 10px;" Visible="False">
                    Groups of fraction:
                    <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                        <asp:Label runat="server" ID="lblGoF" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
                    </div>
                </asp:Panel>
<%--new picture popup --%>                
				    <asp:Panel runat="server" ID="Panel3" CssClass="modalPopup" Height="850px" Width="600px" Style="display: none">
                	    <asp:HiddenField runat="server" ID="HiddenUrl" ></asp:HiddenField>
                	    <div style="padding-top: 10px">
                            <p style="text-align: center;">
                                <asp:Image ID="NewReportPicture" runat="server" BorderWidth="1px" ></asp:Image>
                            </p>
                        </div>
                        <div style="padding-top: 10px">
                            <p style="text-align: center;">
                                <asp:Button ID="NewPictureOkButton" runat="server" Text="OK" />
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Button ID="HiddenNewPictureBtn" runat="server" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenNewPictureBtn" PopupControlID="Panel3" ID="NewPicturePopupExtender" 
                        OkControlID="NewPictureOkButton" DropShadow="true">
                    </ajaxToolkit:ModalPopupExtender>  
             </ContentTemplate>

         </asp:UpdatePanel>
     </div>
</asp:Content>
