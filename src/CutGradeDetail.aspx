﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CutGradeDetail.aspx.cs" Inherits="Corpt.CutGradeDetail" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server" >
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>
<%--    <script type="text/javascript">--%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScrollPercent();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScrollPercent();--%>
<%----%>
<%--        });--%>
<%--        function gridviewScrollPercent() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=tblCutGradeDetail.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 1--%>
<%--            });--%>
<%--        }--%>
<%--    </script>--%>

    <div class="demoarea">
        <div class="demoheading">Cut Grade</div>
        <asp:Panel runat="server" ID="dgPanel">
        <div>
            <asp:ImageButton ID="ImageButton1" ToolTip="Download to Excel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExcelClick" />
             <asp:Label runat="server" ID="InfoLabel" Text="" CssClass="control-label" Font-Bold="True"/>
        </div>
        <!-- Count rows, Measure Name -->
        <div>
            <table width="100%" style="font-family: Arial; font-size: 12px; font-weight: bold">
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="LblRowCount" runat="server" CssClass="control-label"/>
                    </td>
                </tr>
            </table>
        </div>
        <!-- DataGrid -->
        <div id="gridContainer" style="font-size: small;">
            <asp:DataGrid ID="tblCutGradeDetail" runat="server" OnItemDataBound="OnItemDataBound" CellPadding="5">
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                <ItemStyle ForeColor="#0D0D0D"></ItemStyle>
            </asp:DataGrid>
        </div>
        </asp:Panel>
        <!-- Back -->
        <div class="row" align="right">
            <asp:HyperLink ID="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx">Back</asp:HyperLink>
        </div>
    </div>
</asp:Content>
