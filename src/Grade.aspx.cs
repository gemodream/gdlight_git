using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Corpt.Constants;
using Corpt.FullItemCode;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for Grade.
	/// </summary>
	public partial class Grade : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here						
			if(Session["ID"]==null)
				Response.Redirect("Login.aspx");
			txtItemNumber.AutoPostBack = false;
            if (Session["GradingMode"] == null)
            {
                Session["GradingMode"] = "Color";
            }
			if(!Page.IsPostBack)
			{
			    rblModeSelector.Enabled = false;
				switch(Utils.NullValue(Session["GradingMode"]))
				{
					case "Color":
					{
						rblModeSelector.SelectedIndex = rblModeSelector.Items.IndexOf(rblModeSelector.Items.FindByValue("5"));
						//rblModeSelector.Visible=false;
						break;
					}
					case "Clarity":
					{
						rblModeSelector.SelectedIndex = rblModeSelector.Items.IndexOf(rblModeSelector.Items.FindByValue("4"));
						//rblModeSelector.Visible=false;
						break;
					}
					case "Measure":
					{
						rblModeSelector.SelectedIndex = rblModeSelector.Items.IndexOf(rblModeSelector.Items.FindByValue("6"));
						//rblModeSelector.Visible=false;
						break;
					}
					case "" :
					{
						rblModeSelector.Enabled=true;
						break;
					}
				}
			}
			if(Page.IsPostBack)
			{
				if((Utils.NullValue(Session["Grade_PartID"])!="")&&
					(Utils.NullValue(Session["Grade_MeasureID"])!="")&&
					(Utils.NullValue(Session["Grade_MeasureClass"])!="")&&
					(Utils.NullValue(Session["Gade_MeasureName"])!=""))
				{
					CreReCreateControls();			
				}
			}
#if DEBUG
			lblDebug1.Text=(new SqlConnection(Session["MyIP_ConnectionString"].ToString())).DataSource+":"+(new SqlConnection(Session["MyIP_ConnectionString"].ToString())).Database;
#endif
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void CleanUp()
		{
			lstBatch.Items.Clear();
			lblItemNumber.Text="";
			lstItemStructure.Items.Clear();
			lstMeasures.Items.Clear();
			Session["Grade_PartID"] = null;
			Session["Grade_MeasureID"] = null;
			Session["Grade_MeasureClass"] = null;
			Session["Gade_MeasureName"] = null;
			Session["Grade_MeasureCode"] = null;
			ItemPicture.ImageUrl="";
			ItemPicture.Visible=false;
			ActionPanel.Controls.Clear();
			Session["TouchedItemList"] = null;
			Session["EditedItemList"] = null;
		}
		private void CleanUp2()
		{
			lstItemStructure.Items.Clear();
			lstMeasures.Items.Clear();
			Session["Grade_PartID"] = null;
			Session["Grade_MeasureID"] = null;
			Session["Grade_MeasureClass"] = null;
			Session["Gade_MeasureName"] = null;
			Session["Grade_MeasureCode"] = null;
			pnlIvalidItemMessage.Enabled=false;
			pnlIvalidItemMessage.Visible=false;
			ActionPanel.Controls.Clear();
		}
		private void CleanUp3()
		{
			lstMeasures.Items.Clear();
			Session["Grade_PartID"] = null;
			Session["Grade_MeasureID"] = null;
			Session["Grade_MeasureClass"] = null;
			Session["Gade_MeasureName"] = null;
			Session["Grade_MeasureCode"] = null;
			ActionPanel.Controls.Clear();
		}

		protected void cmdLoad_Click(object sender, System.EventArgs e)
		{
			string strItemNumber = "";
			string strGroupCode = "";
			string strBatchCode = "";

			txtItemNumber.AutoPostBack=false;
			txtItemNumber.ReadOnly=true;
			cmdLoad.Enabled=false;
			CleanUp();

            //			if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{10}$|^\d{11}$")) 
            //			{
            //				if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{8}$|^\d{9}$")) 
            //				return;
            //			}

            strItemNumber = Utils.GetItemNumberWithNoDots(txtItemNumber.Text.Trim());

            SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
			SqlCommand command = new SqlCommand("spGetItemCPPictureByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			//		   spGetItemCPPictureByCode @AuthorId=19, 
			//									@AuthorOfficeId=1,
			//									@CustomerCode=null, 
			//									@BGroupState=null, 
			//									@EGroupState= null, 
			//									@BState= null, 
			//									@EState= null, 
			//									@BDate= null, 
			//									@EDate= null, 
			//									@GroupCode=4849,
			//									@BatchCode=1, 
			//									@ItemCode= null
			//

			command.Parameters.Add("@AuthorId",Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState", DBNull.Value);
			command.Parameters.Add("@EGroupState", DBNull.Value);
			command.Parameters.Add("@BState", DBNull.Value);
			command.Parameters.Add("@EState", DBNull.Value);
			command.Parameters.Add("@BDate", DBNull.Value);
			command.Parameters.Add("@EDate", DBNull.Value);

			strGroupCode =	Utils.FillToFiveChars(Utils.ParseOrderCode(strItemNumber).ToString());
			strBatchCode =	Utils.FillToThreeChars(Utils.ParseBatchCode(strItemNumber).ToString(), strGroupCode);

			command.Parameters.Add("@GroupCode", strGroupCode);
			command.Parameters.Add("@BatchCode", strBatchCode);

			command.Parameters.Add("@ItemCode", DBNull.Value);

			
			SqlDataReader reader = command.ExecuteReader();

			ArrayList il = new ArrayList();

			//itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString();
			String path2Picture;
			path2Picture = "";

			while(reader.Read())
			{
				SingleItem sitm = new SingleItem();
				sitm.BatchCode=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("BatchCode")).ToString());
				sitm.OrderCode=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("GroupCode")).ToString());
				sitm.ItemCode=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("ItemCode")).ToString());
				sitm.BatchID=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("BatchID")).ToString());
				sitm.ItemTypeID=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("ItemTypeId")).ToString());
				sitm.NewItemCode=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("NewItemCode")).ToString());
				sitm.NewBatchID=Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("NewBatchID")).ToString());
				sitm.StateID=int.Parse(reader.GetSqlValue(reader.GetOrdinal("StateID")).ToString());
				path2Picture=reader.GetSqlValue(reader.GetOrdinal("path2picture")).ToString();
				il.Add(sitm);
			}

			//itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString();

			if(!(path2Picture.EndsWith(".ico")||path2Picture.ToUpper()=="DEFAULT"||path2Picture==""))
			{
				ItemPicture.ImageUrl ="https://www.gemscience.net/trackingNET/batchpicture/" + Regex.Replace(path2Picture,@"\\","/");
				ItemPicture.Visible=true;
			}
			else
			{
				ItemPicture.Visible=false;
			}

			foreach(SingleItem si in il)
			{
				lstBatch.Items.Add(si.ToString());
			}
			conn.Close();
			Session["Grade_ItemList"]=il;

			//items loaded. Update the history.
			command = new SqlCommand("wspSetBatchHistory");
			command.CommandType= CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

			int intBatchID = (il[0] as SingleItem).BatchID;
			command.Parameters.Add(new SqlParameter("@BatchID", intBatchID));
			int intGDLightGrade = 20;
			command.Parameters.Add(new SqlParameter("@FormID", intGDLightGrade));
			command.Parameters.Add(new SqlParameter("@UserID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@EventID", 1)); // "Opened"
			command.Parameters.Add(new SqlParameter("@NumberOfItemsAffected", il.Count));
			command.Parameters.Add(new SqlParameter("@NumberOfItemsInBatch", il.Count));

			command.Connection.Open();
			command.ExecuteNonQuery();
			command.Connection.Close();
		
		}

		protected void lstBatch_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstBatch.SelectedIndex>-1)
			{
				CleanUp2();

				//check for flagged item.
				ArrayList sal = Session["Grade_ItemList"] as ArrayList;
                AddTouchedItemToList((sal[lstBatch.SelectedIndex] as SingleItem));
				int intStateID = (sal[lstBatch.SelectedIndex] as SingleItem).StateID;
				if(intStateID==10)
				{
					pnlIvalidItemMessage.Visible=true;
					pnlIvalidItemMessage.Enabled=true;
				}
				else
				{
					pnlIvalidItemMessage.Visible=false;
					pnlIvalidItemMessage.Enabled=false;
				}

				lblItemNumber.Text=lstBatch.SelectedItem.Text;
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("spGetPartsByItemType");
				command.Connection = conn;
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.Add("@AuthorID", Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
				command.Parameters.Add("@ItemTypeID",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).ItemTypeID);
				DataTable dt = new DataTable();
				SqlDataAdapter da;
				da = new SqlDataAdapter(command);
				da.Fill(dt);
				foreach(DataRow dr in dt.Rows)
				{
					lstItemStructure.Items.Add(new ListItem(dr["PartName"].ToString(),dr["PartID"].ToString()));
				}

				//Get All Set Measures for selected item.
				command = new SqlCommand("spGetPartValue");
				command.Connection = conn;
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.Add("@AuthorID", Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
				command.Parameters.Add("@BatchID",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).NewBatchID);
				command.Parameters.Add("@ItemCode",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).NewItemCode);
				command.Parameters.Add("@PartID",DBNull.Value);
				command.Parameters.Add("@RecheckNumber","-1");
				//command.Parameters.Add("@ViewAccessCode",DBNull.Value);
				command.Parameters.Add("@ViewAccessCode",rblModeSelector.SelectedValue);

				DataSet ds;
				ds = new DataSet("ItemStuff");
				da = new SqlDataAdapter(command);
				da.Fill(ds);

				Session["Grade_ItemMeasureValues"]=ds.Copy();
				Session["Grade_SelectedItem"]=(Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem;
			}
		}

		protected void txtItemNumber_TextChanged(object sender, System.EventArgs e)
		{
			if(Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{8}$|^\d{9}$"))
			//if(txtItemNumber.Text.Length==8)
			{
				cmdLoad_Click(sender, e);
			}
		}

		protected void lstItemStructure_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstItemStructure.SelectedIndex>-1)
			{
				CleanUp3();

				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("spGetItemMeasuresByViewAccessAndCP");
				command.Connection = conn;
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.Add("@AuthorID", Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
				//***sasha changed the procedure*****
				//command.Parameters.Add("@BatchCode",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).BatchCode);
				//command.Parameters.Add("@GroupCode",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).OrderCode);
				command.Parameters.Add(new SqlParameter("@BatchID",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).BatchID));
				//***********************************

				command.Parameters.Add("@ItemTypeID",((Session["Grade_ItemList"] as ArrayList)[lstBatch.SelectedIndex] as SingleItem).ItemTypeID);
				command.Parameters.Add("@ViewAccessCode",rblModeSelector.SelectedValue);

				DataSet ds;
				ds = new DataSet("ItemStuff");
				SqlDataAdapter da;
				da = new SqlDataAdapter(command);
				da.Fill(ds);

				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					if(dr["PartID"].ToString()==lstItemStructure.SelectedValue)
					{
						lstMeasures.Items.Add(new ListItem(dr["MeasureTitle"].ToString(),dr["PartID"].ToString()+"_"+dr["MeasureID"]+"_"+dr["MeasureClass"]+"_"+dr["MeasureCode"]));
					}
				}				
			}
		}

		protected void lstMeasures_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataTable dt = new DataTable();
			dt = (Session["Grade_ItemMeasureValues"] as DataSet).Tables[0];
			//			DataView dv = new DataView(dt);
			//			dv.RowFilter="PartID = " +  lstItemStructure.SelectedValue + " AND MeasureID = " + lstMeasures.SelectedValue.Split('_')[1];

			//MeasureClass 1-5
			Session["Grade_PartID"] = lstItemStructure.SelectedValue;
			Session["Grade_MeasureID"] = lstMeasures.SelectedValue.Split('_')[1];
			Session["Grade_MeasureClass"] = Int32.Parse(lstMeasures.SelectedValue.Split('_')[2]);
			Session["Grade_MeasureCode"] = Int32.Parse(lstMeasures.SelectedValue.Split('_')[3]);
			Session["Gade_MeasureName"] = lstMeasures.SelectedItem.Text;
			ActionPanel.Controls.Clear();
			lblProcResult.Text="";

			//			//debug
			//			dgDebug.DataSource = dv;
			//			dgDebug.DataBind();
			//			//end-debug

			CreReCreateControls();
		}

		private void CreReCreateControls()
		{
			int measureClass = Int32.Parse(Session["Grade_MeasureClass"].ToString());
			System.Web.UI.WebControls.Label measureName = new Label();
			measureName.Text = Session["Gade_MeasureName"].ToString() + "<br>";
			measureName.CssClass="Text";
			measureName.Font.Bold = true;
			ActionPanel.Controls.Add(measureName);
			System.Web.UI.WebControls.Label measureState = new Label();
			measureState.Text = "";
			measureState.CssClass="text";
			measureState.ID="lblMeasureState";
			ActionPanel.Controls.Add(measureState);

			switch(measureClass)
			{
				case 1 : //enum
				{
					System.Web.UI.WebControls.DropDownList measureList = new DropDownList();
					//GDLight_EnumMeasures
					measureList.CssClass="text";
					DataTable enumMeasures = Session["GDLight_EnumMeasures"] as DataTable;

					//debug
					////					dgDebug.DataSource = enumMeasures;
					////					dgDebug.DataBind();
					//debug-end

					DataView dataView = new DataView(enumMeasures);
					dataView.RowFilter = "MeasureValueMeasureid = " + (Session["Grade_MeasureID"] as String);

					foreach(DataRowView dr in dataView)
					{
						measureList.Items.Add(new ListItem(dr.Row.ItemArray[dr.Row.Table.Columns.IndexOf("ValueTitle")].ToString(),dr.Row.ItemArray[dr.Row.Table.Columns.IndexOf("MeasureValueID")].ToString()+"_"+dr.Row.ItemArray[dr.Row.Table.Columns.IndexOf("MeasureCode")].ToString()));
					}
					if(!(Utils.NullValue(Session["Grade_ItemMeasureValues"])==""))
					{
						DataSet ds = Session["Grade_ItemMeasureValues"] as DataSet;
						//DataRow[] drFiltered = ds.Tables[0].Select("MeasureID = " + (Session["Grade_MeasureID"] as String));
						DataRow[] drFiltered = ds.Tables[0].Select("PartID = " +  (Session["Grade_PartID"] as String) + " AND MeasureID = " + Session["Grade_MeasureID"] as String);
						if(drFiltered.Length>0)
						{
							int measureValueID = Int32.Parse(drFiltered[0].ItemArray[drFiltered[0].Table.Columns.IndexOf("MeasureValueID")].ToString());
							int measureCode = Int32.Parse(drFiltered[0].ItemArray[drFiltered[0].Table.Columns.IndexOf("MeasureCode")].ToString());
							measureList.SelectedIndex=measureList.Items.IndexOf(measureList.Items.FindByValue(measureValueID.ToString()+"_"+measureCode.ToString()));
                            (ActionPanel.FindControl("lblMeasureState") as Label).Text = "Curr. value: " + (measureList.SelectedItem == null ? "" : measureList.SelectedItem.Text) + @"<br>";
						}
						else
						{
							(ActionPanel.FindControl("lblMeasureState") as Label).Text = "Curr. value not set.<br>";
						}
					}
					measureList.AutoPostBack=true;
					measureList.EnableViewState = true;
					measureList.ID="Measure_ENUM_"+Session["Grade_MeasureID"] as String;
					measureList.SelectedIndexChanged += new System.EventHandler(this.MeasureChanged_AutoPostBack);
					ActionPanel.Controls.Add(measureList);
					ActionPanel.Controls.Add(AddButton("Button_ENUM_"+Session["Grade_MeasureID"] as String));
					break;
				}
				case 2 : //String // StringValue
				{
					System.Web.UI.WebControls.TextBox strMeasure = new TextBox();
					strMeasure.CssClass="text";
					strMeasure.Width=200;
					if(!(Utils.NullValue(Session["Grade_ItemMeasureValues"])==""))
					{
						DataSet ds = Session["Grade_ItemMeasureValues"] as DataSet;
						//DataRow[] drFiltered = ds.Tables[0].Select("MeasureID = " + Session["Grade_MeasureID"] as String);	
						DataRow[] drFiltered = ds.Tables[0].Select("PartID = " +  (Session["Grade_PartID"] as String) + " AND MeasureID = " + Session["Grade_MeasureID"] as String);
						if(drFiltered.Length>0)
						{
							strMeasure.Text=Utils.NullValue(drFiltered[0].ItemArray[drFiltered[0].Table.Columns.IndexOf("StringValue")]);
						}
					}

					strMeasure.AutoPostBack=false;
					strMeasure.Enabled = true;
					strMeasure.ReadOnly=false;
					strMeasure.EnableViewState = true;
					strMeasure.ID="Measure_STRING_"+Session["Grade_MeasureID"] as String;
					strMeasure.TextChanged += new System.EventHandler(this.MeasureChanged_AutoPostBack);
					ActionPanel.Controls.Add(strMeasure);
					ActionPanel.Controls.Add(AddButton("Button_STRING_"+Session["Grade_MeasureID"] as String));
					break;
				}
				case 3 : //Numeric Value //MeasureValue
				{
					System.Web.UI.WebControls.TextBox strMeasure = new TextBox();
					strMeasure.CssClass="text";
					strMeasure.Width=75;
					if(!(Utils.NullValue(Session["Grade_ItemMeasureValues"])==""))
					{
						DataSet ds = Session["Grade_ItemMeasureValues"] as DataSet;
						dgDebug.DataSource=ds.Tables[0];
						dgDebug.DataBind();
						//"PartID = " +  lstItemStructure.SelectedValue + " AND MeasureID = " + lstMeasures.SelectedValue.Split('_')[1]
						//Session["Grade_PartID"]
						//DataRow[] drFiltered = ds.Tables[0].Select("MeasureID = " + Session["Grade_MeasureID"] as String);
						DataRow[] drFiltered = ds.Tables[0].Select("PartID = " +  (Session["Grade_PartID"] as String) + " AND MeasureID = " + Session["Grade_MeasureID"] as String);
						if(drFiltered.Length>0)
						{
							strMeasure.Text=Utils.NullValue(drFiltered[0].ItemArray[drFiltered[0].Table.Columns.IndexOf("MeasureValue")]);
						}
					}
					strMeasure.AutoPostBack=false;
					strMeasure.Enabled = true;
					strMeasure.ReadOnly = false;
					strMeasure.EnableViewState = true;
					strMeasure.ID="Measure_NUMBER_"+Session["Grade_MeasureID"] as String;
					strMeasure.TextChanged += new System.EventHandler(this.MeasureChanged_AutoPostBack);
					ActionPanel.Controls.Add(strMeasure);
					ActionPanel.Controls.Add(AddButton("Button_NUMBER_"+Session["Grade_MeasureID"] as String));
					break;
				}
				case 4 :
				{
					break;
				}
				case 5 :
				{
					break;
				}
			}
		}

		private System.Web.UI.WebControls.Button AddButton(String ButtonName)
		{
			System.Web.UI.WebControls.Button myButton = new Button();
			myButton.Text="GO";
			myButton.CssClass="buttonStyle";
			myButton.Click += new System.EventHandler(this.MeasureChanged_AutoPostBack);
			return myButton;
		}



		enum eCheckableMeasurements {
			Total_weight_ct = 2, 
			Measured_Weight_ct = 4, 
			Calculated_Weight = 5, 
			Min = 12, 
			Max = 11};

		private bool MeasureOK(float floatNewMeasureValue)
		{
			//this functionality cheks if the new measurememet differs too much from the existing value.

			///////this disables the check for now
			return true;
			///////disable-end

			string strGradePartID = Session["Grade_PartID"].ToString();
			int intGradePartID = int.Parse(strGradePartID);

			string strGradeMeasureID = Session["Grade_MeasureID"].ToString();
			int intGradeMeasureID = int.Parse(strGradeMeasureID);

			string strGradeMeasureClass = Session["Grade_MeasureClass"].ToString();
			int intGradeMeasureClass = int.Parse(strGradeMeasureClass);

			string strGradeMeasureCode = Session["Grade_MeasureCode"].ToString();
			int intGradeMeasureCode = int.Parse(strGradeMeasureCode);

			string strGadeMeasureName = Session["Gade_MeasureName"].ToString();
			
			DataSet ds = Session["Grade_ItemMeasureValues"] as DataSet;
//			dgDebug.DataSource=ds.Tables[0];
//			dgDebug.DataBind();

			string strCurrmeasureValue = "";

			DataRow[] drFiltered = ds.Tables[0].Select("PartID = " +  (Session["Grade_PartID"] as String) + " AND MeasureID = " + Session["Grade_MeasureID"] as String);
			if(drFiltered.Length>0)
			{
				strCurrmeasureValue=Utils.NullValue(drFiltered[0].ItemArray[drFiltered[0].Table.Columns.IndexOf("MeasureValue")]);
				
				float floatCurrentMeasureValue = float.Parse(strCurrmeasureValue);
				float flDiff = Math.Abs(floatCurrentMeasureValue - floatNewMeasureValue);
				Console.WriteLine(flDiff.ToString());
				if(flDiff>0.03)
				{
					return false;
				}
			}
			return true;
		}

		private void MeasureChanged_AutoPostBack(object sender, System.EventArgs e) //this is the entry point for saving changes in measurements.
		{
			AddEditedItemToList(Session["Grade_SelectedItem"] as SingleItem);
			//check here for current measure being measured weight
			string strCurrMeasureID = Session["Grade_MeasureID"].ToString();
			int intCurrMeasureID = int.Parse(strCurrMeasureID);

			if(sender.GetType().ToString()=="System.Web.UI.WebControls.DropDownList")
			{
				//lblChangedMeasureValue.Text=(sender as System.Web.UI.WebControls.DropDownList).SelectedItem.Text;
				SaveChange(int.Parse(Session["Grade_PartID"].ToString()),((Session["Grade_SelectedItem"]) as SingleItem).NewBatchID,((Session["Grade_SelectedItem"]) as SingleItem).NewItemCode, int.Parse(Session["Grade_MeasureCode"].ToString()), int.Parse((sender as System.Web.UI.WebControls.DropDownList).SelectedValue.Split('_')[0]));
				(ActionPanel.FindControl("lblMeasureState") as Label).Text = "Curr. value: " + (sender as System.Web.UI.WebControls.DropDownList).SelectedItem.Text+@"<br>";
				lblProcResult.Text="Change Saved";
			}
			else if((sender.GetType().ToString()=="System.Web.UI.WebControls.Button")||(sender.GetType().ToString()=="System.Web.UI.WebControls.TextBox"))
			{
				if(Utils.NullValue(ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String))!="")
				{
					if((ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String).GetType().ToString()=="System.Web.UI.WebControls.TextBox")&&((ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text.Trim()!=""))
					{
						//lblChangedMeasureValue.Text=(ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text;
						try
						{
							//check measrue for strange values
							bool blChangeIsOk = true;

							int intPartID = int.Parse(Session["Grade_PartID"].ToString());
							int intBatchID = ((Session["Grade_SelectedItem"]) as SingleItem).NewBatchID;
							int intItemCode = ((Session["Grade_SelectedItem"]) as SingleItem).NewItemCode;
							int intMeasureCode = int.Parse(Session["Grade_MeasureCode"].ToString());
							float floatMeasureValue = float.Parse((ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text.Trim());

							///SARIN CHECK IS FRUTHER BELOW
////							//checking for sarin-measured weight discrepancy
////							if((intCurrMeasureID == 6)||(intCurrMeasureID == 4))
////							{
////								blChangeIsOk = CheckSarinDiscrepancy(intCurrMeasureID, floatMeasureValue, intBatchID, intItemCode, intPartID);
////							}


							//checking here for measurement that is too far from the curren value
							if((intCurrMeasureID == 2)||(intCurrMeasureID == 4)||(intCurrMeasureID == 5)||(intCurrMeasureID == 12)||(intCurrMeasureID == 11))
							{
								bool blWeightOK = false;
                
								blWeightOK = MeasureOK( floatMeasureValue );               
				
								if(!blWeightOK)
								{
									Exception wrongMeasure = new Exception("Wrong Measure Difference");
									throw wrongMeasure;
								}
							}

 
							//private bool SaveChange(, , , )
							if(!blChangeIsOk)
							{
								//measurement differs too much. Notify user, do not update the value;
								return;
							}
							//end check measure for strange values 

							//saving float measurevalue here.
							SaveChange(int.Parse(Session["Grade_PartID"].ToString()),((Session["Grade_SelectedItem"]) as SingleItem).NewBatchID,((Session["Grade_SelectedItem"]) as SingleItem).NewItemCode, int.Parse(Session["Grade_MeasureCode"].ToString()), float.Parse((ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text.Trim()));
							lblProcResult.Text="Change Saved";
						}
						catch(Exception ex)
						{
							if(ex.Message=="Wrong Measure Difference") // new stuff here
							{
								lblProcResult.Text = ex.Message + @"<br>The measure entered differs <br>from the prev. value too much. <br>Please check it and try again.";
							}
							else
							{
								lblProcResult.Text = ex.Message + @"<br>Enter a number and try again.";
							}
						}
						
					}
					else
					{
						lblProcResult.Text = @"No text.<br> Enter a number and try again.";
					}
				}
				else
				{
					if(Utils.NullValue(ActionPanel.FindControl("Measure_STRING_"+Session["Grade_MeasureID"] as String))!="")
					{
						if(ActionPanel.FindControl("Measure_STRING_"+Session["Grade_MeasureID"] as String).GetType().ToString()=="System.Web.UI.WebControls.TextBox")
						{
							//lblChangedMeasureValue.Text=(ActionPanel.FindControl("Measure_STRING_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text;
							SaveChange(int.Parse(Session["Grade_PartID"].ToString()),((Session["Grade_SelectedItem"]) as SingleItem).NewBatchID,((Session["Grade_SelectedItem"]) as SingleItem).NewItemCode, int.Parse(Session["Grade_MeasureCode"].ToString()), (ActionPanel.FindControl("Measure_STRING_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text.Trim());
							lblProcResult.Text="Change Saved";
						}
						else
						{
							lblProcResult.Text = "No text<br> Enter data and try again";
						}
					}
					else
					{
						//debug/new code
						if(Utils.NullValue(ActionPanel.FindControl("Measure_ENUM_"+Session["Grade_MeasureID"] as String))!="")
						{							
							SaveChange(int.Parse(Session["Grade_PartID"].ToString()),((Session["Grade_SelectedItem"]) as SingleItem).NewBatchID,((Session["Grade_SelectedItem"]) as SingleItem).NewItemCode, int.Parse(Session["Grade_MeasureCode"].ToString()), int.Parse((ActionPanel.FindControl("Measure_ENUM_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.DropDownList).SelectedValue.Split('_')[0]));
							(ActionPanel.FindControl("lblMeasureState") as Label).Text = "Curr. value: " + (ActionPanel.FindControl("Measure_ENUM_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.DropDownList).SelectedItem.Text+@"<br>";
							lblProcResult.Text="Change Saved";
						}
						//debug-end/new code
//						lblProcResult.Text = "No Control<br> Notify <a href='mailto:oleg@gemscience.net'>Oleg</a>";
					}
				}
			}			
			
			if(intCurrMeasureID==4)
			{
				if(!CheckForSarinDiscrepancy(int.Parse(Session["Grade_PartID"].ToString()), Session["Grade_SelectedItem"] as SingleItem, float.Parse((ActionPanel.FindControl("Measure_NUMBER_"+Session["Grade_MeasureID"] as String) as System.Web.UI.WebControls.TextBox).Text.Trim())))
				{
					//weights does not match
					pnlIvalidItemMessage.Enabled=true;
					pnlIvalidItemMessage.Visible=true;

					ArrayList sal = Session["Grade_ItemList"] as ArrayList;
					(sal[lstBatch.SelectedIndex] as SingleItem).StateID = 10;

					//save status into the DB
					SqlCommand stageCommand = new SqlCommand("spSetItemStateByCode");
					stageCommand.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					stageCommand.CommandType=CommandType.StoredProcedure;
                    
					stageCommand.Parameters.Add(new SqlParameter("@rId",SqlDbType.VarChar,255));
					stageCommand.Parameters["@rId"].Direction=ParameterDirection.Output;
					stageCommand.Parameters.Add(new SqlParameter("@StateCode",3));

					stageCommand.Parameters.Add(new SqlParameter("@GroupCode",(Session["Grade_SelectedItem"] as SingleItem).OrderCode));
					stageCommand.Parameters.Add(new SqlParameter("@BatchCode",(Session["Grade_SelectedItem"] as SingleItem).BatchCode));
					stageCommand.Parameters.Add(new SqlParameter("@ItemCode",(Session["Grade_SelectedItem"] as SingleItem).ItemCode));
					stageCommand.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));
					stageCommand.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
					stageCommand.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

					stageCommand.Connection.Open();
					stageCommand.ExecuteNonQuery();
					stageCommand.Connection.Close();
					System.Console.WriteLine(stageCommand.Parameters["@rId"].ToString());
				}
				else
				{
					//weights match
					pnlIvalidItemMessage.Enabled=false;
					pnlIvalidItemMessage.Visible=false;
				}
			}
		}
	
		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("GDLight.aspx");
		}

		protected void cmdDoneWithBatch_Click(object sender, System.EventArgs e)
		{
			SaveTrackingInfo();
			txtItemNumber.AutoPostBack=true;
			txtItemNumber.ReadOnly=false;
			cmdLoad.Enabled=true;
			txtItemNumber.Text="";
			CleanUp();
		}

		private bool SaveChange(int partID, int batchID, int itemCode, int measureCode, float measureValue)
		{
			return SaveChange(partID, batchID, itemCode, measureCode, measureValue, int.MaxValue, null, ValueType.NUMBER);
		}

		private bool SaveChange(int partID, int batchID, int itemCode, int measureCode, int measureValueID)
		{
			return SaveChange(partID, batchID, itemCode, measureCode, float.NaN, measureValueID, null, ValueType.ENUM);
		}

		private bool SaveChange(int partID, int batchID, int itemCode, int measureCode, String stringValue)
		{
			return SaveChange(partID, batchID, itemCode, measureCode, float.NaN, int.MaxValue, stringValue, ValueType.STRING);
		}

		private bool SaveChange(int partID, int batchID, int itemCode, int measureCode, float measureValue, int measureValueID, String stringValue, int vt)
		{

			//dbo.spSetPartValue: 
			//				@AuthorId=19; 
			//				@AuthorOfficeId=1; 
			//				@CurrentOfficeId=1; 
			//				@rId=; 
			//				@ItemCode=1; 
			//				@BatchID=5917; 
			//				@PartID=55; 
			//				@MeasureCode=16; 
			//				@MeasureValue=17.61; 
			//				@MeasureValueID=; 
			//				@StringValue=; 
			//				@UseClosedRecheckSession=; 

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			SqlCommand command = new SqlCommand("spSetPartValueCCM");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;
			
			command.Parameters.Add("@rId", SqlDbType.Int);
			command.Parameters["@rId"].Direction=ParameterDirection.Output;
			command.Parameters["@rId"].Value=DBNull.Value;

			command.Parameters.Add("@AuthorID", Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@CurrentOfficeID",Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@ItemCode",itemCode.ToString());
			command.Parameters.Add("@BatchID", batchID.ToString());
			command.Parameters.Add("@PartID", partID.ToString());
			command.Parameters.Add("@MeasureCode", measureCode.ToString());
			command.Parameters.Add("@UseClosedRecheckSession",DBNull.Value);
			
			switch(vt)
			{
				case 10 :
				{
					command.Parameters.Add("@MeasureValue",DBNull.Value);
					command.Parameters.Add("@StringValue", DBNull.Value);
					command.Parameters.Add("@MeasureValueID", measureValueID);
					break;
				}
				case 30 : //Number
				{
					command.Parameters.Add("@MeasureValue",measureValue);
					command.Parameters.Add("@StringValue", DBNull.Value);
					command.Parameters.Add("@MeasureValueID", DBNull.Value);
					break;
				}
				case 20 : //String
				{
					command.Parameters.Add("@MeasureValue",DBNull.Value);
					command.Parameters.Add("@StringValue", stringValue);
					command.Parameters.Add("@MeasureValueID", DBNull.Value);
					break;
				}
				default :
				{
					return false;
				}
			}

			conn.Open();
			command.ExecuteNonQuery();
//			lblProcResult.Text=command.Parameters["@rId"].Value.ToString();
			conn.Close();

			/*
			 * @AuthorId=19; @AuthorOfficeId=1; @CurrentOfficeId=1; @rId=; @BatchID=44453; @ItemCode=1;
			 * */

			SqlCommand commandReCalc = new SqlCommand("spSetEstimatedValues");
			commandReCalc.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			commandReCalc.CommandType = CommandType.StoredProcedure;

			commandReCalc.Parameters.Add("@AuthorID", Session["ID"].ToString());
			commandReCalc.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			commandReCalc.Parameters.Add("@CurrentOfficeID",Session["AuthorOfficeID"].ToString());
			commandReCalc.Parameters.Add("@ItemCode",itemCode.ToString());
			commandReCalc.Parameters.Add("@BatchID", batchID.ToString());
			
			commandReCalc.Parameters.Add("@rId", SqlDbType.Int);
			commandReCalc.Parameters["@rId"].Direction=ParameterDirection.Output;
			commandReCalc.Parameters["@rId"].Value=DBNull.Value;

			commandReCalc.Connection.Open();
			commandReCalc.ExecuteNonQuery();
			commandReCalc.Connection.Close();

			UpdateItemValuesDataSet(batchID, itemCode);

			if(Utils.NullValue(command.Parameters["@rId"])!="")
			{
				return true;
			}

			return false;
		}
		private void UpdateItemValuesDataSet(int batchID, int itemCode)
		{
			SqlCommand command = new SqlCommand();
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

			command = new SqlCommand("spGetPartValue");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;
			command.Parameters.Add("@AuthorID", Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@BatchID",batchID);
			command.Parameters.Add("@ItemCode",itemCode);
			command.Parameters.Add("@PartID",DBNull.Value);
			command.Parameters.Add("@RecheckNumber","-1");
			//command.Parameters.Add("@ViewAccessCode",DBNull.Value);
			command.Parameters.Add("@ViewAccessCode",rblModeSelector.SelectedValue);

			DataSet ds;
			SqlDataAdapter da = new SqlDataAdapter();
			ds = new DataSet("ItemStuff");
			da = new SqlDataAdapter(command);
			da.Fill(ds);
			Session["Grade_ItemMeasureValues"]=ds.Copy();
		}

		protected void cmdMarkItem_Click(object sender, System.EventArgs e)
		{
			if(lstDone.Items.IndexOf(lstDone.Items.FindByValue(lstBatch.SelectedValue))==-1)
			{
				lstDone.SelectedIndex=-1;
				lstDone.Items.Add(new ListItem(lstBatch.SelectedItem.Text,lstBatch.SelectedItem.Value));
				lstBatch.SelectedItem.Text=lstBatch.SelectedItem.Text+"*";
			}
			else
			{
				lstBatch.SelectedItem.Text=lstBatch.SelectedItem.Text.TrimEnd('*');
				lstDone.Items.RemoveAt(lstDone.Items.IndexOf(lstDone.Items.FindByValue(lstBatch.SelectedValue)));
			}			
		}

		protected void cmdSARIN_Click(object sender, System.EventArgs e)
		{
			if(!(lstBatch.Items.Count>0))
			{
				lblSarinMessage.Text="Sarint NOT done : have to have a batch selected";
			}
			else
			{
				lblSarinMessage.Text="Do The SARIN...";
				//Do the Sarin Shtik.
				Corpt.jupiter.GdrService jup;
				jup = new Corpt.jupiter.GdrService();
#if DEBUG
//				jup.Url="http://printing/gemoDream/gdrService.asmx";
#endif
				string loginToken;
				//Log in GemoService
				DataSet o = new DataSet();
				loginToken = jup.Login(Session["LoginName"].ToString(),Session["PasswordString"].ToString(),1,1, out o);
				if(loginToken!="")
				{
					lblSarinMessage.Text=loginToken+"<br>";					
					string graderDir = jup.GetServiceCfgParameter(loginToken, "graderDir");		
					//lblSarinMessage.Text=graderDir;
					lblSarinMessage.Text="";
					//Sarin per file here.
					for(int m = 0; m < lstBatch.Items.Count; m++)
					{						
						SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
						SqlCommand command = new SqlCommand("spGetPartsByItemType");
						command.Connection = conn;
						command.CommandType = CommandType.StoredProcedure;
						command.Parameters.Add("@AuthorID", Session["ID"].ToString());
						command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
						command.Parameters.Add("@ItemTypeID",((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).ItemTypeID);
						DataTable dt = new DataTable();
						SqlDataAdapter da;
						da = new SqlDataAdapter(command);
						da.Fill(dt);

						//Get All Set Measures for selected item.
						command = new SqlCommand("spGetPartValue");
						command.Connection = conn;
						command.CommandType = CommandType.StoredProcedure;
						command.Parameters.Add("@AuthorID", Session["ID"].ToString());
						command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
						command.Parameters.Add("@BatchID",((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).NewBatchID);
						command.Parameters.Add("@ItemCode",((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).NewItemCode);
						command.Parameters.Add("@PartID",DBNull.Value);
						command.Parameters.Add("@RecheckNumber","-1");
						command.Parameters.Add("@ViewAccessCode",DBNull.Value);
						//command.Parameters.Add("@ViewAccessCode",rblModeSelector.SelectedValue);

						DataSet ds;
						ds = new DataSet("ItemStuff");
						da = new SqlDataAdapter(command);
						da.Fill(ds);

						Session["Grade_ItemMeasureValues"]=ds.Copy();
						Session["Grade_SelectedItem"]=(Session["Grade_ItemList"] as ArrayList)[m] as SingleItem;

						lblSarinMessage.Text+=((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).OrderCode + "." + ((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).BatchCode + "." +((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).ItemCode;

						DataTable dt112 = new DataTable();
						foreach(DataColumn dc in ds.Tables[0].Columns)
						{
							dt112.Columns.Add(dc.ColumnName);
						}			
						
						if(ds.Tables[0].Select("MeasureID = 112").Length!=0) //prefix
						{
							foreach(DataRow edr in ds.Tables[0].Select("MeasureID = 112"))
							{
								dt112.Rows.Add(edr.ItemArray); //partid, stringvalue
								string prefixFileName;
								prefixFileName = edr["StringValue"].ToString();
								string itemNameFileName;
								itemNameFileName = ((Session["Grade_ItemList"] as ArrayList)[m] as SingleItem).ToString();
								string res = "";
								string resFileName = graderDir+prefixFileName+".ou1;"+graderDir+itemNameFileName+".ou1";
								try
								{
									jup.AddGraderData(loginToken,resFileName,edr["PartID"].ToString(),out res);
								}
								catch(Exception ex)
								{
									res="File not found";
								}
								if(res=="")
								{
									lblSarinMessage.Text+=" : Done <br>";
								}
								else
								{
									lblSarinMessage.Text+= res + "<br>";
								}

							}
//							DataGrid2.DataSource=dt112;
//							DataGrid2.DataBind();
						}

						lblSarinMessage.Text+="<br>";

//						DataGrid1.DataSource=ds;
//						DataGrid1.DataBind();
					}
					try
					{										 
						jup.Logoff(loginToken);
					}
					catch(Exception ex)
					{
					}
				}
				else
				{
					lblSarinMessage.Text="SARIN NOT DONE: Could not login";
				}
			}
		}

		protected void cmdCloseWarning_Click(object sender, System.EventArgs e)
		{
			pnlIvalidItemMessage.Visible=false;
			pnlIvalidItemMessage.Enabled=false;
		}

		private bool CheckForSarinDiscrepancy(int intPartID, SingleItem siCurrentItem, double mw)
		{
			DataSet dsItemMeasureValues = Session["Grade_ItemMeasureValues"] as DataSet;
//////			Debug			
//////			DataGrid3.DataSource=dsItemMeasureValues.Tables[0];
//////			DataGrid3.DataBind();

			if(dsItemMeasureValues.Tables.Count==1)
			{
				DataRow[] drs = dsItemMeasureValues.Tables[0].Select("MeasureID = '6' and PartID = '" + intPartID + "'");
				foreach(DataRow drExisting in drs)
				{
					//Have SARIN measure - lets compare;
					string strMeasureValue = drExisting["MeasureValue"].ToString();
					double srnw = double.Parse(strMeasureValue);
					if(Math.Abs(srnw-mw)>0.011)
					{
						return false;
					}
				}
			}
			return true;
		}

		private void AddTouchedItemToList(SingleItem si)
		{
			SortedList sl = new SortedList();
			if(Utils.NullValue(Session["TouchedItemList"])=="")
			{				
				sl.Add(si.ToString(),si);
			}
			else
			{
				sl = Session["TouchedItemList"] as SortedList;
				try
				{
					sl.Add(si.ToString(),si);
				}
				catch
				{}
			}
//			DisplayDebugList(sl);
			Session["TouchedItemList"] = sl;
		}

		private void AddEditedItemToList(SingleItem si)
		{
			SortedList sl = new SortedList();
			if(Utils.NullValue(Session["EditedItemList"])=="")
			{				
				sl.Add(si.ToString(),si);
			}
			else
			{
				sl = Session["EditedItemList"] as SortedList;
				try
				{
					sl.Add(si.ToString(),si);
				}
				catch
				{}
			}
			Session["EditedItemList"] = sl;
		}

        //-- done
		private void SaveTrackingInfo()
		{
			if(Utils.NullValue(Session["TouchedItemList"])!="")
			{//have list of touched items.
				var sl = Session["TouchedItemList"] as SortedList;
                if (sl == null) return;
				var si = sl[sl.GetKey(0)] as SingleItem;
                if (si == null) return;
			    QueryUtils.UpdateTracking(
			        new TrackingUpdateModel
			            {
                            BatchId = si.NewBatchID,
			                EventId = DbConstants.TrkEventTouched,
			                ViewAccess = DbConstants.ViewAccessGdLightGradeId,
                            ItemsAffected = sl.Count,
                            ItemsInBatch = 0
			            }, this);
			}
			if(Utils.NullValue(Session["EditedItemList"])!="")
			{//have a list of edited items.
				var sl = Session["EditedItemList"] as SortedList;
                if (sl == null) return;
                var si = sl[sl.GetKey(0)] as SingleItem;
                if (si == null) return;
                QueryUtils.UpdateTracking(
                    new TrackingUpdateModel
                    {
                        BatchId = si.NewBatchID,
                        EventId = DbConstants.TrkEventUpdated,
                        ViewAccess = DbConstants.ViewAccessGdLightGradeId,
                        ItemsAffected = sl.Count,
                        ItemsInBatch = 0
                    }, this);
			}
		}

	}
}
