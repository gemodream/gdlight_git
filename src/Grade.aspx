<%@ Page language="c#" Codebehind="Grade.aspx.cs" AutoEventWireup="True" Inherits="Corpt.Grade" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Grade</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript">
			window.onload = unloadPage();
		</script>
		<script type="text/javascript" src="Utils.js"></script>
	</HEAD>
	<body onload="document.Form1.txtItemNumber.focus(); countDown();" bgColor="whitesmoke"
		MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:textbox style="Z-INDEX: 101; POSITION: absolute; TOP: 16px; LEFT: 16px" id="txtItemNumber"
				runat="server" Width="112px" CssClass="inputStyleCC" AutoPostBack="True" ontextchanged="txtItemNumber_TextChanged"></asp:textbox><asp:label style="Z-INDEX: 121; POSITION: absolute; TOP: 728px; LEFT: 32px" id="lblDebug2"
				runat="server" CssClass="Text"></asp:label><asp:panel style="Z-INDEX: 119; POSITION: absolute; TOP: 136px; LEFT: 64px" id="pnlIvalidItemMessage"
				runat="server" Width="600px" CssClass="text" HorizontalAlign="Center" BorderColor="Silver" Visible="False" BorderStyle="Solid" BackColor="#FFC0C0"
				Font-Size="X-Large" Height="176px" Enabled="False">
				<P>Item is
					<asp:Label id="lblWarningPanel" runat="server"></asp:Label>&nbsp;"Invalid". 
					Please check the weight and/or ld/ff/km.</P>
				<P>
					<asp:Button id="cmdCloseWarning" runat="server" Text="Close" onclick="cmdCloseWarning_Click"></asp:Button></P>
			</asp:panel><asp:listbox style="Z-INDEX: 114; POSITION: absolute; TOP: 112px; LEFT: 824px" id="lstDone" runat="server"
				Width="112px" CssClass="text" AutoPostBack="True" Height="432px" Enabled="False"></asp:listbox><asp:button style="Z-INDEX: 113; POSITION: absolute; TOP: 80px; LEFT: 824px" id="cmdMarkItem"
				runat="server" Width="112px" CssClass="buttonStyle" Text='Toggle "Done"' onclick="cmdMarkItem_Click"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 16px; LEFT: 144px" id="cmdLoad" runat="server"
				CssClass="buttonStyle" Text="Load" onclick="cmdLoad_Click"></asp:button><asp:listbox style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 16px" id="lstBatch" runat="server"
				Width="112px" CssClass="text" AutoPostBack="True" Height="432px" onselectedindexchanged="lstBatch_SelectedIndexChanged"></asp:listbox><asp:panel style="Z-INDEX: 104; POSITION: absolute; TOP: 80px; LEFT: 600px" id="ActionPanel"
				runat="server" Width="216px" Height="32px" EnableViewState="False">
				<P>
					<asp:Label id="lblUpdated" runat="server" CssClass="text"></asp:Label></P>
			</asp:panel>
            <asp:listbox style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 136px; right: 772px;" id="lstItemStructure"
				runat="server" Width="256px" CssClass="text" AutoPostBack="True" Height="168px" 
                onselectedindexchanged="lstItemStructure_SelectedIndexChanged"></asp:listbox><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 48px; LEFT: 144px" id="lblItemNumber"
				runat="server" CssClass="Text" Font-Size="Larger"></asp:label><asp:listbox style="Z-INDEX: 107; POSITION: absolute; TOP: 80px; LEFT: 400px" id="lstMeasures"
				runat="server" Width="192px" CssClass="text" AutoPostBack="True" Height="400px" onselectedindexchanged="lstMeasures_SelectedIndexChanged"></asp:listbox><asp:radiobuttonlist style="Z-INDEX: 108; POSITION: absolute; TOP: 16px; LEFT: 200px" id="rblModeSelector"
				runat="server" CssClass="text" AutoPostBack="True" RepeatDirection="Horizontal">
				<asp:ListItem Value="6" Selected="True">Measure</asp:ListItem>
				<asp:ListItem Value="5">Color</asp:ListItem>
				<asp:ListItem Value="4">Clarity</asp:ListItem>
			</asp:radiobuttonlist><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 0px; LEFT: 0px" id="cmdHome" runat="server"
				Width="16px" CssClass="buttonStyle" Height="16px" Text="H" onclick="cmdHome_Click"></asp:button><asp:button style="Z-INDEX: 110; POSITION: absolute; TOP: 528px; LEFT: 40px" id="cmdDoneWithBatch"
				runat="server" CssClass="buttonStyle" Text="Done with Batch" onclick="cmdDoneWithBatch_Click"></asp:button><asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 240px; LEFT: 600px" id="lblProcResult"
				runat="server" CssClass="text" Font-Underline="True" Font-Bold="True"></asp:label><asp:image style="Z-INDEX: 112; POSITION: absolute; TOP: 256px; LEFT: 160px" id="ItemPicture"
				runat="server" Width="100px" Visible="False" BorderStyle="None" BackColor="#E0E0E0" AlternateText="Item Picutre" ImageAlign="Top"></asp:image>
            <asp:label style="Z-INDEX: 115; POSITION: absolute; TOP: 96px; " id="Label1" runat="server"
				CssClass="text">Done:</asp:label><asp:button style="Z-INDEX: 116; POSITION: absolute; TOP: 480px; LEFT: 24px" id="cmdSARIN" runat="server"
				Width="96px" CssClass="buttonStyle" Text="Import SARIN" onclick="cmdSARIN_Click"></asp:button><asp:label style="Z-INDEX: 117; POSITION: absolute; TOP: 520px; LEFT: 192px" id="lblSarinMessage"
				runat="server" Width="400px" Height="16px"></asp:label><asp:datagrid style="Z-INDEX: 118; POSITION: absolute; TOP: 1504px; LEFT: 32px" id="DataGrid2"
				runat="server"></asp:datagrid><asp:label style="Z-INDEX: 120; POSITION: absolute; TOP: 704px; LEFT: 32px" id="lblDebug1"
				runat="server" CssClass="Text"></asp:label><asp:datagrid style="Z-INDEX: 122; POSITION: absolute; TOP: 760px; LEFT: 32px" id="dgDebug" runat="server"
				CssClass="text"></asp:datagrid><asp:label style="Z-INDEX: 123; POSITION: absolute; TOP: 32px; LEFT: 560px" id="theTime" runat="server"
				name="theTime">Label</asp:label></form>
	</body>
</HTML>
