﻿using System;
using System.Net;
using Corpt.Constants;

namespace Corpt
{
    public partial class PDFRedirect : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
            else
            {
                //lblInfo.Text = "";
                var mode = Request.Params["Mode"];
                if (mode == "View")  PerformViewRedirect();
                if (mode == "DownLoad") PerformDownload();
            }

        }
        private void PerformDownload()
        {
            var fileName = Request.Params["FileName"];
            var dir = Server.MapPath(GetDirName());
            Response.ContentType = "Application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.TransmitFile(dir + fileName);
            Response.End(); 
        }
        private string GetDirName()
        {
            var dirName = "" + Session[SessionConstants.GlobalDocumentRoot];
            if (dirName.EndsWith("\\") || dirName.EndsWith("/")) return dirName;
            return dirName + "/";
            
        }
        private void PerformViewRedirect()
        {
            var fileName = Request.Params["FileName"];
            if (!string.IsNullOrEmpty(fileName))
            {
                var dir = "" + Session[SessionConstants.GlobalDocumentRoot];
                var hasEnd = dir.EndsWith("\\") || dir.EndsWith("/");
                if (!hasEnd) dir += "/";

                var filePath = Server.MapPath(dir) + fileName;
                var webClient = new WebClient();

                Byte[] fileBuffer = webClient.DownloadData(filePath);

                if (fileBuffer != null)
                {

                    Response.ContentType = "application/pdf";

                    Response.AddHeader("content-length", fileBuffer.Length.ToString());

                    Response.BinaryWrite(fileBuffer);

                }

            }

        }
    }
}