﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.Utilities;

namespace Corpt
{
    public class Utlities
    {
        #region Create ShortReportModel 
        public static DataTable CreateTableVertic(DataTable table)
        {
            var result = new DataTable("Short Report");
            result.Columns.Add(new DataColumn("Title"));
            foreach (DataRow row in table.Rows)
            {
                var colName = ("" + row[0]);
                colName = Regex.Replace(colName, @"<[^>]+>", "");
                result.Columns.Add(new DataColumn(colName));
            }
            for (var i = 1; i < table.Columns.Count; i++)
            {
                var newRow = result.NewRow();
                newRow["Title"] = table.Columns[i].Caption;
                result.Rows.Add(newRow);
            }
            result.AcceptChanges();
            for (int r = 0; r < table.Rows.Count; r++)
            {
                for (int c = 1; c < table.Columns.Count; c++)
                {
                    var val = table.Rows[r][c];
                    result.Rows[c - 1][r + 1] = val;
                }
            }
            result.AcceptChanges();
            return result;
        }
        public static ShortReportModel CreateShortReportModelWithDocStruct(BatchModel batchModel, Page p)
        {
            var docId = QueryUtils.GetDocumentId(batchModel, p);
            if (docId == "0")
            {
                return null;
            }
            return new ShortReportModel
                      {
                        BatchModel = batchModel,
                        DocumentId = docId,
                        DocStructure = QueryUtils.GetDocStructure(docId, p)
                      };
        }
        public static ShortReportModel GetShortReportData(BatchModel batchModel, Page p)
        {
            var shortReportModel = new ShortReportModel {BatchModel = batchModel};

            //-- DocumentId
            shortReportModel.DocumentId = QueryUtils.GetDocumentId(batchModel, p);
            
            //-- Document Structure
            shortReportModel.DocStructure = QueryUtils.GetDocStructure(shortReportModel.DocumentId, p);

            //-- Failed Items
            shortReportModel.ItemsFailed = QueryUtils.GetItemsFailed(shortReportModel.BatchModel, p);

            //-- Items and MovedItems
            QueryUtils.GetItemsAndMovedItems(shortReportModel, p);

            //-- Values Items
            shortReportModel.ItemValues = QueryUtils.GetItemValues(shortReportModel.BatchModel, p);

            //--  Report View
            CreateMergedItemStructValueTable(shortReportModel);

            return shortReportModel;
        }
        public static void CreateMergedItemStructValueTable(ShortReportModel shortReportModel)
        {
            var table = new DataTable();
            //-- Create HLink column on ViewItem Details page
            table.Columns.Add(PageConstants.ColNameLinkOnItemView);

            //-- Create Columns by Document Structure
            table.Columns.Add(new DataColumn("Result"));
            foreach (DocStructModel docModel in shortReportModel.DocStructure)
            {
                if (!table.Columns.Contains(docModel.Title))
                {
                    table.Columns.Add(new DataColumn(docModel.Title));
                }
            }
            table.AcceptChanges();

            //-- Set data in columns
            foreach(ItemModel itemModel in shortReportModel.Items)
            {
                var strNewItemNumber = itemModel.FullItemNumber;
                var row = table.NewRow();
                //-- Add Hlink on ItemView
                var sLink = string.Format(PageConstants.LinkOnPageItemViewDetail,
                    shortReportModel.BatchModel.BatchId, itemModel.ItemCode, itemModel.FullItemNumberWithDotes);
                row[PageConstants.ColNameLinkOnItemView] = "<a href=" + sLink + ">" + itemModel.FullItemNumberWithDotes + "</a>";

                foreach(DocStructModel docStructModel in shortReportModel.DocStructure)
                {
                    if(docStructModel.IsCutGrade)
                    {
                        var sPartName = GetPartName(docStructModel.Value);
                        var sPartId = ParsePartIdValue(
                            docStructModel.Value, strNewItemNumber, 
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);
                        var myRequest =
                            "<a href=\"CutGradeDetail.aspx?par1=" + itemModel.OrderCode +
                            "&par2=" + itemModel.BatchCode +
                            "&par3=" + itemModel.ItemCode +
                            "&par4=" + sPartId +
                            "&par5=" + sPartName + "\">" +
                            ParseValue(docStructModel.Value, strNewItemNumber, shortReportModel.ItemValues, shortReportModel.ItemsFailed) +
                            "</a>";
                        row[docStructModel.Title] = myRequest; 
                    } else
                    {
                        row[docStructModel.Title] = ParseValue(
                            docStructModel.Value, strNewItemNumber, 
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);

                    }
                }
                
                table.Rows.Add(row);
            }
            //-- Result column value
            SetFailColumn(table);
            shortReportModel.ReportView = table.Copy();
        }
        
        public static void SetFailColumn(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                for(int i=0; i < table.Columns.Count; i++)
                {
                    string val = row[i].ToString().ToLower();
                    if (val.IndexOf("highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        row["Result"] =  @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                        row.AcceptChanges();
                        break;
                    }
                }
            }
        }
        #endregion

        #region Parse Document Structure Value
        public static string ParseSingleValue(string strPartName, string strPropertyName, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            if (strPropertyName.ToLower() == "item #")
            {
                //get old item number
                var itemNames = itemValueList.FindAll(model => model.NewItemNumber == strItemNumber);
                return itemNames[0].OldItemNumber;
            }
            var drValues = itemValueList.FindAll( model =>
                model.NewItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drValues.Length == 0 || drValues[0].MeasureCode == DbConstants.MeasureCodeInternalComment) //-- TODO
            {
                return "";
            }
            //check here for cprules and mark accordingly
            var strResultValue = drValues[0].ResultValue;
            var drCp = itemFailedList.FindAll(model =>
                model.ItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drCp.Length > 0)
            {
                strResultValue = "<SPAN class=\"text_highlitedyellow\">" + strResultValue + "</SPAN>";
            }

            return strResultValue;
        }

        public static string ParseValue(string strValue, string strItemNumber, List<ItemValueModel> dtItemValueList, List<ItemFailedModel> dtCpResult)
        {
            var strResult = "";
            var strPartName = "";
            var strPropertyName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (char ch in strValue)
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    strPropertyName += ch;
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    insidePropertyName = false;
                    strResult += ParseSingleValue(strPartName, strPropertyName, strItemNumber, dtItemValueList, dtCpResult);
                    strPartName = "";
                    strPropertyName = "";
                }
            }
            return strResult;
        }

        public static string GetPartName(string strValue) //, string strItemNumber, DataTable itemValueList, DataTable itemFailedList)
        {
            var strResult = "";
            var strPartName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (var ch in strValue.ToCharArray())
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    strResult = strPartName;
                    break;
                }
            }
            return strResult;
        }
        public static string ParsePartIdValue(string strValue, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            var strResult = "";
            var strPartName = "";
            var strPropertyName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (var ch in strValue.ToCharArray())
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    strPropertyName += ch;
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    insidePropertyName = false;
                    strResult += ParsePartId(strPartName, strPropertyName, strItemNumber, itemValueList, itemFailedList);
                    strPartName = "";
                    strPropertyName = "";
                }
            }
            return strResult;
        }
        public static string ParsePartId(string strPartName, string strPropertyName, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            if (strPropertyName.ToLower() == "item #")
            {
                //get old item number
                var itemNames = itemValueList.FindAll(model => model.NewItemNumber == strItemNumber).ToArray();
                return itemNames[0].OldItemNumber;
            }
            var drValues = itemValueList.FindAll(model =>
                model.NewItemNumber == strItemNumber &&
                model.PartName == strPartName &&
                model.MeasureName == strPropertyName).ToArray();
            if (drValues.Length == 0)
            {
                return "0";
            }
            if (drValues.Length > 1)
            {
                var msg = string.Format("Duplication item value: NewItemNumber {0}, PartName {1}, MeasureName {2}",
                                        strItemNumber, strPartName, strPropertyName);
                throw new Exception(msg);
            }
            return drValues[0].PartId;
        }

        public static String ParametersToValue(String myValue, String myBatchId, String myItemCode, List<ItemStructModel> itemStructList, List<ItemValueModel> itemValueList, Page p)
        {
            var r = new Regex(@"\[[\d,\w,/\\,\s,),(,#]*[.][\d,\w,/\\,\s,),(,#]*\]"); //accounts for numbers, dashes, spaces
            var mc = r.Matches(myValue);

            if (mc.Count == 0)
            {
                return myValue;
            }
            var replacement = ReplacementDataString(mc[0].Value, myBatchId, myItemCode, p, itemStructList, itemValueList);
            var newValue = r.Replace(myValue, replacement, 1, 0);
            return ParametersToValue(newValue, myBatchId, myItemCode, itemStructList, itemValueList, p);
        }
       
        public static String ReplacementDataString(String myMetaData, String batchId, String itemCode, Page p,
            List<ItemStructModel> itemStructList, List<ItemValueModel> itemValueList)
        {
            //check for correct part name first
            var metaPartName = myMetaData.Trim('[').Trim(']').Split('.')[0].ToUpper();
            var metaMeasure = myMetaData.Trim('[').Trim(']').Split('.')[1].ToUpper();
            
            if (itemStructList.FindAll(model => model.PartName.ToUpper() == metaPartName).Count == 0)
            {
                return "<SPAN class=text_highlitedyellow>wrong structure</SPAN>";
            }


            if (metaMeasure == "Item #")
            {
                return QueryUtils.GetOldItemNumber(batchId, itemCode, p);
            }

            var myValueList =
                itemValueList.FindAll(
                    model => model.PartName.ToUpper() == metaPartName && model.MeasureName.ToUpper() == metaMeasure);
            if (myValueList.Count == 0)
            {
                return "";// "<strong>No measure </strong>";
            }
            if (!myValueList[0].IsCpOne)
            {
                return "<SPAN class=text_highlitedyellow>" + myValueList[0].ResultValue + "</SPAN>";
            }
            return myValueList[0].ResultValue;
        }

        public static bool GetPictureImageUrl(string dbPicture, out string imagePath, out string errMsg, Page p)
        {
            var rootDir = "" + p.Session[SessionConstants.GlobalPictureRoot];
            imagePath = rootDir + dbPicture;
            if (File.Exists(p.MapPath(imagePath)))
            {
                errMsg = "";
                return true;
            }
            errMsg = "File " + imagePath + " does not exist.";
            return false;
        }
        private static void ConvertWmfToPng(string path, string pathGif, out string saveErr)
        {
            saveErr = "";
            Image img = Image.FromFile(path);

            int width = Convert.ToInt32(img.Width * 0.01 * 10 );
            int height = Convert.ToInt32(img.Height * 0.01 * 10);
            Image bitmap = new Bitmap(width, height, img.PixelFormat);
            
            Graphics g = Graphics.FromImage(bitmap);
            g.Clear(Color.White);
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var rect = new Rectangle(0, 0, width, height);
            g.DrawImage(img, rect);
            try
            {
                Thread.Sleep(500);
                bitmap.Save(pathGif, ImageFormat.Png);
                saveErr = "";
            } catch (Exception ex)
            {
                saveErr = ex.Message;
            }
//            bitmap.Dispose();
//            GC.Collect();
        }
        public static bool GetShapeImageUrl(string dbShape, out string imagePath, out string errMsg, Page p, out string pngPath)
        {
            string saveErr;
            var rootDir = "" + p.Session[SessionConstants.GlobalShapeRoot];
            var rootPngDir = "" + p.Session[SessionConstants.GlobalShapePngRoot];
            imagePath = rootDir + dbShape;
            if (!Directory.Exists(p.MapPath(rootPngDir)))
            {
                errMsg = "Convert dir " + rootPngDir + " does not exists.";
                pngPath = imagePath;
                return false;
            }
            
            if (File.Exists(p.MapPath(imagePath)))
            {
                var ps = dbShape.Split('\\');
                var wmfName = ps[ps.Length - 1];
                if (wmfName.IndexOf(".wmf", StringComparison.Ordinal) != -1)
                {
                    wmfName = wmfName.Split('.')[0] + ".png";
                }
                pngPath = rootPngDir + "/" + wmfName;

                ConvertWmfToPng(p.MapPath(imagePath), p.MapPath(pngPath), out errMsg);
                if (!string.IsNullOrEmpty(errMsg))
                {
                    return false;
                }
                errMsg = "";
                return true;
            }
            errMsg = "File " + imagePath + " does not exist.";
            pngPath = "";
            return false;
        }
        #endregion


        #region Define Document, CorelDraw files
        public static List<CorelFileModel> GetCorelDrawFiles(Page p, out string errMsg)
        {
            var fileList = new List<CorelFileModel>();
            var corelFileDir = p.Session[SessionConstants.GlobalRepDir] + "CorelFiles\\";
            if (!Directory.Exists(p.MapPath(corelFileDir)))
            {
                errMsg = String.Format("Can't load corel files. Directory {0} is not exists or access denied. Please, check the directory.",
                    p.MapPath(corelFileDir));
                return fileList;

            }
            errMsg = "";
            var di = new DirectoryInfo(p.MapPath(corelFileDir));
            var files = di.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Extension.ToUpper() == ".JPG" || file.Extension.ToUpper() == ".DB") continue;
                fileList.Add(new CorelFileModel(file.Name, true));
            }

            return fileList;

            //ArrayList lstFileNames = gemoDream.Service.GetFileList(sCorelFilesPath);
        }
        #endregion
    }

}