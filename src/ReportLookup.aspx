<%@ Page language="c#"  MasterPageFile="~/DefaultMaster.Master" Codebehind="ReportLookup.aspx.cs" AutoEventWireup="True" Inherits="Corpt.ReportLookup" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
<style>
 .table tr:hover
            {
            background-color: #b8d1f3;    
            }
</style>

<div class="demoarea">
    <div class="demoheading">Report Lookup</div>
    <table>
        <tr>
            <td>By Item Number: </td>
            <td>
                <asp:Panel runat="server" DefaultButton="ByItemNumberBtn">
                    <asp:TextBox runat="server" ID="ItemNumberField"></asp:TextBox>
                    <asp:ImageButton ID="ByItemNumberBtn" runat="server" ToolTip="Search by Item Number" ImageUrl="~/Images/ajaxImages/search16.png"
                        OnClick="OnSearchByItemNumberClick" />
                </asp:Panel>
            </td>
            <td style="padding-left: 20px"><asp:Label runat="server" ID="VirtualVault1" ToolTip="Virtual Vault Number"></asp:Label></td>
            <td></td>
            <td style="padding-left: 20px"><asp:Label runat="server" ID="ErrLabel1" ForeColor="Red"></asp:Label></td>
        </tr>
<%--        <tr>--%>
<%--            <td>By [a-z] + Item Number: </td>--%>
<%--            <td>--%>
<%--                <asp:Panel ID="Panel1" runat="server" DefaultButton="ByCharItemNumberBtn">--%>
<%--                    <asp:TextBox runat="server" ID="CharItemNumberField"></asp:TextBox>--%>
<%--                    <asp:ImageButton ID="ByCharItemNumberBtn" runat="server" ToolTip="Search by Char + ItemNumber" ImageUrl="~/Images/ajaxImages/search16.png"--%>
<%--                        OnClick="OnSearchByCharItemNumberClick" />--%>
<%--                </asp:Panel>--%>
<%--            </td>--%>
<%--            <td style="padding-left: 20px"><asp:Label runat="server" ID="VirtualVault2" ToolTip="Virtual Vault Number"></asp:Label></td>--%>
<%--            <td></td>--%>
<%--            <td style="padding-left: 20px"><asp:Label runat="server" ID="ErrLabel2" ForeColor="Red"></asp:Label></td>--%>
<%--        </tr>--%>
<%--        <tr>--%>
<%--            <td>By GNumber (HEX): </td>--%>
<%--            <td>--%>
<%--                <asp:Panel ID="Panel2" runat="server" DefaultButton="ByHexGNumberBtn">--%>
<%--                    <asp:TextBox runat="server" ID="GNumberField"></asp:TextBox>--%>
<%--                    <asp:ImageButton ID="ByHexGNumberBtn" runat="server" ToolTip="Search by GNumber (HEX)" ImageUrl="~/Images/ajaxImages/search16.png"--%>
<%--                        OnClick="OnSearchByGNumberClick" />--%>
<%--                </asp:Panel>--%>
<%--            </td>--%>
<%--            <td style="padding-left: 20px"><asp:Label runat="server" ID="VirtualVault3" ToolTip="Virtual Vault Number"></asp:Label></td>--%>
<%--            <td style="padding-left: 20px"><asp:Label runat="server" ID="ItemNumber3" ToolTip="Item Number"></asp:Label></td>--%>
<%--            <td style="padding-left: 20px"><asp:Label runat="server" ID="ErrLabel3" ForeColor="Red"></asp:Label></td>--%>
<%--            <td> --%>
<%--                &nbsp;</td>--%>
<%--        </tr>--%>
    </table>

    <!-- Error message-->
    <div class="control-group error">
        <asp:Label class="control-label" id="InfoLabel" runat="server" />
    </div>
        
    <!-- Item Numbers DataGrid-->
    <div style="font-size: small">
        <asp:DataGrid runat="server" ID="FileGrid"  AutoGenerateColumns="False" CellPadding="5">
            <Columns>
                <asp:BoundColumn DataField="DisplayFile" HeaderText="File Name">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DisplayDate" HeaderText="Date modified">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DownLoad" HeaderText="Download"></asp:BoundColumn>
            </Columns>
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
        </asp:DataGrid>
    </div>

    
    <div class="row" align="right">
        <asp:hyperlink id="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx" >Back</asp:hyperlink>
    </div>
</div>
</asp:Content>