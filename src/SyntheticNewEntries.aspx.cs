﻿using Corpt.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;
using Corpt.TreeModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Collections;
using System.Text;

namespace Corpt
{
    public partial class SyntheticNewEntries : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnPrintLabel);
            scriptManager.RegisterPostBackControl(this.btnPrintItemLabel);
            scriptManager.RegisterPostBackControl(this.btnPrintNewItemLabel);
            Page.Title = "GSI: Synthetic New Entries";
            OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
            OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

            if (!IsPostBack)
            {
                CleanUp();
                SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
                Session["SyntNewEntriesFields"] = null;
            }
            string targetId = Page.Request.Params.Get("__EVENTTARGET");
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "focusthis", "document.getElementById('" + targetId + "').focus()", true);
            if (!string.IsNullOrEmpty(targetId))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "focusthis", "document.getElementById('" + targetId + "').focus()", true);
            }
            return;
            // Session["PassedData"] = null;
            //ShowHideItemDetails(true);
            //CreateMeasurePanel();
            //SynthNumberBox.Focus();
            InvalidLabel.Visible = true;
            if (Session["SyntNewEntriesFields"] != null)
            {
                //TextBox field = (TextBox)Session["SyntNewEntriesFields"];
                //field.Focus();
                string field = (string)Session["SyntNewEntriesFields"];
                if (field == "synth")
                    //SynthNumberBox.Focus();
                    Page.SetFocus(SynthNumberBox);
                else
                    txtOrderNumber.Focus();
                Session["SyntNewEntriesFields"] = null;
                
            }
            else
                SynthNumberBox.Focus();
        }
        #endregion

        #region CleanUp (on PageLoad, on Load button click)
        private void CleanUp()
        {
            CleanItemListArea();
            CleanPartTreeArea();
            //HidePicturePanel();//TODO modify this - done, see line below
            //HideShapePanel();
            ShowHideImages(false);
            CleanValueGridArea();
            CleanViewState();

            InvalidLabel.Text = "";
        }
        private void CleanItemListArea()
        {
            MemoTxt.Text = "";
            VendorValue.Text = "";
            ItemEditing.Value = "";
            TotalQtyBox.Text = "";
            FurtherTestingBox.Text = "";
            //SynthListBox.Text = "";
            //SuspListBox.Text = "";
            txtQtyPass.Text = "";
            txtQtySynth.Text = "";
            txtQtySusp.Text = "";
            InternalComments.Text = "";
            DestinationBox.Text = "";
            DestinationListBox.Items.Clear();
            SynthNumberBox.Text = "";
            SynthListBox.Items.Clear();
            SuspNumberBox.Text = "";
            SuspListBox.Items.Clear();
            GridNewBags.DataSource = null;
            GridNewBags.DataBind();
            //DestinationButton.Visible = false;
            if (ElementPanel.Items.Count > 0)
            {
                foreach (RepeaterItem element in ElementPanel.Items)
                {
                    (element.FindControl("SmTxt") as TextBox).Text = "";
                }
            }
            /* alex
            {
                //if ((element.FindControl("ElementName") as HiddenField).Value == "Destination")
                if ((element.FindControl("ElementName") as HiddenField).Value == "User")
                {
                    synScrData.CreatedBy = (element.FindControl("SmTxt") as TextBox).Text;
                    //synScrData.Destination = (element.FindControl("SmTxt") as TextBox).Text;
                }
                if ((element.FindControl("ElementName") as HiddenField).Value == "TestType")
                {
                    synScrData.Test = (element.FindControl("SmTxt") as TextBox).Text;
                }
                if ((element.FindControl("ElementName") as HiddenField).Value == "ScreeningType")
                {
                    synScrData.ScreeningType = (element.FindControl("SmTxt") as TextBox).Text;
                }
            }
            alex */
        }
        private void CleanPartTreeArea()
        {
            //PartTree.Nodes.Clear();
            PartEditing.Value = "";
        }
        private void CleanValueGridArea()
        {
            //ItemValuesGrid.DataSource = null;
            //ItemValuesGrid.DataBind();

            //New code for unbinding data sources
            ElementPanel.DataSource = null;
            ElementPanel.DataBind();

            //Make Measurement Panel Invisible
            ShowHideItemDetails(false);
            /*
			MeasurementPanel.Visible = false;
			DataForLabel.Visible = false;
			ClarityRadio.Visible = false;
			Comments.Visible = false;
			WeightPanel.Visible = false;
			IntExtCommentPanel.Visible = false;
			LaserPanel.Visible = false;
			SarinPanel.Visible = false;
			*/
        }
        private void CleanViewState()
        {
            //-- Item numbers
            SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);

            //-- Measure Parts
            SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);

            //-- Measure Descriptions
            SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

            //-- Item Measure Values
            SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);
        }
        #endregion
        protected void OnSynthDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = sender as ListBox;
            //var textBox = listBox.Parent.Parent.FindControl("txtValSynth") as TextBox;
            SynthNumberBox.Text = listBox.SelectedItem.Text;

        }
        protected void OnDestinationDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = sender as ListBox;
            //var textBox = listBox.Parent.Parent.FindControl("txtValSynth") as TextBox;
            DestinationBox.Text = listBox.SelectedItem.Text;
            if (listBox.SelectedIndex == 0)//first element is destination from v0group
            {
                SynthNumberBox.Text = txtQtySynth.Text;
                SuspNumberBox.Text = txtQtySusp.Text;
            }
            else
            {
                if (Session["SyntheticData"] != null)
                {
                    DataTable syntDt = (DataTable)Session["SyntheticData"];
                    int i = 0;
                    foreach (DataRow dr in syntDt.Rows)
                    {
                        string synth = dr["MemoNumber"].ToString();
                        string[] tmpSynth = synth.Split('/');
                        string synthNum = tmpSynth[1];
                        if ((tmpSynth[0] == listBox.SelectedItem.Text && listBox.SelectedIndex != 0) ||
                            (listBox.SelectedIndex == 0))
                        {
                            //SynthNumberBox.Text = synthNum;
                            string order = dr["GroupCode"].ToString();
                            string batch = dr["BatchCode"].ToString();
                            string item = dr["ItemCode"].ToString();
                            string gsiNumber = FillToFiveChars(order) + FillToThreeChars(batch) + FillToTwoChars(item);
                            //SynthListBox.Items.Add(gsiNumber);
                            ListItem newItem = new ListItem(gsiNumber);
                            SynthListBox.Items.Add(newItem);
                            i++;
                        }
                    }
                    SynthNumberBox.Text = i.ToString();
                }
                if (Session["SuspData"] != null)
                {
                    int i = 0;
                    DataTable suspDt = (DataTable)Session["SuspData"];
                    foreach (DataRow dr in suspDt.Rows)
                    {
                        string susp = dr["MemoNumber"].ToString();
                        string[] tmpSusp = susp.Split('/');
                        string suspNum = tmpSusp[1];
                        if ((tmpSusp[0] == listBox.SelectedItem.Text && listBox.SelectedIndex != 0) ||
                            (listBox.SelectedIndex == 0))
                        {
                            //SuspNumberBox.Text = suspNum;
                            i++;
                            string order = dr["GroupCode"].ToString();
                            string batch = dr["BatchCode"].ToString();
                            string item = dr["ItemCode"].ToString();
                            string gsiNumber = FillToFiveChars(order) + FillToThreeChars(batch) + FillToTwoChars(item);
                            ListItem newItem = new ListItem(gsiNumber);
                            SuspListBox.Items.Add(newItem);
                        }
                    }
                    SuspNumberBox.Text = i.ToString();
                }
                if (Session["DestinationData"] != null)
                {
                    int total = 0;
                    DataTable destDt = (DataTable)Session["DestinationData"];
                    foreach (DataRow dr in destDt.Rows)
                    {
                        string memo = dr["MemoNumber"].ToString();
                        string[] tmpMemo = memo.Split('/');
                        if (listBox.SelectedIndex == 0)
                        {
                            total += Convert.ToInt16(tmpMemo[1]);
                        }
                        else if (tmpMemo[0] == listBox.SelectedItem.Text)
                        {
                            TotalQtyBox.Text = tmpMemo[1];
                            break;
                        }
                    }
                    if (listBox.SelectedIndex == 0)
                        TotalQtyBox.Text = total.ToString();
                }
            }
        }
        protected void OnSuspDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = sender as ListBox;
            //var textBox = listBox.Parent.Parent.FindControl("txtValSusp") as TextBox;
            SuspNumberBox.Text = listBox.SelectedItem.Text;
        }
        protected void OnSynthTextChanged(object sender, EventArgs e)
        {
            /*
            var textBox = sender as TextBox;
            var listBox = textBox.Parent.FindControl("SynthListBox") as ListBox;
            ListItem newItem = new ListItem(textBox.Text);
            listBox.Items.Add(newItem);
            listBox.SelectedValue = newItem.Text;
            */
        }
        protected void OnSuspTextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var listBox = textBox.Parent.FindControl("SuspListBox") as ListBox;
            ListItem newItem = new ListItem(textBox.Text);
            listBox.Items.Add(newItem);
            listBox.SelectedValue = newItem.Text;
        }
        #region itemFailed
        protected void OnLoadItemPartsClick(object sender, EventArgs e)
        {
            if (gsiNumberTextBox.Text == "")
                return;
            string gsiNumber = gsiNumberTextBox.Text.Trim();
            int groupCode = Convert.ToInt32(gsiNumber.Substring(0, gsiNumber.Length - 5));
            int batchCode = Convert.ToInt16(gsiNumber.Substring(gsiNumber.Length - 5, 3));
            DataTable dt = GSIAppQueryUtils.GetPartIdList(groupCode, batchCode, this);
            if (dt != null)
            {
                //var result = (from DataRow row in dt.Rows select new SamplePriceListModel(row)).ToList();
                //SetViewState(result, SessionConstants.SamplePriceList);
                //List<string> samplePriceList = new List<string>();
                //foreach (DataRow dr in dt.Rows)
                //{
                //    samplePriceList.Add(dr["SamplePriceName"].ToString());
                //}
                ItemPartsList.DataSource = dt;//samplePriceList;
                ItemPartsList.DataBind();
                ItemPartsList.SelectedIndex = -1;
                ItemPartsList.Items.Insert(0, new ListItem("Please select Part", ""));
            }

        }
        protected void OnUpdateFailedItemClick(object sender, EventArgs e)
        {
            if (gsiNumberTextBox.Text == "")
            {
                InvalidLabel.Text = "No item entered";
                return;
            }
            string gsiNumber = gsiNumberTextBox.Text.Trim();
            if (ItemPartsList.SelectedIndex > 0)
            {
                int failedPart = Convert.ToInt32(ItemPartsList.SelectedValue);
                if (SynthNumberBox.Text == "" && SuspNumberBox.Text == "" && InternalComments.Text == "")
                {
                    InvalidLabel.Text = "No synthetic number entered";
                    return;
                }
                string synth = SynthNumberBox.Text;
                if (SuspNumberBox.Text != "")
                    synth += "," +  SuspNumberBox.Text;
                if (InternalComments.Text != "")
                    synth += "," + InternalComments.Text;
                bool updated = GSIAppQueryUtils.UpdateItemFailedInfo(gsiNumber, failedPart, synth, this);
                if (updated)//set state to invalid for batch/item
                {
                    //bool invalid = GSIAppQueryUtils.SetInvalidGroupState(gsiNumber, this);
                    //bool invalid = GSIAppQueryUtils.SetInvalidBatchState(gsiNumber, this);
                    bool invalid = GSIAppQueryUtils.SetInvalidItemState(gsiNumber, this);
                }


            }
            else
            {
                InvalidLabel.Text = "Choose the item part";
                return;
            }
        }
        
        #endregion
        #region Load
        protected void OnLoadClick(object sender, EventArgs e)
        {
            /*
            if (HasUnSavedChanges())
            {
                PopupSaveQDialog(ModeOnLoadClick);
                return;
            }
            */
            //if first time loading, hide side menu - not yet working
            /*
			if(MeasurementPanel.Visible == false)
			{
				Session[SessionConstants.SideMenuState] = SessionConstants.SideMenuHide;
				var state = Session[SessionConstants.SideMenuState] as string ?? "";
				bool show = state != SessionConstants.SideMenuHide;
				(this.Page.Master.FindControl("sidebar") as Control).Visible = show;
				(this.Page.Master.FindControl("HideMenuButton") as ImageButton).Visible = show;
				(this.Page.Master.FindControl("ShowMenuButton") as ImageButton).Visible = !show;
			}
			*/
            // System.Diagnostics.Process.Start(@"c:\temp\2.jpg");
            LoadExecute();
        }

        private void LoadExecute()
        {
            if (Session["MachineName"] == null)//no machine name
                InvalidLabel.Text += "NO MACHINE NAME!!! ";
            //Save current lstItemList temporarily
            /*
            List<string> prevList = new List<string>();
            string prevItemNumber = "0";
            var prevModel = GetItemModelFromView(ItemEditing.Value);
            if (prevModel != null)
            {
                prevItemNumber = prevModel.FullBatchNumber;
                
            }
            */
            ArrayList memoList = new ArrayList();
            bool multiMemo = false;
            CleanUp();

            if (txtOrderNumber.Text.Trim().Length == 0)
            {
                InvalidLabel.Text = "Order Number is required!";
                return;
            }


            //CreateMeasurePanel();
            try
            {
                var gsiNumber = txtOrderNumber.Text.Trim();
                DataSet ds = GSIAppQueryUtils.GetSyntheticItemData(gsiNumber, this);
                if (ds == null || ds.Tables[0].Rows.Count == 0)
                {
                    InvalidLabel.Text = "Not Screening Data!";
                    ShowHideItemDetails(true);
                    MemoPanel.Visible = false;
                    PONumPanel.Visible = false;
                    VendorPanel.Visible = false;
                    FurtherTestingPanel.Visible = false;
                    TotalQtyPanel.Visible = false;
                    QtyPassPanel.Visible = false;
                    QtySuspPanel.Visible = false;
                    QtySynthPanel.Visible = false;
                    btnPrintItemLabel.Visible = false;
                    btnPrintLabel.Visible = false;
                    labelCount.Visible = false;
                    labelCountLbl.Visible = false;
                    gsiNumberTextBox.Text = txtOrderNumber.Text;
                    OnLoadItemPartsClick(null, null);
                    GSIItemLabel.Visible = true;
                    gsiNumberTextBox.Visible = true;
                    itemLoadBtn.Visible = true;
                    ItemPartsLabel.Visible = true;
                    ItemPartsList.Visible = true;
                    synthItemBtn.Visible = true;
                    cmdSaveBatch.Visible = true;
                    BagsTable.Visible = false;
                    ItemsInBag.Text = "";
                    GridNewBags.DataSource = null;
                    GridNewBags.DataBind();
                    return;
                }
                //{
                //    InvalidLabel.Text = "No information from database for " + gsiNumber + ". Check the item type.";
                //    return;
                //}
                DataTable dt = null;
                int total = 0, synth = 0, susp = 0, passed = 0, screeningType = 0, testType = 0;
                if (ds == null)
                {
                    InvalidLabel.Text = "No information from database!";
                    return;
                }
                ShowHideItemDetails(true);
                InvalidLabel.Text = "";
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    dt = ds.Tables[0];
                    PopulateCustomerData(dt);
                }
                else
                    InvalidLabel.Text = "No Customer data, ";
                count = 0;
                if (ds.Tables.Count > 1)
                {
                    count = ds.Tables[1].Rows.Count;
                    if (count > 0)
                    {
                        dt = null;
                        dt = ds.Tables[1];
                        PopulateSyntheticScreeningdata(dt);
                    }
                    else
                        InvalidLabel.Text += "No Screening data, ";
                    count = 0;
                }
                if (ds.Tables.Count > 2)
                {
                    count = ds.Tables[2].Rows.Count;
                    if (count > 0)
                    {
                        ArrayList arrayList = new ArrayList();
                        dt = null;
                        dt = ds.Tables[2];
                        //PopulateSynthFail(dt);
                        int intTotalQty = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            intTotalQty += Convert.ToInt32(dr["BatchCount"].ToString());
                        }
                        //TotalQtyBox.Text = intTotalQty.ToString();
                    }
                    else
                        InvalidLabel.Text += "No data";
                    count = 0;
                }
                if (ds.Tables.Count > 4)
                {
                    count = ds.Tables[4].Rows.Count;
                    if (count > 0)
                    {
                        synth = count;
                        dt = null;
                        dt = ds.Tables[4];
                        txtQtySynth.Text = count.ToString();
                        PopulateSynthFail(dt);
                        Session["SyntheticData"] = dt;
                    }
                    else
                        InvalidLabel.Text += ", No Synth data";
                    count = 0;
                }
                if (ds.Tables.Count > 5)
                {
                    count = ds.Tables[5].Rows.Count;
                    if (count > 0)
                    {
                        susp = count;
                        dt = null;
                        dt = ds.Tables[5];
                        txtQtySusp.Text = count.ToString();
                        PopulateSuspFail(dt);
                        Session["SuspData"] = dt;
                    }
                    else
                        InvalidLabel.Text += ", No Suspect data";
                    count = 0;
                }
                /* alex
                if (ds.Tables.Count > 6)
                {
                    count = ds.Tables[6].Rows.Count;
                    if (count > 0)
                    {
                        screeningType = count;
                        dt = null;
                        dt = ds.Tables[5];
                        txtQtySusp.Text = count.ToString();
                        PopulateScreeningType(dt);
                        Session["ScreeningType"] = dt;
                    }
                    else
                        InvalidLabel.Text += ", No Suspect data";
                    count = 0;
                }
                if (ds.Tables.Count > 6)
                {
                    count = ds.Tables[7].Rows.Count;
                    if (count > 0)
                    {
                        testType = count;
                        dt = null;
                        dt = ds.Tables[5];
                        txtQtySusp.Text = count.ToString();
                        PopulateTestType(dt);
                        Session["TestType"] = dt;
                    }
                    else
                        InvalidLabel.Text += ", No Suspect data";
                    count = 0;
                }
                alex */
                if (ds.Tables.Count > 3)
                {
                    count = ds.Tables[3].Rows.Count;
                    if (count > 0)
                    {
                        dt = null;
                        dt = ds.Tables[3];

                        foreach (DataRow dr in dt.Rows)
                        {
                            string memo = dr["MemoNumber"].ToString();
                            if (memo != MemoValue.Text)
                            {
                                string[] tmpMemo = memo.Split('/');

                                memoList.Add(tmpMemo[0]);
                                DestinationListBox.Items.Add(tmpMemo[0]);
                                multiMemo = true;
                                Session["DestinationData"] = dt;
                            }
                        }
                    }
                }
                if (TotalQtyBox.Text != null && TotalQtyBox.Text != "")
                    total = Convert.ToInt16(TotalQtyBox.Text);
                if (synth == 0 &&
                    (txtQtySynth.Text != null && txtQtySynth.Text != ""))
                    synth = Convert.ToInt16(txtQtySynth.Text);
                if (susp == 0 &&
                    (txtQtySusp.Text != null && txtQtySusp.Text != ""))
                    susp = Convert.ToInt16(txtQtySusp.Text);
                int furtherTesting = Convert.ToInt16(FurtherTestingBox.Text);
                passed = total - synth - susp;
                txtQtyPass.Text = passed.ToString();
                txtQtySynth.Text = synth.ToString();
                txtQtySusp.Text = susp.ToString();

                var tempTxtOrderNumber = txtOrderNumber.Text;

                var itemNumber = tempTxtOrderNumber;

                txtOrderNumber.Text = itemNumber;

                //alex
                if (InternalComments.Text != "" && InternalComments.Text.Contains(";Bag1"))
                {
                    List<BagsItemsModel> bagsList = new List<BagsItemsModel>();
                    
                    string notesBags = InternalComments.Text.Substring(InternalComments.Text.IndexOf(@";Bag1")+1);
                    string[] allBags = notesBags.Split(';');
                    foreach(var bags in allBags)
                    {
                        BagsItemsModel bagItem = new BagsItemsModel();
                        string[] bag = bags.Split(',');
                        bagItem.BagNumber = bag[0].Trim().Replace("Bag", "");
                        bagItem.ItemsInBag = bag[1].Trim().Replace("Items", "");
                        bagsList.Add(bagItem);
                    }
                    GridNewBags.DataSource = bagsList;
                    GridNewBags.DataBind();
                    InternalComments.Text = InternalComments.Text.Substring(0, InternalComments.Text.IndexOf(";Bag1"));
                    
                }
                //alex
                ShowHideItemDetails(true);
                /*
                ClarityRadio.Visible = true;
                Comments.Visible = true;
                WeightPanel.Visible = true;
                SarinPanel.Visible = true;
                IntExtCommentPanel.Visible = true;
                LaserPanel.Visible = true;
                */
                labelCount.Text = "1";
                
                CreateMeasurePanel();
                txtOrderNumber.Focus();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                InvalidLabel.Text = "Error message: " + msg;
                BagsTable.Visible = false;
                ItemsInBag.Text = "";
                GridNewBags.DataSource = null;
                GridNewBags.DataBind();
            }
        }//LoadExecute

        private void PopulateCustomerData(DataTable dt)
        {
            DataRow dr = dt.Rows[0];
            MemoValue.Text = dr["Memo"].ToString();
            PONumValue.Text = dr["PONum"].ToString();
            //SKUValue.Text = dr["SKUName"].ToString();
            TotalQtyBox.Text = dr["TotalQty"].ToString();
            //BagQtyBox.Text = dr["BagQty"].ToString();
            VendorValue.Text = dr["CustomerCode"].ToString();
            PONumValue.Text = dr["PONum"].ToString();
            txtQtyPass.Text = dr["QtyPass"].ToString();
            txtQtySynth.Text = dr["QtySynth"].ToString();
            txtQtySusp.Text = dr["QtySusp"].ToString();
            FurtherTestingBox.Text = dr["FTQuantity"].ToString();
            DestinationBox.Text = dr["Destination"].ToString();
            DestinationListBox.Items.Add(dr["Destination"].ToString());
            InternalComments.Text = dr["Notes"].ToString();
            string passData = @"User=" + dr["CreatedBy"].ToString() + @";ScreeningType=" + dr["ScreeningType"].ToString() + @";TestType=" + dr["Test"].ToString();
            Session["PassedData"] = passData;
        }//PopulateCustomerData

        private void PopulateSyntheticScreeningdata(DataTable dt)
        {
            DataRow dr = dt.Rows[0];
            //SynthScreeningID.Value = dr["ScreeningID"].ToString();
            //txtQtyPass.Text = dr["QtyPass"].ToString();
            //txtQtySusp.Text = dr["QtySuspect"].ToString();
            //txtQtySynth.Text = dr["QtyFail"].ToString();
            //PONumValue.Text = dr["PONum"].ToString();
            //txtQtySynthSusp.Text = dr["QtySynthSusp"].ToString();
        }//PopulateSyntheticData

        private string FillOrder(DataRow dr)
        {
            return dr["ItemNumber"].ToString();
            //string order = dr["GroupCode"].ToString();
            //string batch = dr["BatchCode"].ToString();
            //string item = dr["ItemCode"].ToString();
            //return FillToFiveChars(order) + FillToThreeChars(batch) + FillToTwoChars(item);
        }
        private void PopulateSynthFail(DataTable dt)
        {
            int i = 0;

            SynthListBox.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                //string gsiOrder = FillOrder(dr);
                string synth = dr["SynthNumber"].ToString();
                SynthListBox.Items.Add(synth);
                if (i == 0)
                    SynthNumberBox.Text = synth;
                i++;
            }
        }//PopulateSynthFail

        private void PopulateSuspFail(DataTable dt)
        {
            int i = 0;
            SuspListBox.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                //string gsiOrder = FillOrder(dr);
                string susp = dr["SuspNumber"].ToString();
                SuspListBox.Items.Add(susp);
                if (i == 0)
                    SuspNumberBox.Text = susp;
                i++;
            }
        }//PopulateSuspFail

        private void PopulateScreeningType(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                InternalComments.Text += "Screening: " + dr["ScreeningType"].ToString() + " " + dr["ScreeningNumber"].ToString() + ",";
            }
        }
        private void PopulateTestType(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                InternalComments.Text += "Testing: " + dr["TestType"].ToString() + " " + dr["TestNumber"].ToString() + ",";
            }
        }
        #endregion

        #region Save
        protected void OnSaveClick(object sender, EventArgs e)
        {
            //-- no changes
            /*
            if (!HasUnSavedChanges())
            {
                PopupInfoDialog("No changes!", false);
                return;
            }
            
            if (MemoValue.Text == "" || PONumValue.Text == "" || TotalQtyBox.Text == "" || BagQtyBox.Text == "" ||
                VendorValue.Text == "" || txtOrderNumber.Text == "" || txtQtyPass.Text == "" || txtQtySusp.Text == "" || txtQtySynth.Text == "")
            {
                InvalidLabel.Text = "Populate all necessary fields!";
                return;
            }
            */
            var errMsg = SaveExecute();
            if (errMsg == "success")
            {
                //-- Reload Item Values
                InvalidLabel.Text = "Update successful";
                PopupInfoDialog("Changes were updated successfully.", false);
                //if(lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                //{
                //    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                //    OnItemListSelectedChanged(null, null);
                //}
                LoadExecute();
                return;
            }
            else
            {
                InvalidLabel.Text = "Update failed";
                PopupInfoDialog(errMsg, true);
                return;
            }
        }

        private string SaveExecute()
        {
            string memo = MemoValue.Text;
            //string poNum = PONumValue.Text;
            //string sku = SKUValue.Text;
            string qty = TotalQtyBox.Text;
            //string FTQty = FurtherTestingBox.Text;
            string custCode = VendorValue.Text;
            string gsiOrder = txtOrderNumber.Text;

            SyntheticScreeningModel synScrData = new SyntheticScreeningModel();
            synScrData.ScreeningID = SynthScreeningID.Value == string.Empty ? 0 : Int64.Parse(SynthScreeningID.Value);
            synScrData.GSIOrder = int.Parse(txtOrderNumber.Text);
            synScrData.VendorNum = VendorValue.Text;
            synScrData.PONum = MemoValue.Text;
            synScrData.TotalQty = int.Parse(TotalQtyBox.Text);

            //synScrData.QtyFail = int.Parse(txtQtyFail.Value);
            int synth = 0, susp = 0, furtherTest = 0;
            synth = SynthListBox.Items.Count;
            if (synth == 0)
                synth = Convert.ToInt16(txtQtySynth.Text);
            susp = SuspListBox.Items.Count;
            if (susp == 0)
                susp = Convert.ToInt16(txtQtySusp.Text);
            furtherTest = Convert.ToInt16(FurtherTestingBox.Text);
            if ((synScrData.TotalQty - synth - susp) < 0)
            {
                return "Invalid total quantity, synthetic, suspect and Further Testing entries.";
            }
            
            txtQtySynth.Text = synth.ToString();
            txtQtySusp.Text = susp.ToString();
            txtQtyPass.Text = (synScrData.TotalQty - synth - susp).ToString();
            synScrData.QtySynthetic = int.Parse(txtQtySynth.Text);
            synScrData.QtySuspect = int.Parse(txtQtySusp.Text);
            synScrData.QtyPass = int.Parse(txtQtyPass.Text);
            //synScrData.SyntheticSuspect = int.Parse(txtQtySynthSusp.Text);
            synScrData.QtyFail = synScrData.QtySynthetic + synScrData.QtySuspect;

            //synScrData.FTQuantity = FurtherTestingBox.Text.Trim() == string.Empty ? 0 : int.Parse(FTQty.Text);
            synScrData.FTQuantity = int.Parse(FurtherTestingBox.Text);
            synScrData.SKUName = SKUValue.Text;
            synScrData.Notes = InternalComments.Text;
            //alex
            if (GridNewBags.Items.Count > 0)
            {
                string bags = "";
                foreach(DataGridItem dgItem in GridNewBags.Items)
                {
                    bags += ";Bag" + dgItem.Cells[0].Text + @",Items" + dgItem.Cells[1].Text;
                }
                synScrData.Notes += bags;
            }
            //alex
            synScrData.Destination = DestinationBox.Text;
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                //if ((element.FindControl("ElementName") as HiddenField).Value == "Destination")
                if ((element.FindControl("ElementName") as HiddenField).Value == "User")
                {
                    synScrData.CreatedBy = (element.FindControl("SmTxt") as TextBox).Text;
                    //synScrData.Destination = (element.FindControl("SmTxt") as TextBox).Text;
                }
                if ((element.FindControl("ElementName") as HiddenField).Value == "TestType")
                {
                    synScrData.Test = (element.FindControl("SmTxt") as TextBox).Text;
                }
                if ((element.FindControl("ElementName") as HiddenField).Value == "ScreeningType")
                {
                    synScrData.ScreeningType = (element.FindControl("SmTxt") as TextBox).Text;
                }
            }
            if (synScrData.CreatedBy == null || synScrData.CreatedBy == "")
            {
                int userId = Convert.ToInt16(this.Session["ID"]);
                var userName = GSIAppQueryUtils.GetUserName(userId, this);
                if (userName != null)
                    synScrData.CreatedBy = userName;
            }
            string saveScreeningData = GSIAppQueryUtils.SaveScreeningData(synScrData, null, this);
            if (saveScreeningData == "")
                saveScreeningData = "success";
            int update = 1;
            if (InvalidLabel.Text.Contains("Customer") || InvalidLabel.Text.Contains("Screening") || InvalidLabel.Text.Contains("Fail"))
                update = 0;
            string saveSyntheticList = null, saveSuspList = null;
            //string saveCustomerData = GSIAppQueryUtils.SaveSyntheticCustomerData(memo, poNum, sku, qty, bagQty, this, update, custCode, gsiOrder);
            if (SuspListBox.Items.Count >= 0)
            {
                DataTable dtSusp = new DataTable();
                DataSet dsSusp = new DataSet();
                dsSusp.DataSetName = "DocumentElement";

                dtSusp.Columns.Add("ItemNumber");
                dtSusp.Columns.Add("SuspNumber");
                foreach (var item in SuspListBox.Items)
                {
                    DataRow dr = null;
                    dr = dtSusp.NewRow();
                    dr["ItemNumber"] = txtOrderNumber.Text;
                    dr["SuspNumber"] = item.ToString();
                    dtSusp.Rows.Add(dr);
                }
                Session["SuspItems"] = dtSusp;
                dsSusp.Tables.Add(((DataTable)Session["SuspItems"]).Copy());
                dsSusp.Tables[0].TableName = "SuspectItems";
                var xmlItems = ConvertDatatableToXML(dsSusp);
                saveSuspList = GSIAppQueryUtils.SaveSuspectListData(xmlItems, txtOrderNumber.Text, this);
            }
            if (SynthListBox.Items.Count >= 0)
            {
                DataTable dtSynth = new DataTable();
                DataSet dsSynth = new DataSet();
                dsSynth.DataSetName = "DocumentElement";
                dtSynth.Columns.Add("ItemNumber");
                dtSynth.Columns.Add("SynthNumber");
                foreach (var item in SynthListBox.Items)
                {
                    DataRow dr = null;
                    dr = dtSynth.NewRow();
                    dr["ItemNumber"] = txtOrderNumber.Text;
                    dr["SynthNumber"] = item.ToString();
                    dtSynth.Rows.Add(dr);
                }
                Session["SynthItems"] = dtSynth;
                dsSynth.Tables.Add(((DataTable)Session["SynthItems"]).Copy());
                dsSynth.Tables[0].TableName = "SyntheticItems";
                var xmlItems = ConvertDatatableToXML(dsSynth);
                saveSyntheticList = GSIAppQueryUtils.SaveSyntheticListData(xmlItems, txtOrderNumber.Text, this);
            }
            string saveUserdata = GSIAppQueryUtils.SaveSyntheticUserData(synScrData, this);
            /*alex
            DataTable dtUserData = new DataTable();
            DataSet dsUserData = new DataSet();
            dsUserData.DataSetName = "DocumentElement";
            dtUserData.Columns.Add("ItemNumber");
            dtUserData.Columns.Add("UserName");
            dtUserData.Columns.Add("ScreeningType");
            dtUserData.Columns.Add("TestType");
            dtUserData.Columns.Add("Suspect");
            dtUserData.Columns.Add("Synthetic");
            dtUserData.Columns.Add("FurtherTest");
            dtUserData.Columns.Add("TotalQty");
            DataRow drUserData = null;
            drUserData = dtUserData.NewRow();
            drUserData["ItemNumber"] = txtOrderNumber.Text;
            drUserData["UserName"] = synScrData.CreatedBy;
            drUserData["ScreeningType"] = synScrData.ScreeningType;
            drUserData["TestType"] = synScrData.Test;
            drUserData["FurtherTest"] = synScrData.FTQuantity;
            drUserData["Suspect"] = synScrData.QtySuspect;
            drUserData["Synthetic"] = synScrData.QtySynthetic;
            drUserData["TotalQty"] = synScrData.TotalQty;
            Session["UserData"] = dtUserData;
            dsUserData.Tables.Add(((DataTable)Session["UserData"]).Copy());
            dsUserData.Tables[0].TableName = "UserDataItems";
            var xmlUserData = ConvertDatatableToXML(dsUserData);
            */

            /* alex
        if (chkQCHK.Checked == true) strTestData = strTestData + chkQCHK.Value + ",";
        if (chkDiamondView.Checked == true) strTestData = strTestData + chkDiamondView.Value + ",";
        if (chkYehuda.Checked == true) strTestData = strTestData + chkYehuda.Value + ",";
        if (chkRaman.Checked == true) strTestData = strTestData + chkRaman.Value + ",";
        if (chkFTIR.Checked == true) strTestData = strTestData + chkFTIR.Value + ",";
        synScrData.Test = strTestData.TrimEnd(',');
        synScrData.CertifiedBy = ddlCertifiedBy.SelectedItem.Text;
        synScrData.Destination = ddlDestination.SelectedItem.Text;
        if (IsOrderExist(int.Parse(txtGsiOrder.Value)) == true && hdnScreeningID.Value == string.Empty)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exist.');", true);
        }
        else
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "DocumentElement";
            ds.Tables.Add(((DataTable)Session["FailItems"]).Copy());
            ds.Tables[0].TableName = "SyntheticFailItems";
            if (ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Rows[0]["GSIItemNumber"].ToString() == "")
            {
                xmlItems = "";
            }
            else
            {
                xmlItems = ConvertDatatableToXML(ds);
            }
            string status = GSIAppQueryUtils.SaveScreeningData(synScrData, xmlItems, this);
            if (status != "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order " + synScrData.GSIOrder + " not save.');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order saved successfully.');", true);
                BindGrid(0);
                ClearText();
            }
        }
        string saveCustomerData = GSIAppQueryUtils.SaveSyntheticCustomerData( memo,  poNum,  sku,  qty,  bagQty, this,  update,  custCode);
        return saveCustomerData;
        */

            /*
            GSIAppQueryUtils
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            var result = new List<MeasureValueModel>();
            foreach (var itemValue in itemValues)
            {
                result.Add(new MeasureValueModel(itemValue, itemModel));
            }
            var errMsg = QueryUtils.SaveNewMeasures(result, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;

            //Highlight after saved
            //lstItemList.Items.FindByText(itemModel.FullItemNumber).Attributes.Add("style", "color: green");//For some reason, this doesn't last!
           
            if ((saveSyntheticList != "success" && saveSyntheticList != "" && saveSyntheticList != null) || 
                (saveSuspList != "success" && saveSuspList != "" && saveSuspList != "") ||
                //alex saveCustomerData != "success" || 
                (saveScreeningData != "success" && saveScreeningData != "" && saveScreeningData != null))
                InvalidLabel.Text = "Error inserting data";
                 */

            string passData = @"User=" + synScrData.CreatedBy + @";ScreeningType=" + synScrData.ScreeningType + @";TestType=" + synScrData.Test;
            Session["PassedData"] = passData;
            if (saveScreeningData != "success" && saveScreeningData != "" && saveScreeningData != null)
                InvalidLabel.Text = "Error inserting data";
            else
                InvalidLabel.Text = "success";
            return InvalidLabel.Text;
        }//SaveExecute

        public string ConvertDatatableToXML(DataSet ds)
        {
            MemoryStream str = new MemoryStream();
            ds.Tables[0].WriteXml(str, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }
        #endregion

        #region ItemNumber List

        protected void OnSynthAddClick(object sender, EventArgs e)
        {
            InvalidLabel.Text = "";
            /*
            var textBox = sender as TextBox;
            var listBox = new ListBox();//textBox.Parent.FindControl("SynthListBox") as ListBox;
            ListItem newItem = new ListItem(SynthNumberBox.Text);
            listBox.Items.Add(SynthNumberBox.Text);
            listBox.SelectedValue = newItem.Text;
            */
            var tmpText = SynthNumberBox.Text;
            tmpText = tmpText.Replace(" ", "");
            bool matched = false;
            if (tmpText != "")
            {
                for (int i=0; i<= SynthListBox.Items.Count - 1; i++)
                {
                    if (tmpText == SynthListBox.Items[i].Text)
                    { 
                        matched = true;
                        InvalidLabel.Text = "Synthetic number already exists!";
                        break;
                        }
                }
                if (!matched)
                {
                    SynthListBox.Items.Add(SynthNumberBox.Text);
                    SynthListBox.SelectedValue = SynthNumberBox.Text;
                }
            }

            SynthListBox.SelectedIndex = SynthListBox.Items.Count - 1;
            //SynthListBox.Focus();
            //SynthNumberBox.Focus();
            //Session["SyntNewEntriesFields"] = SynthNumberBox;
            Session["SyntNewEntriesFields"] = "synth";

        }

        protected void OnSynthDelClick(object sender, EventArgs e)
        {
            SynthListBox.Items.Remove(SynthNumberBox.Text);
            SynthListBox.SelectedValue = SynthListBox.Items[0].Text;
            SynthNumberBox.Text = SynthListBox.Items[0].Text;
            SynthListBox.Focus();
        }

        protected void OnDestDelClick(object sender, EventArgs e)
        {
            DestinationListBox.Items.Remove(SynthNumberBox.Text);
            DestinationListBox.SelectedValue = SynthListBox.Items[0].Text;
            //SynthNumberBox.Text = SynthListBox.Items[0].Text;
        }

        protected void OnSuspAddClick(object sender, EventArgs e)
        {
            InvalidLabel.Text = "";
            var tmpText = SuspNumberBox.Text;
            tmpText = tmpText.Replace(" ", "");
            if (tmpText != "")
            {
                bool matched = false;
                for (int i=0; i<=SuspListBox.Items.Count-1; i++)
                {
                    if (tmpText == SuspListBox.Items[i].Text)
                    {
                        matched = true;
                        InvalidLabel.Text = "Suspect number already exists!";
                        break;
                    }
                }
                if (!matched)
                {
                    SuspListBox.Items.Add(SuspNumberBox.Text);
                    SuspListBox.SelectedValue = SuspNumberBox.Text;
                }
            }
            SuspListBox.SelectedIndex = SuspListBox.Items.Count - 1;
            //SuspListBox.Focus();
            Session["SyntNewEntriesFields"] = "susp";
            /*
            var textBox = sender as TextBox;
            var listBox = textBox.Parent.FindControl("SuspListBox") as ListBox;
            ListItem newItem = new ListItem(textBox.Text);
            listBox.Items.Add(newItem);
            listBox.SelectedValue = newItem.Text;
            */
        }

        protected void OnSuspDelClick(object sender, EventArgs e)
        {
            SuspListBox.Items.Remove(SuspNumberBox.Text);
            SuspListBox.SelectedValue = SuspListBox.Items[0].Text;
            SuspNumberBox.Text = SuspListBox.Items[0].Text;
            //SuspNumberBox.Text = "";
        }
        private bool HasUnSavedChanges()
        {
            GetNewValuesFromGrid();
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            return itemValues.Count > 0;
        }
        #endregion

        #region Elements
        private List<String> commentNums = new List<String> { "113", "48", "105", "49", "51", "86", "97", "102", "103", "104" };
        private void CreateMeasurePanel()
        {
            ArrayList screeningList = new ArrayList();
            ArrayList testList = new ArrayList();
            List<ExpandableControlModel> Elements = new List<ExpandableControlModel>();
            //string abc = File.ReadAllText(@"C:\Users\aremennik\Documents\Visual Studio 2010\Projects\NewCorpt_merge0803\SyntheticData.xml");
            //abc = abc.Replace("STERLING", "ABC");
            //File.WriteAllText(@"C:\Users\aremennik\Documents\Visual Studio 2010\Projects\NewCorpt_merge0803\SyntheticDataNew.xml", abc);
            XmlDocument doc = new XmlDocument();
            //if (!File.Exists("SyntheticDataNew.xml"))
            //    return;
            doc.Load(Server.MapPath("SyntheticDataNew.xml"));
            //doc.Load(Server.MapPath("ClarityData.xml"));

            foreach (XmlNode node in doc.SelectNodes("//Element"))
            {
                ExpandableControlModel elem;
                elem = ExpandableControlModel.CreateNew(node);

                if (elem.DropDownPanelVis)
                {
                    elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == elem.ElemMeasure);
                    elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == null);
                }

                if (elem.ElemType == "Standard")
                {
                    var buttonList = ButtonDetailModel.CreateNewStandard(elem.DropDownEnums);
                    //alex
                    /*
                    if (elem.ElemMeasure == "18" || elem.ElemMeasure == "19")
                    {
                        var noneEntry = buttonList[0];
                        buttonList.Remove(buttonList[0]);
                        buttonList.Insert(buttonList.Count() - 1, noneEntry);
                    }
                    */
                    //alex
                    elem.BtnRepeaterSource = buttonList;
                }

                if (elem.ElemType == "Filter" || elem.ElemType == "Custom")
                {
                    if (elem.DropDownPanelVis)
                    {
                        elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == elem.ElemMeasure);
                        elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == null);
                    }
                    elem.DropDownEnums = CreateListEnum(node);
                    elem.BtnRepeaterSource = ButtonDetailModel.CreateNew(node);
                    if (elem.SmTxt == "ScreeningType")
                    {
                        ArrayList screening = new ArrayList();
                        foreach (var btn in elem.BtnRepeaterSource)
                        {
                            if (btn.BigBtn.ToString() == "Collapse")
                                continue;
                            screening.Add(btn.BtnValue.ToString());
                        }
                        Session["Screening"] = screening;
                    }
                    else if (elem.SmTxt == "TestType")
                    {
                        ArrayList test = new ArrayList();
                        foreach (var btn in elem.BtnRepeaterSource)
                        {
                            if (btn.BigBtn.ToString() == "Collapse")
                                continue;
                            test.Add(btn.BtnValue.ToString());
                        }
                        Session["Test"] = test;
                    }
                    if (Session["PassedData"] != null)
                    {
                        var passedData = (string)Session["PassedData"];
                        if (passedData != null && passedData != "")
                        {
                            string[] splitData = passedData.Split(';');
                            //string dest = null, screen = null, test = null;
                            string user = null, screen = null, test = null;
                            for (int i = 0; i <= 2; i++)
                            {
                                string tagval = splitData[i];
                                string[] data = tagval.Split('=');
                                //if (data[0] == "Destination")
                                if (data[0] == "User")
                                    //dest = data[1];
                                    user = data[1];
                                else if (data[0] == "ScreeningType")
                                    screen = data[1];
                                else if (data[0] == "TestType")
                                    test = data[1];
                            }
                            //if (elem.SmTxt == "Destination")
                            //    elem.SmTxt = dest;
                            if (elem.SmTxt == "User")
                                elem.SmTxt = user;
                            else if (elem.SmTxt == "ScreeningType")
                                elem.SmTxt = screen;
                            else if (elem.SmTxt == "TestType")
                                elem.SmTxt = test;


                        }//if
                    }//passedData
                    Elements.Add(elem);
                }
                else if (elem.ElemType == "Comments")
                {

                    foreach (XmlNode item in node.SelectNodes("CommentSource/Comments"))
                    {
                        commentNums.Add(item.InnerText);
                    }

                    elem.BtnRepeaterSource = ButtonDetailModel.CreateNewComments(GetMeasureEnumsFromView(), node);
                }

                //Elements.Add(elem);
            }

            ElementPanel.DataSource = Elements;
            ElementPanel.DataBind();

            //Make filtering dropdown work
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                if ((element.FindControl("ElementType") as HiddenField).Value == "Filter")
                {
                    (element.FindControl("BigTxt") as TextBox).Attributes.Add("onKeyUp", "FilterShapes(this)");
                    (element.FindControl("BigTxt") as TextBox).Attributes.Add("onFocus", "FilterShapes(this)");
                }
            }

            //Apply styles to particular elements
            foreach (RepeaterItem item in ElementPanel.Items)
            {
                if ((item.FindControl("ElementType") as HiddenField).Value.StartsWith("Pic"))
                {
                    (item.FindControl("Pic") as Panel).Style["display"] = "inline-block";
                    //(item.FindControl("Pic") as Panel).Style["clear"] = "right";
                }
                else
                {
                    (item.FindControl("Pic") as Panel).Style["display"] = "none";
                }
            }
        }
        //On selecting item from dropdown list
        protected void OnDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = sender as ListBox;
            var textBox = listBox.Parent.Parent.FindControl("BigTxt") as TextBox;
            var smTextBox = listBox.Parent.Parent.FindControl("SmTxt") as TextBox;
            
            
            
            //alex
            var value = Session["VAL"];
            if (textBox.Text == "ScreeningType" || textBox.Text == "TestType")
            {
                if (smTextBox.Text == null || smTextBox.Text.Length == 0 ||
                    value.ToString() == "")
                    //textBox.Text = value.ToString();
                    smTextBox.Text = value.ToString();
                else
                    smTextBox.Text += @"," + value.ToString();
            }
            else
                smTextBox.Text = value.ToString();
            return;
            //alex
            //var textBox = listBox.Parent.Parent.Parent.FindControl("BigTxt") as TextBox;
            //var smTextBox = listBox.Parent.Parent.Parent.FindControl("SmTxt") as TextBox;
            textBox.Text = listBox.SelectedItem.Text;
            smTextBox.Text = listBox.SelectedItem.Text;
        }

        protected void OnTextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
            var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
            bool changed = false;

            foreach (ListItem item in listBox.Items)
            {
                if (textBox.Text == item.Text)
                {
                    listBox.SelectedValue = item.Value;
                    smTextBox.Text = smTextBox.Text;
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                if (listBox.SelectedItem != null)
                {
                    textBox.Text = listBox.SelectedItem.Text;
                }
                else
                {
                    textBox.Text = "";
                }
            }
        }

        protected void OnButtonListClick(object sender, RepeaterCommandEventArgs e)
        {
            var btn = e.Item.FindControl("RepeatButton") as Button;
            var val = e.Item.FindControl("BtnValue") as HiddenField;
            var redir = e.Item.FindControl("BtnLink") as HiddenField;
            var cat = e.Item.FindControl("BtnCat") as HiddenField;
            var abc = (e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value;
            var xyz = (e.Item.Parent.Parent.FindControl("ElementType") as HiddenField).Value;
            if ((e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value == "Comments")
            //if ((e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value != "Custom")
            {
                var mea = e.Item.FindControl("BtnMeasure") as HiddenField;
                var buttonStyle = btn.Style["border-bottom-color"];//Comment button styles

                if (mea.Value != "0")
                {
                    var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
                    if (itemValues != null)
                    {
                        var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
                        if (itemValue != null)
                        {
                            var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
                            EnumMeasureModel enumValue;
                            if (buttonStyle == "#821717" || buttonStyle == "0")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
                            {
                                enumValue = enumValues.Find(m => m.ValueTitle == "");
                                //btn.Style["background-color"] = "rgb(73,175,205)";
                            }
                            else
                            {
                                enumValue = enumValues.Find(m => m.MeasureValueName == val.Value);
                                //btn.Style["background-color"] = "red";

                                //Find control that is red, if any, for that measure value, and change it to default
                                /*
								var buttonList = e.Item.Parent as Repeater;
								foreach (RepeaterItem button in buttonList.Items)
								{
									if ((button.FindControl("BtnMeasure") as HiddenField).Value == mea.Value)
									{
										if (btn != button.FindControl("RepeatButton") as Button && (button.FindControl("RepeatButton") as Button).Style["background-color"] == "red")
										{
											SetCommentButtonState(button.FindControl("RepeatButton") as Button, false);
											//(button.FindControl("RepeatButton") as Button).Style["background-color"] = "rgb(73,175,205)";
											break;
										}
									}
								}
								*/
                            }

                            if (itemValue != null && enumValue != null)
                            {
                                var newValue = enumValue.MeasureValueId.ToString();
                                itemValue.Value = newValue;
                            }
                            PopulateComments();
                        }
                    }
                }
            }
            else if (val.Value != "0")
            {
                var ddl = e.Item.Parent.Parent.FindControl("DropDownListBox") as ListBox;
                //int count = ddl.Items.Count;
                //for (int i = 0; i<= count - 1; i++)
                //{
                //    if (ddl.Items[i].Text == val.Value.ToString())
                //        ddl.SelectedIndex = i;
                //}
                //var dde = (List<EnumMeasureModel>)HttpContext.Current.Session["NodeEnum"];
                var elemMeasure = e.Item.Parent.Parent.Parent.FindControl("ElementMeasure") as HiddenField;
                //alex
                //ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == val.Value).MeasureValueId.ToString();
                ddl.Items.Add(val.Value);
                //alex
                Session["VAL"] = val.Value;
                OnDropDownSelectedIndexChanged(ddl, null);


            }

            if (redir.Value != "0")
            {
                if (redir.Value == "Small")
                {
                    ElementEditing.Value = "None";
                }
                CategoryEditing.Value = redir.Value;
                if (val.Value == "0")
                    SetButtonCategory();
            }
        }

        protected void OnSmBtnClick(object sender, EventArgs e)
        {
            var btn = sender as Button;
            var smallPanel = btn.Parent as Panel;
            var bigPanel = smallPanel.Parent.FindControl("BigPanel") as Panel;
            var eleName = bigPanel.Parent.FindControl("ElementName") as HiddenField;

            ElementEditing.Value = eleName.Value;

            if (ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
            {
                //CategoryEditing.Value = ClarityRadio.SelectedValue;
            }
            else
            {
                CategoryEditing.Value = "Root";
            }

            SetButtonCategory();
        }

        private void SetButtonCategory()
        {
            Panel activeBigPanel = null;

            //Display big panel for the current element
            foreach (RepeaterItem item in ElementPanel.Items)
            {

                if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
                {
                    activeBigPanel = item.FindControl("BigPanel") as Panel;

                    //(item.FindControl("SmallPanel") as Panel).Visible = false;
                    activeBigPanel.Visible = true;
                }
                else
                {
                    //(item.FindControl("SmallPanel") as Panel).Visible = true;
                    (item.FindControl("BigPanel") as Panel).Visible = false;
                }
            }

            //Display buttons in the big panel for the current category
            foreach (RepeaterItem item in ElementPanel.Items)
            {
                var value = (item.FindControl("ElementName") as HiddenField).Value;
                if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
                {
                    foreach (RepeaterItem btnItem in (item.FindControl("ButtonRepeater") as Repeater).Items)
                    {
                        var btnCat = (btnItem.FindControl("BtnCat") as HiddenField).Value;
                        var btnValue = (btnItem.FindControl("BtnValue") as HiddenField).Value;
                        if ((btnItem.FindControl("BtnCat") as HiddenField).Value == CategoryEditing.Value)
                        {
                            btnItem.FindControl("RepeatButton").Visible = true;
                            //alex
                            //(btnItem.FindControl("BtnValue") as HiddenField).Value = "ABC";
                            //(btnItem.FindControl("BigBtn") as HiddenField).Value = "ZZZ";
                            btnValue = (btnItem.FindControl("BtnValue") as HiddenField).Value;

                            //alex
                        }
                        else
                        {
                            btnItem.FindControl("RepeatButton").Visible = false;
                        }
                    }
                    break;
                }
            }
        }
        private void ClearButtonCategory()
        {
            ElementEditing.Value = "";
            CategoryEditing.Value = "";
            SetButtonCategory();
        }

        protected void ClarityRadioChanged(object sender, EventArgs e)
        {
            if (ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
            {
                // CategoryEditing.Value = ClarityRadio.SelectedValue;
                SetButtonCategory();
            }
        }

        private void ShowImages(string path, string option)
        {
            string imgPath;
            string errMsg;
            Panel imgPanel = null;
            ShowHideImages(true);
            if (option == "Shape")
            {
                //string pngPath;
                if (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\")
                {
                    path = path.Substring(1);
                }
				var pngStream = new MemoryStream();
				var result = Utlities.GetShapeImageUrlAzure(path, out imgPath, out errMsg, this,/* out pngPath,*/ out pngStream);
                foreach (RepeaterItem elem in ElementPanel.Items)
                {
                    if ((elem.FindControl("ElementType") as HiddenField).Value == "PictureShape")
                    {
                        imgPanel = elem.FindControl("Pic") as Panel;
                        break;
                    }
                }
                if (result)
                {
                    (imgPanel.FindControl("PicImg") as Image).ImageUrl = "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).Visible = true;
                }
                else
                {
                    (imgPanel.FindControl("PicImg") as Image).Visible = false;
                }
                (imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Shape");
                (imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
            }
            else if (option == "Picture")
            {
				var ms = new MemoryStream();
				var fileType = "";
				//var result = Utlities.GetPictureImageUrl(path, out imgPath, out errMsg, this);
				//GetPictureImageUrl(string dbPicture, out MemoryStream ms, out string errMsg, out string fileType, Page p)
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				foreach (RepeaterItem elem in ElementPanel.Items)
                {
                    if ((elem.FindControl("ElementType") as HiddenField).Value == "PicturePicture")
                    {
                        imgPanel = elem.FindControl("Pic") as Panel;
                        break;
                    }
                }
                if (result)
                {
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;
					(imgPanel.FindControl("PicImg") as Image).Visible = true;
                }
                else
                {
                    (imgPanel.FindControl("PicImg") as Image).Visible = false;
                }
                //-- Path to picture
                (imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Picture");
                (imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
            }
        }

        private void ShowHideImages(bool show)
        {
            foreach (RepeaterItem elem in ElementPanel.Items)
            {
                (elem.FindControl("Pic") as Panel).Visible = show;
            }
        }
        #endregion

        #region IntExtComments
        protected void OnIntExtCommentChanged(object sender, EventArgs e)
        {
            /*
            var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
            var commentBox = sender as TextBox;
            int measureId = 0;

            if (commentBox.ID == "InternalComments")
            {
                measureId = 26;
            }
            else if (commentBox.ID == "ExternalComments")
            {
                measureId = 9;
            }

            if (measureId != 0)
            {
                var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == measureId);
                if (itemValue != null)
                {
                    itemValue.Value = commentBox.Text;
                }
            }
            */
        }
        #endregion
        /*
        #region ScreeningTyope
        protected void OnScreeningTypeChanged(object sender, EventArgs e)
        {
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                if ((element.FindControl("ElementName") as HiddenField).Value == "ScreeningType")
                {
                    InternalComments.Text += "Screening: " + (element.FindControl("SmTxt") as TextBox).Text + " " + txtQtyScreeningType.Text + ",";
                    break;
                }
            }
        }
        #endregion
        #region TestType
        protected void OnTestTypeChanged(object sender, EventArgs e)
        {
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                if ((element.FindControl("ElementName") as HiddenField).Value == "TestType")
                {
                    InternalComments.Text += "Test " + (element.FindControl("SmTxt") as TextBox).Text + " " + txtQtyTestType.Text + ",";
                    break;
                }
            }
        }
        #endregion
        */
        #region Vendor
        protected void OnVendorChanged(object sender, EventArgs e)
        {
            return;
            //var liscBox = sender as TextBox;
            /*
            var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
            var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 67);
            if (itemValue != null)
            {
                itemValue.Value = liscBox.Text;
            }
            */
        }
        #endregion

        #region Data
        private void LoadMeasuresForEdit(string itemNumber)
        {
            var itemModel = GetItemModelFromView(itemNumber);
            var enumsMeasure = GetMeasureEnumsFromView();
            var parts = GetPartsFromView();
            var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            /* TODO replace this code with something that works with our Element objects - done */
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing.Length > 0);
            if (withShapes.Count > 0)
            {
                var path = withShapes[0].ShapePath2Drawing;
                if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImages(path, "Shape");
                else ShowHideImages(false);
            }
            else
            {
                ShowHideImages(false);
            }
            // */
            var measures = GetMeasuresCpFromView();
            var result = new List<ItemValueEditModel>();
            foreach (var measure in measures)
            {
                if (measure.MeasureClass > 3) continue;
                var partModel = parts.Find(m => m.PartId == measure.PartId);
                if (partModel == null) continue;
                var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
                var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                        ? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
                result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
            }
            result.Sort(new ItemValueEditComparer().Compare);
            SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);

        }

        private void LoadDataForEditing()
        {
            var itemModel = GetSelectedItemModel();

            var itemValues = GetMeasuresForEditFromView(null);

            var enumValues = GetMeasureEnumsFromView();

            //New code
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                var smallTxt = element.FindControl("SmTxt") as TextBox;
                var bigTxt = element.FindControl("BigTxt") as TextBox;
                smallTxt.Text = "";
                bigTxt.Text = "";

                var dropDownList = element.FindControl("DropDownListBox") as ListBox;
                dropDownList.SelectedIndex = -1;

                var dropDownMeasure = element.FindControl("ElementMeasure") as HiddenField;
                var dropDownValue = itemValues.Find(m => m.MeasureId.ToString() == dropDownMeasure.Value);
                if (dropDownValue != null)
                {
                    var dropDownEnumValue = enumValues.Find(m => m.MeasureValueMeasureId.ToString() == dropDownMeasure.Value
                        && m.MeasureValueId.ToString() == dropDownValue.Value);
                    if (dropDownEnumValue != null)
                    {
                        smallTxt.Text = dropDownEnumValue.ValueTitle.ToString();
                        bigTxt.Text = smallTxt.Text;
                        dropDownList.SelectedValue = dropDownValue.Value;
                    }
                }
            }

            PopulateComments();

            MemoValue.Text = "";
            MemoTxt.Text = "Memo Number:";

            //Populate Sarin Panel
            PONumValue.Text = "";
            SKUValue.Text = "";

            InternalComments.Text = "";

            VendorValue.Text = "";

            DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            //OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            //Get Old Number
            //var test = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            ShowHideItemDetails(true);

        }
        private void PopulateComments()
        {
            //CommentsList.Text = "";
            /*
            var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
            
            if (itemValues.Count == 0)
            {
                return;
            }
            */
            //Find comments element
            RepeaterItem commentElement = null;
            foreach (RepeaterItem element in ElementPanel.Items)
            {
                if ((element.FindControl("ElementType") as HiddenField).Value == "Comments")
                {
                    commentElement = element;
                    break;
                }
            }

            foreach (var measureId in commentNums)
            {

                ItemValueEditModel commentValue = null;// itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId.ToString() == measureId);
                if (commentValue != null)
                {
                    if (commentValue.Value == "0")
                    {
                        foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
                        {
                            if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
                            {
                                SetCommentButtonState(btn.FindControl("RepeatButton") as Button, false);
                            }
                        }
                    }

                    var enumValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == measureId && m.MeasureValueId.ToString() == commentValue.Value);
                    if (enumValue != null)
                    {
                        if (enumValue.ValueTitle != "")
                        {
                            //CommentsList.Text += enumValue.MeasureValueName;
                            //CommentsList.Text += "\n";
                        }

                        //Update style of buttons
                        if (commentElement != null)
                        {
                            foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
                            {
                                if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString() /*enumValue.MeasureValueMeasureId.ToString()*/)
                                {
                                    if (enumValue.MeasureValueName == (btn.FindControl("BtnValue") as HiddenField).Value)
                                    {
                                        SetCommentButtonState(btn.FindControl("RepeatButton") as Button, true);
                                    }
                                    else
                                    {
                                        SetCommentButtonState(btn.FindControl("RepeatButton") as Button, false);
                                    }
                                }

                            }
                        }
                    }
                }
                else//If commentvalue is null, clear all styles
                {
                    foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
                    {
                        if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
                        {
                            SetCommentButtonState(btn.FindControl("RepeatButton") as Button, false);
                        }

                    }
                }
            }
        }
        private void SetCommentButtonState(Button btn, bool active)
        {
            if (active)
            {
                btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
                btn.Style["border-top-color"] = "#c32222";
                btn.Style["border-left-color"] = "#c32222";
                btn.Style["border-right-color"] = "#c32222";
                btn.Style["border-bottom-color"] = "#821717";
            }
            else
            {
                btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
                btn.Style["border-top-color"] = "#2f96b4";
                btn.Style["border-left-color"] = "#2f96b4";
                btn.Style["border-right-color"] = "#2f96b4";
                btn.Style["border-bottom-color"] = "#1f6377";
            }
        }
        private void SetSmallButtonState(Button btn, string part)
        {
            if (part.Contains("Item Container"))
            {
                /*
				btn.Style["background"] = "linear-gradient(to bottom, #cc0099, #990073)";
				btn.Style["border-top-color"] = "#990073";
				btn.Style["border-left-color"] = "#990073";
				btn.Style["border-right-color"] = "#990073";
				btn.Style["border-bottom-color"] = "#66004d";
				*/
                btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
                btn.Style["border-top-color"] = "#6620aa";
                btn.Style["border-left-color"] = "#6620aa";
                btn.Style["border-right-color"] = "#6620aa";
                btn.Style["border-bottom-color"] = "#441a88";
            }
            else
            {
                btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
                btn.Style["border-top-color"] = "#2f96b4";
                btn.Style["border-left-color"] = "#2f96b4";
                btn.Style["border-right-color"] = "#2f96b4";
                btn.Style["border-bottom-color"] = "#1f6377";
            }
        }
        protected void OnInfoCloseButtonClick(object sender, EventArgs e)
        {

        }
        void ShowHideItemDetails(bool show)
        {
            DataForLabel.Visible = show;
            //PrefixPanel.Visible = show;
            //OldNumPanel.Visible = show;
            MeasurementPanel.Visible = show;
            //cmdSaveOrder.Visible = show;
            //ClarityRadio.Visible = show;
            //Comments.Visible = show;
            MemoPanel.Visible = show;
            //SKUPanel.Visible = show;
            IntExtCommentPanel.Visible = show;
            VendorPanel.Visible = show;
            PONumPanel.Visible = show;
            TotalQtyPanel.Visible = show;
            FurtherTestingPanel.Visible = show;
            Panel3.Visible = show;
            QtySuspPanel.Visible = show;
            QtyPassPanel.Visible = show;
            QtySynthPanel.Visible = show;
            cmdSaveBatch.Visible = show;
            //cmdSave.Visible = show;
            //shortrpt.Visible = show;
            GSIItemLabel.Visible = false;
            gsiNumberTextBox.Visible = false;
            itemLoadBtn.Visible = false;
            ItemPartsLabel.Visible = false;
            ItemPartsList.Visible = false;
            synthItemBtn.Visible = false;
            btnPrintItemLabel.Visible = show;
            btnPrintLabel.Visible = show;
            labelCount.Visible = show;
            labelCountLbl.Visible = show;
            BagsTable.Visible = show;
        }

        private void GetNewValuesFromGrid()
        {
            var itemValues = GetMeasuresForEditFromView(PartEditing.Value);

            foreach (RepeaterItem element in ElementPanel.Items)
            {
                var eleValue = element.FindControl("ElementMeasure") as HiddenField;
                var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
                if (itemValue != null)
                {
                    var eleList = element.FindControl("DropDownListBox") as ListBox;
                    var newValue = eleList.SelectedValue;
                    itemValue.Value = newValue;
                }
            }

            /* Old code for GetNewValuesFromGrid
			//Shapes
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "8");
			if (itemValue != null)
			{
				var newValue = ShapesList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Polish
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "18");
			if(itemValue != null)
			{
				var newValue = PolishList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Symmetry
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "19");
			if (itemValue != null)
			{
				var newValue = SymmetryList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Clarity
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "29");
			if(itemValue != null)
			{
				var newValue = ClarityList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Inclusion
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "198");
			if(itemValue != null)
			{
				var newValue = InclusionList.SelectedValue;
				itemValue.Value = newValue;
			}
			*/
        }
        #endregion

        #region Get Data From View

        //-- Measure Values
        private List<MeasureValueModel> GetMeasureValuesFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
        }

        //-- Measure Enums
        private List<EnumMeasureModel> GetMeasureEnumsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
        }

        private List<EnumMeasureModel> CreateListEnum(XmlNode node)
        {
            List<EnumMeasureModel> ret = new List<EnumMeasureModel>();
            EnumMeasureModel btn;
            //alex
            //int count = DestinationListBox.Items.Count;
            //var xname = DestinationListBox.Items[0].Text;

            //alex
            int i = 0;
            foreach (XmlNode item in node.SelectNodes("BtnRepeaterSource/Button"))
            {
                //if (i == count)
                //    break;
                //var name = DestinationListBox.Items[i++].Text;
                btn = new EnumMeasureModel();

                btn.MeasureValueName = item.SelectSingleNode("BtnValue").InnerText;
                //btn.MeasureValueName = name;
                btn.ValueTitle = item.SelectSingleNode("BtnValue").InnerText;
                //btn.ValueTitle = name;
                ret.Add(btn);
            }
            //Session["NodeEnum"] = ret;
            return ret;
        }


        //-- Loading Measures Description
        private List<MeasureValueCpModel> GetMeasuresCpFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
        }

        //-- Loading Item Numbers
        private List<SingleItemModel> GetItemNumbersFromView()
        {

            return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
        }
        private SingleItemModel GetItemModelFromView(string itemNumber)
        {
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber);
        }
        private SingleItemModel GetSelectedItemModel()
        {
            var itemNumber = txtOrderNumber.Text;
            if (itemNumber.StartsWith("*"))
            {
                itemNumber = itemNumber.Substring(1);
            }
            if (string.IsNullOrEmpty(itemNumber)) return null;
            return GetItemModelFromView(itemNumber);
        }
        private List<MeasurePartModel> GetPartsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
        }
        private List<ItemValueEditModel> GetMeasuresForEditFromView(string partId)
        {
            var data = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ?? new List<ItemValueEditModel>();
            if (string.IsNullOrEmpty(partId)) return data;
            else return data.FindAll(m => m.PartId == partId);
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }

        #endregion

        #region Question Dialog
        static string ModeOnItemChanges = "item";
        static string ModeOnLoadClick = "load";
        static string ModeOnShortReportClick = "report";
        private void PopupSaveQDialog(string src)
        {
            var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
            SaveDlgMode.Value = src;
            QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
            SaveQPopupExtender.Show();
        }
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            var errMsg = SaveExecute();
            if (!string.IsNullOrEmpty(errMsg))
            {
                // Return on prev item
                txtOrderNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
                PopupInfoDialog(errMsg, true);
                return;
            }


            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
            if (SaveDlgMode.Value == ModeOnShortReportClick)
            {
                //Redirect to Short Report
                var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
                Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
            }
        }
        protected void OnQuesSaveNoClick(object sender, EventArgs e)
        {

            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
            if (SaveDlgMode.Value == ModeOnShortReportClick)
            {
                //Redirect to Short Report
                var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
                Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
            }
        }
        /*alex
        protected void btnPrintLabel_Click(object sender, EventArgs e)
        {
        alex */
        /* tmp
        ClearText();
        txtCreateDateSearch.Value = string.Empty;
        txtToCreateDateSearch.Value = string.Empty;
        gsiOrderFilter.Value = string.Empty;
        grdScreening.PageIndex = 0;
        grdScreening.SelectedIndex = -1;
        BindGrid(ddlDestinationSearch.SelectedItem.Value.Trim());
        gvTitle.InnerText = "Screening Result - For orders having destination " + ddlDestinationSearch.SelectedItem.Value;
        tmp */
        /* alex
        if (DestinationBox.Text.Trim() == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Vendor');", true);
            return;
        }
        if (DestinationBox.Text.Trim() == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Destination');", true);
            return;
        }
        //if (txtPoNum.Value.Trim() == "")
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter PO #');", true);
        //    return;
        //}
        if (txtOrderNumber.Text.Trim() == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter GSI Order number');", true);
            return;
        }
        //if (txtSku.Value.Trim() == "")
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter SKU');", true);
        //    return;
        //}
        if (TotalQtyBox.Text.Trim() == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Total Quantity');", true);
            return;
        }
        var vendor = VendorValue.Text;
        var destination = DestinationBox.Text;
        //var poNum = poNum.Text;
        var gsiOrder = txtOrderNumber.Text;
        //var sku = txtSku.Value;
        var totalQ = TotalQtyBox.Text;
        string bagQ = "250";
        var dir = GetExportDirectory(this);
        string filename = @"label_" + gsiOrder + ".xml";
        if (File.Exists(dir + filename))
            File.Delete(dir + filename);
        //string filename = dir + @"\label_" + gsiOrder + ".xml";
        using (XmlWriter writer = XmlWriter.Create(dir + filename))
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("label");
            writer.WriteElementString("vendor", vendor);
            writer.WriteElementString("destination", destination);
            //writer.WriteElementString("ponum", poNum);
            writer.WriteElementString("gsiOrder", gsiOrder);
            //writer.WriteElementString("sku", sku);
            writer.WriteElementString("totalQ", totalQ);
            writer.WriteElementString("bagQ", bagQ);
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            writer.Flush();
        }
        //var msg = PlottingUtils.CallPlotting(this, "111", "222", "323");
        DownloadExcelFile(filename, this);
    }

    public static void DownloadExcelFile(String filename, Page p)
    {

        var arguments = "TEST";

        StringWriter oStringWriter = new StringWriter();
        oStringWriter.WriteLine(arguments);
        p.Response.ContentType = "text/plain";

        p.Response.AddHeader("content-disposition", "attachment;filename=" + string.Format("{0}.Plotting.txt", "file111.txt"));
        p.Response.Clear();

        using (StreamWriter writer = new StreamWriter(p.Response.OutputStream, Encoding.UTF8))
        {
            writer.Write(oStringWriter.ToString());
        }

        p.Response.End();
        return;

        var dir = GetExportDirectory(p);
        //-- Download
        p.Response.ContentType = "text/plain";
        //p.Response.AddHeader("Refresh", "0.1");
        p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
        p.Response.TransmitFile(dir + filename);
        p.Response.End();
    }
    alex */
        
        protected void btnPrintLabel_Click(object sender, EventArgs e)
        {
            /* alex
            ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            gsiOrderFilter.Value = string.Empty;
            grdScreening.PageIndex = 0;
            grdScreening.SelectedIndex = -1;
            BindGrid(ddlDestinationSearch.SelectedItem.Value.Trim());
            gvTitle.InnerText = "Screening Result - For orders having destination " + ddlDestinationSearch.SelectedItem.Value;
            alex */

            if (VendorValue.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Vendor');", true);
                return;
            }
            if (DestinationBox.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Destination');", true);
                return;
            }

            if (txtOrderNumber.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter GSI Order number');", true);
                return;
            }
            if (TotalQtyBox.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Total Quantity');", true);
                return;
            }
            DataTable dt = GSIAppQueryUtils.GetSyntheticCustomerEntriesByOrder(txtOrderNumber.Text, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No database entries!');", true);
                return;
            }
            DataRow dr = dt.Rows[0];
            var vendor = VendorValue.Text;
            vendor = dr["CustomerName"].ToString();
            var destination = DestinationBox.Text;
            var gsiOrder = txtOrderNumber.Text;
            var totalQ = TotalQtyBox.Text;
            var totalPass = txtQtyPass.Text;
            if (totalPass == "" || totalPass == null)
                totalPass = "0";
            var totalSynth = txtQtySynth.Text;
            if (totalSynth == null || totalSynth == "")
                totalSynth = "0";
            var totalSusp = txtQtySusp.Text;
            if (totalSusp == null || totalSusp == "")
                totalSusp = "0";
            var furtherTest = FurtherTestingBox.Text;
            if (furtherTest == null || furtherTest == "")
                furtherTest = "0";
            string bagQ = "250";
            //var dir = GetExportDirectory(this);
            string filename = @"label_" + gsiOrder + ".xml";
            //if (File.Exists(dir + filename))
            //    File.Delete(dir + filename);
            //string filename = dir + @"\label_" + gsiOrder + ".xml";
            string screen = null, test = null, user = null;
            if (Session["PassedData"] == null)
                return;
            string passedData = (string)Session["PassedData"];
            if (passedData != null && passedData != "")
            {
                string[] splitData = passedData.Split(';');
                //string dest = null, screen = null, test = null;
                //string user = null, screen = null, test = null;
                for (int i = 0; i <= 2; i++)
                {
                    string tagval = splitData[i];
                    string[] data = tagval.Split('=');
                    //if (data[0] == "Destination")
                    if (data[0] == "User")
                        //dest = data[1];
                        user = data[1];
                    else if (data[0] == "ScreeningType")
                        screen = data[1];
                    else if (data[0] == "TestType")
                        test = data[1];

                }
            }
            ArrayList ScreeningList = (ArrayList)Session["Screening"];
            ArrayList TestList = (ArrayList)Session["Test"];
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("BatchLabel");
                writer.WriteElementString("vendor", vendor); //0
                writer.WriteElementString("OrderNo", gsiOrder);//1
                writer.WriteElementString("RetailerName", dr["RetailerName"].ToString()); //2
                writer.WriteElementString("SKU", dr["SKUName"].ToString());
                writer.WriteElementString("PONum", dr["PONum"].ToString());
                writer.WriteElementString("ServiceType", dr["ServiceTypeName"].ToString());
                writer.WriteElementString("TotalQty", totalQ);//3
                //writer.WriteElementString("RetailerName", dr["RetailerName"].ToString());
                //foreach (var screening in ScreeningList)
                //{
                //    if (screening.ToString() == "0" || screening.ToString() == "")
                //        continue;
                //    string scr = screening.ToString();
                //    if (scr == screen)
                //    {
                //        if (scr.Contains(" "))
                //            scr = scr.Replace(" ", "");
                //        writer.WriteElementString(scr, totalQ);
                //    }
                //    else
                //    {
                //        if (scr.Contains(" "))
                //            scr = scr.Replace(" ", "");
                //        writer.WriteElementString(scr, "0");
                //    }
                //}
                //writer.WriteElementString("FurtherTesting", furtherTest);//7
                //foreach (var testing in TestList)
                //{
                //    if (testing.ToString() == "0" || testing.ToString() == "")
                //        continue;
                //    string tst = testing.ToString();
                //    if (tst == test)
                //    {
                //        if (tst.Contains(" "))
                //            tst = tst.Replace(" ", "");
                //        writer.WriteElementString(tst, totalQ);
                //    }
                //    else
                //    {
                //        if (tst.Contains(" "))
                //            tst = tst.Replace(" ", "");
                //        writer.WriteElementString(tst, "0");
                //    }
                //}
                //writer.WriteElementString("totalPass", totalPass);//4
                //writer.WriteElementString("totalSynth", totalSynth);//5
                //writer.WriteElementString("totalSusp", totalSusp);//6
                if (GridNewBags.Items.Count > 0)
                {
                    foreach(DataGridItem item in GridNewBags.Items)
                    {
                        string bagNumber = item.Cells[0].Text;
                        string itemsInBag = item.Cells[1].Text;
                        writer.WriteElementString("Bag" + bagNumber, itemsInBag);
                    }
                }
                //writer.WriteElementString("bagQ", bagQ);
                
                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            bool messageSent = QueryUtils.SendMessageToStorage(stream, this, "Screening");
            //DownloadExcelFile(filename, this);
        }
        protected void OnBagAddClick(object sender, EventArgs e)
        {
            List<BagsItemsModel> bagsList = new List<BagsItemsModel>();
            if (GridNewBags.Items.Count == 0)
            {
                BagsItemsModel bagItem = new BagsItemsModel();
                bagItem.BagNumber = "1";
                bagItem.ItemsInBag = ItemsInBag.Text;
                bagsList.Add(bagItem);
                GridNewBags.DataSource = bagsList;
                GridNewBags.DataBind();
            }
            else
            {
                foreach (DataGridItem dgItem in GridNewBags.Items)
                {
                    BagsItemsModel bagItem = new BagsItemsModel();
                    bagItem.BagNumber = dgItem.Cells[0].Text;
                    bagItem.ItemsInBag = dgItem.Cells[1].Text;
                    bagsList.Add(bagItem);
                }
                BagsItemsModel newItem = new BagsItemsModel();
                newItem.BagNumber = (bagsList.Count + 1).ToString();
                newItem.ItemsInBag = ItemsInBag.Text;
                bagsList.Add(newItem);
                //bagDict.Add((bagDict.Count + 1).ToString(), ItemsInBag.Text);
                GridNewBags.DataSource = null;
                GridNewBags.DataBind();
                GridNewBags.DataSource = bagsList;
                GridNewBags.DataBind();
            }
        }
        protected void btnPrintItemLabel_Click(object sender, EventArgs e)
        {
            if (txtOrderNumber.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter GSI Order number');", true);
                return;
            }
            if (TotalQtyBox.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Total Quantity');", true);
                return;
            }
            if (Session["MachineName"]==null)//no machine name
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No machine name!Please restart GDLight with machine name using bookmark bar');", true);
                return;
            }
            var gsiOrder = txtOrderNumber.Text;
            var lCount = labelCount.Text;
            if (lCount == null || lCount == "")
                lCount = "1";
            string filename = @"labelScreeningItem_" + gsiOrder + @".xml";
            ArrayList ScreeningList = (ArrayList)Session["Screening"];
            ArrayList TestList = (ArrayList)Session["Test"];
            //alex download
            //var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(stream))
            //{
            //    writer.WriteStartDocument();
            //    writer.WriteStartElement("SmallLabel");
            //    writer.WriteElementString("GsiOrder", gsiOrder);//1
            //    writer.WriteElementString("LabelCount", lCount);//3
            //    writer.WriteEndElement();
            //    writer.WriteEndDocument();
            //    this.Session["Memory"] = stream;
            //    writer.Flush();
            //    writer.Close();

            //}
            //DownloadExcelFile(filename, this);
            //alex azure queue
            DataTable dataInputs = new DataTable("SmallLabel");
            dataInputs.Columns.Add("GsiOrder", typeof(String));
            dataInputs.Columns.Add("LabelCount", typeof(String));

            DataRow row;
            row = dataInputs.NewRow();
            row[0] = gsiOrder;
            row[1] = lCount;

            dataInputs.Rows.Add(row);
            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool sent = QueryUtils.SendMessageToStorage(ms, this, "Screening");
            }

        }//btnPrintItemLabel_Click
        protected void btnPrintNewItemLabel_Click(object sender, EventArgs e)
        {
            if (Session["MachineName"] == null)//no machine name
            {
                //PopupInfoDialog("No machine name! Please restart GDLight with machine name using bookmark bar", false);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No machine name!Please restart GDLight with machine name using bookmark bar');", true);
                return;
            }
            if (txtOrderNumber.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter GSI Order number');", true);
                return;
            }
            if (TotalQtyBox.Text.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Total Quantity');", true);
                return;
            }

            var gsiOrder = txtOrderNumber.Text;
            string lCount = "0";
            if (TotalQtyBox.Text != "")
                lCount = TotalQtyBox.Text;
            else
            {
                lCount = labelCount.Text;
                if (lCount == null || lCount == "")
                {
                    if (TotalQtyBox.Text == "")
                        lCount = "1";
                    else
                        lCount = TotalQtyBox.Text;
                }
            }  
            string filename = @"LabelScreeningNewItem_" + gsiOrder + @".xml";
            ArrayList ScreeningList = (ArrayList)Session["Screening"];
            ArrayList TestList = (ArrayList)Session["Test"];
            //alex download
            //var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(stream))
            //{
            //    writer.WriteStartDocument();
            //    writer.WriteStartElement("ItemLabel");
            //    writer.WriteElementString("GsiOrder", gsiOrder);//1
            //    writer.WriteElementString("POnum", PONumValue.Text);
            //    writer.WriteElementString("LabelCount", lCount);//3
            //    writer.WriteEndElement();
            //    writer.WriteEndDocument();
            //    this.Session["Memory"] = stream;
            //    writer.Flush();
            //    writer.Close();

            //}
            //DownloadExcelFile(filename, this);
            //alex azure
            DataTable dataInputs = new DataTable("ItemLabel");
            dataInputs.Columns.Add("GsiOrder", typeof(String));
            dataInputs.Columns.Add("POnum", typeof(String));
            dataInputs.Columns.Add("LabelCount", typeof(String));

            DataRow row;
            row = dataInputs.NewRow();
            row[0] = gsiOrder;
            row[1] = PONumValue.Text;
            row[2] = lCount;

            dataInputs.Rows.Add(row);
            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                string mName = Session["MachineName"].ToString();
                bool sent = QueryUtils.SendMessageToStorage(ms, this, "Screening");
            }
            SynthListBox.SelectedIndex = -1;
            //bool messageSent = QueryUtils.SendMessageToStorage(stream, this, "Screening");

        }//btnPrintItemLabel_Click
        public static void DownloadExcelFile(String filename, Page p)
        {
            p.Response.Clear();
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            //p.Response.AddHeader("Refresh", "0.1");
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.TransmitFile(dir + filename);
            //p.Response.TransmitFile(filename);
            p.Response.End();
        }
        public static void DownloadExcelFile1(String filename, Page p)
        {
            var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            //p.Response.AddHeader("Refresh", "0.1");
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            //p.Response.TransmitFile(filename);
            p.Response.End();
        }
        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }
        protected void OnQuesSaveCancelClick(object sender, EventArgs e)
        {

            txtOrderNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
        }
        private static string FillToFiveChars(string sNumber)
        {
            while (sNumber.Length < 5)
                sNumber = "0" + sNumber;
            return sNumber;
        }
        private static string FillToThreeChars(string sNumber)
        {
            while (sNumber.Length < 3)
                sNumber = "0" + sNumber;
            return sNumber;
        }

        private static string FillToTwoChars(string sNumber)
        {
            while (sNumber.Length < 2)
                sNumber = "0" + sNumber;
            return sNumber;
        }
        #endregion
    }
}