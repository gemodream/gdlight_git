﻿<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="Measure2.aspx.cs" AutoEventWireup="True" Inherits="Corpt.Measure2" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
	<style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
        .auto-style1 {
            height: 29px;
        }
    </style>
	<script type="text/javascript">
		function FilterShapes(caller) {
			var textbox = caller;
			filter = textbox.value.toLowerCase();
			var dropDownArray = document.getElementsByTagName("select");
			var list = dropDownArray[1];
			for (var c = 0; c < list.options.length; c++) {
				if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
					list.options.item(c).style.display = "block";
				}
				else {
					list.options.item(c).style.display = "none";
				}
			}
		}
		function StayFocused() {
			var textbox = document.GetElementById('<%=txtBatchNumber.ClientID%>');
			textbox.focus();
		}
	</script>
	<div class="demoarea">
		<div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
		<asp:UpdatePanel runat="server" ID="MainPanel">
			<ContentTemplate>
                <div class="demoheading">
                    Measure
                    <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 20px"></asp:Label>
                </div>
				<table style="width: 100%;" border="0">
					<tr>
                        <td style="vertical-align: top; white-space: nowrap;" colspan="2" >
                            <!-- 'Batch Number' textbox -->
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                                <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                    placeholder="Batch number" Style="width: 115px;" OnFocus="this.select();"/>
                                <!-- 'Load' button -->
                                <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-large" runat="server" Text="Load"
                                    OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
                                <asp:CheckBox ID="IgnoreCpFlag" runat="server" CssClass="radio-inline" Text="Ignore Cp" style="margin-left: 10px;"
                                    Checked="True" />
                            </asp:Panel>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                        </td>
                        <td style="vertical-align: top; width: 80%;">
                        <div style="float:left;">
                            <asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel" Visible="false" Style="float: left; font-size: 16px;"></asp:Label>
                            <asp:HiddenField runat="server" ID="OldTextNumber" />
                            <asp:HiddenField runat="server" ID="hdnMeasureBatchEventID" />
                            <asp:HiddenField runat="server" ID="hdnMeasureItemEventID" />
                            <!-- 'Save' button -->
                            <asp:Button ID="cmdSave" CssClass="btn btn-info btn-large" runat="server" Text="Save"
                                Style="text-align: center;margin-left:20px; float: left;" OnClick="OnSaveClick" Visible="false"></asp:Button>
							<asp:RadioButtonList runat="server" ID="ClarityRadio" OnSelectedIndexChanged="ClarityRadioChanged" AutoPostBack="true"
								style="margin-left: 20px; float: left;" RepeatDirection="Horizontal" Visible="false">
								<asp:ListItem Text="Loose" Value="Loose" Selected="true" />
								<asp:ListItem Text="Mounted" Value="Mounted" />
								<asp:ListItem Text="India" Value="India" />
							</asp:RadioButtonList>
							<!-- Short Report Button -->
							<asp:Button ID="ResetTotalWeightBtn" CssClass="btn btn-info btn-large" runat="server" Text="Reset Total Weight"
								Style="text-align: center; margin-left: 20px; float: left;" OnClick="OnResetTotalWeightClick" Visible="false" />
                            <asp:HiddenField runat="server" ID="ResetField" />
                            <asp:Button ID="calcBtn" CssClass="btn btn-info btn-large" runat="server" Text="Calculate"
								Style="text-align: center; margin-left: 20px; float: left;" OnClick="OnCalclick" />
                            <asp:Label ID="calcLabel" runat="server" ></asp:Label>
                                </div>
                        </td>
                        <%-- <td style="text-align: left; padding-right: 40px; padding-left: 20px; vertical-align: top">
                        </td> --%>
                    </tr>
					<tr style="height: 200px;">
                        <td style="padding-top: 0px; vertical-align: top">
                            <asp:ListBox ID="lstItemList" runat="server" Width="130px" CssClass="text" Style="vertical-align: top; font-size: 15px;"
                                 AutoPostBack="True" Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged">
                            </asp:ListBox>
                            <br />
                        </td>
                        <%--<td style="vertical-align: top;padding-right:50px;width: 550px; font-size: 19px;">--%>
                        <td style="vertical-align: top;padding-right: 20px;width: 220px; font-size: 19px;">
                            <asp:TreeView ID="PartTree" runat="server" 
                                OnSelectedNodeChanged="OnPartsTreeChanged" NodeIndent="15" >
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False"/>
                                 <NodeStyle VerticalPadding="5px" />
                            </asp:TreeView>
                        </td>
                        <td style="vertical-align: top;" rowspan="2" >
                        <table width="680px" style="left:650px;vertical-align: top;">
                        <tr>
                        <td>
                         <div style="vertical-align: top;" >
                                <asp:Label style="font-weight:normal;visibility:hidden;" id="sCustomer" runat="server" Text="Customer # "></asp:Label> 
                             <asp:Label runat="server" Text="Data For"  ID="Customer"  Visible="false" Style=""></asp:Label>
                            <br /> <asp:Label style="font-weight:normal;visibility:hidden;" id="sProgramm" runat="server" Text ="Program # "></asp:Label><asp:Label runat="server" Text="Data For"  ID="Programm" Visible="false" Style=""></asp:Label>
                           
                                </div>

                        <asp:Repeater runat="server" ID="RptMeasureList" >
                        <ItemTemplate>
                            <table width: 300px;>
                            <tr style="float:left; border:1;padding-bottom:20px;padding-right:20px;padding-top:10px;">
                                <td>
                           <%--<div style="float:left; border:1 ;padding-bottom:20px;padding-right:20px;padding-top:10px;">--%>
                                                            <asp:HiddenField runat="server" ID="MeasureID" Value='<%# Eval("MeasureID") %>' />
                                                            <asp:Label runat="server" ID="RepeatLabel" Text='<%# Eval("MeasureName")%>' Style="text-align: right;"></asp:Label>
															<asp:TextBox runat="server" ID="RepeatTxt" Value='<%# Eval("MeasureValue")%>' readonly="false" Style="margin-right: 0px; width: 100px;"/><br />
                                                            
														
                            <%--</div>--%>
                                    </td>
                                </tr>
                                </table>
                        </ItemTemplate>
                            
                        <%--<SeparatorTemplate>  
                        <tr>  
                            <td>  
                                <hr />  
                            </td>  
                    <td>  
                        <hr />  
                    </td>  
                    <td>  
                        <hr />  
                    </td>  
                </tr>  
                        </SeparatorTemplate>  --%>
                        </asp:Repeater>
							</td>
                        </tr>
                        <tr>
                            <td>
                                <div runat="server" id="ICParms" visible="false" >
                                    <table>
                                    <tr>
                                        <td class="auto-style1">
                                            <asp:Label Text="Weight(grams)" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="ICWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">
                                            <asp:Label Text="Total Weight(cts)" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="ICTotalWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;" OnTextChanged="ICTotalWeightChanged"/>
                                        </td>
                                    </tr>
                                        <tr>
                                                <td>
                                                    <asp:Label Text="Metal Stamp" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ICMetalStampList" OnSelectedIndexChanged="OnMetalStampChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Karatage" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ICMetalKaratageList" OnSelectedIndexChanged="OnMetalKaratageChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                        <tr>
                                                <td>
                                                    <asp:Label Text="Metal Color" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ICMetalColorList" OnSelectedIndexChanged="OnMetalColorChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                        <tr>
                                                <td>
                                                    <asp:Label Text="Item Name" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ICItemNameList" OnSelectedIndexChanged="OnMetalColorChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Lot Number" runat="server"></asp:Label>
                                                <asp:TextBox runat="server" ID="LotNumberTextBox" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                                <td>
                                                    <asp:Label Text="Engraving" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="ICEngravingBox" ReadOnly="false" Style="margin-right: 0px; width: 100%;"/>
                                                </td>
                                            </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div runat="server" id="WDSSParms" visible="false" >
                                    <table>
                                    <tr>
                                        <td>
                                            <asp:Label Text="# Of Units" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="WDSSNumberOfUnitsTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label Text="Total Weight(cts)" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="WDSSTotalWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            <asp:Label Text="Single Unit Weight  " runat="server" Visible="false" ></asp:Label>
                                            <asp:TextBox runat="server" ID="WDSSSingleWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;" OnTextChanged="WDSSSingleWeightTxt_TextChanged" Visible="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-top:1px;">
                                            <asp:Label Text="Number of Items"  runat="server" ></asp:Label>
                                        </td>
                                        <td style="margin-top:81px;">
                                            <asp:TextBox runat="server" ID="WDSSNumberOfItems" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            <asp:Label Text="Weight per Item " runat="server"  style="margin-top:41px;"></asp:Label>
                                            <asp:TextBox runat="server" ID="WSSWeightPerItem" readonly="false" Style="margin-right: 0px; width: 100px;" OnTextChanged="WDSSItemWeight_TextChanged"/>
                                            <asp:Button ID="WSSItemsWeightBtn" CssClass="btn btn-info btn-large" runat="server" Text="ADD" Style="text-align: center; float: right;" OnClick="OnAddWSSItemsWeightClick" Visible="true"></asp:Button>
                                        </td>
                                    </tr>
                                            
                                    </table>
                                </div>
                            </td>
                        </tr>
                                <tr>
                            <td>
                                <div runat="server" id="CSSParms" visible="false" >
                                    <table>
                                    <tr>
                                        <td>
                                            <asp:Label Text="# Of Units" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="CSSNumberOfUnitsTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label Text="Total Weight(cts)" runat="server" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="CSSTotalWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            <asp:Label Text="Single Unit Weight  " runat="server" ></asp:Label>
                                            <asp:TextBox runat="server" ID="CSSingleWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;" OnTextChanged="CSSingleWeightTxt_TextChanged"/>
                                        </td>
                                    </tr>
                                    
                                            
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div runat="server" id="DiaParms" visible ="false">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Table (mm)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="TableTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Max (mm)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="MaxTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Min (mm)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="MinTxt" readonly="false" Style="right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="H_x (mm)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="H_xTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Tab_D (%)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="Tab_DTxt" readonly="false" Style="right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Total Depth (%)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="TotaDepthTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Calc. Weight(cts)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="DiaCalcWeightTxt" readonly="false" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Shape" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="DiaShapeTxt" readonly="true" Style="margin-right: 0px; width: 100px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Variety" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="CSVarietyTxt" readonly="true" Style="margin-right: 0px; width: 100px;" Visible ="false"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Meas. Weight(cts)" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="DiaMeasWeight" readonly="false" Style="margin-right: 0px; width: 100px;"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                             </td>
                        </tr>
                            <tr>
                                <td>
                                    <div runat="server" id="MetalParms" visible="false">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Metal Color" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MetalColorList" OnSelectedIndexChanged="OnMetalColorChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Metal Stamp" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MetalStampList" OnSelectedIndexChanged="OnMetalStampChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Karatage" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MetalKaratageList" OnSelectedIndexChanged="OnMetalKaratageChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Chain Length" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="ChainLenghtTxt" readonly="false" Style="margin-right: 0px; width: 100%;"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Engraving" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="EngravingTxt" ReadOnly="false" Style="margin-right: 0px; width: 100%;"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div runat="server" id="MountParms" visible="false">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Metal" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MountMetalList" OnSelectedIndexChanged="OnMountMetalChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Mounting Type" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MountTypeList" OnSelectedIndexChanged="OnMountTypeChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label Text="Karatage" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="MountKaratageList" OnSelectedIndexChanged="OnMountKaratageChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <asp:Label Text="Stamp" runat="server" ></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="MountStampList" OnSelectedIndexChanged="OnMountStampChanged" 
                                                    AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" style="margin-right;width=100%" />
                                            </td>
                                        </tr>
                                        </table>
                                    </div>
                                    <div runat="server" id="AddShapeParms" visible="false">
                                        <table>
                                            <tr>
                                                <td>
                                                <asp:Label runat="server" Text="Add New Shape" BorderStyle="Double"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" Text="New Shape"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="NewShapeTxt" readonly="false" Style="margin-right: 0px; width: 200px;"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" Text="WCF"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="NewWCFTxt" readonly="false" Style="margin-right: 0px; width: 200px;"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" Text="Shape Type"></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox runat="server" ID="NewShapeTypeTxt" readonly="false" Style="margin-right: 0px; width: 200px;"/>--%>
                                                    <asp:DropDownList runat="server" ID="NewShapeTypeList" OnSelectedIndexChanged="OnShapeTypeListChanged" 
                                AutoPostBack="True" DataValueField="ShapeID" DataTextField="ShapeType" Visible="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="AddNewShapeBtn" CssClass="btn btn-info btn-large" runat="server" Text="Add New Shape"
								                        Style="text-align: center; margin-left: 20px; float: left;" OnClick="OnAddNewShapeBtn_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <asp:Panel runat="server" ID="Pic" CssClass="form-horizontal" Visible='<%# Eval("PicVis")%>' Style="width: 200px; word-wrap: break-word; margin-bottom: 60px;">
												<asp:Label runat="server" ID="PicLbl" Width="200px" Text='<%# Eval("PicLblTxt") %>'></asp:Label>
												<asp:Image runat="server" class="image_gdlight" ID="Image" Width="180px"></asp:Image>
												<asp:Label runat="server" ID="PicErrLbl" ForeColor="Red" Width="200px" Text='<%# Eval("PicErrLblTxt") %>' Visible='<%# Eval("PicErrLblVis")%>'></asp:Label>
											</asp:Panel>
                                    </div>
                                    <div>    
                                    <asp:Label ID="ReqFRanges" runat="server" Text="Fractioin Ranges" CssClass="label"></asp:Label>
                                </div>
                                <div>
                                    <asp:Button runat="server" ID="ReqFRangesBtn" OnClick="OnReqFractionGroupClick" CssClass="btn btn-small btn-info" Text="Fraction Ranges" Width="150px" ToolTip="Fraction Ranges"/>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ReqFRangesGroupList" runat="server" runat="server" AutoPostBack="True" DataTextField="GroupName" OnSelectedIndexChanged="OnReqRangeGroupClick" ToolTip="Range Group List"></asp:DropDownList>
                                    <asp:Button runat="server" ID="ReqFDisplayBtn" OnClick="OnCloseReqRangeGroupClick" CssClass="btn btn-small btn-info" Text="Close" />
                                </div>
                                <div>
                                    <asp:ListBox runat="server" ID="ReqFRangeList" Rows="10" SelectionMode="Multiple" Visible ="False"></asp:ListBox>
                                </div>
                                    <%--<div runat="server" id="MetalParms" visible="false">
                                        <span>Metal Color</span>
                                        <span style="margin-left:200px;">Metal Stamp</span>
                                    </div>
                                    <div>
                                <asp:DropDownList runat="server" ID="MetalColorList" OnSelectedIndexChanged="OnMetalColorChanged" 
                                AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" />
                                <asp:DropDownList runat="server" ID="MetalStampList" OnSelectedIndexChanged="OnMetalStampChanged" 
                                AutoPostBack="True" DataValueField="MeasureValueID" DataTextField="MeasureValueName" Visible="false" />
                            </div>--%>
                                    <br/>
                                </td>
                            </tr>
                        <tr>
                        <td >
                            <div style="width: 450px;" runat="server" id="fieldsetSarin" visible="false">
                                <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px;
                                    "> <%--box-shadow: 0 0 10px #666;--%>
                                    <legend style="width: 130px;padding-left: 20px;margin-bottom: 0px;">Sarin Data</legend>
                                    <asp:Repeater runat="server" ID="RptMeasureSarinList">
                                        <ItemTemplate>
                                            <div style="float: left; border: 1; padding-bottom: 2px; padding-right: 2px; padding-top: 2px;
                                                width: 200px;">
                                                <asp:HiddenField runat="server" ID="MeasureID" Value='<%# Eval("MeasureID") %>' />
                                                <asp:Label runat="server" ID="RepeatLabel" Text='<%# Eval("MeasureName")%>' Style="text-align: right;"></asp:Label>
                                                -
                                                <asp:Label runat="server" ID="RepeatTxt" Text='<%# Eval("MeasureValue")%>' Style="text-align: right;
                                                    color: Black;"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </fieldset>
                            </div>
                        </td>
                        </tr>
                        </table>
                        <br />
                       <%-- </td>
						<td style="vertical-align: top;" rowspan="2" >--%>
							<%--New Measurement Panel--%>
							<asp:Panel runat="server" ID="MeasurementPanel" Visible="false" Style="position: relative;">
								<div style="height: 700px;">
									<asp:HiddenField runat="server" ID="PartEditing" />
									<asp:HiddenField runat="server" ID="ItemEditing" />
									<asp:HiddenField runat="server" ID="ElementEditing" />
									<asp:HiddenField runat="server" ID="CategoryEditing" />
									<asp:Repeater runat="server" ID="ElementPanel">
										<ItemTemplate>
											<asp:HiddenField runat="server" ID="ElementName" Value='<%# Eval("ElemName")%>' />
											<asp:HiddenField runat="server" ID="ElementType" Value='<%# Eval("ElemType") %>' />
											<asp:HiddenField runat="server" ID="ElementMeasure" Value='<%# Eval("ElemMeasure") %>' />
											<asp:Panel runat="server" ID="SmallPanel" visible='<%# Eval("SmVis")%>'>
												<div style="width: 150px; float: left;  margin-bottom: 20px; margin-right: 20px;">
													<asp:Label runat="server" ID="SmLbl" Text='<%# Eval("SmLbl")%>' visible='<%# Eval("SmLblVis")%>'></asp:Label>
													<asp:TextBox runat="server" ID="SmTxt" Text='<%# Eval("SmTxt")%>' readonly='<%# Eval("SmTxtRO")%>' visible='<%# Eval("SmTxtVis")%>' Enabled="false" Style="width: 135px;"></asp:TextBox>
													<asp:Button runat="server" ID="SmBtn" CssClass="btn btn-info btn-large" OnClick="OnSmBtnClick"
														Text='<%# Eval("SmBtn")%>' visible='<%# Eval("SmBtnVis")%>' Style="width: 150px;"></asp:Button>
												</div>
											</asp:Panel>
											<asp:Panel runat="server" ID="BigPanel" visible='<%# Eval("BigVis")%>' Style="position: absolute; width: 492px; top: 200px; background-color: white; border: 2px solid gray; padding: 5px; z-index: 5;">
												<asp:Panel runat="server" ID="DropDownPanel" visible='<%# Eval("DropDownPanelVis")%>'>
													<asp:Label runat="server" ID="BigLbl" Text='<%# Eval("BigLbl")%>' visible='<%# Eval("BigLblVis")%>'></asp:Label>
													<asp:TextBox runat="server" ID="BigTxt" Text='<%# Eval("BigTxt")%>' visible='<%# Eval("BigTxtVis")%>' OnTextChanged="OnTextChanged"></asp:TextBox>
													<ajaxToolkit:DropDownExtender runat="server" TargetControlID="BigTxt" DropDownControlID="DropDownListPanel" />
													<asp:Panel runat="server" ID="DropDownListPanel">
														<asp:ListBox runat="server" ID="DropDownListBox" DataSource='<%# Eval("DropDownEnums")%>'
															DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
															OnSelectedIndexChanged="OnDropDownSelectedIndexChanged"></asp:ListBox>
													</asp:Panel>
												</asp:Panel>
												<asp:Repeater runat="server" ID="TextRepeater" DataSource='<%# Eval("TxtRepeaterSource")%>' visible='<%# Eval("TxtRepeaterVis")%>'>
													<ItemTemplate>
														<div style="display: flex; justify-content: flex-end;">
															<asp:Label runat="server" ID="RepeatLabel" Text='<%# Eval("RepeatLbl")%>' Style="text-align: right;"></asp:Label>
															<asp:TextBox runat="server" ID="RepeatTxt" Value='<%# Eval("RepeatTxt")%>' readonly="true" Style="margin-left: 5px; width: 50px;"/>
														</div>
													</ItemTemplate>
												</asp:Repeater>
												<asp:Repeater runat="server" ID="ButtonRepeater" DataSource='<%# Eval("BtnRepeaterSource")%>' visible='<%# Eval("BtnRepeaterVis")%>'
													OnItemCommand="OnButtonListClick">
													<ItemTemplate>
														<asp:Button runat="server" ID="RepeatButton" CssClass="btn btn-info btn-large"
																Text='<%# Eval("BigBtn")%>'  style="margin: 10px; width: 100px; font-size: 16px; padding-left: 0px; padding-right: 0px; border-bottom-color: #1f6377"/>
														<asp:HiddenField runat="server" ID="BtnValue" Value='<%# Eval("BtnValue")%>' />
														<asp:HiddenField runat="server" ID="BtnLink" Value='<%# Eval("BtnLink")%>' />
														<asp:HiddenField runat="server" ID="BtnCat" Value='<%# Eval("BtnCat")%>' />
														<asp:HiddenField runat="server" ID="BtnMeasure" Value='<%# Eval("BtnMeasure") %>' />
													</ItemTemplate>
												</asp:Repeater>
											</asp:Panel>
											<asp:Panel runat="server" ID="Pic" CssClass="form-horizontal" Visible='<%# Eval("PicVis")%>' Style="width: 200px; word-wrap: break-word; margin-bottom: 60px;">
												<asp:Label runat="server" ID="PicLbl" Width="200px" Text='<%# Eval("PicLblTxt") %>'></asp:Label>
												<asp:Image runat="server" class="image_gdlight" ID="Image" Width="180px"></asp:Image>
												<asp:Label runat="server" ID="PicErrLbl" ForeColor="Red" Width="200px" Text='<%# Eval("PicErrLblTxt") %>' Visible='<%# Eval("PicErrLblVis")%>'></asp:Label>
											</asp:Panel>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</asp:Panel>
						</td>
					</tr>
					<tr>
						<%-- --%>
                        <td style="vertical-align: top;">
        	             
							<asp:Panel runat="server" ID="OldNumPanel" Visible="false">

								<asp:Label runat="server" ID="OldNumLabel" Text="Old #" style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="OldNumText" style="width: 130px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
                            <div style="visibility:hidden;">
							<asp:Panel runat="server" ID="Comments" Visible="false">
								<asp:Label runat="server" Text="Comments" style="float: left; clear: both;"></asp:Label>
								<asp:TextBox runat="server" ID="CommentsList" TextMode="MultiLine" Columns="1" style="width: 130px; height: 542px; float: left;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
                            </div>
						</td> <%-- --%>
                        <td style="vertical-align: top;padding-right:150px;">
                           <asp:Panel runat="server" ID="PrefixPanel" CssClass="label" Visible="false">
								<asp:Label runat="server" ID="PrefixLabel" Text="Prefix" style="float:left; clear:both; font-size: 16px; font-weight: bold;"></asp:Label>
								<asp:TextBox runat="server" ID="PrefixText" style="width: 192px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnPrefixChanged" AutoPostBack="true"></asp:TextBox>
							</asp:Panel>
                            <%--<asp:Label ID="Label1" runat="server" Text="" Width="330px"></asp:Label>--%>
                        </td>
						<td style="vertical-align: top; padding-right:150px;visibility:hidden;">
							<asp:Panel runat="server" ID="PrefixPanel1" CssClass="label" Visible="false">
								<asp:Label runat="server" ID="PrefixLabel1" Text="Prefix" style="float:left; clear:both; font-size: 16px; font-weight: bold;"></asp:Label>
								<asp:TextBox runat="server" ID="PrefixText1" style="width: 192px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnPrefixChanged" AutoPostBack="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="WeightPanel" Visible="false">
								<asp:Label runat="server" ID="WeightTxt" Text="Weight" style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="WeightValue" style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="SarinPanel" Visible="false">
								<asp:Label runat="server" Text="Max/Min Ratio" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="SarinRatio" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
								<br />
								<asp:Label runat="server" Text="Max/Min/Height" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="SarinMeasure" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="LaserPanel" Visible="false">
								<asp:Label runat="server" Text="Laser Inscription" style="float: left; clear: both;"></asp:Label>
								<asp:TextBox runat="server" ID="LaserInscription" style="width: 200px; float: left; clear: both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnLaserInscriptionChanged" AutoPostBack="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="IntExtCommentPanel" Visible="false">
								<asp:Label runat="server" Text="Internal Comments" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="InternalComments" TextMode="MultiLine" Columns="1" Style="width: 200px; height: 150px; margin-bottom: 10px;"
									OnTextChanged="OnIntExtCommentChanged" AutoPostBack="true"></asp:TextBox>
								<asp:Label runat="server" Text="External Comments" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="ExternalComments" TextMode="MultiLine" Columns="1" Style="width: 200px; height: 150px; margin-bottom: 10px;"
									OnTextChanged="OnIntExtCommentChanged" AutoPostBack="true"></asp:TextBox>
							</asp:Panel>

						</td>
					</tr>
				</table>
				<%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px;display: none;border: solid 2px Gray">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px;color: black;text-align:center"
                        id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif;">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OnOkScript="StayFocused" OnCancelScript="StayFocused">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black;padding-top: 10px">Do you want to save the item changes?</div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" OnClick="OnQuesSaveNoClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" OnClick="OnQuesSaveCancelClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close"  Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender" Y="0"
                    PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn" >
                </ajaxToolkit:ModalPopupExtender>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
