﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Utilities;
using Corpt.Models;
using Corpt.TreeModel;
using System.Data;
using System.Collections;
using System.IO;
using Corpt.Models.CustomerProgram;

namespace Corpt
{
    public partial class RetailersVendorInfo : CommonPage
    {
        protected string customerId;
        protected string retailerId;


        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            Page.Title = "GSI: Vendor Info";
            customerId = Request.QueryString["cid"].ToString();
            retailerId = Request.QueryString["rid"].ToString();
            
            ClearMessages();
            if (!IsPostBack)
            {
                SyntheticCustomerModel customer = GetCustomerInfo(customerId);
                lblCustomerName.Text = GetRatailerName(retailerId)  + " - " +GetCustomerName(customerId, customer.customerOfficeId);
                SetRetailerVendors(retailerId);
                LoadCustomerPrograms(customer, retailerId);
                GetAdnSetInitialProgramList();
                BindProgramListForAdding(null, 0);
                LoadSkuFilter();
                SetInitialFilterValues();
                LoadTags();
                LoadBranches(customerId, null);
                LoadGroupOfFraction();
                LoadAdditionalServices();
            }
        }
        #endregion
        private void SetRetailerVendors(string retailerId)
        {
            List<CustomerModel> vendors = new List<CustomerModel>();
            var customers = SyntheticScreeningUtils.GetRetailerCustomers(retailerId, this);
            if (customers != null && customers.Tables.Count > 0)
            {
                var data = customers.Tables[0];
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        var vendor = CustomerModel.SetDataByRetailer(row);
                        vendors.Add(vendor);
                    }
                }
            }
            SetViewState(vendors, "RetailerVendors");
        }
        protected string GetCustomerName(string customerID, string customerOfficeId) 
        {
            string result = "";
            BatchModel btchModel = new BatchModel();
            btchModel.CustomerId = customerID;
            btchModel.CustomerOfficeId = customerOfficeId;
            result = QueryUtils.GetCustomerName(btchModel, this).CustomerName;
            SetViewState(result, "CustomerName");
            return result;
        }

        protected string GetRatailerName(string retailerId)
        {
            string result = "";
            DataTable retailers = SyntheticScreeningUtils.GetRetailers(this);
            DataRow[] foundRows = retailers.Select("RetailerID=" + retailerId);
            if (foundRows != null && foundRows.Length > 0)
            {
                result = foundRows[0]["RetailerName"].ToString();
            }
            SetViewState(result, "RetailerName");
            return result;
        }


        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }

        protected void LoadCustomerPrograms(SyntheticCustomerModel customer, string retailerId)
        {
            List<CustomerProgramModel> cpList = SyntheticScreeningUtils.GetCustomerPrograms(customer, this).OrderBy(x => x.CustomerProgramName).ToList();
            //get block list of cp
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CustomerPrograms, Page);
            //remove items with empty CustomerProgramName
            cpList = cpList.Where(x => !string.IsNullOrEmpty(x.CustomerProgramName.Trim())).Distinct().ToList();
            //Remove items from a list of cp with a CustomerProgramName equals value from block list
            cpList = cpList.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName))).ToList();

             //List<CustomerProgramModel> cpListByRetailer = SyntheticScreeningUtils.GetCustomerProgramsByRetailer(customer, Convert.ToInt32(retailerId), this);
            ////Get only sku in retailer list
            //cpList = cpList.Where(x => cpListByRetailer.Exists(y => x.CustomerProgramName.Equals(y.CustomerProgramName))).ToList();

            SetSkuNature(cpList);

            BuildCPLinks(cpList);
            CpPanel.Visible = cpList.Count > 0;
            BindCPList();
        }

        private SyntheticCustomerModel GetCustomerInfo(string customerID)
        {            
            var officeId = Request.QueryString["cod"].ToString();
            var customerCode = GetCustomerCode(customerId);
            SetViewState(customerCode, "CustomerCode");
            SyntheticCustomerModel customer = new SyntheticCustomerModel();
            customer.customerOfficeId = officeId;
            customer.customerId = customerID;
            customer.customerCode = customerCode;            
            return customer;
        }

        private void BindCPList()
        {
            List<CustomerProgramModel> cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            CpDropDown.DataSource = cpList;
            CpDropDown.DataBind();
            CpDropDown.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            CpDropDown.SelectedIndex = 0;
            OnCpDropDownSelectedChanged(null, null);
        }

        private void BuildCPLinks(List<CustomerProgramModel> cpList)
        {
            lblCp.Text = "";
            if (cpList.Count > 0)
            {
                foreach (CustomerProgramModel cp in cpList)
                {
                    var text = string.Format("<a href='/CustomerProgramNew.aspx?CPName={0}&CustomerId={1}' target='_blank'>{0}</a>, ", cp.CustomerProgramName, cp.CustomerId);
                    lblCp.Text += text;
                }
            }
        }

        protected void LoadSkuFilter()
        {
            BuildSkuCategories();
            BuildSkuTypes();
            BuildSkuNatural();
            GetAllBrands();
            BindBrandList();
            BuildSkuPrograms(null);
            List<CustomerProgramModel> cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            BuildSkuStructures(cpList); 
            SkuFilterPanel.Visible = true;
        }

        protected void SetInitialFilterValues()
        {
            var nature = Request.QueryString["nt"].ToString();
            var brand = Request.QueryString["br"].ToString();
            var program = Request.QueryString["prm"].ToString();
            if (!string.IsNullOrEmpty(nature) && !"-1".Equals(nature))
            {
                ddlSkuNatural.SelectedValue = nature;
                OnSkuNaturalSelectedChanged(null, null);
            }
            if (!string.IsNullOrEmpty(brand) && !"-1".Equals(brand))
            {
                ddlSkuBrand.SelectedValue = brand;
                OnBrandSelectedChanged(null, null);
            }
            if (!string.IsNullOrEmpty(program) && !"-1".Equals(program))
            {
                ddlSkuProgram.SelectedValue = program;
                OnSkuProgramSelectedChanged(null, null);
            }
        }

        private void BuildSkuTypes()
        {
            ddlSkuType.DataSource = GetSKUTypes();
            ddlSkuType.DataTextField = "Text";
            ddlSkuType.DataValueField = "Value";
            ddlSkuType.DataBind();
            ddlSkuType.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            ddlSkuType.SelectedIndex = 0;
        }

        private void BuildSkuCategories()
        {
            ddlSkuCategory.DataSource = GetSKUCategories();
            ddlSkuCategory.DataTextField = "Text";
            ddlSkuCategory.DataValueField = "Value";
            ddlSkuCategory.DataBind();
            ddlSkuCategory.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            ddlSkuCategory.SelectedIndex = 0;
        }

        private void BuildSkuNatural()
        {
            ddlSkuNatural.DataSource = GetSKUNatural();            
            ddlSkuNatural.DataTextField = "Text";
            ddlSkuNatural.DataValueField = "Value";
            ddlSkuNatural.DataBind();
            ddlSkuNatural.Items.Insert(0, new ListItem(SessionConstants.AllTypes, String.Empty));
            ddlSkuNatural.SelectedIndex = 0;
        }

        private void BindBrandList()
        {
            ddlSkuBrand.DataSource = GetBrands();
            ddlSkuBrand.DataTextField = "BrandName";
            ddlSkuBrand.DataValueField = "BrandId";
            ddlSkuBrand.DataBind();
            ddlSkuBrand.Items.Insert(0, new ListItem(SessionConstants.ShowAll, String.Empty));
            ddlSkuBrand.SelectedIndex = 0;
        }

        private void BuildSkuPrograms(string nature)
        {
            var programs = GetViewState("InitialProgramList") as List<BrandProgramModel>;
            var resultList = programs;
            var natureSorted = programs;
            if (nature != null)
            {
                natureSorted = programs.FindAll(x => x.Nature == nature).OrderBy(y => y.ProgramName).ToList();
                resultList = natureSorted;
            }
            var brandName = ddlSkuBrand.SelectedItem.Value != null ? ddlSkuBrand.SelectedItem.Text : "";
            if (!string.IsNullOrEmpty(brandName) && !SessionConstants.ShowAll.Equals(brandName))
            {
                resultList = natureSorted.FindAll(x => x.BrandName == brandName).ToList();
            }
            ddlSkuProgram.DataSource = resultList;
            ddlSkuProgram.DataTextField = "DisplayProgramName";
            ddlSkuProgram.DataValueField = "ID";
            ddlSkuProgram.DataBind();
            ddlSkuProgram.Items.Insert(0, new ListItem(SessionConstants.ShowAll, String.Empty));
            ddlSkuProgram.SelectedIndex = 0;
            OnSkuProgramSelectedChanged(null, null);
        }        

        private List<BrandModel> GetBrands()
        {
            List<BrandModel> allBrands = GetViewState("FullBrandList") as List<BrandModel>;
            List<BrandProgramModel> allPrograms = GetViewState("InitialProgramList") as List<BrandProgramModel>;
            var brandListFromPrograms = allPrograms.GroupBy(x => x.BrandName).Select(y => y.First()).ToList();
            List<BrandModel> result = allBrands;
            var nature = ddlSkuNatural.SelectedItem.Value != null ? ddlSkuNatural.SelectedItem.Text : "";
            if (!string.IsNullOrEmpty(nature) && nature != SessionConstants.AllTypes)
            {
                var natureName = SessionConstants.LGfull.Equals(nature) ? SessionConstants.LGshort : nature;
                var natureSorted = brandListFromPrograms.FindAll(x => x.Nature == natureName).ToList();
                result = result.Where(x => natureSorted.Exists(y => x.BrandId.Equals(y.BrandId))).ToList();
            }
            return result;
        }

        private void GetAllBrands()
        {
            List<BrandModel> brands = SyntheticScreeningUtils.GetAllBrands(this);
            SetViewState(brands, "FullBrandList");
        }

        protected List<ListItem> GetSKUCategories() 
        {
            List<ListItem> result = new List<ListItem>();
            result.Add(new ListItem { Value = "1", Text= "Bridal" });
            result.Add(new ListItem { Value = "2", Text = "Fashion" });
            result.Add(new ListItem { Value = "3", Text = "3stone" });
            return result;
        }
        protected List<ListItem> GetSKUTypes()
        {
            List<ListItem> result = new List<ListItem>();
            result.Add(new ListItem { Value = "1", Text = "Act" });
            result.Add(new ListItem { Value = "2", Text = "Min" });
            return result;
        }
        protected List<ListItem> GetSKUNatural()
        {
            List<ListItem> result = new List<ListItem>();
            result.Add(new ListItem { Value = "158", Text = "Natural" });
            result.Add(new ListItem { Value = "6264", Text = "Lab Grown" });
            return result;
        }

        protected List<ItemTypeModel> GetSkuStructuresByCustomer(List<CustomerProgramModel> cpList)
        {
            List<ItemTypeModel> result = new List<ItemTypeModel>();
            var itemTypesAll = QueryCpUtilsNew.GetItemTypesByGroup(0, this);
            
            if (cpList.Count > 0)
            {
                List<ItemTypeModel> tempList = new List<ItemTypeModel>();
                foreach (CustomerProgramModel cp in cpList)
                {
                    ItemTypeModel itm = itemTypesAll.Find(x => x.ItemTypeId == cp.ItemTypeId);
                    if (itm != null)
                    {
                        tempList.Add(itm);
                    }
                }
                result = tempList.Distinct().OrderBy(y => y.ItemTypeName).ToList();
            }
            
            return result;
        }
        protected void BuildSkuStructures(List<CustomerProgramModel> cpList)
        {
            List<ItemTypeModel> itemTypeList = GetSkuStructuresByCustomer(cpList);
            ddlSkuStructure.DataSource = itemTypeList;
            ddlSkuStructure.DataTextField = "ItemTypeName";
            ddlSkuStructure.DataValueField = "ItemTypeId";
            ddlSkuStructure.DataBind();
            ddlSkuStructure.Items.Insert(0, new ListItem(SessionConstants.ShowAll, String.Empty));
            ddlSkuStructure.SelectedIndex = 0;            
        }

        protected void LoadTags()
        {
            TagsPanel.Visible = true;
            lblTags.Text = "Temporary empty. Waiting for details.";
        }

        protected void LoadCpInfo(CpEditModel cpEdit)
        {
            var cpId = cpEdit.Cp.CpId;
            CustomerProgramModel cpInfo = SyntheticScreeningUtils.GetCPInfo(cpId, this);
            ShowPicture(cpInfo.Path2Picture);
            lblCpDescr.Text = cpInfo.Comment;
            CpDescriptionPanel.Visible = !string.IsNullOrEmpty(cpInfo.Comment);
            lblCpComment.Text = cpInfo.Description;
            CpCommentPanel.Visible = !string.IsNullOrEmpty(cpInfo.Description);            
            BuildDocsInfo(cpEdit);
            BuildPriceInfo(cpEdit);
        }

        private List<TreeViewModel> GetCpReportsTree()
        {
            //CpReportsTree.Nodes.Clear();
            List<TreeViewModel> reportsList = new List<TreeViewModel>();
            var documents = GetCpEditFromView().Reports;
            var docTypes = documents.FindAll(m => !m.IsAttachedToCp);
            if (docTypes != null && docTypes.Count > 0)
            {
                docTypes.Sort((m1, m2) => string.CompareOrdinal(m1.OperationTypeName.ToUpper(), m2.OperationTypeName.ToUpper()));
                foreach (var docType in docTypes)
                {
                    var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = docType.DocumentTypeCode, DisplayName = docType.DisplayUrl } };
                    var docsByType = documents.FindAll(m => m.IsAttachedToCp && m.DocumentTypeCode == docType.DocumentTypeCode);
                    docsByType.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
                    foreach (var docByType in docsByType)
                    {
                        data.Add(new TreeViewModel { ParentId = docByType.DocumentTypeCode, Id = docByType.Key, DisplayName = docByType.DisplayUrl });
                    }
                    var root = TreeUtils.GetRootTreeModel(data);
                    var rootNode = new TreeNode(root.DisplayName, root.Id);
                    TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                    //CpReportsTree.Nodes.Add(rootNode);
                    reportsList.AddRange(data);
                }
            }            
            return reportsList;
        }

        private void BuildDocsInfo(CpEditModel cpEdit)
        {
            foreach (CpDocModel doc in cpEdit.CpDocs)
            {
                doc.AttachedReports = QueryCpUtilsNew.GetAttachedReportsByDoc(doc, cpEdit.Reports, this);
            }
            List<CpDocModel> newdocs = new List<CpDocModel>();
            List<CpDocModel> docsNonDef = new List<CpDocModel>();
            for (var i = 0; i < cpEdit.CpDocs.Count; i++)
            {
                if (cpEdit.CpDocs[i].IsDefault)
                {
                    cpEdit.CpDocs[i].Title = "Default";
                    newdocs.Add(cpEdit.CpDocs[i]);
                    break;
                }
            }
            for (var i = 0; i < cpEdit.CpDocs.Count; i++)
            {
                int j = 1;
                if (cpEdit.CpDocs[i].IsDefault)
                    continue;
                cpEdit.CpDocs[i].Title = "Document " + (j);
                newdocs.Add(cpEdit.CpDocs[i]);
                docsNonDef.Add(cpEdit.CpDocs[i]);
                j++;
            }
            CpDocsGrid.DataSource = newdocs;
            CpDocsGrid.DataBind();
            CpDocPanel.Visible = newdocs.Count > 0;
            ddlCpDocs.DataSource = docsNonDef;
            ddlCpDocs.DataBind();
            ddlCpDocs.Visible = docsNonDef.Count > 0;
            TreePanel.Visible = docsNonDef.Count > 0;
            SetViewState(cpEdit, SessionConstants.CpEditModel);
            //NewReportPicturePanel.Visible = false;
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
        }

        private void BuildPriceInfo(CpEditModel cpEdit)
        {
            LoadPriceRange(cpEdit);
            LoadAddServicePrices(cpEdit);
        }

        private void LoadPriceRange(CpEditModel cpEdit)
        {
            List<PriceRangeModel> cpPrices = cpEdit.CpPrice.PriceRanges;
            CpPricingPanel.Visible = cpPrices.Count > 0;
            PriceRangeGrid.DataSource = cpPrices.Count == 0 ? null : cpPrices;
            PriceRangeGrid.DataBind();
            PriceRangeGrid.Visible = cpPrices.Count > 0;
            lblPriceRange.Visible = cpPrices.Count > 0;
        }

        private void ClearPricePanel()
        {
            PriceRangeGrid.DataSource = null;
            PriceRangeGrid.DataBind();
            PriceRangeGrid.Visible = false;
            lblPriceRange.Visible = false;
            AddServicePriceGrid.DataSource = null;
            AddServicePriceGrid.DataBind();
            AddServicePriceGrid.Visible = false;
            lblAddServ.Visible = false;
            CpPricingPanel.Visible = false;
        }

        protected void OnPriceRangeDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as PriceRangeModel;
            if (priceModel == null) return;

            var lblFrom = e.Item.FindControl("lblFrom") as Label;
            if (lblFrom != null) lblFrom.Text = "" + priceModel.ValueFrom;


            var lblTo = e.Item.FindControl("lblTo") as Label;
            if (lblTo != null) lblTo.Text = "" + priceModel.ValueTo;


            var lblPrice = e.Item.FindControl("lblPrice") as Label;
            if (lblPrice != null) lblPrice.Text = "" + priceModel.ValueTo;
        }

        private void LoadAddServicePrices(CpEditModel cpEditModel)
        {
            AddServicePriceGrid.DataSource = cpEditModel.CpPrice.AddServicePrices.Count == 0 ? null : cpEditModel.CpPrice.AddServicePrices;
            AddServicePriceGrid.DataBind();
            AddServicePriceGrid.Visible = cpEditModel.CpPrice.AddServicePrices.Count > 0;
            lblAddServ.Visible = cpEditModel.CpPrice.AddServicePrices.Count > 0;
        }
        protected void OnAddServicePriceDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as AddServicePriceModel;
            if (priceModel == null) return;

            var lblServiceName = e.Item.FindControl("lblServiceName") as Label;
            if (lblServiceName !=null && !string.IsNullOrEmpty(priceModel.ServiceId))
            {
                var serviceList = GetViewState(SessionConstants.CpCommonAdditionalServices) as List<AddServiceModel>;
                var selected = serviceList.Find(x => x.ServiceId == priceModel.ServiceId);
                if (selected != null)
                {
                    lblServiceName.Text = selected.ServiceName;
                    var lblServicePrice = e.Item.FindControl("lblServicePrice") as Label;
                    if (lblServicePrice != null) lblServicePrice.Text = "" + priceModel.Price;
                }
            }            
        }

        protected void OnDocsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.DataItem != null)
                {
                    Label lblDocTitle = (Label)e.Row.FindControl("lblDocTitle");
                    HiddenField hdnDocId = (HiddenField)e.Row.FindControl("hdnDocId");
                    ListBox reportList = (ListBox)e.Row.FindControl("DocAttachedReportsList");
                    var title = DataBinder.Eval(e.Row.DataItem, "Title").ToString();
                    lblDocTitle.Text = title;
                    var docId = DataBinder.Eval(e.Row.DataItem, "CpDocId").ToString();
                    hdnDocId.Value = docId;
                    var reports = DataBinder.Eval(e.Row.DataItem, "AttachedReports");
                    reportList.DataSource = reports;
                    reportList.DataBind();
                }
            }
        }
        protected void LoadReportByTypeClick(object sender, EventArgs e)
        {
            //LoadCpDocRules(ddlCpDocs.SelectedValue, DocPartTree.SelectedValue);
            List<TreeViewModel> reportsList = GetCpReportsTree();
            string urlName = null;
            Button btn = (Button)sender;
            string btnText = btn.Text;
            foreach (var report in reportsList)
            {
                if (report.DisplayName.Contains(btnText))
                {
                    urlName = report.DisplayName;
                    int first = urlName.IndexOf("'");
                    urlName = urlName.Substring(first + 1);
                    int second = urlName.IndexOf("'");
                    urlName = urlName.Substring(0, second);
                    break;
                }
            }

            CpEditModel editData = new CpEditModel();
            editData = GetViewState(SessionConstants.CpEditModel) as CpEditModel;

            urlName = urlName.Replace("DefineDocument", "DefineDocumentNew");

            foreach (var cpDocs in editData.CpDocs)
            {
                if (cpDocs.AttachedReports.Count > 0)
                {
                    string[] parm = (urlName.Substring(urlName.IndexOf("?") + 1)).Split('&');
                    string[] cpKeys = parm[0].Split('=');
                    string cpKey = cpKeys[1].Substring(cpKeys[1].IndexOf("_") + 1);
                    if (cpKey != cpDocs.CpId)
                        continue;
                    foreach (var report in cpDocs.AttachedReports)
                    {
                        if (!report.OperationTypeName.Contains(btnText))
                            continue;
                        string key = report.Key;
                        urlName += "&Report_Key=" + key;
                        string docId = cpDocs.CpDocId.ToString();
                        urlName = urlName.Replace("CpDocId=", "CpDocId=" + docId);
                    }
                }
            }
            if (!urlName.Contains("Report_Key="))
            {
                var docTitle = (btnText.Contains("LBL") || btnText.Contains("SRT")) ? "Default" : "Document 1";
                var docId = editData.CpDocs.Where(x => x.Title.Contains(docTitle)).FirstOrDefault().CpDocId.ToString();
                if (!string.IsNullOrEmpty(docId))
                {
                    urlName = urlName.Replace("CpDocId=", "CpDocId=" + docId);
                }
            }
            string modifiedURL = "window.open('" + urlName + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", modifiedURL, true);
            Session["RVEditData"] = editData;
        }
        protected void OnDocsGridDataBound(object sender, DataGridItemEventArgs e)
        {
            
        }
        private void ShowPicture(string dbPicture)
        {
            itemPicture.ImageUrl = "";
            if (dbPicture == null || dbPicture == "")
            {
                ErrPictureField.Text = "no pictures";
                return;
            }
            else
            {
                int picLength = dbPicture.Length;
                string tmpPic = dbPicture.ToLower();
                dbPicture = dbPicture.Replace(@"\", @"/");
                if (dbPicture.Substring(picLength - 1, 1) == @"/" || tmpPic.Contains("default"))
                {
                    ErrPictureField.Text = "no pictures";
                    return;
                }
            }
            string errMsg;
            var ms = new MemoryStream();
            var fileType = "";
            var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
            if (result)
            {
                var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exist.";
            }
            ErrPictureField.Text = errMsg;
            CpImagePanel.Visible = true;
        }
        protected void LoadBranches(string customerId, string programId)
        {
            var retailerName = GetViewState("RetailerName") as string;
            CustomerModel objCustomerModel = new CustomerModel();
            objCustomerModel.CustomerId = customerId;
            var customers = QueryUtils.GetCustomerCompanyGroup(objCustomerModel, this);
            var companyGroupName = "";
            List<CustomerModel> fullGroupList = new List<CustomerModel>();
            List<CustomerModel> groupList = new List<CustomerModel>();

            if (customers != null && customers.Rows.Count > 0)
            {
                companyGroupName = customers.Rows[0]["CompanyGroupName"].ToString();
                foreach (DataRow row in customers.Rows)
                {
                    var customer = CustomerModel.SetData(row);
                    fullGroupList.Add(customer);
                }
            }
            groupList = fullGroupList;

            var retailerVendors = GetViewState("RetailerVendors") as List<CustomerModel>;
            //sorting by Retailer
            groupList = groupList.Where(x => retailerVendors.Exists(y => x.CustomerId.Equals(y.CustomerId))).ToList();

            cblGroupList.DataValueField = "CustomerID";
            cblGroupList.DataTextField = "CustomerName";
            cblGroupList.DataSource = groupList;
            cblGroupList.DataBind();

            if (groupList.Count > 0)
            {
                cblGroupList.Visible = true;
                groupListTr.Visible = true;
                lblGroupName.Text = string.Format("Vendors of group {0} working with retailer {1}", companyGroupName, retailerName);
            }
            else
            {
                cblGroupList.Visible = false;
                groupListTr.Visible = false;
                lblGroupName.Text = customers.Rows.Count > 0 ? string.Format("There is no vendors in group {0} working with retailer {1}", companyGroupName, retailerName) : "The company does not belong to any group";
            }
            BranchesPanel.Visible = true;

            EnableGroupForEditing(programId);
        }

        private void EnableGroupForEditing(string programId)
        {
            if (!string.IsNullOrEmpty(programId))
            {
                foreach (ListItem groupMember in cblGroupList.Items)
                {
                    var vendorCode = groupMember.Text.ToString().Split('#')[1];
                    var isAssigned = SyntheticScreeningUtils.IsProgramAssingedToVendor(Convert.ToInt32(programId), Convert.ToInt32(vendorCode), this);
                    groupMember.Selected = isAssigned;
                    groupMember.Enabled = isAssigned;
                }
            }
        }
        private void ClearGroupSettings() 
        {
            foreach (ListItem groupMember in cblGroupList.Items)
            {
                groupMember.Selected = false;
                groupMember.Enabled = true;
            }
        }

        protected void LoadGroupOfFraction()
        {
            GoFPanel.Visible = true;
            lblGoF.Text = "Temporary empty. Waiting for details.";
        }
        private void LoadDocPartsTree()
        {
            CpEditModel cpModel = GetCpEditFromView();
            var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            DocPartTree.Nodes.Clear();
            DocPartTree.Nodes.Add(rootNode);
            DocPartTree.Visible = true;
            if (DocPartTree.Visible)
            {
                DocPartTree.Nodes[0].Select();
            }
            //CpPartTreePanel.Visible = true;
            OnDocPartsTreeChanged(null, null);
        }

        private void LoadCpDocRules(string docId, string partId, int idx = 0, int idx1 = 0)
        {
            if (DocPartTree.SelectedNode != null)
            {
                var partName = DocPartTree.SelectedNode.Text.Trim();
                PartName.Text = partName;
                PartName.Visible = true;
                string cpOfIdAndId = GetCPOfficeIdAndCpID();
                var cpRules = QueryUtils.GetCpRulesByCp(cpOfIdAndId, this);
                var editRules = cpRules.FindAll(m => m.CpDocId == docId);
                var editRulesNow = editRules.FindAll(m => m.PartName == partName);
                //var liRules = editRulesNow.FindAll(x => x.MeasureName == "LI Font" || x.MeasureName == "LI Position" || x.MeasureName == "LI Present" || x.MeasureName == "LI Type");
                //liRules.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureName.ToUpper(), m2.MeasureName.ToUpper()));
                var dt = new DataTable("CpRules");
                dt.Columns.Add("Measure");
                dt.Columns.Add("Minimum Value");
                dt.Columns.Add("Maximum Value");
                dt.AcceptChanges();
                foreach (var cpRuleModel in editRulesNow)
                //    foreach (var cpRuleModel in liRules)
                {
                    dt.Rows.Add(new object[]
                    {
                    cpRuleModel.MeasureName,
                    cpRuleModel.MinName,
                    cpRuleModel.MaxName
                    });
                }
                dt.AcceptChanges();
                RulesGrid.DataSource = dt;
                RulesGrid.DataBind();
                Rules.Visible = true;
            }            
            else
            {
                PartName.Visible = false;
                RulesGrid.DataSource = null;
                RulesGrid.DataBind();
                Rules.Visible = false;
            }          

        }
        private void LoadAdditionalServices()
        {
            SetViewState(QueryCpUtilsNew.GetAdditionalServices(this), SessionConstants.CpCommonAdditionalServices);
        }
        protected string GetCPOfficeIdAndCpID()
        {
            var cplist = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            var cp = cplist.Find(m => m.CpId == CpDropDown.SelectedValue);
            return cp.CpOfficeIdAndCpId;
        }

        private CpEditModel GetCpEditFromView()
        {
            return GetViewState(SessionConstants.CpEditModel) as CpEditModel;
        }

        protected void OnCpDropDownSelectedChanged(object sender, EventArgs e)
        {
            if (CpDropDown.SelectedIndex != 0)
            {
                var cpName = CpDropDown.SelectedItem.Text;
                var cpId = CpDropDown.SelectedItem.Value;
                var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                    new List<CustomerProgramModel>();
                var cp = cpList.Find(m => m.CpId == CpDropDown.SelectedValue);
                var cpEdit = QueryCpUtilsNew.GetCustomerProgramFull(cp, GetEnumMeasure(), this);
                cpEdit.Customer = GetCustomerModelFromView(cpEdit.Cp.CustomerId);
                cpEdit.Vendor = GetCustomerModelFromView(cpEdit.Cp.VendorId);
                string nature = cp.IsNatural ? "Natural" : "LG";
                var customerCode = GetViewState("CustomerCode") as string;
                int selectedProgram = SyntheticScreeningUtils.GetSkuProgramId(customerCode, cpId, this);
                BindProgramListForAdding(nature, selectedProgram);
                ddlProgramList.Enabled = true;
                LoadCpInfo(cpEdit);
                GetItemsByCp(cpName);
                GetCertificateByCp(cpName);
                GetMemoFilesByCp(cpName);
                LoadDocPartsTree();
                List<CustomerModel> cbsList = SyntheticScreeningUtils.GetCustomersBySku(cpName, this);
                SetViewState(cbsList, "CustomersBySkuList");
            }
            else 
            {                
                ClearCpInfo();
            }
            
        }
        private CustomerModel GetCustomerModelFromView(string customerId)
        {
            if (string.IsNullOrEmpty(customerId)) return null;
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);
            customers = customers.Where(x => !excluded.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
            return customers.Find(m => m.CustomerId == customerId);

        }
        protected void OnProgramDropDownSelectedChanged(object sender, EventArgs e)
        {
            if (ddlProgramList.SelectedIndex != 0)
            {
                var programId = ddlProgramList.SelectedItem.Value;
                EnableGroupForEditing(programId);
            }
            else 
            {
                ClearGroupSettings();
            }
        }


        protected void OnSkuStructureSelectedChanged(object sender, EventArgs e)
        {
            var skuStructure = ddlSkuStructure.SelectedIndex !=0 ? Convert.ToInt32(ddlSkuStructure.SelectedItem.Value) : 0;
            var skuProgram = ddlSkuProgram.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuProgram.SelectedItem.Value) : 0;
            var skuNatural = ddlSkuNatural.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuNatural.SelectedItem.Value) : 0;
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            //We check if natural filter have selected value            
            if (skuNatural != 0)
            {
                cpList = GetSkuListByNature(skuNatural, cpList);
            }
            
            //We check if program filter have selected value
            if (skuProgram != 0)
            {
                cpList = GetSkuListByProgram(skuProgram, cpList);
            }
            if (skuStructure != 0)
            {
                cpList = SortByStructure(skuStructure, cpList);
            }
            //BindCPList(cpList);
            BuildCPLinks(cpList);
        }

        private static List<CustomerProgramModel> SortByStructure(int skuStructure, List<CustomerProgramModel> cpList)
        {
            return cpList.FindAll(x => x.ItemTypeId == skuStructure).OrderBy(y => y.CustomerProgramName).ToList();
        }

        protected void OnSkuNaturalSelectedChanged(object sender, EventArgs e)
        {
            var skuNatural = ddlSkuNatural.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuNatural.SelectedItem.Value) : 0;
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            BindBrandList();
            if (skuNatural != 0)
            {
                cpList = GetSkuListByNature(skuNatural, cpList);
                string selectedNature = "Natural".Equals(ddlSkuNatural.SelectedItem.Text) ? "Natural" : "LG";
                BuildSkuPrograms(selectedNature);
            }
            else 
            {
                BuildSkuPrograms(null);
            }
            //we have to rebuild Structure filter with new list
            BuildSkuStructures(cpList);
            //BindCPList(cpList);
            BuildCPLinks(cpList);
        }
        private List<CustomerProgramModel> GetSkuListByNature(int skuNatural, List<CustomerProgramModel> cpList)
        {
            SyntheticCustomerModel customer = GetCustomerInfo(customerId);
            var cpListByNature = SyntheticScreeningUtils.GetCustomerProgramsByNature(customer, skuNatural, this);
            //Get only sku with selected nature
            cpList = cpList.Where(x => cpListByNature.Exists(y => x.CustomerProgramName.Equals(y.CustomerProgramName))).ToList();
            return cpList;
        }

        private void SetSkuNature(List<CustomerProgramModel> cpList)
        {
            SyntheticCustomerModel customer = GetCustomerInfo(customerId);
            //we get natural list
            var cpListByNature = SyntheticScreeningUtils.GetCustomerProgramsByNature(customer, 157, this);
            //Get only sku with selected nature
            List<CustomerProgramModel> cpNaturalList = cpList.Where(x => cpListByNature.Exists(y => x.CustomerProgramName.Equals(y.CustomerProgramName))).ToList();
            foreach (CustomerProgramModel cp in cpList)
            {
                cp.IsNatural = cpNaturalList.Contains(cp);
            }
            SetViewState(cpList, SessionConstants.CustomerProgramList);
        }

        protected void OnBrandSelectedChanged(object sender, EventArgs e)
        {
            var skuNatural = ddlSkuNatural.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuNatural.SelectedItem.Value) : 0;
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            if (skuNatural != 0)
            {
                string selectedNature = "Natural".Equals(ddlSkuNatural.SelectedItem.Text) ? "Natural" : "LG";
                BuildSkuPrograms(selectedNature);
            }
            else
            {
                BuildSkuPrograms(null);
            }
        }
        protected void OnSkuProgramSelectedChanged(object sender, EventArgs e)
        {
            var skuNatural = ddlSkuNatural.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuNatural.SelectedItem.Value) : 0;
            var skuProgram = ddlSkuProgram.SelectedIndex != 0 ? Convert.ToInt32(ddlSkuProgram.SelectedItem.Value) : 0;
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            //We check if natural filter have selected value
            if (skuNatural != 0)
            {
                cpList = GetSkuListByNature(skuNatural, cpList);
            }
            //We get list by program (if ShowAll - we show all sku with any program assinged)
            cpList = GetSkuListByProgram(skuProgram, cpList);
            //we have to rebuild Structure filter with new list
            BuildSkuStructures(cpList);
            //BindCPList(cpList);
            BuildCPLinks(cpList);            
        }        

        private List<CustomerProgramModel> GetSkuListByProgram(int skuProgram, List<CustomerProgramModel> cpList)
        {
            SyntheticCustomerModel customer = GetCustomerInfo(customerId);
            var cpListByProgram= SyntheticScreeningUtils.GetCustomerProgramsByProgram(customer, skuProgram, this);
            //Get only sku with selected program
            cpList = cpList.Where(x => cpListByProgram.Exists(y => x.CustomerProgramName.Equals(y.CustomerProgramName))).ToList();
            return cpList;
        }

        protected void OnCpDocsSelectedChanged(object sender, EventArgs e)
        {
            LoadDocPartsTree();
        }

        protected void OnDocAttachPrintDocSelection(object sender, EventArgs e)
        {
            var list = sender as ListBox;
            var selected = list.SelectedValue;
            var hdn = list.Parent.Parent.FindControl("hdnDocId") as HiddenField;
            var docId = hdn.Value;            
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
            var customerCode = GetViewState("CustomerCode") as string;
            OnCorelFileChange(docId, selected, customerCode);
        }

        private string GetCustomerCode(string customerId)
        {
            var customerCode = "";
            DataTable dtCustomerCode = QueryCpUtils.GetCustomerCodeByID(customerId, this);
            if (dtCustomerCode != null || dtCustomerCode.Rows.Count > 0)
            {
                customerCode = dtCustomerCode.Rows[0].ItemArray[0].ToString();
            }

            return customerCode;
        }

        private void OnCorelFileChange(string docId, string selectedReport, string customerCode, string aRep = null)
        {
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
            var programName = CpDropDown.SelectedItem.Text;
            try
            {
                string docName = null, typeId = null, typeCode = null;

                var cpEditModel = GetCpEditFromView();
                var cpDocModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
                if (selectedReport != "")
                {
                    var attachedReport = cpDocModel.AttachedReports.Find(m => "" + m.Key == selectedReport);
                    docName = attachedReport.DocumentName;
                    typeId = attachedReport.OperationTypeId;
                    typeCode = attachedReport.DocumentTypeCode;
                }
                else if (aRep != null)
                {
                    docName = aRep;
                    var attachedReport = cpDocModel.AttachedReports.Find(m => "" + m.DocumentName == docName);
                    if (attachedReport != null)
                        typeId = attachedReport.OperationTypeId;
                    else
                        return;
                }
                else
                    return;
                string corelFile = QueryCpUtilsNew.GetCorelFileName(docName, typeId, this);
                corelFile = corelFile.ToLower();
                string errMsg = "";
                bool rightTypeCode = (typeCode == "1" || typeCode == "2" || typeCode == "3" || typeCode == "4" || typeCode == "7" || typeCode == "15");
                string imageName = null;
                bool skuFound = false;
                if (rightTypeCode)
                {
                    string filter = programName + @", " + customerCode;
                    var existsFiles = Utlities.GetSkuDrawFilesAzure(filter, this, out errMsg);
                    foreach (var file in existsFiles)
                    {
                        if (file.Name.Contains(programName) && file.Name.Contains(customerCode))
                        {
                            string[] parts = file.Name.Split(',');
                            string custCode = parts[3].Substring(1, parts[3].IndexOf(".") - 1);
                            string sku = parts[2];
                            if (custCode.Trim() == customerCode.Trim() && sku.Trim() == programName.Trim())
                                imageName = file.Name;//parts[3].Substring(1);
                            //imageName = file.Name;
                            break;
                        }
                    }
                    if (imageName != null)
                    {
                        MemoryStream skuMs = new MemoryStream();
                        skuFound = Utlities.GetSkuJpgUrlAzure(imageName, this, out errMsg, out skuMs);
                    }
                }

                if (skuFound)
                {
                    var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"sku_jpg/" + imageName;
                    NewReportPictureButton.ImageUrl = ImageUrl;
                    NewReportPictureButton.Width = 250;
                    NewReportPictureButton.Visible = true;
                }
                else if (corelFile.Contains(".xls"))
                {
                    var ImageUrl = "~/Images/ajaxImages/no_image1.png";
                    NewReportPictureButton.ImageUrl = ImageUrl;
                    NewReportPictureButton.Width = 100;
                    NewReportPictureButton.Visible = true;
                }
                else
                {
                    corelFile = corelFile.Replace(".cdr", ".jpg");
                    //corelFile = "2361400.jpg";

                    MemoryStream ms = new MemoryStream();
                    var fileType = "jpg";
                    bool found = Utlities.GetJpgUrlAzure(corelFile.ToLower(), this, out errMsg, out ms);
                    if (found)
                    {
                        var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"cdr_to_jpg/" + corelFile.ToLower();
                        NewReportPictureButton.ImageUrl = ImageUrl;
                        NewReportPictureButton.Width = 250;
                        NewReportPictureButton.Visible = true;
                    }
                    else
                    {
                        var ImageUrl = "~/Images/ajaxImages/no_image1.png";
                        NewReportPictureButton.ImageUrl = ImageUrl;
                        NewReportPictureButton.Width = 100;
                        NewReportPictureButton.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

        }

        protected void OnNewPictureClick(object sender, ImageClickEventArgs e)
        {
            var item = sender as ImageButton;
            if (item.ImageUrl.Contains("no_image") || item.ImageUrl == "")
                return;
            NewReportPicture.ImageUrl = item.ImageUrl;

            NewPicturePopupExtender.Show();
        }

        private void BindProgramListForAdding(string nature, int selected)
        {
            //get all programs assigned to Vendor
            var customerCode = GetViewState("CustomerCode");
            List<BrandProgramModel> programs = SyntheticScreeningUtils.GetVendorPrograms(Convert.ToInt32(customerCode), this);
            ddlProgramList.Items.Clear();
            if (!string.IsNullOrEmpty(nature))
            {
                programs = programs.FindAll(x => x.Nature == nature).OrderBy(y => y.ProgramName).ToList();
            }
            ddlProgramList.DataSource = programs;
            ddlProgramList.DataValueField = "Id";
            ddlProgramList.DataTextField = "DisplayProgramName";
            ddlProgramList.DataBind();
            ddlProgramList.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            var selectedValue = ddlProgramList.Items.FindByValue(selected.ToString());
            if (selectedValue == null)
            {
                ddlProgramList.SelectedIndex = 0;
            }
            else
            {
                selectedValue.Selected = true;
            }
        }

        private void GetAdnSetInitialProgramList()
        {
            List<BrandProgramModel> programs = SyntheticScreeningUtils.GetRetailerPrograms(retailerId, this);
            SetViewState(programs, "InitialProgramList");
        }


        protected void OnDocPartsTreeChanged(object sender, EventArgs e)
        {
            LoadCpDocRules(ddlCpDocs.SelectedValue, DocPartTree.SelectedValue);
        }

        protected void OnSaveProgramClick(object sender, EventArgs e)
        {
            ClearMessages();
            if (CpDropDown.SelectedIndex != 0)
            {
                var cpId = CpDropDown.SelectedItem.Value;
                var cpName = CpDropDown.SelectedItem.Text;
                var programId = ddlProgramList.SelectedItem.Value;
                var programName = ddlProgramList.SelectedIndex != 0 ? ddlProgramList.SelectedItem.Text : "";
                var customerCode  = GetViewState("CustomerCode") as string;
                var message = "";

                List<CustomerModel> selectedList = new List<CustomerModel>();
                //we add vendor on which page we are first
                CustomerModel vendor = new CustomerModel();
                vendor.CustomerCode = customerCode;
                vendor.CustomerName = GetViewState("CustomerName") as string;
                selectedList.Add(vendor);
                //find selected from group list
                foreach (ListItem chkGroupCompany in cblGroupList.Items)
                {
                    if (chkGroupCompany.Selected == true)
                    {
                        vendor = new CustomerModel();
                        var customerId = chkGroupCompany.Value.ToString();
                        vendor.CustomerCode = GetCustomerCode(customerId);
                        vendor.CustomerName = chkGroupCompany.Text.ToString();
                        //we checked has customer code and if value from list not equal to main vendor on which page we are
                        if (vendor.CustomerCode != "" && vendor.CustomerCode != customerCode)
                        {
                            selectedList.Add(vendor);
                        }
                    }
                }                
                //if selected emty program we delete from sku program it has
                if (string.IsNullOrEmpty(programId))
                {
                    var successNames = "";
                    var errorNames = "";
                    
                    foreach (CustomerModel customer in selectedList)
                    {
                        message = SyntheticScreeningUtils.DeleteSkuProgram(customer.CustomerCode, cpId, this);
                        if (message.Contains("Error"))
                        {
                            errorNames = "".Equals(errorNames) ? customer.CustomerName : errorNames + "; " + customer.CustomerName;
                        }
                        else 
                        {
                            successNames = "".Equals(successNames) ? customer.CustomerName : successNames + "; " + customer.CustomerName;
                        }
                    }
                    var deleteSuccess = "".Equals(successNames) ? "" : string.Format("Program "+ programName +" deleted from sku " + cpName + " for vendor(s) {0}", successNames);
                    var deleteError = "".Equals(errorNames) ? "" : string.Format("Deleting program " + programName + " from sku " + cpName + " failed for vendor(s) {0}", errorNames);
                    lblSaveError.Text = deleteError;
                    lblSaveError.Visible = !"".Equals(deleteError);
                    lblSaveSuccess.Text = deleteSuccess;
                    lblSaveSuccess.Visible = !"".Equals(deleteSuccess);
                }
                else //we add program to sku
                {
                    var successNames = "";
                    var errorNames = "";
                    var customersBySku = GetViewState("CustomersBySkuList") as List<CustomerModel>;
                    //we remove vendors who does not have selected sku
                    selectedList = selectedList.Where(x => customersBySku.Exists(y => x.CustomerCode.Equals(y.CustomerCode))).ToList();
                    foreach (CustomerModel customer in selectedList)
                    {                        
                        message = SyntheticScreeningUtils.SaveSkuProgram(customerCode, cpId, programId, this);
                        if (message.Contains("Error"))
                        {
                            errorNames = "".Equals(errorNames) ? customer.CustomerName : errorNames + "; " + customer.CustomerName;
                        }
                        else
                        {
                            successNames = "".Equals(successNames) ? customer.CustomerName : successNames + "; " + customer.CustomerName;
                        }
                    }
                    var addSuccess = "".Equals(successNames) ? "" : string.Format("Program " + programName + " added to sku " + cpName + " for vendor(s) {0}", successNames);
                    var addError = "".Equals(errorNames) ? "" : string.Format("Adding program " + programName + " to sku " + cpName + " failed for vendor(s) {0}", successNames);
                    lblSaveError.Text = addError;
                    lblSaveError.Visible = !"".Equals(addError);
                    lblSaveSuccess.Text = addSuccess;
                    lblSaveSuccess.Visible = !"".Equals(addSuccess);
                }
                OnSkuProgramSelectedChanged(null, null);
            }
            else 
            {                
                return;
            }

        }
        protected void OnRefreshDocsClick(object sender, EventArgs e)
        {
            OnCpDropDownSelectedChanged(null, null);
        }
        
        protected void ClearMessages()
        {
            lblSaveError.Text = "";
            lblSaveError.Visible = false;
            lblSaveSuccess.Text = "";
            lblSaveSuccess.Visible = false;
        }
        protected void GetItemsByCp(string cpName)
        {
            var items = SyntheticScreeningUtils.GetTop10ItemsByCpName(cpName, this);
            var itemsString = "";
            if (items.Tables.Count > 0)
            {
                foreach (DataRow itemRow in items.Tables[0].Rows)
                {
                    itemsString += itemRow["groupcode"] + "." + itemRow["batchcode"] + "." + itemRow["itemcode"] + ", ";
                }
            }
            
            lblCpItems.Text = itemsString;
            CpItemsPanel.Visible = itemsString != "";
        }

        protected void GetCertificateByCp(string cpName)
        {
            string fileType = QueryCpUtils.FileUploaded(cpName.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(" ", "").Replace(@"/", "") + @"_" + customerId.Trim().Replace("Customer ID: ", ""), this);
            string certUrl = "";
            if (fileType != null)
            {
                if (fileType.ToLower() != "jpg" && fileType.ToLower() != "png")
                {
                    string urlPrefix = "";
                    if (fileType == "xls" || fileType == "xlsx" || fileType == "xlsm")
                    {
                        urlPrefix = @"https://view.officeapps.live.com/op/embed.aspx?src=";
                    }
                        
                    certUrl = urlPrefix + @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + cpName.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + customerId.Trim().Replace("Customer ID: ", "") + "." + fileType;
                }
                else
                {
                    certUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + cpName.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + customerId.Trim().Replace("Customer ID: ", "") + "." + fileType;
                }
            }
            else
            {
                DataTable dt = QueryCpUtils.GetSkuMemoUrl(cpName.Trim().Replace("Program Name: ", ""), customerId, this);
                if (dt != null && dt.Rows.Count > 0)
                {
                    certUrl = dt.Rows[0]["SkuReceiptPDF"].ToString();
                }
                    
            }
            if (string.IsNullOrEmpty(certUrl))
            {
                lblCert.Text = "No sku file found";
            }
            else
            {
                string fileName = GetFileNameFromUrl(certUrl);
                lblCert.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", certUrl, fileName);
            }
            CertPanel.Visible = true;
        }

        protected void GetMemoFilesByCp(string cpName)
        {
            DataTable dt = QueryUtils.GetAllSkuMemoPerSku(cpName, customerId, this);
            string memoFiles = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string url = row["MemoReceiptPDF"].ToString();
                    if (!string.IsNullOrEmpty(url))
                    {
                        string fileName = GetFileNameFromUrl(url);
                        memoFiles += string.Format("<a href='{0}' target='_blank'>{1}</a>, ", url, fileName);
                    }
                }
            }            
            if (string.IsNullOrEmpty(memoFiles))
            {
                lblLinesheets.Text = string.Format("Memo files for sku {0} not found", cpName);
            }
            else 
            {
                lblLinesheets.Text = memoFiles;
            }
            LinesheetsPanel.Visible = true;
        }

        private static string GetFileNameFromUrl(string url)
        {
            var urlParts = url.Split('/');
            var idx = urlParts.Length - 1;
            string fileName = urlParts[idx].Split('.')[0];
            return fileName;
        }

        protected void ClearCpInfo()
        {
            lblCpDescr.Text = "";
            CpDescriptionPanel.Visible = false;
            lblCpComment.Text = "";
            CpCommentPanel.Visible = false;
            CpImagePanel.Visible = false;
            lblCpItems.Text = "";
            CpItemsPanel.Visible = false;
            lblCert.Text = "";            
            CertPanel.Visible = false;
            lblLinesheets.Text = "";            
            LinesheetsPanel.Visible = false;
            PartName.Visible = false;
            DocPartTree.Nodes.Clear();
            DocPartTree.Visible = false;
            OnDocPartsTreeChanged(null, null);
            CpDocsGrid.DataSource = null;
            CpDocsGrid.DataBind();
            CpDocPanel.Visible = false;
            ddlCpDocs.DataSource = null;
            ddlCpDocs.DataBind();
            ddlCpDocs.Visible = false;
            TreePanel.Visible = false;
            ddlProgramList.SelectedIndex = 0;
            ClearPricePanel();
            SetViewState(new List<CustomerModel>(), "CustomersBySkuList");
        }
    }
}