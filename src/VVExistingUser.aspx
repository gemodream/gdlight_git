<%@ Page language="c#" Codebehind="VVExistingUser.aspx.cs" AutoEventWireup="True" Inherits="Corpt.VVExistingUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>NewVVActivation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="/VirtualVault/css/main.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bgColor="#f9ffff" MS_POSITIONING="GridLayout">
		<FORM id="Form2" method="post" runat="server">
			<DIV style="Z-INDEX: 101; LEFT: 992px; WIDTH: 440px; POSITION: absolute; TOP: 216px; HEIGHT: 454px"
				ms_positioning="FlowLayout">
				<P>&nbsp;</P>
				<TABLE id="table1" width="418" align="center" style="WIDTH: 418px; HEIGHT: 363px">
					<TR>
						<TD align="center"></TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="table2" width="100%" border="0">
								<TR>
									<TD width="62" style="WIDTH: 62px"><asp:label id="FirstNameLabel" runat="server" CssClass="bold" Width="64px">First Name</asp:label></TD>
									<TD width="211" style="WIDTH: 211px"><asp:textbox id="FirstName" runat="server" CssClass="InputStyleTwo" MaxLength="50" Wrap="False"></asp:textbox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="FirstName"
											Enabled="False"></asp:RequiredFieldValidator></TD>
									<TD width="22" style="WIDTH: 22px"><asp:label id="Label3" runat="server" CssClass="text">M.I.</asp:label></TD>
									<TD><asp:textbox id="MiddleInitial" runat="server" CssClass="inputStyleShort" MaxLength="5"></asp:textbox></TD>
								</TR>
								<TR>
									<TD width="62" style="WIDTH: 62px"><asp:label id="LastNameLabel" runat="server" CssClass="bold" Width="64px">Last Name</asp:label></TD>
									<TD width="211" style="WIDTH: 211px"><asp:textbox id="LastName" runat="server" CssClass="InputStyleTwo" MaxLength="50"></asp:textbox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="LastName"
											Enabled="False"></asp:RequiredFieldValidator></TD>
									<TD width="22" style="WIDTH: 22px">&nbsp;</TD>
									<TD>&nbsp;</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="table3" width="100%" border="0">
								<TR>
									<TD width="57" style="WIDTH: 57px"><asp:label id="AddressLabel" runat="server" CssClass="bold">Address</asp:label></TD>
									<TD width="216" style="WIDTH: 216px"><asp:textbox id="Address" runat="server" CssClass="InputStyleTwo" MaxLength="100"></asp:textbox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="Address"
											Enabled="False"></asp:RequiredFieldValidator></TD>
									<TD width="20" style="WIDTH: 20px"></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD width="57" style="WIDTH: 57px"><asp:label id="CityLabel" runat="server" CssClass="bold">City</asp:label></TD>
									<TD width="216" style="WIDTH: 216px"><asp:textbox id="City" runat="server" CssClass="InputStyleTwo" MaxLength="50"></asp:textbox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="City"
											Enabled="False"></asp:RequiredFieldValidator></TD>
									<TD width="20" style="WIDTH: 20px"><asp:label id="ZipLabel" runat="server" CssClass="bold">Zip</asp:label></TD>
									<TD><asp:textbox id="ZipCode" runat="server" CssClass="inputStyleShort" Width="80px" MaxLength="10"></asp:textbox>
										<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" CssClass="text" ErrorMessage="*"
											ControlToValidate="ZipCode" ValidationExpression="\d{5}(-\d{4})?" Enabled="False"></asp:RegularExpressionValidator>
										<asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="ZipCode"
											Enabled="False"></asp:RequiredFieldValidator></TD>
								</TR>
								<TR>
									<TD width="57" style="WIDTH: 57px"><asp:label id="StateLabel" runat="server" CssClass="bold">State</asp:label></TD>
									<TD width="216" style="WIDTH: 216px"><asp:dropdownlist id="State" runat="server" CssClass="inputStyleCC2"></asp:dropdownlist>
										<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="State"
											Enabled="False"></asp:RequiredFieldValidator></TD>
									<TD width="20" style="WIDTH: 20px">&nbsp;</TD>
									<TD>&nbsp;</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="table4" width="100%" border="0">
								<TR>
									<TD width="126"><asp:label id="DayPhoneLabel" runat="server" CssClass="text">Day Phone</asp:label></TD>
									<TD width="197"><asp:textbox id="DayPhone" runat="server" CssClass="inputStyleCC" MaxLength="20"></asp:textbox></TD>
									<TD width="81"><asp:label id="OtherPhoneLabel" runat="server" CssClass="text">Other Phone</asp:label></TD>
									<TD><asp:textbox id="OtherPhone" runat="server" CssClass="inputStyleCC" MaxLength="20"></asp:textbox></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="table5" width="100%" border="0">
								<TR>
									<TD width="89" style="WIDTH: 89px"><asp:label id="EmailLabel" runat="server" CssClass="bold">E-mail Address</asp:label></TD>
									<TD><asp:textbox id="EmailAddress" runat="server" CssClass="InputStyleTwo" MaxLength="100"></asp:textbox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="EmailAddress"
											Enabled="False"></asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" CssClass="text" ErrorMessage="*"
											ControlToValidate="EmailAddress" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Enabled="False"></asp:RegularExpressionValidator></TD>
								</TR>
								<TR>
									<TD width="89" style="WIDTH: 89px"></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD></TD>
					</TR>
					<TR>
						<TD align="center"><asp:button id="cmdAdd" runat="server" CssClass="buttonStyle" Width="65px" Text="Add" Height="24px"></asp:button><asp:button id="cmdClear" runat="server" CssClass="buttonStyle" Width="65px" Text="Clear" Height="24px"
								CausesValidation="False"></asp:button></TD>
					</TR>
				</TABLE>
				<P></P>
			</DIV>
			<asp:panel id="pnlQ" runat="server" Visible="False" Enabled="False"></asp:panel><asp:panel id="pnlQ2" runat="server" CssClass="CalBackground" Visible="False" Enabled="False">
				<P align="center">
					<DIV align="center">
						<TABLE id="Table6" cellSpacing="1" cellPadding="1" width="0" align="center" border="0">
							<TR>
								<TD class="text" vAlign="middle">
									<asp:Label id="lblQuestion" runat="server" CssClass="text"></asp:Label></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center">
									<P>
										<asp:Button id="cmdYes" runat="server" Width="65px" CssClass="buttonStyle" Height="24px" Text="Yes"
											CausesValidation="False" onclick="cmdYes_Click"></asp:Button>
										<asp:Button id="cmdNo" runat="server" Width="65px" CssClass="buttonStyle" Height="24px" Text="No"
											CausesValidation="False" onclick="cmdNo_Click"></asp:Button></P>
								</TD>
							</TR>
						</TABLE>
					</DIV>
				<P></P>
			</asp:panel>
			<TABLE id="Table8" style="Z-INDEX: 104; LEFT: 752px; WIDTH: 296px; POSITION: absolute; TOP: 680px; HEIGHT: 92px"
				width="296">
				<TR>
					<TD>
						<TABLE id="Table9" width="100%" border="0">
							<TR>
								<TD class="text">Report Number</TD>
								<TD><asp:textbox id="ReportNumber" runat="server" CssClass="inputStyleCC" MaxLength="11"></asp:textbox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="ReportNumber"
										Enabled="False"></asp:RequiredFieldValidator></TD>
							</TR>
							<TR>
								<TD class="text">VirtualVault™ Number</TD>
								<TD><asp:textbox id="VirtualVaultNumber" runat="server" CssClass="inputStyleCC" MaxLength="10"></asp:textbox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator9" runat="server" CssClass="text" ErrorMessage="*" ControlToValidate="VirtualVaultNumber"
										Enabled="False"></asp:RequiredFieldValidator></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center"><asp:button id="cmdLoadReport" runat="server" CssClass="buttonStyle" Width="85px" Text="Check Report"
							Height="24px" CausesValidation="False"></asp:button></TD>
				</TR>
			</TABLE>
			<asp:button id="cmdHome" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
				Width="16px" CssClass="buttonStyle" Height="16px" Text="H" CausesValidation="False" onclick="cmdHome_Click"></asp:button>
			<asp:panel id="pnlM" runat="server" CssClass="CalBackground" Visible="False" Enabled="False">
				<P align="center">
					<DIV align="center">
						<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="0" align="center" border="0">
							<TR>
								<TD class="text" vAlign="middle">
									<asp:Label id="lblMessage" runat="server" CssClass="text"></asp:Label></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center">
									<P>
										<asp:Button id="cmdOK" runat="server" Width="65px" CssClass="buttonStyle" Height="24px" Text="Ok"
											CausesValidation="False" onclick="cmdOK_Click"></asp:Button></P>
								</TD>
							</TR>
						</TABLE>
					</DIV>
				<P></P>
			</asp:panel><asp:button id="Button1" style="Z-INDEX: 102; LEFT: 896px; POSITION: absolute; TOP: 16px" runat="server"
				Text="Button" CausesValidation="False" onclick="Button1_Click"></asp:button><asp:button id="Button2" style="Z-INDEX: 103; LEFT: 952px; POSITION: absolute; TOP: 16px" runat="server"
				Text="Button" CausesValidation="False" onclick="Button2_Click"></asp:button><asp:hyperlink id="lnkReport" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 152px"
				runat="server" CssClass="text">Report</asp:hyperlink>
			<asp:Panel id="pnlSearch" style="Z-INDEX: 107; LEFT: 48px; POSITION: absolute; TOP: 240px"
				runat="server" Width="328px" CssClass="text" Height="248px">
				<DIV style="WIDTH: 336px; POSITION: relative; HEIGHT: 248px" ms_positioning="GridLayout">
					<asp:Label id="Label1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 0px" runat="server"
						CssClass="text" Font-Bold="True">Search</asp:Label>
					<asp:button id="cmdSearch" style="Z-INDEX: 103; LEFT: 120px; POSITION: absolute; TOP: 200px"
						runat="server" Width="85px" CssClass="buttonStyle" Height="24px" Text="Search" CausesValidation="False" onclick="cmdSearch_Click"></asp:button>
					<DIV class="txt" style="DISPLAY: inline; Z-INDEX: 104; LEFT: 8px; WIDTH: 90%; POSITION: absolute; TOP: 16px; HEIGHT: 48px"
						align="center" ms_positioning="FlowLayout">
						<P>Enter as much info as you have.<BR>
							You can use wildcart - '*' too.</P>
					</DIV>
					<TABLE class="text" id="Table10" style="Z-INDEX: 114; LEFT: 16px; POSITION: absolute; TOP: 88px"
						cellSpacing="0" cellPadding="1" width="90%" align="center" border="0">
						<TR>
							<TD>Report Number</TD>
							<TD>
								<asp:textbox id="txtSearchReportNumber" runat="server" CssClass="inputStyleCC" MaxLength="255"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Virtual Vault Number</TD>
							<TD>
								<asp:textbox id="txtSearchVirtualVaultNumber" runat="server" CssClass="inputStyleCC" MaxLength="255"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Email</TD>
							<TD>
								<asp:textbox id="txtSearchEmail" runat="server" CssClass="inputStyleCC" MaxLength="255"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>First Name</TD>
							<TD>
								<asp:textbox id="txtSearchFirstName" runat="server" CssClass="inputStyleCC" MaxLength="255"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Last Name</TD>
							<TD>
								<asp:textbox id="txtSearchLastName" runat="server" CssClass="inputStyleCC" MaxLength="255"></asp:textbox></TD>
						</TR>
					</TABLE>
				</DIV>
			</asp:Panel>
			<asp:Panel id="pnlSearchResult" style="Z-INDEX: 108; LEFT: 432px; POSITION: absolute; TOP: 248px"
				runat="server" Width="328px" Height="232px">
				<DIV style="WIDTH: 405px; POSITION: relative; HEIGHT: 240px" ms_positioning="GridLayout">
					<asp:Label id="Label2" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
						CssClass="text" Font-Bold="True">Search Results</asp:Label>
					<asp:DataGrid id="tblSearchResult" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 32px"
						runat="server" Width="402px" CssClass="text" AllowSorting="True" CellPadding="0" BackColor="White"
						BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" onselectedindexchanged="tblSearchResult_SelectedIndexChanged">
						<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
						<EditItemStyle Wrap="False"></EditItemStyle>
						<AlternatingItemStyle Wrap="False" BackColor="WhiteSmoke"></AlternatingItemStyle>
						<ItemStyle ForeColor="#000066"></ItemStyle>
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#1C4C8E"></HeaderStyle>
					</asp:DataGrid></DIV>
			</asp:Panel></FORM>
	</body>
</HTML>
