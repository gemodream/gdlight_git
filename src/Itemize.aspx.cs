using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for Itemize.
	/// </summary>
	public partial class Itemize : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button cmdHome;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			AjustMode();
			cmdGo.Attributes.Add("onclick","javascript:document['PleaseWaitPicture'].src='images/5-1.gif';if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate();");
			PopulateLotNumbers();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void txtOrderNumber_TextChanged(object sender, System.EventArgs e)
		{
			if(Regex.IsMatch(txtOrderNumber.Text.Trim(), @"^\d{5}$|^\d{6}$|^\d{7}$"))
			//if(txtOrderNumber.Text.Length == 5)
			{

				//txtNumberOfItems.Text=System.DateTime.Now.ToString();
////				dbo.spGetGroupByCode2 @AuthorId=19, 
////					@AuthorOfficeId=1, 
////					@CustomerCode=null, 
////					@BGroupState=null, 
////					@EGroupState=null, 
////					@BDate=null, 
////					@EDate=null, 
////					@GroupCode=8295

				Session["Itemize_OrderInfo"] = null;

				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
				SqlCommand command = new SqlCommand("spGetGroupByCode2");
				command.Connection = conn;
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeID"].ToString());
				command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BGroupState", SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EGroupState", SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BDate", DBNull.Value);
				command.Parameters.Add("@EDate", DBNull.Value);

				try
				{
					command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = int.Parse(txtOrderNumber.Text);
				}
				catch(Exception ex)
				{
					return;
				}

				SqlDataAdapter da = new SqlDataAdapter(command);

				DataSet ds = new DataSet();
			
				da.Fill(ds);

				CurrentOrder currOrder = new CurrentOrder();
				foreach(DataRow dr in ds.Tables[0].Rows)
				{	                
					currOrder.CustomerID=int.Parse(dr.ItemArray[dr.Table.Columns.IndexOf("CustomerID")].ToString());
					currOrder.CustomerCode=int.Parse(dr.ItemArray[dr.Table.Columns.IndexOf("CustomerCode")].ToString());
					currOrder.CustomerName = dr.ItemArray[dr.Table.Columns.IndexOf("CustomerName")].ToString();
					currOrder.CustomerOfficeID = int.Parse(dr.ItemArray[dr.Table.Columns.IndexOf("CustomerOfficeID")].ToString());
					currOrder.GroupCode = int.Parse(txtOrderNumber.Text);
					currOrder.GroupID = int.Parse(dr.ItemArray[dr.Table.Columns.IndexOf("GroupID")].ToString());
					currOrder.GroupOfficeID = int.Parse(dr.ItemArray[dr.Table.Columns.IndexOf("GroupOfficeID")].ToString());
					//OrderCode
				}
				Session["CurrOrder"] = currOrder;

				if(ds.Tables.Count>0)
				{
					Session["Itemize_OrderInfo"] = currOrder;
                    lblCustName.Text=currOrder.CustomerName;
				
					//spGetGroupMemoNumber: @AuthorId=19; @AuthorOfficeId=1; @GroupID=4234; 

					conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
					command = new SqlCommand("spGetGroupMemoNumber");
					command.Connection = conn;
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
					command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeID"].ToString());
					command.Parameters.Add("@GroupID", SqlDbType.Int).Value=currOrder.GroupID;

					SqlDataReader reader;

					SortedList myList = new SortedList();

					conn.Open();
					reader = command.ExecuteReader();

					lstMemoNumber.DataSource=reader;
					lstMemoNumber.DataTextField="Name";
					lstMemoNumber.DataValueField="MemoNumberID";
					lstMemoNumber.DataBind();
					conn.Close();

					if(lstMemoNumber.Items.Count<1)
					{
						lstMemoNumber.Items.Add(new ListItem("[NONE]","-1"));
					}											   

					conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
								
					command = new SqlCommand("spGetCustomerProgramsPerCustomer");
						
					command.Connection=conn;
					command.CommandType=CommandType.StoredProcedure;

					command.Parameters.Add("@CustomerOfficeID", currOrder.CustomerOfficeID);
					command.Parameters.Add("@CustomerID", currOrder.CustomerID);
					command.Parameters.Add("@VendorOfficeID", currOrder.CustomerOfficeID);
					command.Parameters.Add("@VendorID", currOrder.CustomerID);
			
					command.Parameters.Add("@AuthorID", Session["ID"].ToString().Trim());
					command.Parameters.Add("@AuthorOfficeID", Session["AuthorOfficeID"].ToString().Trim());
									
					SqlDataAdapter das = new SqlDataAdapter(command);
					DataTable dat = new DataTable();
					das.Fill(dat);
					if(dat.Rows.Count>0)
					{
						lstCustomerProgram.DataSource=dat;
						lstCustomerProgram.DataTextField="CustomerProgramName";
						lstCustomerProgram.DataValueField="CPOfficeID_CPID";
						lstCustomerProgram.DataBind();
						Session["CustomerProgramListForCustomer"] = dat;
					}
				}
			}
		}

		private void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("GDLight.aspx");
		}

		protected void lstCustomerProgram_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstCustomerProgram.SelectedIndex!=-1)
			{
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("spGetCustomerProgramByNameAndCustomer");
						
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;

				command.Parameters.Add("@CustomerProgramName", lstCustomerProgram.SelectedItem.Text);

				command.Parameters.Add("@CustomerOfficeID", (Session["CurrOrder"] as CurrentOrder).CustomerOfficeID);
				command.Parameters.Add("@CustomerID", (Session["CurrOrder"] as CurrentOrder).CustomerID);
				command.Parameters.Add("@VendorOfficeID", (Session["CurrOrder"] as CurrentOrder).CustomerOfficeID);
				command.Parameters.Add("@VendorID", (Session["CurrOrder"] as CurrentOrder).CustomerID);
			
				command.Parameters.Add("@AuthorID", Session["ID"].ToString().Trim());
				command.Parameters.Add("@AuthorOfficeID", Session["AuthorOfficeID"].ToString().Trim());
								
				conn.Open();
				SqlDataReader reader = command.ExecuteReader();

				if(reader.HasRows)
				{
					while(reader.Read())
					{
						itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString();
						itemPicture.Visible = true;
					}
				}
				conn.Close();
			}
		}

		protected void cmdSelectProgram_Click(object sender, System.EventArgs e)
		{
			lstCustomerProgram_SelectedIndexChanged(sender, e);
		}

		protected void cmdGo_Click(object sender, System.EventArgs e)
		{
			//			procedure sp_AddItemsByCPandOrderCode(
			//	@OrderNumber int,
			//	@CustomerProgramName varchar(250),
			//	@ItemNumber int, 
			//	@MemoNumber varchar(500),
			//	@MemoNumberID int,
			//	@AuthorId int, 
			//	@AuthorOfficeId int, 
			//	@CurrentOfficeId int,
			//	@GroupOfficeID int,
			//	@CPOfficeID int)
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			SqlCommand command = new SqlCommand("sp_AddItemsByCPandOrderCode");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add("@OrderNumber", SqlDbType.Int);
			command.Parameters["@OrderNumber"].Value=(Session["CurrOrder"] as CurrentOrder).GroupCode;
			command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
			command.Parameters["@CustomerProgramName"].Size = 250;
			command.Parameters["@CustomerProgramName"].Value=lstCustomerProgram.SelectedItem.Text;
			command.Parameters.Add("@ItemNumber", SqlDbType.Int);
			command.Parameters["@ItemNumber"].Value=int.Parse(txtNumberOfItemsDivision.Text.Trim());
			command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
			command.Parameters["@MemoNumber"].Size = 500;

			if(int.Parse(lstMemoNumber.SelectedItem.Value)== -1)
			{
				command.Parameters["@MemoNumber"].Value = DBNull.Value;
				// stop here:
				Label2.Text = "Please Select Memo Number.";
				return;
			}
			else
			{
				if(Utils.NullValue(lstMemoNumber.SelectedItem.Text)=="")
				{
					Label2.Text = "Please Select Memo Number.";
					return;
				}
				else
				{
					command.Parameters["@MemoNumber"].Value = lstMemoNumber.SelectedItem.Text;
				}
			}
			command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
			if(int.Parse(lstMemoNumber.SelectedItem.Value)==-1)
			{
				command.Parameters["@MemoNumberID"].Value = DBNull.Value;
			}
			else
			{
				command.Parameters["@MemoNumberID"].Value=int.Parse(lstMemoNumber.SelectedItem.Value);
			}
			command.Parameters.Add("@AuthorId",SqlDbType.Int);
			command.Parameters["@AuthorID"].Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId",SqlDbType.Int);
			command.Parameters["@AuthorOfficeID"].Value=int.Parse(Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@CurrentOfficeId",SqlDbType.Int).Value = (Session["CurrOrder"] as CurrentOrder).GroupOfficeID;
			command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = (Session["CurrOrder"] as CurrentOrder).GroupOfficeID;
			command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(lstCustomerProgram.SelectedItem.Value.Split('_')[0].ToString());
			command.Parameters.Add("@ParNo", SqlDbType.VarChar).Size=255;
			command.Parameters["@ParNo"].Value=DBNull.Value;


			SqlDataAdapter da = new SqlDataAdapter(command);
			DataSet ds = new DataSet();
			command.CommandTimeout = 0;
			da.Fill(ds);

			ds.DataSetName="DataSetNewBatchList";
			ds.Tables[0].TableName="TableNewBatchList";
		
			ShowInfoMessage(ds);			
		}


		private void UpdateTracking(int intBatchID, int intFromID, int intNumberOfItemsAffected, int intNumberOfItemsInBatch, int intEventID)		
		{
			SqlCommand command = new SqlCommand("wspSetBatchHistory");
			command.CommandType= CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			
			command.Parameters.Add(new SqlParameter("@BatchID", intBatchID));
			command.Parameters.Add(new SqlParameter("@FormID", intFromID));
			command.Parameters.Add(new SqlParameter("@UserID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@EventID", intEventID));
			command.Parameters.Add(new SqlParameter("@NumberOfItemsAffected", intNumberOfItemsAffected));
			command.Parameters.Add(new SqlParameter("@NumberOfItemsInBatch", intNumberOfItemsInBatch));

			command.Connection.Open();
			command.ExecuteNonQuery();
			command.Connection.Close();
		}

		private void ShowInfoMessage2(DataSet ds)
		{
			pnlMessage.Enabled=true;
			pnlMessage.Visible=true;
			pnlMessage.BorderStyle= BorderStyle.Solid;
			pnlMessage.BorderWidth=Unit.Pixel(2);
			pnlMessage.Height=Unit.Percentage(75);
			pnlMessage.Width=Unit.Percentage(75);
			pnlMessage.HorizontalAlign=HorizontalAlign.Center;
			pnlMessage.BackColor=Color.White;
			DataGrid3.DataSource=ds;
			DataGrid3.DataBind();
			Session["Itemizing_NewBatches"] = ds;
		}


		private void ShowInfoMessage(DataSet ds)
		{
			pnlMessage.Enabled=true;
			pnlMessage.Visible=true;
			pnlMessage.BorderStyle= BorderStyle.Solid;
			pnlMessage.BorderWidth=Unit.Pixel(2);
			pnlMessage.Height=Unit.Percentage(75);
			pnlMessage.Width=Unit.Percentage(75);
			pnlMessage.HorizontalAlign=HorizontalAlign.Center;
			pnlMessage.BackColor=Color.White;			
//#if DEBUG
//			DataGrid3.DataSource = ds;
//			DataGrid3.DataBind();
//#endif

			foreach(DataColumn cl in ds.Tables[0].Columns)
			{
				cl.ColumnMapping = System.Data.MappingType.Attribute;
			}

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			SqlCommand command = new SqlCommand("sp_GetBatchInfoFromList");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add("@in_values", SqlDbType.Text);
			command.Parameters["@in_values"].Value=ds.GetXml();
			DataSet extendedBatchList = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(extendedBatchList);

			//update trucking info for simple itemizing here
			foreach(DataRow dr in extendedBatchList.Tables[0].Rows)
			{
				int intBatchID = int.Parse(dr["BatchID"].ToString());
				int intFormID = 19;
				int intNumberOfItemsAffected = int.Parse(dr["StoredItemsQuantity"].ToString());
				int intEventID = 5;
				UpdateTracking(intBatchID, intFormID, intNumberOfItemsAffected, intNumberOfItemsAffected, intEventID);
			}
			//done updating - resume old code

			DataGrid3.DataSource=extendedBatchList;
			DataGrid3.DataBind();

            Session["Itemizing_NewBatches"] = extendedBatchList;
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			//ShowInfoMessage(null);
			for(int i=1; i<70; i++)
			{
				txtLotNumber.Text=i.ToString();
				cmdAddLot_Click(sender, e);
			}
		}

		protected void cmdDismiss_Click(object sender, System.EventArgs e)
		{
			pnlMessage.Enabled=false;
			pnlMessage.Visible=false;
		}

		protected void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			pnlMessage.Enabled=false;
			pnlMessage.Visible=false;
			txtOrderNumber.Text="";
			lstMemoNumber.Items.Clear();
			txtNumberOfItemsDivision.Text="";
			lstCustomerProgram.Items.Clear();
			itemPicture.Visible=false;
			itemPicture.ImageUrl="";
			lblCustName.Text="";
		}

		protected void cmdDeleteLot_Click(object sender, System.EventArgs e)
		{
			if(lstLotNumbers.SelectedIndex>-1)
			{
				if((Session["LotNumberList"]!=null)&&(lstLotNumbers.SelectedIndex!=-1))
				{
					DataTable lotNumbers;
					lotNumbers = Session["LotNumberList"] as DataTable;
					txtLotNumber.Text=lstLotNumbers.SelectedValue;
					lotNumbers.Rows.RemoveAt(lstLotNumbers.SelectedIndex);
					Session["LotNumberList"]=lotNumbers;
				}
				lstLotNumbers.SelectedIndex=-1;
				Page_Load(sender, e);
			}
		}
		private void PopulateLotNumbers()
		{
			if((Session["LotNumberList"]!=null)&&(lstLotNumbers.SelectedIndex==-1))
			{
				DataTable lotNumbers = Session["LotNumberList"] as DataTable;
				lstLotNumbers.DataSource = lotNumbers;
				lstLotNumbers.DataTextField="Memo";
				lstLotNumbers.DataBind();
				lblNumberOfLotNumbers.Text=lotNumbers.Rows.Count.ToString();
//				DataGrid2.DataSource=lotNumbers;
//				DataGrid2.DataBind();
			}
		}

		protected void cmdAddLot_Click(object sender, System.EventArgs e)
		{
			if(txtLotNumber.Text!="")
			{
				if(Session["LotNumberList"]==null)
				{
					DataTable lotNumbers=new DataTable();
					lotNumbers.Columns.Add(new DataColumn("Memo"));
					lotNumbers.Columns.Add(new DataColumn("LID", Type.GetType("System.Int32")));
					DataRow datarow = lotNumbers.NewRow();
					datarow["memo"]=txtLotNumber.Text;
					datarow["LID"]=0;
					lotNumbers.Rows.Add(datarow);
					Session["LotNumberList"]=lotNumbers;
				}
				else
				{
					DataTable lotNumbers;
					lotNumbers = Session["LotNumberList"] as DataTable;
					DataRow datarow = lotNumbers.NewRow();
					datarow["memo"]=txtLotNumber.Text;
					lotNumbers.TableName="t1";
					try
					{
						datarow["LID"] = Int32.Parse(lotNumbers.Compute("MAX(LID)","").ToString())+1;
					}
					catch(Exception ex)
					{
						datarow["LID"] = 0;
					}
					lotNumbers.Rows.Add(datarow);
					Session["LotNumberList"]=lotNumbers;
				}
			}
			txtLotNumber.Text="";
			lstLotNumbers.SelectedIndex=-1;
			PopulateLotNumbers();
		}

		protected void cmdDone_Click(object sender, System.EventArgs e)
		{
			if(Session["LotNumberList"]==null) 
			{
				return;
			}
			DataTable lotNumbers;
			lotNumbers = (Session["LotNumberList"] as DataTable).Copy();
			DataSet dsp= new DataSet();
			dsp.DataSetName="LotNumberSet";
			dsp.Tables.Add(lotNumbers);
			dsp.Tables[0].TableName="LotNumberSetTable";
			dsp.Tables["LotNumberSetTable"].Columns[0].ColumnName="LotNumber";
			foreach(DataColumn cl in dsp.Tables[0].Columns)
			{
				cl.ColumnMapping = System.Data.MappingType.Attribute;
			}

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
            //--sp_AddItemsByCPandOrderCode
			SqlCommand command = new SqlCommand("sp_AddItemsBatchFromList");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add("@OrderNumber", SqlDbType.Int);
			command.Parameters["@OrderNumber"].Value=(Session["CurrOrder"] as CurrentOrder).GroupCode;
			command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
			command.Parameters["@CustomerProgramName"].Size=250;
			command.Parameters["@CustomerProgramName"].Value=lstCustomerProgram.SelectedItem.Text;
			command.Parameters.Add("@ItemNumber", SqlDbType.Int);
			command.Parameters["@ItemNumber"].Value=DBNull.Value;
			command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
			command.Parameters["@MemoNumber"].Size = 500;
			if(int.Parse(lstMemoNumber.SelectedItem.Value)==-1)
			{
				command.Parameters["@MemoNumber"].Value = DBNull.Value;
				Label2.Text = "Please Select Memo Number.";
				return;
			}
			else
			{
				if(Utils.NullValue(lstMemoNumber.SelectedItem.Text)=="")
				{
					Label2.Text = "Please Select Memo Number.";
					return;
				}
				else
				{
					command.Parameters["@MemoNumber"].Value = lstMemoNumber.SelectedItem.Text;
				}
			}
			command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
			if(int.Parse(lstMemoNumber.SelectedItem.Value)==-1)
			{
				command.Parameters["@MemoNumberID"].Value = DBNull.Value;
			}
			else
			{
				command.Parameters["@MemoNumberID"].Value=int.Parse(lstMemoNumber.SelectedItem.Value);
			}
			command.Parameters.Add("@AuthorId",SqlDbType.Int);
			command.Parameters["@AuthorID"].Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId",SqlDbType.Int);
			command.Parameters["@AuthorOfficeID"].Value=int.Parse(Session["AuthorOfficeID"].ToString());
			command.Parameters.Add("@CurrentOfficeId",SqlDbType.Int).Value = (Session["CurrOrder"] as CurrentOrder).GroupOfficeID;
			command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = (Session["CurrOrder"] as CurrentOrder).GroupOfficeID;
			command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(lstCustomerProgram.SelectedItem.Value.Split('_')[0].ToString());
			command.Parameters.Add("@LotNumberList", SqlDbType.Text);			
			command.Parameters["@LotNumberList"].Value=dsp.GetXml();

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataSet ds = new DataSet();
			command.CommandTimeout = 0;
			try
			{
				da.Fill(ds);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			UpdateTruckingInfoFromLotList(ds);

			ds.Tables[0].Columns["GroupCode"].ColumnName="Order Number";
			ds.Tables[0].Columns["BatchCode"].ColumnName="Batch Number";
			ds.Tables[0].Columns["ItemCode"].ColumnName="Item Number";
			ds.Tables[0].Columns["LotNumber"].ColumnName="Lot Number";
			
			ShowInfoMessage2(ds);
			Session["LotNumberList"]=null;
			lstLotNumbers.Items.Clear();
		}

		private void UpdateTruckingInfoFromLotList(DataSet ds)
		{
			SortedList slBatchNumbers = new SortedList();
			foreach(DataRow dr in ds.Tables[0].Rows)
			{//get rid of duplicates
				String strGroupCodeBatchCode = dr["GroupCode"].ToString()+"_"+dr["BatchCode"].ToString();
				try
				{
					slBatchNumbers.Add(strGroupCodeBatchCode, strGroupCodeBatchCode);
					SqlCommand command = new SqlCommand("wspSetBatchHistoryByGroupCodeBatchCode");
					command.CommandType = CommandType.StoredProcedure;
					command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

					command.Parameters.Add(new SqlParameter("@GroupCode", dr["GroupCode"].ToString()));
					command.Parameters.Add(new SqlParameter("@BatchCode", dr["BatchCode"].ToString()));
					command.Parameters.Add(new SqlParameter("@FormID", 19));
					command.Parameters.Add(new SqlParameter("@UserID", Session["ID"].ToString()));
					command.Parameters.Add(new SqlParameter("@EventID", 5));

					command.Connection.Open();
					command.ExecuteNonQuery();
					command.Connection.Close();
				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
		}

		protected void modeSelector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			AjustMode();
		}
		private void AjustMode()
		{
			switch(modeSelector.SelectedIndex)
			{
				case 0: 
				{
					lotNumbers.Visible=false; 
					lotNumbers.Enabled=false; 
					cmdGo.Visible=true; 
					cmdGo.Enabled=true; 
					txtNumberOfItemsDivision.Visible=true; 
					txtNumberOfItemsDivision.Enabled=true; 
					break;
				}
				case 1: 
				{
					lotNumbers.Visible=true; 
					lotNumbers.Enabled=true; 
					cmdGo.Visible=false; 
					cmdGo.Enabled=false; 
					txtNumberOfItemsDivision.Visible=false; 
					txtNumberOfItemsDivision.Enabled=false; 
					break;
				}
				default : 
				{
					lotNumbers.Visible=false; 
					lotNumbers.Enabled=false; 
					cmdGo.Visible=false; 
					cmdGo.Enabled=false; 
					txtNumberOfItemsDivision.Visible=false; 
					txtNumberOfItemsDivision.Enabled=false; 
					break;
				}
			}
		}
	}
}
