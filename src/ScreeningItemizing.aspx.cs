﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Corpt.Models;
using Corpt.Constants;

namespace Corpt
{
    public partial class ScreeningItemizing : CommonPage
    {
        #region Load Info

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Form.DefaultFocus = txtOrder.ClientID;
            //this.Form.DefaultButton = btnOrderSearch.UniqueID;
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Screening Itemizing";

            OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
            OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnOrderSearch);
			this.Form.DefaultFocus = txtOrder.ClientID;
			this.Form.DefaultButton = btnOrderSearch.UniqueID;

			if (!Page.IsPostBack)
            {
              //  LoadCertifiedBy();
                ClearFields();
                InvalidLabel.Text = "";
                divResult.Visible = false;
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
            InvalidLabel.Text = "";
            divResult.Visible = false;
        }

        public void ClearFields()
        {
            txtOrder.Text = "";
            lblTotQty.Text = "";

         //   LoadSyntheticScreeningInstrument();
          //  InsertBlankRecord();
            btnItemize.Enabled = false;

            //lblMemo.Text = "";
            //lblRetailer.Text = "";
            //lblServiceType.Text = "";          
            //ddlCertifiedBy.SelectedIndex = 0;
            //txtItemsLeft.Text = "";
            //txtAllocateItems.Text = "";
            //chkJewelry.Checked = false;
            //chkLoose.Checked = false;
            //btnCreateBatches.Enabled = false;
        }

        //public void LoadCertifiedBy()
        //{
        //    DataTable dt = SyntheticScreeningUtils.GetSyntheticCertifiedBy(this);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        ddlCertifiedBy.DataTextField = "CertifiedBy";
        //        ddlCertifiedBy.DataValueField = "CertifiedCode";
        //        ddlCertifiedBy.DataSource = dt;
        //        ddlCertifiedBy.DataBind();
        //    }
        //    System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
        //    li.Value = "0";
        //    li.Text = "";
        //    ddlCertifiedBy.Items.Insert(0, li);
        //}

        //public void LoadSyntheticScreeningInstrument()
        //{
        //    DataTable dt = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        chkLstScreeningInstrument.DataTextField = "InstrumentName";
        //        chkLstScreeningInstrument.DataValueField = "InstrumentCode";
        //        chkLstScreeningInstrument.DataSource = dt;
        //        chkLstScreeningInstrument.DataBind();
        //    }
        //}


        #endregion

        #region Order Info

       
        protected void btnOrderSearch_Click(object sender, EventArgs e)
        {
            if (txtOrder.Text.Trim() == "")
            {
                return;
            }
            GetOrderDetails();
        }


        public void GetOrderDetails()
        {
            //Get Order and memo details
            var orderModel = QueryUtils.GetOrderInfo(txtOrder.Text.Trim(), this);

            if (orderModel == null)
            {
                var message = string.Format("Order {0} not found.", txtOrder.Text.Trim());
                InvalidLabel.Text = message;
                ClearFields();
                divResult.Visible = false;
                return;
            }

            //Get Synthetic Order details

            DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrder.Text), this);

            if (ds.Tables[0].Rows.Count == 0)
            {
                InvalidLabel.Text = "Order is not associated with Synthetic Screening";
                divResult.Visible = false;
                return;
            }

            if (ds.Tables[1].Rows.Count != 0)
            {
                bool doItemization = false;
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    if (Convert.ToInt32(dr["TesterFailItems"].ToString()) != 0)
                    {
                        doItemization = true;
                    }
                }
                if(doItemization == false)
                {
                    InvalidLabel.Text = "No fail items in Order. Itemization not required.";
                    divResult.Visible = false;
                    return;
                }
            }

            if (ds.Tables[2].Rows.Count != 0)
            {
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    if (Convert.ToInt32(dr["StatusID"].ToString()) == (int) SynhteticOrderStatus.Itemized)
                    {
                        InvalidLabel.Text = "Order is already itemized";
                        divResult.Visible = false;
                        return;
                    }
                }
            }


            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            if (cpList.Count(cp => cp.CustomerProgramName == "TND") == 0)
            {
                InvalidLabel.Text = "Please add TND customer program for the customer.";
                //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer Program Name TND not found.');", true);
                ClearFields();
                divResult.Visible = false;
                return;
            }


            //lblMemo.Text = ds.Tables[0].Rows[0]["MemoNum"].ToString();
            //lblServiceType.Text = ds.Tables[0].Rows[0]["ServiceTypeName"].ToString();
            //lblRetailer.Text = ds.Tables[0].Rows[0]["RetailerName"].ToString();
           
            //ddlCertifiedBy.ClearSelection();

            //if (ddlCertifiedBy.Items.FindByValue(ds.Tables[0].Rows[0]["CertifiedBy"].ToString()) != null)
            //{
            //    ddlCertifiedBy.Items.FindByValue(ds.Tables[0].Rows[0]["CertifiedBy"].ToString()).Selected = true;
            //}
            //else
            //{
            //    ddlCertifiedBy.SelectedIndex = 0;
            //}
           
            //string productType = ds.Tables[0].Rows[0]["ProductType"].ToString();
            //if (productType == "Loose")
            //    chkLoose.Checked = true;
            //else if (productType == "Jewelry")
            //    chkJewelry.Checked = true;
            //else
            //{
            //    chkLoose.Checked = false;
            //    chkJewelry.Checked = false;
            //}

            //if(ds.Tables[0].Rows[0]["ScreeningInstrument"].ToString() != "")
            //{ 
            //    string[] screeningInstrument = ds.Tables[0].Rows[0]["ScreeningInstrument"].ToString().Split(';');
            //    foreach (string inst in screeningInstrument)
            //    {
            //        int i = Convert.ToInt16(inst);
            //        //chkLstScreeningInstrument.Items[i - 1].Selected = true;
            //        //chkLstScreeningInstrument.Items.FindByValue(i.ToString()).Selected = true;
            //        System.Web.UI.WebControls.ListItem item = chkLstScreeningInstrument.Items.FindByValue(i.ToString());
            //        if (item != null)
            //        {
            //            chkLstScreeningInstrument.SelectedValue = i.ToString();
            //        }
            //    }
            //}

            //if (chkLoose.Checked)
            //    productType = "Loose";
            //else if (chkJewelry.Checked)
            //    productType = "Jewelry";
            //else
            //    productType = "";

            if (ds.Tables[1].Rows.Count != 0)
            {
                gvItems.DataSource = ds.Tables[1];
                gvItems.DataBind();
                gvItems.Columns[4].Visible = false;
                btnItemize.Enabled = true;

                //btnAddBatch.Enabled = false;
                //btnCreateBatches.Enabled = false;
                //txtItemsLeft.Text = "0";

                int minStatusID = int.MaxValue;
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    int statusID = Convert.ToInt32(dr["StatusID"].ToString());
                    minStatusID = Math.Min(minStatusID, statusID);
                }
 
				if (minStatusID != 14)
				{
					btnItemize.Enabled = false;
					InvalidLabel.Text = "Packing not done.";
					ClearFields();
					divResult.Visible = false;
					return;
				}

				InvalidLabel.Text = "";
                divResult.Visible = true;
                lblTotQty.Text = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            }
            else
            {
                btnItemize.Enabled = false;
                InvalidLabel.Text = "Batch not created.";
                ClearFields();
                divResult.Visible = false;
                return;

                //btnAddBatch.Enabled = true;
                //btnCreateBatches.Enabled = true;
                //txtItemsLeft.Text = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            }
            
        }

        protected void btnItemize_Click(object sender, EventArgs e)
        {

            DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrder.Text), this);
            if (ds.Tables[0].Rows.Count == 0) return;

            if (ds.Tables[1].Rows.Count != 0)
            {
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    int itemQtyFail = Convert.ToInt32(dr["TesterFailItems"].ToString());

                    SyntheticItemModel objSyntheticItemModel = new SyntheticItemModel();
                    objSyntheticItemModel.BatchID = Convert.ToInt32(dr["BatchID"].ToString());
                    objSyntheticItemModel.BatchCode = Convert.ToInt32(dr["BatchCode"].ToString());
                    objSyntheticItemModel.GroupCode = Convert.ToInt32(txtOrder.Text);

                    for (int i = 1; i <= itemQtyFail; i++)
                    {
                        objSyntheticItemModel.ItemCode = i;
                        bool result = SyntheticScreeningUtils.AddSyntheticItem(objSyntheticItemModel, this);
                    }
                }
            }

            SyntheticOrderHistoryModel objSyntheticOrderHistory = new SyntheticOrderHistoryModel();
            objSyntheticOrderHistory.OrderCode = Convert.ToInt32(txtOrder.Text);
            objSyntheticOrderHistory.BatchCode = 0;
            objSyntheticOrderHistory.ItemQty = 0;
            objSyntheticOrderHistory.AssignedTo = 0;
            objSyntheticOrderHistory.StatusId = (int)SynhteticOrderStatus.Itemized;
            SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrderHistory, this);
            SyntheticScreeningUtils.SetSyntheticStatus(objSyntheticOrderHistory, this);

            //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Items added successfully');", true);
            PopupInfoDialog("Itemization done successfully", false);
            btnItemize.Enabled = false;
        }

        #endregion

        #region printing

        protected void btnPrint_Click(object sender, EventArgs e)
        {
           // PrintPDFReceipt();
        }

        public void PrintPDFReceipt()
        {
            DataSet ds = new DataSet();
            ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries("", "", txtOrder.Text, this);

            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                return;
            }

            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();

            dt = ds.Tables[0];

            string requestID = dt.Rows[0]["RequestID"].ToString();

            // Create a Document object
            var document = new Document(PageSize.A4, 25, 25, 25, 25);

            // Create a new PdfWrite object, writing the output to a MemoryStream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            // Open the Document for writing
            document.Open();

            // First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
            var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
            var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
            var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
            var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
            var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
            var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);

            // Finally, add an image in the upper right corner
            var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/logo.gif"));
            logo.SetAbsolutePosition(50, 760);
            logo.ScalePercent(60);
            document.Add(logo);

            // Add the Receipt title
            var rptHeader = new Paragraph("Gemological Science International", headerFont);

            rptHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(rptHeader);

            var repSubHeader = new Paragraph("www.gemscience.net", bodyFont);
            repSubHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(repSubHeader);
            document.Add(Chunk.NEWLINE);

            // Add the Receipt title
            var rptTitle = new Paragraph("Batch Receipt", titleFont);
            rptTitle.SpacingBefore = 10;
            document.Add(rptTitle);

            // Now add the "Your order details are below." message
            document.Add(new Paragraph("Your order details are below.", bodyFont));
            document.Add(Chunk.NEWLINE);
            document.Add(Chunk.NEWLINE);

            Barcode128 bc = new Barcode128();
            bc.TextAlignment = Element.ALIGN_CENTER;
            bc.Code = requestID;
            bc.StartStopText = false;
            bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
            bc.Extended = true;

            PdfContentByte cb = writer.DirectContent;
            iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
            patImage.ScaleToFit(250, 50);
            patImage.SetAbsolutePosition(450, 700);
            document.Add(patImage);

            // Add the "Order Information" subtitle
            document.Add(new Paragraph("Order Information", subTitleFont));

            PdfPTable mtable = new PdfPTable(2);
            mtable.WidthPercentage = 100;
            mtable.SetWidths(new int[] { 300, 300 });

            mtable.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            // Create the Order Information table - see http://www.mikesdotnetting.com/Article/86/iTextSharp-Introducing-Tables for more info
            var orderInfoTable = new PdfPTable(2);
            orderInfoTable.HorizontalAlignment = 0;
            orderInfoTable.SpacingBefore = 10;
            orderInfoTable.SpacingAfter = 7;
            orderInfoTable.DefaultCell.Border = 0;
            orderInfoTable.TotalWidth = 300;
            orderInfoTable.LockedWidth = true;
            orderInfoTable.SetWidths(new int[] { 100, 200 });

            orderInfoTable.AddCell(new Phrase("RequestID:", boldTableFont));
            orderInfoTable.AddCell(requestID);

            orderInfoTable.AddCell(new Phrase("Memo:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["MemoNum"].ToString());

            orderInfoTable.AddCell(new Phrase("PO Number:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["PONum"].ToString());

            orderInfoTable.AddCell(new Phrase("Style:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["StyleName"].ToString());

            orderInfoTable.AddCell(new Phrase("SKU:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["SKUName"].ToString());

            orderInfoTable.AddCell(new Phrase("Total Quantity:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["TotalQty"].ToString());

            orderInfoTable.AddCell(new Phrase("Retailer:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["RetailerName"].ToString());

            orderInfoTable.AddCell(new Phrase("Category:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["CategoryName"].ToString());

            orderInfoTable.AddCell(new Phrase("Service Type:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["ServiceTypeName"].ToString());

            orderInfoTable.AddCell(new Phrase("Service Time:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["ServiceTimeName"].ToString());

            orderInfoTable.AddCell(new Phrase("Order Number:", boldTableFont));
            orderInfoTable.AddCell(dt.Rows[0]["GSIOrder"].ToString());

            orderInfoTable.AddCell(new Phrase("Order Created On:", boldTableFont));

            string OrderCreateDate = dt.Rows[0]["CreateDate"].ToString() == "01/01/1900 12:00:00 AM" ? "" : Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
            orderInfoTable.AddCell(OrderCreateDate);

            string batchTotal = "";
            if (ds.Tables.Count == 3)
            {
                batchTotal = ds.Tables[2].Rows.Count == 0 ? "" : ds.Tables[2].Rows.Count.ToString();
            }

            orderInfoTable.AddCell(new Phrase("Total Batches:", boldTableFont));
            orderInfoTable.AddCell(batchTotal);

            mtable.AddCell(orderInfoTable);

            // Create the Order Details table
            orderInfoTable = new PdfPTable(4);
            orderInfoTable.TotalWidth = 250;
            orderInfoTable.LockedWidth = true;
            //relative col widths in proportions - 1/3 and 2/3
            float[] widths = new float[] { 45, 45, 80, 80 };
            orderInfoTable.SetWidths(widths);

            orderInfoTable.HorizontalAlignment = 1;
            orderInfoTable.SpacingBefore = 10;
            orderInfoTable.SpacingAfter = 10;

            orderInfoTable.AddCell(new Phrase("Batch #", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Quantity", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Screener", boldTableFont));
            orderInfoTable.AddCell(new Phrase("Tester", boldTableFont));

            if (ds.Tables.Count == 3)
            {
                if (ds.Tables[2].Rows.Count != 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        orderInfoTable.AddCell(row["BatchCode"].ToString());
                        orderInfoTable.AddCell(row["Quantity"].ToString());
                        orderInfoTable.AddCell(row["Screener"].ToString());
                        orderInfoTable.AddCell(row["Tester"].ToString());
                    }
                }
            }

            //orderInfoTable.AddCell("1");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("2");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("3");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("4");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("5");
            //orderInfoTable.AddCell("50");

            //orderInfoTable.AddCell("6");
            //orderInfoTable.AddCell("50");

            mtable.AddCell(orderInfoTable);

            document.Add(mtable);

            var screenerInfoTable = new PdfPTable(4);
            screenerInfoTable.HorizontalAlignment = 0;
            screenerInfoTable.SpacingBefore = 10;
            screenerInfoTable.SpacingAfter = 10;
            screenerInfoTable.TotalWidth = 460f;
            screenerInfoTable.LockedWidth = true;
            float[] tableWidth = new float[] { 175f, 55f, 175f, 55f };

            screenerInfoTable.SetWidths(tableWidth);

            PdfPCell cellScreener = new PdfPCell(new Phrase("Screener", subTitleFont));
            cellScreener.Colspan = 2;
            screenerInfoTable.AddCell(cellScreener);
            PdfPCell cellTester = new PdfPCell(new Phrase("Tester", subTitleFont));
            cellTester.Colspan = 2;
            screenerInfoTable.AddCell(cellTester);

            string TotalPassScreening = "";
            string TotalSynthetic = "";
            string TotalFailScreening = "";
            string TotalSuspect = "";
            string TotalPass = "";

            if (ds.Tables.Count >= 2)
            {
                dtResult = ds.Tables[1];
                if (dtResult.Rows.Count != 0)
                {
                    //Screener = dtResult.Rows[0]["Screener"].ToString();
                    //Tester = dtResult.Rows[0]["Tester"].ToString();
                    TotalPassScreening = dtResult.Rows[0]["TotalPassScreening"].ToString();
                    TotalSynthetic = dtResult.Rows[0]["TotalSynthetic"].ToString();
                    TotalFailScreening = dtResult.Rows[0]["TotalFailScreening"].ToString();
                    TotalSuspect = dtResult.Rows[0]["TotalSuspect"].ToString();
                    TotalPass = dtResult.Rows[0]["TotalPass"].ToString();
                }
            }

            screenerInfoTable.AddCell(new Phrase("Total Pass Screening Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalPassScreening, bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Synthetic Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalSynthetic, bodyFont));

            screenerInfoTable.AddCell(new Phrase("Total Fail Screening Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalFailScreening, bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Suspect Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalSuspect, bodyFont));

            screenerInfoTable.AddCell(new Phrase("", bodyFont));
            screenerInfoTable.AddCell(new Phrase("", bodyFont));
            screenerInfoTable.AddCell(new Phrase("Total Pass Items/Stones:", boldTableFont));
            screenerInfoTable.AddCell(new Phrase(TotalPass, bodyFont));

            document.Add(screenerInfoTable);

            // Add ending message
            var endingMessage = new Paragraph("For assistance please email to support@gemscience.net or call GSI office in your country.\nYou can find the phone information by following the link www.gemscience.net/contact/.", endingMessageFont);
            //endingMessage.
            document.Add(endingMessage);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", requestID));
            Response.BinaryWrite(output.ToArray());
        }

        #endregion

        #region Popup Dialog

        protected void OnInfoCloseButtonClick(object sender, EventArgs e)
        {

        }

        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }

        #endregion

        // #region Batch Items Grid

        //private void InsertBlankRecord()
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;
        //    dt.Columns.Add("BatchCode");
        //    dt.Columns.Add("AllocateItemQty");
        //    dt.Columns.Add("ItemQtyFail");
        //    dt.Columns.Add("Status");
        //    dr = dt.NewRow();
        //    dr["BatchCode"] = string.Empty;
        //    dr["AllocateItemQty"] = string.Empty;
        //    dr["ItemQtyFail"] = string.Empty;
        //    dr["Status"] = string.Empty;
        //    dt.Rows.Add(dr);
        //    //Store the DataTable in Session
        //    Session["BatchItems"] = dt;
        //    gvItems.DataSource = dt;
        //    gvItems.DataBind();
        //    gvItems.Columns[4].Visible = true;
        //}

        //private void SetBatchNumber()
        //{
        //    int totalAllocQty = 0;
        //    if (Session["BatchItems"] != null)
        //    {
        //        DataTable dt = (DataTable)Session["BatchItems"];
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                Label lblBatchCode = (Label)gvItems.Rows[0].Cells[0].FindControl("lblBatchCode");
        //                dt.Rows[i]["BatchCode"] = i + 1;
        //                if (dt.Rows[i]["AllocateItemQty"].ToString() != "")
        //                {
        //                    totalAllocQty = totalAllocQty + Convert.ToInt32(dt.Rows[i]["AllocateItemQty"]);
        //                }
        //                else
        //                {
        //                    txtAllocateItems.Text = "";
        //                    totalAllocQty = 0;
        //                    dt.Rows[i]["BatchCode"] = "";
        //                }
        //            }
        //            if(lblTotQty.Text != "")
        //                txtItemsLeft.Text = (Convert.ToInt32(lblTotQty.Text) - totalAllocQty).ToString();
        //            Session["BatchItems"] = dt;

        //            gvItems.DataSource = dt;
        //            gvItems.DataBind();
        //        }

        //    }
        //}

        //protected void btnAddBatch_Click(object sender, EventArgs e)
        //{
        //    DataTable dt = new DataTable();
        //    dt = (DataTable)Session["BatchItems"];

        //    if (txtItemsLeft.Text == "" || txtAllocateItems.Text == "") return;

        //    if (Convert.ToInt32(txtItemsLeft.Text) <= 0 || Convert.ToInt32(txtAllocateItems.Text) <= 0) return;

        //    if (txtAllocateItems.Text.Trim() == string.Empty || Convert.ToInt32(txtAllocateItems.Text) == 0)
        //    {
        //        //ClientScript.RegisterStartupScript(this.GetType(),  "alert", "alert('Please enter valid item quantity.');", true);
        //        InvalidLabel.Text = "Please enter valid item quantity.";
        //        txtAllocateItems.Focus();
        //        return;
        //    }

        //    if (Convert.ToInt32(txtAllocateItems.Text) > Convert.ToInt32(txtItemsLeft.Text))
        //    {
        //        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Item quantity should be less than or equal to Items left.');", true);
        //        InvalidLabel.Text = "Item quantity should be less than or equal to Items left.";
        //        txtAllocateItems.Focus();
        //        return;
        //    }

        //    if (dt.Rows.Count == 1 && (dt.Rows[0]["BatchCode"].ToString() == string.Empty && dt.Rows[0]["AllocateItemQty"].ToString() == string.Empty))
        //    {
        //        dt.Rows[0]["BatchCode"] = "1";
        //        dt.Rows[0]["AllocateItemQty"] = txtAllocateItems.Text;
        //    }
        //    else
        //    {
        //        DataRow dr = dt.NewRow();
        //        dr["BatchCode"] = dt.Rows.Count+1;
        //        dr["AllocateItemQty"] = txtAllocateItems.Text;
        //        dr["ItemQtyFail"] = string.Empty;
        //        dr["Status"] = string.Empty;
        //        dt.Rows.Add(dr);
        //    }
        //    //Store the DataTable in Session
        //    Session["BatchItems"] = dt;
        //    gvItems.DataSource = dt;
        //    gvItems.DataBind();

        //    int itemLeft = Convert.ToInt16(txtItemsLeft.Text);
        //    itemLeft = itemLeft - Convert.ToInt16(txtAllocateItems.Text.Trim());
        //    txtItemsLeft.Text = itemLeft.ToString();
        //    InvalidLabel.Text = "";
        //}

        //protected void gvItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    int index = Convert.ToInt32(e.RowIndex);
        //    DataTable dt = Session["BatchItems"] as DataTable;
        //    dt.Rows[index].Delete();
        //    dt.AcceptChanges();
        //    Session["BatchItems"] = dt;
        //    gvItems.DataSource = dt;
        //    gvItems.DataBind();
        //    if (dt.Rows.Count == 0)
        //        InsertBlankRecord();
        //    SetBatchNumber();
        //}

        //  #endregion

        //#region Create Batch

        //protected void btnCreateBatches_Click(object sender, EventArgs e)
        //{
        //    string message = "";
        //   // string productType = "";
        //    int certifiedBy = Convert.ToInt16(ddlCertifiedBy.SelectedItem.Value);
        //    string screeningInstrument = "";

        //    string selectedItems = String.Join(";",
        //                            chkLstScreeningInstrument.Items.OfType<System.Web.UI.WebControls.ListItem>().Where(r => r.Selected)
        //                            .Select(r => r.Value));
        //    screeningInstrument = selectedItems;

        //    //if (chkLoose.Checked)
        //    //    productType = "Loose";
        //    //else if (chkJewelry.Checked)
        //    //    productType = "Jewelry";
        //    //else
        //    //    productType = "";

        //    if (txtItemsLeft.Text != "0")
        //    {
        //        message += "Please allocate all item quantity to create batches. ";
        //    }

        //    if (certifiedBy == 0)
        //    {
        //        message += "Please specify Certified-By. ";
        //    }

        //    //if (productType == "")
        //    //{
        //    //    message += "Please specify product type. ";
        //    //}

        //    if (screeningInstrument == "")
        //    {
        //        message += "Please specify Screening Instrument. ";
        //    }

        //    if (message != "")
        //    {
        //        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + message + "');", true);
        //        InvalidLabel.Text = message;
        //        return;
        //    }

        //    //  OrderModel orderModel = GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel;

        //    SyntheticScreeningUtils.SetSyntheticScreeningInstrument(Convert.ToInt32(txtOrder.Text), certifiedBy, screeningInstrument, this);

        //    if (Session["BatchItems"] != null)
        //    {
        //        DataTable dt = (DataTable)Session["BatchItems"];
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                int batchCode = Convert.ToInt32(dt.Rows[i]["BatchCode"]);
        //                int allocateItemQty = Convert.ToInt32(dt.Rows[i]["AllocateItemQty"]);
        //                SyntheticScreeningUtils.AddSyntheticBatch(Convert.ToInt32(txtOrder.Text), "TND", batchCode, allocateItemQty, this);
        //            }
        //        }
        //    }

        //    //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Batches created successfully');", true);
        //    InvalidLabel.Text = "Batches created successfully.";
        //    GetOrderDetails();
        //}


        //#endregion

    }
}