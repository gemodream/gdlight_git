﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Grade1.aspx.cs" Inherits="Corpt.Grade1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">
         <div class="demoheading"><asp:Label runat="server" Text="Grade" ID="TitleLabel"></asp:Label>
         </div>
         <asp:UpdatePanel runat="server">
             <ContentTemplate>
         <div class="navbar nav-tabs form-inline" style="font-size: small">
             <asp:RadioButtonList ID="rblModeSelector" runat="server" CssClass="radio" Visible="False"
                 RepeatDirection="Horizontal" Width="250px">
                 <asp:ListItem Value="6" Selected="True">Measure</asp:ListItem>
                 <asp:ListItem Value="5">Color</asp:ListItem>
                 <asp:ListItem Value="4">Clarity</asp:ListItem>
             </asp:RadioButtonList>
             <div>
                 <asp:CheckBox ID="IgnoreCpBox" runat="server" CssClass="checkbox" Text="Ignore Customer Program" Checked="True" Width="240px" />
                 <asp:Label runat="server" ID="NotEnteredLabel" ForeColor="Red" Style="padding-left: 140px;font-weight: bold;font-size: small"></asp:Label>
             </div>
             <table>
                 <tr>
                     <td>
                         <asp:Panel ID="Panel2" runat="server" DefaultButton="LoadButton" CssClass="form-inline">
                             Batch or Item Number:
                             <asp:TextBox runat="server" ID="BatchItemField" placeholder="Batch or Item #"></asp:TextBox>
                             <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item Numbers" ImageUrl="~/Images/ajaxImages/search.png"
                                 OnClick="OnLoadClick" />
                         </asp:Panel>
                     </td>
                     <td>
                         <asp:Button runat="server" ID="NextBatchButton" OnClick="OnNextBatchClick" Text="Next #"
                             Enabled="False" CssClass="btn btn-info" />
                     </td>
                     <td>
                         <asp:Button runat="server" ID="SaveButton" OnClick="OnSaveClick" Text="Save" Enabled="False"
                             CssClass="btn btn-info" />
                     </td>
                     <td>
                         <asp:Button runat="server" ID="UndoButton" OnClick="OnUndoClick" Text="Undo" Enabled="False"
                             CssClass="btn btn-info" />
                     </td>
                     <td>
                         <asp:HyperLink ID="ViewKeyLink" runat="server">View</asp:HyperLink>
                     </td>
                     <td>
                         <asp:Label runat="server" ID="SaveInfoLabel"></asp:Label>
                     </td>
                 </tr>
             </table>
         </div>
         <table>
             <tr style="height: 23px">
                 <td colspan="3"><asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Font-Size="small"></asp:Label></td>
             </tr>
             <tr>
                 <td><b>Items</b></td>
                 <td style="padding-left: 20px"><b>Parts</b></td>
                 <td style="padding-left: 20px"><asp:Label runat="server" ID="ValueLabel" Visible="False"></asp:Label></td>
                 <td style="padding-left: 20px">
                     <asp:Panel runat="server" ID="KeysPanel" CssClass="form-inline" DefaultButton="ApplyKeysButton">
                         <b>Measure Values</b>
                         <asp:TextBox runat="server" ID="KeysField" Width="300px"></asp:TextBox>
                         <asp:ImageButton ID="ApplyKeysButton" runat="server" ToolTip="Apply Hot Keys" ImageUrl="~/Images/ajaxImages/apply.png"
                             OnClick="OnApplyKeysClick" />
                     </asp:Panel>
                 </td>
             </tr>
             <tr>
                 <td style="padding-top: 0px; vertical-align: top;">
                     <asp:ListBox ID="lstItemList" runat="server" Width="125px" 
                         Style="vertical-align: top; overflow-y: hidden" DataValueField="FullItemNumber" DataTextField="FullItemNumber"
                         Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged" 
                         AutoPostBack="True" Font-Size="Small">
                     </asp:ListBox>
                 </td>
                 <td style="vertical-align: top;padding-left: 20px" rowspan="2">
                     <asp:ListBox runat="server" ID="lstParts" AutoPostBack="True" Font-Size="small" DataValueField="PartId"
                         DataTextField="PartName" Rows="5" OnSelectedIndexChanged="OnPartListSelectedChanged">
                     </asp:ListBox>
                     <br />
                     <b>Measures</b><br />
                     <asp:ListBox runat="server" ID="lstMeasures" AutoPostBack="True" Rows="10" OnSelectedIndexChanged="OnMeasureSelected"
                          DataValueField="MeasureId" DataTextField="MeasureTitle" Font-Size="small" />
                 </td>
                 <td style="vertical-align: top;padding-left: 20px">
                     
                     <asp:Panel ID="Panel1" runat="server" CssClass="form-horizontal" DefaultButton="SaveButton"
                         Width="200px">
                         <asp:DropDownList runat="server" ID="MeasureEnumField" Visible="False" Width="100%"
                             DataValueField="MeasureValueID" DataTextField="ValueTitle" OnSelectedIndexChanged="OnEnumMeasureChoice"
                             AutoPostBack="True" />
                         <asp:TextBox ID="MeasureNumericField" runat="server" onkeypress="return IsOneDecimalPoint(event);"
                             OnTextChanged="OnMeasureNumbChanged" AutoPostBack="True" Visible="False" Width="100%"
                             ToolTip="Numeric Value" BackColor="#CCFFCC" />
                         <asp:TextBox ID="MeasureTextField" runat="server" Visible="False" Width="100%" AutoPostBack="True"
                             OnTextChanged="OnMeasureTextChanged"></asp:TextBox>
                     </asp:Panel>
                     <asp:Label runat="server" ID="PicturePathAbbr" Width="200px"></asp:Label><br/>
                     <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image><br/>
                     <asp:Label ID="ErrPictureField" ForeColor="Red"  runat="server" Width="200px" Style="font-size: small;width: 200px;word-wrap: break-word"></asp:Label><br/>
                     <asp:Panel runat="server" ID="ShapePanel" CssClass="form-horizontal" Visible="False"
                         Style="padding-top: 20px">
                         <asp:Label runat="server" ID="ShapePathAbbr" Width="200px" Font-Size="small"></asp:Label><br />
                         <asp:Image ID="itemShape" runat="server" Width="180px" Visible="False"></asp:Image><br />
                         <asp:Label ID="ErrShapeField" ForeColor="Red" runat="server" Width="200px" Style="font-size: small;
                             width: 200px; word-wrap: break-word"></asp:Label><br />
                         <asp:Button runat="server" ID="ShapeEditButton" OnClick="OnShapeEditClick" Text="Plotting"
                             CssClass="btn-info btn-small" Style="margin-top: 10px "/><br/>
                         <asp:Label ID="PlottingLabel" ForeColor="Red" runat="server" Width="200px" Style="font-size: small;
                             width: 200px; word-wrap: break-word"></asp:Label><br />

                     </asp:Panel>
                 </td>
                 <td style="vertical-align: top; padding-left: 20px">
                     <asp:DataGrid runat="server" ID="MeasureValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed" DataKeyField="UniqueKey"
                         onitemdatabound="OnItemDataBound" onitemcommand="OnUndoCommand" >
                         <Columns>
                             <asp:ButtonColumn Text="Undo" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/undo.png" DataTextField="DisplayUndo" /> 
                             <asp:BoundColumn DataField="DisplayByCp" HeaderText="by Cp" ></asp:BoundColumn>
                             <asp:BoundColumn DataField="PartName" HeaderText="Part" ></asp:BoundColumn>
                             <asp:BoundColumn DataField="MeasureTitle" HeaderText="Measure"></asp:BoundColumn>
                             <asp:BoundColumn DataField="ValuePrev" HeaderText="Prev Value"></asp:BoundColumn>
                             <asp:BoundColumn DataField="ValueCurr" HeaderText="Curr Value"></asp:BoundColumn>
                             <asp:BoundColumn DataField="HasChanges" HeaderText="Changed" Visible="False"/>
                             <asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"/>
                         </Columns>
                         <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                         <ItemStyle Font-Names="Cambria" Font-Size="Small"  />
                     </asp:DataGrid>
                 </td>
             </tr>
             </table>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="BatchItemField"
        Display="None" 
                     ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." 
                     ValidationGroup="BatchGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="BatchItemField"
        Display="None" ValidationExpression=".{7,15}" 
                     ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>8 or 9 numeric characters</strong><br/> or item number in the format:<br/><strong>10 or 11 numeric characters</strong>" 
                     ValidationGroup="BatchGroup" /> 
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server"
        FilterType="Custom" ValidChars="01234567890.-" TargetControlID="MeasureNumericField">
    </ajaxToolkit:FilteredTextBoxExtender>
             </ContentTemplate>
              <Triggers>
                <asp:PostBackTrigger ControlID="ShapeEditButton"/>
            </Triggers>
         </asp:UpdatePanel>
     </div>
</asp:Content>
