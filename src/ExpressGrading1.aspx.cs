﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.TreeModel;
using Corpt.Utilities;
using Corpt.Utilities.AccountRepresentative;
using static System.Int32;

namespace Corpt
{
    public partial class ExpressGrading1 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Express Grading";
            if (IsPostBack) return;
            SetViewState(new List<BulkUpdateViewModel>(), SessionConstants.BulkEnteredValues);

            //-- Load Enum Measures
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.BulkEnumMeasures);

            HideRulesGrid();
        }
        #region Load
        int ListMaxRow = 40;
        int ListMinRow = 10;
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            LoadMsgLabel.Text = "";
            SaveMsgLabel.Text = "";
            //-- Load Similar Batches
            var FullBatchNumber = BatchNumberFld.Text.Trim();
            var order = "" + Utils.ParseOrderCode(FullBatchNumber).ToString();
            var batchNumber = Utils.ParseBatchCode(FullBatchNumber).ToString();
            order = Utils.FillToFiveChars(order);
            OrderField.Value = order;
            BatchField.Value = batchNumber;
            LoadExecute();
        }
        private void LoadExecute()
        {
            if (BatchNumberFld.Text.Trim().Length == 0)
            {
                LoadMsgLabel.Text = "A Batch Number is required!";
                return;
            }
            DataTable customerCodeDt = QueryUtils.GetCustomerCodeByItem(BatchNumberFld.Text, this.Page);
            if (customerCodeDt.Rows.Count > 0)
            {
                CustomerCodeValue.Value = customerCodeDt.Rows[0].ItemArray[0].ToString();
                string serviceTypeID = customerCodeDt.Rows[0].ItemArray[1].ToString();
                if (!IfCanLoad(serviceTypeID))
                {
                    LoadMsgLabel.Text = "Wrong service type - screening order";
                    return;
                }
            }
            DataTable dt = new DataTable();
            dt = QueryUtils.GetItemsCpOldNew(BatchNumberFld.Text.Trim(), this.Page);
            //var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row)).ToList();
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
            Session["ItemList"] = dt;
            //var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
            if (itemList.Count == 0)
            {
                LoadMsgLabel.Text = "Items not found";
                return;
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);
            var tempTxtBatchNumber = BatchNumberFld.Text;//Sergey
            var itemNumber = itemList[0];
            string matchedItemNumber = null;
            if (BatchNumberFld.Text != itemNumber.FullBatchNumber)//not batch
            {
                RemeasParamModel matchItemNumber = QueryUtils.GetNewItemNumber(BatchNumberFld.Text, this);
                Session["MatchItemNumber"] = matchItemNumber.NewItemNumber;
                matchedItemNumber = matchItemNumber.NewItemNumber;
            }
            List<string> newItemList = Utils.GetOriginalItemListValues(dt, itemList, BatchNumberFld.Text, matchedItemNumber);
            string prevItemCode = null;
            if (itemNumber.FullBatchNumber != BatchNumberFld.Text)
            {
                int numberLength = BatchNumberFld.Text.Length;
                if (numberLength == 10 || numberLength == 11)//item
                {

                    if (itemNumber.FullBatchNumber != BatchNumberFld.Text.Substring(0, BatchNumberFld.Text.Length - 2))//search by batch
                    {

                        //prevItemCode = Utils.getPrevItemCode(dt, txtBatchNumber.Text);//search by item
                        prevItemCode = Utils.getPrevItemCodeNew(itemList, BatchNumberFld.Text, matchedItemNumber);//search by item
                        if (prevItemCode != null)
                        {
                            BatchNumberFld.Text = prevItemCode;
                            //tempTxtBatchNumber = prevItemCode;
                        }
                    }
                }
                else //batch
                    BatchNumberFld.Text = itemNumber.FullBatchNumber;
            }
            //-- Loading ItemList control
            lstItemList.Items.Clear();
            if (newItemList == null)
            {
                foreach (var singleItemModel in itemList)
                {
                    lstItemList.Items.Add(singleItemModel.FullItemNumber);
                }
            }
            else
            {
                BatchNumberFld.Text = Utils.getPrevItemCodeNew(itemList, tempTxtBatchNumber, matchedItemNumber);
                foreach (var item in newItemList)
                    lstItemList.Items.Add(item);
            }
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                //alex oldnew
                if (newItemList == null)
                    lstItemList.Rows = itemList.Count + 1;
                else
                    lstItemList.Rows = itemList.Count + 1;
            }
            lstItemList.Style["display"] = "";
            if (lstItemList.Items.Count > 0)
            {
                lstItemList.SelectedIndex = 0;
		for (int i=0; i<=lstItemList.Items.Count-1; i++)
                {
                    if (lstItemList.Items[i].Value == BatchNumberFld.Text)
                    {
                        lstItemList.SelectedIndex = i;
                        break;
                    }
                }
                LoadDataForEditing(GetItemNumber(lstItemList.SelectedValue));
            }
            foreach (ListItem item in lstItemList.Items)
            {
                if (CheckIfFailed(item.Value))
                {
                    RemoveReports(GetItemNumber(item.Text));
                    item.Text = ItemTextAddString(item.Text, "-");
                }
                else
                {
                    AddReports(GetItemNumber(item.Text));
                    item.Text = ItemTextAddString(item.Text, "+");
                }
            }
            var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
            SetViewState(parts, SessionConstants.GradeMeasureParts);
            LoadPartsTree();
        }
        private bool IfCanLoad(string serviceTypeID) 
        {
            if (DbConstants.GDScreeningServiceTypeID.Equals(serviceTypeID) || DbConstants.GDQCServiceTypeID.Equals(serviceTypeID) || DbConstants.GDQCAndScreeningServiceTypeID.Equals(serviceTypeID))
            {
                return false;
            }
            return true;
        }
        #endregion

        private void LoadPartsTree()
        {
            PartTree.Nodes.Clear();
            var parts = GetPartsFromView();
            if (parts.Count == 0)
            {
                return;
            }
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Selected = true;
            OnPartsTreeChanged(null, null);
        }
        private List<MeasurePartModel> GetPartsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
        }
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
        {
            var itemNumber = GetItemNumber(lstItemList.SelectedValue);
            LoadDataForEditing(itemNumber);
        }
        protected void OnItemLoad(object sender, EventArgs e)
        {
            var box = sender as ListBox;
            foreach (ListItem item in box.Items)
            {                
                if (item.Text.Contains("-"))
                {
                    item.Attributes.Add("style", "background-color: #f5cbd3e6");
                }
                else 
                {
                    item.Attributes.Add("style", "background-color: #c5edc5");
                }
                if (item.Selected)
                {
                    item.Attributes.Remove("style");
                }
            }        
        }
        protected bool CheckIfFailed(string str)
        {
            string itemNumber = GetItemNumber(str);
            List<ExpressGradingModel> measures = QueryUtils.GetSkuRules(Convert.ToInt64(itemNumber), this);
            QueryUtils.SetRealValues(itemNumber, measures, this);
            foreach (ExpressGradingModel rule in measures)
            {
                //IvanB 17/05 start
                if (rule.MeasureClass == ExpressGradingModel.MeasureClassNumeric)
                {
                    var numericSkuVal = rule.MeasureValueSku != null ? Convert.ToDecimal(rule.MeasureValueSku) : 0;
                    var numericRealVal = rule.MeasureValueReal != null ? Convert.ToDecimal(rule.MeasureValueReal) : 0;
                    if (numericSkuVal != numericRealVal)
                        return true;
                }
                else 
                {
                    if (!rule.MeasureValueSku.ToLower().Equals(rule.MeasureValueReal.ToLower()))
                    {
                        if (!("None".Equals(rule.MeasureValueSku) && "".Equals(rule.MeasureValueReal)) &&
                                !("".Equals(rule.MeasureValueSku) && "None".Equals(rule.MeasureValueReal)))
                        {
                            return true;
                        }
                    }
                }
               
                //IvanB 17/05 end
            }
            return false;
        }
        protected string ItemTextAddString(string text, string addition)
        {
            string result = text;
            if (text.Contains("-"))
            {
                result = text.Split('-')[0] + addition;
            }
            else if (text.Contains("+"))
            {
                result = text.Split('+')[0] + addition;
            }
            else 
            {
                result = text + addition;
            }
            return result;
        }
        protected string GetItemNumber(string str)
        {
            string result = "";
            if (str.StartsWith("*"))
            {
                str = str.Substring(1);
            }
            if (str.Contains("-"))
            {
                result = str.Split('-')[0];
            }
            else if (str.Contains("+"))
            {
                result = str.Split('+')[0];
            }
            else
            {
                result = str;
            }
            return result;
        }
        protected void HideRulesGrid()
        {
            ButtonsUpdatePanel.Visible = false;
            Rules.Visible = false;
        }
        protected void ShowRulesGrid()
        {
            ButtonsUpdatePanel.Visible = true;
            Rules.Visible = true;
        }
        protected void OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var textbox = e.Row.Cells[3].FindControl("MeasureValueReal") as TextBox;
                var dropdown = e.Row.Cells[3].FindControl("MeasureRealEnumFld") as DropDownList;
                var isEnum = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "MeasureClass")) == ExpressGradingModel.MeasureClassEnum;
                //IvanB 17/05 start
                var isNumeric = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "MeasureClass")) == ExpressGradingModel.MeasureClassNumeric;
                string realValue = DataBinder.Eval(e.Row.DataItem, "MeasureValueReal").ToString();
                string skuValue = DataBinder.Eval(e.Row.DataItem, "MeasureValueSku").ToString();
                if (isEnum)
                {
                    BuildDropdownList(dropdown, (DataBinder.Eval(e.Row.DataItem, "MeasureCode")).ToString());
                    var selected = dropdown.Items.Cast<ListItem>().FirstOrDefault(i => i.Text.Equals(realValue, StringComparison.InvariantCultureIgnoreCase));
                    if (selected != null)
                    {
                        selected.Selected = true;
                    }

                    if (skuValue != dropdown.SelectedItem.Text)
                    {
                        if (("None".Equals(skuValue) && "".Equals(dropdown.SelectedItem.Text.Trim())) ||
                            ("".Equals(skuValue.Trim()) && "None".Equals(dropdown.SelectedItem.Text)))
                        {
                            e.Row.Cells[3].BorderColor = System.Drawing.Color.Empty;
                        }
                        else
                        {
                            e.Row.Cells[3].BorderColor = System.Drawing.Color.Red;
                        }
                    }
                    else
                    {
                        e.Row.Cells[3].BorderColor = System.Drawing.Color.Empty;
                    }
                }
                else if (isNumeric)
                {
                    textbox.Text = realValue;
                    var numericRealValue = (realValue != null && realValue != "") ?  Convert.ToDecimal(realValue) : 0;
                    var numericSkuValue = (skuValue != null && skuValue != "") ? Convert.ToDecimal(skuValue) : 0;
                    e.Row.Cells[3].BorderColor = numericSkuValue != numericRealValue ? System.Drawing.Color.Red : System.Drawing.Color.Empty;
                }
                else
                {
                    textbox.Text = realValue;
                    e.Row.Cells[3].BorderColor = skuValue != realValue ? System.Drawing.Color.Red : System.Drawing.Color.Empty;
                }
                //IvanB 17/05 end
                textbox.Visible = !isEnum;
                dropdown.Visible = isEnum;
            }
        }
        private void BuildDropdownList(DropDownList dropdown, string measureCode)
        {
            List<EnumMeasureModel> measures = QueryUtils.GetEnumMeasureByCode(this, Convert.ToInt32(measureCode));
            if (Convert.ToInt32(measureCode) == (int)EnumDropDownBlock.Shapes)
            {
                var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, this);
                measures = measures.Where(x => !blockList.Exists(y => x.MeasureValueName.Equals(y.BlockedDisplayName))).ToList();
                //Remove duplicates
                measures = measures.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            dropdown.DataSource = measures;
            dropdown.DataBind();
        }
        private bool IsAnyFailed()
        {
            foreach (GridViewRow row in RulesGrid.Rows)
            {
                foreach (TableCell cell in row.Cells)
                {
                    if (cell.BorderColor.Name == "Red")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected void RealValue_TextboxChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var row = textBox.Parent.Parent as GridViewRow;
            var measureSkuVal = row.FindControl("HdnMeasureValueSku") as HiddenField;
            var measureClass = row.FindControl("HdnMeasureClass") as HiddenField;
            //IvanB 23/05 start
            if (measureClass.Value == ExpressGradingModel.MeasureClassNumeric.ToString())
            {
                var numericSkuVal = measureSkuVal.Value != null ? Convert.ToDecimal(measureSkuVal.Value) : 0;
                var numericRealVal = textBox.Text != null ? Convert.ToDecimal(textBox.Text) : 0;
                row.Cells[3].BorderColor = numericSkuVal != numericRealVal ? System.Drawing.Color.Red : System.Drawing.Color.Empty;
            }
            else
            {
                row.Cells[3].BorderColor = measureSkuVal.Value.Trim() != textBox.Text.Trim() ? System.Drawing.Color.Red : System.Drawing.Color.Empty;                
            }
            //IvanB 23/05 end
            PassButton.Enabled = !IsAnyFailed();
            FailButton.Enabled = IsAnyFailed();
        }
        protected void RealValue_DropdownChanged(object sender, EventArgs e)
        {
            var dropdown = sender as DropDownList;
            var row = dropdown.Parent.Parent as GridViewRow;
            var measureSkuVal = row.FindControl("HdnMeasureValueSku") as HiddenField;
            //IvanB 23/05 start
            row.Cells[3].BorderColor = System.Drawing.Color.Empty;
            if (!measureSkuVal.Value.Trim().ToLower().Equals(dropdown.SelectedItem.Text.Trim().ToLower()))
            {
                if (!("None".Equals(measureSkuVal.Value.Trim()) && "".Equals(dropdown.SelectedItem.Text.Trim())) &&
                        !("".Equals(measureSkuVal.Value.Trim()) && "None".Equals(dropdown.SelectedItem.Text.Trim())))
                {
                    row.Cells[3].BorderColor = System.Drawing.Color.Red;
                }
            }
            //row.Cells[3].BorderColor = measureSkuVal.Value.Trim() != dropdown.SelectedItem.Text.Trim() ? System.Drawing.Color.Red : System.Drawing.Color.Empty;
            //IvanB 23/05 end
            PassButton.Enabled = !IsAnyFailed();
            FailButton.Enabled = IsAnyFailed();
        }
        protected void OnPartsTreeChanged(object sender, EventArgs e)
        {
            if (GetSelectedItemModel() == null) return;
            var currPart = PartTree.SelectedValue;
            var prevPart = PartEditing.Value;
            if (!string.IsNullOrEmpty(prevPart) && currPart != prevPart)
            {
                GetNewValuesFromGrid();
            }
            LoadDataForEditing(GetItemNumber(lstItemList.SelectedValue));
            var ordernumber = lstItemList.SelectedValue;
            //alex if (sender != null)
            PartEditing.Value = currPart;
        }
        private SingleItemModel GetSelectedItemModel()
        {
            var itemNumber = lstItemList.SelectedValue;
            if (itemNumber.StartsWith("*"))
            {
                itemNumber = itemNumber.Substring(1);
            }
            if (string.IsNullOrEmpty(itemNumber)) return null;
            return GetItemModelFromView(itemNumber);
        }
        private SingleItemModel GetItemModelFromView(string itemNumber)
        {
            //return GetItemNumbersFromView().Find(m => m.FullOldItemNumber == itemNumber);
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber || m.FullOldItemNumber == itemNumber || m.FullBatchNumber == itemNumber);
        }
        private List<SingleItemModel> GetItemNumbersFromView()
        {

            return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
        }
        private void GetNewValuesFromGrid()
        {
            //get values from new grid
        }
        private void LoadDataForEditing(string item)
        {
            List <ExpressGradingModel> measures= QueryUtils.GetSkuRules(Convert.ToInt64(item), this);
            QueryUtils.SetRealValues(item, measures, this);
            RulesGrid.DataSource = measures;            
            RulesGrid.DataBind();
            ShowRulesGrid();
            PassButton.Enabled = !IsAnyFailed();
            FailButton.Enabled = IsAnyFailed();
        }
        //IvanB 23/05 start
        protected void OnPassButtonClick(object sender, EventArgs e)
        {
            string err = "";
            List<MeasureValueModel> changedMeasures = FindChangedValues();
            if (changedMeasures.Count > 0)
            {
                err = QueryUtils.SaveNewMeasures(changedMeasures, this);
            }

            string item = lstItemList.SelectedValue.Replace("*", "").Replace("-", "").Replace("+", "");
            SaveMsgLabel.Text = item + " passed";
            AddReports(GetItemNumber(lstItemList.SelectedValue));
            lstItemList.SelectedItem.Text = err == "" ? ItemTextAddString(lstItemList.SelectedItem.Text, "+") : lstItemList.SelectedItem.Text;
            lstItemList.SelectedItem.Value = err == "" ? ItemTextAddString(lstItemList.SelectedItem.Text, "+") : lstItemList.SelectedItem.Text;

            if (!lstItemList.SelectedValue.StartsWith("*"))
            {
                lstItemList.SelectedItem.Text = @"*" + lstItemList.SelectedValue;
                lstItemList.SelectedItem.Value = @"*" + lstItemList.SelectedValue;
            }
            if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
            {           
                lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;                
            }
            RefreshRealValuesAndButtons();
        }

        protected void OnFailedButtonClick(object sender, EventArgs e)
        {
            string err = "";
            List <MeasureValueModel> changedMeasures = FindChangedValues();

            if (changedMeasures.Count > 0)
            {
                err = QueryUtils.SaveNewMeasures(changedMeasures, this);                
            }
            string item = lstItemList.SelectedValue.Replace("*", "").Replace("-", "").Replace("+", "");
            SaveMsgLabel.Text = item + " failed";
            if (string.IsNullOrEmpty(err))
            {
                RemoveReports(GetItemNumber(lstItemList.SelectedValue));
                if (!lstItemList.SelectedValue.StartsWith("*"))
                {
                    lstItemList.SelectedItem.Text = @"*" + lstItemList.SelectedValue;
                    lstItemList.SelectedItem.Value = @"*" + lstItemList.SelectedValue;
                }

                lstItemList.SelectedItem.Text = ItemTextAddString(lstItemList.SelectedItem.Text, "-");
                lstItemList.SelectedItem.Value = ItemTextAddString(lstItemList.SelectedItem.Text, "-");

                if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                {
                    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                }
                RefreshRealValuesAndButtons();
            }
        }
        
        private List<MeasureValueModel> FindChangedValues()
        {
            var dataRows = RulesGrid.Rows;
            var itemNumber = GetItemNumber(lstItemList.SelectedValue);
            var itemCode = Utils.ParseItemCode(itemNumber);
            var batchId = QueryUtils.GetBatchIdByItemNumber(itemNumber, this);
            List<MeasureValueModel> changedMeasures = new List<MeasureValueModel>();
            foreach (GridViewRow row in dataRows)
            {
                var real = (row.FindControl("HdnMeasureRealValue") as HiddenField).Value;
                var measureClass = (row.FindControl("HdnMeasureClass") as HiddenField).Value;
                var newRealValue = "";
                switch (measureClass)
                {
                    case "1"://Enum
                        newRealValue = (row.FindControl("MeasureRealEnumFld") as DropDownList).SelectedItem.Text.Trim().ToLower();
                        real = real.Trim().ToLower();
                        if (newRealValue != real)
                        {
                            MeasureValueModel measure = ParseFailedRowData(itemCode, Convert.ToInt32(batchId), row);
                            changedMeasures.Add(measure);
                        }
                        continue;
                    case "2"://text
                        newRealValue = (row.FindControl("MeasureValueReal") as TextBox).Text;
                        if(newRealValue.Trim().ToLower() != real.Trim().ToLower())
                        {
                            MeasureValueModel measure = ParseFailedRowData(itemCode, Convert.ToInt32(batchId), row);
                            changedMeasures.Add(measure);
                        }
                        continue;
                    case "3"://numeric
                        newRealValue = (row.FindControl("MeasureValueReal") as TextBox).Text;
                        if (Convert.ToDecimal(newRealValue) != Convert.ToDecimal(real))
                        {
                            MeasureValueModel measure = ParseFailedRowData(itemCode, Convert.ToInt32(batchId), row);
                            changedMeasures.Add(measure);
                        }
                        continue;
                }
            }
            return changedMeasures;
        }
        //IvanB 23/05 end
        private MeasureValueModel ParseFailedRowData(int itemCode, int batchId, GridViewRow row)
        {
            MeasureValueModel measure = new MeasureValueModel();
            measure.ItemCode = itemCode.ToString();
            measure.BatchId = batchId;
            measure.PartId = Convert.ToInt32((row.FindControl("HdnPartId") as HiddenField).Value);
            measure.MeasureId = (row.FindControl("HdnMeasureID") as HiddenField).Value;
            var measureClass = Convert.ToInt32((row.FindControl("HdnMeasureClass") as HiddenField).Value);
            if (measureClass == MeasureModel.MeasureClassEnum)
            {
                measure.MeasureValueId = (row.FindControl("MeasureRealEnumFld") as DropDownList).SelectedItem.Value; 
                measure.MeasureValue = null;
                measure.StringValue = null;
            }
            if (measureClass == MeasureModel.MeasureClassText)
            {
                measure.MeasureValueId = null;
                measure.MeasureValue = null;
                measure.StringValue = (row.FindControl("MeasureValueReal") as TextBox).Text; 
            }
            if (measureClass == MeasureModel.MeasureClassNumeric)
            {
                measure.MeasureValueId = null;
                measure.MeasureValue = (row.FindControl("MeasureValueReal") as TextBox).Text;
                measure.StringValue = null;
            }
            return measure;
        }
        protected void AddReports(string itemNumber)
        {
            string err = QueryUtils.AddPassedReports(itemNumber, this);
        }
        protected void RemoveReports(string itemNumber)
        {
            string err = QueryUtils.VoidItemReport(itemNumber, this);
        }
        protected void RefreshRealValuesAndButtons()
        {
            LoadDataForEditing(GetItemNumber(lstItemList.SelectedValue));
            PassButton.Enabled = !IsAnyFailed();
            FailButton.Enabled = IsAnyFailed();
        }

        protected List<Item4Invoice> RunBilling(List<Item4Invoice> item4Invoices)
        {
            if (item4Invoices.Count == 0)
            {
                throw new Exception($@"There are no items loaded.");
            }

            // Save Additional Services
            var additionalServices = QueryAccountRep.GetAdditionalServicesByOrderList(
                new List<int> { item4Invoices[0].GroupCode }, this);
            var batchShortModel = new BatchShortModel
            {
                BatchId = item4Invoices[0].BatchId,
                BatchCode = item4Invoices[0].BatchCode,
                GroupCode = item4Invoices[0].GroupCode
            };
            var additionalServiceRows = BillingUtils.GetAdditionalServicesRows(
                additionalServices, 
                new List<BatchShortModel> { batchShortModel } );
            additionalServiceRows.ForEach(row => 
                LogUtils.UpdateSpLog($"Additional Service: {row.OrderCode} {row.BatchCode} {row.Name} {row.Price}", this.Session));
            BillingUtils.SaveAdditionalServices(additionalServiceRows, this.Session);

            // Create Invoice
            var customerModel = QueryAccountRep.GetCustomerByOrderCode(item4Invoices[0].GroupCode, this.Session);
            if (customerModel == null)
            {
                throw new Exception($"Cannot Get Customer By Order Code={item4Invoices[0].GroupCode}");
            }

            var customerId = Parse(customerModel.CustomerId);
            var customerOfficeId = Parse(customerModel.OfficeId);

            var invoiceId = QueryAccountRep.AddInvoice2(customerId, customerOfficeId, this.Session);
            return BillingUtils.InvoiceGo(
                invoiceId, 
                customerId,
                customerOfficeId, 
                1, 
                true, 
                false, 
                new List<BatchShortModel> {batchShortModel},
                item4Invoices, 
                this.Session);
        }

        protected void OnFinishButtonClick(object sender, EventArgs e)
        {
            var item4Invoices = lstItemList.Items.Cast<ListItem>().Select(item =>
            {
                var fullItemNumber = GetItemNumber(item.Value);
                if (!Utils.DissectItemNumber(fullItemNumber, out int orderCode, out var batchCode, out var itemCode))
                {
                    throw new Exception($"Cannot parse item number '{item.Value}'");
                }
                var singleItemModel = GetFullBatchNumber(fullItemNumber);
                SaveMsgLabel.Text = singleItemModel.FullBatchNumber + " finished";
                return new Item4Invoice(singleItemModel.BatchId, 
                    Convert.ToInt32(singleItemModel.ItemCode), 
                    Convert.ToInt32(singleItemModel.OrderCode), 
                    Convert.ToInt32(singleItemModel.BatchCode));
            }).ToList();

            var results = RunBilling(item4Invoices);
            results.ForEach(r =>
                LogUtils.UpdateSpLog($"Create Invoice result: {r.Result} {r.Description}", this.Session));
        }
        //IvanB 11/05 start
        protected SingleItemModel GetFullBatchNumber(string fullItemNumber)
        {
            DataTable dt = new DataTable();
            dt = QueryUtils.GetItemsCpOldNew(fullItemNumber, this.Page);
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
            var item = itemList.FirstOrDefault(x => x.FullOldItemNumber == fullItemNumber);
            return item;
        }
        //IvanB 11/05 end
    }
}