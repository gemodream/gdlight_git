using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace Corpt
{
	/// <summary>
	/// Summary description for CertLabels.
	/// </summary>
	public partial class CertLabels : System.Web.UI.Page
	{

		public const int CertLabel = 10;
		public const int ItemizedLabel = 20;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["ID"]==null)
			{
				Response.Redirect("Login.aspx");
			}
			switch(int.Parse(Session["GDLight_LabelType"].ToString()))
			{
				case CertLabels.CertLabel :
				{
					lblType.Text="CertLabel";
					break;
				}
				case CertLabels.ItemizedLabel :
				{
					lblType.Text="ItemizedLabel";
					break;
				}
				default :
				{
					lblType.Text="...";
					break;
				}
			}		
		}

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void txtOrderBatchNumber_TextChanged(object sender, System.EventArgs e)
		{
//			cmdNewFormat_Click(sender, e);
		}

		
		private bool CustomLabel(string orderBatchCode)
		{
			
			if(Regex.IsMatch(orderBatchCode, @"^\d{8}$|^\d{9}$"))
			{
				//GetDocument, check if Custom Label is attahced.
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				conn.Open();
	
				//Check if the batch number is valid and get BatchID if yes.
				SqlCommand checkIfBatchExists = new SqlCommand("sp_BatchExists");
				
				checkIfBatchExists.Connection=conn;
				checkIfBatchExists.CommandType=CommandType.StoredProcedure;
						
				checkIfBatchExists.Parameters.Add("@BatchFullNumber",orderBatchCode.Trim()+"00");
				
				SqlDataReader checkIfBatchExistsReader = checkIfBatchExists.ExecuteReader();
				
				if (!checkIfBatchExistsReader.HasRows)
				{
					WrongInfoLabel.Text="This batch number is invalid. Please check the batch number and try again.";
					conn.Close();
					return false;
				}
				else
				{
					checkIfBatchExistsReader.Read();
					String myBatchID;
					myBatchID=checkIfBatchExistsReader.GetSqlValue(checkIfBatchExistsReader.GetOrdinal("BatchID")).ToString();
					String myDocumentID;
					if(Utils.IsCustomLabel(myBatchID,this, out myDocumentID))
					{
						conn.Close();
						Session["BatchID"]=myBatchID;
						Session["DocumentID"]=myDocumentID;
						return true;
					}
					else
					{
						WrongInfoLabel.Text="Unabble to print label for this batch. Please attach label template into Customer Program";
						conn.Close();
						return false;
					}
				}
				conn.Close();
			}
			return false;
		}


		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("GDLight.aspx");
		}
		

		private void CertLabelsGo(object sender, System.EventArgs e)
		{
			DataTable dt = new DataTable();
			if(CustomLabel(txtBatchNumber.Text.Trim()))
			{
				Label2.Text=Session["BatchID"].ToString()+"</br>"+Session["DocumentID"].ToString();

				ArrayList errList = new ArrayList();
				Session["LabelNoPrintList"] = errList;
				//fill dataset for labels here
				dt = NewFormat(Session["BatchID"].ToString(),Session["DocumentID"].ToString());
				//display "Will Not Print";
				errList = (ArrayList) Session["LabelNoPrintList"];
#if TRUE
#else
				if(errList.Count>0)
				{
					//display some harsh words
					WrongInfoLabel.Text="<h1>Can not print labels - missing data.</h1><br/>";
					foreach(String s in errList)
					{
						WrongInfoLabel.Text+=s+"<br/>";
					}
					return;
				}
				else
				{
					WrongInfoLabel.Text="";
				}
#endif
				
				DataSet ds = new DataSet("Labels");
				ds.Tables.Add(dt);
				String phisicalDoc;
				phisicalDoc = Session["TempDir"].ToString() + Session.SessionID + ".pjd";
				XmlTextWriter textWriter = new XmlTextWriter(phisicalDoc, null);
				textWriter.Formatting = Formatting.Indented;
				textWriter.Indentation=6;
				textWriter.Namespaces=false;

				textWriter.WriteStartDocument();
				textWriter.WriteStartElement("","PrintJob","");

				textWriter.WriteStartElement("","Type","");
				textWriter.WriteString("CertLabel");
				textWriter.WriteEndElement();
// change to include schema - makes incompatible with prev. printutil version
				textWriter.WriteStartElement("","DataSchema","");
				textWriter.WriteRaw(ds.GetXmlSchema().Substring("<?xml version=\"1.0\" encoding=\"utf-16\"?>".Length));
				textWriter.WriteEndElement();
// end change

				textWriter.WriteStartElement("","Data","");
				textWriter.WriteRaw(ds.GetXml());
				textWriter.WriteEndElement();

				textWriter.WriteStartElement("","Template","");

				FileStream fs = new FileStream(Session["CertLabel"].ToString(), FileMode.Open, FileAccess.Read);
				BinaryReader br = new BinaryReader(fs);

				ArrayList al = new ArrayList();
                
				try
				{
					while(true)
					{
						MyByteWrapper mbw = new MyByteWrapper(br.ReadByte());						
						al.Add(mbw);
					}
				}
				catch(Exception ex)
				{//EOF
					System.Console.WriteLine(ex.Message);
				}

				byte[] alb = new byte[al.Count];
				for(int i=0; i<al.Count; i++)
				{

					alb[i]=(al[i] as MyByteWrapper).myVal;
				}

				String enqdString = "";
				enqdString = System.Convert.ToBase64String(alb);
				textWriter.WriteString(enqdString);
				textWriter.WriteEndDocument();
				textWriter.Flush();
				textWriter.Close();	
				Response.Redirect("Temp\\"+Session.SessionID + ".pjd",false);
			}
			else
			{
				Label2.Text="Could not print. Batch number is wrong";
			}
		}		

		protected void cmdNewFormat_Click(object sender, System.EventArgs e)
		{
			WrongInfoLabel.Text="";
			switch(int.Parse(Session["GDLight_LabelType"].ToString()))
			{
				case CertLabels.CertLabel :
				{
					CertLabelsGo(sender,e);
					break;
				}
				case CertLabels.ItemizedLabel :
				{
					ItemizedLabelPrint();
					break;
				}
				default :
				{
					lblType.Text="...";
					break;
				}
			}		
		}

		private void ItemizedLabelPrint()
		{
			string sFullBatchNumber = txtBatchNumber.Text.Trim();
			if(Regex.IsMatch(sFullBatchNumber, @"^\d{8}$|^\d{9}$"))
			//if(txtBatchNumber.Text.Trim().Length==8)
			{
				DataTable dataTable = new DataTable();
				sFullBatchNumber = sFullBatchNumber + "00";
				dataTable = ItemizedLabelSet(Utils.ParseOrderCode(sFullBatchNumber).ToString(), Utils.ParseBatchCode(sFullBatchNumber).ToString());
			
				if(dataTable.Rows.Count>0)
				{
					DataSet ds = new DataSet("Labels");
					ds.Tables.Add(dataTable);
					String phisicalDoc;
					phisicalDoc = Session["TempDir"].ToString() + Session.SessionID + ".pjd";
					XmlTextWriter textWriter = new XmlTextWriter(phisicalDoc, null);
					textWriter.Formatting = Formatting.Indented;
					textWriter.Indentation=6;
					textWriter.Namespaces=false;

					textWriter.WriteStartDocument();
					textWriter.WriteStartElement("","PrintJob","");

					textWriter.WriteStartElement("","Type","");
					textWriter.WriteString("ItemizedLabel");
					textWriter.WriteEndElement();

					textWriter.WriteStartElement("","Data","");
					textWriter.WriteRaw(ds.GetXml());
					textWriter.WriteEndElement();

					textWriter.WriteStartElement("","Template","");

					FileStream fs = new FileStream(Session["ItemizedLabel"].ToString(), FileMode.Open, FileAccess.Read);
					BinaryReader br = new BinaryReader(fs);

					ArrayList al = new ArrayList();
            
					try
					{
						while(true)
						{
							MyByteWrapper mbw = new MyByteWrapper(br.ReadByte());						
							al.Add(mbw);
						}
					}
					catch(Exception ex)
					{//EOF
						System.Console.WriteLine(ex.Message);
					}

					byte[] alb = new byte[al.Count];
					for(int i=0; i<al.Count; i++)
					{

						alb[i]=(al[i] as MyByteWrapper).myVal;
					}

					String enqdString = "";
					enqdString = System.Convert.ToBase64String(alb);
					textWriter.WriteString(enqdString);
					textWriter.WriteEndDocument();
					textWriter.Flush();
					textWriter.Close();	
					WrongInfoLabel.Text="";
					Response.Redirect("Temp\\"+Session.SessionID + ".pjd");
					WrongInfoLabel.Text="";
				}
				else
				{
					//Didn't find any items for this batch number
				}
			}
			else
			{
				//Wrong txtBatchNumber data length
			}
		}

		
		private DataTable ItemizedLabelSet(String groupCode, String batchCode)
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("sp_GetBatchForLabels");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@OrderCode", groupCode);
			command.Parameters.Add("@BatchCode", batchCode);

			SqlDataAdapter da = new SqlDataAdapter(command);

			DataSet dsItems = new DataSet("dsItems");
			DataTable dt = new DataTable();
			DataColumn dc = new DataColumn("BarCode");
			dt.Columns.Add(dc);
			dc = new DataColumn("CombinedNumber");
			dt.Columns.Add(dc);
			dc = new DataColumn("Column1");
			dt.Columns.Add(dc);
			dc = new DataColumn("Column2");
			dt.Columns.Add(dc);
			dc = new DataColumn("Column3");
			dt.Columns.Add(dc);
			dc = new DataColumn("Column4");
			dt.Columns.Add(dc);
			
			da.Fill(dsItems);

			if((dsItems.Tables.Count>0)&&(dsItems.Tables[0].Rows.Count>0))
			{
				foreach(DataRow dr in dsItems.Tables[0].Rows)
				{
					DataRow rdr = dt.NewRow();
					rdr["BarCode"]="*"+Utils.FullItemNumber(dr["OrderCode"].ToString(),dr["BatchCode"].ToString(),dr["ItemCode"].ToString())+"*";
					if(Utils.FullItemNumber(dr["OrderCode"].ToString(),dr["BatchCode"].ToString(),dr["ItemCode"].ToString())==Utils.FullItemNumber(dr["PrevOrderCode"].ToString(),dr["PrevBatchCode"].ToString(),dr["PrevItemCode"].ToString()))
					{
						rdr["CombinedNumber"]=Utils.FullItemNumber(dr["OrderCode"].ToString(),dr["BatchCode"].ToString(),dr["ItemCode"].ToString());
					}
					else
					{
						rdr["CombinedNumber"]=Utils.FullItemNumber(dr["OrderCode"].ToString(),dr["BatchCode"].ToString(),dr["ItemCode"].ToString())+"("+Utils.FullItemNumber(dr["PrevOrderCode"].ToString(),dr["PrevBatchCode"].ToString(),dr["PrevItemCode"].ToString())+")";
					}
					rdr["Column1"]="Wt: ";
					rdr["Column2"]=Utils.NullValue(dr["Weight"]);
					rdr["Column3"]=dr["CustomerProgramName"].ToString();
					rdr["Column4"]="";
					dt.Rows.Add(rdr);
				}
			}
			return dt;
		}

	
		private DataTable NewFormat(String batchId, String documentId)
		{
			var myDocStructure = new DataSet("Doc Structure");

			var itemList = new DataSet("Item List");
            			
			var conn = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

            var command = new SqlCommand("wspvvGetItemsByBatchRealNumbers") { CommandType = CommandType.StoredProcedure, Connection = conn };
            command.Parameters.AddWithValue("@BatchID", batchId);
		
			var dataAdapter = new SqlDataAdapter(command);
			dataAdapter.Fill(itemList);

			command = new SqlCommand("spGetDocumentValue") {Connection = conn};
		    command.Parameters.AddWithValue("@DocumentID",Int32.Parse(documentId));
			command.Parameters.AddWithValue("@AuthorID", Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", Session["AuthorOfficeID"]);
			command.CommandType=CommandType.StoredProcedure;

			dataAdapter=new SqlDataAdapter(command);
			dataAdapter.Fill(myDocStructure);

			var myItemList = itemList.Tables[0].Copy();
			myItemList.TableName="MyItemList";

			var myLabelStructure = myDocStructure.Tables[0].Copy();
			myLabelStructure.TableName="MyLabelStructure";

            //-- Result table columns
            var result = new DataTable("LabelSet");
            result.Columns.Add("BarCode");
            result.Columns.Add("CombinedNumber");
            result.Columns.Add("newBatchNumber");
            result.AcceptChanges();

            var dtLabelComplete = myLabelStructure.Copy();
            
            if (myLabelStructure.Rows.Count > 4)
			{
				myLabelStructure.Rows.RemoveAt(0);
				myLabelStructure.AcceptChanges();
			}
		    var c = 1;
			foreach(DataRow dr in myLabelStructure.Rows)
			{
				result.Columns.Add("Column" + c);
			    c++;
			}


            
			foreach(DataRow dr in myItemList.Rows)
			{
				var itemLabel = result.NewRow();				

                var strOldItemNumber = Utils.FullItemNumber(dr["prevOrderCode"].ToString(), dr["prevBatchCode"].ToString(), dr["PrevITemCode"].ToString());
                itemLabel["BarCode"] = "*" + strOldItemNumber + "*";

				var strNewItemNumber = Utils.FullItemNumber(dr["OrderCode"].ToString(), dr["BatchCode"].ToString(), dr["ItemCode"].ToString());
                itemLabel["CombinedNumber"] = strOldItemNumber;

                var strNewNumberDot = Utils.GetItemNumberWithDots(strNewItemNumber);
				itemLabel["newBatchNumber"]= strNewNumberDot.Substring(0, strNewNumberDot.Length - 3);	

				//check  if short numbers
				if(dtLabelComplete.Rows.Count>4)
				{
					var strDirtyShortNumber = Utils.ParametersToValueNOHighlights(
                        dtLabelComplete.Rows[0]["Value"].ToString(), batchId, dr["ItemCode"].ToString(), this);
					var intShortItemNumber = int.Parse(strDirtyShortNumber.Split('.')[0]);
					itemLabel[0] = "*"+intShortItemNumber.ToString("X")+"*";
					itemLabel[1] = intShortItemNumber.ToString("X");
					itemLabel["newBatchNumber"]=strNewItemNumber;
				}
				if(myLabelStructure.Rows.Count>4)
				{
					WrongInfoLabel.Text="Incorrect Label Structure. Please correct report in the Customer Program.";
					((ArrayList) Session["LabelNoPrintList"]).Add("Incorrect Label Structure. Please correct report in the Customer Program.");
				}
				else
				{
					int i=1;
					foreach(DataRow ls in myLabelStructure.Rows)
					{
						itemLabel["Column" + i]=Utils.ParametersToValueNOHighlights(
                            ls["Value"].ToString(), batchId,dr["ItemCode"].ToString(), this) + Utils.NullValue(ls["Unit"]);
					    i++;
					}
				}
				
				result.Rows.Add(itemLabel);
			}
			return result;
		}
	}
}
