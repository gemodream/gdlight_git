<%@ Page language="c#" Codebehind="TrackingII.aspx.cs" AutoEventWireup="True" Inherits="Corpt.TrackingII" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TrackingII</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="Label1" runat="server" CssClass="text" style="Z-INDEX: 0">Customer:</asp:label>
			<asp:dropdownlist id="lstCustomerList" Runat="server" CssClass="inputStyleCC5" style="Z-INDEX: 0"></asp:dropdownlist><br>
			<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5">
				<TR>
					<TD><asp:label id="Label3" runat="server" CssClass="text">From</asp:label>
						<asp:calendar id="calFrom" runat="server" CssClass="text" style="Z-INDEX: 0"></asp:calendar></TD>
					<TD>
						<asp:label id="Label2" runat="server" CssClass="text">To</asp:label>
						<asp:calendar id="calTo" runat="server" CssClass="text" style="Z-INDEX: 0"></asp:calendar></TD>
				</TR>
			</TABLE>
			<asp:button id="cmdLookup" runat="server" CssClass="buttonStyle" Width="88px" Text="Lookup"
				style="Z-INDEX: 0" onclick="cmdLookup_Click"></asp:button>
			<asp:CheckBox id="chkHideCurrent" runat="server" Text="Hide Current Batches" Checked="True"></asp:CheckBox>
			<asp:CheckBox style="Z-INDEX: 0" id="chkHideCheckedOut" runat="server" Text="Hide Checked Out Batches"
				Checked="True"></asp:CheckBox><br>
			<br>
			<asp:dropdownlist style="Z-INDEX: 0" id="lstAction" CssClass="inputStyleCC5" Runat="server"></asp:dropdownlist>
			<asp:dropdownlist style="Z-INDEX: 0" id="lstForm" CssClass="inputStyleCC5" Runat="server"></asp:dropdownlist>
			<br>
			<asp:label id="lblInfo" runat="server" CssClass="text"></asp:label><br>
			<br>
			<br>
			Unitemized orders older then 5 hrs:&nbsp;<asp:DataGrid id="dgUnitemizedOrders" runat="server" CssClass="text" AllowSorting="True"></asp:DataGrid><br>
			Batches stuck for more then 5 hrs:&nbsp;<asp:DataGrid id="dgBatchStatus" runat="server" CssClass="text" AllowSorting="True"></asp:DataGrid><br>
			<br>
			<br>
			<br>
			<asp:hyperlink id="HyperLink1" runat="server" CssClass="text" NavigateUrl="Middle.aspx">Back</asp:hyperlink>
		</form>
	</body>
</HTML>
