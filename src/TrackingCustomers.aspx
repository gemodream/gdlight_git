﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" 
    CodeBehind="TrackingCustomers.aspx.cs" Inherits="Corpt.TrackingCustomers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        
        .GridPager a, .GridPager span
        {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }
        
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        
        .GridPager span
        {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }
        
        .divCol
        {
            float: left;
            width: 190px;
        }
        
        .divBottom
        {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        $().ready(function () {
        
            $('#<%=btnCreateDateSearch.ClientID%>').click(function () {
                return true;
            })
            $('#<%=txtCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();

            $('#<%=txtToCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();
        });

        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>

    <div class="demoarea">
        <h2 style="margin-left: 10px; margin-bottom: 10px; margin-top: 10px" class="demoheading">
            Customer Tracking
        </h2>
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">
                <div style="float: left; width: 600px;">
                    <div class="form-grouph" style="margin-bottom: 7px;">
                        <label class="control-label" for="customerCodeFilter">
                            Filter By Customer ID / Create Date:</label>
                        <div style="padding-bottom: 5px; display: block;width:800px;">
                            <input id="customerCodeFilter" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter Customer ID" maxlength="6" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"></span>
                            <input id="txtCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="From Date"
                                readonly />
                            <input id="txtToCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="To Date"
                                readonly />
                            <asp:Button ID="btnCreateDateSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCreateDateSearch_Click">
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;">
                            </span>
                            <input id="totalRecNumber" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Total # Found" maxlength="6" />
                            <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px;" OnClick="btnExportToExcel_Click" />
                        </div>
                        <div class="form-group" style="margin-bottom: 7px;">
                            <div id="mainContainer" class="container" style="padding-left: 0px;">
                                <div class="shadowBox">
                                    <div class="page-container">
                                        <div class="container" style="padding-left: 0px;">
                                            <div style="padding-bottom: 5px;">
                                                <span id="gvTitle" runat="server" class="text-info"></span>
                                            </div>
                                            <div>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="grdTrackingCustomers" runat="server" Width="1200" CssClass="table striped table-bordered"
                                                        AutoGenerateColumns="False" DataKeyNames="CompanyName" EmptyDataText="There are no data records to display."
                                                        AllowPaging="true" PageSize="20" RowStyle-Height="2px" OnPageIndexChanging="grdScreening_PageIndexChanging"
                                                        OnRowDeleting="grdTrackingCustomers_RowDeleting" OnSelectedIndexChanged="grdTrackingCustomers_SelectedIndexChanged"
                                                        AllowSorting="true" OnSorting="sort_table"
                                                        Height="200px">
                                                        <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                            Height="25px" />
                                                        <RowStyle Font-Size="12px" />
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                        <Columns>
                                                            <asp:BoundField DataField="BatchID" HeaderText="BatchID" HeaderStyle-Width="100px"
                                                                SortExpression="BatchID" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="GroupCode" HeaderText="GroupCode" HeaderStyle-Width="40px" ItemStyle-Wrap="true"
                                                                SortExpression="GroupCode" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="BatchCode" HeaderText="BatchCode" HeaderStyle-Width="60px"
                                                                SortExpression="BatchCode" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="MemoNumber" HeaderText="MemoNumber" HeaderStyle-Width="110px"
                                                                SortExpression="MemoNumber" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="TotalQty" HeaderText="Total Qty" HeaderStyle-Width="110px"
                                                                SortExpression="TotalQty" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="WorkType" HeaderText="Work Type" HeaderStyle-Width="110px"
                                                                SortExpression="WorkType" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="ItemizingDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Itemizing Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="ItemizingDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="OrderDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Order Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="OrderDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CustomerProgramName" HeaderText="SKU" HeaderStyle-Width="110px"
                                                                SortExpression="CustomerProgramName" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CompanyName" HeaderText="Company Name" HeaderStyle-Width="110px"
                                                                SortExpression="CompanyName" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="BilledDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Billed Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="BilledDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="ShippedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Shipped Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="ShippedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="80px"
                                                                SortExpression="Status" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-Width="30px"
                                                                SortExpression="Comments" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
