﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="BillingInfo.aspx.cs" Inherits="Corpt.BillingInfo" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
        <style>
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
    </style>
    <div class="demoarea">
    <div class="demoheading">Billing Info</div>
    <asp:Panel class="form-inline" runat="server" DefaultButton="LookupButton">
        <asp:Label ID="Label1" runat="server" Text="Order Number: "></asp:Label>
        <asp:TextBox runat="server" ID="OrderField" ></asp:TextBox>
        <asp:Button ID="LookupButton" runat="server" Text="Lookup" CssClass="btn btn-primary" OnClick="OnLookupClick" />
        <asp:Label runat="server" ID="MessageField" ForeColor="Red"></asp:Label>
    </asp:Panel>
    <br/>
    <!-- Item Numbers DataGrid-->
    <div>
        <asp:DataGrid runat="server" ID="InfoGrid" AutoGenerateColumns="False" CellPadding="5" CellSpacing="10" Style="font-size: small">
            <Columns>
                <asp:BoundColumn DataField="CurrItemNumber" HeaderText="New Item Number">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="OrigItemNumber" HeaderText="Orig Item Number">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="InvoiceNumber" HeaderText="Invoice Number">
                </asp:BoundColumn>               
                <asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
                <asp:BoundColumn DataField="BillingDate" HeaderText="Billing Date">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Description" HeaderText="Description">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Price" HeaderText="Price">
                </asp:BoundColumn>
            </Columns>

            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" 
                ForeColor="White" />
            
        </asp:DataGrid>
    </div>

    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="OrderField"
        Display="None" 
            ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required." 
            ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="OrderField"
        Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" 
            ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>" 
            ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
</div>
</asp:Content>
