<%@ Page language="c#" Codebehind="PostReport.aspx.cs" AutoEventWireup="True" Inherits="Corpt.PostReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PostReport</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/main.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:dropdownlist id="lstCustomerList" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 160px"
				runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstCustomerList_SelectedIndexChanged"></asp:dropdownlist><asp:button id="Button1" style="Z-INDEX: 116; LEFT: 136px; POSITION: absolute; TOP: 200px" runat="server"
				CssClass="buttonStyle" Text="." onclick="Button1_Click"></asp:button><asp:button id="cmdAddSelected" style="Z-INDEX: 115; LEFT: 328px; POSITION: absolute; TOP: 400px"
				runat="server" CssClass="buttonStyle" Text="Add Selected" Width="104px" onclick="cmdAddSelected_Click"></asp:button><asp:button id="cmdDelete" style="Z-INDEX: 114; LEFT: 680px; POSITION: absolute; TOP: 424px"
				runat="server" CssClass="buttonStyle" Text="Delete Selected" Width="104px" onclick="cmdDelete_Click"></asp:button><asp:button id="cmdAddAll" style="Z-INDEX: 113; LEFT: 328px; POSITION: absolute; TOP: 424px"
				runat="server" CssClass="buttonStyle" Text="Add All" Width="104px" onclick="cmdAddAll_Click"></asp:button><asp:button id="cmdClear" style="Z-INDEX: 112; LEFT: 680px; POSITION: absolute; TOP: 400px"
				runat="server" CssClass="buttonStyle" Text="Clear All" Width="104px" onclick="cmdClear_Click"></asp:button><asp:button id="cmdPost" style="Z-INDEX: 111; LEFT: 680px; POSITION: absolute; TOP: 488px" runat="server"
				CssClass="buttonStyle" Text="Post" Width="104px" onclick="cmdPost_Click"></asp:button><asp:label id="Label1" style="Z-INDEX: 110; LEFT: 504px; POSITION: absolute; TOP: 184px" runat="server"
				CssClass="text" Font-Bold="True">To Post</asp:label><asp:listbox id="lstToPost" style="Z-INDEX: 109; LEFT: 504px; POSITION: absolute; TOP: 200px"
				runat="server" CssClass="inputStyleCC5" Width="160px" SelectionMode="Multiple" Height="312px"></asp:listbox><asp:listbox id="lstFiles" style="Z-INDEX: 108; LEFT: 160px; POSITION: absolute; TOP: 200px"
				runat="server" CssClass="inputStyleCC5" Width="160px" SelectionMode="Multiple" Height="256px"></asp:listbox><asp:calendar id="calTo" style="Z-INDEX: 106; LEFT: 248px; POSITION: absolute; TOP: 24px" runat="server"
				CssClass="text" Width="224px" Height="120px" CellPadding="0" BorderColor="Black" BorderStyle="Solid" BackColor="WhiteSmoke">
				<DayStyle CssClass="text"></DayStyle>
				<DayHeaderStyle Font-Bold="True" CssClass="text"></DayHeaderStyle>
				<TitleStyle Font-Bold="True" CssClass="text"></TitleStyle>
			</asp:calendar><asp:calendar id="calFrom" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 24px" runat="server"
				CssClass="text" Width="224px" Height="120px" CellPadding="0" BorderColor="Black" BorderStyle="Solid" BackColor="WhiteSmoke">
				<DayStyle CssClass="text"></DayStyle>
				<DayHeaderStyle Font-Bold="True" CssClass="text"></DayHeaderStyle>
				<TitleStyle Font-Bold="True" CssClass="text"></TitleStyle>
			</asp:calendar><asp:label id="Label2" style="Z-INDEX: 104; LEFT: 288px; POSITION: absolute; TOP: 0px" runat="server"
				CssClass="text" Font-Bold="True">To</asp:label><asp:label id="Label3" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 0px" runat="server"
				CssClass="text" Font-Bold="True">From</asp:label><asp:button id="cmdListProgram" style="Z-INDEX: 101; LEFT: 272px; POSITION: absolute; TOP: 160px"
				runat="server" CssClass="buttonStyle" Text="." onclick="cmdListProgram_Click"></asp:button><asp:listbox id="orderList" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 200px" runat="server"
				CssClass="inputStyleCC5" AutoPostBack="True" Width="128px" Height="160px" onselectedindexchanged="orderList_SelectedIndexChanged"></asp:listbox></form>
	</body>
</HTML>
