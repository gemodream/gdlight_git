﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class BulkUpdateMeasureModel
    {
        public BulkUpdateMeasureModel()
        {
            Items = new List<BatchItemModel>();
            MeasureValues = new List<MeasureValueModel>();
        }

        public List<BatchItemModel> Items { get; set; }
        public List<int> Batches { get; set; }
        public int ItemTypeId { get; set; }
        public List<MeasureValueModel> MeasureValues { get; set; }
        public int PartId { get; set; }
        public MeasureModel Measure { get; set; }
        public int MeasureEnumValue { get; set; }
        public float? MeasureFloatValue { get; set; }
        public string MeasureTextValue { get; set; }
    }
}