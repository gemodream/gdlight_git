﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class IpModel
    {
        public IpModel() { }
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string City { get; set; }
    }
}