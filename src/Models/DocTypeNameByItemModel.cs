﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DocTypeNameByItemModel
    {
        public DocTypeNameByItemModel()
        {
        }

        public DocTypeNameByItemModel(DataRow dataRow)
        {
            ItemNumber = Convert.ToString(dataRow["ItemNumber"]);
            Reports = Convert.ToString(dataRow["Reports"]);
        }

        public string Reports { get; set; }

        public string ItemNumber { get; set; }
    }
}