﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureGradeItemModel
    {
        public MeasureGradeItemModel(SingleItemModel singleItemModel)
        {
            ItemModel = singleItemModel;
        }
        public SingleItemModel ItemModel { get; set; }
    }
}