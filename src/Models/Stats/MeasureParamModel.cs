﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class MeasureParamModel
    {
        public MeasureParamModel(DataRow row)
        {
            MeasureId = "" + row["MeasureID"];
            MeasureName = "" + row["MeasureName"];
            MeasureGroupId = "" + row["grp"];
            MeasureGroupName = "" + row["grp"];
        }
        public string MeasureId { get; set; }
        public string MeasureName { get; set; }

        public string MeasureGroupId { get; set; }
        public string MeasureGroupName { get; set; }
    }
}