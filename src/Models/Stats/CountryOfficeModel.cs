﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class CountryOfficeModel
    {
        public CountryOfficeModel() { }
        /// <summary>
        /// spCountryOffices.sql
        /// </summary>
        /// <param name="row"></param>
        public CountryOfficeModel(DataRow row)
        {
            ParentOfficeId = "" + row["ParentOfficeID"];
            Country = "" + row["Country"];
            Offices = new List<OfficeModel>();
        }
        public string ParentOfficeId { get; set; }
        public string Country { get; set; }
        public List<OfficeModel> Offices { get; set; }
    }
}