﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class SessionPageModel
    {
        public string activeFormat = "<li class='active'><span>{0}</span></li>";
        public string noActiveFormat = "";
        public string searchFormat = 
            "<li><a href='RemoteSession.aspx?" +
            StatsFilterModel.ParDateFrom +"={0}&" +
            StatsFilterModel.ParDateTo + "={1}&" +
            StatsFilterModel.ParUserId + "={2}&" +
            StatsFilterModel.ParPage + "={3}'>{4}</a></li>";

        public int PageNo { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrevious { get; set; }
        public bool IsNext { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string UserId { get; set; }

        public string DisplayNo
        {
            get
            {
                if (IsActive) return "" + PageNo;
                if (IsPrevious) return "Prev";
                if (IsNext) return "Next";
                return "" + PageNo;
            }
        }
        public string PageUrl
        {
            get
            {
                if (IsActive) return string.Format(activeFormat, PageNo);
                return string.Format(searchFormat, DateFrom, DateTo, UserId, PageNo, DisplayNo);
            }
        }
    }

}