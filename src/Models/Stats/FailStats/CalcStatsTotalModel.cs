﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Corpt.Utilities;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class CalcStatsTotalModel
    {
        public CalcStatsTotalModel() { }
        public CalcStatsTotalModel(DataRow row)
        {
            CustomerId = "" + row["customer_id"];
            CompanyName = "" + row["CompanyName"];
            CustomerName = "" + row["CustomerName"];
            StoneQty = Convert.ToInt32(row["stone_qty"]);
            PassQty = Convert.ToInt32(row["pass_qty"]);
            FailQty = Convert.ToInt32(row["fail_qty"]);
            MissQty = Convert.ToInt32(row["miss_qty"]);
        }
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string CustomerName { get; set; }
        public int StoneQty { get; set; }

        #region Passed
        public int PassQty { get; set; }
        public float PassPercentNum
        {
            get
            {
                return StatsUtils.CalcPercentNum(StoneQty, PassQty);
            }
        }
        public string PassPercent
        {
            get 
            {
                return StatsUtils.CalcPercent(StoneQty, PassQty);
            }
        }
        public string DisplayPassQty
        {
            get
            {
                var f = "<table><tr><td title='Qty'>{0}</td><td title='%'>{1}</td></tr></table>";
                if (StoneQty == 0) return "";
                return string.Format(f, PassQty, PassPercent);
            }
        }
        #endregion

        #region Failed
        public int FailQty {get;set;}
        public float FailPercentNum
        {
            get
            {
                return StatsUtils.CalcPercentNum(StoneQty, FailQty);
            }
        }

        public string FailPercent
        {
            get
            {
                return StatsUtils.CalcPercent(StoneQty, FailQty);
            }
        }
        public string FailDescr 
        {
            get
            {
                return GetRejectionDescr(TotalFail);
            }
        }
        #endregion

        #region Missing
        public int MissQty { get; set; }
        public string MissPercent
        {
            get
            {
                return StatsUtils.CalcPercent(StoneQty, MissQty);
            }
        }
        public float MissPercentNum
        {
            get
            {
                return StatsUtils.CalcPercentNum(StoneQty, MissQty);
            }
        }

        
        public string MissDescr 
        {
            get
            {
                return GetRejectionDescr(TotalMiss);
            }
        }
        #endregion

        private static string GetRejectionDescr(List<CalcStatsTotalReasonModel> items)
        {
            var desc = "";
            var rFormat = "<tr> <td>{0}: </td><td style='padding-left: 10px'>{1}</td></tr> ";
                var reasons = items.GroupBy(m => m.ReasonName);
                foreach (var reason in reasons)
                {
                    var qty = items.FindAll(m => m.ReasonName == reason.Key).Sum(m => m.Qty);
                    if (qty > 0)
                    {
                        desc += string.Format(rFormat, reason.Key, qty);
                    }
                }
                if (string.IsNullOrEmpty(desc)) return "";
                return string.Format("<table>{0}</table>", desc);
        }
        public List<CalcStatsTotalReasonModel> TotalFail { get; set; }
        public List<CalcStatsTotalReasonModel> TotalMiss { get; set; }
        public static CalcStatsTotalModel Create(DataRow row)
        {
            return new CalcStatsTotalModel { 
                StoneQty = Convert.ToInt32(row["stone_qty"]),
                PassQty = Convert.ToInt32(row["pass_qty"]),
                FailQty = Convert.ToInt32(row["fail_qty"]),
                MissQty = Convert.ToInt32(row["miss_qty"])
            };
        }
    }
}