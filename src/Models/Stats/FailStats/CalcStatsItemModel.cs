﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class CalcStatsItemModel
    {
        public CalcStatsItemModel(DataRow row)
        {
            ItemId = "" + row["item_id"];
            OrderCode = "" + row["order_code"];
            BatchCode = "" + row["batch_code"];
            ItemCode = "" + row["item_code"];
            CreateDate = Convert.ToDateTime(row["item_CreateDate"]);
            CpName = "" + row["cp_name"];
            CustomerId = "" + row["customer_id"];
            ItemValues = new List<CalcStatsItemValueModel>();
        }
        public string ItemId { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public DateTime CreateDate { get; set; }
        public string CpName { get; set; }
        public string CustomerId { get; set; }

        public List<CalcStatsItemValueModel> ItemValues { get; set; }
        public bool HasFailed { get; set; }
        public bool HasMissing { get; set; }
        public bool IsRejected
        {
            get { return HasFailed || HasMissing; }
        }
        public string RejectionDescr 
        { 
            get
            {
                var aggrDesc = ItemValues.Select(i => i.RejectionDesc).Aggregate((i, j) => i + " " + j).Trim();
                return (aggrDesc == "" ? "" : string.Format("<table>{0}</table>", aggrDesc));
            } 
        }
        public string RejectionExcDescr
        {
            get
            {
                var items = ItemValues.FindAll(i => i.RejectionExcDesc.Length > 0);
                if (items.Count == 0) return "";
                var aggrDesc = items.Select(i => i.RejectionExcDesc).Aggregate((i, j) => i + "; " + j).Trim();
                return aggrDesc;
            }
        }
        public string FullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);

        public string FullItemNumber =>
            Utils.FillToFiveChars(OrderCode) + 
            Utils.FillToThreeChars(BatchCode, OrderCode) + 
            Utils.FillToTwoChars(ItemCode);
    }
}