﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class StatsReportDetailsModel
    {
        public StatsReportDetailsModel(DataRow row)
        {
            DateFrom = Convert.ToDateTime(row["date_from"]);
            DateTo = Convert.ToDateTime(row["date_to"]);
            Comments = "" + row["comments"];

        }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Comments { get; set; }
        public List<string> Customers { get; set; }
        public List<ReasonMeasureModel> Rules { get; set; }
        public string DisplayPeriod
        {
            get { return DisplayDateFrom + " " + DisplayDateTo; }
        }
        public string DisplayDateFrom
        {
            get
            {
                return DateFrom.ToString("d");
            }
        }
        public string DisplayDateTo
        {
            get
            {
                return DateTo.ToString("d");
            }
        }
        public string DisplayCustomers
        {
            get
            {
                if (Customers.Count == 0) return "All";
                return Customers.Aggregate((i, j) => i + ", " + j);
            }
        }
        public string DisplayRules
        {
            get 
            {
                var desc = "";
                var rFormat = "<tr><td title='Reason'>{0}: </td><td title='Measures'>{1}</td></tr>";
                var tFormat = "<table>{0}</table>";
                var reasons = Rules.GroupBy(m => m.ReasonName).ToList();
                foreach (var reason in reasons)
                {
                    desc += string.Format(rFormat, reason.Key, Rules.FindAll(m => m.ReasonName == reason.Key).Select(i => i.MeasureName).Aggregate((i, j) => i + ", " + j));
                }
                if (desc.Length > 0) 
                {
                    desc = string.Format(tFormat, desc);
                }
                return desc;
            }
        }
    }
}