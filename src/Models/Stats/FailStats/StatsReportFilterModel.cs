﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class StatsReportFilterModel
    {
        public StatsReportFilterModel() { }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
    }
}