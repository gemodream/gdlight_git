﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    public class CalcStatsItemValueModel
    {
        public CalcStatsItemValueModel(DataRow row)
        {
            ItemId = "" + row["item_id"];
            MinValue = "" + row["minValue"];
            MaxValue = "" + row["maxValue"];
            CurrValue = "" + row["currValue"];
            RejectionType = Convert.ToInt16(row["rejection_type"]);
            PartName = "" + row["PartName"];
            MeasureName = "" + row["MeasureName"];
            ReasonName = "" + row["ReasonName"];
        }
        public string ColumnTable { get; set; }
        public string ItemId { get; set; }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public string CurrValue { get; set; }
        public int RejectionType { get; set; }
        public string ReasonName { get; set; }
        public string PartName { get; set; }
        public string MeasureName { get; set; }
        public bool IsFailed { get { return RejectionType == 2; } }
        public bool IsMissing { get { return RejectionType == 3; } }
        public string RejectionDesc
        {
            get
            {
                if (RejectionType == 1) return "";
                if (RejectionType == 2)
                {
                    return string.Format(DescFailFormat, PartName, MeasureName, MinValue, MaxValue, CurrValue);
                }
                if (RejectionType == 3)
                {
                    return string.Format(DescMissFormat, PartName, MeasureName, MinValue, MaxValue);
                }
                return "";
            }
        }
        public string RejectionExcDesc
        {
            get
            {
                if (RejectionType == 1) return "";
                if (RejectionType == 2)
                {
                    return string.Format(DescFailExcFormat, PartName, MeasureName, MinValue, MaxValue, CurrValue);
                }
                if (RejectionType == 3)
                {
                    return string.Format(DescMissExcFormat, PartName, MeasureName, MinValue, MaxValue);
                }
                return "";
            }
        }
        // Diamond: Color Grade-Fail, Current Grade: Reject, Min. grade: F
        // Type -> PartName -> MeasureName -> MinValue -> MaxValue -> CurrValue
        private static string DescFailFormat = 
             "<tr> <td> <span class='text_highlitedFail'>Fail</span></td> <td>{0} -> {1}</td> <td title='Min'>{2}</td> <td title='Max'>{3}</td> <td title='Curr'>{4}</td> </tr>";
        private static string DescFailExcFormat =
             "Fail: {0} -> {1}, Min={2}, Max={3}, Curr={4}";
        private static string DescMissFormat =
            "<tr> <td> <span class='text_highlitedMiss'>Miss</span></td> <td>{0} -> {1}</td> <td title='Min'>{2}</td> <td title='Max'>{3}</td> <td title='Curr'></td> </tr>";
        private static string DescMissExcFormat =
             "Miss: {0} -> {1}, Min={2}, Max={3}";

    }
}