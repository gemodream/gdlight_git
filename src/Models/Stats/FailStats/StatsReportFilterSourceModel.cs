﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class StatsReportFilterSourceModel
    {
        public StatsReportFilterSourceModel() 
        {
            CustomerPrograms = new List<string>();
            CustomerPrograms.Add("Any");
            RejectionTypes = RejectionTypeModel.GetTypes();
            ItemNumberSelected = "";
        }
        public int ItemsCount { get; set; }
        public string RejectionTypeSelected { get; set; }
        public string CpSelected { get; set; }
        public string ItemNumberSelected { get; set; }
        public List<string> CustomerPrograms { get; set; }
        public List<string> ItemNumbers { get; set; }
        public List<ReportFilterReasonModel> Reasons { get; set; }
        public List<RejectionTypeModel> RejectionTypes { get; set; }
        private static string CpFormat = " and i.cp_name = '{0}'";
        private static string OrderFormat = " and i.order_code = {0}";
        private static string BatchFormat = " and i.batch_code = {0}";
        private static string ItemFormat = " and i.item_code = {0}";
        public string ItemCriteria
        {
            get
            {
                var criteria = "";
                //-- Customer program
                if (!string.IsNullOrEmpty(CpSelected))
                {
                    criteria += string.Format(CpFormat, CpSelected);
                }

                //-- Order/Batch/Item number
                criteria += ItemNumberCriteria;
                return criteria;
            }
        }
        public string ItemNumberCriteria
        {
            get 
            {
                var number = ItemNumberSelected;
                if (number.Length == 0) return "";
                var criteria = "";
                if (number.Length >= 5)
                {
                    criteria += string.Format(OrderFormat, Utils.ParseOrderCode(number));
                }
                if (number.Length >= 8)
                {
                    criteria += string.Format(BatchFormat, Utils.ParseBatchCode(number));
                }
                if (number.Length >= 10)
                {
                    criteria += string.Format(ItemFormat, Utils.ParseItemCode(number));
                }
                return criteria;
            }
        }
    }
}