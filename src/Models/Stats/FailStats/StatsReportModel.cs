﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class StatsReportModel
    {
        public StatsReportModel(DataRow row)
        {
            ReportId = "" + row["report_id"];
            CalcDate = Convert.ToDateTime(row["calc_date"]);
            if (!string.IsNullOrEmpty("" + row["calc_end"]))
            {
                CalcEnd = Convert.ToDateTime(row["calc_end"]);
            }
            else
            {
                CalcEnd = null;
            }
            UserName = "" + row["user_name"];
            OrderStateCode = "" + row["order_state_code"];
            ReportDetails = new StatsReportDetailsModel(row);
        }
        public string ReportId { get; set; }
        public string UserName { get; set; }
        public DateTime CalcDate { get; set; }
        public DateTime? CalcEnd { get; set; }
        public string OrderStateCode {get;set;}
        public string ExecutionTime 
        {
            get
            {
                if (CalcEnd == null) return "";
                TimeSpan diff = (DateTime)CalcEnd - CalcDate;
                return diff.ToString(@"hh\:mm\:ss");
            }
        }
        public string ReportTitle
        {
            get { return CalcDate.ToString("G") + " (" + UserName + ")"; }
        }
        public StatsReportDetailsModel ReportDetails { get; set; }
    }
}