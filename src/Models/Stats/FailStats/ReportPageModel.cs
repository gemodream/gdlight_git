﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class ReportPageModel
    {
        public ReportPageModel() { }
        public DataTable ReportPageTable { get; set; }
        public PaginationModel Pagination { get; set; }
    }
}