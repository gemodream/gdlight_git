﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class OrderStateModel
    {
        public OrderStateModel() {
            OrderStateCode = null;
            OrderStateName = "Any";
        }
        public OrderStateModel(DataRow row)
        {
            OrderStateCode = "" + row["StateCode"];
            OrderStateName = "" + row["StateName"];
        }
        public string OrderStateCode { get; set; }
        public string OrderStateName { get; set; }
    }
}