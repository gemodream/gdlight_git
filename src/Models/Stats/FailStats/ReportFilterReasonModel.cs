﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class ReportFilterReasonModel
    {
        public ReportFilterReasonModel() { }
        public ReportFilterReasonModel(DataRow row)
        {
            ReasonId = "" + row["reason_id"];
            ReasonName = "" + row["ReasonName"];
            Values = new List<string>();
            Values.Add("Any");
        }
        public string ReasonId { get; set; }
        public string ReasonName { get; set; }
        public string ValueSelected { get; set; }
        public List<string> Values { get; set; }
        public void AddValue(DataRow row)
        {
            var value =( "" + row["currValue"]).Trim();
            if (value == "") value = "None";
            Values.Add("" + row["currValue"]);
        }
    }
}