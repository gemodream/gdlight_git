﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class RejectionTypeModel
    {
        public RejectionTypeModel() { }
        public string RejectionTypeId { get; set; }
        public string RejectionTypeName { get; set; }
        public static List<RejectionTypeModel> GetTypes()
        {
            var types = new List<RejectionTypeModel>();
            types.Add(new RejectionTypeModel { RejectionTypeId = "", RejectionTypeName = "Any" });
            //types.Add(new RejectionTypeModel { RejectionTypeId = "1", RejectionTypeName = "Passed" });
            types.Add(new RejectionTypeModel { RejectionTypeId = "2", RejectionTypeName = "Failed" });
            types.Add(new RejectionTypeModel { RejectionTypeId = "3", RejectionTypeName = "Missing" });
            return types;
        }
    }
}