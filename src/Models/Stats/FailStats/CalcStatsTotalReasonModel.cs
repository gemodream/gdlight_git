﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class CalcStatsTotalReasonModel
    {
        public CalcStatsTotalReasonModel() { }
        public string ReasonId { get; set; }
        public string ReasonName { get; set; }
        public string CustomerId { get; set; }
        public int Qty { get; set; }
        public static CalcStatsTotalReasonModel CreateFail(DataRow row)
        {
            return new CalcStatsTotalReasonModel 
            { 
                ReasonId = "" + row["reason_id"],
                ReasonName = "" + row["ReasonName"],
                Qty = Convert.ToInt32(row["fail_qty"]),
                CustomerId = "" + row["customer_id"],

            };
        }
        public static CalcStatsTotalReasonModel CreateFailTotal(DataRow row)
        {
            return new CalcStatsTotalReasonModel
            {
                ReasonId = "" + row["reason_id"],
                ReasonName = "" + row["ReasonName"],
                Qty = Convert.ToInt32(row["fail_qty"])

            };
        }
        public static CalcStatsTotalReasonModel CreateMiss(DataRow row)
        {
            return new CalcStatsTotalReasonModel
            {
                ReasonId = "" + row["reason_id"],
                ReasonName = "" + row["ReasonName"],
                Qty = Convert.ToInt32(row["miss_qty"]),
                CustomerId = "" + row["customer_id"],

            };
        }
        public static CalcStatsTotalReasonModel CreateMissTotal(DataRow row)
        {
            return new CalcStatsTotalReasonModel
            {
                ReasonId = "" + row["reason_id"],
                ReasonName = "" + row["ReasonName"],
                Qty = Convert.ToInt32(row["miss_qty"])
            };
        }
    }
}