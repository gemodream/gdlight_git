﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats.FailStats
{
    [Serializable]
    public class PaginationModel
    {
        public PaginationModel() { }
        public PaginationModel(DataRow row)
        {
            RowCount = "" + row["RowCountByCriteria"];
            PageCount = "" + row["PageCountByCriteria"];
            CurrPage = "" + row["PageNo"];
            PageSize = "" + row["PageSize"];
        }
        public string RowCount { get; set; }
        public string PageCount { get; set; }
        public string CurrPage { get; set; }
        public string PageSize { get; set; }
    }
}