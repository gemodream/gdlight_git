﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class PageModel
    {
        public string activeFormat = "<li class='active'><span>{0}</span></li>";
        public string noActiveFormat = "";
        public string searchFormat = 
            "<li><a href='StatsStoneNumbers.aspx?" +
            StatsFilterModel.ParPeriodType + "={0}&" +
            StatsFilterModel.ParDateFrom +"={1}&" +
            StatsFilterModel.ParDateTo + "={2}&" +
            StatsFilterModel.ParCustomerId + "={3}&" +
            StatsFilterModel.ParOfficeId + "={4}&" + 
            StatsFilterModel.ParPage + "={5}'>{6}</a></li>";

        public int PageNo { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrevious { get; set; }
        public bool IsNext { get; set; }
        public string PeriodType { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string CustomerId { get; set; }
        public string ParentOfficeId { get; set; }

        public string DisplayNo
        {
            get
            {
                if (IsActive) return "" + PageNo;
                if (IsPrevious) return "Prev";
                if (IsNext) return "Next";
                return "" + PageNo;
            }
        }
        public string PageUrl
        {
            get
            {
                if (IsActive) return string.Format(activeFormat, PageNo);
                return string.Format(searchFormat, PeriodType, DateFrom, DateTo, CustomerId, ParentOfficeId, PageNo, DisplayNo);
            }
        }
    }
}