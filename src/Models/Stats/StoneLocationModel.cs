﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    public class StoneLocationModel
    {
        public StoneLocationModel(DataRow row)
        {
            Quentity = Convert.ToInt32(row["cnt"]);
            Country = "" + row["Country"];
            OfficeCode = "" + row["OfficeCode"];
            OfficeId = "" + row["officeId"];

        }
        public int Quentity {get;set;}
        public string Country { get; set; }
        public string OfficeCode { get; set; }
        public string OfficeId { get; set; }
    }
}