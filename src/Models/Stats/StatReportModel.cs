﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StatReportModel
    {
        public StatReportModel() { }
        public StatReportModel(DataRow row)
        {
            Quantity = Convert.ToInt32(row["quantity"]);
            CustomerId = "" + row["CustomerID"];
            CustomerName = "" + row["CustomerName"];
            Description = "" + row["details"];
            Description = Description.Replace(" Report", "");

        }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int Quantity { get; set; }
        public string DocumentTypeName { get; set; }
        public string Description { get; set; }
        public static StatReportModel Create(DataRow row)
        {
            var model = new StatReportModel { Quantity = Convert.ToInt32(row["quantity"]), DocumentTypeName = "" + row["DocumentTypeName"] };
            return model;
        }
    }
}