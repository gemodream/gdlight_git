﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StoneCustomerModel
    {
        public StoneCustomerModel(DataRow row)
        {
            Quantity = Convert.ToInt32(row["cnt"]);
            CustomerId = "" + row["CustomerID"];
            CustomerName = "" + row["CustomerName"];
            CompanyName = "" + row["CompanyName"];
        }
        public string CompanyName { get; set; }
        public int Quantity { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
    }
}