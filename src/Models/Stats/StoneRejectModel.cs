﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StoneRejectModel
    {
        public StoneRejectModel(StoneCustomerModel customerModel, List<StoneRejectDetailsModel> details) 
        {
            CustomerId = customerModel.CustomerId;
            CustomerName = customerModel.CustomerName;
            Quantity = customerModel.Quantity;
            Details = details;
            var groups = details.GroupBy(m => m.OrderCode + ";" + m.BatchCode + ";" + m.ItemCode ).ToList();
            InvalidQuantity = groups.Count;
        }
        public StoneRejectModel(DataRow row)
        {

            Quantity = Convert.ToInt32("" + row["quantity"]);
            InvalidQuantity = Convert.ToInt32("" + row["invalid"]);    
            CustomerName = "" + row["CustomerName"];
            CustomerId = "" + row["CustomerId"];
            Details = new List<StoneRejectDetailsModel>();
        }
        public int Quantity { get; set; }
        public int InvalidQuantity { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string RejectRate
        {
            get
            {
                if (Quantity == 0) return "0";
                return ((float)(InvalidQuantity) / Quantity).ToString("0.000%"); ;
            }
        }
        public List<StoneRejectDetailsModel> Details {get;set;}
    }
}