﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StatsFilterModel
    {
        public StatsFilterModel() {
            MeasureIds = new List<string>();
            ReasonIds = new List<string>();
        }
        public DateTime? DateFrom { get; set;}
        public DateTime? DateTo { get; set;}
        public string CustomerId { get; set;}
        public List<string> CustomerIds { get; set; }
        public string OfficeId { get; set;}
        public string UserId { get; set; }
        public int PageNo { get; set; }
        public string PeriodType { get; set; }
        public string RejectionType { get; set; }
        public bool Valid 
        { 
            get { return DateFrom != null && DateTo != null; } 
        }
        public List<string> OfficesIds { get; set; }
        public List<string> MeasureIds { get; set; }
        public List<string> ReasonIds { get; set; }
        public string OrderStateCode { get; set; }
        #region URL Parameter Names
        public static string ParPeriodType = "pt";
        public static string ParDateFrom = "df";
        public static string ParDateTo = "dt";
        public static string ParCustomerId = "cId";
        public static string ParOfficeId = "oId";
        public static string ParUserId = "uId";
        public static string ParPage = "page";
        #endregion
    }
}