﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class ReasonMeasureModel
    {
        public ReasonMeasureModel() { }
        public ReasonMeasureModel(DataRow row)
        {
            Id = "" + row["ID"];
            MeasureId = "" + row["MeasureID"];
            MeasureName = "" + row["MeasureName"];
            ReasonId = "" + row["ReasonID"];
            ReasonName = "" + row["ReasonName"];
        }
        public string Id { get; set; }
        public string MeasureId { get; set; }
        public string MeasureName { get; set; }
        public string ReasonId { get; set; }
        public string ReasonName { get; set; }
        public static ReasonMeasureModel CreateShort(DataRow row)
        {
            return new ReasonMeasureModel { MeasureName = "" + row["MeasureName"], ReasonName = "" + row["ReasonName"] };
        }
    }
}