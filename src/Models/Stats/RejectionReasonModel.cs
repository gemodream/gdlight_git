﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class RejectionReasonModel
    {
        public RejectionReasonModel()
        {
            ReasonName = "";
            ReasonId = "";
        }
        public RejectionReasonModel(DataRow row)
        {
            ReasonId = "" + row["ReasonID"];
            ReasonName = "" + row["ReasonName"];
        }
        public string ReasonId { get; set; }
        public string ReasonName { get; set; }
    }
}