﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class PartTypeModel
    {
        public PartTypeModel(DataRow row)
        {
            PartTypeId = "" + row["PartTypeID"];
            PartTypeName = "" + row["PartTypeName"];
        }
        public string PartTypeId { get; set; }
        public string PartTypeName { get; set; }
    }
}