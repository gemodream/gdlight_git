﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class OfficeModel
    {
        public OfficeModel()
        {
        }

        public OfficeModel(DataRow row)
        {
            OfficeId = "" + row["OfficeID"];
            ParentOfficeId = "" + row["ParentOfficeID"];
            OfficeCode = "" + row["OfficeCode"];
            OfficeAddress = "" + row["OfficeAddress"];
        }

        public string OfficeId { get; set; }
        public string ParentOfficeId { get; set; }
        public string OfficeCode { get; set; }
        public string OfficeAddress { get; set; }
        public string DisplayOffice
        {
            get { return OfficeCode; }
        }

    }
}