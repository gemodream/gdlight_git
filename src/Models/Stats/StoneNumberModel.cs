﻿using System;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StoneNumberModel
    {
        /// <summary>
        /// spStatStoneNumbers.sql
        /// </summary>
        /// <param name="r"></param>
        public StoneNumberModel(DataRow r)
        {
            ItemCode = "" + r["ItemCode"];
            OrderCode = "" + r["OrderCode"];
            BatchCode = "" + r["BatchCode"];
            CreateDate = Convert.ToDateTime(r["CreateDate"]);
            CustomerName = "" + r["CustomerName"];
        }
        public DateTime CreateDate { get; set; }
        public string ItemCode { get; set; }
        public string BatchCode { get; set; }
        public string OrderCode { get; set; }
        public string CustomerName { get; set; }
        public string FullItemNumber =>
            Utils.FillToFiveChars(OrderCode) + Utils.FillToThreeChars(BatchCode, OrderCode) +
            Utils.FillToTwoChars(ItemCode);

        public string FullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);
    }
}