﻿using System;
using System.Data;

namespace Corpt.Models.Stats
{
    [Serializable]
    public class StoneRejectDetailsModel
    {
        public StoneRejectDetailsModel(DataRow row, string customerId, string companyName)
        {
            CustomerId = customerId;
            CompanyName = companyName;
            OrderCode = "" + row["OrderCode"];
            BatchCode = "" + row["BatchCode"];
            ItemCode = "" + row["ItemCode"];
            Message = "" + row["StateMsg"];
            //if (Message.Contains("Fail")) RejectionType = "Fail";
            //if (Message.Contains("Missing")) RejectionType = "Missing";
            CreateDate = Convert.ToDateTime(row["CreateDate"]);
            var msgs = Message.Split(';');
            //Result: Reason;PartName;MeasureName;CurrentValue;MinValue;MaxValue;TypeRejection(Fail/Missing/Pass);CPName
            if (msgs.Length == 8)
            {
                ReasonName = msgs[0].Length > 0 ? msgs[0] : msgs[1] + " -> " + msgs[2];
                ValueCurrent = msgs[3];
                ValueMin = msgs[4];
                ValueMax = msgs[5];
                RejectionType = msgs[6];
                CustomerProgramName = msgs[7];
            }
        }
        public string CompanyName { get; set; }
        public string CustomerProgramName { get; set; }
        public string ValueCurrent { get; set; }
        public string ValueMin { get; set; }
        public string ValueMax { get; set; }
        public string ReasonName { get; set; }
        public string CustomerId { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public DateTime CreateDate { get; set; }
        public string DateDisplay => CreateDate.ToString("MM/dd/yyyy");
        public string Message { get; set; }
        public string RejectionType { get; set; }
        public string FullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);
    }
}