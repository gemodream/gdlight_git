﻿using System;
using System.Data;
using System.Web;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingDetModel : TrackingExModel
    {
        public TrackingDetModel(DataRow row, TrackDetailsFilterModel filterModel)
        {
            MemoNumber = string.IsNullOrEmpty("" + row["MemoNumber"]) ? "" : "" + row["MemoNumber"];
            OrderCode = string.IsNullOrEmpty("" + row["OrderCode"]) ? "" : "" + row["OrderCode"];
            CpName = "" + row["CustomerProgramName"];
            CpName = string.IsNullOrEmpty("" + row["CustomerProgramName"]) ? "" : "" + row["CustomerProgramName"];
            if (row["BatchID"] != null)
            {
                BatchId = Int32.Parse("" + row["BatchID"]);
            }
            OrderBatch = string.IsNullOrEmpty("" + row["Order_Batch"]) ? "" : "" + row["Order_Batch"];
            if (row["LastModifiedDate"] != null)
            {
                LastModifiedDate = DateTime.Parse("" + row["LastModifiedDate"]);
            }

            //-- For jump on Tracking Details
            CustomerOffice = filterModel.CustomerId + "_" + filterModel.OfficeId;
            DateFrom = filterModel.DateFrom;
            DateTo = filterModel.DateTo;

        }

        public DateTime LastModifiedDate { get; set; }
        public int BatchId { get; set; }
        public string OrderBatch { get; set; }
        public override string OrderDisplay
        {
            get 
            { 
                return "<a href=\"TrackingDetails.aspx" +
                    "?p1=" + SessionConstants.TrackDetailsModeOrder + 
                    "&p2=" + HttpUtility.UrlEncode(OrderCode) +
                    "&p3=" + CustomerOffice +
                    "&p4=" + DateFrom +
                    "&p5=" + DateTo + "\">" +
                    OrderCode + "</a>";
            }
        }
        public string BatchDisplay
        {
            get
            {
                return "<a href=\"TrackingBatchHistory.aspx?p1=" + BatchId + "\">" + OrderBatch + "</a>";
            }
        }
    }
}