﻿using System;
using System.Collections.Generic;
using Corpt.Utilities;

namespace Corpt.Models
{
    public class PreFillingComparer : IComparer<PreFillingValueModel>
    {
        public int Compare(PreFillingValueModel x, PreFillingValueModel y)
        {
            var result = ComparerUtils.NumberCompare(Convert.ToInt32(x.PartId), Convert.ToInt32(y.PartId));
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(x.MeasureTitle, y.MeasureTitle);
            }
            return result;
        }
    }
}