﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CpRuleModel
    {
        public CpRuleModel()
        {
        }

        public CpRuleModel(DataRow row)
        {
            PartName = "" + row["PartName"];
            MeasureName = "" + row["MeasureName"];
            MinName = "" + row["MinName"];
            MaxName = "" + row["MaxName"];
            Ranges = "" + row["Ranges"];
            CpDocId = "" + row["CPDocID"];
        }
        public string PartName { get; set; }
        public string MeasureName { get; set; }
        public string MinName { get; set; }
        public string MaxName { get; set; }
        public string Ranges { get; set; }
        public string CpDocId { get; set; }
        public string Name
        {
            get
            {
                return CleanPartName(PartName);
            }
        }
        public static String CleanPartName(String partName)
        {
            if ((partName.ToLower().IndexOf("container", StringComparison.Ordinal) != -1))
                return "Item";
            return (partName.ToLower().IndexOf("Body", StringComparison.Ordinal) != -1) ? "Metal" : partName;
        }
    }
}