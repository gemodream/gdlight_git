﻿using System;
using System.Data;
using System.Globalization;

namespace Corpt.Models
{
    [Serializable]
    public class QueueModel
    {
        public QueueModel(DataRow row)
        {
            OperationChar = "" + row["OperationChar"];
            OldItemCode = ("" + row["OldItemCode"]).Substring(OperationChar.Length);
            DocumentTypeName = "" + row["DocumentTypeName"];
            State = "" + row["State"];
            OrderCode = "" + row["GroupCode"];
            BatchNumber = "" + row["BatchCode"];
            ItemCode = "" + row["ItemCode"];    //-- ItemCode = NewItem
            NewItem = "" + row["NewItem"];
            BatchId = Convert.ToInt32("" + row["BatchID"]);
            var printDate = "" + row["PrintDate"];
            if (!string.IsNullOrEmpty(printDate) && !printDate.Equals("-"))
            {
                PrintDate = DateTime.Parse(printDate, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
            }
            
        }

        public string OperationChar { get; set; }
        public string DocumentTypeName { get; set; }
        public string State { get; set; }
        public string OrderCode { get; set; }
        public string BatchNumber { get; set; }
        public string ItemCode { get; set; }
        public string NewItem { get; set; }
        public DateTime PrintDate { get; set; }
        public int BatchId { get; set; }
        public string OldItemCode { get; set; }

    }
}