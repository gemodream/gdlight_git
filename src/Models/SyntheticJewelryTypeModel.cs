﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticJewelryTypeModel
	{
		public SyntheticJewelryTypeModel() { }

		public SyntheticJewelryTypeModel(DataRow row)
		{
			JewelryTypeName = "" + row["JewelryTypeName"];
			JewelryTypeID = "" + row["JewelryTypeID"];
		}
		public string JewelryTypeID { get; set; }
		public string JewelryTypeName { get; set; }
		public string expireDate { get; set; }
	}
}
