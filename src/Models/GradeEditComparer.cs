﻿using System;
using System.Collections.Generic;
using Corpt.Utilities;

namespace Corpt.Models
{
    public class GradeEditComparer : IComparer<GradeEditModel>
    {
        public int Compare(GradeEditModel g1, GradeEditModel g2)
        {

            var result = ComparerUtils.NumberCompare(Convert.ToInt32(g1.PartId), Convert.ToInt32(g2.PartId));
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(g1.MeasureTitle, g2.MeasureTitle);
            }
            return result;
        }
    }
}