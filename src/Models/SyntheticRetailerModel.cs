﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticRetailerModel
	{
		public SyntheticRetailerModel() { }
		public SyntheticRetailerModel(DataRow row)
		{
			retailerName = "" + row["retailerName"];
			customerName = "" + row["customerName"];
			customerCode = "" + row["customerCode"];
			retailerId = "" + row["retailerId"];
		}
		public string retailerId { get; set; }
		public string retailerName { get; set; }
		public string ServiceTypeCodeSet { get; set; }
		public string customerName { get; set; }
		public string customerCode { get; set; }
		public string expireDate { get; set; }


	}
}