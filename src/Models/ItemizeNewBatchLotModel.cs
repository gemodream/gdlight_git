﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemizeNewBatchLotModel
    {
        public ItemizeNewBatchLotModel(DataRow row)
        {
            GroupCode = "" + row["GroupCode"];
            BatchCode = "" + row["BatchCode"];
            ItemCode = "" + row["ItemCode"];
            LotNumber = "" + row["LotNumber"];
            BatchID = "" + row["BatchID"];
        }
        public string GroupCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public string LotNumber { get; set; }
        public string BatchID { get; set; }
    }
}