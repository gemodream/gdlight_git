﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DocumentTypeModel
    {
        public DocumentTypeModel() { }
        public DocumentTypeModel(DataRow row) 
        {
            DocumentTypeName = "" + row["DocumentTypeName"];
            DocumentOperationChar = "" + row["DocumentOperationChar"];
            DocumentTypeCode = "" + row["DocumentTypeCode"];
            Order = Convert.ToInt32("" + row["nOrder"]);
        }
        public string DocumentTypeName { get; set; }
        public string DocumentOperationChar { get; set; }
        public string DocumentTypeCode { get; set; }
        public int Order {get;set;}
    }
}