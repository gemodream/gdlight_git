﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.SyntheticScreening
{
	[Serializable]
	public class SyntheticDataViewModel
	{
		public SyntheticDataViewModel() { }

        public int CustomerCode { get; set; }
		public int RetailerID { get; set; }
		public string MemoNum { get; set; }
		public string PONum { get; set; }
		public string SKUName { get; set; }
		public string StyleName { get; set; }
		public int Status { get; set; }
		public int CertifiedBy { get; set; }
		public int ServiceTypeCode { get; set; }
		public int CategoryCode { get; set; }
		public int ServiceTimeCode { get; set; }
		public int DeliveryMethodCode { get; set; }
        public int CarrierCode { get; set; }
		public int InstrumentCode { get; set; }
		public int JewelryTypeCode { get; set; }
		public string CreateDate { get; set; }
	}
}