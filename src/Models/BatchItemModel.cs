﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class BatchItemModel
    {
        public BatchItemModel()
        {
        }

        public int BatchId { get; set; }
        public int ItemCode { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
    }
}