﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MovedItemModel : ItemModel
    {
        public MovedItemModel(DataRow r) : base(r)
        {
            NewBatchCode    = "" + r["newBatchCode"];
            NewGroupCode    = "" + r["newGroupCode"];
            NewItemCode     = "" + r["newItemCode"];
            if (r.Table.Columns.Contains("BatchID"))
            {
                BatchId = "" + r["BatchID"];
            }
        }
        public string Order => Utils.FillToFiveChars(OrderCode);
        public string NewOrder => Utils.FillToFiveChars(NewGroupCode);
        public string NewGroupCode { get; set; }
        public string NewBatchCode { get; set; }
        public string NewItemCode { get; set; }
        public string NewFullItemNumber =>
            Utils.FillToFiveChars(NewGroupCode) + Utils.FillToThreeChars(NewBatchCode, NewGroupCode) +
            Utils.FillToTwoChars(NewItemCode);

        public string NewFullItemNumberWithDotes =>
            Utils.FillToFiveChars(NewGroupCode) + "." +
            Utils.FillToThreeChars(NewBatchCode, NewGroupCode) + "." +
            Utils.FillToTwoChars(NewItemCode);
    }
}