﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PartModelMulti
    {
        public PartModelMulti() { }
        public PartModelMulti(DataRow row)
        {
            PartId = Int32.Parse("" + row["PartID"]);
            PartName = "" + row["PartName"];
            PartTypeId = Int32.Parse("" + row["PartTypeID"]);
        }
        public string PartName { get; set; }
        public int PartId { get; set; }
        public int PartTypeId { get; set; }
    }
}