﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemFailedModel
    {
        /* sp_RulesTracking24, Tables[1] */
        public ItemFailedModel(DataRow row)
        {
            ItemNumberWithDotes = "" + row["ItemNumber"];
            OldItemNumberWithDotes = "" + row["OldItemNumber"];
            PartName = "" + row["PartName"];
            MeasureName = "" + row["MeasureName"];
            RealValue = "" + row["RealValue"];
        }

        public string RealValue { get; set; }
        public bool IsGood
        {
            get { return RealValue == "Indented Natural GR" || RealValue == "Open GR"; }    
        }
        public string ItemNumberWithDotes { get; set; }
        public string OldItemNumberWithDotes { get; set; }
        public string PartName { get; set; }
        public string MeasureName { get; set; }
        public string ItemNumber
        {
            get
            {
                return ItemNumberWithDotes.Replace(".", "");
            }
        }
        public string OldItemNumber {
            get
            {
                return OldItemNumberWithDotes.Replace(".", "");
            }
        }
    }
}