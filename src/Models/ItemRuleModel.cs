﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemRuleModel
    {
        public const string StatusMigrate = "Migrated to new #";
        public const string StatusFailed = "Failed";
        public const string StatusOk = "OK to Print";
        /// <summary>
        /// sp_RulesTracking24, Tables[0]
        /// </summary>
        /// <param name="row"></param>
        public ItemRuleModel(DataRow row)
        {
            ItemNumber = "" + row["ItemNumber"];
            NewItemNumber = "" + row["NewItemNumber"];
            OldItemNumber = "" + row["OldItemNumber"];
            RulesNumber   = "" + row["RulesNumber"];
            Status = "" + row["Status"];
        }
        public string ItemNumber { get; set; }
        public string NewItemNumber { get; set; }
        public string OldItemNumber { get; set; }
        public string Status { get; set; }
        public string RulesNumber { get; set; }
        public bool IsMigrate
        {
            get { return Status == StatusMigrate; }
        }

        public bool IsFailed
        {
            get { return Status.EndsWith(StatusFailed); }
        }
        public bool IsOk
        {
            get { return Status.IndexOf(StatusOk, StringComparison.Ordinal) != -1; }
        }
    }
}