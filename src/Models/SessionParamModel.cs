﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class SessionParamModel
    {
        public SessionParamModel(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }
        public string MapFile { get; set; }
    }
}