﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SortingStatModel
    {
        /// <summary>
        /// sp_SortingStat
        /// </summary>
        /// <param name="row"></param>
        public SortingStatModel(DataRow row)
        {
            OrderDate = Convert.ToDateTime(row["OrderDate"]);
            CompanyName = Convert.ToString(row["companyname"]);
            CustomerCode = Convert.ToString(row["customercode"]);
            OrderCode = Convert.ToString(row["OrderCode"]);
            BatchCode = Convert.ToString(row["BatchCode"]);
            CpName = Convert.ToString(row["customerprogramname"]);
            TotalWeight = float.Parse("" + row["TotalWeight"]);
            FailedColClar = float.Parse("" + row["FailedColClar"]);
            FailedPolSym = float.Parse("" + row["FailedPolSym"]);
            FailedCutGrade = float.Parse("" + row["FailedCutGrade"]);
            TotalPass = float.Parse("" + row["TotalPass"]);
            TotalFailed = float.Parse("" + row["TotalFalied"]);
            CheckColumn = float.Parse("" + row["CheckColumn"]);
        }

        public DateTime OrderDate { get; set; }
        public string CompanyName { get; set; }
        public string CustomerCode { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string CpName { get; set; }
        public float TotalWeight { get; set; }
        public float FailedColClar { get; set; }
        public float FailedPolSym { get; set; }
        public float FailedCutGrade { get; set; }
        public float TotalPass { get; set; }
        public float TotalFailed { get; set; }
        public float CheckColumn { get; set; }
    }
}