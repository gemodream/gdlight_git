﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureAccessModel
    {
        public MeasureAccessModel(){}
        public int AccessId { get; set; }
        public int AccessCode { get; set; }
        public string AccessName { get; set; }
        public static readonly MeasureAccessModel AccessColor = new MeasureAccessModel { AccessId = 3, AccessCode = 5, AccessName = "Color" };
        public static readonly MeasureAccessModel AccessClarity = new MeasureAccessModel { AccessId = 4, AccessCode = 4, AccessName = "Clarity" };
        public static readonly MeasureAccessModel AccessMeasure = new MeasureAccessModel { AccessId = 5, AccessCode = 6, AccessName = "Measure" };
    }
}