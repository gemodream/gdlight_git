﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DeliveryLogModel
    {
        public DeliveryLogModel()
        {
        }

        /// <summary>
        /// trkGetItemsOut
        /// </summary>
        /// <param name="row"></param>
        public DeliveryLogModel(DataRow row)
        {
            FullItemNumber = Convert.ToString(row["FullItemNumber"]);
            EventName = Convert.ToString(row["EventName"]);
            RecordId = Convert.ToInt32(row["TrackingRecordID"]);
            RecordTimeStamp = Convert.ToDateTime(row["RecordTimeStamp"]);
        }
        public string EventName { get; set; }
        public string FullItemNumber { get; set; }
        public DateTime RecordTimeStamp { get; set; }
        public int RecordId{get; set; }
        public string FullBatchNumber { get; set; }
        
        /// <summary>
        /// trkSpGetDeliveryLog
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static DeliveryLogModel Create1(DataRow row)
        {
            return new DeliveryLogModel
                {
                    FullBatchNumber = Utils.FullBatchNumber("" + row["GroupCode"], "" + row["BatchCode"]),
                    EventName = "" + row["EventName"],
                    RecordTimeStamp = Convert.ToDateTime(row["RecordTimeStamp"]),
                    FullItemNumber = Convert.ToString(row["FullItemNumber"])
                };
        }
        public string DisplayFullItemNumber
        {
            get
            {
                return "<a href=\"DeliveryLogBatch.aspx?ItemNumber=" + FullItemNumber.Trim() + "\">" +
                    FullItemNumber + "</a>";
            }
        }

    }
}