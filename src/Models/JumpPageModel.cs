﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class JumpPageModel
    {
        public JumpPageModel(){}
        public string MeasureValue { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public string PartId { get; set; }
        public string MeasureId { get; set; }
        
        public string FullBatchNumber => Utils.FullBatchNumber(OrderCode, BatchCode);

        public string FullItemNumber => Utils.FullItemNumber(OrderCode, BatchCode, ItemCode);

        public string FullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);

        public string JumpGradeII => "<a href=\"GradeII.aspx?BatchNumber=" + FullBatchNumber + "&ItemNumber=" + FullItemNumber + "\">GradeII " + FullBatchNumber + "</a>";

        public string JumpBulkUpdate => "<a href=\"BulkUpdate2.aspx?OrderCode=" + OrderCode + "&BatchCode=" + BatchCode + "\">BulkUpdate " + OrderCode + "</a>";

        public string JumpFullOrder => "<a href=\"ShortReportII.aspx?OrderCode=" + OrderCode + "\">Full Order " + OrderCode + "</a>";

        public string JumpColor =>
            "<a href=\"Grade1.aspx?Mode=Color" +
            "&BatchNumber=" + FullBatchNumber + 
            "&ItemNumber=" + FullItemNumber + 
            "&PartId=" + PartId +
            "&MeasureId=" + MeasureId + 
            "\">Color " + FullItemNumber + "</a>";

        public string JumpClarity =>
            "<a href=\"Grade1.aspx?Mode=Clarity" +
            "&BatchNumber=" + FullBatchNumber +
            "&ItemNumber=" + FullItemNumber +
            "&PartId=" + PartId +
            "&MeasureId=" + MeasureId + 
            "\">Clarity " + FullItemNumber + "</a>";

        public string JumpMeasure =>
            "<a href=\"Grade1.aspx?Mode=Measure" +
            "&BatchNumber=" + FullBatchNumber +
            "&ItemNumber=" + FullItemNumber +
            "&PartId=" + PartId +
            "&MeasureId=" + MeasureId + 
            "\">Measure " + FullItemNumber + "</a>";

        public static JumpPageModel Create(List<BatchModel> batches, ItemValueModel item)
        {
            var batch = batches.Find(m => m.BatchId == item.BatchId);
            return new JumpPageModel()
                {
                    MeasureValue =  item.ResultValue,
                    OrderCode = batch == null ? "" : batch.GroupCode,
                    BatchCode = batch == null ? "" : batch.BatchCode,
                    ItemCode = item.NewItemCode,
                    PartId =  item.PartId,
                    MeasureId = item.MeasureId
                };
        }
    }
}