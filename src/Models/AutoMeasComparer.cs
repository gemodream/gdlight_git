﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Corpt.Utilities;

namespace Corpt.Models
{
    public class AutoMeasComparer : IComparer<AutoMeasModel>
    {
        public int Compare(AutoMeasModel x, AutoMeasModel y)
        {
            var result = ComparerUtils.NumberCompare(Convert.ToInt32(x.PartId), Convert.ToInt32(y.PartId));
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(x.MeasureName, y.MeasureName);
            }
            return result;

        }
    }
}