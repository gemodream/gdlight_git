﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CpRuleLookupModel
    {
        public CpRuleLookupModel()
        {
            CpRuleList = new List<CpRuleModel>();
            DocList = new List<DocModel>();
        }
        /// <summary>
        /// from sp_GetCPRules (Lookup by Batch number)
        /// </summary>
        /// <param name="row"></param>
        public CpRuleLookupModel(DataRow row)
        {
            Customer = new CustomerModel
            {
                CustomerId = "" + row["customerid"],
                CustomerName = "" + row["customername"],
                OfficeId = "" + row["customerofficeid"]
            };
            Cp = new CustomerProgramModel
            {
                Comment     = "" + row["comment"],
                Description = "" + row["description"],
                CustomerProgramName = "" + row["customerprogramname"],
                Path2Picture = "" + row["Path2Picture"]
            };
            CpRuleList = new List<CpRuleModel>();
            DocList = new List<DocModel>();
        }
        public string DisplayValue
        {
            get
            {
                return BySku ? 
                    string.Format("{0}; {1}; {2}", Cp.CustomerProgramName, Customer.CustomerName, Cp.CustomerStyle) :
                    string.Format("{0}; {1}; {2}", Cp.CustomerStyle, Customer.CustomerName, Cp.CustomerProgramName);
            }
        }
        public string Id
        {
            get { return Cp.CpOfficeIdAndCpId; } 
        }
        public bool BySku { get; set; }
        public List<DocModel> DocList { get; set; }
        public List<CpRuleModel> CpRuleList { get; set; }
        public CustomerModel Customer { get; set; }
        public CustomerProgramModel Cp { get; set; }
        /// <summary>
        ///  from sp_GetCPRules (Lookup by Batch number)
        /// </summary>
        /// <param name="row"></param>
        public void AddCpRule(DataRow row)
        {
            var cpRule = new CpRuleModel
            {
                PartName = "" + row["PartName"],      
                MeasureName = "" + row["MeasureName"],
                MinName = "" + row["MinName"],
                MaxName = "" + row["MaxName"],
                Ranges = "" + row["Ranges"],
                CpDocId =  "" + row["CPDocID"]        
            };
            CpRuleList.Add(cpRule);
        }
        /// <summary>
        /// from cursor sp_GetCPRulesByCPOfficeID_CPID
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CpRuleLookupModel CreateByStyleOrSku(DataRow row, bool bySku)
        {
            
            var model = new CpRuleLookupModel
            {
                Customer = new CustomerModel
                {
                    CustomerId = "" + row["customerid"],
                    CustomerName = "" + row["customername"],
                    OfficeId = "" + row["customerofficeid"]
                },
                Cp = new CustomerProgramModel
                {
                    CustomerProgramName = "" + row["customerprogramname"],
                    CustomerStyle = "" + row["customerstyle"],
                    CpOfficeIdAndCpId = "" + row["CPOfficeID_CPID"],
                    CpId = "" + row["cpid"],
                    ExtData = false
                },
                BySku = bySku
            };
            return model;
        }
    }
}