﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class LabelModel
    {
        public LabelModel()
        {
        }

        /// <summary>
        /// GetNumbers
        /// </summary>
        /// <param name="row"></param>
        public LabelModel(DataRow row, string prefix)
        {
            CombinedNumber = prefix + Convert.ToString(row["LastNumber"]);
            BarCode = "*" + prefix + Convert.ToString(row["LastNumber"] + "*");
            Column1 = " . ";
            Column2 = " . ";
            Column3 = " . ";
            Column4 = " . ";
        }

        public string BarCode { get; set; }
        public string CombinedNumber { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string NewBatchNumber { get; set; }
        public string OldBatchNumber { get; set; }
        
        /// <summary>
        /// sp_GetBatchForLabels
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static LabelModel Create1(DataRow row)
        {
            var label = new LabelModel();
            label.NewBatchNumber = Utils.FullItemNumber(Convert.ToString(row["OrderCode"]), Convert.ToString(row["BatchCode"]), Convert.ToString(row["ItemCode"]));
            label.OldBatchNumber = Utils.FullItemNumber(Convert.ToString(row["PrevOrderCode"]), Convert.ToString(row["PrevBatchCode"]), Convert.ToString(row["PrevItemCode"]));
            label.BarCode = label.NewBatchNumber;// "*" + label.NewBatchNumber + "*";
            if (label.NewBatchNumber == label.OldBatchNumber)
            {
                label.CombinedNumber = label.NewBatchNumber;
            }
            else
            {
                label.CombinedNumber = label.NewBatchNumber + "(" + label.OldBatchNumber + ")";
            }
            label.Column1 = "Wt: ";
            label.Column2 = Utils.NullValue(row["Weight"]);
            label.Column3 = row["CustomerProgramName"].ToString();
            label.Column4 = "";

            return label;
        }
    }
}