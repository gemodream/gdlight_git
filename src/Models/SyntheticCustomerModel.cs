﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticCustomerModel
	{
		public SyntheticCustomerModel() { }
		public SyntheticCustomerModel(DataRow row)
		{
			customerName = "" + row["CustomerName"];
			customerCode = "" + row["CustomerCode"];
			customerId = "" + row["CustomerID"];
			customerOfficeId = "" + row["CustomerOfficeID"];
		}
		public string retailerId { get; set; }
		public string ServiceTypeCodeSet { get; set; }
		public string customerName { get; set; }
		public string customerCode { get; set; }
		public string customerId { get; set; }
		public string customerOfficeId { get; set; }
		public string expireDate { get; set; }
		public List<CustomerProgramModel> CPList { get; set; }
		public string serviceTypeName { get; set; }


	}
}