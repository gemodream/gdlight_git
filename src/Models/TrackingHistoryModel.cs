﻿using System;
using System.Data;
using System.Globalization;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingHistoryModel : TrackingModel
    {
        public TrackingHistoryModel(DataRow row)
        {
            CompanyName = "" + row["CompanyName"];
            GroupCode = "" + row["GroupCode"];
            BatchCode = "" + row["BatchCode"];
            try
            {
                CreateDate = Convert.ToDateTime(row["RecordTimeStamp"]);
            } catch (Exception x)
            {
                var msg = x.Message;
            }
            AgeInHrs = string.IsNullOrEmpty("" + row["ageInHrs"]) ? 0 : Int32.Parse("" + row["ageInHrs"]);
            EventName = "" + row["EventName"];
            ViewAccessName = "" + row["VewAccessName"];
        }
        public string EventName { get; set; }
        public string ViewAccessName { get; set; }
        public string BatchCode { get; set; }
        public string FullBatchNumber
        {
            get
            {
                return Utils.FullBatchNumber(GroupCode, BatchCode);
            }
        }
        public string BatchDisplay
        {
            get
            {
                return "<a href='Tracking2Details.aspx?BatchNumber=" + FullBatchNumber + "' target=\"_blank\" >" + FullBatchNumber + "</a>";
            }
        }
    }
}