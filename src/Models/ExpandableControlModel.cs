﻿using Corpt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;

namespace Corpt.Utilities
{
	public class ExpandableControlModel
	{
		//Properties
		//Name of Element, used to differentiate elements by name
		public string ElemName { get; set; }
		public string ElemType { get; set; }//Filter, Standard, Clarity(needed manual buttons), TextList, or Picture
		public string ElemMeasure { get; set; }//MeasureValueMeasureId

		//"Small" refers to elements in Small (or collapsed) panel
		//"Big refers to elements in Big (or expanded) panel

		//Small Panel
		public bool SmVis { get; set; }//Sets whether the small panel display is visible or not
		public string SmDivStyle { get; set; }//Style of small div

		//Small Label
		public string SmLbl { get; set; }//Sets text of small label
		public bool SmLblVis { get; set; }//Sets visibility of small label

		//Small TextBox
		public string SmTxt { get; set; }//Sets text of small text box
		public bool SmTxtRO { get; set; }//Sets readonly property of small text box
		public bool SmTxtVis { get; set; }//Sets visible property of small text box

		//Small Button
		public string SmBtnStyle { get; set; }//Sets Style of small button
		public string SmBtn { get; set; }//Sets Text of small button
		public bool SmBtnVis { get; set; }//Sets visible property of small button

		//Big Panel
		public bool BigVis { get; set; }//Sets visibility of Big Panel
		public string BigDivStyle { get; set; }

		//DropDown Panel
		public bool DropDownPanelVis { get; set; }//Sets visibility of dropdown panel in Big Panel

		//Big Label
		public string BigLbl { get; set; }//Text
		public bool BigLblVis { get; set; }

		//Big TextBox
		public string BigTxt { get; set; }//Text
		public bool BigTxtVis { get; set; }

		//DropDown List
		public List<EnumMeasureModel> DropDownEnums { get; set; }

		//TextBox Repeater
		public List<TextBoxRepeaterModel> TxtRepeaterSource { get; set; }
		public bool TxtRepeaterVis { get; set; }

		/* Within the object of TxtRepeaterSource
		public string RepeatLbl{get; set;}//Label
		public string RepeatTxt{get; set;}//Textbox
		*/

		//Button Repeater
		public List<ButtonDetailModel> BtnRepeaterSource { get; set; }
		public bool BtnRepeaterVis { get; set; }

		/* Within the object of BtnRepeaterSource
		public string BigBtn{get; set;}//Text on button
		public string BigBtnStyle{get; set;}//Style of Big Button
		public string BtnValue{get; set;}//MeasureValueID hidden in Button
		public string BtnLink{get; set;}//Subcategory/Category to go to upon hitting button
		*/

		//Picture Panel
		public string PicDivStyle { get; set; }
		public bool PicVis { get; set; }
		public string PicLblTxt { get; set; }
		public bool PicImgVis { get; set; }
		public string PicErrLblTxt { get; set; }
		public bool PicErrLblVis { get; set; }

		//Constructors
		public static ExpandableControlModel CreateNew(XmlNode node)
		{
			string test = node.SelectSingleNode("ElemMeasure").InnerText;

			return new ExpandableControlModel
			{
				ElemName = node.SelectSingleNode("ElemName").InnerText,
				ElemType = node.SelectSingleNode("ElemType").InnerText,
				ElemMeasure = node.SelectSingleNode("ElemMeasure").InnerText,
				SmVis = node.SelectSingleNode("SmVis").InnerText == "true" ? true : false,
				SmDivStyle = node.SelectSingleNode("SmDivStyle").InnerText,
				SmLbl = node.SelectSingleNode("SmLbl").InnerText,
				SmLblVis = node.SelectSingleNode("SmLblVis").InnerText == "true" ? true : false,
				SmTxt = node.SelectSingleNode("SmTxt").InnerText,
				SmTxtRO = node.SelectSingleNode("SmTxtRO").InnerText == "true" ? true : false,
				SmTxtVis = node.SelectSingleNode("SmTxtVis").InnerText == "true" ? true : false,
				SmBtnStyle = node.SelectSingleNode("SmBtnStyle").InnerText,
				SmBtn = node.SelectSingleNode("SmBtn").InnerText,
				SmBtnVis = node.SelectSingleNode("SmBtnVis").InnerText == "true" ? true : false,
				BigVis = node.SelectSingleNode("BigVis").InnerText == "true" ? true : false,
				BigDivStyle = node.SelectSingleNode("BigDivStyle").InnerText,
				DropDownPanelVis = node.SelectSingleNode("DropDownPanelVis").InnerText == "true" ? true : false,
				BigLbl = node.SelectSingleNode("BigLbl").InnerText,
				BigLblVis = node.SelectSingleNode("BigLblVis").InnerText == "true" ? true : false,
				BigTxt = node.SelectSingleNode("BigTxt").InnerText,
				BigTxtVis = node.SelectSingleNode("BigTxtVis").InnerText == "true" ? true : false,
				BtnRepeaterVis = node.SelectSingleNode("BtnRepeaterVis").InnerText == "true" ? true : false,
				PicDivStyle = node.SelectSingleNode("PicDivStyle").InnerText,
				PicVis = node.SelectSingleNode("PicVis").InnerText == "true" ? true : false,
				PicLblTxt = node.SelectSingleNode("PicLblTxt").InnerText,
				PicImgVis = node.SelectSingleNode("PicImgVis").InnerText == "true" ? true : false,
				PicErrLblTxt = node.SelectSingleNode("PicErrLblTxt").InnerText,
				PicErrLblVis = node.SelectSingleNode("PicErrLblVis").InnerText == "true" ? true : false
			};

			/*
			return new ExpandableControlModel
			{
				ElemName = "" + row["ElemName"],
				ElemType = "" + row["ElemType"],
				ElemMeasure = "" + row["ElemMeasure"],
				SmVis = "" + row["SmVis"] == "true" ? true : false,
				SmDivStyle = "" + row["SmDivStyle"],
				SmLbl = "" + row["SmLbl"],
				SmLblVis = "" + row["SmLblVis"] == "true" ? true : false,
				SmTxt = "" + row["SmTxt"],
				SmTxtRO = "" + row["SmTxtRO"] == "true" ? true : false,
				SmTxtVis = "" + row["SmTxtVis"] == "true" ? true : false,
				SmBtnStyle = "" + row["SmBtnStyle"],
				SmBtn = "" + row["SmBtn"],
				BigVis = "" + row["BigVis"] == "true" ? true : false,
				BigDivStyle = "" + row["BigDivStyle"],
				DropDownPanelVis = "" + row["DropDownPanelVis"] == "true" ? true : false,
				BigLbl = "" + row["BigLbl"],
				BigLblVis = "" + row["BigLblVis"] == "true" ? true : false,
				BigTxt = "" + row["BigTxt"],
				BigTxtVis = "" + row["BigTxtVis"] == "true" ? true : false,
				BtnRepeaterVis = "" + row["BtnRepeaterVis"] == "true" ? true : false,
				PicDivStyle = "" + row["PicDivStyle"],
				PicVis = "" + row["PicVis"] == "true" ? true : false,
				PicLblTxt = "" + row["PicLblTxt"],
				PicImgVis = "" + row["PicImgVis"] == "true" ? true : false,
				PicErrLblTxt = "" + row["PicErrLblTxt"],
				PicErrLblVis = "" + row["PicErrLblVis"] == "true" ? true : false
			};
			*/
		}
	}
}