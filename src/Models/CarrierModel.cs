﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CarrierModel
    {
        private const string UrlFedex = "http://www.fedex.com/Tracking?language=english&cntry_code=us&tracknumbers=";
        private const string UrlUps = "http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=";
        private const string UrlUsPs = "http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=";   
        public CarrierModel(DataRow row)
        {
            TrackingNumber = ("" + row["CarrierTrackingNumber"]) == "Scan Package Bar-Code Here"
                ? "N/A" : ("" + row["CarrierTrackingNumber"]).Trim();
            CarrierCode = Int32.Parse("" + row["carriercode"]);
        }
        public int CarrierCode { get; set; }
        public string TrackingNumber { get; set; }
        public string CarrierDisplay
        {
            get
            {
                switch (CarrierCode)
                {
                    case 1:
                        {
                            if (TrackingNumber.Length == 16)
                                return "FedEx with a tracking number <a href=\"" + 
                                    UrlFedex + TrackingNumber.Substring(0, 12) + "&track.x=0&track.y=0\">" + TrackingNumber.Substring(0, 12) + "</a>";
                            if (TrackingNumber.Length == 32)
                                return "FedEx with a tracking number <a href=\"" + 
                                    UrlFedex + TrackingNumber.Substring(16, 12) + "&track.x=0&track.y=0\">" + TrackingNumber.Substring(16, 12) + "</a>";
                            if (TrackingNumber == "N/A")
                                return "FedEx";
                            return "";
                        }
                    case 2:
                        {
                            if (TrackingNumber != "N/A")
                            {
                                return "UPS with a tracking number <a href=\"" +
                                    UrlUps + TrackingNumber + "&track.x=0&track.y=0\">" + TrackingNumber + "</a>";
                            }
                            return "UPS";
                        }
                    case 3:
                        {
                            if (TrackingNumber != "N/A")
                            {
                                return "USPS with a tracking number <a href=\"" + UrlUsPs + TrackingNumber + "\">" + TrackingNumber + "</a>";
                            }
                            return "USPS";
                        }
                    case 4:
                        {
                            if (TrackingNumber != "N/A")
                                return "Brinks with a traking number " + TrackingNumber;
                            return "Brinks";
                        }
                    case 5:
                        {
                            if (TrackingNumber != "N/A")
                                return "MalcaAmit with a tracking number " + TrackingNumber;
                            return "MalcaAmit";
                        }
                    case 6:
                        {
                            return "GSI messenger.";
                        }
                    case 7:
                        {
                            if (TrackingNumber != "N/A")
                                return "Dunbar with a tracking number " + TrackingNumber;
                            return "Dunbar";
                        }
                    case 8:
                        {
                            if (TrackingNumber != "N/A")
                                return "K n S Armed Courier inc with a tracking number " + TrackingNumber;
                            return "K n S Armed Courier inc";
                        }
                    case 9:
                        {
                            return (TrackingNumber == "N/A" ?
                                "Parcel Pro" : "Parcel Pro with a tracking number " + TrackingNumber);
                        }
                    case 10:
                        {
                            return (TrackingNumber == "N/A" ?
                                "DHL" : "DHL with a tracking number " + TrackingNumber);
                        }
                    case 11:
                        {
                            return (TrackingNumber == "N/A" ?
                                "BAX" : "BAX with a tracking number " + TrackingNumber);
                        }
                    case -1:
                        {
                            return "messenger " + TrackingNumber;
                        }
                    case -2:
                        {
                            return "GSI messenger.";
                        }
                }
                return "";
                
            }
        }
    }
}