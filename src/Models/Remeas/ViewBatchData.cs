﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.Remeas
{
    [Serializable]
    public class ViewBatchData
    {
        public ViewBatchData()
        {
            Items = new List<SingleItemModel>();
        }

        public string BatchNumber { get; set; }
        public List<SingleItemModel> Items { get; set; }
        public CustomerProgramModel Cp { get; set; }
    }
}