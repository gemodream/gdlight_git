﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.Remeas
{
    [Serializable]
    public class ViewMeasures
    {
        public ViewMeasures()
        {
            Measures = new List<MeasureModel>();
        }
        public int ItemTypeId { get; set; }
        public List<MeasureModel> Measures { get; set; }
    }
}