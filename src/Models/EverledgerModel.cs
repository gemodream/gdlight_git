﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO.Ports;
using Newtonsoft.Json;
using Corpt.Constants;
using NPOI.POIFS.Properties;
using Corpt.Models.Customer;

namespace Corpt.Models
{
    [Serializable]
    public class EverledgerAPIInputModel
    {
        public EverledgerAPIInputModel()
        {
        }

        /// <summary>
        /// spGetEverledgerDataByItemNumber
        /// </summary>
        /// <param name="row"></param>
        public EverledgerAPIInputModel(DataRow row)
        {
            ItemNumber = "" + row["ItemNumber"];
            Certifier = "" + row["Certifier"];
            EverledgerURL = "" + row["EverledgerURL"];
            ShortCode = "" + row["ShortCode"];
            Brand = "" + row["Brand"];
            RangeID = "" + row["RangeID"];
            ManufacturerID = "" + row["ManufacturerID"];
            ManufacturerDescription = "" + row["ManufacturerDescription"];
            SKUNo = "" + row["SKUNo"];           
            LightPerformanceData = new EverledgerLightPerformanceDataModel("" + row["BrillianceValue"], "" + row["FireValue"], "" + row["ScintillationValue"]);
            Color = "" + row["Color"];
            Clarity = "" + row["Clarity"];
            Weight = "" + row["Weight"];
            Cut = "" + row["Cut"];
        }
               
        public string ItemNumber { get; set; }               
        public string Certifier { get; set; }
        public string EverledgerURL { get; set; }
        public string ShortCode { get; set; }
        public string Brand { get; set; }
        public string RangeID { get; set; }
        public string ManufacturerID { get; set; }
        public string ManufacturerDescription { get; set; }      
        public string SKUNo { get; set; }
        public EverledgerLightPerformanceDataModel LightPerformanceData { get; set; }
        public string Color { get; set; }
        public string Clarity { get; set; }
        public string Weight { get; set; }
        public string Cut { get; set; }
    }


    [Serializable]    
    public class EverledgerLightPerformanceDataModel
    {
        public EverledgerLightPerformanceDataModel(string brilliance, string fire, string scintillation)
        {
            BrillianceValue = brilliance;
            FireValue = fire;
            ScintillationValue = scintillation;
        }

        public string BrillianceValue { get; set; }
        public string FireValue { get; set; }
        public string ScintillationValue { get; set; }       
    }

    [Serializable]
    public class EverledgerAPIOutputModel
    {
        public EverledgerAPIOutputModel()
        {
        }
        public string ItemNumber { get; set; }
        public string APIErrorMessage { get; set; }
        public string APIStatus { get; set; }
        public string URL { get; set; }
        public string ShortCode { get; set; }
    }

    [Serializable]
    public class EverledgerAPIStatusModel
    {
        public EverledgerAPIStatusModel()
        {
        }
        public string ItemNumber { get; set; }               
        public string CreateItemStatus { get; set; }
        public string AssociateDiamondStatus { get; set; }
        public string URLGenStatus { get; set; }
        public string AddGradingStatus { get; set; }       
        public string URL { get; set; }
        public string ShortCode { get; set; }
        public string APIStatus { get; set; }

        public string DBSaveURL{ get; set; }

    }

    
}