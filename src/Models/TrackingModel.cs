﻿using System;
using System.Data;
using System.Globalization;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingModel
    {
        public TrackingModel()
        {
        }
        public TrackingModel(DataRow row)
        {
            CompanyName = "" + row["CompanyName"];
            GroupCode = "" + row["GroupCode"];
            CreateDate = Convert.ToDateTime(row["CreateDate"]);
            AgeInHrs = string.IsNullOrEmpty("" + row["ageInHrs"]) ? 0 : Int32.Parse("" + row["ageInHrs"]);
            //DateTime.Parse(PersonBirthDate.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
        }
        public string CompanyName { get; set; }
        public string GroupCode { get; set; }
        public DateTime CreateDate { get; set; }
        public int AgeInHrs { get; set; }
    }
}