﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureValueModel
    {
        public MeasureValueModel()
        {
        }
        /// <summary>
        /// spGetPartValue
        /// </summary>
        /// <param name="row"></param>
        /// <param name="forItemModel"></param>
        public MeasureValueModel(DataRow row, SingleItemModel forItemModel)
        {
            BatchId = Int32.Parse("" + row["BatchID"]);
            MeasureCode = "" + row["MeasureCode"];
            MeasureClass = Convert.ToInt32(row["MeasureClass"]);
            ItemCode = "" + row["ItemCode"];
            PartId = Convert.ToInt32(row["PartID"]);
            MeasureId = "" + row["MeasureID"];
            MeasureValueId = "" + row["MeasureValueID"];
            MeasureValue = "" + row["MeasureValue"];
            StringValue = "" + row["StringValue"];
            ShapePath2Drawing = "" + row["ShapePath2Drawing"];
            EnumValueTitle = "" + row["ValueTitle"];
            MeasureValueName = "" + row["MeasureValueName"];
            ForItem = forItemModel;
        }
        /*IvanB start 12/09*/
        public MeasureValueModel(DataRow row)
        {
            MeasureValueId = "" + row["MeasureValueID"];
            EnumValueTitle = "" + row["ValueTitle"];
            MeasureValueName = "" + row["MeasureValueName"];
        }
        /*IvanB end 12/09*/
        public MeasureValueModel(AutoMeasModel valueModel, BatchItemModel itemModel)
        {
            PartId = Convert.ToInt32(valueModel.PartId);
            MeasureId = valueModel.MeasureId;
            MeasureCode = valueModel.MeasureCode;
            MeasureClass = valueModel.MeasureClass;
            if (valueModel.IsEnum)
            {
                MeasureValueId = valueModel.RandValue;
            }
            if (valueModel.IsNumeric)
            {
                MeasureValue = valueModel.RandValue;
            }
            BatchId = itemModel.BatchId;
            ItemCode = "" + itemModel.ItemCode;
        }
        public MeasureValueModel(GradeEditModel source)
        {
            PartId = Convert.ToInt32(source.PartId);
            MeasureCode = source.MeasureCode;
            MeasureClass = source.MeasureClass;
            MeasureId = source.MeasureId;
            MeasureValue = source.ValueNumbCurr;        //-- Numeric
            StringValue = source.ValueTextCurr;         //-- Text
            MeasureValueId = source.ValueEnumIdCurr;    //-- Enum

        }
        public MeasureValueModel(MeasureValueCpModel srcModel)
        {
            PartId = Convert.ToInt32(srcModel.PartId);
            MeasureCode = srcModel.MeasureCode;
        }
        public MeasureValueModel(ItemValueEditModel src, SingleItemModel itemModel)
        {
            BatchId = itemModel.NewBatchId;
            ItemCode = itemModel.NewItemCode;
            PartId = Convert.ToInt32(src.PartId);
            MeasureClass = src.MeasureClass;
            MeasureId = "" + src.MeasureId;
            if (MeasureClass == MeasureModel.MeasureClassEnum)
            {
                MeasureValueId = src.Value;
                MeasureValue = null;
                StringValue = null;
            }
            if (MeasureClass == MeasureModel.MeasureClassText)
            {
                MeasureValueId = null;
                MeasureValue = null;
                StringValue = src.Value;
            }
            if (MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                MeasureValueId = null;
                MeasureValue = src.Value;
                StringValue = null;
            }
        }
        public MeasureValueModel(ItemValueEditModel src)
        {
            PartId = Convert.ToInt32(src.PartId);
            MeasureClass = src.MeasureClass;
            MeasureId = "" + src.MeasureId;
            if (MeasureClass == MeasureModel.MeasureClassEnum)
            {
                MeasureValueId = src.Value;
            }
            if (MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                MeasureValue = src.Value;
            }
            if (MeasureClass == MeasureModel.MeasureClassText)
            {
                StringValue = src.Value;
            }
        }
        public string EnumValueTitle { get; set; }
        public string EnumValueTitleMax { get; set; }
        public string MeasureCode { get; set; }
        public string MeasureId { get; set; }
        public int PartId { get; set; }
        public string MeasureValueId { get; set; }
        public string MeasureValueIdMax { get; set; }
        public string StringValue { get; set; }
        public string MeasureValue { get; set; }
        public string MeasureValueMax { get; set; }
        public string ShapePath2Drawing { get; set; }
        public int MeasureClass { get; set; }
        public string MeasureValueName { get; set; }
        public string ShapePath2DrawingGif
        {
            get { return string.IsNullOrEmpty(ShapePath2Drawing) ? "" : ShapePath2Drawing.Replace(".wmf", ".gif"); }
        }
        public int BatchId { get; set; }
        public string ItemCode { get; set; }
        public object MeasureValueSql
        {
            get
            {
                if (string.IsNullOrEmpty(MeasureValue)) return DBNull.Value;
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    try
                    {
                        return double.Parse(MeasureValue);
                    }
                    catch (Exception x)
                    {
                        var msg = x.Message;
                    }
                    //Numeric Value
                    return 0;
                }
                return MeasureValue;
            }
        }
        public object MeasureValueMaxSql
        {
            get
            {
                if (string.IsNullOrEmpty(MeasureValueMax)) return DBNull.Value;
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    try
                    {
                        return double.Parse(MeasureValueMax);
                    }
                    catch (Exception x)
                    {
                        var msg = x.Message;
                    }
                    //Numeric Value
                    return 0;
                }
                return MeasureValueMax;
            }
        }
        public object MeasureValueIdSql
        {
            get
            {
                if (string.IsNullOrEmpty(MeasureValueId) || MeasureValueId == "0") return DBNull.Value;
                return Int32.Parse(MeasureValueId);
            }
        }
        public object MeasureValueIdMaxSql
        {
            get
            {
                if (string.IsNullOrEmpty(MeasureValueIdMax) || MeasureValueIdMax == "0") return DBNull.Value;
                return Int32.Parse(MeasureValueIdMax);
            }
        }
        public object StringValueSql
        {
            get
            {
                return (StringValue ?? (object)DBNull.Value);
            }
        }

        public SingleItemModel ForItem { get; set; }

        public static MeasureValueModel GetEmptyForEnum()
        {
            return new MeasureValueModel { EnumValueTitle = "(Value not set)", MeasureValueId = "0" };
        }
        public static MeasureValueModel Create(PreFillingValueModel src, string batchId, string itemCode)
        {
            var dst = new MeasureValueModel
            {
                BatchId = Convert.ToInt32(batchId),
                ItemCode = itemCode,
                PartId = src.PartId,
                MeasureCode = "" + src.MeasureCode,
                MeasureClass = src.MeasureClass
            };
            if (src.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                dst.MeasureValueId = src.MeasureValue;
            }
            if (src.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                dst.MeasureValue = src.MeasureValue;
            }
            if (src.MeasureClass == MeasureModel.MeasureClassText)
            {
                dst.StringValue = src.MeasureValue;
            }
            return dst;
        }
    }
}