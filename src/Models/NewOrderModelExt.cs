﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class NewOrderModelExt
    {
        public NewOrderModelExt()
        {
        }
        public NewOrderModelExt(DataRow row)
        {
            StartDate = Convert.ToDateTime(row["StartDate"]);
            GroupHistoryID = Convert.ToInt32(row["GroupHistoryID"]);
            ServiceTypeID = Convert.ToInt32(row["ServiceTypeID"]);
            CarrierID = Convert.ToInt32(row["CarrierID"]);
            VendorID = Convert.ToInt32(row["VendorID"]);
            NotInspectedTotalWeight = Convert.ToDecimal(row["NotInspectedTotalWeight"]);
            VendorOfficeID = Convert.ToInt32(row["VendorOfficeID"]);
            Memo = "" + row["Memo"];
            SpecialInstructions = "" + row["SpecialInstructions"];
            CarrierTrackingNumber = "" + row["CarrierTrackingNumber"];
            AuthorID = Convert.ToInt32(row["AuthorID"]);
            AuthorOfficeID = Convert.ToInt32(row["AuthorOfficeID"]);
            InspectedWeightUnitID = Convert.ToInt32(row["InspectedWeightUnitID"]);
            NotInspectedWeightUnitID = Convert.ToInt32(row["NotInspectedWeightUnitID"]);
            StateID = Convert.ToInt32(row["StateID"]);
            StateTargetID = Convert.ToInt32(row["StateTargetID"]);
            ShipmentCharge = Convert.ToInt32(row["ShipmentCharge"]);
            PersonID = Convert.ToInt32(row["PersonID"]);
            PersonCustomerOfficeID = Convert.ToInt32(row["PersonCustomerOfficeID"]);
            GroupOfficeID = Convert.ToInt32(row["GroupOfficeID"]);
            GroupID = Convert.ToInt32(row["GroupID"]);
            PickedUpByOurMessenger = Convert.ToBoolean(row["PickedUpByOurMessenger"]);
            BatchID = Convert.ToInt32(row["BatchID"]);
            ItemCode = Convert.ToInt32(row["ItemCode"]);
            GroupCode = Convert.ToInt32(row["GroupCode"]);
            IsNoGoods = Convert.ToBoolean(row["IsNoGoods"]);
        }
        public CustomerModel Customer { get; set; }
        public DateTime StartDate { get; set; }
        public int GroupHistoryID { get; set; }
        public int ServiceTypeID { get; set; }
        public bool IsIQInspected{ get; set; }
        public bool IsTWInspected { get; set; }
        public int NumberOfItems { get; set; }
        public decimal TotalWeight { get; set; }
        public int MeasureUnitID { get; set; }
        public int CarrierID { get; set; }
        public int VendorID { get; set; }
        public decimal NotInspectedTotalWeight { get; set; }
        public decimal InspectedTotalWeight { get; set; }
        public int VendorOfficeID { get; set; }
        public string Memo { get; set; }
        public List<string> MemoNumbers { get; set; }
        public string SpecialInstructions { get; set; }
        public string CarrierTrackingNumber { get; set; }
        public int AuthorID { get; set; }
        public int AuthorOfficeID { get; set; }
        public int InspectedWeightUnitID { get; set; }
        public int NotInspectedWeightUnitID { get; set; }
        public int StateID { get; set; }
        public int StateTargetID { get; set; }
        public int ShipmentCharge { get; set; }
        public int PersonID { get; set; }
        public int PersonCustomerOfficeID { get; set; }
        public int PersonCustomerID { get; set; }
        public int GroupOfficeID { get; set; }
        public int GroupID { get; set; }
        public bool PickedUpByOurMessenger { get; set; }
        public string OrderNumber { get; set; }
        public int BatchID  { get; set; }
        public int ItemCode { get; set; }
        public int GroupCode { get; set; }
		public int BatchCode { get; set; }
		public string OperationChar { get; set; }
		public bool TakenOutByOurMessenger { get; set; }
        public bool IsNoGoods { get; set; }
    }
}

//NotInspectedQuantity	    InspectedQuantity 
//NotInspectedTotalWeight	InspectedTotalWeight