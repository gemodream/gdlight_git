﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Corpt.Models
{
    [Serializable]
    public class DocStructModel
    {
        public DocStructModel()
        {
        }

        /// <summary>
        ///  spGetDocumentValue
        /// </summary>
        /// <param name="row"></param>
        public DocStructModel(DataRow row)
        {
            Title = ("" + row["Title"]).Trim();
            if (Title == "")
            {
                Title = "Empty";
            }
            Value = "" + row["Value"];
            DocumentId = "" + row["DocumentId"];
            DocumentValueId = "" + row["DocumentValueId"];
            Unit = "" + row["Unit"];
        }
        
        public string Title { get; set; }
        public string Value { get; set; }
        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentValueId { get; set; }
        public string Unit { get; set; }
        public bool IsCutGrade
        {
            get
            {
                return (Title.ToUpper().IndexOf("CUT", StringComparison.Ordinal) >= 0 &&
                       Title.ToUpper().IndexOf("GRADE", StringComparison.Ordinal) >= 0) || (Value.ToUpper().Contains("CUTGRADE"));
            }
        }
        public static bool CompareStruct(List<DocStructModel> l1, List<DocStructModel> l2)
        {
            return l1.Count == l2.Count && 
                l1.All(docStructModel => l2.FindAll(model => model.Title.Trim() == docStructModel.Title.Trim()).Count != 0);
        }

        public static string GetAbbr(List<DocStructModel> list, string forText)
        {
            var title = list.Aggregate("", (current, item) => current + ((current == "" ? "" : ", ") + item.Title));
            return string.Format("<abbr title=\"{0}\">{1}</abbr>", title, forText);
        }
        public static DocStructModel Create1(DataRow row)
        {
            var model = new DocStructModel();
            model.Title = ("" + row["Title"]).Trim();
            if (model.Title == "")
            {
                model.Title = "Empty";
            }
            model.Value = "" + row["Value"];
            model.DocumentId = "" + row["DocumentId"];
            model.DocumentName = "" + row["DocumentName"];
            model.DocumentValueId = "" + row["DocumentValueId"];
            model.Unit = "" + row["Unit"];
            return model;
        }
    }
}