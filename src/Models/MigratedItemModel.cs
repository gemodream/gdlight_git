﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MigratedItemModel
    {
        public MigratedItemModel(DataRow row)
        {
            CurrentItemNumber = "" + row["CurrentOrderItemNumber"];
            PrevItemNumber = "" + row["PrevItemNumber"];
            NewItemNumber = "" + row["NewItemNumber"];
        }

        public string CurrentItemNumber { get; set; }
        public string PrevItemNumber { get; set; }
        public string NewItemNumber { get; set; }
    }
}