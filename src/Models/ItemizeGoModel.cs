﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class ItemizeGoModel
    {
        public ItemizeGoModel()
        {
        }

        public OrderModel Order { get; set; }
        public int NumberOfItems { get; set; }
        public MemoModel Memo { get; set; }
        public CustomerProgramModel Cp { get; set; }
        public List<string> LotNumbers { get; set; }
        
    }
}