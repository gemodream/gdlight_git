﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CpRuleShortModel
    {
        public CpRuleShortModel()
        {
        }

        public CpRuleShortModel(DataRow row)
        {
            MeasureValueID = int.Parse("" + row[nameof(MeasureValueID)]);
            MeasureName = "" + row[nameof(MeasureName)];
            MeasureValueName = "" + row[nameof(MeasureValueName)];
            ValueTitle = "" + row[nameof(ValueTitle)];
        }
        public int MeasureValueID { get; set; }
        public string MeasureName { get; set; }
        public string MeasureValueName { get; set; }
        public string ValueTitle { get; set; }


    }
}