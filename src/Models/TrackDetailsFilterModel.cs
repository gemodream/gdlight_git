﻿using System;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class TrackDetailsFilterModel
    {
        public TrackDetailsFilterModel()
        {
        }

        public string CustomerId { get; set; }
        public string OfficeId { get; set; }
        public string Mode { get; set; }
        public string Value { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public bool IsMemoMode
        {
            get { return Mode == SessionConstants.TrackDetailsModeMemo; }
        }
        public bool IsCpMode
        {
            get { return Mode == SessionConstants.TrackDetailsModeCp; }
        }
        public bool IsOrderMode
        {
            get { return Mode == SessionConstants.TrackDetailsModeOrder; }
        }
        public string Description
        {
            get
            {
                var range = "From " + DateTime.Parse(DateFrom).ToShortDateString() + " To " +
                            DateTime.Parse(DateTo).ToShortDateString();
                if (IsMemoMode)
                {
                    return "Memo: " + Value + ", " + range;
                }
                if (IsOrderMode)
                {
                    return "Order: " + Value + ", " + range;
                }
                if (IsCpMode)
                {
                    return "Cp: " + Value + ", " + range;
                }
                return "";
            }
        }
    }
}