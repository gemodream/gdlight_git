﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ColorClarityStatModel
    {
        public ColorClarityStatModel(){}
        /// <summary>
        /// sp_StatColorClarity2
        /// </summary>
        /// <param name="row"></param>
        public ColorClarityStatModel(DataRow row)
        {
            MeasureName = Convert.ToString(row["Measuretitle"]);
            Vendor = Convert.ToString(row["companyName"]);
            Year = Convert.ToInt32(row["y"]);
            Month = Convert.ToInt32(row["m"]);
            MeasureValue = Convert.ToString(row["mactname"]);
            Qnty = Convert.ToInt32(row["items"]);
            Level = 0;
        }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Qnty { get; set; }
        public string Vendor { get; set; }
        public string MeasureName { get; set; }
        public string MeasureValue { get; set; }
        public int Level { get; set; }
    }
}