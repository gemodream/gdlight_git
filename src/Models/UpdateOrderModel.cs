﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class UpdateOrderModel
    {
        public UpdateOrderModel()
        {
            Batches = new List<UpdateOrderBatchModel>();
        }

        public string OrderCode { get; set; }
        public List<UpdateOrderBatchModel> Batches { get; set; }
        
    }
}