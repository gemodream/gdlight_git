﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class ItemValueEditModel
    {
        public ItemValueEditModel()
        {
        }
        public ItemValueEditModel(MeasurePartModel partModel , MeasureValueCpModel measureModel, MeasureValueModel valueModel, List<EnumMeasureModel> enums, 
            SingleItemModel itemModel, bool includedInDocument)
        {
            EditingItem = itemModel;
            PartId = "" + partModel.PartId;
            PartName = partModel.PartName;
            MeasureId = Convert.ToInt32(measureModel.MeasureId);
            MeasureClass = measureModel.MeasureClass;
            MeasureName = measureModel.MeasureName;
            MeasureTitle = measureModel.MeasureTitle;
            IncludedInDocument = includedInDocument;
            if (valueModel != null)
            {
                Value = GetValue(valueModel);
                PrevValue = Value;
            }
            if (enums != null)
            {
                // Add empty item
                if (string.IsNullOrEmpty(Value))
                {
                    var empty = new EnumMeasureModel
                    {
                        ValueTitle = "",
                        MeasureValueName = "",
                        MeasureValueId = 0,
                        MeasureClass = MeasureModel.MeasureClassEnum,
                        MeasureValueMeasureId = Convert.ToInt32(measureModel.MeasureId)
                    };
                    enums.Insert(0, empty);
                    Value = "0";
                    PrevValue = Value;
                }
                EnumSource = enums;
            }

        }
        public ItemValueEditModel Clone()
        {
            var cloneModel = new ItemValueEditModel
            {
                EditingItem = EditingItem,
                PartId = PartId,
                PartName = PartName,
                MeasureId = MeasureId,
                MeasureName = MeasureName,
                MeasureTitle = MeasureTitle,
                MeasureClass = MeasureClass,
                IncludedInDocument = IncludedInDocument,
                Value = Value,
                PrevValue = PrevValue,
                EnumSource = EnumSource
            };
            return cloneModel;
        }
        public string PartId { get; set; }
        public string PartName { get; set; }
        public int MeasureId { get; set; }
        public int MeasureClass { get; set; }
        public string MeasureName { get; set; }
        public string MeasureTitle { get; set; }
        public string Value { get; set; }
        public string PrevValue { get; set; }
        public List<EnumMeasureModel> EnumSource { get; set; }
        public SingleItemModel EditingItem { get; set; }
        public bool IncludedInDocument { get; set; }
        public string UniqueKey
        {
            get { return string.Format("{0};{1}", PartId, MeasureId); }
        }

        public bool HasValues
        {
            get { return !string.IsNullOrEmpty(Value) && Value != "0"; }
        }
        public string DisplayValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value) || Value == "0") return "";
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    if (EnumSource != null)
                    {
                        var enumId = (int)Convert.ToDecimal(Value);
                        var enumModel = EnumSource.Find(m => m.MeasureValueMeasureId == MeasureId && m.MeasureValueId == enumId);
                        if (enumModel != null)
                        {
                            return enumModel.MeasureValueName;
                        }
                    }
                    else
                    {
                        return Value;
                    }

                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return string.Format("{0:0.##}", Convert.ToDecimal(Value));
                }
                return Value;
            }
        }
        public string GetValue(MeasureValueModel valueModel)
        {
            if (MeasureClass == MeasureModel.MeasureClassText) return valueModel.StringValue;
            if (MeasureClass == MeasureModel.MeasureClassNumeric) return valueModel.MeasureValue;
            return MeasureClass == MeasureModel.MeasureClassEnum ? valueModel.MeasureValueId : "";
        }
        public bool HasChange
        {
            get { return HasChanged(Value); }
        }
        public bool HasChanged(string newValue)
        {
            if (MeasureClass == MeasureModel.MeasureClassText)
            {
                var parsePrev = string.IsNullOrEmpty(PrevValue) ? "" : PrevValue;
                var parseNew = string.IsNullOrEmpty(newValue) ? "" : newValue;
                return parsePrev != parseNew;
            }
            if (MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                var parsePrev = string.IsNullOrEmpty(PrevValue) ? 0 : Convert.ToDecimal(PrevValue);
                var parseNew = string.IsNullOrEmpty(newValue) ? 0 : Convert.ToDecimal(newValue);
                return parsePrev != parseNew;
            }
            if (MeasureClass == MeasureModel.MeasureClassEnum)
            {
                var parsePrev = string.IsNullOrEmpty(PrevValue) ? 0 : Convert.ToInt32(PrevValue);
                var parseNew = string.IsNullOrEmpty(newValue) ? 0 : Convert.ToInt32(newValue);
                return parsePrev != parseNew;
            }
            return false;
        }
    }
    public class RemeasurePartsModel
    {
        public RemeasurePartsModel()
        {
        }
        public RemeasurePartsModel(string id, string name)
        {
            PartId = id;
            PartName = name;
        }
        public string PartId { get; set; }
        public string PartName { get; set; }
    }
}