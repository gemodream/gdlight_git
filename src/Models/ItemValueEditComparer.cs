﻿using System;
using System.Collections.Generic;
using Corpt.Utilities;

namespace Corpt.Models
{
    public class ItemValueEditComparer : IComparer<ItemValueEditModel>
    {
        public int Compare(ItemValueEditModel x, ItemValueEditModel y)
        {
            var result = ComparerUtils.NumberCompare(Convert.ToInt32(x.PartId), Convert.ToInt32(y.PartId));
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(x.MeasureName.ToUpper(), y.MeasureName.ToUpper());
            }
            return result;
        }
    }
}