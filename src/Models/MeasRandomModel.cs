﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class MeasRandomModel
    {
        public MeasRandomModel()
        {
            BatchItems = new List<BatchItemModel>();
        }
        public int MeasureId { get; set; }
        public int PartId { get; set; }
        public double MaxFrom { get; set; }
        public double MaxTo { get; set; }
        public double MinFrom { get; set; }
        public double MinTo { get; set; }
        public double HxFrom { get; set; }
        public double HxTo { get; set; }
        public double WeightFrom { get; set; }
        public double WeightTo { get; set; }
        public List<BatchItemModel> BatchItems;
    }
}