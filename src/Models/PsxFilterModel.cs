﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class PsxFilterModel
    {
        public PsxFilterModel()
        {
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int CustomerId { get; set; }
        public string ScanLike { get; set; }
        public int Xfactor { get; set; }
        public bool HasScan
        {
            get { return ScanLike.Length > 0 && ScanLike != "*"; }
        }
    }
}