﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class QualityControlModel
    {

        public QualityControlModel() { }

        public QualityControlModel(DataRow row)
        {
            QCID = Int64.Parse("" + row["QCID"]);
            OrderCode = Int32.Parse("" + row["OrderCode"]);
            CustomerName = "" + row["CustomerName"];
            CustomerProgram = "" + row["CustomerProgram"];
            ProgramName = "" + row["ProgramName"];
            Quantity = Int32.Parse("" + row["Quantity"]);
            Comments = "" + row["Comments"];
            IsQCDone = Boolean.Parse(row["IsQCDone"].ToString());
            IsDisable = Boolean.Parse(row["IsDisable"].ToString());
            CreatedBy = "" + row["CreatedBy"];
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?) null : DateTime.Parse("" + row["CreatedDate"]);
            ModifiedBy = "" + row["ModifiedBy"];
            ModifiedDate = row["ModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ModifiedDate"]);
            QCDoneBy = "" + row["QCDoneBy"];
            QCDoneOn = row["QCDoneOn"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["QCDoneOn"]); 
        }    

//       QCID, OrderCode, CustomerName, CustomerProgram, ProgramName, Quantity, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, QCDoneOn, QCDoneBy

        public Int64 QCID { get; set; }
        public int OrderCode { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerProgram { get; set; }
        public int @ProgramID { get; set; }
        public string ProgramName { get; set; }
        public int Quantity { get; set; }
        public string Comments { get; set; }
        public bool IsQCDone { get; set; }
        public bool IsDisable { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string QCDoneBy { get; set; }
        public DateTime? QCDoneOn { get; set; }

    }
}


