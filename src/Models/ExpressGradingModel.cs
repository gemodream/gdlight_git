﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ExpressGradingModel
    {
        public ExpressGradingModel(DataRow row)
        {
            PartName = "" + row["PartName"];
            PartId = Int32.Parse("" + row["PartID"]);
            MeasureName = "" + row["MeasureName"];
            MeasureCode = "" + row["MeasureCode"];
            MeasureID = row["MeasureID"] != DBNull.Value ? Int32.Parse("" + row["MeasureID"]) : -1;
            MeasureValueSku = ("" + row["ResultValue"]).Trim();
            MeasureClass = row["MeasureClass"] != DBNull.Value ? Int32.Parse("" + row["MeasureClass"]) : 2;
            MeasureValueReal = "";
        }
        public string PartName { get; set; }
        public int PartId { get; set; }
        public string MeasureName { get; set; }
        public string MeasureCode { get; set; }
        public int MeasureID { get; set; }
        public int MeasureClass { get; set; }
        public string MeasureValueSku { get; set; }
        public string MeasureValueReal { get; set; }
        public bool IsEnum { get { return MeasureClass == MeasureClassEnum; } }

        public const int MeasureClassEnum = 1;
        public const int MeasureClassText = 2;
        public const int MeasureClassNumeric = 3;
        public bool failed { get; set; }

        //temporary init
        public ExpressGradingModel(string partName, string measureName, string measureCode, string measureValue, int measureClass)
        {
            PartName = partName;
            MeasureName = measureName;
            MeasureCode = measureCode;
            MeasureValueSku = measureValue;
            MeasureValueReal = "";
            MeasureClass = measureClass;
            failed = false;
        }
    }
}