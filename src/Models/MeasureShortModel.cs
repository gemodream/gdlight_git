﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureShortModel
    {
        /// <summary>
        /// spGetMeasuresWithAdditional
        /// </summary>
        /// <param name="row"></param>
        public MeasureShortModel(DataRow row)
        {
            MeasureTitle = "" + row["MeasureTitle"];
            MeasureId = "" + row["MeasureID"];
            PartTypeId = "" + row["PartTypeID"];
        }

        public string MeasureTitle { get; set; }
        public string MeasureId { get; set; }
        public string PartTypeId { get; set; }
    }
}