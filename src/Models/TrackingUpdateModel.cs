﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingUpdateModel
    {
        public TrackingUpdateModel()
        {
        }

        public int BatchId { get; set; }
        public int ViewAccess { get; set; } //formId
        public int EventId { get; set; }
        public int ItemsAffected { get; set; }
        public int ItemsInBatch { get; set; }

    }
}