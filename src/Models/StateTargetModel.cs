﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class StateTargetModel
    {
        public static int TrgCodeItem = 1;
        public static int TrgCodeBatch = 2;
        public static int TrgCodeOrder = 3;
        public static int TrgCodeCustomer = 4;
        public static int TrgCodeDocument = 5;
        public StateTargetModel()
        {
        }

        /// <summary>
        /// spGetStates
        /// </summary>
        /// <param name="row"></param>
        public StateTargetModel(DataRow row)
        {
            StateId = Convert.ToInt32(row["StateID"]);
            StateCode = Convert.ToInt32(row["StateCode"]);
            StateName = "" + row["StateName"];
            
            TargetId = Convert.ToInt32(row["StateTargetID"]);
            TargetCode = Convert.ToInt32(row["StateTargetCode"]);
            TargetName = "" + row["StateTargetName"];

            IconIndex = "" + row["IconIndex"];
        }

        public int StateId { get; set; }
        public int StateCode { get; set; }
        public string StateName { get; set; }

        public int TargetId { get; set; }
        public int TargetCode { get; set; }
        public string TargetName { get; set; }
        
        public string IconIndex { get; set; }
        public string ImageUrl
        {
            get
            {
                return string.IsNullOrEmpty(IconIndex) ? "" : "Images/stateImages/" + IconIndex + ".png";
            }
        }
        public string ImageTooltip
        {
            get { return StateName ?? ""; }
        }
    }
}