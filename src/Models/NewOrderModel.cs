﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class NewOrderModel
    {
        public CustomerModel Customer { get; set; }
        public List<string> MemoNumbers { get; set; }
        public string SpecialInstructions { get; set; }
        public int? NumberOfItems { get; set; }
        public string OrderMemo { get; set; }
        public int GroupId { get; set; }
        public int GroupOfficeId { get; set; }
        public string OrderNumber { get; set; }
		public decimal? TotalWeight { get; set; }
        public bool? IsNoGoods { get; set; }
    }
}