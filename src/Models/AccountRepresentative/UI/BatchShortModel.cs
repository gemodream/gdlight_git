﻿using System;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class BatchShortModel
    {
        public int GroupCode { get; set; }
        public int BatchCode { get; set; }
        public int ItemsQuantity { get; set; }
        public int BatchId { get; set; }
    }
}