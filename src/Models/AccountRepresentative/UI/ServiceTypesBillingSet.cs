﻿namespace Corpt.Models.AccountRepresentative.UI
{
    public enum ServiceTypesBillingSet
    {
        Screening,
        LabServices,
        Mixed,
        Unset
    }
}