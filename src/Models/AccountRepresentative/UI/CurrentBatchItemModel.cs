﻿using System;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    internal class CurrentBatchItemModel
    {
        public string FullItemNumber { get; set; }
        public string ToolTip { get; set; }
    }
}