﻿using System;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class AdditionalServiceRow
    {
        public bool Checked { get; set; }
        public int OrderCode { get; set; }
        // AdditionalServiceID	ServiceName	BatchID	BatchCode	Price	PriceID	AuthorID
        public int AdditionalServiceId { get; set; }
        public string Name { get; set; }
        public int BatchId { get; set; }
        public int BatchCode { get; set; }
        public decimal Price { get; set; }
        public int PriceId { get; set; }
        public int UserId { get; set; }
    }
}