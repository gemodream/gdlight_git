﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class AllServices
    {
        [XmlElement("Services")]
        public List<AdditionalService> Services { get; set; }
    }
}