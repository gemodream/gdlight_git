﻿using System;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class ItemToBeInvoiced
    {
        public int OrderCode { get; set; }
        public int BatchId { get; set; }
        public int ItemCode { get; set; }
        public double Cost { get; set; }
        public string Description { get; set; }
        public int BatchCode { get; set; }
    }
}