﻿using System.ComponentModel;

namespace Corpt.Models.AccountRepresentative.UI
{
    public enum DisplayTimeZoneModes
    {
        [Description("Client Time Zone")]
        Client,

        [Description("By Office Time Zone")]
        ByOffice,

        [Description("Coordinated Universal Time")]
        // ReSharper disable once InconsistentNaming
        UTC
    }
}