﻿using System.ComponentModel;

namespace Corpt.Models.AccountRepresentative.UI
{
    public enum ServiceTypesCategory
    {
        [Description("Screening Services")]
        ScreeningServices = 0,

        [Description("Lab Services")]
        LabServices = 1,

        [Description("All")]
        All = 2,

        [Description("Custom")]
        Custom = 3
    }
}