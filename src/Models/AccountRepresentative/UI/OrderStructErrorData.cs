﻿using System;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class OrderStructWarningData
    {
        public OrderStructWarningData()
        {
        }

        public OrderStructWarningData(int orderCode, string missingOrderItem, string costName, int quantity, double cost)
        {
            OrderCode = orderCode;
            MissingOrderItem = missingOrderItem;
            CostName = costName;
            Quantity = quantity;
            Cost = cost;
        }

        public int OrderCode { get; set; }
        public string MissingOrderItem { get; set; }
        public string CostName { get; set; }
        public int Quantity { get; set; }
        public double Cost { get; set; }
        public static string WarningText => "Cannot place invoice item - there is no order item";

        public override string ToString()
        {
            return $"Order: {OrderCode}, Missing={MissingOrderItem}, Quantity={Quantity}, Cost {Cost:C} for \"{CostName}\", Error={WarningText}";
        }
    }
}