﻿using System;
using System.Collections.Generic;
using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class PricesGridViewItemModel
    {
        public int OrderCode { get; set; }

        public string OrderText { get; set; }

        public int RetailerId { get; set; }

        public string RetailerName { get; set; }

        public string QuestionText { get; set; }

        public bool IsSatisfyACondition { get; set; }

        public PricingModel Pricing { get; set; }

        public PricingModel DefaultPricing { get; set; }

        public PricingModel SpecialPricing { get; set; }

        public string ShortDescription { get; set; }

        public QuantitiesModel Quantities { get; set; }

        public PricingModel InvoiceCosts { get; set; }

        public PricingModel Costs { get; set; }

        public string OrderStructurePattern { get; set; }

        public int? ServiceTypeId { get; set; }

        public string ServiceTypeName { get; set; }

        public bool IsAlreadyBilled { get; set; }

        public string YesAnswerText { get; set; }

        public string NoAnswerText { get; set; }

        public string CreateDate { get; set; }

        public string SynthServiceTypeName { get; set; }

        public int? SynthServiceTypeCode { get; set; }

        public bool IsRepack { get; set; }

        public bool IsReadyToBilling => !IsAlreadyBilled && (Quantities != null);

        public AlreadyBilledOrderData AlreadyBilledData { get; set; }

        public List<TreeNodeItemModel> OrderItems { get; set; }

        public bool HasCustomerRequest { get; set; }

        public string QbCustomerName { get; set; }
    }
}