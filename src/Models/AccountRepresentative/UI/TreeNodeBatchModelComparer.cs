﻿using System.Collections.Generic;
using Corpt.AccountRepModels;

namespace Corpt.Models.AccountRepresentative.UI
{
    public class TreeNodeBatchModelComparer : IEqualityComparer<TreeNodeBatchModel>
    {
        public bool Equals(TreeNodeBatchModel x, TreeNodeBatchModel y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.BatchId == y.BatchId;
        }

        public int GetHashCode(TreeNodeBatchModel obj)
        {
            return obj.BatchId;
        }
    }
}