﻿using System;
using System.Xml.Serialization;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class AdditionalService
    {
        [XmlElement("col0")]
        public int Col0 { get; set; }

        [XmlElement("col1")]
        public string Col1 { get; set; }

        [XmlElement("col2")]
        public int Col2 { get; set; }

        [XmlElement("col3")]
        public int Col3 { get; set; }

        [XmlElement("col4")]
        public decimal Col4 { get; set; }

        [XmlElement("col5")]
        public int Col5 { get; set; }

        [XmlElement("col6")]
        public int Col6 { get; set; }
    }
}