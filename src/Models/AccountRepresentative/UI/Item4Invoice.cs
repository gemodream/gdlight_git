﻿using System;
using Corpt.AccountRepModels;

namespace Corpt.Models.AccountRepresentative.UI
{
    [Serializable]
    public class Item4Invoice
    {
        public Item4Invoice(TreeNodeItemModel treeNodeItemModel)
        {
            BatchId = treeNodeItemModel.BatchId;
            ItemCode = treeNodeItemModel.ItemCode;
            GroupCode = treeNodeItemModel.GroupCode;
            BatchCode = treeNodeItemModel.BatchCode;
        }

        public Item4Invoice(int batchId, int itemCode, int groupCode, int batchCode)
        {
            BatchId = batchId;
            ItemCode = itemCode;
            GroupCode = groupCode;
            BatchCode = batchCode;
        }

        public int Passed { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public string FullBatch { get; set; }
        public int BatchId { get; set; }
        public int ItemCode { get; set; }
        public int GroupCode { get; set; }
        public int BatchCode { get; set; }

    }
}