﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    public class QbInvoiceRequestModel
    {
        // Status Constants
        public const string RequestStatusNew = "New";
        public const string RequestStatusCompleted = "Completed";
        public const string RequestStatusError = "Error";

        // Request Type Constants
        public const string RequestTypeGet = "Get";
        public const string RequestTypeUpdate = "Update";

        public QbInvoiceRequestModel(DataRow row)
        {
            RequestId = row.ConvertToInt("ID");
            RequestType = row.ConvertToString("RequestType");
            Status = row.ConvertToString("Status");
            QbInvoiceNumber = row.ConvertToString("QBInvoiceNumber");
            QbCustomerName = row.ConvertToString("QBCustomerName");
            InvoiceData = row.ConvertToString("InvoiceData");
            CreatedDate = row.ConvertToDateTime("CreatedDate");
            ModifiedDate = row.ConvertToDateTime("ModifiedDate");
        }

        public DateTime ModifiedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public string QbCustomerName { get; set; }

        public int RequestId { get; set; }

        public string RequestType { get; set; }

        public string Status { get; set; }

        public string QbInvoiceNumber { get; set; }

        public string InvoiceData { get; set; }
    }
}