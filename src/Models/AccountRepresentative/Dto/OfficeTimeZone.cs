﻿using System;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class OfficeTimeZone
    {
        public OfficeTimeZone()
        {
        }

        public OfficeTimeZone(int officeId, int offsetMin, string offsetDisplay, string timeZoneAbbr, string timeZoneName)
        {
            OfficeId = officeId;
            OffsetMin = offsetMin;
            OffsetDisplay = offsetDisplay;
            TimeZoneAbbr = timeZoneAbbr;
            TimeZoneName = timeZoneName;
        }

        public int OfficeId { get; set; }
        public int OffsetMin { get; set; }
        public string OffsetDisplay { get; set; }
        public string TimeZoneAbbr { get; set; }
        public string TimeZoneName { get; set; }
    }
}