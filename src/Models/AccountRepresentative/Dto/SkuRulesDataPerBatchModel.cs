﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class SkuRulesDataPerBatchModel
    {
        public SkuRulesDataPerBatchModel(DataRow row)
        {
            MeasureCode = row.ConvertToInt("MeasureCode");
            MeasureName   =  row.ConvertToString("MeasureName");
            ValueCode     =  row.ConvertToString("ValueCode");
            ResultValue   =  row.ConvertToString("ResultValue");
            PartId        =  row.ConvertToInt("PartID");
            PartName      =  row.ConvertToString("PartName");
            BatchId       =  row.ConvertToInt("BatchID");
            MeasureClass  =  row.ConvertToInt("MeasureClass");
            MeasureId     =  row.ConvertToInt("MeasureID");
        }

        public SkuRulesDataPerBatchModel(string measureName, string resultValue)
        {
            MeasureName = measureName;
            ResultValue = resultValue;
        }

        public int MeasureCode { get; }

        public string MeasureName { get; }

        public string ValueCode { get; }

        public string ResultValue { get; }

        public int PartId { get; }

        public string PartName { get; }

        public int BatchId { get; }

        public int MeasureClass { get; }

        public int MeasureId { get; }
    }
}