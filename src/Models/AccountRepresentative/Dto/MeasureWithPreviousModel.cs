﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class MeasureWithPreviousModel
    {
        public MeasureWithPreviousModel()
        {
        }

        public MeasureWithPreviousModel(DataRow row)
        {
            PartId = row.ConvertToInt("PartID");
            MeasureId = row.ConvertToInt("MeasureID");
            MeasureGroupId = row.ConvertToInt("MeasureGroupID");
            MeasureClass = row.ConvertToInt("MeasureClass");
            PartName = row.ConvertToString("PartName");
            MeasureName = row.ConvertToString("MeasureName");
            ResultValue = row.ConvertToString("ResultValue");
            PrevResultValue = row.ConvertToString("PrevResultValue");
        }

        public int MeasureGroupId { get; set; }

        public int PartId { get; set; }

        public int MeasureId { get; set; }

        public int MeasureClass { get; set; }

        public string PartName { get; set; }

        public string MeasureName { get; set; }

        public string ResultValue { get; set; }

        public string PrevResultValue { get; set; }
    }
}