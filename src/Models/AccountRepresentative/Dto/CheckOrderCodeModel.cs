﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class CheckOrderCodeModel
    {
        public CheckOrderCodeModel()
        {
        }

        public CheckOrderCodeModel(DataRow row)
        {
            GroupCode = row.ConvertToInt("GroupCode");
            GroupId = row.ConvertToIntNullable("GroupID");
            CustomerCode = row.ConvertToIntNullable("CustomerCode");
        }

        public int GroupCode { get; set; }

        public int? GroupId { get; set; }

        public int? CustomerCode { get; set; }

        public bool Found => GroupId != null;
    }
}