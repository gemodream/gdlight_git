﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class QuantitiesData
    {
        public QuantitiesData()
        {
        }
        public QuantitiesData(DataRow row)
        {
            GroupCode = int.Parse(row.ConvertToString("GSINumber"));
            TotalQuantity = row.ConvertToInt("QTY");
            QuantityPass = row.ConvertToInt("QTYPass");
            QuantityFail = row.ConvertToInt("QTYFail");
            Faults = row.ConvertToInt("Faults");
            SyntheticsItems = row.ConvertToInt("SynthItems");
            Synthetics = row.ConvertToInt("Synthetics");
            SyntheticsGems = row.ConvertToInt("SynthGems");
            VendorId = row.ConvertToInt("VendorID");

            ItemsNumberWithBrokenFaults = row.ConvertToInt("Broken");
            ItemsNumberWithSynthFaults = row.ConvertToInt("Synth");
            ItemsNumberWithAnyFaults = row.ConvertToInt("ItemsQnty");
            ServiceTypeId = row.ConvertToInt("ServiceTypeID");
            InvoiceStatus = row.ConvertToString("InvoiceStatus");

        }

        public int ItemsNumberWithAnyFaults { get; set; }
        public int ServiceTypeId { get; set; }
        public string InvoiceStatus { get; set; }
        public int ItemsNumberWithSynthFaults { get; set; }
        public int ItemsNumberWithBrokenFaults { get; set; }

        public int VendorId { get; set; }
        public int GroupCode { get; set; }
        public int TotalQuantity { get; set; }
        public int QuantityPass { get; set; }
        public int QuantityFail { get; set; }
        public int Faults { get; set; }
        public int SyntheticsItems { get; set; }
        public int Synthetics { get; set; }
        public int SyntheticsGems { get; set; }
    }
}