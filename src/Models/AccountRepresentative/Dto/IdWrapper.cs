﻿using System;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class IdWrapper
    {
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
    }
}