﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class OrdersInvoiceModel
    {
        public OrdersInvoiceModel()
        {
        }

        public OrdersInvoiceModel(DataRow row)
        {
            GroupCode = row.ConvertToInt("GroupCode");
            InvoiceId = row.ConvertToInt("InvoiceId") + " " + Guid.NewGuid();
        }

        public string InvoiceId { get; set; }

        public int GroupCode { get; set; }
    }
}