﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class BatchItemInvoiceModel
    {
        public BatchItemInvoiceModel()
        {
        }

        public BatchItemInvoiceModel(DataRow row)
        {
            InvoiceId = int.Parse("" + row["InvoiceID"]);
            BatchId = int.Parse("" + row["BatchID"]);
            ItemCode = int.Parse("" + row["ItemCode"]);
            QuickBooksItemId = row.ConvertToIntNullable("QuickBooksItemId");
            Description = row.ConvertToString("Description");
            Price = Convert.ToDouble(row["Price"]);
        }
        
        public int InvoiceId { get; set; }

        public int BatchId { get; set; }

        public int ItemCode { get; set; }

        public int? QuickBooksItemId { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }
    }
}