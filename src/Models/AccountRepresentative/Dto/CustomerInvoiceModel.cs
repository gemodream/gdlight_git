﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class CustomerInvoiceModel
    {
        public CustomerInvoiceModel(DataRow row)
        {
            InvoiceId = row.ConvertToInt("InvoiceID");
            Status = row.ConvertToString("Status");
            BeginDate = row.ConvertToDateTime("BeginDate");
            QuickBooksId = row.ConvertToString("QuickBooksID");
            QBInvoiceNumber = row.ConvertToString("QBInvoiceNumber");
            InvoiceNumber = row.ConvertToString("InvoiceNumber");
            AuthorId = row.ConvertToInt("AuthorID");
            AuthorOfficeId = row.ConvertToInt("AuthorOfficeID");
            AuthorLogin = row.ConvertToString("AuthorLogin");
            AuthorName = row.ConvertToString("AuthorName");
        }

        // ReSharper disable once InconsistentNaming
        public string QBInvoiceNumber { get; set; }

        public string InvoiceNumber { get; set; }

        public int AuthorOfficeId { get; set; }

        public int AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorLogin { get; set; }

        public string QuickBooksId { get; set; }

        public int InvoiceId { get; set; }

        public string Status { get; set; }

        public DateTime BeginDate { get; set; }
    }
}