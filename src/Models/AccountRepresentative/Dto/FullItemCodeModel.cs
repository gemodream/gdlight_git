﻿using System;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class FullItemCodeModel
    {
        // ReSharper disable once InconsistentNaming
        public int GroupCode { get; set; }
        public int BatchCode { get; set; }
        public int ItemCode { get; set; }
    }
}