﻿using System;
using System.Data;
using Corpt.Utilities.BulkBilling;
using Corpt.Utilities.BulkBilling.CostManager.Models;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class AlreadyBilledOrderData
    {
        public AlreadyBilledOrderData()
        {
        }
        public AlreadyBilledOrderData(DataRow row)
        {
            GroupCode = row.ConvertToInt("OrderCode");
            BatchesCount = row.ConvertToInt("BatchesCount");
            ItemsCount = row.ConvertToInt("ItemsCount");
            CostStrList = row.ConvertToString("Prices");
            SynthServiceTypeCode = row.ConvertToInt("SynthServiceTypeCode");
            Cost = BulkBillingUtils.IsRepack(SynthServiceTypeCode)
                ? (PricingModel)SinglePricingModel.GetInstance(CostStrList)
                : RegularPricingModel.GetInstance(CostStrList);
            InvoiceStatus = row.ConvertToString("InvoiceStatus");
        }

        public string InvoiceStatus { get; set; }

        public int SynthServiceTypeCode { get; set; }

        public bool IsAlreadyBilled => CostStrList != null;

        public PricingModel Cost { get; set; }

        public string CostStrList { get; set; }

        public int? ItemsCount { get; set; }

        public int? BatchesCount { get; set; }

        public int GroupCode { get; set; }
    }
}