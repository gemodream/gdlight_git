﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class InvoiceDetailsData
    {
        public InvoiceDetailsData(DataRow row)
        {
            GroupCode = row.ConvertToInt("GroupCode");	       
            BatchCode = row.ConvertToInt("BatchCode");          
            ItemCode = row.ConvertToInt("ItemCode");           
            BatchId = row.ConvertToInt("BatchID");            
            InvoiceId = row.ConvertToInt("InvoiceId");          
            Status = row.ConvertToString("Status");          
            Description = row.ConvertToString("Description");     
            Price = double.Parse(row.ConvertToString("Price"));           
            CreateDate = row.ConvertToDateTime("ItemCreateDate");     
            QuickBooksId = row.ConvertToString("QuickBooksID");    
            QuickBooksItemId = row.ConvertToString("QuickBooksItemId");
            AuthorId = row.ConvertToInt("AuthorID");
            Author = row.ConvertToString("Author");
            AuthorOfficeId = row.ConvertToInt("AuthorOfficeID");
        }

        public int AuthorOfficeId { get; set; }

        public int AuthorId { get; set; }

        public string Author { get; set; }

        public string QuickBooksItemId { get; set; }

        public string QuickBooksId { get; set; }

        public DateTime CreateDate { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public int InvoiceId { get; set; }

        public int BatchId { get; set; }

        public int ItemCode { get; set; }

        public int BatchCode { get; set; }

        public int GroupCode { get; set; }

        public string FullItemCode => Utils.FullItemNumberWithDots(GroupCode, BatchCode, ItemCode);

        public override string ToString()
        {
            return $"{FullItemCode}, \"{Description}\", {Price:C}, ";
        }                                       
    }                                            
}