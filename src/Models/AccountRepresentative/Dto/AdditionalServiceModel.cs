﻿using System;
using System.Data;
using System.Globalization;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class AdditionalServiceModel
    {
        public AdditionalServiceModel()
        {
        }

        public AdditionalServiceModel(DataRow row)
        {
            AdditionalServiceId = int.Parse("" + row["AdditionalServiceID"]);
            Name = "" + row["Name"];
            BatchId = int.Parse("" + row["BatchID"]);
            BatchCode = int.Parse("" + row["BatchCode"]);
            Price = decimal.Parse("" + row["Price"], NumberStyles.AllowDecimalPoint);
            PriceId = int.Parse("" + row["PriceID"]);
            UserId = int.Parse("" + row["UserID"]);
        }

        public string Name { get; set; }

        public int AdditionalServiceId { get; set; }

        public int UserId { get; set; }

        public int PriceId { get; set; }

        public decimal Price { get; set; }

        public int BatchCode { get; set; }

        public int BatchId { get; set; }
    }
}