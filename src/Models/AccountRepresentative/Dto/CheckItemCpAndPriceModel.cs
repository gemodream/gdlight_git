﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class CheckItemCpAndPriceModel
    {
        public CheckItemCpAndPriceModel()
        {
        }

        public CheckItemCpAndPriceModel(DataRow row)
        {
            Pass = int.Parse("" + row["Pass"]);
            Description = row.ConvertToString("Description");
        }

        public int Pass { get; set; }

        public string Description { get; set; }
    }
}