﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.AccountRepresentative.Dto
{
    [Serializable]
    public class CustomerInvoicesResult
    {
        public List<CustomerInvoiceModel> CustomerInvoiceModels { get; set; }
        public List<OrdersInvoiceModel> OrdersInvoice { get; set; }
    }
}