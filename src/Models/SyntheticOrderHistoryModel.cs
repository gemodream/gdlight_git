﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticOrderHistoryModel
	{
		public SyntheticOrderHistoryModel() { }

		public int OrderCode { get; set; }
		public int BatchCode { get; set; }
		public int ItemQty   { get; set; }
		public int AssignedTo { get; set; }
		public int StatusId { get; set; }
		public int BatchScreenerID { get; set; }
		public int BatchScreeningInstrumentID { get; set; }
	}
}