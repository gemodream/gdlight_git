﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureItemModel
    {
        #region All columns spGetKMLDFFByItemNumber
        /*
	        BatchID, 
	        ItemCode,

	        MeasureId, 
	        MeasureCode, 
	        MeasureName, 
	        MeasureClass, 
	        MeasureTitle, 
	        MeasureValueName, 
	        MeasureValueOrder, 
	        MeasureValueMeasureId, 
	        MeasureValueId, 
	        MeasureValue, 

	        ValueTitle, 
	        ValueCode, 
	        StringValue, 

	        ReportName, 
	        ShortReportName,
	        LongReportName, 
	        ShapePath2Drawing,
	        RapaportName,

	        PartId, 
	        PartValueHistoryId,
	        ExpireDate,
	        IsClosed,
	        RecheckSessionId, 
	        RecheckStartDate, 
	        RecheckNumber,

	        CreateDate, 
	        LastModifiedDate, 
         */
        #endregion
        public MeasureItemModel(DataRow drKm, ItemModel itemModel)
        {
            OrderCode = itemModel.OrderCode;
            BatchCode = "" + itemModel.BatchCode;
            MeasureValueName = "" + drKm["MeasureValueName"];
        }
        public string ItemNumber
        {
            get { return Utils.FullItemNumber(BatchCode, OrderCode, ItemCode); }
        }
        public string ItemNumberWithDots
        {
            get { return Utils.GetItemNumberWithDots(ItemNumber); }
        }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public string MeasureValueName { get; set; }
        public bool HasValue
        {
            get { return Utils.NullValue(MeasureValueName) != ""; }
        }
        public string DisplayMessage
        {
            get
            {
                return "Found: " + ItemNumberWithDots + ":" + MeasureValueName + "<br />";
            }
        }
    }
}