﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class UpdateMeasureModel
    {
        public UpdateMeasureModel()
        {
        }
        public MeasureValueModel MeasureValue { get; set; }
        public SingleItemModel Item { get; set; }
    }
}