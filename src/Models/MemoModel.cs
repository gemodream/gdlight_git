﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MemoModel
    {
        public MemoModel()
        {
        }

        /// <summary>
        /// sp_GetMemoNumbersByCustomerByDateRange
        /// </summary>
        /// <param name="row"></param>
        public MemoModel(DataRow row)
        {
            MemoNumber = "" + row["MemoNumber"];
        }
        public string MemoNumber { get; set; }
        public string MemoId { get; set; }
        /// <summary>
        /// spGetGroupMemoNumber
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static MemoModel Create1(DataRow row)
        {
            return new MemoModel { MemoNumber = "" + row["Name"], MemoId = "" + row["MemoNumberID"] };
        }
    }
}