﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class UpdateOrderEditModel
    {
        public UpdateOrderEditModel()
        {
        }

        public string BatchId { get; set; }
        public string ItemCode { get; set; }
        public string FullItemNumber { get; set; }
        public string DocKey { get; set; }
        public string DocChar { get; set; }
        public string CpName { get; set; }
        public string UniqueKey { get { return string.Format("{0},{1},{2}", BatchId, ItemCode, DocKey); } }
    }
}