﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class PrintLblPaintModel
    {
        public PrintLblPaintModel()
        {
        }

        public string CellName { get; set; }
        public string CellValue { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
    }
}