﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class InvoiceModel
    {
        public InvoiceModel(DataRow row)
        {
            InvoiceNumber = string.IsNullOrEmpty("" + row["QBInvoiceNumber"]) ? "N/A" : "" + row["QBInvoiceNumber"];
            GroupCode = "" + row["Groupcode"];
            BatchCode = "" + row["BatchCode"];
            BeginDate = "" + row["BeginDate"].ToString();
            status = "" + row["status"];
        }
        public string InvoiceNumber { get; set; }
        public string GroupCode { get; set; }
        public string BatchCode { get; set; }
        public string BeginDate { get; set; }
        public string status { get; set; }
    }
}