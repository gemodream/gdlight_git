﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DropDownNamesDocMergeModel
    {
        public DropDownNamesDocMergeModel(){}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        public DropDownNamesDocMergeModel(DataRow row)
        {
            DocumentID = Convert.ToInt32(row[nameof(DocumentID)]);
            DocumentName = "" + row[nameof(DocumentName)];
        }

        public string DocumentName { get; set; }
        public int DocumentID { get; set; }
        public string IsLabel { get; set; }
        public string DropDownValue { get; set; }
    }
}