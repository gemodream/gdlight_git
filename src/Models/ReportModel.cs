﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    public class ReportModel
    {
        public ReportModel(ReportModel other)
        {
            BatchKey = other.BatchNumber;
            IsFooter = true;
            Reports = string.Format("## in the batch: {0} / ## pass: {1}", other.ItemsCount, other.ReportsCount);
        }
        public ReportModel()
        {
            
        }
        public ReportModel(DataRow row)
        {
            BatchNumber     = "" + row["BatchNumber"];
            BatchKey = BatchNumber;
            ItemNumber      = "" + row["ItemNumber"];
            Reports         = "" + row["Reports"];
            ReportsDate     = "" + row["ReportDate"];
            OldItemNumber   = "" + row["OldItemNumber"];
            ItemStatus      = "" + row["Item_Status"];
            IsFooter = false;
        }
        public bool IsPassed
        {
            get { return ItemStatus == "Pass"; }
        }
        public bool IsRejected
        {
            get { return ItemStatus == "Rejected"; }
        }
        public static ReportModel CreateFooter(List<ReportModel> grpModel)
        {
            var batchNumber = grpModel[0].BatchNumber;
            var itemsCount = grpModel.Count;
            var passCount = grpModel.FindAll(m => m.IsPassed).Count;
            return new ReportModel
            {
                BatchKey = batchNumber, 
                IsFooter = true, 
                ItemsCount = itemsCount,
                ReportsCount = passCount,
                Reports = string.Format("## in the batch: {0} / ## pass: {1}", itemsCount, passCount)
            };

        }
        public string SortExpr
        {
            get { return BatchKey + (IsFooter ? "_" : ItemNumber); }
        }
        public string BatchKey { get; set; }
        private int ParseToInt(object value)
        {
            if (value == null || value.ToString().Trim() == "") return 0;
            try
            {
                return Int32.Parse(value.ToString());
            } catch
            {
                return 0;
            }
        }
        public string BatchNumber { get; set; }
        public string ItemNumber { get; set; }
        public string OldItemNumber { get; set; }
        public string Reports { get; set; }
        public string ReportsDisplay
        {
            get { return ItemStatus == "Rejected" ? "No reports ordered" : Reports; }
        }
        public int ItemsCount { get; set; }
        public int ReportsCount { get; set; }
        public string ReportsBatch { get; set; }
        public string ReportsDate { get; set; }
        public string ItemStatus { get; set; }
        public bool IsFooter { get; set; }
//        public int NewBatchId { get; set; }
//        public int NewItemCode { get; set; }
    }
}