﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class DirFileModel
    {
        public DirFileModel(string name, bool isDir)
        {
            Name = name;
            IsDir = isDir ? "D" : "F";
        }

        public string Name { get; set; }
        public string IsDir { get; set; }
        public string DisplayName
        {
            get
            {
                return IsDir + ": " + Name;
            }
        }
    }

}