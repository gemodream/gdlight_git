﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BlockModel
    {
        public BlockModel(){}
        /// <summary>
        /// spGetDropDownBlockList
        /// </summary>
        /// <param name="row"></param>
        public BlockModel(DataRow row)
        {
            BlockedDisplayName = "" + row[nameof(BlockedDisplayName)];
            Id = (int)row[nameof(Id)];
            DropDownId = (int)row[nameof(DropDownId)];

        }

        public string BlockedDisplayName { get; set; }
        public int Id { get; set; }
        public int DropDownId { get; set; }
    }
}