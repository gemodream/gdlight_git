﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticBatchModel
	{
		public SyntheticBatchModel() { }

		public int BatchID { get; set; }
		public int BatchCode { get; set; }
		public int OrderCode { get; set; }
		public int Quantity { get; set; }
		public int StatusID { get; set; }
		public int ScreenerID { get; set; }
		public int ScreeningInstrumentID { get; set; }
		public int ScreenerPassItems { get; set; }
		public int ScreenerFailItems { get; set; }
		public int ScreenerFailedStones { get; set; }
		public string ScreenerFailedStonesPerItem { get; set; }
		public string ScreenerComments { get; set; }
		public int TesterID { get; set; }
		public int TestingInstrumentID { get; set; }
		public int TesterPassItems { get; set; }
		public int TesterFailItems { get; set; }
		public int TesterSyntheticStones { get; set; }
		public int TesterSuspectStones { get; set; }
		public int TesterFailedStones { get; set; }
		public string TesterFailedStonesPerItem { get; set; }
		public string TesterComments { get; set; }
		public int PackingTotalItemsPerOrder { get; set; }		
		public int PackingTotalItemsFailPerOrder { get; set; }
		public int PackingTotalStonesFailPerOrder { get; set; }
        public int PackingTotalBlueBagsPerOrder { get; set; }
        public string PackingComment { get; set; }
		public string PackingBillingInfo { get; set; }
		public int PackerID { get; set; }
		public int UpdateID { get; set; }
	}
}
