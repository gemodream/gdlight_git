﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BatchNewModel
    {
        public BatchNewModel()
        {
        }
        /// <summary>
        /// sp_GetBatchInfoFromList
        /// </summary>
        /// <param name="row"></param>
        public BatchNewModel(DataRow row)
        {
            //Form = "19";
            //Action = "5";
            BatchId = Int32.Parse("" + row["BatchID"]);
            ItemsAffected = string.IsNullOrEmpty("" + row["StoredItemsQuantity"]) ? 
                0 : Int32.Parse("" + row["StoredItemsQuantity"]);
            OrderCode = "" + row["OrderCode"];
            BatchCode = "" + row["BatchCode"];
            CpName = "" + row["CustomerProgramName"];
        }

        public int BatchId { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string CpName { get; set; }
        public int ItemsAffected { get; set; }

    }
}