﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class OpenBatchModel
    {
        /// <summary>
        /// sp_ListOpenBatchesWithStatus
        /// </summary>
        /// <param name="row"></param>
        public OpenBatchModel(DataRow row)
        {
            OrderCode = Convert.ToString(row["OrderCode"]);
            BatchCode = Convert.ToString(row["BatchCode"]);
            QBInvoiceNumber = Convert.ToString(row["QBInvoiceNumber"]);
            NumberOfItems = Convert.ToInt32(row["NumberOfItems"]);
            Customer = Convert.ToString(row["CustomerName"]);
            OrderDate = Convert.ToDateTime(row["OrderCreationDate"]);
            CustomerOfficeName = Convert.ToString(row["CustomerOfficeName"]);
            Status = Convert.ToString(row["Status"]);
        }

        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public int NumberOfItems { get; set; }
        public string Customer { get; set; }
        public string CustomerOfficeName { get; set; }
        // ReSharper disable once InconsistentNaming
        public string QBInvoiceNumber { get; set; }
        public string Status { get; set; }
        public DateTime OrderDate { get; set; }
        public string DisplayOrder { get { return Utils.FillToFiveChars(OrderCode); } }
        public string DisplayBatch { get { return Utils.FillToThreeChars(BatchCode, OrderCode); } }
        public string IconStatus
        {
            get
            {
                if (string.IsNullOrEmpty(Status) )
                {
                    //return "<img src=\"Images/red_circle.jpg\" width=\"20\" height=\"20\" border=\"0\"  />";
                    return "Images/red_circle.jpg";
                }
                if (Status == "Completed")
                {
                    //return "<img src=\"Images/green_circle.jpg\" width=\"20\" height=\"20\" border=\"0\" />";
                    return "Images/green_circle.jpg";
                }
                //return "<img src=\"Images/yellow_circle.jpg\" width=\"20\" height=\"20\" border=\"0\" />";
                return "Images/yellow_circle.jpg";
            }
        }
    }
}