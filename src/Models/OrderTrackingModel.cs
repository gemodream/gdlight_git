﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	public class OrderTracking
	{
		public int CustomerID { get; set; }
		public int AuthorOfficeID { get; set; }
        public string SrNo { get; set; }
        public string RequestID { get; set; }
		public string RequestDate { get; set; }
		public string GSIOrder { get; set; }
		public string TakeinDate { get; set; }
		public string Status { get; set; }
		public string GiveOutDate { get; set; }
		public string PackingDate { get; set; }
		public string Hours { get; set; }
		public string ServiceTypeName { get; set; }
		public string TimezoneOffset { get; set; }
		public string CustomerName { get; set; }
        public string TotalQty { get; set; }
        public string TotalWeight { get; set; }
        public string MeasureUnit { get; set; }
        public string SKUName { get; set; }
        public string StyleName { get; set; }
        public string RetailerName { get; set; }
        public string SpecialInstructions { get; set; }
        public DateTime? TakeinDateFrom { get; set; }
		public DateTime? TakeinDateTo { get; set; }
        public int RealOfficeID { get; set; }
        public string OfficeName { get; set; }        
    }
}