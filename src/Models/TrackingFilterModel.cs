﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingFilterModel
    {
        public TrackingFilterModel()
        {
        }

        public int EventId { get; set; }
        public int CustomerId { get; set; }
        public int CustomerOfficeId { get; set; }
        public int ViewAccessId { get; set; }
        public bool ShowCheckedOut { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int AgeCurrent { get; set; }
        
    }
}