﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemValueModel
    {
        public string BatchId { get; set; }
        public string NewBatchId { get; set; }
        public string NewItemNumber{ get; set; }
        public string NewItemCode{ get; set; }
        public string OldItemNumber{ get; set; }
        public string MeasureCode{ get; set; }
        public string MeasureName{ get; set; }
        public string ValueCode{ get; set; }
        public string ResultValue{ get; set; }
        public string PartId{ get; set; }
        public string PartName { get; set; }
        public string MeasureId { get; set; }
        public string Cp { get; set; }
        public bool IsCpOne
        {
            get { return Cp == "1"; }
        }
        public string SortExpr
        {
            get { return PartId + MeasureName; }
        }

        /// <summary>
        /// From spGetItemDataFromOrderbatchItem
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemValueModel Create1(DataRow row)
        {
            return new ItemValueModel
                       {
                           BatchId = "" + row["BatchID"],
                           NewBatchId = "" + row["NewBatchID"],
                           NewItemNumber = "" + row["NewItemNumber"],
                           NewItemCode = "" + row["NewItemCode"],
                           OldItemNumber = "" + row["OldItemNumber"],
                           MeasureCode = "" + row["MeasureCode"],
                           MeasureName = "" + row["MeasureName"],
                           ValueCode = "" + row["ValueCode"],
                           ResultValue = "" + row["ResultValue"],
                           PartId = "" + row["PartId"],
                           PartName = "" + row["PartName"],
                       };            
        }
        /// <summary>
        /// From spGetItemDataFromOrderbatchItemAndCP
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemValueModel Create2(DataRow row)
        {
            return new ItemValueModel
            {
                BatchId = "" + row["BatchID"],
                NewBatchId = "" + row["NewBatchID"],
                NewItemNumber = "" + row["NewItemNumber"],
                NewItemCode = "" + row["NewItemCode"],
                OldItemNumber = "" + row["OldItemNumber"],
                MeasureCode = "" + row["MeasureCode"],
                MeasureName = "" + row["MeasureName"],
                ValueCode = "" + row["ValueCode"],
                ResultValue = "" + row["ResultValue"],
                PartId = "" + row["PartId"],
                PartName = "" + row["PartName"],
                Cp = "" + row["CP"]
            };
        }
        /// <summary>
        /// spGetPartValueByBatchIDItemCode
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemValueModel Create3(DataRow row)
        {
            return new ItemValueModel()
            {
                PartId = "" + row["PartID"],
                MeasureName = "" + row["MeasureName"],
                ResultValue = "" + row["ResultValue"],
                PartName = "" + row["PartName"]
            };
        }
        public static ItemValueModel Create4(DataRow row)
        {
            return new ItemValueModel
            {
                PartId = "" + row["PartID"],
                BatchId = "" + row["BatchID"],
                NewItemCode = "" + row["ItemCode"],
                ResultValue = "" + row["StringValue"],
                MeasureId = "" + row["MeasureID"]

            };
        }

        public static ItemValueModel Create5(DataRow row)
        {
            return new ItemValueModel
            {
                PartId = "" + row["PartID"],
                MeasureName = "" + row["MeasureName"],
                ResultValue = "" + row["ResultValue"],
                PartName = "" + row["PartName"],
                NewItemNumber = "" + row["NewItemNumber"],
                MeasureCode = "" + row["MeasureCode"] 

            };
        }

    }
}