﻿using System;
using System.Data;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class DocumentModel
    {
        public DocumentModel()
        {
            DocumentTypeCode = DbConstants.DocumentTypeCodeLblReport;
            DocumentId = "0";
            DocumentName = "Default";
        }

        /// <summary>
        /// spGetDefaultDocumentTypeCodeByBatchID (exp. 3657)
        /// </summary>
        /// <param name="row"></param>
        public DocumentModel(DataRow row)
        {
            DocumentTypeCode = "" + row["DocumentTypeCode"];
            DocumentId = "" + row["DocumentId"];
            DocumentName = "" + row["DocumentName"];
            CorelFile = "" + row["CorelFile"];
        }

        public string DocumentTypeCode { get; set; }
        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string CorelFile { get; set; }
    }
}