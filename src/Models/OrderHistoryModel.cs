﻿using System;
using System.Collections.Generic;
using Corpt.Models.Customer;

namespace Corpt.Models
{
    [Serializable]
    public class OrderHistoryModel
    {
        public OrderHistoryModel()
        {
        }

        public string OrderCode { get; set; }
        public List<CustomerHistoryModel> HistoryItems { get; set; }
    }
}