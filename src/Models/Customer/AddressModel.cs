﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class AddressModel
    {
        public AddressModel()
        {
            //Country = "USA";
            //CountryPhoneCode = "+1";
            //CountryFaxCode = "+1";
            //CountryCellCode = "+1";
        }
        public AddressModel(DataRow row)
        {
            AddressId = "" + row["AddressID"];
            Country = "" + row["Country"];
            City = "" + row["City"];
            CountryPhoneCode = "" + row["CountryPhoneCode"];
            Phone = "" + row["Phone"];
            ExtPhone = "" + row["ExtPhone"];
            CountryFaxCode = "" + row["CountryFaxCode"];
            Fax = "" + row["Fax"];
            CountryCellCode = "" + row["CountryCellCode"];
            Cell = "" + row["Cell"];
            Email = "" + row["Email"];
            Address1 = "" + row["Address1"];
            Address2 = "" + row["Address2"];
            Zip1 = "" + row["Zip1"];
            Zip2 = "" + row["Zip2"];
            UsStateId = "" + row["USStateID"];
        }

        public string AddressId { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string UsStateId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Zip1 { get; set; }
        public string Zip2 { get; set; }

        public string Phone { get; set; }
        public string ExtPhone { get; set; }
        public string CountryPhoneCode { get; set; }

        public string Fax { get; set; }
        public string CountryFaxCode { get; set; }

        public string Cell { get; set; }
        public string CountryCellCode { get; set; }
        public string Email { get; set; }
        public bool IsUsa { get { return Country == "USA"; } }
    }
}