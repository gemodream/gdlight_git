﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CompanyModel
    {
        public CompanyModel()
        {
            Address = new AddressModel();
            GoodsMovement = new GoodsMovementModel();
            Communication = "f0p0e0m0";
        }
        public CompanyModel(DataRow row)
        {
            Address = new AddressModel(row);
            CompanyName = "" + row["CompanyName"];
            ShortName = "" + row["ShortName"];
            GoodsMovement = new GoodsMovementModel(row);
            UseTheirAccount = ("" + row["UseTheirAccount"]) == "1";
            Account = "" + row["Account"];
            BusinessTypeId = "" + row["BusinessTypeID"];
            IndustryMembership = "" + row["IndustryMembership"];
            Permissions = "" + row["Permission"];
            CarrierId = "" + row["CarrierID"];
            Account = "" + row["Account"];
            Communication = "" + row["Communication"];
        }
        public string CompanyName { get; set; }
        public string ShortName { get; set; }
        public AddressModel Address { get; set; }
        public GoodsMovementModel GoodsMovement { get; set; }
        public bool UseTheirAccount { get; set; }
        public string Account { get; set; }
        public string BusinessTypeId { get; set; }
        public string IndustryMembership { get; set; }
        public string Permissions { get; set; }
        public string CarrierId { get; set; }
        public string Communication { get; set; }
    }
}