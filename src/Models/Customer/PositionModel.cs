﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class PositionModel
    {
        public PositionModel()
        {
            Id = "";
            Name = "";
        }

        /// <summary>
        /// spGetPositions
        /// </summary>
        /// <param name="row"></param>
        public PositionModel(DataRow row)
        {
            Id = "" + row["PositionID"];
            Code = Convert.ToInt32(row["PositionCode"]);
            Name = "" + row["PositionName"];
        }

        public string Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}