﻿using System;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class GoodMovementModel
    {
        public GoodMovementModel()
        {
        }

        public string Id { get; set; }
        public string Name { get; set; }
    }
}