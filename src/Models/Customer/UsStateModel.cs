﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class UsStateModel
    {
        public UsStateModel()
        {
            StateId = "";
            State = "";
        }
        /// <summary>
        /// spGetUSStates
        /// </summary>
        /// <param name="row"></param>
        public UsStateModel(DataRow row)
        {
            StateId = "" + row["USStateID"];
            State = ("" + row["State"]).Trim();
        }
        public string StateId { get; set; }
        public string State { get; set; }
    }
}