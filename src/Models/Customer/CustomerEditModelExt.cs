﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models.Customer
{
	[Serializable]
	public class CustomerEditModelExt : CustomerEditModel
	{
		public CustomerEditModelExt()
		{
			Persons = new List<PersonExModel>();
			Company = new CompanyModelExt();
			CreateDate = DateTime.Now;
		}
		public CustomerEditModelExt(DataRow row)
		{
			Company = new CompanyModelExt(row);
			CustomerId = "" + row["CustomerID"];
			CustomerHistoryId = "" + row["CustomerHistoryID"];
			CustomerName = "" + row["CustomerName"];
			CustomerCode = "" + row["CustomerCode"];
			CustomerOfficeId = "" + row["CustomerOfficeID"];
			StateId = "" + row["StateID"];
			StateTargetId = "" + row["StateTargetID"];
			CreateDate = Convert.ToDateTime(row["CreateDate"]);
			Persons = new List<PersonExModel>();
		}

		public new CompanyModelExt Company { get; set; }
	}
}