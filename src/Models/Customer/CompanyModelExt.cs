﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CompanyModelExt : CompanyModel
	{
		public List<LimitCarrierModel> Carriers { get; set; }
		public CompanyModelExt()
		{
			Address = new AddressModel();
			GoodsMovement = new GoodsMovementModel();
			Communication = "f0p0e0m0";
			Carriers = new List<LimitCarrierModel>();
		}
		public CompanyModelExt(DataRow row)
		{
			Address = new AddressModel(row);
			CompanyName = "" + row["CompanyName"];
			ShortName = "" + row["ShortName"];
			GoodsMovement = new GoodsMovementModel(row);
			UseTheirAccount = ("" + row["UseTheirAccount"]) == "1";
			Account = "" + row["Account"];
			BusinessTypeId = "" + row["BusinessTypeID"];
			IndustryMembership = "" + row["IndustryMembership"];
			Permissions = "" + row["Permission"];
			Communication = "" + row["Communication"];
			Carriers = new List<LimitCarrierModel>();
		}
		//TODO create function which allows you to input all the carriers at once
		public void AddCarrier(DataRow row)
		{
			Carriers.Add(new LimitCarrierModel(row));
		}
	}
}