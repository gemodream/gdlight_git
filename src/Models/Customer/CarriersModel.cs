﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CarriersModel
    {
        public CarriersModel()
        {
        }

        /// <summary>
        /// spGetCarriers
        /// </summary>
        /// <param name="row"></param>
        public CarriersModel(DataRow row)
        {
            Id = "" + row["CarrierID"];
            Code = Convert.ToInt32(row["CarrierCode"]);
            Name = "" + row["CarrierName"];
        }

        public string Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}