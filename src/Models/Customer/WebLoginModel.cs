﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class WebLoginModel
    {
        public WebLoginModel()
        {
            UniqueKey = "";
        }
        public WebLoginModel(DataRow row)
        {
            LoginName = "" + row["WebLogin"];
            Password = "" + row["WebPwd"];
            UniqueKey = "" + row["PersonCustomerOfficeID_PersonCustomerID_PersonID"];   //CustomerOfficeId + CustomerId + PersonI: 1_611_992
        }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string UniqueKey { get; set; }

    }
}