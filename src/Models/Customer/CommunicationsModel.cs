﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CommunicationsModel
    {
        public CommunicationsModel()
        {
            Items = new List<CommunicationModel>
                        {
                            new CommunicationModel{Name="Fax", Code = "f"},
                            new CommunicationModel{Name="Phone", Code = "p"},
                            new CommunicationModel{Name="Email", Code = "e"},
                            new CommunicationModel{Name="Mail", Code = "m"}
                        };
        }
        public List<CommunicationModel> Items { get; set; }
        public void SetDbValue(string value)
        {
            var cnt = Convert.ToInt32(value.Length/2);
            for (int i=0; i < cnt; i++)
            {
                var code = value.Substring(i*2, 1);
                var chk = value.Substring(i*2 + 1, 1);
                var item = Items.Find(m => m.Code == code);
                if (item == null) continue;
                item.Order = i;
                item.Checked = chk == "1";
            }
        }
    }
}