﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models.Customer
{
	public class PersonServiceModel
	{
		public PersonServiceModel()
		{

		}
		public PersonServiceModel(DataRow row)
		{
			ServiceID = Convert.ToInt32(row["ServiceID"]);
			ServiceName = "" + row["ServiceName"];
		}
		public int ServiceID { get; set; }
		public string ServiceName { get; set; }
		public int PersonID { get; set; }
		public string ServiceIDList { get; set; }
	}
}