﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class AuthorModel
    {
        public AuthorModel()
        {
            Login = "";
            FirstName = "";
            LastName = "";
            AuthorOfficeIdAuthorId = "";
        }
        public AuthorModel(DataRow row)
        {
            Login = "" + row["Login"];
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
            AuthorOfficeIdAuthorId = "" + row["AuthorOfficeID_AuthorID"];
        }

        public string AuthorOfficeIdAuthorId { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get { return FirstName.Trim() + " " + LastName.Trim(); } }
    }
}