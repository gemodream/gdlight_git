﻿using System;
using System.Collections.Generic;
using System.Data;
using Corpt.Constants;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CustomerHistoryModel
    {
        public CustomerHistoryModel()
        {
            StateTarget = new StateTargetModel();
        }

        /// <summary>
        /// Order,      spGetGroupByCode
        /// Batch,      spGetBatchByCode
        /// Item,       spGetItemByCode
        /// Document,   spGetItemDocByCode
        /// </summary>
        /// <param name="row"></param>
        /// <param name="targetCode">StateTargetCode</param>
        /// <param name="states"> </param>
        /// <param name="customerCode"> </param>
        public CustomerHistoryModel(DataRow row, int targetCode, List<StateTargetModel> states, string customerCode)
        {
            IsInvalidItem = false;
            TargetCode = targetCode;
            var iconIndex = "" + row["IconIndex"];
            StateTarget = states.Find(m => m.IconIndex == iconIndex);
            CustomerCode = customerCode;

            if (targetCode == StateTargetModel.TrgCodeOrder)
            {
                FillOrderModel(row);                
            }
            if (targetCode == StateTargetModel.TrgCodeBatch)
            {
                FillBatchModel(row);
            }
            if (targetCode == StateTargetModel.TrgCodeItem)
            {
                FillItemModel(row);
            }
            if (targetCode == StateTargetModel.TrgCodeDocument)
            {
                FillDocumentModel(row);
            }
        }

        public CustomerHistoryModel(CustomerModel customer, List<StateTargetModel> states)
        {
            TargetCode = StateTargetModel.TrgCodeCustomer;
            StateTarget = states.Find(m => m.IconIndex == customer.IconIndex);
            ParentId = "";
            Id = customer.CustomerCode;
            Title = "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " + customer.CustomerName;
            CustomerCode = customer.CustomerCode;
        }

        public string CustomerCode { get; set; }
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Title { get; set; }
        public int TargetCode { get; set; }
        public StateTargetModel StateTarget { get; set; }
        public string BatchId { get; set; } 
        private void FillOrderModel(DataRow row)
        {
            var id = Utils.FillToFiveChars("" + row["OrderCode"]);
            ParentId = CustomerCode;
            Id = CustomerCode + "_" + id;
            var date = Convert.ToDateTime(row["CreateDate"]);
            var memo = "" + row["Memo"];
            Title = "<img alt='' src='" + StateTarget.ImageUrl + "' title='" + StateTarget.ImageTooltip + "'/> " +
                id + 
                string.Format(" ({0:G})", date) + 
                (string.IsNullOrEmpty(memo) ? "" : string.Format(" (Memo: {0})", memo));
        }
        private void FillBatchModel(DataRow row)
        {
            ParentId = CustomerCode + "_" + Utils.FillToFiveChars("" + row["OrderCode"]);
            var id = Utils.FillToFiveChars("" + row["OrderCode"]) + "." + Utils.FillToThreeChars("" + row["BatchCode"], "" + row["OrderCode"]);
            Id = CustomerCode + "_" + id;
            var memo = "" + row["MemoNumber"];
            Title = "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " + 
                id + (string.IsNullOrEmpty(memo) ? "" : string.Format(" (Memo: {0})", memo));
            BatchId = "" + row["BatchID"];
        }
        public string TitleBatch
        {
            get
            {
                var id = Id.Split('_')[1];
                return "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " +
                id + (string.IsNullOrEmpty(DocumentName) ? "" : string.Format(" ({0})", DocumentName));
            }
        }
        private void FillItemModel(DataRow row)
        {
            var parentId = Utils.FillToFiveChars("" + row["OrderCode"]) + "." + Utils.FillToThreeChars("" + row["BatchCode"], "" + row["OrderCode"]);
            var id = parentId + "." + Utils.FillToTwoChars("" + row["ItemCode"]);

            var prevItem = Utils.FillToFiveChars("" + row["PrevOrderCode"]) + "." +
                           Utils.FillToThreeChars("" + row["PrevBatchCode"], "" + row["PrevOrderCode"]) + "." + 
                           Utils.FillToTwoChars("" + row["PrevItemCode"]);
            OldFullItemNumber = prevItem.Replace(".", "");
            BatchId = "" + row["NewBatchID"];
            var weight = "" + row["Weight"];
            weight += string.IsNullOrEmpty(weight) ? "" : " " + row["WeightUnitName"];
            
            var colorClarity = row["Color"] + "/" + row["Clarity"];

            Title = "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " + id + 
                (string.IsNullOrEmpty(weight) ? "" : string.Format(" {0}", weight)) + 
                (colorClarity == "/" ? "" : string.Format(" ({0})", colorClarity)) +
                (id == prevItem ? "" : string.Format(" (Old # {0})", prevItem));
            ParentId = CustomerCode + "_" + parentId;
            Id = CustomerCode + "_" + id;
            var stateId = Convert.ToInt32(row["StateID"]);
            IsInvalidItem = (stateId == DbConstants.ItemInvalidStateId);

        }

        public bool IsInvalidItem { get; set; }
        public string OldFullItemNumber { get; set; }   //-- For Item level only
        private void FillDocumentModel(DataRow row)
        {
            var parentId = 
                Utils.FillToFiveChars("" + row["GroupCode"]) + "." + 
                Utils.FillToThreeChars("" + row["BatchCode"], "" + row["GroupCode"]) + "." + 
                Utils.FillToTwoChars("" + row["ItemCode"]);
            var id = row["OperationChar"] + parentId;
            Title = "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " + id;
            ParentId = CustomerCode + "_" + parentId;
            Id = CustomerCode + "_" + id;
            
        }

        public string DocumentName { get; set; }
    }
}