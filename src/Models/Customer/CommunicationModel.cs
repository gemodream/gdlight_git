﻿using System;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CommunicationModel
    {
        public CommunicationModel()
        {
        }

        public int Order { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Checked { get; set; }
    }
}