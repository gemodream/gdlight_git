﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class PermissionsModel
    {
        public PermissionsModel()
        {
            InitItems();
        }
        public PermissionsModel(DataRow row)
        {
            InitItems();
            SetDbValue("" + row["Permission"]);
        }
        private void InitItems()
        {
            Items = new List<PermissionModel>
            {
                new PermissionModel {Id = 1, Name = "Order"},
                new PermissionModel {Id = 2, Name = "Pick Up"},
                new PermissionModel {Id = 3, Name = "Submit"},
                new PermissionModel {Id = 4, Name = "Change Order"},
                new PermissionModel {Id = 5, Name = "Check Order status On-line"},
                new PermissionModel {Id = 6, Name = "Check Results On-line"},
                new PermissionModel {Id = 7, Name = "Place Order Online"},
                new PermissionModel {Id = 8, Name = "Change Order Online"}
            };
        }
        public List<PermissionModel> Items { get; set; }
        public void SetDbValue(string values)
        {
            var items = values.Split(',');
            foreach (var item in items)
            {
                if (string.IsNullOrEmpty(item)) return;
                var permisiion = Items.Find(m => "" + m.Id == item.Trim());
                if (permisiion != null) permisiion.IsChecked = true;
            }
        }
    }
}