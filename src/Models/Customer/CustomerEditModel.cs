﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CustomerEditModel
    {
        public CustomerEditModel()
        {
            Persons = new List<PersonExModel>();
            Company = new CompanyModel();
            CreateDate = DateTime.Now;
        }
        public CustomerEditModel(DataRow row)
        {
            Company = new CompanyModel(row);
            CustomerId = "" + row["CustomerID"];
            CustomerHistoryId = "" + row["CustomerHistoryID"];
            CustomerName = "" + row["CustomerName"];
            CustomerCode = "" + row["CustomerCode"];
            CustomerOfficeId = "" + row["CustomerOfficeID"];
            StateId = "" + row["StateID"];
            StateTargetId = "" + row["StateTargetID"];
            CreateDate = Convert.ToDateTime(row["CreateDate"]);
            Persons = new List<PersonExModel>();
        }

        public string StateId { get; set; }
        public string StateTargetId { get; set; }
        public string CustomerHistoryId { get; set; }
        public CompanyModel Company { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string CustomerOfficeId { get; set; }
        public string CustomerCode { get; set; }
        public DateTime CreateDate { get; set; }
        public List<PersonExModel> Persons { get; set; }
    }
}