﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models.Customer
{
	[Serializable]
	public class KYCModel
	{
		public KYCModel()
		{
		}
		public KYCModel(DataRow row)
		{
			KycID = Convert.ToInt32(row["KycID"]);
			KYCDate = "" + row["KYCDate"];
			YearofEstd = "" + row["YearofEstd"];
			IsDomestic = Convert.ToBoolean(row["IsDomestic"]);
			IsExistingCustomer = Convert.ToBoolean(row["IsExistingCustomer"]);
			AddressType = "" + row["AddressType"];
			CompanyName = "" + row["CompanyName"];
			OwnerName = "" + row["OwnerName"];
			Address1 = "" + row["Address1"];
			Address2 = "" + row["Address2"];
			City = "" + row["City"];
			Zip = "" + row["Zip"];
			State = "" + row["State"];
			CityCode = "" + row["CityCode"];
			Tel1 = "" + row["Tel1"];
			Tel2 = "" + row["Tel2"];
			Mobile = "" + row["Mobile"];
			FAX = "" + row["FAX"];
			Email = "" + row["Email"];
			Website = "" + row["Website"];
			Representative = "" + row["Representative"];
			Designation = "" + row["Designation"];
			Memberof = "" + row["Memberof"];
			Ref1 = "" + row["Ref1"];
			RefContact1 = "" + row["RefContact1"];
			Ref2 = "" + row["Ref2"];
			RefContact2 = "" + row["RefContact2"];
			PAN = "" + row["PAN"];
			GSTNo = "" + row["GSTNo"];
			GSTwef = "" + row["GSTwef"];
			PhotoIDProof = "" + row["PhotoIDProof"];
			CompanyTypeID =  Convert.ToInt32(row["CompanyTypeID"]);
			BusinessTypeID = Convert.ToInt32(row["BusinessTypeID"]);
			DistributionID =  Convert.ToInt32(row["DistributionID"]);
			CompanyBrief = "" + row["CompanyBrief"];
			CustomerID = Convert.ToInt32(row["CustomerID"]);
			CustomerImg = "" + row["CustomerImg"];
		}
		public int KycID { get; set; }
		public string KYCDate { get; set; }
		public string YearofEstd { get; set; }
		public bool IsDomestic { get; set; }
		public bool IsExistingCustomer { get; set; }
		public string AddressType { get; set; }
		public string CompanyName { get; set; }
		public string OwnerName { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string Zip { get; set; }
		public string State { get; set; }
		public string CityCode { get; set; }
		public string Tel1 { get; set; }
		public string Tel2 { get; set; }
		public string Mobile { get; set; }
		public string FAX { get; set; }
		public string Email { get; set; }
		public string Website { get; set; }
		public string Representative { get; set; }
		public string Designation { get; set; }
		public string Memberof { get; set; }
		public string Ref1 { get; set; }
		public string RefContact1 { get; set; }
		public string Ref2 { get; set; }
		public string RefContact2 { get; set; }
		public string PAN { get; set; }
		public string GSTNo { get; set; }
		public string GSTwef { get; set; }
		public string PhotoIDProof { get; set; }
		public int CompanyTypeID { get; set; }
		public int BusinessTypeID { get; set; }
		public int DistributionID { get; set; }
		public string CompanyBrief { get; set; }
		public int CustomerID { get; set; }
		public string CustomerImg { get; set; }
	}

}