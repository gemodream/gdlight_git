﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class PersonExModel : PersonModel
    {
        public PersonExModel()
        {
            Position = new PositionModel();
            Communication = "f0p0e0m0";
            CreateDate = DateTime.Now;
            WebLogin = new WebLoginModel();
            Address = new AddressModel();
        }

        /// <summary>
        /// spGetPersonsByCustomer
        /// </summary>
        /// <param name="row"></param>
        public PersonExModel(DataRow row)
        {
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
            PersonCode = "" + row["PersonCode"];
            PersonId = Convert.ToInt32("" + row["PersonID"]);
            if (!string.IsNullOrEmpty("" + row["BirthDate"])) BirthDate = Convert.ToDateTime(row["BirthDate"]);
            Path2Photo = "" + row["Path2PhotoFile"];
            Path2Signature = "" + row["Path2StoredSignature"];
            CreateDate = Convert.ToDateTime(row["CreateDate"]);

            Address = new AddressModel(row);
            Position = new PositionModel(row);
            Communication = "" + row["Communication"];
            Permissions = "" + row["Permission"];
            WebLogin = new WebLoginModel(row);
            PersonHistoryId = "" + row["PersonHistoryID"];
            CustomerOfficeId_CustomerId = "" + row["CustomerOfficeID_CustomerID"];
        }

        public string PersonCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public CustomerModel Customer { get; set; }
        public AddressModel Address { get; set; }
        public PositionModel Position { get; set; }
        public DateTime CreateDate { get; set; }
        public WebLoginModel WebLogin { get; set; }
        public string Permissions { get; set; }
        public string Communication { get; set; }
        public string Path2Photo { get; set; }
        public string Path2Signature { get; set; }
		public string AuthFormImg { get; set; }

		public string PersonHistoryId { get; set; }
        public string CustomerOfficeId_CustomerId { get; set; }
        public string DisplayPhone
        {
            get { return Address.CountryPhoneCode + Address.Phone; }
        }
        public string DisplayFax
        {
            get { return Address.CountryFaxCode + Address.Fax; }
        }
        public string DisplayCell
        {
            get { return Address.CountryCellCode + Address.Cell; }
        }
        public string DisplayEmail
        {
            get { return Address.Email; }
        }
        public string DisplayPosition
        {
            get { return Position.Name; }
        }
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}