﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class CustomerItemModel
    {
        public CustomerItemModel(DataRow row)
        {
            var weight = "" + row["Weight"];
            weight += string.IsNullOrEmpty(weight) ? "" : " " + row["WeightUnitName"];
            Weight = weight;
            FullItemNumberWithDots = Utils.FillToFiveChars("" + row["OrderCode"]) + "." +
                           Utils.FillToThreeChars("" + row["BatchCode"], "" + row["OrderCode"]) + "." +
                           Utils.FillToTwoChars("" + row["NewItemCode"]);
            FullItemNumber = FullItemNumberWithDots.Replace(".","");
            Color = "" + row["Color"];
            Clarity = "" + row["Clarity"];
            StateId = Convert.ToInt32(row["StateID"]);
            StateName = "" + row["StateName"];
            Shape = "" + row["Shape"];
            CreateDate = Convert.ToDateTime(row["CreateDate"]);
            OrderDate = Convert.ToDateTime(row["OrderDate"]);
        }
        public string Weight { get; set; }
        public string Clarity { get; set; }
        public string Color { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string FullItemNumber { get; set; }
        public string FullItemNumberWithDots { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime OrderDate { get; set; }
        public string Shape { get; set; }
        public string CreateDateDisplay
        {
            get { return CreateDate.ToString("G"); }
        }
        public string OrderDateDisplay
        {
            get { return OrderDate.ToString("G"); }
        }
    }
}