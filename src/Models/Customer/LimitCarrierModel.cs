﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models.Customer
{
	[Serializable]
	public class LimitCarrierModel
	{
		public string CarrierID { get; set; }
        public string CarrierName { get; set; }
        public string Account { get; set; }
		public string LowerLimit { get; set; }
		public string UpperLimit { get; set; }

		public LimitCarrierModel()
		{
		}

		public LimitCarrierModel(DataRow row)
		{
            CarrierID = "" + row["CarrierID"];
            CarrierName = "" + row["CarrierName"];
            Account = "" + row["Account"];
			LowerLimit = "" + row["LowerLimit"];
			UpperLimit = "" + row["UpperLimit"];
		}
	}
}