﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class BusinessTypeModel
    {
        public BusinessTypeModel()
        {
            BusinessTypeId = "";
            BusinessTypeName = "";
        }

        /// <summary>
        /// spGetBusinessTypes
        /// </summary>
        /// <param name="row"></param>
        public BusinessTypeModel(DataRow row)
        {
            BusinessTypeId = "" + row["BusinessTypeID"];
            BusinessTypeClass = Convert.ToInt32(row["BusinessTypeClass"]);
            BusinessTypeCode = Convert.ToInt32(row["BusinessTypeCode"]);
            BusinessTypeName = "" + row["BusinessTypeName"];
        }
        public string BusinessTypeId { get; set; }
        public int BusinessTypeClass { get; set; }
        public int BusinessTypeCode { get; set; }
        public string BusinessTypeName { get; set; }
    }
}