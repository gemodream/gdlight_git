﻿using System;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class IndustryMembershipModel
    {
        /// <summary>
        /// spGetIndustryMemberships
        /// </summary>
        /// <param name="row"></param>
        public IndustryMembershipModel(DataRow row)
        {
            Id = Convert.ToInt32(row["IndustryMembershipID"]);
            Code = Convert.ToInt32(row["IndustryMembershipCode"]);
            Name = "" + row["IndustryMembershipName"];
        }

        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}