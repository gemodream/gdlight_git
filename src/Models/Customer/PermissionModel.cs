﻿using System;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class PermissionModel
    {
        public PermissionModel()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
}