﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.Customer
{
    [Serializable]
    public class GoodsMovementModel
    {
        public GoodsMovementModel()
        {
            InitItems();
        }
        public GoodsMovementModel(DataRow row)
        {
            if ("" + row["WeCarry"] == "1") WeCarry = true;
            if ("" + row["TheyCarry"] == "1") TheyCarry = true;
            if ("" + row["WeShipCarry"] == "1") WeShipCarry = true;
        }
        private void InitItems()
        {
            Items = new List<GoodMovementModel>
            {
                new GoodMovementModel{Id="0", Name="We Carry"},
                new GoodMovementModel{Id="1", Name="They Carry"},
                new GoodMovementModel{Id="2", Name="We Ship"}
            };
        }

        public List<GoodMovementModel> Items { get; set; }
        public bool WeCarry { get; set; }
        public bool TheyCarry { get; set; }
        public bool WeShipCarry { get; set; }
        public bool HasChecked { get { return WeCarry || TheyCarry || WeShipCarry; } }
    }
}