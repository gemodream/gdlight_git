﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DocModel
    {
        public DocModel(DataRow row)
        {
            DocName = "" + row["DocumentName"];
        }
        public string DocName { get; set; }
    }
}