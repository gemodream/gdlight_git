﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SyntheticScreeningModel
    {
        public SyntheticScreeningModel() { }
        public SyntheticScreeningModel(DataRow row)
        {
            ScreeningID = Int64.Parse("" + row["ScreeningID"]);
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?) null : int.Parse("" + row["GSIOrder"]);
            VendorNum = "" + row["VendorNum"];
            PONum = "" + row["PONum"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            QtyPass = row["QtyPass"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyPass"]);
            QtyFail = row["QtyFail"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyFail"]);
			if (row.Table.Columns.Contains("QtySynthetic"))
				QtySynthetic = row["QtySynthetic"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySynthetic"]);
			if (row.Table.Columns.Contains("QtySuspect"))
				QtySuspect = row["QtySuspect"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySuspect"]);
			if (row.Table.Columns.Contains("SyntheticSuspect"))
				SyntheticSuspect = row["SyntheticSuspect"] == DBNull.Value ? (int?)null : int.Parse("" + row["SyntheticSuspect"]);
            FTQuantity = row["FTQuantity"] == DBNull.Value ? (int?)null : int.Parse("" + row["FTQuantity"]);
            SKUName = "" + row["SKUName"];
            Test = "" + row["Test"];
            ScreeningType = "" + row["CertifiedBy"];
            CertifiedBy = "" + row["CertifiedBy"];
            Destination = "" + row["Destination"];
            Notes = "" + row["Notes"];
			if (row.Table.Columns.Contains("CreatedBy"))
				CreatedBy = "" + row["CreatedBy"];
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?) null : DateTime.Parse("" + row["CreatedDate"]);
            ModifiedBy = "" + row["ModifiedBy"];
            ModifiedDate = row["ModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ModifiedDate"]);
            CustomerName = "" + row["CustomerName"];

        }       

        public Int64 ScreeningID { get; set; }
        public int? GSIOrder { get; set; }
        public string VendorNum { get; set; }
        public string PONum { get; set; }      
        public string SKUName { get; set; }
        public string Test { get; set; }
        public string ScreeningType { get; set; }
        public string CertifiedBy { get; set; }
        public string Destination { get; set; }
        public string Notes { get; set; }
        public int? TotalQty { get; set; }
        public int? FTQuantity { get; set; }
        public int? QtyPass { get; set; }
        public int? QtyFail { get; set; }
        public int? QtySynthetic { get; set; }
        public int? QtySuspect { get; set; }
        public string ValSynthetic { get; set; }
        public string ValSuspect { get; set; }
        public int? SyntheticSuspect { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CustomerName { get; set; }

    }
    internal class BatchCodeCount
    {
        int BatchCode { get; set; }
        int BatchCount { get; set; }
    }
}