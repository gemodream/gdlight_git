﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class PrintLabel2Model
    {
        public PrintLabel2Model() { }
        public PrintLabelModel LabelOne { get; set; }
        public PrintLabelModel LabelTwo { get; set; }
        public string UniqueKey
        {
            get { return LabelOne.UniqueKey + ";" + LabelTwo.UniqueKey; }
        }
    }
}