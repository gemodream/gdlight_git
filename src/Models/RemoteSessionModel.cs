﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models
{
    public class RemoteSessionModel
    {
        public RemoteSessionModel() { }
        public RemoteSessionModel(DataRow row)
        {
            Id = "" + row["Id"];
            Ip = "" + row["Ip"];
            CountryCode = "" + row["CountryCode"];
            RegionCode = "" + row["ZipCode"];
            LoginDate = Convert.ToDateTime(row["LoginDate"]);
            UserId = "" + row["UserId"];
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
        }
        public string Id { get; set; }
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime LoginDate { get; set; }
        public string DisplayName { get { return FirstName + " " + LastName; } }
    }
}