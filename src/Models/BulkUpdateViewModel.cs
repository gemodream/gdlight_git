﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class BulkUpdateViewModel
    {
        public BulkUpdateViewModel()
        {
            MeasureValue = new MeasureValueModel();
        }
        public string PartName { get; set; }
        public string MeasureName { get; set; }
        public MeasureValueModel MeasureValue { get; set; }
        public string DisplayValue
        {
            get
            {
                if (MeasureValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    return MeasureValue.EnumValueTitle;
                }
                if (MeasureValue.MeasureClass == MeasureModel.MeasureClassText)
                {
                    return MeasureValue.StringValue;
                }
                if (MeasureValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return MeasureValue.MeasureValue;
                }
                return "";
                
            }
        }
        public string DisplayValueMax
        {
            get
            {
                if (MeasureValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    return MeasureValue.EnumValueTitleMax;
                }
                if (MeasureValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return MeasureValue.MeasureValueMax;
                }
                return "";

            }
        }
        public string UniqueKey
        {
            get { return MeasureValue.PartId + ";" + MeasureValue.MeasureId; }
        }
    }
}