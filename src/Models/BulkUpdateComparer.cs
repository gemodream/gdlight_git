﻿using System;
using System.Collections.Generic;
using Corpt.Utilities;

namespace Corpt.Models
{
    public class BulkUpdateComparer : IComparer<BulkUpdateViewModel>
    {
        public int Compare(BulkUpdateViewModel x, BulkUpdateViewModel y)
        {
            var result = ComparerUtils.NumberCompare(Convert.ToInt32(x.MeasureValue.PartId), Convert.ToInt32(y.MeasureValue.PartId));
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(x.MeasureName, y.MeasureName);
            }
            return result;

        }
    }
}