﻿using System;
using System.Data;

namespace Corpt.Models
{
    public class OpenOrderModel
    {
        public OpenOrderModel(DataRow row)
        {
            OrderCode = Int32.Parse("" + row["GroupCode"]);
            CreateDate = DateTime.Parse("" + row["CreateDate"]);
            if (!string.IsNullOrEmpty("" + row["NotInspectedQuantity"]))
            {
                NotInspectedQuantity = Int32.Parse("" + row["NotInspectedQuantity"]);
            }
            Memo = "" + row["Memo"];
            CustomerName = "" + row["CustomerName"];
        }
        public int OrderCode { get; set; }
        public DateTime CreateDate { get; set; }
        public int? NotInspectedQuantity { get; set; }
        public string Memo { get; set; }
        public string CustomerName { get; set; }
    }
}