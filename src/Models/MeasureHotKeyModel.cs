﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureHotKeyModel
    {
        public MeasureHotKeyModel()
        {
        }
        public MeasureHotKeyModel(DataRow row)
        {
            Access = new MeasureAccessModel
                {
                    AccessCode = Convert.ToInt32(row["ViewAccessCode"]),
                    AccessId = Convert.ToInt32(row["ViewAccessID"]),
                    AccessName = Convert.ToString(row["VewAccessName"])
                };
            Measure = new MeasureModel
                {
                    MeasureClass = MeasureModel.MeasureClassEnum,
                    MeasureId = Convert.ToInt32(row["MeasureID"]),
                    MeasureName = Convert.ToString(row["MeasureName"])
                };
            EnumValueId = Convert.ToInt32(row["MeasureValueID"]);
            EnumValueTitle = Convert.ToString(row["ValueTitle"]);
            HotKey = Convert.ToString(row["HotKeys"]);
        }
        public MeasureModel Measure { get; set; }
        public int EnumValueId { get; set; }
        public string EnumValueTitle { get; set; }
        public string HotKey { get; set; }
        public MeasureAccessModel Access { get; set; }
        public string Help
        {
            get { return HotKey + ": " + Access.AccessName + ", " + Measure.MeasureName + ", " + EnumValueTitle; }
        }

    }
}