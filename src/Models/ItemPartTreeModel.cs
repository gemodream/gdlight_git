﻿using System;
using System.Collections.Generic;

namespace Corpt.Models
{
    [Serializable]
    public class ItemPartTreeModel
    {
        public ItemPartTreeModel()
        {
            Childs = new List<ItemPartTreeModel>();
        }
        public ItemPartTreeModel(MeasurePartModel partModel)
        {
            PartModel = partModel;
            Childs = new List<ItemPartTreeModel>();
        }

        public MeasurePartModel PartModel { get; set; }
        public List<ItemPartTreeModel> Childs { get; set; }
        public string PartName { get { return PartModel.PartName; } }
        public int PartId { get { return Convert.ToInt32(PartModel.PartId); } }
        public int PartTypeId { get { return Convert.ToInt32(PartModel.PartTypeId); } }
        public ItemPartTreeModel Parent { get; set; }
    }
}