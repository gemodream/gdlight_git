﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ViewAccessModel
    {
        public ViewAccessModel()
        {
        }
        public ViewAccessModel(DataRow row)
        {
            ViewAccessId = Int32.Parse("" + row["ViewAccessID"]);
            ViewAccessName = "" + row["VewAccessName"];
        }

        public int ViewAccessId { get; set; }
        public string ViewAccessName { get; set; }
    }
}