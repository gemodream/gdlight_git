﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CustomerVendorCpModel
    {
        public CustomerVendorCpModel(DataRow row, List<CustomerModel> customers)
        {
            ProgramName = Convert.ToString(row["CustomerProgramName"]);
            CustomerId = Convert.ToString(row["CustomerID"]);
            VendorId = Convert.ToString(row["VendorID"]);
            var customer = customers.Find(m => m.CustomerId == CustomerId);
            if (customer != null)
            {
                CustomerName = customer.CustomerName;
            }
            var vendor = customers.Find(m => m.CustomerId == VendorId);
            if (vendor != null)
            {
                VendorName = vendor.CustomerName;
            }
        }

        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public string ProgramName { get; set; }
    }
}