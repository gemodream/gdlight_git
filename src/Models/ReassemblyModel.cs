﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    public class ReassemblyModel
    {
        public ReassemblyModel() { }
        public ReassemblyModel(string[] src, string[] target)
        {
            SourceItemFld = "" + src[0];
            SourcePartNameFld = src[1];
            SourcePartIdFld = src[2];
            TargetItemFld = "" + target[0];
            TargetPartNameFld = target[1];
            TargetPartIdFld = target[2];
        }
        public ReassemblyModel(string srcItem, string srcPart, string srcPartId, string targetItem, string targetPart, string targetPartId)
        {
            SourceItemFld = "" + srcItem;
            SourcePartNameFld = "" + srcPart;
            SourcePartIdFld = "" + srcPartId;
            TargetItemFld = "" + targetItem;
            TargetPartNameFld = "" + targetPart;
            TargetPartIdFld = "" + targetPartId;
        }

        public string SourceItemFld { get; set; }
        public string SourcePartNameFld { get; set; }
        public string SourcePartIdFld { get; set; }
        public string TargetItemFld { get; set; }
        public string TargetPartNameFld { get; set; }
        public string TargetPartIdFld { get; set; }
    }
    
}