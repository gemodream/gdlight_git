﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemCycleModel
    {
        public ItemCycleModel(DataRow row)
        {
            OrderCode = "" + row["OrderCode"];
            BatchCode = "" + row["BatchCode"];
            ItemCode = "" + row["ItemCode"];
            CreateDate = Convert.ToDateTime(row["CreateDate"]);
            BatchId = "" + row["BatchId"];

        }
        public string BatchId { get; set; }
        public string OrderCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }
        public bool IsParam { get; set; }
        public DateTime CreateDate { get; set; }
        public string FullItemNumber
        {
            get
            {
                return
                   Utils.FillToFiveChars(OrderCode) + 
                   Utils.FillToThreeChars(BatchCode, OrderCode) + 
                   Utils.FillToTwoChars(ItemCode);
            }
        }
        public string FullItemNumberWithDotes
        {
            get
            {
                return
                    Utils.FillToFiveChars(OrderCode) + "." +
                    Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
                    Utils.FillToTwoChars(ItemCode);
            }
        }
    }
}