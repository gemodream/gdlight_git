﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CustomerModel
    {
        public CustomerModel()
        {
        }
        /// <summary>
        /// spGetCustomers
        /// </summary>
        /// <param name="row"></param>
        public CustomerModel(DataRow row)
        {
            CustomerName = "" + row["CustomerName"];
            CustomerId = ("" + row["CustomerOfficeID_CustomerID"]).Split(Convert.ToChar("_"))[1];
            OfficeId = ("" + row["CustomerOfficeID_CustomerID"]).Split(Convert.ToChar("_"))[0];
            CustomerCode = "" + row["CustomerCode"];
            IconIndex = "" + row["IconIndex"];
        }

        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string OfficeId { get; set; }
        public string CustomerCode { get; set; }
        public string IconIndex { get; set; }
        /// <summary>
        /// wspvvGetCustomerList
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerModel Create1(DataRow row)
        {
            return new CustomerModel
            {
                CustomerName = Convert.ToString(row["CustomerName"]),
                CustomerId = Convert.ToString(row["customerid"])
            };
        }

        /// <summary>
        /// spGetCustomerCompanyGroupByCustomerID
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerModel SetData(DataRow row)
        {
            return new CustomerModel
            {
                CustomerName = Convert.ToString(row["CustomerName"]),
                CustomerId = Convert.ToString(row["CustomerID"])
            };
        }

        /// <summary>
        /// spGetCustomersByRetailer
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerModel SetDataByRetailer(DataRow row)
        {
            return new CustomerModel
            {
                CustomerName = Convert.ToString(row["CustomerName"]),
                CustomerId = Convert.ToString(row["CustomerID"]),
                CustomerCode = Convert.ToString(row["CustomerCode"]),
                OfficeId = Convert.ToString(row["CustomerOfficeID"])
            };
        }
    }
}