﻿using Corpt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;


/* Class for providing properties for button lists in expanded panels in new interface
 */
namespace Corpt.Utilities
{
	public class ButtonDetailModel
	{
		public string BigBtn { get; set; }//Text
		public string BtnMeasure { get; set; }//FOR COMMENTS ONLY!
		public string BtnLink { get; set; }//Link to Category
		public string BtnValue { get; set; }
		public string BtnCat { get; set; }//Category
		public ButtonDetailModel()
		{

		}
		public ButtonDetailModel(string text)
		{
			BigBtn = text;
		}
		public ButtonDetailModel(string text, string redirect)
		{
			BigBtn = text;
			BtnLink = redirect;
		}
		public ButtonDetailModel(string text, string redirect, string value)
		{
			BigBtn = text;
			BtnLink = redirect;
			BtnValue = value;
		}
		public ButtonDetailModel(string text, string link, string cat, string value)
		{
			BigBtn = text;
			BtnLink = link;
			BtnCat = cat;
			BtnValue = value;
		}
		public static List<ButtonDetailModel> CreateNew(XmlNode node)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			foreach (XmlNode item in node.SelectNodes("BtnRepeaterSource/Button"))
			{
				btn = new ButtonDetailModel();
				btn.BigBtn = item.SelectSingleNode("BigBtn").InnerText;
				btn.BtnCat = item.SelectSingleNode("BtnCat").InnerText;
				btn.BtnLink = item.SelectSingleNode("BtnLink").InnerText;
				btn.BtnValue = item.SelectSingleNode("BtnValue").InnerText;

				ret.Add(btn);
			}

			return ret;
		}
		public static List<ButtonDetailModel> CreateNewDiamondQuality(XmlNode node)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			foreach (XmlNode item in node.SelectNodes("BtnRepeaterSource/Button"))
			{
				btn = new ButtonDetailModel();
				btn.BigBtn = item.SelectSingleNode("BigBtn").InnerText;
				btn.BtnCat = item.SelectSingleNode("BtnCat").InnerText;
				btn.BtnLink = item.SelectSingleNode("BtnLink").InnerText;
				btn.BtnValue = item.SelectSingleNode("BtnValue").InnerText;
				btn.BtnMeasure = item.SelectSingleNode("BtnMeasure").InnerText;
				ret.Add(btn);
			}
			return ret;
		}
		public static List<ButtonDetailModel> CreateNewStandard(List<EnumMeasureModel> items)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			foreach (var item in items)
			{
				btn = new ButtonDetailModel();

				if (item.ValueTitle != "")
				{
					btn.BigBtn = item.ValueTitle;
				}
				else
				{
					btn.BigBtn = "None";
				}

				btn.BtnCat = "Root";
				btn.BtnLink = "Small";
				btn.BtnValue = item.ValueTitle;
				ret.Add(btn);
			}

			btn = new ButtonDetailModel();
			btn.BigBtn = "Collapse";
			btn.BtnCat = "Root";
			btn.BtnLink = "Small";
			btn.BtnValue = "0";
			ret.Add(btn);

			return ret;
		}
		public static List<ButtonDetailModel> CreateNewCollapse()
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			btn = new ButtonDetailModel();
			btn.BigBtn = "Collapse";
			btn.BtnCat = "Root";
			btn.BtnLink = "Small";
			btn.BtnValue = "0";
			ret.Add(btn);

			return ret;
		}
		//public static int[] commentNums = { 48, 49, 51, 86, 97, 102, 103, 104, 105, 113 };//For Clarity only
		/*public static List<ButtonDetailModel> CreateNewComments(List<EnumMeasureModel> items)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			foreach(var item in items)
			{
				btn = new ButtonDetailModel();

				foreach(var commentNum in commentNums)
				{
					if(item.MeasureValueMeasureId == commentNum)
					{
						if(item.ValueTitle != "")
						{
							btn.BigBtn = item.MeasureValueName;
							btn.BtnCat = "Root";
							btn.BtnLink = "0";
							btn.BtnValue = item.MeasureValueName;
							btn.BtnMeasure = commentNum.ToString();
							ret.Add(btn);
						}
						break;
					}
				}
			}

			btn = new ButtonDetailModel();
			btn.BigBtn = "Collapse";
			btn.BtnCat = "Root";
			btn.BtnLink = "Small";
			btn.BtnValue = "0";
			ret.Add(btn);

			return ret;
		}*/
		public static List<ButtonDetailModel> CreateNewComments(List<EnumMeasureModel> items, XmlNode node)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;

			foreach (XmlNode values in node.SelectNodes("CommentSource/Comment"))
			{
				var item = items.FindAll(m => m.MeasureValueMeasureId.ToString() == values.InnerText);
				foreach(var itemInst in item)
				{
					if (itemInst.MeasureValueId == 6141 || itemInst.MeasureValueId == 6140)//remove GR values for INT and TRT
						continue;
					if (itemInst.ValueTitle != "")
					{
						btn = new ButtonDetailModel();
						btn.BigBtn = itemInst.MeasureValueName;
						btn.BtnCat = "Root";
						btn.BtnLink = "0";
						btn.BtnValue = itemInst.MeasureValueName;
						btn.BtnMeasure = itemInst.MeasureValueMeasureId.ToString();
						ret.Add(btn);
					}
				}
			}

			btn = new ButtonDetailModel();
			btn.BigBtn = "Collapse";
			btn.BtnCat = "Root";
			btn.BtnLink = "Small";
			btn.BtnValue = "0";
			ret.Add(btn);

			return ret;
		}
		public static List<ButtonDetailModel> CreateNewComments(List<EnumMeasureModel> items, int[] commentCustomNums)
		{
			List<ButtonDetailModel> ret = new List<ButtonDetailModel>();
			ButtonDetailModel btn;
			
			foreach(var commentNum in commentCustomNums)
			{
				btn = new ButtonDetailModel();
				var item = items.FindAll(m => m.MeasureValueMeasureId == commentNum);
				foreach (var itemInst in item)
				{
					if (itemInst.ValueTitle != "")
					{
						btn.BigBtn = itemInst.MeasureValueName;
						btn.BtnCat = "Root";
						btn.BtnLink = "0";
						btn.BtnValue = itemInst.MeasureValueName;
						btn.BtnMeasure = itemInst.MeasureValueMeasureId.ToString();
						ret.Add(btn);
					}
				}
			}
			
			/*
			foreach (var item in items)
			{
				btn = new ButtonDetailModel();

				foreach (var commentNum in commentCustomNums)
				{
					if (item.MeasureValueMeasureId == commentNum)
					{
						if (item.ValueTitle != "")
						{
							btn.BigBtn = item.MeasureValueName;
							btn.BtnCat = "Root";
							btn.BtnLink = "0";
							btn.BtnValue = item.MeasureValueName;
							btn.BtnMeasure = commentNum.ToString();
							ret.Add(btn);
						}
						break;
					}
				}
			}
			*/

			btn = new ButtonDetailModel();
			btn.BigBtn = "Collapse";
			btn.BtnCat = "Root";
			btn.BtnLink = "Small";
			btn.BtnValue = "0";
			ret.Add(btn);

			return ret;
		}
	}
}