﻿using System;
using System.Data;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class SingleItemModel
    {
        public SingleItemModel()
        {
        }
        /// <summary>
        /// spGetItemCPPictureByCode
        /// </summary>
        /// <param name="row"></param>
        public SingleItemModel(DataRow row)
        {
            BatchCode   = "" + row["BatchCode"];
            OrderCode   = "" + row["GroupCode"];
            ItemCode    = "" + row["ItemCode"];
            BatchId     = Int32.Parse("" + row["BatchID"]);
            ItemTypeId  = Int32.Parse("" + row["ItemTypeId"]);
            NewItemCode = "" + row["NewItemCode"];
            NewBatchId  = Int32.Parse("" + row["NewBatchID"]);
            StateId     = Int32.Parse("" + row["StateID"]);
            Path2Picture = "" + row["path2picture"];
        }
        public SingleItemModel(DataRow row, string old)
        {
            BatchCode = "" + row["BatchCode"];
            OrderCode = "" + row["GroupCode"];
            ItemCode = "" + row["ItemCode"];
            PrevBatchCode = "" + row["PrevBatchCode"];
            PrevOrderCode = "" + row["PrevGroupCode"];
            PrevItemCode = "" + row["PrevItemCode"];
            BatchId = Int32.Parse("" + row["BatchID"]);
            ItemTypeId = Int32.Parse("" + row["ItemTypeId"]);
            NewItemCode = "" + row["NewItemCode"];
            NewBatchId = Int32.Parse("" + row["NewBatchID"]);
            StateId = Int32.Parse("" + row["StateID"]);
            Path2Picture = "" + row["path2picture"];
        }
        /// <summary>
        /// From Leo Short Report 2
        /// </summary>
        /// <param name="batchModel"></param>
        /// <param name="itemCode"></param>
        public SingleItemModel(BatchModel batchModel, string itemCode)
        {
            BatchCode = batchModel.BatchCode;
            OrderCode = batchModel.GroupCode;
            ItemCode = itemCode;
            BatchId = Convert.ToInt32(batchModel.BatchId);
            NewBatchId = Convert.ToInt32(batchModel.BatchId);
            ItemTypeId = batchModel.ItemTypeId;
            Path2Picture = batchModel.PathToPicture;
            NewItemCode = itemCode;
        }
        public bool IsInvalid
        {
            get { return StateId == DbConstants.ItemInvalidStateId; }
        }
        public string BatchCode { get; set; }
        public string OrderCode { get; set; }
        public string ItemCode { get; set; }
        public string PrevBatchCode { get; set; }
        public string PrevOrderCode { get; set; }
        public string PrevItemCode { get; set; }
        public int BatchId { get; set; }
        public int ItemTypeId { get; set; }
        public string NewItemCode { get; set; }
        public int NewBatchId { get; set; }
        public int StateId { get; set; }
        public string Path2Picture { get; set; }
        public string FullOldItemNumber => string.IsNullOrEmpty(PrevOrderCode) ? "" : (Utils.FillToFiveChars(PrevOrderCode) + Utils.FillToThreeChars(PrevBatchCode, PrevOrderCode) + Utils.FillToTwoChars(PrevItemCode));

        public string FullItemNumber => string.IsNullOrEmpty(OrderCode) ? "" : (Utils.FillToFiveChars(OrderCode) + Utils.FillToThreeChars(BatchCode, OrderCode) + Utils.FillToTwoChars(ItemCode));

        public string FullItemNumberWithDot =>
            string.IsNullOrEmpty(OrderCode) ? "" : (
                Utils.FillToFiveChars(OrderCode) + "." +
                Utils.FillToThreeChars(BatchCode, OrderCode) + "." + 
                Utils.FillToTwoChars(ItemCode)
            );

        public string FullBatchNumber =>
            string.IsNullOrEmpty(OrderCode) ? "" : (
                Utils.FillToFiveChars(OrderCode) + 
                Utils.FillToThreeChars(BatchCode, OrderCode)
            );
    }
}