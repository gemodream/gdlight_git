﻿using System.Data;

namespace Corpt.Models
{
    public class RemeasParamModel
    {
        public RemeasParamModel(string inputItemNumber)
        {
            InputItemNumber = inputItemNumber;
        }
        public RemeasParamModel(DataRow row, string inputItemNumber)
        {
            InputItemNumber = inputItemNumber;
            var oldItem = "" + row["NewItemNumber"];
            var keys = oldItem.Split('.');
            NewItemNumber = keys[1] + keys[2] + keys[3];
            NewItemNumberWithDotes = keys[1] + "." + keys[2] + "." + keys[3];
            CustomerCode = "" + row["CustomerCode"];
            NewOrder = "" + row["OrderCode"];
        }
        public string InputItemNumber { get; set; }
        public string NewItemNumber { get; set; }
        public string NewItemNumberWithDotes { get; set; }
        public string CustomerCode { get; set; }
        public string NewOrder { get; set; }
    }
}