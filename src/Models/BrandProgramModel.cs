﻿using System;
using System.Data;
using Corpt.Models;

namespace Corpt.Models
{
    [Serializable]
    public class BrandProgramModel : BrandModel
    {

        public BrandProgramModel()
        {
            
        }
     
        public BrandProgramModel(DataRow dr)
        {
            ID     = "" + dr["ProgramId"];
            BrandId = "" + dr["BrandId"];
            BrandName = "" + dr["BrandName"];
            ProgramName = "" + dr["ProgramName"];
            Nature = "" + dr["Nature"];
            DisplayProgramName = ProgramName + " (" + Nature + ")";
        }
 

        public string ID { get; set; }

        public string ProgramName { get; set; }

        public string DisplayProgramName { get; set; }
     


    }
}