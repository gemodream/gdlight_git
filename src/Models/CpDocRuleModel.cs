﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CpDocRuleModel
    {
        /// <summary>
        /// spGetCPDocRule
        /// </summary>
        /// <param name="row"></param>
        public CpDocRuleModel(DataRow row)
        {
            CpDocId = Int32.Parse("" + row["CPDocID"]);
            PartId = Int32.Parse("" + row["PartID"]);
            MeasureId = Int32.Parse("" + row["MeasureID"]);
            MinMeasure = "" + row["MinMeasure"];
            MaxMeasure = "" + row["MaxMeasure"];
            IsDefaultMeasureValue = 
                (row["IsDefaultMeasureValue"] != null && row["IsDefaultMeasureValue"].ToString() == "1");
            IsInfoOnlyMeasureValue =
                (row["IsDefaultMeasureValue"] != null && row["IsDefaultMeasureValue"].ToString() == "2");
            NotVisibleInCcm = ("" + row["NotVisibleInCCM"]) == "1";
        }
        public int CpDocId { get; set; }
        public int PartId { get; set; }
        public int MeasureId { get; set; }
        public string MinMeasure { get; set; }
        public string MaxMeasure { get; set; }
        public bool IsDefaultMeasureValue { get; set; }
        public bool IsInfoOnlyMeasureValue { get; set; }
        public bool NotVisibleInCcm { get; set; }
    }
}