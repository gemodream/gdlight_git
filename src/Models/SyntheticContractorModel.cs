﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticContractorModel
	{
		public SyntheticContractorModel() { }
		public SyntheticContractorModel(DataRow row)
		{
			//contractorName = "" + row["contractorName"];
			customerName = "" + row["customerName"];
			customerCode = "" + row["customerCode"];
			contractorId = "" + row["contractorId"];
		}
		public string contractorId { get; set; }
		public string contractorName { get; set; }
		public string ServiceTypeCodeSet { get; set; }
		public string customerName { get; set; }
		public string customerCode { get; set; }
		public string expireDate { get; set; }


	}
}