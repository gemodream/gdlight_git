﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PartModel
    {
        public PartModel() { }
        public PartModel(DataRow row)
        {
            PartId = Int32.Parse("" + row["PartID"]);
            PartName = "" + row["PartName"];
        }
        public string PartName { get; set; }
        public int PartId { get; set; }
    }
}