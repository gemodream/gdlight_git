﻿using System;
using System.Collections.Generic;
using System.Data;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class ItemModel
    {
        public ItemModel(){}
        /*Procedure wspvvGetItemsByBatchExistingNumbersOnly*/
        public ItemModel(DataRow r)
        {
            ItemCode        = "" + r["ItemCode"];
            OrderCode       = "" + r["orderCode"];
            BatchCode       = "" + r["BatchCode"];
            MemoNumber      = "" + r["MemoNumber"];
            LotNumber       = "" + r["LotNumber"];
            ItemTypeName    = "" + r["ItemTypeName"];
            MeasureItemList = new List<MeasureItemModel>();
        }
        public string ItemCode { get; set; }
        public string BatchCode { get; set; }
        public string OrderCode { get; set; }
        public string BatchId { get; set; }
        public string NewBatchId { get; set; }
        public string ItemTypeName { get; set; }
        public string MemoNumber { get; set; }
        public string LotNumber { get; set; }
        public string LeoOrdered { get; set; }
        public string LeoPrinted { get; set; }
        public string OldFullIemNumber { get; set; }
        public bool SkipOnGeneration { get; set; }
        public string StateId { get; set; }
        public FileInfoModel PdfFile { get; set; }
        public string FullItemNumber =>
            Utils.FillToFiveChars(OrderCode) + Utils.FillToThreeChars(BatchCode, OrderCode) +
            Utils.FillToTwoChars(ItemCode);

        public string FullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);

        public string OldFullItemNumberWithDotes =>
            Utils.FillToFiveChars(OrderCode) + "." +
            Utils.FillToThreeChars(BatchCode, OrderCode) + "." +
            Utils.FillToTwoChars(ItemCode);

        public List<MeasureItemModel> MeasureItemList { get; set; }
        /// <summary>
        /// spGetNewItemNumberFromOldItemNumber
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemModel Create1(DataRow row)
        {
            var itemModel = new ItemModel
            {
                OrderCode = "" + row["GroupCode"],
                BatchCode = "" + row["BatchCode"],
                ItemCode = "" + row["ItemCode"]
            };
            return itemModel;
        }
        /// <summary>
        /// spGetItemByCode
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemModel Create2(DataRow row)
        {
            var itemModel = new ItemModel
            {
                OrderCode = "" + row["GroupCode"],
                BatchCode = "" + row["BatchCode"],
                ItemCode = "" + row["ItemCode"],
                BatchId = "" + row["BatchID"],
                NewBatchId = "" + row["NewBatchID"],
                OldFullIemNumber = Utils.FillToFiveChars("" + row["PrevGroupCode"]) + Utils.FillToThreeChars("" + row["PrevBatchCode"], "" + row["PrevGroupCode"]) +
                       Utils.FillToTwoChars("" + row["PrevItemCode"]),
                StateId = "" + row["StateID"]
            };
            return itemModel;
            
        }
        public bool IsInvalid => StateId == "" + DbConstants.ItemInvalidStateId;
    }
}