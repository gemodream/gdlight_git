﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ShortReportModel
    {
        public ShortReportModel()
        {
            Items = new List<ItemModel>();
            MovedItems = new List<MovedItemModel>();
            DocStructure = new List<DocStructModel>();
            ItemValues = new List<ItemValueModel>();
            ItemsFailed = new List<ItemFailedModel>();
            ItemsQueue = new List<QueueModel>();
            ItemsPrinted = new List<PrintedModel>();
            LabelDocuments = new List<DocumentModel>();
        }
        public bool IsShortReport
        {
            get { return DocumentId != ""; }
        }

        public BatchModel BatchModel { get; set; }

        public List<DocStructModel> DocStructure { get; set; }

        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
        public List<MovedItemModel> MovedItems { get; set; }
        public List<ItemModel> Items { get; set; }
        public List<ItemValueModel> ItemValues { get; set; }
        public List<ItemFailedModel> ItemsFailed { get; set; }
        public List<QueueModel> ItemsQueue { get; set; }
        public List<PrintedModel> ItemsPrinted { get; set; }
        public string KmMessage { get; set; }

        public string BatchNumber { get; set; }
        public int DocStructGrp { get; set; }
        public DataTable ReportView { get; set; }
        public List<DocumentModel> LabelDocuments { get; set; }
    }
}