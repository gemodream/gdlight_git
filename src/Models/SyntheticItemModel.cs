﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class SyntheticItemModel
    {
        public SyntheticItemModel() { }
        public int GroupCode { get; set; }
        public int BatchCode { get; set; }
        public int ItemCode { get; set; }
        public int BatchID { get; set; }
        
    }
    [Serializable]
    public class SyntheticAcceptanceModel
    {
        public SyntheticAcceptanceModel() { }
        public string RequestID { get; set; }
        public string Memo { get; set; }
        public string TotalQty { get; set; }
        public string RetailerName { get; set; }
    }
}

