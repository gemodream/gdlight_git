﻿using System;
using System.Collections.Generic;
using System.Data;
using Corpt.Models.CustomerProgram;

namespace Corpt.Models
{
    [Serializable]
    public class CustomerProgramModel
    {
        public CustomerProgramModel()
        {
            
        }
        /// <summary>
        /// spGetCustomerProgramsPerCustomer
        /// </summary>
        /// <param name="row"></param>
        public CustomerProgramModel(DataRow row)
        {
            InitData(row, false, true);
        }

        /// <summary>
        /// spGetCustomerProgramInstanceByCPID
        /// </summary>
        /// <param name="row"></param>
        /// <param name="skipTypeGroup"> </param>
        public CustomerProgramModel(DataRow row, bool skipTypeGroup)
        {
            InitData(row, skipTypeGroup, true);
        }

        /// <summary>
        /// spGetCustomerProgramInstanceByBatchCode
        /// </summary>
        /// <param name="row"></param>
        /// <param name="withPrice"></param>
        public static CustomerProgramModel CreateCombineCp(DataRow row, CustomerProgramModel withPrice)
        {
            var cp = new CustomerProgramModel();
            cp.InitData(row, false, false);
            cp.PriceId = withPrice.PriceId;
            cp.IsFixed = withPrice.IsFixed;
            cp.FixedPrice = withPrice.FixedPrice;
            cp.DeltaFix = withPrice.DeltaFix;
            cp.Discount = withPrice.Discount;
            cp.FailFixed = withPrice.FailFixed;
            cp.FailDiscount = withPrice.FailDiscount;
            return cp;
        }
        private void InitData(DataRow row, bool skipTypeGroup, bool hasPrice)
        {
            CustomerProgramName = "" + row["CustomerProgramName"];
            CpOfficeId = "" + row["CPOfficeID"];
            CpOfficeIdAndCpId = "" + row["CPOfficeID_CPID"];
            Srp = "" + row["SRP"];
            VendorId = "" + row["VendorID"];
            CustomerId = "" + row["CustomerID"];
            CustomerStyle = "" + row["CustomerStyle"];
            Description = "" + row["Description"];
            Comment = "" + row["Comment"];
            Path2Picture = "" + row["Path2Picture"];
            CpId = "" + row["CPID"];
            CpPropertyCustId = "" + row["CPPropertyCustomerID"];
            ItemTypeId = Convert.ToInt32("" + row["ItemTypeID"]);
            if (!skipTypeGroup)
            {
                ItemTypeGroupId = Convert.ToInt32("" + row["ItemTypeGroupID"]);
            }
            IsCopy = ("" + row["IsCopy"]) == "1";
            if (hasPrice)
            {
                PriceId = string.IsNullOrEmpty("" + row["PriceID"]) ? 0 : Convert.ToInt32(row["PriceId"]);
                IsFixed = ("" + row["isFixed"] == "1");
                FixedPrice = string.IsNullOrEmpty("" + row["FixedPrice"]) ? 0 : Convert.ToDouble(row["FixedPrice"]);
                DeltaFix = string.IsNullOrEmpty("" + row["DeltaFix"]) ? 0 : Convert.ToDouble(row["DeltaFix"]);
                Discount = string.IsNullOrEmpty("" + row["Discount"]) ? 0 : Convert.ToDouble(row["Discount"]);
                FailFixed = string.IsNullOrEmpty("" + row["FailFixed"]) ? 0 : Convert.ToDouble(row["FailFixed"]);
                FailDiscount = string.IsNullOrEmpty("" + row["FailDiscount"]) ? 0 : Convert.ToDouble(row["FailDiscount"]);
            }
            ExtData = true;
        }

        public string CustomerProgramName { get; set; }
        public string CpOfficeId { get; set; }
        public string CpOfficeIdAndCpId { get; set; }
        
        public string CpId { get; set; }
        public string Srp { get; set; }
        public string CustomerId { get; set; }
        public string VendorId { get; set; }
        public string CustomerStyle { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string Path2Picture { get; set; }
        public string CpPropertyCustId { get; set; }
        public int ItemTypeId { get; set; }
        public int ItemTypeGroupId { get; set; }
        public bool IsCopy { get; set; }
        public bool IsFixed { get; set; }
        public double FixedPrice { get; set; }
        public double DeltaFix { get; set; }
        public double Discount { get; set; }
        public double FailFixed { get; set; }
        public double FailDiscount { get; set; }
        public int PriceId { get; set; }
        public bool IsNatural { get; set; }

        public string CommentDisplay
        {
            get { return string.IsNullOrEmpty(Comment) ? "" : Utils.CleanCD(Comment); }
        }
        public string DescriptionDisplay
        {
            get { return string.IsNullOrEmpty(Description) ? "" : Utils.CleanCD(Description); }
        }

        public bool ExtData { get; set; }
        public static void SetExtData(DataRow row, CustomerProgramModel cpModel)
        {
            cpModel.Comment = "" + row["Comment"];
            cpModel.Path2Picture = "" + row["Path2Picture"];
            cpModel.Srp = "" + row["SRP"];
            cpModel.Description = "" + row["Description"];
        }
        /// <summary>
        /// wspvvGetCustomerPrograms
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerProgramModel Create1(DataRow row)
        {
            return new CustomerProgramModel
            {
                CustomerProgramName = Convert.ToString(row["customerprogramname"]), 
                CpId = Convert.ToString(row["cpid"]),
                CustomerId = Convert.ToString(row["customerid"])
            };
        }
        /// <summary>
        /// spGetCustomerProgramByBatchID
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerProgramModel Create2(DataRow row)
        {
            return new CustomerProgramModel
            {
                CustomerProgramName = Convert.ToString(row["CustomerProgramName"]),
                CpId = Convert.ToString(row["CPID"]),
                CpOfficeId =  Convert.ToString(row["CPOfficeID"]),
                CpOfficeIdAndCpId = Convert.ToString(row["CPOfficeID_CPID"]),
                CurrentDocs = new List<CpDocPrintModel>()
            };
        }

        /// <summary>
        /// spGetCPByRetailer
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerProgramModel Create3(DataRow row)
        {
            return new CustomerProgramModel
            {
                CustomerProgramName = Convert.ToString(row["SKUName"])
            };
        }

        /// <summary>
        /// spGetSkuListByNaturalOrigin
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerProgramModel Create4(DataRow row)
        {
            return new CustomerProgramModel
            {
                CustomerProgramName = Convert.ToString(row["CustomerProgramName"])
            };
        }

        public List<CpDocPrintModel> CurrentDocs { get; set; }
    }
}