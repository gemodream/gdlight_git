﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BrandModel
    {

        public BrandModel()
        {
            
        }
        public BrandModel(string id, string name)
        {
            BrandId = id;
            BrandName = name;
        }


        public BrandModel(DataRow dr)
        {
            BrandId = "" + dr["BrandId"];
            BrandName = "" + dr["BrandName"];
            Nature = "" + dr["Nature"];
            DisplayBrandName = BrandName + " (" + Nature + ")";
        }

        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string Nature { get; set; }
        public string DisplayBrandName { get; set; }



    }
}