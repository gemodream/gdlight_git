﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class CurrencyModel
    {
        public CurrencyModel() { }
        public CurrencyModel(DataRow row)
        {
            CurrencyID = Int32.Parse("" + row["CurrencyID"]);
            Currency = "" + row["Currency"];
        }

        public Int32 CurrencyID { get; set; }
        public string Currency { get; set; }
       
    }
}