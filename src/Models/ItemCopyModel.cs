﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemCopyModel
    {
        public ItemCopyModel()
        {
        }

        /// <summary>
        /// spGetItemByCode
        /// </summary>
        /// <param name="row"></param>
        public ItemCopyModel(DataRow row)
        {
            BatchId = Convert.ToInt32("" + row["NewBatchID"]);
            OfficeId = Convert.ToInt32("" + row["CustomerOfficeID"]);
            ItemCode = Convert.ToInt32("" + row["NewItemCode"]);
            ItemTypeId = Convert.ToInt32("" + row["ItemTypeID"]);
            OldItemNumber = Utils.FillToFiveChars("" + row["PrevGroupCode"]) +
                    Utils.FillToThreeChars("" + row["PrevBatchCode"], "" + row["PrevGroupCode"]) +
                    Utils.FillToTwoChars("" + row["PrevItemCode"]);
            ItemParts = new List<MeasurePartModel>();
        }

        public int BatchId { get; set; }
        public int OfficeId { get; set; }
        public int ItemCode { get; set; }
        public int ItemTypeId { get; set; }
        public string OldItemNumber { get; set; }
        public string CustomerName { get; set; }
        public List<ItemValueModel> Measures { get; set; }
        public int CopyPartId { get; set; }
        public List<MeasurePartModel> ItemParts { get; set; }
    }
}