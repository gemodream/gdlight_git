﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class SortingModel
    {
        public SortingModel(){}
        public string ColumnName { get; set; }
        public bool Direction { get; set; }
        public static SortingModel CreateRevert(SortingModel prevModel, string nowColName)
        {
            return prevModel != null && prevModel.ColumnName == nowColName ? 
                new SortingModel{ColumnName = nowColName, Direction = !prevModel.Direction} : 
                new SortingModel{ColumnName = nowColName, Direction = true};
        }
    }
}