﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;

namespace Corpt.Models.CustomerProgram
{
    public class FractionRangesModel
    {
        public FractionRangesModel()
        {

        }
        public FractionRangesModel(DataRow row)
        {
            GroupName = row["GroupName"].ToString();
            GroupId = Convert.ToInt32(row["GroupId"]);
            Fraction = row["Fraction"].ToString();
            RangeName = row["RangeName"].ToString();
        }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public string Fraction { get; set; }
        public string RangeName { get; set; }
    }
    public class FractionRangesGroupModel
    {
        public FractionRangesGroupModel()
        {

        }
        public FractionRangesGroupModel(DataRow row)
        {
            GroupName = row["GroupName"].ToString();
            GroupId = Convert.ToInt32(row["GroupID"]);
        }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
    }
}