﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DocumentValueModel
    {
        public DocumentValueModel()
        {
        }
        /// <summary>
        /// spGetDocumentValue
        /// </summary>
        /// <param name="row"></param>
        public DocumentValueModel(DataRow row)
        {
            DocumentId = "" + row["DocumentID"];
            Title = "" + row["Title"];
            Value = "" + row["Value"];
            DocumentValueId = Convert.ToInt32(row["DocumentValueID"]);
            Unit = "" + row["Unit"];
        }

        public string DocumentId { get; set; }
        public int DocumentValueId { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public bool Selected { get; set; }
        public int Order { get; set; }
    }
    
}