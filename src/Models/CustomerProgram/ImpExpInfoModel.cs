﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class ImpExpInfoModel
    {
        /// <summary>
        /// spGetImpExInfo, Tables[0] - Import, Tables[1] - Export
        /// </summary>
        /// <param name="row"></param>
        public ImpExpInfoModel(DataRow row)
        {
            Id = "" + row["MethodID"];
            Name = "" + row["Name"];
            Order = "" + row["nOrder"];
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Order { get; set; }
    }
}