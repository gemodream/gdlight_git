﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpPriceModel
    {
        public CpPriceModel()
        {
            
            PricePartMeasures = new List<PricePartMeasureModel>();
            PriceRanges = new List<PriceRangeModel>();
            AddServicePrices = new List<AddServicePriceModel>();
        }

        public List<PricePartMeasureModel> PricePartMeasures{get; set; }
        public List<PriceRangeModel> PriceRanges { get; set; }
        public List<AddServicePriceModel> AddServicePrices { get; set; }
    }
}