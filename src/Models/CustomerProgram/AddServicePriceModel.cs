﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class AddServicePriceModel : AddServiceModel
    {
        public AddServicePriceModel()
        {
        }

        /// <summary>
        /// spGetAdditionalServicePriceByCPID
        /// </summary>
        /// <param name="row"></param>
        public AddServicePriceModel(DataRow row)
        {
            ServiceId = "" + row["ASID"];
            ServiceName = "" + row["Expr1"];
            Price = Convert.ToDouble(row["Price"]);
            PriceId = Convert.ToInt32(row["PriceID"]);
            Rownum = Convert.ToInt32(row["rownum"]);
        }

        public double Price { get; set; }
        public int PriceId { get; set; }
        public int Rownum { get; set; }
    }
}