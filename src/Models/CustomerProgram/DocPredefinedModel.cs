﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DocPredefinedModel
    {
        public DocPredefinedModel()
        {
            DocumentValues = new List<DocumentValueModel>();
        }

        /// <summary>
        /// spGetDocumentsPredefined
        /// </summary>
        /// <param name="row"></param>
        public DocPredefinedModel(DataRow row)
        {
            Key = "" + row["DocumentID_OperationTypeOfficeID_OperationTypeID"];
            DocumentId = "" + row["DocumentID"];
            DocumentName = "" + row["DocumentName"];
            
            DocumentTypeName = "" + row["DocumentTypeName"];
            DocumentOperationChar = "" + row["DocumentOperationChar"];
            DocumentTypeCode = "" + row["DocumentTypeCode"];
            OperationTypeId = "" + row["OperationTypeID"];

            BarCodeFixedText = "" + row["BarCodeFixedText"];
            UseDate = Convert.ToBoolean(row["UseDate"]);
            UseVvNumber = Convert.ToBoolean(row["UseVirtualVaultNumber"]);

            ImportTypeId = "" + row["ImportTypeId"];
            ExportTypeId = "" + row["ExportTypeId"];
            FormatTypeId = "" + row["FormatTypeId"];
            CorelFile = "" + row["CorelFile"];
            DocumentValues = new List<DocumentValueModel>();
        }

        public string Key { get; set; }
        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentTypeCode { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentOperationChar { get; set; }
        public string CorelFile { get; set; }
        
        public string BarCodeFixedText { get; set; }
        public bool UseDate { get; set; }
        public bool UseVvNumber { get; set; }
       
        public string ImportTypeId { get; set; }
        public string ExportTypeId { get; set; }
        public string FormatTypeId { get; set; }
        public string OperationTypeId { get; set; }
        public string IsAttachedMyCp { get; set; }  //-- 'Y'/'No'
        public List<DocumentValueModel> DocumentValues { get; set; }
        public static DocPredefinedModel GetNewDocument(string documentTypeCode)
        {
            return new DocPredefinedModel
            {
                DocumentTypeCode = documentTypeCode,
                DocumentId = "0", 
                DocumentName = "New", 
                ImportTypeId = "1", ExportTypeId = "1", 
                FormatTypeId = "1",
                IsAttachedMyCp = "N",
                UseDate = true,
                UseVvNumber = true,
            };
        }
    }
}