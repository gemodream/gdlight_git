﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class PriceRangeModel
    {
        public PriceRangeModel()
        {
        }

        /// <summary>
        /// spGetPriceRangeByCPID
        /// </summary>
        /// <param name="row"></param>
        public PriceRangeModel(DataRow row)
        {
            Rownum = Convert.ToInt32(row["rownum"]);
            PriceId = Convert.ToInt32(row["PriceID"]);
            HomogeneousClassId = "" + row["HomogeneousClassID"];
            ValueFrom = Convert.ToDouble(row["ValueFrom"]);
            ValueTo = Convert.ToDouble(row["ValueTo"]);
            Price = Convert.ToDouble(row["Price"]);
        }

        public int PriceId { get; set; }
        public string HomogeneousClassId { get; set; }
        public double ValueFrom { get; set; }
        public double ValueTo { get; set; }
        public double Price { get; set; }
        public int Rownum { get; set; }
    }
}