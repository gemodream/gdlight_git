﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpReturnDataModel
    {
        public CpReturnDataModel()
        {

        }
        public string DociId { get; set; }
        public string CustomerCode { get; set; }
        public string ProgramName { get; set; }
        public string AttachedReport { get; set; }
    }
}