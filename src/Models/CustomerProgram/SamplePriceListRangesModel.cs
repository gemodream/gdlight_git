﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class SamplePriceListRangesModel
    {
        public SamplePriceListRangesModel()
        {

        }
        public SamplePriceListRangesModel(DataRow row)
        {
            ValueFrom = row["ValueFrom"].ToString();
            ValueTo = row["ValueTo"].ToString();
            Price = row["Price"].ToString();
            //CPID = row["CPID"].ToString();

        }
        public string ValueFrom { get; set; }
        public string ValueTo { get; set; }
        public string Price { get; set; }
        //public string CPID { get; set; }
    }
	public class SampleServicePriceModel
    {
        public SampleServicePriceModel()
        {

        }
        public SampleServicePriceModel(DataRow row)
        {
            ServiceId = "" + row["ASID"];
            ServiceName = "" + row["Expr1"];
            Price = Convert.ToDouble(row["Price"]);
            PriceId = Convert.ToInt32(row["PriceID"]);
            Rownum = Convert.ToInt32(row["rownum"]);
        }
        public double Price { get; set; }
        public int PriceId { get; set; }
        public int Rownum { get; set; }
        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}