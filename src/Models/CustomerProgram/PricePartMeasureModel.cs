﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class PricePartMeasureModel
    {
        public PricePartMeasureModel()
        {
        }

        /// <summary>
        /// spGetPricePartsMeasuresByCPID
        /// </summary>
        /// <param name="row"></param>
        public PricePartMeasureModel(DataRow row)
        {
            PriceId = Convert.ToInt32(row["PriceID"]);
            PartId = "" + row["PartID"];
            MeasureCode = "" + row["MeasureCode"];
            HomogeneousClassId = "" + row["HomogeneousClassId"];
            PartNameMeasureName = "" + row["PartNameMeasureName"];
            RowNum = Convert.ToInt32(row["rownum"]);
        }

        public int PriceId { get; set; }
        public string PartId { get; set; }
        public string MeasureCode { get; set; }
        public string HomogeneousClassId { get; set; }
        public string PartNameMeasureName { get; set; }
        public int RowNum { get; set; }

    }
}