﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class ItemTypeModel
    {
        public ItemTypeModel()
        {
        }

        /// <summary>
        /// spGetItemTypesByGroup
        /// </summary>
        /// <param name="row"></param>
        /// <param name="isRecently"> </param>
        public ItemTypeModel(DataRow row, bool isRecently)
        {
            ItemTypeGroupId = Convert.ToInt32(row["ItemTypeGroupID"]);
            ItemTypeId = Convert.ToInt32(row["ItemTypeID"]);
            ItemTypeName = "" + row["ItemTypeName"];
            Path2Icon = "" + row["Path2Icon"];
            IsRecently = isRecently;
        }

        public int ItemTypeGroupId { get; set; }
        public int ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        public string Path2Icon { get; set; }
        public bool IsRecently { get; set; }
        public string ImageUrl
        {
            get
            {
                return string.IsNullOrEmpty(Path2Icon) ? "" : "Images/ItemTypeImages/" + Path2Icon;
            }
        }

        const string TitleFormat = "<img src='{0}' width='16px'/>{1}";
        public string Title
        {
            get { return string.IsNullOrEmpty(ImageUrl) ? "" : string.Format(TitleFormat, ImageUrl, ItemTypeName); }
        }
        /// <summary>
        /// select * from v0ItemType where ItemTypeId in (...)
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static ItemTypeModel Create1(DataRow row)
        {
            return new ItemTypeModel
                       {ItemTypeId = Convert.ToInt32(row["ItemTypeID"]), ItemTypeName = "" + row["ItemTypeName"]};
        }
    }
    public class SKUTypesLIstModel
    {
        public SKUTypesLIstModel()
        {
             
        }
        public SKUTypesLIstModel(DataRow row)
        {
            ItemTypeID = Convert.ToInt32(row["ItemTypeID"]);
            ItemTypeName = "" + row["ItemTypeName"];
        }
        public int ItemTypeID { get; set; }
        public string ItemTypeName { get; set; }
    }
}