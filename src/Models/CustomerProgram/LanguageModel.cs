﻿using System.Data;

namespace Corpt.Models.CustomerProgram
{
    public class LanguageModel
    {
        /// <summary>
        /// spGetDocumentLanguage
        /// </summary>
        /// <param name="row"></param>
        public LanguageModel(DataRow row)
        {
            Id = "" + row["DocumentLanguageID"];
            Name = "" + row["Name"];
        }

        public string Id { get; set; }
        public string Name { get; set; }
    }

}