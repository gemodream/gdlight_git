﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class ItemTypeGroupModel
    {
        public ItemTypeGroupModel()
        {
            
        }
        /// <summary>
        /// spGetItemTypeGroups
        /// </summary>
        /// <param name="row"></param>
        public ItemTypeGroupModel(DataRow row)
        {
            ItemTypeGroupId = Convert.ToInt32("" + row["ItemTypeGroupID"]);
            ItemTypeGroupName = "" + row["ItemTypeGroupName"];
            Path2Icon = "" + row["Path2Icon"];
        }

        public int ItemTypeGroupId { get; set; }
        public string ItemTypeGroupName { get; set; }
        public string Path2Icon { get; set; }
        public string ImageUrl
        {
            get
            {
                return string.IsNullOrEmpty(Path2Icon) ? "" : "Images/ItemTypeImages/" + Path2Icon;
            }
        }

        const string TitleFormat = "<img src='{0}'  width='16px'/>{1}";
        public string Title
        {
            get { return string.IsNullOrEmpty(ImageUrl) ? ItemTypeGroupName : string.Format(TitleFormat, ImageUrl, ItemTypeGroupName); }
        }

    }
}