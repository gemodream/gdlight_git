﻿using System.Data;

namespace Corpt.Models.CustomerProgram
{
    public class OperationModel
    {
        /// <summary>
        /// spGetOperationTree
        /// </summary>
        /// <param name="row"></param>
        public OperationModel(DataRow row)
        {
            TreeParentId = "" + row["TreeParentID"];
            TreeId = "" + row["TreeID"];
            TreeItemName = "" + row["TreeItemName"];
            OperationTypeId = "" + row["OperationTypeID"];
            OperationTypeOfficeId = "" + row["OperationTypeOfficeID"];
        }

        public string TreeParentId { get; set; }
        public string TreeId { get; set; }
        public string TreeItemName { get; set; }
        public string OperationTypeId { get; set; }
        public string OperationTypeOfficeId { get; set; }
    }
}