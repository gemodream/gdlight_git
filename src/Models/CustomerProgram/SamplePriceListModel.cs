﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SamplePriceListModel
    {

        public SamplePriceListModel()
        {

        }
        public SamplePriceListModel(DataRow row)
        {
            PriceRangeID = row["PriceRangeID"].ToString();
            SamplePriceName = row["SamplePriceName"].ToString();
            PriceID = row["PriceID"].ToString();
            CPID = row["CPID"].ToString();

        }
        public string PriceRangeID { get; set; }
        public string SamplePriceName { get; set; }
        public string PriceID { get; set; }
        public string CPID { get; set; }
    }
    [Serializable]
    public class SavedDescCommentsListModel
    {
        public SavedDescCommentsListModel()
        {

        }
        public SavedDescCommentsListModel(DataRow row)
        {
            ID = Convert.ToInt32(row["ID"].ToString());
            Type = row["Type"].ToString();
            Name = row["Name"].ToString();
            Text = row["Text"].ToString();
            ItemTypeID = Convert.ToInt32(row["ItemTypeID"].ToString());
        }
        public int ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int ItemTypeID { get; set; }
    }
}