﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DocumentTypeModel
    {
        public DocumentTypeModel(DataRow row)
        {
            DocumentTypeCode = "" + row["DocumentTypeCode"];
            DocumentTypeName = "" + row["DocumentTypeName"];
            DocumentOperationChar = "" + row["DocumentOperationChar"];
            DocumentFileName = "" + row["DocumentTypeReportFileName"];
        }

        public string DocumentTypeCode { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentOperationChar { get; set; }
        public string DocumentFileName { get; set; }
    }
}