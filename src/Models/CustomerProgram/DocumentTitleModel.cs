﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DocumentTitleModel
    {
        /// <summary>
        /// spGetDefaultDocumentTitle
        /// </summary>
        /// <param name="row"></param>
        public DocumentTitleModel(DataRow row)
        {
            TitleCode = "" + row["TitleCode"];
            TitleName = "" + row["TitleName"];
        }

        public string TitleCode { get; set; }
        public string TitleName { get; set; }
        
    }
}