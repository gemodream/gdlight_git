﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpDocRuleEditModel
    {
        public CpDocRuleEditModel()
        {

        }
        public CpDocRuleEditModel(int docId, MeasureModel measure,  List<EnumMeasureModel> enums, CpDocRuleModel rule, string partId)
        {
            DocId = docId;
            PartTypeId = Convert.ToInt32(measure.PartTypeId);
            PartId = partId;
            MeasureId = measure.MeasureId;
            MeasureName = measure.MeasureName;
            MeasureClass = measure.MeasureClass;
            if (enums != null)
            {
                // Add empty item
                if (enums.Find(m => string.IsNullOrEmpty(m.MeasureValueName)) == null)
                {
                    var empty = new EnumMeasureModel
                    {
                        ValueTitle = "",
                        MeasureValueName = "(not set)",
                        MeasureValueId = 0, 
                        MeasureClass = MeasureModel.MeasureClassEnum,
                        MeasureValueMeasureId = measure.MeasureId
                    };
                    enums.Insert(0, empty);
                }
            }
            EnumSource = enums;
            if (rule != null)
            {
                MinValue = rule.MinMeasure;
                MaxValue = rule.MaxMeasure;
                IsDefault = rule.IsDefaultMeasureValue;
                IsInfoOnly = rule.IsInfoOnlyMeasureValue;
                NotVisibleInCcm = rule.NotVisibleInCcm;
            }
            if (MeasureClass == MeasureModel.MeasureClassEnum)
            {
                if (!string.IsNullOrEmpty(MinValue))
                {
                    if (MinValue.Contains("."))
                    {
                        MinValue = MinValue.Split('.')[0];
                    }
                } else
                {
                    MinValue = "0";
                }
                if (!string.IsNullOrEmpty(MaxValue))
                {
                    if (MaxValue.Contains("."))
                    {
                        MaxValue = MaxValue.Split('.')[0];
                    }
                }
                else
                {
                    MaxValue = "0";
                }

            }
        }

        public string UniqueKey
        {
            get { return string.Format("{0};{1};{2}", DocId, PartId, MeasureId); }
        }
        public int DocId { get; set;}
        public int PartTypeId { get; set; }
        public string PartId { get; set; }
        public int MeasureId { get; set; }
        public int MeasureClass { get; set; }
        public string MeasureName { get; set; }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public bool HasValues
        {
            get { return Do || NotVisibleInCcm; }
        }
        public bool Do
        {
            get { return (!string.IsNullOrEmpty(MinValue) && MinValue !="0") || (!string.IsNullOrEmpty(MaxValue) && MaxValue != "0") ; }
        }
        public bool IsDefault { get; set; }
        public bool IsInfoOnly { get; set; }
        public bool NotVisibleInCcm { get; set; }
        public string DisplayMinValue 
        { 
            get
            {
                if (string.IsNullOrEmpty(MinValue) || MinValue=="0") return "";
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    if (EnumSource != null)
                    {
                        var enumId = (int)Convert.ToDecimal(MinValue);
                        var enumModel = EnumSource.Find(m => m.MeasureValueMeasureId == MeasureId && m.MeasureValueId == enumId);
                        if (enumModel != null)
                        {
                           return enumModel.MeasureValueName;
                        }
                    }
                    else
                    {
                        return MinValue;
                    }

                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return string.Format("{0:0.##}", Convert.ToDecimal(MinValue));
                }
                return MinValue;
            }
        }
        public string DisplayMaxValue
        {
            get
            {
                if (string.IsNullOrEmpty(MaxValue) || MaxValue == "0") return "";
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    if (EnumSource != null)
                    {
                        var enumId = (int)Convert.ToDecimal(MaxValue);
                        var enumModel = EnumSource.Find(m => m.MeasureValueMeasureId == MeasureId && m.MeasureValueId == enumId);
                        if (enumModel != null)
                        {
                            return enumModel.MeasureValueName;
                        }
                    }
                    else
                    {
                        return MaxValue;
                    }

                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return string.Format("{0:0.##}", Convert.ToDecimal(MaxValue));
                }
                return MaxValue;
            }
        }
        public List<EnumMeasureModel> EnumSource { get; set; }
    }
}