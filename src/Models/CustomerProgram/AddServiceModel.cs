﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class AddServiceModel
    {
        public AddServiceModel()
        {
        }

        /// <summary>
        /// spGetAdditionalService
        /// </summary>
        /// <param name="row"></param>
        public AddServiceModel(DataRow row)
        {
            ServiceId = "" + row["AdditionalServiceID"];
            ServiceName = "" + row["Name"];
        }

        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}