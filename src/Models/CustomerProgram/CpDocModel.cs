﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpDocModel
    {
        public CpDocModel()
        {
            EditDocRules = new List<CpDocRuleEditModel>();
            MeasureGroups = new List<MeasureGroupModel>();
            AttachedReports = new List<CpDocPrintModel>();
            RulesCount = 0;
        }

        /// <summary>
        /// spGetCPDocs
        /// </summary>
        /// <param name="row"></param>
        public CpDocModel(DataRow row)
        {
            CpId = "" + row["CPID"];
            CpOfficeId = "" + row["CPOfficeID"];
            CpHistoryId = "" + row["CustomerProgramHistoryID"];
            CpDocId = Convert.ToInt32(row["CPDocID"]);
            IsReturn = ("" + row["IsReturn"]) == "1";
            Description = "" + row["Description"];
            RulesCount = Convert.ToInt32(row["nDocRule"]);
            IsDefault = RulesCount == 0;
            EditDocRules = new List<CpDocRuleEditModel>();
            MeasureGroups = new List<MeasureGroupModel>();
            AttachedReports = new List<CpDocPrintModel>();
        }

        public string CpId { get; set; }
        public string CpOfficeId { get; set; }
        public int CpDocId { get; set; }
        public string CpHistoryId { get; set; }
        public int RulesCount { get; set; }
        public bool IsReturn { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public bool IsDefault { get; set; }
        public List<CpDocRuleEditModel> EditDocRules { get; set; }
        public List<MeasureGroupModel> MeasureGroups { get; set; }
        public List<CpDocPrintModel> AttachedReports { get; set; }
        public bool HasValue
        {
            get
            {
                if (IsDefault)
                {
                    return MeasureGroups.FindAll(m => m.NoRecheck > 0).Count > 0 || AttachedReports.Count > 0;
                }
                return EditDocRules.FindAll(m => m.HasValues).Count > 0;
            }
        }
    }
}