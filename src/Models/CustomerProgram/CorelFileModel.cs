﻿using System;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CorelFileModel
    {
        public CorelFileModel()
        {
            
        }
        public CorelFileModel(string name, bool exists)
        {
            Name = name;
            Exists = exists;
        }

        public string Name { get; set; }
        public bool Exists { get; set; }
        public string DisplayName {
            get { return Name + (Exists ? "" : " (Doesn't exist!)"); }
        }
    }
}