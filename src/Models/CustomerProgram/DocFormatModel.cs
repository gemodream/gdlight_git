﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DocFormatModel
    {
        /// <summary>
        /// spGetImpExInfo, Tables[2]
        /// </summary>
        /// <param name="row"></param>
        public DocFormatModel(DataRow row)
        {
            Id = "" + row["FormatID"];
            Name = "" + row["Format"];
            Order = "" + row["nOrder"];
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Order { get; set; }
    }
}