﻿using System;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class MeasureGroupModel
    {
        public MeasureGroupModel()
        {
        }

        /// <summary>
        /// spGetMeasureGroups, isDo = false
        /// spGetCPDoc_MeasureGroup, isDo = true
        /// </summary>
        /// <param name="row"></param>
        /// <param name="byDoc"> </param>
        public MeasureGroupModel(DataRow row, bool byDoc)
        {
            MeasureGroupId = Convert.ToInt32(row["MeasureGroupID"]);
            if (byDoc)
            {
                NoRecheck = Convert.ToInt32(row["NoRecheck"]);
            } else
            {
                MeasureGroupName = "" + row["MeasureGroupName"];
                NoRecheck = 0;
            }
        }

        public string CpDocId { get; set; }
        public int MeasureGroupId { get; set; }
        public string MeasureGroupName { get; set; }
        public int NoRecheck { get; set; }
        public string UniqueKey { get { return CpDocId + ";" + MeasureGroupId; } }
        public bool Do
        {
            get { return NoRecheck > 0; }
        }

        public static MeasureGroupModel Copy(MeasureGroupModel src, int cpDocId)
        {
            return new MeasureGroupModel
            {
                MeasureGroupId = src.MeasureGroupId,
                MeasureGroupName = src.MeasureGroupName,
                CpDocId =  ""+cpDocId
            };
        }
    }
}