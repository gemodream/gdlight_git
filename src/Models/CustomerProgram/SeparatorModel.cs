﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class SeparatorModel
    {
        public SeparatorModel(string text, string value, string separator)
        {
            DisplayName = text;
            Value = value;
            Separator = separator;
        }

        public string DisplayName { get; set; }
        public string Value { get; set; }
        public string Separator { get; set; }

        public static List<SeparatorModel> GetSeparatorList()
        {
            var list = new List<SeparatorModel>();
            list.Add(new SeparatorModel("/", "1", "/"));
            list.Add(new SeparatorModel("x", "2", "x"));
            list.Add(new SeparatorModel("*", "3", "*"));
            list.Add(new SeparatorModel("Space", "4", " "));
            list.Add(new SeparatorModel("Space x 2", "5", "  "));
            list.Add(new SeparatorModel("Space x 3", "6", "   "));
            list.Add(new SeparatorModel("Space x 4", "7", "    "));
            return list;
        }
        public static string AddFormatingValue(string dst, string part, string measure, string separatorValue)
        {
            var sm = GetSeparatorList().Find(m => m.Value == separatorValue);
            if (dst.Length == 0) return string.Format("[{0}.{1}]", part, measure);
            return dst + 
                (sm.Separator.StartsWith(" ") ? 
                string.Format("{0}[{1}.{2}]", sm.Separator, part, measure) : 
                String.Format(" {0} [{1}.{2}]", sm.Separator, part, measure));
        }
        public static string AddFormatingTitle(string dst, string title, string separatorValue)
        {
            var sm = GetSeparatorList().Find(m => m.Value == separatorValue);
            if (dst.Length == 0) return String.Format(" {0} ", title);
            return dst + string.Format("{0} {1}", sm.Separator, title);
        }
    }
}