﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class DefineDocumentInitModel
    {
        public DefineDocumentInitModel()
        {
            MeasureParts = new List<MeasurePartModel>();
            MeasuresByParts = new List<MeasureModel>();
            ImportInfo = new List<ImpExpInfoModel>();
            ExportInfo = new List<ImpExpInfoModel>();
            FormatInfo = new List<DocFormatModel>();
        }

        public string CpId { get; set; }
        public string CpOfficeId { get; set; }
        public string CpDocId { get; set; }

        public string ItemTypeId { get; set; }
        public DocumentTypeModel DocumentType { get; set; }
        public CustomerProgramModel Cp { get; set; }
        public List<MeasurePartModel> MeasureParts { get; set; }
        public List<MeasureModel> MeasuresByParts { get; set; }
        public List<ImpExpInfoModel> ImportInfo { get; set; }
        public List<ImpExpInfoModel> ExportInfo { get; set; }
        public List<DocFormatModel> FormatInfo { get; set; }
        public List<DocPredefinedModel> Documents { get; set; }
    }
}