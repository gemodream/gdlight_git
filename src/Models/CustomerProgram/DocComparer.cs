﻿using System.Collections.Generic;
using Corpt.Utilities;

namespace Corpt.Models.CustomerProgram
{
    public class DocComparer : IComparer<CpDocPrintModel>
    {
        public int Compare(CpDocPrintModel x, CpDocPrintModel y)
        {
            var result = ComparerUtils.NumberCompare(x.Order, y.Order);
            if (result == 0)
            {
                result = ComparerUtils.StringCompare(x.OperationTypeName.ToUpper(), y.OperationTypeName.ToUpper());
            }
            return result;
        }
    }
}