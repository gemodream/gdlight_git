﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpDocPrintModel
    {
        public CpDocPrintModel()
        {
        }
        public CpDocPrintModel(CpDocPrintModel other)
        {
            DocumentTypeCode = other.DocumentTypeCode;
            OperationChar = other.OperationChar;
            OperationTypeName = other.OperationTypeName;
            Key = other.Key;
            OperationTypeOfficeId = other.OperationTypeOfficeId;
            OperationTypeId = other.OperationTypeId;
            IsAttachedToCp = other.IsAttachedToCp;
            Order = other.Order;
        }

        /// <summary>
        /// spGetDocsByCP
        /// </summary>
        /// <param name="row"></param>
        /// <param name="cpKey"> </param>
        public CpDocPrintModel(DataRow row, string cpKey)
        {
            DocumentTypeCode = "" + row["DocumentTypeCode"];
            OperationChar = "" + row["OperationChar"];
            OperationTypeName = "" + row["OperationTypeName"];
            Key = "" + row["OperationTypeOfficeID_OperationTypeID"];
            CpKey = cpKey;
            var keys = Key.Split('_');
            if (keys.Length == 2)
            {
                OperationTypeOfficeId = keys[0];
                OperationTypeId = keys[1];
            } else
            {
                OperationTypeId = Key;
            }
            IsAttachedToCp = keys.Length == 2;
            Order = Convert.ToInt32(row["nOrder"]);
        }

        /// <summary>
        /// spGetCurrentDocsByCP
        /// </summary>
        /// <param name="row"></param>
        public CpDocPrintModel(DataRow row)
        {
            OperationChar = "" + row["OperationChar"];
            OperationTypeName = "" + row["OperationTypeName"];
            Key = "" + row["OperationTypeOfficeID_OperationTypeID"];
            var keys = Key.Split('_');
            if (keys.Length == 2)
            {
                OperationTypeOfficeId = keys[0];
                OperationTypeId = keys[1];
            }
            else
            {
                OperationTypeId = Key;
            }
            IsAttachedToCp = true;
            Order = Convert.ToInt32(row["nOrder"]);
        }
        public string OperationChar { get; set; }
        public string OperationTypeName { get; set; }
        public string DocumentTypeCode { get; set; }
        public bool IsAttachedToCp { get; set; }
        public bool IsAttachedToDoc { get { return DocId > 0; } }
        public string OperationTypeOfficeId { get; set; }
        public string OperationTypeId { get; set; }
        public string Key { get; set; }
        public int Order { get; set; }
        public int DocId { get; set; }
        public string DocumentName
        {
            get
            {
                var p = OperationTypeName.LastIndexOf(',');
                return p < 0 ? OperationTypeName : OperationTypeName.Substring(0, p);
            }
        }

        public string CpKey { get; set; }
        public static string DocumentUrlFormat = "<a href='" +
            "DefineDocument.aspx?" +
                    DefineDocument.ParamCpKey + "={0}&" +
                    DefineDocument.ParamDocTypeCode + "={1}&" +
                    DefineDocument.ParamCpDocId + "={2}&" +
                    DefineDocument.ParamOperationTypeId + "={3}" +
                    "' target='_blank'>{4}" + "<img src='{5}' />" + "</a>"; 
        public string DisplayUrl
        {
            get
            {
                //"\" target=\"_blank\">" + "DownLoad" + "</a>"
                return string.Format(DocumentUrlFormat, CpKey, DocumentTypeCode, "", OperationTypeId, DocumentName, !IsAttachedToCp ? "" : "Images/ajaxImages/attach.png");
                //var Title = "<img alt=\"\" src=\"" + StateTarget.ImageUrl + "\" title=\"" + StateTarget.ImageTooltip + "\"/> " + id;
            }
        }
        /// <summary>
        /// spGetCPDoc_Operation
        /// </summary>
        /// <param name="row"></param>
        /// <param name="all"> </param>
        /// <returns></returns>
        public static CpDocPrintModel CreateAttachedReport(DataRow row, List<CpDocPrintModel> all)
        {
            var doc = new CpDocPrintModel
            {
                Key = "" + row["OperationTypeOfficeID_OperationTypeID"], 
                DocId = Convert.ToInt32(row["CPDocID"]), 
                OperationTypeId = "" + row["OperationTypeID"],
                OperationTypeOfficeId = "" + row["OperationTypeOfficeID"],
                Order = 1
            };
            var find = all.Find(m => m.Key == doc.Key);
            //if (find == null)
            //    return doc;
            doc.OperationTypeName = find.OperationTypeName;
            doc.DocumentTypeCode = find.DocumentTypeCode;
            doc.OperationTypeOfficeId = find.OperationTypeOfficeId;
            doc.OperationTypeId = find.OperationTypeId;
            return doc;
        }
        
    }
}