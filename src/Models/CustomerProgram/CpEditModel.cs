﻿using System;
using System.Collections.Generic;

namespace Corpt.Models.CustomerProgram
{
    [Serializable]
    public class CpEditModel
    {
        public CpEditModel()
        {
            CpDocs = new List<CpDocModel>();
            OperationTypes = new List<string>();
            Reports = new List<CpDocPrintModel>();
            MeasureParts = new List<MeasurePartModel>();
            MeasuresByItemType = new List<MeasureModel>();
            CpPrice = new CpPriceModel();
        }

        public CustomerProgramModel Cp { get; set; }
        public List<CpDocModel> CpDocs { get; set; }
        public List<CpDocPrintModel> Reports { get; set; } 
        public List<string> OperationTypes { get; set; }
        public List<MeasurePartModel> MeasureParts { get; set; }
        public List<MeasureModel> MeasuresByItemType { get; set; }
        public CustomerModel Vendor { get; set; }
        public CustomerModel Customer { get; set; }
        public CpPriceModel CpPrice { get; set; }
        public string OperationTypeOfficeId { get; set; }
        
    }
    
}