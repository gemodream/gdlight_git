﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemStructModel
    {
        /// <summary>
        /// From spGetItemStructureByBatchID
        /// </summary>
        /// <param name="row"></param>
        public ItemStructModel(DataRow row)
        {
            PartName = "" + row["PartName"];
        }
        public string PartName { get; set; }
    }
}