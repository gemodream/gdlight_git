﻿using System;
using System.Data;
using System.Globalization;

namespace Corpt.Models
{
    [Serializable]
    public class PrintedModel
    {
        /// <summary>
        /// spGetPrintingHistory
        /// </summary>
        /// <param name="row"></param>
        /// <param name="batchId"> </param>
        public PrintedModel(DataRow row, int batchId)
        {
            BatchId = batchId;
            OrderCode = "" + row["GroupCode"];
            BatchNumber = ("" + row["BatchCode"]).Replace(".", "");
            PrevBatchNumber = ("" + row["PrevBatchCode"]).Replace(".", "");
            FullItemNumber = ("" + row["ItemCode"]).Replace(".", "");
            OldFullItemNumber = ("" + row["PrevItemCode"]).Replace(".", "");
            OperationChar = "" + row["OperationChar"];
            ReportName = "" + row["Report"];
            ReportStatus = "" + row["ReportStatus"];
            ReportDate = "" + row["ReportDate"];
        }
        public string OrderCode { get; set; }
        public int BatchId { get; set; }
        public string BatchNumber { get; set; }
        public string PrevBatchNumber { get; set; }
        public string FullItemNumber { get; set; }
        public string OldFullItemNumber { get; set; }
        public string OperationChar { get; set; }
        public string ReportName { get; set; }
        public string ReportStatus { get; set; }
        public string ReportDate { get; set; }
        public bool IsPrinted
        {
            get { return ReportStatus.ToLower() == "printed"; }
        }

    }
}