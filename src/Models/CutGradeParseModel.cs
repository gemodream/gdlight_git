﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class CutGradeParseModel
    {
        public CutGradeParseModel() { }
        public string PartId {get;set;}
        public string PartName {get;set;}
        public string MeasureName {get;set; }
        public string MeasureValue {get; set; }
    }
}