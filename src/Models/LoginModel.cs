﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class LoginModel
    {
        public LoginModel()
        {
            User = new UserModel();
            UserIp = new IpModel();
        }

        public string LoginName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string OfficeIpGroup 
        { 
            get { return "NY"; }
        }
        public int AuthorOfficeId { get; set; }
        public UserModel User { get; set; }
        public IpModel UserIp { get; set; }
        public bool Error { get; set; }
        public string ErrorText { get; set; }
    }
}