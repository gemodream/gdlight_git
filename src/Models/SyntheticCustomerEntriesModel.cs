﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticCustomerEntriesModel
	{
		public SyntheticCustomerEntriesModel() { }

        public int ID { get; set; }
        public int RequestID { get; set; }
        public int GSIOrder { get; set; }
        public int CustomerCode { get; set; }
        public string MemoNum { get; set; }
		public string PONum { get; set; }
		public string SKUName { get; set; }
		public string StyleName { get; set; }
        public int TotalQty { get; set; }
        public int ServiceTypeCode { get; set; }
        public int DeliveryMethodCode { get; set; }
        public int CarrierCode { get; set; }
        public string RetailerName { get; set; }		
        public int RetailerID { get; set; }
        public int CategoryCode { get; set; }
        public int ServiceTimeCode { get; set; }
		public int JewelryTypeCode { get; set; }
		
		public string SpecialInstructions { get; set; }
        public string Comment { get; set; }
        public string Messenger { get; set; }

        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public int PersonID { get; set; }

        public string Account { get; set; }
        public Boolean IsSave { get; set; }
		public int Status { get; set; }

        public decimal TotalWeight { get; set; }
        public string MemoReceiptPDF { get; set; }
        public int GDServiceTypeId { get; set; }
        public string BatchMemo { get; set; }
        public string ASN { get; set; }

        //IvanB 28/03
        public int BNCategoryId { get; set; }
        public int BNDestinationId { get; set; }
        //IvanB 28/03
    }
}