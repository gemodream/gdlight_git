﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class EventModel
    {
        public EventModel(){}
        /// <summary>
        /// trkSpGetTrackingActions
        /// </summary>
        /// <param name="row"></param>
        public EventModel(DataRow row)
        {
            EventId = Int32.Parse("" + row["EventID"]);
            EventName = "" + row["EventName"];
        }
        public int EventId { get; set; }
        public string EventName { get; set; }
    }
}