﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SyntheticShortStatsModel
    {
        public SyntheticShortStatsModel() { }
        public SyntheticShortStatsModel(DataRow row)
        {
            OfficeID = "" + row["OfficeID"];
            Memo = "" + row["Memo"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            ReadyDate = row["ReadyDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ReadyDate"]);
            LastModifiedDate = row["LastModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["LastModifiedDate"]);
            PickupDate = row["PickupDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["PickupDate"]);
            Status = "" + row["Status"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            QtyPass = row["QtyPass"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyPass"]);
            QtySynth = row["QtySynth"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySynth"]);
            QtySusp = row["QtySusp"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySusp"]);
            CustomerCode = row["CustomerCode"] == DBNull.Value ? (int?)null : int.Parse("" + row["CustomerCode"]);
            CompanyName = "" + row["CompanyName"];
            Destination = "" + row["Destination"];
            UserName = "" + row["UserName"];
            Notes = "" + row["Notes"];
        }

        public string OfficeID { get; set; }
        public string Memo { get; set; }
        public int? GSIOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ReadyDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? PickupDate { get; set; }
        public string Status { get; set; }
        public int? TotalQty { get; set; }
        public int? QtyPass { get; set; }
        public int? QtySynth { get; set; }
        public int? QtySusp { get; set; }
        public int? CustomerCode { get; set; }
        public string CompanyName {get; set;}
        public string Destination { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
    }
}