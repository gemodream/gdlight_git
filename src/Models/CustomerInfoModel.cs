﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class CustomerInfoModel
    {
        public CustomerInfoModel()
        {
        }
        /// <summary>
        /// spFindUser
        /// </summary>
        /// <param name="row"></param>
        public CustomerInfoModel(DataRow row)
        {
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
            MiddleName = "" + row["MI"];
            Address = "" + row["Address"];

            City = "" + row["City"];
            Zip = "" + row["Zip"];
            State = "" + row["State"];
            DayPhone = "" + row["DayPhone"];
            OtherPhone = "" + row["OtherPhone"];
            Email = "" + row["EmailAddress"];
            Password = "" + row["Password"];
            Id = "" + row["ID"];
            CreatedDate = Convert.ToDateTime(row["CreatedDate"]);
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string DayPhone { get; set; }
        public string OtherPhone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Id { get; set; }
        
        public string CcResponse { get; set; }
        public string ReportNumber { get; set; }
        public string VirtualVaultNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DisplayName
        {
            get { return FirstName + " " + LastName; }
        }
        /// <summary>
        /// spFindByReportNumber
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CustomerInfoModel CreateModel(DataRow row)
        {
            var model = new CustomerInfoModel(row)
            {
                VirtualVaultNumber = "" + row["VirtualVaultNumber"],
                ReportNumber = "" + row["ReportNumber"]
            };
            return model;
        }
    }
}