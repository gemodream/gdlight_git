﻿using System;
using System.Collections.Generic;
using Corpt.Models.CustomerProgram;

namespace Corpt.Models
{
    [Serializable]
    public class UpdateOrderBatchModel
    {
        public UpdateOrderBatchModel()
        {
            CurrentDocs = new List<CpDocPrintModel>();
            EditItems = new List<UpdateOrderEditModel>();
        }
        public string BatchId { get; set; }
        public int ItemsInBatch { get; set; }
        public CustomerProgramModel Cp { get; set; }
        public List<CpDocPrintModel> CurrentDocs { get; set; }
        public List<UpdateOrderEditModel> EditItems { get; set; }
    }
}