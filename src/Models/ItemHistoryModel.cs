﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ItemHistoryModel
    {
        public ItemHistoryModel()
        { }
        public ItemHistoryModel(DataRow row)
        {
            NewNumber = "" + row["NewNumber"];
            OldNumber = "" + row["OldNumber"];
            StartDate = Convert.ToDateTime(row["StartDate"]);
            PartName = "" + row["PartName"];
            MeasureName = "" + row["MeasureName"];
            RecheckNumber = "" + row["RecheckNumber"];
            OldValue = "" + row["OldValue"];
            NewValue = "" + row["NewValue"];
            Person = "" + row["Person"];
        }

        public string NewNumber { get; set; }
        public string OldNumber { get; set; }
        public DateTime StartDate { get; set; }
        public string PartName { get; set; }
        public string MeasureName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string Person { get; set; }
        public string RecheckNumber { get; set; }
    }
}