﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BillingInfoExModel : BillingInfoModel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        public BillingInfoExModel(DataRow row)
        {
            BatchId = "" + row["BatchID"];
            ItemCode = "" + row["ItemCode"];
            Pass = Convert.ToBoolean(row["Pass"]);
            CheckDescrip = "" + row["Descrip"];
        }
        public bool Pass { get; set; }
        public string CheckDescrip { get; set; }
    }
}