﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class EnumMeasureModel
    {
        public EnumMeasureModel()
        {
        }

        public EnumMeasureModel(string measureId)
        {
            MeasureValueMeasureId = Int32.Parse(measureId);
            ValueTitle = "";
            NOrder = -10;
        }
        public EnumMeasureModel(DataRow row)
        {
            MeasureValueMeasureId = Int32.Parse("" + row["MeasureValueMeasureID"]);
            if (string.IsNullOrEmpty("" + row["nOrder"]))
            {
                NOrder = 0;
            }
            else
            {
                NOrder = Int32.Parse("" + row["nOrder"]);
            }
            MeasureValueId = Int32.Parse("" + row["MeasureValueID"]);
            MeasureClass = Int32.Parse("" + row["MeasureClass"]);
            MeasureValueName = "" + row["MeasureValueName"];
            ValueTitle = ("" + row["ValueTitle"]).Trim();
        }
        public EnumMeasureModel(DataRow row, bool isVariety)
        {
            if (isVariety)
            {
                NOrder = 0;
                MeasureValueId = Int32.Parse("" + row["VarietyID"]);
                MeasureValueName = "" + row["VarietyName"];
                ValueTitle = ("" + row["VarietyTitle"]).Trim();
            }
            else 
            {
                NOrder = 0;
                MeasureValueId = Int32.Parse("" + row["MeasureValueID"]);
                MeasureValueName = "" + row["MeasureValueName"];
                ValueTitle = ("" + row["ValueTitle"]).Trim();
            }
        }
        //IvanB 25/04 start
        public EnumMeasureModel(DataRow row, int measureClass)
        {
            MeasureValueMeasureId = Int32.Parse("" + row["MeasureValueMeasureID"]);
            if (string.IsNullOrEmpty("" + row["nOrder"]))
            {
                NOrder = 0;
            }
            else
            {
                NOrder = Int32.Parse("" + row["nOrder"]);
            }
            MeasureValueId = Int32.Parse("" + row["MeasureValueID"]);
            MeasureClass = measureClass;
            MeasureValueName = "" + row["MeasureValueName"];
            ValueTitle = ("" + row["ValueTitle"]).Trim();
        }
        //IvanB 25/04 end
        public int MeasureValueMeasureId { get; set; }
        public int NOrder { get; set; }
        public string MeasureValueName { get; set; }
        public int MeasureValueId { get; set; }
        public int MeasureClass { get; set; }
        public string ValueTitle { get; set; }

        public static EnumMeasureModel GetEmptyItem(int measureId)
        {
            return new EnumMeasureModel
            {
                ValueTitle = "(Value not set)",
                MeasureValueId = 0,
                MeasureValueMeasureId = measureId, 
                NOrder = -10
            };
        }
        public static EnumMeasureModel GetEmptyModel(int measureId)
        {
            return new EnumMeasureModel
            {
                ValueTitle = "(Value not set)", 
                MeasureValueId = 0, 
                MeasureClass = MeasureModel.MeasureClassEnum,
                MeasureValueMeasureId = measureId
            };
        }
    }

}