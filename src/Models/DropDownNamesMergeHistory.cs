﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class DropDownNamesMergeHistory
    {
        public DropDownNamesMergeHistory(){}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        public DropDownNamesMergeHistory(DataRow row)
        {
            Id = (int)row[nameof(Id)];
            DocumentId = (int)row[nameof(DocumentId)];
            OldName = "" + row[nameof(OldName)];
            NewName = "" + row[nameof(NewName)];

        }

        public string OldName { get; set; }
        public string NewName { get; set; }
        public int Id { get; set; }
        public int DocumentId { get; set; }
    }
}