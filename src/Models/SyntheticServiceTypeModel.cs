﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
	[Serializable]
	public class SyntheticServiceTypeModel
	{
		public SyntheticServiceTypeModel() { }
		public SyntheticServiceTypeModel(DataRow row)
		{
			serviceTypeName = "" + row["serviceTypeName"];
			serviceTypeID = "" + row["serviceTypeID"];
		}
		public string serviceTypeID { get; set; }
		public string serviceTypeName { get; set; }
		public string categoryCodeSet { get; set; }
		public string expireDate { get; set; }

		




	}
}