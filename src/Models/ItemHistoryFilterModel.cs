﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class ItemHistoryFilterModel
    {
        public ItemHistoryFilterModel()
        {
        }

        public string AuthorId { get; set; }
        public string AuthorOfficeId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Order { get; set; }
        public string Batch { get; set; }
        public string Item { get; set; }
    }
}