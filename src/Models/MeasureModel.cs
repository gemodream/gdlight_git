﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class MeasureModel
    {
        public MeasureModel()
        {
        }
        /// <summary>
        /// spGetItemMeasuresByCP, spGetMeasuresByItemTypePartID
        /// </summary>
        /// <param name="row"></param>
        public MeasureModel(DataRow row)
        {
            MeasureId = Int32.Parse("" + row["MeasureID"]);
            MeasureName = "" + row["MeasureName"];
            MeasureTitle = "" + row["MeasureTitle"];
            PartId = Int32.Parse("" + row["PartID"]);
            MeasureClass = Int32.Parse("" + row["MeasureClass"]);
            MeasureCode = "" + row["MeasureCode"];
        }
        public string MeasureCode { get; set; }
        public int MeasureId { get; set; }
        public string MeasureName { get; set; }
        public string MeasureTitle { get; set; }
        public int PartId { get; set; }
        public int MeasureClass { get; set; }
        public string PartTypeId { get; set; }
        public bool IsEnum { get { return MeasureClass == MeasureClassEnum; } }
        public string ClassName 
        { 
            get
            {
                if (MeasureClass == 1) return "enum";
                if (MeasureClass == 2) return "text";
                if (MeasureClass == 3) return "numeric";
                if (MeasureClass == 4) return "label";
                return "unknown";
            }
        }
        public string DisplayName { get { return MeasureName + "(" + ClassName + ")"; } }
        public const int MeasureClassEnum = 1;
        public const int MeasureClassText = 2;
        public const int MeasureClassNumeric = 3;
        public bool Billable { get; set; }
        
        /// <summary>
        /// select * from tblMeasure where MeasureClass=2 order by MeasureName
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static MeasureModel Create1(DataRow row)
        {
            return new MeasureModel
            {
                MeasureId = Int32.Parse("" + row["MeasureID"]),
                MeasureName = "" + row["MeasureName"]
            };
        }
        /// <summary>
        /// spGetMeasuresByItemType
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static MeasureModel Create2(DataRow row)
        {
            return new MeasureModel
            {
                MeasureId = Int32.Parse("" + row["MeasureID"]),
                MeasureName = "" + row["MeasureName"],
                PartTypeId = "" + row["PartTypeID"],
                //PartId = Int32.Parse("" + row["PartID"]),
                MeasureClass = Int32.Parse("" + row["MeasureClass"]),
                Billable = !string.IsNullOrEmpty("" + row["Billable"]) && Convert.ToBoolean(row["Billable"])
            };
        }
    }
}