﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class FileInfoModel
    {
        public FileInfoModel(){}
        public string FileName { get; set; }
        public DateTime LastWriteTimeUtc { get; set; }
        public bool Exists { get; set; }
        public string VirtualPath { get; set; }
        public string DisplayDate
        {
            get { return !Exists ? "" : Convert.ToString(LastWriteTimeUtc); }
        }
        public string DisplayFile
        {
            get
            {
                //LeoReportToCpRulesRedirect
                return !Exists ? FileName : 
                    "<a href=\"PDFRedirect.aspx?Mode=View&FileName=" + FileName + "\" target=\"_blank\">" + FileName + "</a>";
            }
        }
        public string DownLoad
        {
            get
            {
                return !Exists ? "" :  "<a href=\"PDFRedirect.aspx?Mode=DownLoad&FileName=" + FileName + "\" target=\"_blank\">" + "DownLoad" + "</a>";
            }
        }
        public string RedirectUrl
        {
            get { return "PDFRedirect.aspx?Mode=View&FileName=" + FileName ; }
        }
    }
}