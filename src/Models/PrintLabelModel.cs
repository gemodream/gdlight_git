﻿using System;
using System.Collections.Generic;
namespace Corpt.Models
{
    [Serializable]
    public class PrintLabelModel
    {
        public PrintLabelModel()
        {
            PaintValues = new List<PrintLblPaintModel>();
        }

        public string Barcode { get; set; }
        public string BarcodeNum { get; set; }
        public string CaratWeight { get; set; } //carat weight
        public string Color { get; set; }
        public string Clarity { get; set; }
        public string ShortShapeName { get; set; }
        public string IsPrinted { get; set; }
        public string Gnumber { get; set; }
        public string NewItemNumber { get; set; }
        public string OldItemNumber { get; set; }
       
        public string DotNewItemNumber { get; set; }
        public string DotOldItemNumber { get; set; }

        public string DotNewBatchNumber { get; set; }
        public List<PrintLblPaintModel> PaintValues { get; set; }
        public List<string> GradeFailCodes { get; set; }
        public bool IsTlkw { get; set; }
        public bool IsSecond { get; set; }
        public string LblDocumentName { get; set; }
        public string UniqueKey
        {
            get { return NewItemNumber + ";" + (IsSecond ? 2 : 1); }
        }
    }
}