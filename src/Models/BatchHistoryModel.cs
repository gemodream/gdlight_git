﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BatchHistoryModel
    {
        public BatchHistoryModel()
        {
        }

        public BatchHistoryModel(DataRow row)
        {

            TimeStamp = Convert.ToDateTime(row["RecordTimeStamp"]);
            Action = "" + row["EventName"];
            Form = "" + row["VewAccessName"];
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
            ItemsAffected = string.IsNullOrEmpty("" + row["NumberOfItemsAffected"]) ? 0 : Int32.Parse("" + row["NumberOfItemsAffected"]);
        }
        public string Action { get; set; }
        public string Form { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ItemsAffected { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}