﻿using System;
using System.Data;
using System.Web;
using Corpt.Constants;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingExModel
    {
        public TrackingExModel(){}
        public TrackingExModel(DataRow row, TrackingFilterModel filterModel)
        {
            MemoNumber = "" + row["MemoNumber"];
            CpName = "" + row["CustomerProgramName"];
            OrderCode = "" + row["OrderCode"];
            CpOfficeId = Int32.Parse("" + row["CPOfficeID"]);
            CpId = Int32.Parse("" + row["CPID"]);
            GroupId = Int32.Parse("" + row["GroupId"]);
            GroupOfficeId = Int32.Parse("" + row["GroupOfficeId"]);
            StateId = Int32.Parse("" + row["StateId"]);

            //-- For jump on Tracking Details
            CustomerOffice = filterModel.CustomerId + "_" + filterModel.CustomerOfficeId;
            DateFrom = filterModel.DateFrom.ToString();
            DateTo = filterModel.DateTo.ToString();
        }

        public string CustomerOffice { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string MemoNumber { get; set; }
        public string CpName { get; set; }
        public string OrderCode { get; set; }
        public int CpOfficeId { get; set; }
        public int CpId { get; set; }
        public int GroupId { get; set; }
        public int GroupOfficeId { get; set; }
        public int StateId { get; set; }
        public string MemoDisplay
        {
            get
            {
                return string.IsNullOrEmpty(MemoNumber) ? "NoMemo" :
                    "<a href=\"TrackingDetails.aspx" + 
                    "?p1=" + SessionConstants.TrackDetailsModeMemo +
                    "&p2=" + HttpUtility.UrlEncode(MemoNumber) + 
                    "&p3=" + CustomerOffice + 
                    "&p4=" + DateFrom + 
                    "&p5=" + DateTo + "\" target=\"_blank\">" +
                    MemoNumber + "</a>,&nbsp; ";
            }
        }
        public string CpNameDisplay
        {
            get
            {
                return string.IsNullOrEmpty(CpName) ? "No Cp" : 
                    "<a href=\"TrackingDetails.aspx" + 
                    "?p1=" + SessionConstants.TrackDetailsModeCp +
                    "&p2=" + HttpUtility.UrlEncode(CpName) +
                    "&p3=" + CustomerOffice + 
                    "&p4=" + DateFrom + 
                    "&p5=" + DateTo + "\" target=\"_blank\">" +
                    CpName + "</a>,&nbsp; ";
            }
        }
        public string OrderHighlited
        {
            get
            {
                return StateId == 4 ? 
                    "<span class='text_highlitedgray'>" + OrderCode + "</span>" :
                    "<span class='text_highlitedyellow'>" + OrderCode + "</span>";
            }
        }
        public virtual string OrderDisplay
        {
            get
            {
                return 
                    "<a href=\"TrackingDetails.aspx" + 
                    "?p1=" + SessionConstants.TrackDetailsModeOrder +
                    "&p2=" + HttpUtility.UrlEncode(OrderCode) +
                    "&p3=" + CustomerOffice +
                    "&p4=" + DateFrom +
                    "&p5=" + DateTo + "\" target=\"_blank\">" +
                    OrderHighlited + "</a>,&nbsp; ";
            }
        }
    }
}