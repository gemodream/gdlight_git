﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class CutGradeModel
    {
        public CutGradeModel(DataRow row)
        {
            Comment     = Utils.NullValue(row["Comment"]);
            MeasureName = Utils.NullValue(row["MeasureName"]);
            Pass        = Utils.NullValue(row["Pass?"]);
            MinValue    = ParseValueAsFloat(Utils.NullValue(row["MinValue"]));
            MaxValue    = ParseValueAsFloat(Utils.NullValue(row["MaxValue"]));
            MeasureValue = ParseValueAsFloat(Utils.NullValue(row["MeasureValue"]));
            MinValueName = Utils.NullValue(row["MinValueName"]);
            MaxValueName = Utils.NullValue(row["MaxValueName"]);
            ValueTitle  = Utils.NullValue(row["ValueTitle"]);

        }
        public string Comment { get; set; }
        public string MeasureName { get; set; }
        public string Pass { get; set; }
        public bool IsPass
        {
            get
            {
                return !string.IsNullOrEmpty(Pass) && Pass.ToLower() == "yes";
            }
        }
        public string PassFail
        {
            get { return !IsPass ? "<SPAN class=text_highlitedyellow>" + Pass + @"</span>" : Pass; }
        }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public string MeasureValue { get; set; }
        public string MinValueName { get; set; }
        public string MaxValueName { get; set; }
        public string ValueTitle { get; set; }
        public string DisplayMinValue
        {
            get { return MinValueName != "" ? MinValueName : MinValue; }
        }
        public string DisplayMaxValue
        {
            get { return MaxValueName != "" ? MaxValueName : MaxValue; }
        }
        public string DisplayActualValue
        {
            get { return ValueTitle != "" ? ValueTitle : MeasureValue; }
        }
        private string ParseValueAsFloat(string value)
        {
            if (value == "") return value;
            try
            {
                object asFloat = float.Parse(value).ToString("#.00");
                return asFloat.ToString();
            } catch (Exception x)
            {
                return "";
            }
        }
    }
}