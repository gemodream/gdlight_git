﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PersonModel 
    {
        public PersonModel()
        {
        }

        public PersonModel(DataRow row)
        {
            FirstName   = "" + row["FirstName"];
            LastName    = "" + row["LastName"];
            Email       = "" + row["email"];
            PersonId = Convert.ToInt32("" + row["PersonID"]);

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonId { get; set; }
        public string Email { get; set; }
        public string DisplayName
        {
            get { return FirstName + " " + LastName + (string.IsNullOrEmpty(Email) ? "" : "<" + Email + ">"); }
        }
    }
}