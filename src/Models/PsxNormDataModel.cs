﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PsxNormDataModel
    {
        public PsxNormDataModel(){}
        public PsxNormDataModel(DataRow row)
        {
            ScanId = Convert.ToString(row["ScanID"]);
            Brilliance = Convert.ToInt32(row["Brilliance"]);
            Fire = Convert.ToInt32(row["Fire"]);
            Scintillation = Convert.ToInt32(row["Scintillation"]);
            Contrast = Convert.ToInt32(row["Contrast"]);
            Efficiency = Convert.ToInt32(row["Efficiency"]);
            OldItemNumber = Convert.ToString(row["Old Item Number"]);
            NewItemNumber = Convert.ToString(row["New Item Number"]);
            ScanDate = Convert.ToDateTime(row["ScanTimeStamp"]);
            //-- X-Factor
            var intX = 0;
            if (DisplayBrilliance.IndexOf("Excellent", System.StringComparison.Ordinal) != -1)
            {
                intX++;
            }
            if (DisplayFire.IndexOf("Excellent", System.StringComparison.Ordinal) != -1)
            {
                intX++;
            }
            if (DisplayScintillation.IndexOf("Excellent", System.StringComparison.Ordinal) != -1)
            {
                intX++;
            }
            XFactor = intX;
        }

        public string ScanId { get; set; }
        public DateTime ScanDate { get; set; }
        public string OldItemNumber { get; set; }
        public string NewItemNumber { get; set; }
        public int Brilliance { get; set; }
        public int Fire { get; set; }
        public int Scintillation { get; set; }
        public int Contrast { get; set; }
        public int Efficiency { get; set; }
        public int XFactor { get; set; }
        #region Display Values
        public string ReLinkOldItemNumber
        {
            get
            {
                return string.IsNullOrEmpty(OldItemNumber) ? "" :
                    "<a href=\"ItemRedirector.aspx?ItemNumber=" + OldItemNumber + "&mode=psx\">" + 
                    OldItemNumber + "</a>";
            }
        }
        public string ReLinkNewItemNumber
        {
            get
            {
                return string.IsNullOrEmpty(NewItemNumber) ? "" : 
                    "<a href=\"ItemRedirector.aspx?ItemNumber=" + NewItemNumber + "&mode=psx\">" + 
                    NewItemNumber + "</a>";
            }
        }
        public string DisplayContrast { get { return GetVisual(Contrast); } }
        public string DisplayScintillation { get { return GetVisual(Scintillation); } }
        public string DisplayFire { get { return GetVisual(Fire); } }
        public string DisplayEfficiency { get { return GetVisual(Efficiency); } }
        public string DisplayBrilliance { get { return GetVisual(Brilliance); } }
        public string DisplayConrtast { get { return GetVisual(Contrast); } }
        public string DisplayXFactor { get { return CalcXFactor(XFactor); } }
        private static string GetVisual(int value)
        {
            if (value < 25) return "Fair";
            if (value < 50) return "Good";
            if (value < 75) return "Very&nbsp;Good";
            return value >= 75 ? "Excellent" : "";

        }
        private static String CalcXFactor(int xFactor)
        {
            switch (xFactor)
            {
                case 0:
                    return 
                        @"<img src='images/1xOFF.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/2xOFF.jpg' width='15' height='14' alt='' />"+
                        "<img src='images/3xOFF.jpg' width='15' height='14' alt='' />";
                case 1:
                    return 
                        @"<img src='images/1x.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/2xOFF.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/3xOFF.jpg' width='15' height='14' alt='' />";
                case 2:
                    return 
                        @"<img src='images/1xOFF.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/2x.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/3xOFF.jpg' width='15' height='14' alt='' />";
                case 3:
                    return 
                        @"<img src='images/1xOFF.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/2xOFF.jpg' width='15' height='14' alt='' />" + 
                        "<img src='images/3x.jpg' width='15' height='14' alt='' />";
            }
            return "";
        }
        public bool CompareFactor(int filterFactor)
        {
            if (filterFactor == 0) return true;
            return XFactor == filterFactor;
        }
        #endregion
    }
}