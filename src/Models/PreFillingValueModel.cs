﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PreFillingValueModel
    {
        public PreFillingValueModel(DataRow row, List<EnumMeasureModel> enums)
        {
            BatchId = Convert.ToInt32(row["BatchID"]);
            PartId = Convert.ToInt32(row["PartID"]);
            PartName = "" + row["PartName"];
            MeasureId = Convert.ToInt32(row["MeasureID"]);
            MeasureTitle = "" + row["MeasureTitle"];
            MeasureValue = "" + row["MeasureValue"];
            MeasureClass = Convert.ToInt32(row["MeasureClass"]);
            MeasureCode = Convert.ToInt32(row["MeasureCode"]);
            if (MeasureClass != MeasureModel.MeasureClassEnum) return;
            var pos = MeasureValue.IndexOf(".", StringComparison.Ordinal);
            if (pos != -1)
            {
                MeasureValue = MeasureValue.Substring(0, pos);
            }
            var enumItem =
                enums.Find(
                    m => m.MeasureValueMeasureId == MeasureId && m.MeasureValueId == Convert.ToInt32(MeasureValue));
            if (enumItem != null)
            {
                EnumTitle = enumItem.ValueTitle;
            }
        }

        public BatchModel BatchModel { get; set; }
        public int BatchId { get; set; }
        public int PartId { get; set; }
        public string PartName { get; set; }
        public int MeasureId { get; set; }
        public int MeasureCode { get; set; }
        public string MeasureTitle { get; set; }
        public string EnumTitle { get; set; }
        public string MeasureValue { get; set; }
        public int MeasureClass { get; set; }
        public string DisplayValue
        {
            get { return MeasureClass == MeasureModel.MeasureClassEnum ? EnumTitle : MeasureValue; }
        }
        public string DisplayBatch
        {
            get { return BatchModel.FullBatchNumber; }
        }
    }
}