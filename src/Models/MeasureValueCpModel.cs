﻿
using System;
using System.Data;

namespace Corpt.Models
{
	[Serializable]
	public class MeasureValueCpModel
	{
		public MeasureValueCpModel()
		{

		}

		/// <summary>
		/// spGetItemMeasuresByCP_New, spGetItemMeasuresByViewAccessAndCP
		/// </summary>
		/// <param name="row"></param>
		/// <param name="forItem"> </param>
		public MeasureValueCpModel(DataRow row, SingleItemModel forItem)
		{
			PartId = Convert.ToInt32(row["PartID"]);
			PartName = "" + row["PartName"];
			MeasureId = "" + row["MeasureID"];
			MeasureCode = "" + row["MeasureCode"];
			MeasureName = "" + row["MeasureName"];
			MeasureTitle = "" + row["MeasureTitle"];
			MeasureClass = Int32.Parse("" + row["MeasureClass"]);
			ForItem = forItem;
			ByCp = true;
		}

		public int PartId { get; set; }
		public int PartTypeId { get; set; }
		public string MeasureId { get; set; }
		public string MeasureCode { get; set; }
		public string MeasureName { get; set; }
		public string MeasureTitle { get; set; }
		public int MeasureClass { get; set; }
		public string PartName { get; set; }
		public bool ByCp { get; set; }
		public SingleItemModel ForItem { get; set; }

		public MeasureValueCpModel(MeasureModel measureModel)
		{
			MeasureId = "" + measureModel.MeasureId;
			MeasureClass = measureModel.MeasureClass;
			MeasureName = measureModel.MeasureName;
			MeasureTitle = measureModel.MeasureTitle;
		}
		public static MeasureValueCpModel Create(DataRow row)
		{
			return new MeasureValueCpModel
			{
				PartId = Convert.ToInt32(row["PartID"]),
				PartTypeId = Convert.ToInt32(row["PartTypeId"]),
				PartName = "" + row["PartName"],
				MeasureId = "" + row["MeasureID"],
				MeasureCode = "" + row["MeasureCode"],
				MeasureName = "" + row["MeasureName"],
				MeasureTitle = "" + row["MeasureTitle"],
				MeasureClass = Int32.Parse("" + row["MeasureClass"]),
				ByCp = true
			};
		}
	}
}