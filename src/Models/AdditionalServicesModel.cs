﻿using System;
using System.Data;

namespace Corpt.Models
{
	[Serializable]
	public class AdditionalServicesModel
	{
		public AdditionalServicesModel()
		{
		}
		/// <summary>
		/// spGetBatchRecheckChangeSKU
		/// </summary>
		/// <param name="row"></param>
		public AdditionalServicesModel(DataRow row)
		{

			ReportNumber = "" + row["ReportNumber"];
			BatchID = "" + row["BatchID"];
			GroupID = "" + row["GroupID"];
			CustomerID = "" + row["CustomerID"];
			CPID = "" + row["CPID"];

			GroupCode = "" + row["GroupCode"];
			BatchCode = "" + row["BatchCode"];
			CustomerCode = "" + row["CustomerCode"];
			CustomerName = "" + row["CustomerName"];
			CustomerProgramName = "" + row["CustomerProgramName"];

			IsSKUChange = Convert.ToBoolean(row["IsSKUChange"]);
			IsRecheck = Convert.ToBoolean(row["IsRecheck"]);
			IsUrgent = Convert.ToBoolean(row["IsUrgent"]);
			IsColorStone = Convert.ToBoolean(row["IsColorStone"]);

			IsRePrint = Convert.ToBoolean(row["IsRePrint"]);
			IsTND = Convert.ToBoolean(row["IsTND"]);
			IsLaserInscription = Convert.ToBoolean(row["IsLaserInscription"]);
			IsLaserRemoval = Convert.ToBoolean(row["IsLaserRemoval"]);
            IsUrgent48 = Convert.ToBoolean(row["IsUrgent48"]);

            ChangeSKUPrice = "" + row["ChangeSKUPrice"];
			RecheckPrice = "" + row["RecheckPrice"];
			UrgentServicePrice = "" + row["UrgentServicePrice"];
			ColorStonePrice = "" + row["ColorStonePrice"];

			RePrintPrice = "" + row["RePrintPrice"];
			TNDPrice = "" + row["TNDPrice"];
			LaserInscriptionPrice = "" + row["LaserInscriptionPrice"];
			LaserRemovalPrice = "" + row["LaserRemovalPrice"];
            UrgentService48Price = "" + row["UrgentService48Price"];

        }

		public string ReportNumber { get; set; }
		public string BatchID { get; set; }
		public string GroupID { get; set; }
		public string CustomerID { get; set; }
		public string CPID { get; set; }

		public string GroupCode { get; set; }
		public string BatchCode { get; set; }
		public string CustomerCode { get; set; }
		public string CustomerName { get; set; }
		public string CustomerProgramName { get; set; }

		public bool IsSKUChange { get; set; }
		public bool IsRecheck { get; set; }
		public bool IsUrgent { get; set; }
		public bool IsColorStone { get; set; }

		public bool IsRePrint { get; set; }
		public bool IsTND { get; set; }
		public bool IsLaserInscription { get; set; }
		public bool IsLaserRemoval { get; set; }
        public bool IsUrgent48 { get; set; }

        public string ChangeSKUPrice { get; set; }
		public string RecheckPrice { get; set; }
		public string UrgentServicePrice { get; set; }
		public string ColorStonePrice { get; set; }

		public string RePrintPrice { get; set; }
		public string TNDPrice { get; set; }
		public string LaserInscriptionPrice { get; set; }
		public string LaserRemovalPrice { get; set; }
        public string UrgentService48Price { get; set; }
    }
}