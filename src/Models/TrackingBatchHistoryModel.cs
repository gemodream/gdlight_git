﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class TrackingBatchHistoryModel
    {
        public TrackingBatchHistoryModel(DataRow row)
        {
            Stage = "" + row["Stage"];
            Date = DateTime.Parse(row["pDate"].ToString()).ToShortDateString();
            Time = DateTime.Parse(row["pDate"].ToString()).ToShortTimeString();

        }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Stage { get; set; }
        public bool IsCheckedOut
        {
            get { return Stage == "Checked Out"; }
        }

    }
}