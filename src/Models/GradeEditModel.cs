﻿using System;

namespace Corpt.Models
{
    [Serializable]
    public class GradeEditModel
    {
        public GradeEditModel()
        {
            
        }
        public GradeEditModel(MeasureValueCpModel measureModel, MeasureValueModel valueModel)
        {
            //-- Part
            PartId = Convert.ToInt32(measureModel.PartId);
            PartName = measureModel.PartName;
            ByCp = measureModel.ByCp;

            //-- Measure
            MeasureId = measureModel.MeasureId;
            MeasureTitle = measureModel.MeasureTitle;
            MeasureClass = measureModel.MeasureClass;
            MeasureCode = measureModel.MeasureCode;
            HasSavingMeasure = valueModel != null;
            if (valueModel == null && MeasureClass != MeasureModel.MeasureClassEnum) return;
            CopyValueModel(valueModel ?? MeasureValueModel.GetEmptyForEnum());
        }
        private void CopyValueModel(MeasureValueModel valueModel)
        {
            //-- Measure Value prev
            ValueEnumIdPrev = valueModel.MeasureValueId;
            ValueEnumTitlePrev = valueModel.EnumValueTitle;
            ValueTextPrev = valueModel.StringValue;
            ValueNumbPrev = valueModel.MeasureValue;

            //-- Measure Value current
            ValueEnumIdCurr = valueModel.MeasureValueId;
            ValueTextCurr = valueModel.StringValue;
            ValueNumbCurr = valueModel.MeasureValue;
            ValueEnumTitleCurr = valueModel.EnumValueTitle;
        }
        public bool ByCp { get; set; }
        public int PartId { get; set; }
        public string PartName { get; set; }

        public string MeasureId { get; set; }
        public string MeasureTitle { get; set; }
        public int MeasureClass { get; set; }
        public string MeasureCode { get; set; }

        public string ValueEnumIdPrev { get; set; }
        public string ValueEnumTitlePrev { get; set; }
        public string ValueTextPrev { get; set; }
        public string ValueNumbPrev { get; set; }

        public string ValueEnumIdCurr { get; set; }
        public string ValueEnumTitleCurr { get; set; }
        public string ValueTextCurr { get; set; }
        public string ValueNumbCurr { get; set; }
        public bool HasSavingMeasure { get; set; }
        public string DisplayUndo
        {
            get { return HasChanges ? "Undo" : ""; }
        }
        public string UniqueKey
        {
            get { return PartId + ";" + MeasureId; }
        }
        public bool NotEntered
        {
            get { return ByCp && !HasValue; }
        }
        public string DisplayByCp
        {
            get { return ByCp ? "Y" : ""; }
        }
        protected bool HasValue
        {
            get
            {
               if (MeasureClass == MeasureModel.MeasureClassEnum)
               {
                   return ValueEnumIdCurr != "0";
               } 
               if (MeasureClass == MeasureModel.MeasureClassText)
               {
                   return !string.IsNullOrEmpty(ValueTextCurr);
               }
               if (MeasureClass == MeasureModel.MeasureClassNumeric)
               {
                   return !string.IsNullOrEmpty(ValueNumbCurr);
               }
               return false;
            }
        }

        public bool HasChanges
        {
            get
            {
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    return ValueEnumIdPrev != ValueEnumIdCurr;
                }
                if (MeasureClass == MeasureModel.MeasureClassText)
                {
                    return ValueTextPrev != ValueTextCurr;
                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return ValueNumbPrev != ValueNumbCurr;
                }
                return false;
            }
        }
        public string ValuePrev
        {
            get
            {
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    return ValueEnumTitlePrev;
                }
                if (MeasureClass == MeasureModel.MeasureClassText)
                {
                    return ValueTextPrev;
                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return ValueNumbPrev;
                }
                return "";
            }
        }
        public string ValueCurr
        {
            get
            {
                if (MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    return ValueEnumTitleCurr;
                }
                if (MeasureClass == MeasureModel.MeasureClassText)
                {
                    return ValueTextCurr;
                }
                if (MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    return ValueNumbCurr;
                }
                return "";
            }
        }
        public void UndoChanges()
        {
            //-- Measure Value current
            ValueEnumIdCurr = ValueEnumIdPrev;
            ValueTextCurr = ValueTextPrev;
            ValueNumbCurr = ValueNumbPrev;
            ValueEnumTitleCurr = ValueEnumTitlePrev;

        }
    }
}