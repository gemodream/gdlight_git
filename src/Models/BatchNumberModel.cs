﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BatchNumberModel
    {
        /// <summary>
        /// wspvvGetItemsByBatchRealNumbers
        /// </summary>
        /// <param name="row"></param>
        public BatchNumberModel(DataRow row)
        {
            OrderCode = Convert.ToString(row["OrderCode"]);
            PrevOrderCode = Convert.ToString(row["prevOrderCode"]);
            
            BatchCode = Convert.ToString(row["BatchCode"]);
            PrevBatchCode = Convert.ToString(row["prevBatchCode"]);

            ItemCode = Convert.ToString(row["ItemCode"]);
            PrevItemCode = Convert.ToString(row["prevItemCode"]);
        }
        public string NewItemNumber
        {
            get { return Utils.FullItemNumber(OrderCode, BatchCode, ItemCode); }
        }
        public string OldItemNumber
        {
            get
            {
                return Utils.FullItemNumber(PrevOrderCode, PrevBatchCode, PrevItemCode);
            }
        }
        public string OrderCode { get; set; }
        public string PrevOrderCode { get; set; }
        public string ItemCode { get; set; }
        public string PrevItemCode { get; set; }
        public string BatchCode { get; set; }
        public string PrevBatchCode { get; set; }

    }
}