﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BatchModel
    {

        public BatchModel()
        {
            
        }
        /// <summary>
        /// DataRow from: 
        /// spGetBatch (prm: batchId, example 251377), 
        /// spGetBatchByCode (par: orderNUmber, exp: 12875)
        /// </summary>
        /// <param name="dr"></param>
        public BatchModel(DataRow dr)
        {
            BatchId     = "" + dr["BatchID"];
            GroupCode   = "" + dr["GroupCode"];
            BatchCode   = "" + dr["BatchCode"];
            GroupId     = "" + dr["GroupID"];
            CpId        = "" + dr["CPID"];
            CustomerId = "" + dr["CustomerID"];
            CustomerOfficeId = "" + dr["CustomerOfficeID"];

        }
        /// <summary>
        /// sp_GetBatchesByCustomerByDateRangeByMemoNumber
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="addMemo"></param>
        public BatchModel(DataRow dr, bool addMemo)
        {
            BatchId = "" + dr["BatchID"];
            GroupCode = "" + dr["GroupCode"];
            BatchCode = "" + dr["BatchCode"];
            GroupId = "" + dr["GroupID"];
            CpId = "" + dr["CPID"];
            MemoNumber = "" + dr["MemoNumber"];
        }

        public string MemoNumber { get; set; }
        public string FullBatchNumber => GroupCodeFiveChars + BatchCodeTreeChars;
        public string FullBatchNumberWithDot => GroupCodeFiveChars + "." + BatchCodeTreeChars;

        public string PathToPicture { get; set; }
        public int ItemTypeId { get; set; }
        public string BatchId { get; set; }
        public string BatchCode { get; set; }
        public string GroupId { get; set; }
        public string GroupCode { get; set; }
        public string CustomerId { get; set; }
        public string CustomerOfficeId { get; set; }
        public string BatchCodeTreeChars => Utils.FillToThreeChars(BatchCode, GroupCode);
        public string GroupCodeFiveChars => Utils.FillToFiveChars(GroupCode);
        public string CpId { get; set; }
        public string ErrMsg { get; set; }
        public string TestDisplay => FullBatchNumber + " " + ErrMsg;

        /// <summary>
        /// sp_getSimialrBatches
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static BatchModel CreateSimilar(DataRow row)
        {
            return new BatchModel
                       {
                           BatchCode = "" + row["BatchCode"],
                           BatchId = "" + row["BatchID"],
                           ItemTypeId = Int32.Parse("" + row["ItemTypeID"]),
                           GroupCode   = "" + row["GroupCode"]
                       };    
        }
    }
}