﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class ReportFindModel
    {
        public ReportFindModel(){}
        public ReportFindModel(DataRow row)
        {
            VirtualVaultNumber = "" + row["VirtualVaultNumber"];
            ItemNumber = "" + row["OldItemNumber"];
        }

        public string HexGnumber { get; set; }
        public int IntGNumber { get; set; }
        public string VirtualVaultNumber { get; set; }
        public string ItemNumber { get; set; }
        public string DocumentChar { get; set; }
        
    }
}