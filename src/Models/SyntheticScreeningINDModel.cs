﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SyntheticScreeningINDModel
    {
        public SyntheticScreeningINDModel() { }
        public SyntheticScreeningINDModel(DataRow row)
        {
            ScreeningID = Int64.Parse("" + row["ScreeningID"]);
            //RequestID = Int32.Parse("" + row["RequestID"]);
            RequestID = row["RequestID"] == DBNull.Value ? (int?)null : int.Parse("" + row["RequestID"]);
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?) null : int.Parse("" + row["GSIOrder"]);
            CustomerCode = "" + row["CustomerCode"];
            MemoNum = "" + row["MemoNum"];
            PONum = "" + row["PONum"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            QtyPass = row["QtyPass"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyPass"]);
            QtyFail = row["QtyFail"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyFail"]);
            FTQuantity = row["FTQuantity"] == DBNull.Value ? (int?)null : int.Parse("" + row["FTQuantity"]);
            SKUName = "" + row["SKUName"];
            StyleName = "" + row["StyleName"];
            Test = "" + row["Test"];
            CertifiedBy = "" + row["CertifiedBy"];
            Destination = "" + row["Destination"];
            Notes = "" + row["Notes"];
            CreatedBy = "" + row["CreatedBy"];
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?) null : DateTime.Parse("" + row["CreatedDate"]);
            ModifiedBy = "" + row["ModifiedBy"];
            ModifiedDate = row["ModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ModifiedDate"]);
            CustomerName = "" + row["CustomerName"];

        }       

        public Int64 ScreeningID { get; set; }
        public int? RequestID { get; set; }
        public int? GSIOrder { get; set; }
        public string CustomerCode { get; set; }
        public string MemoNum { get; set; }
        public string PONum { get; set; }      
        public string SKUName { get; set; }
        public string StyleName { get; set; }
        public string Test { get; set; }
        public string CertifiedBy { get; set; }
        public string Destination { get; set; }
        public string Notes { get; set; }
        public int? TotalQty { get; set; }
        public int? FTQuantity { get; set; }
        public int? QtyPass { get; set; }
        public int? QtyFail { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CustomerName { get; set; }

    }
}