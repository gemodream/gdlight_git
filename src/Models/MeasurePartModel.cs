﻿using System;
using System.Data;
using System.Linq;

namespace Corpt.Models
{
    [Serializable]
    public class MeasurePartModel
    {
        public MeasurePartModel()
        {
        }

        /// <summary>
        /// spGetPartsByItemType
        /// </summary>
        /// <param name="row"></param>
        public MeasurePartModel(DataRow row)
        {
            var array = row.ItemArray;
            var uid = array.Aggregate("", (current, o) => current + (o == null ? "" : o.ToString()));
            Uid = uid;
            PartName = "" + row["PartName"];
            PartId = Convert.ToInt32(row["PartID"]);

            PartTypeId = "" + row["PartTypeID"];
            PartTypeName = "" + row["PartTypeName"];
            ParentPartId = !string.IsNullOrEmpty("" + row["ParentPartID"]) ? 
                Convert.ToInt32("" + row["ParentPartID"]) : 0;
            ItemTypeId = Convert.ToInt32(row["ItemTypeID"]);
            HomogeneousClassId = "" + row["HomogeneousClassID"];
        }

        public int ItemTypeId { get; set; }
        public int PartId { get; set; }
        public string PartName { get; set; }
        public string PartTypeId { get; set; }
        public string PartTypeName { get; set; }
        public string Uid { get; set; }
        public int ParentPartId { get; set; }
        public string HomogeneousClassId { get; set; }
        public bool HasParent { get { return ParentPartId > 0; } }
        public string PartTypeCode { get; set; }
        
        /// <summary>
        /// spGetPartsByItemTypeEx
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static MeasurePartModel Create(DataRow row)
        {
            return new MeasurePartModel
            {

             PartId = Convert.ToInt32(row["PartID"]) ,
             PartTypeCode = "" + row["PartTypeCode"],
             ItemTypeId = Convert.ToInt32(row["ItemTypeID"])
            };
        }
    }
    [Serializable]
    public class BagsItemsModel
    {
        public BagsItemsModel()
        {
        }
        public string BagNumber { get; set; }
        public string ItemsInBag { get; set; }
    }
}