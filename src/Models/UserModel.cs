﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class UserModel
    {
        public UserModel()
        {
        }
        public UserModel(DataRow row)
        {
            UserId = Convert.ToInt32(row["UserID"]);
            FirstName = "" + row["FirstName"];
            LastName = "" + row["LastName"];
        }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName
        { get { return FirstName + " " + LastName; } }
    }
}