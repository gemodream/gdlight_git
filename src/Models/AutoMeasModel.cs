﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Corpt.Models
{
    [Serializable]
    public class AutoMeasModel
    {
        public AutoMeasModel()
        {
            EnumSource = new List<EnumMeasureModel>();
        }
        public AutoMeasModel(MeasureModel measure, PartModel part, List<EnumMeasureModel> enums)
        {
            PartId = ""+part.PartId;
            PartName = part.PartName;
            MeasureId = "" + measure.MeasureId;
            MeasureCode = measure.MeasureCode;
            MeasureName = measure.MeasureName;
            MeasureClass = measure.MeasureClass;
            EnumSource = enums;

        }
        public string PartId { get; set; }
        public string PartName { get; set; }
        public string MeasureCode { get; set; }
        public string MeasureId { get; set; }
        public string MeasureName { get; set; }
        public int MeasureClass { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }
        public string RandValue { get; set; }
        public List<string> RandValues { get; set; }
        public bool IsEnum { get { return MeasureClass == MeasureModel.MeasureClassEnum; } }
        public bool IsNumeric { get { return MeasureClass == MeasureModel.MeasureClassNumeric; } }
        public bool HasValues
        {
            get { return (!string.IsNullOrEmpty(FromValue) && FromValue != "0") && (!string.IsNullOrEmpty(ToValue) && ToValue != "0"); }
        }
        public string UniqueKey
        {
            get { return PartId + ";" + MeasureId; }
        }
        public List<EnumMeasureModel> EnumSource { get; set; }

    }
}