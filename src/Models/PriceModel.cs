﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class PriceModel
    {
        public PriceModel(MeasurePartModel part, string sum, PriceModel detail)
        {
            ItemNumber = detail.ItemNumber;
            Price = sum;
            PriceNew = sum;
            RapaportName = ""; // detail.RapaportName;
            ShortReportName = ""; //detail.ShortReportName;
            ItemTypeId = detail.ItemTypeId;
            PartId = part.PartId;
            PartName = part.PartName;
            BatchId = detail.BatchId;
            IsContainer = detail.IsContainer;
        }
        public PriceModel(DataRow row, int batchId, List<MeasurePartModel> parts)
        {
            //CultureInfo ci = new CultureInfo("en-us");
            ItemNumber = "" + row["ItemNUmber"];
            PartName = "" + row["PartName"];
            Weight = "" + row["Weight/part"];
            UpperPrice = "" + row["UpperPrice/ct"].ToString().Trim();
            LowerPrice = "" + row["LowerPrice/ct"].ToString().Trim();
            AveragePrice = "" + row["Price/part"].ToString().Trim();
            MarkUp = "" + row["MarkUp"];
            Price = "" + row["Price/part"].ToString().Trim();
            PartId = int.Parse(row["PartID"].ToString());
            PartTypeId = int.Parse(row["PartTypeID"].ToString());
            PriceNew = Price;
            _Appraisal = "" + row["Appraisal"].ToString().Trim();
            var numbers = int.Parse("" + (row["numberOfStones"].ToString().Trim() == "" ? "0" : row["numberOfStones"]));
            Old_MSRP = "" + row["Saved MSRP"].ToString().Trim();

            NumberOfStones = Convert.ToInt16(numbers);

            UpperColor = "" + row["UpperColor"];
            LowerColor = "" + row["LowerColor"];
            Color = "" + row["Color"];

            UpperClarity = "" + row["UpperClarity"];
            LowerClarity = "" + row["LowerClarity"];
            Clarity = "" + row["Clarity"];

            RapaportName = "" + row["RapaportName"];
            //ItemName        = "" + row["ItemName"];
            Metal = "" + row["Metal"];
            Weight_Per_Stone = "" + row["Weight/stone"];
            CalcWeight_Per_Stone = "" + row["CalcWeight/stone"];
            //Comments        = "" + row["Comments"];
            IsContainer = (PartTypeId == 15 ? true : false);
            //ShapeCode       = "" + row["ShapeCode"];
            //ShortReportName = "" + row["Shape"];

            //ItemTypeId      = int.Parse("" + row["ItemTypeId"]);
            //PartId          = int.Parse("" + row["PartId"]);
            //var part = parts.Find(m => m.PartId == PartId);
            //if (part != null)
            //{
            //    PartName = part.PartName;
            //}
            BatchId = batchId;
        }

        public PriceModel(DataRow row, int batchId)
        {
            //CultureInfo ci = new CultureInfo("en-us");
            ItemNumber = "" + row["ItemNUmber"];
            PartName = "" + row["PartName"];
            Weight = "" + row["Weight/part"];
            UpperPrice = "" + row["UpperPrice/ct"].ToString().Trim();
            LowerPrice = "" + row["LowerPrice/ct"].ToString().Trim();
            AveragePrice = "" + row["Price/part"].ToString().Trim();
            MarkUp = "" + row["MarkUp"];
            Price = "" + row["Price/part"].ToString().Trim();
            PartId = int.Parse(row["PartID"].ToString());
            PartTypeId = int.Parse(row["PartTypeID"].ToString());
            PriceNew = Price;
            _Appraisal = "" + row["Appraisal"].ToString().Trim();
            var numbers = int.Parse("" + (row["numberOfStones"].ToString().Trim() == "" ? "0" : row["numberOfStones"]));
            Old_MSRP = "" + row["Saved MSRP"].ToString().Trim();

            NumberOfStones = Convert.ToInt16(numbers);

            UpperColor = "" + row["UpperColor"];
            LowerColor = "" + row["LowerColor"];
            Color = "" + row["Color"];

            UpperClarity = "" + row["UpperClarity"];
            LowerClarity = "" + row["LowerClarity"];
            Clarity = "" + row["Clarity"];

            RapaportName = "" + row["RapaportName"];
            //ItemName        = "" + row["ItemName"];
            Metal = "" + row["Metal"];
            Weight_Per_Stone = "" + row["Weight/stone"];
            CalcWeight_Per_Stone = "" + row["CalcWeight/stone"];
            //Comments        = "" + row["Comments"];
            IsContainer = (PartTypeId == 15 ? true : false);
            BatchId = batchId;
            //IvanB 27/09 start
            ShapeName = "" + row["Shape"];
            Polish = "" + row["Polish"];
            Symmetry = "" + row["Symmetry"];
            LWFlourescence = "" + row["LWFlourescence"];
            Treatment = "" + row["Treatment"];
            //IvanB 27/09 start
        }


        public string ItemCode
        {
            get { return ItemNumber.Substring(ItemNumber.Length - 2); }
        }
        public bool IsContainer { get; set; }
        public string ItemNumber { get; set; }
        public string Weight { get; set; }

        public string UpperPrice { get; set; }
        public string LowerPrice { get; set; }
        public string AveragePrice { get; set; }
        public string MarkUp { get; set; }
        public string Price { get; set; }
        public string PriceNew { get; set; }
        public string PriceSaved { get; set; }
        public string _Appraisal { get; set; }
        public string Old_MSRP { get; set; }

        public int NumberOfStones { get; set; }

        public string UpperColor { get; set; }
        public string LowerColor { get; set; }
        public string Color { get; set; }

        public string UpperClarity { get; set; }
        public string LowerClarity { get; set; }
        public string Clarity { get; set; }

        public string RapaportName { get; set; }
        public string ShapeCode { get; set; }
        public string ShortReportName { get; set; }
        public int ItemTypeId { get; set; }
        public int PartId { get; set; }
        public int PartTypeId { get; set; }

        public string PartName { get; set; }

        public string Weight_Per_Stone { get; set; }
        public string CalcWeight_Per_Stone { get; set; }

        public string Metal { get; set; }
        public string ItemName { get; set; }
        //public string Comments { get; set; }

        public int BatchId { get; set; }

        //IvanB 27/09 start
        public string ShapeName { get; set; }
        public string Polish { get; set; }
        public string Symmetry { get; set; }
        public string LWFlourescence { get; set; }
        public string Treatment { get; set; }

        //IvanB 27/09 end

        public string StonesDisplay
        {
            get { return NumberOfStones == 0 ? "" : "" + NumberOfStones; }
        }
        public string UniqueKey
        {
            get
            {
                return PartTypeId.ToString() + "_" + PartId.ToString() + "_" + ItemNumber;
            }
        }
        public bool NeedSave
        {
            get { return NeedSaving(PriceNew); }
        }
        public bool NeedSaving(string newValue)
        {
            return !IsEquals(PriceSaved, newValue);
        }
        public bool ManualAppraisal
        {
            get { return ManualApprais(PriceNew); }
        }
        public bool ManualApprais(string newValue)
        {
            return !IsEquals(Price, newValue);
        }
        public static bool IsEquals(string n1, string n2)
        {
            var num_1 = (string.IsNullOrEmpty(n1) || n1 == "&nbsp;" ? 0 : Convert.ToDecimal(n1));
            var num_2 = (string.IsNullOrEmpty(n2) || n2 == "&nbsp;" ? 0 : Convert.ToDecimal(n2));
            return num_1 == num_2;
        }
        public static string CalcContainerSum(List<PriceModel> list)
        {
            decimal sum = 0;
            foreach (var item in list)
            {
                if (item.IsContainer) continue;
                if (string.IsNullOrEmpty(item.PriceNew)) continue;
                sum += Convert.ToDecimal(item.PriceNew);
            }
            return sum.ToString();
        }
    }
}