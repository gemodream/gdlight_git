﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class OrderModel
    {
		/// <summary>
		/// spGetGroupByCode2
		/// </summary>
		/// <param name="row"></param>
		public OrderModel()
		{ }
		public OrderModel(DataRow row, string orderCode)
        {
            var customer = new CustomerModel
                               {
                                   CustomerId = "" + row["CustomerID"],
                                   CustomerCode = "" + row["CustomerCode"],
                                   OfficeId = "" + row["CustomerOfficeID"],
                                   CustomerName = "" + row["CustomerName"]
                               };
            Customer = customer;
            GroupCode = Int32.Parse(orderCode);
            GroupId = Int32.Parse("" + row["GroupID"]);
            GroupOfficeId = Int32.Parse("" + row["GroupOfficeID"]);
            notInspected = row["NotInspectedQuantity"] == DBNull.Value ? 0 : int.Parse("" + row["NotInspectedQuantity"]);
            inspected = row["InspectedQuantity"] == DBNull.Value ? 0 : int.Parse("" + row["InspectedQuantity"]);
            itemized = row["ItemsQuantity"] == DBNull.Value ? 0 : int.Parse("" + row["ItemsQuantity"]);
        }
        public CustomerModel Customer { get; set; }
        public int GroupCode { get; set; }
        public int GroupId { get; set; }
        public int GroupOfficeId { get; set; }
        public int notInspected { get; set; }
        public int inspected { get; set; }
        public int itemized { get; set; }
    }
    [Serializable]
    public class ItemizedModel
    {
        public string Memo { get; set; }
        public string Items { get; set; }
        public string Sku { get; set; }
        public string ItemsSet { get; set; }
    }
    [Serializable]
    public class ItemizedLotModel
    {
        public string Memo { get; set; }
        public string Items { get; set; }
        public string Sku { get; set; }
        public string ItemsSet { get; set; }
        public string Lot { get; set; }
    }
    [Serializable]
    public class ItemizedChangedCPModel
    {
        public string oldSku { get; set; }
        public string newSku { get; set; }
        public string oldItem { get; set; }
        public string newOrder { get; set; }
        public string itemTypeId { get; set; }
    }
}