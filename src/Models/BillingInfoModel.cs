﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class BillingInfoModel
    {
        public BillingInfoModel() { }
        /// <summary>
        /// spGetInvoiceForOrder
        /// </summary>
        /// <param name="row"></param>
        public BillingInfoModel(DataRow row)
        {
            CurrItemNumber = "" + row["itemnumber"];
            OrigItemNumber = "" + row["previtemnumber"];
            InvoiceNumber = "" + row["invoiceid"];
            Status = "" + row["status"];
            BillingDate = Convert.ToDateTime(row["begindate"]);
            Description = "" + row["description"];
            Price = "" + row["price"];
            ItemCode = "" + row["itemcode"];
            BatchId = "" + row["BatchID"];
        }

        public string CurrItemNumber { get; set; }
        public string OrigItemNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string Status { get; set; }
        public DateTime BillingDate { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string BatchId { get; set; }
        public string ItemCode { get; set; }

        public string DisplayStatus
        {
            get
            {
                return Status != "Completed" ? "<SPAN class=text_highlitedyellow>" + Status + "<span>" : Status;
            }
        }
    }
}