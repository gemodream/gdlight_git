﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ExpressGrading1.aspx.cs" Inherits="Corpt.ExpressGrading1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
    <style type="text/css">
        .auto-style1 {
            width: 988px;
        }
   /*    IvanB*/
        table
		{
			border-collapse: separate !important;
		}
		table td {
		  position: relative;
		}

		.table td input {
		  position: absolute;
		  display: block;
		  top:0;
		  left:0;
		  margin: 0;
		  height: 100% !important;
		  width: 100%;
		  border-radius: 0 !important;
		  border: none;
		  padding: 10px;
		  box-sizing: border-box;
		}
		.centered {
			position: fixed;
			top: 25%;
			left: 50%;
			transform: translate(-50%, -50%);
			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
		}
        .headerClass > th {
            text-align:center;
        }
        .big-btn {
            width:200px !important;
            height: 30px;
            float:inherit;
        }
        /*    IvanB*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
<%--IvanB--%>
    <script type="text/javascript">
        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {            
            $(".filtered-select").select2({
                scrollAfterSelect: true,
            });
        }
    </script>
<%--IvanB--%>
    <div class="demoarea">
        <div class="demoheading">
            Bulk Grading
        </div>
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="navbar nav-tabs">
                    <table>
                        <tr>
                            <td style="padding-top: 10px; padding-bottom: 5px; font-size: small; width: 400px">
                                    <asp:Panel ID="Panel" runat="server" DefaultButton="LoadButton" Width="380px">
                                        Batch Number:
                                        <asp:TextBox runat="server" ID="BatchNumberFld" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item Numbers" ImageUrl="~/Images/ajaxImages/search.png"
                                            OnClick="OnLoadClick" />
                                        <asp:HiddenField runat="server" ID="BatchField" />
                                        <asp:HiddenField runat="server" ID="OrderField" />
                                        <asp:HiddenField runat="server" ID="CustomerCodeValue" />
                                        <asp:HiddenField runat="server" ID="PartEditing" />
                                    </asp:Panel>
                                </td>
                        </tr>
                        <tr>
                                <td colspan="2">
                                    <asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                    <asp:Label ID="LoadMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                </td>
                            </tr>
                        <tr style="height: 200px;">
                            <td style="padding-top: 0px; vertical-align: top">
                                <asp:ListBox ID="lstItemList" runat="server" Width="140px" CssClass="text" Style="vertical-align: top; font-size: 15px;"
                                 AutoPostBack="True" Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged" OnPreRender="OnItemLoad">
                                </asp:ListBox>
                                <asp:TreeView ID="PartTree" runat="server" 
                                    OnSelectedNodeChanged="OnPartsTreeChanged" NodeIndent="15" Enabled="false">
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False"/>
                                    <NodeStyle VerticalPadding="5px" />
                                </asp:TreeView>
                                <br />
                            </td>
                            <td style="padding-left: 20px; vertical-align: top">
                                <asp:UpdatePanel runat="server" ID="SelectedBatchValues">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="SelectedItemNumber" Style="font-family: Cambria; font-weight: bold"></asp:Label>
<%--IvanB--%>
                                        <div id="Rules" runat="server" style="width: 100%; height: 500px; overflow-y:auto">
                                            <asp:GridView class="table table-hover table-bordered" ID="RulesGrid" runat="server" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound">
                                                <HeaderStyle CssClass="headerClass"/>
										        <Columns>
                                                    <asp:TemplateField HeaderText="Part Name">
                                                        <HeaderStyle Width="200px" />
												        <ItemStyle Width="200px" />
												        <ItemTemplate>
                                                            <asp:HiddenField ID="HdnPartId" runat="server" Value='<%#Eval("PartId") %>' />
                                                            <asp:Label runat="server"><%#Eval("PartName") %></asp:Label>
												        </ItemTemplate>
											        </asp:TemplateField>
											        <asp:TemplateField HeaderText="Measure">
                                                        <HeaderStyle Width="200px" />
												        <ItemStyle Width="200px" />
												        <ItemTemplate>
													        <asp:HiddenField ID="HdnMeasureCode" runat="server" Value='<%#Eval("MeasureCode") %>' />
                                                            <asp:HiddenField ID="HdnMeasureID" runat="server" Value='<%#Eval("MeasureID") %>' />
                                                            <asp:HiddenField ID="HdnMeasureClass" runat="server" Value='<%#Eval("MeasureClass") %>' />
                                                            <asp:Label runat="server"><%#Eval("MeasureName") %></asp:Label>
												        </ItemTemplate>
											        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Measure Value">
                                                        <HeaderStyle Width="200px" />
												        <ItemStyle Width="200px" />
												        <ItemTemplate>
                                                             <asp:HiddenField ID="HdnMeasureValueSku" runat="server" Value='<%#Eval("MeasureValueSku") %>' />
                                                            <asp:Label runat="server"><%#Eval("MeasureValueSku") %></asp:Label>
												        </ItemTemplate>
											        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Real Value">
                                                        <HeaderStyle Width="230px" />
												        <ItemStyle Width="230px" />
												        <ItemTemplate>
                                                             <asp:HiddenField ID="HdnMeasureRealValue" runat="server" Value='<%#Eval("MeasureValueReal") %>' />
                                                             <asp:TextBox runat="server" ID="MeasureValueReal" OnTextChanged="RealValue_TextboxChanged" AutoPostBack="true" Width="100%"></asp:TextBox>
                                                             <asp:DropDownList runat="server" ID="MeasureRealEnumFld" DataTextField="ValueTitle" OnSelectedIndexChanged="RealValue_DropdownChanged" AutoPostBack="true"
                                                                                            DataValueField="MeasureValueId" Width="100%" class="filtered-select" />
												        </ItemTemplate>
											        </asp:TemplateField>									
										        </Columns>
									        </asp:GridView>
                                        </div>
<%--IvanB--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>   
<%--IvanB--%>
                                <asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="PassButton" />
                                            <asp:PostBackTrigger ControlID="FailButton" />
                                        </Triggers>                                    
                                        <ContentTemplate>
                                            <div align="center">
                                                <asp:Button runat="server" ID="PassButton" Text="Pass" OnClick="OnPassButtonClick"
                                                    CssClass="btn btn-info big-btn" Enabled="True" />
                                                <asp:Button runat="server" ID="FailButton" Text="Failed" OnClick="OnFailedButtonClick"
                                                    Enabled="False" CssClass="btn btn-info big-btn" />
                                                <asp:Button runat="server" ID="FinishButton" Text="FINISH" OnClick="OnFinishButtonClick"
                                                    Enabled="True" CssClass="btn btn-info big-btn" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
<%--IvanB--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
