﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using Corpt.Constants;
using Corpt.Models.Stats;

namespace Corpt
{
    public partial class ReportQuantities : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Reports Quantity";
            if (!IsPostBack)
            {
                LoadCustomers();
                LoadDocumentTypes();
                OnChangedPeriodType(null, null);
            }

        }
        #region Document types
        public void LoadDocumentTypes()
        {
            var docTypes = QueryUtils.GetDocumentTypes(this);
            docTypes.Insert(0, new DocumentTypeModel { DocumentTypeCode = "", DocumentTypeName = "All" });
            SetViewState(docTypes, SessionConstants.DocumentTypeList);
            DocumentTypeList.DataSource = docTypes;
            DocumentTypeList.DataBind();
            DocumentTypeList.SelectedIndex = 0;
        }
        #endregion

        #region Customer Search
        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this);
            customers.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
            lstCustomerList.SelectedValue = "";
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            filtered.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (filtered.Count == 1) lstCustomerList.SelectedValue = "";
            if (filtered.Count > 1) lstCustomerList.SelectedValue = filtered.ElementAt(1).CustomerId;
        }
        #endregion


        #region Period Type
        private static string PeriodMonthly = "m";
        private static string PeriodWeekly = "w";
        private static string PeriodRandom = "r";
        protected void OnChangedPeriodType(object sender, EventArgs e)
        {
            var type = periodType.SelectedValue;
            var isRandom = (type == PeriodRandom);
            calFrom.Enabled = isRandom;
            calTo.Enabled = isRandom;
            if (isRandom) return;
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var isWeekly = (type == PeriodWeekly);
            var dateFrom = PaginatorUtils.ConvertDateToString(isWeekly ? today.AddDays(-7) : today.AddMonths(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;
        }
        #endregion
        #region Filter
        private StatsFilterModel GetFilterFromPage()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime)dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.PeriodType = periodType.SelectedValue;
            filterModel.CustomerId = lstCustomerList.SelectedValue;
            return filterModel;
        }
        #endregion
        /*
        protected void OnLookupAllClick(object sender, ImageClickEventArgs e)
        {
            ReportQuantityLabel.Text = QueryUtils.GetReportQuantity(GetFilterFromPage(), this);
        }
        */
        protected void OnLookupByDocumentTypeClick(object sender, ImageClickEventArgs e)
        {
            var filter = GetFilterFromPage();
            var data = QueryUtils.GetReportQuantityByDocumentType(filter, this);
            GridDocuments.DataSource = data;
            GridDocuments.DataBind();
            var total = data.Sum(m => m.Quantity);
            var format = "Total: {0} document types, {1} reports";
            ByDocumentTotal.Text = string.Format(format, data.Count, total);
        }

        protected void OnLookupByCustomerClick(object sender, ImageClickEventArgs e)
        {
            var filter = GetFilterFromPage();
            var data = QueryUtils.GetReportQuantityByCustomer(filter, this);
            GridCustomers.DataSource = data;
            GridCustomers.DataBind();
            var total = data.Sum(m => m.Quantity);
            var format = "Total: {0} customers, {1} reports";
            ByCustomerTotal.Text = string.Format(format, data.Count, total);
        }
        /*
        protected void OnLookupRejectRatesClick(object sender, ImageClickEventArgs e)
        {
            var filter = GetFilterFromPage();
            var data = QueryUtils.GetRejectRatesByCustomer(filter, this);
            GridReject.DataSource = data;//.FindAll(m=> m.PrintedQuantity > 0);
            GridReject.DataBind();
            var total = data.Sum(m => m.Quantity);
            var printed = data.Sum(m => m.PrintedQuantity);
            var format = "Total: {0} customers, {1} stones, {2} reports";
            RejectRatesLabel.Text = string.Format(format, data.Count, total, printed);
        }
        */
    }
}