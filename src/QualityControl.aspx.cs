﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Utilities;
using System.Linq;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using System.Reflection;
using System.Globalization;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Text.RegularExpressions;

namespace Corpt
{
    public partial class QualityControl : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid(0);
                gvTitle.InnerText = "Quality Control Result - Top 50";
                LoadCustomers();
                LoadProgram();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearText();
            txtCreateDateSearch.Value = string.Empty;
            grdQualityControl.PageIndex = 0;
            grdQualityControl.SelectedIndex = -1;
            if (gsiOrderFilter.Value.Trim() == "")
            {
                BindGrid(0);
                gvTitle.InnerText = "Quality Control Result - Top 50";
            }
            else
            {
                BindGrid(int.Parse(gsiOrderFilter.Value));
                gvTitle.InnerText = "Quality Control Result - For orders starts with " + gsiOrderFilter.Value;
            }            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            QualityControlModel qcData = new QualityControlModel();
            qcData.QCID = hdnQualityControlID.Value == string.Empty ? 0 : Int64.Parse(hdnQualityControlID.Value);
            qcData.OrderCode = int.Parse(txtGsiOrder.Value);
            qcData.CustomerID = int.Parse(CustomerList.SelectedItem.Value);
            qcData.CustomerProgram = txtSKU.Value;
            qcData.ProgramID = int.Parse(ddlProgram.SelectedItem.Value);
            qcData.Quantity = int.Parse(txtQuantity.Value);
            qcData.Comments = txtNotes.Value;
            qcData.IsQCDone = chkIsQCDone.Checked;
            qcData.IsDisable = chkIsDisable.Checked;
            
            if (IsOrderExist(int.Parse(txtGsiOrder.Value)) == true && hdnQualityControlID.Value == string.Empty)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exist.');", true);
            }
            else
            {
                string status = GSIAppQueryUtils.SaveQualityControl(qcData, this);
                if (status != "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order " + qcData.OrderCode + " not save.');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order saved successfully.');", true);
                    BindGrid(int.Parse(txtGsiOrder.Value));
                    ClearText();
                }
            }
        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (CustomerList.Text != "")
            {
                CustomerLike.Text = "";
                LoadCpList();
            }
            else
            {
                CpList.Items.Clear();
            }
        }

        protected void OnCpSelectedChanged(object sender, EventArgs e)
        {
            if(CpList.Items.Count == 0)            
                txtSKU.Value = "";
            else
                txtSKU.Value = CpList.SelectedItem.Text;
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);

            if (filtered.Count == 1)
            {
                CustomerList.SelectedValue = filtered[0].CustomerId;
            }
            else
            {
                ResetCpList();
            }

            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }

        protected void grdQualityControl_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdQualityControl.PageIndex = e.NewPageIndex;
            if (txtCreateDateSearch.Value.Trim() != string.Empty)
                BindGrid(txtCreateDateSearch.Value.Trim());
            else
                BindGrid(int.Parse(gsiOrderFilter.Value == string.Empty ? "0" : gsiOrderFilter.Value));
        }

        protected void grdQualityControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = GSIAppQueryUtils.GetQualityControlByQCID(Int32.Parse(grdQualityControl.SelectedRow.Cells[1].Text.Trim()), this);

            hdnQualityControlID.Value = ds.Tables[0].Rows[0]["QCID"].ToString();
            txtGsiOrder.Value = ds.Tables[0].Rows[0]["OrderCode"].ToString();
            CustomerList.SelectedValue = ds.Tables[0].Rows[0]["CustomerID"].ToString();
            txtSKU.Value = ds.Tables[0].Rows[0]["CustomerProgram"].ToString();
            ddlProgram.SelectedValue = ds.Tables[0].Rows[0]["ProgramID"].ToString();
            txtQuantity.Value = ds.Tables[0].Rows[0]["Quantity"].ToString();
            txtNotes.Value = ds.Tables[0].Rows[0]["Comments"].ToString().Replace("&nbsp;", string.Empty); ;
            chkIsQCDone.Checked = bool.Parse(ds.Tables[0].Rows[0]["IsQCDone"].ToString());
            btnSave.Text = "Update";
          //  dvSave.Style.Add("margin-left", "89px");
        }

        protected void btnClear_ServerClick(object sender, EventArgs e)
        {
            gvTitle.InnerText = "Quality Control Result - Top 50";
            //dvSave.Style.Add("margin-left", "175px");
            grdQualityControl.PageIndex = 0;
            grdQualityControl.SelectedIndex = -1;
            ClearText();
            ClearFilterText();
        }

        protected void btnCreateDateSearch_Click(object sender, EventArgs e)
        {
            if (txtCreateDateSearch.Value.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter date to serach.');", true);
                return;
            }
            ClearText();
            gsiOrderFilter.Value = string.Empty;
            grdQualityControl.PageIndex = 0;
            grdQualityControl.SelectedIndex = -1;
            BindGrid(txtCreateDateSearch.Value.Trim());
            gvTitle.InnerText = "Quality Control Result - For orders created on " + txtCreateDateSearch.Value;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DataSet dsScr = new DataSet();
            dsScr = (DataSet)Session["QualityControlData"];
            Export(dsScr, "QualityControl", false, this);
        }

        private void BindGrid(int orderCode)
        {
            List<QualityControlModel> lstQC = new List<QualityControlModel>();
            DataSet dsScr = new DataSet();
            lstQC = GSIAppQueryUtils.GetQualityControlByOrder(orderCode, this);
            dsScr.Tables.Add(ToDataTable<QualityControlModel>(lstQC));
            Session["QualityControlData"] = dsScr;
            grdQualityControl.DataSource = lstQC;
            grdQualityControl.DataBind();
        }

        private void BindGrid(string strCreateDate)
        {
            List<QualityControlModel> lstQC = new List<QualityControlModel>();
            DataSet dsScr = new DataSet();
            DateTime dtCreate = DateTime.Parse(strCreateDate, CultureInfo.CreateSpecificCulture("en-US"));
            lstQC = GSIAppQueryUtils.GetQualityControlByCreateDate(dtCreate, this);
            dsScr.Tables.Add(ToDataTable<QualityControlModel>(lstQC));
            Session["QualityControlData"] = dsScr;
            grdQualityControl.DataSource = lstQC;
            grdQualityControl.DataBind();
        }

        private void ClearText()
        {
            hdnQualityControlID.Value = string.Empty;
            txtGsiOrder.Value = string.Empty;
            CustomerList.SelectedIndex = 0;
            CpList.Items.Clear();
            txtSKU.Value = string.Empty;
            ddlProgram.SelectedIndex = 0;
            txtQuantity.Value = string.Empty;
            txtNotes.Value = string.Empty;
            chkIsQCDone.Checked = false;
            chkIsDisable.Checked = false;
            btnSave.Text = "Add New";
            grdQualityControl.SelectedIndex = -1;
        }

        private void ClearFilterText()
        {
            gsiOrderFilter.Value = string.Empty;
            txtCreateDateSearch.Value = string.Empty;
            BindGrid(0);
        }

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            CustomerList.DataSource = customers;
            CustomerList.DataBind();
            CustomerList.SelectedIndex = 0;
        }

        private void LoadProgram()
        {
            ddlProgram.DataSource = GSIAppQueryUtils.GetPrograms(this);
            ddlProgram.DataBind();
            ddlProgram.Items.Insert(0, new ListItem("", "0"));
            ddlProgram.SelectedIndex = 0;
        }

        private void LoadCpList()
        {
            var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            if (customersList.Count == 0)
            {
                return;
            }
            var customerModel = customersList.Find(m => m.CustomerId == CustomerList.SelectedValue);
            if (customerModel == null) return;
            var cpList = QueryUtils.GetCustomerPrograms(customerModel, null, this);
            //-- Add Vendor model
            SetViewState(cpList, SessionConstants.CustomerProgramList);

            CpList.DataSource = cpList;
            CpList.DataBind();

            OnCpSelectedChanged(null, null);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
           // dataTable.Columns.Remove("Comments");
            return dataTable;
        }

        private void ResetCpList()
        {
            SetViewState(null, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.DataSource = null;
            CpList.DataBind();
        }

        private List<CustomerModel> GetCustomersFromView()
        {
            return GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }

        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
        }

        private bool IsOrderExist(int orderCode)
        {
            bool isOrderExist = false;
            if (GSIAppQueryUtils.GetScreeningByOrder(orderCode, this).Count > 0)
                isOrderExist = true;
            return isOrderExist;
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }


            //-- Save File
            //var dir = GetExportDirectory(p);

            //var filename = fname + ".xlsx";
            //var sw = File.Create(dir + filename);
            //workbook.Write(sw);
            //sw.Close();
            //if (!skipDownload) DownloadExcelFile(filename, p);

            var filename = fname + ".xlsx";
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p);
            Memory.Flush();
        }

        public static void DownloadExcelFile(String filename, Page p)
        {
            var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            p.Response.End();
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            if (!forHeader)
            {
                //style.WrapText = true;
            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Center;
            return style;
        }

        

    }
}