﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="NotifyBilling.aspx.cs" Inherits="Corpt.NotifyBilling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1"></ajaxToolkit:ToolkitScriptManager>
    
    <style>
        .text_highlitedyellow {
            background-color: #FFFF00;
        }
    </style>

    <div class="demoarea">
        <div class="demoheading">Billing Notification</div>
        <asp:Panel class="form-inline" runat="server" DefaultButton="btnNotification">            
            <div style="margin-top:10px;">Order Number:&nbsp<asp:TextBox runat="server" ID="txtOrder" MaxLength="7" Width="100px" style="margin-left:26px;"></asp:TextBox></div>            
            <div style="margin-top:10px;">Program Name:&nbsp<asp:TextBox runat="server" ID="txtProgramName" MaxLength="100" Width="300px" style="margin-left:22px;"></asp:TextBox></div>
            <div style="margin-top:10px;">Certificates/Tag:&nbsp<asp:TextBox runat="server" ID="txtCertificates" MaxLength="5" Width="100px" style="margin-left:17px;"></asp:TextBox></div>
            <div style="margin-top:10px;">Laser Inscription:&nbsp<asp:TextBox runat="server" ID="txtLaserInscription" MaxLength="5" Width="100px" style="margin-left:13px;"></asp:TextBox></div>
            <div style="margin-top:10px;">Comment/Remark:&nbsp<asp:TextBox runat="server" ID="txtDescription" MaxLength="100" Width="300px" style="margin-left:5px;"></asp:TextBox></div>            
            <asp:Button ID="btnNotification" runat="server" Text="Notify For NY Billing" CssClass="btn btn-primary" style="margin-top:10px;" OnClick="btnNotification_Click"  />
        </asp:Panel>
        <div>
            <asp:Label runat="server" ID="MessageField" ForeColor="Red"></asp:Label>
        </div>
        <br />
        <!-- Item Numbers DataGrid-->
        <div>
            <asp:DataGrid runat="server" ID="InfoGrid" AutoGenerateColumns="False" CellPadding="5" CellSpacing="10" Style="font-size: small">
                <Columns>
                    <asp:BoundColumn DataField="CurrItemNumber" HeaderText="New Item Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="OrigItemNumber" HeaderText="Orig Item Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InvoiceNumber" HeaderText="Invoice Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
                    <asp:BoundColumn DataField="BillingDate" HeaderText="Billing Date"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Description" HeaderText="Description"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Price" HeaderText="Price"></asp:BoundColumn>
                </Columns>

                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria"
                    ForeColor="White" />

            </asp:DataGrid>
        </div>

        <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrder"
            Display="None"
            ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required."
            ValidationGroup="OrderGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
            HighlightCssClass="validatorCalloutHighlight" />
        <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrder"
            Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$"
            ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>"
            ValidationGroup="OrderGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
            HighlightCssClass="validatorCalloutHighlight" />
    </div>
</asp:Content>
