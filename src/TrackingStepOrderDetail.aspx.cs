using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for TrackingStepOrderDetail.
	/// </summary>
	public partial class TrackingStepOrderDetail : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			ShowInfo();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowInfo()
		{
			LoadBatchHistory();
		}

		private void LoadBatchHistory()
		{
			SqlCommand command = new SqlCommand("wspvvGetHistory2");
			command.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@BatchID",Request.Params["p2"].Trim()));

			Console.WriteLine(Request.Params["p2"].Trim());

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);

			DataTable dtBatchHistory = new DataTable();
			dtBatchHistory.Columns.Add("Date");
			dtBatchHistory.Columns.Add("Time");
			dtBatchHistory.Columns.Add("Stage");

			bool showTracking = false;
			foreach(DataRow dr in dt.Rows)
			{
				DataRow drBH = dtBatchHistory.NewRow();
				drBH["Stage"]=dr["Stage"].ToString();
				drBH["Date"]=DateTime.Parse(dr["pDate"].ToString()).ToShortDateString();
				drBH["Time"]=DateTime.Parse(dr["pDate"].ToString()).ToShortTimeString();
				showTracking=(drBH["Stage"].ToString()=="Checked Out");
				dtBatchHistory.Rows.Add(drBH);
			}
			dgHistory.DataSource=dtBatchHistory;
			dgHistory.DataBind();

			if(showTracking)
			{
				ShowTrackingInfo();
			}
		}

		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Middle.aspx");
		}

		protected void cmdTrackingStart_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("TrackingStart.aspx");
		}

		protected void cmdBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("TrackingStep2.aspx?p1="+HttpUtility.UrlEncode(Session["trP1"].ToString())+"&p2="+HttpUtility.UrlEncode(Session["trP2"].ToString()));
		}

		private void ShowTrackingInfo()
		{
            SqlCommand command = new SqlCommand("sp_getTrackingInfoByBatchId");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;
			
			command.Parameters.Add(new SqlParameter("@BatchID",Request.Params["p2"].Trim()));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();

			da.Fill(dt);

			if(dt.Rows.Count>0)
			{
				lblTrackingInfo.Text="<b>Delivery: </b>Sent out with " + Utils.FormatCarrierString(dt.Rows[0]["CarrierTrackingNumber"].ToString(),Int32.Parse(dt.Rows[0]["CarrierCode"].ToString()));
			}
			else
			{
				lblTrackingInfo.Text="";
			}
			//lblCarrier.Text="<b>Delivery: </b>Sent out with " + Utils.FormatCarrierString(reader.GetSqlValue(reader.GetOrdinal("CarrierTrackingNumber")).ToString(),Int32.Parse(reader.GetSqlValue(reader.GetOrdinal("CarrierCode")).ToString()));
		}
	}
}
