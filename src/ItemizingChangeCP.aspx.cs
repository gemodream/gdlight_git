﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
namespace Corpt
{
    public partial class ItemizingChangeCP : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: ItemizingChangeCP";
           
            if (!IsPostBack)
            {
                var tmp = Request.UrlReferrer.ToString();
                Session["Redirect"] = tmp;
                string newOrder = null;
                string oldItem = null;
                if (Request.QueryString["OrderCode"] != null)
                {
                    newOrder = (string)Request.QueryString["OrderCode"];
                    NewOrder.Value = newOrder;
                }
                if (Request.QueryString["Item"] != null)
                {
                    oldItem = (string)Request.QueryString["Item"];
                    OldItem.Value = oldItem;
                }
                
                LoadItemizingScreen(newOrder, oldItem);
                

                /* Security to prevent user accessing directly via url after logging in 
                string err = "";
                bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgReassembly, this, out err);
                if (!access || err != "")
                {
                    Response.Redirect("Middle.aspx");
                }
                 */
                //CopyButton.Attributes.Add("onclick", "return confirm('Are you sure you want to copy?');");
            }

        }

        #region load initial screeing
        protected void LoadItemizingScreen(string newOrder, string oldItem)
        {
            string oldCPName = null, oldCustomerId = null, oldCustomerStyle = null, oldDescription = null, oldComment = null, oldPicturePath = null, itemTypeID = null;
            string newItemTypeId = null, newCustomerId = null;
            DataTable dtOldBatch = QueryUtils.GetOldCustomerProgramByBatchCode(oldItem, this);
            if (dtOldBatch != null && dtOldBatch.Rows.Count != 0)
            {
                DataRow drOldBatch = dtOldBatch.Rows[0];
                oldCPName = drOldBatch["CustomerProgramName"].ToString();
                oldCustomerId = drOldBatch["CustomerID"].ToString();
                oldCustomerStyle = drOldBatch["CustomerStyle"].ToString();
                oldDescription = drOldBatch["Description"].ToString();
                oldComment = drOldBatch["Comment"].ToString();
                oldPicturePath = drOldBatch["Path2Picture"].ToString();
                itemTypeID = drOldBatch["ItemTypeID"].ToString();
                OldCP.Text = "Old CP: " + oldCPName;
                OldCustomerIDBox.Text = oldCustomerId;
                OldCustomerStyleBox.Text = oldCustomerStyle;
                OldDescBox.Text = oldDescription;
                OldCommentBox.Text = oldComment;
                ShowPicture(oldPicturePath, true);
                OldCP.Enabled = false;
                OldCustomerIDBox.Enabled = false;
                OldCustomerStyleBox.Enabled = false;
                OldDescBox.Enabled = false;
                OldCommentBox.Enabled = false;
                OldSRPBox.Enabled = false;
                NewCP.Enabled = false;
                NewCustomerIDBox.Enabled = false;
                NewCustomerStyleBox.Enabled = false;
                NewDescBox.Enabled = false;
                NewCommentBox.Enabled = false;
                NewSRPBox.Enabled = false;

            }
            else
                return;
            DataTable dtNewOrder = QueryUtils.GetGroupByCode(newOrder, this);
            if (dtNewOrder != null && dtNewOrder.Rows.Count != 0)
            {
                DataRow drNewOrder = dtNewOrder.Rows[0];
                newItemTypeId = itemTypeID;
                newCustomerId = drNewOrder["CustomerID"].ToString();
                Session["CustomerID"] = newCustomerId;
                DataTable dtNewCP = QueryUtils.GetCPPerCustomer(newCustomerId, this);
                if (dtNewCP != null && dtNewCP.Rows.Count != 0)
                {
                    foreach (DataRow dr in dtNewCP.Rows)
                    {
                        if (dr["ItemTypeID"].ToString() == newItemTypeId)
                        CpListField.Items.Add(dr["CustomerProgramName"].ToString());
                    }
                    CpListField.SelectedIndex = -1;
                    var cpList = (from DataRow row in dtNewCP.Rows select row).Where(m => m["ItemTypeID"].ToString() == newItemTypeId);
                    Session["NewCPs"] = cpList;
                }
            }
        }

        protected void ShowPicture(string dbPicture, bool old)
        {
            string errMsg;
            var ms = new MemoryStream();
            var fileType = "";
            var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
            //var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
                var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                if (old)
                {
                    OldItemPicture.ImageUrl = ImageUrl;
                    OldItemPicture.Visible = true;
                }
                else
                {
                    NewItemPicture.ImageUrl = ImageUrl;
                    NewItemPicture.Visible = true;
                }
            }
            //-- Path to picture
            //Picture2PathField.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            //ErrorPictureField.Text = errMsg;
        }
        #endregion
        #region load new batch
        protected void OnCpChanged(object sender, EventArgs e)
        {
            string newCP = CpListField.SelectedItem.Text;
            string customerId = (string)Session["CustomerID"];
            IEnumerable<DataRow> cps = (IEnumerable<DataRow>)Session["NewCPs"];
            foreach(var cp in cps)
            {
                if (cp["CustomerProgramName"].ToString() == CpListField.SelectedItem.Text)
                {
                    NewCP.Text = "New CP: " + cp["CustomerProgramName"].ToString();
                    NewCustomerIDBox.Text = cp["CustomerID"].ToString();
                    NewCustomerStyleBox.Text = cp["CustomerStyle"].ToString();
                    NewDescBox.Text = cp["Description"].ToString();
                    NewCommentBox.Text = cp["Comment"].ToString();
                    ShowPicture(cp["Path2Picture"].ToString(), false);
                    break;
                }
            }
        }
        #endregion
        #region Copy button
        protected void OnCPCopyClick(object sender, EventArgs e)
        {
            ItemizedChangedCPModel itemChangedCP = new ItemizedChangedCPModel();
            itemChangedCP.newOrder = NewOrder.Value;
            itemChangedCP.oldItem = OldItem.Value;
            itemChangedCP.newSku = NewCP.Text.Substring(8);
            itemChangedCP.oldSku = OldCP.Text.Substring(8);
            Session["BackToItemizing"] =itemChangedCP;
            ClientScript.RegisterStartupScript(this.GetType(), "Close", "window.close();", true);
            /*
            var srcModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            var trgModel = GetViewState(SessionConstants.ReassemblyTargetItem) as ItemCopyModel;
            if (srcModel == null || trgModel == null) return;
            var srcPartType = "";
            var trgPartType = "";
            if (SrcTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(SrcTreeView.SelectedNode.Value);
                srcModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = srcModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) srcPartType = partModel.PartTypeId;
            }
            if (TargetTreeView.SelectedNode != null)
            {
                var partId = Convert.ToInt32(TargetTreeView.SelectedNode.Value);
                trgModel.CopyPartId = Convert.ToInt32(partId);
                var partModel = trgModel.ItemParts.Find(m => m.PartId == partId);
                if (partModel != null) trgPartType = partModel.PartTypeId;
            }
            var msg = "";
            if (string.IsNullOrEmpty(srcPartType))
            {
                msg = "Select Source Item Part!";
            }
            else if (string.IsNullOrEmpty(trgPartType))
            {
                msg = "Select Target Item Part!";
            }
            else if (srcPartType != trgPartType)
            {
                msg = "Different part types. Copying can't be done.";
            }
            if (!string.IsNullOrEmpty(msg))
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;

            }
            msg = QueryUtils.CopyItemPart(srcModel, trgModel, this);
            if (string.IsNullOrEmpty(msg))
            {
                msg = "Copy done.";
            }
            System.Web.HttpContext.Current.Response.Write(
               "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
               */
        }
        #endregion

        #region Source Item

        protected void OnSourceLoadClick(object sender, ImageClickEventArgs e)
        {
            /* alex
            ResetSourceData();
            var itemNumber = SrcItemField.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ResetSourceData();
                return;
            }
            var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
            if (myResult.Trim() != "")
            {
                SrcItemField.Text = myResult;
                itemNumber = myResult;
            }
            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetSourceData();
                var msg = "Please enter a Source Item in the format: 10 or 11 numeric characters";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            var sourceModel = QueryUtils.GetItemByCode(SrcItemField.Text.Trim(), this);
            if (sourceModel == null)
            {
                ResetSourceData();
                var msg = "Source Item # " + itemNumber + " does not exist!";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;

            }
            SetViewState(sourceModel, SessionConstants.ReassemblySourceItem);
            var itemName = itemNumber != sourceModel.OldItemNumber
                               ? itemNumber + "/" + sourceModel.OldItemNumber
                               : itemNumber;
            SrcItemName.Text = itemName;
            SrcCustomer.Text = sourceModel.CustomerName;

            var itemParts = QueryUtils.GetMeasureParts(sourceModel.ItemTypeId, this);
            sourceModel.ItemParts = itemParts;
            var treeRoot = GetRootTreeModel(itemParts);

            SrcTreeView.Nodes.Clear();
            var rootNode = new TreeNode(treeRoot.PartName, "" + treeRoot.PartId);
            FillNode(rootNode, treeRoot);
            SrcTreeView.Nodes.Add(rootNode);
            alex */
        }
        protected void SrcNodeChanged(object sender, EventArgs e)
        {
            /*
            var selNode = SrcTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var itemModel = GetViewState(SessionConstants.ReassemblySourceItem) as ItemCopyModel;
            if (itemModel == null) return;
            var measures = itemModel.Measures.FindAll(m => m.PartId == partId);
            SrcGrid.DataSource = measures;
            SrcGrid.DataBind();
            */
        }

        private void ResetSourceData()
        {
            /*
            SrcCustomer.Text = "";
            SrcItemName.Text = "";
            SrcTreeView.Nodes.Clear();
            SrcGrid.DataSource = null;
            SrcGrid.DataBind();
            */
        }
        #endregion

        #region Target Item
        protected void OnTargetLoadClick(object sender, ImageClickEventArgs e)
        {
            /* alex
            ResetTargetData();
            var itemNumber = TargetItemField.Text.Trim();
            if (string.IsNullOrEmpty(itemNumber))
            {
                ResetTargetData();
                return;
            }
            var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(itemNumber, this.Page);
            if (myResult.Trim() != "")
            {
                TargetItemField.Text = myResult;
                itemNumber = myResult;
            }

            if (!Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                ResetTargetData();
                const string msg = "Please enter a Target Item in the format: 10 or 11 numeric characters";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            var targetModel = QueryUtils.GetItemByCode(TargetItemField.Text.Trim(), this);
            if (targetModel == null)
            {
                ResetTargetData();
                var msg = "Target Item # " + itemNumber + " does not exist!";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;

            }
            SetViewState(targetModel, SessionConstants.ReassemblyTargetItem);

            var itemName = itemNumber != targetModel.OldItemNumber
                               ? itemNumber + "/" + targetModel.OldItemNumber
                               : itemNumber;
            TargetItemName.Text = itemName;

            TargetCustomer.Text = targetModel.CustomerName;

            var itemParts = QueryUtils.GetMeasureParts(targetModel.ItemTypeId, this);
            targetModel.ItemParts = itemParts;
            var treeRoot = GetRootTreeModel(itemParts);

            TargetTreeView.Nodes.Clear();
            var rootNode = new TreeNode(treeRoot.PartName, "" + treeRoot.PartId);
            FillNode(rootNode, treeRoot);
            TargetTreeView.Nodes.Add(rootNode);
            alex */
        }
        protected void TargetNodeChanged(object sender, EventArgs e)
        {
            /*
            var selNode = TargetTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var itemModel = GetViewState(SessionConstants.ReassemblyTargetItem) as ItemCopyModel;
            if (itemModel == null) return;
            var measures = itemModel.Measures.FindAll(m => m.PartId == partId);
            TargetGrid.DataSource = measures;
            TargetGrid.DataBind();
            */
        }

        private void ResetTargetData()
        {
            /*
            TargetCustomer.Text = "";
            TargetItemName.Text = "";
            TargetTreeView.Nodes.Clear();
            TargetGrid.DataSource = null;
            TargetGrid.DataBind();
            */

        }
        #endregion

        #region Tree builder
        private static void FillNode(TreeNode node, ItemPartTreeModel root)
        {
            if (root.Childs.Count <= 0) return;
            foreach (var child in root.Childs)
            {
                var addNode = new TreeNode(child.PartName, "" + child.PartId);
                FillNode(addNode, child);
                node.ChildNodes.Add(addNode);
                node.Expanded = true;
            }
        }
        
        private static ItemPartTreeModel GetRootTreeModel(List<MeasurePartModel> parts)
        {
            var root = parts.Find(m => !m.HasParent);
            var rootNode = root == null ? null : new ItemPartTreeModel(root);
            if (rootNode != null)
            {
                GetTreeChild(parts, rootNode);
            }
            return rootNode;
        }
        private static void GetTreeChild(List<MeasurePartModel> parts, ItemPartTreeModel root)
        {
            var childs = parts.FindAll(m => m.ParentPartId == root.PartId);
            foreach (var subItem in childs.Select(child => new ItemPartTreeModel(child)))
            {
                subItem.Parent = root;
                root.Childs.Add(subItem);
                GetTreeChild(parts, subItem);
            }
        }
        #endregion
        #region Clear Button
        protected void OnCPClearClick(object sender, EventArgs e)
        {
            //string redirect = (string)Session["Redirect"];
            //Response.Redirect(redirect);
            //Response.Redirect(Request.UrlReferrer.ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "Close", "window.close();", true);
            //SrcItemField.Text = "";
            ResetSourceData();
            //TargetItemField.Text = "";
            ResetTargetData();

        }
        #endregion
    }
}