﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ReItemizn.aspx.cs" Inherits="Corpt.ReItemizn" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading" runat="server" ID="TitleDiv"></div>
        <asp:HiddenField runat="server" ID="ModeFld"/>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
             <ContentTemplate>
                 <div class="navbar nav-tabs">
                     <asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
                         <asp:Label runat="server" Text="Order: " CssClass="label" Style=""></asp:Label>
                         <asp:TextBox runat="server" ID="OrderField" MaxLength="7" Style="margin-top: 10px;margin-left: 5px"></asp:TextBox>
                         <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                             ValidChars="0123456789" TargetControlID="OrderField" />
                         <asp:ImageButton ID="SearchButton" runat="server" ToolTip="Load Order" ImageUrl="~/Images/ajaxImages/search.png"
                             OnClick="OnSearchClick" Style="margin-right: 15px" />
                         <asp:Button runat="server" ID="ValidButton" Text="Valid" CssClass="btn btn-info" OnClientClick = 'return confirm("Are you sure you want to change items state to Valid?");'
                              Visible="False"  OnClick="OnValidClick" ToolTip="Set changes to db" />
                         <asp:Button runat="server" ID="InvalidButton" Text="Invalid" CssClass="btn btn-info" OnClientClick = 'return confirm("Are you sure you want to change items state to Invalid?");'
                              Visible="False" OnClick="OnInvalidClick" ToolTip="Set changes to db" />
                         <asp:Button runat="server" ID="EndSessionButton" Text="End Session" CssClass="btn btn-info" OnClientClick = 'return confirm("Are you sure you want to end session?");'
                              Visible="False" OnClick="OnEndSessionClick"  />
                     </asp:Panel>
                 </div>
                 <asp:TreeView ID="TreeHistory" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                     onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                     Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                     ShowLines="False" ShowExpandCollapse="true">
                 </asp:TreeView>
                 <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>
             </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
