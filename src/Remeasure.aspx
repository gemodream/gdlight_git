﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Remeasure.aspx.cs" Inherits="Corpt.Remeasure" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%-- IvanB 15/03 start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<%-- IvanB 15/03 end --%>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
    </style>
    <script type="text/javascript">
        /*IvanB 15/03 start*/
        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {        
            $(".filtered-select").select2({
                scrollAfterSelect: true,
            } );
            $( ".js-example-placeholder-single" ).select2( {
                theme: "bootstrap",
                placeholder: "Select Item",
                allowClear: true
            } );
        }
        /*IvanB 15/03 end*/
        function jsSetEditMode() {
			document.getElementById('<%= SaveButton.ClientID %>').disabled = false;
			document.getElementById('<%= SaveButton2.ClientID %>').disabled = false;
            document.getElementById('<%= UndoButton.ClientID %>').disabled = false;
            document.getElementById('<%= FindInputItemBtn.ClientID %>').disabled = true;
            document.getElementById('<%= FullSetModeFld.ClientID %>').disabled = true;
            document.getElementById('<%= InputItemFld.ClientID %>').disabled = true;
            document.getElementById('<%= TreeHistory.ClientID %>').disabled = true;
            var tv = document.getElementById('<%= TreeHistory.ClientID %>');
            var links = tv.getElementsByTagName("a");
            for (var loop = 0; loop < links.length; loop++){
                $(links[loop]).click(function (event) {
                    event.preventDefault();
                    return false;
                });
            }
        }
    </script>
    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
            <ContentTemplate>
                 <div class="demoheading">Remeasure</div>
                <div >
                    <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="FindInputItemBtn">
                        <label>
                            Item:</label>
                        <asp:TextBox runat="server" ID="InputItemFld" MaxLength="11"></asp:TextBox>
                        <label>
                            New #</label>
                        <asp:TextBox runat="server" ID="NewItemNumberFld" Enabled="False"></asp:TextBox>
                        <asp:Button runat="server" ID="FindInputItemBtn" OnClick="OnFindItemBtnClick" CssClass="btn btn-info btn-small"
                            Text="Load" style="height: 22px" />
                        <asp:Button runat="server" ID="SaveButton" OnClick="OnSaveButtonClick"  Enabled="true" CssClass="btn btn-small btn-info"
                            Text="Save" OnClientClick='return confirm("Are you sure you want to save changed measures?");' />
                        
                        <asp:Button runat="server" ID="UndoButton" disabled="disabled" OnClick="OnUndoButtonClick" Text="Undo" CssClass="btn btn-small btn-info" 
                            OnClientClick='return confirm("Are you sure you want to undo changed measures?");'/>
                        <asp:CheckBox ID="FullSetModeFld" runat="server" Text="Full Set" Checked="True" OnCheckedChanged="OnSetModeChanged" AutoPostBack="True" Enabled="False"/>

                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                            ValidChars="0123456789-" TargetControlID="InputItemFld" Enabled="True" />
                    </asp:Panel>
                    <table>
                         <tr>
                             <td  style="vertical-align: top">
                                 <asp:TreeView ID="TreeHistory" runat="server" ShowCheckBoxes="None" 
                                      Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                     Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="False" EnableClientScript="False"
                                     ShowLines="False" ShowExpandCollapse="true" OnSelectedNodeChanged="OnTreeHistorySelectedChanged">
                                     <SelectedNodeStyle BackColor="#D7FFFF" />
                                 </asp:TreeView>
                             </td>
                             <td style="width: 20px"></td>
                             <td style="vertical-align: top">
                                 <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False" ActiveTabIndex="0"
                                     Width="100%" Height="100%" Visible="False">
                                     <ajaxToolkit:TabPanel runat="server" HeaderText="Measurements" OnDemandMode="Once"
                                         ID="TabEditor">
                                         <ContentTemplate>
                                             <table>
                                                 <tr>
                                                     <td style="width: 20px">
                                                     </td>
                                                     <td style="vertical-align: top">
                                                         <asp:Panel runat="server" ID="MeasurementPanel" Visible="False">
                                                             <asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel"></asp:Label>
                                                             <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red"></asp:Label>
                                                             <asp:Label runat="server" ID="ModeLabel" Text="(Full Set)" />
                                                             <div style="overflow-y: scroll; max-height:600px; width: 500px; color: black; border: silver solid 1px;
                                                                 margin-top: 5px" id="EditItemDiv" runat="server">
                                                                 <asp:DataGrid runat="server" ID="ItemValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                                     DataKeyField="UniqueKey" OnItemDataBound="OnEditItemDataBound" Style="max-height:100px;overflow-y:scroll;">
                                                                     <Columns>
                                                                         <asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"></asp:BoundColumn>
                                                                         <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                                                         <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                                                         <asp:BoundColumn DataField="MeasureClass" HeaderText="Measure Class" Visible="False">
                                                                         </asp:BoundColumn>
                                                                         <asp:TemplateColumn>
                                                                             <HeaderTemplate>
                                                                                 Value</HeaderTemplate>
                                                                             <ItemTemplate>
                                                                                 <asp:Panel runat="server" ID="ItemValuePanel" CssClass="form-inline" DefaultButton="EditHiddenBtn">
                                                                                     <asp:Button runat="server" ID="EditHiddenBtn" Style="display: none" Enabled="False" />
                                                                                     <asp:TextBox runat="server" ID="ValueStringFld" Width="135px" onchange="jsSetEditMode();"></asp:TextBox>
                                                                                     <asp:TextBox runat="server" ID="ValueNumericFld" Width="135px" onchange="jsSetEditMode();"></asp:TextBox>
                                                                                     <ajaxToolkit:FilteredTextBoxExtender ID="ValueNumericExtender" runat="server" FilterType="Custom"
                                                                                         ValidChars="0123456789." TargetControlID="ValueNumericFld" />
                                                                                     <asp:DropDownList runat="server" ID="ValueEnumFld" DataTextField="ValueTitle" DataValueField="MeasureValueId"
                                                                                         Width="150px"  onchange="jsSetEditMode();" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" />
                                                                                 </asp:Panel>
                                                                             </ItemTemplate>
                                                                         </asp:TemplateColumn>
                                                                     </Columns>
                                                                     <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                     <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                                 </asp:DataGrid>
                                                             </div>
                                                         </asp:Panel>
                                                     </td>
                                                     <td style="width: 20px">
                                                     </td>
                                                     <td style="width: 20px">
                                                     </td>
                                                     <td style="vertical-align: top; padding-left: 10px; padding-right: 10px; font-family: sans-serif">
                                                         <asp:Panel runat="server" ID="CpPanel">
                                                             <asp:Label runat="server" Text="Item Parts" ></asp:Label><br />
                                                             <asp:ListBox runat="server" ID="PartsList" DataTextField="PartName" DataValueField="PartId"             OnSelectedIndexChanged="OnPartsListChanged" Width="150px" AutoPostBack="True"></asp:ListBox>
                                                             <asp:Label runat="server" ID="CpPathToPicture"></asp:Label><br />
                                                             <asp:Image ID="itemPicture" runat="server" Width="90px" Visible="False"></asp:Image><br />
                                                             <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Font-Size="12px"></asp:Label>
                                                             <div runat="server" id="CpDescDiv" style="font-size: small; width: 300px">
                                                             </div>
                                                             <div runat="server" id="CpCommDiv" style="font-size: small; width: 300px">
                                                             </div>
                                                             <div>
                                                                 <a href="CustomerProgram.aspx" target="_blank" id="CpRef" runat="server"></a>
                                                             </div>
                                                         </asp:Panel>
                                                     </td>
                                                 </tr>
                                             </table>
                                         </ContentTemplate>
                                     </ajaxToolkit:TabPanel>
                                     <ajaxToolkit:TabPanel runat="server" HeaderText="Migrated/Invalid Items" OnDemandMode="Once"
                                         ID="TabPanel1">
                                         <ContentTemplate>
                                             <table>
                                                 <tr>
                                                     <td style="vertical-align: top">
                                                         <asp:Label ID="Label2" runat="server" Text="Invalid Items" Style="font-weight: bold;
                                                             color: red; margin-bottom: 5px"></asp:Label><br />
                                                         <asp:ListBox ID="ItemsInvalidList" runat="server" Width="120px" />
                                                     </td>
                                                     <td style="width: 20px">
                                                     </td>
                                                     <td>
                                                         <asp:Label ID="MigratedLabel" runat="server" Text="" CssClass="label" Style="margin-bottom: 5px"></asp:Label><br />
                                                         <div style="overflow-y: auto; overflow-x: hidden; height: 530px; width: 300px; color: black">
                                                             <asp:DataGrid runat="server" ID="MigratedGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                                                 <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                 <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                                 <Columns>
                                                                     <asp:BoundColumn DataField="PrevItemNumber" HeaderText="From Old"></asp:BoundColumn>
                                                                     <asp:BoundColumn DataField="CurrentItemNumber" HeaderText="Current"></asp:BoundColumn>
                                                                     <asp:BoundColumn DataField="NewItemNumber" HeaderText="To New"></asp:BoundColumn>
                                                                 </Columns>
                                                             </asp:DataGrid>
                                                         </div>
                                                     </td>
                                                 </tr>
                                             </table>
                                         </ContentTemplate>
                                     </ajaxToolkit:TabPanel>
                                 </ajaxToolkit:TabContainer>
								 <asp:Button runat="server" ID="SaveButton2" OnClick="OnSaveButtonClick"  disabled="disabled" Visible="false" CssClass="btn btn-small btn-info"
									Text="Save" OnClientClick='return confirm("Are you sure you want to save changed measures?");' />
                             </td>
                         </tr>
                     </table>

                 </div>
                 
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;color: black"
                        id="MessageDiv" runat="server">
                    </div>
                    <asp:HiddenField runat="server" ID="EditDialogUp"/>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" OnClick="OnCloseInfoDialogClick"/>
                            <asp:Button ID="InfoPsevdoCloseButton" runat="server" Text="Ok" Style="display: none" Enabled="False"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OkControlID="InfoPsevdoCloseButton">
                </ajaxToolkit:ModalPopupExtender>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div></div>
</asp:Content>
