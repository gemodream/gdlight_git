﻿using System;
using System.IO;
using System.Web;

namespace Corpt
{
    /// <summary>
    /// Сводное описание для PdfMapHandler
    /// </summary>
    public class PdfMapHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CreateImage(context);
        }
        private void CreateImage(HttpContext context)
        {
            if (context.Request.QueryString["pdf"] != null)
            {
                string strPdf = context.Request.QueryString["pdf"];
                FileStream fs = null;
                BinaryReader br = null;
                byte[] data;
                try
                {
                    fs = new FileStream(context.Server.MapPath(strPdf), FileMode.Open, FileAccess.Read, FileShare.Read);
                    br = new BinaryReader(fs, System.Text.Encoding.Default);
                    data = new byte[Convert.ToInt32(fs.Length)];
                    br.Read(data, 0, data.Length);
                    context.Response.Clear();
                    context.Response.ContentType = "application/pdf";
                    context.Response.BinaryWrite(data);
                    context.Response.End();
                }
                catch (Exception ex)
                {
                    context.Response.Write(ex.Message);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        fs.Dispose();
                    }
                    
                    if (br != null) br.Close();
/*
                    data = null;
*/
                }
            }
            else
            {
                context.Response.Write("Wrong path");
            }
        }
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}