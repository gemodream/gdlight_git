﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;

namespace Corpt
{
    public partial class SyntheticDataEntry : System.Web.UI.Page
    {
        public string memoNum = null, poNum = null, skuName = null, customerCode = null;
        public string totalQty = null, styleName = null, retailer = null;
        //public int requestId = 0;
        string loginName = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
            if (false/*Session["ID"].ToString()!="10"*/)
                Response.Redirect("Middle.aspx");
            loginName = (string)HttpContext.Current.Session["LoginName"];
            string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
            txtCustomerCode.Text = custCode;
            // ClearText();

        }//Page_Load

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //loginName = (string)HttpContext.Current.Session["LoginName"];
            customerCode = txtCustomerCode.Text.Trim();
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            styleName = txtStyle.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            int orderCount = IsOrderExist(txtMemoNum.Text);
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 2)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo already exists. Press UPDATE');", true);
            else if (orderCount == 0)//no order
            {
                //string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                
                    string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, styleName, retailer, this, 0, customerCode, null);
                    if (status == "failed")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                    }
                    else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                        if (status == "failed")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                            int requestId = GSIAppQueryUtils.GetRequestId(memoNum, Convert.ToInt16(customerCode), this);
                            txtRequestID.Text = requestId.ToString();
                            bool printed = PrintCustomerLabel(memoNum, poNum, skuName, totalQty, styleName, retailer, Convert.ToInt16(customerCode), requestId, this);
                            if (printed)
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                            else
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                            //ClearText();
                        }

                        //ClearText();
                    }
                
            }
        }//btnSave_Click
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            loginName = (string)HttpContext.Current.Session["LoginName"];
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            styleName = txtStyle.Text.Trim();
            retailer = txtRetailer.Text.Trim();

            int orderCount = IsOrderExist(memoNum);
            /*alex
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 0)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo does not exist. Press ADD NEW');", true);
            else if (orderCount == 2)//save record
            {
            alex */
            string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
            string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, styleName, retailer, this, 1, custCode, null);
                if (status == "failed")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                    //string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                int requestId = GSIAppQueryUtils.GetRequestId(memoNum, Convert.ToInt16(custCode), this);
                if (requestId == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Request ID not found. Contact administrator');", true);
                    return;
                }
                bool printed = PrintCustomerLabel(memoNum, poNum, skuName, totalQty, styleName, retailer, Convert.ToInt16(custCode), requestId, this);
                    if (printed)
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                    //ClearText();
                }
            //} alex
        }//btnUpdate_Click

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearText();
            memoNum = txtMemoNum.Text.Trim();
            loginName = (string)HttpContext.Current.Session["LoginName"];
            int orderCount = IsOrderExist(memoNum);
            if (orderCount > 0)
            {
                string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                DataSet ds = new DataSet();
                //bool found = false;
                ds = GSIAppQueryUtils.GetSyntheticRecordByMemo(memoNum, this);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    DataRow dr;
                    dr = ds.Tables[0].Rows[0];
                    txtPONum.Text = dr["PONum"].ToString();
                    txtSku.Text = dr["SKUName"].ToString();
                    txtQuantity.Text = dr["TotalQty"].ToString();
                    txtStyle.Text = dr["Style"].ToString();
                    txtRetailer.Text = dr["Retailer"].ToString();
                    txtRequestID.Text = dr["RequestID"].ToString();
                    txtCustomerCode.Text = dr["CustomerCode"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Record found.');", true);
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error finding record.');", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No order found.');", true);
            bool printed = PrintCustomerLabel(memoNum, poNum, skuName, totalQty, styleName, retailer, Convert.ToInt16(txtCustomerCode.Text), Convert.ToInt32(txtRequestID.Text), this);
            if (printed)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);

        }
            private int IsOrderExist(string memoNum)
        {
            return GSIAppQueryUtils.GetScreeningByMemo(memoNum, this);
        }

        protected void PrintOnClick(object sender, EventArgs e)
        {
            int requestId = Convert.ToInt32(txtRequestID.Text);
            customerCode = txtCustomerCode.Text.Trim();
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            styleName = txtStyle.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            bool printed = PrintCustomerLabel(memoNum, poNum, skuName, totalQty, styleName, retailer, Convert.ToInt16(customerCode), requestId, this);
        }

        private void ClearText()
        {
            //txtMemoNum.Text = string.Empty;
            txtPONum.Text = string.Empty;
            txtStyle.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtSku.Text = string.Empty;
            txtRetailer.Text = string.Empty;
        }
        private bool PrintCustomerLabel(string memoNum, string poNum, string skuName, string totalQty, string styleName, string retailer, int custCode, int requestId, Page p)
        {
            //string sReportName = @"c:\PrintLabel\Label_Synthetic.xlsx";
            string URL = "https://gdlightstorage.blob.core.windows.net/gdlight/temp/Label_Synthetic.xlsx";
            var webClient = new WebClient();
            var stream = webClient.OpenRead(URL);
            string sReportName = @"\\mercury\c$\PrintLabel\Label_Synthetic.xlsx";
            var _ms = new MemoryStream();
            stream.CopyTo(_ms);
            var temp = Path.GetTempPath(); // Get %TEMP% path
            var file = Path.GetFileNameWithoutExtension(Path.GetRandomFileName()); // Get random file name without extension
            var path = Path.Combine(temp, file + ".xlsx"); // Get random file path
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                // Write content of your memory stream into file stream
                _ms.WriteTo(fs);
            }
            Excel.Application excel = new Excel.Application();
            excel.Workbooks.Open(path, ReadOnly: true);
            Microsoft.Office.Interop.Excel._Workbook BookTemp = excel.Workbooks.Open(path, ReadOnly: true);
            //Microsoft.Office.Interop.Excel._Workbook BookTemp = Open_Excel(sReportName);
            Excel.Worksheet SheetData = (Excel.Worksheet)BookTemp.Sheets[1];
            //Excel.Worksheet SheetData1 = (Excel.Worksheet)BookTemp.Sheets[2];

            Excel.Range crCell = null;
            //string sTitleColumn = "b";
            //string sDataColumn = "c";
            //crCell = SheetData.get_Range("B2:C9", Type.Missing);
            //crCell.Cells.Value2 = "";
            //int i1 = 0;
            //int iBaseCellRow = 2;
            try
            {
                //int count = 0;//ds.Tables[0].Columns.Count;
                crCell = SheetData.get_Range("g20", Type.Missing);
                crCell.Cells.Value2 = custCode;
                crCell = SheetData.get_Range("g22", Type.Missing);
                crCell.Cells.Value2 = memoNum;
                crCell = SheetData.get_Range("g24", Type.Missing);
                crCell.Cells.Value2 = poNum;
                crCell = SheetData.get_Range("g26", Type.Missing);
                crCell.Cells.Value2 = skuName;
                crCell = SheetData.get_Range("g28", Type.Missing);
                crCell.Cells.Value2 = styleName;
                crCell = SheetData.get_Range("g30", Type.Missing);
                crCell.Cells.Value2 = retailer;
                crCell = SheetData.get_Range("g32", Type.Missing);
                crCell.Cells.Value2 = totalQty;
                //crCell = SheetData.get_Range("h21", Type.Missing);
                //crCell.Cells.Value2 = DateTime.Now.ToString();
                crCell = SheetData.get_Range("d16", Type.Missing);
                //crCell.Cells.Value2 = @"*" + memoNum.ToUpper() + @"*";
                crCell.Cells.Value2 = @"*" + requestId.ToString() + @"*";


                


                //string sPrinterName = @"\\GSI-NY-18\Elizabeth HP Printer";//ConfigurationManager.AppSettings["PrinterName"].ToString();
                //#if DEBUG
                //string myFile = @"C:\Users\aremennik\Documents\Visual Studio 2010\Projects\NewCorpt_merge0803\" + memoNum.ToString() + ".pdf";
                //string myFile = @"\\mercury\c$\Temp\" + memoNum.ToString() + ".pdf";
                //string myFile = @"Images/" + memoNum.ToString() + ".pdf";
                //if (File.Exists(myFile)) File.Delete(myFile);
                //if (File.Exists(myFile))
                //    myFile = @"\\mercury\c$\Temp\" + memoNum.ToString() + "-1.pdf";
                //Worksheet sheet = workbook.Worksheets[1];
                //sheet.SaveToImage(@"c:\temp.jpg");
                //if (File.Exists(@"c:\temp\tst.xlsx")) File.Delete(@"c:\temp\tst.xlsx");
                //SheetData.SaveAs(@"c:\temp\tst.xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //BookTemp.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, myFile);
                //BookTemp.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, myFile, Excel.XlFixedFormatQuality.xlQualityStandard,
                BookTemp.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, null, Excel.XlFixedFormatQuality.xlQualityStandard,
        true,
        true,
        1,
        10,
        true);
                //SheetData.PrintOut(1, 1, 1, false, sPrinterName, Type.Missing, true, Type.Missing);
                //#else
                //SheetData.PrintOut(1, 1, 1, false, sPrinterName, Type.Missing, true, Type.Missing);               
                //#endif

                BookTemp.Close(false, sReportName, null);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }//PrintCustomerLabel

        private Excel._Workbook Open_Excel(string MyReport)
        {
            try
            {
                if (MyReport.Trim() != "")
                {
                    Excel.Application objExcel;
                    objExcel = new Excel.Application();
                    Excel._Workbook BookTemp = objExcel.Workbooks.Open(MyReport, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    return BookTemp;
                }
                else
                {
                    //writeFile.WriteLine("Cannot open Excel book for blank MyReport");
                    return null;
                }
            }
            catch (Exception ex)
            {
                //writeFile.WriteLine("Excel book problem for " + MyReport);
                //writeFile.WriteLine(ex);
                return null;
            }
        }//Open_Excel
    }
}