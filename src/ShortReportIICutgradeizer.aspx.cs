using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for ShortReportIICutgradeizer.
	/// </summary>
	public partial class ShortReportIICutgradeizer : CommonPage
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
			{
				Response.Redirect("Login.aspx");
			}
			else
			{
                ShowReport();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	    private DataTable getTable()
	    {
	        
            return null;
	    }

		private void ShowReport()
		{
            var dtIncoming = Utlities.GetActiveShortReport(this);
            if (dtIncoming == null) return;
			dgResult.DataSource = dtIncoming;
			dgResult.DataBind();
			
			var ds = new DataSet {DataSetName = "Collected CutgradeResults"};

		    if(dtIncoming.Columns.IndexOf("Cut Grade") > -1)
			{
				foreach(DataRow dr in dtIncoming.Rows)
				{
					if(!dr["Cut Grade"].ToString().EndsWith("Ideal</a>"))
					{
						var strFillLink = dr["Cut Grade"].ToString();
                        var s = strFillLink.Split('?')[1];
                        strFillLink = s.Split('"')[0];
						
						var strParams = strFillLink.Split('&');

						var strGroupCode = strParams[0].Split('=')[1];
						var strBatchCode = strParams[1].Split('=')[1];
						var strItemCode = strParams[2].Split('=')[1];

						try
						{
							ds.Tables.Add(GetCutgrade(strGroupCode, strBatchCode, strItemCode));
						}
						catch(Exception ex)
						{
							Console.WriteLine(ex.Message);
						}

                        lstPrepped.Items.Add(strGroupCode+":"+strBatchCode+":"+strItemCode);

						lstFailedCutGrades.Items.Add(dr["Cut Grade"].ToString());
					}
				}
			}

			if(ds.Tables.Count > 0)
			{
				var cgdt = new DataTable();
				foreach(DataColumn dc in ds.Tables[0].Columns)
				{
					cgdt.Columns.Add(new DataColumn(dc.ColumnName));
				}
				foreach(DataTable dt in ds.Tables)
				{
					foreach(DataRow dr in dt.Rows)
					{
						cgdt.Rows.Add(dr.ItemArray);
					}
				}

				dgCutgrade.DataSource=cgdt;
				dgCutgrade.DataBind();

                var dtBatchList = Session[SessionConstants.BatchList] as DataTable;

				string strShortReportId;					
				Utils.IsShortReport(dtBatchList.Rows[0]["BatchID"].ToString(),this, out strShortReportId);

				var command = new SqlCommand("spGetDocumentValue")
				{
				    Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString()),
				    CommandType = CommandType.StoredProcedure
				};

			    command.Parameters.AddWithValue("@DocumentID",Int32.Parse(strShortReportId));
				command.Parameters.AddWithValue("@AuthorID", Session["ID"]);
				command.Parameters.AddWithValue("@AuthorOfficeID", Session["AuthorOfficeID"]);
				
				var daShrp = new SqlDataAdapter(command);
                
				var dtShortReportStructure = new DataTable();
				daShrp.Fill(dtShortReportStructure);

				dgBatchList.DataSource=dtShortReportStructure;
				dgBatchList.DataBind();

				var dtPrettyResult = PrettyResult(dtIncoming, dtShortReportStructure, cgdt);
				
				dgPrettyReport.DataSource = dtPrettyResult;
				dgPrettyReport.DataBind();

				Session[SessionConstants.ShortReportPrettyTable] = dtPrettyResult;
			}
		}
		private string GetShortReportHeader(string strValue, DataTable dtShortReportStructure)
		{
			foreach(DataRow dr in dtShortReportStructure.Rows)
			{
				string strTempValue = dr["Value"].ToString();
				if(strTempValue.IndexOf(strValue)>-1)
				{
					return dr["Title"].ToString();
				}
			}
			return "";
		}

		private DataTable PrettyResult(DataTable dtIncoming, DataTable dtShortReportStructure, DataTable dtCutGradeList)
		{

			dgCutGradeDebug.DataSource= dtCutGradeList;
			dgCutGradeDebug.DataBind();

			var dtResult = dtIncoming.Copy();

			foreach(DataRow drResult in dtResult.Rows)
			{
				string strItemNumber = drResult["Item #"].ToString();

				var drsCg = dtCutGradeList.Select("[Pass?] = 'No' and [OldItemNumber] = '"+strItemNumber+"'");
				if(drsCg.Length > 0)
				{
					foreach(DataRow drCg in drsCg)
					{
						var strValue = drCg["MeasureName"].ToString();
						var strShortReportHeader = GetShortReportHeader(strValue, dtShortReportStructure);
						if(strShortReportHeader.Length>0)
						{
							var strExisitngValue = drResult[strShortReportHeader].ToString();
							if(strExisitngValue.StartsWith("<SPAN class=text_highlitedyellow>"))
							{
							}
							else
							{
								var strNewValue = "<SPAN class=text_highlitedyellow>"+strExisitngValue+"</SPAN>";
								drResult[strShortReportHeader]=strNewValue;
							}
						}
					}
				}

			}

			return dtResult;
		}

		private string GetOldItemNumber(string strGroupCode, string strBatchCode, string strItemCode)
		{
			SqlCommand command = new SqlCommand("oleg_sp_GetOldITemNumberByNewNumber");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@GroupCode", int.Parse(strGroupCode)));
			command.Parameters.Add(new SqlParameter("@BatchCode", int.Parse(strBatchCode)));
			command.Parameters.Add(new SqlParameter("@ItemCode", int.Parse(strItemCode)));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();

			da.Fill(dt);

			string res = "";

			res = Utils.FullItemNumber(dt.Rows[0]["PrevGroupCode"].ToString(), dt.Rows[0]["PrevBatchCode"].ToString(),dt.Rows[0]["PrevItemCode"].ToString());
			return res;
		}

		private DataTable GetCutgrade(string strGroupCode, string strBatchCode, string strItemCode)
		{			
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			SqlCommand command = new SqlCommand("sp_GetCutGrade");
			command.CommandType=CommandType.StoredProcedure;
			command.Connection=conn;
			/*@OrderCode int,
			@BatchCode int,
			@ItemCode int,*/
			try
			{
				command.Parameters.Add("@OrderCode",SqlDbType.Int);
				command.Parameters["@OrderCode"].Value=Int32.Parse(strGroupCode);
		
				command.Parameters.Add("@BatchCode",SqlDbType.Int);
				command.Parameters["@BatchCode"].Value=Int32.Parse(strBatchCode);

				command.Parameters.Add("@ItemCode",SqlDbType.Int);
				command.Parameters["@ItemCode"].Value=Int32.Parse(strItemCode);

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dt = new DataTable();
				da.Fill(dt);
				
				//"fix" the result
				dt.Columns.Add(new DataColumn("ItemNumber"));
				dt.AcceptChanges();
				foreach(DataRow dr in dt.Rows)
				{
					dr["ItemNumber"]=Utils.FullItemNumber(strGroupCode, strBatchCode,strItemCode);
				}

                dt.Columns.Add(new DataColumn("OldItemNumber"));
				dt.AcceptChanges();
				foreach(DataRow dr in dt.Rows)
				{
					dr["OldItemNumber"]=GetOldItemNumber(strGroupCode, strBatchCode, strItemCode);
				}					
                

				//deal with non-gsi cutgrade
				DataTable dtCutGradeCopy = dt.Copy();
				//dtCutGradeCopy.Rows.Clear();
				//dtCutGradeCopy.AcceptChanges();

				for(int i = dt.Rows.Count - 1; i > 0; i--)
				{
					if((dt.Rows[i]["MeasureName"].ToString()=="CutGrade Type") && (dt.Rows[i]["Pass?"].ToString()=="No"))
					{
						var strCutGradeCategoryToKill = dt.Rows[i]["Comment"].ToString();
						for(int l = 0; l < dt.Rows.Count; l++)
						{
							try
							{
								if(dt.Rows[l]["Comment"].ToString() == strCutGradeCategoryToKill)
								{
									//dt.Rows[l].Delete();
									dtCutGradeCopy.Rows[l].Delete();
								}
							}
							catch(Exception ex)
							{
								Console.WriteLine(ex.Message);
							}
						}
					}
				}
				
				dtCutGradeCopy.AcceptChanges();
				string strTableName = dt.TableName;
				dt = dtCutGradeCopy.Copy();
				dt.TableName = strTableName;
				
				dt.AcceptChanges();
				//end deal with non-gsi cutgrade 


				//leave only ideal fail 
				string strComment;
				strComment = dt.Rows[0]["Comment"].ToString();
				for(int i=1;i<dt.Rows.Count;i++)
				{
					if(dt.Rows[i]["Comment"].ToString()!=strComment)
					{
						dt.Rows[i].Delete();
					}
				}
				dt.AcceptChanges();
				//end leave only ideal fail 

				return dt;			
			}
			
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			return null;
		}

		protected void CmdSaveExcelClick(object sender, EventArgs e)
		{
			var ds = new DataSet();
			ds.Tables.Add(Session["PrettyResult"] as DataTable);
            ExcelUtils.ExportThrouPoi(ds, DateTime.Now.Ticks.ToString(), this);
		}

	    protected void OnExcelClick(object sender, ImageClickEventArgs e)
	    {
	        

	    }
	}
}
