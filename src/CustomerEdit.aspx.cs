﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CustomerEdit : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Customer Maintenance";
            if (IsPostBack)
            {
                Page.Validate(ValidatorGroupCompany);
                Page.Validate(ValidatorGroupPerson);
                return;
            }
            //-- Customers List
            LoadCustomers();

            //-- US States
            var states = QueryCustomerUtils.GetUsStates(this);
            CompanyUsState.DataSource = states;
            CompanyUsState.DataBind();

            PersonUsState.DataSource = states;
            PersonUsState.DataBind();

            //-- Business Type
            BusinessType.DataSource = QueryCustomerUtils.GetBusinessTypes(this);
            BusinessType.DataBind();

            //-- IndustryMembers
            IndustryMembers.DataSource = QueryCustomerUtils.GetIndustryMemberships(this);
            IndustryMembers.DataBind();

            //-- Permissions
            CompanyPermissionsList.DataSource = new PermissionsModel().Items;
            CompanyPermissionsList.DataBind();

            PersonPermissionsList.DataSource = new PermissionsModel().Items;
            PersonPermissionsList.DataBind();

            //-- Goods Movement
            MovementsList.DataSource = new GoodsMovementModel().Items;
            MovementsList.DataBind();

            //-- Carriers
            CarrierList.DataSource = QueryCustomerUtils.GetCarriers(this);
            CarrierList.DataBind();

            //-- Preferred Methods of Communication
            SetTreeMethodValue("f0p0e0m0", CompanyMethods);
            SetTreeMethodValue("f0p0e0m0", PersonMethods);

            //-- Person Position Types
            PersonPosition.DataSource = QueryCustomerUtils.GetPositions(this);
            PersonPosition.DataBind();

            CompanyDetailsPanel.Visible = false;
            PersonListPanel.Visible = false;
            PersonDetailsPanel.Visible = false;

            PersonUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this person information?');");
            PersonDeleteBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this person information?');");
            CustomerUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this customer information?');");

            CustomerLike.Focus();

        }

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel{CustomerId = "", CustomerName = ""});
            customers.Sort((m1,m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
            lstCustomerList.SelectedIndex = 0;
        }

        #endregion

        #region Search Customer
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            HideCustomerDetails();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (filtered.Count == 1) lstCustomerList.SelectedValue = filtered[0].CustomerId;
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            HideCustomerDetails();
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;
            
            var customer = GetCustomerFromView(lstCustomerList.SelectedValue);
            if (customer == null) return;
            HistoryRef.HRef = "~/CustomerHistory.aspx?CustomerCode=" + customer.CustomerCode;
            var infoModel = QueryCustomerUtils.GetCustomer(customer, this);

            SetViewState(infoModel, SessionConstants.CustomerEditData);
            if (infoModel == null) return;
            
            SetCompanyInfo(infoModel);
            
        }
        private CustomerModel GetCustomerFromView(string customerId)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
                            new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);
        }
        private void HideCustomerDetails()
        {
            MessageLabel.Text = "";
            CompanyDetailsPanel.Visible = PersonDetailsPanel.Visible = PersonListPanel.Visible = false;
        }
        #endregion

        #region Company Details Panel
        private CustomerEditModel GetCompanyInfo()
        {
            var customerModel = GetViewState(SessionConstants.CustomerEditData) as CustomerEditModel ??
                                    new CustomerEditModel();
            
            var infoModel = new CustomerEditModel();
            infoModel.StateId = customerModel.StateId;
            infoModel.StateTargetId = customerModel.StateTargetId;
            infoModel.CustomerOfficeId = customerModel.CustomerOfficeId;
            infoModel.CustomerHistoryId = customerModel.CustomerHistoryId;
            infoModel.CustomerId = customerModel.CustomerId;
            infoModel.CustomerCode = customerModel.CustomerCode;
            infoModel.Company.Address.AddressId = customerModel.Company.Address.AddressId;

            infoModel.Company.CompanyName = CompanyName.Text.Trim();
            infoModel.Company.ShortName = ShortName.Text.Trim();
            infoModel.Company.Address.Address1 = CompanyAddress1.Text.Trim();
            infoModel.Company.Address.Address2 = CompanyAddress2.Text.Trim();

            infoModel.Company.Address.Zip1 = CompanyZip1.Text.Trim();
            infoModel.Company.Address.Zip2 = CompanyZip2.Text.Trim();
            infoModel.Company.Address.City = CompanyCity.Text.Trim();
            infoModel.Company.Address.Country = CompanyCountry.Text.Trim();
            if (CompanyCountry.Text.Trim() == "USA")
            {
                infoModel.Company.Address.UsStateId = CompanyUsState.SelectedValue;
            }

            infoModel.Company.Address.CountryPhoneCode = CompanyPhoneCode.Text;
            infoModel.Company.Address.Phone = CompanyPhone.Text.Trim();

            infoModel.Company.Address.CountryFaxCode = CompanyFaxCode.Text;
            infoModel.Company.Address.Fax = CompanyFax.Text.Trim();

            infoModel.Company.Address.Email = CompanyEmail.Text.Trim();
            infoModel.Company.Communication = GetTreeMethodValue(CompanyMethods);
            infoModel.Company.Permissions = GetCheckBoxListValue(CompanyPermissionsList);
            infoModel.Company.IndustryMembership = GetCheckBoxListValue(IndustryMembers);

            infoModel.Company.BusinessTypeId = BusinessType.SelectedValue;
            infoModel.Company.GoodsMovement = GetGoodsMovement();
            infoModel.Company.UseTheirAccount = UseAccountFlag.Checked;
            infoModel.Company.Account = AccountField.Text.Trim();
            infoModel.Company.CarrierId = CarrierList.SelectedValue;
            return infoModel;
        }
        private void SetCompanyInfo(CustomerEditModel infoModel)
        {

            CompanyDetailsPanel.Visible = true;
            
            CompanyName.Text = infoModel.Company.CompanyName;
            ShortName.Text = infoModel.Company.ShortName;
            CompanyAddress1.Text = infoModel.Company.Address.Address1;
            CompanyAddress2.Text = infoModel.Company.Address.Address2;
            CompanyCity.Text = infoModel.Company.Address.City;
            CompanyCountry.Text = infoModel.Company.Address.Country;
            if (!string.IsNullOrEmpty(infoModel.Company.Address.UsStateId))
            {
                CompanyUsState.SelectedValue = infoModel.Company.Address.UsStateId;
            } else
            {
                CompanyUsState.SelectedIndex = -1;
            }
            OnCompanyCountryChanging(null, null);
            CustomerCode.Text = infoModel.CustomerCode;
            CustomerId.Text = infoModel.CustomerId;
            CustomerStartDate.Text = String.Format("{0:G}", infoModel.CreateDate);
            if (!string.IsNullOrEmpty(infoModel.Company.BusinessTypeId))
            {
                BusinessType.SelectedValue = infoModel.Company.BusinessTypeId;
            } else
            {
                BusinessType.SelectedIndex = -1;
            }
            
            LoadCheckBoxList(infoModel.Company.IndustryMembership, IndustryMembers);
            
            CompanyZip1.Text = infoModel.Company.Address.Zip1;
            CompanyZip2.Text = infoModel.Company.Address.Zip2;

            CompanyPhoneCode.Text = infoModel.Company.Address.CountryPhoneCode;
            CompanyPhone.Text =infoModel.Company.Address.Phone;

            CompanyFaxCode.Text = infoModel.Company.Address.CountryFaxCode;
            CompanyFax.Text = infoModel.Company.Address.Fax;
            CompanyEmail.Text = infoModel.Company.Address.Email;

            LoadCheckBoxList(infoModel.Company.Permissions, CompanyPermissionsList);

            SetGoodsMovement(infoModel.Company.GoodsMovement);
            UseAccountFlag.Checked = infoModel.Company.UseTheirAccount;
            OnCheckedMovement(null, null);

            if (!string.IsNullOrEmpty(infoModel.Company.CarrierId))
            {
                CarrierList.SelectedValue = infoModel.Company.CarrierId;
            } else
            {
                CarrierList.SelectedIndex = -1;
            }
            AccountField.Text = infoModel.Company.Account;
            SetTreeMethodValue(infoModel.Company.Communication, CompanyMethods);

            //-- Load Persons List
            PersonsGrid.DataSource = infoModel.Persons;
            PersonsGrid.DataBind();
            PersonListPanel.Visible = true;

            if (infoModel.Persons.Count > 0)
            {
                SetPersonInfo(infoModel.Persons[0]);
            }
            NewPersonButton.Enabled = !string.IsNullOrEmpty(infoModel.CustomerId);
            Page.Validate("ValGrpCompany");
        }
        #endregion

        #region Company Country
        protected void OnCompanyCountryChanging(object sender, EventArgs e)
        {
            var isUsa = CompanyCountry.Text.Trim().ToUpper() == "USA";
            CompanyUsState.Enabled = isUsa;
            CompUsStateValidator.Enabled = isUsa;
        }
        #endregion

        #region CheckBoxList Utils
        private void LoadCheckBoxList(string values, CheckBoxList control)
        {
            foreach (ListItem item in control.Items)
            {
                item.Selected = false;
            }
            if (string.IsNullOrEmpty(values)) return;
            var keys = values.Split(',');
            foreach (ListItem item in control.Items)
            {
                
                foreach (var key in keys)
                {
                    if (key == item.Value) item.Selected = true;
                }
            }
        }
        private string GetCheckBoxListValue(CheckBoxList control)
        {
            var value = "";
            foreach (ListItem item in control.Items)
            {
                if (item.Selected) value += item.Value + ",";
            }
            return value;
        }
        #endregion

        #region Goods Movement
        private void SetGoodsMovement(GoodsMovementModel movement)
        {
            foreach (ListItem item in MovementsList.Items)
            {
                item.Selected = false;
            }
            if (movement.WeCarry) MovementsList.SelectedValue = "0";
            if (movement.TheyCarry) MovementsList.SelectedValue = "1";
            if (movement.WeShipCarry) MovementsList.SelectedValue = "2";
            HiddenMovements.Text = movement.HasChecked ? "Y" : "";
        }
        protected void OnCheckedMovement(object sender, EventArgs e)
        {
            var isShip = (MovementsList.SelectedValue == "2");
            UseAccountFlag.Enabled = isShip;
            if (!isShip)
            {
                UseAccountFlag.Checked = false;
            }
            OnUseAccountChecked(null, null);

            HiddenMovements.Text = GetGoodsMovement().HasChecked ? "Y" : "";
        }
        private GoodsMovementModel GetGoodsMovement()
        {
            var movement = new GoodsMovementModel();
            foreach(ListItem item in MovementsList.Items)
            {
                if (!item.Selected) continue;
                if (item.Value == "0") movement.WeCarry = true;
                if (item.Value == "1") movement.TheyCarry = true;
                if (item.Value == "2") movement.WeShipCarry = true;
            }
            return movement;
        }
        #endregion

        #region Use Account Flag
        protected void OnUseAccountChecked(object sender, EventArgs e)
        {
            var check = UseAccountFlag.Checked;
            AccountField.Enabled = check;
            CarrierList.Enabled = check;
            CarrierValidator.Enabled = check;
            AccountValidator.Enabled = check;
            if (!check)
            {
                AccountField.Text = "";
                CarrierList.SelectedIndex = -1;
            }
        }
        #endregion

        #region Company: Methods of Communication
        protected void OnCompanyMethodsCheckChanged(object sender, TreeNodeEventArgs e)
        {
            OnTreeMethodCheck(CompanyMethods);
        }

        protected void OnCompanyDownClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(false, CompanyMethods);
        }

        protected void OnCompanyUpClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(true, CompanyMethods);
        }
        #endregion

        #region Person: Methods of Communication
        protected void OnPersonMethodsCheckChanged(object sender, TreeNodeEventArgs e)
        {
            OnTreeMethodCheck(PersonMethods);
        }

        protected void OnPersonDownClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(false, PersonMethods);
        }

        protected void OnPersonUpClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(true, PersonMethods);
        }
        #endregion

        #region TreeView Methods Utils
        private static void LoadTreeMethods(List<CommunicationModel> items, TreeView tree)
        {
            items.Sort((m1, m2) => m1.Order.CompareTo(m2.Order));
            tree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Communications" } };
            data.AddRange(items.Select(item => new TreeViewModel
            {
                ParentId = "0",
                Id = item.Code,
                DisplayName = item.Name,
            }));

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tree.Nodes.Add(rootNode);
            foreach (TreeNode node in rootNode.ChildNodes)
            {
                var src = items.Find(m => m.Code == node.Value && m.Checked);
                node.Checked = src != null;
            }

        }
        private static void ReorderMethods(bool up, TreeView tree)
        {
            if (tree.SelectedNode == null || tree.SelectedNode.Value == "0") return;
            var nowInd = 0;
            var items = new List<CommunicationModel>();
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                if (node.Value == tree.SelectedValue) nowInd = i;
                items.Add(new CommunicationModel { Checked = node.Checked, Code = node.Value, Name = node.Text, Order = i });
            }
            if (up && nowInd == 0) return;
            if (!up && nowInd == (items.Count - 1)) return;
            var newInd = up ? (nowInd - 1) : (nowInd + 1);
            var item1 = items.Find(m => m.Order == nowInd);
            var item2 = items.Find(m => m.Order == newInd);
            if (item1 != null) item1.Order = newInd;
            if (item2 != null) item2.Order = nowInd;
            LoadTreeMethods(items, tree);
            //tree.Focus();
            tree.Nodes[0].ChildNodes[newInd].Selected = true;
        }
        private static void OnTreeMethodCheck(TreeView tree)
        {
            if (tree.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tree.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        private static string GetTreeMethodValue(TreeView tree)
        {
            var result = "";
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                result += node.Value + (node.Checked ? "1" : "0");
            }
            return result;
        }
        private static void SetTreeMethodValue(string value, TreeView tree)
        {
            var communications = new CommunicationsModel();
            communications.SetDbValue(value);
            LoadTreeMethods(communications.Items, tree);
        }
        #endregion

        #region Persons
        protected void OnAddPersonClick(object sender, EventArgs e)
        {
            var customer = GetCustomerShort();

            var person = new PersonExModel { Customer = customer };
            SetPersonInfo(person);
            PersonFirstName.Focus();
        }

        private CustomerModel GetCustomerShort()
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            var customer = customers.Find(m => m.CustomerId == CustomerId.Text);
            if (customer != null) return customer;
            //-- Refresh short Customer List
            LoadCustomers();
            lstCustomerList.SelectedValue = CustomerId.Text;
            customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == CustomerId.Text) ?? new CustomerModel();
        }
        protected void OnPersonItemDataBound(object sender, DataGridItemEventArgs e)
        {
        }
        protected void OnViewPersonCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + PersonsGrid.DataKeys[e.Item.ItemIndex]; 
            //-- Load Person Details Panel
            var customer = GetViewState(SessionConstants.CustomerEditData) as CustomerEditModel ??
                           new CustomerEditModel();
            var person = customer.Persons.Find(m => ("" + m.PersonId) == uniqueKey);
            if (person != null) SetPersonInfo(person);
        }
        #endregion

        #region Person Country
        protected void OnPersonCountryChanging(object sender, EventArgs e)
        {
            PersonUsState.Enabled = PersonCountry.Text.Trim().ToUpper() == "USA";

        }
        #endregion

        #region Person Details Panel
        private PersonExModel GetPersonInfo()
        {
            var exists = !string.IsNullOrEmpty(PersonId.Text);
            var infoModel = new PersonExModel();
            if (exists)
            {
                var customerModel = GetViewState(SessionConstants.CustomerEditData) as CustomerEditModel ??
                                    new CustomerEditModel();
                var person = customerModel.Persons.Find(m => m.PersonCode == PersonId.Text);
                if (person != null)
                {
                    infoModel.PersonId = person.PersonId;
                    infoModel.PersonHistoryId = person.PersonHistoryId;
                    infoModel.Address.AddressId = person.Address.AddressId;
                    infoModel.CreateDate = person.CreateDate;
                    infoModel.WebLogin = person.WebLogin;
                }
            }

            infoModel.WebLogin.LoginName = WebLogin.Text.Trim();
            infoModel.WebLogin.Password = WebPassword.Text.Trim();
            infoModel.Customer = GetCustomerShort();

            infoModel.PersonCode = PersonId.Text;
            infoModel.FirstName = PersonFirstName.Text.Trim();
            infoModel.LastName = PersonLastName.Text.Trim();
            if (CalendarExt.SelectedDate != null)
            {
                infoModel.BirthDate = CalendarExt.SelectedDate;
            }
            infoModel.Address.Address1 = PersonAddress1.Text.Trim();
            infoModel.Address.Address2 = PersonAddress2.Text.Trim();
            infoModel.Address.Zip1 = PersonZip1.Text.Trim();
            infoModel.Address.Zip2 = PersonZip2.Text.Trim();
            infoModel.Address.City = PersonCity.Text.Trim();
            infoModel.Address.Country = PersonCountry.Text.Trim();
            if (infoModel.Address.IsUsa)
            {
                infoModel.Address.UsStateId = PersonUsState.SelectedValue;
            }
            infoModel.Address.CountryPhoneCode = PersonPhoneCode.Text;
            infoModel.Address.Phone = PersonPhone.Text.Trim();
            infoModel.Address.ExtPhone = PersonPhoneExt.Text.Trim();

            infoModel.Address.CountryFaxCode = PersonFaxCode.Text;
            infoModel.Address.Fax = PersonFax.Text.Trim();

            infoModel.Address.CountryCellCode = PersonCellCode.Text;
            infoModel.Address.Cell = PersonCell.Text.Trim();

            infoModel.Address.Email = PersonEmail.Text.Trim();
            infoModel.Communication = GetTreeMethodValue(PersonMethods);
            infoModel.Permissions = GetCheckBoxListValue(PersonPermissionsList);
            infoModel.Position = new PositionModel{Id = PersonPosition.SelectedValue};
            return infoModel;
        }
        private void SetPersonInfo(PersonExModel infoModel)
        {
            PersonDeleteBtn.Enabled = infoModel.PersonId > 0;
            SetViewState(infoModel, SessionConstants.PersonEditData);
            PersonDetailsPanel.Visible = true;
            PersonFirstName.Text = infoModel.FirstName;
            PersonLastName.Text = infoModel.LastName;
            if (infoModel.BirthDate != null)
            {
                PersonBirthDate.Text = String.Format("{0:m/d/yyyy}", infoModel.BirthDate);
                CalendarExt.SelectedDate = infoModel.BirthDate;
            } else
            {
                PersonBirthDate.Text = "";
            }
            PersonAddress1.Text = infoModel.Address.Address1;
            PersonAddress2.Text = infoModel.Address.Address2;
            
            PersonCity.Text = infoModel.Address.City;
            
            PersonCountry.Text = infoModel.Address.Country;
            if (!string.IsNullOrEmpty(infoModel.Address.UsStateId))
            {
                PersonUsState.SelectedValue = infoModel.Address.UsStateId;
            } else
            {
                PersonUsState.SelectedIndex = -1;
            }
            OnPersonCountryChanging(null, null);
            PersonId.Text = "" + infoModel.PersonCode;
            PersonStartDate.Text = String.Format("{0:G}", infoModel.CreateDate);

            PersonZip1.Text = infoModel.Address.Zip1;
            PersonZip2.Text = infoModel.Address.Zip2;

            PersonPhoneCode.Text = infoModel.Address.CountryPhoneCode;
            PersonPhone.Text = infoModel.Address.Phone;
            PersonPhoneExt.Text = infoModel.Address.ExtPhone;

            PersonFaxCode.Text = infoModel.Address.CountryFaxCode;
            PersonFax.Text = infoModel.Address.Fax;

            PersonCellCode.Text = infoModel.Address.CountryCellCode;
            PersonCell.Text = infoModel.Address.Cell;

            PersonEmail.Text = infoModel.Address.Email;

            LoadCheckBoxList(infoModel.Permissions, PersonPermissionsList);
            SetTreeMethodValue(infoModel.Communication, PersonMethods);

            if (!string.IsNullOrEmpty(infoModel.Position.Name))
            {
                PersonPosition.SelectedValue = "" + infoModel.Position.Id;
            }
            else
            {
                PersonPosition.SelectedIndex = -1;
            }
            AsCompanyAddress.Checked = false;
            OnAsCompanyChecked(null, null);

            WebLogin.Text = infoModel.WebLogin.LoginName;
            WebPassword.Text = infoModel.WebLogin.Password;
            Page.Validate("ValGrpPerson");
        }
        #endregion

        #region Person Birth Date
        protected void OnChangedBirthDate(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (PersonBirthDate.Text != "")
            {
                try
                {
                    date = DateTime.Parse(PersonBirthDate.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExt.SelectedDate = date;
        }
        #endregion

        #region Same As Company Address
        protected void OnAsCompanyChecked(object sender, EventArgs e)
        {
            var asCompany = AsCompanyAddress.Checked;
            PersonAddress1.Enabled = PersonAddress2.Enabled = PersonCity.Enabled = PersonCountry.Enabled = PersonUsState.Enabled = !asCompany;
            if (!asCompany) return;
            PersonAddress1.Text = CompanyAddress1.Text;
            PersonAddress2.Text = CompanyAddress2.Text;
            PersonCity.Text = CompanyCity.Text;
            PersonCountry.Text = CompanyCountry.Text;
            PersonUsState.SelectedValue = CompanyUsState.SelectedValue;
        }
        #endregion

        #region Refresh Persons List after Update, Delete
        private void RefreshCompanyInfo(string selPersonId)
        {
            var customerModel = QueryCustomerUtils.GetCustomer(GetCustomerShort(), this);
            SetCompanyInfo(customerModel);
            if (selPersonId == "0" || selPersonId== "") return;
            var person = customerModel.Persons.Find(m => m.PersonId == Convert.ToInt32(selPersonId));
            if (person != null) SetPersonInfo(person);
        }
        #endregion

        #region Validators

        private const string ValidatorGroupCompany = "ValGrpCompany";
        private const string ValidatorGroupPerson = "ValGrpPerson";

        protected bool IsGroupValid(string sValidationGroup)
        {
            Page.Validate(sValidationGroup);
            foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
            {
                if (!validator.IsValid)
                {
                    return false;
                }
            }
            return true;
        }
        private string GetErrMessage(string sValidationGroup)
        {
            var msg = "";
            foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
            {
                if (!validator.IsValid)
                {
                    msg += "<br/>" + validator.ToolTip;
                }
            }
            return msg;
        }
        #endregion

        #region Save Company, Person
        private string GetErrMessageOnUpdateCompany()
        {
            var msg = "";
            if (!RequiredFieldValidatorMovements.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorMovements.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyName.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyName.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyShortName.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyShortName.ToolTip;
            }
            if (!RequiredFieldValidatorAddr1.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorAddr1.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyCity.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyCity.ToolTip;
            }
            if (!CompUsStateValidator.IsValid)
            {
                msg += "<br/>" + CompUsStateValidator.ToolTip;
            }
            if (!RequiredFieldValidatorZip.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorZip.ToolTip;
            }
            if (!RequiredFieldValidator1BusinessType.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidator1BusinessType.ToolTip;
            }
            if (!CarrierValidator.IsValid)
            {
                msg += "<br/>" + CarrierValidator.ToolTip;
            }
            if (!AccountValidator.IsValid)
            {
                msg += "<br/>" + AccountValidator.ToolTip;
            }

            return msg;
        }
        protected void OnUpdateCompanyClick(object sender, EventArgs e)
        {
            MessageLabel.Text = "";
            Page.Validate(ValidatorGroupCompany);
            if (!IsGroupValid(ValidatorGroupCompany))
            {
                PopupInfoDialog(GetErrMessage(ValidatorGroupCompany), true);
                return;
            }
            var customerInfo = GetCompanyInfo();
            var msg = QueryCustomerUtils.UpdateCompany(customerInfo, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true);
               // MessageLabel.Text = msg;
                return;
            }
            CustomerId.Text = customerInfo.CustomerId;
            RefreshCompanyInfo("0");
            MessageLabel.Text = "Customer was saved successfully";
            //PopupInfoDialog("Customer was saved successfully", false);
        }

        protected void OnUpdatePersonClick(object sender, EventArgs e)
        {

            UpdatePersonLabel.Text = "";
            Page.Validate(ValidatorGroupPerson);
            if (!IsGroupValid(ValidatorGroupPerson))
            {
                PopupInfoDialog(GetErrMessage(ValidatorGroupPerson), true);
                return;
            }
            
            if (WebPassword.Text != RePassword.Text)
            {
                PopupInfoDialog("Password doesn't match retyped password. <br/>Please, type and retype again.", true);
                //UpdatePersonLabel.Text = "Password doesn't match retyped password. Please, type and retype again.";
                return;
            }
            var personInfo = GetPersonInfo();
            if (QueryCustomerUtils.IsWebLoginExists(personInfo.WebLogin, this))
            {
                PopupInfoDialog("Person with this login and password already exists.", true);
                //UpdatePersonLabel.Text = "Person with this login and password already exists.";
                return;
            }
            
            var msg = QueryCustomerUtils.UpdatePerson(personInfo, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true);
                //UpdatePersonLabel.Text = msg;
                return;
            } 
            //-- Refresh Persons List

            //PopupInfoDialog("Person was saved successfully", false);
            RefreshCompanyInfo("" + personInfo.PersonId);
            UpdatePersonLabel.Text = "Person was saved successfully!";

        }
        protected void OnDeletePersonClick(object sender, EventArgs e)
        {
            var infoModel = GetPersonInfo();
            var msg = QueryCustomerUtils.DeletePerson(infoModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                MessageLabel.Text = msg;
                return;
            }
            RefreshCompanyInfo("0");
        }
        #endregion

        #region New Customer Button
        protected void OnNewCustomerClick(object sender, EventArgs e)
        {
            /* alex
            < td >
                                < asp:Button runat = "server" ID = "NewCustomerButton" OnClick = "OnNewCustomerClick"
                                        Style = "margin-left: 10px;margin-top: 2px" Text = "New Customer" CssClass = "btn btn-info btn-small" />
    
                                </ td >
                                */
                HideCustomerDetails();
            var customer = new CustomerEditModel();
            SetViewState(customer, SessionConstants.CustomerEditData);
            SetCompanyInfo(customer);
            CompanyName.Focus();
        }
        #endregion
        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerHtml = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    }
}