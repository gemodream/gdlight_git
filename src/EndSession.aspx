﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="EndSession.aspx.cs" Inherits="Corpt.EndSession2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">
         <div class="demoheading">End Session</div>
         <asp:Panel runat="server" CssClass="form-inline">
             <asp:TextBox runat="server" ID="BatchNumber" placeholder="Batch Number"></asp:TextBox>
             <asp:Button runat="server" ID="LoadButton" OnClick="OnLoadClick" Text="End Session" CssClass="btn btn-info" Style="margin-left: 15px"/>
         </asp:Panel>
         <br/>
         <table>
             <tr>
                 <td>
                     <asp:ListBox ID="lstItemList" runat="server" Width="160px" CssClass="dropdown" Style="vertical-align: top; top: 0px; left: 0px;"
                         Height="350px" AutoPostBack="True" Rows="10" 
                         OnSelectedIndexChanged="OnItemListSelectedChanged">
                     </asp:ListBox>
                 </td>
                 <td style="vertical-align: top; padding-left: 20px;">
                     <asp:ListBox ID="resultList" runat="server" Width="250px" CssClass="dropdown" Style="vertical-align: top"
                         Height="350px" Rows="10"></asp:ListBox>
                 </td>
             </tr>
             <tr>
                 <td colspan="2">
                     <asp:Label runat="server" ID="CpPathToPicture"></asp:Label><br />
                     <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image><br />
                     <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server"></asp:Label>
                 </td>
             </tr>
         </table>
     </div>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="BatchNumber"
        Display="None" 
        ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." 
        ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="BatchNumber"
        Display="None" ValidationExpression="^\d{8}$|^\d{9}$" 
        ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>" 
        ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
</asp:Content>
