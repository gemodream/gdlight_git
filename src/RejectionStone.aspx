﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="RejectionStone.aspx.cs" Inherits="Corpt.RejectionStone" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">
            Rejected Stones</div>
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:Panel ID="UpdatePanel1" runat="server">
                <table>
                    <tr>
                        <td style="padding-left: 20px">
                            <asp:RadioButtonList ID="periodType" runat="server" CssClass="radio" OnSelectedIndexChanged="OnChangedPeriodType"
                                RepeatDirection="Horizontal" Width="250px" AutoPostBack="true">
                                <asp:ListItem Value="m">Monthly</asp:ListItem>
                                <asp:ListItem Value="w" Selected="True">Weekly</asp:ListItem>
                                <asp:ListItem Value="r">Random</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="padding-left: 30px">
                            Customer Like
                        </td>
                        <td style="padding-left: 20px">
                            Type of rejections
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                <label>
                                    From</label>
                                <asp:TextBox runat="server" ID="calFrom" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                                <!-- Date To -->
                                <label style="padding-left: 20px;">
                                    To</label>
                                <asp:TextBox runat="server" ID="calTo" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                            </asp:Panel>
                        </td>
                        <td style="padding-left: 30px">
                            <asp:Panel ID="Panel1" runat="server" CssClass="form-inline">
                                <!-- Date From -->
                                <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="SearchPanelByCustomerCp"
                                    CssClass="form_inline">
                                    <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                                        width: 80px"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                                    <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                        DataValueField="CustomerId" AutoPostBack="True" Width="350px" Style="font-family: Arial;
                                        font-size: 12px" />
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                        <td style="padding-left: 30px; width: 150px">
                            <asp:RadioButtonList ID="TypeRejection" runat="server" CssClass="radio"
                                RepeatDirection="Horizontal" Width="150px">
                                <asp:ListItem Value="Fail" Selected="True">Fail</asp:ListItem>
                                <asp:ListItem Value="Missing">Missing</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <br />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                    PopupButtonID="Image1" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                    PopupButtonID="Image2" />
                <table>
                    <tr>
                        <td><asp:Label ID="Label2" runat="server" class="label" Text="Measures for checking"></asp:Label></td>
                        <td style="padding-left: 40px; vertical-align: top">
                            <asp:Label ID="Label1" runat="server" class="label" Text="Stones by Customer"></asp:Label>
                            <asp:ImageButton ID="LookupButton" runat="server" 
                                ImageUrl="~/Images/ajaxImages/search.png" 
                                OnClick="OnLookupByCustomerClick" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px; vertical-align: top">
                            <asp:Label ID="RejectRatesLabel" runat="server"></asp:Label>
                        </td>
                        <td style="padding-left: 40px; vertical-align: top">
                            <asp:Label ID="CustomerDetailsLabel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: small; text-align: center; vertical-align: top; ">
                            <asp:TreeView ID="MeasureTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                ShowLines="False" ShowExpandCollapse="true">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                        </td>
                        <td style="font-size: small; text-align: center; vertical-align: top; padding-left: 40px">
                            <asp:GridView ID="GridReject" runat="server" AutoGenerateColumns="false" 
                                class="table-bordered" DataKeyNames="CustomerId"
                                CellPadding="5" OnRowCommand="GridReject_OnRowCommand" 
                                onrowdatabound="GridReject_RowDataBound">
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <Columns>
                                    <asp:BoundField HeaderText="CustomerId" DataField="CustomerId" ItemStyle-HorizontalAlign="Left" Visible="false"/>
                                    <asp:BoundField HeaderText="Customer" DataField="CustomerName" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="Stones" DataField="Quantity" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField HeaderText="Invalid" DataField="InvalidQuantity" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField HeaderText="Rejected %" DataField="RejectRate" ItemStyle-HorizontalAlign="Right" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="Details"
                                                Text="Details" CommandArgument='<%# Eval("CustomerId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td style="padding-left: 40px; vertical-align: top">
                            <asp:GridView ID="GridRejectDetails" runat="server" AutoGenerateColumns="false" 
                                class="table-bordered" CellPadding="5" >
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="FullItemNumberWithDotes" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="Create Date" DataField="CreateDate" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Description" DataField="Message" ItemStyle-HorizontalAlign="left" />
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                </table>
        </asp:Panel>
    </div>
</asp:Content>
