﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Reassembly.aspx.cs" Inherits="Corpt.Reassembly" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <script type = "text/javascript">
        function clickUploadButton()
        {
            $( "#<%= btnSaveBulkFiles.ClientID %>" ).click();
        }
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to save data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <div class="demoarea">
    <div class="demoheading">Reassembly</div>
    <div class="navbar nav-tabs">
        <asp:Button ID="CopyButton" runat="server" OnClick="OnCopyClick" Text="Copy" />
        <asp:Button runat="server" ID="ClearButton" Text="Clear" OnClick="OnClearClick" CssClass="btn btn-info" />
        <asp:Button ID="CopyDiff" runat="server" OnClick="OnConfirm" Text="Copy Diff" OnClientClick="Confirm()" />
    </div>
    <table class="auto-style1">
        <tr>
            <td style="width: 350px"><strong>Source Item</strong></td>
            <td style="width: 350px;padding-left: 20px;"><strong>Target Item</strong></td>
        </tr>
        <tr>
            <!-- Source data -->
            <td>
                <asp:Panel runat="server" DefaultButton="SourceLoadButton" ID="SourceLoadPanel">
                    <asp:TextBox ID="SrcItemField" runat="server" ></asp:TextBox>
                    <asp:ImageButton ID="SourceLoadButton" runat="server" ToolTip="Search source item" ImageUrl="~/Images/ajaxImages/search16.png"
                        OnClick="OnSourceLoadClick" />
                </asp:Panel>
            </td>
            <!-- Destination data -->
            <td style="padding-left: 20px;">
                <asp:Panel ID="DestinLoadPanel" runat="server" DefaultButton="DestinLoadButton">
                    <asp:TextBox runat="server" ID="TargetItemField"></asp:TextBox>
                    <asp:ImageButton ID="DestinLoadButton" runat="server" ToolTip="Search destination item" ImageUrl="~/Images/ajaxImages/search16.png"
                        OnClick="OnTargetLoadClick" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td ><asp:Label runat="server" ID="SrcItemName" ForeColor="black" ToolTip="New/Old Item number"></asp:Label></td>
            <td style="padding-left: 20px;"><asp:Label runat="server" ID="TargetItemName" ForeColor="black" ToolTip="New/Old Item number"></asp:Label></td>
        </tr>
        <tr >
            <td style="vertical-align: top;padding-top: 10px;"><asp:Label runat="server" ID="SrcCustomer" ToolTip="Customer Name"></asp:Label></td>
            <td style="padding-left: 20px;vertical-align: top;padding-top: 10px;"><asp:Label runat="server" ID="TargetCustomer" ToolTip="Customer Name"></asp:Label></td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 10px;border-top: dotted silver 1px">
                <asp:TreeView ID="SrcTreeView" runat="server" 
                    onselectednodechanged="SrcNodeChanged">
                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                </asp:TreeView>
            </td>
            <td style="padding-left: 20px;vertical-align: top;padding-top: 10px;border-top: dotted silver 1px">
                <asp:TreeView ID="TargetTreeView" runat="server" 
                    onselectednodechanged="TargetNodeChanged">
                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Overline="False" 
                        Font-Underline="False" />
                </asp:TreeView>
            </td>
            <td>
                <table>
                    <tr>
                        <td>
                                <asp:UpdatePanel runat="server" ID="SelectedBatchValues">
                                    
                                    <ContentTemplate>
                                        <table style="border-color:brown; border-style:double;">
                                            <tr>
											    <td>
                                                    <label class="control-label" for="MultiFileUploader" style="font-weight: 600; float: left; margin-top: 4px; width: 150px;">Multiple Upload:</label>
                                                    <asp:FileUpload ID="MultiFileUploader" runat="server" Multiple="true" OnChange="javascript:clickUploadButton();" Font-Bold="True" Width="200px" ToolTip="Please select files in PDF or XLS format" />
                                                    <asp:Button ID="btnSaveBulkFiles" runat="server" CssClass="btn btn-info btn-small" Style="width: 100px; font-size: 15px; top: 0px; left: 0px;margin-top:5px;display:none;" Text="Bulk Open" OnClick="btnSaveBulkFiles_Click" ToolTip="Press to Save Bulk Files" />
                                                </td>
                                            </tr>
                                        </table>
                                </ContentTemplate>
                                <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveBulkFiles" runat="server" />
                                </Triggers>
                                </asp:UpdatePanel>
                        <td style="border-color:brown; border-style:double;">
                            <asp:Label ID="ErrMultiLbl" ForeColor="Red" runat="server" Width="200px" Enabled="false"/>
                            <asp:Label ID="ActivePartNameLbl" ForeColor="Green" runat="server" Width="200px" ToolTip="Active Part Name" Enabled="false" />
                            <asp:Button ID="SavePartNameBtn" Text="Save Part Name" runat="server" CssClass="btn btn-info" Style="margin-left:10px;" OnClick="SavePartNameBtn_Click" />
                            <asp:TextBox ID="SavedPartNameBox" ForeColor="Blue" runat="server" Width="200px" style="margin-left:10px;" Enabled="false" ToolTip="Saved Part Name" />
                            <asp:HiddenField ID="HdnPartId" runat="server" />
                            <asp:HiddenField ID="HdnPartTypeId" runat="server" />
                            <asp:HiddenField ID="HdnPartName" runat="server" />
                        </td>
                    </tr>
                    <tr style="border-color:brown; border-style:double;">
                        <td>
                             <asp:Panel runat="server" DefaultButton="LoadSourceBtn" ID="Panel1" Visible="true" >
                                <asp:Label runat="server" Text="Source item" Style="margin-left:70px;" />
                                <asp:TextBox runat="server" ID="MultiSourceBox" Style="margin-left:50px;border:double;"></asp:TextBox>
                                <asp:ImageButton runat="server" ID="LoadSourceBtn" ImageUrl="~/Images/ajaxImages/search.png"
                                ToolTip="Load Order data" OnClick="OnLoadMultiSourceClick" />
                            </asp:Panel>
                            <asp:Panel runat="server" DefaultButton="LoadTargetBtn" ID="Panel2" Visible="true">
                                <asp:Label runat="server" Text="Target Item" Style="margin-left:70px;" />
                                <asp:TextBox runat="server" ID="MultiTargetBox" Style="margin-left:50px;border:double;"></asp:TextBox>
                                <asp:ImageButton runat="server" ID="LoadTargetBtn" ImageUrl="~/Images/ajaxImages/search.png"
                                ToolTip="Load Order data" OnClick="OnLoadMultiTargetClick"/>
                            </asp:Panel>
                            
                        </td>
                        <td>
                            <asp:Panel runat="server" ID="MultiBtnPanel" Visible="true" Width="33px">
                                <asp:Button runat="server" ID="AddNewMultiBtn" Text="Add Item" Style="margin-left:20px;" OnClick="OnAddNewMultiBtnClick" CssClass="btn btn-info" />
                                <asp:Button runat="server" ID="MultiMergeBtn" Text="Merge Multiple items" Style="margin-left:20px; margin-top:5px; " OnClick="OnMultiMergeBtnClick" CssClass="btn btn-info" Width="154px"/>
                                <asp:Button runat="server" ID="ClearMultiMergeBtn" Text="Clear Multi List" Style="margin-left:20px; margin-top:5px; " OnClick="OnClearMultiMergeClick" CssClass="btn btn-info"/>
                                <asp:Button ID="ReverseBtn" runat="server" CssClass="btn btn-info btn-small" Style="width: 90px; font-size:15px; top: 0px; margin-left: 20px;margin-top:5px;" Text="Reverse" OnClick="ReverseBtn_OnClick" Enabled ="false" />
                                <asp:Button runat="server" ID="ItemizeBtn" Text="Itemize" Style="margin-left:20px; margin-top:5px; " OnClick="OnItemizeBtnClick" CssClass="btn btn-info" Enabled="false"/>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 10px;">
                <asp:DataGrid runat="server" ID="SrcGrid" AutoGenerateColumns="False" 
                    Width="100%" ShowHeader="False">
                    <Columns>
                        <asp:BoundColumn DataField="MeasureName" HeaderText="Measure" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="ResultValue" HeaderText="Value"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
            <td style="padding-left: 20px; vertical-align: top; padding-top: 10px;">
                <asp:DataGrid runat="server" ID="TargetGrid" AutoGenerateColumns="False" 
                    Width="100%" ShowHeader="False">
                    <Columns>
                        <asp:BoundColumn DataField="MeasureName" HeaderText="Measure" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="ResultValue" HeaderText="Value"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
            <td style="vertical-align: top; width: 550px">
                <table>
                    <tr>
                        <td>
                            <asp:DataGrid runat="server" ID="RulesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                OnItemDataBound="OnRuleItemDataBound" OnSelectedIndexChanged="RulesGrid_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="RuleResetBtn" runat="server" Text="Del" OnClick="OnRuleResetBtnClick"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SourceItemFld" HeaderText="Source Item"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SourcePartNameFld" HeaderText="Source Part"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SourcePartIdFld" HeaderText="Source Part ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TargetItemFld" HeaderText="Target Item"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TargetPartNameFld" HeaderText="Target Part"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TargetPartIdFld" HeaderText="Target Part ID" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White"
                                    Font-Size="Small" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:Label ID="BadItemsLbl" runat="server" Text="Bad Items"></asp:Label>
                            <asp:ListBox runat="server" ID="lstMovedItems" Rows="10" DataValueField="ID" AutoPostBack="True"
                                DataTextField="RequestId" Width="240px" />
                        </td>
                    </tr>
                    
                    <tr>

                        <td>
                            <asp:Panel runat="server" DefaultButton="ItemizeNewOrderBtn" ID="Panel3">
                                <asp:Label ID="ItemizeOrderLbl" runat="server" Text="New Order" ></asp:Label>
                                <asp:TextBox ID="ItemizeOrderBox" runat="server" style="margin-left:60px;" Width="145px"></asp:TextBox>
                                <asp:ImageButton ID="ItemizeNewOrderBtn" runat="server" ToolTip="New Order Number" ImageUrl="~/Images/ajaxImages/search16.png"
                                    OnClick="OnItemizeNewOrderBtnClick" Style="margin-right:0px;" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="ItemizeSkuLbl" runat="server" Text="SKU" ></asp:Label>
                            <asp:TextBox ID="ItemizeSkuBox" runat="server" style="margin-left:93px;" Width="145px" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="ItemizeCountLbl" runat="server" Text="# of Items" ></asp:Label>
                            <asp:TextBox ID="ItemizeCountBox" runat="server" style="margin-left:60px;" Width="145px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="ItemizeMemoLbl" runat="server" Text="MEMO" ></asp:Label>
                            <asp:TextBox ID="ItemizeMemoBox" runat="server" style="margin-left:78px;" Width="145px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="ItemizingProcessBtn" Text="RUN ITEMIZING" Style="margin-left:50px; margin-top:5px; " OnClick="OnItemizingProcessBtn_Click" CssClass="btn btn-info"/>
                        </td>
                    </tr>
                        
                </table>
            </td>
        </tr>
        
    </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="PageHead">
    <style type="text/css">
        .auto-style1 {
            width: 1269px;
        }
    </style>
</asp:Content>

