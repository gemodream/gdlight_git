﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.IO;
//using VY;

namespace TalkToTheNDC.Common
{
    public delegate void backBind(DataGridItem item, DataRow foundRow);
    public class UIcalls
    {
        private System.Web.SessionState.HttpSessionState thisSession;
        private System.Web.UI.Page thispage;
        public Hashtable FormtoForm = new Hashtable();
        public event backBind ItemToDataColumn;
        

        # region "Constructors

        public UIcalls(System.Web.UI.Page pg)
        //, backBind BackBindProc)
        {

            this.thisSession = pg.Session;
            this.thispage = pg;
            //ItemToDataColumn += new backBind(doUpdateInRowFromDataGrid);
        }

        public UIcalls(System.Web.UI.Page pg, backBind BackBindProc)
        //, backBind BackBindProc)
        {
            this.thisSession = pg.Session;
            this.thispage = pg;
            ItemToDataColumn += new backBind(BackBindProc);
            //ItemToDataColumn += new backBind(doUpdateInRowFromDataGrid);
        }
        #endregion
        public static string SplitErrorMessage(
            ref string message)
        {
            const string delim = "#DESC#";

            int delimPos = message.IndexOf(delim);
            if (delimPos == -1)
                return "";

            string description = message.Substring(delimPos + delim.Length);
            message = message.Substring(0, delimPos);

            return description;
        }



      

        public void deleterowfromItem(DataGrid dgr, int deletedItem, DataTable dt)
        {

            deleteFromClientTable(dgr.DataKeys[deletedItem], dt);
            int index = this.GettPageIndexAfterDelete(dgr, dt);
            dgr.CurrentPageIndex = index;
        }






        private void deleteFromClientTable(object keys, DataTable dt)
        {
            DataRow foundRow;
            foundRow = dt.Rows.Find(keys);
            foundRow.Delete();
        }



        public void sortEventResponce(string sortExpression, DataGrid dgr, DataTable dt)
        {
            dt.DefaultView.Sort = sortExpression;
            dgr.CurrentPageIndex = 0;
        }

        private int GettPageIndexAfterDelete(DataGrid dgr, DataTable dt)
        {
            int nextPage;
            int Index = dgr.CurrentPageIndex;
            int items = dt.DefaultView.Count;

            int size = dgr.PageSize;
            if (((Index * size) >= items) && (Index != 0))
            {
                nextPage = Index - 1;
            }
            else
            {
                nextPage = Index;
            }
            return nextPage;
        }



       
        //submitButton 
        


      


        public void submitclient(DataGrid dgr, DataTable dt)
        {
            dgr.SelectedIndex = -1;
            DataRow foundRow;

            foreach (DataGridItem item in dgr.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    foundRow = dt.Rows.Find(dgr.DataKeys[item.ItemIndex]);
                    if (ItemToDataColumn != null && foundRow != null)
                    {
                        ItemToDataColumn(item, foundRow);
                    }
                }
            }
        }

    

        public static void MakeGridSelection(int ID, DataView dvReadingTable, DataGrid dgr, string prk)
        {
            int i = 0;
            Int32 pageindex;
            double d;

            while (i < dvReadingTable.Count)
            {
                if (ID == (int)dvReadingTable[i][prk])
                {
                    d = i;
                    if (dgr.AllowPaging)
                    { pageindex = (int)Math.Floor(d / dgr.PageSize); }
                    else
                    { pageindex = 0; }
                    dgr.CurrentPageIndex = pageindex;
                    dgr.SelectedIndex = i - pageindex * dgr.PageSize;
                    break;
                }
                i++;
            }

        }


        private int GetLastPageIndexBeforeBinding(DataGrid dgr, DataTable dt)
        {
            int nextPage;
            int LastIndex = dgr.PageCount;
            int items = dt.DefaultView.Count;
            //--- getDeletedRows(dt)+1;
            int size = dgr.PageSize;

            if ((LastIndex * size) < items)
            { nextPage = LastIndex; }
            else
            { nextPage = LastIndex - 1; }
            return nextPage;
        }

        //get rid of fBalanceUpdate
       

     
      
    }
}
