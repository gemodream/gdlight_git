﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DeliveryLogBatch : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Batch Delivery Log";
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.Params["ItemNumber"]))
                {
                    var itemNumber = Request.Params["ItemNumber"];
                    var batchNumber = Utils.FullBatchNumber("" + Utils.ParseOrderCode(itemNumber),
                                                            "" + Utils.ParseBatchCode(itemNumber));
                    BatchNumber.Text = batchNumber;
                    OnLoadClick(null, null);
                } else
                {
                    BatchNumber.Focus();
                }
            }

        }
        private void ResetDataGrid()
        {
            dgrLog.DataSource = null;
            dgrLog.DataBind();

        }
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            tvwItems.Nodes.Clear();
            ResetDataGrid();
            InfoLabel.Text = "";
            var batchNumber = BatchNumber.Text.Trim();
            if (string.IsNullOrEmpty(batchNumber))
            {
                return;
            }
            var cps = QueryUtils.GetItemsByBatchNumber(batchNumber, this);
            if (cps.Count == 0)
            {
                InfoLabel.Text = "Batch number " + batchNumber + " not found.";
                return;
            }
            var batchId = cps.ElementAt(0).NewBatchId;
            
            //-- Item numbres
            var data = new List<TreeViewModel>
                           {new TreeViewModel {ParentId = "", Id = batchId, DisplayName = batchNumber}};
            data.AddRange(cps.Select(cp => new TreeViewModel { ParentId = batchId, Id = cp.ItemCode, DisplayName = cp.OldFullIemNumber }));
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tvwItems.Nodes.Add(rootNode);

            //-- Batch History data grid
            FillDataGrid(batchId);
        }
        protected void TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            if (tvwItems.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tvwItems.CheckedNodes)
                {

                    if (node.ChildNodes.Count > 0)
                    {
                        foreach (TreeNode childNode in node.ChildNodes)
                        {
                            childNode.Checked = true;
                        }
                    }

                }

            }
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            InfoLabel.Text = "";
            ResetDataGrid();
            var checkedItems = new List<ItemModel>();
            foreach (TreeNode node in tvwItems.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0 || node.Parent == null) continue;
                var chkItem = new ItemModel
                {
                    ItemCode = node.Value,
                    OldFullIemNumber = node.Text.Trim(),
                    NewBatchId = node.Parent.Value
                };
                checkedItems.Add(chkItem);
            }
            if (checkedItems.Count == 0)
            {
                InfoLabel.Text = "Checked Items Not Found!";
                return;
            }
            var msg = QueryUtils.AddRecordsDeliveryLog(checkedItems, BatchNumber.Text.Trim(), EventList.SelectedValue, this);
            if (!string.IsNullOrEmpty(msg))
            {
                InfoLabel.Text = msg;
                return;
            }
            FillDataGrid(checkedItems.ElementAt(0).NewBatchId);
        }
        private void FillDataGrid(string batchId)
        {
            var events = QueryUtils.GetBatchDeliveryLog(batchId, this);
            if (events.Count > 0)
            {
                dgrLog.DataSource = events;
                dgrLog.DataBind();
            }

        }
        protected void OnClearClick(object sender, EventArgs e)
        {
            BatchNumber.Text = "";
            tvwItems.Nodes.Clear();
            InfoLabel.Text = "";
            ResetDataGrid();
        }
    }
}