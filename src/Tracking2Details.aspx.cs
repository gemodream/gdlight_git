﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class Tracking2Details : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var batch = "";
            try
			{
                batch = Request.Params["BatchNumber"];
				batchNumberFld.Text = "Batch number: " + batch;
			}
			catch(Exception ex)
			{
				infoLabel.Text = "Not enough parameters: "+ex.Message;
				return;
			}
            //-- Permission
            if (!QueryUtils.HasWebAccess21(this))
            {
                infoLabel.Text = "Do not have access";
                return;
            }
            var batches = QueryUtils.GetBatchesByBatchNumber(batch, this);
            if (batches.Count == 1)
            {
                DisplayData(batches[0]);
            } else
            {
                infoLabel.Text = "No history for batch " + batch;
            }
        }

        private void DisplayData(BatchModel batchModel)
        {
            var items = QueryUtils.GetBatchHistory(batchModel, this);
            lblHistoryRows.Text = items.Count == 0 ? "" : items.Count + " rows";
            SetViewState(items, SessionConstants.BatchedHistoryList);
            ApplySortGrid();
        }
        private void ApplySortGrid()
        {
            var batches = GetViewState(SessionConstants.BatchedHistoryList) as List<BatchHistoryModel> ??
                          new List<BatchHistoryModel>();
            var table = new DataTable("BatchesHistory");
            table.Columns.Add(new DataColumn { ColumnName = "TimeStamp", DataType = typeof(DateTime) });
            table.Columns.Add(new DataColumn { ColumnName = "Action", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Form", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "FirstName", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "LastName", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "ItemsAffected", DataType = typeof(int) });

            foreach (var trackingModel in batches)
            {
                table.Rows.Add(new object[]
                {
                    trackingModel.TimeStamp, 
                    trackingModel.Action,
                    trackingModel.Form,
                    trackingModel.FirstName,
                    trackingModel.LastName, 
                    trackingModel.ItemsAffected
                });
            }
            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.HistorySortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "TimeStamp";
                SetViewState(new SortingModel { ColumnName = "TimeStamp", Direction = true }, SessionConstants.HistorySortModel);
            }

            grdDetails.DataSource = dv;
            grdDetails.DataBind();
                        
        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.HistorySortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.HistorySortModel);
            ApplySortGrid();
        }
    }
}