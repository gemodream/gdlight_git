﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Appraisal.aspx.cs" Inherits="Corpt.Appraisal" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <style>
        .lbl
        {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma, Arial, sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }

        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }
        
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
        .text_nohighlitedyellow
        {
            background-color: white;
        }
    </style>
    <script type="text/javascript">
        function jsSetEditMode() {
            var doc = document.getElementById('<%= ItemValuesGrid.ClientID %>');
        }
    </script>

    <div class="demoarea">
         <div style="height: 25px">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                 <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                     <b>Please, wait....</b>
                 </ProgressTemplate>
             </asp:UpdateProgress>
         </div>
         <asp:UpdatePanel runat="server" ID="MainPanel">
             <ContentTemplate>
                 <div class="demoheading">
                     Appraisal
                     <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 20px"></asp:Label>
                 </div>
                 <table>
                    <tr>
                        <td style="vertical-align: top" colspan="2">
                            <%-- 'Batch Number' textbox --%>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                                <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                    placeholder="Batch number" Style="width: 115px;" />
                                <%-- 'Load' button --%>
                                <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-small" runat="server" Text="Load"
                                    OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
                            </asp:Panel>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                        </td>
                        <td style="vertical-align: top" nowrap="nowrap">
                            <asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel" Visible="false"></asp:Label>
                            <%-- 'Save' button --%>
                            <asp:Button ID="cmdSave" CssClass="btn btn-info btn-small" runat="server" Text="Save"
                                Style="text-align: center; margin-left: 20px" OnClick="OnSaveClick" Visible="false">
                            </asp:Button>
                            <%--Alex Bulk Update changes for colored diamonds --%>
                            <asp:Button ID="bulkUpdateColoredDiamond" CssClass="btn btn-info btn-small" runat="server" Text="Bulk Save Colored Diamonds"
                                Style="text-align: center; margin-left: 20px" OnClick="OnBulkUpdateCDClick" Visible="false">
                            </asp:Button>
							<%-- Sergey: Batch Mode Button --%>
							<asp:Button ID="batchModeButton" CssClass="btn btn-info btn-small" runat="server" Text="Batch Mode"
								Style="text-align: center; margin-left: 20px;" OnClick="OnBatchModeClick" Visible="false" />
							<%-- Sergey: Batch Mode Metals --%>
							<asp:Button ID="batchMetals" CssClass="btn btn-info btn-small" runat="server" Text="Change Selected Metals"
								Style="text-align: center; margin-left: 20px;" OnClick="OnBatchMetalClick" Visible="false" />
							<asp:TextBox ID="batchMetalTextBox" runat="server" MaxLength="12" visible="false" style="text-align: center; width: 100px;"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="batchMetalValidator" runat="server" FilterType="Custom"
																ValidChars="0123456789." TargetControlID="batchMetalTextBox" />
							<%--Sergey: Batch Mode Price Bump --%>
							<div id="batchBumpItems" visible="false" runat="server" style="display:inline;">
								<asp:Button ID="batchBump" CssClass="btn btn-info btn-small" runat="server" Text="Price Bump"
									Style="text-align: center; margin-left: 20px;" OnClick="OnBatchBumpClick"  />
								<asp:TextBox ID="batchBumpTextBox" runat="server" Text="10.0" MaxLength="10" 
									Style="text-align:center; width:100px;"></asp:TextBox>
								<ajaxToolkit:FilteredTextBoxExtender ID="batchBumpValidator" runat="server" FilterType="Custom"
																ValidChars="0123456789.-" TargetControlID="batchBumpTextBox" />
								<asp:Label ID="PercentLabel" Text="%" runat="server"></asp:Label>
							</div>
							<%-- Sergey: Currency selector elements --%>
							<asp:Label runat="server" Text="Currency:" ID="currencyLabel" Visible="false" style="margin-left: 20px;"></asp:Label>
							<asp:TextBox runat="server" ID="currencySelector" placeholder="Enter Currency" MaxLength="25" OnTextChanged="currencySelector_TextChanged" AutoPostBack="true"
								style="width:130px; text-align:center;" Visible="false">
							</asp:TextBox>
							<asp:HiddenField runat="server" ID="oldCurrencySelector" />
							<asp:TextBox runat="server" ID="currencyPrice" style="text-align:center; margin-left: 10px; width: 100px;" ReadOnly="true" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0px; vertical-align: top">
                            <asp:ListBox ID="lstItemList" runat="server" Width="130px" CssClass="text" Style="vertical-align: top"
                                AutoPostBack="True" Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged">
                            </asp:ListBox>
                            <br />
                        </td>
                        <td style="vertical-align: top;padding-right: 20px;width: 220px;">
                            <asp:TreeView ID="PartTree" runat="server" 
                                OnSelectedNodeChanged="OnPartsTreeChanged" NodeIndent="15">
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                            </asp:TreeView>
                        </td>
                        <td style="vertical-align: top">
                            <asp:Panel runat="server" ID="MeasurementPanel" >
                                <asp:HiddenField runat="server" ID="ItemEditing" />
                                
                                <div style="overflow: auto; color: black; border: silver solid 1px;
                                    margin-top: 5px" id="EditItemDiv" runat="server">
                                    <asp:DataGrid runat="server" ID="ItemValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                        DataKeyField="UniqueKey" OnItemDataBound="OnEditItemDataBound" Width="1000px">
                                        <Columns>
                                            <asp:BoundColumn DataField="IsContainer"        HeaderText="IsContainer" Visible="False"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="UniqueKey"          HeaderText="UniqueKey" Visible="False"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="PartID"             HeaderText="PartID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PartTypeID"         HeaderText="PartTypeID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ItemNumber"         HeaderText="Item #"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PartName"           HeaderText="Part/Item"></asp:BoundColumn>
				<%--								ItemStyle-Width="110px">
                                                <ItemStyle Width="110px" />--%>
            <%--                                </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Weight"             HeaderText="Weight"></asp:BoundColumn>
		<%--									ItemStyle-Width="80px">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="StonesDisplay"      HeaderText="Stones"></asp:BoundColumn>
		<%--									ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Weight_Per_Stone"   HeaderText="Weight/stone"></asp:BoundColumn>
		<%--									ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="CalcWeight_Per_Stone"   HeaderText="Calc.W/stone" Visible="false"></asp:BoundColumn>
	<%--										ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
<%-- IvanB 27/09 start --%>
                                            <asp:BoundColumn DataField="ShapeName"       HeaderText="Shape"></asp:BoundColumn>
	<%--										ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Color"              HeaderText="Color"></asp:BoundColumn>
	<%--										ItemStyle-Width="60px">
                                                <ItemStyle Width="60px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Clarity"            HeaderText="Clarity"></asp:BoundColumn>
<%--											ItemStyle-Width="60px">
                                                <ItemStyle Width="60px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Polish"            HeaderText="Polish"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Symmetry"          HeaderText="Symmetry"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LWFlourescence"    HeaderText="LW Fl"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Treatment"         HeaderText="Treat"></asp:BoundColumn>
<%-- IvanB 27/09 start --%>
                                            <asp:BoundColumn DataField="Metal"              HeaderText="Metal"></asp:BoundColumn>
<%--											ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="UpperPrice"         HeaderText="Up.Price/ct"></asp:BoundColumn>
<%--											ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="LowerPrice"         HeaderText="Low.Price/ct"></asp:BoundColumn>
<%--											ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <%--index 20 old index 16 --%>
                                            <asp:BoundColumn DataField="Price"              HeaderText="Price/part"></asp:BoundColumn>
<%--											ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Markup"             HeaderText="Markup"></asp:BoundColumn>
<%--											ItemStyle-Width="100px">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="Old_MSRP"           HeaderText="Saved MSRP"></asp:BoundColumn>
<%--											ItemStyle-Width="40px">
                                                <ItemStyle Width="40px" />
                                            </asp:BoundColumn>--%>
                                            <asp:TemplateColumn> 
                                                <HeaderTemplate>
                                                    Calculated MSRP</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel runat="server" ID="ItemValuePanel" CssClass="form-inline" DefaultButton="EditHiddenBtn">
                                                        <asp:Button runat="server" ID="EditHiddenBtn" Style="display: none" Enabled="True" />
                                                        <asp:TextBox runat="server" ID="ValueNumericFld" Width="80px" onchange="jsSetEditMode();" ontextchanged="OnNumericFldChanged" AutoPostBack="true"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ValueNumericExtender" runat="server" FilterType="Custom"
                                                            ValidChars="0123456789." TargetControlID="ValueNumericFld" />
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
											<%-- Sergey: Check Box Column --%>
											<asp:TemplateColumn>
												<HeaderTemplate>
													<asp:CheckBox runat="server" ID="batchSelectAll" OnCheckedChanged="batchSelectAll_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox runat="server" ID="batchCheckBoxes"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                    </asp:DataGrid>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                 </table>
                 <%-- Information Dialog --%>
                 <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px;
                     display: none; border: solid 2px Gray">
                     <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                         border: solid 1px Silver; color: black; text-align: left">
                         <div>
                             <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                             <b>Information</b>
                         </div>
                     </asp:Panel>
                     <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px;
                         color: black; text-align: center" id="MessageDiv" runat="server">
                     </div>
                     <div style="padding-top: 10px">
                         <p style="text-align: center; font-family: sans-serif;">
                             <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" />
                         </p>
                     </div>
                 </asp:Panel>
                 <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                 <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                     PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                     OkControlID="InfoCloseButton">
                 </ajaxToolkit:ModalPopupExtender>

                 <%-- Save Question Dialog --%>
                 <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup" Style="width: 430px;
                     display: none">
                     <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;
                         border: solid 1px Silver; color: black; text-align: left">
                         <div>
                             <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                             <asp:Label ID="QTitle" runat="server" />
                         </div>
                     </asp:Panel>
                     <div style="color: black; padding-top: 10px">
                         Do you want to save the item changes?</div>
                     <div style="padding-top: 10px">
                         <p style="text-align: center; font-family: sans-serif">
                             <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes"
                                 class="btn btn-info btn-small" />
                             <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" OnClick="OnQuesSaveNoClick"
                                 class="btn btn-info btn-small" />
                             <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" OnClick="OnQuesSaveCancelClick"
                                 class="btn btn-info btn-small" />
                             <asp:Button ID="QuesCloseBtn" runat="server" Text="Close" Style="display: none" />
                         </p>
                     </div>
                     <asp:HiddenField ID="SaveDlgMode" runat="server" />
                 </asp:Panel>
                 <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                 <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog"
                     ID="SaveQPopupExtender" PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn">
                 </ajaxToolkit:ModalPopupExtender>
             </ContentTemplate>
         </asp:UpdatePanel>
     </div>
</asp:Content>
