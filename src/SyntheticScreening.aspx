﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="SyntheticScreening.aspx.cs" Inherits="Corpt.SyntheticScreening" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        
        .GridPager a, .GridPager span
        {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }
        
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        
        .GridPager span
        {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }
        
        .divCol
        {
            float: left;
            width: 190px;
        }
        
        .divBottom
        {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        $().ready(function () {
        
            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtGsiOrder.ClientID%>').val().trim() == "") {
                    alert('Please fill out order field.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtGsiOrder.ClientID%>').val().trim().length != 6 && $('#<%=txtGsiOrder.ClientID%>').val().trim().length != 7) {
                    alert('Please enter valid six or seven digit order code.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtVendorNum.ClientID%>').val().trim() == "") {
                    alert('Please fill out vendor number field.'); $('#<%=txtVendorNum.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtPoNum.ClientID%>').val().trim() == "") {
                    alert('Please fill out PO number field..'); $('#<%=txtPoNum.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtTotalQTY.ClientID%>').val().trim() == "") {
                    alert('Please fill out total quantity field..'); $('#<%=txtTotalQTY.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQtyPass.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity pass field..'); $('#<%=txtQtyPass.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQtyFail.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity fail field..'); $('#<%=txtQtyFail.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtFTQuantity.ClientID%>').val().trim() == "") {
                    alert('Please fill out further test quantity field..'); $('#<%=txtFTQuantity.ClientID%>').focus(); return false;
                }
                else if (parseInt($('#<%=txtTotalQTY.ClientID%>').val()) != parseInt($('#<%=txtQtyPass.ClientID%>').val()) + parseInt($('#<%=txtQtyFail.ClientID%>').val()) + parseInt($('#<%=txtFTQuantity.ClientID%>').val())) {
                    alert('Please fill out valid entry.\nTotal quantity is not equal to quantity pass plus quantity fail plus further test quantity.'); $('#<%=txtFTQuantity.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtSku.ClientID%>').val().trim() == "") {
                    alert('Please fill out SKU field..'); $('#<%=txtSku.ClientID%>').focus(); return false;
                }
                else {
                    return true;
                }

            });

            $('#<%=txtGsiOrder.ClientID%>, #<%=gsiOrderFilter.ClientID%>, #<%=txtTotalQTY.ClientID%>, #<%=txtQtyPass.ClientID%>, #<%=txtQtyFail.ClientID%>, #<%=txtFTQuantity.ClientID%>, #<%=txtGSIItemNumber.ClientID%>, #<%=txtItemQTYFail.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

            $('#<%=txtCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();

            $('#<%=txtToCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();
        });

        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>

    <div class="demoarea">
        <h2 style="margin-left: 10px; margin-bottom: 10px; margin-top: 10px" class="demoheading">
            Synthetic Screening Data Enrty
        </h2>
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">
                <div class="divCol" style="width: 170px;">
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtGsiOrder">
                            GSI Order #:</label>
                        <div>
                            <input id="hdnScreeningID" runat="server" type="hidden" />
                            <input id="txtGsiOrder" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter GSI Order #" maxlength="7" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtVendorNum">
                            Vendor #:</label>
                        <div>
                            <input id="txtVendorNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter Vendor #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtPoNum">
                            PO #:</label>
                        <div>
                            <input id="txtPoNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter PO #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtSku">
                            SKU/Style:</label>
                        <div>
                            <input id="txtSku" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter SKU/Style" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label">
                            Test:</label>
                        <div>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkQCHK" runat="server" type="checkbox" style="margin-top: 0px;" value="QCHK" />QCHK++
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkYehuda" runat="server" type="checkbox" style="margin-top: 0px;" value="Yehuda" />Yehuda
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkDiamondView" runat="server" type="checkbox" style="margin-top: 0px;"
                                    value="DiamondView" />Diamond View
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkRaman" runat="server" type="checkbox" style="margin-top: 0px;" value="Raman" />Raman
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkFTIR" runat="server" type="checkbox" style="margin-top: 0px;" value="FTIR" />FTIR
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="width: 170px; margin-top: 25px;">
                        <div>
                            <label class="control-label">
                                Add Fail Items for Order#:</label>
                        </div>
                        <div style="width: 350px;">
                            <input id="txtGSIItemNumber" runat="server" maxlength="11" style="width: 30%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="GSI Item Number" />
                            <input id="txtItemQTYFail" runat="server" maxlength="3" style="width: 20%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="Quantity Fail" />
                            <asp:Button ID="btnAddItem" runat="server" CssClass="btn btn-primary" Text="Add Item"
                                Style="height: 25px; margin-bottom: 10px;" OnClick="btnAddItem_Click"></asp:Button>
                        </div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" Style="margin-top: 5px;"
                                        OnRowDeleting="gvItems_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Item Number" HeaderStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGSIItemNumber" runat="server" Text='<%# Eval("GSIItemNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QTY Fail" HeaderStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemQTYFail" runat="server" Text='<%# Eval("ItemQTYFail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="true" DeleteImageUrl="Images/delete.png" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="divCol" style="margin-right: 55px;">
                    <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtTotalQTY">
                            Certified By:</label>
                        <div>
                            <asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="form-control form-control-height selectpicker"
                                Style="padding-top: 2px; padding-bottom: 2px;">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Not Certified</asp:ListItem>
                                <asp:ListItem>GSI Certified</asp:ListItem>
                                <asp:ListItem>GIA Certified</asp:ListItem>
                                <asp:ListItem>GSL Certified</asp:ListItem>
                                <asp:ListItem>IGI Certified</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtDestination">
                            Destination:</label>
                        <div>
                            <asp:DropDownList ID="ddlDestination" runat="server" CssClass="form-control form-control-height selectpicker"
                                Style="padding-top: 2px; padding-bottom: 2px;">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>BenBridge(US)</asp:ListItem>
                                <asp:ListItem>Peoples Jewelers(CANADA)</asp:ListItem>
                                <asp:ListItem>Signet(USA)</asp:ListItem>
                                <asp:ListItem>Signet(UK)</asp:ListItem>
                                <asp:ListItem>STERLING</asp:ListItem>
                                <asp:ListItem>UK</asp:ListItem>
                                <asp:ListItem>Zale(US)</asp:ListItem>
                                <asp:ListItem>Zale(CANADA)</asp:ListItem>
                                <asp:ListItem>Zale(Peoples Jewelers)</asp:ListItem>
                                <asp:ListItem>Zale(Outlet)</asp:ListItem>
                                <asp:ListItem>Zale(Pagoda)</asp:ListItem>
								<asp:ListItem>Internal PC</asp:ListItem>
								<asp:ListItem>PIERCING PAGODA</asp:ListItem>
								<asp:ListItem>PIERCING PAGODA/US</asp:ListItem>
								<asp:ListItem>STERLING US</asp:ListItem>
								<asp:ListItem>Zales Jewelers - US</asp:ListItem>
								<asp:ListItem>PAGODA/US</asp:ListItem>
								<asp:ListItem>Internal Jewelex</asp:ListItem>
								<asp:ListItem>Fred Meyer (USA)</asp:ListItem>								
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtNotes">
                            Notes:</label>
                        <div>
                            <textarea id="txtNotes" runat="server" class="form-control form-control-height" rows="3"
                                placeholder="Enter Notes" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtTotalQTY">
                                        Total QTY:</label>
                                    <div>
                                        <input id="txtTotalQTY" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter Total QTY" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtFTQuantity">
                                        Further Test QTY:</label>
                                    <div>
                                        <input id="txtFTQuantity" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter FT QTY" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyPass">
                                        Quantity Pass:</label>
                                    <div>
                                        <input id="txtQtyPass" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Pass" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyFail">
                                        Quantity Fail:</label>
                                    <div>
                                        <input id="txtQtyFail" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Fail" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <div style="text-align: left;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Add New"
                                OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary" Text="Clear"
                                OnClick="btnClear_ServerClick"></asp:Button>
                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back" Style="display: none;"
                                OnClick="btnBack_Click"></asp:Button>
                        </div>
                    </div>
                </div>
                <div style="float: left; width: 600px;">
                    <div class="form-grouph" style="margin-bottom: 7px;">
                        <label class="control-label" for="gsiOrderFilter">
                            Filter By GSI Order# / Destination / Create Date:</label>
                        <div style="padding-bottom: 5px; display: block;width:800px;">
                            <input id="gsiOrderFilter" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter Order#" maxlength="7" />
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnSearch_Click">
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                            <asp:DropDownList ID="ddlDestinationSearch" runat="server" CssClass="form-control form-control-height selectpicker"
                                Style="width: 17%; display: inline">
                                <asp:ListItem selected hidden >Select Destination</asp:ListItem>
                                <asp:ListItem>BenBridge(US)</asp:ListItem>
                                <asp:ListItem>Peoples Jewelers(CANADA)</asp:ListItem>
                                <asp:ListItem>Signet(USA)</asp:ListItem>
                                <asp:ListItem>Signet(UK)</asp:ListItem>
                                <asp:ListItem>STERLING</asp:ListItem>
                                <asp:ListItem>UK</asp:ListItem>
                                <asp:ListItem>Zale(US)</asp:ListItem>
                                <asp:ListItem>Zale(CANADA)</asp:ListItem>
                                <asp:ListItem>Zale(Peoples Jewelers)</asp:ListItem>
                                <asp:ListItem>Zale(Outlet)</asp:ListItem>
                                <asp:ListItem>Zale(Pagoda)</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Button ID="btnDestSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" 
                                onclick="btnDestSearch_Click" >
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                            <input id="txtCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="From Date"
                                readonly />
                            <input id="txtToCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="To Date"
                                readonly />
                            <asp:Button ID="btnCreateDateSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCreateDateSearch_Click">
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;">
                            </span>
                            <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px;" OnClick="btnExportToExcel_Click" />
                        </div>
                        <div class="form-group" style="margin-bottom: 7px;">
                            <div id="mainContainer" class="container" style="padding-left: 0px;">
                                <div class="shadowBox">
                                    <div class="page-container">
                                        <div class="container" style="padding-left: 0px;">
                                            <div style="padding-bottom: 5px;">
                                                <span id="gvTitle" runat="server" class="text-info"></span>
                                            </div>
                                            <div>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="grdScreening" runat="server" Width="1200" CssClass="table striped table-bordered table-hover"
                                                        AutoGenerateColumns="False" DataKeyNames="ScreeningID" EmptyDataText="There are no data records to display."
                                                        AllowPaging="true" RowStyle-Height="2px" OnPageIndexChanging="grdScreening_PageIndexChanging"
                                                        OnRowDeleting="grdScreening_RowDeleting" OnSelectedIndexChanged="grdScreening_SelectedIndexChanged"
                                                        Height="200px">
                                                        <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                            Height="25px" />
                                                        <RowStyle Font-Size="12px" />
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" SelectText="Select" HeaderStyle-Width="30px" />
                                                            <asp:BoundField DataField="ScreeningID" HeaderText="Sr.No." Visible="true" ReadOnly="True"
                                                                HeaderStyle-Width="30px" SortExpression="ScreeningID" ItemStyle-CssClass="visible-lg"
                                                                HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="CreatedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="GSIOrder" HeaderText="Order #" HeaderStyle-Width="60px"
                                                                SortExpression="GSIOrder" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="VendorNum" HeaderText="Vendor #" HeaderStyle-Width="100px"
                                                                SortExpression="VendorNum" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Test" HeaderText="Test" HeaderStyle-Width="40px" ItemStyle-Wrap="true"
                                                                SortExpression="Test" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CertifiedBy" HeaderText="Certified By" HeaderStyle-Width="80px"
                                                                SortExpression="CertifiedBy" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Destination" HeaderText="Destination" HeaderStyle-Width="110px"
                                                                SortExpression="Destination" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="TotalQty" HeaderText="Total QTY" HeaderStyle-Width="30px"
                                                                SortExpression="TotalQty" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="FTQuantity" HeaderText="QTY FT" HeaderStyle-Width="30px"
                                                                SortExpression="FTQuantity" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtyPass" HeaderText="QTY Pass" HeaderStyle-Width="30px"
                                                                SortExpression="QtyPass" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtyFail" HeaderText="QTY Fail" HeaderStyle-Width="30px"
                                                                SortExpression="QtyFail" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:CommandField Visible="false" ShowDeleteButton="false" ButtonType="Image" DeleteImageUrl="~/Images/delete.png"
                                                                HeaderText="Delete" HeaderStyle-Width="15px" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="PONum" HeaderText="PO #" HeaderStyle-Width="150px" SortExpression="PONum"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="SKUName" HeaderText="SKU" HeaderStyle-Width="150px" ItemStyle-Wrap="true"
                                                                SortExpression="SKUName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" HeaderStyle-Width="350px" ItemStyle-Wrap="true"
                                                                SortExpression="CustomerName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" HeaderStyle-Width="250"
                                                                HeaderStyle-CssClass="hidden-lg" ItemStyle-CssClass="hidden-lg" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
