﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class RMeas : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: AutoMeasure";
            if (!IsPostBack)
            {
				/* Security to prevent user accessing directly via url after logging in */
				string err = "";
				bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgAutoMeasure, this, out err);
				if (!access || err != "")
				{
					Response.Redirect("Middle.aspx");
				}
				// */
				LoadEnumMeasures();
                MultiParts.Checked = false;
                AddBatchBox.Enabled = false;
                AddBatchBox.Checked = false;
            }
        }
        #region Save button
        protected void OnSaveMeasureClick(object sender, EventArgs e)
        {
            SaveMsgLabel.Text = "";
            //-- Get Checked Batches
            
            var checkedItems = new List<BatchItemModel>();
            foreach (TreeNode node in tvwItems.CheckedNodes)
            {
                if (node.Depth < 2) continue;
                var chkItem = new BatchItemModel
                {
                    BatchId = Convert.ToInt32(node.Parent.Value),
                    ItemCode = Convert.ToInt32(node.Value)
                };
                checkedItems.Add(chkItem);
            }

            if (checkedItems.Count == 0)
            {
                var msg = "Checked Items Not Found!";
                //SaveMsgLabel.Text = msg;
                //System.Web.HttpContext.Current.Response.Write(
                //    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                PopupInfoDialog(msg, "Save",true);
                return;
            }
            var randValues = new List<MeasureValueModel>();
            var rulesData = GetDataFromRuleGrid().FindAll(m => m.HasValues);
            
            if (MultiParts.Checked)
            {
                List<AutoMeasModel> multiMeas = MultiPartsRules(rulesData);
                if (multiMeas != null)
                    rulesData.AddRange(multiMeas);
            }
            
            if (rulesData.Count == 0)
            {
                var msg = "Calculation Rules with values Not Found!";
                //SaveMsgLabel.Text = msg;
               // System.Web.HttpContext.Current.Response.Write(
               //     "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                PopupInfoDialog(msg, "Save", true);
                return;
            }
            
            foreach (var rule in rulesData)
            {
                SetRandomValues(rule, checkedItems.Count);
                for (int i = 0; i < checkedItems.Count; i++)
                {
                    var item = checkedItems[i];
                    rule.RandValue = rule.RandValues[i];
                    if (rule.MeasureCode == "128" || rule.MeasureCode == "129" || rule.MeasureCode == "130" || rule.MeasureCode == "131" || rule.MeasureCode == "132")//PSX...Value
                    {
                        double dValue = Convert.ToDouble(rule.RandValue);
                        int iValue = (int)Math.Round(dValue);
                        rule.RandValue = iValue.ToString();
                    }
                    randValues.Add(new MeasureValueModel(rule, item));
                }
            }
            var errMsg = QueryUtils.SavePreFilling(randValues, this);
            var message = string.IsNullOrEmpty(errMsg) ? "The calculation has been completed successfully." : errMsg;
            //SaveMsgLabel.Text = message;
            //System.Web.HttpContext.Current.Response.Write(
            //        "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + message + "\")</SCRIPT>");
            PopupInfoDialog(message, "Save", !string.IsNullOrEmpty(errMsg));
        }
        protected List<AutoMeasModel> MultiPartsRules(List<AutoMeasModel> rulesData)
        {
            List<AutoMeasModel> multiMeas = new List<AutoMeasModel>();
            var partsMulti = GetViewState(SessionConstants.BulkPartsMulti) as List<PartModelMulti>;
            foreach (var rule in rulesData)
            {
                int partTypeId = partsMulti.Find(m => m.PartName.ToLower() == rule.PartName.ToLower()).PartTypeId;
                if (partTypeId == 1)//diamonds
                {
                    //var partsMulti = GetViewState(SessionConstants.BulkPartsMulti) as List<PartModelMulti>;
                    //int partTypeId = partsMulti.Find(m => m.PartName.ToLower() == rule.PartName.ToLower()).PartTypeId;
                    for (int i = 0; i <= PartsListField.Items.Count - 1; i++)
                    {
                        if (PartsListField.Items[i].Value == rule.PartId)
                        {
                            continue;
                        }
                        string partId = "";
                        int value = Convert.ToInt32(PartsListField.Items[i].Value);
                        int multiPartTypeId = partsMulti.Find(m => m.PartId == value).PartTypeId;

                        if (partTypeId == multiPartTypeId)
                        {
                            partId = PartsListField.Items[i].Value;
                            var measure = GetMeasureFromView(partId, rule.MeasureId);
                            var part = GetPartModel(partId);
                            var enumMeas = measure.IsEnum ? GetEnumMeasure(rule.MeasureId) : new List<EnumMeasureModel>();
                            var autoMeas = new AutoMeasModel(measure, part, enumMeas);
                            autoMeas.FromValue = rule.FromValue;
                            autoMeas.ToValue = rule.ToValue;
                            multiMeas.Add(autoMeas);
                        }
                    }
                }
            }
            if (multiMeas != null && multiMeas.Count > 0)
                return multiMeas;
            else
                return null;
        }
        #endregion

        #region Clear button
        protected void OnClearClick(object sender, EventArgs e)
        {
            BatchNumberFld.Text = "";
            InfoPanel.Visible = false;
            AddBatchBox.Checked = false;
            AddBatchBox.Enabled = false;
            HdnItemTypeID.Value = "";
        }
        #endregion

        #region Load button
        protected void OnLoadOrderClick(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            OnLoadClick(null, null);
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            MultiPartsList.Visible = false;
            MultiPartsList.Items.Clear();
            MultiParts.BackColor = System.Drawing.Color.LightBlue;
            Page.Validate("BatchGroup");
            if (!Page.IsValid) return;
            if (!AddBatchBox.Enabled)
            {
                AddBatchBox.Enabled = true;
                AddBatchBox.Checked = false;
                HdnItemTypeID.Value = "";
            }
            if (!AddBatchBox.Checked)
            {
                tvwItems.Nodes.Clear();
            }
            else
            {
                if (HdnItemTypeID.Value != "")
                {
                    bool validated = ValidateMultiBatch(BatchNumberFld.Text, HdnItemTypeID.Value);
                    if (!validated)
                    {
                        var msg = "Batch type of " + BatchNumberFld.Text + " does not match initial batch type!";
                        PopupInfoDialog(msg, "Save", true);
                        return;
                    }
                }
            }
            
            RulesGrid.DataSource = new List<AutoMeasModel>();
            RulesGrid.DataBind();

            //-- Load Similar Batches
            
            var batchNumber = BatchNumberFld.Text.Trim();
            var order = "" + Utils.ParseOrderCode(batchNumber);
            order = Utils.FillToFiveChars(order);
            
            var batchList = QueryUtils.GetSimilarBatches(order, "" + Utils.ParseBatchCode(batchNumber), this);
            InfoPanel.Visible = batchList.Count > 0;
            if (batchList.Count == 0)
            {
                var msg = "Batch number " + batchNumber + " Not Found!";
                PopupInfoDialog(msg, "Save",true);
                return;
            }
            //-- Load Tree Items

            var itemList = QueryUtils.GetItemsByOrderCode(order, this);
            var data = new List<TreeViewModel>();
            data.Add(new TreeViewModel { ParentId = "", Id = order, DisplayName = order });
            string itemTypeId = batchList[0].ItemTypeId.ToString();
            if (itemTypeId != null && itemTypeId != "")
                HdnItemTypeID.Value = itemTypeId;
            foreach (var batch in batchList)
            {
                if (AddBatchBox.Checked && HdnItemTypeID.Value != "")
                {
                    string newItemTypeId = batch.ItemTypeId.ToString();
                    if (newItemTypeId != HdnItemTypeID.Value)
                        continue;
                }
                var items = itemList.FindAll(m => m.BatchId == batch.BatchId);
                if (items.Count == 0) continue;
                var batchTvModel = new TreeViewModel
                {
                    ParentId = order,
                    Id = batch.BatchId,
                    DisplayName = batch.FullBatchNumberWithDot
                };
                data.Add(batchTvModel);
                foreach (var item in items)
                {
                    data.Add(new TreeViewModel { ParentId = item.BatchId, Id = Utils.FillToTwoChars(item.ItemCode), DisplayName = item.FullItemNumberWithDotes });
                }
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tvwItems.Nodes.Add(rootNode);

            SetViewState(batchList, SessionConstants.BulkBatches);

            LoadPartTypes();
            LoadMeasures();

            //-- Load Measures
            LoadMeasuresField();
        }
        private BatchModel GetFirstBatch()
        {
            var batches = GetViewState(SessionConstants.BulkBatches) as List<BatchModel>;
            return batches == null ? null : batches[0];
        }

        #endregion

        #region Measures from View
        private void LoadMeasures()
        {
            var measures = QueryUtils.GetMeasures(GetFirstBatch(), false, this);
            measures = measures.FindAll(
                m => (m.MeasureClass == MeasureModel.MeasureClassEnum || m.MeasureClass == MeasureModel.MeasureClassNumeric) && m.MeasureId != 65);
            measures.Sort((m1, m2) => string.Compare(m1.MeasureName, m2.MeasureName));
            SetViewState(measures, SessionConstants.BulkMeasures);
        }
        private List<MeasureModel> GetMeasuresByPartFromView(string partId)
        {
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel> ?? new List<MeasureModel>();
            return measures.FindAll(m => ""+m.PartId == partId);
        }
        private MeasureModel GetMeasureFromView(string partId,string  measureId) 
        {
            return GetMeasuresByPartFromView(partId).Find(m => ""+m.MeasureId == measureId);
        }
        #endregion

        #region Enum Measures
        private void LoadEnumMeasures()
        {
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.CpCommonEnumMeasures);
        }
        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }
        private List<EnumMeasureModel> GetEnumMeasure(string measureId)
        {
            return GetEnumMeasure().FindAll(m => ""+m.MeasureValueMeasureId == measureId);
        }
        #endregion


        #region Not Used
        protected void TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            
        }

        protected void OnTreeSelectedChanged(object sender, EventArgs e)
        {
            

        }

        #endregion

        #region Add Measures Button
        protected void OnAddMeasureImgClick(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            OnAddMeasureClick(null, null);
        }


        protected void OnAddMeasureClick(object sender, EventArgs e)
        {
            var forAdding = GetCheckedMeasuresForAdd();
            if (forAdding.Count == 0)
            {
                PopupInfoDialog("Checked Measures Not Found!", "Add Measures", true);
                return;
            }
            //-- Create new rows
            var addItems = new List<AutoMeasModel>();
            var partModel = new PartModel { PartName = PartsListField.SelectedItem.Text, PartId = Convert.ToInt32(PartsListField.SelectedValue) };
            foreach (var measure in forAdding)
            {
                if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enums = GetEnumMeasure(""+measure.MeasureId);
                    addItems.Add(new AutoMeasModel(measure, partModel, enums));
                }
                else
                {
                    addItems.Add(new AutoMeasModel(measure, partModel, new List<EnumMeasureModel>()));
                }
            }
            addItems.AddRange(GetDataFromRuleGrid());
            addItems.Sort(new AutoMeasComparer());
            
            RulesGrid.DataSource = addItems;
            RulesGrid.DataBind();
        }
        #endregion

        #region Part Types Combo by Batch number
        private void LoadPartTypes()
        {
            var order = Utils.FillToFiveChars("" + Utils.ParseOrderCode(BatchNumberFld.Text));
            var batch = Utils.FillToThreeChars("" + Utils.ParseBatchCode(BatchNumberFld.Text), order);
            var parts = QueryUtils.GetPartsMeasureMulti(order, batch, this);
            MultiParts.Checked = false;
            MultiParts.Enabled = false;
            foreach (var part in parts)
            {
                if (part.PartName.ToLower() == "diamond 2")
                {
                    MultiParts.Enabled = true;
                    SetViewState(parts, SessionConstants.BulkPartsMulti);
                    break;
                }
            }
            PartsListField.DataSource = parts;
            PartsListField.DataBind();
            if (parts.Count > 0)
            {
                PartsListField.SelectedValue = "" + parts[0].PartId;
                OnPartChanged(null, null);
            }
        }
        protected void OnPartChanged(object sender, EventArgs e)
        {
            LoadMeasuresField();
        }
        private PartModel GetPartModel(string partId)
        {
            var partItem = PartsListField.Items.FindByValue(partId);
            if (partItem != null)
            {
                return new PartModel { PartId = Convert.ToInt32(partId), PartName = partItem.Text };
            }
            return null;
        }
        #endregion

        #region Measures Grid per selected PartType
        private void LoadMeasuresField()
        {
            var partId = PartsListField.SelectedValue;
            if (string.IsNullOrEmpty(partId))
            {
                grdMeasure.DataSource = null;
                grdMeasure.DataBind();
            }
            else
            {
                var measures = GetMeasuresByPartFromView(partId);
                grdMeasure.DataSource = measures;
                grdMeasure.DataBind();
            }
        }
        private List<MeasureModel> GetCheckedMeasuresForAdd()
        {
            var partId = PartsListField.SelectedValue;
            var measures = GetMeasuresByPartFromView(partId);
            var forAddItems = new List<MeasureModel>();
            var addedItems = GetAddedMeasures(partId);
            foreach (DataGridItem item in grdMeasure.Items)
            {
                var checkBox = item.FindControl("Checkbox1") as CheckBox;
                if (checkBox == null || !checkBox.Checked) continue;
                var measureId = "" + grdMeasure.DataKeys[item.ItemIndex];
                var forAdd = measures.Find(m => "" + m.MeasureId == measureId);
                if (forAdd != null && !addedItems.Contains("" + forAdd.MeasureId)) forAddItems.Add(forAdd);
            }
            return forAddItems;
        }
        #endregion

        #region Rule Grid
        private List<AutoMeasModel> GetDataFromRuleGrid()
        {
            var data = new List<AutoMeasModel>();
            foreach (DataGridItem item in RulesGrid.Items)
            {
                var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
                var keys = uniqueKey.Split(';');
                var partId = keys[0];
                var measureId = keys[1];
                var measure = GetMeasureFromView(partId, measureId);
                var enumMeas = measure.IsEnum ? GetEnumMeasure(measureId) : new List<EnumMeasureModel>();
                var part = GetPartModel(partId);
                var autoMeas = new AutoMeasModel(measure, part, enumMeas);

                //-- Values From, To
                if (measure.IsEnum)
                {
                    var fromFld = item.FindControl(RuleEnumFldMin) as DropDownList;
                    var toFld = item.FindControl(RuleEnumFldMax) as DropDownList;
                    if (fromFld != null) autoMeas.FromValue = fromFld.SelectedValue;
                    if (toFld != null) autoMeas.ToValue = toFld.SelectedValue;

                }
                else 
                {
                    var fromFld = item.FindControl(RuleNumericFldMin) as TextBox;
                    var toFld = item.FindControl(RuleNumericFldMax) as TextBox;
                    if (fromFld != null) autoMeas.FromValue = fromFld.Text;
                    if (toFld != null) autoMeas.ToValue = toFld.Text;
                }
                data.Add(autoMeas);
            }
            return data;
        }
        private List<string> GetAddedMeasures(string partId)
        {
            var ids = new List<string>();
            foreach (DataGridItem item in RulesGrid.Items)
            {
                var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
                var keys = uniqueKey.Split(';');
                if (keys[0] == partId) ids.Add(keys[1]);
            }
            return ids;
        }
        private const string RuleNumericFldMin = "RuleMinNumericFld";
        private const string RuleNumericFldMax = "RuleMaxNumericFld";
        private const string RuleEnumFldMin = "RuleMinEnumFld";
        private const string RuleEnumFldMax = "RuleMaxEnumFld";
        protected void OnRuleItemDataBound(object sender, DataGridItemEventArgs e) 
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rule = e.Item.DataItem as AutoMeasModel;
                if (rule == null) return;
                var numFldMin = e.Item.FindControl(RuleNumericFldMin) as TextBox;
                var numFldMax = e.Item.FindControl(RuleNumericFldMax) as TextBox;
                var enumFldMin = e.Item.FindControl(RuleEnumFldMin) as DropDownList;
                var enumFldMax = e.Item.FindControl(RuleEnumFldMax) as DropDownList;
                if (numFldMin == null || numFldMax == null || enumFldMin == null || enumFldMax == null) return;
                var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
                numFldMin.Visible = isNumeric;
                numFldMax.Visible = isNumeric;
                enumFldMin.Visible = !isNumeric;
                enumFldMax.Visible = !isNumeric;
                if (!isNumeric)
                {
                    enumFldMax.DataSource = rule.EnumSource;
                    enumFldMax.DataBind();
                    enumFldMin.DataSource = rule.EnumSource;
                    enumFldMin.DataBind();
                    enumFldMax.SelectedValue = rule.ToValue;
                    enumFldMin.SelectedValue = rule.FromValue;

                }
                else
                {
                    numFldMax.Text = rule.ToValue;
                    numFldMin.Text = rule.FromValue;
                }
            }
        }

        protected void OnRuleResetBtnClick(object sender, EventArgs e)
        {
            var grItem = ((LinkButton)sender).NamingContainer as DataGridItem;
            var uniqueKey = "" + RulesGrid.DataKeys[grItem.ItemIndex];
            var dataRule = GetDataFromRuleGrid();
            var item = dataRule.Find(m => m.UniqueKey == uniqueKey);
            if (item != null)
            {
                dataRule.Remove(item);
                RulesGrid.DataSource = dataRule;
                RulesGrid.DataBind();
            }
        }
        #endregion
        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
            
        }
        #endregion

        #region Random
        private void SetRandomValues(AutoMeasModel ruleModel, int count)
        {
            if (!ruleModel.HasValues) return;
            if (ruleModel.IsEnum)
            {
                var enums = GetEnumMeasure(ruleModel.MeasureId);
                var fromVal = enums.Find(m => "" + m.MeasureValueId == ruleModel.FromValue);
                var toVal = enums.Find(m => "" + m.MeasureValueId == ruleModel.ToValue);

                var n1 = enums.IndexOf(fromVal);
                var n2 = enums.IndexOf(toVal);
                var randIndexes = new List<int>();
                if (n1 < n2) randIndexes = GetRandomInt(n1, n2, count);
                if (n2 < n1) randIndexes = GetRandomInt(n2, n1, count);
                if (n1 == n2) randIndexes.Add(n1);
                var randValues = new List<string>();
                foreach (var index in randIndexes)
                {
                    randValues.Add(""+enums[index].MeasureValueId);
                }
                ruleModel.RandValues = randValues;
            }
            else
            {
                float fn1 = 0;
                float fn2 = 0;
                try
                {
                    fn1 = float.Parse(ruleModel.FromValue);
                    fn2 = float.Parse(ruleModel.ToValue);
                    var n1 = (int)(fn1 * 100);
                    var n2 = (int)(fn2 * 100);
                    
                    var randInt = new List<int>();
                    if (n1 < n2) randInt = GetRandomInt(n1, n2, count);
                    if (n2 < n1) randInt = GetRandomInt(n2, n1, count);
                    if (n1 == n2) randInt.Add(n1);

                    var randFloat = new List<string>();
                    foreach (var intVal in randInt)
                    {
                        randFloat.Add((((float)intVal) / 100).ToString());
                    }
                    ruleModel.RandValues = randFloat;
                }
                catch (Exception x)
                {
                    var msg = x.Message;
                }
            }
        }
        private static List<int> GetRandomInt(int min, int max, int count)
        {
            var randList = new List<int>();
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                randList.Add(random.Next(min, max));
            }
            int randomNumber = random.Next(min, max);
            return randList;
        }
        #endregion
        protected void MultiParts_CheckedChanged(object sender, EventArgs e)
        {
            if (MultiParts.Checked)
            {
                MultiParts.BackColor = System.Drawing.Color.Pink;
                MultiPartsList.Visible = true;
                var rulesData = GetDataFromRuleGrid().FindAll(m => m.HasValues);
                foreach (var rule in rulesData)
                {
                    var partsMulti = GetViewState(SessionConstants.BulkPartsMulti) as List<PartModelMulti>;
                    int partTypeId = partsMulti.Find(m => m.PartName.ToLower() == rule.PartName.ToLower()).PartTypeId;
                    //if (rule.PartName.ToLower() == "diamond 1")
                    if (partTypeId == 1)//diamonds
                    {
                        foreach (var part in partsMulti)
                        {
                            if (part.PartTypeId == partTypeId)
                                MultiPartsList.Items.Add(part.PartName);
                        }
                        break;
                    }
                }
            }
            else
            {
                MultiParts.BackColor = System.Drawing.Color.LightBlue;
                MultiPartsList.Visible = false;
                MultiPartsList.Items.Clear();
            }
        }
        protected bool ValidateMultiBatch(string batchNumber, string oldItemTypeId)
        {
            var order = "" + Utils.ParseOrderCode(batchNumber);
            order = Utils.FillToFiveChars(order);
            var batch = Utils.ParseBatchCode(batchNumber).ToString();
            batch = Utils.FillToThreeChars(batch, order);
            var batchList = QueryUtils.GetSimilarBatches(order, "" + Utils.ParseBatchCode(batchNumber), this);
            if (batchList != null && batchList.Count > 0)
            {
                string newPartTypeId = batchList[0].ItemTypeId.ToString();
                if (newPartTypeId == oldItemTypeId)
                    return true;
            }
            return false;
        }

        protected void DeleteCheckedItemsClick(object sender, EventArgs e)
        {
            List<TreeNode> checkedItems = new List<TreeNode>();
            List<string> parentBatchList = new List<string>();
            //String parentBatch = "";
            for (int i = 0; i < tvwItems.CheckedNodes.Count; i++)
            {
                TreeNode checkedNode = tvwItems.CheckedNodes[i];
                if (checkedNode.Depth == 1)//batch
                {
                    //parentBatch = checkedNode.Text;
                    parentBatchList.Add(checkedNode.Text);
                }
                if (checkedNode.Depth == 2)//items
                {
                    checkedItems.Add(checkedNode);
                }
            }
            if (checkedItems.Count == 0)
                return;
            try
            {
                //foreach (TreeNode rootNode in tvwItems.Nodes)
                for (int i = 0; i <= tvwItems.Nodes.Count; i++)
                {
                    TreeNode rootNode = tvwItems.Nodes[i];
                    if (rootNode == null)
                        return;
                    //foreach (TreeNode node in rootNode.ChildNodes)
                    for (int j=0; j<= rootNode.ChildNodes.Count-1; j++)
                    {
                        TreeNode node = rootNode.ChildNodes[j];
                        var match = parentBatchList.Find(x => x.Contains(node.Text));
                        if (match != null)
                        {
                            var childNodes = node.ChildNodes;
                            foreach (TreeNode checkedItem in checkedItems)
                            {
                                childNodes.Remove(checkedItem);
                            }
                            if (childNodes.Count == 0)
                            {
                                node.Checked = false;
                                rootNode.ChildNodes.Remove(rootNode.ChildNodes[j]);
                                j = j - 1;
                            }
                            if (rootNode.ChildNodes.Count == 0)
                            {
                                tvwItems.Nodes.Remove(tvwItems.Nodes[i]);
                                i = i - 1;
                            }
                        }
                    }
                }
            }
            catch (ArgumentNullException ex1)
            {
                string msg = ex1.Message;
                PopupInfoDialog("Error deleting batch", "Save", !string.IsNullOrEmpty(msg));
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}