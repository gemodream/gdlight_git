<%@ Page language="c#" Codebehind="CustomerVendorCpOld.aspx.cs" AutoEventWireup="True" Inherits="Corpt.CustomerVendorCpOld" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerVendorCpOld</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/main.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:DropDownList id="lstCustomer" style="Z-INDEX: 100; LEFT: 48px; POSITION: absolute; TOP: 104px"
				runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstCustomer_SelectedIndexChanged"></asp:DropDownList>
			<asp:button id="cmdHome" style="Z-INDEX: 111; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
				CssClass="buttonStyle" Height="16px" Width="16px" Text="H" onclick="cmdHome_Click"></asp:button>
			<asp:DropDownList id="lstVendor" 
                style="Z-INDEX: 101; LEFT: 48px; POSITION: absolute; TOP: 48px; right: 976px;" runat="server"
				CssClass="inputStyleCC5" AutoPostBack="True" 
                onselectedindexchanged="lstVendor_SelectedIndexChanged"></asp:DropDownList>
			<asp:DataGrid id="grdCpList" style="Z-INDEX: 102; LEFT: 48px; POSITION: absolute; TOP: 208px"
				runat="server" CssClass="text"></asp:DataGrid>
			<asp:Label id="Label1" style="Z-INDEX: 103; LEFT: 48px; POSITION: absolute; TOP: 80px" runat="server"
				CssClass="text">Customer</asp:Label>
			<asp:Label id="Label2" style="Z-INDEX: 104; LEFT: 48px; POSITION: absolute; TOP: 24px" runat="server"
				CssClass="text" Height="8px">Vendor</asp:Label>
			<asp:Button id="cmdShowConnected" style="Z-INDEX: 105; LEFT: 192px; POSITION: absolute; TOP: 144px"
				runat="server" CssClass="buttonStyle" Text="Show Connected" Width="106px" onclick="cmdShowConnected_Click"></asp:Button>
			<asp:Button id="cmdConnect" style="Z-INDEX: 106; LEFT: 584px; POSITION: absolute; TOP: 48px"
				runat="server" CssClass="buttonStyle" Text="Connect" Enabled="False" onclick="cmdConnect_Click"></asp:Button>
			<asp:DropDownList id="cmdCustomerProgramList" style="Z-INDEX: 107; LEFT: 320px; POSITION: absolute; TOP: 48px"
				runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="cmdCustomerProgramList_SelectedIndexChanged"></asp:DropDownList>
			<asp:Label id="Label3" style="Z-INDEX: 109; LEFT: 320px; POSITION: absolute; TOP: 24px" runat="server"
				CssClass="text">Customer Program</asp:Label>
			<asp:Label id="lblMessage" style="Z-INDEX: 110; LEFT: 320px; POSITION: absolute; TOP: 88px"
				runat="server" CssClass="text" Width="240px" Font-Bold="True"></asp:Label>
		</form>
	</body>
</HTML>
