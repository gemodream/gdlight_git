﻿using System;
using System.Data;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	public partial class ScreeningUserView : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if (!IsPostBack)
				FillGrid();
		}
		public void FillGrid()
		{
			int UserId =Convert.ToInt32(Session["ID"].ToString());
			DataSet dsOrderAlloted = SyntheticScreeningUtils.GetSyntheticOrderByUserID(UserId, this);
			if (dsOrderAlloted != null && dsOrderAlloted.Tables[0].Rows.Count != 0)
			{
				dtgListAcceptQty.DataSource = dsOrderAlloted.Tables[0];
				dtgListAcceptQty.DataBind();
				dvEmptyRecord.Visible = false;
				tblAssignmentDetail.Visible = true;
				dvAllocationHeader.Visible = true;
			}
			else
			{
				dtgListAcceptQty.DataSource = null;
				dtgListAcceptQty.DataBind();
				dvEmptyRecord.Visible = true;
				tblAssignmentDetail.Visible = false;
				dvAllocationHeader.Visible = false;
			}
			
		}
		public void LoadStatus(int StatusID)
		{
			DataTable dtStatus = SyntheticScreeningUtils.GetSyntheticOrderStatus(0,this).Tables[0]; // set 2 for Process
			DataTable dtFilterStatus = dtStatus.Select("StatusID="+ (StatusID+1)).CopyToDataTable();
			ddlStatus.DataSource = dtFilterStatus;
			ddlStatus.DataTextField = "StatusName";
			ddlStatus.DataValueField = "StatusID";
			ddlStatus.DataBind();
		}
		protected void dtgListSelectQty_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtAssignmentId.Text = dtgListAcceptQty.SelectedRow.Cells[1].Text;
			txtOrderCode.Text = dtgListAcceptQty.SelectedRow.Cells[2].Text;
			txtBatchCode.Text = dtgListAcceptQty.SelectedRow.Cells[3].Text;

			txtAssignedQty.Text = dtgListAcceptQty.SelectedRow.Cells[4].Text;
			txtAssignDate.Text = dtgListAcceptQty.SelectedRow.Cells[5].Text;
			txtAssignedBy.Text = dtgListAcceptQty.SelectedRow.Cells[6].Text;
			txtStatus.Text = dtgListAcceptQty.SelectedRow.Cells[7].Text;

			int StatusID = Convert.ToInt16(dtgListAcceptQty.SelectedRow.Cells[8].Text);
			txtStyleName.Text = dtgListAcceptQty.SelectedRow.Cells[9].Text;
			txtStyleQty.Text = dtgListAcceptQty.SelectedRow.Cells[10].Text;
			hdnStatusID.Value = StatusID.ToString();

			LoadStatus(StatusID);
			txtComment.Enabled = true;
			ddlStatus.Enabled = true;
			btnUpdate.Enabled = true;
			btnClear.Enabled = true;
			lblMsg.Text = "";
			if(StatusID==7)
			{
				fsScreener.Visible = true;
			}
			else
			{
				fsScreener.Visible = false;
			}
			if (StatusID == 10)
			{
				fsTester.Visible = true;
			}
			else
			{
				fsTester.Visible = false;
			}


			int OrderCode = Convert.ToInt32(txtOrderCode.Text.ToString());
			DataSet dtOrderStatus= SyntheticScreeningUtils.GetSyntheticOrderStatusByOrderCode(OrderCode, "", this);
			if (dtOrderStatus != null)
			{
				txtComment.Text = dtOrderStatus.Tables[0].Rows[0]["Comment"].ToString();
			}

		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			ClearForm();
			lblMsg.Text = "";
		}
		private void ClearForm()
		{
			txtOrderCode.Text = "";
			txtBatchCode.Text = "";
			txtStyleName.Text = "";
			txtStyleQty.Text = "";
			txtAssignedQty.Text = "";
			txtAssignDate.Text = "";
			txtAssignedBy.Text = "";
			txtComment.Text = "";
			txtAssignmentId.Text = "";
			txtStatus.Text = "";
			txtScreenerFailItems.Text = "0";
			txtScreenerPassItems.Text = "0";
			txtTesterSyntheticItems.Text = "0";
			txtTesterPassItems.Text = "0";
			txtTesterSuspectItems.Text = "0";

			ddlStatus.Items.Clear();
			ddlStatus.Enabled = false;
			btnUpdate.Enabled = false;
			btnClear.Enabled = false;
			txtComment.Enabled = false;
			fsScreener.Visible = false;
			fsTester.Visible = false;
		}
		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}
		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			try
			{
				if(Convert.ToInt32(ddlStatus.SelectedItem.Value)-1 == 7 &&(Convert.ToInt16(txtScreenerPassItems.Text)+ Convert.ToInt16(txtScreenerFailItems.Text) != Convert.ToInt32(txtAssignedQty.Text)))
				{
					PopupInfoDialog("Screener Pass and Fail Item Total Should match to Assigned Qty", true);
					return;
				}
				if (Convert.ToInt32(ddlStatus.SelectedItem.Value) - 1 == 10 && (Convert.ToInt16(txtTesterSyntheticItems.Text) + Convert.ToInt16(txtTesterSuspectItems.Text) + Convert.ToInt16(txtTesterPassItems.Text) != Convert.ToInt32(txtAssignedQty.Text)))
				{
					PopupInfoDialog("Tester Synhtetic ,Suspect  and Pass Item Total Should match to Assigned Qty", true);
					return;
				}
				SyntheticOrderHistoryModel objSyntheticOrder = new SyntheticOrderHistoryModel();
				objSyntheticOrder.OrderCode = Convert.ToInt32(dtgListAcceptQty.SelectedRow.Cells[2].Text);
				objSyntheticOrder.BatchCode = Convert.ToInt32(dtgListAcceptQty.SelectedRow.Cells[3].Text);
				objSyntheticOrder.AssignedTo = Convert.ToInt32(Convert.ToInt16(Session["ID"].ToString()));
				objSyntheticOrder.StatusId = Convert.ToInt32(ddlStatus.SelectedItem.Value);
				objSyntheticOrder.ItemQty = Convert.ToInt32(txtAssignedQty.Text);
				string msg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);
				string Comment = txtComment.Text.ToString().Trim();
				bool msgComment = SyntheticScreeningUtils.SetSyntheticOrderComment(objSyntheticOrder.OrderCode, Comment, this);
				InsertSyntheticBatch();
				FillGrid();
				ClearForm();
				lblMsg.Text = "Status Update Successfully";
			}
			catch(Exception ex)
			{
				lblMsg.Text = ex.Message.ToString();
				lblMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		public void InsertSyntheticBatch()
		{
			try
			{
				SyntheticBatchModel objSyntheticBatchModel = new SyntheticBatchModel();
				objSyntheticBatchModel.BatchCode = Convert.ToInt32(txtBatchCode.Text);
				objSyntheticBatchModel.OrderCode = Convert.ToInt32(txtOrderCode.Text);
				if (hdnStatusID.Value == "7")
				{
					objSyntheticBatchModel.ScreenerID = Convert.ToInt32(Session["ID"].ToString());
				}
				else
				{
					objSyntheticBatchModel.ScreenerID = 0;
				}
				objSyntheticBatchModel.ScreenerPassItems = Convert.ToInt32(txtScreenerPassItems.Text);
				objSyntheticBatchModel.ScreenerFailItems =  Convert.ToInt32(txtScreenerFailItems.Text);
				if (hdnStatusID.Value == "10")
				{
					objSyntheticBatchModel.TesterID = Convert.ToInt32(Session["ID"].ToString());
				}
				else
				{
					objSyntheticBatchModel.TesterID = 0;
				}
				objSyntheticBatchModel.TesterSyntheticStones =  Convert.ToInt32(txtTesterSyntheticItems.Text);
				objSyntheticBatchModel.TesterSuspectStones = Convert.ToInt32(txtTesterSuspectItems.Text);
				objSyntheticBatchModel.TesterPassItems =  Convert.ToInt32(txtTesterPassItems.Text);
				bool msgComment = SyntheticScreeningUtils.SetSyntheticBatch(objSyntheticBatchModel, this);
			}
			catch (Exception ex)
			{
				lblMsg.Text = ex.Message.ToString();
				lblMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		
	}
}