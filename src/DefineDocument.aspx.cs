﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DefineDocument : CommonPage
    {
        public static string ParamDocTypeCode = "DocTypeCode";
        public static string ParamCpKey = "CpKey"; //-CPOfficeID_CPID
        public static string ParamCpDocId = "CpDocId";
        public static string ParamOperationTypeId = "OperationTypeId";
        public static string ParamDebug = "Debug";
        private const string CmdAddValue = "CmdAddValue";
        private const string CmdAddTitle = "CmdAddTitle";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack)
            {
                var initData = GetViewState(SessionConstants.DdInitData) as DefineDocumentInitModel;
                if (initData != null)
                {
                     Page.Title = PageConstants.AppTitle + initData.DocumentType.DocumentTypeName;
                }
                PostBackHandler();
                return;
            }
            MeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(MeasureList, CmdAddValue));
            TitleList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(MeasureList, CmdAddTitle));
            IniData();
        }

        #region PostBackHandler
        private void PostBackHandler()
        {
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddValue)
            {
                OnInsertValueEvent();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddTitle)
            {
                OnInsertTitle();
            }

        }
        #endregion

        private const string ValueField = "ValueField";
        private const string TitleField = "TitleField";
        private const string UnitField = "UnitField";
        private const string SelectCheckBox = "SelectCheckBox";

        #region Insert Value, Insert Title
        private void OnInsertValueEvent()
        {
            var part = PartTree.SelectedNode == null ? "" : PartTree.SelectedNode.Text.Trim();
            var measure = MeasureList.SelectedItem == null ? "" : MeasureList.SelectedItem.Text.Trim();
            var documentId = DocumentList.SelectedValue;
            if (string.IsNullOrEmpty(part) || string.IsNullOrEmpty(measure) || string.IsNullOrEmpty(documentId)) return;
            DataGridItem currItem = GetSelectedGridItem();
            if (currItem == null) return;
            var valueFld = currItem.FindControl(ValueField) as TextBox;
            if (valueFld != null)
            {
                valueFld.Text = SeparatorModel.AddFormatingValue(valueFld.Text, part, measure,
                                                                 SeparatorList.SelectedValue);
            }
        }
        private void OnInsertTitle()
        {
            var title = TitleList.SelectedItem == null ? "" : TitleList.SelectedItem.Text.Trim();
            var documentId = DocumentList.SelectedValue;
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(documentId)) return;
            var currItem = GetSelectedGridItem();
            if (currItem == null) return;
            var titleFld = currItem.FindControl(TitleField) as TextBox;
            if (titleFld != null)
            {
                titleFld.Text = SeparatorModel.AddFormatingTitle(titleFld.Text, title,
                                                                 SeparatorList.SelectedValue);
            }
        }
        private DataGridItem GetSelectedGridItem()
        {
            foreach (DataGridItem item in ValuesGrid.Items)
            {
                var chBox = item.FindControl(SelectCheckBox) as CheckBox;
                if (chBox == null) return null;
                if (!chBox.Checked) continue;
                return item;
            }
            return null;
        }
        #endregion

        #region Init Data
        private void IniData()
        {
            //-- Customer Program
            var showDebug = Request.Params[ParamDebug] != null;
            DownloadBtn.Visible = showDebug;

            var cpKey = Request.Params[ParamCpKey];
            var cpOfficeId = cpKey.Split('_')[0];
            var cpId = cpKey.Split('_')[1];
            var cpDocId = Request.Params[ParamCpDocId];
            var docTypeCode = Request.Params[ParamDocTypeCode];
            var operationTypeId = Request.Params[ParamOperationTypeId];
            if (string.IsNullOrEmpty(cpId) || string.IsNullOrEmpty(cpOfficeId) || string.IsNullOrEmpty(docTypeCode)) return;
            var initData = QueryCpUtils.GetDefineDocumentInit(cpId, cpOfficeId, docTypeCode, cpDocId, this);
            SetViewState(initData, SessionConstants.DdInitData);
            
            ShowPicture(initData.Cp.Path2Picture);

            //-- Languages
            LanguageList.DataSource = QueryCpUtils.GetLanguages(this);
            LanguageList.DataBind();
            LanguageList.SelectedIndex = 0;

            //-- Title Reference
            OnLanguageChanged(null, null);

            //-- Parts Tree
            LoadPartsTree(initData.MeasureParts);
            PageTitleLabel.Text = string.Format("Define Document - Document Type: {0}, SKU: {1}", 
                initData.DocumentType.DocumentTypeName,
                initData.Cp.CustomerProgramName);
            Page.Title = PageConstants.AppTitle + initData.DocumentType.DocumentTypeName;
            //-- Import List
            ImportList.DataSource = initData.ImportInfo;
            ImportList.DataBind();

            //-- Export List
            ExportList.DataSource = initData.ExportInfo;
            ExportList.DataBind();

            //-- File Formats 
            FormatList.DataSource = initData.FormatInfo;
            FormatList.DataBind();

            //-- Documents
            LoadCorelFilesList();
            LoadDocumentsList(operationTypeId);

            //-- Separator List
            SeparatorList.DataSource = SeparatorModel.GetSeparatorList();
            SeparatorList.DataBind();
            SeparatorList.SelectedValue = "1";
        }
        private DefineDocumentInitModel GetInitDataFromView()
        {
            return GetViewState(SessionConstants.DdInitData) as DefineDocumentInitModel ?? new DefineDocumentInitModel();
        }
        #endregion

        #region Cp Picture
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exists";
            }

            ErrPictureField.Text = errMsg;

        }

        #endregion

        #region Save, Update, Attach, Clear Buttons
        protected void OnSaveAsClick(object sender, EventArgs e)
        {


        }
        protected void OnSaveClick(object sender, EventArgs e)
        {
            Page.Validate("DocumentValidatorGrp");
            if (!Page.IsValid)
            {
                PopupInfoDialog("Not all requirements are satisfied for documents", "Save",true);
                return;
            }
            var msg = QueryCpUtils.UpdateDefineDocument(GetDocumentDetails(), this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Save document", true);
            } else
            {
                PopupInfoDialog("Document was successfully updated.", "Save document", false); 
            }
        }
        private DocPredefinedModel GetDocumentDetails()
        {
            var document = new DocPredefinedModel
            {
                DocumentId = DocumentList.SelectedValue,
                DocumentName = DocumentList.SelectedItem.Text,
                UseDate = UseDateCb.Checked,
                UseVvNumber = UseVvNumberCb.Checked,
                BarCodeFixedText = BarCodeOptionList.SelectedIndex == 0 ? "" : FixedTextFld.Text,
                ImportTypeId = ImportList.SelectedValue,
                ExportTypeId = ExportList.SelectedValue,
                FormatTypeId = FormatList.SelectedValue,
                CorelFile = CorelFilesList.SelectedValue,
                DocumentTypeCode = GetInitDataFromView().DocumentType.DocumentTypeCode,
                DocumentValues = GetValuesFromGrid(DocumentList.SelectedValue, false)
            };
            return document;
        }
        protected void OnAttachClick(object sender, EventArgs e)
        {
            var document = GetCurrDocument();
            if (document.IsAttachedMyCp != "N")
            {
                UpdateStateSaveButtons();
                return;
            }
            var initData = GetInitDataFromView();
            var msg = QueryCpUtils.AttachDocumentToCp(document.DocumentId, initData.Cp.CpId, initData.Cp.CpOfficeId, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Attach document", true);
            } else
            {
                
                AttachedRefresh();
                PopupInfoDialog("Document was successfully attached.", "Attach document", false);
            }

        }
        private void AttachedRefresh()
        {
            var document = GetCurrDocument();
            var initData = GetInitDataFromView();
            var attached = QueryCpUtils.IsAttachedDocument(document.DocumentId, initData.CpId, initData.CpOfficeId,
                                                           this);
            document.IsAttachedMyCp = (attached ? "Y" : "N");
            UpdateStateSaveButtons();
        }
        protected void OnNewClick(object sender, EventArgs e)
        {

        }
        #endregion

        #region Part Tree
        private void LoadPartsTree(IEnumerable<MeasurePartModel> parts)
        {
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PartTree.Nodes.Clear();
            PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Select();
            OnPartsTreeChanged(null, null);
        }

        protected void OnPartsTreeChanged(object sender, EventArgs e)
        {
            var selNode = PartTree.SelectedNode;
            if (selNode == null || string.IsNullOrEmpty(selNode.Value))
            {
                MeasureList.Items.Clear();
                return;
            }
            var partId = selNode.Value;
            var initData = GetInitDataFromView();
            var measures = initData.MeasuresByParts.FindAll(m => ""+m.PartId == partId);
            measures.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureName.ToUpper(), m2.MeasureName.ToUpper()));
            MeasureList.DataSource = measures;
            MeasureList.DataBind();
        }

        #endregion

        #region Languages and DocumentTitles
        protected void OnLanguageChanged(object sender, EventArgs e)
        {
            var titles = QueryCpUtils.GetDocumentTitles(LanguageList.SelectedValue, this);
            TitleList.DataSource = titles;
            TitleList.DataBind();

        }
        #endregion

        #region Documents
        private void LoadCorelFilesList()
        {
            string errMsg;
            var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                RepDirLabel.Text = errMsg;
            } else
            {
                RepDirLabel.Text = "";
            }
            var documents = GetInitDataFromView().Documents;
            foreach(var doc in documents)
            {
                if (doc.CorelFile != null && doc.CorelFile != "")
                    doc.CorelFile = (doc.CorelFile).ToLower();
                if (existsFiles.Find(m=>m.Name == doc.CorelFile) == null)
                {
                    existsFiles.Add(new CorelFileModel{Name = doc.CorelFile, Exists = false});
                }
            }
            CorelFilesList.DataSource = existsFiles;
            CorelFilesList.DataBind();
        }
        private void LoadDocumentsList(string operationTypeId)
        {
            var data = GetInitDataFromView();
            
            DocumentList.DataSource = data.Documents;
            DocumentList.DataBind();
            if (!string.IsNullOrEmpty(operationTypeId))
            {
                var initDoc = data.Documents.Find(m => m.OperationTypeId == operationTypeId);
                if (initDoc != null)
                {
                    DocumentList.SelectedValue = initDoc.DocumentId;
                    OnDocumentChanged(null, null);
                    return;
                }
            }
            if (DocumentList.SelectedItem != null)
            {
                OnDocumentChanged(null, null);
                return;
            }
            if (DocumentList.SelectedItem == null && data.Documents.Count > 0)
            {
                DocumentList.SelectedIndex = 0;
                OnDocumentChanged(null, null);
            }
        }
        protected void OnDocumentChanged(object sender, EventArgs e)
        {
            SetDocumentDetails();
        }
        private void UpdateStateSaveButtons()
        {
            var document = GetCurrDocument();
            AttachBtn.Enabled = (document != null && document.DocumentId != "0" && document.IsAttachedMyCp != "Y");
            SaveBtn.Enabled = (document != null && document.DocumentId != "0");
            SaveAsBtn.Enabled = (document != null);
        }
        private void SetDocumentDetails()
        {

            var document = GetCurrDocument();
            if (document == null)
            {
                UpdateStateSaveButtons();
                return;
            }
            
            ImportList.SelectedValue = document.ImportTypeId;
            ExportList.SelectedValue = document.ExportTypeId;
            FormatList.SelectedValue = document.FormatTypeId;
            CorelFilesList.SelectedValue = document.CorelFile;
            UseDateCb.Checked = document.UseDate;
            UseVvNumberCb.Checked = document.UseVvNumber;
            
            var barCodeIsFixed = !string.IsNullOrEmpty(document.BarCodeFixedText);
            FixedTextFld.Text = document.BarCodeFixedText;
            BarCodeOptionList.SelectedIndex = (barCodeIsFixed ? 1 : 0);
            OnBarCodeOptionChanged(null, null);

            if (document.DocumentValues.Count == 0)
            {
                document.DocumentValues =  QueryCpUtils.GetDocumentValues(document.DocumentId, this);
            }
            //-- Load Document Values
            LoadValuesGrid(document.DocumentValues);

            //-- Attached
            if (string.IsNullOrEmpty(document.IsAttachedMyCp))
            {
                AttachedRefresh();
            }
            UpdateStateSaveButtons();
        }
        private DocPredefinedModel GetCurrDocument()
        {
            var docId = DocumentList.SelectedValue;
            return GetInitDataFromView().Documents.Find(m => m.DocumentId == docId);
        }
        #endregion

        #region Document Values Grid
        private void LoadValuesGrid(List<DocumentValueModel> values)
        {
            values.Sort((m1, m2) => ComparerUtils.NumberCompare(m1.DocumentValueId, m2.DocumentValueId));
            ValuesGrid.DataSource = values;
            ValuesGrid.DataBind();
        }
        protected void OnValuesDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var valueModel = e.Item.DataItem as DocumentValueModel;
            if (valueModel == null) return;
            var document = GetCurrDocument();
            var minId = document.DocumentValues.Min(m => m.DocumentValueId);
            var maxId = document.DocumentValues.Max(m => m.DocumentValueId);
            var documentValueId = Convert.ToInt32(ValuesGrid.DataKeys[e.Item.ItemIndex]);

            var moveUpBtn = e.Item.FindControl("MoveUpBtn");
            if (moveUpBtn != null)
            {
                moveUpBtn.Visible = documentValueId != minId;
            }

            var moveDownBtn = e.Item.FindControl("MoveDownBtn");
            if (moveDownBtn != null)
            {
                moveDownBtn.Visible = documentValueId != maxId;
            }
            var selectFld = e.Item.FindControl(SelectCheckBox) as CheckBox;
            if (selectFld != null) selectFld.Checked = valueModel.Selected;

            var titleFld = e.Item.FindControl(TitleField) as TextBox;
            if (titleFld != null)
            {
                titleFld.Text = valueModel.Title;
                titleFld.Enabled = valueModel.Selected;
            }

            var valueFld = e.Item.FindControl(ValueField) as TextBox;
            if (valueFld != null)
            {
                valueFld.Text = valueModel.Value;
                valueFld.Enabled = valueModel.Selected;
            }

            var unitFld = e.Item.FindControl(UnitField) as TextBox;
            if (unitFld != null)
            {
                unitFld.Text = valueModel.Unit;
                unitFld.Enabled = valueModel.Selected;
            }

        }
        protected void OnCheckRowChanged(object sender, EventArgs e)
        {
            var chBox = sender as CheckBox;
            if (chBox == null) return;
            var dgItem = chBox.NamingContainer as DataGridItem;
            if (dgItem == null) return;
            var documentValueId = "" + ValuesGrid.DataKeys[dgItem.ItemIndex];

            var document = GetCurrDocument();
            if (document == null) return;

            var newValues = GetValuesFromGrid(document.DocumentId, false);
            var currValueModel = newValues.Find(m => "" + m.DocumentValueId == documentValueId);
            if (currValueModel != null) currValueModel.Selected = chBox.Checked;
            document.DocumentValues = newValues;
            LoadValuesGrid(newValues);

        }
        private List<DocumentValueModel> GetValuesFromGrid(string documentId, bool withSelected)
        {
            var result = new List<DocumentValueModel>();
            foreach (DataGridItem item in ValuesGrid.Items)
            {
                var documentValueId = ValuesGrid.DataKeys[item.ItemIndex];
                var valueModel = new DocumentValueModel { DocumentId = documentId, DocumentValueId = Convert.ToInt32(documentValueId) };

                var titleFld = item.FindControl(TitleField) as TextBox;
                if (titleFld != null)
                {
                    valueModel.Title = titleFld.Text;
                }

                var valueFld = item.FindControl(ValueField) as TextBox;
                if (valueFld != null)
                {
                    valueModel.Value = valueFld.Text;
                }

                var unitFld = item.FindControl(UnitField) as TextBox;
                if (unitFld != null)
                {
                    valueModel.Unit = unitFld.Text;
                }
                if (withSelected)
                {
                    var chBox = item.FindControl(SelectCheckBox) as CheckBox;
                    if (chBox != null) valueModel.Selected = chBox.Checked;
                }
                result.Add(valueModel);
            }
            return result;
        }

        #endregion


        #region Add Line button
        protected void OnAddLineClick(object sender, EventArgs e)
        {
            var document = GetCurrDocument();
            if (document == null) return;
            var maxId = (document.DocumentValues.Count == 0 ? 0 : document.DocumentValues.Max(m => m.DocumentValueId));
            var addLine = new DocumentValueModel { DocumentId = document.DocumentId, DocumentValueId = maxId + 1, Selected = true };
            var currValues = GetValuesFromGrid(document.DocumentId, false);
            currValues.Add(addLine);
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }
        #endregion

        #region Line Command: MoveUp, MoveDn, Delete, Insert
        private int GetCommandLineKey(object sender)
        {
            var btn = sender as ImageButton;
            if (btn == null) return 0;
            var dgItem = btn.NamingContainer as DataGridItem;
            if (dgItem == null) return 0;
            return Convert.ToInt32(ValuesGrid.DataKeys[dgItem.ItemIndex]);

        }
        protected void OnMoveUpRowClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var prevLine = currValues.Find(m => m.DocumentValueId == (lineKey - 1));
            var nowLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (prevLine != null) prevLine.DocumentValueId = prevLine.DocumentValueId + 1;
            if (nowLine != null) nowLine.DocumentValueId = nowLine.DocumentValueId - 1;
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnMoveDownRowClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var nextLine = currValues.Find(m => m.DocumentValueId == (lineKey + 1));
            var nowLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (nextLine != null) nextLine.DocumentValueId = nextLine.DocumentValueId - 1;
            if (nowLine != null) nowLine.DocumentValueId = nowLine.DocumentValueId + 1;
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnDelClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var delLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (delLine == null) return;
            currValues.Remove(delLine);
            var order = 0;
            foreach(var line in currValues)
            {
                order++;
                line.DocumentValueId = order;
            }
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnInsertClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var reorderValues = currValues.FindAll(m => m.DocumentValueId > lineKey);
            foreach(var line in reorderValues)
            {
                line.DocumentValueId++;
            }
            var insertLine = new DocumentValueModel
                                 {DocumentId = document.DocumentId, DocumentValueId = lineKey + 1, Selected = true};
            currValues.Add(insertLine);
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }
        #endregion

        protected void OnBarCodeOptionChanged(object sender, EventArgs e)
        {

            var isFixedOption = (BarCodeOptionList.SelectedIndex == 1);
            FixedTextFld.Enabled = isFixedOption;
            BarCodeTextValidator.Enabled = isFixedOption;
            if (!isFixedOption)
            {
                FixedTextFld.Text = "";
            }
        }

        #region Update Button
        #endregion

        protected void OnDownloadClick(object sender, EventArgs e)
        {
            var fileName = CorelFilesList.SelectedItem.Text.Trim();
            var dirSource = "" + Session[SessionConstants.GlobalRepDir] + @"CorelFiles\"; 
            if (!(dirSource.EndsWith("\\") || dirSource.EndsWith("/")))
                dirSource = dirSource + "/";
            //-- Exists dir
            try
            {
                var dir = new DirectoryInfo(Server.MapPath(dirSource));
                if (!dir.Exists) return;
            }
            catch (Exception ex)
            {

                return;
            }
            var webClient = new WebClient();
            webClient.DownloadFile("file://" + Server.MapPath(dirSource) + fileName, @"c:\temp\MyCorelFiles\" + fileName);

        }
        #region Save As dialog
        protected void OnSaveAsOkClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(NewReportName.Text.Trim()))
            {
                SaveAsPopupExtender.Show();
                return;
            }
            //-- dublicate
            var editData = GetInitDataFromView();
            var eqDocs = editData.Documents.FindAll(m => m.DocumentName == NewReportName.Text.Trim());
            if (eqDocs.Count > 0)
            {
                SaveAsPopupExtender.Show();
                SaveAsMessageLbl.Text = "Document with this name already exists!";
                return;
            }
            var saveDoc = GetDocumentDetails();
            saveDoc.DocumentName = NewReportName.Text.Trim();

            string docId;
            var msg = 
            QueryCpUtils.InsertDefineDocument(saveDoc, "" + editData.Cp.ItemTypeId, editData.Cp.CpOfficeId,
                                              out docId, this);
            if(!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Save as", true);
                return;
            }
            //-- Refresh Documents List
            
            editData.Documents = QueryCpUtils.GetDocumentsByItemTypeAndDocumentTypeCode(
                "" + editData.Cp.ItemTypeId, editData.DocumentType.DocumentTypeCode, this);
            DocumentList.DataSource = editData.Documents;
            DocumentList.DataBind();

            if (editData.Documents.Find(m => m.DocumentId == docId) != null)
            {
                DocumentList.SelectedValue = docId;    
            }
            if (DocumentList.SelectedItem != null)
            {
                OnDocumentChanged(null, null);
            }
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    }
}