﻿<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="Color2.aspx.cs" AutoEventWireup="True" Inherits="Corpt.Color2" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
	<style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
    </style>
	<script type="text/javascript">
		function FilterShapes(caller) {
			var textbox = caller;
			filter = textbox.value.toLowerCase();
			var dropDownArray = document.getElementsByTagName("select");
			var list = dropDownArray[1];
			for (var c = 0; c < list.options.length; c++) {
				if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
					list.options.item(c).style.display = "block";
				}
				else {
					list.options.item(c).style.display = "none";
				}
			}
		}
		function StayFocused() {
			var textbox = document.getElementById('<%=txtBatchNumber.ClientID%>');
			textbox.focus();
		}
		function OnSmBtnClickJS(caller) {
			var btn = caller;			   
			var elePanel = btn.parentElement.parentElement.parentElement;
			var eleChildren = elePanel.children;

			var eleEditing = document.getElementById('<%=ElementEditing.ClientID%>');
			eleEditing.value = eleChildren[0].value;

			var list = document.getElementById('<%=ColorRadio.ClientID%>');
			var inputs = list.getElementsByTagName("input");
			var catval;

			for (var i = 0; i < inputs.length; i++) {
				if (inputs[i].checked) {
					catval = inputs[i].value;
					break;
				}
			}

			if (eleEditing.value == "Color" || eleEditing.value == "SS Color" || eleEditing.value == "FL") {
				document.getElementById('<%=CategoryEditing.ClientID%>').value = catval;
			}
			else {
				document.getElementById('<%=CategoryEditing.ClientID%>').value = "Root";
			}

			SetButtonCategoryJS();
			StayFocused();
			return false;
		}
		function SetButtonCategoryJS(){
			var eleItems = document.getElementById('<%=MeasurementPanel.ClientID%>').children[0].children[4].children;
			var activePanel;
			var c;
			var activePanelExists = 0;
			for (c = 0; c < eleItems.length; c++) {
				if (eleItems[c].children[0].value == document.getElementById('<%=ElementEditing.ClientID%>').value) {
					eleItems[c].children[4].style.display = "";
					activePanel = eleItems[c].children[4];
					activePanelExists = 1;
				}
				else {
					eleItems[c].children[4].style.display = "none";
				}
			}
			if (activePanelExists) {
				var bigBtns = activePanel.children[2].children;
				for (c = 0; c < bigBtns.length; c++) {
					if (bigBtns[c].children[3].value == document.getElementById('<%=CategoryEditing.ClientID%>').value) {
						bigBtns[c].children[0].style.display = "";
					}
					else {									 
						bigBtns[c].children[0].style.display = "none";
					}
				}
			}
		}
		function ColorRadioChangedJS() {
			var eleEditing = document.getElementById('<%=ElementEditing.ClientID%>');

			var list = document.getElementById('<%=ColorRadio.ClientID%>');
			var inputs = list.getElementsByTagName("input");
			var catval;

			for (var i = 0; i < inputs.length; i++) {
				if (inputs[i].checked) {
					catval = inputs[i].value;
					break;
				}
			}

			if (eleEditing.value == "Color" || eleEditing.value == "SS Color" || eleEditing.value == "FL") {
				document.getElementById('<%=CategoryEditing.ClientID%>').value = catval;
				SetButtonCategoryJS();
			}

			StayFocused();
		}
		function OnButtonListClickJS(caller) {
			var btn = caller;
			var redir = btn.parentElement.children[2];
			var btnval = btn.parentElement.children[1];
			var comments = btn.parentElement.parentElement.parentElement.parentElement.children[1];
			var txtbox = btn.parentElement.parentElement.parentElement.children[0].children[1].children[0];
			var smtxtbox = btn.parentElement.parentElement.parentElement.parentElement.children[3].children[0].children[1];
			var select = btn.parentElement.parentElement.parentElement.children[0].children[2].children[0];

			if (comments.value == "Comments") {
				if (btn.parentElement.children[4].value != null && btn.parentElement.children[4].value != "") {
					
					var buttonActive = false;
					if (btn.parentElement.children[5].value == "1") {
						buttonActive = true;
					}

					var btnList = btn.parentElement.parentElement.children;
					for (var c = 0; c < btnList.length; c++) {
						if (btnList[c].children[4].value != "" && btnList[c].children[4].value == btn.parentElement.children[4].value) {
							SetButtonStyle(btnList[c].children[0], false);
							btnList[c].children[5].value = "0";
						}
					}
					if (!buttonActive) {
						SetButtonStyle(btn, true);
						btn.parentElement.children[5].value = "1";
					}
				}
				UpdateCommentList();
			}
			else if (btnval.value != "0") {
				for (var c = 0; c < select.children.length; c++) {
					if (select.children[c].innerHTML == btnval.value) {
						select.value = select.children[c].value;
						txtbox.value = btnval.value;
						smtxtbox.value = btnval.value;

						if (btn.parentElement.parentElement.parentElement.parentElement.children[0].value == "FL") {
                            if (select.children[c].innerHTML == "Medium" || select.children[c].innerHTML == "Strong" || select.children[c].innerHTML == "Very Strong") {
                                var elements = btn.parentElement.parentElement.parentElement.parentElement.parentElement.children;
                                for (var b = 0; b < elements.length; b++) {
                                    if (elements[b].children[0].value == "FL Color") {
                                        txtbox = elements[b].children[4].children[0].children[1].children[0];
                                        smtxtbox = elements[b].children[3].children[0].children[1];
                                        select = elements[b].children[4].children[0].children[2].children[0];

                                        for (var a = 0; a < select.children.length; a++) {
                                            if (select.children[a].innerHTML == "Blue") {
                                                select.value = select.children[a].value;
                                                txtbox.value = "Blue";
                                                smtxtbox.value = "Blue";
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            else {
                                var elements = btn.parentElement.parentElement.parentElement.parentElement.parentElement.children;
                                for (var b = 0; b < elements.length; b++) {
                                    if (elements[b].children[0].value == "FL Color") {
                                        txtbox = elements[b].children[4].children[0].children[1].children[0];
                                        smtxtbox = elements[b].children[3].children[0].children[1];
                                        select = elements[b].children[4].children[0].children[2].children[0];

                                        for (var a = 0; a < select.children.length; a++) {
                                            if (select.children[a].innerHTML == "None" || select.children[a].innerHTML == "") {
                                                select.value = select.children[a].value;
                                                txtbox.value = "";
                                                smtxtbox.value = "";
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
						}
						break;
					}
				}
			}
			
			if (redir.value != "0") {
				if (redir.value == "Small") {
					var eleEditing = document.getElementById('<%=ElementEditing.ClientID%>');
					eleEditing.value = "None";
				}
				document.getElementById('<%=CategoryEditing.ClientID%>').value = redir.value;
				SetButtonCategoryJS();
			}

			StayFocused();
			return false;
		}
		function SetButtonStyle(button, state) {
			if (state == true) {
				button.style.background = "linear-gradient(to bottom, #e61919, #c32222)";
				button.style.borderTopColor = "#c32222";
				button.style.borderLeftColor = "#c32222";
				button.style.borderRightColor = "#c32222";
				button.style.borderBottomColor = "#821717";
			}
			else {
				button.style.background = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				button.style.borderTopColor = "#2f96b4";
				button.style.borderLeftColor = "#2f96b4";
				button.style.borderRightColor = "#2f96b4";
				button.style.borderBottomColor = "#1f6377";
			}
		}
		function UpdateCommentList() {
			var part = document.getElementById('<%=PartEditing.ClientID%>');
			var elements = part.parentElement.children[4].children;
			var commentBtns;
			var commentTextBox = document.getElementById('<%=CommentsList.ClientID%>');
			commentTextBox.value = "";

			for (var c = 0; c < elements.length; c++) {
				if (elements[c].children[1].value == "Comments") {
					commentBtns = elements[c].children[4].children[2].children;
					break;
				}
			}

			for (var c = 0; c < commentBtns.length; c++) {
				if (commentBtns[c].children[5].value == "1") {
					commentTextBox.value += commentBtns[c].children[1].value;
					commentTextBox.value += "\n";
				}
			}
		}
	</script>
	<div class="demoarea">
		<div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
		<asp:UpdatePanel runat="server" ID="MainPanel">
			<ContentTemplate>
                <div class="demoheading">
                    Color
                    <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 20px"></asp:Label>
                </div>
				<table style="width: 100%;">
					<tr>
                        <td style="vertical-align: top; white-space: nowrap;" colspan="2">
                            <!-- 'Batch Number' textbox -->
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                                <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                    placeholder="Batch number" Style="width: 115px;" OnFocus="this.select();" autocomplete="off"/>
                                <!-- 'Load' button -->
                                <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-large" runat="server" Text="Load"
                                    OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
                                <asp:CheckBox ID="IgnoreCpFlag" runat="server" CssClass="radio-inline" Text="Ignore Cp" style="margin-left: 10px;"
                                    Checked="True" />

                            </asp:Panel>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                        </td>
                        <td style="vertical-align: top; width: 80%;">
                            <asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel" Style="float: left; font-size: 16px; display: none;"></asp:Label>
                            <asp:HiddenField runat="server" ID="OldTextNumber" />
							<asp:HiddenField runat="server" ID="hdnColorBatchEventID" />
							<asp:HiddenField runat="server" ID="hdnColorItemEventID" />
                            <!-- 'Save' button -->
                            <asp:Button ID="cmdSave" CssClass="btn btn-info btn-large" runat="server" Text="Save"
                                Style="text-align: center;margin-left:20px; float: left; display:none;" OnClick="OnSaveClick" ></asp:Button>
							<table>
								<tr>
									<td>
										<asp:RadioButtonList runat="server" ID="ColorRadio" OnClick="ColorRadioChangedJS();" AutoPostBack="false"
										style="margin-left: 20px; float: left; display:none;" RepeatDirection="Horizontal">
										<asp:ListItem Text="Loose" Value="Loose" Selected="true" />
										<asp:ListItem Text="Mounted" Value="Mounted" />
										<asp:ListItem Text="India" Value="India" />
										</asp:RadioButtonList>
									</td>
								</tr>
								<div id="YesNoDiv" runat="server">
								<tr>
									<td>
										<asp:Label ID="SaveDialogLbl" runat="server" ForeColor="Red" BackColor="White" CssClass="label" Style="font-size: 20px; display: none;" Text="Do you want to save changes?"></asp:Label>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Button ID="YesBtn" runat="server" CssClass="btn btn-info btn-large" OnClick="OnQuesSaveYesClick" Style="text-align: center; margin-left: 20px; float: left; display: none;" Text="Yes" />
                                        <asp:Button ID="NoBtn" runat="server" CssClass="btn btn-info btn-large" OnClick="OnQuesSaveNoClick" Style="text-align: center; margin-left: 20px; float: left; display: none;" Text="No" />
                                        <asp:Button ID="CancelBtn" runat="server" CssClass="btn btn-info btn-large" OnClick="OnQuesSaveCancelClick" Style="text-align: center; margin-left: 20px; float: left; display: none;" Text="Cancel" />
									</td>
								</tr>
									</div>
							</table>
							<!-- Short Report Button -->
							<asp:Button ID="shortrpt" CssClass="btn btn-info btn-large" runat="server" Text="Short Report"
								Style="text-align: center; margin-left: 20px; float: left; display:none;" OnClick="OnShortReportClick" />
                        </td>
                        <%-- <td style="text-align: left; padding-right: 40px; padding-left: 20px; vertical-align: top">
                        </td> --%>
                    </tr>
					<tr style="height: 200px;">
                        <td style="padding-top: 0px; vertical-align: top">
                            <asp:ListBox ID="lstItemList" runat="server" Width="130px" CssClass="text" Style="vertical-align: top; font-size: 15px;"
                                 AutoPostBack="True" Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged">
                            </asp:ListBox>
                            <br />
                        </td>
                        <td style="vertical-align: top;padding-right: 20px;width: 220px; font-size: 19px;">
                            <asp:TreeView ID="PartTree" runat="server" 
                                OnSelectedNodeChanged="OnPartsTreeChanged" NodeIndent="15">
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                <NodeStyle VerticalPadding="5px" />
                            </asp:TreeView>
                        </td>
						<td style="vertical-align: top;" rowspan="2">
							<asp:Panel runat="server" ID="MeasurementPanel" Style="position: relative; display:none">
								<div style="height: 700px;">
									<asp:HiddenField runat="server" ID="PartEditing" />
									<asp:HiddenField runat="server" ID="ItemEditing" />
									<asp:HiddenField runat="server" ID="ElementEditing" />
									<asp:HiddenField runat="server" ID="CategoryEditing" />
									<div id="ElementPanelDiv">
										<asp:Repeater runat="server" ID="ElementPanel">
											<ItemTemplate>
												<div id="ElementDiv">
													<asp:HiddenField runat="server" ID="ElementName" Value='<%# Eval("ElemName")%>' />
													<asp:HiddenField runat="server" ID="ElementType" Value='<%# Eval("ElemType") %>' />
													<asp:HiddenField runat="server" ID="ElementMeasure" Value='<%# Eval("ElemMeasure") %>' />
													<asp:Panel runat="server" ID="SmallPanel">
														<div style="width: 150px; float: left;  margin-bottom: 20px; margin-right: 20px;">
															<asp:Label runat="server" ID="SmLbl" Text='<%# Eval("SmLbl")%>' style="display:none;"></asp:Label>
															<asp:TextBox runat="server" ID="SmTxt" Text='<%# Eval("SmTxt")%>' readonly='<%# Eval("SmTxtRO")%>' Enabled="false" Style="width: 135px;"></asp:TextBox>
															<asp:Button runat="server" ID="SmBtn" CssClass="btn btn-info btn-large" OnClientClick="return OnSmBtnClickJS(this);"
																Text='<%# Eval("SmBtn")%>' Style="width: 150px;"></asp:Button>
														</div>
													</asp:Panel>
													<asp:Panel runat="server" ID="BigPanel" Style="position: absolute; width: 640px; top: 200px; background-color: white; border: 2px solid gray; padding: 5px; z-index: 5; display:none;">
														<asp:Panel runat="server" ID="DropDownPanel">
															<asp:Label runat="server" ID="BigLbl" Text='<%# Eval("BigLbl")%>' ></asp:Label>
															<asp:TextBox runat="server" ID="BigTxt" Text='<%# Eval("BigTxt")%>' OnTextChanged="OnTextChanged"></asp:TextBox>
															<ajaxToolkit:DropDownExtender runat="server" TargetControlID="BigTxt" DropDownControlID="DropDownListPanel" />
															<asp:Panel runat="server" ID="DropDownListPanel">
																<asp:ListBox runat="server" ID="DropDownListBox" Rows="10" DataSource='<%# Eval("DropDownEnums")%>'
																	DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
																	OnSelectedIndexChanged="OnDropDownSelectedIndexChanged"></asp:ListBox>
															</asp:Panel>
														</asp:Panel>
														<div id="TextRepeaterDiv">
															<asp:Repeater runat="server" ID="TextRepeater" DataSource='<%# Eval("TxtRepeaterSource")%>'>
																<ItemTemplate>
																	<div style="display: flex; justify-content: flex-end;">
																		<asp:Label runat="server" ID="RepeatLabel" Text='<%# Eval("RepeatLbl")%>' Style="text-align: right;"></asp:Label>
																		<asp:TextBox runat="server" ID="RepeatTxt" Value='<%# Eval("RepeatTxt")%>' readonly="true" Style="margin-left: 5px; width: 50px;"/>
																	</div>
																</ItemTemplate>
															</asp:Repeater>
														</div>
														<div id="ButtonRepeaterDiv">
															<asp:Repeater runat="server" ID="ButtonRepeater" DataSource='<%# Eval("BtnRepeaterSource")%>'>
																<ItemTemplate>
																	<div id="RepeatButtonDiv">
																		<asp:Button runat="server" ID="RepeatButton" CssClass="btn btn-info btn-large" OnClientClick="return OnButtonListClickJS(this);"
																				Text='<%# Eval("BigBtn")%>'  style="margin: 10px; width: 100px; font-size: 16px; padding-left: 0px; padding-right: 0px; border-bottom-color: #1f6377; float:left;"/>
																		<asp:HiddenField runat="server" ID="BtnValue" Value='<%# Eval("BtnValue")%>' />
																		<asp:HiddenField runat="server" ID="BtnLink" Value='<%# Eval("BtnLink")%>' />
																		<asp:HiddenField runat="server" ID="BtnCat" Value='<%# Eval("BtnCat")%>' />
																		<asp:HiddenField runat="server" ID="BtnMeasure" Value='<%# Eval("BtnMeasure") %>' />
																		<asp:HiddenField runat="server" ID="BtnActive" Value="0" />
																	</div>
																</ItemTemplate>
															</asp:Repeater>
														</div>
													</asp:Panel>
													<asp:Panel runat="server" ID="Pic" CssClass="form-horizontal" Style="width: 200px; word-wrap: break-word; margin-bottom: 70px; display:none;">
														<asp:Label runat="server" ID="PicLbl" Width="200px" Text='<%# Eval("PicLblTxt") %>'></asp:Label>
														<asp:Image runat="server" class="image_gdlight" ID="Image" Width="180px"></asp:Image>
														<asp:Label runat="server" ID="PicErrLbl" ForeColor="Red" Width="200px" Text='<%# Eval("PicErrLblTxt") %>'></asp:Label>
													</asp:Panel>
												</div>
											</ItemTemplate>
										</asp:Repeater>
									</div>
								</div>
							</asp:Panel>
						</td>
					</tr>
					<tr>

						<%-- --%><td style="vertical-align: top;">
							<asp:Panel runat="server" ID="OldNumPanel" style="display:none;">
								<asp:Label runat="server" ID="OldNumLabel" Text="Old #" style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="OldNumText" style="width: 130px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="Comments" style="display:none;">
								<asp:Label runat="server" Text="Comments" style="float: left; clear: both;"></asp:Label>
								<asp:TextBox runat="server" ID="CommentsList" TextMode="MultiLine" Columns="1" style="width: 130px; height: 542px; float: left;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
						</td> <%-- --%>
						<td style="vertical-align: top; padding-right:150px;">
							<asp:Panel runat="server" ID="PrefixPanel" CssClass="label" style="display:none;" DefaultButton="DummyButton">
								<asp:Label runat="server" ID="PrefixLabel" Text="Prefix" style="float:left; clear:both; font-size: 16px; font-weight: bold;"></asp:Label>
								<asp:TextBox runat="server" ID="PrefixText" style="width: 192px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"
									AutoPostBack="false"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="WeightPanel" style="display:none;">
								<asp:Label runat="server" ID="WeightTxt" Text="Weight" style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="WeightValue" style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="SarinPanel" style="display:none;">
								<asp:Label runat="server" Text="Max/Min Ratio" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="SarinRatio" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
								<br />
								<asp:Label runat="server" Text="Max/Min/Height" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="SarinMeasure" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" ReadOnly="true"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="LaserPanel" style="display:none;" DefaultButton="DummyButton">
								<asp:Label runat="server" Text="Laser Inscription" style="float: left; clear: both;"></asp:Label>
								<asp:TextBox runat="server" ID="LaserInscription" style="width: 200px; float: left; clear: both; margin-bottom: 10px;" autocomplete="off"
									AutoPostBack="false"></asp:TextBox>
							</asp:Panel>
							<asp:Panel runat="server" ID="IntExtCommentPanel" style="display:none;">
								<asp:Label runat="server" Text="Internal Comments" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="InternalComments" TextMode="MultiLine" Columns="1" Style="width: 200px; height: 150px; margin-bottom: 10px;"
									AutoPostBack="false"></asp:TextBox>
								<asp:Label runat="server" Text="External Comments" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="ExternalComments" TextMode="MultiLine" Columns="1" Style="width: 200px; height: 150px; margin-bottom: 10px;"
									AutoPostBack="false"></asp:TextBox>
							</asp:Panel>
							<asp:Button runat="server" ID="DummyButton" UseSubmitBehavior="false" OnClientClick="StayFocused(); return false;" style="display:none;"/>
						</td>
					</tr>
				</table>
				<%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px;display: none;border: solid 2px Gray">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px;color: black;text-align:center"
                        id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif;">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick"  />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OnOkScript="StayFocused" OnCancelScript="StayFocused">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black;padding-top: 10px">Do you want to save the item changes?</div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" OnClick="OnQuesSaveNoClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" OnClick="OnQuesSaveCancelClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close"  Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender" Y="0"
                    PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn" >
                </ajaxToolkit:ModalPopupExtender>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
