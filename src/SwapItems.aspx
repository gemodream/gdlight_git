<%@ Page language="c#" Codebehind="SwapItems.aspx.cs" AutoEventWireup="True" Inherits="Corpt.SwapItems" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SwapItems</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/main.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:TextBox style="Z-INDEX: 101; POSITION: absolute; TOP: 104px; LEFT: 56px" id="txtItemOne"
				runat="server" CssClass="inputStyleCC"></asp:TextBox>
			<asp:TextBox style="Z-INDEX: 102; POSITION: absolute; TOP: 104px; LEFT: 296px" id="txtItemSecond"
				runat="server" CssClass="inputStyleCC"></asp:TextBox>
			<asp:Label style="Z-INDEX: 103; POSITION: absolute; TOP: 40px; LEFT: 56px" id="Label1" runat="server"
				CssClass="text">Swap Items</asp:Label>
			<asp:Button style="Z-INDEX: 104; POSITION: absolute; TOP: 104px; LEFT: 512px" id="cmdSwap" runat="server"
				CssClass="buttonStyle" Text="SWAP" onclick="cmdSwap_Click"></asp:Button>
			<asp:Label style="Z-INDEX: 105; POSITION: absolute; TOP: 72px; LEFT: 56px" id="lblInfo" runat="server"
				CssClass="text"></asp:Label>
			<asp:DataGrid style="Z-INDEX: 106; POSITION: absolute; TOP: 144px; LEFT: 56px" id="dgDebug" runat="server"
				CssClass="text"></asp:DataGrid>
		</form>
	</body>
</HTML>
