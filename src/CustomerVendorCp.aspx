﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CustomerVendorCp.aspx.cs" Inherits="Corpt.CustomerVendorCp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div class="demoarea">
    <div class="demoheading">Customer/Vendor/Cp</div>
    <div class="navbar nav-tabs">
        <asp:ImageButton ID="FilterButton" runat="server" ToolTip="Apply a filter" ImageUrl="~/Images/ajaxImages/filter.png"
            OnClick="OnFilterClick" />
    </div>
    <table>
        <!-- Customer -->
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Customer: " ></asp:Label></td>
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="CustomerLikeButton" CssClass="form-inline">
                    <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                        width: 80px" placeHolder="Name Like"></asp:TextBox>
                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                    <asp:DropDownList ID="lstCustomerList" runat="server" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                        Width="400px" Style="font-family: Arial; font-size: 12px" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td><asp:Label ID="Label2" runat="server" Text="Vendor: "></asp:Label></td>
            <td>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="VendorLikeButton" CssClass="form-inline">
                    <asp:TextBox runat="server" ID="VendorLike" Style="font-family: Arial; font-size: 12px;
                        width: 80px" placeHolder="Name Like"></asp:TextBox>
                    <asp:ImageButton ID="VendorLikeButton" runat="server" ToolTip="Filtering the list of vendors"
                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnVendorSearchClick" />
                    <asp:DropDownList ID="lstVendorList" runat="server" OnSelectedIndexChanged="OnVendorSelectedChanged"
                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                        Width="400px" Style="font-family: Arial; font-size: 12px" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="Label3" runat="server" Text="Program"></asp:Label>
            </td>
            <td>
                <asp:Panel ID="Panel3" runat="server" DefaultButton="CpLikeButton" CssClass="form-inline">
                    <asp:TextBox runat="server" ID="CpLike" Style="font-family: Arial; font-size: 12px;
                        width: 80px" placeHolder="Name Like"></asp:TextBox>
                    <asp:ImageButton ID="CpLikeButton" runat="server" ToolTip="Filtering the list of programs"
                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCpSearchClick" />
                    <asp:DropDownList runat="server" ID="CpList" OnSelectedIndexChanged="OnCpChanged"
                        AutoPostBack="True" DataValueField="CustomerProgramName" DataTextField="CustomerProgramName"
                        Width="400px" Style="font-family: Arial; font-size: 12px" />
                </asp:Panel>
            </td>
        </tr>
    </table><br/>
    <asp:Label runat="server" ID="RowsCount" ForeColor="#5377A9"  Font-Bold="True"></asp:Label>
    <div style="font-size: small">
        <asp:DataGrid runat="server" ID="grdCps" AutoGenerateColumns="False" CellPadding="5">
            <Columns>
                <asp:BoundColumn DataField="CustomerName" HeaderText="Customer">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="VendorName" HeaderText="Vendor"></asp:BoundColumn>
                <asp:BoundColumn DataField="ProgramName" HeaderText="Program"></asp:BoundColumn>
            </Columns>
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" 
                ForeColor="White" />

        </asp:DataGrid>
    </div>
    </div>
</asp:Content>
