﻿using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//<asp:TextBox runat="server" ID="SkuNameBox" Width="185px" onkeydown ="return (event.keyCode!=13);" OnTextChanged="SkuNameChanged"></asp:TextBox>
namespace Corpt
{
	public partial class ScreeningUtilities : CommonPage
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			/*IvanB start*/
			/* Security to prevent user accessing directly via url after logging in */
			string err = "";
			bool access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.PrgUtilities, this, out err);
			bool managementAccess = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.Management, this, out err);
			if (!access || err != "")
			{
				TabPanel1.Enabled = false;
				TabPanel9.Enabled = false;
				TabPanel10.Enabled = false;
				TabPanel11.Enabled = false;
				TabPanel12.Enabled = false;
				TabPanel2.Enabled = false;
				TabPanel15.Enabled = false;
				TabPanel16.Enabled = false;
				TabPanel20.Enabled = false;
				DocumentMergePanel.Enabled = false;
				BNEntriesPanel.Enabled = false;
				BulkCutGradePanel.Enabled = false;
				TabPanel13.Enabled = false;
				TabPanel14.Enabled = false;
				TPRetailersVendor.Enabled = false;
				//Response.Redirect("Middle.aspx");
			}
			/*IvanB end*/
			//alex 05082024 allow only management to block
			if (managementAccess)
			{
				TPRetailersVendor.Enabled = true;
				MoveToBlockBtn.Enabled = true;
				MoveToActualBtn.Enabled = true;
				AddRetailerToCustomer.Enabled = true;
				//ButtonSaveCustomer.Enabled = true;
				gvCustomersRetailer.Enabled = true;
				gvRetailersCustomers.Enabled = true;
				TabPanel9.Enabled = true;
				btnbtnCustomersContractorSave.Enabled = true;
				gvCustomersContractor.Enabled = true;
				btnAddContractor.Enabled = true;
				gvContractor.Enabled = true;
				btnAddServiceType.Enabled = true;
				gvServiceType.Enabled = true;
				btnAddCategory.Enabled = true;
				gvCategory.Enabled = true;
				btnAddServiceTime.Enabled = true;
				gvServiceTime.Enabled = true;
				btnAddJewelryType.Enabled = true;
				gvJewelryType.Enabled = true;
				DocumentMergePanel.Enabled = true;
				CompanyGroupPanel.Enabled = true;
				BNEntriesPanel.Enabled = true;
			}
			else
			{
				MoveToBlockBtn.Enabled = false;
				MoveToActualBtn.Enabled = false;
				AddRetailerToCustomer.Enabled = false;
				//ButtonSaveCustomer.Enabled = false;
				gvCustomersRetailer.Enabled = false;
				//gvRetailersCustomers.Enabled = false;
				TabPanel9.Enabled = false;
				btnbtnCustomersContractorSave.Enabled = false;
				gvCustomersContractor.Enabled = false;
				btnAddContractor.Enabled = false;
				gvContractor.Enabled = false;
				btnAddServiceType.Enabled = false;
				gvServiceType.Enabled = false;
				btnAddCategory.Enabled = false;
				gvCategory.Enabled = false;
				btnAddServiceTime.Enabled = false;
				gvServiceTime.Enabled = false;
				btnAddJewelryType.Enabled = false;
				gvJewelryType.Enabled = false;
				DocumentMergePanel.Enabled = false;
				CompanyGroupPanel.Enabled = false;
				BNEntriesPanel.Enabled = false;
				TPRetailersVendor.Enabled = false;
			}
			/*IvanB end*/

			this.Form.DefaultFocus = txtCustomerCode1.ClientID;
			//this.Form.DefaultButton = CustomerLikeButton.UniqueID;
			CleanActionMessages();
			if (!IsPostBack)
			{
				BindServiceType();
				BindRetailersGrid();
				BindCategory();
				BindServiceTypeGrid();
				BindCategoryGrid();
				BindServiceTimeGrid();
				BindDeliveryMethodGrid();
				BindShippingCourierGrid();
				BindScreeningInstrumentGrid();
				BindCertifiedByGrid();
				BindJewelryTypeGrid();
				BindServiceTime();
				LoadCustomers("");
				LoadBNCustomers("");
				LoadRetailers();
				//BindCustomerRetailerGrid();
				LoadSelectors();
				BindListsForMerge();
				/*IvanB start 12/09*/
				BindMeasureListForSkuOnlyTab();
				/*IvanB end 12/09*/
				BindListSkuOnly();
				clearBulkCutGradeBtn_Click(null, null);
				ErrorSuccess.Visible = false;

				//Contractor

				BindContractorsGrid();
				LoadCustomersContractor("");
				LoadContractors();
				BindContractorServiceType();
				BindContractorServiceTypeGrid();
				BindCompanyGroupsGrid();
				BindCustomerList();
				ClearCompanyGroup();
				
			}
			/*IvanB start*/
			//ActualNamesListApplyStyles();
			/*IvanB end*/
		}
		#endregion


		protected void OnActiveTab_Changed(object sender, EventArgs e)
		{ 
			//default
		}

		#region Retailer
		public void BindServiceType()
		{
			DataTable dt = SyntheticScreeningUtils.GetSyntheticServiceType(this);
			chklstServiceType.DataSource = dt;
			chklstServiceType.DataTextField = "ServiceTypeName";
			chklstServiceType.DataValueField = "ServiceTypeCode";
			chklstServiceType.DataBind();
		}
		public void BindRetailersGrid(string sortExpression = null)
		{
			DataTable retailers = SyntheticScreeningUtils.GetAllRetailers(this);

			if (sortExpression != null)
			{
				DataView dv = retailers.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvRetailer.DataSource = dv;
			}
			else
			{
				gvRetailer.DataSource = retailers;

			}
			gvRetailer.DataBind();
		}

		protected void OnSortingRetailer(object sender, GridViewSortEventArgs e)
		{
			this.BindRetailersGrid(e.SortExpression);
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticRetailerModel retailer = new SyntheticRetailerModel();
				retailer.retailerId = hdnRetailerId.Value.Trim();
				retailer.retailerName = txtRetailerName.Text.Trim();
				List<string> selectedValue = (from ListItem lst in chklstServiceType.Items where lst.Selected select lst.Value).ToList();
				retailer.ServiceTypeCodeSet = string.Join(";", selectedValue.Select(x => string.Format("{0}", x)).ToArray());
				DataTable dtRetailer = SyntheticScreeningUtils.SetRetailer(retailer, this);
				if (dtRetailer.Rows.Count >= 1)
				{

					lblRetailerMsg.Text = "Retailer already exist.";
					lblRetailerMsg.ForeColor = System.Drawing.Color.Red;
					//ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Retailer already exist.');", true);
					//Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<script type = 'text/javascript'>alert('Retailer Already Exist!'); </script>", true);
					return;
				}
				BindRetailersGrid();
				Clear();
				lblRetailerMsg.Text = "Retailer data added successfully";
				lblRetailerMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblRetailerMsg.Text = ex.Message.ToString();
				lblRetailerMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void gvRetailer_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				LinkButton lnkServiceType = (LinkButton)e.Row.FindControl("lnkServiceType");
				if (lnkServiceType.Text != "")
				{
					lnkServiceType.Text = lnkServiceType.Text.Replace(",", "</br>");
				}
			}
		}

		protected void EditRetailer(object sender, EventArgs e)
		{
			Clear();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkRetailerName = (LinkButton)clickedRow.FindControl("lnkRetailerName");
			HiddenField hdnRetailerID = (HiddenField)clickedRow.FindControl("hdnRetailerID");
			LinkButton lnkServiceType = (LinkButton)clickedRow.FindControl("lnkServiceType");
			HiddenField hdnServiceTypeCodeSet = (HiddenField)clickedRow.FindControl("hdnServiceTypeCodeSet");

			txtRetailerName.Text = lnkRetailerName.Text;
			hdnRetailerId.Value = hdnRetailerID.Value;
			lblRetailerMsg.Text = "";
			string serviceType = lnkServiceType.Text;
			string[] serviceTypeCodeSet = hdnServiceTypeCodeSet.Value.Split(';');

			if (hdnServiceTypeCodeSet.Value != "")
			{
				foreach (string inst in serviceTypeCodeSet)
				{
					int i = Convert.ToInt16(inst);
					chklstServiceType.Items.FindByValue(i.ToString()).Selected = true;
				}
			}
		}

		private string SortDirection
		{
			get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			Clear();
		}

		private void Clear()
		{
			hdnRetailerId.Value = "0";
			txtRetailerName.Text = "";
			foreach (ListItem li in chklstServiceType.Items)
				li.Selected = false;
		}

		#endregion

		#region ServiceType
		public void BindCategory()
		{
			DataTable dt = SyntheticScreeningUtils.GetSyntheticCategory(this);
			chklstCategory.DataSource = dt;
			chklstCategory.DataTextField = "CategoryName";
			chklstCategory.DataValueField = "CategoryCode";
			chklstCategory.DataBind();
		}

		public void BindServiceTypeGrid(string sortExpression = null)
		{
			DataTable serviceType = SyntheticScreeningUtils.GetAllServiceType(this);

			if (sortExpression != null)
			{
				DataView dv = serviceType.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvServiceType.DataSource = dv;
			}
			else
			{
				gvServiceType.DataSource = serviceType;
			}
			gvServiceType.DataBind();

		}

		protected void OnSortingServiceType(object sender, GridViewSortEventArgs e)
		{
			this.BindServiceTypeGrid(e.SortExpression);
		}

		protected void gvServiceType_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				LinkButton lnkCategoryNames = (LinkButton)e.Row.FindControl("lnkCategoryNames");
				if (lnkCategoryNames.Text != "")
				{
					lnkCategoryNames.Text = lnkCategoryNames.Text.Replace(",", "</br>");
				}
			}
		}
		protected void EditServiceType(object sender, EventArgs e)
		{
			ClearServiceType();
			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkServiceTypeName = (LinkButton)clickedRow.FindControl("lnkServiceTypeName");
			HiddenField hdnRowServiceTypeID = (HiddenField)clickedRow.FindControl("hdnServiceTypeID");
			LinkButton lnkCategoryNames = (LinkButton)clickedRow.FindControl("lnkCategoryNames");
			HiddenField hdnCategoryCodeSet = (HiddenField)clickedRow.FindControl("hdnCategoryCodeSet");

			txtServiceType.Text = lnkServiceTypeName.Text;
			hdnServiceTypeId.Value = hdnRowServiceTypeID.Value;
			lblServiceTypeMsg.Text = "";
			string serviceType = lnkCategoryNames.Text;
			string[] serviceTypeCodeSet = hdnCategoryCodeSet.Value.Split(';');

			if (hdnCategoryCodeSet.Value != "")
			{
				foreach (string inst in serviceTypeCodeSet)
				{
					int i = Convert.ToInt16(inst);
					chklstCategory.Items.FindByValue(i.ToString()).Selected = true;
				}
			}

		}
		private void ClearServiceType()
		{
			hdnServiceTypeId.Value = "0";
			txtServiceType.Text = "";
			foreach (ListItem li in chklstCategory.Items)
				li.Selected = false;
		}
		protected void btnAddServiceType_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticServiceTypeModel serviceType = new SyntheticServiceTypeModel();
				serviceType.serviceTypeID = hdnServiceTypeId.Value.Trim();
				serviceType.serviceTypeName = txtServiceType.Text.Trim();
				List<string> selectedValue = (from ListItem lst in chklstCategory.Items where lst.Selected select lst.Value).ToList();
				serviceType.categoryCodeSet = string.Join(";", selectedValue.Select(x => string.Format("{0}", x)).ToArray());
				DataTable dtServiceType = SyntheticScreeningUtils.SetSyntheticServiceType(serviceType, this);
				if (dtServiceType.Rows.Count >= 1)
				{

					lblServiceTypeMsg.Text = "ServiceType already exist.";
					lblServiceTypeMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindServiceTypeGrid();
				ClearServiceType();
				lblServiceTypeMsg.Text = "ServiceType data added successfully";
				lblServiceTypeMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblServiceTypeMsg.Text = ex.Message.ToString();
				lblServiceTypeMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearServiceType_Click(object sender, EventArgs e)
		{
			ClearServiceType();
		}
		#endregion

		#region Category
		public void BindCategoryGrid(string sortExpression = null)
		{
			DataTable category = SyntheticScreeningUtils.GetAllCategory(this);

			if (sortExpression != null)
			{
				DataView dv = category.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvCategory.DataSource = dv;
			}
			else
			{
				gvCategory.DataSource = category;
			}
			gvCategory.DataBind();
		}

		public void BindServiceTime()
		{
			DataTable dt = SyntheticScreeningUtils.GetSyntheticServiceTime(this);
			chklstServiceTime.DataSource = dt;
			chklstServiceTime.DataTextField = "ServiceTimeName";
			chklstServiceTime.DataValueField = "ServiceTimeCode";
			chklstServiceTime.DataBind();
		}

		protected void OnSortingCategory(object sender, GridViewSortEventArgs e)
		{
			this.BindCategoryGrid(e.SortExpression);
		}

		protected void EditCategory(object sender, EventArgs e)
		{
			ClearCategory();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkCategoryName = (LinkButton)clickedRow.FindControl("lnkCategoryName");
			HiddenField hdnRowCategoryID = (HiddenField)clickedRow.FindControl("hdnCategoryID");
			LinkButton lnkServiceTimeNames = (LinkButton)clickedRow.FindControl("lnkServiceTimeNames");
			HiddenField hdnServiceTimeCodeSet = (HiddenField)clickedRow.FindControl("hdnServiceTimeCodeSet");

			txtCategoryName.Text = lnkCategoryName.Text;
			hdnCategoryId.Value = hdnRowCategoryID.Value;
			//lblCategoryMsg.Text = "";


			lblCategoryMsg.Text = "";
			string categoryName = lnkCategoryName.Text;
			string[] serviceTimeCodeSet = hdnServiceTimeCodeSet.Value.Split(';');

			if (hdnServiceTimeCodeSet.Value != "")
			{
				foreach (string inst in serviceTimeCodeSet)
				{
					int i = Convert.ToInt16(inst);
					chklstServiceTime.Items.FindByValue(i.ToString()).Selected = true;
				}
			}
		}

		private void ClearCategory()
		{
			hdnCategoryId.Value = "0";
			txtCategoryName.Text = "";
			foreach (ListItem li in chklstServiceTime.Items)
				li.Selected = false;
		}

		protected void btnAddCategory_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticCategoryModel category = new SyntheticCategoryModel();
				category.categoryId = hdnCategoryId.Value.Trim();
				category.categoryName = txtCategoryName.Text.Trim();
				List<string> selectedValue = (from ListItem lst in chklstServiceTime.Items where lst.Selected select lst.Value).ToList();
				category.ServiceTimeCodeSet = string.Join(";", selectedValue.Select(x => string.Format("{0}", x)).ToArray());
				DataTable dtCategory = SyntheticScreeningUtils.SetSyntheticCategory(category, this);
				if (dtCategory.Rows.Count >= 1)
				{
					lblCategoryMsg.Text = "Category already exist.";
					lblCategoryMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindCategoryGrid();
				ClearCategory();
				lblCategoryMsg.Text = "Category data added successfully";
				lblCategoryMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblCategoryMsg.Text = ex.Message.ToString();
				lblCategoryMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void gvCategory_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				LinkButton lnkCategoryNames = (LinkButton)e.Row.FindControl("lnkServiceTimeNames");
				if (lnkCategoryNames.Text != "")
				{
					lnkCategoryNames.Text = lnkCategoryNames.Text.Replace(",", "</br>");
				}
			}
		}

		protected void btnClearCategory_Click(object sender, EventArgs e)
		{
			ClearCategory();
		}
		#endregion

		#region ServiceTime
		public void BindServiceTimeGrid(string sortExpression = null)
		{
			DataTable serviceTime = SyntheticScreeningUtils.GetSyntheticServiceTime(this);

			if (sortExpression != null)
			{
				DataView dv = serviceTime.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvServiceTime.DataSource = dv;
			}
			else
			{
				gvServiceTime.DataSource = serviceTime;
			}
			gvServiceTime.DataBind();
		}

		protected void OnSortingServiceTime(object sender, GridViewSortEventArgs e)
		{
			this.BindServiceTimeGrid(e.SortExpression);
		}

		protected void EditServiceTime(object sender, EventArgs e)
		{
			ClearServiceTime();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkServiceTimeName = (LinkButton)clickedRow.FindControl("lnkServiceTimeName");
			HiddenField hdnServiceTimeID = (HiddenField)clickedRow.FindControl("hdnServiceTimeID");

			txtServiceTimeName.Text = lnkServiceTimeName.Text;
			hdnServiceTimeId.Value = hdnServiceTimeID.Value;
		}

		private void ClearServiceTime()
		{
			hdnServiceTimeId.Value = "0";
			txtServiceTimeName.Text = "";
		}

		protected void btnAddServiceTime_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticServiceTimeModel serviceTime = new SyntheticServiceTimeModel();
				serviceTime.ServiceTimeId = hdnServiceTimeId.Value.Trim();
				serviceTime.ServiceTimeName = txtServiceTimeName.Text.Trim();
				DataTable dtServiceTime = SyntheticScreeningUtils.SetSyntheticServiceTime(serviceTime, this);
				if (dtServiceTime.Rows.Count >= 1)
				{

					lblServiceTimeMsg.Text = "ServiceTime already exist.";
					lblServiceTimeMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindServiceTimeGrid();
				ClearServiceTime();
				lblServiceTimeMsg.Text = "ServiceTime data added successfully";
				lblServiceTimeMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblServiceTimeMsg.Text = ex.Message.ToString();
				lblServiceTimeMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearServiceTime_Click(object sender, EventArgs e)
		{
			ClearServiceTime();
		}


		#endregion

		#region DeliveryMethod
		public void BindDeliveryMethodGrid(string sortExpression = null)
		{
			DataTable deliveryMethod = SyntheticScreeningUtils.GetSyntheticDeliveryMethod(this);

			if (sortExpression != null)
			{
				DataView dv = deliveryMethod.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvDeliveryMethod.DataSource = dv;
			}
			else
			{
				gvDeliveryMethod.DataSource = deliveryMethod;
			}
			gvDeliveryMethod.DataBind();
		}

		protected void OnSortingDeliveryMethod(object sender, GridViewSortEventArgs e)
		{
			this.BindDeliveryMethodGrid(e.SortExpression);
		}

		protected void EditDeliveryMethod(object sender, EventArgs e)
		{
			ClearDeliveryMethod();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkDeliveryMethodName = (LinkButton)clickedRow.FindControl("lnkDeliveryMethodName");
			HiddenField hdnDeliveryMethodID = (HiddenField)clickedRow.FindControl("hdnDeliveryMethodID");

			txtDeliveryMethodName.Text = lnkDeliveryMethodName.Text;
			hdnDeliveryMethodId.Value = hdnDeliveryMethodID.Value;
		}

		private void ClearDeliveryMethod()
		{
			hdnDeliveryMethodId.Value = "0";
			txtDeliveryMethodName.Text = "";
		}

		protected void btnAddDeliveryMethod_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticDeliveryMethodModel deliveryMethod = new SyntheticDeliveryMethodModel();
				deliveryMethod.DeliveryMethodId = hdnDeliveryMethodId.Value.Trim();
				deliveryMethod.DeliveryMethodName = txtDeliveryMethodName.Text.Trim();
				DataTable dtDeliveryMethod = SyntheticScreeningUtils.SetSyntheticDeliveryMethod(deliveryMethod, this);
				if (dtDeliveryMethod.Rows.Count >= 1)
				{

					lblDeliveryMethodMsg.Text = "DeliveryMethod already exist.";
					lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindDeliveryMethodGrid();
				ClearDeliveryMethod();
				lblDeliveryMethodMsg.Text = "DeliveryMethod data added successfully";
				lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblDeliveryMethodMsg.Text = ex.Message.ToString();
				lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearDeliveryMethod_Click(object sender, EventArgs e)
		{
			ClearDeliveryMethod();
		}


		#endregion

		#region ShippingCourier  / Carriers
		public void BindShippingCourierGrid(string sortExpression = null)
		{
			DataTable dtShippingCourier = SyntheticScreeningUtils.GetCarriers(this);

			if (sortExpression != null)
			{
				DataView dv = dtShippingCourier.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvShippingCourier.DataSource = dv;
			}
			else
			{
				gvShippingCourier.DataSource = dtShippingCourier;
			}
			gvShippingCourier.DataBind();
		}

		protected void OnSortingCarrierName(object sender, GridViewSortEventArgs e)
		{
			this.BindShippingCourierGrid(e.SortExpression);
		}

		protected void EditShippingCourier(object sender, EventArgs e)
		{
			ClearShippingCourier();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkShippingCourierName = (LinkButton)clickedRow.FindControl("lnkShippingCourierName");
			HiddenField hdnShippingCourierID = (HiddenField)clickedRow.FindControl("hdnShippingCourierID");

			txtShippingCourierName.Text = lnkShippingCourierName.Text;
			hdnShippingCourierId.Value = hdnShippingCourierID.Value;
		}

		private void ClearShippingCourier()
		{
			hdnShippingCourierId.Value = "0";
			txtShippingCourierName.Text = "";
		}

		protected void btnAddShippingCourier_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticShippingCourierModel shippingCourier = new SyntheticShippingCourierModel();
				shippingCourier.CarrierID = hdnShippingCourierId.Value.Trim();
				shippingCourier.CarrierName = txtShippingCourierName.Text.Trim();
				DataTable dtShippingCourier = SyntheticScreeningUtils.SetCarriers(shippingCourier, this);
				if (dtShippingCourier.Rows.Count >= 1)
				{

					lblShippingCourierMsg.Text = "ShippingCourier already exist.";
					lblShippingCourierMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindShippingCourierGrid();
				ClearShippingCourier();
				lblShippingCourierMsg.Text = "ShippingCourier data added successfully";
				lblShippingCourierMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblShippingCourierMsg.Text = ex.Message.ToString();
				lblShippingCourierMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearShippingCourier_Click(object sender, EventArgs e)
		{
			ClearShippingCourier();
		}


		#endregion

		#region ScreeningInstrument
		public void BindScreeningInstrumentGrid(string sortExpression = null)
		{
			DataTable dtScreeningInstrument = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);

			if (sortExpression != null)
			{
				DataView dv = dtScreeningInstrument.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvScreeningInstrument.DataSource = dv;
			}
			else
			{
				gvScreeningInstrument.DataSource = dtScreeningInstrument;
			}
			gvScreeningInstrument.DataBind();
		}

		protected void OnSortingInstrumentName(object sender, GridViewSortEventArgs e)
		{
			this.BindScreeningInstrumentGrid(e.SortExpression);
		}

		protected void EditScreeningInstrument(object sender, EventArgs e)
		{
			ClearScreeningInstrument();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkInstrumentName = (LinkButton)clickedRow.FindControl("lnkInstrumentName");
			HiddenField hdnInstrumentID = (HiddenField)clickedRow.FindControl("hdnInstrumentID");

			txtScreeningInstrumentName.Text = lnkInstrumentName.Text;
			hdnScreeningInstrumentId.Value = hdnInstrumentID.Value;
		}

		private void ClearScreeningInstrument()
		{
			hdnScreeningInstrumentId.Value = "0";
			txtScreeningInstrumentName.Text = "";
		}

		protected void btnAddScreeningInstrument_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticScreeningInstrumentModel screeningInstrument = new SyntheticScreeningInstrumentModel();
				screeningInstrument.InstrumentID = hdnScreeningInstrumentId.Value.Trim();
				screeningInstrument.InstrumentName = txtScreeningInstrumentName.Text.Trim();
				DataTable dtScreeningInstrument = SyntheticScreeningUtils.SetSyntheticInstruments(screeningInstrument, this);
				if (dtScreeningInstrument.Rows.Count >= 1)
				{

					lblScreeningInstrumentMsg.Text = "Instrument already exist.";
					lblScreeningInstrumentMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindScreeningInstrumentGrid();
				ClearScreeningInstrument();
				lblScreeningInstrumentMsg.Text = "Instrument data added successfully";
				lblScreeningInstrumentMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblScreeningInstrumentMsg.Text = ex.Message.ToString();
				lblScreeningInstrumentMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearScreeningInstrument_Click(object sender, EventArgs e)
		{
			ClearScreeningInstrument();
		}


		#endregion

		#region CertifiedBy
		public void BindCertifiedByGrid(string sortExpression = null)
		{
			DataTable dtCertifiedBy = SyntheticScreeningUtils.GetSyntheticCertifiedBy(this);

			if (sortExpression != null)
			{
				DataView dv = dtCertifiedBy.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvCertifiedBy.DataSource = dv;
			}
			else
			{
				gvCertifiedBy.DataSource = dtCertifiedBy;
			}
			gvCertifiedBy.DataBind();
		}

		protected void OnSortingCertifiedBy(object sender, GridViewSortEventArgs e)
		{
			this.BindCertifiedByGrid(e.SortExpression);
		}

		protected void EditCertifiedBy(object sender, EventArgs e)
		{
			ClearCertifiedBy();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkCertifiedBy = (LinkButton)clickedRow.FindControl("lnkCertifiedBy");
			HiddenField hdnCertifiedID = (HiddenField)clickedRow.FindControl("hdnCertifiedID");

			txtCertifiedByName.Text = lnkCertifiedBy.Text;
			hdnCertifiedById.Value = hdnCertifiedID.Value;
		}

		private void ClearCertifiedBy()
		{
			hdnCertifiedById.Value = "0";
			txtCertifiedByName.Text = "";
		}

		protected void btnAddCertifiedBy_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticCertifiedByModel certifiedBy = new SyntheticCertifiedByModel();
				certifiedBy.CertifiedID = hdnCertifiedById.Value.Trim();
				certifiedBy.CertifiedBy = txtCertifiedByName.Text.Trim();
				DataTable dtCertifiedBy = SyntheticScreeningUtils.SetSyntheticCertifiedBy(certifiedBy, this);
				if (dtCertifiedBy.Rows.Count >= 1)
				{

					lblCertifiedByMsg.Text = "CertifiedBy already exist.";
					lblCertifiedByMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindCertifiedByGrid();
				ClearCertifiedBy();
				lblCertifiedByMsg.Text = "CertifiedBy data added successfully";
				lblCertifiedByMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblCertifiedByMsg.Text = ex.Message.ToString();
				lblCertifiedByMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearCertifiedBy_Click(object sender, EventArgs e)
		{
			ClearCertifiedBy();
		}


		#endregion

		#region Customers Retailer
		private void LoadCustomers(string filter)
		{

			var customers = QueryUtils.GetCustomers(this);
			customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
			customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
			/*IvanB start*/
			//create and fill blocklist
			var blocked = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);
			//Remove items from a list of customers with a CustomerName equals value from block list
			customers = customers.Where(x => !blocked.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
			/*IvanB end*/
			if (filter != "")
			{
				customers = customers.Where(x => x.CustomerName.Contains(filter)).ToList();
			}
			ddlCutomer.Items.Clear();
			ddlCutomer.DataSource = customers;
			ddlCutomer.DataBind();

		}
		private void LoadBNCustomers(string filter)
		{

			var customers = QueryUtils.GetCustomers(this);
			customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
			customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
			/*IvanB start*/
			//create and fill blocklist
			var blocked = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);
			//Remove items from a list of customers with a CustomerName equals value from block list
			customers = customers.Where(x => !blocked.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
			/*IvanB end*/
			if (filter != "")
			{
				customers = customers.Where(x => x.CustomerName.Contains(filter)).ToList();
			}
			ddlBNCustomer.Items.Clear();
			ddlBNCustomer.DataSource = customers;
			ddlBNCustomer.DataBind();

		}
		private void LoadRetailers()
		{
			var retailers = SyntheticScreeningUtils.GetRetailers(this);
			SetViewState(retailers, "Retailers");
			ddlRetailer.DataSource = retailers;
			ddlRetailer.DataBind();
			ListItem liRetailer = new ListItem();
			liRetailer.Text = "";
			liRetailer.Value = "0";
			ddlRetailer.Items.Insert(0, liRetailer);
			repRetailer.DataSource = retailers;
			repRetailer.DataBind();
			//for new tab
			ddlRetailerList.DataSource = retailers;
			ddlRetailerList.DataBind();
			ddlRetailerList.Items.Insert(0, liRetailer);

			ddlRetailerVendors.DataSource = null;
			ddlRetailerVendors.DataBind();
			ddlRetailerVendors.Enabled = false;
			SetViewState(new DataTable(), "RetailerVendors");

			ddlProgram.DataSource = null;
			ddlProgram.DataBind();
			ddlProgram.Enabled = false;

			GetAllBrands();
			ddlBrand.DataSource = null;
			ddlBrand.DataBind();
			ddlBrand.Enabled = false;

			ddlNature.DataSource = null;
			ddlNature.DataBind();
			ddlNature.Enabled = false;
		}
		private void BindCustomerRetailerGrid()
		{
			gvCustomersRetailer.Visible = true;
			EnteredLabel.Visible = true;
			SyntheticRetailerModel retailer = new SyntheticRetailerModel();
			retailer.customerCode = ddlCutomer.SelectedItem.Value;
			//var retailers = SyntheticScreeningUtils.GetSyntheticCustomerRetailer(retailer, this);
			var retailers = SyntheticScreeningUtils.GetRetailerAttachments(retailer, this);
			gvCustomersRetailer.DataSource = retailers;
			gvCustomersRetailer.DataBind();
			//GenerateUniqueData0(1);
			//GenerateUniqueData0(2);
			//GenerateUniqueData0(3);
		}
		private void BindBNCustomerEntriesGrid()
		{
			string customerCode = ddlBNCustomer.SelectedItem.Value;
			hdnBNCustomerName.Value = ddlBNCustomer.SelectedItem.Text;
			//var retailers = SyntheticScreeningUtils.GetSyntheticCustomerRetailer(retailer, this);
			DataSet bdData = SyntheticScreeningUtils.GetBNAttachments(customerCode, this);
			if (bdData.Tables[0].Rows.Count > 0)
			{
				bnCategoryCheckList.DataSource = bdData.Tables[0];
				bnCategoryCheckList.DataTextField = "CategoryName";
				bnCategoryCheckList.DataValueField = "CategoryID";
				bnCategoryCheckList.DataBind();
				for (int i = 0; i <= bnCategoryCheckList.Items.Count - 1; i++)
				{
					string name = bnCategoryCheckList.Items[i].Text;
					DataRow[] nameFound = bdData.Tables[2].Select("CategoryName = '" + name + "'");
					if (nameFound.Length > 0)
						bnCategoryCheckList.Items[i].Selected = true;
				}
				List<string> values = new List<string>();
				foreach (ListItem Item in bnCategoryCheckList.Items)
				{
					if (Item.Selected)
						values.Add(Item.Value);
				}
				DataTable tmpTbl = (DataTable)bnCategoryCheckList.DataSource;
				//var selected = GetSelectedItems(bnCategoryCheckList);
				//bnCategoryCheckList.Items[0].Selected = true;
				//bnCategoryCheckList.Items[2].Selected = true;
				//bnCategoryGridView.DataSource = bdData.Tables[0];
				//bnCategoryGridView.DataBind();
			}
			if (bdData.Tables[1].Rows.Count > 0)
			{
				bnDestinationCheckList.DataSource = bdData.Tables[1];
				bnDestinationCheckList.DataTextField = "DestinationName";
				bnDestinationCheckList.DataValueField = "DestinationID";
				bnDestinationCheckList.DataBind();
				for (int i = 0; i <= bnDestinationCheckList.Items.Count - 1; i++)
				{
					string name = bnDestinationCheckList.Items[i].Text;
					DataRow[] nameFound = bdData.Tables[3].Select("DestinationName = '" + name + "'");
					if (nameFound.Length > 0)
						bnDestinationCheckList.Items[i].Selected = true;
				}
				//bnDestinationGridView.DataSource = bdData.Tables[0];
				//bnCategoryGridView.DataBind();
			}
			//GenerateUniqueData0(1);
			//GenerateUniqueData0(2);
			//GenerateUniqueData0(3);
		}
		//public IEnumerable<ListItem> GetSelectedItems(this ListControl checkBoxList)
		//{
		//	return from ListItem li in checkBoxList.Items where li.Selected select li;
		//}
		protected void gvCustomersRetailer_OnRowDataBound(object sender, EventArgs e)
		{
			for (int i = gvCustomersRetailer.Rows.Count - 1; i > 0; i--)
			{
				GridViewRow row = gvCustomersRetailer.Rows[i];
				GridViewRow previousRow = gvCustomersRetailer.Rows[i - 1];
				for (int j = 0; j < row.Cells.Count - 1; j++)
				{
					//Define what index on your template field cell that contain same value
					if (((LinkButton)row.Cells[1].FindControl("lnkRetailerName")).Text == ((LinkButton)previousRow.Cells[1].FindControl("lnkRetailerName")).Text)
					{
						if (previousRow.Cells[1].RowSpan == 0)
						{
							if (row.Cells[1].RowSpan == 0)
							{
								previousRow.Cells[1].RowSpan += 2;
							}
							else
							{
								previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
								previousRow.Cells[1].VerticalAlign = VerticalAlign.Top;
							}
							row.Cells[1].Visible = false;
						}
						if (previousRow.Cells[0].RowSpan == 0)
						{
							if (row.Cells[0].RowSpan == 0)
							{
								previousRow.Cells[0].RowSpan += 2;
							}
							else
							{
								previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
								previousRow.Cells[0].VerticalAlign = VerticalAlign.Top;
							}
							row.Cells[0].Visible = false;
						}
					}
					if (((LinkButton)row.Cells[2].FindControl("lnkServiceTypeName")).Text != "")
					{
						if (((LinkButton)row.Cells[2].FindControl("lnkServiceTypeName")).Text == ((LinkButton)previousRow.Cells[2].FindControl("lnkServiceTypeName")).Text)
						{
							if (previousRow.Cells[2].RowSpan == 0)
							{
								if (row.Cells[2].RowSpan == 0)
								{
									previousRow.Cells[2].RowSpan += 2;
								}
								else
								{
									previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
									previousRow.Cells[2].VerticalAlign = VerticalAlign.Top;
								}
								row.Cells[2].Visible = false;
							}
						}
					}
				}
			}
		}
		protected void OnCustomerSelectedChanged(object sender, EventArgs e)
		{
			var customer = ddlCutomer.Items.FindByValue(ddlCutomer.SelectedValue);

			if (customer != null)
			{
				txtCustomerCode1.Text = ddlCutomer.SelectedItem.Value;
				BindCustomerRetailerGrid();
			}
		}

		protected void OnBNCustomerSelectedChanged(object sender, EventArgs e)
		{
			var customer = ddlBNCustomer.Items.FindByValue(ddlBNCustomer.SelectedValue);

			if (customer != null)
			{
				//bnCustCodeTxt.Text = ddlBNCustomer.SelectedItem.Value;
				BindBNCustomerEntriesGrid();
			}
		}

		protected void OnCustCodeClick(object sender, EventArgs e)
		{
			LoadCustomers(txtCustomerCode1.Text);
			for (int i = 0; i <= ddlCutomer.Items.Count - 1; i++)
			{
				if (ddlCutomer.Items[i].Text.Contains(txtCustomerCode1.Text.Trim()))
				{
					ddlCutomer.SelectedIndex = i;
					BindCustomerRetailerGrid();
					break;
				}
			}
			if (ddlCutomer.Items.Count == 0)
			{
				ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('Customer Code not found');", true);
				return;
			}
		}
		protected void OnBNCustCodeClick(object sender, EventArgs e)
		{
			if (txtBNCustomerCode.Text == "")
			{
				PopupInfoDialog("Missing Customer Code", true);
				btnBNClear_Click(null, null);
				lblBNError.Visible = true;
				lblBNError.Text = "Missing Customer Code";
				return;
			}
			LoadBNCustomers(txtBNCustomerCode.Text);
			for (int i = 0; i <= ddlBNCustomer.Items.Count - 1; i++)
			{
				if (ddlBNCustomer.Items[i].Text.Contains(txtBNCustomerCode.Text.Trim()))
				{
					ddlBNCustomer.SelectedIndex = i;
					BindBNCustomerEntriesGrid();
					break;
				}
			}
			if (ddlBNCustomer.Items.Count == 0)
			{
				ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('Customer Code not found');", true);
				return;
			}
		}
		protected void AddRetailerToCustomer_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticRetailerModel retailerGet = new SyntheticRetailerModel();
				retailerGet.customerCode = ddlCutomer.SelectedItem.Value;
				var retailers = SyntheticScreeningUtils.GetSyntheticCustomerRetailer(retailerGet, this);

				if (retailers.Count > 0)
				{
					foreach (var retailer in retailers)
						if (retailer.retailerId.ToString().Trim().ToLower() == ddlRetailer.SelectedItem.Value.Trim().ToLower())
						{
							ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('retailer already exists for customer.');", true);
							return;
						}
				}
				SyntheticRetailerModel retailerSet = new SyntheticRetailerModel();
				retailerSet.retailerId = ddlRetailer.SelectedItem.Value;
				retailerSet.customerCode = ddlCutomer.SelectedItem.Value;
				retailerSet.customerName = ddlCutomer.SelectedItem.Text;
				bool added = SyntheticScreeningUtils.SetSyntheticCustomerRetailer(retailerSet, this);
				BindCustomerRetailerGrid();
				ddlRetailer.SelectedIndex = 0;
				txtRetailer.Text = "";
				hdnRetailerId.Value = "0";
				lblMsg.Text = "Vendor added to Customer successfully";

			}
			catch (Exception ex)
			{
				lblMsg.Text = ex.Message.ToString();
				lblMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		protected void DeleteRetailer(object sender, EventArgs e)
		{
			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			List<SyntheticRetailerModel> retailers = new List<SyntheticRetailerModel>();
			SyntheticRetailerModel retailer = new SyntheticRetailerModel();
			LinkButton lnkRetailerName = (LinkButton)clickedRow.FindControl("lnkRetailerName");
			HiddenField hdnRetailerID = (HiddenField)clickedRow.FindControl("hdnRetailerID");
			retailer.customerCode = ddlCutomer.SelectedItem.Value;
			retailer.retailerId = hdnRetailerID.Value;
			bool removed = SyntheticScreeningUtils.DelSyntheticCustomerRetailer(retailer, this);
			BindCustomerRetailerGrid();
		}
		protected void btnRetailerClear_Click(object sender, EventArgs e)
		{
			//lblRetailerMsg.Text = "";
			lblMsg.Text = "";
			txtCustomerCode1.Text = "";
			//txtRetailerName.Value = "";
			LoadCustomers("");
			ddlCutomer.SelectedIndex = -1;
			gvCustomersRetailer.DataSource = null;
			gvCustomersRetailer.DataBind();
			/*IvanB start*/
			gvCustomersRetailer.Visible = false;
			EnteredLabel.Visible = false;
			/*IvanB end*/
			//BindCustomerRetailerGrid();
		}
		protected void repRetailer_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (string.Compare(e.CommandName, "RetailerId", false) == 0)
			{
				txtRetailer.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
				hdnRetailerCode.Value = ((System.Web.UI.WebControls.Button)e.CommandSource).CommandArgument;
				ddlRetailer.ClearSelection();
				ddlRetailer.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
			}
		}

		protected void ddlRetailer_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtRetailer.Text = ddlRetailer.SelectedItem.Text;
			hdnRetailerCode.Value = ddlRetailer.SelectedItem.Value;
			ModalPopupExtender3.Hide();
		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerHtml = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion

		#region JewelryType
		public void BindJewelryTypeGrid(string sortExpression = null)
		{
			DataTable dtJewelryType = SyntheticScreeningUtils.GetSyntheticJewelryType(this);

			if (sortExpression != null)
			{
				DataView dv = dtJewelryType.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvJewelryType.DataSource = dv;
			}
			else
			{
				gvJewelryType.DataSource = dtJewelryType;
			}
			gvJewelryType.DataBind();
		}

		protected void OnSortingJewelryType(object sender, GridViewSortEventArgs e)
		{
			this.BindJewelryTypeGrid(e.SortExpression);
		}

		protected void EditJewelryType(object sender, EventArgs e)
		{
			ClearJewelryType();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkJewelryTypeName = (LinkButton)clickedRow.FindControl("lnkJewelryTypeName");
			HiddenField hdnJewelryTypeID = (HiddenField)clickedRow.FindControl("hdnJewelryTypeID");

			txtJewelryTypeName.Text = lnkJewelryTypeName.Text;
			hdnJewelryTypeId.Value = hdnJewelryTypeID.Value;
		}

		private void ClearJewelryType()
		{
			hdnJewelryTypeId.Value = "0";
			txtJewelryTypeName.Text = "";
		}

		protected void btnAddJewelryType_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticJewelryTypeModel jewelryType = new SyntheticJewelryTypeModel();
				jewelryType.JewelryTypeID = hdnJewelryTypeId.Value.Trim();
				jewelryType.JewelryTypeName = txtJewelryTypeName.Text.Trim();
				DataTable dtJewelryType = SyntheticScreeningUtils.SetSyntheticJewelryType(jewelryType, this);
				if (dtJewelryType.Rows.Count >= 1)
				{

					lblJewelryTypeMsg.Text = "JewelryType already exist.";
					lblJewelryTypeMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindJewelryTypeGrid();
				ClearJewelryType();
				lblJewelryTypeMsg.Text = "JewelryType data added successfully";
				lblJewelryTypeMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblJewelryTypeMsg.Text = ex.Message.ToString();
				lblJewelryTypeMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearJewelryType_Click(object sender, EventArgs e)
		{
			ClearJewelryType();
		}


		#endregion

		#region BlockListEditing
		protected void LoadSelectors()
		{

			SelectorListBox.DataSource = QueryDropDownBlock.GetDropDownEditList(); ;
			SelectorListBox.DataTextField = "Text";
			SelectorListBox.DataValueField = "Value";
			SelectorListBox.DataBind();
			SelectorListBox.SelectedIndex = 0;
			BindActualAndBlockLists((int)EnumDropDownBlock.Customers);

		}

		protected void OnSelectorSelectedChanged(object sender, EventArgs e)
		{
			BindActualAndBlockLists(int.Parse(SelectorListBox.SelectedValue));
		}

		protected void BindActualAndBlockLists(int SelectorId)
		{
			ActualList.Items.Clear();
			BlockList.Items.Clear();

			var blockList = QueryDropDownBlock.GetDropDownBlockList(SelectorId, Page);

			if (SelectorId == (int)EnumDropDownBlock.Customers)
			{
				//no dublicates in DB
				var customers = QueryUtils.GetAllCustomersForBlockEditing(Page).Where(x => !blockList.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName)));

				ActualList.DataSource = customers;
				ActualList.DataTextField = nameof(CustomerModel.CustomerName);
				ActualList.DataValueField = nameof(CustomerModel.CustomerId);
			}
			else if (SelectorId == (int)EnumDropDownBlock.CustomerPrograms)
			{
				//take customer program list and remove dublicates
				var customerPrograms = QueryUtils.GetAllCustomerPrograms(Page).GroupBy(x => x.CustomerProgramName).Select(x => x.FirstOrDefault());
				//remove items with empty CustomerProgramName
				customerPrograms = customerPrograms.Where(x => !string.IsNullOrEmpty(x.CustomerProgramName.Trim())).Distinct().ToList();

				//remove block list from actual list
				customerPrograms = customerPrograms.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName)));

				ActualList.DataSource = customerPrograms;
				ActualList.DataTextField = nameof(CustomerProgramModel.CustomerProgramName);
				ActualList.DataValueField = nameof(CustomerProgramModel.CpId);
			}
			else if (SelectorId == (int)EnumDropDownBlock.Documents)
			{
				//take document list and remove dublicates
				var documents = QueryCpUtilsNew.GetAllDocuments(Page).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault());
				//remove block list from actual list
				documents = documents.Where(x => !blockList.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName)));

				ActualList.DataSource = documents;
				ActualList.DataTextField = nameof(DocPredefinedModel.DocumentName);
				ActualList.DataValueField = nameof(DocPredefinedModel.DocumentId);
			}
			else if (SelectorId == (int)EnumDropDownBlock.CorelFiles)
			{
				string errMsg;
				//take core files list and remove dublicates
				var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg).GroupBy(x => x.DisplayName).Select(x => x.FirstOrDefault());
				//remove block list from actual list
				existsFiles = existsFiles.Where(x => !blockList.Exists(y => x.DisplayName.Equals(y.BlockedDisplayName)));

				ActualList.DataSource = existsFiles;
				ActualList.DataTextField = nameof(CorelFileModel.DisplayName);
				ActualList.DataValueField = nameof(CorelFileModel.Name);
			}
			else if (SelectorId == (int)EnumDropDownBlock.Shapes)
			{
				//take shapes list
				var shapes = QueryCpUtilsNew.GetAllShapeNames(Page);

				//MeasureValueName is used for combobox in CustomerProgramNew.aspx
				//ValueTitles are used for combobox in Clarity2.aspx
				//Some times they are not equal, so we take both and extract unique names for block
				var measuresNames = shapes.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();
				var valueTitles = shapes.Select(x => x.ValueTitle).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();
				var shapeNames = measuresNames.Union(valueTitles);

				//remove block list from actual list
				shapeNames = shapeNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = shapeNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			else if (SelectorId == (int)EnumDropDownBlock.Variety)
			{
				//take shapes list
				var varietyList = QueryCpUtilsNew.GetAllVarietyNames(Page);

				var measuresNames = varietyList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var varietyNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = varietyNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			else if (SelectorId == (int)EnumDropDownBlock.Species)
			{
				//take shapes list
				var speciesList = QueryCpUtilsNew.GetAllSpeciesNames(Page);

				var measuresNames = speciesList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var speciesNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = speciesNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			else if (SelectorId == (int)EnumDropDownBlock.ColorGroup)
			{
				//take shapes list
				var colorGroupList = QueryCpUtilsNew.GetAllColorGroupNames(Page);

				var measuresNames = colorGroupList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var colorGroupNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = colorGroupNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			else if (SelectorId == (int)EnumDropDownBlock.Karatage)
			{
				//take shapes list
				var karatageList = QueryCpUtilsNew.GetAllKaratageNames(Page);

				var measuresNames = karatageList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var karatageNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = karatageNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			//alex 04152027
			else if (SelectorId == (int)EnumDropDownBlock.ItemName)
			{
				//take shapes list
				var itemNameList = QueryCpUtilsNew.GetAllItemNameNames(Page);

				var measuresNames = itemNameList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var itemNameNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = itemNameNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			else if (SelectorId == (int)EnumDropDownBlock.ColorDiamondColors)
			{
				//take shapes list
				var colorDiamondColorsList = QueryCpUtilsNew.GetAllColorDiamondColors(Page);

				var measuresNames = colorDiamondColorsList.Select(x => x.MeasureValueName).Where(x => !string.IsNullOrEmpty(x.Trim())).Distinct().ToList();

				//remove block list from actual list
				var colorDiamondColorsNames = measuresNames.Where(x => !blockList.Exists(y => x.Equals(y.BlockedDisplayName))).OrderBy(x => x);

				ActualList.DataSource = colorDiamondColorsNames;
				ActualList.DataTextField = null;
				ActualList.DataValueField = null;
			}
			//alex 04152027
			ActualList.DataBind();

			BlockList.DataSource = blockList;
			BlockList.DataTextField = nameof(BlockModel.BlockedDisplayName);
			BlockList.DataValueField = nameof(BlockModel.Id);
			BlockList.DataBind();

		}

		protected void OnMoveToBlockClick(object sender, EventArgs e)
		{
			var selectedVals = new List<String>();
			foreach (var i in ActualList.GetSelectedIndices())
			{
				selectedVals.Add(ActualList.Items[i].Text);
			}
			var dropDownId = int.Parse(SelectorListBox.SelectedValue);
			QueryDropDownBlock.AddDropDownBlock(dropDownId, selectedVals, Page);
			BindActualAndBlockLists(dropDownId);

		}

		protected void OnMoveToActualClick(object sender, EventArgs e)
		{
			var selectedVals = new List<int>();
			foreach (var i in BlockList.GetSelectedIndices())
			{
				selectedVals.Add(int.Parse(BlockList.Items[i].Value));

			}
			var dropDownId = int.Parse(SelectorListBox.SelectedValue);
			QueryDropDownBlock.DeleteDropDownBlock(selectedVals, Page);
			BindActualAndBlockLists(dropDownId);
		}

		#endregion

		#region DocumentMerge
		protected void BindListsForMerge()
		{
			DropDownNamesList.Items.Clear();
			ActualNamesList.Items.Clear();
			//take documents block list 
			var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Documents, Page);
			//take document list and remove dublicates 
			var dropDownDocNames = QueryCpUtilsNew.GetAllDocuments(Page).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault());
			//remove block list from dropdown Names List
			dropDownDocNames = dropDownDocNames.Where(x => !blockList.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName)));

			DropDownNamesList.DataSource = dropDownDocNames;
			DropDownNamesList.DataTextField = nameof(DocPredefinedModel.DocumentName);
			DropDownNamesList.DataValueField = nameof(DocPredefinedModel.DocumentId);
			DropDownNamesList.DataBind();

			//take document list before merge and remove dublicates 
			var actualDocNames = QueryDropDownNamesMergeHistory.GetDocumentNamesBeforeMerge(Page).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault());
			//remove block list from actual doc Names List
			actualDocNames = actualDocNames.Where(x => !blockList.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName)));
			//take list of all newNames from history table
			var newNames = QueryDropDownNamesMergeHistory.GetMergeHistoryNewNames(Page);
			//check if item of list is label
			foreach (DropDownNamesDocMergeModel doc in actualDocNames)
			{
				doc.IsLabel = newNames.Contains(doc.DocumentName) ? "1" : "0";
				doc.DropDownValue = doc.DocumentID + "_" + doc.IsLabel;
			}

			//temporary we bind the same list for Replace list
			ActualNamesList.DataSource = actualDocNames;
			ActualNamesList.DataTextField = nameof(DropDownNamesDocMergeModel.DocumentName);
			ActualNamesList.DataValueField = nameof(DropDownNamesDocMergeModel.DropDownValue);
			ActualNamesList.DataBind();
			ActualNamesListApplyStyles();

		}

		//Method to color document names in actual listbox if they used as labels for other document names
		private void ActualNamesListApplyStyles()
		{
			if (ActualNamesList.Items != null)
			{
				foreach (ListItem item in ActualNamesList.Items)
				{
					if (item.Value.ToString().Split('_')[1].Equals("1"))
					{
						item.Attributes.CssStyle.Add("color", "#0088cc");
					}
				}
			}
		}

		protected void OnReplaceClick(object sender, EventArgs e)
		{
			ErrorSuccess.Visible = false;

			if (ActualNamesList.SelectedItem != null && DropDownNamesList.GetSelectedIndices().Length > 0)
			{
				var labelText = ActualNamesList.SelectedItem.Text;
				var notifyMsg = "";

				foreach (var i in DropDownNamesList.GetSelectedIndices())
				{
					var nameToReplaceForLabel = DropDownNamesList.Items[i].Text;
					//We don't want replace same document names
					if (nameToReplaceForLabel != labelText)
					{
						QueryDropDownNamesMergeHistory.ReplaceDocumentNameForLabel(labelText, nameToReplaceForLabel, Page, ref notifyMsg);
					}
				}

				BindListsForMerge();
				ShowNotifyMessage("Replace", notifyMsg, string.IsNullOrEmpty(notifyMsg));
			}
		}

		protected void onRollBackClick(object sender, EventArgs e)
		{
			ErrorSuccess.Visible = false;
			if (ActualNamesList.SelectedItem != null)
			{
				var notifyMsg = "";
				var labelText = ActualNamesList.SelectedItem.Text;
				QueryDropDownNamesMergeHistory.RollBackDocumentName(labelText, Page, ref notifyMsg);

				BindListsForMerge();
				ShowNotifyMessage("Rollback", notifyMsg, string.IsNullOrEmpty(notifyMsg));
			}

		}
		private void ShowNotifyMessage(string functionName, string notifyMsg, bool isSuccess)
		{
			ErrorSuccess.Text = isSuccess ? functionName + " - successed" : notifyMsg;
			ErrorSuccess.CssClass = isSuccess ? "SuccessText" : "ErrorText";
			ErrorSuccess.Visible = true;
		}

		#endregion

		#region SkuOnly
		/*IvanB start 12/09*/
		protected void BindMeasureListForSkuOnlyTab()
		{
			var measureListSkuOnly = new List<ListItem>();
			measureListSkuOnly.Add(new ListItem("SKU", "-1"));
			measureListSkuOnly.Add(new ListItem("Brand", SyntheticScreeningUtils.GetMeasureIDByName(this, "Brand")));
			measureListSkuOnly.Add(new ListItem("Banner/Retailer", SyntheticScreeningUtils.GetMeasureIDByName(this, "Banner/Retailer")));
			ddlMeasures.DataSource = measureListSkuOnly;
			ddlMeasures.DataTextField = "Text";
			ddlMeasures.DataValueField = "Value";
			ddlMeasures.DataBind();
			ddlMeasures.SelectedIndex = 0;
		}
		protected void BindListSkuOnly()
		{
			ErrorSuccess1.Text = "";
			SkuNameBox.Text = "";
			var measuresValue = int.Parse(ddlMeasures.SelectedItem.Value);
			if (measuresValue < 0)
			{
				var skuOnlyList = SyntheticScreeningUtils.GetSkuOnly(this);
				lstSkus.DataSource = skuOnlyList;
				lstSkus.DataValueField = "SkuID";
				lstSkus.DataTextField = "SkuName";
			}
			else
			{
				var measuresList = SyntheticScreeningUtils.GetMeasureValueListByID(this, measuresValue);
				lstSkus.DataSource = measuresList;
				lstSkus.DataTextField = nameof(MeasureValueModel.MeasureValueName);
				lstSkus.DataValueField = nameof(MeasureValueModel.MeasureValueId);
			}

			lstSkus.DataBind();
		}
		/*IvanB end 12/09*/
		protected void OnMeasuresSelectedChanged(object sender, EventArgs e)
		{
			BindListSkuOnly();
		}
		//protected void OnAddSkuClick(object sender, EventArgs e)
		//      {
		//	if (SkuNameBox.Text == "")
		//		return;
		//	var skuOnlyList = SyntheticScreeningUtils.AddNewSku(SkuNameBox.Text, this);
		//	if (skuOnlyList != null)
		//	{
		//		lstSkus.Items.Clear();
		//		lstSkus.DataSource = skuOnlyList;
		//		lstSkus.DataBind();
		//		ErrorSuccess1.Text = SkuNameBox.Text + " added";
		//	}
		//	else
		//		ErrorSuccess1.Text = "Error adding sku " + SkuNameBox.Text;
		//}

		protected void AddSkuOnly_Click(object sender, EventArgs e)
		{
			if (SkuNameBox.Text == "")
				return;
			/*IvanB start 12/09*/
			var measureID = int.Parse(ddlMeasures.SelectedItem.Value);
			if (measureID < 0)
			{
				var skuOnlyList = SyntheticScreeningUtils.AddNewSku(SkuNameBox.Text, this);
				if (skuOnlyList != null)
				{
					ErrorSuccess1.Text = SkuNameBox.Text + @" added";
					SkuNameBox.Text = "";
					lstSkus.Items.Clear();
					lstSkus.DataSource = skuOnlyList;
					lstSkus.DataValueField = "SkuID";
					lstSkus.DataTextField = "SkuName";
					lstSkus.DataBind();
				}
				else
					ErrorSuccess1.Text = "Error adding sku";
			}
			else
			{
				var measuresList = SyntheticScreeningUtils.AddNewMeasureValue(this, measureID, SkuNameBox.Text);
				if (measuresList != null)
				{
					ErrorSuccess1.Text = SkuNameBox.Text + @" added";
					lstSkus.Items.Clear();
					lstSkus.DataSource = measuresList;
					lstSkus.DataTextField = nameof(MeasureValueModel.MeasureValueName);
					lstSkus.DataValueField = nameof(MeasureValueModel.MeasureValueId);
					lstSkus.DataBind();
				}
				else
					ErrorSuccess1.Text = "Error adding " + ddlMeasures.SelectedItem.Text;
			}
			/*IvanB end 12/09*/

		}
		protected void OnDeleteSkuClick(object sender, EventArgs e)
		{
			if (lstSkus.SelectedIndex == -1)
				return;
			string measureValueName = lstSkus.SelectedItem.Text;
			string measureValueId = lstSkus.SelectedItem.Value;
			/*IvanB start 12/09*/
			var measureID = int.Parse(ddlMeasures.SelectedItem.Value);
			if (measureID < 0)
			{
				var skuOnlyList = SyntheticScreeningUtils.DeleteSelectedSku(measureValueName, this);
				if (skuOnlyList != null)
				{
					lstSkus.Items.Clear();
					lstSkus.DataSource = skuOnlyList;
					lstSkus.DataValueField = "SkuID";
					lstSkus.DataTextField = "SkuName";
					lstSkus.DataBind();
					DeleteSkuBtn.Visible = false;
					ErrorSuccess1.Text = measureValueName + " deleted";
				}
				else
				{
					ErrorSuccess1.Text = "Error deleting " + measureValueName;
				}
			}
			else
			{
				var measuresList = SyntheticScreeningUtils.DeleteSelectedMeasureValue(Convert.ToInt32(measureValueId), measureID, this);
				if (measuresList != null)
				{

					lstSkus.Items.Clear();
					lstSkus.DataSource = measuresList;
					lstSkus.DataTextField = nameof(MeasureValueModel.MeasureValueName);
					lstSkus.DataValueField = nameof(MeasureValueModel.MeasureValueId);
					lstSkus.DataBind();
					DeleteSkuBtn.Visible = false;
					ErrorSuccess1.Text = measureValueName + @" deleted";
				}
				else
					ErrorSuccess1.Text = "Error deleting " + measureValueName;
			}

			/*IvanB end 12/09*/
		}
		protected void skuNameChanged(object sender, EventArgs e)
		{
			AddSkuOnly_Click(null, null);

		}
		protected void SkuSelected_Click(object sender, EventArgs e)
		{
			DeleteSkuBtn.Visible = true;
			DeleteSkuBtn.Enabled = true;
		}

		protected void SkuNameChanged(object sender, EventArgs e)
		{
			AddSkuOnly_Click(null, null);
		}



		#endregion

		#region Others
		protected void fullBox_CheckedChanged(object sender, EventArgs e)
		{
			//passedBox.Checked = false;
			//notPassedBox.Checked = false;
		}

		protected void passedBox_CheckedChanged(object sender, EventArgs e)
		{
			//notPassedBox.Checked = false;
			//fullBox.Checked = false;
		}

		protected void notPassedBox_CheckedChanged(object sender, EventArgs e)
		{
			//passedBox.Checked = false;
			//fullBox.Checked = false;
		}

		protected void runBulkCutGrade_Click(object sender, EventArgs e)
		{
			if (orderText.Text == "")
			{
				errorCutGradeLbl.Visible = true;
				errorCutGradeLbl.Text = "Enter Order";
				return;
			}

			Utils.DissectItemNumber(orderText.Text, out string order, out var batch, out var item);

			//if (batchText.Text == "")
			//{
			//	batchText.Text = "0";
			//	itemText.Text = "0";
			//}
			//else if (itemText.Text == "")
			//	itemText.Text = "0";
			//if (notPassedBox.Checked == false && passedBox.Checked == false && fullBox.Checked == false)
			//	passedBox.Checked = true;
			int checkedBox = 2;
			if (passedBox.Checked)
				checkedBox = 1;

			//var cutGradeList = SyntheticScreeningUtils.GetBulkCutGrades(Convert.ToInt32(orderText.Text), Convert.ToInt16(batchText.Text), Convert.ToInt16(itemText.Text), checkedBox, this);
			var cutGradeList = SyntheticScreeningUtils.GetBulkCutGrades(Convert.ToInt64(order), Convert.ToInt16(batch), Convert.ToInt16(item), checkedBox, this);
			if (cutGradeList != null)
			{
				for (int i = 0; i <= cutGradeList.Rows.Count - 1; i++)
				{
					if (cutGradeList.Rows[i].ItemArray[0].ToString() == "")
						cutGradeList.Rows[i].Delete();

				}

				errorCutGradeLbl.Text = orderText.Text + @" processed";
				CutGradesGrid.Visible = true;
				CutGradesGrid.DataSource = null;
				CutGradesGrid.DataBind();
				CutGradesGrid.DataSource = cutGradeList;
				CutGradesGrid.DataBind();
			}
			else
				errorCutGradeLbl.Text = "Error getting cut grades for " + orderText.Text;
		}

		protected void clearBulkCutGradeBtn_Click(object sender, EventArgs e)
		{
			orderText.Text = "";
			batchText.Text = "";
			itemText.Text = "";
			notPassedBox.Checked = false;
			passedBox.Checked = false;
			fullBox.Checked = false;
			CutGradesGrid.Visible = false;
			CutGradesGrid.DataSource = null;
			CutGradesGrid.DataBind();
		}

		protected void SaveBNEntries_Click(object sender, EventArgs e)
		{
			string customerCode = ddlBNCustomer.SelectedItem.Value;
			DataTable CategoryValues = CreateBNtoCustomerTable("category");
			DataTable DestinationValues = CreateBNtoCustomerTable("destination");
			bool saved = SyntheticScreeningUtils.UpdateBNEntries(CategoryValues, DestinationValues, Convert.ToInt32(customerCode), this);
			if (!saved)
			{
				lblBNError.Visible = true;
				lblBNError.Text = "Error Saving New BN Entries";
				PopupInfoDialog("Error Saving New BN Entries", true);
			}
		}

		protected DataTable CreateBNtoCustomerTable(string type)
		{
			string customerCode = ddlBNCustomer.SelectedItem.Value;
			string customerName = ddlBNCustomer.SelectedItem.Text;
			if (type == "category")
			{
				var table = new DataTable("CategoryValues");
				table.Columns.Add("CustomerName");
				table.Columns.Add("CustomerCode");
				table.Columns.Add("CategoryID");
				for (int i = 0; i <= bnCategoryCheckList.Items.Count - 1; i++)
				{
					if (!bnCategoryCheckList.Items[i].Selected)
						continue;
					string categoryId = bnCategoryCheckList.Items[i].Value;
					table.Rows.Add(new object[]
					{
						customerName, customerCode, categoryId
					}
						);

				}

				table.AcceptChanges();
				return table;
			}
			else if (type == "destination")
			{
				var table = new DataTable("DestinationValues");
				table.Columns.Add("CustomerName");
				table.Columns.Add("CustomerCode");
				table.Columns.Add("DestinationID");
				for (int i = 0; i <= bnDestinationCheckList.Items.Count - 1; i++)
				{
					if (!bnDestinationCheckList.Items[i].Selected)
						continue;
					string destinationId = bnDestinationCheckList.Items[i].Value;
					table.Rows.Add(new object[]
					{
						customerName, customerCode, destinationId
					}
						);

				}

				table.AcceptChanges();
				return table;
			}
			return null;
		}
		protected void btnBNClear_Click(object sender, EventArgs e)
		{
			ddlBNCustomer.SelectedIndex = 0;
			bnCategoryCheckList.Items.Clear();
			bnDestinationCheckList.Items.Clear();
			lblBNError.Text = "";
			lblBNError.Visible = false;
			txtBNCategory.Text = "";
			txtBNDestination.Text = "";
		}

		protected void OnBNAddCategoryClick(object sender, EventArgs e)
		{
			if (txtBNCategory.Text != "")
			{
				bnCategoryCheckList.Items.Add(txtBNCategory.Text);
				bool bdData = SyntheticScreeningUtils.SetNewBNEntries(txtBNCategory.Text, "category", this);
				if (bdData)
					BindBNCustomerEntriesGrid();
			}

		}

		protected void OnBNAddDestinationClick(object sender, EventArgs e)
		{
			if (txtBNDestination.Text != "")
			{
				bnDestinationCheckList.Items.Add(txtBNDestination.Text);
				bool bnData = SyntheticScreeningUtils.SetNewBNEntries(txtBNDestination.Text, "destination", this);
				if (bnData)
					BindBNCustomerEntriesGrid();
			}
		}
		#endregion

		#region Customer's Contractor
		private void LoadCustomersContractor(string filter)
		{

			var customers = QueryUtils.GetCustomers(this);
			customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
			customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
			/*IvanB start*/
			//create and fill blocklist
			var blocked = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);
			//Remove items from a list of customers with a CustomerName equals value from block list
			customers = customers.Where(x => !blocked.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
			/*IvanB end*/
			if (filter != "")
			{
				customers = customers.Where(x => x.CustomerName.Contains(filter)).ToList();
			}
			ddlCustomerforContractor.Items.Clear();
			ddlCustomerforContractor.DataSource = customers;
			ddlCustomerforContractor.DataBind();

			ddlContractorCustomer.Items.Clear();
			ddlContractorCustomer.DataSource = customers;
			ddlContractorCustomer.DataBind();
		}
		private void LoadContractors()
		{
			var contractors = SyntheticScreeningUtils.GetContractors(this);
			ddlContractor.DataSource = contractors;
			ddlContractor.DataBind();
			ListItem liRetailer = new ListItem();
			liRetailer.Text = "";
			liRetailer.Value = "0";
			ddlContractor.Items.Insert(0, liRetailer);
			repContractor.DataSource = contractors;
			repContractor.DataBind();
		}

		private void BindCustomerContractorGrid()
		{
			gvCustomersContractor.Visible = true;
			lblCustomersContractor.Visible = true;
			SyntheticContractorModel contractor = new SyntheticContractorModel();
			contractor.customerCode = ddlCustomerforContractor.SelectedItem.Value;

			var contractors = SyntheticScreeningUtils.GetContractorsAttachments(contractor, this);
			gvCustomersContractor.DataSource = contractors;
			gvCustomersContractor.DataBind();

			//GenerateUniqueData0(1);
			//GenerateUniqueData0(2);
			//GenerateUniqueData0(3);
		}

		protected void ddlContractor_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtContractor.Text = ddlContractor.SelectedItem.Text;
			hdnContractorCode.Value = ddlContractor.SelectedItem.Value;
			ModalPopupExtender2.Hide();
		}
		protected void repContractor_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (string.Compare(e.CommandName, "ContractorId", false) == 0)
			{
				txtContractor.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
				hdnContractorCode.Value = ((System.Web.UI.WebControls.Button)e.CommandSource).CommandArgument;
				ddlContractor.ClearSelection();
				ddlContractor.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
			}
		}
		protected void OnddlCustomerforContractorSelectedChanged(object sender, EventArgs e)
		{
			var customer = ddlCustomerforContractor.Items.FindByValue(ddlCustomerforContractor.SelectedValue);

			if (customer != null)
			{
				txtCustomerCode1.Text = ddlCustomerforContractor.SelectedItem.Value;
				BindCustomerRetailerGrid();
			}
		}
		protected void AddContractorToCustomer_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticContractorModel contractorGet = new SyntheticContractorModel();
				contractorGet.customerCode = ddlCustomerforContractor.SelectedItem.Value;
				var contractors = SyntheticScreeningUtils.GetSyntheticCustomerContractor(contractorGet, this);

				if (contractors.Count > 0)
				{
					foreach (var contractor in contractors)
						if (contractor.contractorId.ToString().Trim().ToLower() == hdnContractorCode.Value)
						{
							ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('contractor already exists for customer.');", true);
							return;
						}
				}
				SyntheticContractorModel contractorSet = new SyntheticContractorModel();
				contractorSet.contractorId = ddlContractor.SelectedItem.Value;
				contractorSet.customerCode = ddlCustomerforContractor.SelectedItem.Value;
				contractorSet.customerName = ddlCustomerforContractor.SelectedItem.Text;
				bool added = SyntheticScreeningUtils.SetSyntheticCustomerContractor(contractorSet, this);
				BindCustomerContractorGrid();
				ddlRetailer.SelectedIndex = 0;
				txtRetailer.Text = "";
				hdnRetailerId.Value = "0";
				lblContractorMsg.Text = "Contractor added to Vendor successfully";
				lblContractorMsg.ForeColor = System.Drawing.Color.Green;

			}
			catch (Exception ex)
			{
				lblContractorMsg.Text = ex.Message.ToString();
				lblContractorMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		protected void DeleteContractor(object sender, EventArgs e)
		{
			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			SyntheticContractorModel contractor = new SyntheticContractorModel();
			HiddenField hdnContractorID = (HiddenField)clickedRow.FindControl("hdnContractorID");
			contractor.customerCode = ddlCustomerforContractor.SelectedItem.Value;
			contractor.contractorId = hdnContractorID.Value;
			bool removed = SyntheticScreeningUtils.DelSyntheticCustomerContractor(contractor, this);
			BindCustomerContractorGrid();
		}
		protected void btnContractorClear_Click(object sender, EventArgs e)
		{
			//lblRetailerMsg.Text = "";
			lblContractorMsg.Text = "";
			ddlContractorCustomer.SelectedIndex = -1;
			txtContractor.Text = "";
			//txtRetailerName.Value = "";
			LoadCustomersContractor("");
			ddlCustomerforContractor.SelectedIndex = -1;
			gvCustomersContractor.DataSource = null;
			gvCustomersContractor.DataBind();
			/*IvanB start*/
			gvCustomersContractor.Visible = false;
			lblCustomersContractor.Visible = false;
			/*IvanB end*/
			//BindCustomerRetailerGrid();
		}
		protected void gvCustomersContractor_OnRowDataBound(object sender, EventArgs e)
		{
			for (int i = gvCustomersContractor.Rows.Count - 1; i > 0; i--)
			{
				GridViewRow row = gvCustomersContractor.Rows[i];
				GridViewRow previousRow = gvCustomersContractor.Rows[i - 1];
				for (int j = 0; j < row.Cells.Count - 1; j++)
				{
					//Define what index on your template field cell that contain same value
					if (((LinkButton)row.Cells[1].FindControl("lnkContractorName")).Text == ((LinkButton)previousRow.Cells[1].FindControl("lnkContractorName")).Text)
					{
						if (previousRow.Cells[1].RowSpan == 0)
						{
							if (row.Cells[1].RowSpan == 0)
							{
								previousRow.Cells[1].RowSpan += 2;
							}
							else
							{
								previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
								previousRow.Cells[1].VerticalAlign = VerticalAlign.Top;
							}
							row.Cells[1].Visible = false;
						}
						if (previousRow.Cells[0].RowSpan == 0)
						{
							if (row.Cells[0].RowSpan == 0)
							{
								previousRow.Cells[0].RowSpan += 2;
							}
							else
							{
								previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
								previousRow.Cells[0].VerticalAlign = VerticalAlign.Top;
							}
							row.Cells[0].Visible = false;
						}
					}
					if (((LinkButton)row.Cells[2].FindControl("lnkServiceTypeName")).Text != "")
					{
						if (((LinkButton)row.Cells[2].FindControl("lnkServiceTypeName")).Text == ((LinkButton)previousRow.Cells[2].FindControl("lnkServiceTypeName")).Text)
						{
							if (previousRow.Cells[2].RowSpan == 0)
							{
								if (row.Cells[2].RowSpan == 0)
								{
									previousRow.Cells[2].RowSpan += 2;
								}
								else
								{
									previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
									previousRow.Cells[2].VerticalAlign = VerticalAlign.Top;
								}
								row.Cells[2].Visible = false;
							}
						}
					}
				}
			}
		}

		protected void OnCustomersContractorCodeClick(object sender, EventArgs e)
		{
			LoadCustomers(txtCustomerforContractor.Text);
			for (int i = 0; i <= ddlCutomer.Items.Count - 1; i++)
			{
				if (ddlCustomerforContractor.Items[i].Text.Contains(txtCustomerforContractor.Text.Trim()))
				{
					ddlCustomerforContractor.SelectedIndex = i;
					BindCustomerContractorGrid();
					break;
				}
			}
			if (ddlCustomerforContractor.Items.Count == 0)
			{
				ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('Customer Code not found');", true);
				return;
			}
		}

		protected void OnCustomersContractorSelectedChanged(object sender, EventArgs e)
		{
			var customer = ddlCustomerforContractor.Items.FindByValue(ddlCustomerforContractor.SelectedValue);

			if (customer != null)
			{
				txtCustomerforContractor.Text = ddlCustomerforContractor.SelectedItem.Value;
				BindCustomerContractorGrid();
			}
		}

		#endregion

		#region Contractor
		public void BindContractorServiceType()
		{
			DataTable dt = SyntheticScreeningUtils.GetSyntheticServiceType(this);
			chklstContractorServiceType.DataSource = dt;
			chklstContractorServiceType.DataTextField = "ServiceTypeName";
			chklstContractorServiceType.DataValueField = "ServiceTypeCode";
			chklstContractorServiceType.DataBind();
		}
		public void BindContractorServiceTypeGrid(string sortExpression = null)
		{
			DataTable serviceType = SyntheticScreeningUtils.GetAllServiceType(this);

			if (sortExpression != null)
			{
				DataView dv = serviceType.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvServiceType.DataSource = dv;
			}
			else
			{
				gvServiceType.DataSource = serviceType;
			}
			gvServiceType.DataBind();

		}
		protected void btnContractorAdd_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticContractorModel contractor = new SyntheticContractorModel();
				contractor.contractorId = hdnContractorId.Value;
				contractor.customerCode = ddlContractorCustomer.SelectedItem.Value.Trim();
				List<string> selectedValue = (from ListItem lst in chklstContractorServiceType.Items where lst.Selected select lst.Value).ToList();
				contractor.ServiceTypeCodeSet = string.Join(";", selectedValue.Select(x => string.Format("{0}", x)).ToArray());
				DataTable dtContractor = SyntheticScreeningUtils.SetContractor(contractor, this);
				if (dtContractor.Rows.Count >= 1)
				{

					lblContractorMsg.Text = "Contractor already exist.";
					lblContractorMsg.ForeColor = System.Drawing.Color.Red;
					//ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Retailer already exist.');", true);
					//Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<script type = 'text/javascript'>alert('Retailer Already Exist!'); </script>", true);
					return;
				}
				BindContractorsGrid();
				ClearContractor();
				lblContractorsMsg.Text = "Contractor data added successfully";
				lblContractorsMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblContractorsMsg.Text = ex.Message.ToString();
				lblContractorsMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		protected void EditContractor(object sender, EventArgs e)
		{
			ClearContractor();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			LinkButton lnkContractorName = (LinkButton)clickedRow.FindControl("lnkContractorName");
			HiddenField hdnContractorID = (HiddenField)clickedRow.FindControl("hdnContractorID");
			LinkButton lnkServiceType = (LinkButton)clickedRow.FindControl("lnkServiceType");
			HiddenField hdnServiceTypeCodeSet = (HiddenField)clickedRow.FindControl("hdnServiceTypeCodeSet");

			//txtContractorName.Text = lnkContractorName.Text;
			hdnContractorId.Value = hdnContractorID.Value;
			ddlContractorCustomer.Items.FindByValue(hdnContractorID.Value).Selected = true;
			lblRetailerMsg.Text = "";
			string serviceType = lnkServiceType.Text;
			string[] serviceTypeCodeSet = hdnServiceTypeCodeSet.Value.Split(';');

			if (hdnServiceTypeCodeSet.Value != "")
			{
				foreach (string inst in serviceTypeCodeSet)
				{
					int i = Convert.ToInt16(inst);
					chklstContractorServiceType.Items.FindByValue(i.ToString()).Selected = true;
				}
			}
		}
		//private string SortDirection
		//{
		//	get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
		//	set { ViewState["SortDirection"] = value; }
		//}

		protected void btnContractorsClear_Click(object sender, EventArgs e)
		{
			ClearContractor();
		}

		private void ClearContractor()
		{
			//hdnContractorId.Value = "0";
			//txtContractorName.Text = "";
			ddlContractorCustomer.SelectedIndex = -1;
			lblContractorsMsg.Text = "";
			foreach (ListItem li in chklstContractorServiceType.Items)
				li.Selected = false;
		}
		protected void OnSortingContractor(object sender, GridViewSortEventArgs e)
		{
			this.BindContractorsGrid(e.SortExpression);
		}
		protected void gvContractor_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				LinkButton lnkServiceType = (LinkButton)e.Row.FindControl("lnkServiceType");
				if (lnkServiceType.Text != "")
				{
					lnkServiceType.Text = lnkServiceType.Text.Replace(",", "</br>");
				}
			}
		}
		public void BindContractorsGrid(string sortExpression = null)
		{
			DataTable contractor = SyntheticScreeningUtils.GetAllContractor(this);

			if (sortExpression != null)
			{
				DataView dv = contractor.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvContractor.DataSource = dv;
			}
			else
			{
				gvContractor.DataSource = contractor;

			}
			gvContractor.DataBind();
		}


		#endregion

		#region CompanyGroupName

		protected void imgMoveToGroupList_Click(object sender, ImageClickEventArgs e)
		{
			// Loop through the items in listBox1 and add the selected items to listBox2
			foreach (ListItem item in lstCustomerList.Items)
			{
				if (item.Selected)
				{
					// Check if the item is already in listBox2 to avoid duplicates
					if (!lstGroupList.Items.Contains(item))
					{
						lstGroupList.Items.Add(item);
						break;
					}
				}
			}
		}

		protected void imgMoveToCustomerList_Click(object sender, ImageClickEventArgs e)
		{
			// Loop through the items in listBox1 and add the selected items to listBox2
			foreach (ListItem item in lstGroupList.Items)
			{
				if (item.Selected)
				{
					// Remove selected item
					lstGroupList.Items.Remove(item);
					break;
				}
			}
		}

		protected void btnSaveCompanyGroup_Click(object sender, EventArgs e)
		{
			string customerIDList = "";
			foreach (ListItem item in lstGroupList.Items)
			{
				if (lstGroupList.Items.Count == 1)
					customerIDList = item.Value;
				else
				{
					if (customerIDList.Length == 0)
						customerIDList = item.Value;
					else
						customerIDList = customerIDList + "," + item.Value;
				}
			}

			if (txtCompanyGroupName.Text == "")
				lblMsgCompanyGroup.Text = "Please provide Company Group Name";
			else if (lstGroupList.Items.Count == 0)
				lblMsgCompanyGroup.Text = "Please add Company to Company Group";
			else
			{
				SyntheticScreeningUtils.SetCompanyGroup(txtCompanyGroupName.Text, customerIDList, this);
				lblMsgCompanyGroup.Text = "Company set to Company Group successfully";
			}
			BindCompanyGroupsGrid();
		}

		protected void btnClearCompanyGroup_Click(object sender, EventArgs e)
		{
			ClearCompanyGroup();
		}

		protected void gvCompanyGroup_Sorting(object sender, GridViewSortEventArgs e)
		{
			this.BindCompanyGroupsGrid(e.SortExpression);
		}

		protected void gvCompanyGroup_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				LinkButton lnkCustomerNameSet = (LinkButton)e.Row.FindControl("lnkCustomerNameSet");
				if (lnkCustomerNameSet.Text != "")
				{
					lnkCustomerNameSet.Text = lnkCustomerNameSet.Text.Replace(";", "</br>");
				}
			}
		}

		protected void EditCompanyGroupName(object sender, EventArgs e)
		{

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			HiddenField hdnCompanyGroupID = (HiddenField)clickedRow.FindControl("hdnCompanyGroupID");
			LinkButton lnkCompanyGroupName = (LinkButton)clickedRow.FindControl("lnkCompanyGroupName");

			txtCompanyGroupName.Text = lnkCompanyGroupName.Text;
			txtCompanyGroupName.Enabled = false;
			hdnCompanyGroupID.Value = hdnCompanyGroupID.Value;

			DataTable companyGroups = SyntheticScreeningUtils.GetCompanyGroupByCompanyGroupID(int.Parse(hdnCompanyGroupID.Value.Trim()), this);
			var customers = (from DataRow row in companyGroups.Rows select CustomerModel.Create1(row)).ToList();

			lstGroupList.Items.Clear();
			lstGroupList.DataSource = customers;
			lstGroupList.DataTextField = nameof(CustomerModel.CustomerName);
			lstGroupList.DataValueField = nameof(CustomerModel.CustomerId);
			lstGroupList.DataBind();
			lblMsgCompanyGroup.Text = "";
		}

		private void ClearCompanyGroup()
		{
			txtCompanyGroupName.Text = "";
			txtCompanyGroupName.Enabled = true;
			hdnCompanyGroupID.Value = "0";
			lstGroupList.Items.Clear();
			lblMsgCompanyGroup.Text = "";
			filterTextbox.Text = "";
		}

		public void BindCompanyGroupsGrid(string sortExpression = null)
		{
			DataTable companyGroups = SyntheticScreeningUtils.GetAllCompanyGroup(this);

			if (sortExpression != null)
			{
				DataView dv = companyGroups.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvCompanyGroup.DataSource = dv;
			}
			else
			{
				gvCompanyGroup.DataSource = companyGroups;

			}
			gvCompanyGroup.DataBind();
		}

		public void BindCustomerList()
		{
			lstCustomerList.Items.Clear();
			var blockList = QueryDropDownBlock.GetDropDownBlockList(1, Page);
			var customers = QueryUtils.GetAllCustomersForBlockEditing(Page).Where(x => !blockList.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName)));
			lstCustomerList.DataSource = customers;
			lstCustomerList.DataTextField = nameof(CustomerModel.CustomerName);
			lstCustomerList.DataValueField = nameof(CustomerModel.CustomerId);
			lstCustomerList.DataBind();
		}



		#endregion
		#region Retailer Customer	

		protected void OnRetailerSelectedChanged(object sender, EventArgs e)
		{
			var retailer = ddlRetailerList.Items.FindByValue(ddlRetailerList.SelectedValue);
			var retailerId = retailer != null ? ddlRetailerList.SelectedItem.Value : "0";
			if (!string.IsNullOrEmpty(retailerId) && retailerId != "0")
			{
				//txtRetailerID.Text = retailerId;

				List<BrandModel> brands = SyntheticScreeningUtils.GetRetailerBrands(Convert.ToInt32(retailerId), this);
				SetViewState(brands, "RetailerBrands");

				List<BrandProgramModel> programs = SyntheticScreeningUtils.GetRetailerPrograms(retailerId, this);
				SetViewState(programs, "RetailerPrograms");

				BuildRetailerVendorsList(retailerId);

				BindNatureList(ddlNature);
				ddlNature.Enabled = true;

				BindBrandList();
				ddlBrand.Enabled = true;

				BindProgramList();
				ddlProgram.Enabled = true;
			}
			else
			{
				btnCustomerClear_Click(null, null);
			}
		}

		private void BindProgramList()
		{
			ddlProgram.DataSource = GetAllPrograms();
			ddlProgram.DataValueField = "Id";
			ddlProgram.DataTextField = "DisplayProgramName";
			ddlProgram.DataBind();
			ddlProgram.Items.Insert(0, new ListItem(SessionConstants.ShowAll, String.Empty));
			ddlProgram.SelectedIndex = 0;
		}

		private void BindBrandList()
		{
			ddlBrand.DataSource = GetBrands();
			ddlBrand.DataTextField = "DisplayBrandName";
			ddlBrand.DataValueField = "BrandId";
			ddlBrand.DataBind();
			ddlBrand.Items.Insert(0, new ListItem(SessionConstants.ShowAll, String.Empty));
			ddlBrand.SelectedIndex = 0;
		}

		private void BuildRetailerVendorsList(string retailerId)
		{
			var vendors = GetRetailerVendors(retailerId);
			AddGroupInfo(vendors);
			AddProgramInfo(vendors);

			BindRetailerCustomerGrid(vendors, retailerId, null);
			SetViewState(vendors, "RetailerVendors");
			ddlRetailerVendors.DataSource = vendors;
			ddlRetailerVendors.DataBind();
			ddlRetailerVendors.Items.Insert(0, new ListItem(String.Empty, String.Empty));
			ddlRetailerVendors.SelectedIndex = 0;
			ddlRetailerVendors.Enabled = true;
		}
		private void AddGroupInfo(DataTable vendors)
		{
			if (!vendors.Columns.Contains("VendorGroup"))
			{
				vendors.Columns.Add("VendorGroup");
			}
			foreach (DataRow dr in vendors.Rows)
			{
				var customerId = dr["CustomerID"].ToString();
				var groupName = GetVendorGroupName(customerId);
				dr["VendorGroup"] = groupName;
			}
		}

		private void AddProgramInfo(DataTable vendors)
		{
			if (!vendors.Columns.Contains("VendorProgram"))
			{
				vendors.Columns.Add("VendorProgram");
			}
			foreach (DataRow dr in vendors.Rows)
			{
				dr["VendorProgram"] = "";
			}
		}


		protected void OnNatureSelectedChanged(object sender, EventArgs e)
		{
			BindBrandList();
			BindProgramList();
			OnProgramSelectedChanged(null, null);
		}
		protected void OnBrandSelectedChanged(object sender, EventArgs e)
		{
			BindProgramList();
			OnProgramSelectedChanged(null, null);
		}

		protected void OnProgramSelectedChanged(object sender, EventArgs e)
		{
			var programId = (ddlProgram.SelectedItem != null && !string.IsNullOrEmpty(ddlProgram.SelectedItem.Value)) ? Convert.ToInt32(ddlProgram.SelectedItem.Value) : 0;
			var vendors = GetViewState("RetailerVendors") as DataTable;
			if (programId > 0)
			{
				List<string> vendorsByProg = SyntheticScreeningUtils.GetCustomersByProgram(programId, this);

				if (vendorsByProg.Count > 0)
				{
					var result = vendors.Clone();
					foreach (string vbp in vendorsByProg)
					{
						var rows = vendors.AsEnumerable().Where(dr => dr["CustomerCode"].ToString().Equals(vbp)).ToList();
						foreach (DataRow row in rows)
						{
							result.ImportRow(row);
						}
					}
					gvRetailersCustomers.DataSource = result;
					gvRetailersCustomers.DataBind();

					ddlRetailerVendors.DataSource = result;
					ddlRetailerVendors.DataBind();
					ddlRetailerVendors.Items.Insert(0, new ListItem(String.Empty, String.Empty));
					ddlRetailerVendors.SelectedIndex = 0;
					ddlRetailerVendors.Enabled = true;
				}
				else
				{
					gvRetailersCustomers.DataSource = new DataTable();
					gvRetailersCustomers.DataBind();

					ddlRetailerVendors.DataSource = null;
					ddlRetailerVendors.DataBind();
				}
			}
			else
			{
				gvRetailersCustomers.DataSource = vendors;
				gvRetailersCustomers.DataBind();

				ddlRetailerVendors.DataSource = vendors;
				ddlRetailerVendors.DataBind();
				ddlRetailerVendors.Items.Insert(0, new ListItem(String.Empty, String.Empty));
				ddlRetailerVendors.SelectedIndex = 0;
				ddlRetailerVendors.Enabled = true;
			}
		}

		private void BindNatureList(DropDownList ddl)
		{
			ddl.Items.Clear();
			ddl.DataSource = GetSKUNature();
			ddl.DataTextField = "Text";
			ddl.DataValueField = "Value";
			ddl.DataBind();
			ddl.Items.Insert(0, new ListItem(SessionConstants.AllTypes, String.Empty));
			ddl.SelectedIndex = 0;
		}

		protected List<ListItem> GetSKUNature()
		{
			List<ListItem> result = new List<ListItem>();
			result.Add(new ListItem { Value = "158", Text = "Natural" });
			result.Add(new ListItem { Value = "6264", Text = "Lab Grown" });
			return result;
		}

		protected void OnRetailerVendorsSelectedChanged(object sender, EventArgs e)
		{
			var customerId = ddlRetailerVendors.SelectedItem.Value;
			var retailerId = ddlRetailerList.SelectedItem.Value;
			var vendors = GetViewState("RetailerVendors") as DataTable;
			BindRetailerCustomerGrid(vendors, retailerId, customerId);
		}


		protected void OnRetailerIDClick(object sender, EventArgs e)
		{
			//do smt
		}

		protected void DeleteVendor(object sender, EventArgs e)
		{
			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			SyntheticRetailerModel retailer = new SyntheticRetailerModel();
			HiddenField hdnVendorCode = (HiddenField)clickedRow.FindControl("hdnVendorCode");
			var retailerId = ddlRetailerList.SelectedItem.Value;
			retailer.customerCode = hdnVendorCode.Value;
			retailer.retailerId = retailerId;
			bool removed = SyntheticScreeningUtils.DelSyntheticCustomerRetailer(retailer, this);
			BuildRetailerVendorsList(retailerId);
		}

		protected void ViewVendor(object sender, EventArgs e)
		{
			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			List<SyntheticRetailerModel> vendors = new List<SyntheticRetailerModel>();
			SyntheticRetailerModel vendor = new SyntheticRetailerModel();
			HiddenField hdnVendorId = (HiddenField)clickedRow.FindControl("hdnVendorId");
			HiddenField hdnVendorOfficeId = (HiddenField)clickedRow.FindControl("hdnVendorOfficeId");

			var customerId = hdnVendorId.Value.ToString();
			var retailerId = ddlRetailerList.SelectedItem.Value;
			var officeId = hdnVendorOfficeId.Value.ToString();
			var nature = ddlNature.SelectedItem.Value != null ? ddlNature.SelectedItem.Value : "";
			var brand = ddlBrand.SelectedItem.Value != null ? ddlBrand.SelectedItem.Value : "";
			var program = ddlProgram.SelectedItem.Value != null ? ddlProgram.SelectedItem.Value : "";
			string url = string.Format("RetailersVendorInfo.aspx?cid={0}&rid={1}&cod={2}&nt={3}&br={4}&prm={5}", customerId, retailerId, officeId, nature, brand, program);
			string fullURL = "window.open('" + url + "', '_blank' );";
			ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
		}
		private void BindRetailerCustomerGrid(DataTable vendors, string retailerId, string customerId)
		{
			gvRetailersCustomers.Visible = true;
			lblRetailersVen.Visible = true;
			var result = vendors.Clone();
			if (!string.IsNullOrEmpty(customerId))
			{
				var rows = vendors.AsEnumerable().Where(dr => dr.Field<decimal>("CustomerID") == Convert.ToInt32(customerId)).ToList();
				foreach (DataRow row in rows)
				{
					result.ImportRow(row);
				}
			}
			else
			{
				result = vendors;
			}
			gvRetailersCustomers.DataSource = result;
			gvRetailersCustomers.DataBind();
		}
		private DataTable GetRetailerVendors(string retailerId)
		{
			var customers = SyntheticScreeningUtils.GetRetailerCustomers(retailerId, this);
			var data = customers.Tables[0];
			return data;
		}

		private List<BrandProgramModel> GetAllPrograms()
		{
			List<BrandProgramModel> allPrograms = GetViewState("RetailerPrograms") as List<BrandProgramModel>;
			var natureSorted = allPrograms;
			var result = allPrograms;

			var nature = ddlNature.SelectedItem.Value != null ? ddlNature.SelectedItem.Text : "";
			if (!string.IsNullOrEmpty(nature) && nature != SessionConstants.AllTypes)
			{
				var natureName = SessionConstants.LGfull.Equals(nature) ? SessionConstants.LGshort : nature;
				natureSorted = allPrograms.FindAll(x => x.Nature == natureName).ToList();
				result = natureSorted;
			}

			var brandName = ddlBrand.SelectedItem.Value != null ? ddlBrand.SelectedItem.Text : "";
			if (!string.IsNullOrEmpty(brandName) && brandName != SessionConstants.ShowAll)
			{
				var brandId = ddlBrand.SelectedItem.Value;
				result = natureSorted.FindAll(x => x.BrandId == brandId).ToList();
			}
			return result;
		}

		private List<BrandModel> GetBrands()
		{
			List<BrandModel> allBrands = GetViewState("RetailerBrands") as List<BrandModel>;
			List<BrandModel> result = allBrands;
			var nature = ddlNature.SelectedItem.Value != null ? ddlNature.SelectedItem.Text : "";
			if (!string.IsNullOrEmpty(nature) && nature != SessionConstants.AllTypes)
			{
				var natureName = SessionConstants.LGfull.Equals(nature) ? SessionConstants.LGshort : nature;
				result = allBrands.FindAll(x => x.Nature == natureName).ToList();
			}
			return result;
		}

		private void GetAllBrands()
		{
			List<BrandModel> brands = SyntheticScreeningUtils.GetAllBrands(this);
			SetViewState(brands, "FullBrandList");
		}

		protected void gvRetailersCustomer_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var retailerId = ddlRetailerList.SelectedItem.Value;
			var programList = GetViewState("RetailerPrograms") as List<BrandProgramModel>;
			if (e.Row.RowType == DataControlRowType.DataRow)
			{

				if (e.Row.DataItem != null)
				{
					Label lblVendorPrograms = (Label)e.Row.FindControl("lblVendorPrograms");
					var customerCode = DataBinder.Eval(e.Row.DataItem, "CustomerCode").ToString();
					string programListString = GetProgramListString(customerCode);
					lblVendorPrograms.Text = programListString;
					e.Row.Cells[3].Attributes.Add("style", "overflow-wrap:anywhere;");
					e.Row.Cells[4].Attributes.Add("style", "width: 115px;");
					//var customerId = DataBinder.Eval(e.Row.DataItem, "CustomerID").ToString();
					//var groupName = GetVendorGroupName(customerId);
					//lblVendorGroup.Text = groupName;
				}
			}
		}

		protected string GetProgramListString(string customerCode)
		{
			string result = "";
			List<BrandProgramModel> programList = SyntheticScreeningUtils.GetVendorPrograms(Convert.ToInt32(customerCode), this);
			if (programList.Count > 0)
			{
				foreach (BrandProgramModel program in programList)
				{
					result = result != "" ? result + ", " + program.ProgramName : program.ProgramName;
				}
			}
			return result;
		}
		protected string GetVendorGroupName(string customerId)
		{
			string result;
			CustomerModel objCustomerModel = new CustomerModel();
			objCustomerModel.CustomerId = customerId;
			var customers = QueryUtils.GetCustomerCompanyGroup(objCustomerModel, this);
			if (customers != null && customers.Rows.Count > 0)
			{
				var companyGroupName = customers.Rows[0]["CompanyGroupName"].ToString();
				result = companyGroupName.Trim();
			}
			else
			{
				result = "";
			}
			return result;
		}
		protected void gvRetailersCustomer_OnDataBound(object sender, EventArgs e)
		{


		}
		protected void btnCustomerClear_Click(object sender, EventArgs e)
		{
			lblMsg.Text = "";
			//txtRetailerID.Text = "";
			ddlRetailerList.SelectedIndex = -1;
			SetViewState(new DataTable(), "RetailerVendors");
			gvRetailersCustomers.DataSource = null;
			gvRetailersCustomers.DataBind();
			gvRetailersCustomers.Visible = false;
			lblRetailersVen.Visible = false;
			ddlRetailerVendors.Items.Clear();
			ddlRetailerVendors.Enabled = false;
			ddlProgram.Items.Clear();
			ddlProgram.Enabled = false;
			ddlBrand.Items.Clear();
			ddlBrand.Enabled = false;
			ddlNature.Items.Clear();
			ddlNature.Enabled = false;
			SetViewState(new List<BrandProgramModel>(), "RetailerPrograms");
		}
		protected void CleanActionMessages()
		{
			lblActionError.Text = "";
			lblActionError.Visible = false;
			lblActionSuccess.Text = "";
			lblActionSuccess.Visible = false;
		}
		protected void btnAddNewBrand_OnClick(object sender, EventArgs e)
		{
			CleanNewBrandDialog();
			BindAndSelectNewBrandDdl();
			NewBrandPopupExtender.Show();
		}

		protected void CleanNewBrandDialog()
		{
			NewBrandNameFld.Text = "";
			ErrNewBrandNameLabel.Text = "";
			ErrNewBrandNameLabel.Visible = false;
		}

		protected void BindAndSelectNewBrandDdl()
		{
			//binding nature list
			ddlNewBrandNature.DataSource = GetSKUNature();
			ddlNewBrandNature.DataTextField = "Text";
			ddlNewBrandNature.DataValueField = "Value";
			ddlNewBrandNature.DataBind();
			//selecting chosen nature or first in list
			var selectedNatureId = ddlNature.SelectedItem != null ? ddlNature.SelectedItem.Value : "";
			var selectedNatureItem = ddlNewBrandNature.Items.FindByValue(selectedNatureId);
			if (selectedNatureItem == null)
			{
				selectedNatureItem = ddlNewBrandNature.Items[0];
			}
			selectedNatureItem.Selected = true;
		}

		protected void OnNewBrandDialogOkClick(object sender, EventArgs e)
		{
			var newBrandName = NewBrandNameFld.Text.Trim();
			var natureName = ddlNewBrandNature.SelectedItem.Text;
			var nature = SessionConstants.LGfull.Equals(natureName) ? SessionConstants.LGshort : SessionConstants.Natural;
			var error = "";
			//Check if name is empty
			if (string.IsNullOrEmpty(newBrandName))
			{
				error = "Brand Name is empty!";
				ShowPopapError(error, ErrNewBrandNameLabel, NewBrandPopupExtender);
				return;
			}
			//Check if already in list
			List<BrandModel> allBrands = GetViewState("FullBrandList") as List<BrandModel>;
			var found = allBrands.Find(x => (x.BrandName.Trim() == newBrandName && x.Nature==nature));
			if (found != null)
			{
				error = "Brand " + newBrandName + " already exist!";
				ShowPopapError(error, ErrNewBrandNameLabel, NewBrandPopupExtender);
				return;
			}
			else
			{
				var message = SyntheticScreeningUtils.AddNewBrand(newBrandName, nature, this);
				var targetLbl = message.Contains("failed") ? lblActionError : lblActionSuccess;
				targetLbl.Text = message;
				targetLbl.Visible = true;
				if (!message.Contains("failed"))
				{
					GetAllBrands();
					if (ddlBrand.Enabled)
					{
						BindBrandList();
					}
				}
			}
		}

		protected void btnAddNewProgram_OnClick(object sender, EventArgs e)
		{
			CleanNewProgramDialog();
			BindAndSelectNewProgramDdlDialog();
			NewProgramPopupExtender.Show();
		}
		protected void CleanNewProgramDialog()
		{
			NewProgramNameFld.Text = "";
			ErrNewProgramNameLabel.Text = "";
			ErrNewProgramNameLabel.Visible = false;
			ddlNewProgramBrand.Items.Clear();
			ddlNewProgramNature.Items.Clear();
		}
		protected void BindAndSelectNewProgramDdlDialog()
        {
            //binding brand list
            List<BrandModel> allBrands = GetViewState("FullBrandList") as List<BrandModel>;
            ddlNewProgramBrand.DataSource = allBrands;
            ddlNewProgramBrand.DataTextField = "DisplayBrandName";
            ddlNewProgramBrand.DataValueField = "BrandId";
            ddlNewProgramBrand.DataBind();
            //selecting chosen brand or first in list
            var selectedBrandId = ddlBrand.SelectedItem != null ? ddlBrand.SelectedItem.Value : "";
            var selectedBrandItem = ddlNewProgramBrand.Items.FindByValue(selectedBrandId);
            if (selectedBrandItem == null)
            {
                selectedBrandItem = ddlNewProgramBrand.Items[0];
            }
            selectedBrandItem.Selected = true;
            //binding nature list
            ddlNewProgramNature.DataSource = GetSKUNature();
            ddlNewProgramNature.DataTextField = "Text";
            ddlNewProgramNature.DataValueField = "Value";
            ddlNewProgramNature.DataBind();
            //selecting nature by Brand Nature, for Non-branded first in list
            SelectNatureByChosenBrand();
        }

        private void SelectNatureByChosenBrand()
        {
            var nature = GetBrandNature(ddlNewProgramBrand.SelectedItem.Value);
            if ("".Equals(nature))
            {
                ddlNewProgramNature.Items[0].Selected = true;
                ddlNewProgramNature.Enabled = true;
            }
            else
            {
				var selIndx = SessionConstants.Natural.Equals(nature) ? 0 : 1;
				ddlNewProgramNature.SelectedIndex = selIndx;
                ddlNewProgramNature.Enabled = false;
            }
        }

        protected string GetBrandNature(string brandId)
		{
			var result = "";
			List <BrandModel> allBrands = GetViewState("FullBrandList") as List<BrandModel>;
			var selected = allBrands.Where(x => x.BrandId == brandId).FirstOrDefault();
			if (selected != null)
			{
				result = selected.Nature;
			}
			return result;
		}
		protected void OnNewProgramBrandSelectedChanged(object sender, EventArgs e)
		{
			SelectNatureByChosenBrand();
			NewProgramPopupExtender.Show();
		}

		protected void OnNewProgramDialogOkClick(object sender, EventArgs e)
		{
			var error = "";
			var programName = NewProgramNameFld.Text;
			if (string.IsNullOrEmpty(programName))
			{
				error = "Program Name is empty!";
				ShowPopapError(error, ErrNewProgramNameLabel, NewProgramPopupExtender);
				return;
			}

			var brandId = ddlNewProgramBrand.SelectedItem.Value;
			var brandName = ddlNewProgramBrand.SelectedItem.Text;
			var natureName = ddlNewProgramNature.SelectedItem.Text;
			var nature = SessionConstants.LGfull.Equals(natureName) ? SessionConstants.LGshort : SessionConstants.Natural;

			var isExist = SyntheticScreeningUtils.IsProgramExist(Convert.ToInt32(brandId), programName, nature, this);

			if (isExist)
			{
				error = natureName + " program " + programName + " for brand " + brandName + " already exist!";
				ShowPopapError(error, ErrNewProgramNameLabel, NewProgramPopupExtender);
				return;
			}
			else
			{
				var message = SyntheticScreeningUtils.AddNewProgram(Convert.ToInt32(brandId), brandName, programName, nature, this);
				var targetLbl = message.Contains("failed") ? lblActionError : lblActionSuccess;
				targetLbl.Text = message;
				targetLbl.Visible = true;
				if (!message.Contains("failed"))
				{
					GetAllRetailerPrograms();
					if (ddlProgram.Enabled)
					{
						BindProgramList();
					}
				}

			}

		}

		private void GetAllRetailerPrograms()
		{
			var retailer = ddlRetailerList.Items.FindByValue(ddlRetailerList.SelectedValue);
			var retailerId = retailer != null ? ddlRetailerList.SelectedItem.Value : "0";
			List<BrandProgramModel> programs = SyntheticScreeningUtils.GetRetailerPrograms(retailerId, this);
			SetViewState(programs, "RetailerPrograms");
		}

		protected void btnBrandToRetailer_OnClick(object sender, EventArgs e)
		{

			CleanBrandToVendorDialog();
			BindAndSelectRetailerBrandDialog();
			AssignBrandToRetailerPopupExtender.Show();
		}


		protected void CleanBrandToVendorDialog()
		{
			lblAssignBrandToRetailerError.Text = "";
			lblAssignBrandToRetailerError.Visible = false;
			ddlABRetailer.Items.Clear();
			ddlABBrand.Items.Clear();
			ClearAssignedBrandList();
		}
		protected void BindAndSelectRetailerBrandDialog()
        {
            //bind nature list, we not selecting chosen
            BindNatureList(ddlABNature);
			//disable nature selection before retailer selected
			ddlABNature.Enabled = false;
            //binding retailer list
            DataTable retailers = GetViewState("Retailers") as DataTable;
            ddlABRetailer.DataSource = retailers;
            ddlABRetailer.DataTextField = "RetailerName";
            ddlABRetailer.DataValueField = "RetailerID";
            ddlABRetailer.DataBind();
            ListItem liRetailer = new ListItem();
            liRetailer.Text = "";
            liRetailer.Value = "0";
            ddlABRetailer.Items.Insert(0, liRetailer);
            //selecting chosen retailer or empty one
            var selectedRetailerId = ddlRetailerList.SelectedValue;
            if (!string.IsNullOrEmpty(selectedRetailerId) && selectedRetailerId != "0")
            {
                var selectedRetailerItem = ddlABRetailer.Items.FindByValue(selectedRetailerId);
                if (selectedRetailerItem != null)
                {
                    selectedRetailerItem.Selected = true;
                }
                BindAssignedBrandsList(selectedRetailerId, SessionConstants.AllTypes);
				ddlABNature.Enabled = true;
            }
            else
            {
                ddlABRetailer.SelectedIndex = 0;
                ClearAssignedBrandList();
            }

            //binding Brand list, initialy show all types of nature
            BindABBrandDll(SessionConstants.AllTypes);
            //selecting chosen brand or first in list
            var selectedBrandId = ddlBrand.SelectedItem != null ? ddlBrand.SelectedItem.Value : "";
            var selectedBrandItem = ddlABBrand.Items.FindByValue(selectedBrandId);
            if (selectedBrandItem != null)
            {
                selectedBrandItem.Selected = true;
            }
            else
            {
                ddlABBrand.SelectedIndex = 0;
            }
        }

        private void BindABBrandDll(string nature)
        {
            List<BrandModel> allBrands = GetViewState("FullBrandList") as List<BrandModel>;
            var filtered = allBrands.Where(x => x.BrandName != SessionConstants.NonBranded).ToList();
			var result = filtered;
			if (!SessionConstants.AllTypes.Equals(nature))
			{
				result = filtered.Where(x => x.Nature == nature).ToList();
			}
            ddlABBrand.DataSource = result;
            ddlABBrand.DataTextField = "DisplayBrandName";
            ddlABBrand.DataValueField = "BrandId";
            ddlABBrand.DataBind();
            ddlABBrand.Items.Insert(0, new ListItem(String.Empty, String.Empty));
        }

        protected void OnAssignBrandDialogOkClick(object sender, EventArgs e)
		{
			var error = "";
			var retailerId = ddlABRetailer.SelectedValue;
			//Check if retailer selected
			if (string.IsNullOrEmpty(retailerId) || retailerId == "0")
			{
				error = "Please select Retailer.";
				ShowPopapError(error, lblAssignBrandToRetailerError, AssignBrandToRetailerPopupExtender);
				return;
			}

			var brandId = ddlABBrand.SelectedValue;
			//Check if brand selected
			if (string.IsNullOrEmpty(brandId) || brandId == "0")
			{
				error = "Please select Brand.";
				ShowPopapError(error, lblAssignBrandToRetailerError, AssignBrandToRetailerPopupExtender);
				return;
			}
			//Check if Brand already in list
			var retailerName = ddlABRetailer.SelectedItem.Text;
			var brandName = ddlABBrand.SelectedItem.Text;
			var assigned = retailersBrandList.Items;
			var isExist = assigned.FindByValue(brandId);
			if (isExist != null)
			{
				error = "Brand " + brandName + " already assigned to Retailer " + retailerName + "!";
				ShowPopapError(error, lblAssignBrandToRetailerError, AssignBrandToRetailerPopupExtender);
				return;
			}
			else
			{
				var message = SyntheticScreeningUtils.AddBrandToRetailer(Convert.ToInt32(retailerId), Convert.ToInt32(brandId), this);
				if (message.Contains("failed"))
				{
					message = "Assigning Brand " + brandName + " to Retailer " + retailerName + " failed!";
				}
				else
				{
					message = "Brand " + brandName + " successfully assigned to Retailer " + retailerName + "!";
				}
				var targetLbl = message.Contains("failed") ? lblActionError : lblActionSuccess;
				targetLbl.Text = message;
				targetLbl.Visible = true;
			}

		}
		protected void OnDeleteBrandDialogOkClick(object sender, EventArgs e)
		{
			var error = "";
			var retailerId = ddlABRetailer.SelectedValue;
			//Check if retailer selected
			if (string.IsNullOrEmpty(retailerId) || retailerId == "0")
			{
				error = "Please select Retailer.";
				ShowPopapError(error, lblAssignBrandToRetailerError, AssignBrandToRetailerPopupExtender);
				return;
			}
			var selectedBrand = retailersBrandList.SelectedItem;
			//Check if brand to delete selected
			if (selectedBrand == null)
			{
				error = "Please select Brand from Assigned Brands list!";
				ShowPopapError(error, lblAssignBrandToRetailerError, AssignBrandToRetailerPopupExtender);
				return;
			}
			var message = SyntheticScreeningUtils.DeleteRetailerBrand(Convert.ToInt32(retailerId), Convert.ToInt32(selectedBrand.Value), this);
			var retailerName = ddlABRetailer.SelectedItem.Text;
			var brandName = retailersBrandList.SelectedItem.Text;
			if (message.Contains("failed"))
			{
				message = "Deleting Brand " + brandName + " from Retailer " + retailerName + " failed!";
			}
			else
			{
				message = "Brand " + brandName + " successfully deleted from Retailer " + retailerName + "!";
			}
			var targetLbl = message.Contains("failed") ? lblActionError : lblActionSuccess;
			targetLbl.Text = message;
			targetLbl.Visible = true;
		}

		protected void OnAssignBrandRetailerSelectedChanged(object sender, EventArgs e)
		{
			var retailerId = ddlABRetailer.SelectedValue;
			ClearAssignedBrandList();
			if (!string.IsNullOrEmpty(retailerId) && retailerId != "0")
			{
				BindAssignedBrandsList(retailerId, SessionConstants.AllTypes);
				ddlABNature.Enabled = true;
			}
			AssignBrandToRetailerPopupExtender.Show();
		}
		protected void OnAssignBrandNatureSelectedChanged(object sender, EventArgs e)
		{
			var retailerId = ddlABRetailer.SelectedValue;
			var natureName = ddlABNature.SelectedItem.Text;
			var nature = natureName;
			if (!SessionConstants.AllTypes.Equals(natureName))
			{
				nature = SessionConstants.LGfull.Equals(natureName) ? SessionConstants.LGshort : SessionConstants.Natural;
			}
			ClearAssignedBrandList();
			//binding Brand list by nature
			BindABBrandDll(nature);
			if (!string.IsNullOrEmpty(retailerId) && retailerId != "0")
			{
				BindAssignedBrandsList(retailerId, nature);
			}

			AssignBrandToRetailerPopupExtender.Show();
		}

		private void ClearAssignedBrandList()
		{
			retailersBrandList.Items.Clear();
			retailersBrands.Visible = false;
		}

		private void BindAssignedBrandsList(string retailerId, string nature)
		{
			var assignedBrandsList = SyntheticScreeningUtils.GetRetailerBrands(Convert.ToInt32(retailerId), this);
			var result = assignedBrandsList;
			if (!SessionConstants.AllTypes.Equals(nature))
			{
				result = assignedBrandsList.FindAll(x => x.Nature == nature).ToList();
			}
			retailersBrandList.DataSource = result;
			retailersBrandList.DataTextField = "DisplayBrandName";
			retailersBrandList.DataValueField = "BrandId";
			retailersBrandList.DataBind();
			retailersBrands.Visible = assignedBrandsList.Count > 0;
		}
		protected void btnProgramToVendor_OnClick(object sender, EventArgs e)
		{
			CleanProgramToVendorDialog();
			BindAndSelectVendorProgramDialog();
			AssignProgramToVendorPopupExtender.Show();
		}
		protected void CleanProgramToVendorDialog()
		{
			lblAssignProgramToVendorError.Text = "";
			lblAssignProgramToVendorError.Visible = false;
			ddlAPVendor.Items.Clear();
			ddlAPProgram.Items.Clear();
			ClearAssignedProgramList();
			ClearGroupList();
		}
		private void ClearAssignedProgramList()
		{
			vendorProgramsList.Items.Clear();
			assignedPrograms.Visible = false;
		}

		private void ClearGroupList()
		{
			cblAPVendorGroupList.Items.Clear();
			lblCompanyGroup.InnerText = "";
			vendorGroupTr.Visible = false;
		}

		private void BindAssignedProgramsList(string customerCode)
		{
			var assignedProgramsList = SyntheticScreeningUtils.GetVendorPrograms(Convert.ToInt32(customerCode), this);
			vendorProgramsList.DataSource = assignedProgramsList;
			vendorProgramsList.DataTextField = "DisplayProgramName";
			vendorProgramsList.DataValueField = "ID";
			vendorProgramsList.DataBind();
			assignedPrograms.Visible = assignedProgramsList.Count > 0;
		}

		protected void BindAndSelectVendorProgramDialog()
		{
			//Bind Vendor list
			var vendors = GetViewState("RetailerVendors") as DataTable;
			ddlAPVendor.DataSource = vendors;
			ddlAPVendor.DataTextField = "CustomerName";
			ddlAPVendor.DataValueField = "CustomerCode";
			ddlAPVendor.DataBind();
			ddlAPVendor.Items.Insert(0, new ListItem(String.Empty, String.Empty));
			//Select chosen Vendor or empty one
			var selectedItem = ddlRetailerVendors.SelectedItem;
			if (selectedItem != null)
			{
				var customerName = selectedItem.Text;
				var vendor = ddlAPVendor.Items.FindByText(customerName);
				if (vendor != null)
				{
					vendor.Selected = true;
					var customerCode = vendor.Value;
					if(!string.IsNullOrEmpty(customerCode) && customerCode != "0")
					{
						BindAssignedProgramsList(customerCode);
						var customerId = GetCustomerID(customerCode);
						if (!string.IsNullOrEmpty(customerId))
						{
							LoadCustomerCompanyGroup(customerId);
						}
					}
				}
			}
			else
			{
				ddlAPVendor.SelectedIndex = 0;
			}

			//Bind program list
			List<BrandProgramModel> allPrograms = GetViewState("RetailerPrograms") as List<BrandProgramModel>;
			ddlAPProgram.DataSource = allPrograms;
			ddlAPProgram.DataTextField = "DisplayProgramName";
			ddlAPProgram.DataValueField = "ID";
			ddlAPProgram.DataBind();
			ddlAPProgram.Items.Insert(0, new ListItem(String.Empty, String.Empty));
			var selectedProgram = ddlProgram.SelectedItem;
			if (selectedProgram != null)
			{
				var program = ddlAPProgram.Items.FindByValue(selectedProgram.Value);
				if (program != null)
				{
					program.Selected = true;
				}
			}
			else 
			{
				ddlAPProgram.SelectedIndex = 0;
			}
		}

		protected void OnAssignProgramVendorSelectedChanged(object sender, EventArgs e)
		{
			var customerCode = ddlAPVendor.SelectedValue;
			ClearAssignedProgramList();
			ClearGroupList();
			if (!string.IsNullOrEmpty(customerCode) && customerCode != "0")
			{
				BindAssignedProgramsList(customerCode);
				var customerId = GetCustomerID(customerCode);
				if (!string.IsNullOrEmpty(customerId))
				{
					LoadCustomerCompanyGroup(customerId);
				}
			}
			AssignProgramToVendorPopupExtender.Show();
		}

		private void LoadCustomerCompanyGroup(string CustomerId)
		{
			CustomerModel objCustomerModel = new CustomerModel();
			objCustomerModel.CustomerId = CustomerId;
			var customers = QueryUtils.GetCustomerCompanyGroup(objCustomerModel, this);
			cblAPVendorGroupList.DataValueField = "CustomerID";
			cblAPVendorGroupList.DataTextField = "CustomerName";
			cblAPVendorGroupList.DataSource = customers;
			cblAPVendorGroupList.DataBind();
			
			var companyGroupName = (customers!=null && customers.Rows.Count>0) ? customers.Rows[0]["CompanyGroupName"].ToString() : "";
			if (customers.Rows.Count >= 1)
			{
				cblAPVendorGroupList.Visible = true;
				vendorGroupTr.Visible = true;
				lblCompanyGroup.InnerText = companyGroupName;
			}
			else
			{
				cblAPVendorGroupList.Visible = false;
				vendorGroupTr.Visible = false;
			}
		}
		private string GetCustomerID(string customerCode)
		{
			var customerId = "";
			DataTable dtCustomerId = SyntheticScreeningUtils.GetCustomerIdByCode(customerCode, this);
			if (dtCustomerId != null || dtCustomerId.Rows.Count > 0)
			{
				customerId = dtCustomerId.Rows[0].ItemArray[0].ToString();
			}

			return customerId;
		}
		private string GetCustomerCode(string customerId)
		{
			var customerCode = "";
			DataTable dtCustomerCode = QueryCpUtils.GetCustomerCodeByID(customerId, this);
			if (dtCustomerCode != null || dtCustomerCode.Rows.Count > 0)
			{
				customerCode = dtCustomerCode.Rows[0].ItemArray[0].ToString();
			}

			return customerCode;
		}

		protected void OnAssignProgramDialogOkClick(object sender, EventArgs e)
		{
			var error = "";
			var customerCode = ddlAPVendor.SelectedValue;
			//Check if vendor selected
			if (string.IsNullOrEmpty(customerCode) || customerCode == "0")
			{
				error = "Please select Vendor.";
				ShowPopapError(error, lblAssignProgramToVendorError, AssignProgramToVendorPopupExtender);
				return;
			}

			var programId = ddlAPProgram.SelectedValue;
			//Check if retailer selected
			if (string.IsNullOrEmpty(programId) || programId == "0")
			{
				error = "Please select Program.";
				ShowPopapError(error, lblAssignProgramToVendorError, AssignProgramToVendorPopupExtender);
				return;
			}
			var vendorName = ddlAPVendor.SelectedItem.Text;
			var programName = ddlAPProgram.SelectedItem.Text;
			var selectedGroupList = new List <CustomerModel>();
			//first in list - selected vendor from dropdown
			var selectedCompany = new CustomerModel();
			selectedCompany.CustomerCode = customerCode;
			selectedCompany.CustomerName = vendorName;
			selectedGroupList.Add(selectedCompany);
			//find selected from group list
			foreach (ListItem chkGroupCompany in cblAPVendorGroupList.Items)
			{
				if (chkGroupCompany.Selected == true)
				{
					selectedCompany = new CustomerModel();
					var customerId = chkGroupCompany.Value.ToString();
					selectedCompany.CustomerCode = GetCustomerCode(customerId);
					selectedCompany.CustomerName = chkGroupCompany.Text.ToString();
					//we checked has customer code and if value from list not equal to selected vendor from dropdown
					if (selectedCompany.CustomerCode != "" && selectedCompany.CustomerCode != customerCode)
					{
						selectedGroupList.Add(selectedCompany);
					}

				}
			}
			var succeedNames = "";
			var failedNames = "";
			foreach (CustomerModel customer in selectedGroupList)
			{
				var message = SyntheticScreeningUtils.AddProgramToVendor(Convert.ToInt32(customer.CustomerCode), Convert.ToInt32(programId), this);
				if (message.Contains("failed"))
				{
					failedNames = "".Equals(failedNames) ? customer.CustomerName : failedNames + "; " + customer.CustomerName;
				}
				else 
				{
					succeedNames = "".Equals(succeedNames) ? customer.CustomerName : succeedNames + "; " + customer.CustomerName;
				}
			}
			var successMessage = "".Equals(succeedNames) ? "" : string.Format("Program " + programName + " successfully assigned to Vendor(s) {0}!", succeedNames);
			var errorMessage = "".Equals(failedNames) ? "" : string.Format("Assigning Program " + programName + " to Vendor(s) {0} failed!", failedNames);

			lblActionError.Text = errorMessage;
			lblActionError.Visible = !"".Equals(errorMessage);			

			if (!"".Equals(successMessage))
			{
				lblActionSuccess.Text = successMessage;
				lblActionSuccess.Visible = true;
				OnProgramSelectedChanged(null, null);
			}

		}
		protected void OnDeleteProgramDialogOkClick(object sender, EventArgs e)
		{
			var error = "";
			var customerCode = ddlAPVendor.SelectedValue;
			//Check if vendor selected
			if (string.IsNullOrEmpty(customerCode) || customerCode == "0")
			{
				error = "Please select Vendor.";
				ShowPopapError(error, lblAssignProgramToVendorError, AssignProgramToVendorPopupExtender);
				return;
			}
			var selectedProgram = vendorProgramsList.SelectedItem;
			//Check if program to delete selected
			if (selectedProgram == null)
			{
				error = "Please select Program from Assigned Programs list!";
				ShowPopapError(error, lblAssignProgramToVendorError, AssignProgramToVendorPopupExtender);
				return;
			}
			var vendorName = ddlAPVendor.SelectedItem.Text;
			var programName = ddlAPProgram.SelectedItem.Text;
			var selectedGroupList = new List<CustomerModel>();
			//first in list - selected vendor from dropdown
			var selectedCompany = new CustomerModel();
			selectedCompany.CustomerCode = customerCode;
			selectedCompany.CustomerName = vendorName;
			selectedGroupList.Add(selectedCompany);
			//find selected from group list
			foreach (ListItem chkGroupCompany in cblAPVendorGroupList.Items)
			{
				if (chkGroupCompany.Selected == true)
				{
					selectedCompany = new CustomerModel();
					var customerId = chkGroupCompany.Value.ToString();
					selectedCompany.CustomerCode = GetCustomerCode(customerId);
					selectedCompany.CustomerName = chkGroupCompany.Text.ToString();
					//we checked has customer code and if value from list not equal to selected vendor from dropdown
					if (selectedCompany.CustomerCode != "" && selectedCompany.CustomerCode != customerCode)
					{
						selectedGroupList.Add(selectedCompany);
					}

				}
			}
			var succeedNames = "";
			var failedNames = "";
			foreach (CustomerModel customer in selectedGroupList)
			{
				var message = SyntheticScreeningUtils.DeleteVendorProgram(Convert.ToInt32(customer.CustomerCode), Convert.ToInt32(selectedProgram.Value), this);
				if (message.Contains("failed"))
				{
					failedNames = "".Equals(failedNames) ? customer.CustomerName : failedNames + "; " + customer.CustomerName;
				}
				else
				{
					succeedNames = "".Equals(succeedNames) ? customer.CustomerName : succeedNames + "; " + customer.CustomerName;
				}
			}

			var successMessage = "".Equals(succeedNames) ? "" : string.Format("Program " + programName + " successfully deleted from Vendor(s) {0}!", succeedNames);
			var errorMessage = "".Equals(failedNames) ? "" : string.Format("Deleting Program " + programName + " from Vendor(s) {0} failed!", failedNames);


			lblActionError.Text = errorMessage;
			lblActionError.Visible = !"".Equals(errorMessage);

			if (!"".Equals(successMessage))
			{
				lblActionSuccess.Text = successMessage;
				lblActionSuccess.Visible = true;
				OnProgramSelectedChanged(null, null);
			}
		}

		private void ShowPopapError(string error, Label targetLbl, AjaxControlToolkit.ModalPopupExtender targetPopap)
		{
			targetLbl.Text = error;
			targetLbl.Visible = true;
			targetPopap.Show();
		}
		protected void sort_table(object sender, GridViewSortEventArgs e)
		{
			List<SyntheticShortStatsModel> lstScreening = new List<SyntheticShortStatsModel>();
			var vendors = GetViewState("RetailerVendors") as DataTable;				
			string direction = null;
			if (System.Web.HttpContext.Current.Session["sortExpression"] != null)
			{
				string[] sortData = Session["sortExpression"].ToString().Trim().Split(' ');
				direction = sortData[1];
				if (e.SortExpression == sortData[0])
				{
					if (direction == "ASC")
					{
						vendors.DefaultView.Sort = e.SortExpression + " " + "DESC";
						Session["sortExpression"] = e.SortExpression + " " + "DESC";
					}
					else
					{
						vendors.DefaultView.Sort = e.SortExpression + " " + "ASC";
						Session["sortExpression"] = e.SortExpression + " " + "ASC";
					}
				}
				else
				{
					vendors.DefaultView.Sort = e.SortExpression + " " + "ASC";
					Session["sortExpression"] = e.SortExpression + " " + "ASC";
				}
			}
			else
			{
				vendors.DefaultView.Sort = e.SortExpression + " " + "ASC";
				Session["sortExpression"] = e.SortExpression + " " + "ASC";
			}

			SetViewState(vendors, "RetailerVendors");
			gvRetailersCustomers.DataSource = vendors.DefaultView;
			gvRetailersCustomers.DataBind();

		}//sort_table

		
		#endregion
	}
}