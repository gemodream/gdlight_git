﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" Title="Screening Manager View" AutoEventWireup="true" CodeBehind="ScreeningManager.aspx.cs" Inherits="Corpt.ScreeningManager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

	<script src="Style/jquery-1.12.4.js" type="text/javascript"></script>
	<script src="Style/jquery-ui.js" type="text/javascript"></script>
	<script src="Style/bootstrap2.0/js/bootstrap.js" type="text/javascript"></script>

	<link rel="icon" type="image/png" href="logo@2x.png" />
	<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
	<link href="Style/bootstrap2.0/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="Style/jquery-ui.css" rel="stylesheet" type="text/css" />
	<style>
		.lbl {
			width: 400px;
			display: inline-block;
			font-size: 12px;
		}

		select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
			display: inline-block;
			padding: 4px 6px;
			margin-bottom: 3px;
			font-size: 12px;
			line-height: 13px;
			color: black; /* #555555;*/
			vertical-align: middle;
			font-family: Tahoma,Arial,sans-serif;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
		}

		input, button, select, textarea {
			font-family: Tahoma,Arial,sans-serif;
			font-size: 14px;
		}

			input[type="checkbox"], label {
				margin-top: 0;
				margin-bottom: 0;
				line-height: normal;
			}

		body {
			font-family: Tahoma,Arial,sans-serif;
		/*font-size: 75%;*/
		}

		.headingPanel {
			padding-bottom: 2px;
			color: #5377A9;
			font-family: Arial, Sans-Serif;
			font-weight: bold;
			font-size: 1.0em;
		}

		.text_highlitedyellow {
			background-color: #FFFF00;
		}

		.text_nohighlitedyellow {
			background-color: white;
		}
	</style>
	<style type="text/css">
		body {
			font-family: Tahoma,Arial,sans-serif;
			font-size: 14px;
			font-weight: normal;
			line-height: 20px;
		}

		.split {
			margin-top: 0px;
		}

		.bagtext {
			margin-bottom: 0px !important;
		}

		.bagLable {
			text-align: center;
			margin-left: 6px;
		}

		.ajax__tab_body {
			font-family: unset !important;
			padding: 0px !important;
		}

		td > Label {
			display: unset !important;
			margin-left: 5px !important;
		}

		input[type="text"] {
			margin-bottom: 0px !important;
		}

		.auto-style1 {
			height: 23px;
		}

		.txtCss {
			width: 80px;
		}

		select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
			display: inline-block;
			padding: 4px 6px;
			margin-bottom: 3px;
			font-size: 12px;
			line-height: 13px;
			color: #555555;
			vertical-align: middle;
			font-family: Tahoma,Arial,sans-serif;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
		}

		.radioButtonList input[type="radio"] {
			width: auto;
			float: left;
			width: 20px;
			height: 20px;
			/*margin-top: 13px;*/
			margin-left: 7px;
			margin-left: 10px;
			position: absolute;
			z-index: 1;
			font-size: 15px;
		}

		.radioButtonList label {
			color: white;
			font-weight: bold;
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #49afcd;
			*background-color: #2f96b4;
			background-repeat: repeat-x;
			background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
			border-left-color: #2f96b4;
			border-right-color: #2f96b4;
			border-top-color: #2f96b4;
			border-bottom-color: #1f6377;
			height: 30px;
			padding-left: 30px;
			width: max-content;
			line-height: 35px;
			/*margin: 5px 5px 5px 5px;*/
			padding-top: 10px;
			padding-bottom: 10px;
		}
	</style>
	<script type="text/javascript">
        function textboxMultilineMaxNumber(txt, maxLen) {
					 try {
						 if (txt.value.length > (maxLen - 1))
							 return false;
					 } catch (e) {
					 }
				 }
	</script >
	<div class="demoarea" style="text-align: left;">
		<ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True"></ajaxToolkit:ToolkitScriptManager>
		<asp:UpdatePanel ID="UpdatePanel1" runat="server">
			<ContentTemplate>
				
			 <div style="font-size: smaller;height:25px;">
								<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
									<ProgressTemplate>
										<img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
										<b>Please, wait....</b>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
				<table>
					<tr>
						<td>
							<div class="demoheading">Manager View
								</div>
							</div>
							
						</td>
						<td>
							
						</td>
				<td><div class="demoheading"><asp:Label runat="server" ID="lblInvalidLabel" ForeColor="Red" Style="padding-left: 10px" Width="400px"></asp:Label></div></td>
					</tr>
				</table>

				<table style="width: auto;" border="0">

					<tr>
						<td style="vertical-align: top; padding-top: 15px;" rowspan="4">

							<table style="width: auto;">
								<tr>
									<td colspan="3">
										<fieldset style="border: 1px groove #ddd !important; padding: 5px 5px 5px 5px; border-radius: 8px; width: 200px;">
											<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
												class="label">View By</legend>
											<table class="radioButtonList" style="width: 220px;">
												<tr>
													<td>
													<asp:RadioButton ID="rblViewByOrder" runat="server" Text="Order#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" GroupName="ViewBy" 
												AutoPostBack="true" OnCheckedChanged="rblViewBy_SelectedIndexChanged" Checked="true" />
														<asp:RadioButton ID="rblViewByDate" runat="server" Text="Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" GroupName="ViewBy" 
												AutoPostBack="true" OnCheckedChanged="rblViewBy_SelectedIndexChanged"  />

													</td>
												</tr>
												<tr>
												<td style="padding-top: 5px;">
													<asp:RadioButton ID="rblViewByMemo" runat="server" Text="Memo#&nbsp;&nbsp;&nbsp;&nbsp;" GroupName="ViewBy" 
													AutoPostBack="true" OnCheckedChanged="rblViewBy_SelectedIndexChanged" />

														<asp:RadioButton ID="rblViewByCustomer" runat="server" Text="Customer#" GroupName="ViewBy"
															AutoPostBack="true" OnCheckedChanged="rblViewBy_SelectedIndexChanged" />
														</td>
											</tr>
											<tr>
												<td style="padding-top: 5px;">
													<asp:RadioButton ID="rblViewByPosition" runat="server" Text="Position&nbsp;&nbsp;&nbsp;&nbsp;" GroupName="ViewBy"
															AutoPostBack="true" OnCheckedChanged="rblViewBy_SelectedIndexChanged" />
													</td>
											</tr>
											</table>

									</fieldset>
									
								</td>
								
							</tr>
							<tr><td colspan="2" style="padding-top: 15px;"></td></tr>

								<tr>
								<td style="padding-top: 5px;">
									
										<asp:TextBox ID="txtSearch" runat="server" Width="135"></asp:TextBox>
										<asp:DropDownList ID="drpPosition" runat="server" Visible="false" Width="148px">
											

										</asp:DropDownList>
										<asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtSearch"
											Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Detail is required."
											ValidationGroup="BatchGroup" />
										<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
											HighlightCssClass="validatorCalloutHighlight" />
									</td>
									<td>
				 <asp:Button ID="imgOrderSearch" runat="server" Text="Load" class="btn btn-info btn-large" OnClick="imgOrderSearch_Click" />

										<%--<asp:ImageButton ID="imgOrderSearch" runat="server" ToolTip="Filtering the list of Order"
											ImageUrl="~/Images/ajaxImages/search.png" OnClick="imgOrderSearch_Click" /> --%>
									</td>
								</tr>
								<tr>
									<td></td>
								</tr>
								<tr >
									<td rowspan="8" style="vertical-align: top; padding-right: 0px; padding-top: 15px;" colspan="2">
										<asp:ListBox ID="lstOrderIDs" runat="server" AutoPostBack="True" Height="200px"
								OnSelectedIndexChanged="lstOrderIDs_SelectedIndexChanged" 
							Style="margin-bottom: 0px !important;" Width="230px"></asp:ListBox>
									</td>

								</tr>

							</table>

						</td>
						<td style="vertical-align: top; padding-top: 15px; padding-left: 15px; vertical-align: top;">
							<fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; width: 200px;">
								<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
									class="label">Order Detail</legend>
								<table style="width: 800px;">
									<tr>
										<td>
											<asp:Label ID="Label1" runat="server" Text="Order Code"></asp:Label>
										</td>

										<td>
											<asp:Label ID="Label2" runat="server" Text="Memo"></asp:Label>
										</td>
										<td>
											<asp:Label ID="Label11" runat="server" Text="PO No" Width="70px"></asp:Label>
										</td>
										<td>
											<asp:Label ID="Label10" runat="server" Text="SKU"></asp:Label>

										</td>
										<td>
											<asp:Label ID="Label6" runat="server" Text="Styles" Width="70px"></asp:Label>

										</td>
										<td>
											<asp:Label ID="Label7" runat="server" Text="Customer"></asp:Label>

										</td>
										<td>
											<asp:Label ID="Label8" runat="server" Text="Retailer"></asp:Label>
										</td>


										<td>
											<asp:Label ID="Label3" runat="server" Text="Total Qty" Width="70px"></asp:Label>
										</td>
									</tr>

									<tr>
										<td>
											<asp:TextBox ID="txtOrderCode" runat="server" CssClass="txtCss" Enabled="false"></asp:TextBox>
										</td>
										<td>
											<asp:TextBox ID="txtMemo" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>
										</td>
										<td>
											<asp:TextBox ID="txtPONumber" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>

										</td>
										<td>
											<asp:TextBox ID="txtSKU" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>
										</td>
										<td>
											<asp:TextBox ID="txtStyle" runat="server" CssClass="txtCss" Enabled="false"></asp:TextBox>
										</td>
										<td>
											<asp:TextBox ID="txtCustomer" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>
										</td>
										<td>
											<asp:TextBox ID="txtRetailer" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>
										</td>


										<td>
											<asp:TextBox ID="txtTotalQty" runat="server" Enabled="false" CssClass="txtCss"></asp:TextBox>
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding-top: 15px; padding-left: 15px; vertical-align: top;">
							<fieldset style="border: 1px groove #ddd !important; padding: 10px; border-radius: 8px; width: max-content;min-width:550px;">
								<legend class="label" style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;">Batch Current Status</legend>
								<table style="width: 800px;">
									<tr>
										<td>
											<asp:Panel ID="pnlBatchStatus" runat="server" Height="180px" ScrollBars="Vertical">
												<asp:GridView ID="gdvBatchStatus" runat="server" AutoGenerateColumns="False" CellPadding="4"
													Font-Size="12px" ForeColor="#333333">
													<AlternatingRowStyle BackColor="White" />
													<Columns>
														<asp:BoundField DataField="OrderCode" HeaderText="Order Code" />
														<asp:BoundField DataField="BatchCode" HeaderText="Batch Code" />
														<asp:BoundField DataField="Quantity" HeaderText="Quantity" />
														<asp:BoundField DataField="StatusName" HeaderText="Status Name" />
														<asp:BoundField DataField="ScreenerName" HeaderText="Screener Name" />
														<asp:BoundField DataField="ScreenerPassItems" HeaderText="Screener Pass Items" />
														<asp:BoundField DataField="ScreenerFailItems" HeaderText="Screener Fail Items" />
														<asp:BoundField DataField="TesterName" HeaderText="Tester Name" />
														<asp:BoundField DataField="TesterSyntheticStones" HeaderText="Tester Synthetic Stones" />
														<asp:BoundField DataField="TesterSuspectStones" HeaderText="Tester Suspect Stones" />
														<asp:BoundField DataField="TesterPassItems" HeaderText="Tester Pass Items " />
													</Columns>
													<EditRowStyle BackColor="#2461BF" />
													<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
													<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
													<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
													<RowStyle BackColor="#EFF3FB" />
													<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
													<SortedAscendingCellStyle BackColor="#F5F7FB" />
													<SortedAscendingHeaderStyle BackColor="#6D95E1" />
													<SortedDescendingCellStyle BackColor="#E9EBEF" />
													<SortedDescendingHeaderStyle BackColor="#4870BE" />
												</asp:GridView>
												<div id="dvBatchStatus" runat="server" style="border: 1px solid black; padding-left: 10%; width: 700px; padding: 10px; text-align: center; v">
													No Order History Found
												</div>
											</asp:Panel>
										</td>
									</tr>
									<tr>
										<td style="text-align: right;">
											<asp:Label ID="lblMsg" runat="server" ForeColor="Blue" Style="padding-right: 10px;" Text=""></asp:Label>
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td style="padding-left: 15px; padding-top: 10px;">
							<fieldset style="border: 1px groove #ddd !important; padding: 10px; border-radius: 8px; width: max-content;min-width:550px;">
								<legend class="label" style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;">Order Status History</legend>
								<asp:Panel ID="pnlSyntheticOrderHistory" runat="server" Height="350px" ScrollBars="Vertical">
									<asp:GridView ID="gdvSyntheticOrderHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Size="12px"
										ForeColor="#333333" Width="100%">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
											<asp:BoundField DataField="OrderCode" HeaderText="OrderCode" />
											<asp:BoundField DataField="BatchCode" HeaderText="BatchCode" />
											<asp:BoundField DataField="ItemQty" HeaderText="ItemQty" />
											<asp:BoundField DataField="StartDate" HeaderText="StartDate" />
											<asp:BoundField DataField="AllocatedTo" HeaderText="Allocated To" />
											<asp:BoundField DataField="StatusName" HeaderText="Status" />
										</Columns>
										<EditRowStyle BackColor="#2461BF" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
									<div id="dvOrderHistory" runat="server" style="border: 1px solid black; padding-left: 10%; width: 700px; padding: 10px; text-align: center;">
										No Order History Found
									</div>
								</asp:Panel>
							</fieldset>
						</td>
					</tr>
					<tr>

						<td style="vertical-align: top; padding-top: 10px; padding-left: 15px;visibility:hidden;">
							<fieldset style="border: 1px groove #ddd !important; padding: 10px; border-radius: 8px; width: max-content;">
								<legend class="label" style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;">User Current Status</legend>
								<asp:Panel ID="Panel1" runat="server" Height="150px" ScrollBars="Vertical">
									<asp:GridView ID="gdvUserStatus" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333"
										Font-Size="12px" Width="100%">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
											<asp:BoundField DataField="AssignedTo" HeaderText="User" />
											<asp:BoundField DataField="OrderCode" HeaderText="Order Code" />
											<asp:BoundField DataField="BatchCode" HeaderText="Batch Code" />
											<asp:BoundField DataField="ItemQty" HeaderText="Allocated Qty" />
											<asp:BoundField DataField="StatusName" HeaderText="Status" />
										</Columns>
										<EditRowStyle BackColor="#2461BF" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
									<div id="dvUserEmptyRecord" runat="server" style="border: 1px solid black; padding-left: 10%; width: 700px; padding: 10px; text-align: center;">
										No User Status
									</div>
								</asp:Panel>
							</fieldset>
						</td>
					</tr>

				</table>


				<%-- Information Dialog --%>
				<asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray; margin-top: 25%;">
					<asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
						<div>
							<asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
							<b>Information</b>
						</div>
					</asp:Panel>
					<div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
						id="MessageDiv" runat="server">
					</div>
					<div style="padding-top: 10px">
						<p style="text-align: center; font-family: sans-serif;">
							<asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
						</p>
					</div>
				</asp:Panel>
				<asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
				<ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
					PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
				</ajaxToolkit:ModalPopupExtender>

			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
