﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="VVAdmin1.aspx.cs" Inherits="Corpt.VvAdmin1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Verify VV Registration</div>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
                    Search by Report Number:
                    <asp:TextBox ID="ReportNumberFld" runat="server" MaxLength="11"></asp:TextBox>
                    <asp:ImageButton ID="SearchButton" runat="server" ToolTip="Search users by Report Number"
                        ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnSearchClick" />
                    <asp:Button runat="server" ID="NewUserButton" Text="New User" Style="margin-left: 20px;
                        margin-top: -10px" CssClass="btn btn-info" OnClick="OnNewUserClick" />
                </asp:Panel>
                <asp:DataGrid runat="server" ID="UsersGrid" AutoGenerateColumns="False" CellPadding="5"
                    CellSpacing="5" OnItemCommand="OnEditUserCommand" DataKeyField="Id">
                    <Columns>
                        <asp:BoundColumn DataField="Id" HeaderText="User Id"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ReportNumber" HeaderText="Report #"></asp:BoundColumn>
                        <asp:BoundColumn DataField="VirtualVaultNumber" HeaderText="VV #"></asp:BoundColumn>
                        <asp:BoundColumn DataField="DisplayName" HeaderText="User name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Email" HeaderText="User email"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Password" HeaderText="Password"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"></asp:BoundColumn>
                        <asp:ButtonColumn Text="PW Reset" ButtonType="LinkButton" HeaderText="" />
                    </Columns>
                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                </asp:DataGrid>
                <%--  Change Password Dialog--%>
                <asp:Panel runat="server" ID="ChangePwdPanel" CssClass="modalPopup"  Style="font-family: sans-serif;display: none">
                    <asp:Panel ID="ChangePwdDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div>
                            <b>Change password</b>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" Style="padding-top: 10px" DefaultButton="ChangePwdHiddenBtn">
                        <asp:Button runat="server" ID="ChangePwdHiddenBtn" Style="display: none" Enabled="False"></asp:Button>
                        <asp:HiddenField runat="server" ID="UserIdFld"/>
                        <table>
                            <tr>
                                <td>Old Password:</td>
                                <td><asp:TextBox runat="server" ID="OldPasswordFld" ReadOnly="True" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>New Password:</td>
                                <td><asp:TextBox runat="server" ID="NewPasswordFld" MaxLength="12"></asp:TextBox></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="ChangePwdOkBtn" runat="server" OnClick="OnChangePwdOkClick" Text="Save"/>
                            <asp:Button ID="ChangePwdCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenBtnPwd" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBtnPwd" PopupControlID="ChangePwdPanel" ID="ChangePwdPopupExtender"
                PopupDragHandleControlID="ChangePwdDragHandle" OkControlID="ChangePwdCancBtn" >
                </ajaxToolkit:ModalPopupExtender>
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none;border: solid 1px Silver; color: black;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>
                

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
