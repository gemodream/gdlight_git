﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ListItem = System.Web.UI.WebControls.ListItem;
//using Spire.Pdf;
//using Spire.Pdf.Graphics;
//using Spire.Pdf.Fields;
//using Spire.Pdf.Widget;
//using Spire.Pdf.Barcode;
using System.Xml;
using System.Threading;
using System.Net.Mime;
using System.Text.RegularExpressions;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;

namespace Corpt
{
    public partial class ScreeningFront : CommonPage
    {
        private const string CmdDelAccept = "CmdDelAccept";
        private const string CmdDelBulkGiveOut = "CmdDelBulkGiveOut";
        #region Page Load

        private DataTable dtotGiveOutVal;

        public string imgTransperant = "";//"data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Front";
            Panel2.Enabled = false;
            TabPanel2.Enabled = false;
            //btnSubmitTakeInAll.Visible = false;
            lstRequests.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(lstRequests, CmdDelAccept));
            BulkGiveOutList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(BulkGiveOutList, CmdDelBulkGiveOut));
            //lnkView.Visible = true;
            if (!IsPostBack)
            {
                iframePDFViewer.Visible = false;
                //Customers List
                LoadCustomers();

                //LoadSyntheticJewelryType
                LoadSyntheticJewelryType();
                LoadSubmissionShippingCourier();
                LoadPersonServices();

                //--Load Weight Type
                DataView dv = FrontUtils.GetMeasureUnits(this).DefaultView;
                dv.RowFilter = "MeasureUnitCode = 2";
                ddlSubmissionWeightType.DataSource = dv;
                ddlSubmissionWeightType.DataBind();

                //--Load Service Time
                var ServiceTime = SyntheticScreeningUtils.GetSyntheticServiceTime(this);

                rdlServiceTimeTakeIn.DataSource = ServiceTime;
                rdlServiceTimeTakeIn.DataBind();
                rdlServiceTimeTakeIn.SelectedIndex = 1;

                //-- Load Carries
                var dtCarriers = SyntheticScreeningUtils.GetCarriers(this);
                rdlCarriersShipOut.DataSource = dtCarriers;
                rdlCarriersShipOut.DataBind();

                var dtDeliveryMethod = SyntheticScreeningUtils.GetSyntheticDeliveryMethod(this);
                rdlDeliveryMethod.DataSource = dtDeliveryMethod;
                rdlDeliveryMethod.DataBind();
                
                var dtJewelryType = SyntheticScreeningUtils.GetSyntheticJewelryType(this);
                rdlJewelryType.DataSource = dtJewelryType;
                rdlJewelryType.DataBind();

                var country = QueryCustomerUtils.GetCountry(this);
                ddlCustomerCountry.DataSource = country;
                ddlCustomerCountry.DataBind();
                ddlCustomerCountry.Items.Insert(0, "");

                //Business Type
                var BusinessType = QueryCustomerUtils.GetBusinessTypes(this);
                ddlCustomerBusinessType.DataSource = BusinessType;
                ddlCustomerBusinessType.DataBind();

                //--Desable All Controls
                TakeInControlsEnableDisable(false);
                GiveOutControlsEnableDisable(false);

                TakeInDefaultFields();

                txtCustomerLookupReq.Attributes.Add("onKeyUp", "FilterShapes_TakeIn(this)");
                txtCustomerLookupReq.Attributes.Add("onFocus", "FilterShapes_TakeIn(this)");

                txtShippingDestination.Attributes.Add("onKeyUp", "FilterDestination(this)");
                txtShippingDestination.Attributes.Add("onFocus", "FilterDestination(this)");

                txtSpecialInstructions.Attributes.Remove("MaxLength");
                txtSpecialInstructions.Attributes.Add("MaxLength", "2000");
                txtMemoTakeIn.Attributes.Remove("MaxLength");
                txtMemoTakeIn.Attributes.Add("MaxLength", "99");

                Page.Validate(ValidatorGroupCompany);
                Page.Validate(ValidatorGroupPerson);

                rdbServiceTimeTakeIn_IndexChanged(this, e);

				this.Form.DefaultFocus = txtRequest.ClientID;
                //this.Form.DefaultFocus = Panel6.ClientID;
				//this.Form.DefaultButton = btnRequestSearch.UniqueID;

				//txtRequest.Focus();
				//TabContainer1_ActiveTabChanged(this, e);
				//this.txtRequest.Focus();
				//ScriptManager.RegisterStartupScript(this, this.GetType(), "selectAndFocus", "$get('" + txtRequest.ClientID + "').focus();$get('" + txtRequest.ClientID + "').select();", true);
				//ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
				//scriptManager.SetFocus(txtRequest.ClientID);
				//ScriptManager.GetCurrent(this.Page).SetFocus(this.txtRequest);
				//TabContainer1.ActiveTabIndex = 1;
				TabContainer1.ActiveTabIndex = 0;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "document.getElementById('" + txtRequest.ClientID + "').focus();", true);
               // Session["MachineName"] = "gsi-ny-6";
                if (Session["MachineName"] != null)
                    lblCreateRequestMessege.Text = Session["MachineName"].ToString();
                else
                {
                    lblCreateRequestMessege.Text = "No machine name";
                    PopupInfoDialog("No machine name. Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx", true);
                }
                imgStoredSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                imgStoredPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                lnkView.Visible = true;
                lnkView.Enabled = false;
                takeOutLnkView.Visible = false;
                hdnAcceptCustomerID.Value = "";
                hdnAcceptMessenger.Value = "";
                hdnAcceptCarrier.Value = "";
                lblBulkGiveOutTotal.Text = "";
                ddlCriteria.Items.Add("None");
                ddlCriteria.Items.Add("MemoNum");
                ddlCriteria.Items.Add("PONum");
            }
            else
            {
                //var txt = sender as TextBox;
                //string value = txt.Text;
                //var ctrlName = Request.Params[Page.postEventSourceID];
                //var args = Request.Params[Page.postEventArgumentID];

                //if (ctrlName == txtViewOrder.UniqueID && args == "OnKeyPress")
                //{
                //    txtViewOrder_Changed(null, null);
                //}
                    PostBackHandler();
                return;
            }

        }
        private void PostBackHandler()
        {
            var abc = Request["__EVENTARGUMENT"];
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelAccept)
            {
                OnRemoveAcceptListItem();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelBulkGiveOut)
            {
                OnRemoveBulkGiveOutListItem();
            }
        }

        private void OnRemoveAcceptListItem()
        {
            if (lstRequests.SelectedItem == null)
                return;
            for (int i=0; i<= lstRequests.Items.Count-1; i++)
            {
                if (i == lstRequests.SelectedIndex)
                {
                    lstRequests.Items.Remove(lstRequests.Items[i]);
                    break;
                }
            }
            int oldNumber = Convert.ToInt16(lblBulkGiveOutTotal.Text);
            lblBulkGiveOutTotal.Text = (oldNumber - 1).ToString();
        }

        private void OnRemoveBulkGiveOutListItem()
        {
            if (BulkGiveOutList.SelectedItem == null)
                return;
            for (int i = 0; i <= BulkGiveOutList.Items.Count - 1; i++)
            {
                if (i == BulkGiveOutList.SelectedIndex)
                {
                    BulkGiveOutList.Items.Remove(BulkGiveOutList.Items[i]);
                    break;
                }
            }
            int oldNumber = Convert.ToInt16(lblBulkGiveOutTotal.Text);
            lblBulkGiveOutTotal.Text = (oldNumber - 1).ToString();
        }
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);

            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";

            ddlCustomerListReq.DataSource = customers;
            ddlCustomerListReq.DataBind();
            ddlCustomerListReq.Items.Insert(0, li);

            //start code for Submission & Front
            ddlSubmissionCustomerlst.DataSource = customers.OrderBy(item => item.CustomerName);
            ddlSubmissionCustomerlst.DataBind();
            ddlSubmissionCustomerlst.Items.Insert(0, li);

            lstCustomerlookupReq.DataSource = customers;
            lstCustomerlookupReq.DataBind();

            foreach (CustomerModel liDest in customers)
            {
                liDest.CustomerName = liDest.CustomerName.Substring(3, liDest.CustomerName.Length - 3); // Remove Location first 2 Char with space
                liDest.CustomerName = liDest.CustomerName.Substring(0, liDest.CustomerName.Length - 7); // Remove customer code last 5 Char with space
            }
            
            ddlDestination.DataSource = customers.OrderBy(item => item.CustomerName);
            ddlDestination.DataBind();

            ddlDestination.Items.Insert(0, li);

            lstDestination.DataSource = customers.OrderBy(item => item.CustomerName);
            lstDestination.DataBind();

            lstDestination.Items.Insert(0, li);
        }

        #endregion

        #region TakeIn (Tab-1)

        protected void txtRequest_TextChanged(object sender, EventArgs e)
        {
            //Session["TakeOutShipping"] = "true";
            // this.Form.DefaultButton = btnRequestSearch.UniqueID;
        }

        protected void txtViewOrder_Changed(object sender, EventArgs e)
        {
            btnViewOrder_Click(null, null);
        }
        protected void btnRequestSearch_Click(object sender, EventArgs e)
        {
            if (Session["MachineName"] == null)
            {
                PopupInfoDialog("No machine name. Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx", true);
                return;
            }
            //if (Session["Shipping"] != null && Session["Shipping"].ToString() == "true")
            //{
            //    Session["Shipping"] = null;
            //    return;
            //}
            lnkView.Visible = true;
            takeOutLnkView.Visible = false;

            //if (txtRequest.Text[0] == 'B')
            //{
            //    LoadRequestsList();
            //    btnSubmitTakeInAll.Enabled = true;
            //    btnPrintLabelTakeIn.Visible = true;
            //    btnPrintLabelTakeIn.Enabled = true;
            //}
            //else
            //{
            //    hdnRequestID.Value = txtRequest.Text;
            //    btnSubmitTakeInAll.Enabled = false;
            //    btnPrintLabelTakeIn.Visible = true;
            //    btnPrintLabelTakeIn.Enabled = true;
            //    LoadRequestDetails();
            //}

            hdnRequestID.Value = txtRequest.Text;
            //btnSubmitTakeInAll.Enabled = false;
            btnPrintLabelTakeIn.Visible = true;
            btnPrintLabelTakeIn.Enabled = true;
            btnSendEmail.Visible = true;
            btnSendEmail.Enabled = true;
            //alex
            //btnSendEmail.Visible = false;
            //btnSendEmail.Enabled = false;
            //alex
            LoadRequestDetails();
            if (saveShipInfoBox.Checked && savedTrackingNumberTxt.Text != "")
            {
                txtScanPackageBarcodeShipReceiving.Text = savedTrackingNumberTxt.Text;
                for (int i= 0; i <= ddlShippingCourier.Items.Count - 1; i++)
                {
                    if (ddlShippingCourier.Items[i].Text == savedCarrierTxt.Text)
                    {
                        ddlShippingCourier.SelectedIndex = i;
                        break;
                    }
                }
                btnShippingOverwrite.Visible = true;
                MsgDetails.Visible = false;
                ShippingDetails.Visible = true;
            }
            else
            {
                btnShippingOverwrite.Visible = false;
                MsgDetails.Visible = true;
                ShippingDetails.Visible = false;
            }
            if (saveShipInfoBox.Checked && hdnCarriersShipReceiving.Value == "" && hdnTrackingInfo.Value != "")
            {
                txtScanPackageBarcodeShipReceiving.Text = hdnTrackingInfo.Value;
                ddlShippingCourier.SelectedIndex = Convert.ToInt32(hdnCourierInfo.Value);
                MsgDetails.Visible = false;
                ShippingDetails.Visible = true;

            }
            if (saveShipInfoBox.Checked)
                lstRequests.Items.Add(txtRequest.Text);
        }

        protected void OnRequestChanged(object sender, EventArgs e)
        {
            //txtRequest.Text = lstRequests.SelectedItem.Text;
            //for (int i = 0; i <= lstRequests.Items.Count - 1; i++)
            //{
            //    if (lstRequests.Items[i].Text == lstRequests.SelectedItem.Text)
            //    {
            //        lstRequests.Items.Remove(lstRequests.Items[i]);
            //        break;
            //    }
            //}
        }
        protected void txtScanPackageBarCode_Changed(object sender, EventArgs e)
        {
            //Session["Shipping"] = "true";
        }

        private void LoadRequestsList()
        {
            DataTable dt = SyntheticScreeningUtils.GetRequestsList(txtRequest.Text, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lstRequests.Visible = false;
                PopupInfoDialog("Request Group not found", false);
                return;
            }
            if (dt.Rows[0]["GSIOrder"].ToString().Trim().Length > 0)
            {
                lstRequests.Visible = false;
                btnSubmitTakeIn.Enabled = false;
                btnAcceptRequest.Enabled = false;
                PopupInfoDialog("Orders already created for this request group.", false);
                return;
            }
            lstRequests.DataSource = dt;
            lstRequests.DataBind();
            lstRequests.Visible = true;
            lstRequests.Enabled = true;
            lnkView.Enabled = false;
        }
        private void LoadRequestDetails()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            string requestId = txtRequest.Text;
            if (requestId == null || requestId == "")
                PopupInfoDialog("Please enter Request ID or Request Group", false);
            if (acceptanceBox.Checked)
            {
                lblBulkGiveOutTotal.Visible = true;
                for (int i = 0; i <= lstRequests.Items.Count - 1; i++)
                {
                    if (lstRequests.Items[i].Text == txtRequest.Text)
                    {
                        lblRequestError.Text = "Duplicate request ID " + txtRequest.Text;
                        txtRequest.Text = "";
                        txtRequest.Focus();
                        return;
                    }
                }
                ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    lblRequestError.Text = "No record found for Request ID " + requestId;
                    return;
                }
                dt = ds.Tables[0];
                if (dt.Rows[0]["GSIOrder"].ToString().Trim().Length > 0)
                {
                    lblRequestError.Text = "Order " + dt.Rows[0]["GSIOrder"].ToString().Trim() + "  already created for " + txtRequest.Text;
                    txtRequest.Text = "";
                    txtRequest.Focus();
                    return;
                }
                string prevServiceType = "", newServiceType = "";
                newServiceType = dt.Rows[0]["GDServiceTypeID"].ToString();
                if (Session["PrevServiceType"] != null)
                {
                    prevServiceType = Session["PrevServiceType"].ToString();
                    bool prevIsLab = (prevServiceType == "1") ? true : false;
                    bool newIsScreenng = (newServiceType != "1") ? true : false;
                    if ((prevIsLab && newIsScreenng) || (!prevIsLab && !newIsScreenng))
                    {
                        lblRequestError.Text = "Order " + dt.Rows[0]["GSIOrder"].ToString().Trim() + " has service type " + newServiceType + " and initial service type was " + prevServiceType;
                        return;
                    }
                    else
                        lblRequestError.Text = "";
                }
                else
                {
                    Session["PrevServiceType"] = newServiceType;
                    lblRequestError.Text = "ServiceType=" + newServiceType;
                }
                string customerId = dt.Rows[0]["CustomerID"].ToString();
                string customerName = dt.Rows[0]["CustomerName"].ToString();
                string contactId = dt.Rows[0]["PersonID"].ToString();
                string acceptMessenger = dt.Rows[0]["Messenger"].ToString();
                string acceptCarrier = dt.Rows[0]["CarrierName"].ToString();
                string acceptTrackingNumber = "";
                string batchMemos = dt.Rows[0]["BatchMemo"].ToString();
                if (batchMemos != null && batchMemos != "")
                    txtBatchMemos.Text = batchMemos;
                if (txtMessengerTakeIn.Text != "")
                    acceptMessenger = txtMessengerTakeIn.Text;
                if (txtCarriersShipReceiving.Text != "" && txtScanPackageBarcodeShipReceiving.Text != "")
                {
                    acceptCarrier = txtCarriersShipReceiving.Text;
                    acceptTrackingNumber = txtScanPackageBarcodeShipReceiving.Text;
                }
                if (hdnAcceptCustomerID.Value == "")
                {
                    hdnAcceptCustomerID.Value = customerId;
                    hdnContactId.Value = contactId;
                    hdnAcceptCustomerName.Value = customerName;
                    hdnAcceptMessenger.Value = acceptMessenger;
                    hdnAcceptCarrier.Value = acceptCarrier;
                    hdnAcceptTrackingNumber.Value = acceptTrackingNumber;
                }
                else
                {
                    if (hdnAcceptCustomerID.Value != customerId)
                    {
                        PopupInfoDialog("Customer ID for Request ID " + requestId + "does not match with existing customer", false);
                        return;
                    }
                    
                }
                btnAcceptRequest.Enabled = true;
                lstRequests.Visible = true;
                lstRequests.Enabled = true;
                lstRequests.Items.Add(txtRequest.Text);
                if (lblBulkGiveOutTotal.Text == "")
                    lblBulkGiveOutTotal.Text = "1";
                else
                {
                    int oldNumber = Convert.ToInt16(lblBulkGiveOutTotal.Text);
                    lblBulkGiveOutTotal.Text = (oldNumber + 1).ToString();
                }
                //lstRequests.Items.Insert(0, txtRequest.Text);
                if (lstRequests.Items.Count != 1)//not first item
                {
                    txtRequest.Text = "";
                    txtRequest.Focus();
                    return;
                }
            }
            ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);

            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                PopupInfoDialog("Request ID not found", false);
                TakeInDefaultFields();
				txtRequest.Focus();
			} 
            else
            {
                dt = ds.Tables[0];
                if (dt.Rows[0]["GSIOrder"].ToString().Trim().Length > 0)
                {
                    lblOrderNumbeTakeIn.Text = "Order# \n\r" + Environment.NewLine + dt.Rows[0]["GSIOrder"].ToString();
                    hdnOrderCode.Value = dt.Rows[0]["GSIOrder"].ToString();
                    btnSubmitTakeIn.Enabled = false;
                    btnAcceptRequest.Enabled = false;
                    PopupInfoDialog("Order already created for this request.", false);
                    hdnMemoPicturePath.Value = dt.Rows[0]["MemoReceiptPDF"].ToString();
                    if (hdnMemoPicturePath.Value != "")
                        lnkView.Visible = true;
                    lnkView.Enabled = true;
                    return;
                }
                
                TakeInControlsEnableDisable(true);
                txtCustomerLookupTakeIn.Text = dt.Rows[0]["CustomerName"].ToString();
                hdnCustomerId.Value = dt.Rows[0]["CustomerID"].ToString();
                hdnContactId.Value = dt.Rows[0]["PersonID"].ToString();
				txtNoOfItemsTakeIn.Text = dt.Rows[0]["TotalQty"].ToString();
                txtSpecialInstructions.Text = dt.Rows[0]["SpecialInstructions"].ToString();
                txtMemoTakeIn.Text = dt.Rows[0]["MemoNum"].ToString();
                if (dt.Rows[0]["BatchMemo"] != null)
                    txtBatchMemos.Text = dt.Rows[0]["BatchMemo"].ToString();
                txtPONumber.Text = dt.Rows[0]["PONum"].ToString();
                txtSKU.Text = dt.Rows[0]["SKUName"].ToString();
                txtStyle.Text = dt.Rows[0]["StyleName"].ToString();
                txtTotalQty.Text = dt.Rows[0]["TotalQty"].ToString();
                //txtServiceTimeTakeIn.Text = dt.Rows[0]["ServiceTypeName"].ToString();
                txtRetailerName.Text = dt.Rows[0]["RetailerName"].ToString();
                if (txtRetailerName.Text.ToLower() == "blue nile" && dt.Columns.Contains("BNCategoryName"))
                {
                    if (dt.Rows[0]["BNCategoryName"].ToString() == "")
                    {
                        lblBNCategory.Visible = false;
                        txtBNCategory.Visible = false;
                    }
                    else
                    {
                        lblBNCategory.Visible = true;
                        txtBNCategory.Visible = true;
                        txtBNCategory.Text = dt.Rows[0]["BNCategoryName"].ToString();
                    }
                    if (dt.Rows[0]["BNDestinationName"].ToString() == "")
                    {
                        lblBNDestination.Visible = false;
                        txtBNDestination.Visible = false;
                    }
                    else
                    {
                        lblBNDestination.Visible = true;
                        txtBNDestination.Visible = true;
                        txtBNDestination.Text = dt.Rows[0]["BNDestinationName"].ToString();
                    }
                }

                //IvanB 13/02 start
                txtServiceTypeTakeIn.Text = dt.Rows[0]["ServiceTypeName"].ToString();
                txtCategoryTakeIn.Text = dt.Rows[0]["CategoryName"].ToString();
                hdnServiceTypeId.Value = dt.Rows[0]["GDServiceTypeId"].ToString();
                //IvanB 13/02 end
                txtTotalW.Text = dt.Rows[0]["TotalWeight"].ToString();
                hdnMemoPicturePath.Value = dt.Rows[0]["MemoReceiptPDF"].ToString();
                if (hdnMemoPicturePath.Value == "")
                    lnkView.Enabled = false;
                else
                    lnkView.Enabled = true;
                if (txtTotalW.Text != "" && txtTotalW.Text != "0")
                {
                    QtyNumber.Visible = false;
                    TotalW.Visible = true;
                }
                SetMessengersList(dt);
                LoadSyntheticShippingCourier();


                if (dt.Rows[0]["PersonID"].ToString() != "0")
                {
                    txtMessengerTakeIn.Text = dt.Rows[0]["Messenger"].ToString();
                    for (int i=0; i<= ddlMessengersList.Items.Count-1; i++)
                    {
                        string name = ddlMessengersList.Items[i].Text;
                        if (name == txtMessengerTakeIn.Text)
                        {
                            ddlMessengersList.SelectedValue = ddlMessengersList.Items[i].Value;
                            break;
                        }
                    }
                    string imgroot = Session["WebPictureShapeRoot"].ToString();
                    string imgTransperant_s = imgroot + @"Person/Signature/Empty.png";
                    int rand = new Random().Next(99999999);
                    hdnMessengerTakeIn.Value = ddlMessengersList.SelectedValue;
                    imgStoredPhoto.ImageUrl = hdnMessengerPath2Photo.Value=="" ? imgTransperant : string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
                    imgStoredSignature.ImageUrl = hdnMessengerPath2Signature.Value == "" ? imgTransperant_s : string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
                    //imgStoredPhoto.ImageUrl = string.IsNullOrEmpty(dt.Rows[0]["Path2PhotoFile"].ToString()) ? imgTransperant : dt.Rows[0]["Path2PhotoFile"].ToString();
                    //imgStoredSignature.ImageUrl = string.IsNullOrEmpty(dt.Rows[0]["Path2StoredSignature"].ToString()) ? imgTransperant : dt.Rows[0]["Path2StoredSignature"].ToString();
                    //imgStoredSignature.ImageUrl = hdnMessengerPath2Signature.Value;
                    //imgStoredPhoto.ImageUrl = string.IsNullOrEmpty(dt.Rows[0]["Path2PhotoFile"].ToString()) ? imgTransperant : dt.Rows[0]["Path2PhotoFile"].ToString();
                    imgStoredPhoto.Visible = true;
                    imgStoredSignature.Visible = true;
                    if (!saveShipInfoBox.Checked)
                    {
                        txtCarriersShipReceiving.Text = "";
                        hdnCarriersShipReceiving.Value = "0";
                    }
                    lblVendorTakeIn.Text = "";
                    hdnVendorTakeIn.Value = "0";
                }
                if (saveShipInfoBox.Checked)
                {
                    txtMessengerTakeIn.Text = "";
                }
                else
                {
                    if (dt.Rows[0]["CarrierCode"].ToString() != "")
                    {
                        txtCarriersShipReceiving.Text = dt.Rows[0]["CarrierName"].ToString();
                        ddlShippingCourier.SelectedIndex = ddlShippingCourier.Items.IndexOf(ddlShippingCourier.Items.FindByText(txtCarriersShipReceiving.Text));

                        hdnCarriersShipReceiving.Value = dt.Rows[0]["CarrierCode"].ToString();
                        lblVendorTakeIn.Text = dt.Rows[0]["VendorName"].ToString() == "" ? "" : "Destinatiom: " + dt.Rows[0]["VendorName"].ToString();
                        hdnVendorTakeIn.Value = dt.Rows[0]["VendorID"].ToString() == "" ? "0" : dt.Rows[0]["VendorID"].ToString();

                        txtMessengerTakeIn.Text = "";
                        hdnMessengerTakeIn.Value = "0";
                        imgStoredPhoto.ImageUrl = imgTransperant;
                        imgStoredSignature.ImageUrl = imgTransperant;
                        MsgDetails.Visible = false;
                        ShippingDetails.Visible = true;
                    }
                    else
                    {
                        MsgDetails.Visible = true;
                        ShippingDetails.Visible = false;
                    }
                }
                if (dt.Rows[0]["DeliveryMethodCode"].ToString() == "0")
                {
                    chkPickedUpByOurMessengerTakeIn.Checked = true;

                    txtMessengerTakeIn.Text = "";
                    hdnMessengerTakeIn.Value = "0";
                    imgStoredPhoto.ImageUrl = imgTransperant;
                    imgStoredSignature.ImageUrl = imgTransperant;

                    txtCarriersShipReceiving.Text = "";
                    hdnCarriersShipReceiving.Value = "0";
                    lblVendorTakeIn.Text = "";
                    hdnVendorTakeIn.Value = "0";
                }
                lblOrderNumbeTakeIn.Text = "";
                hdnOrderCode.Value = "0";
                if (!saveShipInfoBox.Checked)
                {
                    btnSubmitTakeIn.Enabled = true;
                    btnAcceptRequest.Enabled = true;
                }
            }
        }

        //private void SetMessengersList(DataTable dt)
        //{
        //    var customerModel = GetCustomerFromView(hdnCustomerId.Value);
        //    DataTable dt_personsList = QueryCustomerUtils.GetPersonsByCustomerMsg(customerModel, this);
        //    //var personsList = (from DataRow row in dt.Rows select new PersonExModel(row)).ToList();
        //    //var messengersList = personsList.FindAll(m => m.Position.Id == "1");
        //    //if (messengersList.Count > 0)
        //    ddlMessengersList.Items.Clear();
        //    foreach (DataRow row in dt_personsList.Rows)
        //    {
        //        string positionId = row["PositionID"].ToString();
        //        if (positionId != "5")
        //            continue;
        //        string firstName = row["FirstName"].ToString();
        //        string lastName = row["LastName"].ToString();
        //        string id = row["PersonID"].ToString();
        //        string fullName = firstName + " " + lastName;

        //        ddlMessengersList.Items.Add(firstName + " " + lastName);
        //        ddlMessengersList.Items[ddlMessengersList.Items.Count - 1].Value = id;
        //        if (fullName == dt.Rows[0]["Messenger"].ToString())
        //        {
        //            hdnMessengerPath2Photo.Value = row["Path2PhotoFile"].ToString();
        //            hdnMessengerPath2Signature.Value = row["Path2StoredSignature"].ToString();
        //        }
        //        //hdnMessengerPath2Photo.Value =

        //        //foreach (var messenger in messengersList)
        //        //{
        //        //    string firstName = null, lastName = null;
        //        //    if (messenger.FirstName != "")
        //        //        firstName = messenger.FirstName;
        //        //    if (messenger.LastName != "")
        //        //        lastName = messenger.LastName;
        //        //    ddlMessengersList.Items.Add(firstName + " " + lastName);
        //        //    ddlMessengersList.Items[ddlMessengersList.Items.Count -1].Value = messenger.PersonId.ToString();
        //        //    if (fullName == dt.Rows[0]["Messenger"].ToString())
        //        //    {
        //        //        hdnMessengerPath2Photo.Value = 
        //        //    if (messenger.FullName == dt.Rows[0]["Messenger"].ToString())
        //        //    {
        //        //        string id = messenger.PersonId.ToString();
        //        //        foreach (DataRow dr in dt_personsList.Rows)
        //        //        {
        //        //            string rowId = dr["PersonID"].ToString();
        //        //            if (rowId == id)
        //        //            {
        //        //                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
        //        //                hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
        //        //            }
        //        //        }

        //        //        //hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id +@"_Photo.png";
        //        //        //hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
        //        //    }
        //        //    //if (dt.Rows[0]["Messenger"].ToString() == messenger.PersonId)

        //        //}
        //    }
        //    //ValidateMessengersBtn.Visible = true;
        //    signatureBtn.Visible = true;
        //    photoBtn.Visible = true;
        //}
        //private void SetMessengersList(DataTable dt)
        //{
        //    var customerModel = GetCustomerFromView(hdnCustomerId.Value);
        //    List<PersonExModel> personsList = QueryCustomerUtils.GetPersonsByCustomer(customerModel, this);
        //    var messengersList = personsList.FindAll(m => m.Position.Id == "1");
        //    if (messengersList.Count > 0)
        //    {
        //        ddlMessengersList.Items.Clear();
        //        foreach (var messenger in messengersList)
        //        {
        //            string firstName = null, lastName = null;
        //            if (messenger.FirstName != "")
        //                firstName = messenger.FirstName;
        //            if (messenger.LastName != "")
        //                lastName = messenger.LastName;
        //            ddlMessengersList.Items.Add(firstName + " " + lastName);
        //            ddlMessengersList.Items[ddlMessengersList.Items.Count - 1].Value = messenger.PersonId.ToString();
        //            if (messenger.FullName == dt.Rows[0]["Messenger"].ToString())
        //            {
        //                string id = messenger.PersonId.ToString();
        //                bool exist = QueryUtils.GetInitialSignature(id, this);
        //                bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
        //                int rand = new Random().Next(99999999);

        //                if (exist)
        //                {
        //                    hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
        //                }
        //                else
        //                {
        //                    hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
        //                }
        //                imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
        //                if (photoExist)
        //                {
        //                    hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
        //                }
        //                else
        //                {
        //                    hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
        //                }
        //                imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
        //                //hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
        //            }

        //        }
        //    }
        //    //ValidateMessengersBtn.Visible = true;
        //    signatureBtn.Visible = true;
        //    photoBtn.Visible = true;
        //}

        private void SetMessengersList(DataTable dt, string dir = "takeIn")
        {
            if (dir == "takeIn")
            {
                var customerModel = GetCustomerFromView(hdnCustomerId.Value);
                List<PersonExModel> personsList = QueryCustomerUtils.GetPersonsByCustomer(customerModel, this);
                if (personsList.Count == 0)
                    return;
                var messengersList = personsList.FindAll(m => m.Position.Id == "1");
                ddlMessengersList.Items.Clear();
                foreach (var messenger in messengersList)
                {
                    string firstName = null, lastName = null;
                    if (messenger.FirstName != "")
                        firstName = messenger.FirstName;
                    if (messenger.LastName != "")
                        lastName = messenger.LastName;
                    ddlMessengersList.Items.Add(firstName + " " + lastName);
                    ddlMessengersList.Items[ddlMessengersList.Items.Count - 1].Value = messenger.PersonId.ToString();
                    if (messenger.FullName == dt.Rows[0]["Messenger"].ToString())
                    {
                        string id = messenger.PersonId.ToString();
                        bool exist = QueryUtils.GetInitialSignature(id, this);
                        bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
                        int rand = new Random().Next(99999999);

                        if (exist)
                        {
                            hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        }
                        else
                        {
                            hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                        }
                        imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
                        if (photoExist)
                        {
                            hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                        }
                        else
                        {
                            hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                        }
                        imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
                        //hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    }

                }
                if (ddlMessengersList.Items.Count == 0)
                {
                    ListItem customerContact = new ListItem();
                    customerContact.Value = "0";
                    customerContact.Text = "";
                    ddlMessengersList.Items.Insert(0, customerContact);
                    //PopupInfoDialog("Missing Messenger!", true);
                    //return;
                }
                if ((dt.Rows[0]["Messenger"].ToString() == "" || dt.Rows[0]["Messenger"].ToString() == null) && messengersList.Count > 0)//bulk load without messenger - choose first messenger from the list
                {
                    string id = messengersList[0].PersonId.ToString();
                    bool exist = QueryUtils.GetInitialSignature(id, this);
                    bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
                    int rand = new Random().Next(99999999);

                    if (exist)
                    {
                        hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    }
                    else
                    {
                        hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                    }
                    imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
                    if (photoExist)
                    {
                        hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                    }
                    else
                    {
                        hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                    }
                    imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
                    //hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    hdnMessengerTakeIn.Value = ddlMessengersList.SelectedItem.Value;
                }

            }
            else
            {
                int rand = new Random().Next(99999999);
                string customerId = dt.Rows[0]["CustomerID"].ToString();
                var customerModel = GetCustomerFromView(customerId);
                List<PersonExModel> personsList = QueryCustomerUtils.GetPersonsByCustomer(customerModel, this);
                if (personsList.Count == 0)
                    return;
                var messengersList = personsList.FindAll(m => m.Position.Id == "1");
                var tmp = dt.Rows[0]["GiveOutPersonId"].ToString();
                int outMessengerID = 0;
                if (dt.Columns.Contains("GiveOutPersonId") && dt.Rows[0]["GiveOutPersonId"] != null && dt.Rows[0]["GiveOutPersonId"].ToString() != "")
                    outMessengerID = Convert.ToInt32(dt.Rows[0]["GiveOutPersonId"].ToString());
                if (ddlGiveOutMessengersList.Items.Count > 0)
                    ddlGiveOutMessengersList.Items.Clear();
                bool found = false;
                foreach (var messenger in messengersList)
                {
                    string firstName = null, lastName = null;
                    if (messenger.FirstName != "")
                        firstName = messenger.FirstName;
                    if (messenger.LastName != "")
                        lastName = messenger.LastName;
                    ddlGiveOutMessengersList.Items.Add(firstName + " " + lastName);
                    ddlGiveOutMessengersList.Items[ddlGiveOutMessengersList.Items.Count - 1].Value = messenger.PersonId.ToString();
                    if (outMessengerID == messenger.PersonId)
                    {
                        ddlGiveOutMessengersList.SelectedValue = messenger.PersonId.ToString();
                        found = true;
                    }
                }//foreach
                //alex 0216
                //if (!found)
                //    ddlGiveOutMessengersList.SelectedIndex = 0;
                //alex
                bool personExists = false;
                for (int z = 0; z <= ddlGiveOutMessengersList.Items.Count - 1; z++)
                {
                    if (ddlGiveOutMessengersList.Items[z].Value == ddlGiveOutMessengersList.SelectedValue)
                    {
                        ddlGiveOutMessengersList.SelectedIndex = z;
                        txtGiveOutMessengersList.Text = ddlGiveOutMessengersList.SelectedItem.ToString();
                        hdnMessengerTakeOut.Value = ddlGiveOutMessengersList.Items[z].Value;
                        string id = ddlGiveOutMessengersList.Items[z].Value;
                        bool exist = QueryUtils.GetInitialSignature(id, this);
                        string oldGiveOutPhoto = SyntheticScreeningUtils.GetTakeOutPhotoPath(id, this);
                        bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
                        rand = new Random().Next(99999999);

                        if (exist)
                        {
                            hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        }
                        else
                        {
                            hdnTakeOutMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                        }
                        imgGiveOutSignature.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Signature.Value, '?', rand);
                        if (oldGiveOutPhoto != "" && oldGiveOutPhoto != null)
                            hdnTakeOutMessengerPath2Photo.Value = oldGiveOutPhoto;
                        else if (photoExist)
                        {
                            hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                        }
                        else
                        {
                            hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                        }
                        imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Photo.Value, '?', rand);
                        personExists = true;
                        break;
                    }
                }
                if (!personExists)
                {
                    string id = "";
                    bool exist = false, photoExist = false;
                    if (ddlGiveOutMessengersList.Items.Count > 0)
                    {
                        ddlGiveOutMessengersList.SelectedIndex = 0;
                        txtGiveOutMessengersList.Text = ddlGiveOutMessengersList.SelectedItem.ToString();
                        hdnMessengerTakeOut.Value = ddlGiveOutMessengersList.Items[0].Value;
                        id = ddlGiveOutMessengersList.Items[0].Value;
                        exist = QueryUtils.GetInitialSignature(id, this);
                        photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
                    }
                    rand = new Random().Next(99999999);

                    if (exist)
                    {
                        hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    }
                    else
                    {
                        hdnTakeOutMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                    }
                    imgGiveOutSignature.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Signature.Value, '?', rand);
                    if (photoExist)
                    {
                        hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                    }
                    else
                    {
                        hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                    }
                    imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Photo.Value, '?', rand);
                }
            }
            //}
            //ValidateMessengersBtn.Visible = true;
            signatureBtn.Visible = true;
            photoBtn.Visible = true;
        }//SetMessengersList
        protected void CourierListSelectedChanged(object sender, EventArgs e)
        {
            txtCarriersShipReceiving.Text = ddlShippingCourier.Text;
            hdnCarriersShipReceiving.Value = ddlShippingCourier.SelectedItem.Value;
        }
        protected void OnMessengerListIndexChanged(object sender, EventArgs e)
        {
            txtMessengerTakeIn.Text = ddlMessengersList.Text;
            hdnMessengerTakeIn.Value = ddlMessengersList.SelectedItem.Value;
            string id = hdnMessengerTakeIn.Value;
            bool exist = QueryUtils.GetInitialSignature(id, this);
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            int rand = new Random().Next(99999999);
            if (exist)
                hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            else
                hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            if (photoExist)
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
            imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
        }

        //protected void OnMsgSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id = msgListBox.SelectedItem.Value;
        //    string name = msgListBox.SelectedItem.Text;
        //    bool sent = QueryUtils.SendMessengerSignatureToStorage(id, this);
        //}
        protected void ValidateSignature_Click(object sender, EventArgs e)
        {
            //SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturesComparePopupExtender.Show();
            //return;
            string id = ddlMessengersList.SelectedItem.Value;
            string name = ddlMessengersList.SelectedItem.Text;
            string machineName = "";
            try
            {
                machineName = Session["MachineName"].ToString();
            }
            catch (Exception ex)
            {
                lblRequestError.Visible = true;
                lblRequestError.Text = "No machine name";
                return;
            }
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblRequestError.Visible = true;
                lblRequestError.Text = "Tablet is not configured";
                return;
            }
            //string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            //string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            int timeAdd = SyntheticScreeningUtils.GetTimeDiff(this);
            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");
            Session["DateStamp"] = date;
            string old_new = "old";
            if (imgStoredSignature.ImageUrl == "")
                old_new = "new";
            signatureBtn.Enabled = false;
            photoBtn.Enabled = false;
            bool sent = true;
            name = name.Replace(" ", "_");
            bool sigExists = QueryUtils.GetInitialSignature(id, this);
            if (!sigExists)
                old_new = "new";

            if (old_new == "new")
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date + " " + "n", dt, "signature", this);
            else
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date, dt, "signature", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i=0; i<=retryCount; i++)
                {
                    //found = QueryUtils.GetSignatureForValidation(frontMadenName, clientMadenName, date, this);
                    found = QueryUtils.GetSignatureForValidation(id, date, this);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        signatureBtn.Enabled = true;
                        photoBtn.Enabled = true;
                        
                        if (old_new == "new")
                        {
                            string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                            hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        }
                        imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value + '?' + rand);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblRequestError.Text = "Did not receive signature for validation. Please try again.";
                        signatureBtn.Enabled = true;
                        photoBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblRequestError.Text = "No signature to validate";
                    //lblCreateRequestMessege.Text += ": No Signature to validate";
                }
                else
                {
                    SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/FrontInOut/Signature/" + id + @"_" + date + @".png";
                    SignaturesComparePopupExtender.Show();
                    if (!lblCreateRequestMessege.Text.Contains("validation"))
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    else
                    {
                        int idx = lblCreateRequestMessege.Text.IndexOf(":");
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Substring(0, idx);
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    }
                    //lblRequestError.Text = "Signature was validated";
                    //if (old_new == "new")
                    //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                }
                
                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, frontMadenName, clientMadenName);
                //}
            }
            signatureBtn.Enabled = true;
            photoBtn.Enabled = true;
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }
        //protected void ViewPdf(object sender, EventArgs e)
        //{
        //    string pdfUrl = hdnMemoPicturePath.Value;
        //    //string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"500px\" height=\"300px\">";
        //    string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"700px\" height=\"700px\">";
        //    embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        //    embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        //    embed += "</object>";
        //    //ltEmbed.Text = string.Format(embed, ResolveUrl(@"https://gdlightstorage.blob.core.windows.net/gdlight/cdr_to_pdf/CDX.PDF"));
        //    //ltEmbed.Text = string.Format(embed, ResolveUrl(pdfUrl));
        //}
        protected void ViewPdf(object sender, EventArgs e)
        {
            string pdfUrl = hdnMemoPicturePath.Value;
            iframePDFViewer.Visible = true;
            iframePDFViewer.Src = pdfUrl;
            lnkView.Visible = false;
        }
        protected void ViewTakeOutReceipt(object sender, EventArgs e)
        {
            //WebClient webClient = new WebClient();
            //try
            //{
                
            //    string imgPath = @"https://gdlightstorage.blob.core.windows.net/takeoutreceipts/" + txtViewOrder.Text + @".pdf";
            //    byte[] data1 = webClient.DownloadData(imgPath);
            //    data1 = null;
            //}
            //catch (Exception ex)
            //{
            //    if (ex.Message.Contains("404"))
            //    {
            //        lblCreateRequestMessege.Text += " Receipt not found";
            //        return;
            //    }
            //    else
            //    {
            //        lblCreateRequestMessege.Text += " Error finding receipt";
            //        return;
            //    }
            //}
            bool exists = false;
            exists = AzureStorageBlob.BlobExists(txtViewOrder.Text + @".pdf", this);
            if (!exists)
            {
                lblCreateRequestMessege.Text += " Receipt not found";
                return;
            }
            string pdfUrl = @"https://gdlightstorage.blob.core.windows.net/takeoutreceipts/" + txtViewOrder.Text + @".pdf";
            if (lblCreateRequestMessege.Text.Contains("Receipt not found"))
                lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Replace("Receipt not found", "");
            iframePDFViewer.Visible = true;
            iframePDFViewer.Src = pdfUrl;
            takeOutLnkView.Visible = false;
        }
        protected void ValidatePhoto_Click(object sender, EventArgs e)
        {
            string id = ddlMessengersList.SelectedItem.Value;
            string name = ddlMessengersList.SelectedItem.Text;
            string machineName = Session["MachineName"].ToString();
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblRequestError.Visible = true;
                lblRequestError.Text = "Tablet is not configured";
                return;
            }
            
            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");
            
            Session["DateStamp"] = date;
            string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            photoBtn.Enabled = false;
            signatureBtn.Enabled = false;
            //to reprint initial photo delete the old 
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            if (photoExist)
            {
                bool deleted = QueryUtils.DeleteScreeningInitialPhoto(id, this);

            }
            bool sent = QueryUtils.SendMessengerInfoToStorage(id, dt, "photo", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i = 0; i <= retryCount; i++)
                {
                    found = QueryUtils.GetPhotoForValidation(id, this);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                        imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
                        photoBtn.Enabled = true;
                        signatureBtn.Enabled = true;
                        //string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblRequestError.Text = "Did not receive signature for validation. Please try again.";
                        photoBtn.Enabled = true;
                        signatureBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblRequestError.Text = "Error getting the picture";
                }
                else
                    lblCreateRequestMessege.Text += ": Photo validated";
                //else
                //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, machineName, clientMachineName);
                //}
                photoBtn.Enabled = true;
                signatureBtn.Enabled = true;
            }
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }
        protected void ValidateGiveOutSignature_Click(object sender, EventArgs e)
        {
            //SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturesComparePopupExtender.Show();
            //return;
            string id = ddlGiveOutMessengersList.SelectedItem.Value;
            string name = ddlGiveOutMessengersList.SelectedItem.Text;
            string machineName = "";
            try
            {
                machineName = Session["MachineName"].ToString();
            }
            catch (Exception ex)
            {
                lblCreateRequestMessege.Visible = true;
                lblCreateRequestMessege.Text = "No machine name";
                return;
            }
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblCreateRequestMessege.Visible = true;
                lblCreateRequestMessege.Text = "Tablet is not configured";
                return;
            }
            //string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            //string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            int timeAdd = SyntheticScreeningUtils.GetTimeDiff(this);
            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");
            Session["DateStamp"] = date;
            string old_new = "old";
            if (imgGiveOutSignature.ImageUrl == "")
                old_new = "new";
            giveOutSignatureBtn.Enabled = false;
            giveOutPhotoBtn.Enabled = false;
            bool sent = true;
            name = name.Replace(" ", "_");
            bool sigExists = QueryUtils.GetInitialSignature(id, this);
            if (!sigExists)
                old_new = "new";

            if (old_new == "new")
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date + " " + "n", dt, "signature", this);
            else
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date, dt, "signature", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i = 0; i <= retryCount; i++)
                {
                    //found = QueryUtils.GetSignatureForValidation(frontMadenName, clientMadenName, date, this);
                    found = QueryUtils.GetSignatureForValidation(id, date, this);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        giveOutSignatureBtn.Enabled = true;
                        giveOutPhotoBtn.Enabled = true;

                        if (old_new == "new")
                        {
                            string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                            //    hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        }
                        hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        imgGiveOutSignature.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Signature.Value + '?' + rand);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblCreateRequestMessege.Text = "Did not receive signature for validation. Please try again.";
                        giveOutSignatureBtn.Enabled = true;
                        giveOutPhotoBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblCreateRequestMessege.Text = "No signature to validate";
                    //lblCreateRequestMessege.Text += ": No Signature to validate";
                }
                else
                {
                    SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/FrontInOut/Signature/" + id + @"_" + date + @".png";

                    // testing string SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    SignaturesComparePopupExtender.Show();
                    if (!lblCreateRequestMessege.Text.Contains("validation"))
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    else
                    {
                        int idx = lblCreateRequestMessege.Text.IndexOf(":");
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Substring(0, idx);
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    }
                    //lblRequestError.Text = "Signature was validated";
                    //if (old_new == "new")
                    //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                }

                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, frontMadenName, clientMadenName);
                //}
            }
            giveOutSignatureBtn.Enabled = true;
            giveOutPhotoBtn.Enabled = true;
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }
        protected void ValidateGiveOutPhoto_Click(object sender, EventArgs e)
        {
            string id = ddlGiveOutMessengersList.SelectedItem.Value;
            string name = ddlGiveOutMessengersList.SelectedItem.Text;
            string machineName = Session["MachineName"].ToString();
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblCreateRequestMessege.Visible = true;
                lblCreateRequestMessege.Text = "Tablet is not configured";
                return;
            }
            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");
            Session["DateStamp"] = date;
            string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            giveOutPhotoBtn.Enabled = false;
            giveOutSignatureBtn.Enabled = false;
            bool sent = QueryUtils.SendMessengerInfoToStorage(id + " " + date, dt, "photo", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i = 0; i <= retryCount; i++)
                {
                    found = QueryUtils.GetPhotoForValidation(id, this, date);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/FrontInOut/Photo/" + id + "_" + date +  @".png";
                        imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Photo.Value, '?', rand);
                        giveOutPhotoBtn.Enabled = true;
                        giveOutSignatureBtn.Enabled = true;
                        //string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblRequestError.Text = "Did not receive photo for validation. Please try again.";
                        giveOutPhotoBtn.Enabled = true;
                        giveOutSignatureBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblCreateRequestMessege.Text = "Error getting the picture";
                }
                else
                {
                    lblCreateRequestMessege.Text += ": Photo validated";
                    string photoPath = @"https://gdlightstorage.blob.core.windows.net/gdlight/FrontInOut/Photo/" + id + "_" + date + @".png";
                    bool saved = SyntheticScreeningUtils.SaveTakeOutPhotoPath(txtViewOrder.Text, photoPath, this);
                    if (saved)
                    {
                        btnPrintReceiptTakeOut.Enabled = true;
                        if (Session["PickupMail"].ToString() == "0")
                            btnPickupMail.Visible = false;
                        hdnTakeOutPhoto.Value = photoPath;
                        btnUsePhotoTakeOut.Enabled = false;
                        btnSavePhotoTakeOut.Enabled = true;
                    }
                }
                //else
                //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, machineName, clientMachineName);
                //}
                
                giveOutPhotoBtn.Enabled = true;
                giveOutSignatureBtn.Enabled = true;
            }
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }
        public void btnUseTakeOutPhotoViewOrder_Click(object sender, EventArgs e)//reuse existing photo for different orders
        {
            int rand = new Random().Next(99999999);
            imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutPhoto.Value, '?', rand);
            bool saved = SyntheticScreeningUtils.SaveTakeOutPhotoPath(txtViewOrder.Text, hdnTakeOutPhoto.Value, this);
            if (saved)
            {
                btnPrintReceiptTakeOut.Enabled = true;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                //btnSavePhotoTakeOut.Enabled = false;
            }
        }
        public void btnSaveTakeOutPhotoViewOrder_Click(object sender, EventArgs e)
        { 
            btnUsePhotoTakeOut.Enabled = true;
        }
        public void ValidateSignatures(string id, string machineName, string clientMachineName)
        {
            string queueNameRoute = Session["WebPictureShapeRoot"].ToString();
            string url1 = queueNameRoute + @"FrontInOut/Person/Signature/" + id + @"_Signature.png";
            string url2 = queueNameRoute + @"FrontInOut/Person/Signature/temp/" + id + @"_Signature.png";
            //NewReportPicture1.ImageUrl = url1;
            //NewReportPicture2.ImageUrl = url2;
            //NewPicturePopupExtender.Show();
        }
        protected void btnSwitchToMessenger_Click(object sender, EventArgs e)
        {
            MsgDetails.Visible = true;
            ShippingDetails.Visible = false;
            //ddlShippingCourier.Text = "";
            hdnCarriersShipReceiving.Value = "0";
            if (hdnMessengerTakeIn.Value == "0")
                hdnMessengerTakeIn.Value = ddlMessengersList.Items[0].Value;
            //ValidateMessengersBtn.Visible = true;
            signatureBtn.Visible = true;
            photoBtn.Visible = true;
            string id = hdnMessengerTakeIn.Value;
            bool exist = QueryUtils.GetInitialSignature(id, this);
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            int rand = new Random().Next(99999999);
            if (exist)
                hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            else
                hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            if (photoExist)
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
            imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
        }
        protected void btnSwitchToCourier_Click(object sender, EventArgs e)
        {
            ShippingDetails.Visible = true;
            MsgDetails.Visible = false;
            //ddlMessengersList.Text = "";
            hdnMessengerTakeIn.Value = "0";
            hdnCarriersShipReceiving.Value = ddlShippingCourier.Items[0].Value;
            hdnMessengerTakeIn.Value = ddlMessengersList.SelectedItem.Value;
            
        }
        protected void btnGiveOutSwitchToMessenger_Click(object sender, EventArgs e)
        {
            btnPrintReceiptTakeOut.Enabled = true;
            if (Session["PickupMail"].ToString() == "0")
                btnPickupMail.Visible = false;
            else
                btnPickupMail.Enabled = true;
            GiveOutShipping.Visible = false;
            GiveOutMessenger.Visible = true;
            //ddlShippingCourier.Text = "";
            hdnGiveOutCarriersShip.Value = "0";
            if (hdnMessengerTakeOut.Value == "0" && ddlGiveOutMessengersList.Items.Count > 0)
                hdnMessengerTakeOut.Value = ddlGiveOutMessengersList.Items[0].Value;
            //ValidateMessengersBtn.Visible = true;
            signatureBtn.Visible = true;
            photoBtn.Visible = true;
            string id = hdnMessengerTakeOut.Value;
            bool exist = false, photoExist = false;
            if (id != "" && id != "0")
            {
                exist = QueryUtils.GetInitialSignature(id, this);
                photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            }
            else
            {
                PopupInfoDialog("No messengers found for customer ", false);
            }
            int rand = new Random().Next(99999999);
            if (exist)
                hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            else
                hdnTakeOutMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            if (photoExist)
                hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            imgGiveOutSignature.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Signature.Value, '?', rand);
            imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Photo.Value, '?', rand);
            //if (iframePDFViewer.Visible == false)
            //    takeOutLnkView.Visible = true;
            iframePDFViewer.Visible = false;
            takeOutLnkView.Visible = true;
            takeOutLnkView.Enabled = true;
        }
        protected void btnGiveOutSwitchToCourier_Click(object sender, EventArgs e)
        {
            GiveOutShipping.Visible = true;
            GiveOutMessenger.Visible = false;
            //ddlMessengersList.Text = "";
            hdnMessengerTakeOut.Value = "0";
            if (ddlGiveOutShippingCourier.Items.Count == 0)
                LoadGiveOutSyntheticShippingCourier();
            hdnGiveOutCarriersShip.Value = ddlGiveOutShippingCourier.Items[0].Value;
            btnPrintReceiptTakeOut.Enabled = true;
            if (Session["PickupMail"].ToString() == "0")
                btnPickupMail.Visible = false;
            else
                btnPickupMail.Enabled = true;
            //if (iframePDFViewer.Visible == false)
            //    takeOutLnkView.Visible = true;
            iframePDFViewer.Visible = false;
            takeOutLnkView.Visible = true;
            takeOutLnkView.Enabled = true;
            //hdnMessengerTakeOut.Value = ddlGiveOutMessengersList.SelectedItem.Value;
            btnGiveOutSwitchToMessenger.Enabled = true;
        }
        protected void chkSubmissionPickedUpByGSIMessenger_CheckedChanged(object sender, EventArgs e)
		{
            if(chkSubmissionPickedUpByGSIMessenger.Checked == true)
			{
                rfvSubmissionCourierList.Enabled = false;
                rfvSubmissionMessengerList.Enabled = false;
                imgSubmissionStoredSignature.ImageUrl = imgTransperant;
                imgSubmissionStoredPhoto.ImageUrl = imgTransperant;
            }
            else
            {
                rfvSubmissionCourierList.Enabled = true;
                rfvSubmissionMessengerList.Enabled = true;
            }
        }
        protected void chkPickedUpByOurMessengerTakeIn_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPickedUpByOurMessengerTakeIn.Checked == true)
            {
                txtMessengerTakeIn.Text = "";
                hdnMessengerTakeIn.Value = "0";
                imgStoredPhoto.ImageUrl = imgTransperant;
                imgStoredSignature.ImageUrl = imgTransperant;
            }
           
        }

        public void LoadSyntheticShippingCourier()
        {
            DataSet ds = QueryUtils.GetCarriers(this);
            DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlShippingCourier.DataTextField = "CarrierName";
                ddlShippingCourier.DataValueField = "CarrierCode";
                ddlShippingCourier.DataSource = dt;
                ddlShippingCourier.DataBind();
            }
        }
        public void LoadGiveOutSyntheticShippingCourier()
        {
            DataSet ds = QueryUtils.GetCarriers(this);
            DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlGiveOutShippingCourier.DataTextField = "CarrierName";
                ddlGiveOutShippingCourier.DataValueField = "CarrierCode";
                ddlGiveOutShippingCourier.DataSource = dt;
                ddlGiveOutShippingCourier.DataBind();
            }
        }
        public void TakeInControlsEnableDisable(bool Enabled)
        {
            rdlNoOfItems.Enabled = Enabled;
            btnServiceTimeTakeIn.Enabled = Enabled;
            btnSubmitTakeIn.Enabled = Enabled;
            btnAcceptRequest.Enabled = Enabled;
            btnSubmitTakeInAll.Enabled = Enabled;
            lblOrderNumbeTakeIn.Text = "";
            btnSwithToMessenger.Enabled = Enabled;
            btnSwithToCourier.Enabled = Enabled;
        }

        private CustomerModel GetCustomerFromView(string customerId)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
                            new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);
        }

        private PersonExModel GetPersonFromView(int personId)
        {
            var persons = GetViewState(SessionConstants.PersonEditData) as List<PersonExModel> ??
                            new List<PersonExModel>();
            return persons.Find(m => m.PersonId == personId);
        }

        protected void rdbServiceTimeTakeIn_IndexChanged(object sender, EventArgs e)
        {
            txtServiceTimeTakeIn.Text = rdlServiceTimeTakeIn.SelectedItem.Text.ToString();
            hdnServiceTimeTakeIn.Value = rdlServiceTimeTakeIn.SelectedItem.Value.ToString();
        }
        
        protected void btnClearTakeIn_Click(object sender, EventArgs e)
        {
            TakeInDefaultFields();
            txtRequest.Text = "";
            if (Session["MachineName"] != null)
                lblCreateRequestMessege.Text = Session["MachineName"].ToString();
            else
                lblCreateRequestMessege.Text = "No machine name.";
            lblRequestError.Text = "";
            hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            hdnTakeOutMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            imgStoredSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            imgStoredPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            ddlMessengersList.Items.Clear();
            ddlShippingCourier.Items.Clear();
            btnSwithToCourier.Enabled = false;
            btnSwithToMessenger.Enabled = false;
            btnSendEmail.Enabled = false;
            //ltEmbed.Text = "";
            lnkView.Enabled = false;
            iframePDFViewer.Visible = false;
            lstRequests.Items.Clear();
            hdnAcceptCustomerID.Value = "";
            txtRequest.Focus();
            nbrCopiesBox.Text = "1";
            hdnAcceptCustomerID.Value = "";
            hdnContactId.Value = "";
            hdnAcceptCarrier.Value = "";
            hdnAcceptMessenger.Value = "";
            acceptanceBox.Checked = false;
            lstRequests.Visible = false;
            lblBulkGiveOutTotal.Visible = false;
            lblBulkGiveOutTotal.Text = "";
            //IvanB 13/02 start
            hdnServiceTypeId.Value = "";
            txtGiveOutScanPackageBarCodeShip.Text = "";
            txtBNCategory.Text = "";
            txtBNCategory.Visible = false;
            txtBNDestination.Text = "";
            txtBNDestination.Visible = false;
            lblBNCategory.Visible = false;
            lblBNDestination.Visible = false;
            txtBatchMemos.Text = "";
            Session["PrevServiceType"] = null;
            //saveShipInfoBox.Checked = false;
            //IvanB 13/02 end
        }

        private void TakeInDefaultFields()
        {
            if (txtRequest.Text != "" && txtRequest.Text[0] != 'B')
                txtRequest.Text = "";            
            txtCustomerLookupTakeIn.Text = "";
            hdnCustomerId.Value = "0";
            lblVendorTakeIn.Text = "";
            hdnVendorTakeIn.Value = "0";
            txtMessengerTakeIn.Text = "";
            if (txtRequest.Text == "")
                hdnMessengerTakeIn.Value = "0";
            imgStoredPhoto.ImageUrl = imgTransperant;
            imgStoredSignature.ImageUrl = imgTransperant;
            //hdnContactId.Value = "0";
            txtNoOfItemsTakeIn.Text = "";
            txtTotalW.Text = "";
            txtServiceTimeTakeIn.Text = "72 Hours";
            hdnServiceTimeTakeIn.Value = "2";

            rdlServiceTimeTakeIn.SelectedIndex = 1;
            txtMemoTakeIn.Text = "";
            txtSpecialInstructions.Text = "";

            txtRetailerName.Text = "";
            //IvanB 13/02
            txtCategoryTakeIn.Text = "";
            txtServiceTypeTakeIn.Text = "";
            //IvanB 13/02
            txtPONumber.Text = "";
            txtSKU.Text = "";
            txtStyle.Text = "";
            txtTotalQty.Text = "";

            rdlNoOfItems.SelectedIndex = 1;
            lblOrderNumbeTakeIn.Text = "";

            txtCarriersShipReceiving.Text = "";
            if (!saveShipInfoBox.Checked)
                hdnCarriersShipReceiving.Value = "0";
            txtScanPackageBarcodeShipReceiving.Text = "";
            TotalW.Visible = false;
            QtyNumber.Visible = true;
            chkPickedUpByOurMessengerTakeIn.Checked = false;
            btnSubmitTakeIn.Enabled = false;
            btnAcceptRequest.Enabled = false;
            //btnSubmitTakeInAll.Enabled = false;
            btnPrintLabelTakeIn.Enabled = false;
            btnPrintReceiptTakeIn.Enabled = false;
            btnSendEmail.Enabled = false;
            TakeInControlsEnableDisable(false);
        }
        protected void SignatureFailed_Click(object sender, EventArgs e)
        {
            //PopupInfoDialog("Signature is not validated", false);
            lblRequestError.Text = "Signature Validation Failed";
            btnClearTakeIn_Click(null, null);
        }
        protected void nbrCopies_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            if (btn.Text == "")
                return;
            string txt = nbrCopiesBox.Text.Trim() + btn.Text;
            nbrCopiesBox.Text = txt;
        }
        protected void nbrCopiesView_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            if (btn.Text == "")
                return;
            string txt = NmbrOfCopiesBox.Text.Trim() + btn.Text;
            NmbrOfCopiesBox.Text = txt;
        }
        protected void nbrCopiesClear_Click(object sender, EventArgs e)
        {
            nbrCopiesBox.Text = "";
        }
        protected void nbrCopiesClearView_Click(object sender, EventArgs e)
        {
            NmbrOfCopiesBox.Text = "";
        }
        protected void btnSubmitTakeIn_Click(object sender, EventArgs e)
        {
            //if (Session["MachineName"] == null)//no machine name
            //{
            //    PopupInfoDialog("No machine name.Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx", false);
            //    return;
            //}
            //if (hdnMessengerTakeIn.Value == "0" && chkPickedUpByOurMessengerTakeIn.Checked == false && hdnCarriersShipReceiving.Value == "0")
            //{
            //    PopupInfoDialog("Please select Messenger or Carrier or Taken-Out-By-Our-Messenger", false);
            //    return;
            //}
            if (Session["MachineName"] == null)//no machine name
            {
                lblRequestError.Text = "No machine name.Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx";
                return;
            }
            if (hdnMessengerTakeIn.Value == "0" && chkPickedUpByOurMessengerTakeIn.Checked == false && hdnCarriersShipReceiving.Value == "0")
            {
                lblRequestError.Text = "Please select Messenger or Carrier or Taken-Out-By-Our-Messenger";
                return;
            }


            //SaveQPopupExtender.Show();
            OnQuesSaveYesClick(null, null);
            btnPrintReceiptTakeIn.Enabled = true;
            btnPrintLabelTakeIn.Enabled = true;
        }
        protected void btnSubmitTakeInAll_Click(object sender, EventArgs e)
        {
            if (txtScanPackageBarcodeShipReceiving.Text == "" || ddlShippingCourier.SelectedItem.Text == "")
            {
                lblRequestError.Text = "Missing shipping info";
                return;
            }
            savedTrackingNumberTxt.Text = txtScanPackageBarcodeShipReceiving.Text;
            savedCarrierTxt.Text = ddlShippingCourier.SelectedItem.Text;
            //string bRequestId = txtRequest.Text;

            //for (int i= 0; i <= lstRequests.Items.Count - 1; i++)
            //{
            //    string requestId = lstRequests.Items[i].Text;
            //    txtRequest.Text = requestId;
            //    hdnRequestID.Value = requestId;
            //    LoadRequestDetails();
            //    SubmitTakeIn();
            //    lstRequests.Items[i].Text += "-" + lblOrderNumbeTakeIn.Text;
            //}
        }

        protected void btnAcceptRequest_Click(object sender, EventArgs e)
        {
            if (Session["MachineName"] == null)//no machine name
            {
                PopupInfoDialog("No machine name. Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx", false);
                return;
            }
            //IvanB 13/02 start
            if (!acceptanceBox.Checked)
            {
                return;
            }
            //IvanB 13/02 end
                string messenger = "", carrier = "", carrierTrackingNumber = "";
            if (txtScanPackageBarcodeShipReceiving.Text == "")//messenger
            {
                if (ddlMessengersList.Items.Count > 0)
                    messenger = ddlMessengersList.SelectedItem.Text;
                else if (hdnAcceptMessenger.Value != "")
                    messenger = hdnMessengerTakeIn.Value;
            }
            else
            {
                carrier = hdnAcceptCarrier.Value;
                carrierTrackingNumber = txtScanPackageBarcodeShipReceiving.Text;
            }
            string customerId = hdnCustomerId.Value.ToString();
            string customerName = hdnAcceptCustomerName.Value.ToString();
            if (lstRequests.Items.Count == 0)
            {
                PopupInfoDialog("Request list is empty", false);
                return;
            }
            List<string> badMails = new List<string>();
            lblCreateRequestMessege.Text += ", Acceptance email sent for ";
            List<SyntheticAcceptanceModel> requests = new List<SyntheticAcceptanceModel>();
            foreach (var item in lstRequests.Items)
            {
                SyntheticAcceptanceModel acceptRecord = new SyntheticAcceptanceModel();
                string rId = item.ToString();
                int requestId = Convert.ToInt32(rId);
                DataTable dt = FrontUtils.SetAcceptRequest(requestId, this);
                if (dt == null || dt.Rows.Count == 0)
                {
                    PopupInfoDialog("Error Accepting Request ID " + rId, true);
                    return;
                }
                string result = dt.Rows[0].ItemArray[0].ToString();
                if (result == "BAD")
                {
                    PopupInfoDialog("Request ID " + rId + " not found", true);
                    return;
                }
                
                DataSet dsRequest = SyntheticScreeningUtils.GetSyntheticCustomerEntries(rId, this);
                DataRow drRequest = dsRequest.Tables[0].Rows[0];
                string order = drRequest["GSIOrder"].ToString();
                string contactId = drRequest["PersonID"].ToString();
                string totalQty = drRequest["TotalQty"].ToString();
                string mailAddr = SyntheticScreeningUtils.GetContactEmail(contactId, this);
                string contactName = drRequest["PersonName"].ToString();
                string memo = drRequest["MemoNum"].ToString();
                string retailerName = drRequest["RetailerName"].ToString();
                
                acceptRecord.Memo = memo;
                acceptRecord.TotalQty = totalQty;
                acceptRecord.RequestID = rId;
                acceptRecord.RetailerName = retailerName;
                requests.Add(acceptRecord);
            }//foreach
            string authorName = Session["AuthorName"].ToString();
            PrintAcceptanceReceipt(requests, customerId, customerName, messenger, carrier, carrierTrackingNumber, authorName);
            //foreach (var item in lstRequests.Items)
            //{
            //    SyntheticAcceptanceModel acceptRecord = new SyntheticAcceptanceModel();
            //    string rId = item.ToString();
            //    int requestId = Convert.ToInt32(rId);
            //    DataTable dt = FrontUtils.SetAcceptRequest(requestId, this);
            //    if (dt == null || dt.Rows.Count == 0)
            //    {
            //        PopupInfoDialog("Error Accepting Request ID " + rId, true);
            //        return;
            //    }
            //    string result = dt.Rows[0].ItemArray[0].ToString();
            //    if (result == "BAD")
            //    {
            //        PopupInfoDialog("Request ID " + rId + " not found", true);
            //        return;
            //    }
            //    DataSet dsRequest = SyntheticScreeningUtils.GetSyntheticCustomerEntries(rId, this);
            //    DataRow drRequest = dsRequest.Tables[0].Rows[0];
            //    string order = drRequest["GSIOrder"].ToString();
            //    string contactId = drRequest["PersonID"].ToString();
            //    string totalQty = drRequest["TotalQty"].ToString();
            //    string mailAddr = SyntheticScreeningUtils.GetContactEmail(contactId, this);
            //    string contactName = drRequest["PersonName"].ToString();
            //    string memo = drRequest["MemoNum"].ToString();
            //    string retailerName = drRequest["RetailerName"].ToString();


            //    if (result != "OLD")
            //    {
            //        //PopupInfoDialog("Request ID " + rId + " was accepted before", true);
            //        //return;
            //        dtList.Rows.Add(mailAddr, rId, memo, totalQty);
            //        string sent = sendAcceptEmailToContact(mailAddr, rId, memo, totalQty, this);
            //        if (sent != "OK")
            //        {
            //            for (int i = 0; i <= 3; i++)
            //            {
            //                sent = sendAcceptEmailToContact(mailAddr, rId, memo, totalQty, this);
            //                if (sent == "OK")
            //                    break;
            //            }
            //        }
            //        if (sent == "OK")
            //            lblCreateRequestMessege.Text += rId + ", ";
            //        else
            //            badMails.Add(rId);
            //    }
            //}//foreach
            //string authorName = Session["AuthorName"].ToString();
            //PrintAcceptanceReceipt(requests, customerId, customerName, messenger, carrier, carrierTrackingNumber, authorName);
            //string bulkSent = sendAcceptBulkEmailToContact(dtList);
            //if (badMails.Count >0)
            //{
            //    lblCreateRequestMessege.Text += ",Error sending emails for: ";
            //    foreach (string badMail in badMails)
            //        lblCreateRequestMessege.Text += badMail + ",";
            //}
            
        }//btnAcceptRequest_Click

        private string SubmitTakeIn()
        {
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
            string totalQty = txtTotalQty.Text;
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            string customerId = hdnCustomerId.Value.ToString();

            objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);

            objNewOrderModelExt.NumberOfItems = txtNoOfItemsTakeIn.Text.ToString() == "" ? 0 : Convert.ToInt32(txtNoOfItemsTakeIn.Text.ToString());
            objNewOrderModelExt.TotalWeight = 0;
            objNewOrderModelExt.IsIQInspected = rdlNoOfItems.Items[0].Selected == true ? true : false;
            objNewOrderModelExt.IsTWInspected = false;

            objNewOrderModelExt.MeasureUnitID = 2;
            //IvanB 13/02 start
            objNewOrderModelExt.ServiceTypeID = Convert.ToInt32(hdnServiceTypeId.Value.ToString());
            //IvanB 13/02 start
            objNewOrderModelExt.Memo = txtMemoTakeIn.Text.ToString();
            objNewOrderModelExt.SpecialInstructions = txtSpecialInstructions.Text.ToString();
            
            objNewOrderModelExt.VendorID = Convert.ToInt32(hdnVendorTakeIn.Value);
            if (TotalW.Visible == true)
            {
                objNewOrderModelExt.TotalWeight = Convert.ToDecimal(txtTotalW.Text.ToString());
            }
            else
                objNewOrderModelExt.TotalWeight = 0;
            string vendorId = hdnVendorTakeIn.Value;
            if (vendorId != "0")
            {
                var vendors = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
                objNewOrderModelExt.VendorOfficeID = 0;
            }

            if (hdnCarriersShipReceiving.Value.ToString() == "0" && savedTrackingNumberTxt.Text == "")
            {
                objNewOrderModelExt.PersonID = hdnMessengerTakeIn.Value == "0" ? 0 : Convert.ToInt32(hdnMessengerTakeIn.Value.ToString());
                var custId = customers.Find(m => m.CustomerId == customerId);
                objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
                objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).OfficeId);
                objNewOrderModelExt.CarrierID = 0;
                objNewOrderModelExt.CarrierTrackingNumber = "";
            }
            else
            {
                objNewOrderModelExt.PersonID = 0;
                objNewOrderModelExt.PersonCustomerID = 0;
                objNewOrderModelExt.PersonCustomerOfficeID = 0;
                objNewOrderModelExt.CarrierID = hdnCarriersShipReceiving.Value.ToString() == "0" ? 0 : Convert.ToInt32(hdnCarriersShipReceiving.Value.ToString());
                objNewOrderModelExt.CarrierTrackingNumber = txtScanPackageBarcodeShipReceiving.Text.ToString();
                if (savedTrackingNumberTxt.Text != "")
                {
                    objNewOrderModelExt.CarrierTrackingNumber = savedTrackingNumberTxt.Text.ToString(); ;
                }
                else
                    objNewOrderModelExt.CarrierTrackingNumber = txtScanPackageBarcodeShipReceiving.Text.ToString();
            }

            string errMsg = string.Empty;
            string reqId = "";
            if (txtRequest.Text != "")
                reqId = txtRequest.Text;
            else if (hdnRequestID.Value != "")
                reqId = hdnRequestID.Value;
            string sku = "", batchMemos = "";
            if (reqId != "")
            {
                try
                {
                    DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(reqId, this);
                    DataRow tmpDr = ds.Tables[0].Rows[0];
                    string order = tmpDr["GSIOrder"].ToString();
                    sku = tmpDr["SKUName"].ToString();
                    batchMemos = tmpDr["BatchMemo"].ToString();
                    if (order != "")
                        return "Order " + order + " already exist for requestId " + reqId;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return "Error finding order for " + reqId; 
                }
            }
            var result = FrontUtils.CreateOrder(objNewOrderModelExt, "TakeIn", this, out errMsg);

            if (result != null)
            {
                objNewOrderModelExt.OrderNumber = result;
                string groupId = null;
                DataSet dsGroupId = new DataSet();
                dsGroupId = GSIAppQueryUtils.GetGroupId(objNewOrderModelExt.OrderNumber, this);
                if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
                {
                    groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                bool added = true;
                if (batchMemos == "")
                    added = SyntheticScreeningUtils.SetMemoNumber(groupId, txtMemoTakeIn.Text.ToString(), this);
                else
                {
                    string[] batchMemoList = batchMemos.Split('|');
                    foreach (string batchMemo in batchMemoList)
                        added = SyntheticScreeningUtils.SetMemoNumber(groupId, batchMemo, this);
                }

				//Set SetOrderLocation
				if (groupId != null)
					SetOrderOffice(Convert.ToInt32(groupId));

				objNewOrderModelExt.PickedUpByOurMessenger = chkPickedUpByOurMessengerTakeIn.Checked == true ? true : false;
                if (chkPickedUpByOurMessengerTakeIn.Checked == true)
                {
                    var result_Messenger = FrontUtils.SetPickedUpByOurMessenger(objNewOrderModelExt, this, out errMsg);
                }

                var setOrder = SyntheticScreeningUtils.SetSyntheticCustomerOrder(Convert.ToInt32(txtRequest.Text.Trim()), objNewOrderModelExt.OrderNumber, Convert.ToInt32(hdnServiceTimeTakeIn.Value.ToString()),this);
                bool qc_lab = false;
                if (setOrder != null && setOrder.Tables[0].Rows.Count >0)
                {
                    if (setOrder.Tables[0].Rows[0].ItemArray[0].ToString() == "Y")
                        qc_lab = true;
                }
                if ((sku != "" && (objNewOrderModelExt.ServiceTypeID == 1 || objNewOrderModelExt.ServiceTypeID == 14 || qc_lab) && (batchMemos == "" || !batchMemos.Contains("|"))) && (Convert.ToInt32(totalQty) < 2476))//if sku exists in customerProgram - run itemizing and number of batches < 100
                {
                    if (batchMemos != "")//single batch memo - use it for itemizing
                        objNewOrderModelExt.Memo = batchMemos;
                    int remoteItemsSize = Convert.ToInt32(Session["RemoteItemizingSize"].ToString());
                    if (Session["RemoteItemizing"].ToString() == "1" && objNewOrderModelExt.NumberOfItems > remoteItemsSize)
                    {
                        string id = Session["ID"].ToString();
                        string officeId = Session["AuthorOfficeID"].ToString();
                        string sent = SendBatchItemToAzure(objNewOrderModelExt, id, officeId, sku);
                        if (sent == "success")
                        {
                            lblCreateRequestMessege.Text += ", order " + objNewOrderModelExt.OrderNumber + " sent for itemizing";
                        }
                        else
                        {
                            lblCreateRequestMessege.Text += ", error sending order " + objNewOrderModelExt.OrderNumber + " for itemizing";
                        }
                    }
                    else
                    {
                        string created = CreateBatchItem(objNewOrderModelExt, sku);
                        if (!created.Contains("success"))
                            PopupInfoDialog("Error creating batch: " + created, true);
                        else
                        {
                            created = created.Replace("success: ", "");
                            string[] batches = created.Split(',');
                            foreach (string batch in batches)
                            {
                                int BatchID = Convert.ToInt32(batch);
                                int EventID = FrontUtils.BatchEvents.Created;
                                int ItemsAffected = Convert.ToInt32(totalQty);
                                int ItemsInBatch = Convert.ToInt32(totalQty);
                                int FormID = FrontUtils.Codes.Itemizing;
                                FrontUtils.SetBatchEvent(EventID, BatchID, FormID, ItemsAffected, ItemsInBatch, this);//Procedure dbo.spSetBatchEvents
                                                                                                                      //[3 / 2 / 2022 12:43:31 PM]ItemizingUpdateGroup: GroupOfficeID = 1; GroupID = 1704310; VendorOfficeID =; VendorID =; InspectedQuantity = 3; InspectedTotalWeight = 0.00; InspectedWeightUnitID = 2;
                                ///* ItemizingUpdateGroup: @AuthorId = 261; @AuthorOfficeId = 1; @CurrentOfficeId = 1; @rId =; @GroupOfficeID = 1; @GroupID = 1704310; @VendorOfficeID =; @VendorID =; @Insp*/ectedQuantity = 3; @InspectedTotalWeight = 0.00; @InspectedWeightUnitID = 2;

                                //if (sku == Session["JewellerySKU"].ToString())
                                //{
                                //    SetJewelryMeasureByRequestID(Convert.ToInt32(txtRequest.Text.Trim()));
                                //}
                            }
                            lblCreateRequestMessege.Text += ", " + batches.Length.ToString() + " batches for order " + objNewOrderModelExt.OrderNumber.Trim();
                        }
                    }
                }
                else if (Convert.ToInt32(totalQty) > 2475)
                    PopupInfoDialog("Error creating batches: the number will exceed 99!", true);
                if (saveShipInfoBox.Checked)
                {
                    if (txtScanPackageBarcodeShipReceiving.Text != "")
                        SaveShippingInfo(null, null);
                }
                else
                {
                    hdnCourierInfo.Value = "";
                    hdnTrackingInfo.Value = "";
                }
                TakeInDefaultFields();
                lblOrderNumbeTakeIn.Text = "Last Order# \n\r" + Environment.NewLine + objNewOrderModelExt.OrderNumber;
                hdnOrderCode.Value = objNewOrderModelExt.OrderNumber;
            }
            if (errMsg == "" || (errMsg.ToLower().Contains("no sku info") && result != null))
            {
                string rId = txtRequest.Text;
                if (txtRequest.Text == "" && hdnRequestID.Value != "")
                {
                    rId = hdnRequestID.Value;
                }
                //string mailAddress = SyntheticScreeningUtils.GetContactEmail(hdnContactId.Value, this);
                btnPrintReceiptTakeIn.Enabled = true;
                btnPrintLabelTakeIn.Enabled = true;
                string nOfCopies = nbrCopiesBox.Text;
                if (nOfCopies == "")
                    nOfCopies = "1";
                PrintPDFReceipt1("both", "create", nOfCopies);
                //alex 0310
                //string mailAddr = SyntheticScreeningUtils.GetContactEmail(hdnContactId.Value, this);
                //string sent = sendEmailToContact(mailAddr, rId, objNewOrderModelExt.OrderNumber, objNewOrderModelExt.Memo, totalQty, this);
                //alex 0310
                //if (sent != "OK")
                //{
                //    for (int i=0; i<=3; i++)
                //    {
                //        sent = sendEmailToContact(mailAddr, rId, objNewOrderModelExt.OrderNumber, objNewOrderModelExt.Memo, totalQty, this);
                //        if (sent == "OK")
                //            break;
                //    }
                //}
            }
            
            txtRequest.Focus();
            return errMsg;
        }

        private string CreateBatchItem(NewOrderModelExt objNewOrderModelExt, string sku)
        {
            var orderModel = QueryUtils.GetOrderInfo(objNewOrderModelExt.OrderNumber.Trim(), this);
            if (orderModel == null)
                return "OrderModel error";
            var memos = QueryUtils.GetOrderMemos(orderModel, this);
            if (memos == null)
                return "No memo found";
            ItemizedModel batch = new ItemizedModel();
            batch.Items = objNewOrderModelExt.NumberOfItems.ToString();
            batch.Memo = objNewOrderModelExt.Memo;
            batch.Sku = sku;
            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            if (cpList == null)
                return "No sku list found";
            var cpModel = cpList.Find(m => m.CustomerProgramName == sku);
            if (cpModel == null)
                return "No sku info found for " + sku + ", No batch/items created";
            var memoModel = memos.Find(m => m.MemoNumber == objNewOrderModelExt.Memo);
            if (memoModel == null) 
                return "No memo info found";
            var model = new ItemizeGoModel
            {
                Cp = cpModel,
                Memo = memoModel,
                //NumberOfItems = GetNumberItems(),
                NumberOfItems = Convert.ToInt16(batch.Items),
                Order = orderModel,
                LotNumbers = new List<string>()
            };
            List<BatchNewModel> fullBatchList = new List<BatchNewModel>();
            var newBatches = QueryUtils.ItemizeAddItemsByCount(model, this, "");
            if (newBatches == null || newBatches.Count == 0)
                return "No batches created";
            string result = "success: ";
            foreach (var newBatch in newBatches)
            {
                fullBatchList.Add(newBatch);
                result += newBatch.BatchId + ", ";
            }
            result = result.Substring(0, result.Length - 2);
            return result;
        }
        private string SendBatchItemToAzure(NewOrderModelExt objNewOrderModelExt, string id, string officeId, string sku)
        {
            DataTable dataInputs = new DataTable("RemoteItemizing");
            dataInputs.Columns.Add("OrderNumber", typeof(String));
            dataInputs.Columns.Add("NumberOfItems", typeof(String));
            dataInputs.Columns.Add("Memo", typeof(String));
            dataInputs.Columns.Add("Sku", typeof(String));
            dataInputs.Columns.Add("ID", typeof(String));
            dataInputs.Columns.Add("OfficeID", typeof(String));
            DataRow row;
            row = dataInputs.NewRow();
            row[0] = objNewOrderModelExt.OrderNumber.Trim();
            row[1] = objNewOrderModelExt.NumberOfItems.ToString();
            row[2] = objNewOrderModelExt.Memo;
            row[3] = sku;
            row[4] = id;
            row[5] = officeId;
            dataInputs.Rows.Add(row);
            bool sent = false;
            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                sent = QueryUtils.SendMessageToStorage(ms, this, "RemoteItemizing");

            }
            dataInputs.Clear();
            if (sent)
                return "success";
            else
                return "failed";

            var orderModel = QueryUtils.GetOrderInfo(objNewOrderModelExt.OrderNumber.Trim(), this);
            var memos = QueryUtils.GetOrderMemos(orderModel, this);
            if (memos == null)
                return "No memo found";
            ItemizedModel batch = new ItemizedModel();
            batch.Items = objNewOrderModelExt.NumberOfItems.ToString();
            batch.Memo = objNewOrderModelExt.Memo;
            batch.Sku = sku;
            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            if (cpList == null)
                return "No sku list found";
            var cpModel = cpList.Find(m => m.CustomerProgramName == sku);
            if (cpModel == null)
                return "No sku info found for " + sku + ", No batch/items created";
            var memoModel = memos.Find(m => m.MemoNumber == objNewOrderModelExt.Memo);
            if (memoModel == null)
                return "No memo info found";
            var model = new ItemizeGoModel
            {
                Cp = cpModel,
                Memo = memoModel,
                //NumberOfItems = GetNumberItems(),
                NumberOfItems = Convert.ToInt16(batch.Items),
                Order = orderModel,
                LotNumbers = new List<string>()
            };
            List<BatchNewModel> fullBatchList = new List<BatchNewModel>();
            var newBatches = QueryUtils.ItemizeAddItemsByCount(model, this, "");
            if (newBatches.Count == 0)
                return "No batches created";
            string result = "success: ";
            foreach (var newBatch in newBatches)
            {
                fullBatchList.Add(newBatch);
                result += newBatch.BatchId + ", ";
            }
            result = result.Substring(0, result.Length - 2);
            return result;
        }
        private void LoadCheckBoxList(string values, CheckBoxList control)
        {
            foreach (ListItem item in control.Items)
            {
                item.Selected = false;
            }
            if (string.IsNullOrEmpty(values)) return;
            var keys = values.Split(',');
            foreach (ListItem item in control.Items)
            {

                foreach (var key in keys)
                {
                    if (key == item.Value) item.Selected = true;
                }
            }
        }

        private string GetCheckBoxListValue(CheckBoxList control)
        {
            var value = "";
            foreach (ListItem item in control.Items)
            {
                if (item.Selected) value += item.Value + ",";
            }
            return value;
        }
		public string SetJewelryMeasureByRequestID(int requestID)
		{
			string msg = string.Empty;
			DataSet dsJewelry = FrontUtils.GetJewelryItemDetail(requestID, this);
			var result = new List<MeasureValueModel>();

			int batchId = 0;
			int measureID = 0;
			string measureValue = "";
			int measureValueId = 0;

			string itemCode = string.Empty;
			var enumMeasureList = QueryUtils.GetEnumMeasure(this);
			int itemIndex = 1;
			if (dsJewelry.Tables[0].Rows.Count > 0)
			{
				foreach (DataRow drItem in dsJewelry.Tables[0].Rows)
				{
					batchId = Convert.ToInt32(drItem["BatchId"].ToString());
					itemCode = drItem["ItemCode"].ToString();
					int partID = 0;
					//Item Measures
					DataTable dtItemNo = dsJewelry.Tables[1].Select("ItemNo=" + itemIndex).CopyToDataTable();
					foreach (DataRow drJewelry in dtItemNo.Rows)
					{
						partID = Convert.ToInt32(drJewelry["PartID"].ToString());
						measureID = Convert.ToInt32(drJewelry["measureID"].ToString());
						measureValue = drJewelry["measureValue"].ToString();

						List<EnumMeasureModel> lstItemName = enumMeasureList.FindAll(m => m.MeasureValueMeasureId == measureID);
						List<EnumMeasureModel> selectedItemName;
						if (measureID == 69 || measureID == 32 || measureID == 204)
							selectedItemName = lstItemName.FindAll(m => m.ValueTitle == measureValue).ToList();
						else
							selectedItemName = lstItemName.FindAll(m => m.MeasureValueName == measureValue).ToList();

						if (selectedItemName.Count > 0)
						{
							measureValueId = selectedItemName[0].MeasureValueId;
							result.AddRange(AddMeasureValue(partID, measureID.ToString(), measureValueId, measureValue, batchId, itemCode));
							measureValueId = 0;
						}
						else
						{
							result.AddRange(AddMeasureValue(partID, measureID.ToString(), measureValueId, measureValue, batchId, itemCode));
						}
					}
					itemIndex++;
				}
			}
			QueryUtils.SaveNewMeasures(result, this);
			FrontUtils.DelJewelryItemDetail(requestID, this);
			return msg;
		}

		public List<MeasureValueModel> AddMeasureValue(int partId, string measureId, int measureValueId, string val, int NewBatchId, string NewItemCode)
		{
			var objMeasureValueModel = new List<MeasureValueModel>();
			float f = 0;
			if (measureValueId > 0)
			{
				objMeasureValueModel.Add(new MeasureValueModel
				{
					PartId = partId,
					MeasureId = measureId,
					MeasureValueId = "" + measureValueId,
					MeasureValue = null,
					StringValue = null,
					BatchId = NewBatchId,
					ItemCode = NewItemCode,
					MeasureClass = MeasureModel.MeasureClassEnum
				});
			}
			else if (float.TryParse(val, out f) == true && !measureId.Contains("122"))
			{
				objMeasureValueModel.Add(new MeasureValueModel
				{
					PartId = partId,
					MeasureId = measureId,
					MeasureValueId = null,
					MeasureValue = "" + val,
					StringValue = null,
					BatchId = NewBatchId,
					ItemCode = NewItemCode,
					MeasureClass = MeasureModel.MeasureClassNumeric
				});
			}
			else if (val != "")
			{
				objMeasureValueModel.Add(new MeasureValueModel
				{
					PartId = partId,
					MeasureId = measureId,
					MeasureValueId = null,
					MeasureValue = null,
					StringValue = val,
					BatchId = NewBatchId,
					ItemCode = NewItemCode,
					MeasureClass = MeasureModel.MeasureClassText
				});
			}
			return objMeasureValueModel;
		}

        public void SetOrderOffice(int groupID)
        {
            int realOfficeID = 0;
            int realUserID = 0;
            string IPAddress = string.Empty;
            if (Session["RealOfficeID"] != null)
                realOfficeID = Convert.ToInt16(Session["RealOfficeID"].ToString());
            if (Session["ID"] != null)
                realUserID = Convert.ToInt16(Session["ID"].ToString());
            if (Session["IPAddress"] != null)
                IPAddress = Session["IPAddress"].ToString();

            FrontUtils.SetOrderOffice(groupID, realOfficeID, realUserID, IPAddress, this);
        }


        #endregion

        #region Give Out (Tab-2)
        protected void btnSubmitGiveOut_Click(object sender, EventArgs e)
        {
            if (hdnMessengerLookupGiveOut.Value == "0" && chkTakenOutByOurMessengerGiveOut.Checked == false && hdnCarriersShipOut.Value == "0")
            {
                PopupInfoDialog("Please select Messenger or Carrier or Taken-Out-By-Our-Messenger", false);
                return;
            }
            
            SaveQPopupExtender.Show();
        }

        public string SubmitGiveOut()
        {
            int isOrderValidToGiveOut = 0;
            string errMsg = "";
            lnkView.Visible = false;
            DataSet ds = SyntheticScreeningUtils.IsSyntheticOrderFinishedPacking(Convert.ToInt32(txtItemNoGiveOut.Text.Trim()), this);
            isOrderValidToGiveOut = Convert.ToInt16(ds.Tables[0].Rows[0]["IsFinishedPacking"].ToString());
            if (isOrderValidToGiveOut == 0)
            {
                PopupInfoDialog("Unable to do GiveOut.\nPacking is pending for this order.", false);
                return "";
            }

            DataSet dsOrderDet = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtItemNoGiveOut.Text.Trim()), this);

            if (dsOrderDet.Tables[1].Rows.Count != 0)
            {
                bool doItemization = false;
                bool isItemized = false;
                foreach (DataRow dr in dsOrderDet.Tables[1].Rows)
                {
                    if (Convert.ToInt32(dr["TesterFailItems"].ToString()) != 0)
                    {
                        doItemization = true;
                    }
                }
                if (doItemization == true)
                {
                   
                    foreach (DataRow dr in dsOrderDet.Tables[2].Rows)
                    {
                        if (Convert.ToInt32(dr["StatusID"].ToString()) == (int)SynhteticOrderStatus.Itemized)
                        {
                            isItemized = true;
                        }
                    }
                }

                if (doItemization == true && isItemized == false)
                { 
                    PopupInfoDialog("Unable to do GiveOut.\nItemization is pending for this order.", false);
                    return "";
                }
            }

            DataSet otGiveOut = GetViewState("otGiveOut") as DataSet;
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

            objNewOrderModelExt.GroupOfficeID = 0;
            objNewOrderModelExt.GroupID = 0;
            objNewOrderModelExt.VendorOfficeID = 0;
            objNewOrderModelExt.VendorID = 0;
            objNewOrderModelExt.ShipmentCharge = 0;
            objNewOrderModelExt.CarrierTrackingNumber = "";
            objNewOrderModelExt.CarrierID = 0;
            objNewOrderModelExt.PersonID = 0;

            if (hdnCarriersShipOut.Value != "0")
            { 
                objNewOrderModelExt.GroupOfficeID = otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[0].ToString());
                objNewOrderModelExt.GroupID = otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[1].ToString());
                objNewOrderModelExt.VendorOfficeID = otGiveOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString());
                objNewOrderModelExt.VendorID = hfVendorShipOut.Value == "0" ? 0 : Convert.ToInt32(hfVendorShipOut.Value);
                //  objNewOrderModelExt.ShipmentCharge = txtAccountNumberGiveOut.Text.ToString().Trim() == "" ? 0 : Convert.ToInt32(txtAccountNumberGiveOut.Text.ToString().Trim());
                objNewOrderModelExt.ShipmentCharge = 0;
                objNewOrderModelExt.CarrierTrackingNumber = txtBarcodeShipOut.Text.ToString().Trim() == "" ? "" : txtBarcodeShipOut.Text.ToString().Trim();
                objNewOrderModelExt.CarrierID = hdnCarriersShipOut.Value == "0" ? 0 : Convert.ToInt32(hdnCarriersShipOut.Value.ToString().Trim());
                var result_GroupVendor = FrontUtils.SetGroupVendor(objNewOrderModelExt, this, out errMsg); ////spSetGroupVendor
            }

            if (hdnMessengerLookupGiveOut.Value != "0")
            {
                objNewOrderModelExt.PersonID = hdnMessengerLookupGiveOut.Value == "0" ? 0 : Convert.ToInt32(hdnMessengerLookupGiveOut.Value);
            }

            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            string customerId = hfCustomerGiveOut.Value.ToString();
            objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);
            objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
            objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).OfficeId);

            objNewOrderModelExt.TakenOutByOurMessenger = chkTakenOutByOurMessengerGiveOut.Checked == true ? true : false;
            objNewOrderModelExt.GroupCode = Convert.ToInt32(txtItemNoGiveOut.Text);

            DataTable dtTemp = new DataTable();
            dtTemp = otGiveOut.Tables["tblItem"].Clone();

            dtotGiveOutVal = new DataTable();
            dtotGiveOutVal = otGiveOut.Tables["tblItem"].Clone();
            dtTemp = GetCheckedNodes(trvOrderTreeGiveOut.Nodes, otGiveOut.Tables["tblItem"]);

            DataTable dtItemOut = FrontUtils.GetItemOutStruct(this);

            //This code for give out order and empty batch
            if (dtTemp.Rows.Count == 0)
            {
                //PopupInfoDialog("Please select atleast one item.", false);
                SyntheticOrderHistoryModel objSyntheticOrderHistory = new SyntheticOrderHistoryModel();
                objSyntheticOrderHistory.OrderCode = Convert.ToInt32(txtItemNoGiveOut.Text.Trim());
                objSyntheticOrderHistory.BatchCode = 0;
                objSyntheticOrderHistory.ItemQty = 0;
                objSyntheticOrderHistory.AssignedTo = 0;
                objSyntheticOrderHistory.StatusId = (int) SynhteticOrderStatus.OrderComplete;
                SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrderHistory, this);
                SyntheticScreeningUtils.SetSyntheticStatus(objSyntheticOrderHistory, this);
                SyntheticScreeningUtils.SetSyntheticShippingAccountNumber(Convert.ToInt32(txtItemNoGiveOut.Text.Trim()), txtAccountNumberGiveOut.Text, this);

                //Procedure dbo.spSetCloseGroupStateByCode and loop with procedure dbo.spSetCloseBatchStateByCode and dbo.spSetCloseItemStateByCode
                var resultCloseGroup = FrontUtils.SetCloseGroupStateByCode(objNewOrderModelExt, this, out errMsg);

                //Procedure dbo.spSetCloseOrderStateByCode and for close detail to tblOrderOut Close table 
                var resultSetorder = FrontUtils.SetOrderOut(objNewOrderModelExt, this, out errMsg);

                DataSet dsTemp = otGiveOut;
                dsTemp.Tables["tblItem"].Clear();
                dsTemp.Tables["tblItem"].Merge(dtTemp);

                if (dsTemp.Tables["tblBatch"].Rows.Count > 0)
                {
                    FrontUtils.SetCloseBatchStateByCode(objNewOrderModelExt, this, out errMsg);
                }

                ClearGiveOut();
                txtItemNoGiveOut.Text = "";
                
                if (errMsg == "")
                {
                    lblMsgGiveOut.Text = "Data was successfully added";
                    lblMsgGiveOut.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    lblMsgGiveOut.Text = "Error in GiveOut";
                    lblMsgGiveOut.ForeColor = System.Drawing.Color.Red;
                }

                return errMsg;
            }

            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow drItem = dtItemOut.NewRow();
                drItem["BatchID_ItemCode"] = row["BatchID_ItemCode"];
                dtItemOut.Rows.Add(drItem);
            }

            //Procedure dbo.spSetCloseGroupStateByCode and loop with procedure dbo.spSetCloseBatchStateByCode and dbo.spSetCloseItemStateByCode
            var result_setclosegroup = FrontUtils.SetCloseGroupStateByCode(objNewOrderModelExt, this, out errMsg);

            //Procedure dbo.spSetCloseOrderStateByCode and for close detail to tblOrderOut Close table 
            var result_setcloseorder = FrontUtils.SetOrderOut(objNewOrderModelExt, this, out errMsg);

            //	#region BatchTracking
            DataSet dsTemp1 = otGiveOut;
            dsTemp1.Tables["tblItem"].Clear();
            dsTemp1.Tables["tblItem"].Merge(dtTemp);

            if (dsTemp1.Tables["tblBatch"].Rows.Count > 0)
            {
                foreach (DataRow dr in dsTemp1.Tables["tblBatch"].Rows)
                {
                    DataRow[] drSet = dsTemp1.Tables["tblItem"].Select("NewBatchID = '" + dr["BatchID"].ToString() + "'");

                    if (drSet.Length > 0 && drSet.Length > 0)
                    {
                        DataTable dtitem = drSet.CopyToDataTable();

                        int BatchID = Convert.ToInt32(dr["BatchID"]);
                        int EventID = FrontUtils.BatchEvents.TakeOut;
                        int ItemsAffected = drSet.Length;
                        int ItemsInBatch = Convert.ToInt32(dr["ItemsQuantity"]);
                        int FormID = FrontUtils.Codes.AccRep;
                        FrontUtils.SetBatchEvent(EventID, BatchID, FormID, ItemsAffected, ItemsInBatch, this);//Procedure dbo.spSetBatchEvents

                        foreach (DataRow dritem in dtitem.Rows)
                        {
                            objNewOrderModelExt.ItemCode = Convert.ToInt32(dritem["ItemCode"]);// dsTemp1.Tables["tblItem"].Rows[0]["ItemCode"]
                            objNewOrderModelExt.BatchCode = Convert.ToInt32(dritem["BatchCode"].ToString());//Convert.ToInt32(dsTemp1.Tables["tblItem"].Rows[0]["BatchCode"].ToString());
                            FrontUtils.SetCloseItemStateByCode(objNewOrderModelExt, this, out errMsg); //spSetCloseItemStateByCode

                            objNewOrderModelExt.BatchID = Convert.ToInt32(dr["BatchID"]);

                            if (chkTakenOutByOurMessengerGiveOut.Checked == true)
                            {
                                FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
                                FrontUtils.SetItemOutByOurMessenger(objNewOrderModelExt, this, out errMsg);
                            }
                            else
                            {
                                FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
                            }
                        }
                    }
                    FrontUtils.SetCloseBatchStateByCode(objNewOrderModelExt, this, out errMsg);
                }
            }

            SyntheticOrderHistoryModel objSyntheticOrderHistoryModel = new SyntheticOrderHistoryModel();
            objSyntheticOrderHistoryModel.OrderCode = objNewOrderModelExt.GroupCode;
            objSyntheticOrderHistoryModel.BatchCode = 0;
            objSyntheticOrderHistoryModel.ItemQty = 0;
            objSyntheticOrderHistoryModel.AssignedTo = 0;
            objSyntheticOrderHistoryModel.StatusId = (int)SynhteticOrderStatus.OrderComplete;
            SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrderHistoryModel, this);
            SyntheticScreeningUtils.SetSyntheticStatus(objSyntheticOrderHistoryModel, this);
            ClearGiveOut();
            txtItemNoGiveOut.Text = "";
            if (errMsg == "")
            {
                lblMsgGiveOut.Text = "Data was successfully added";
                lblMsgGiveOut.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                lblMsgGiveOut.Text = "Error in GiveOut";
                lblMsgGiveOut.ForeColor = System.Drawing.Color.Red;
            }
            return errMsg;

        }

        public void GiveOutControlsEnableDisable(bool Enabled)
        {
            btnSubmitGiveOut.Enabled = Enabled;
            txtBarcodeShipOut.Enabled = Enabled;
            txtAccountNumberGiveOut.Enabled = Enabled;
            btnSetDepartureSetting.Enabled = Enabled;
            btnCarriersShipOut.Enabled = Enabled;

            btnMessengerLookupGiveOut.Enabled = !Enabled;

            chkTakenOutByOurMessengerGiveOut.Checked = Enabled;
        }


        protected void btnClearGiveOut_Click(object sender, EventArgs e)
        {
            GiveOutControlsEnableDisable(false);
            txtItemNoGiveOut.Text = "";
            ClearGiveOut();
        }

        public void ClearGiveOut()
        {
            GiveOutControlsEnableDisable(false);
            txtCustomerGiveOut.Text = "";
            hfCustomerGiveOut.Value = "0";

            txtMessengerLookupGiveOut.Text = "";
            hdnMessengerLookupGiveOut.Value = "0";
            rblMessengerLookupGiveOut.Items.Clear();
            rblMessengerLookupGiveOut.DataSource = null;
            rblMessengerLookupGiveOut.DataBind();
            imgStoredSignGiveOut.ImageUrl = imgTransperant;
            imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
            trvOrderTreeGiveOut.Nodes.Clear();
            btnSubmitGiveOut.Enabled = false;
            chkTakenOutByOurMessengerGiveOut.Checked = false;
            lblMsgGiveOut.Text = "";

            txtCarriersShipOut.Text = "";
            hdnCarriersShipOut.Value = "0";
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";

            rdlCarriersShipOut.ClearSelection();
            rblMessengerLookupGiveOut.ClearSelection();
            lblVendorShipOut.Text = "";
            hfVendorShipOut.Value = "0";

            txtBarcodeShipOut.Enabled = false;
            txtAccountNumberGiveOut.Enabled = false;
            btnSetDepartureSetting.Enabled = false;
            btnCarriersShipOut.Enabled = false;
            rdbGiveOut.Checked = true;
            btnMessengerLookupGiveOut.Enabled = true;

        }

        protected void btnItemGiveOut_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtItemNoGiveOut.Text.Trim().Length > 4)
                {
                    ClearGiveOut();
                    lblMsgGiveOut.Text = "";
                    string[] sCodesArray = txtItemNoGiveOut.Text.Split('.');
                    DataSet dsOrders = new DataSet();

                    int GroupCode = Convert.ToInt32(sCodesArray[0]);
                    NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
                    objNewOrderModelExt.GroupCode = GroupCode;
                    objNewOrderModelExt.BatchCode = 0;
                    objNewOrderModelExt.ItemCode = 0;
                    objNewOrderModelExt.OperationChar = "";

                    //Procedures spGetGroupByCode, spGetBatchByCode, spGetItemByCode, spGetItemDocByCode
                    DataTable tblOrder = FrontUtils.GetGroupByCode(objNewOrderModelExt, this);
                    DataTable tblBatch = FrontUtils.GetBatchbyCode(objNewOrderModelExt, this);
                    DataTable tblItem = FrontUtils.GetItembyCode(objNewOrderModelExt, this);
                    DataTable tblDoc = FrontUtils.GetItemDocByCode(objNewOrderModelExt, this);
                    tblOrder.TableName = "tblOrder";
                    tblBatch.TableName = "tblBatch";
                    tblItem.TableName = "tblItem";
                    tblDoc.TableName = "tblDoc";
                    dsOrders.Tables.Add(tblOrder.Copy());
                    dsOrders.Tables.Add(tblBatch.Copy());
                    dsOrders.Tables.Add(tblItem.Copy());
                    dsOrders.Tables.Add(tblDoc.Copy());

                    SetViewState(dsOrders, "otGiveOut");
                    if (dsOrders.Tables[0].Rows.Count == 0)
                    {
                        lblMsgGiveOut.Text = "Order # " + sCodesArray[0] + " doesn't exists";
                        lblMsgGiveOut.ForeColor = System.Drawing.Color.Red;
                        GiveOutControlsEnableDisable(false);
                        return;
                    }
                    GiveOutControlsEnableDisable(false);
                    BatchModel batchModel = new BatchModel();
                    batchModel.CustomerId = dsOrders.Tables[0].Rows[0]["CustomerID"].ToString();
                    batchModel.CustomerOfficeId = dsOrders.Tables[0].Rows[0]["CustomerOfficeID"].ToString();

                    var dtCustomer = QueryUtils.GetCustomerName(batchModel, this); //Procedure spGetCustomer
                    txtCustomerGiveOut.Text = dtCustomer.CustomerName;
                    hfCustomerGiveOut.Value = dtCustomer.CustomerId;

                    var customerModel = GetCustomerFromView(batchModel.CustomerId);
                    var Persons = QueryCustomerUtils.GetPersonsByCustomer(customerModel, Page);
                    SetViewState(Persons, SessionConstants.PersonEditData);
                    rblMessengerLookupGiveOut.DataSource = Persons;
                    rblMessengerLookupGiveOut.DataBind();

                    //Load Order Tree
                    LoadItemNumbersTree1(txtItemNoGiveOut.Text.ToString().Trim(), trvOrderTreeGiveOut);
                    if (tblItem.Rows.Count == 0)
                    {
                        btnSubmitGiveOut.Enabled = false;
                    }
                    hdnOrderCode.Value = txtItemNoGiveOut.Text.ToString().Trim();

                    //Check All Tree Nodes
                    CheckAllNodes(trvOrderTreeGiveOut.Nodes);
                    trvOrderTreeGiveOut.Enabled = false;

                    if (dsOrders.Tables[0].Rows[0]["StateName"].ToString() == "closed")
                    {
                        btnSubmitGiveOut.Enabled = false;
                        lblMsgGiveOut.Text = "Giveout for this order already done!";
                        lblMsgGiveOut.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        btnSubmitGiveOut.Enabled = true;
                        lblMsgGiveOut.Text = "";
                        lblMsgGiveOut.ForeColor = System.Drawing.Color.Blue;
                    }
                }
            }
            catch (Exception exc)
            {
                PopupInfoDialog("Data wasn't Loaded. Error: " + exc.Message, true);
            }
        }

        public void CheckAllNodes(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                node.Checked = true;
                CheckChildren(node, true);
            }
        }

        private void CheckChildren(TreeNode rootNode, bool isChecked)
        {
            foreach (TreeNode node in rootNode.ChildNodes)
            {
                CheckChildren(node, isChecked);
                node.Checked = isChecked;
            }
        }

        protected void rblMessengerLookupGiveOut_Changed(object sender, EventArgs e)
        {
            txtMessengerLookupGiveOut.Text = rblMessengerLookupGiveOut.SelectedItem.Text;
            hdnMessengerLookupGiveOut.Value = rblMessengerLookupGiveOut.SelectedItem.Value;
            var Person = GetPersonFromView(Convert.ToInt32(rblMessengerLookupGiveOut.SelectedItem.Value));
            imgStoredSignGiveOut.ImageUrl = string.IsNullOrEmpty(Person.Path2Signature) ? imgTransperant : Person.Path2Signature;
            imgStoredPhotoGiveOut.ImageUrl = string.IsNullOrEmpty(Person.Path2Photo) ? imgTransperant : Person.Path2Photo;

            txtCarriersShipOut.Text = "";
            hdnCarriersShipOut.Value = "0";
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";
            lblVendorShipOut.Text = "";
            rdlCarriersShipOut.ClearSelection();

            chkTakenOutByOurMessengerGiveOut.Checked = false;
        }

        protected void rdlCarriersShipOut_Changed(object sender, EventArgs e)
        {
            txtCarriersShipOut.Text = rdlCarriersShipOut.SelectedItem.Text;
            hdnCarriersShipOut.Value = rdlCarriersShipOut.SelectedItem.Value;
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";
            lblVendorShipOut.Text = "";

            txtMessengerLookupGiveOut.Text = "";
            hdnMessengerLookupGiveOut.Value = "0";
            imgStoredSignGiveOut.ImageUrl = imgTransperant;
            imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
            rblMessengerLookupGiveOut.ClearSelection();

            chkTakenOutByOurMessengerGiveOut.Checked = false;
        }

        protected void chkTakenOutByOurMessengerGiveOut_CheckedChanged(object sender, EventArgs e)
        {
            txtMessengerLookupGiveOut.Text = "";
            hdnMessengerLookupGiveOut.Value = "0";
            imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
            imgStoredSignGiveOut.ImageUrl = imgTransperant;
            rblMessengerLookupGiveOut.ClearSelection();

            txtCarriersShipOut.Text = "";
            hdnCarriersShipOut.Value = "0";
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";
            lblVendorShipOut.Text = "";
            rdlCarriersShipOut.ClearSelection();

            txtBarcodeShipOut.Enabled = false;
            txtAccountNumberGiveOut.Enabled = false;

            btnSetDepartureSetting.Enabled = false;
            btnCarriersShipOut.Enabled = false;
            btnMessengerLookupGiveOut.Enabled = false;
            rdbGiveOut.Checked = false;
            rdbShipOut.Checked = false;
        }

        protected void rdbGiveOut_CheckedChanged(object sender, EventArgs e)
        {
            txtBarcodeShipOut.Enabled = false;
            txtAccountNumberGiveOut.Enabled = false;
            btnSetDepartureSetting.Enabled = false;
            btnCarriersShipOut.Enabled = false;

            btnMessengerLookupGiveOut.Enabled = true;

            chkTakenOutByOurMessengerGiveOut.Checked = false;

            txtMessengerLookupGiveOut.Text = "";
            hdnMessengerLookupGiveOut.Value = "0";
            imgStoredSignGiveOut.ImageUrl = imgTransperant;
            imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
            lblMsgGiveOut.Text = "";

            txtCarriersShipOut.Text = "";
            hdnCarriersShipOut.Value = "0";
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";

            rdlCarriersShipOut.ClearSelection();
            rblMessengerLookupGiveOut.ClearSelection();

            lblVendorShipOut.Text = "";
            hfVendorShipOut.Value = "0";

        }

        protected void rdbShipOut_CheckedChanged(object sender, EventArgs e)
        {
            txtBarcodeShipOut.Enabled = true;
            txtAccountNumberGiveOut.Enabled = true;
            btnSetDepartureSetting.Enabled = true;
            btnCarriersShipOut.Enabled = true;

            btnMessengerLookupGiveOut.Enabled = false;

            chkTakenOutByOurMessengerGiveOut.Checked = false;

            txtMessengerLookupGiveOut.Text = "";
            hdnMessengerLookupGiveOut.Value = "0";
            imgStoredSignGiveOut.ImageUrl = imgTransperant;
            imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
            lblMsgGiveOut.Text = "";

            txtCarriersShipOut.Text = "";
            hdnCarriersShipOut.Value = "0";
            txtBarcodeShipOut.Text = "";
            txtAccountNumberGiveOut.Text = "";

            rdlCarriersShipOut.ClearSelection();
            rblMessengerLookupGiveOut.ClearSelection();

            lblVendorShipOut.Text = "";
            hfVendorShipOut.Value = "0";
        }

        public DataTable GetCheckedNodes(TreeNodeCollection nodes, DataTable dt)
        {
            foreach (TreeNode aNode in nodes)
            {
                //edit
                if (aNode.Checked)
                {
                    if (aNode.Value.Length >= 18)
                    {
                        string itemNumber = aNode.Value.Substring(5, 13);
                        Utils.DissectItemNumber(itemNumber, out string groupCode, out var batchCode, out var itemCode);
                        dtotGiveOutVal.Merge(dt.Select("GroupCode = " + groupCode + " AND BatchCode = " + batchCode + " AND ItemCode = " + itemCode).CopyToDataTable());
                    }
                }
                if (aNode.ChildNodes.Count != 0)
                    GetCheckedNodes(aNode.ChildNodes, dt);
            }
            return dtotGiveOutVal;
        }

        private void LoadItemNumbersTree1(string OrderCode, TreeView trv)
        {
            var items = QueryItemiznUtils.GetOrderHistory(OrderCode, false, this);
            if (items.Count == 0)
            {
                PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", OrderCode), true);

            }
            SetViewState(items);
            ShowTreeHistory(items, trv);
        }

        private const string OrderHistoryItems = "orderHistoryItems";

        void SetViewState(List<CustomerHistoryModel> items)
        {
            SetViewState(items, OrderHistoryItems);
        }

        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items, TreeView trv)
        {
            TreeUtils.ShowTreeHistory(items, trv);
        }

        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }


        #endregion

        #region View Order (Tab-6)

        public void ClearViewOrder()
		{
			txtViewOrder.Text = "";
			lblViewOrderCustomer.Text = "";
            lblNoGoods.Text = "";
            lblViewOrderVendorName.Text = "";

			lblViewTakeInOrderCarrier.Text = "";
			lblViewTakeInOrderCarrierTrackingNumber.Text = "";
            //hdnTakeOutPhoto.Value = "";
			//lblViewGiveOutOrderCarrier.Text = "";
			//lblViewGiveOutOrderCarrierTrackingNumber.Text = "";

            //lblViewOrderOrderState.Text = "";
            lblViewOrderCreateDate.Text = "";
            lblViewOrderStatus.Text = "";
            lblViewRequestID.Text = "";
            lblViewTakeInSignatureTimeStamp.Text = "";
            //lblViewGiveOutSignatureTimeStamp.Text = "";

            lblViewTakeInRecieptUploadedTimeStamp.Text = "";
            //lblViewGiveOutRecieptUploadedTimeStamp.Text = "";

            lblRecieptTakeIn.Text = "";
            lblRecieptGiveOut.Text = "";
            lblViewOrderSearch.Text = "";

            btnUploadSignedOrderReciept.Enabled = false;
            btnPrintLabelViewOrder.Enabled = false;
            btnPrintReceiptViewOrder.Enabled = false;

            trViewOrderSummary.Visible = false;
            trViewOrderOperation.Visible = false;
            btnUploadSignedOrderReciept.Visible = false;
            btnPrintLabelViewOrder.Visible = false;
            btnUsePhotoTakeOut.Visible = false;
            btnSavePhotoTakeOut.Visible = false;
            btnPrintLabelViewOrder.Visible = false;
            btnPrintReceiptViewOrder.Visible = false;
            btnClearViewOrder.Visible = false;
            btnUsePhotoTakeOut.Enabled = false;
            //btnSavePhotoTakeOut.Enabled = false;
            takeOutLnkView.Visible = false;
            btnCreateTakeOutList.Visible = false;
            btnBulkGiveOut.Visible = false;

        }

		protected void btnClearViewOrder_Click(object sender, EventArgs e)
		{
			ClearViewOrder();
            NmbrOfCopiesBox.Text = "1";
            //imgViewTakeOutOrder.Visible = false;
            btnPrintReceiptTakeOut.Visible = false;
            btnPickupMail.Visible = false;
            btnUsePhotoTakeOut.Visible = false;
            btnSavePhotoTakeOut.Visible = false;
            hdnTakeOutPhoto.Value = "";
            iframePDFViewer.Visible = false;
            txtViewOrder.Focus();
            lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Replace("ORDER CLOSED!", "");
            BulkGiveOutCheckBox.Checked = false;
            BulkGiveOutList.Visible = false;
            BulkGiveOutList.Items.Clear();
            lblBulkGiveOutTotal.Text = "";
            lblBulkGiveOutTotal.Visible = false;
            hdnBulkGiveOutCustomercode.Value = "";
            imgTakeInMessanger.ImageUrl = "";
            imgTakeInSignature.ImageUrl = "";
            imgGiveOutSignature.ImageUrl = "";
            imgGiveOutPhoto.ImageUrl = "";
            txtGiveOutMessengersList.Text = "";
            ddlGiveOutMessengersList.Items.Clear();
            lblTakeInSubmittedBy.Text = "";
            lblViewOrderMemo.Text = "";
            string machineName = "";
            txtGiveOutScanPackageBarCodeShip.Text = "";
            if (Session["MachineName"] != null)
            {
                machineName = Session["MachineName"].ToString();
                lblCreateRequestMessege.Text = machineName;
            }
            else
                lblCreateRequestMessege.Text = "";
            BulkGiveOutCheckBoxNoPrint.Checked = false;
        }

        protected void btnViewOrder_Click(object sender, EventArgs e)
        {
            if (Session["MachineName"] == null)
            {
                PopupInfoDialog("No machine name. Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx", true);
                return;
            }
            if (txtViewOrder.Text == "" && !BulkGiveOutCheckBox.Checked && !BulkGiveOutCheckBoxNoPrint.Checked)
            {
                PopupInfoDialog("Please enter the order", false);
                return;
            }
            lblViewOrderStatus.Text = "";
            string criteria = ddlCriteria.SelectedValue;
            bool takeOut = false;
            string bulkRequestId = "";
            string customerCode = "";
            if (Session["Sent"] != null && Session["Sent"].ToString() == "Y")
            {
                Session["Sent"] = "N";
                return;
            }
            else
                Session["Sent"] = "Y";
            if (txtViewOrder.Text == "" && BulkGiveOutCheckBox.Checked)
            {
                Session["Sent"] = "N";
                PopupInfoDialog("Please enter the order", false);
                return;
            }
            if (txtViewOrder.Text.Substring(0, 1).ToUpper() == "G" && txtViewOrder.Text.Length > 1 && ddlCriteria.SelectedValue.ToLower() == "none")
            {
                if (Session["MachineName"] != null)
                {
                    string machineName = Session["MachineName"].ToString();
                    lblCreateRequestMessege.Text = machineName;
                }
                else
                    lblCreateRequestMessege.Text = "no machine name";
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;
                bulkRequestId = txtViewOrder.Text.ToUpper();
                DataTable dtBulk = FrontUtils.GetBulkGiveOutList(bulkRequestId, this);
                if (dtBulk == null || dtBulk.Rows.Count == 0)//G number does not exist
                {
                    PopupInfoDialog("Group number " + bulkRequestId + " does not exist", false);
                    return;
                }
                BulkGiveOutList.Visible = true;
                BulkGiveOutList.Enabled = false;
                BulkGiveOutList.Items.Clear();
                foreach (DataRow row in dtBulk.Rows)
                {
                    string order = row["GSIOrder"].ToString();
                    if (BulkGiveOutList.Items.Count == 0)//use first item to verify messenger/shipping
                        txtViewOrder.Text = order;
                    BulkGiveOutList.Items.Add(order);
                }
                if (BulkGiveOutList.Items.Count > 0)
                {
                    lblBulkGiveOutTotal.Visible = true;
                    lblBulkGiveOutTotal.Text = BulkGiveOutList.Items.Count.ToString();
                }
                else
                {
                    lblBulkGiveOutTotal.Visible = false;
                    lblBulkGiveOutTotal.Text = "";
                }
                //SendBulkGiveOutEmails(bulkRequestId);
                btnPrintLabelViewOrder.Enabled = true;
                btnClearViewOrder.Visible = true;
                btnClearViewOrder.Enabled = true;
                btnBulkGiveOut.Visible = true;
                btnBulkGiveOut.Enabled = true;
                btnPrintReceiptViewOrder.Visible = false;
                btnCreateTakeOutList.Visible = true;
                btnCreateTakeOutList.Enabled = true;
                Session["Sent"] = "Y";
                //return;
            }
            else if (ddlCriteria.SelectedValue.ToLower() != "none")
            {
                BulkGiveOutCheckBoxNoPrint.Checked = false;
                if (Session["MachineName"] != null)
                {
                    string machineName = Session["MachineName"].ToString();
                    lblCreateRequestMessege.Text = machineName;
                }
                else
                    lblCreateRequestMessege.Text = "no machine name";

                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;
                string searchcriteria = ddlCriteria.SelectedValue;
                DataTable orderDetails = FrontUtils.GetItemsByCriteria(searchcriteria, txtViewOrder.Text, this);
                //if (Session["BulkOldNumber"] != null)
                //{
                //    Session["BulkOldNumber"] = null;
                //    return;
                //}
                //DataTable orderDetails = FrontUtils.GetOrderSummery(txtViewOrder.Text, this);
                if (orderDetails == null || orderDetails.Rows.Count == 0)
                {
                    PopupInfoDialog("Orders for " + txtViewOrder.Text + " were not found", false);
                    return;
                }
                string status = orderDetails.Rows[0]["OrderStatus"].ToString();
                string typeId = orderDetails.Rows[0]["ServiceTypeID"].ToString();
                //if (typeId !="7")
                //{
                //    PopupInfoDialog("This order is not for Screening", false);
                //    Session["Sent"] = "N";
                //    return;
                //}
                //if (status.ToLower() == "closed")
                //{
                //    PopupInfoDialog("ORDER " + txtViewOrder.Text + " IS CLOSED", false);
                //    Session["Sent"] = "N";
                //    return;
                //}
                if (status.ToLower() == "closed" || status.ToLower() == "giveout" || status.ToLower() == "shipout")
                {
                    PopupInfoDialog("ORDER " + txtViewOrder.Text + " IS CLOSED", false);
                    Session["Sent"] = "N";
                    return;
                }
                customerCode = orderDetails.Rows[0]["CustomerCode"].ToString();
                if (hdnBulkGiveOutCustomercode.Value == "")
                    hdnBulkGiveOutCustomercode.Value = customerCode;
                else
                {
                    if (hdnBulkGiveOutCustomercode.Value != customerCode)
                    {
                        PopupInfoDialog("Item's customer code " + customerCode + " is different from previous " + hdnBulkGiveOutCustomercode.Value, true);
                        Session["Sent"] = "N";
                        return;
                    }
                }
                string itemRequestId = orderDetails.Rows[0]["RequestID"].ToString();

                //Session["BulkOldNumber"] = "true";
                BulkGiveOutList.Visible = true;
                lblBulkGiveOutTotal.Visible = true;
                for (int i = 0; i <= BulkGiveOutList.Items.Count - 1; i++)
                {
                    if (BulkGiveOutList.Items[i].Text == txtViewOrder.Text + "-" + itemRequestId)
                    {
                        PopupInfoDialog("Duplicate order " + txtViewOrder.Text, false);
                        Session["Sent"] = "N";
                        return;
                    }
                }
                BulkGiveOutList.Items.Add(txtViewOrder.Text + "-" + itemRequestId);
                if (lblBulkGiveOutTotal.Text == "")
                    lblBulkGiveOutTotal.Text = "1";
                else
                {
                    int oldNumber = Convert.ToInt16(lblBulkGiveOutTotal.Text);
                    lblBulkGiveOutTotal.Text = (oldNumber + 1).ToString();
                }
                txtViewOrder.Text = "";
                txtViewOrder.Focus();
                if (lblBulkGiveOutTotal.Text != "1")
                    return;
                NmbrOfCopiesBox.Text = "1";
                btnPrintReceiptTakeOut.Visible = true;

                btnPrintReceiptTakeOut.Enabled = false;
                //alex
                btnPrintReceiptTakeOut.Enabled = true;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                {
                    btnPickupMail.Visible = true;
                    btnPickupMail.Enabled = true;
                }
                //alex
                imgGiveOutSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                imgGiveOutPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                takeOut = true;
                btnUsePhotoTakeOut.Visible = true;
                btnSavePhotoTakeOut.Visible = true;
                if (hdnTakeOutPhoto.Value != "")
                    btnUsePhotoTakeOut.Enabled = true;
                else
                    btnUsePhotoTakeOut.Enabled = false;
                btnSavePhotoTakeOut.Enabled = false;
                trViewOrderSummary.Visible = true;
                trViewOrderOperation.Visible = true;
                //btnUploadSignedOrderReciept.Visible = true;
                //alex
                btnPrintLabelViewOrder.Visible = false;
                //if (bulkRequestId != "")
                btnPrintReceiptViewOrder.Visible = false;
                //else
                //    btnPrintReceiptViewOrder.Visible = false;
                btnClearViewOrder.Visible = true;
                btnCreateTakeOutList.Visible = true;
                btnCreateTakeOutList.Enabled = true;
                //btnBulkGiveOut.Visible = true;
                //btnBulkGiveOut.Enabled = true;
                GiveOutShipping.Visible = false;
                GiveOutMessenger.Visible = true;
                btnPrintLabelViewOrder.Enabled = false;
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;

                return;
            }
            else if (BulkGiveOutCheckBoxNoPrint.Checked)
            {
                btnBulkGiveOut.Visible = true;
                btnBulkGiveOut.Enabled = true;
            }
            //else if (txtViewTrackingNumber.Text != "")
            //{
            //    if (Session["MachineName"] != null)
            //    {
            //        string machineName = Session["MachineName"].ToString();
            //        lblCreateRequestMessege.Text = machineName;
            //    }
            //    else
            //        lblCreateRequestMessege.Text = "no machine name";
            //    btnPrintReceiptTakeOut.Enabled = false;
            //    bulkRequestId = txtViewOrder.Text.ToUpper();
            //    DataTable dtTrack = FrontUtils.GetTrackingNumberGiveOutList(bulkRequestId, this);
            //    if (dtTrack == null || dtTrack.Rows.Count == 0)//G number does not exist
            //    {
            //        PopupInfoDialog("Group number " + bulkRequestId + " does not exist", false);
            //        return;
            //    }
            //    BulkGiveOutList.Visible = true;
            //    BulkGiveOutList.Enabled = false;
            //    BulkGiveOutList.Items.Clear();
            //    foreach (DataRow row in dtTrack.Rows)
            //    {
            //        string order = row["GSIOrder"].ToString();
            //        if (BulkGiveOutList.Items.Count == 0)//use first item to verify messenger/shipping
            //            txtViewOrder.Text = order;
            //        BulkGiveOutList.Items.Add(order);
            //    }
            //    if (BulkGiveOutList.Items.Count > 0)
            //    {
            //        lblBulkGiveOutTotal.Visible = true;
            //        lblBulkGiveOutTotal.Text = BulkGiveOutList.Items.Count.ToString();
            //    }
            //    else
            //    {
            //        lblBulkGiveOutTotal.Visible = false;
            //        lblBulkGiveOutTotal.Text = "";
            //    }
            //    //SendBulkGiveOutEmails(bulkRequestId);
            //    btnClearViewOrder.Visible = true;
            //    btnClearViewOrder.Enabled = true;
            //    btnBulkGiveOut.Visible = true;
            //    btnBulkGiveOut.Enabled = true;
            //    btnPrintReceiptViewOrder.Visible = false;
            //    btnCreateTakeOutList.Visible = true;
            //    btnCreateTakeOutList.Enabled = true;
            //    Session["Sent"] = "Y";
            //}
            else
            {
                btnBulkGiveOut.Visible = false;
                BulkGiveOutList.Enabled = true;
            }
            BulkGiveOutCheckBox.Visible = true;
            if (!BulkGiveOutCheckBox.Checked)
                hdnBulkGiveOutCustomercode.Value = "";
            if (BulkGiveOutCheckBox.Checked && Session["BulkLoad"] == null)
            {
                BulkGiveOutCheckBoxNoPrint.Visible = true;
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;
                //if (Session["BulkOldNumber"] != null)
                //{
                //    Session["BulkOldNumber"] = null;
                //    return;
                //}
                DataTable orderDetails = FrontUtils.GetOrderSummery(txtViewOrder.Text, this);
                if (orderDetails == null || orderDetails.Rows.Count == 0)
                {
                    PopupInfoDialog("Order " + txtViewOrder.Text + " was not found", false);
                    return;
                }
                string status = orderDetails.Rows[0]["OrderStatus"].ToString();
                string typeId = orderDetails.Rows[0]["ServiceTypeID"].ToString();
                //if (typeId !="7")
                //{
                //    PopupInfoDialog("This order is not for Screening", false);
                //    Session["Sent"] = "N";
                //    return;
                //}
                //if (status.ToLower() == "closed")
                //{
                //    PopupInfoDialog("ORDER " + txtViewOrder.Text + " IS CLOSED", false);
                //    Session["Sent"] = "N";
                //    return;
                //}
                if (status.ToLower() == "closed" || status.ToLower() == "giveout" || status.ToLower() == "shipout")
                {
                    PopupInfoDialog("ORDER " + txtViewOrder.Text + " IS CLOSED", false);
                    Session["Sent"] = "N";
                    return;
                }
                customerCode = orderDetails.Rows[0]["CustomerCode"].ToString();
                if (hdnBulkGiveOutCustomercode.Value == "")
                    hdnBulkGiveOutCustomercode.Value = customerCode;
                else
                {
                    if (hdnBulkGiveOutCustomercode.Value != customerCode)
                    {
                        PopupInfoDialog("Item's customer code " + customerCode + " is different from previous " + hdnBulkGiveOutCustomercode.Value, true);
                        Session["Sent"] = "N";
                        return;
                    }
                }
                string itemRequestId = orderDetails.Rows[0]["RequestID"].ToString();
                
                //Session["BulkOldNumber"] = "true";
                BulkGiveOutList.Visible = true;
                lblBulkGiveOutTotal.Visible = true;
                for (int i=0; i<= BulkGiveOutList.Items.Count-1; i++)
                {
                    if (BulkGiveOutList.Items[i].Text == txtViewOrder.Text + "-" + itemRequestId)
                    {
                        PopupInfoDialog("Duplicate order " + txtViewOrder.Text, false);
                        Session["Sent"] = "N";
                        return;
                    }
                }
                BulkGiveOutList.Items.Add(txtViewOrder.Text + "-" + itemRequestId);
                if (lblBulkGiveOutTotal.Text == "")
                    lblBulkGiveOutTotal.Text = "1";
                else
                {
                    int oldNumber = Convert.ToInt16(lblBulkGiveOutTotal.Text);
                    lblBulkGiveOutTotal.Text = (oldNumber + 1).ToString();
                }
                txtViewOrder.Text = "";
                txtViewOrder.Focus();
                if (lblBulkGiveOutTotal.Text != "1")
                    return;
                NmbrOfCopiesBox.Text = "1";
                btnPrintReceiptTakeOut.Visible = true;

                btnPrintReceiptTakeOut.Enabled = false;
                //alex
                btnPrintReceiptTakeOut.Enabled = true;
                //alex
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                {
                    btnPickupMail.Visible = true;
                    btnPickupMail.Enabled = true;
                }
                imgGiveOutSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                imgGiveOutPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                takeOut = true;
                btnUsePhotoTakeOut.Visible = true;
                btnSavePhotoTakeOut.Visible = true;
                if (hdnTakeOutPhoto.Value != "")
                    btnUsePhotoTakeOut.Enabled = true;
                else
                    btnUsePhotoTakeOut.Enabled = false;
                btnSavePhotoTakeOut.Enabled = false;
                trViewOrderSummary.Visible = true;
                trViewOrderOperation.Visible = true;
                //btnUploadSignedOrderReciept.Visible = true;
                //alex
                btnPrintLabelViewOrder.Visible = false;
                //if (bulkRequestId != "")
                btnPrintReceiptViewOrder.Visible = false;
                //else
                //    btnPrintReceiptViewOrder.Visible = false;
                btnClearViewOrder.Visible = true;
                btnCreateTakeOutList.Visible = true;
                btnCreateTakeOutList.Enabled = true;
                //btnBulkGiveOut.Visible = true;
                //btnBulkGiveOut.Enabled = true;
                GiveOutShipping.Visible = false;
                GiveOutMessenger.Visible = true;
                btnPrintLabelViewOrder.Enabled = false;
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;
                return;
            }//BulkGiveOutCheckBox.Checked
            //else
            //    BulkGiveOutCheckBoxNoPrint.Checked = false;
            //if (BulkGiveOutCheckBox.Checked)
            //{
            //    BulkGiveOutList.Visible = true;
            //    BulkGiveOutList.Enabled = false;
            //    BulkGiveOutList.Items.Add(txtViewOrder.Text);
            //    txtViewOrder.Text = "";
            //    txtViewOrder.Focus();
            //    if (BulkGiveOutList.Items.Count > 1)
            //    {
            //        if (txtGiveOutMessengersList.Text != "")
            //            hdnBulkGiveOutMessenger.Value = txtGiveOutMessengersList.Text;
            //        if (txtGiveOutCarriersShip.Text != "" && txtGiveOutScanPackageBarCodeShip.Text != "")
            //        {
            //            hdnBulkGiveOutCarrier.Value = txtGiveOutCarriersShip.Text;
            //            hdnBulkGiveOutTrackingNumber.Value = txtGiveOutScanPackageBarCodeShip.Text;
            //        }
            //        return;
            //    }
            //}
            //else
            //{
            //    BulkGiveOutList.Visible = false;
            //    BulkGiveOutList.Items.Clear();
            //}
            lnkView.Visible = false;
            takeOutLnkView.Visible = true;
            takeOutLnkView.Enabled = true;
            lstRequests.Visible = false;
            Session["Shipping"] = null;
            if (Session["TakeOutShipping"] != null && Session["TakeOutShipping"].ToString() == "true")
            {
                Session["TakeOutShipping"] = null;
                return;
            }
            //var btn = sender as Button;
            string text = "TAKE OUT";
            takeOut = false;
            string customerName = "";
            if (text == "TAKE OUT")
            {
                if (BulkGiveOutCheckBox.Checked)
                {
                    BulkGiveOutList.Visible = true;
                    lblBulkGiveOutTotal.Visible = true;
                    btnCreateTakeOutList.Visible = true;
                    btnCreateTakeOutList.Enabled = true;
                    btnBulkGiveOut.Visible = true;
                    btnBulkGiveOut.Enabled = true;
                }
                else if (BulkGiveOutCheckBoxNoPrint.Checked)
                {
                    btnBulkGiveOut.Visible = true;
                    btnBulkGiveOut.Enabled = true;
                }
                NmbrOfCopiesBox.Text = "1";
                
                
                btnPrintReceiptTakeOut.Enabled = false;
                //alex
                btnPrintReceiptTakeOut.Enabled = true;
                //alex
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = true;
                imgGiveOutSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                imgGiveOutPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                takeOut = true;
                
                if (hdnTakeOutPhoto.Value != "")
                    btnUsePhotoTakeOut.Enabled = true;
                else
                    btnUsePhotoTakeOut.Enabled = false;
                btnSavePhotoTakeOut.Enabled = false;
            }
            try
			{
				DataTable orderDetails = FrontUtils.GetOrderSummery(txtViewOrder.Text, this);
				if (orderDetails != null)
				{
                    string serviceTypeID = orderDetails.Rows[0]["ServiceTypeID"].ToString();
                    //IvanB 16/02 start
                    //if (serviceTypeID != "7")//not screening
                    //{
                    //    PopupInfoDialog("This order is not for Screening", true);
                    //    Session["Sent"] = "N";
                    //    return;
                    //}
                    //IvanB 16/02 end
                    btnUsePhotoTakeOut.Visible = true;
                    btnSavePhotoTakeOut.Visible = true;
                    btnPrintReceiptTakeOut.Visible = true;
                    if (Session["PickupMail"].ToString() == "0")
                        btnPickupMail.Visible = false;
                    else
                        btnPickupMail.Visible = true;
                    trViewOrderSummary.Visible = true;
					trViewOrderOperation.Visible = true;
					//btnUploadSignedOrderReciept.Visible = true;
                    //alex
					btnPrintLabelViewOrder.Visible = true;
					btnPrintReceiptViewOrder.Visible = false;
					btnClearViewOrder.Visible = true;
                    hdnMessengerTakeIn.Value = orderDetails.Rows[0]["PersonID"].ToString();
                    hdnOrderCode.Value = orderDetails.Rows[0]["OrderCode"].ToString(); //For Print Receipt
                    hdnViewRequestID.Value = orderDetails.Rows[0]["RequestID"].ToString();
                    lblViewRequestID.Text = hdnViewRequestID.Value;
                    lblViewOrderCustomer.Text = orderDetails.Rows[0]["CustomerName"].ToString();
                    lblNoGoods.Text = orderDetails.Rows[0]["IsNoGoods"].ToString();
                    customerName = orderDetails.Rows[0]["CustomerName"].ToString();
                    customerCode = orderDetails.Rows[0]["CustomerCode"].ToString();
                    //lblViewOrderVendorName.Text = orderDetails.Rows[0]["VendorName"].ToString();
                    lblViewOrderVendorName.Text = orderDetails.Rows[0]["CustomerName"].ToString();
                    lblViewOrderNumOfItemsNotInspected.Text = orderDetails.Rows[0]["NotInspectedQuantity"].ToString();
					lblViewOrderTotalWeightNotInspected.Text = orderDetails.Rows[0]["NotInspectedTotalWeight"].ToString();
					lblViewOrderNumOfItemsInspected.Text = orderDetails.Rows[0]["InspectedQuantity"].ToString();
					lblViewOrderTotalWeightInspected.Text = orderDetails.Rows[0]["InspectedTotalWeight"].ToString();
                    //IvanB 16/02 start
                    if (serviceTypeID == "1")
                    {
                        lblViewOrderServiceType.Text = "Laboratory Service";
                    }
                    else
                        lblViewOrderServiceType.Text = orderDetails.Rows[0]["ServiceTypeName"].ToString();
                    //if (serviceTypeID == "7")
                    //{
                    //    lblViewOrderServiceType.Text = orderDetails.Rows[0]["ServiceTypeName"].ToString();
                    //} else if (serviceTypeID == "1")
                    //{
                    //    lblViewOrderServiceType.Text = "Laboratory Service";
                    //}
                    //IvanB 16/02 end
                    
					lblViewOrderMemo.Text = orderDetails.Rows[0]["Memo"].ToString();
                    
                    DateTime dateTime = Convert.ToDateTime(orderDetails.Rows[0]["CreateDate"]);
                    int timeAdd = SyntheticScreeningUtils.GetTimeDiff(this);
                    if (timeAdd == 5)//India
                        lblViewOrderCreateDate.Text = dateTime.ToLocalTime().AddHours(timeAdd).AddMinutes(30).ToString();
                    else
                        lblViewOrderCreateDate.Text = dateTime.ToLocalTime().AddHours(timeAdd).ToString();
                    lblViewOrderStatus.Text = orderDetails.Rows[0]["OrderStatus"].ToString();

                    hdnViewOrder.Value = orderDetails.Rows[0]["OrderCode"].ToString();

                    btnUploadSignedOrderReciept.Enabled = true;
                    btnPrintLabelViewOrder.Enabled = true;
                    btnPrintReceiptViewOrder.Enabled = false;
                    lblViewOrderSearch.Text = "";

                    var PickedUpByOurMessenger = orderDetails.Rows[0]["PickedUpByOurMessenger"].ToString();
                    var TakenOutByOurMessenger = orderDetails.Rows[0]["TakenOutByOurMessenger"].ToString();
                    if (PickedUpByOurMessenger == "1")
                    {
                        lblTakeInPickedUpByOurMessenger.Visible = true;

                        trTakeInImages.Visible = false;
                        trViewTakeInSignatureTimeStamp.Visible = false;
                        trViewTakeInRecieptUploadedTimeStamp.Visible = false;
                        trViewTakeInRecieptDownload.Visible = false;

                        trViewTakeInOrderCarrier.Visible = false;
                        trViewTakeInOrderCarrierTrackingNumber.Visible = false;
                        lblTakeInSubmittedbytext.Text = "Submitted By:";

                    }
                    else if (orderDetails.Rows[0]["CarrierName"].ToString() != "")
                    {
                        lblTakeInPickedUpByOurMessenger.Visible = false;
                        trTakeInImages.Visible = false;
                        trViewTakeInSignatureTimeStamp.Visible = false;
                        trViewTakeInRecieptUploadedTimeStamp.Visible = false;
                        trViewTakeInRecieptDownload.Visible = false;

                        trViewTakeInOrderCarrier.Visible = true;
                        trViewTakeInOrderCarrierTrackingNumber.Visible = true;
                        lblTakeInSubmittedbytext.Text = "Shipped By:";
                    }
                    else
                    {
                        lblTakeInPickedUpByOurMessenger.Visible = false;

                        trTakeInImages.Visible = true;
                        trViewTakeInSignatureTimeStamp.Visible = true;
                        trViewTakeInRecieptUploadedTimeStamp.Visible = true;
                        trViewTakeInRecieptDownload.Visible = true;

                        trViewTakeInOrderCarrier.Visible = false;
                        trViewTakeInOrderCarrierTrackingNumber.Visible = false;
                        lblTakeInSubmittedbytext.Text = "Submitted By:";

                    }

                    if (TakenOutByOurMessenger == "1")
                    {
                        //trGiveOutPickedUpByOurMessenger.Visible = true;

                        //trGiveOutImages.Visible = false;
                        //trViewGiveOutSignatureTimeStamp.Visible = false;
                        //trViewGiveOutRecieptUploadedTimeStamp.Visible = false;
                        //trViewGiveOutRecieptDownload.Visible = false;

                        //trViewGiveOutOrderCarrier.Visible = false;
                        //trViewGiveOutOrderCarrierTrackingNumber.Visible = false;

                        //lblViewGiveOutSubmittedBytext.Text = "Accepted By:";
                    }
                    else if (orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() != "")
                    {
                        GiveOutShipping.Visible = true;
                        GiveOutMessenger.Visible = false;
                        //ddlMessengersList.Text = "";
                        hdnMessengerTakeOut.Value = "0";
                        if (ddlGiveOutShippingCourier.Items.Count == 0)
                            LoadGiveOutSyntheticShippingCourier();
                        ddlGiveOutShippingCourier.SelectedValue = orderDetails.Rows[0]["GiveOutCarrierID"].ToString();
                        hdnGiveOutCarriersShip.Value = orderDetails.Rows[0]["GiveOutCarrierID"].ToString();
                        btnPrintReceiptTakeOut.Enabled = true;
                        if (Session["PickupMail"].ToString() == "0")
                            btnPickupMail.Visible = false;
                        else
                            btnPickupMail.Enabled = true;
                        //if (iframePDFViewer.Visible == false)
                        //    takeOutLnkView.Visible = true;
                        iframePDFViewer.Visible = false;
                        takeOutLnkView.Visible = true;
                        takeOutLnkView.Enabled = true;
                        LoadGiveOutSyntheticShippingCourier();
                        txtGiveOutScanPackageBarCodeShip.Text = orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();

                        SetMessengersList(orderDetails, "takeOut");
                        //trGiveOutPickedUpByOurMessenger.Visible = false;

                        //trGiveOutImages.Visible = false;
                        //trViewGiveOutSignatureTimeStamp.Visible = false;
                        //trViewGiveOutRecieptUploadedTimeStamp.Visible = false;
                        //trViewGiveOutRecieptDownload.Visible = false;

                        //trViewGiveOutOrderCarrier.Visible = true;
                        //trViewGiveOutOrderCarrierTrackingNumber.Visible = true;
                        //lblViewGiveOutSubmittedBytext.Text = "Shipped By:";
                    }
					else
					{
						//trGiveOutPickedUpByOurMessenger.Visible = false;

						//trGiveOutImages.Visible = true;
						//trViewGiveOutSignatureTimeStamp.Visible = true;
						//trViewGiveOutRecieptUploadedTimeStamp.Visible = true;
						//trViewGiveOutRecieptDownload.Visible = true;

						//trViewGiveOutOrderCarrier.Visible = false;
						//trViewGiveOutOrderCarrierTrackingNumber.Visible = false;
						//lblViewGiveOutSubmittedBytext.Text = "Accepted By:";
					}
                    //if (BulkGiveOutCheckBox.Checked)
                    //    BulkGiveOutList.Items.Add(hdnViewRequestID.Value);
					///Carrier For Take In
					lblViewTakeInOrderCarrier.Text = orderDetails.Rows[0]["CarrierName"].ToString();
					lblViewTakeInOrderCarrierTrackingNumber.Text = orderDetails.Rows[0]["CarrierTrackingNumber"].ToString();
					lblTakeInSubmittedBy.Text = orderDetails.Rows[0]["Person"].ToString();
					lblViewTakeInAcceptedBy.Text = orderDetails.Rows[0]["TakeInUserName"].ToString();
                    string id = hdnMessengerTakeIn.Value;
                    bool exist = true, photoExist = true;
                    if (id == "")
                        exist = false;
                    else
                        exist = QueryUtils.GetInitialSignature(id, this);
                    if (id == "")
                        photoExist = false;
                    else
                        photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
                    
                    if (id == "" && orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() == "" && orderDetails.Rows[0]["CarrierTrackingNumber"].ToString() == "")
                    {
                        exist = false;
                        photoExist = false;
                        if (hdnViewRequestID.Value != "")//submitted by customer
                        {
                            PopupInfoDialog("No messengers found for customer. Contact Administrator.", false);
                            //return;
                        }
                        else//not submitted by customer
                        {
                            if (lblViewOrderStatus.Text.ToLower() == "closed")
                            {
                                if (!lblCreateRequestMessege.Text.Contains("ORDER CLOSED"))
                                    lblCreateRequestMessege.Text += " ORDER " + txtViewOrder.Text + " CLOSED!";
                                PopupInfoDialog("ORDER IS CLOSED", false);
                                btnGiveOutSwitchToMessenger.Enabled = false;
                                Button27.Enabled = false;
                                btnPrintReceiptTakeOut.Enabled = false;
                                if (Session["PickupMail"].ToString() == "0")
                                    btnPickupMail.Visible = false;
                                else
                                    btnPickupMail.Enabled = false;
                                return;
                            }
                        }
                    }
                    
                    int rand = new Random().Next(99999999);
                    string imgSignature = "", imgPhoto = "";
                    if (exist)
                    {
                        imgSignature = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    }
                    else
                    {
                        imgSignature = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                    }
                    imgTakeInSignature.ImageUrl = string.Concat(imgSignature, '?', rand);
                    if (photoExist)
                    {
                        imgPhoto = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                    }
                    else
                    {
                        imgPhoto = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                    }
                    imgTakeInMessanger.ImageUrl = string.Concat(imgPhoto, '?', rand);
                    if (orderDetails.Rows[0]["CarrierName"].ToString() != "")
					{
						lblTakeInViewOrderLabel.Text = "Ship In";
					}
					else
					{
						lblTakeInViewOrderLabel.Text = "Take In";
					}
                    if (takeOut && orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() == "")
                    {
                        GiveOutMessenger.Visible = true;
                        GiveOutShipping.Visible = false;
                        tbNumberOfCopies.Visible = false;
                        //tdGiveOut.Visible = false;
                        //                  tbGiveOutOrder.Visible = false;
                        tdTakeIn.Visible = true;
                        tbTakeIn.Visible = true;
                        tdGiveOut1.Visible = true;
                        SetMessengersList(orderDetails, "takeOut");

                        LoadGiveOutSyntheticShippingCourier();
                    }
                    else
                    {
                        //tbNumberOfCopies.Visible = true;
                        //tbTakeIn.Visible = true;
                        //tdTakeIn.Visible = true;
                        tdGiveOut1.Visible = true;
                        //SetMessengersList(orderDetails, "takeOut");
                    }
                    ///Carrier For Give Out
                    //lblViewGiveOutOrderCarrier.Text = orderDetails.Rows[0]["GiveOutCarrierName"].ToString();
                    //lblViewGiveOutOrderCarrierTrackingNumber.Text = orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
                    //lblViewGiveOutSubmittedBy.Text = orderDetails.Rows[0]["GiveOutPerson"].ToString();
                    //lblViewGiveOutAcceptedBy.Text = orderDetails.Rows[0]["GiveOutUserName"].ToString();

                    //if (orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() != "")
                    //{
                    //	lblGiveOutViewOrderLabel.Text = "Ship Out";
                    //}
                    //else
                    //{
                    //	lblGiveOutViewOrderLabel.Text = "Give Out";
                    //}

                    //if (orderDetails.Rows[0]["OrderStatus"].ToString() ==""|| orderDetails.Rows[0]["OrderStatus"].ToString() == "TakeIn" || orderDetails.Rows[0]["OrderStatus"].ToString() == "ShipIn" || orderDetails.Rows[0]["OrderStatus"].ToString() == "Itemizing")
                    //{
                    //	tdGiveOut.Visible = false;
                    //}
                    //else
                    //{
                    //	tdGiveOut.Visible = true; 
                    //}

                    string orderStatus = "";
                    DataSet ds = SyntheticScreeningUtils.IsSyntheticOrderFinishedPacking(Convert.ToInt32(txtViewOrder.Text.Trim()), this);
                    if (ds.Tables[1].Rows.Count != 0)
                    {
                        orderStatus = ds.Tables[1].Rows[0]["OrderStatus"].ToString();
                        lblViewOrderStatus.Text = orderStatus;
                    }

                    DataTable orderInOutDetails = FrontUtils.GetOrderInOutDetails(txtViewOrder.Text.Trim(), this);
					if (orderInOutDetails != null)
					{
						string TakeInRecieptImage = orderInOutDetails.Rows[0]["TakeInRecieptImage"].ToString();
						string GiveOutRecieptImage = orderInOutDetails.Rows[0]["GiveOutRecieptImage"].ToString();

						lblViewTakeInSignatureTimeStamp.Text = orderInOutDetails.Rows[0]["TakeInSignatureTimeStamp"].ToString();
						//lblViewGiveOutSignatureTimeStamp.Text = orderInOutDetails.Rows[0]["GiveOutSignatureTimeStamp"].ToString();

						lblViewTakeInRecieptUploadedTimeStamp.Text = orderInOutDetails.Rows[0]["TakeInRecieptUploadedTimeStamp"].ToString();
						//lblViewGiveOutRecieptUploadedTimeStamp.Text = orderInOutDetails.Rows[0]["GiveOutRecieptUploadedTimeStamp"].ToString();

						lblRecieptTakeIn.Text = "";
						lblRecieptGiveOut.Text = "";
						// Take In
						//if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["TakeInPersonPhoto"].ToString()))
						//{
						//	imgTakeInMessanger.ImageUrl = orderInOutDetails.Rows[0]["TakeInPersonPhoto"].ToString();
						//}
						//else
						//{
						//	imgTakeInMessanger.ImageUrl = imgTransperant;
						//}
						//if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["TakeInCaptureSignature"].ToString()))
						//{
						//	imgTakeInSignature.ImageUrl = orderInOutDetails.Rows[0]["TakeInCaptureSignature"].ToString();
						//}
						//else
						//{
						//	imgTakeInSignature.ImageUrl = imgTransperant;
						//}

						// Give out 

						//if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["GiveOutPersonPhoto"].ToString()))
						//{
						//	imgGiveOutMessanger.ImageUrl = orderInOutDetails.Rows[0]["GiveOutPersonPhoto"].ToString();
						//}
						//else
						//{
						//	imgGiveOutMessanger.ImageUrl = imgTransperant;
						//}
						//if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["GiveOutCaptureSignature"].ToString()))
						//{
						//	imgGiveOutSignature.ImageUrl = orderInOutDetails.Rows[0]["GiveOutCaptureSignature"].ToString();
						//}
						//else
						//{
						//	imgGiveOutSignature.ImageUrl = imgTransperant;
						//}

						if (!string.IsNullOrEmpty(TakeInRecieptImage))
						{
							lnkRecieptTakeInDownloadfrm.Attributes.Add("href", TakeInRecieptImage);
							lnkRecieptTakeInDownloadfrm.Visible = true;
						}
                        else
                        {
                            lnkRecieptTakeInDownloadfrm.Attributes.Remove("href");
                            lnkRecieptTakeInDownloadfrm.Visible = false;
                        }

      //                  if (!string.IsNullOrEmpty(GiveOutRecieptImage))
						//{
						//	lnkRecieptGiveOutDownloadfrm.Attributes.Add("href", GiveOutRecieptImage);
						//	lnkRecieptGiveOutDownloadfrm.Visible = true;
						//}
      //                  else
      //                  {
      //                      lnkRecieptGiveOutDownloadfrm.Attributes.Remove("href");
      //                      lnkRecieptGiveOutDownloadfrm.Visible = false;
      //                  }
                       
                    }
					else
					{
						lblRecieptTakeIn.Text = "";
						lblRecieptGiveOut.Text = "";
					}
				}
				else
				{
					trViewOrderSummary.Visible = false;
					trViewOrderOperation.Visible = false;

					btnUploadSignedOrderReciept.Visible = false;
					btnPrintLabelViewOrder.Visible = false;
					btnPrintReceiptViewOrder.Visible = false;
					btnClearViewOrder.Visible = false;
					btnPrintLabelViewOrder.Enabled = false;
					btnPrintReceiptViewOrder.Enabled = false;
                    btnPrintReceiptTakeOut.Visible = false;
                    btnPickupMail.Visible = false;
                    btnUsePhotoTakeOut.Visible = false;
                    btnSavePhotoTakeOut.Visible = false;

                    lblViewOrderSearch.Text = "No Order Found !";
				}
			}
			catch(Exception ex)
			{
				PopupInfoDialog(ex.Message.ToString(), true);
			}
            //imgViewTakeOutOrder.Visible = false;
            if (lblViewOrderStatus.Text.ToLower() == "closed")
            {
                if (!lblCreateRequestMessege.Text.Contains("ORDER CLOSED"))
                    lblCreateRequestMessege.Text += " ORDER " + txtViewOrder.Text + " CLOSED!";
                PopupInfoDialog("ORDER IS CLOSED", false);
                btnGiveOutSwitchToMessenger.Enabled = false;
                Button27.Enabled = false;
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;

            }
            else
            {
                Button27.Enabled = true;
                if (BulkGiveOutCheckBoxNoPrint.Checked)
                {
                    BulkGiveOutList.Items.Add(txtViewOrder.Text);
                    txtViewOrder.Text = "";
                }
            }
            if (bulkRequestId != "")
                txtViewOrder.Text = bulkRequestId;
            txtViewOrder.Focus();
            if (BulkGiveOutCheckBox.Checked)
            {
                hdnBulkGiveOutCustomercode.Value = customerCode;
                hdnBulkGiveOutCustomerName.Value = customerName;
                btnPrintLabelViewOrder.Enabled = false;
            }
            if (bulkRequestId != "")
            {
                btnPrintLabelViewOrder.Enabled = true;
                btnPrintReceiptTakeOut.Enabled = false;
                if (Session["PickupMail"].ToString() == "0")
                    btnPickupMail.Visible = false;
                else
                    btnPickupMail.Enabled = false;
                //btnCreateTakeOutList.Enabled = false;
            }
        }//btnViewOrder_Click

        public void SendBulkGiveOutEmails(string bulkRequestId)
        {
            DataTable dtBulk = FrontUtils.GetBulkGiveOutList(bulkRequestId, this);
            if (dtBulk == null || dtBulk.Rows.Count == 0)
            {
                PopupInfoDialog("Error finding bulk update items for " + bulkRequestId, true);
                return;
            }
            //DataTable dtTakeOutInfo = FrontUtils.GetBulkGiveOutInfo(bulkRequestId, this);
            foreach (DataRow row in dtBulk.Rows)
            { 
                string result = row.ItemArray[0].ToString();
                if (result == "failed")
                {
                    PopupInfoDialog("Bulk ID not found for " + bulkRequestId, true);
                    return;
                }
                
                string order = row["GSIOrder"].ToString();

                DataTable dtOrderSummery = FrontUtils.GetOrderSummery(order, this);
                DataRow orderRow = dtOrderSummery.Rows[0];
                string trackingNumber = "", carrierName = "", messengerName = "", messenger = "";
                //trackingNumber = orderRow["GiveOutCarrierTrackingNumber"].ToString();
                trackingNumber = txtGiveOutScanPackageBarCodeShip.Text;
                string requestId = row["RequestID"].ToString();
                messenger = hdnMessengerTakeOut.Value;
                string carrier = hdnGiveOutCarriersShip.Value;
                if (trackingNumber != "")
                    carrier = ddlGiveOutShippingCourier.SelectedValue;
                //string trackNumber = txtGiveOutScanPackageBarCodeShip.Text;
                DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);
                DataTable dt = ds.Tables[0];
                if (trackingNumber != null && trackingNumber != "")
                {
                    carrierName = txtGiveOutCarriersShip.Text;
                    //carrier = "";
                    //carrierName = orderRow["GiveOutCarrierName"].ToString();
                    messenger = "0";
                }
                else
                {
                    messengerName = txtGiveOutMessengersList.Text;
                    //messengerName = dt.Rows[0]["Messenger"].ToString();
                    trackingNumber = "";
                }
                DataTable dtLoadTakeoutInfo = QueryUtils.SetTakeOutInfo(order, messenger, carrier, trackingNumber, this);
                if (dtLoadTakeoutInfo != null && dtLoadTakeoutInfo.Rows.Count > 0)
                {
                    
                    if (dtLoadTakeoutInfo.Rows[0][0].ToString() != "success")
                        lblCreateRequestMessege.Text = " Error saving order " + order + " give out data,";
                    else
                    {
                        string sent = FrontUtils.MarkGiveOutBulkEmail(requestId, this);
                        if (sent != "OK")
                            lblCreateRequestMessege.Text += "; Error Sending TakeOut Email for order " + order;
                        //else
                        //    lblCreateRequestMessege.Text += "; Give Out Emails sent for " + bulkRequestId;
                    }
                    //if (dtLoadTakeoutInfo.Rows[0][0].ToString() == "success")
                    //{
                    //    string contactId = dt.Rows[0]["PersonID"].ToString();
                    //    string customerCode = dt.Rows[0]["CustomerCode"].ToString();
                    //    string memo = row["MemoNum"].ToString();
                    //    string mailAddr = SyntheticScreeningUtils.GetContactEmail(contactId, this);
                    //    string sent = "";
                    //    sent = FrontUtils.MarkGiveOutBulkEmail(requestId, this);
                    //    sent = sendEmailToContactOut(mailAddr, requestId, order, memo, messengerName, carrierName, trackingNumber, this);
                    //    if (sent != "OK")//retry 4 times
                    //    {
                    //        for (int i = 0; i <= 3; i++)
                    //        {
                    //            sent = sendEmailToContactOut(mailAddr, requestId, order, memo, messengerName, carrierName, trackingNumber, this);
                    //            if (sent == "OK")
                    //                break;
                    //        }
                    //    }
                    //    if (sent != "OK")
                    //        lblCreateRequestMessege.Text += "; Error Sending TakeOut Email";
                }
                    else
                        lblCreateRequestMessege.Text = " Order " + order + " was not saved,";
                
            }//foreach
            lblCreateRequestMessege.Text += "; Give Out Emails processed for " + bulkRequestId;
        }//SendBulkGiveOutEmails
        protected void GiveOutCourierListSelectedChanged(object sender, EventArgs e)
        {

        }
        protected void OnGiveOutMessengerListIndexChanged(object sender, EventArgs e)
        {
            txtGiveOutMessengersList.Text = ddlGiveOutMessengersList.SelectedItem.ToString();
            hdnMessengerTakeOut.Value = ddlGiveOutMessengersList.SelectedItem.Value;
            hdnTakeOutPhoto.Value = "";
            btnSavePhotoTakeOut.Enabled = false;
            btnUsePhotoTakeOut.Enabled = false;
            //hdnTakeOutMessengerPath2Photo.Value = ddlGiveOutMessengersList.SelectedItem.Value;
            string id = hdnMessengerTakeOut.Value;
            bool exist = QueryUtils.GetInitialSignature(id, this);
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            int rand = new Random().Next(99999999);
            if (exist)
                hdnTakeOutMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            else
                hdnTakeOutMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            if (photoExist)
                hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                hdnTakeOutMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            imgGiveOutSignature.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Signature.Value, '?', rand);
            imgGiveOutPhoto.ImageUrl = string.Concat(hdnTakeOutMessengerPath2Photo.Value, '?', rand);
            string firstChar = txtViewOrder.Text.Substring(0, 1);
            bool update = true;
            if (firstChar == "G")
                update = SyntheticScreeningUtils.UpdateTakeOutMessengerID(hdnViewOrder.Value, id, this); 
            else if (hdnViewOrder.Value != "")
                update = SyntheticScreeningUtils.UpdateTakeOutMessengerID(txtViewOrder.Text, id, this);
        }

        protected void AsyncFileUploadSignedOrderRecieptTakeIn_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadSignedOrderRecieptTakeIn.HasFile)
			{
				Session["AsyncFileUploadSignedOrderRecieptTakeIn"] = Convert.ToBase64String(AsyncFileUploadSignedOrderRecieptTakeIn.FileBytes);
				lblRecieptTakeIn.Text = "";
			}
		}

		protected void AsyncFileUploadSignedOrderRecieptGiveOut_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadSignedOrderRecieptGiveOut.HasFile)
			{
				Session["AsyncFileUploadSignedOrderRecieptGiveOut"] = Convert.ToBase64String(AsyncFileUploadSignedOrderRecieptGiveOut.FileBytes);
				lblRecieptGiveOut.Text = "";
			}
		}

		protected void btnUploadSignedOrderRecieptTakeIn_Click(object sender, EventArgs e)
		{
			int orderCode = Convert.ToInt32(hdnViewOrder.Value);
			string TakeInRecieptImage = Session["AsyncFileUploadSignedOrderRecieptTakeIn"].ToString();
            string fileName = AzureStorageBlob.GenerateFrontInOutFileName(orderCode.ToString(), FrontInOutFile.TakeInReceipt, this);
            string fileImagePath = AzureStorageBlob.UploadFileToBlob(TakeInRecieptImage, fileName, this);

            var msg = FrontUtils.SetOrderInOutDetails(orderCode, 0, null, fileImagePath, 0, null, null, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}

            lblRecieptTakeIn.Text = "Signed receipt uploaded successfully!";
			
			ModalPopupExtender17.Show();
			MainPanel.Update();
		}

		protected void btnUploadSignedOrderRecieptGiveOut_Click(object sender, EventArgs e)
		{
			int orderCode = Convert.ToInt32(hdnViewOrder.Value);
			string GiveOutRecieptImage = Session["AsyncFileUploadSignedOrderRecieptGiveOut"].ToString();
            string fileName = AzureStorageBlob.GenerateFrontInOutFileName(orderCode.ToString(), FrontInOutFile.GiveOutReceipt, this);
            string fileImagePath = AzureStorageBlob.UploadFileToBlob(GiveOutRecieptImage, fileName, this);

            var msg = FrontUtils.SetOrderInOutDetails(orderCode, 0, null, null, 0, null, fileImagePath, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}
			lblRecieptGiveOut.Text = "Signed receipt uploaded successfully!";
			
			ModalPopupExtender17.Show();
			MainPanel.Update();
		}
        #endregion

        #region Submission & Front (Tab-3)


        protected void btnSubmissionSubmit_Click(object sender, EventArgs e)
        {
            if (!IsInputValid())
            {
                return;
            }
            int orderCount = IsOrderExist(txtSubmissionOrderMemo.Text, hdnID.Value);
            if (orderCount == 1)//order number exists
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Memo already exists - please use another memo number');", true);
                return;
            }
            //Create Request
            SubmissionRequestSubmit();

            //Create Order
            string errMsg = SubmissionOrderSubmit();
            if (!string.IsNullOrEmpty(errMsg))
            {
                PopupInfoDialog(errMsg, true);
                return;
            }
        }

        protected void btnSubmissionPrintLabel_Click(object sender, EventArgs e)
        {
            string nOfCopies = txtSubmissionNoOfCopies.Text;
            if (nOfCopies == "")
                nOfCopies = "1";
            PrintPDFReceiptSubmission("both", "create", nOfCopies);
        }
        // Create Request
        public void SubmissionRequestSubmit()
        {
            spMessenger.Visible = false;
            SyntheticCustomerEntriesModel objCustomerEntrie = new SyntheticCustomerEntriesModel();
            objCustomerEntrie.ID = 0;// Convert.ToInt32(hdnID.Value);
            objCustomerEntrie.MemoNum = txtSubmissionOrderMemo.Text.Trim();
            objCustomerEntrie.PONum = txtSubmissionPONumber.Text.Trim();
            objCustomerEntrie.SKUName = txtSubmissionSKU.Text.Trim();
            objCustomerEntrie.TotalQty = Convert.ToInt32(txtSubmissionNoofItems.Text.Trim() == "" ? "0" : txtSubmissionNoofItems.Text.Trim());
            objCustomerEntrie.StyleName = txtSubmissionStyle.Text.Trim();
            objCustomerEntrie.ServiceTypeCode = Convert.ToInt32(ddlSubmissionServiceType.SelectedItem.Value);
            string serviceId = ddlSubmissionCustomerService.SelectedItem.Value;
            if (serviceId == "5")//screening
                objCustomerEntrie.GDServiceTypeId = 7;
            else if (serviceId == "4")//QA
                objCustomerEntrie.GDServiceTypeId = 12;
            else if (serviceId == "6")//Lab Service
                objCustomerEntrie.GDServiceTypeId = 1;

            hdnGDServiceTypeId.Value = objCustomerEntrie.GDServiceTypeId.ToString();
            objCustomerEntrie.RetailerID = Convert.ToInt32(ddlSubmissionRetailer.SelectedItem.Value);
            objCustomerEntrie.CategoryCode = Convert.ToInt32(ddlSubmissionCategory.SelectedItem.Value);
            objCustomerEntrie.JewelryTypeCode = Convert.ToInt32(ddlSubmissionJewelryType.SelectedItem.Value);
            objCustomerEntrie.SpecialInstructions = txtSubmissionSpecialInstruction.Text;
            objCustomerEntrie.CustomerCode = Convert.ToInt32(GetCustomerFromView(ddlSubmissionCustomerlst.SelectedItem.Value).CustomerCode); //Convert.ToInt32(ddlSubmissionCustomerlst.SelectedItem.Value);
            if (dvSubmissionMessanger.Visible == true)
            {
                objCustomerEntrie.DeliveryMethodCode = 1;
                objCustomerEntrie.Messenger = ddlSubmissionMessengerList.SelectedItem.ToString();
                objCustomerEntrie.CarrierCode = 0;
            }
            else if (dvSubmissionShipping.Visible == true)
            {
                objCustomerEntrie.DeliveryMethodCode = 2;
                objCustomerEntrie.CarrierCode = Convert.ToInt32(ddlSubmissionCourierList.SelectedItem.Value);
                objCustomerEntrie.Messenger = "";
            }

            objCustomerEntrie.VendorName = "";
            if (txtSubmissionWeight.Text != "")
                objCustomerEntrie.TotalWeight = Convert.ToDecimal(txtSubmissionWeight.Text);
            else
                objCustomerEntrie.TotalWeight = 0;

            if (txtSubmissionWeightloose.Text != "")
                objCustomerEntrie.TotalWeight = Convert.ToDecimal(txtSubmissionWeightloose.Text);

            if (ddlSubmissionCustomerContact.SelectedItem.Text.ToString() != "")
            {
                hdnSubmissionCustomerContact.Value = ddlSubmissionCustomerContact.SelectedItem.Value.ToString().Trim();
            }
            else
            {
                ddlSubmissionMessengerList.SelectedIndex = -1;
                hdnSubmissionCustomerContact.Value = "0";
            }
            objCustomerEntrie.PersonID = Convert.ToInt32(hdnSubmissionCustomerContact.Value == "" ? "0" : hdnSubmissionCustomerContact.Value);
            objCustomerEntrie.VendorID = 0;
            objCustomerEntrie.Account = "";
            objCustomerEntrie.IsSave = true;
            objCustomerEntrie.MemoReceiptPDF = Regex.Replace(txtVendorMemoPDF.Text, "[^a-zA-Z0-9_.:/]+", "", RegexOptions.Compiled);
            //objCustomerEntrie.MemoReceiptPDF = GetMemoPath();//GetMemoReceiptPath();
            hdnMemoPicturePath.Value = objCustomerEntrie.MemoReceiptPDF;
            lnkView.Visible = true;
            lnkView.Enabled = true;
            var batchList = lstBatchMemos.Items;
            string batchMemo = "";
            if (lstBatchMemos.Items.Count > 0 && objCustomerEntrie.ServiceTypeCode == DbConstants.LabServiceTypeCode)
            {
                foreach (var batch in batchList)
                {
                    if (batchMemo == "")
                        batchMemo = batch.ToString();
                    else
                        batchMemo = batchMemo + "|" + batch.ToString();
                }
            }
            objCustomerEntrie.BatchMemo = batchMemo;

            //IvanB 15/03
            if (IsASNShowed())
                objCustomerEntrie.ASN = txtASN.Text.Trim();
            else
                objCustomerEntrie.ASN = "";
            //IvanB 15/03
            //IvanB 28/03
            if (ddlSubmissionRetailer.SelectedItem.Text == "Blue Nile")
            {
                if (ddlSubmissionCategory.SelectedItem.Text != "")
                    objCustomerEntrie.BNCategoryId = Convert.ToInt32(ddlSubmissionCategory.SelectedItem.Value);
                if (ddlSubmissionDestination.SelectedItem.Text != "")
                    objCustomerEntrie.BNDestinationId = Convert.ToInt32(ddlSubmissionDestination.SelectedItem.Value);
            }
            else
            {
                objCustomerEntrie.BNCategoryId = 0;
                objCustomerEntrie.BNDestinationId = 0;
            }
            //IvanB 28/03
            DataSet ds = SyntheticScreeningUtils.SetSyntheticCustomerEntriesNew(objCustomerEntrie, this);

            if (ds == null || ds.Tables[0].Rows.Count == 0) //failed
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Request of MEMO number " + objCustomerEntrie.MemoNum + " is not saved.');", true);
                return;
            }
            else
            {
                string requestId = "", onlineOrder = "", bnDestination = "", bnCategory = "";
                requestId = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                //if (txtOnlineOrderNumber.Text != "")
                //	onlineOrder = txtOnlineOrderNumber.Text;
                if (ddlSubmissionCategory.SelectedItem.Text != "")
                    bnCategory = ddlSubmissionCategory.SelectedItem.Text;
                if (ddlDestination.SelectedItem.Text != "")
                    bnDestination = ddlDestination.SelectedItem.Text;
                DataSet bnDs = new DataSet();
                if (bnCategory != "" && bnDestination != "")
                    bnDs = SyntheticScreeningUtils.SetBlueNileValues(requestId, onlineOrder, bnCategory, bnDestination, this);
                else
                    bnDs = null;
                hdnSubmissionRequestID.Value = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                SubmissionEnableDisableText(false);
            }
            string customerName = ddlSubmissionCustomerlst.SelectedItem.Text.Replace(", #", "#").Split('#')[0];
        }

        //Create Order from request
        private string SubmissionOrderSubmit()
        {
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

            objNewOrderModelExt.NumberOfItems = txtSubmissionNoofItems.Text.ToString() == "" ? 0 : Convert.ToInt32(txtSubmissionNoofItems.Text.ToString());
            if (txtSubmissionWeight.Text != "")
                objNewOrderModelExt.TotalWeight = Convert.ToDecimal(txtSubmissionWeight.Text.ToString());
            else
                objNewOrderModelExt.TotalWeight = 0;

            if (txtSubmissionWeightloose.Text != "")
                objNewOrderModelExt.TotalWeight = Convert.ToDecimal(txtSubmissionWeightloose.Text.ToString());

            objNewOrderModelExt.IsIQInspected = rblSubmissionNoofItems.Items[0].Selected == true ? true : false;
            objNewOrderModelExt.IsTWInspected = rblSubmissionWeight.Items[0].Selected == true ? true : false;

            objNewOrderModelExt.MeasureUnitID = Convert.ToInt32(ddlSubmissionWeightType.SelectedValue.ToString());
            objNewOrderModelExt.ServiceTypeID = Convert.ToInt32(hdnGDServiceTypeId.Value.ToString());
            objNewOrderModelExt.Memo = txtSubmissionOrderMemo.Text.ToString();
            objNewOrderModelExt.SpecialInstructions = txtSubmissionSpecialInstruction.Text.ToString();
            objNewOrderModelExt.PersonID = Convert.ToInt32(ddlSubmissionMessengerList.SelectedItem.Value);//hdnMessengerTakeIn.Value == "" ? 0 : Convert.ToInt32(hdnMessengerTakeIn.Value.ToString());
            objNewOrderModelExt.VendorID = 0;// Convert.ToInt32(ddlVendorDepartureSettingTakeIn.SelectedItem.Value.ToString() == "" ? "0" : ddlVendorDepartureSettingTakeIn.SelectedItem.Value.ToString());
            objNewOrderModelExt.CarrierID = Convert.ToInt32(ddlSubmissionCourierList.SelectedItem.Value);
            objNewOrderModelExt.CarrierTrackingNumber = txtSubmissionBarcode.Text.ToString();
           
            if (lstBatchMemos.Items.Count == 0 && objNewOrderModelExt.Memo != "")
            {
                lstBatchMemos.Items.Add(objNewOrderModelExt.Memo);
            }
            objNewOrderModelExt.MemoNumbers = lstBatchMemos.Items.Cast<ListItem>().Select(x => x.Value).ToList<string>();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();

            string customerId = ddlSubmissionCustomerlst.SelectedValue.ToString();
            objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);

            //string personId = hdnMessengerTakeIn.Value.ToString();
            objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
            objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt16(customers.Find(m => m.CustomerId == customerId).OfficeId);

            string errMsg = string.Empty;

            var result = FrontUtils.CreateOrder(objNewOrderModelExt, "TakeIn", this, out errMsg);

            if (result != null)
            {
                string sku = "", batchMemos = "";
                string totalQty = txtSubmissionNoofItems.Text.ToString();
                objNewOrderModelExt.OrderNumber = result;
                hdnSubmissionOrderCode.Value = objNewOrderModelExt.OrderNumber;
                string groupId = null;
                DataSet dsGroupId = new DataSet();
                dsGroupId = GSIAppQueryUtils.GetGroupId(objNewOrderModelExt.OrderNumber, this);
                if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
                {
                    groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                bool added = true;
                if (lstBatchMemos.Items.Count == 0)
                    added = SyntheticScreeningUtils.SetMemoNumber(groupId, txtSubmissionOrderMemo.Text.ToString(), this);
                else
                {
                    //List<string> batchMemoList = lstBatchMemos.Items.Cast<ListItem>().Select(x => x.Value).ToList<string>();
                    string[] batchMemoList = batchMemos.Split('|');
                    foreach (string batchMemo in batchMemoList)
                        added = SyntheticScreeningUtils.SetMemoNumber(groupId, batchMemo, this);
                }
                objNewOrderModelExt.PickedUpByOurMessenger = chkSubmissionPickedUpByGSIMessenger.Checked == true ? true : false;
                if (chkSubmissionPickedUpByGSIMessenger.Checked == true)
                {
                    var result_Messenger = FrontUtils.SetPickedUpByOurMessenger(objNewOrderModelExt, this, out errMsg);
                }

                string reqId = hdnSubmissionRequestID.Value;
                if (reqId != "")
                {
                    // Update order number in SyntheticCustomerOrder
                    var setOrder = SyntheticScreeningUtils.SetSyntheticCustomerOrder(Convert.ToInt32(hdnSubmissionRequestID.Value.Trim()), objNewOrderModelExt.OrderNumber, Convert.ToInt32(hdnServiceTimeTakeIn.Value.ToString()), this);
                    // Get SKU Detail
                    DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(reqId, this);
                    DataRow tmpDr = ds.Tables[0].Rows[0];
                    sku = tmpDr["SKUName"].ToString();
                    batchMemos = tmpDr["BatchMemo"].ToString();
                }
                //if (sku != "" && objNewOrderModelExt.ServiceTypeID == 1 && batchMemos == "")//if sku exists in customerProgram - run itemizing
                if ((sku != "" && objNewOrderModelExt.ServiceTypeID == 1 && (batchMemos == "" || !batchMemos.Contains("|"))) && (Convert.ToInt32(totalQty) < 2476))//if sku exists in customerProgram - run itemizing and total batches < 100
                {
                    if (batchMemos != "")//single batch memo - use it for itemizing
                        objNewOrderModelExt.Memo = batchMemos;
                    string created = CreateBatchItem(objNewOrderModelExt, sku);
                    if (!created.Contains("success"))
                        PopupInfoDialog("Error creating batch: " + created, true);
                    else
                    {
                        created = created.Replace("success: ", "");
                        string[] batches = created.Split(',');
                        foreach (string batch in batches)
                        {
                            int BatchID = Convert.ToInt32(batch);
                            int EventID = FrontUtils.BatchEvents.Created;
                            int ItemsAffected = Convert.ToInt32(totalQty);
                            int ItemsInBatch = Convert.ToInt32(totalQty);
                            int FormID = FrontUtils.Codes.Itemizing;
                            FrontUtils.SetBatchEvent(EventID, BatchID, FormID, ItemsAffected, ItemsInBatch, this);//Procedure dbo.spSetBatchEvents
                                                                                                                  //[3 / 2 / 2022 12:43:31 PM]ItemizingUpdateGroup: GroupOfficeID = 1; GroupID = 1704310; VendorOfficeID =; VendorID =; InspectedQuantity = 3; InspectedTotalWeight = 0.00; InspectedWeightUnitID = 2;
                            ///* ItemizingUpdateGroup: @AuthorId = 261; @AuthorOfficeId = 1; @CurrentOfficeId = 1; @rId =; @GroupOfficeID = 1; @GroupID = 1704310; @VendorOfficeID =; @VendorID =; @Insp*/ectedQuantity = 3; @InspectedTotalWeight = 0.00; @InspectedWeightUnitID = 2;

                        }
                        lblCreateRequestMessege.Text += ", " + batches.Length.ToString() + " batches for order " + objNewOrderModelExt.OrderNumber.Trim();
                    }
                }
                else if (Convert.ToInt32(totalQty) > 2475)
                    PopupInfoDialog("Error creating batch: number of batches will exceed 99!", true);
                if (errMsg == "")
                {
                    string nOfCopies = txtSubmissionNoOfCopies.Text;
                    if (nOfCopies == "")
                        nOfCopies = "1";
                    PrintPDFReceiptSubmission("both", "create", nOfCopies);
                }

                lblOrderCodelbl.Visible = true;
                lblOrderCode.Visible = true;
                lblOrderCode.Text = objNewOrderModelExt.OrderNumber;
                string serviceCode = "";
                if (ddlSubmissionCustomerService.SelectedValue == "4")
                {
                    serviceCode = "Q";
                }
                else if (ddlSubmissionCustomerService.SelectedValue == "5")
                {
                    serviceCode = "S";
                }
                if (ddlSubmissionCustomerService.SelectedValue == "6")
                {
                    serviceCode = "L";
                }
                if (ddlSubmissionCustomerService.SelectedValue == "8")
                {
                    serviceCode = "A";
                }
                lblServiceCode.Text = serviceCode;
            }
            return errMsg;
        }

        public void PrintPDFReceiptSubmission(string printAction, string prtSource, string nOfCopies, string direction = "takeIn")
        {
            string machineName = null;
            if (Session["MachineName"] != null)
                machineName = Session["MachineName"].ToString();
            else
            {
                lblCreateRequestMessege.Text = "No machine name: Cannot print.";
                return;
            }
            string CustomerCode = "";
            string requestID = "";
            if (prtSource == "create")
            {
                requestID = hdnSubmissionRequestID.Value;
                hdnViewRequestID.Value = "";
            }
            else
            {
                requestID = hdnViewRequestID.Value;
                hdnRequestID.Value = "";
            }
            string orderCode = hdnSubmissionOrderCode.Value.ToString().Trim();
            if (orderCode == "" || orderCode == "0")
            {
                PopupInfoDialog("No order code found", true);
                return;
            }
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

            DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
            if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
            {
                objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
                objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
                CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
            }
            string messengerId = hdnMessengerTakeIn.Value;
            DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);
            if (dsOrderSummery == null || dsOrderSummery.Rows.Count == 0)
            {
                PopupInfoDialog("No record found for order " + orderCode, true);
                return;
            }
            objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
            objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
            CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
            var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
            var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
            var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
            var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
            objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

            string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
            string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
            string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
            string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
            string notInspectedQuantity = totalQ;
            string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
            string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
            string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
            string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
            string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
            string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
            string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();
            string serviceTypeID = dsOrderSummery.Rows[0]["ServiceTypeID"].ToString();
            string syntheticServiceTypeCode = dsOrderSummery.Rows[0]["SyntheticServiceTypeCode"].ToString();

            if (serviceTypeID == "1")
                serviceTypeID = "L";
            else if (serviceTypeID == "12")//Q
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG/Q";
                else
                {
                    if (CustomerCode == "1599")
                        serviceTypeID = "S";
                    else
                        serviceTypeID = "Q";
                }
            }
            else if (serviceTypeID == "9")
                serviceTypeID = "V";
            else if (serviceTypeID == "13")//Both
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG/Q";
                else
                {
                    if (CustomerCode == "1599")
                        serviceTypeID = "S";
                    else
                        serviceTypeID = "B";
                }
            }
            else if (serviceTypeID == "7")//Screening
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG";
                else
                    serviceTypeID = "S";
            }
            string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
            if (sPersonCustomerOfficeID == "")
                sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["CustomerOfficeID"].ToString();
            string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
            string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
            string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
            string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();
            string email = dsOrderSummery.Rows[0]["Email"].ToString();
            //Take In
            string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
            string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
            string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
            string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
            string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
            string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
            string TakeInOrShipReceivingBy = string.Empty;
            if (TakeInCarrierName != "")
                TakeInOrShipReceivingBy = "Ship Receiving";
            else
                TakeInOrShipReceivingBy = "Take In";

            //Give Out
            string GiveOutPersonID = dsOrderSummery.Rows[0]["GiveOutPersonID"].ToString();
            string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
            string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
            string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
            string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
            string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
            //string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
            string GiveOutUserName = TakeInUserName;
            string GiveOutOrShipOutBy = string.Empty;
            if (GiveOutCarrierName != "")
                GiveOutOrShipOutBy = "Ship Out";
            else
                GiveOutOrShipOutBy = "Take Out";
            string customerAddress = customerName + ",\n" + address + ",\n" + city + ", " + stateName + ",\n" + country + "-" + zip + ",\n" + phone + ",\n" + fax;
            OrderModel objOrderModel = new OrderModel();
            objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
            var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
            objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

            string groupMemoNumbers = null;
            if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
                groupMemoNumbers = "";
            else
            {
                for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
                    groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
            }
            groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);
            string BNCategoryName = "", BNDestinationName = "";
            if (dsOrderSummery.Columns.Contains("BNCategoryName"))
            {
                BNCategoryName = dsOrderSummery.Rows[0]["BNCategoryName"].ToString();
                BNDestinationName = dsOrderSummery.Rows[0]["BNDestinationName"].ToString();
            }
            //create xml
            //string machineName = abc;

            DataTable dataInputs = new DataTable("ExtLabel");
            dataInputs.Columns.Add("customerAddress", typeof(String));
            dataInputs.Columns.Add("orderCreateDate", typeof(String));
            dataInputs.Columns.Add("totalQ", typeof(String));
            dataInputs.Columns.Add("totalW", typeof(String));
            dataInputs.Columns.Add("specialInstr", typeof(String));
            dataInputs.Columns.Add("memo", typeof(String));
            dataInputs.Columns.Add("TakeInOrShipReceivingBy", typeof(String));
            dataInputs.Columns.Add("GiveOutOrShipOutBy", typeof(String));
            dataInputs.Columns.Add("PickedUpByOurMessenger", typeof(String));
            dataInputs.Columns.Add("TakeInCaptureSignature", typeof(String));
            dataInputs.Columns.Add("TakeInPersonName", typeof(String));
            dataInputs.Columns.Add("TakeInCarrierName", typeof(String));
            dataInputs.Columns.Add("TakenOutByOurMessenger", typeof(String));
            dataInputs.Columns.Add("GiveOutPersonName", typeof(String));
            dataInputs.Columns.Add("GiveOutCaptureSignature", typeof(String));
            dataInputs.Columns.Add("GiveOutCarrierTrackingNumber", typeof(String));
            dataInputs.Columns.Add("TakeInUserName", typeof(String));
            dataInputs.Columns.Add("GiveOutUserName", typeof(String));
            dataInputs.Columns.Add("orderCode", typeof(String));
            dataInputs.Columns.Add("TakeInCarrierTrackingNumber", typeof(String));
            dataInputs.Columns.Add("GiveOutCarrierName", typeof(String));
            dataInputs.Columns.Add("requestID", typeof(String));
            dataInputs.Columns.Add("GroupMemoNumbers", typeof(String));
            dataInputs.Columns.Add("PrintAction", typeof(String));
            dataInputs.Columns.Add("NumberOfCopies", typeof(String));
            dataInputs.Columns.Add("MessengerID", typeof(String));
            dataInputs.Columns.Add("Email", typeof(String));
            dataInputs.Columns.Add("OfficeID", typeof(String));
            dataInputs.Columns.Add("GiveOutMessengerID", typeof(String));
            dataInputs.Columns.Add("ServiceTypeID", typeof(String));
            dataInputs.Columns.Add("BNCategoryName", typeof(String));
            dataInputs.Columns.Add("BNDestinationName", typeof(String));
            if (machineName != null && machineName != "")
                dataInputs.Columns.Add("MachineName", typeof(String));
            DataRow row;
            row = dataInputs.NewRow();
            row[0] = customerAddress;
            row[1] = orderCreateDate;
            row[2] = totalQ;
            row[3] = totalW;
            row[4] = specialInstr;
            row[5] = memo;
            row[6] = TakeInOrShipReceivingBy;
            row[7] = GiveOutOrShipOutBy;
            row[8] = PickedUpByOurMessenger;
            row[9] = TakeInCaptureSignature;
            row[10] = TakeInPersonName;
            row[11] = TakeInCarrierName;
            row[12] = TakenOutByOurMessenger;
            row[13] = GiveOutPersonName;
            row[14] = GiveOutCaptureSignature;
            row[15] = GiveOutCarrierTrackingNumber;
            row[16] = TakeInUserName;
            row[17] = GiveOutUserName;
            row[18] = orderCode;
            row[19] = TakeInCarrierTrackingNumber;
            row[20] = GiveOutCarrierName;
            row[21] = requestID;
            row[22] = groupMemoNumbers;
            row[23] = printAction;
            row[24] = nOfCopies;
            row[25] = messengerId;
            row[26] = email;
            row[27] = sPersonCustomerOfficeID;
            if (direction != "takeOut")
                GiveOutPersonID = "";
            row[28] = GiveOutPersonID;
            row[29] = serviceTypeID;
            row[30] = BNCategoryName;
            row[31] = BNDestinationName;
            if (machineName != null)
                row[32] = machineName;

            dataInputs.Rows.Add(row);


            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool sent = QueryUtils.SendMessageToStorage(ms, this);

            }
            dataInputs.Clear();
        }//PrintPDFReceipt1

        private void SubmissionEnableDisableText(bool val)
        {
            ddlSubmissionCustomerlst.Enabled = val;
            ddlSubmissionCustomerService.Enabled = val;
            ddlSubmissionRetailer.Enabled = val;
            ddlSubmissionServiceType.Enabled = val;
            ddlSubmissionCategory.Enabled = val;

            ddlSubmissionJewelryType.Enabled = val;
            ddlSubmissionCustomerContact.Enabled = val;
            txtASN.Enabled = val;
            ddlSubmissionBNCategory.Enabled = val;
            ddlSubmissionDestination.Enabled = val;

            txtSubmissionNoofItems.Enabled = val;
            txtSubmissionWeight.Enabled = val;
            rblSubmissionNoofItems.Enabled = val;
            rblSubmissionWeight.Enabled = val;
            btnSubmissionServiceTime.Enabled = val;
            txtSubmissionSpecialInstruction.Enabled = val;
            txtBatchMemo.Enabled = val;
            btnAddMemo.Enabled = val;
            lstBatchMemos.Enabled = val;
            fuVendorMemo.Enabled = val;

            txtSubmissionOrderMemo.Enabled = val;
            txtSubmissionPONumber.Enabled = val;
            txtSubmissionSKU.Enabled = val;
            txtSubmissionStyle.Enabled = val;
            txtSubmissionNoofItems.Enabled = val;
            txtSubmissionNoofItems.Enabled = val;
            ddlSubmissionSKU.Enabled = val;

            ddlSubmissionMessengerList.Enabled = val;
            btnSwitchToSubmissionCourier.Enabled = val;
            btnValidateSubmissionSignature.Enabled = val;
            btnValidateSubmissionPhoto.Enabled = val;

            ddlSubmissionCourierList.Enabled = val;
            txtSubmissionBarcode.Enabled = val;


            chkSubmissionPickedUpByGSIMessenger.Enabled = val;
            btnSubmissionSubmit.Enabled = val;
        }

        protected void UploadMemoFileToAzure(object sender, EventArgs e)
        {
            int fileLen = fuVendorMemo.PostedFile.ContentLength;
            byte[] input = new byte[fileLen - 1];
            input = fuVendorMemo.FileBytes;
            string fileName = fuVendorMemo.FileName;
            //string fileName = txtRequestID.Text + @".pdf";
            fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.:/]+", "", RegexOptions.Compiled);
            using (MemoryStream ms = new MemoryStream())
            {
                var msInput = new MemoryStream(input);
                ms.Write(input, 0, fuVendorMemo.FileBytes.Length);
                bool uploaded = UploadImages(fileName, "gdlight/SyntheticMemoReceipt", ms);
            }
            string container = Session["AzureContainerName"].ToString();
            string directory = "SyntheticMemoReceipt";
            string url = @"https://gdlightstorage.blob.core.windows.net/" + container + @"/" + directory + @"/" + fileName;

            Session["url"] = url;
            MainPanel.Update();
        }
        protected void UploadMemoFileToAzure_Click(object sender, EventArgs e)
        {
            string url = Session["url"].ToString();
            if (url != "")
            {
                iframePDFViewer.Src = url;
                txtVendorMemoPDF.Text = url;
                iframePDFViewer.Visible = true;
            }
        }
        private static bool UploadImages(string fileName, string pathName, MemoryStream ms)
        {
            try
            {
                string myAccountName = HttpContext.Current.Session["AzureAccountName"].ToString();// "gdlightstorage"; 
                string myAccountKey = HttpContext.Current.Session["AzureAccountKey"].ToString(); //@"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
                StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                CloudBlobContainer container = blobClient.GetContainerReference(pathName);
                var blockBlob = container.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = "application/pdf";
                ms.Position = 0;
                blockBlob.UploadFromStream(ms);
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

        private bool IsASNShowed()
        {
            if (ddlSubmissionRetailer.SelectedItem.Text == "Blue Nile")
            {
                var workCat = ddlSubmissionBNCategory.SelectedItem != null ? ddlSubmissionBNCategory.SelectedItem.Text : "";
                if (workCat == "Vendor on Demand" || workCat == "Made on Demand and SPO")
                    return true;
                else
                    return false;
            }
            return false;
        }

        public string GetMemoPath()
        {
            string fileName = fuVendorMemo.FileName;
            string container = Session["AzureContainerName"].ToString();
            string directory = "SyntheticMemoReceipt";
            string url = @"https://gdlightstorage.blob.core.windows.net/" + container + @"/" + directory + @"/" + fileName;
            return url;
        }

        protected void btnSubmissionClear_Click(object sender, EventArgs e)
        {
            SubmissionEnableDisableText(true);

            lnkView.Visible = false;
            lnkView.Enabled = false;
            iframePDFViewer.Visible = false;
            iframePDFViewer.Src = "";

            spCustomerlst.Visible = true;
            spCustomerServices.Visible = true;
            spMemo.Visible = true;
            spPONumber.Visible = true;
            spStyle.Visible = true;
            spSKU.Visible = true;
            spQuantity.Visible = true;
            spRetailer.Visible = true;
            spServiceType.Visible = true;
            spCategory.Visible = true;
            spJewType.Visible = true;
            spCustomerContact.Visible = true;
            spMessenger.Visible = true;
            JQuantity.Visible = true;
            JWeight.Visible = false;
            txtVendorMemoPDF.Text = "";
            txtASN.Text="";

            ddlSubmissionCustomerlst.SelectedIndex = -1;
            ddlSubmissionCustomerService.SelectedIndex = -1;
            ddlSubmissionRetailer.SelectedIndex = -1;
            ddlSubmissionServiceType.SelectedIndex = -1;
            ddlSubmissionCategory.SelectedIndex = -1;
            ddlSubmissionJewelryType.SelectedIndex = -1;

            ddlSubmissionCustomerContact.SelectedIndex = -1;
            ddlSubmissionServiceType.SelectedIndex = -1;
            ddlSubmissionCategory.SelectedIndex = -1;
            ddlSubmissionBNCategory.SelectedIndex = -1;
            ddlSubmissionDestination.SelectedIndex = -1;
            ddlSubmissionSKU.SelectedIndex = -1;

            txtSubmissionNoofItems.Text = "";
            txtSubmissionWeight.Text = "";
            txtSubmissionOrderMemo.Text = "";
            txtSubmissionPONumber.Text = "";
            txtSubmissionSKU.Text = "";
            txtSubmissionStyle.Text = "";
            txtSubmissionSpecialInstruction.Text = "";

            lstBatchMemos.Items.Clear();
            ddlSubmissionMessengerList.SelectedIndex = -1;
            ddlSubmissionCourierList.SelectedIndex = -1;
            txtSubmissionBarcode.Text = "";
            txtSubmissionServiceTime.Text = "";
            chkSubmissionPickedUpByGSIMessenger.Checked = false;

            lblOrderCodelbl.Visible = false;
            lblOrderCode.Visible = false;

            lblServiceCode.Text = "";

            imgSubmissionStoredSignature.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            imgSubmissionStoredPhoto.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";

        }

        protected void ValidateSubmissionPhoto_Click(object sender, EventArgs e)
        {
            string id = ddlMessengersList.SelectedItem.Value;
            string name = ddlMessengersList.SelectedItem.Text;
            string machineName = Session["MachineName"].ToString();
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblRequestError.Visible = true;
                lblRequestError.Text = "Tablet is not configured";
                return;
            }

            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");

            Session["DateStamp"] = date;
            string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            photoBtn.Enabled = false;
            signatureBtn.Enabled = false;
            //to reprint initial photo delete the old 
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            if (photoExist)
            {
                bool deleted = QueryUtils.DeleteScreeningInitialPhoto(id, this);

            }
            bool sent = QueryUtils.SendMessengerInfoToStorage(id, dt, "photo", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i = 0; i <= retryCount; i++)
                {
                    found = QueryUtils.GetPhotoForValidation(id, this);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
                        imgStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
                        photoBtn.Enabled = true;
                        signatureBtn.Enabled = true;
                        //string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblRequestError.Text = "Did not receive signature for validation. Please try again.";
                        photoBtn.Enabled = true;
                        signatureBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblRequestError.Text = "Error getting the picture";
                }
                else
                    lblCreateRequestMessege.Text += ": Photo validated";
                //else
                //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "photo", this);
                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, machineName, clientMachineName);
                //}
                photoBtn.Enabled = true;
                signatureBtn.Enabled = true;
            }
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }

        protected void ValidateSubmissionSignature_Click(object sender, EventArgs e)
        {
            //SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/1259_Signature.png";
            //SignaturesComparePopupExtender.Show();
            //return;
            string id = ddlSubmissionMessengerList.SelectedItem.Value;
            string name = ddlSubmissionMessengerList.SelectedItem.Text;
            string machineName = "";
            try
            {
                machineName = Session["MachineName"].ToString();
            }
            catch (Exception ex)
            {
                lblCreateRequestMessege.Visible = true;
                lblCreateRequestMessege.Text = "No machine name";
                return;
            }
            DataTable dt = QueryUtils.GetClientMachineName(machineName, this);
            if (dt == null || dt.Rows.Count == 0)
            {
                lblCreateRequestMessege.Visible = true;
                lblCreateRequestMessege.Text = "Tablet is not configured";
                return;
            }
            //string frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
            //string clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
            int timeAdd = SyntheticScreeningUtils.GetTimeDiff(this);
            string date = DateTime.Now.ToLocalTime().ToString("MMddyyyyhhmmss");
            Session["DateStamp"] = date;
            string old_new = "old";
            if (imgStoredSignature.ImageUrl == "")
                old_new = "new";
            signatureBtn.Enabled = false;
            photoBtn.Enabled = false;
            bool sent = true;
            name = name.Replace(" ", "_");
            bool sigExists = QueryUtils.GetInitialSignature(id, this);
            if (!sigExists)
                old_new = "new";

            if (old_new == "new")
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date + " " + "n", dt, "signature", this);
            else
                sent = QueryUtils.SendMessengerInfoToStorage(id + " " + name + " " + date, dt, "signature", this);
            if (sent)
            {
                string found = "false";
                int retryCount = Convert.ToInt16(Session["RetryCount"].ToString());
                int sleepingSeconds = Convert.ToInt16(Session["SleepingSeconds"].ToString());
                for (int i = 0; i <= retryCount; i++)
                {
                    //found = QueryUtils.GetSignatureForValidation(frontMadenName, clientMadenName, date, this);
                    found = QueryUtils.GetSignatureForValidation(id, date, this);
                    if (found == "true")
                    {
                        int rand = new Random().Next(99999999);
                        signatureBtn.Enabled = true;
                        photoBtn.Enabled = true;

                        if (old_new == "new")
                        {
                            string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                            hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                        }
                        imgStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value + '?' + rand);
                        break;
                    }
                    if (i == retryCount)
                    {
                        lblRequestError.Text = "Did not receive signature for validation. Please try again.";
                        signatureBtn.Enabled = true;
                        photoBtn.Enabled = true;
                        return;
                    }
                    Thread.Sleep(1000 * sleepingSeconds);
                }
                if (found == "false")
                {
                    lblRequestError.Text = "No signature to validate";
                    //lblCreateRequestMessege.Text += ": No Signature to validate";
                }
                else
                {
                    SignaturePicture1333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
                    SignaturePicture333.ImageUrl = "https://gdlightstorage.blob.core.windows.net/gdlight/FrontInOut/Signature/" + id + @"_" + date + @".png";
                    SignaturesComparePopupExtender.Show();
                    if (!lblCreateRequestMessege.Text.Contains("validation"))
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    else
                    {
                        int idx = lblCreateRequestMessege.Text.IndexOf(":");
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Substring(0, idx);
                        lblCreateRequestMessege.Text += ": Signature validation processed";
                    }
                    //lblRequestError.Text = "Signature was validated";
                    //if (old_new == "new")
                    //    string result = QueryUtils.UpdateMessengerSignaturePhoto(id, "signature", this);
                }

                //if (found != null && found == "true")
                //{
                //    ValidateSignatures(id, frontMadenName, clientMadenName);
                //}
            }
            signatureBtn.Enabled = true;
            photoBtn.Enabled = true;
            //NewReportPicture.ImageUrl = item.ImageUrl;

            //NewPicturePopupExtender.Show();
        }

        protected void OnSubmissionMessengerListIndexChanged(object sender, EventArgs e)
        {
            txtMessengerTakeIn.Text = ddlSubmissionMessengerList.Text;
            hdnMessengerTakeIn.Value = ddlSubmissionMessengerList.SelectedItem.Value;
            string id = hdnMessengerTakeIn.Value;
            bool exist = QueryUtils.GetInitialSignature(id, this);
            bool photoExist = QueryUtils.GetInitialSignature(id, this, "photo");
            int rand = new Random().Next(99999999);
            if (exist)
                hdnMessengerPath2Signature.Value = "https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            else
                hdnMessengerPath2Signature.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            if (photoExist)
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                hdnMessengerPath2Photo.Value = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            imgSubmissionStoredSignature.ImageUrl = string.Concat(hdnMessengerPath2Signature.Value, '?', rand);
            imgSubmissionStoredPhoto.ImageUrl = string.Concat(hdnMessengerPath2Photo.Value, '?', rand);
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoryCode = ddlSubmissionCategory.SelectedValue;
            rdlSubmissionServiceTime.DataSource = SyntheticScreeningUtils.GetSyntheticServiceTimeByCategory(categoryCode, this);
            rdlSubmissionServiceTime.DataBind();
            txtSubmissionServiceTime.Text = "";
            hdnSubmissionServiceTime.Value = "";
            spServiceTime.Visible = true;
        }

        protected void rdlSubmissionServiceTime_IndexChanged(object sender, EventArgs e)
        {
            txtSubmissionServiceTime.Text = rdlSubmissionServiceTime.SelectedItem.Text.ToString();
            hdnSubmissionServiceTime.Value = rdlSubmissionServiceTime.SelectedItem.Value.ToString();
            spServiceTime.Visible = false;
        }

        protected void dllSubmissionSKU_SelectedIndexChanged(object sender, EventArgs e)
        {
            var skuName = ddlSubmissionSKU.SelectedItem != null ? ddlSubmissionSKU.SelectedItem.Text : "";
            txtSubmissionSKU.Text = skuName;
            spSKU.Visible = skuName == "" & txtStyle.Text.Trim() == "";
            spStyle.Visible = skuName == "" & txtStyle.Text.Trim() == "";
        }

        protected void ddlSubmissionBNCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsASNShowed())
                asnTr.Visible = true;
            else
                asnTr.Visible = false;
        }

        protected bool IsInputValid()
        {
            var retval = true;
            //IvanB 10/03
            retval = CheckIfAllMandatoryFieldsFilled(retval);
            //IvanB 10/03

            if (retval)
            {
               
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please fill all mandatory fields');", true);
            }
            return retval;
        }

        protected void InputFieldChanged(object sender, EventArgs e)
        {
            TextBox txt = sender as TextBox;
            String Text = txt.Text;
            int servType = ddlSubmissionServiceType.SelectedItem != null ? Convert.ToInt32(ddlSubmissionServiceType.SelectedItem.Value) : 0;
            String skuText = txtSubmissionSKU.Text.Trim();
            String styleText = txtSubmissionStyle.Text.Trim();

            if (txt != null)
            {
                switch (txt.ID)
                {
                    case "txtSubmissionOrderMemo":
                        spMemo.Visible = !((Text != "") & (Text != "0"));
                        break;
                    case "txtSubmissionPONumber":
                        spPONumber.Visible = !((Text != "") & (Text != "0"));
                        break;
                    case "txtSubmissionStyle":
                        if (servType == DbConstants.LabServiceTypeCode)
                        {
                            spStyle.Visible = ((Text == "") || (Text == "0")) & ((skuText == "") || (skuText == "0"));
                            spSKU.Visible = ((skuText == "") || (skuText == "0")) & ((styleText == "") || (styleText == "0"));
                        }
                        else
                        {
                            spStyle.Visible = !((Text != "") & (Text != "0"));
                        }
                        break;
                    case "txtSubmissionSKU":
                        if (servType == DbConstants.LabServiceTypeCode)
                        {
                            spSKU.Visible = ((Text == "") || (Text == "0")) & ((styleText == "") || (styleText == "0"));
                            spStyle.Visible = ((styleText == "") || (styleText == "0")) & ((skuText == "") || (skuText == "0"));
                        }
                        else
                        {
                            spSKU.Visible = !((Text != "") & (Text != "0"));
                        }
                        txtTotalQty.Focus();
                        break;
                    case "txtSubmissionNoofItems":
                        spQuantity.Visible = !((Text != "") & (Text != "0"));
                        //rblSubmissionMethod.Items[0].Text.Focus();
                        break;
                }
            }
        }

        private bool CheckIfAllMandatoryFieldsFilled(bool retval)
        {
            //IvanB 11/04
            int serviceTypeCode = ddlSubmissionServiceType.SelectedItem != null ? Convert.ToInt32(ddlSubmissionServiceType.SelectedItem.Value) : 0;
            //IvanB 11/04
            if (txtSubmissionOrderMemo.Text == "" && serviceTypeCode != DbConstants.LabServiceTypeCode)
            {
                spMemo.Visible = true;
                retval = false;
            }
            else
                spMemo.Visible = false;
           
            //if (txtSubmissionNoofItems.Text.Trim() == "")
            //{
            //    spQuantity.Visible = true;
            //    retval = false;
            //}
            //else
            //    spQuantity.Visible = false;


            if (ddlSubmissionServiceType.Text.Trim() == "" || (ddlSubmissionServiceType.Text == "0"))
            {
                spServiceType.Visible = true;
                retval = false;
            }
            else
                spServiceType.Visible = false;
            if (ddlSubmissionCategory.Text.Trim() == "" || (ddlSubmissionCategory.Text == "0"))
            {
                spCategory.Visible = true;
                retval = false;
            }
            else
                spCategory.Visible = false;

            if (txtSubmissionPONumber.Text == "" && serviceTypeCode != DbConstants.LabServiceTypeCode)
            {
                spPONumber.Visible = true;
                retval = false;
            }
            else
                spPONumber.Visible = false;

            if (serviceTypeCode == DbConstants.LabServiceTypeCode)
            {
                retval = txtStyle.Text.Trim() != "" || txtSubmissionSKU.Text.Trim() != "";
                spStyle.Visible = !retval ? txtStyle.Text.Trim() == "" : false;
                spSKU.Visible = !retval ? txtSubmissionSKU.Text.Trim() == "" : false;
            }
            else
            {
                if (txtSubmissionStyle.Text.Trim() == "")
                {
                    spStyle.Visible = true;
                    retval = false;
                }
                else
                    spStyle.Visible = false;
                if (txtSubmissionSKU.Text.Trim() == "")
                {
                    spSKU.Visible = true;
                    retval = false;
                }
                else
                    spSKU.Visible = false;
            }
            if ((ddlSubmissionRetailer.Text.Trim() == "") || (ddlSubmissionRetailer.Text == "0"))
            {
                spRetailer.Visible = true;
                retval = false;
            }
            else
                spRetailer.Visible = false;

            if (((ddlSubmissionJewelryType.Text.Trim() == "") || (ddlSubmissionJewelryType.Text == "0")) && serviceTypeCode != DbConstants.LabServiceTypeCode)
            {
                if (ddlSubmissionCategory.Text.Trim() != "5")
                {
                    spJewType.Visible = true;
                    retval = false;
                }
            }
            if ((ddlSubmissionCustomerContact.Text.Trim() == "") || (ddlSubmissionCustomerContact.Text == "0"))
            {
                spCustomerContact.Visible = true;
                retval = false;
            }
            else
                spCustomerContact.Visible = false;
            //if ((ddlSubmissionMessengerList.SelectedValue == "") || (ddlSubmissionMessengerList.SelectedValue == "0") && dvSubmissionMessanger.Visible == true)
            //{
            //    spMessenger.Visible = true;
            //    retval = false;
            //}
            //else
            //    spMessenger.Visible = false;

            //if ((ddlSubmissionCourierList.SelectedValue == "") || (ddlSubmissionCourierList.SelectedValue == "0") && dvSubmissionShipping.Visible == true)
            //{
            //    spCourier.Visible = true;
            //    retval = false;
            //}
            //else
            //    spCourier.Visible = false;
            if (txtSubmissionServiceTime.Text.Trim() == "")
            {
                spServiceTime.Visible = true;
                retval = false;
            }
            else
                spServiceTime.Visible = false;
            if (txtVendorMemoPDF.Text == "")
            {
                retval = false;
            }
            if (hfServiceTypeCode.Value == "")
            {
                retval = false;
            }
            return retval;
        }

        protected void InputBoxChanged(object sender, EventArgs e)
        {
            DropDownList txt = sender as DropDownList;
            String Text = txt.Text;
            var serviceTypeCode = ddlSubmissionServiceType.SelectedItem != null ? Convert.ToInt32(ddlSubmissionServiceType.SelectedItem.Value) : 0;
            var category = ddlSubmissionCategory.SelectedItem != null ? ddlSubmissionCategory.SelectedItem.Value : "";
            if (txt != null)
            {
                switch (txt.ID)
                {
                    case "ddlSubmissionCustomerlst":
                        spCustomerlst.Visible = !((Text != "") & (Text != "0"));
                        ddlSubmissionCustomerService.Focus();
                        break;
                    case "ddlSubmissionCustomerService":
                        spCustomerServices.Visible = !((Text != "") & (Text != "0"));
                        ddlSubmissionRetailer.Focus();
                        break;
                    case "ddlSubmissionRetailer":
                        spRetailer.Visible = !((Text != "") & (Text != "0"));
                        ddlSubmissionServiceType.Focus();
                        break;
                    case "ddlSubmissionServiceType":
                        if (serviceTypeCode == DbConstants.LabServiceTypeCode)
                        {
                            bool oneOfTwo = txtStyle.Text.Trim() != "" || txtSubmissionSKU.Text.Trim() != "";
                            spStyle.Visible = !oneOfTwo ? txtStyle.Text.Trim() == "" : false;
                            spSKU.Visible = !oneOfTwo ? txtSubmissionSKU.Text.Trim() == "" : false;
                            if (txtSubmissionSKU.Text.Trim() != "")
                            {
                                ddlSubmissionSKU.Items.Insert(0, new ListItem(txtSubmissionSKU.Text.Trim(), "-1"));
                                ddlSubmissionSKU.SelectedValue = "-1";
                            }
                        }
                        else
                        {
                            spStyle.Visible = txtStyle.Text.Trim() == "";
                            spSKU.Visible = txtSubmissionSKU.Text.Trim() == "";
                        }
                        spServiceType.Visible = !((Text != "") & (Text != "0"));
                        spCategory.Visible = !((category != "") & (category != "0"));
                        ddlSubmissionCategory.Focus();
                        break;
                    case "ddlSubmissionCategory":
                        spCategory.Visible = !((Text != "") & (Text != "0"));
                        ddlSubmissionJewelryType.Focus();
                        break;
                    case "ddlSubmissionJewelryType":
                        spJewType.Visible = !((Text != "") & (Text != "0"));
                        ddlSubmissionCustomerContact.Focus();
                        break;
                    case "ddlSubmissionCustomerContact":
                        spCustomerContact.Visible = !((Text != "") & (Text != "0"));
                        //btnDeleteCustomerContact.Visible = true;
                        Session["ContactID"] = ddlSubmissionCustomerContact.SelectedItem.Value.ToString().Trim();
                        ddlSubmissionMessengerList.Focus();
                        break;

                    case "ddlSubmissionMessengerList":
                        spMessenger.Visible = !((Text != "") & (Text != "0"));
                        //btnDeleteMessenger.Visible = true;
                        txtSubmissionSpecialInstruction.Focus();
                        break;
                }
                //}

                if (ddlSubmissionCategory.Text != "")
                {
                    if (ddlSubmissionCategory.SelectedItem.Text.ToUpper() == "LOOSE DIAMONDS")
                    {
                        trJewelryType.Visible = false;
                        JWeight.Visible = true;
                        JQuantity.Visible = false;
                        txtSubmissionNoofItems.Text = "1";
                        //ddlJewelryType.Text = "";
                        ddlSubmissionJewelryType.Items.FindByValue("0").Selected = true;
                        Loose.Value = "1";
                    }
                    else
                    {
                        trJewelryType.Visible = true;
                        JWeight.Visible = false;
                        JQuantity.Visible = true;
                        txtSubmissionWeightloose.Text = "";
                        Loose.Value = "0";
                    }
                    if (serviceTypeCode == DbConstants.LabServiceTypeCode)
                    {
                        trJewelryType.Visible = false;
                    }
                }
            }
        }

        private int IsOrderExist(string memoNum, string SaveLaterID)
        {
            int IsOrderExist = 0;
            string dbSaveLaterID = string.Empty;
            SaveLaterID = SaveLaterID == "0" ? "" : SaveLaterID;
            DataTable dtMemo = SyntheticScreeningUtils.GetSyntheticCustomerMemo(memoNum, this).Tables[0];
            if (dtMemo != null && dtMemo.Rows.Count > 0)
            {
                dbSaveLaterID = dtMemo.Rows[0]["ID"].ToString();
            }
            if (SaveLaterID == dbSaveLaterID)
            {
                IsOrderExist = 0;
            }
            else
            {
                IsOrderExist = 1;
            }
            return IsOrderExist;
        }

        protected void btnAddMemo_Click(object sender, EventArgs e)
        {
            //if (txtBatchMemo.Text.Trim() != "")
            ListItem li = new ListItem();
            li.Text = txtBatchMemo.Text.Trim().ToLower();
            if (!lstBatchMemos.Items.Contains(li))
            {
                lstBatchMemos.Items.Add(txtBatchMemo.Text);
                txtBatchMemo.Text = "";
                lstBatchMemos.DataBind();
            }
            else
			{
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "alert('Batch memo already exist');", true);
            }
        }
        protected void ddlServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int serviceTypeCode = Convert.ToInt32(ddlSubmissionServiceType.SelectedItem.Value);
            LoadSyntheticCategoryByServiceType(serviceTypeCode);
            trJewelryType.Visible = serviceTypeCode != DbConstants.LabServiceTypeCode;
            if (serviceTypeCode == DbConstants.LabServiceTypeCode)
            {
                txtSubmissionSKU.Visible = false;
                ddlSubmissionSKU.Visible = true;
                rfvddlSubmissionSKU.Enabled = true;
                rfvtxtSubmissionSKU.Enabled = false;
            }
            else
            {
                txtSubmissionSKU.Visible = true;
                ddlSubmissionSKU.Visible = false;
                rfvtxtSubmissionSKU.Enabled = false;
                rfvddlSubmissionSKU.Enabled = true;
            }
            //ShowFieldsDependOnServiceType(serviceTypeCode);
        }

        protected void ddlRetailer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int retailerId = Convert.ToInt32(ddlSubmissionRetailer.SelectedItem.Value);
            LoadSyntheticServiceTypeByRetailer(retailerId);
            LoadSyntheticCategoryByServiceType(0);
            LoadSyntheticBNCategory();
            LoadDestination();
            if (ddlSubmissionRetailer.SelectedItem.Text == "Blue Nile")
            {
                trBNCategory.Visible = true;
                trDestination.Visible = true;
            }
            else
            {
                trBNCategory.Visible = false;
                trDestination.Visible = false;
                asnTr.Visible = false;
            }
        }

        private void LoadSKUList(CustomerModel customer)
        {
            var skuList = SyntheticScreeningUtils.GetCustomerPrograms(customer, null, this);
            ddlSubmissionSKU.DataSource = skuList;
            ddlSubmissionSKU.DataBind();
            ddlSubmissionSKU.Items.Insert(0, new ListItem("", "0"));
        }

        public void LoadPersonServices()
        {
            ddlSubmissionCustomerService.DataSource = QueryCustomerUtils.GetFrontServiceList(this);
            ddlSubmissionCustomerService.DataBind();

            ListItem frontService = new ListItem();
            frontService.Value = "0";
            frontService.Text = "";
            ddlSubmissionCustomerService.Items.Insert(0, frontService);
        }

        public void LoadSubmissionShippingCourier()
        {
            DataSet ds = QueryUtils.GetCarriers(this);
            DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlSubmissionCourierList.DataTextField = "CarrierName";
                ddlSubmissionCourierList.DataValueField = "CarrierCode";
                ddlSubmissionCourierList.DataSource = dt;
                ddlSubmissionCourierList.DataBind();

                ListItem courierList = new ListItem();
                courierList.Value = "0";
                courierList.Text = "";
                ddlSubmissionCourierList.Items.Insert(0, courierList);
            }
        }

        private void LoadCustomerPerson()
        {
            List<PersonExModel> personList = GetViewState("PersonList") as List<PersonExModel>;
            var contactPersons = personList.FindAll(m => m.Position.Name == "Contact");
            var datasource = from x in contactPersons
                             select new
                             {
                                 x.PersonId,
                                 DisplayField = String.Format("{0} {1}", x.FirstName, x.LastName)
                             };
            //alex
            SetViewState(contactPersons, SessionConstants.PersonEditData);
            //SetViewState(personList, SessionConstants.PersonEditData);
            //alex
            ddlSubmissionCustomerContact.DataSource = datasource;
            ddlSubmissionCustomerContact.DataValueField = "PersonID";
            ddlSubmissionCustomerContact.DataTextField = "DisplayField";
            ddlSubmissionCustomerContact.DataBind();

            ListItem customerContact = new ListItem();
            customerContact.Value = "0";
            customerContact.Text = "";
            ddlSubmissionCustomerContact.Items.Insert(0, customerContact);

        }

        private void LoadMessengers()
        {
            List<PersonExModel> personList = GetViewState("PersonList") as List<PersonExModel>;

            var contactPersons = personList.FindAll(m => m.Position.Name == "Messenger");
            var datasource = from x in contactPersons
                             select new
                             {
                                 x.PersonId,
                                 DisplayField = String.Format("{0} {1}", x.FirstName, x.LastName)
                             };
            //alex
            //SetViewState(contactPersons, SessionConstants.MessengerEditData);
            //SetViewState(personList, SessionConstants.PersonEditData);
            //alex
            ddlSubmissionMessengerList.DataSource = datasource;
            ddlSubmissionMessengerList.DataValueField = "PersonID";
            ddlSubmissionMessengerList.DataTextField = "DisplayField";
            ddlSubmissionMessengerList.DataBind();

            ListItem customerContact = new ListItem();
            customerContact.Value = "0";
            customerContact.Text = "";
            ddlSubmissionMessengerList.Items.Insert(0, customerContact);
        }
        public void LoadSyntheticJewelryType()
        {
            DataTable dt = SyntheticScreeningUtils.GetSyntheticJewelryType(this);
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlSubmissionJewelryType.DataTextField = "JewelryTypeName";
                ddlSubmissionJewelryType.DataValueField = "JewelryTypeCode";
                ddlSubmissionJewelryType.DataSource = dt;
                ddlSubmissionJewelryType.DataBind();
            }
            System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
            li.Value = "0";
            li.Text = "";
            ddlSubmissionJewelryType.Items.Insert(0, li);
        }

        public void LoadSyntheticCategoryByServiceType(int serviceTypeCode)
        {
            ddlSubmissionCategory.Items.Clear();
            DataTable dt = SyntheticScreeningUtils.GetSyntheticCategoryByServiceType(serviceTypeCode, this);
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlSubmissionCategory.DataTextField = "CategoryName";
                ddlSubmissionCategory.DataValueField = "CategoryCode";
                ddlSubmissionCategory.DataSource = dt;
                ddlSubmissionCategory.DataBind();
                System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
                li.Value = "0";
                li.Text = "";
                ddlSubmissionCategory.Items.Insert(0, li);
            }
            else
            {
                ddlSubmissionCategory.DataSource = null;
                ddlSubmissionCategory.DataBind();
            }
        }

        public void LoadSyntheticServiceTypeByRetailer(int retailerID)
        {
            ddlSubmissionServiceType.Items.Clear();
            DataTable dt = SyntheticScreeningUtils.GetSyntheticServiceTypeByRetailer(retailerID, this);
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlSubmissionServiceType.DataTextField = "ServiceTypeName";
                ddlSubmissionServiceType.DataValueField = "ServiceTypeCode";
                ddlSubmissionServiceType.DataSource = dt;
                ddlSubmissionServiceType.DataBind();
                System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
                li.Value = "0";
                li.Text = "";
                ddlSubmissionServiceType.Items.Insert(0, li);
            }
            else
            {
                ddlSubmissionServiceType.DataSource = null;
                ddlSubmissionServiceType.DataBind();
            }
        }

        public void LoadSyntheticBNCategory()
        {
            DataTable dt = new DataTable();
            //IvanB 28/03 start
            ddlSubmissionBNCategory.DataTextField = "CategoryName";
            ddlSubmissionBNCategory.DataValueField = "CategoryId";
            dt = SyntheticScreeningUtils.GetBNScreeningCategoryList(this);
            //IvanB 28/03 end
            ddlSubmissionBNCategory.DataSource = dt;
            ddlSubmissionBNCategory.DataBind();
            System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
            li.Value = "0";
            li.Text = "";
            ddlSubmissionBNCategory.Items.Insert(0, li);
        }

        public void LoadDestination()
        {
            DataTable dt = new DataTable();
            //IvanB 28/03 start
            ddlSubmissionDestination.DataTextField = "DestinationName";
            ddlSubmissionDestination.DataValueField = "DestinationId";
            dt = SyntheticScreeningUtils.GetBNScreeningDestinationList(this);
            //IvanB 28/03 end
            ddlSubmissionDestination.DataSource = dt;
            ddlSubmissionDestination.DataBind();
            System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
            li.Value = "0";
            li.Text = "";
            ddlSubmissionDestination.Items.Insert(0, li);
        }

        protected void btnSwitchToSubmissionCourier_Click(object sender, EventArgs e)
        {
            dvSubmissionMessanger.Visible = false;
            dvSubmissionShipping.Visible = true;
        }

        protected void btnSwitchToSubmissionMessenger_Click(object sender, EventArgs e)
        {
            dvSubmissionMessanger.Visible = true;
            dvSubmissionShipping.Visible = false;
        }

        protected void OnSubmissionCustomerLookupSelectedChanged(object sender, EventArgs e)
        {
            var customer = GetCustomerFromView(ddlSubmissionCustomerlst.SelectedValue);
            if (customer == null) return;
            var infoModel = QueryCustomerUtils.GetCustomer(customer, this);

            SetViewState(infoModel, SessionConstants.CustomerEditData);
            if (infoModel == null) return;

            var Persons = QueryCustomerUtils.GetPersonsByCustomer(customer, Page);
            SetViewState(Persons, SessionConstants.PersonEditData);

            List<PersonExModel> personList = QueryCustomerUtils.GetPersonsByCustomer(customer, this);
            SetViewState(personList, "PersonList");

            LoadCustomerPerson();
            LoadMessengers();
            LoadSKUList(customer);

            txtCustomerLookupReq.Text = ddlSubmissionCustomerlst.SelectedItem.Text;
            hfCustomerIDReq.Value = customer.CustomerId;
            hfCustomerCodeReq.Value = customer.CustomerCode;

            var retailer = SyntheticScreeningUtils.GetRetailerListByCustomerCode(customer.CustomerCode, Page);
            rdlRetailer.Items.Clear();
            rdlRetailer.DataSource = retailer;
            rdlRetailer.DataBind();

            ddlSubmissionRetailer.DataSource = retailer;
            ddlSubmissionRetailer.DataBind();
            System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
            li.Value = "0";
            li.Text = "";
            ddlSubmissionRetailer.Items.Insert(0, li);
            if (retailer.Rows.Count == 1) lblRetailerMsg.Visible = true;

        }
        #endregion

        #region ActiveTab Event
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
		{
			var errMsg = "";

			if (TabContainer1.ActiveTabIndex == 0)  
			{
                errMsg = SubmitTakeIn();  //Take In

            }
			else if (TabContainer1.ActiveTabIndex == 1) //Give Out
			{
				errMsg = SubmitGiveOut();
			} 

            if (!string.IsNullOrEmpty(errMsg))
			{
				PopupInfoDialog(errMsg, true);
				return;
			}
            btnPrintReceiptTakeIn.Enabled = true;
            btnPrintLabelTakeIn.Enabled = true;
            btnSendEmail.Enabled = true;
        }

		protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
		{
            ClearRequest();
			TakeInDefaultFields();
			ClearGiveOut();
            ClearViewOrder();
            txtRequest.Text = "";
            txtItemNoGiveOut.Text = "";
            //IvanB 16/02 start
            lstRequests.Items.Clear();
            lstRequests.Visible = false;
            BulkGiveOutList.Visible = false;
            BulkGiveOutList.Items.Clear();
            //IvanB 16/02 end

			if (TabContainer1.ActiveTabIndex == 0)  //Take In
			{
                int rand = new Random().Next(99999999);
                string img = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
                imgStoredSignature.ImageUrl = string.Concat(img, '?', rand);
                img = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
                imgStoredPhoto.ImageUrl = string.Concat(img, '?', rand);
                lnkView.Visible = true;
                lnkView.Enabled = false;
                iframePDFViewer.Visible = false;
                nbrCopiesBox.Text = "1";
                btnSendEmail.Enabled = false;
                txtRequest.Focus();
                BulkGiveOutList.Visible = false;
                BulkGiveOutList.Items.Clear();
                BulkGiveOutCheckBox.Checked = false;
                acceptanceBox.Checked = false;
                saveShipInfoBox.Checked = false;
                lstRequests.Items.Clear();
                lblBulkGiveOutTotal.Text = "";
                BulkGiveOutCheckBoxNoPrint.Checked = false;
                saveShipInfoBox.Checked = false;
                savedCarrierTxt.Text = "";
                savedCarrierTxt.Visible = false;
                savedTrackingNumberTxt.Text = "";
                savedTrackingNumberTxt.Visible = false;
            }
			else if (TabContainer1.ActiveTabIndex == 1) //Give Out
			{
				txtItemNoGiveOut.Focus();
			}
			else if (TabContainer1.ActiveTabIndex == 2) //View Order
			{
                //imgViewTakeOutOrder.Visible = false;
                btnPrintReceiptTakeOut.Visible = false;
                btnPickupMail.Visible = false;
                BulkGiveOutCheckBoxNoPrint.Checked = false;
                lnkView.Visible = false;
                iframePDFViewer.Visible = false;
                txtViewOrder.Focus();
                BulkGiveOutList.Items.Clear();
                lblBulkGiveOutTotal.Text = "";
                lstRequests.Visible = false;
                lstRequests.Items.Clear();
                saveShipInfoBox.Checked = false;
                savedCarrierTxt.Text = "";
                savedCarrierTxt.Visible = false;
                savedTrackingNumberTxt.Text = "";
                savedTrackingNumberTxt.Visible = false;
            }
            else if (TabContainer1.ActiveTabIndex == 3) //View Order
            {
                txtCustomerLookupReq.Focus();
            }
        }
        #endregion

        #region TreeView Methods Utils
        private static void LoadTreeMethods(List<CommunicationModel> items, TreeView tree)
        {
            items.Sort((m1, m2) => m1.Order.CompareTo(m2.Order));
            tree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Communications" } };
            data.AddRange(items.Select(item => new TreeViewModel
            {
                ParentId = "0",
                Id = item.Code,
                DisplayName = item.Name,
            }));

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tree.Nodes.Add(rootNode);
            foreach (TreeNode node in rootNode.ChildNodes)
            {
                var src = items.Find(m => m.Code == node.Value && m.Checked);
                node.Checked = src != null;
            }

        }
        private static void ReorderMethods(bool up, TreeView tree)
        {
            if (tree.SelectedNode == null || tree.SelectedNode.Value == "0") return;
            var nowInd = 0;
            var items = new List<CommunicationModel>();
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                if (node.Value == tree.SelectedValue) nowInd = i;
                items.Add(new CommunicationModel { Checked = node.Checked, Code = node.Value, Name = node.Text, Order = i });
            }
            if (up && nowInd == 0) return;
            if (!up && nowInd == (items.Count - 1)) return;
            var newInd = up ? (nowInd - 1) : (nowInd + 1);
            var item1 = items.Find(m => m.Order == nowInd);
            var item2 = items.Find(m => m.Order == newInd);
            if (item1 != null) item1.Order = newInd;
            if (item2 != null) item2.Order = nowInd;
            LoadTreeMethods(items, tree);
            //tree.Focus();
            tree.Nodes[0].ChildNodes[newInd].Selected = true;
        }
        private static void OnTreeMethodCheck(TreeView tree)
        {
            if (tree.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tree.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        private static string GetTreeMethodValue(TreeView tree)
        {
            var result = "";
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                result += node.Value + (node.Checked ? "1" : "0");
            }
            return result;
        }
        private static void SetTreeMethodValue(string value, TreeView tree)
        {
            var communications = new CommunicationsModel();
            communications.SetDbValue(value);
            LoadTreeMethods(communications.Items, tree);
        }


		
		#endregion

		#region Validators

		private const string ValidatorGroupCompany = "ValGrpCompany";
		private const string ValidatorGroupPerson = "ValGrpPerson";

		protected bool IsGroupValid(string sValidationGroup)
		{
			Page.Validate(sValidationGroup);
			foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
			{
				if (!validator.IsValid)
				{
					return false;
				}
			}
			return true;
		}

		private string GetErrMessage(string sValidationGroup)
		{
			var msg = "";
			foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
			{
				if (!validator.IsValid)
				{
					msg += "<br/>" + validator.ToolTip;
				}
			}
			return msg;
		}

		#endregion

		#region Print Data in PDF
		protected void btnPrintReceiptTakeIn_Click(object sender, EventArgs e)
		{
            string nOfCopies = nbrCopiesBox.Text;
            if (nOfCopies == "")
                nOfCopies = "1";
            PrintPDFReceipt1("receipt", "create", nOfCopies);
            
		}
        
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {

            string requestId = txtRequest.Text;
            DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);
            DataRow dr = ds.Tables[0].Rows[0];
            string order = dr["GSIOrder"].ToString();
            string contactId = dr["PersonID"].ToString();
            string totalQty = dr["TotalQty"].ToString();
            string mailAddr = SyntheticScreeningUtils.GetContactEmail(contactId, this);
            string contactName = dr["PersonName"].ToString();
            string msg = "OK";
            for (int i = 0; i <= 4; i++)
            {
                msg = SendMail(mailAddr, requestId, contactName, this);
                if (msg == "OK")
                    break;
            }
            if (msg != "OK")
                lblCreateRequestMessege.Text += ", Error sending submission email for " + requestId;
            if (order != "" && order != null)
            {
                DataTable dsOrderSummery = FrontUtils.GetOrderSummery(order, this);
                string memo = dsOrderSummery.Rows[0]["Memo"].ToString();
                string trackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
                string carrierName = "", messengerName = "";

                if (trackingNumber != null && trackingNumber != "")
                {
                    carrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();

                }
                else
                {
                    messengerName = dr["Messenger"].ToString();
                    trackingNumber = "";
                }
                string sent = "OK";
                //for (int i = 0; i <= 4; i++)
                //{
                    sent = sendEmailToContact(mailAddr, requestId, order, memo, totalQty, this);
                    //if (sent == "OK")
                    //    break;
                //}
                if (sent != "OK")
                    lblCreateRequestMessege.Text += ", Error sending acceptance mail for " + requestId;
                DataTable dtOrderState = FrontUtils.GetScreeningOrderState(order, this);
                if (dtOrderState != null && dtOrderState.Rows.Count > 0)
                {
                    string stateName = dtOrderState.Rows[0].ItemArray[0].ToString();
                    if (stateName.ToLower() == "closed")
                    {
                        for (int i = 0; i <= 4; i++)
                        {
                            sent = sendEmailToContactOut(mailAddr, requestId, order, memo, messengerName, carrierName, trackingNumber, this);
                            if (sent == "OK")
                                break;
                        }
                        if (sent != "OK")
                            lblCreateRequestMessege.Text += ", Error sending pickup mail for " + requestId;
                    }
                }
                
            }
        }//btnSendEmail_Click

        protected void btnPrintLabelTakeIn_Click(object sender, EventArgs e)
		{
            //PrintPDFLabel();
            string requestID = hdnRequestID.Value;
            //PrintPDFLabel1(requestID);
            //PrintPDFLabel();
            string nOfCopies = nbrCopiesBox.Text;
            if (nOfCopies == "")
                nOfCopies = "1";
            PrintPDFReceipt1("label", "create", nOfCopies);
            
        }

        protected void btnPrintLabelViewOrder_Click(object sender, EventArgs e)
		{
            if (txtViewOrder.Text.Substring(0, 1).ToUpper() == "G")
            {
                PrintBulkGiveOutLabel(txtViewOrder.Text, lblViewOrderCustomer.Text);
                return;
            }
            string requestID = hdnViewRequestID.Value;
            //PrintPDFLabel1(requestID);
            //PrintPDFLabel();
            string nOfCopies = NmbrOfCopiesBox.Text;
            if (nOfCopies == "")
                nOfCopies = "1";
            PrintPDFReceipt1("label", "reprint", nOfCopies);
            txtViewOrder.Focus();
        }

        protected void btnCreateTakeOutList_Click(object sender, EventArgs e)
        {
            if (Session["PickupMail"].ToString() == "0")
                btnPickupMail.Visible = false;
            else
            {
                var btn = sender as Button;
                if (btn.ID == "btnPickupMail")
                {
                    if (txtViewOrder.Text == "")
                    {
                        lblCreateRequestMessege.Text = "Enter the GSI Order";
                        return;
                    }
                    string gsiOrder = txtViewOrder.Text.Trim();
                    DataTable dsOrderSummery = FrontUtils.GetOrderSummery(gsiOrder, this);
                    string requestId = dsOrderSummery.Rows[0]["RequestID"].ToString();
                    string sentRequest = FrontUtils.MarkPickupBulkEmail(requestId, this, true);
                    if (sentRequest == "OK")
                        lblCreateRequestMessege.Text = "Pickup Email sent for " + gsiOrder;
                    else
                        lblCreateRequestMessege.Text = "Error sending Pickup Email for " + gsiOrder;
                    return;
                }
                else
                    btnPickupMail.Enabled = false;
            }
            if (BulkGiveOutList.Items.Count == 0)
            {
                PopupInfoDialog("No items in bulk list", false);
                return;
            }
            if (txtViewOrder.Text != "" && txtViewOrder.Text.Substring(0, 1).ToUpper() == "G" && txtViewOrder.Text.Length > 1)
            {
                string gsiOrder = BulkGiveOutList.Items[0].Text;
                DataTable dsOrderSummery = FrontUtils.GetOrderSummery(gsiOrder, this);
                string requestId = dsOrderSummery.Rows[0]["RequestID"].ToString();
                DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);
                DataTable dt = ds.Tables[0];
                string custCode = dt.Rows[0]["CustomerCode"].ToString();
                string custName = dt.Rows[0]["CustomerName"].ToString();
                PrintBulkGiveOutLabel(txtViewOrder.Text, custName);
                return;
            }
            Session["BulkLoad"] = "true";
            string[] parts = BulkGiveOutList.Items[0].Text.Split('-');
            txtViewOrder.Text = parts[0]; //BulkGiveOutList.Items[0].Text;
            Session["Sent"] = "N";
            btnViewOrder_Click(null, null);
            btnPickupMail.Enabled = false;
            Session["Sent"] = "N";
            //for (int i=0; i<= BulkGiveOutList.Items.Count-1; i++)
            //{
            //    txtViewOrder.Text = BulkGiveOutList.Items[i].Text;
            //    //btnClearViewOrder_Click(null, null);
            //    btnViewOrder_Click(null, null);
            //    btnPrintReceiptViewOrder_Click(null, null);

            //}
            Session["BulkLoad"] = null;
            string customerName = hdnBulkGiveOutCustomerName.Value;
            string customerCode = hdnBulkGiveOutCustomercode.Value;
            if (customerName == "")
            {
                PopupInfoDialog("Error getting customer name for bulk items", true);
                return;
            }
            string bulkReqId = FrontUtils.InsertBulkGiveOutInfo(Convert.ToInt32(customerCode), this);
            for (int i=0; i <= BulkGiveOutList.Items.Count - 1; i++)
            {
                if (!BulkGiveOutList.Items[i].Text.Contains("-"))
                    BulkGiveOutList.Items.Remove(BulkGiveOutList.Items[i]);
            }
            for (int i=0; i<=BulkGiveOutList.Items.Count-1; i++)
            {
                string[] bulkItem = BulkGiveOutList.Items[i].Text.Split('-');
                string groupCode = bulkItem[0];
                string requestId = bulkItem[1];
                DataTable dt = FrontUtils.UpdateBulkGiveOutData(Convert.ToInt32(requestId), bulkReqId, this);
                bool updated = SetGiveOutPhoto(groupCode);
            }
            if (Session["PickupMail"].ToString() == "1")
            {
                string sent = FrontUtils.MarkPickupBulkEmail(bulkReqId, this);
                if (sent == "failed")
                    lblCreateRequestMessege.Text += "; Error Sending Pickup Email for " + bulkReqId;
                else
                    lblCreateRequestMessege.Text += "; Pickup Email was sent for " + bulkReqId;
            }
            if (!BulkGiveOutCheckBoxNoPrint.Checked)
                PrintBulkGiveOutLabel(bulkReqId, customerName);
            lblCreateRequestMessege.Text += ", Bulk Label created for " + bulkReqId + ",";
        }//btnCreateTakeOutList_Click

        protected void btnBulkGiveOut_Click(object sender, EventArgs e)
        {
            if (!BulkGiveOutCheckBoxNoPrint.Checked)
            {
                string bulkRequestId = txtViewOrder.Text;
                SendBulkGiveOutEmails(bulkRequestId);
            }
            else if (BulkGiveOutList.Items.Count > 0)
            {
                for (int i=0; i<=BulkGiveOutList.Items.Count-1; i++)
                {
                    string order = BulkGiveOutList.Items[i].Text;
                    hdnOrderCode.Value = BulkGiveOutList.Items[i].Text.Trim();
                    hdnGiveOutCarriersShip.Value = ddlGiveOutShippingCourier.SelectedItem.Value;
                    btnPrintReceiptViewOrder_Click(null, null);
                }
            }
        }
        protected void btnPrintReceiptViewOrder_Click(object sender, EventArgs e)
		{
            var btn = sender as Button;
            bool takeOut = true;
            //if (btn.ID == "btnPrintReceiptTakeOut" || btn.ID == "btnReprintReceiptTakeOut")
            //    takeOut = true;
            string nOfCopies = nbrCopiesBox.Text;
            if (nOfCopies == "")
                nOfCopies = "1";
            if (takeOut)
            {
                string order = hdnOrderCode.Value.ToString().Trim();
                string messenger = hdnMessengerTakeOut.Value;
                string carrier = hdnGiveOutCarriersShip.Value;
                string trackNumber = txtGiveOutScanPackageBarCodeShip.Text;
                if (messenger == "0" && trackNumber == "")
                {
                    PopupInfoDialog("Missing Tracking Number", true);
                    return;
                }
                //DataTable dtOrderState = FrontUtils.GetScreeningOrderState(order, this);
                //if (dtOrderState != null && dtOrderState.Rows.Count > 0)
                //{
                //    string stateName = dtOrderState.Rows[0].ItemArray[0].ToString();
                //    if (stateName.ToLower() == "closed")
                //    {

                //    }
                //}
                DataTable dtLoadTakeoutInfo = QueryUtils.SetTakeOutInfo(order, messenger, carrier, trackNumber, this);
                if (dtLoadTakeoutInfo != null && dtLoadTakeoutInfo.Rows.Count > 0)
                {
                    if (dtLoadTakeoutInfo.Rows[0][0].ToString() == "success")
                    {
                        PrintPDFReceipt1("receipt", "reprint", nOfCopies, "takeOut");
                        DataTable dsOrderSummery = FrontUtils.GetOrderSummery(order, this);
                        string trackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
                        string carrierName = "", messengerName = "";
                        if (trackingNumber != null && trackingNumber != "")
                        {
                            carrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
                            messenger = "";
                        }
                        else
                        {
                            if (ddlGiveOutMessengersList.Items.Count > 0)
                                messengerName = ddlGiveOutMessengersList.SelectedItem.Text;
                            trackingNumber = "";
                        }
                        string requestId = dsOrderSummery.Rows[0]["RequestID"].ToString();
                        DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, this);
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count != 0)
                        {
                            string contactId = dt.Rows[0]["PersonID"].ToString();
                            string customerCode = dt.Rows[0]["CustomerCode"].ToString();
                            string customerName = dt.Rows[0]["CustomerName"].ToString();
                            string memo = dsOrderSummery.Rows[0]["Memo"].ToString();
                            if (!BulkGiveOutCheckBox.Checked)
                            {
                                string mailAddr = SyntheticScreeningUtils.GetContactEmail(contactId, this);
                                string sent = "";
                                if (BulkGiveOutCheckBoxNoPrint.Checked)
                                    sent = FrontUtils.MarkGiveOutBulkEmail(requestId, this);
                                else if (btn.ID == "btnPrintReceiptTakeOut")
                                {
                                    sent = FrontUtils.MarkGiveOutBulkEmail(requestId, this);
                                    //sent = sendEmailToContactOut(mailAddr, requestId, order, memo, messengerName, carrierName, trackingNumber, this);
                                }
                                if (sent != "OK")//retry 4 times
                                {
                                    for (int i = 0; i <= 3; i++)
                                    {
                                        sent = sendEmailToContactOut(mailAddr, requestId, order, memo, messengerName, carrierName, trackingNumber, this);
                                        if (sent == "OK")
                                            break;
                                    }
                                }
                                if (sent != "OK")
                                    lblCreateRequestMessege.Text += "; Error Sending TakeOut Email";
                                lblCreateRequestMessege.Text += ", ORDER SUBMITTED for " + requestId + ",";
                            }
                            else
                            {
                                BulkGiveOutList.Items.Add(order + "-" + requestId);
                                hdnBulkGiveOutCustomercode.Value = customerCode;
                                hdnBulkGiveOutCustomerName.Value = customerName;
                            }
                        }
                        lblViewRequestID.Text = requestId;
                    }
                    else
                    {
                        PopupInfoDialog("Take Out data was not saved", true);
                    }

                }
            }
            else
                PrintPDFReceipt1("receipt", "reprint", nOfCopies);
            
            
            txtViewOrder.Focus();
            //string requestID = hdnViewRequestID.Value;
            ////PrintPDFLabel1(requestID);
            //string nOfCopies = nbrCopiesBox.Text;
            //if (nOfCopies == "")
            //    nOfCopies = "1";
            //PrintPDFReceipt1("receipt", "reprint", nOfCopies);
            //txtViewOrder.Focus();
        }

        private bool SetGiveOutPhoto(string groupCode)
        {
            string url = imgGiveOutPhoto.ImageUrl;
            if (url.Contains("empty.png"))
                return true;
            if (txtGiveOutScanPackageBarCodeShip.Text != "")
                return true;
            if (hdnTakeOutPhoto.Value != "")//already updated
                return true;
            if (hdnTakeOutMessengerPath2Photo.Value != "")
            {
                bool saved = SyntheticScreeningUtils.SaveTakeOutPhotoPath(groupCode, hdnTakeOutMessengerPath2Photo.Value, this);
                return saved;
            }
            return false;
        }
        public void PrintPDFReceipt()
		{
            var abc = System.Net.Dns.GetHostEntry("localhost").HostName;
            string name1 = Environment.MachineName;
            string name2 = System.Net.Dns.GetHostName();
            string name3 = System.Windows.Forms.SystemInformation.ComputerName;
            string name4 = System.Environment.GetEnvironmentVariable("COMPUTERNAME");
            string[] computer_name = System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName.Split(new Char[] { '.' });
            string CustomerCode = "";
			string requestID = hdnViewRequestID.Value;
			string orderCode = hdnOrderCode.Value.ToString().Trim();

			NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

			DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
			if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
			{
				objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
				objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
				CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
			}

			DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);

			objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
			objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
			CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
			var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
			var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
			var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
			var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
			objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

			string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
			string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
			string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
			string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
			string notInspectedQuantity = totalQ;
			string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
			string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
			string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
			string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
			string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
			string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
			string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();

			string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
			string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
			string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
			string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
			string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
			string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
			string inspectedUnit = "", notInspectedUnit = "";
			string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();

			//Take In
			string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
			string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
			string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
			string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
			string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
			string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
			string TakeInOrShipReceivingBy = string.Empty;
			if (TakeInCarrierName != "")
				TakeInOrShipReceivingBy = "Ship Receiving";
			else
				TakeInOrShipReceivingBy = "Take In";


			//Give Out
			string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
			string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
			string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
			string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
			string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
			string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
			string GiveOutOrShipOutBy = string.Empty;
			if (GiveOutCarrierName != "")
				GiveOutOrShipOutBy = "Ship Out";
			else
				GiveOutOrShipOutBy = "Take Out";

			OrderModel objOrderModel = new OrderModel();
			objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
			var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
			objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

			string groupMemoNumbers = null;
			if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
				groupMemoNumbers = "";
			else
			{
				for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
					groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
			}
			groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);

			string groupCode = orderCode;
			string filename = @"label_FrontNew_Take_In_" + groupCode + ".xml";

			var custCode = CustomerCode;
			string OrderBy = "";
			if (TabContainer1.ActiveTabIndex == 0)
			{
				if (!string.IsNullOrEmpty(PickedUpByOurMessenger))
					OrderBy = "TakeInPickedUpByOurMessenger";
				else
					OrderBy = "TakeIn";
			}
			else if (TabContainer1.ActiveTabIndex == 1)
			{
				OrderBy = "TakeOut";
			}

			if (inspectedUnitID == "1")
				inspectedUnit = "g";
			else if (inspectedUnitID == "2")
				inspectedUnit = "ct.";
			else if (inspectedUnitID == "3")
				inspectedUnit = "abst.";
			else if (inspectedUnitID == "4")
				inspectedUnit = "enum";
			else if (inspectedUnitID == "5")
				inspectedUnit = "string";
			if (notInspectedUnitID == "1")
				notInspectedUnit = "g";
			else if (notInspectedUnitID == "2")
				notInspectedUnit = "ct.";
			else if (notInspectedUnitID == "3")
				notInspectedUnit = "abst.";
			else if (notInspectedUnitID == "4")
				notInspectedUnit = "enum";
			else if (notInspectedUnitID == "5")
				notInspectedUnit = "string";
            // Create a Document object
            var document = new Document(PageSize.A4, 15, 15, 15, 15);
			float PageWidth = 95f;
			// Create a new PdfWrite object, writing the output to a MemoryStream
			var output = new MemoryStream();
			var writer = PdfWriter.GetInstance(document, output);
            // Open the Document for writing
            document.Open();

            // First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
            var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
            var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
            var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
            var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
            var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
            var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var bodyFontSmall = FontFactory.GetFont("Arial", 8, Font.NORMAL);

            // Finally, add an image in the upper right corner
            var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/logo.gif"));
            logo.SetAbsolutePosition(30, 760);
            logo.ScalePercent(60);
            document.Add(logo);

            // Add the Receipt title
            var rptHeader = new Paragraph("Gemological Science International", headerFont);

            rptHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(rptHeader);

            var repSubHeader = new Paragraph("www.gemscience.net", bodyFont);
            repSubHeader.Alignment = Element.ALIGN_CENTER;
            document.Add(repSubHeader);
            document.Add(Chunk.NEWLINE);

            PdfPTable table = new PdfPTable(3);
            table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            PdfPCell cellNY = new PdfPCell(new Phrase("New York Headquarters", FontFactory.GetFont("Arial", 12, Font.BOLD)));
            cellNY.Border = iTextSharp.text.Rectangle.NO_BORDER;
            PdfPCell cellMB = new PdfPCell(new Phrase("Mumbai", FontFactory.GetFont("Arial", 12, Font.BOLD)));
            cellMB.Border = iTextSharp.text.Rectangle.NO_BORDER;
            PdfPCell cellRG = new PdfPCell(new Phrase("Ramat Gan", FontFactory.GetFont("Arial", 12, Font.BOLD)));
            cellRG.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPCell cellNYadd = new PdfPCell(new Phrase("581 5th Avenue, Fourth floor,\nNew York, New York, 10017 \nPhone: +1(212) 207 - 4140 \nFax: +1(212) 207 - 4156", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
            cellNYadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPCell cellMBadd = new PdfPCell(new Phrase("Gemological Science International Pvt. Ltd. \n601 - B, 6th Floor, Trade \nCenter BKC, Bandra(East) \nMumbai - 400051 \nPhone: +91(022) 26520354 \nFax: +91(022) 26524091 / 26527209", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
            cellMBadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPCell cellRGadd = new PdfPCell(new Phrase("GW Gemological Science Israel Ltd \n54 Bezalel Street, \nRamat Gan, 52521, Israel \nPhone: +972 03 - 752 - 3272", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
            cellRGadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

            table.SetTotalWidth(new float[] { 300f, 410f, 340f });
            table.AddCell(cellNY);
            table.AddCell(cellMB);
            table.AddCell(cellRG);
            table.AddCell(cellNYadd);
            table.AddCell(cellMBadd);
            table.AddCell(cellRGadd);
            document.Add(new Paragraph("\n"));
            table.WidthPercentage = PageWidth;
            document.Add(table);

            // Add the Order Barcode
            Barcode128 bc = new Barcode128();
            bc.TextAlignment = Element.ALIGN_LEFT;
            bc.Code = orderCode;
            bc.StartStopText = false;
            bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
            bc.Extended = true;
            bc.AltText = "";
            bc.X = 1.5f;
            //bc.BarHeight = 10f;
            bc.BarHeight = (bc.BarcodeSize.Width) * (float)(0.15);


            PdfContentByte cb = writer.DirectContent;
            iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
            patImage.SetAbsolutePosition(30, 600);
            patImage.ScaleAbsolute(100, 40);
            //patImage.ScalePercent(100);
            document.Add(patImage);

            PdfPCell barcodeBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
            barcodeBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
            // Add the Receipt Order
            var rptOrder = new Paragraph("Order   " + orderCode, FontFactory.GetFont("Arial", 16, Font.BOLD));
            rptOrder.SpacingBefore = 10;

            // Add the Memo
            var rptMemo = new Paragraph("Main Memo:   " + memo, FontFactory.GetFont("Arial", 14, Font.BOLD));
            rptMemo.SpacingBefore = 10;

            PdfPTable tableBarcode = new PdfPTable(3);
            tableBarcode.SetTotalWidth(new float[] { 300f, 310f, 440f });
            tableBarcode.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tableBarcode.AddCell(barcodeBlank);
            tableBarcode.AddCell(rptOrder);
            tableBarcode.AddCell(rptMemo);
            document.Add(new Paragraph("\n"));
            document.Add(new Paragraph("\n"));
            tableBarcode.WidthPercentage = PageWidth;
            document.Add(tableBarcode);


            //Add Customer Detail
            string customerAddress = customerName + ",\n" + address + ",\n" + city + ", " + stateName + ",\n" + country + "-" + zip + ",\n" + phone + ",\n" + fax;
            PdfPCell cell = new PdfPCell(new Phrase(customerAddress, FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
            cell.SetLeading(1, 1.2f);

            PdfPTable tableNoOfItem = new PdfPTable(3);

            PdfPCell cellAcceptAt = new PdfPCell(new Phrase("Accepted at: " + orderCreateDate, FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellAcceptAt.Colspan = 3;
            cellAcceptAt.HorizontalAlignment = 0;
            cellAcceptAt.FixedHeight = 25f;
            cellAcceptAt.Border = Rectangle.BOTTOM_BORDER;

            PdfPCell cellNoOfItems = new PdfPCell(new Phrase("Number Of Items:\n(Not Inspected)", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellNoOfItems.Colspan = 2;
            cellNoOfItems.FixedHeight = 30f;
            cellNoOfItems.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
            PdfPCell cellNoOfItemsVal = new PdfPCell(new Phrase(totalQ, FontFactory.GetFont("Arial", 13, Font.BOLDITALIC)));
            cellNoOfItemsVal.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
            cellNoOfItemsVal.Padding = 5;

            PdfPCell cellTotalWeight = new PdfPCell(new Phrase("Total Weight:\n(Not Inspected)", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellTotalWeight.Colspan = 2;
            cellTotalWeight.FixedHeight = 30f;
            cellTotalWeight.Border = Rectangle.TOP_BORDER;
            PdfPCell cellTotalWeightVal = new PdfPCell(new Phrase(totalW + "  ct.", FontFactory.GetFont("Arial", 13, Font.BOLDITALIC)));
            cellTotalWeightVal.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
            cellTotalWeightVal.Padding = 5;


            //tableNoOfItem.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tableNoOfItem.AddCell(cellAcceptAt);
            tableNoOfItem.AddCell(cellNoOfItems);
            tableNoOfItem.AddCell(cellNoOfItemsVal);
            tableNoOfItem.AddCell(cellTotalWeight);
            tableNoOfItem.AddCell(cellTotalWeightVal);


            PdfPTable tableDetail = new PdfPTable(2);
            tableDetail.SetTotalWidth(new float[] { 600f, 400f });
            tableDetail.AddCell(cell);
            tableDetail.AddCell(tableNoOfItem);
            document.Add(new Paragraph("\n"));
            tableDetail.WidthPercentage = PageWidth;
            document.Add(tableDetail);

            //Add Instruction and memo

            PdfPCell cellInstruction = new PdfPCell(new Phrase("Special Instructions:", new Font(bodyFont)));
            cellInstruction.FixedHeight = 70f;
            cellInstruction.VerticalAlignment = Element.ALIGN_MIDDLE;
            PdfPCell cellInstructionVal = new PdfPCell(new Phrase(specialInstr, new Font(bodyFont)));

            PdfPCell cellMemo = new PdfPCell(new Phrase("Memo Numbers:", new Font(bodyFont)));
            cellMemo.FixedHeight = 70f;
            cellMemo.VerticalAlignment = Element.ALIGN_MIDDLE;
            PdfPCell cellMemoVal = new PdfPCell(new Phrase(memo, new Font(bodyFont)));

            PdfPTable tableInstruction = new PdfPTable(2);
            tableInstruction.SetTotalWidth(new float[] { 200f, 800f });
            tableInstruction.AddCell(cellInstruction);
            tableInstruction.AddCell(cellInstructionVal);
            tableInstruction.AddCell(cellMemo);
            tableInstruction.AddCell(cellMemoVal);
            document.Add(new Paragraph("\n"));
            tableInstruction.WidthPercentage = PageWidth;
            document.Add(tableInstruction);

            //Add Takein TakeOut
            PdfPTable tableInOut = new PdfPTable(2);
            tableInOut.SetTotalWidth(new float[] { 500f, 500f });
            tableInOut.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPTable tableIn = new PdfPTable(2);
            tableIn.SetTotalWidth(new float[] { 130f, 370f });
            PdfPTable tableOut = new PdfPTable(2);
            tableOut.SetTotalWidth(new float[] { 130f, 370f });
            PdfPCell cellTakeIn = new PdfPCell(new Phrase(TakeInOrShipReceivingBy, new Font(bodyFont)));
            cellTakeIn.Border = Rectangle.RIGHT_BORDER;
            cellTakeIn.Colspan = 2;
            cellTakeIn.HorizontalAlignment = 1;


            PdfPCell cellTakeOut = new PdfPCell(new Phrase(GiveOutOrShipOutBy, new Font(bodyFont)));
            cellTakeOut.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellTakeOut.HorizontalAlignment = 1;
            cellTakeOut.Colspan = 2;


            PdfPCell cellTakeInrow = new PdfPCell(new Phrase("\n\n", new Font(bodyFont)));
            cellTakeInrow.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellTakeInrow.Border = Rectangle.RIGHT_BORDER;
            cellTakeInrow.HorizontalAlignment = 1;
            PdfPCell cellTakeOutrow = new PdfPCell(new Phrase("\n\n", new Font(bodyFont)));
            cellTakeOutrow.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellTakeOutrow.HorizontalAlignment = 1;


            PdfPCell cellSubmitedBy = new PdfPCell(new Phrase("Submitted By:", new Font(bodyFont)));
            cellSubmitedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellSubmitedBy.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellSubmitedBy.FixedHeight = 50f;

            PdfPCell cellAcceptedBy = new PdfPCell(new Phrase("Accepted By:", new Font(bodyFont)));
            cellAcceptedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellAcceptedBy.VerticalAlignment = Element.ALIGN_TOP;
            cellAcceptedBy.FixedHeight = 50f;
            cellAcceptedBy.PaddingTop = 20f;

            tableIn.AddCell(cellTakeIn);
            tableOut.AddCell(cellTakeOut);
            tableIn.AddCell(cellSubmitedBy);
            tableOut.AddCell(cellAcceptedBy);

            if (PickedUpByOurMessenger != "")
            {
                PdfPCell cellPickedUp = new PdfPCell(new Phrase("\n\nPicked Up By Our Messenger\n\n\n", new Font(bodyFont)));
                cellPickedUp.Border = Rectangle.RIGHT_BORDER;
                tableIn.AddCell(cellPickedUp);
            }
            else if (TakeInPersonName != "")
            {
                if (TakeInCaptureSignature != "")
                {
                    iTextSharp.text.Image SubmitedBySign = iTextSharp.text.Image.GetInstance(TakeInCaptureSignature);
                    SubmitedBySign.SetAbsolutePosition(120, 230);
                    SubmitedBySign.ScaleAbsolute(100, 100);
                    document.Add(SubmitedBySign);
                    PdfPCell SubmitedBySignBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
                    SubmitedBySignBlank.Border = Rectangle.RIGHT_BORDER;
                    SubmitedBySignBlank.FixedHeight = 70f;
                    tableIn.AddCell(SubmitedBySignBlank);
                }
                else
                {
                    PdfPCell SubmitedBySign = new PdfPCell(new Phrase("", new Font(bodyFont)));
                    SubmitedBySign.Border = Rectangle.RIGHT_BORDER;
                    tableIn.AddCell(SubmitedBySign);
                }
                PdfPCell CarrierName = new PdfPCell(new Phrase("", new Font(bodyFont)));
                CarrierName.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tableIn.AddCell(CarrierName);

                PdfPCell TakeInPersonNamecell = new PdfPCell(new Phrase("\n" + TakeInPersonName + "\n\n\n", new Font(bodyFont)));
                TakeInPersonNamecell.Border = Rectangle.RIGHT_BORDER;
                TakeInPersonNamecell.VerticalAlignment = Element.ALIGN_MIDDLE;
                TakeInPersonNamecell.PaddingLeft = 45f;
                tableIn.AddCell(TakeInPersonNamecell);

            }
            else if (TakeInCarrierName != "")
            {
                PdfPCell TrackingNumber = new PdfPCell(new Phrase("\n\n" + TakeInCarrierName + "\nTracking Number: " + TakeInCarrierTrackingNumber + "\n\n\n", new Font(bodyFont)));
                TrackingNumber.Border = Rectangle.RIGHT_BORDER;
                tableIn.AddCell(TrackingNumber);
            }
            else
            {
                PdfPCell TrackingNumber = new PdfPCell(new Phrase("", new Font(bodyFont)));
                TrackingNumber.Border = Rectangle.RIGHT_BORDER;
                tableIn.AddCell(TrackingNumber);
            }
            //Give Out /Ship Out
            //if (TakenOutByOurMessenger != "")
            if (!(String.IsNullOrEmpty(TakenOutByOurMessenger) || TakenOutByOurMessenger == "0"))
            {
                PdfPCell cellPickedUp = new PdfPCell(new Phrase("\n\nTaken Out By Our Messenger\n\n\n", new Font(bodyFont)));
                cellPickedUp.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tableOut.AddCell(cellPickedUp);
            }
            else if (GiveOutPersonName != "")
            {
                if (GiveOutCaptureSignature != "")
                {
                    var AcceptedBySign = iTextSharp.text.Image.GetInstance(GiveOutCaptureSignature);
                    AcceptedBySign.SetAbsolutePosition(390, 240);
                    AcceptedBySign.ScaleAbsolute(100, 100);
                    document.Add(AcceptedBySign);
                    PdfPCell AcceptedBySignBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
                    AcceptedBySignBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    AcceptedBySignBlank.FixedHeight = 70f;
                    tableOut.AddCell(AcceptedBySignBlank);
                }
                else
                {
                    PdfPCell AcceptedBySign = new PdfPCell(new Phrase("", new Font(bodyFont)));
                    tableOut.AddCell(AcceptedBySign);
                }
                PdfPCell CarrierName = new PdfPCell(new Phrase("", new Font(bodyFont)));
                CarrierName.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tableOut.AddCell(CarrierName);

                PdfPCell GiveOutPersonNamecell = new PdfPCell(new Phrase("\n" + GiveOutPersonName + "\n\n\n", new Font(bodyFont)));
                GiveOutPersonNamecell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                GiveOutPersonNamecell.VerticalAlignment = Element.ALIGN_MIDDLE;
                GiveOutPersonNamecell.PaddingLeft = 40f;
                tableOut.AddCell(GiveOutPersonNamecell);
            }
            else if (GiveOutCarrierName != "")
            {

                PdfPCell TrackingNumber = new PdfPCell(new Phrase("\n\n" + GiveOutCarrierName + "\nTracking Number: " + GiveOutCarrierTrackingNumber + "\n\n\n", new Font(bodyFont)));
                TrackingNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tableOut.AddCell(TrackingNumber);
            }
            else
            {
                PdfPCell TrackingNumber = new PdfPCell(new Phrase("", new Font(bodyFont)));
                TrackingNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tableOut.AddCell(TrackingNumber);
            }

            PdfPCell cellTakeInAcceptedBy = new PdfPCell(new Phrase("Accepted By:", new Font(bodyFont)));
            cellTakeInAcceptedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tableIn.AddCell(cellTakeInAcceptedBy);
            PdfPCell cellTakeInAcceptedByVal = new PdfPCell(new Phrase(TakeInUserName, new Font(bodyFont)));
            cellTakeInAcceptedByVal.Border = Rectangle.RIGHT_BORDER;
            tableIn.AddCell(cellTakeInAcceptedByVal);

            PdfPCell cellGiveOutSubmittedBy = new PdfPCell(new Phrase("Submitted By:", new Font(bodyFont)));
            cellGiveOutSubmittedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tableOut.AddCell(cellGiveOutSubmittedBy);
            PdfPCell cellGiveOutSubmittedByVal = new PdfPCell(new Phrase(GiveOutUserName, new Font(bodyFont)));
            cellGiveOutSubmittedByVal.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tableOut.AddCell(cellGiveOutSubmittedByVal);



            tableInOut.AddCell(tableIn);
            tableInOut.AddCell(tableOut);

            document.Add(new Paragraph("\n"));
            tableInOut.WidthPercentage = PageWidth;
            document.Add(tableInOut);
            document.Add(new Paragraph("\n"));
            document.Add(new Paragraph("\n"));

            string Note = "The diamonds  have been purchased from legitimate sources not involved in funding conflict and in compliance with United Nations ";
            Note += "resolutions.The submitter hereby guarantees that these diamonds are conflict free, based on personal knowledge and/ or written ";
            Note += "guarantees provided by the supplier of these diamonds.";

            PdfPTable tableNote = new PdfPTable(1);
            PdfPCell cellNote = new PdfPCell(new Phrase(Note, new Font(bodyFont)));
            cellNote.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellNote.SetLeading(1, 1.2f);
            tableNote.AddCell(cellNote);
            tableNote.WidthPercentage = PageWidth;
            document.Add(tableNote);
            //document.Add(new Paragraph(Note, bodyFont));

			//Add Customer Detail


            document.Close();

            // alex create xml
            //var stream = new MemoryStream();
            var stream1 = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            //using (XmlWriter xmlWriter = XmlWriter.Create(stream1))
            //{
            XmlWriter xmlWriter = null;
            //xmlWriter = XmlWriter.Create(@"c:\temp\alextemp.xml");
            xmlWriter = XmlWriter.Create(stream1);


            xmlWriter.WriteStartDocument();
                //xmlWriter.WriteStartElement("labels");
                xmlWriter.WriteStartElement("extLabel");
            xmlWriter.WriteElementString("customerAddress", customerAddress); //0
            xmlWriter.WriteElementString("orderCreateDate", orderCreateDate);//1
            xmlWriter.WriteElementString("totalQ", totalQ); //2
            xmlWriter.WriteElementString("totalW", totalW);//3

            xmlWriter.WriteElementString("specialInstr", specialInstr);//4
            xmlWriter.WriteElementString("memo", memo);//5
            xmlWriter.WriteElementString("TakeInOrShipReceivingBy", TakeInOrShipReceivingBy);//6
            xmlWriter.WriteElementString("GiveOutOrShipOutBy", GiveOutOrShipOutBy);//7
            xmlWriter.WriteElementString("PickedUpByOurMessenger", PickedUpByOurMessenger);//8
            xmlWriter.WriteElementString("TakeInCaptureSignature", TakeInCaptureSignature);//9
            xmlWriter.WriteElementString("TakeInPersonName", TakeInPersonName);//10
            xmlWriter.WriteElementString("TakeInCarrierName", TakeInCarrierName);//11
            xmlWriter.WriteElementString("TakenOutByOurMessenger", TakenOutByOurMessenger);//12
            xmlWriter.WriteElementString("GiveOutPersonName", GiveOutPersonName);//13
            xmlWriter.WriteElementString("GiveOutCaptureSignature", GiveOutCaptureSignature);//14
            xmlWriter.WriteElementString("GiveOutCarrierTrackingNumber", GiveOutCarrierTrackingNumber);//15
            xmlWriter.WriteElementString("TakeInUserName", TakeInUserName);//16
            xmlWriter.WriteElementString("GiveOutUserName", GiveOutUserName);//17
            xmlWriter.WriteElementString("orderCode", orderCode);
            xmlWriter.WriteElementString("TakeInCarrierTrackingNumber", TakeInCarrierTrackingNumber);
            xmlWriter.WriteElementString("GiveOutCarrierName", GiveOutCarrierName);
            xmlWriter.WriteElementString("requestID", requestID);
                
                xmlWriter.WriteEndElement();

                
                //xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                //this.Session["Memory"] = stream1;
                xmlWriter.Flush();
                xmlWriter.Close();
            //alex
            //DataTable dataInputs = new DataTable("ExtLabel");
            //dataInputs.Columns.Add("customerAddress", typeof(String));
            //dataInputs.Columns.Add("orderCreateDate", typeof(String));
            //dataInputs.Columns.Add("totalQ", typeof(String));
            //dataInputs.Columns.Add("totalW", typeof(String));
            //dataInputs.Columns.Add("specialInstr", typeof(String));
            //dataInputs.Columns.Add("memo", typeof(String));
            //dataInputs.Columns.Add("TakeInOrShipReceivingBy", typeof(String));
            //dataInputs.Columns.Add("GiveOutOrShipOutBy", typeof(String));
            //dataInputs.Columns.Add("PickedUpByOurMessenger", typeof(String));
            //dataInputs.Columns.Add("TakeInCaptureSignature", typeof(String));
            //dataInputs.Columns.Add("TakeInPersonName", typeof(String));
            //dataInputs.Columns.Add("TakeInCarrierName", typeof(String));
            //dataInputs.Columns.Add("TakenOutByOurMessenger", typeof(String));
            //dataInputs.Columns.Add("GiveOutPersonName", typeof(String));
            //dataInputs.Columns.Add("GiveOutCaptureSignature", typeof(String));
            //dataInputs.Columns.Add("GiveOutCarrierTrackingNumber", typeof(String));
            //dataInputs.Columns.Add("TakeInUserName", typeof(String));
            //dataInputs.Columns.Add("GiveOutUserName", typeof(String));
            //dataInputs.Columns.Add("orderCode", typeof(String));
            //dataInputs.Columns.Add("TakeInCarrierTrackingNumber", typeof(String));
            //dataInputs.Columns.Add("GiveOutCarrierName", typeof(String));
            //dataInputs.Columns.Add("requestID", typeof(String));
            //DataRow row;
            //    row = dataInputs.NewRow();
            //    row[0] = customerAddress;
            //    row[1] = orderCreateDate;
            //    row[2] = totalQ;
            //    row[3] = totalW;
            //    row[4] = specialInstr;
            //    row[5] = memo;
            //    row[6] = TakeInOrShipReceivingBy;
            //    row[7] = GiveOutOrShipOutBy;
            //    row[8] = PickedUpByOurMessenger;
            //    row[9] = TakeInCaptureSignature;
            //    row[10] = TakeInPersonName;
            //    row[11] = TakeInCarrierName;
            //    row[12] = TakenOutByOurMessenger;
            //    row[13] = GiveOutPersonName;
            //    row[14] = GiveOutCaptureSignature;
            //    row[15] = GiveOutCarrierTrackingNumber;
            //    row[16] = TakeInUserName;
            //    row[17] = GiveOutUserName;
            //    row[18] = orderCode;
            //    row[19] = TakeInCarrierTrackingNumber;
            //    row[20] = GiveOutCarrierName;
            //    row[21] = requestID;

            //    dataInputs.Rows.Add(row);
           

            //using (MemoryStream ms = new MemoryStream())
            //{
            //    dataInputs.WriteXml(ms);
            //    bool sent = QueryUtils.SendMessageToStorage(ms, this);
                
            //}
            //alex
            bool msgSent = QueryUtils.SendMessageToStorage(stream1, this);
            var streamArray = stream1.ToArray();
            Response.Clear();
            //var dir = GetExportDirectory(p);
            //-- Download
            //MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            Response.ContentType = "text/plain";
            //Response.AddHeader("Refresh", "0.1");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.AppendHeader("Content-Length", streamArray.Length.ToString());
            Response.BinaryWrite(stream1.ToArray());
            //}
            //DownloadExcelFile(filename, this);
            //alex create xml
            //DownloadExcelFile1(filename, this);

   //         this.Response.Clear();
			//this.Response.ContentType = "application/pdf";
			//this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=External_Receipt_Order-{0}.pdf", orderCode));
			//this.Response.BinaryWrite(output.ToArray());
			//this.Response.End();
		}//PrintPDFReceipt

        public void PrintPDFReceipt1(string printAction, string prtSource, string nOfCopies, string direction = "takeIn")
        {
            string machineName = null;
            if (Session["MachineName"] != null)
                machineName = Session["MachineName"].ToString();
            else
            {
                lblCreateRequestMessege.Text = "No machine name: Cannot print.";
                return;
            }
            string CustomerCode = "";
            string requestID = "";
            if (prtSource == "create")
            {
                requestID = hdnRequestID.Value;
                hdnViewRequestID.Value = "";
            }
            else
            {
                requestID = hdnViewRequestID.Value;
                hdnRequestID.Value = "";
            }
            string orderCode = hdnOrderCode.Value.ToString().Trim();
            if (orderCode == "" || orderCode == "0")
            {
                PopupInfoDialog("No order code found", true);
                return;
            }
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

            DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
            if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
            {
                objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
                objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
                CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
            }
            string messengerId = hdnMessengerTakeIn.Value;
            DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);
            if (dsOrderSummery == null || dsOrderSummery.Rows.Count == 0)
            {
                PopupInfoDialog("No record found for order " + orderCode, true);
                return;
            }
            objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
            objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
            CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
            var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
            var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
            var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
            var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
            objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

            string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
            string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
            string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
            string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
            string notInspectedQuantity = totalQ;
            string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
            string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
            string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
            string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
            string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
            string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
            string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();
            string serviceTypeID = dsOrderSummery.Rows[0]["ServiceTypeID"].ToString();
            string syntheticServiceTypeCode = dsOrderSummery.Rows[0]["SyntheticServiceTypeCode"].ToString();

            if (serviceTypeID == "1")
                serviceTypeID = "L";
            else if (serviceTypeID == "12")//Q
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG/Q";
                else
                {
                    if (CustomerCode == "1599")
                        serviceTypeID = "S";
                    else
                        serviceTypeID = "Q";
                }
            }
            else if (serviceTypeID == "9")
                serviceTypeID = "V";
            else if (serviceTypeID == "13")//Both
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG/Q";
                else
                    //alex removed 08102023
                {
                //    if (CustomerCode == "1599")
                //        serviceTypeID = "S";
                //    else
                        serviceTypeID = "B";
                }
            }
            else if (serviceTypeID == "7")//Screening
            {
                if (syntheticServiceTypeCode == "11" || syntheticServiceTypeCode == "12")//Red bag
                    serviceTypeID = "LG";
                else
                    serviceTypeID = "S";
            }
            string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
            if (sPersonCustomerOfficeID == "")
                sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["CustomerOfficeID"].ToString();
            string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
            string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
            string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
            string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();
            string email = dsOrderSummery.Rows[0]["Email"].ToString();
            //Take In
            string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
            string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
            string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
            string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
            string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
            string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
            string TakeInOrShipReceivingBy = string.Empty;
            if (TakeInCarrierName != "")
                TakeInOrShipReceivingBy = "Ship Receiving";
            else
                TakeInOrShipReceivingBy = "Take In";

            //Give Out
            string GiveOutPersonID = dsOrderSummery.Rows[0]["GiveOutPersonID"].ToString();
            string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
            string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
            string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
            string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
            string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
            //string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
            string GiveOutUserName = TakeInUserName;
            string GiveOutOrShipOutBy = string.Empty;
            if (GiveOutCarrierName != "")
                GiveOutOrShipOutBy = "Ship Out";
            else
                GiveOutOrShipOutBy = "Take Out";
            string customerAddress = customerName + ",\n" + address + ",\n" + city + ", " + stateName + ",\n" + country + "-" + zip + ",\n" + phone + ",\n" + fax;
            OrderModel objOrderModel = new OrderModel();
            objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
            var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
            objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

            string groupMemoNumbers = null;
            if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
                groupMemoNumbers = "";
            else
            {
                for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
                    groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
            }
            groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);
            string BNCategoryName = "", BNDestinationName = "";
            if (dsOrderSummery.Columns.Contains("BNCategoryName"))
            {
                BNCategoryName = dsOrderSummery.Rows[0]["BNCategoryName"].ToString();
                BNDestinationName = dsOrderSummery.Rows[0]["BNDestinationName"].ToString();
            }
            //create xml
            //string machineName = abc;
            
            DataTable dataInputs = new DataTable("ExtLabel");
            dataInputs.Columns.Add("customerAddress", typeof(String));
            dataInputs.Columns.Add("orderCreateDate", typeof(String));
            dataInputs.Columns.Add("totalQ", typeof(String));
            dataInputs.Columns.Add("totalW", typeof(String));
            dataInputs.Columns.Add("specialInstr", typeof(String));
            dataInputs.Columns.Add("memo", typeof(String));
            dataInputs.Columns.Add("TakeInOrShipReceivingBy", typeof(String));
            dataInputs.Columns.Add("GiveOutOrShipOutBy", typeof(String));
            dataInputs.Columns.Add("PickedUpByOurMessenger", typeof(String));
            dataInputs.Columns.Add("TakeInCaptureSignature", typeof(String));
            dataInputs.Columns.Add("TakeInPersonName", typeof(String));
            dataInputs.Columns.Add("TakeInCarrierName", typeof(String));
            dataInputs.Columns.Add("TakenOutByOurMessenger", typeof(String));
            dataInputs.Columns.Add("GiveOutPersonName", typeof(String));
            dataInputs.Columns.Add("GiveOutCaptureSignature", typeof(String));
            dataInputs.Columns.Add("GiveOutCarrierTrackingNumber", typeof(String));
            dataInputs.Columns.Add("TakeInUserName", typeof(String));
            dataInputs.Columns.Add("GiveOutUserName", typeof(String));
            dataInputs.Columns.Add("orderCode", typeof(String));
            dataInputs.Columns.Add("TakeInCarrierTrackingNumber", typeof(String));
            dataInputs.Columns.Add("GiveOutCarrierName", typeof(String));
            dataInputs.Columns.Add("requestID", typeof(String));
            dataInputs.Columns.Add("GroupMemoNumbers", typeof(String));
            dataInputs.Columns.Add("PrintAction", typeof(String));
            dataInputs.Columns.Add("NumberOfCopies", typeof(String));
            dataInputs.Columns.Add("MessengerID", typeof(String));
            dataInputs.Columns.Add("Email", typeof(String));
            dataInputs.Columns.Add("OfficeID", typeof(String));
            dataInputs.Columns.Add("GiveOutMessengerID", typeof(String));
            dataInputs.Columns.Add("ServiceTypeID", typeof(String));
            dataInputs.Columns.Add("BNCategoryName", typeof(String));
            dataInputs.Columns.Add("BNDestinationName", typeof(String));
            if (machineName != null && machineName != "")
                dataInputs.Columns.Add("MachineName", typeof(String));
            DataRow row;
            row = dataInputs.NewRow();
            row[0] = customerAddress;
            row[1] = orderCreateDate;
            row[2] = totalQ;
            row[3] = totalW;
            row[4] = specialInstr;
            row[5] = memo;
            row[6] = TakeInOrShipReceivingBy;
            row[7] = GiveOutOrShipOutBy;
            row[8] = PickedUpByOurMessenger;
            row[9] = TakeInCaptureSignature;
            row[10] = TakeInPersonName;
            row[11] = TakeInCarrierName;
            row[12] = TakenOutByOurMessenger;
            row[13] = GiveOutPersonName;
            row[14] = GiveOutCaptureSignature;
            row[15] = GiveOutCarrierTrackingNumber;
            row[16] = TakeInUserName;
            row[17] = GiveOutUserName;
            row[18] = orderCode;
            row[19] = TakeInCarrierTrackingNumber;
            row[20] = GiveOutCarrierName;
            row[21] = requestID;
            row[22] = groupMemoNumbers;
            row[23] = printAction;
            row[24] = nOfCopies;
            row[25] = messengerId;
            row[26] = email;
            row[27] = sPersonCustomerOfficeID;
            if (direction != "takeOut")
                GiveOutPersonID = "";
            row[28] = GiveOutPersonID;
            row[29] = serviceTypeID;
            row[30] = BNCategoryName;
            row[31] = BNDestinationName;
            if (machineName != null)
                row[32] = machineName;

            dataInputs.Rows.Add(row);


            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool sent = QueryUtils.SendMessageToStorage(ms, this);

            }
            dataInputs.Clear();
        }//PrintPDFReceipt1

        public void PrintBulkGiveOutLabel(string bulkRequestId, string customerName)
        {
            string machineName = null;
            if (Session["MachineName"] != null)
                machineName = Session["MachineName"].ToString();
            else
            {
                lblCreateRequestMessege.Text = "No machine name: Cannot print.";
                return;
            }
            

            //create xml
            //string machineName = abc;

            DataTable dataInputs = new DataTable("BulkGiveOutLabel");
            dataInputs.Columns.Add("BulkRequestId", typeof(String));
            dataInputs.Columns.Add("MachineName", typeof(String));
            dataInputs.Columns.Add("ItemsCount", typeof(String));
            dataInputs.Columns.Add("CustomerName", typeof(String));
            int dataCount = BulkGiveOutList.Items.Count;
            for (int i = 1; i <= dataCount; i++)
            {
                string name = "BulkData" + i.ToString();
                dataInputs.Columns.Add(name, typeof(String));
            }
            DataRow row;
            row = dataInputs.NewRow();
            row[0] = bulkRequestId;
            row[1] = machineName;
            row[2] = dataCount.ToString();
            row[3] = customerName;
            
            for (int i=1; i<=dataCount; i++)
            {
                int rowNum = i + 3;
                string dataValue = "";
                if (BulkGiveOutList.Items[i - 1].Text.Contains("-"))
                    dataValue = BulkGiveOutList.Items[i - 1].Text;
                else
                {
                    DataTable dsOrderSummery = FrontUtils.GetOrderSummery(BulkGiveOutList.Items[i - 1].Text, this);
                    string requestId = dsOrderSummery.Rows[0]["RequestID"].ToString();
                    dataValue = BulkGiveOutList.Items[i - 1].Text + "-" + requestId;
                }
                row[rowNum] = dataValue;
            }
            dataInputs.Rows.Add(row);


            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool sent = QueryUtils.SendMessageToStorage(ms, this, "Screening");

            }
            dataInputs.Clear();
        }//PrintPDFReceipt1
        public void PrintAcceptanceReceipt(List<SyntheticAcceptanceModel> requests, string customerId, string customerName, string messenger, 
            string carrier, string carrierTrackingNumber, string authorName)
        {
            try
            {
                string machineName = null;
                if (Session["MachineName"] != null)
                    machineName = Session["MachineName"].ToString();
                else
                {
                    lblCreateRequestMessege.Text = "No machine name: Cannot print.";
                    return;
                }
                string pName = null;
                if (Session["PName"] != null)
                    pName = Session["PName"].ToString();
                DataTable dataInputs = new DataTable("AcceptanceLabel");
                dataInputs.Columns.Add("CustomerName", typeof(String));
                dataInputs.Columns.Add("MachineName", typeof(String));
                dataInputs.Columns.Add("Messenger", typeof(String));
                dataInputs.Columns.Add("Carrier", typeof(String));
                dataInputs.Columns.Add("CarrierTrackingNumber", typeof(String));
                dataInputs.Columns.Add("Requests", typeof(String));
                dataInputs.Columns.Add("SumOfQty", typeof(String));
                dataInputs.Columns.Add("TotalItems", typeof(String));
                dataInputs.Columns.Add("AuthorName", typeof(String));
                dataInputs.Columns.Add("ServiceType", typeof(string));
                
                string serviceType = "7";
                if (Session["PrevServiceType"] != null)
                    serviceType = Session["PrevServiceType"].ToString();
                Session["PrevServiceType"] = null;
                string requestsMulti = "";
                int sumOfQty = 0;
                foreach (SyntheticAcceptanceModel request in requests)
                {
                    string memo = request.Memo;
                    string requestId = request.RequestID;
                    string totalQty = request.TotalQty;
                    string retailerName = request.RetailerName;
                    sumOfQty += Convert.ToInt16(totalQty);
                    string reqFormatted = FormatRequest(requestId, memo, totalQty, retailerName);
                    requestsMulti += reqFormatted + Environment.NewLine;
                    //requestsMulti +=  requestId + @"|" + memo + @"|" + totalQty + @"|" + retailerName + Environment.NewLine;
                }
                DataRow row;
                if (messenger == "" && carrier == "")
                {
                    messenger = "N/A";
                    carrier = "N/A";
                    carrierTrackingNumber = "N/A";
                }
                row = dataInputs.NewRow();
                row[0] = customerName;
                row[1] = machineName;
                row[2] = messenger;
                row[3] = carrier;
                row[4] = carrierTrackingNumber;
                row[5] = requestsMulti;
                row[6] = sumOfQty.ToString();
                row[7] = requests.Count.ToString();
                row[8] = authorName;
                row[9] = serviceType;
                dataInputs.Rows.Add(row);


                using (MemoryStream ms = new MemoryStream())
                {
                    string printLocal = null;
                    if (Session["PrintLocalAcceptance"] != null && Session["PrintLocalAcceptance"].ToString() == "1" && Session["PrintLocalMachines"] != null)
                    {
                        bool localPrinted = false;
                        string[] localMachines = Session["PrintLocalMachines"].ToString().Split(';');
                        foreach (string localMachine in localMachines)
                        {
                            if (localMachine == machineName)
                            {
                                localPrinted = true;
                                bool print = FrontUtils.PrintAcceptanceLabel(dataInputs, this);
                                break;
                            }
                        }
                        if (localPrinted == false)
                        {
                            dataInputs.WriteXml(ms);
                            bool sent = QueryUtils.SendMessageToStorage(ms, this, "Screening");
                        }
                    }
                    else
                    {
                        dataInputs.WriteXml(ms);
                        bool sent = QueryUtils.SendMessageToStorage(ms, this, "Screening");
                    }
                    

                }
                dataInputs.Clear();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                lblCreateRequestMessege.Text = "Error sending Acceptance email";
            }

        }//PrintAcceptanceReceipt

        public string FormatRequest(string requestId, string memo, string qty, string retailerName)
        {
            if (retailerName.Length > 15)
                retailerName = retailerName.Substring(0, 15);
            return FillToNChars(requestId.Trim(), 18) + FillToNChars(memo.Trim(), 17) + FillToNChars(qty.Trim(), 6) + retailerName;
        }
        public static string FillToNChars(string sNumber, int n)
        {
            while (sNumber.Length < n)
                sNumber = sNumber + ' ';
            return sNumber;
        }
        //public void Write(Stream from, Stream to)
        //{
        //    for (int a = from.ReadByte(); a != -1; a = from.ReadByte())
        //        to.WriteByte((byte)a);
        //}
        //public void DownloadExcelFile1(String filename, Page p)
        //{
        //    p.Response.Clear();
        //    //var dir = GetExportDirectory(p);
        //    //-- Download
        //    MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
        //    p.Response.ContentType = "text/plain";
        //    p.Response.AddHeader("Refresh", "0.1");
        //    p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
        //    p.Response.BinaryWrite(msMemory.ToArray());
        //    //p.Response.TransmitFile(filename);
        //    //p.Response.TransmitFile(filename);
        //    //p.Response.End();
        //}

        //public void DownloadExcelFile(String filename, Page p)
        //{
        //    MemoryStream ms = (MemoryStream)p.Session["Memory"];
        //    ms.Position = 0;
        //    StreamReader sr = new StreamReader(ms);
        //    string myStr = sr.ReadToEnd();
        //    //p.Response.Clear();
        //    ////p.Response.Buffer = true; //alex
        //    //MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
        //    ////var dir = GetExportDirectory(p);
        //    ////-- Download
        //    //p.Response.ContentType = "text/plain";
        //    //p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
        //    //p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
        //    //p.Response.BinaryWrite(msMemory.ToArray());
        //}

        public void PrintPDFLabel()
		{
			string CustomerCode = "";
			string requestID = "";
			string orderCode = hdnOrderCode.Value.ToString().Trim();

			NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

			DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
			if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
			{
				objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
				objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
				CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
			}

			DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);

			objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
			objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
			CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
			var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
			var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
			var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
			var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
			objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

			string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
			string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
			string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
			string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
			string notInspectedQuantity = totalQ;
			string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
			string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
			string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
			string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
			string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
			string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
			string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();

			string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
			string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
			string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
			string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
			string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
			string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
			string inspectedUnit = "", notInspectedUnit = "";
			string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();

			//Take In
			string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
			string TakeInPositionName = dsOrderSummery.Rows[0]["PositionName"].ToString();
			string TakeInPersonPhone = dsOrderSummery.Rows[0]["Phone"].ToString();

			string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
			string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
			string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
			string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
			string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
			string TakeInOrShipReceivingBy = string.Empty;
			if (TakeInCarrierName != "")
				TakeInOrShipReceivingBy = "Ship Receiving";
			else
				TakeInOrShipReceivingBy = "Take In";

			//Give Out
			string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
			string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
			string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
			string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
			string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
			string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
			string GiveOutOrShipOutBy = string.Empty;
			if (GiveOutCarrierName != "")
				GiveOutOrShipOutBy = "Ship Out";
			else
				GiveOutOrShipOutBy = "Take Out";

			OrderModel objOrderModel = new OrderModel();
			objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
			var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
			objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

			string groupMemoNumbers = null;
			if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
				groupMemoNumbers = "";
			else
			{
				for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
					groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
			}
			groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);

			string groupCode = orderCode;
			string filename = @"label_FrontNew_Take_In_" + groupCode + ".xml";

			var custCode = CustomerCode;
			string OrderBy = "";
			if (TabContainer1.ActiveTabIndex == 0)
			{
				if (!string.IsNullOrEmpty(PickedUpByOurMessenger))
					OrderBy = "TakeInPickedUpByOurMessenger";
				else
					OrderBy = "TakeIn";
			}
			else if (TabContainer1.ActiveTabIndex == 1)
			{
				OrderBy = "TakeOut";
			}

			if (inspectedUnitID == "1")
				inspectedUnit = "g";
			else if (inspectedUnitID == "2")
				inspectedUnit = "ct.";
			else if (inspectedUnitID == "3")
				inspectedUnit = "abst.";
			else if (inspectedUnitID == "4")
				inspectedUnit = "enum";
			else if (inspectedUnitID == "5")
				inspectedUnit = "string";
			if (notInspectedUnitID == "1")
				notInspectedUnit = "g";
			else if (notInspectedUnitID == "2")
				notInspectedUnit = "ct.";
			else if (notInspectedUnitID == "3")
				notInspectedUnit = "abst.";
			else if (notInspectedUnitID == "4")
				notInspectedUnit = "enum";
			else if (notInspectedUnitID == "5")
				notInspectedUnit = "string";

			// Create a Document object
			var document = new Document(PageSize.A4, 15, 15, 15, 15);
			float PageWidth = 95f;
			// Create a new PdfWrite object, writing the output to a MemoryStream
			var output = new MemoryStream();
			var writer = PdfWriter.GetInstance(document, output);

			// Open the Document for writing
			document.Open();

			// First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
			var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
			var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
			var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
			var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
			var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
			var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
			var bodyFontSmall = FontFactory.GetFont("Arial", 8, Font.NORMAL);

			// Add the Order Barcode
			Barcode128 bc = new Barcode128();
			bc.TextAlignment = Element.ALIGN_LEFT;
			bc.Code = orderCode;
			bc.StartStopText = false;
			bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
			bc.Extended = true;
			//bc.AltText = "";
			bc.X = 1.5f;
			//bc.BarHeight = 10f;
			bc.BarHeight = (bc.BarcodeSize.Width) * (float)(0.15);


			PdfContentByte cb = writer.DirectContent;
			iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
			patImage.SetAbsolutePosition(30, 795);
			patImage.ScaleAbsolute(70, 30);
			document.Add(patImage);

			PdfPTable table = new PdfPTable(2);
			table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
			table.SetTotalWidth(new float[] { 200f, 800f });

			PdfPCell cellBlank = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.BOLD)));
			cellBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
			PdfPCell cellDate = new PdfPCell(new Phrase(orderCreateDate + "\n" + TakeInPositionName + "\n" + TakeInPersonPhone, FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellDate.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPCell cellCustomerID = new PdfPCell(new Phrase("\nCustomer ID #" + CustomerCode, FontFactory.GetFont("Arial", 18, Font.BOLD)));
			cellCustomerID.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellCustomerID.Colspan = 2;

			PdfPCell cellNoOfItems = new PdfPCell(new Phrase("Number of items:  " + totalQ + "_____not inspected", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellNoOfItems.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellNoOfItems.Colspan = 2;

			PdfPCell cellTotalWeight = new PdfPCell(new Phrase("Total weight:         " + totalW + "ct.   not inspected", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellTotalWeight.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellTotalWeight.Colspan = 2;

			PdfPCell cellMainMemo = new PdfPCell(new Phrase("Main Memo\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMainMemo.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMainMemo.Colspan = 2;

			PdfPCell cellMemoNumber = new PdfPCell(new Phrase("  Memo Number \n  " + memo + "\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMemoNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMemoNumber.Colspan = 2;

			PdfPCell cellMemoNumbers = new PdfPCell(new Phrase("  " + groupMemoNumbers.Replace(",", "\n  ") + "\n\n\n\n\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMemoNumbers.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMemoNumbers.Colspan = 2;


			PdfPCell cellSpecialInstruction = new PdfPCell(new Phrase("Special Instructions: \n" + specialInstr + "\n\n\n\n\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellSpecialInstruction.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellSpecialInstruction.Colspan = 2;

			PdfPCell cellDue = new PdfPCell(new Phrase("Due:", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellDue.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellDue.Colspan = 2;

			table.AddCell(cellBlank);
			table.AddCell(cellDate);
			table.AddCell(cellCustomerID);
			table.AddCell(cellNoOfItems);
			table.AddCell(cellTotalWeight);
			table.AddCell(cellMainMemo);
			table.AddCell(cellMemoNumber);
			table.AddCell(cellMemoNumbers);
			table.AddCell(cellSpecialInstruction);
			table.AddCell(cellDue);
			table.WidthPercentage = 95f;
			document.Add(table);

			document.Close();

			this.Response.Clear();
			this.Response.ContentType = "application/pdf";
			this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Label_Front-{0}.pdf", orderCode));
			this.Response.BinaryWrite(output.ToArray());
			this.Response.End();
		}

        //public void PrintPDFLabel1(string requestID)
        //{
        //    string orderCode = null;
        //    using (WebClient webClient = new WebClient())
        //    {
        //        var rootDir = "" + Session[SessionConstants.WebPictureShapeRoot];
        //        string fileName = "Label.pdf";
        //        var imgPath = rootDir + @"FrontInOut/" + fileName;
        //        byte[] data = webClient.DownloadData(imgPath);

        //        using (MemoryStream stream = new MemoryStream(data))
        //        {
        //            Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument(stream);
        //            orderCode = hdnOrderCode.Value.ToString().Trim();
        //            DataSet dsLabel = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestID, this);
        //            string memonum = "", poNum = "", style = "", sku = "", servicetypename = "", retailername = "", categoryname = "", servicetimename = "", customerCode = "",
        //                   jewelrytypename = "", contactname = "", carriername = "", messengername = "", deliverymethodname = "", gsiOrder = "", totalQty = "", specInstr = "", totalWeight = "";
        //            if (dsLabel != null && dsLabel.Tables[0].Rows.Count > 0)
        //            {
        //                DataRow dr = dsLabel.Tables[0].Rows[0];
        //                poNum = "" + dr["PONum"].ToString();
        //                style = "" + dr["StyleName"].ToString();
        //                sku = "" + dr["SKUName"].ToString();
        //                categoryname = "" + dr["CategoryName"].ToString();
        //                messengername = "" + dr["Messenger"].ToString();
        //                servicetypename = "" + dr["ServiceTypeName"].ToString();
        //                retailername = "" + dr["RetailerName"].ToString();
        //                deliverymethodname = "" + dr["DeliveryMethodName"].ToString();
        //                jewelrytypename = "" + dr["JewelryTypeName"].ToString();
        //                contactname = "" + dr["PersonName"].ToString();
        //                carriername = "" + dr["CarrierName"].ToString();
        //                //messengername = "" + dr["Messenger"].ToString();
        //                gsiOrder = "" + dr["GSIOrder"].ToString();
        //                totalQty = "" + dr["TotalQty"].ToString();
        //                specInstr = "" + dr["SpecialInstructions"].ToString();
        //                memonum = "" + dr["MemoNum"].ToString();
        //                customerCode = dr["CustomerCode"].ToString();
        //                totalWeight = dr["TotalWeight"].ToString();
        //                if (totalWeight != "")
        //                    jewelrytypename = "";
        //            }
        //            PdfFormWidget formWidget = doc.Form as PdfFormWidget;
        //            var campoText = null as PdfTextBoxFieldWidget;
        //            PdfFormFieldWidgetCollection myCollection = formWidget.FieldsWidget;
        //            if (orderCode != null)
        //            {
        //                campoText = formWidget.FieldsWidget["BarNumber"] as PdfTextBoxFieldWidget;
        //                campoText.Text = orderCode;
        //                campoText = formWidget.FieldsWidget["BarCode"] as PdfTextBoxFieldWidget;
        //                campoText.Text = "*" + orderCode + "*";
        //            }
        //            //string crzzz = "\x0A\x0B";
        //            /* alex
        //            string crzzz = "";
        //            crzzz += (char)10;
        //            crzzz += (char)13;



        //            string fieldNames = "", fieldData = "";
        //            if (memonum != "")
        //            {
        //                fieldNames += "Memo: " + crzzz;
        //                fieldData += memonum + crzzz;
        //                campoText = formWidget.FieldsWidget["Instructions"] as PdfTextBoxFieldWidget;
        //                campoText.Text = specInstr; //"Special Instructions";
        //            }
        //            if (poNum != "")
        //            {
        //                fieldNames += "PONum: " + crzzz;
        //                fieldData += poNum + crzzz;
        //            }
        //            if (memonum != "" || poNum != "")
        //            {
        //                fieldNames += crzzz;
        //                fieldData += crzzz;
        //            }
        //            if (style != "")
        //            {
        //                fieldNames += "Style: " + crzzz;
        //                fieldData += style + crzzz;
        //            }
        //            if (sku != "")
        //            {
        //                fieldNames += "SKU: " + crzzz;
        //                fieldData += sku + crzzz;
        //            }
        //            if (style != "" || sku != "")
        //            {
        //                fieldNames += crzzz;
        //                fieldData += crzzz;
        //            }

        //            if (totalQty != "")
        //            {
        //                fieldNames += "Total Quantity: " + crzzz;
        //                fieldData += totalQty + crzzz;
        //            }
        //            if (totalWeight != "")
        //            {
        //                fieldNames += "Total Weight: " + crzzz;
        //                fieldData += totalQty + crzzz;
        //            }
        //            if (retailername != "")
        //            {
        //                fieldNames += "Retailer: " + crzzz;
        //                fieldData += retailername + crzzz;
        //            }
        //            if (categoryname != "")
        //            {
        //                fieldNames += "Category: " + crzzz;
        //                fieldData += categoryname + crzzz;
        //            }
        //            if (servicetypename != "")
        //            {
        //                fieldNames += "Service Type: " + crzzz;
        //                fieldData += servicetypename + crzzz;
        //            }
        //            if (jewelrytypename != "")
        //            {
        //                fieldNames += "Jewelry Type: " + crzzz;
        //                fieldData += jewelrytypename + crzzz;
        //            }
        //            if (contactname != "")
        //            {
        //                fieldNames += "Contact: " + crzzz;
        //                fieldData += contactname + crzzz;
        //            }
        //            if (messengername != "")
        //            {
        //                fieldNames += "Messenger: " + crzzz;
        //                fieldData += messengername + crzzz;
        //            }
        //            if (carriername != "")
        //            {
        //                fieldNames += "Carrier: " + crzzz;
        //                fieldData += carriername + crzzz;
        //            }
        //            alex */
        //            //alex
        //            //campoText = formWidget.FieldsWidget["FieldNames"] as PdfTextBoxFieldWidget;
        //            //campoText.Text = fieldNames; //"Field Names";
        //            //campoText = formWidget.FieldsWidget["FieldData"] as PdfTextBoxFieldWidget;
        //            //campoText.Text = fieldData; //"Field data";
        //            campoText = formWidget.FieldsWidget["F1"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Memo: "; //memo label;
        //            campoText = formWidget.FieldsWidget["D1"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + memonum; //memo value;
        //            campoText = formWidget.FieldsWidget["F2"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "PONum: "; //ponum label;
        //            campoText = formWidget.FieldsWidget["D2"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + poNum; //poNum value;
        //            campoText = formWidget.FieldsWidget["F3"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Sku: "; //sku label;
        //            campoText = formWidget.FieldsWidget["D3"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + sku; //sku value;
        //            campoText = formWidget.FieldsWidget["F4"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Style: "; //style label;
        //            campoText = formWidget.FieldsWidget["D4"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + style; //style value;
        //            campoText = formWidget.FieldsWidget["F5"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Total Quantity: "; //total quantity label;
        //            campoText = formWidget.FieldsWidget["D5"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + totalQty; //total quantity value;
        //            campoText = formWidget.FieldsWidget["F6"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Total Weight: "; //total weight label;
        //            campoText = formWidget.FieldsWidget["D6"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + totalWeight; //total weight value;
        //            campoText = formWidget.FieldsWidget["F7"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Retailer: "; //retailer label;
        //            campoText = formWidget.FieldsWidget["D7"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + retailername; //retailer value;
        //            campoText = formWidget.FieldsWidget["F8"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Category: "; //category label;
        //            campoText = formWidget.FieldsWidget["D8"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + categoryname; //category value;
        //            campoText = formWidget.FieldsWidget["F9"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Service Type: "; //servicetype label;
        //            campoText = formWidget.FieldsWidget["D9"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + servicetimename; //servicetype value;
        //            campoText = formWidget.FieldsWidget["F10"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Jewelry Type: "; //jewelrytype label;
        //            campoText = formWidget.FieldsWidget["D10"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + jewelrytypename; //jewelrytype value;
        //            campoText = formWidget.FieldsWidget["F11"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Contact: "; //contact label;
        //            campoText = formWidget.FieldsWidget["D11"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + contactname; //contact value;
        //            campoText = formWidget.FieldsWidget["F12"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Messenger: "; //messenger label;
        //            campoText = formWidget.FieldsWidget["D12"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + messengername; //messenger value;
        //            campoText = formWidget.FieldsWidget["F13"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "Carrier: "; //carrier label;
        //            campoText = formWidget.FieldsWidget["D13"] as PdfTextBoxFieldWidget;
        //            campoText.Text = "" + carriername; //carrier value;
        //            //alex
        //            campoText = formWidget.FieldsWidget["Instructions"] as PdfTextBoxFieldWidget;
        //            campoText.Text = specInstr; //"Special Instructions";
        //            campoText = formWidget.FieldsWidget["Date"] as PdfTextBoxFieldWidget;
        //            campoText.Text = DateTime.Now.ToLocalTime().ToString(); //"Date";
        //            campoText = formWidget.FieldsWidget["RequestID"] as PdfTextBoxFieldWidget;
        //            campoText.Text = requestID; //"Request ID";
        //            campoText = formWidget.FieldsWidget["Customer"] as PdfTextBoxFieldWidget;
        //            campoText.Text = customerCode; //"Customer code";
                    
        //            doc.Form.ReadOnly = true;
        //            MemoryStream msdoc = new MemoryStream();
        //            doc.SaveToStream(msdoc);
        //            doc.Dispose();
        //            bool sent = QueryUtils.SendMessageToStorage(msdoc, this);
        //            this.Response.Clear();
        //            this.Response.ContentType = "application/pdf";
        //            this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Label_Front-{0}.pdf", orderCode));
        //            this.Response.BinaryWrite(msdoc.ToArray());
        //            this.Response.End();

        //        }
        //    }
        //    this.Response.Clear();
        //    this.Response.ContentType = "application/pdf";
        //    this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Label_Front-{0}.pdf", orderCode));
        //    this.Response.End();
        //}//PrintPDFLabel1
        #endregion

        #region Create Screening Request

        protected void txtCustomerLookupReq_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            bool changed = false;

            foreach (ListItem item in ddlCustomerListReq.Items)
            {
                string custNumbers = new String(textBox.Text.Where(Char.IsDigit).ToArray());
                if (item.Text.Contains(custNumbers == "" ? "$" : custNumbers))
                {
                    ddlCustomerListReq.SelectedValue = item.Value;
                    txtCustomerLookupTakeIn.Text = txtCustomerLookupTakeIn.Text;
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                if (ddlCustomerListReq.SelectedItem != null)
                {
                    textBox.Text = ddlCustomerListReq.SelectedItem.Text;
                }
                else
                {
                    textBox.Text = "";
                }
            }
        }

        protected void OnCustomerReqSelectedChanged(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            ddlCustomerListReq.Items.Clear();
            ddlCustomerListReq.DataSource = customers;
            ddlCustomerListReq.DataBind();

            ddlCustomerListReq.ClearSelection();
            ddlCustomerListReq.Items.FindByValue(lstCustomerlookupReq.SelectedItem.Value).Selected = true;

            if (string.IsNullOrEmpty(ddlCustomerListReq.SelectedValue)) return;

            var customer = GetCustomerFromView(ddlCustomerListReq.SelectedValue);
            if (customer == null) return;
            var infoModel = QueryCustomerUtils.GetCustomer(customer, this);

            SetViewState(infoModel, SessionConstants.CustomerEditData);
            if (infoModel == null) return;

            var Persons = QueryCustomerUtils.GetPersonsByCustomer(customer, Page);
            SetViewState(Persons, SessionConstants.PersonEditData);
           
            txtCustomerLookupReq.Text = ddlCustomerListReq.SelectedItem.Text;
            hfCustomerIDReq.Value = customer.CustomerId;
            hfCustomerCodeReq.Value = customer.CustomerCode;

            var retailer = SyntheticScreeningUtils.GetRetailerListByCustomerCode(customer.CustomerCode, Page);
            rdlRetailer.Items.Clear();
            rdlRetailer.DataSource = retailer;
            rdlRetailer.DataBind();
            if (retailer.Rows.Count == 1) lblRetailerMsg.Visible = true;

        }

        protected void rdlCustomerContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCustomerContact.Text = rdlCustomerContact.SelectedItem.Text.ToString();
            hdnPersonID.Value = rdlCustomerContact.SelectedItem.Value.ToString();
        }

        protected void rdlDeliveryMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDeliveryMethod.Text = rdlDeliveryMethod.SelectedItem.Text.ToString();
            hfDeliveryMethodCode.Value = rdlDeliveryMethod.SelectedItem.Value.ToString();

            rdlShippingCourier.Items.Clear();
            rdlShippingCourier.DataSource = null;
            rdlShippingCourier.DataBind();
            lblShippingCourierMsg.Visible = true;
            txtShippingCourier.Text = "";
            hfShippingCourierCode.Value = "0";
            hfDestination.Value = "0";
            txtDestination.Text = "";
            txtAccountNumber.Text = "";
            txtAccountNumber.Enabled = false;

            rdlCustomerContact.Items.Clear();
            rdlCustomerContact.DataSource = null;
            rdlCustomerContact.DataBind();
            lblCustomerContactMsg.Visible = true;
            txtCustomerContact.Text = "";
            hdnPersonID.Value = "0";

            if (rdlDeliveryMethod.SelectedItem.Value == "0")
            {
                return;
            }
            else if (rdlDeliveryMethod.SelectedItem.Value == "1")
            {
                var Persons = GetViewState(SessionConstants.PersonEditData) as List<PersonExModel> ?? new List<PersonExModel>();
                if (Persons.Count >= 1)
                {
                    rdlCustomerContact.DataSource = Persons;
                    rdlCustomerContact.DataBind();
                    lblCustomerContactMsg.Visible = false;
                }
                else
                {
                    rdlCustomerContact.DataSource = null;
                    rdlCustomerContact.DataBind();
                    lblCustomerContactMsg.Visible = true;
                }

            }
            else if (rdlDeliveryMethod.SelectedItem.Value == "2")
            {
                var carriers = SyntheticScreeningUtils.GetCarriers(this);
                rdlShippingCourier.DataSource = carriers;
                rdlShippingCourier.DataBind();
                lblShippingCourierMsg.Visible = false;
                txtAccountNumber.Enabled = true;
                txtShippingDestination.Text = "";
                ddlDestination.ClearSelection();
                lstDestination.ClearSelection();
                ddlDestination.SelectedIndex = 0;
                lstDestination.SelectedIndex = 0;

               // modelDepartureSetting.Show();
            }
            
        }

        protected void rdlJewelryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtJewelryType.Text = rdlJewelryType.SelectedItem.Text.ToString();
            hfJewelryTypeCode.Value = rdlJewelryType.SelectedItem.Value.ToString();
        }

        protected void rdlShippingCourier_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtShippingCourier.Text = rdlShippingCourier.SelectedItem.Text.ToString();
            hfShippingCourierCode.Value = rdlShippingCourier.SelectedItem.Value.ToString();
        }

        protected void rdlRetailer_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRetailer.Text = rdlRetailer.SelectedItem.Text.ToString();
            hfRetailerID.Value = rdlRetailer.SelectedItem.Value.ToString();

            rdlServiceType.Items.Clear();
            var serviceType = SyntheticScreeningUtils.GetSyntheticServiceTypeByRetailer(Convert.ToInt32(hfRetailerID.Value), this);
            if (serviceType.Rows.Count >= 1)
            {
                rdlServiceType.DataSource = serviceType;
                rdlServiceType.DataBind();
                lblServiceTypeMsg.Visible = false;
            }
            else
            {
                rdlServiceType.DataSource = null;
                rdlServiceType.DataBind();
                lblServiceTypeMsg.Visible = true;
            }
        }

        protected void rdlServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtServiceType.Text = rdlServiceType.SelectedItem.Text.ToString();
            hfServiceTypeCode.Value = rdlServiceType.SelectedItem.Value.ToString();

            rdlCategory.Items.Clear();
            var category = SyntheticScreeningUtils.GetSyntheticCategoryByServiceType(Convert.ToInt16(hfServiceTypeCode.Value), this);
            if (category.Rows.Count >= 1)
            {
                rdlCategory.DataSource = category;
                rdlCategory.DataBind();
                lblCategoryMsg.Visible = false;
            }
            else
            {
                rdlCategory.DataSource = null;
                rdlCategory.DataBind();
                lblCategoryMsg.Visible = true;
            }
        }

        protected void rdlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCategory.Text = rdlCategory.SelectedItem.Text.ToString();
            hfCategoryCode.Value = rdlCategory.SelectedItem.Value.ToString();
            var serviceTime = SyntheticScreeningUtils.GetSyntheticServiceTimeByCategory(hfCategoryCode.Value, this);
            if (serviceTime.Rows.Count >= 1)
            {
                hfServiceTimeName.Value = serviceTime.Rows[0]["ServiceTimeName"].ToString();
                hfServiceTimeCode.Value = serviceTime.Rows[0]["ServiceTimeCode"].ToString();
            }
            else
            {
                hfServiceTimeName.Value = "0";
                hfServiceTimeCode.Value = "0";
            }
        }

        protected void btnCreateRequest_Click(object sender, EventArgs e)
        {
            Page.Validate("CreateRequest");

            if (!Page.IsValid)
                return;

            bool isValidPage = true;
            lblCreateRequestMessege.Text = "";
            string errorMessage = @"<ul>";

            if ((txtStyleReq.Text.Trim() == "" && txtSkuReq.Text.Trim() == "") || (txtStyleReq.Text.Trim() != "" && txtSkuReq.Text.Trim() != ""))
            {
                lblValidateSKUReq.Visible = true;
                lblValidateStyleReq.Visible = true;
                errorMessage = errorMessage + "<li>Please enter either Style or SKU!</li>";
                isValidPage = false;
            }
            else
            {
                lblValidateSKUReq.Visible = false;
                lblValidateStyleReq.Visible = false;
            }
            
            if (IsMemoExist(txtMemoNum.Text.Trim()) == true)
            {
                lblValidateMemo.Visible = true;
                errorMessage = errorMessage + "<li>Memo already exists!</li>";
                isValidPage = false;
            }
            else
            {
                lblValidateMemo.Visible = false;
            }

            if (hfDeliveryMethodCode.Value == "2" && txtShippingCourier.Text.Trim() == "")
            {
                lblValidateShippingCourier.Visible = true;
                errorMessage = errorMessage + "<li>Please select shipping courier if delivery method is shipping!</li>";
                isValidPage = false;
            }
            else
            {
                lblValidateShippingCourier.Visible = false;
            }

            if (txtDeliveryMethod.Text == "") hfDeliveryMethodCode.Value = "0";

            errorMessage = errorMessage + @"</ul>";

            lblCreateRequestMessege.Visible = true;
            lblCreateRequestMessege.Text = errorMessage;

            if (!isValidPage)
                return;

            SyntheticCustomerEntriesModel objCustomerEntrie = new SyntheticCustomerEntriesModel();
            objCustomerEntrie.ID = 0;
            objCustomerEntrie.MemoNum = txtMemoNum.Text.Trim();
            objCustomerEntrie.PONum = txtPONum.Text.Trim();
            objCustomerEntrie.SKUName = txtSkuReq.Text.Trim();
            objCustomerEntrie.TotalQty = Convert.ToInt32(txtTotalQtyReq.Text.Trim());
            objCustomerEntrie.StyleName = txtStyleReq.Text.Trim();
            objCustomerEntrie.ServiceTypeCode = Convert.ToInt32(hfServiceTypeCode.Value);
            objCustomerEntrie.DeliveryMethodCode = Convert.ToInt32(hfDeliveryMethodCode.Value);
            objCustomerEntrie.CarrierCode = Convert.ToInt32(hfShippingCourierCode.Value);
            objCustomerEntrie.RetailerID = Convert.ToInt32(hfRetailerID.Value);
            objCustomerEntrie.CategoryCode = Convert.ToInt32(hfCategoryCode.Value);
            objCustomerEntrie.JewelryTypeCode = Convert.ToInt32(hfJewelryTypeCode.Value);
            objCustomerEntrie.SpecialInstructions = txtSpecialInstruction.Text;
            objCustomerEntrie.CustomerCode = Convert.ToInt32(hfCustomerCodeReq.Value);
            objCustomerEntrie.Messenger = "GSIUser-" + HttpContext.Current.Session["LoginName"].ToString();
           // objCustomerEntrie.VendorName = ddlDeparture.SelectedItem == null ? "" : ddlDeparture.SelectedItem.Text;
            objCustomerEntrie.VendorID = Convert.ToInt32(hfDestination.Value);
            objCustomerEntrie.VendorName = txtDestination.Text;
            objCustomerEntrie.PersonID = Convert.ToInt32(hdnPersonID.Value);
            objCustomerEntrie.Account = txtAccountNumber.Text;
            objCustomerEntrie.IsSave = true;

            DataSet ds = SyntheticScreeningUtils.SetSyntheticCustomerEntries(objCustomerEntrie, this);

            if (ds == null || ds.Tables[0].Rows.Count == 0) //failed
            {
                lblCreateRequestMessege.Text = "Request of MEMO number '" + objCustomerEntrie.MemoNum + "' is not created.";
                return;
            }
            else
            {
                TabContainer1.ActiveTabIndex = 0;
                hfRequestID.Value = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                FillTakeInFields();
            }

            modelDepartureSetting.Hide();

        }

        private void FillTakeInFields()
        {
            TakeInControlsEnableDisable(true);

            txtRequest.Text = hfRequestID.Value;
            txtCustomerLookupTakeIn.Text = txtCustomerLookupReq.Text;
            hdnCustomerId.Value = hfCustomerIDReq.Value;
            txtRetailerName.Text = txtRetailer.Text;
            txtNoOfItemsTakeIn.Text = txtTotalQtyReq.Text;
            hdntxtNoOfItemsTakeIn.Value = txtNoOfItemsTakeIn.Text;
            txtMemoTakeIn.Text = txtMemoNum.Text;
            txtPONumber.Text = txtPONum.Text;
            txtSKU.Text = txtSkuReq.Text;
            txtStyle.Text = txtStyleReq.Text;
            txtServiceTimeTakeIn.Text = hfServiceTimeName.Value;
            hdnServiceTimeTakeIn.Value = hfServiceTimeCode.Value;
            txtSpecialInstructions.Text = txtSpecialInstruction.Text;
            txtTotalQty.Text = txtTotalQtyReq.Text;
            //btnMessengerlistTakeIn.Enabled = false;

            if (hdnServiceTimeTakeIn.Value != "0")  
            {
                rdlServiceTimeTakeIn.Items.FindByValue(hdnServiceTimeTakeIn.Value.ToString()).Selected = true;
                rdbServiceTimeTakeIn_IndexChanged(null, null);
            }
            
            hndDeliveryMethodCodeTakeIn.Value = hfDeliveryMethodCode.Value;

            if (hndDeliveryMethodCodeTakeIn.Value == "0")   //Picked up By Our Messenger -  0
            {
                chkPickedUpByOurMessengerTakeIn.Checked = true;

                txtCustomerContact.Text = "";
                hdnPersonID.Value = "0";
                txtMessengerTakeIn.Text = "";
                hdnMessengerTakeIn.Value = "0";
                imgStoredPhoto.ImageUrl = imgTransperant;
                imgStoredSignature.ImageUrl = imgTransperant;

                txtCarriersShipReceiving.Text = "";
                hdnCarriersShipReceiving.Value = "0";
                lblVendorTakeIn.Text = "";

                txtShippingCourier.Text = "";
                hfShippingCourierCode.Value = "0";
                txtAccountNumber.Text = "";

            }
            else if (hndDeliveryMethodCodeTakeIn.Value == "1")  //Messenger Drop Off - 1
            {
                chkPickedUpByOurMessengerTakeIn.Checked = false;

                txtMessengerTakeIn.Text = txtCustomerContact.Text;
                hdnMessengerTakeIn.Value = hdnPersonID.Value.ToString();
                var Person = GetPersonFromView(Convert.ToInt32(hdnPersonID.Value));
                imgStoredSignature.ImageUrl = string.IsNullOrEmpty(Person.Path2Signature) ? imgTransperant : Person.Path2Signature;
                imgStoredPhoto.ImageUrl = string.IsNullOrEmpty(Person.Path2Photo) ? imgTransperant : Person.Path2Photo;
                
                txtCarriersShipReceiving.Text = "";
                hdnCarriersShipReceiving.Value = "0";
                lblVendorTakeIn.Text = "";

                txtShippingCourier.Text = "";
                hfShippingCourierCode.Value = "0";
                txtAccountNumber.Text = "";
            }
            else if (hndDeliveryMethodCodeTakeIn.Value == "2")   //Shipping  -  2
            {
                chkPickedUpByOurMessengerTakeIn.Checked = false;

                txtCustomerContact.Text = "";
                hdnPersonID.Value = "0";
                txtMessengerTakeIn.Text = "";
                hdnMessengerTakeIn.Value = "0";
                imgStoredPhoto.ImageUrl = imgTransperant;
                imgStoredSignature.ImageUrl = imgTransperant;

                txtCarriersShipReceiving.Text = txtShippingCourier.Text;
                hdnCarriersShipReceiving.Value = hfShippingCourierCode.Value;
                lblVendorTakeIn.Text = "Destination: " + txtDestination.Text;
                hdnVendorTakeIn.Value = hfDestination.Value;
                modelDepartureSetting.Show();
            }

        }

        private bool IsMemoExist(string memo)
        {
            bool IsExist = false;
            DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerMemo(memo, this);
            if(ds.Tables[0].Rows.Count != 0)
                IsExist = true;
            return IsExist;
        }

        protected void btnAddDestination_OnClick(object sender, EventArgs e)
        {
            modelDepartureSetting.Show();
            ClearCompany();
            modelAddCustomer.Show();
        }

        protected void btnSetDepartureSetting_Click(object sender, EventArgs e)
        {
            if (TabContainer1.ActiveTabIndex == 1)
            {
                string customerName = txtShippingDestination.Text.Trim();
                if (customerName.Contains("#"))
                {
                    customerName = customerName.Substring(3, customerName.Length - 3); // Remove Location first 2 Char with space
                    customerName = customerName.Substring(0, customerName.Length - 7); // Remove customer code last 5 Char with space
                }
                lblVendorShipOut.Text = "Destination: " + customerName;
                hfVendorShipOut.Value = ddlDestination.SelectedItem.Value.ToString().Trim();
            }
            else
            { 
                if (ddlDestination.SelectedItem.Text.ToString() != "")
                {
                    string customerName = txtShippingDestination.Text.Trim();
                    if (customerName.Contains("#"))
                    {
                        customerName = customerName.Substring(3, customerName.Length - 3); // Remove Location first 2 Char with space
                        customerName = customerName.Substring(0, customerName.Length - 7); // Remove customer code last 5 Char with space
                    }
                    txtDestination.Text = customerName;
                    hfDestination.Value = ddlDestination.SelectedItem.Value.ToString().Trim();
                }
                else if (ddlDestination.SelectedItem.Text.ToString() == "" && txtShippingDestination.Text.Trim() != "")
                {
                    ddlDestination.Items.FindByText(txtShippingDestination.Text.Trim()).Selected = true;
                    txtDestination.Text = ddlDestination.Items.FindByText(txtShippingDestination.Text.Trim()).Text;
                    hfDestination.Value = ddlDestination.Items.FindByText(txtShippingDestination.Text.Trim()).Value;
                }
                else
                {
                    txtDestination.Text = "";
                    hfDestination.Value = "0";
                }
            }
            modelDepartureSetting.Hide();
        }

        protected void txtShippingDestination_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            bool changed = false;

            foreach (ListItem item in ddlDestination.Items)
            {
                string custNumbers = new String(textBox.Text.Where(Char.IsDigit).ToArray());
                if (item.Text.Contains(custNumbers == "" ? "$" : custNumbers))
                {
                    ddlDestination.SelectedValue = item.Value;
                    txtShippingDestination.Text = txtShippingDestination.Text;
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                if (ddlDestination.SelectedItem != null)
                {
                    textBox.Text = ddlDestination.SelectedItem.Text;
                }
                else
                {
                    textBox.Text = "";
                }
            }
        }

        protected void OnCustomerReqSelectedChangedPopup(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
           
            ddlDestination.Items.Clear();
            ddlDestination.DataSource = customers.OrderBy(item => item.CustomerName); 
            ddlDestination.DataBind();

            ListItem vendroLookup = new ListItem();
            vendroLookup.Value = "0";
            vendroLookup.Text = "";
            ddlDestination.Items.Insert(0, vendroLookup);

            ddlDestination.ClearSelection();
            ddlDestination.Items.FindByValue(lstDestination.SelectedItem.Value).Selected = true;
            var customer = ddlDestination.Items.FindByValue(lstDestination.SelectedValue);
            if (customer != null)
            {
                string customerName = ddlDestination.SelectedItem.Text;
                txtShippingDestination.Text = customerName;
               
            }
            modelDepartureSetting.Show();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearRequest();
        }

        private void ClearRequest()
        {
            lblValidateMemo.Visible = false;
            lblValidateStyleReq.Visible = false;
            lblValidateShippingCourier.Visible = false;
            lblValidateSKUReq.Visible = false;
            lblRequestError.Text = "";
            if (Session["MachineName"] != null)
            {
                string machineName = Session["MachineName"].ToString();
                lblCreateRequestMessege.Text = machineName;
            }
            else
                lblCreateRequestMessege.Text = "No machine name. Please restart GDLight with machine name using bookmark bar. Example: https://gdlight20181109.azurewebsites.net/Login.aspx?p=gsi-ny-xx";

            txtCustomerLookupReq.Text = "";
            hfCustomerCodeReq.Value = "0";
            hfCustomerIDReq.Value = "0";
            hfRequestID.Value = "0";
            lstCustomerlookupReq.ClearSelection();
            ddlCustomerListReq.ClearSelection();

            txtMemoNum.Text = "";
            hfDestination.Value = "0";

            txtPONum.Text = "";
            hfDeliveryMethodCode.Value = "0";
            txtDeliveryMethod.Text = "";

            txtStyleReq.Text = "";
            hfShippingCourierCode.Value = "0";
            txtShippingCourier.Text = "";

            hfDestination.Value = "0";
            txtDestination.Text = "";
            txtAccountNumber.Text = "";
            txtAccountNumber.Enabled = false;

            txtSkuReq.Text = "";
            hfRetailerID.Value = "0";
            txtRetailer.Text = "";

            txtTotalQtyReq.Text = "";

            hfServiceTypeCode.Value = "0";
            txtServiceType.Text = "";

            txtSpecialInstruction.Text = "";

            hfCategoryCode.Value = "0";
            txtCategory.Text = "";
            hfServiceTimeName.Value = "0";
            hfServiceTimeCode.Value = "0";

            hfJewelryTypeCode.Value = "0";
            txtJewelryType.Text = "";
            hdnPersonID.Value = "0";
            txtCustomerContact.Text = "";

            rdlCustomerContact.ClearSelection();
            rdlDeliveryMethod.ClearSelection();
            rdlJewelryType.ClearSelection();
            rdlShippingCourier.ClearSelection();
            rdlRetailer.ClearSelection();
            rdlServiceType.ClearSelection();
            rdlCategory.ClearSelection();
            //ValidateMessengersBtn.Visible = false;
            signatureBtn.Visible = true;
            photoBtn.Visible = true;
            lblBulkGiveOutTotal.Text = "";
            lblBulkGiveOutTotal.Visible = false;
            //int rand = new Random().Next(99999999);
            //string img = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            //imgStoredSignature.ImageUrl = string.Concat(img, '?', rand);
            //img = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
            //imgStoredPhoto.ImageUrl = string.Concat(img, '?', rand);
            //imgStoredSignature.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/empty.png";
            //imgStoredPhoto.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/empty.png";
        }

        #endregion

        #region Combo box To Touch screen Events

        protected void ddlCustomerCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

            CompanyPhoneCode.Enabled = false;
            CompanyFaxCode.Enabled = false;
            if (ddlCustomerCountry.SelectedItem.Text == "India")
            {
                //	btnCustomerState.Text = "State";
                CompanyPhoneCode.Text = "+91";
                CompanyFaxCode.Text = "+91";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Hong Kong")
            {
                //btnCustomerState.Text = "Region";
                CompanyPhoneCode.Text = "+852";
                CompanyFaxCode.Text = "+852";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "UAE")
            {
                //btnCustomerState.Text = "Emirate";
                CompanyPhoneCode.Text = "+971";
                CompanyFaxCode.Text = "+971";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Israel")
            {
                //btnCustomerState.Text = "Region";
                CompanyPhoneCode.Text = "+972";
                CompanyFaxCode.Text = "+972";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Botswana")
            {
                //btnCustomerState.Text = "District";
                CompanyPhoneCode.Text = "+267";
                CompanyFaxCode.Text = "+267";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Belgium")
            {
                //btnCustomerState.Text = "Province";
                CompanyPhoneCode.Text = "+32";
                CompanyFaxCode.Text = "+32";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "USA")
            {
                //btnCustomerState.Text = "State";
                CompanyPhoneCode.Text = "+1";
                CompanyFaxCode.Text = "+1";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Canada")
            {
                //btnCustomerState.Text = "Province";
                CompanyPhoneCode.Text = "+1";
                CompanyFaxCode.Text = "+1";

            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Other")
            {
                //btnCustomerState.Text = "State";
                ddlCustomerState.Items.Clear();
                ddlCustomerState.DataBind();
                CompanyPhoneCode.Enabled = true;
                CompanyFaxCode.Enabled = true;
                CompanyPhoneCode.Text = "";
                CompanyFaxCode.Text = "";
            }

           
            ddlCustomerState.Items.Clear();
            var state = QueryCustomerUtils.GetStateByCountry(ddlCustomerCountry.SelectedValue.ToString(), this);
            ddlCustomerState.DataSource = state;
            ddlCustomerState.SelectedValue = null;
            ddlCustomerState.DataBind();
            ddlCustomerState.Items.Insert(0, "");
                       
            modelDepartureSetting.Show();
            modelAddCustomer.Show();
        }

        protected void OnUpdateCompanyClick(object sender, EventArgs e)
        {
            modelDepartureSetting.Show();
            modelAddCustomer.Show();
            lblAddCustMessageLabel.Text = "";
            Page.Validate(ValidatorGroupCompany);
            if (!IsGroupValid(ValidatorGroupCompany))
            {
                //PopupInfoDialog(GetErrMessage(ValidatorGroupCompany), true);
                Literal li = new Literal();
                li.Text = GetErrMessage(ValidatorGroupCompany);
                AddCustomerFooter.Controls.Add(li);
                return;
            }
            var customerInfo = GetCompanyInfo();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            List<CustomerModel> customerCount = customers.Where(m => m.CustomerName == CompanyName.Text).ToList();
            if (customerCount.Count >= 1)
            {
                Literal li = new Literal();
                li.Text = "Company name already exists";
                AddCustomerFooter.Controls.Add(li);
                return;
            }

            var msg = QueryCustomerUtils.UpdateCompanyExt(customerInfo, this);

            if (!string.IsNullOrEmpty(msg))
            {
                Literal li = new Literal();
                li.Text = msg;

                AddCustomerFooter.Controls.Add(li);
                return;
            }

            CustomerId.Text = customerInfo.CustomerId;

            if (customerInfo.CustomerId != "")
            {
                LoadCustomers();
                ddlDestination.ClearSelection();
                ddlDestination.Items.FindByValue(customerInfo.CustomerId).Selected = true;
                var customer = ddlDestination.Items.FindByValue(customerInfo.CustomerId);

                if (customer != null)
                {
                    string customerName = ddlDestination.SelectedItem.Text;
                    //customerName = customerName.Substring(3, customerName.Length - 3); // Remove Location first 2 Char with space
                    //customerName = customerName.Substring(0, customerName.Length - 7); // Remove customer code last 5 Char with space
                    txtShippingDestination.Text = customerName;
                }
            }
            //RefreshCompanyInfo("0");
            //LoadCustomers();
            lblAddCustMessageLabel.Text = "Customer is added successfully";
            //PopupInfoDialog("Customer was saved successfully", false);

        }

        #endregion

        #region Company Details Panel
        private CustomerEditModelExt GetCompanyInfo()
        {
            var customerModel = GetViewState(SessionConstants.CustomerEditDataExt) as CustomerEditModelExt ??
                                    new CustomerEditModelExt();

            var infoModel = new CustomerEditModelExt();
            infoModel.StateId = customerModel.StateId;
            infoModel.StateTargetId = customerModel.StateTargetId;
            infoModel.CustomerOfficeId = customerModel.CustomerOfficeId;
            infoModel.CustomerHistoryId = customerModel.CustomerHistoryId;
            infoModel.CustomerId = customerModel.CustomerId;
            infoModel.CustomerCode = customerModel.CustomerCode;
            infoModel.Company.Address.AddressId = customerModel.Company.Address.AddressId;

            infoModel.Company.CompanyName = CompanyName.Text.Trim();
            //   infoModel.CustomerName = CompanyName.Text.Trim();
            infoModel.Company.ShortName = ShortName.Text.Trim();
            infoModel.Company.Address.Address1 = CompanyAddress1.Text.Trim();
            infoModel.Company.Address.Address2 = CompanyAddress2.Text.Trim();

            infoModel.Company.Address.Zip1 = CompanyZip1.Text.Trim();
            infoModel.Company.Address.Zip2 = CompanyZip2.Text.Trim();
            infoModel.Company.Address.City = CompanyCity.Text.Trim();
            //infoModel.Company.Address.Country = CompanyCountry.Text.Trim();
            infoModel.Company.Address.Country = ddlCustomerCountry.SelectedItem.Text;

            //if (CompanyCountry.Text.Trim() == "USA")
            //{
            //    infoModel.Company.Address.UsStateId = CompanyUsState.SelectedValue;
            //}

            infoModel.Company.Address.UsStateId = ddlCustomerState.SelectedValue;

            infoModel.Company.Address.CountryPhoneCode = CompanyPhoneCode.Text;
            infoModel.Company.Address.Phone = CompanyPhone.Text.Trim();

            infoModel.Company.Address.CountryFaxCode = CompanyFaxCode.Text;
            infoModel.Company.Address.Fax = CompanyFax.Text.Trim();

            infoModel.Company.Address.Email = CompanyEmail.Text.Trim();
            //infoModel.Company.Communication = GetTreeMethodValue(CompanyMethods);
            infoModel.Company.Permissions = "";
            //infoModel.Company.IndustryMembership = GetCheckBoxListValue(IndustryMembers);
            infoModel.Company.IndustryMembership = "";

            infoModel.Company.BusinessTypeId = "5";
          //  infoModel.Company.GoodsMovement = GetGoodsMovement();
            infoModel.Company.UseTheirAccount = false; //UseAccountFlag.Checked;
            infoModel.Company.Account = "";//txtCarrierAccount.Text.Trim();
                                           //infoModel.Company.CarrierId = ddlCarrierList.SelectedValue;
            infoModel.Company.GoodsMovement.WeShipCarry = true;
            infoModel.Company.GoodsMovement.WeCarry = false;
            infoModel.Company.GoodsMovement.TheyCarry = false;
            return infoModel;
        }

        private void SetCompanyInfo(CustomerEditModelExt infoModel)
        {
            //CompanyDetailsPanel.Visible = true;
            CompanyName.Text = infoModel.Company.CompanyName;
            ShortName.Text = infoModel.Company.ShortName;
            CompanyAddress1.Text = infoModel.Company.Address.Address1;
            CompanyAddress2.Text = infoModel.Company.Address.Address2;
            CompanyCity.Text = infoModel.Company.Address.City;
            // CompanyCountry.Text = infoModel.Company.Address.Country;

            if (ddlCustomerCountry.Items.FindByText(infoModel.Company.Address.Country) != null)
            {
                ddlCustomerCountry.ClearSelection();
                ddlCustomerCountry.Items.FindByText(infoModel.Company.Address.Country).Selected = true;
                //txtCustomerCountry.Text = infoModel.Company.Address.Country;
                ddlCustomerCountry_SelectedIndexChanged(this, EventArgs.Empty);

                if (ddlCustomerState.Items.FindByValue(infoModel.Company.Address.UsStateId) != null)
                {
                    ddlCustomerState.SelectedValue = infoModel.Company.Address.UsStateId;
                    //txtCustomerState.Text = ddlCustomerState.SelectedItem.Text.ToString();
                }
                else
                {
                    ddlCustomerState.SelectedIndex = -1;

                }
                OnCompanyCountryChanging(null, null);
            }
            else
            {
                ddlCustomerCountry.SelectedIndex = -1;
                ddlCustomerState.Items.Clear();
                //btnCustomerState.Text = "State";
                CompanyPhoneCode.Text = "";
                CompanyFaxCode.Text = "";
                //txtCustomerCountry.Text = "";
                //txtCustomerState.Text = "";
            }

            CustomerCode.Text = infoModel.CustomerCode;
            CustomerId.Text = infoModel.CustomerId;
            CustomerStartDate.Text = String.Format("{0:G}", infoModel.CreateDate);
            if (!string.IsNullOrEmpty(infoModel.Company.BusinessTypeId))
            {
                ddlCustomerBusinessType.SelectedValue = infoModel.Company.BusinessTypeId;
                //txtCustomerBusinessType.Text = ddlCustomerBusinessType.SelectedItem.Text.ToString();
            }
            else
            {
                ddlCustomerBusinessType.SelectedIndex = -1;
                //txtCustomerBusinessType.Text = "";
            }

            //   LoadCheckBoxList(infoModel.Company.IndustryMembership, IndustryMembers);

            CompanyZip1.Text = infoModel.Company.Address.Zip1;
            CompanyZip2.Text = infoModel.Company.Address.Zip2;

            CompanyPhoneCode.Text = infoModel.Company.Address.CountryPhoneCode;
            CompanyPhone.Text = infoModel.Company.Address.Phone;

            CompanyFaxCode.Text = infoModel.Company.Address.CountryFaxCode;
            CompanyFax.Text = infoModel.Company.Address.Fax;
            CompanyEmail.Text = infoModel.Company.Address.Email;
                     
            if (!string.IsNullOrEmpty(infoModel.Company.CarrierId))
            {
                //ddlCarrierList.SelectedValue = infoModel.Company.CarrierId;
                //txtCarrierList.Text = ddlCarrierList.SelectedItem.Text.ToString();
            }
            else
            {
                //ddlCarrierList.SelectedIndex = -1;
            }
           
            Page.Validate("ValGrpCompany");
            lblAddCustMessageLabel.Text = "";
                       
        }

        public void ClearCompany()
        {
            CompanyName.Text = "";
            ShortName.Text = "";
            CompanyAddress1.Text = "";
            CompanyAddress2.Text = "";
            CompanyZip1.Text = "";
            CompanyZip2.Text = "";
            CompanyCity.Text = "";
            ddlCustomerCountry.SelectedIndex = 0;
            ddlCustomerState.SelectedIndex = 0;
            ddlCustomerBusinessType.SelectedIndex = 8; // Set Default Vender
            CompanyPhoneCode.Text = "";
            CompanyPhone.Text = "";
            CompanyFaxCode.Text = "";
            CompanyFax.Text = "";
            CompanyEmail.Text = "";
            
            lblAddCustMessageLabel.Text = "";
        }

        protected void OnCompanyCountryChanging(object sender, EventArgs e)
        {
            
            ddlCustomerState.Enabled = true;
            CompUsStateValidator.Enabled = true;
        }

        public string sendAcceptEmailToContact(string toAddress, string requestId, string memo, string totalQty, Page p)
        {
            return "OK";
            MailMessage myEmail;
            try
            {
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                myEmail.Subject = "GSI Customer Request for " + requestId;
                myEmail.To.Add(toAddress);
                myEmail.IsBodyHtml = false;
                myEmail.Body = "Your Request #" + requestId + " was accepted at " + DateTime.Now.ToLocalTime().ToString() + Environment.NewLine;
                myEmail.Body += "Total Quantity of Items: " + totalQty + "." + Environment.NewLine;
                myEmail.Body += "Memo: " + memo + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Thank you," + Environment.NewLine;
                myEmail.Body += "Your GSI team.";

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                //lblCreateRequestMessege.Text += ", Error sending email for: " + requestId + ";;";
                return ex.Message;
            }
        }//sendAcceptEmailToContact

        public string sendAcceptBulkEmailToContact(DataTable dt)
        {
            //return "OK";
            
            try
            {
                MailMessage myEmail;
                string toAddress = dt.Rows[0]["MailAddress"].ToString();
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                myEmail.Subject = "GSI Customer Request ";
                myEmail.To.Add(toAddress);
                myEmail.IsBodyHtml = true;
                myEmail.Body = "Your Request was accepted at " + DateTime.Now.ToLocalTime().ToString() + Environment.NewLine;
                string textBody = " <table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 400 + "><tr bgcolor='#4da6ff'><td><b>Request ID</b></td> <td> <b> Memo</b> </td> <td> <b> Total Qty</b> </td> </tr>";
                foreach (DataRow dr in dt.Rows)
                {
                    string rId = dr["RequestID"].ToString();
                    string memo = dr["Memo"].ToString();
                    string total = dr["TotalQty"].ToString();
                    textBody += "<tr><td>" + rId + "</td><td> " + memo + "</td><td> " + total + "</td> </tr>";
                }
                
                //for (int loopCount = 0; loopCount < data_table.Rows.Count; loopCount++)
                //{
                //    textBody += "<tr><td>" + data_table.Rows[loopCount]["RowName"] + "</td><td> " + data_table.Rows[loopCount]["RowName2"] + "</td> </tr>";
                //}
                textBody += "</table>";
                myEmail.Body += textBody;
                //myEmail.Body += "Total Quantity of Items: " + totalQty + "." + Environment.NewLine;
                //myEmail.Body += "Memo: " + memo + Environment.NewLine + Environment.NewLine;
                myEmail.Body += Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Thank you," + Environment.NewLine;
                myEmail.Body += "Your GSI team.";

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                //lblCreateRequestMessege.Text += ", Error sending email for: " + requestId + ";;";
                return ex.Message;
            }
        }//sendAcceptBulkEmailToContact

        public string sendEmailToContact(string toAddress, string requestId, string orderNumber, string memo, string totalQty, Page p)
        {
            MailMessage myEmail = null;
            
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.UseDefaultCredentials = false;
            client.Port = 587;
            client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
            client.EnableSsl = true;
            bool sent = false;
            for (int i = 0; i <= 4; i++)
            {
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                try
                {
                    
                    myEmail.From = new MailAddress("app@gemscience.net");
                    myEmail.Subject = "GSI Customer Request for " + requestId;
                    myEmail.To.Add(toAddress);
                    myEmail.IsBodyHtml = false;
                    myEmail.Body = "Your Request #" + requestId + " was submitted at " + DateTime.Now.ToLocalTime().ToString() + Environment.NewLine;
                    myEmail.Body += "Total Quantity of Items: " + totalQty + "." + Environment.NewLine;
                    myEmail.Body += "GSI Order # " + orderNumber + Environment.NewLine + Environment.NewLine;
                    myEmail.Body += "Memo: " + memo + Environment.NewLine + Environment.NewLine;
                    myEmail.Body += "Thank you," + Environment.NewLine;
                    myEmail.Body += "Your GSI team.";
                    client.Send(myEmail);
                    sent = true;
                    break;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    Thread.Sleep(1000);
                }
                
            }
            myEmail.Dispose();
            client.Dispose();
            if (!sent)
            {
                lblCreateRequestMessege.Text += ", Error sending email for: " + orderNumber + ";;";
                return "failed";
            }
                //SmtpClient client = new SmtpClient();
                //client.Host = "smtp.office365.com";
                //client.UseDefaultCredentials = false;
                //client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                //client.EnableSsl = true;
                //client.Send(myEmail);
                if (lblCreateRequestMessege.Text.Contains("Email was sent for:"))
                {
                    int idx = lblCreateRequestMessege.Text.IndexOf(",Email was sent for:");
                    int idxEnd = lblCreateRequestMessege.Text.IndexOf(";;");
                    string oldNumber = lblCreateRequestMessege.Text.Substring(idx + 20, idxEnd - idx - 20);
                    if (lblCreateRequestMessege.Text.Contains(oldNumber))
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Replace(oldNumber, orderNumber);
                    if (lblCreateRequestMessege.Text.Contains(", Error sending email for: " + orderNumber + ";;"))
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Replace(", Error sending email for: " + orderNumber + ";;", "");

                }
                else
                {
                    lblCreateRequestMessege.Text += ",Email was sent for:" + orderNumber + ";;";
                    if (lblCreateRequestMessege.Text.Contains(", Error sending email for: " + orderNumber + ";;"))
                        lblCreateRequestMessege.Text = lblCreateRequestMessege.Text.Replace(", Error sending email for: " + orderNumber + ";;", "");
                }
                return "OK";
            //}
            //catch (Exception ex)
            //{
            //    lblCreateRequestMessege.Text += ", Error sending email for: " + orderNumber + ";;";
            //    return ex.Message;
            //}
        }

        public string sendEmailToContactOut(string toAddress, string requestId, string orderNumber, string memo, string messenger, string carrierName, string trackingNumber, Page p)
        {
            MailMessage myEmail;
            try
            {
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                myEmail.Subject = "GSI Customer Request for " + requestId;
                myEmail.To.Add(toAddress);
                myEmail.IsBodyHtml = false;
                myEmail.Body = "Your GSI Order # " + orderNumber + " was picked up " + DateTime.Now.ToLocalTime().ToString() + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Memo #: " + memo + Environment.NewLine + Environment.NewLine;
                if (messenger != "")
                    myEmail.Body += "Picked Up by: " + messenger + Environment.NewLine + Environment.NewLine;
                else
                {
                    myEmail.Body += "Shipping Carrier: " + carrierName + Environment.NewLine + Environment.NewLine;
                    myEmail.Body += "Tracking Number: " + trackingNumber + Environment.NewLine + Environment.NewLine;
                }
                myEmail.Body += "Thank you," + Environment.NewLine;
                myEmail.Body += "Your GSI team.";

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SendMail(string toAddress, string requestId, string contactName, Page p)
        {
            MailMessage myEmail;
            try
            {
                DataSet ds = SyntheticScreeningUtils.GetSyntheticCustomerEntries(requestId, p);
                DataTable dt = ds.Tables[0];
                string customerCode = dt.Rows[0]["CustomerCode"].ToString();
                string memoNum = dt.Rows[0]["MemoNum"].ToString();
                string poNum = dt.Rows[0]["PONum"].ToString();
                string skuName = dt.Rows[0]["SKUName"].ToString();
                string styleName = dt.Rows[0]["StyleName"].ToString();
                string retailer = dt.Rows[0]["RetailerName"].ToString();
                string totalQty = dt.Rows[0]["TotalQty"].ToString();
                string categoryName = dt.Rows[0]["CategoryName"].ToString();
                string serviceTypeName = dt.Rows[0]["ServiceTypeName"].ToString();
                string serviceTimeName = dt.Rows[0]["ServiceTimeName"].ToString();
                string jewelryTypeName = dt.Rows[0]["JewelryTypeName"].ToString();
                string carrierName = dt.Rows[0]["CarrierName"].ToString();
                string messenger = dt.Rows[0]["Messenger"].ToString();
                string vendorName = dt.Rows[0]["VendorName"].ToString();
                string specInst = dt.Rows[0]["SpecialInstructions"].ToString();
                string customerName = dt.Rows[0]["CustomerName"].ToString();
                //string contactName = dt.Rows[0]["PersonName"].ToString();
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                myEmail.Subject = "GSI Customer Request " + requestId;
                myEmail.To.Add(toAddress);
                myEmail.IsBodyHtml = false;
                char tab = '\u0009';
                myEmail.Body = "Dear " + contactName + "," + Environment.NewLine;
                myEmail.Body += "you have successfully submitted your request." + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Customer Request " + requestId + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Customer Name: " + customerName + Environment.NewLine;
                myEmail.Body += "Total Quantity: " + totalQty + Environment.NewLine;
                if (memoNum != "")
                    myEmail.Body += "Memo: " + memoNum + Environment.NewLine;
                if (poNum != "")
                    myEmail.Body += "PO Num: " + poNum + Environment.NewLine;
                if (skuName != "")
                    myEmail.Body += "SKU Name: " + skuName + Environment.NewLine;
                if (styleName != "")
                    myEmail.Body += "Style: " + styleName + Environment.NewLine;
                //myEmail.Body += "Category Name: " + categoryName + Environment.NewLine;
                //myEmail.Body += "Service Type: " + serviceTypeName + Environment.NewLine;
                if (serviceTimeName != "")
                    myEmail.Body += "Service Time: " + serviceTimeName + Environment.NewLine;
                //myEmail.Body += "Jewelry Type: " + jewelryTypeName + Environment.NewLine;
                if (carrierName != "")
                    myEmail.Body += "Carrier Name: " + carrierName + Environment.NewLine;
                if (messenger != "")
                    myEmail.Body += "Messenger Name: " + messenger + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Thank you" + Environment.NewLine;
                myEmail.Body += "Your GSI team.";
                var contentType = new ContentType();
                contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
                //var temp = p.Session["CustomerReceiptsDir1"].ToString();
                //var filename = requestId.ToString() + ".pdf";
                //var path = p.Server.MapPath(temp) + filename;
                //contentType.Name = requestId + ".pdf";
                //myEmail.Attachments.Add(new Attachment(path, contentType));
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        /*
        public string sendEmailToContactOut(string toAddress, string requestId, string orderNumber, string memo, string messenger, string carrierName, string trackingNumber, Page p)
        {
            MailMessage myEmail;
            try
            {
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                myEmail.Subject = "GSI Order Picked Up";
                myEmail.To.Add(toAddress);
                myEmail.IsBodyHtml = false;
                myEmail.Body = "Your GSI Order # " + orderNumber + " was picked up " + DateTime.Now.ToString() + Environment.NewLine + Environment.NewLine;
                myEmail.Body += "Memo #: " + memo + Environment.NewLine + Environment.NewLine;
                if (messenger != "")
                    myEmail.Body += "Picked Up by: " + messenger + Environment.NewLine + Environment.NewLine;
                else
                {
                    myEmail.Body += "Shipping Carrier: " + carrierName + Environment.NewLine + Environment.NewLine;
                    myEmail.Body += "Tracking Number: " + trackingNumber + Environment.NewLine + Environment.NewLine;
                }
                myEmail.Body += "Thank you" + Environment.NewLine;

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }//sendEmailToContactOut
        */
        #endregion

        protected void OnBulkGiveOutListChanged(object sender, EventArgs e)
        {
            txtRequest.Text = BulkGiveOutList.SelectedItem.Text;
            for (int i = 0; i <= BulkGiveOutList.Items.Count - 1; i++)
            {
                if (BulkGiveOutList.Items[i].Text == BulkGiveOutList.SelectedItem.Text)
                {
                    BulkGiveOutList.Items.Remove(BulkGiveOutList.Items[i]);
                    break;
                }
            }
        }

        protected void btnOverwriteShipInfo_Click(object sender, EventArgs e)
        {
            ddlShippingCourier.SelectedValue = hdnCourierInfo.Value;
            txtScanPackageBarcodeShipReceiving.Text = hdnTrackingInfo.Value;
        }

        protected void SaveShippingInfo(object sender, EventArgs e)
        {
            if (saveShipInfoBox.Checked)
            {
                hdnCourierInfo.Value = ddlShippingCourier.SelectedIndex.ToString();
                hdnTrackingInfo.Value = txtScanPackageBarcodeShipReceiving.Text;
            }
            else
            {
                hdnCourierInfo.Value = "";
                hdnTrackingInfo.Value = "";
            }
        }

        protected void BulkGiveOutCheckBoxNoPrint_CheckedChanged(object sender, EventArgs e)
        {
            if (BulkGiveOutCheckBoxNoPrint.Checked)
            {
                BulkGiveOutList.Visible = true;
                BulkGiveOutCheckBox.Checked = false;
                //btnBulkGiveOut.Visible = true;
                //btnBulkGiveOut.Enabled = true;
            }
        }

        protected void BulkGiveOutCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (BulkGiveOutCheckBox.Checked)
                BulkGiveOutCheckBoxNoPrint.Checked = false;
        }

        protected void saveShipInfoBox_CheckedChanged(object sender, EventArgs e)
        {
            if (saveShipInfoBox.Checked)
            {
                acceptanceBox.Checked = false;
                btnSubmitTakeInAll.Visible = true;
                btnSubmitTakeInAll.Enabled = true;
                lstRequests.Visible = false;
                savedCarrierTxt.Text = "";
                savedTrackingNumberTxt.Text = "";
                savedCarrierTxt.Visible = true;
                savedTrackingNumberTxt.Visible = true;
            }
            else
            {
                savedCarrierTxt.Text = "";
                savedTrackingNumberTxt.Text = "";
                savedCarrierTxt.Visible = false;
                savedTrackingNumberTxt.Visible = false;
                btnSubmitTakeInAll.Visible = false;
                btnSubmitTakeInAll.Enabled = false;
            }
        }

        protected void acceptanceBox_CheckedChanged(object sender, EventArgs e)
        {
            if (acceptanceBox.Checked)
            {
                btnSubmitTakeInAll.Visible = false;
                saveShipInfoBox.Checked = false;
            }
        }

        protected void onqueuessaveyes(object sender, EventArgs e)
        {

        }
    }
}