using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for ShortReportII.
	/// </summary>
	public partial class ShortReportII : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Full Order";
            if (Page.IsPostBack) return;
		    CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
		    CalendarExtenderFrom.SelectedDate = DateTime.Now.Date.AddDays(-7);
		    GoEmailButton.Enabled = false;
		    GoEmailButton.Attributes.Add("onclick", "return confirm('Are you sure you want to send e-mail?');");
            StateValidators(0);
            TabContainer.Focus();
            OrderFilterPanel.Focus();
            txtOrderNumber.Focus();
            var orderCode = Request.Params["OrderCode"] ?? "";
            if (!string.IsNullOrEmpty(orderCode))
            {
                txtOrderNumber.Text = orderCode;
                OnLoadClick(null, null);
            }
		}
        #endregion

        #region Reset Data (On changing Tab, On Loading BatchFullCheckList)
        private void ResetLoadData()
        {
            SetViewState(null, SessionConstants.MovedItems);
            lblCustomerName.Text = "";
            SendEmailMessage.Text = "";
            fullBatchList.Items.Clear();
            checkedBatchList.Items.Clear();
            MailAddressList.Items.Clear();
            grdShortReport.DataSource = null;
            grdShortReport.DataBind();

            btnMovedItems.Visible = false;
            cmdSaveExcel.Enabled = false;
            cmdSaveExcelQR.Enabled = false;
            btnExport.Enabled = false;
            cmdReorient.Visible = false;
            cmdFullAuto.Enabled = false;
            btnFullOrderWithImage.Enabled = false;
            GoEmailButton.Enabled = false;
            checkAll.Checked = false;
            gdPanel.Visible = false;
        }
        #endregion
        
        const string DisplayField = "BatchNumbers";
        const string ValueField = "StructGrp";

        #region Full Batch List
        protected void OnFullBatchListChanged(object sender, EventArgs e)
        {
            var fullModelList = GetFullShortReportList();
            //-- Reset DocStructGrp
            foreach (var srModel in fullModelList)
            {
                srModel.DocStructGrp = 0;
            }
            var checkedModelList = new List<ShortReportModel>();
            foreach (ListItem item in fullBatchList.Items)
            {
                if (!item.Selected) continue;
                checkedModelList.Add(fullModelList.Find(model => model.BatchModel.BatchId == item.Value));
            }
            //-- Generate DocStructGrp
            int structGrp = 0;
            foreach (var shortReportModel in checkedModelList)
            {
                if (shortReportModel.DocStructGrp == 0)
                {
                    structGrp++;
                    shortReportModel.DocStructGrp = structGrp;
                }
                for (var j = 1; j < checkedModelList.Count; j++)
                {
                    var otherModel = checkedModelList[j];
                    if (otherModel.DocStructGrp != 0) continue;
                    if (DocStructModel.CompareStruct(shortReportModel.DocStructure, otherModel.DocStructure))
                    {
                        otherModel.DocStructGrp = shortReportModel.DocStructGrp;
                    }
                }
            }

            //-- Load CheckedBatchList

            var table = new DataTable();
            table.Columns.Add(new DataColumn(DisplayField));
            table.Columns.Add(new DataColumn(ValueField));
            for (int i = 0; i < structGrp; i++)
            {
                var batchNumbers = "";
                var sameGrp = checkedModelList.FindAll(model => model.DocStructGrp == i + 1);
                foreach (var reportModel in sameGrp)
                {
                    batchNumbers += (batchNumbers == "" ? "" : " ") + reportModel.BatchModel.FullBatchNumber;
                }
                var dataRow = table.NewRow();
                dataRow[ValueField] = i + 1;
                dataRow[DisplayField] = batchNumbers + " " + DocStructModel.GetAbbr(sameGrp[0].DocStructure, (i + 1).ToString(CultureInfo.InvariantCulture));
                table.Rows.Add(dataRow);
            }
            table.AcceptChanges();

            checkedBatchList.DataTextField = DisplayField;
            checkedBatchList.DataValueField = ValueField;
            checkedBatchList.DataSource = table;
            checkedBatchList.DataBind();
            if (table.Rows.Count > 0)
            {
                checkedBatchList.SelectedIndex = 0;
            }

            OnCheckedBatchListChanged(null, null);
            cmdFullAuto.Enabled = table.Rows.Count > 0;
            btnFullOrderWithImage.Enabled = table.Rows.Count > 0;
            GoEmailButton.Enabled = table.Rows.Count > 0 && SendFullCheckbox.Checked;
        }

        protected void OnCheckAllClick(object sender, EventArgs e)
        {

            foreach (ListItem item in fullBatchList.Items)
            {
                item.Selected = checkAll.Checked;
            }
            OnFullBatchListChanged(null, null);
        }

        #endregion


        #region Load Full Order
        protected void OnLoadClick(object sender, EventArgs e)
		{
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
            //-- button Load on tab "Order"
            var shortReportsNew = QueryUtils.GetFullOrder(txtOrderNumber.Text, this);
            shortReportsNew = shortReportsNew.FindAll(m => m.DocStructure.Count > 0);
            
            LoadBatchFullCheckList(shortReportsNew);
		}
        private void LoadBatchFullCheckList(List<ShortReportModel> shortReportList)
        {
            ResetLoadData();
            if (shortReportList.Count == 0)
            {
                var msg = "The Order '" + txtOrderNumber.Text + "' is not found!";
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }

            //-- Customer Name
            var customerModel = new CustomerModel();
            if (TabContainer.ActiveTabIndex == 1)
            {
                var customers = GetCustomers();
                if (customers != null)
                {
                    var customer = customers.Find(m => m.CustomerId == lstCustomerList.SelectedValue);
                    if (customer != null)
                    {
                        customerModel = customer;
                    }
                }
            }
            else
            {
                customerModel = QueryUtils.GetCustomerName(shortReportList[0].BatchModel, this);
            }
            lblCustomerName.Text = " / " + customerModel.CustomerName;

            //-- Mail Address
            var addresses = QueryUtils.GetPersons(customerModel.CustomerId, customerModel.OfficeId, this);
            MailAddressList.DataSource = addresses.FindAll(m => !string.IsNullOrEmpty(m.Email));
            MailAddressList.DataBind();
            if (!string.IsNullOrEmpty(MailAddressList.SelectedValue))
            {
                OnChoiceMailFromContacts(null, null);
            }

            SetViewState(shortReportList, SessionConstants.ShortReportModelList);

            //-- Load table for Checkbox list
            var batchList = shortReportList.Select(shortReport => shortReport.BatchModel).ToList();
            fullBatchList.DataSource = batchList;
            fullBatchList.DataBind();
            if (batchList.Count == 1)
            {
                fullBatchList.SelectedIndex = 0;
                OnFullBatchListChanged(null, null);
            }

            batchNumbersPanel.Visible = true;
            checkAll.Checked = false;
        }
        #endregion


        #region Save Moved Items List
        private bool SaveMovedItemsList(int grp)
        {
            //-- Make table
            var movedItemsList = new List<MovedItemModel>();
            var shortReportList = GetFullShortReportList();
            
            var grpShortReportList = shortReportList.FindAll(model => model.DocStructGrp == grp);
            if (grpShortReportList.Count > 0)
            {
                //-- Create Table for display moved items
                var moveTable = new DataTable("MovedItems");
                moveTable.Columns.Add(new DataColumn("Old Item Number"));
                moveTable.Columns.Add(new DataColumn("New Item Number"));
                moveTable.AcceptChanges();
                foreach (var shortReportModel in grpShortReportList)
                {
                    foreach (var movedItemModel in shortReportModel.MovedItems)
                    {
                        movedItemsList.Add(movedItemModel);
                        moveTable.Rows.Add(new object[] { movedItemModel.FullItemNumberWithDotes, movedItemModel.NewFullItemNumberWithDotes });
                    }
                }
                moveTable.AcceptChanges();
                dgPopup.DataSource = moveTable;
                dgPopup.DataBind();
            }
            SetViewState(movedItemsList, SessionConstants.MovedItems);

            return movedItemsList.Count > 0;
        }
        #endregion
        private List<ShortReportModel> GetFullShortReportList()
        {
            return GetViewState(SessionConstants.ShortReportModelList) as List<ShortReportModel> ??
                   new List<ShortReportModel>();
        }
        #region Grouping Batch List
        protected void OnCheckedBatchListChanged(object sender, EventArgs e)
        {
            DataTable table = null;
            var hasCheckedBatch = !string.IsNullOrEmpty(checkedBatchList.SelectedValue);

            if (hasCheckedBatch)
            {
                var grp = Int32.Parse(checkedBatchList.SelectedValue);
                table = new ShortReportUtils(false).GetReportViewByGroup(grp, reorientNowFld.Text, GetFullShortReportList());
                btnMovedItems.Visible = SaveMovedItemsList(grp);
            } else
            {
                btnMovedItems.Visible = false;
            }
            cmdSaveExcel.Enabled = hasCheckedBatch;
            cmdSaveExcelQR.Enabled = hasCheckedBatch;
            btnExport.Enabled = hasCheckedBatch;
            cmdReorient.Visible = table != null && table.Rows.Count > 0;
            
            LblRowCounts.Text = table == null ? "" : string.Format("{0} rows", table.Rows.Count);
            gdPanel.Visible = hasCheckedBatch;

            //-- For sorting
            SetViewState(table, SessionConstants.ShortReportActive);
            
            UpdateDataView(table);

		}
        #endregion

        #region To Excel Selected Group
        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            var table = GenerateGroupTable(false);
            if (table == null) return;
            var fname = txtOrderNumber.Text.Trim() == "" ? "XXX" : txtOrderNumber.Text.Trim();
            var ds = new DataSet();
            ds.Tables.Add(table.Copy());
            string strExcel=ExcelUtils.ExportThrouPoiNew(ds, fname,true, this);
            var minute = 0;
            ScriptManager.RegisterStartupScript(this, GetType(), "downloadExcelFile", "downloadExcelFile('" + strExcel + "','" + fname + ".xlsx'," + minute + ");", true);
        }

        protected void OnExcelClickQR(object sender, ImageClickEventArgs e)
        {
            var table = GenerateGroupTable(false);
            if (table == null) return;
            var fname = txtOrderNumber.Text.Trim() == "" ? "XXX" : txtOrderNumber.Text.Trim();
            ExcelUtils.ExportThrouPoiQR(table, fname, this);
        }
		protected void btnExport_Click(object sender, EventArgs e)
		{
			try
			{
				DateTime dtStart = DateTime.Now;
				btnExport.Enabled = false;
				string strExcel = string.Empty;
				var fname = string.Empty;
				if (ddlReportType.SelectedValue == "1") //FullOrderWithQRLink
                {
					var table = GenerateGroupTable(false);
					if (table == null) return;
					//var fname = txtOrderNumber.Text.Trim() == "" ? "XXX" : txtOrderNumber.Text.Trim();
					fname = txtOrderNumber.Text.Trim() == "" ? "FullOrderWithQRLink" : "FullOrderWith_QR_Link_" + txtOrderNumber.Text.Trim();
					strExcel = ExcelUtils.ExportThrouPoiQR(table, fname, this);
				}
				else if (ddlReportType.SelectedValue == "2") //FullOrderWithImage
                {
					var dss = GenerateFullAuto(false);
					if (dss == null) return;
					fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrderWithImage" : "FullOrderWith_SKUImage_" + txtOrderNumber.Text.Trim();
					strExcel = ExcelUtils.ExportWithImage(dss, fname, false, this);
				}
				else if (ddlReportType.SelectedValue == "3") //FullOrderWithItemImage
                {
					var dss = GenerateFullAuto(false);
					if (dss == null) return;
					fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrderWithItemImage" : "FullOrderWith_ItemImage_QR_" + txtOrderNumber.Text.Trim();
					strExcel = ExcelUtils.ExportWithItemImage(dss, fname, false, this);
				}
				else if (ddlReportType.SelectedValue == "4") //FullOrderWithItemImage
                {
					var dss = GenerateFullAuto(false);
					if (dss == null) return;
					fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrderWithItemImage" : "FullOrderWith_ItemImage_QR_" + txtOrderNumber.Text.Trim();
					strExcel = ExcelUtils.ExportWithItemImageWithLink(dss, fname, false, this);
				}
                else if (ddlReportType.SelectedValue == "5") // FullOrder Only
                {
                    var dss = GenerateFullAuto(false);
                    if (dss == null) return;
                    fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrder" : "FullOrder_" + txtOrderNumber.Text.Trim();
                    strExcel = ExcelUtils.ExportThrouPoiNew(dss, fname, false, this);
                }
                else if (ddlReportType.SelectedValue == "6") // FullOrderWithLink
                {
                    var dss = GenerateFullAuto(false);
                    if (dss == null) return;
                    fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrderWithLink" : "FullOrderWith_Link_" + txtOrderNumber.Text.Trim();
                    strExcel = ExcelUtils.ExportThrouPoiLink(dss, fname, false, this);
                }
                DateTime dtEnd = DateTime.Now;
				var minute = (dtEnd - dtStart).TotalSeconds;
				ScriptManager.RegisterStartupScript(this, GetType(), "downloadExcelFile", "downloadExcelFile('" + strExcel + "','" + fname + ".xlsx'," + minute + ");", true);
			}
			catch (Exception x)
			{
				Console.Out.WriteLine(x.Message);
			}
			finally
			{
				btnExport.Enabled = true;
			}
		}
		private DataTable GenerateGroupTable(bool forEmail)
        {
            if (string.IsNullOrEmpty(checkedBatchList.SelectedValue)) return null;
            var grp = Int32.Parse(checkedBatchList.SelectedValue);
            var shortReportModelList = GetFullShortReportList();
            var dataTable = new ShortReportUtils(forEmail).GetReportViewByGroup(grp, PageConstants.DirHorizontal, shortReportModelList);
            if (dataTable == null) return null;
            var table = dataTable.Copy();
            var dv = new DataView { Table = table };

            // Apply sort information to the view
            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null && dv.Table.Columns.Contains(sortModel.ColumnName))
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }

            var result = table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < dv.Count; i++)
            {
                result.Rows.Add(dv[i].Row.ItemArray);
            }
            result.AcceptChanges();
            return result;
        }
        #endregion

        #region To Excel Checked Batch numbers (Full AUTO)
        protected void OnFullAutoClick(object sender, EventArgs e)
		{
            var dss = GenerateFullAuto(false);
            if (dss == null) return;
            var fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullAuto" : "FullAuto_" + txtOrderNumber.Text.Trim();
            ExcelUtils.ExportThrouPoi(dss, fname, false, this);
		}
        private DataSet GenerateFullAuto(bool forEmail)
        {
            var shortReportModelList = GetFullShortReportList();
            if (txtOrderNumber.Text.Trim().Length == 0)
            {
                return null;
            }
            //-- groups count
            var cnt = checkedBatchList.Items.Count;
            var dss = new DataSet();
            for (var i = 0; i < cnt; i++)
            {
                var table = new ShortReportUtils(forEmail).GetReportViewByGroup(i + 1, PageConstants.DirHorizontal, shortReportModelList);
                if (table != null)
                {
                    table.TableName = "" + (i + 1);
                    dss.Tables.Add(table);
                }
            }
            return dss;
        }
        #endregion


        #region To Excel Checked Batch numbers (Images per row)
        protected void btnFullOrderWithImage_Click(object sender, EventArgs e)
        {
            var dss = GenerateFullAuto(false);
            if (dss == null) return;
            var fname = txtOrderNumber.Text.Trim().Length == 0 ? "FullOrderWithImage" : "FullOrderWithImage_" + txtOrderNumber.Text.Trim();
            ExcelUtils.ExportWithImage(dss, fname, false, this);
        }
        #endregion


        #region Sort Command
        protected void GrdShortReportSortCommand(object source, DataGridSortCommandEventArgs e)
		{
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);


            UpdateDataView(GetViewState(SessionConstants.ShortReportActive) as DataTable);
        }
        
        void UpdateDataView(DataTable table)
        {
            if (table == null)
            {
                grdShortReport.DataSource = null;
                grdShortReport.DataBind();
                return;
            }
            var dv = new DataView {Table = table};
            if (grdShortReport.AllowSorting)
            {
                // Apply sort information to the view
                var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
                if (sortModel != null && dv.Table.Columns.Contains(sortModel.ColumnName))
                {
                    dv.Sort = sortModel.ColumnName;
                    if (!sortModel.Direction) dv.Sort += " DESC";
                }
            }

            // Rebind data 
            grdShortReport.DataSource = dv;
            grdShortReport.DataBind();
            
        }
        #endregion

        #region Reorient Button
        protected void OnReorientClick(object sender, ImageClickEventArgs e)
	    {
	        var nowDir = reorientNowFld.Text;
            var newDir = (nowDir == PageConstants.DirHorizontal ? PageConstants.DirVertical : PageConstants.DirHorizontal);
            reorientNowFld.Text = newDir;
	        //-- Reset sort data
            SetViewState(null, SessionConstants.CurrentSortModel);
            //-- remove sorting data
            grdShortReport.AllowSorting = (newDir == PageConstants.DirHorizontal);
	        OnCheckedBatchListChanged(null, null);

	    }
        #endregion

        #region Search by Customer & Memo

        protected void OnChangedDateFrom(object sender, EventArgs e)
	    {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.ParseExact(calFrom.Text, "m/d/yyyy", CultureInfo.InvariantCulture);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
            LoadMemoNumbers();
        }

	    protected void OnChangedDateTo(object sender, EventArgs e)
	    {
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.ParseExact(calTo.Text, "m/d/yyyy", CultureInfo.InvariantCulture);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
            LoadMemoNumbers();

	    }

	    protected void OnCustomerSelectedChanged(object sender, EventArgs e)
	    {
            LoadMemoNumbers();
	    }

        protected void OnLoadByMemoClick(object sender, EventArgs e)
        {
            if (TabContainer.ActiveTabIndex == 0) return;
            if (string.IsNullOrEmpty(lstMemos.SelectedValue)) return;
            var batchModelList = QueryUtils.GetBatchesByCustomerByDateRangeByMemoNumber(
                lstCustomerList.SelectedValue,
                CalendarExtenderFrom.SelectedDate,
                CalendarExtenderTo.SelectedDate,
                lstMemos.SelectedValue, this);
            if (batchModelList.Count == 0) return;
            var order = batchModelList[0].GroupCodeFiveChars;
            LoadBatchFullCheckList(QueryUtils.GetFullOrder(order, this));
        }
        
        #region Memo
        private void LoadMemoNumbers()
        {
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;
            lstMemos.SelectedValue = null;
            //-- Get Memos
            var memoList = QueryUtils.GetMemoNumbersByCustomerByDateRange(
                lstCustomerList.SelectedValue,
                CalendarExtenderFrom.SelectedDate,
                CalendarExtenderTo.SelectedDate, this);


            lstMemos.DataSource = memoList;
            lstMemos.DataBind();
            if (lstMemos.SelectedValue != null)
            {
                OnMemoSelectedChanged(null, null);
            }
        }
        protected void OnMemoSelectedChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lstMemos.SelectedValue))
            {
                OnLoadByMemoClick(null, null);
            }

        }

        #endregion
        
        #region Search Customer
        private List<CustomerModel>  GetCustomers()
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
                            new List<CustomerModel>();
            if (customers.Count == 0)
            {
                customers.AddRange(QueryUtils.GetCustomers(this));
                SetViewState(customers, SessionConstants.CustomersList);
            }
            return customers;
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick();

        }
        protected void OnCustomerLikeClick()
        {

            var customers = GetCustomers();
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        #endregion

        #endregion

        #region Tab Container
        private void StateValidators(int tabIndex)
        {
            OrderReq.Enabled = tabIndex == 0;
            OrderRegExpr.Enabled = tabIndex == 0;
        }

        protected void OnActiveTabChanged(object sender, EventArgs e)
	    {
            if (TabContainer.ActiveTabIndex == 1 && GetViewState(SessionConstants.CustomersList) == null)
            {
                lstCustomerList.DataSource = GetCustomers();
                lstCustomerList.DataBind();
            }
            StateValidators(TabContainer.ActiveTabIndex);
            ResetLoadData();
	    }
        #endregion

        #region Send Email
        protected void OnSendEmailClick(object sender, ImageClickEventArgs e)
        {
            var toAddress = AddressTo.Text.Trim();
            if (string.IsNullOrEmpty(toAddress))
            {
                SendEmailMessage.Text = "Please enter a 'Address to'!";
                return;
            }
            if (SendFullCheckbox.Checked)
            {
                //-- generate short report
                var dss = GenerateFullAuto(true);
                if (dss == null)
                {
                    SendEmailMessage.Text = "A 'Full Short Report' is empty!";
                    return;
                }
                //-- Generate excel file
                var fname = (txtOrderNumber.Text.Trim().Length == 0 ? "FullAuto" : "FullAuto_" + txtOrderNumber.Text.Trim()) + "_" + DateTime.Now.Ticks;
                ExcelUtils.ExportThrouPoi(dss, fname, true, this);

                //-- send email
                SendAttachedShortReport(fname, toAddress);
            } else
            {
                var table = GenerateGroupTable(true);
                if (table == null)
                {
                    SendEmailMessage.Text = "A 'Selected Short Report' is empty!";
                    return;
                }
                var dss = new DataSet();
                dss.Tables.Add(table);
                var fname = (txtOrderNumber.Text.Trim().Length == 0 ? "ShortReport" : "ShortReport_" + txtOrderNumber.Text.Trim()) + "_" + DateTime.Now.Ticks;
                //-- Generate excel file
                ExcelUtils.ExportThrouPoi(dss, fname, true, this);
                SendAttachedShortReport(fname, toAddress);
            }

        }
        protected void OnChoiceMailFromContacts(object sender, EventArgs e)
        {
            AddressTo.Text = MailAddressList.SelectedValue;
        }
        protected void OnSendFullChecked(object sender, EventArgs e)
        {
            SetButtonsState();
        }
        private void SendAttachedShortReport(string fname, string toAddress)
        {
            var result = ExcelUtils.SendMailWithAttachExcel(toAddress, fname, this);
            SendEmailMessage.Text = result;
        }
        #endregion

        #region Buttons State
        private void SetButtonsState()
        {
            //-- Send Email Button
            GoEmailButton.Enabled =
                (SendFullCheckbox.Checked && checkedBatchList.Items.Count > 0) ||
                (!SendFullCheckbox.Checked && !string.IsNullOrEmpty(checkedBatchList.SelectedValue));

            //-- Selected group batches download to Excel
            cmdSaveExcel.Enabled = 
                !string.IsNullOrEmpty(checkedBatchList.SelectedValue) &&
                grdShortReport.Items.Count > 0;

            cmdSaveExcelQR.Enabled =
               !string.IsNullOrEmpty(checkedBatchList.SelectedValue) &&
               grdShortReport.Items.Count > 0;

            btnExport.Enabled = cmdSaveExcelQR.Enabled;

            //Full order with Images
            btnFullOrderWithImage.Enabled = checkedBatchList.Items.Count > 0;

            //-- Full Auto Button
            cmdFullAuto.Enabled = checkedBatchList.Items.Count > 0;

            //-- Show Moved Items Button
            var movedItems = GetViewState(SessionConstants.MovedItems) as List<MovedItemModel>;
            btnMovedItems.Visible = movedItems != null && movedItems.Count > 0;
        }
        #endregion
    }
}
