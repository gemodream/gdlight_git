using System;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class Person
	{
		private String pFirstName;
		private String pLastName;
		private String pEmail;
		private String pPersonID;

		public Person()
		{
			pFirstName="";
			pLastName="";
			pEmail="";
			pPersonID="";
		}
		public Person(String firstName, String lastName, String email)
		{
			pFirstName = firstName;
			pLastName = lastName;
			pEmail = email;
		}
		public Person(String firstName, String lastName, String email, String personID)
		{
			pPersonID = personID;
			pFirstName = firstName;
			pLastName = lastName;
			pEmail = email;
		}
		public override string ToString()
		{
			return pFirstName + " " + pLastName;
		}

		public override int GetHashCode()
		{
			if (pPersonID!="")
				return Int32.Parse(pPersonID);
			else
			return base.GetHashCode ();
		}


		public String FirstName
		{
			get
			{
				return pFirstName;
			}
			set
			{
				pFirstName = value;
			}
		}
		public String LastName
		{
			get
			{
				return pLastName;
			}
			set
			{
				pLastName = value;
			}
		}
		public String Email
		{
			get
			{
				return pEmail;
			}
			set
			{
				pEmail = value;
			}
		}
        public String PersonId
        {
            get
            {
                return pPersonID;
            }
            set { pPersonID = value; }
        }
    }
}
