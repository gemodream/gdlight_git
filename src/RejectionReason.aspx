﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="RejectionReason.aspx.cs" Inherits="Corpt.RejectionReason" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">    
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">
        <div class="demoheading">Report Struct</div>
         <div style="height: 20px">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="10">
                 <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                     <b>Please, wait....</b>
                 </ProgressTemplate>
             </asp:UpdateProgress>
         </div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <div>
                 <asp:Button ID="Button1" runat="server" Text="Add Reason" CssClass="btn btn-small btn-info"
                     OnClick="OnAddReasonClick" /><br />
             </div>
             <table>
                <tr>
                    <td style="vertical-align: top; border: silver solid 1px; padding: 5px">
                        <div>
                            <asp:Label ID="Label1" runat="server" Text="Attached Measure" CssClass="label"></asp:Label>
                            <asp:Button ID="RemoveBtn" runat="server" Text="Remove Measure" CssClass="btn btn-small btn-info"
                                OnClick="OnDelMeasureClick" /><br />
                            <asp:TreeView ID="RuleTree" runat="server" Font-Names="Tahoma" 
                                Font-Size="small" AfterClientCheck="CheckChildNodes();" onclick="OnTreeClick(event)"
                                ForeColor="Black" Font-Bold="False" ExpandDepth="2" ShowLines="False" ShowExpandCollapse="true"
                                NodeIndent="17">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                        </div>
                    </td>
                    <td style="padding-left: 30px; vertical-align: top; padding: 5px">
                    </td>
                    <td style="width: 50px"></td>
                    <td style="vertical-align: top; border: silver solid 1px; padding: 5px">
                        <div>
                            <asp:Label ID="RulesTitle" runat="server" Text="Unattached Measure" CssClass="label"></asp:Label>
                            <asp:Button ID="AddMeasureBtn" runat="server" Text="Add Measures" OnClick="OnAddMeasureClick"
                                CssClass="btn btn-small btn-info" /><br />
                            <div style="overflow-y: auto; overflow-x: hidden; height: 500px; width: 300px; color: black">
                                <asp:DataGrid ID="grdMeasure" runat="server" AllowSorting="False" CssClass="table table-condensed"
                                    DataKeyField="MeasureId" AutoGenerateColumns="False">
                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White"
                                        Font-Size="small" />
                                    <ItemStyle ForeColor="#0D0D0D" BackColor="White" BorderColor="silver" Font-Size="Small">
                                    </ItemStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" Text='' ID="Checkbox1"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="MeasureName" HeaderText="Measure" />
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

              <!-- Add Reason Dialog  -->
             <asp:Panel runat="server" ID="AddReasonPanel" CssClass="modalPopup" Style="display: none;border: solid 2px #5377A9;color:black">
                 <asp:Panel ID="AddReasonDragHandle" runat="server" Style="cursor: move; background-color: #E9EDF0; border: solid 1px Silver; color: black; text-align: left">
                     <div style="text-align: center; color: #5377A9; font-weight: bold">
                         <asp:Label ID="AddReasonTitle" runat="server" Text="Add Reason"/>
                     </div>
                 </asp:Panel>
                 <asp:Panel ID="Panel2" runat="server" CssClass="form-inline" Style="padding-top: 10px;color:black" DefaultButton="HiddenBtnAdd">
                     Reason Name:
                     <asp:TextBox runat="server" ID="AddReasonFld" ForeColor="Black"/>
                     <asp:HiddenField ID="ReasonIdFld" runat="server" />
                 </asp:Panel>
                 <div style="padding-top: 10px">
                     <p style="text-align: center;">
                         <asp:Button ID="AddOkBtn" runat="server" OnClick="OnAddReasonOkClick" Text="Save" CssClass="btn btn-small"/>
                         <asp:Button ID="AddCancBtn" runat="server" Text="Cancel" CssClass="btn btn-small"/>
                     </p>
                 </div>
             </asp:Panel>
             <asp:Button ID="HiddenBtnAdd" runat="server" Style="display: none" />
             <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBtnAdd"
                 PopupControlID="AddReasonPanel" ID="AddReasonPopupExtender" PopupDragHandleControlID="AddReasonDragHandle"
                 OkControlID="AddCancBtn">
             </ajaxToolkit:ModalPopupExtender>

             <!-- Information Dialog  -->
             <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="display: none;
                 width: 310px; border: solid 2px #5390D5">
                 <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #E9EDF0; border: solid 1px Silver; color: black; text-align: left">
                     <div>
                         <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                         <asp:Label runat="server" ID="InfoTitle"></asp:Label>
                     </div>
                 </asp:Panel>
                 <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;
                     color: black; text-align: center" id="MessageDiv" runat="server">
                     Test
                 </div>
                 <div style="padding-top: 10px">
                     <p style="text-align: center; font-family: sans-serif">
                         <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                     </p>
                 </div>
             </asp:Panel>
             <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
             <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                 PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                 OkControlID="InfoCloseButton">
             </ajaxToolkit:ModalPopupExtender>

         </ContentTemplate>

         </asp:UpdatePanel>
     </div>
</asp:Content>
