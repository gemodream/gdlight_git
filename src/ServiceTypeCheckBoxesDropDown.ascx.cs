﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.Utilities;

// ReSharper disable ArrangeAccessorOwnerBody

namespace Corpt
{
    public partial class ServiceTypeCheckBoxesDropDown : UserControl
    {
        private const string IsDefaultSelectionAlreadySetName = "IsDefaultSelectionAlreadySet";

        public event CheckBoxesDropDownEventHandler SelectedIndexChanged;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                return;                        
            }

            if (this.CheckBoxListId.Items.Count > 0)
            {

                if (!IsDefaultSelectionAlreadySet)
                {
                    SetSelection(DefaultSelection);
                    IsDefaultSelectionAlreadySet = true;
                }
                else
                {
                    SetText(SelectedItems.Select(item => item.Value).ToArray());
                }
            }

            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "PopupHide")
            {
                OnSelectedIndexChanged(null, null);
            }
            
        }

        private bool IsDefaultSelectionAlreadySet
        {
            get
            {
                var o = this.ViewState[IsDefaultSelectionAlreadySetName];
                return o != null && (bool)o;
            }
            set
            {
                this.ViewState[IsDefaultSelectionAlreadySetName] = value;
            }
        }

        public string[] EmptySelection => new string[] { };

        public string[] DefaultSelection
        {
            get { return this.ViewState["ServiceTypesDefaultSelection"] as string[]; }
            set { this.ViewState["ServiceTypesDefaultSelection"] = value; }
        }

        [TypeConverter(typeof(StringArrayConverter))]
        public string[] ScreeningDefaultSelection
        {
            get { return this.ViewState["ScreeningDefaultSelection"] as string[]; }
            set { this.ViewState["ScreeningDefaultSelection"] = value; }
        }

        [TypeConverter(typeof(StringArrayConverter))]
        public string[] LabServicesDefaultSelection
        {
            get => this.ViewState["LabServicesDefaultSelection"] as string[];
            set => this.ViewState["LabServicesDefaultSelection"] = value;
        }

        public ServiceTypesCategory ServiceTypesCategory
        {
            get => (ServiceTypesCategory) this.ViewState["ServiceTypesCategory"];
            set => this.ViewState["ServiceTypesCategory"] = value;
        }

        public void SetSelectionByServiceTypeCategory(ServiceTypesCategory serviceTypeCategory)
        {
            ServiceTypesCategory = serviceTypeCategory;
            if (serviceTypeCategory == ServiceTypesCategory.ScreeningServices)
            {
                DefaultSelection = ScreeningDefaultSelection;
                SetSelection(ScreeningDefaultSelection);
            }
            else if (serviceTypeCategory == ServiceTypesCategory.LabServices)
            {
                DefaultSelection = LabServicesDefaultSelection;
                SetSelection(LabServicesDefaultSelection);
            }
            else if (serviceTypeCategory == ServiceTypesCategory.All)
            {
                DefaultSelection = null;
                SetSelection(this.Items.Select(item => item.Value).ToArray());
            }
            else if (serviceTypeCategory == ServiceTypesCategory.Custom)
            {
                DefaultSelection = null;
            }
        }

        [DefaultValue(typeof(Unit), "")]
        public Unit Width
        {
            get => this.TextBoxId.Width;
            set
            {
                this.TextBoxId.Width = value;
                this.CheckBoxListId.Width = value;
            }
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        public List<ListItem> SelectedItems => Items.Where(listItem => listItem.Selected).ToList();

        public string SelectedSummary => SelectedItems.Count == 0 ? 
            "Not Set" : 
            SelectedItems.Select(listItem => listItem.Text).Aggregate((summary, current) => summary + ", " + current);

        public List<ListItem> Items => this.CheckBoxListId.Items.Cast<ListItem>().ToList();

        [IDReferenceProperty(typeof(DataSourceControl))]
        // ReSharper disable once InconsistentNaming
        public string DataSourceID
        {
            get => this.CheckBoxListId.DataSourceID;
            set => this.CheckBoxListId.DataSourceID = value;
        }

        public void SetSelection(string[] selection)
        {
            if (selection == null )
            {
                Items.ForEach(item => item.Selected = true);
                return;
            }

            for (var index = 0; index < Items.Count; index++)
            {
                var listItem = this.CheckBoxListId.Items[index];
                listItem.Selected = selection.Contains(Items[index].Value);
            }

            SetText(selection);
        }

        private void SetText(string[] selection)
        {
            var text = "";
            if (selection.Length == 0)
            {
                text = "Nothing Selected";
            }
            else
            {
                var firstSelected = Items.FirstOrDefault(listItem => listItem.Value == selection[0]);
                if (firstSelected != null)
                {
                    text = firstSelected.Text;
                    if (selection.Length > 1)
                    {
                        text += ", ... etc.";
                    }
                }
            }

            this.TextBoxId.Text = text;
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedItems = SelectedItems.Select(item => item.Value == null ? (int?)null : int.Parse(item.Value)).ToList();
                SelectedIndexChanged?.Invoke(this, new CheckBoxesDropDownEventHandlerArgs(selectedItems, this.CheckBoxListId.Items.Count));
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this.Session);
                throw;
            }
        }

        public bool IsScreening(int id)
        {
            return this.ScreeningDefaultSelection.Contains(id.ToString());
        }

        protected void CheckBoxListId_OnDataBound(object sender, EventArgs e)
        {
            if (this.LabServicesDefaultSelection == null)
            {
                this.LabServicesDefaultSelection = InverseSelection(this.ScreeningDefaultSelection, Items);
                SetSelectionByServiceTypeCategory(this.ServiceTypesCategory);
            }
        }

        private static string[] InverseSelection(string[] subSet, List<ListItem> fullSet)
        {
            return fullSet
                .Where(item => !subSet.Contains(item.Value))
                .Select(item => item.Value)
                .ToArray();
        }

    }

    public delegate void CheckBoxesDropDownEventHandler(object sender, CheckBoxesDropDownEventHandlerArgs args);

    public class CheckBoxesDropDownEventHandlerArgs : EventArgs
    {
        public List<int?> SelectedServiceTypeIds { get; }
        public int ServiceTypesNumber { get; }

        public CheckBoxesDropDownEventHandlerArgs(List<int?> selectedServiceTypeIds, int serviceTypesNumber)
        {
            SelectedServiceTypeIds = selectedServiceTypeIds;
            ServiceTypesNumber = serviceTypesNumber;
        }
    }

    class StringArrayConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            return !(value is string valueStr) 
                ? new string[0] 
                : valueStr.Split(',').Select(s => s.Trim()).ToArray();
        }
    }
}