﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ExpressGrading.aspx.cs" Inherits="Corpt.ExpressGrading" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
    <style type="text/css">
        .auto-style1 {
            width: 988px;
        }
        table
		{
			border-collapse: separate !important;
		}
					table td {
		  position: relative;
		}

		.table td input {
		  position: absolute;
		  display: block;
		  top:0;
		  left:0;
		  margin: 0;
		  height: 100% !important;
		  width: 100%;
		  border-radius: 0 !important;
		  border: none;
		  padding: 10px;
		  box-sizing: border-box;
		}
		.centered {
			position: fixed;
			top: 25%;
			left: 50%;
			transform: translate(-50%, -50%);
			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
		}
        .headerClass > th {
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">
            Express Grading</div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="navbar nav-tabs">
                    <asp:Panel runat="server" ID="NavPanel">
                        <table>
                            <tr>
                                <td style="padding-top: 10px; padding-bottom: 5px; font-size: small; width: 400px">
                                    <asp:Panel ID="Panel" runat="server" DefaultButton="LoadButton" Width="380px">
                                        Batch Number:
                                        <asp:TextBox runat="server" ID="BatchNumberFld" MaxLength="9" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item Numbers" ImageUrl="~/Images/ajaxImages/search.png"
                                            OnClick="OnLoadClick" />
                                        <asp:HiddenField runat="server" ID="BatchField" />
                                        <asp:HiddenField runat="server" ID="OrderField" />
                                    </asp:Panel>
                                </td>
                                <td style="padding-top: 0px; font-size: small">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="UpdateButton" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:Button runat="server" ID="UpdateButton" Text="Update" OnClick="OnUpdateButtonClick"
                                                CssClass="btn btn-info" Enabled="False" />
                                            <asp:Button runat="server" ID="NextOrderButton" Text="Next Order" OnClick="OnNextOrderClick"
                                                Enabled="False" CssClass="btn btn-info" />
                                            <asp:Button runat="server" ID="UndoAllButton" Text="Undo All" OnClick="OnUndoAllButtonClick"
                                                CssClass="btn btn-info" Enabled="False" />
                                             <asp:Button runat="server" ID="ResetButton" Text="Reset" OnClick="ResetButton_OnClick"
                                                CssClass="btn btn-info"  Enabled="False"/>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                    <asp:Label ID="LoadMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <div runat="server" id="InfoPanel1">                    
                    <table class="auto-style1">
                        <tr>
                            <td style="vertical-align: top; width: 250px;">
                                <asp:UpdatePanel runat="server" ID="BatchTreePanel">
                                    <ContentTemplate>
                                        <asp:TreeView ID="tvwItems" runat="server" ShowCheckBoxes="All" ExpandDepth="1" AfterClientCheck="CheckChildNodes();"
                                            PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeNodeCheckChanged"
                                            OnSelectedNodeChanged="OnTreeSelectedChanged" onclick="OnTreeClick(event)">
                                            <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                            <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                        </asp:TreeView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            </tr>
                        <tr>
                            <td colspan="4">
                                <div id="Rules" runat="server" style="width: 100%; height: 500px; overflow-y:auto">
                                    <asp:GridView class="table table-hover table-bordered" ID="RulesGrid" runat="server" AutoGenerateColumns="false" >
                                        <HeaderStyle CssClass="headerClass"/>
										<Columns>
											<asp:TemplateField HeaderText="Measure">
                                                <HeaderStyle Width="230px" />
												<ItemStyle Width="230px" />
												<ItemTemplate>
													<asp:HiddenField ID="HdnMeasureCode" runat="server" Value='<%#Eval("MeasureCode") %>' />
                                                    <asp:Label runat="server"><%#Eval("MeasureName") %></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
                                            <asp:TemplateField HeaderText="Measure Value">
                                                <HeaderStyle Width="230px" />
												<ItemStyle Width="230px" />
												<ItemTemplate>
                                                    <asp:Label runat="server"><%#Eval("MeasureValueSku") %></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
                                            <asp:TemplateField HeaderText="Failed">
                                                <HeaderStyle Width="60px" />
												<ItemStyle Width="60px" />
												<ItemTemplate>
                                                     <asp:CheckBox ID="FailedCheckbox" runat="server" Checked='<%#Eval("Failed") %>' OnCheckedChanged="FailedCheckbox_CheckedChanged" AutoPostBack="true" />
												</ItemTemplate>
											</asp:TemplateField>
                                            <asp:TemplateField HeaderText="Real Value">
                                                <HeaderStyle Width="230px" />
												<ItemStyle Width="230px" />
												<ItemTemplate>
                                                     <asp:TextBox runat="server" ID="MeasureValueReal" OnTextChanged="RealValue_TextboxChanged" AutoPostBack="true" Width="100%"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateField>
									
										</Columns>
									</asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="PassButton" />
                                            <asp:PostBackTrigger ControlID="FailButton" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:Button runat="server" ID="PassButton" Text="Pass" OnClick="OnPassButtonClick"
                                                CssClass="btn btn-info" Enabled="True" />
                                            <asp:Button runat="server" ID="FailButton" Text="Failed" OnClick="OnFailedButtonClick"
                                                Enabled="False" CssClass="btn btn-info" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
