using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


namespace Corpt
{
	/// <summary>
	/// Summary description for SwapItems.
	/// </summary>
	public partial class SwapItems : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			cmdSwap.Attributes.Add("onclick", "return confirm('Are you sure you want to copy?');");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdSwap_Click(object sender, System.EventArgs e)
		{
			if(txtItemOne.Text.Trim().Length!=10)
			{
				lblInfo.Text="Plese enter a valid 10-digit item number.";
				return;
			}
			lblInfo.Text=txtItemOne.Text.Trim();
			
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState",DBNull.Value);
			command.Parameters.Add("@EGroupState",DBNull.Value);
			command.Parameters.Add("@BState",DBNull.Value);
			command.Parameters.Add("@EState",DBNull.Value);
			command.Parameters.Add("@BDate",DBNull.Value);
			command.Parameters.Add("@EDate",DBNull.Value);
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value = int.Parse(txtItemOne.Text.Trim().Substring(0,5));
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value = int.Parse(txtItemOne.Text.Trim().Substring(5,3));
			command.Parameters.Add("@ItemCode",SqlDbType.Int).Value = int.Parse(txtItemOne.Text.Trim().Substring(8,2));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			int itemTypeID;
			dgDebug.DataSource=dt;
			dgDebug.DataBind();

		}
	}
}
