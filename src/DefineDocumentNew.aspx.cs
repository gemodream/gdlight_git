﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DefineDocumentNew : CommonPage
    {
        //<iframe src="PdfMapHandler.ashx?pdf=a.pdf" width="600" height="400" runat="server" ID="PdfFrame"></iframe>
        public static string ParamDocTypeCode = "DocTypeCode";
        public static string ParamCpKey = "CpKey"; //-CPOfficeID_CPID
        public static string ParamCpDocId = "CpDocId";
        public static string ParamOperationTypeId = "OperationTypeId";
        public static string ParamDebug = "Debug";
        private const string CmdAddValue = "CmdAddValue";
        private const string CmdAddTitle = "CmdAddTitle";
        private const string ProgramName = "ProgramName";
        private const string DocTypeName = "DocTypeName";
        private const string ParamReportKey = "Report_Key";
        public bool access = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Label2.Visible = false;
            DetailLabel.Visible = false;
            ImportExport.Visible = false;
            SeparatorList.Visible = false;
            string err = "";
            access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.CustomerProgramAccess, this, out err);
            if (!access || err != "")
            {
                SaveAsBtn.Enabled = false;
                SaveBtn.Enabled = false;
                AttachBtn.Enabled = false;
            }
            if (IsPostBack)
            {
                var initData = GetViewState(SessionConstants.DdInitData) as DefineDocumentInitModel;
                if (initData != null)
                {
                    Page.Title = PageConstants.AppTitle + initData.DocumentType.DocumentTypeName;
                }
                PostBackHandler();
                return;
            }
            MeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(MeasureList, CmdAddValue));
            TitleList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(MeasureList, CmdAddTitle));
            CorelFilesList.Attributes.Add("onchange", ClientScript.GetPostBackEventReference(MeasureList, CmdAddTitle));
            IniData();
            //alex 6/22
            if (Session["CP_View"] != null)//Call comes from view
            {
                DocumentList.Enabled = false;
                BackToSku.Enabled = false;
                Session["CP_View"] = null;
            }
            //alex 6/22
        }

        #region PostBackHandler
        private void PostBackHandler()
        {
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddValue)
            {
                OnInsertValueEvent();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddTitle)
            {
                OnInsertTitle();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddTitle)
            {
                OnCorelFileChange();
            }

        }
        #endregion

        private const string ValueField = "ValueField";
        private const string TitleField = "TitleField";
        private const string UnitField = "UnitField";
        private const string SelectCheckBox = "SelectCheckBox";

        #region Insert Value, Insert Title
        private void OnInsertValueEvent()
        {
            var part = PartTree.SelectedNode == null ? "" : PartTree.SelectedNode.Text.Trim();
            var measure = MeasureList.SelectedItem == null ? "" : MeasureList.SelectedItem.Text.Trim();
            var documentId = DocumentList.SelectedValue;
            if (string.IsNullOrEmpty(part) || string.IsNullOrEmpty(measure) || string.IsNullOrEmpty(documentId)) return;
            DataGridItem currItem = GetSelectedGridItem();

            if (currItem == null) return;
            var valueFld = currItem.FindControl(ValueField) as TextBox;
            if (valueFld != null)
            {
                valueFld.Text = SeparatorModel.AddFormatingValue(valueFld.Text, part, measure,
                                                                 SeparatorList.SelectedValue);
            }
        }
        
        private void OnInsertTitle()
        {
            var title = TitleList.SelectedItem == null ? "" : TitleList.SelectedItem.Text.Trim();
            var documentId = DocumentList.SelectedValue;
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(documentId)) return;
            var currItem = GetSelectedGridItem();
            if (currItem == null) return;
            var titleFld = currItem.FindControl(TitleField) as TextBox;
            if (titleFld != null)
            {
                titleFld.Text = SeparatorModel.AddFormatingTitle(titleFld.Text, title,
                                                                 SeparatorList.SelectedValue);
            }
        }
        private void OnCorelFileChange(string corelFile = null)
        {
            if (corelFile == null || corelFile == "")
                corelFile = CorelFilesList.SelectedItem == null ? "" : CorelFilesList.SelectedItem.Text.Trim();
            corelFile = corelFile.ToLower();
            if (corelFile.Contains(".xls"))
            {
                var ImageUrl = "Images/ajaxImages/no_image1.png";
                NewCorelFileButton.ImageUrl = ImageUrl;
                NewCorelFileButton.Width = 200;
                NewCorelFileButton.Visible = true;
            }
            else
            {
                corelFile = corelFile.Replace(".cdr", ".jpg");
                //corelFile = "2361400.jpg";
                string errMsg;
                MemoryStream ms = new MemoryStream();
                //Stream stream = new Stream();
                var fileType = "jpg";
                bool found = Utlities.GetJpgUrlAzure(corelFile, this, out errMsg, out ms);
                //Stream stream = Utlities.GetPdfUrlAzure(corelFile, this, out errMsg);
                if (found)
                {
                    //var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                    var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"cdr_to_jpg/" + corelFile.ToLower();
                    NewCorelFileButton.ImageUrl = ImageUrl;
                    NewCorelFileButton.Width = 300;
                    NewCorelFileButton.Visible = true;
                }
                else
                {
                    var ImageUrl = "Images/ajaxImages/no_image1.png";
                    NewCorelFileButton.ImageUrl = ImageUrl;
                    NewCorelFileButton.Width = 200;
                    NewCorelFileButton.Visible = true;
                }
            }
            //using (var fileStream = new FileStream(dest, FileMode.Create, FileAccess.Write))
            //{
            //    stream.CopyTo(fileStream);
            //}
            //PdfFrame.Src = "PdfMapHandler.ashx?pdf=" + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
            //PdfFrame.Attributes["src"] = "PdfMapHandler.ashx?pdf=" + dest;// + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);

        }
        protected void OnNewPictureClick(object sender, ImageClickEventArgs e)
        {
            var item = sender as ImageButton;
            if (item.ImageUrl.Contains("no_image") || item.ImageUrl == "")
                return;
            NewCorelFilePicture.ImageUrl = item.ImageUrl;

            NewPicturePopupExtender.Show();
        }
        //Nimesh code Start
        private DataGridItem GetSelectedGridItem()
        {
            string focusedControlData = focusedControlId.Value;
            foreach (DataGridItem item in ValuesGrid.Items)
            {
                var txtBox = item.FindControl(ValueField) as TextBox;
                if (txtBox != null)
                {
                    if (txtBox.ClientID == focusedControlData)
                        return item;
                }
            }
            return null;
        }
        //Nimesh code End
        private DataGridItem GetSelectedGridItem_Old()
        {
            foreach (DataGridItem item in ValuesGrid.Items)
            {
                var chBox = item.FindControl(SelectCheckBox) as CheckBox;
                if (chBox == null) return null;
                if (!chBox.Checked) continue;
                return item;
            }
            return null;
        }
        #endregion

        #region Init Data
        private void IniData()
        {
            //-- Customer Program
            var showDebug = Request.Params[ParamDebug] != null;
            DownloadBtn.Visible = showDebug;

            var cpKey = Request.Params[ParamCpKey];
            var cpOfficeId = cpKey.Split('_')[0];
            var cpId = cpKey.Split('_')[1];
            var cpDocId = Request.Params[ParamCpDocId];
            var docTypeCode = Request.Params[ParamDocTypeCode];
            var operationTypeId = Request.Params[ParamOperationTypeId];
            string itemTypeId = (string)Session["ItemTypeId"];
            var cpReports = new List<CpDocPrintModel>();
            if (Session["RVEditData"] != null)
            {
                var saveData = (CpEditModel)Session["RVEditData"];
                SetViewState(saveData, SessionConstants.CpEditModel);
                itemTypeId = saveData.Cp.ItemTypeId.ToString();
                cpReports = saveData.Reports;
                Session["RVEditData"] = null;
            }
            string programName = Request.Params[ProgramName];
            string docTypeName = Request.Params[DocTypeName];
            //alex 6/29
            string reportKey = Request.Params[ParamReportKey];
            string corelFileName = null;
            if (reportKey != null)
            {
                string[] rKeys = reportKey.Split('_');
                operationTypeId = rKeys[1];
             }  
            //alex /29
            if ((string.IsNullOrEmpty(cpId) || string.IsNullOrEmpty(cpOfficeId) || string.IsNullOrEmpty(docTypeCode)) && string.IsNullOrEmpty(itemTypeId)) return;
            DefineDocumentInitModel initData = new DefineDocumentInitModel();
            if (string.IsNullOrEmpty(itemTypeId))
                initData = QueryCpUtilsNew.GetDefineDocumentInit(cpId, cpOfficeId, docTypeCode, cpDocId, this);
            else
                initData = QueryCpUtilsNew.GetDefineDocumentInit(cpId, cpOfficeId, docTypeCode, cpDocId, this, itemTypeId);
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Documents, Page);
            //Remove items from a list of documents with a DocumentName equals value from block list + remove doubles
            initData.Documents = initData.Documents.Where(x => !blockList.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName))).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault()).ToList();
            if (cpReports.Count > 0)
            {
                initData.Documents = initData.Documents.Where(x => cpReports.Exists(y => x.DocumentName.Equals(y.DocumentName))).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault()).ToList();
            }
            SetViewState(initData, SessionConstants.DdInitData);
            if (initData.Cp != null)
                ShowPicture(initData.Cp.Path2Picture);
            if (initData.Documents.Count != 0)
            {
                var currentDocument = initData.Documents.Find(m => m.OperationTypeId == operationTypeId);
                if (currentDocument != null)
                corelFileName = currentDocument.CorelFile;
            }

            //-- Languages
            LanguageList.DataSource = QueryCpUtilsNew.GetLanguages(this);
            LanguageList.DataBind();
            LanguageList.SelectedIndex = 0;

            //-- Title Reference
            OnLanguageChanged(null, null);
            SetViewState(QueryCpUtilsNew.GetMeasuresShort(this), SessionConstants.CpCommonShortMeasures);
            //-- Parts Tree
            //LoadPartsTree(initData.MeasureParts);
            LoadPartsTree(initData.MeasureParts);
            if (programName == null)
                programName = initData.Cp.CustomerProgramName;
            PageTitleLabel.Text = string.Format("Define Document - Document Type: {0}, SKU: {1}",
                initData.DocumentType.DocumentTypeName,
                programName);
            //initData.Cp.CustomerProgramName);
            if (docTypeName == null)
                docTypeName = initData.DocumentType.DocumentTypeName;
            Page.Title = PageConstants.AppTitle + docTypeName;
            //Page.Title = PageConstants.AppTitle + initData.DocumentType.DocumentTypeName;
            //-- Import List
            ImportList.DataSource = initData.ImportInfo;
            ImportList.DataBind();

            //-- Export List
            ExportList.DataSource = initData.ExportInfo;
            ExportList.DataBind();

            //-- File Formats 
            FormatList.DataSource = initData.FormatInfo;
            FormatList.DataBind();

            //-- Documents
            LoadCorelFilesList(docTypeCode);
            LoadDocumentsList(operationTypeId);
            OnCorelFileChange(corelFileName);

            //-- Separator List
            SeparatorList.DataSource = SeparatorModel.GetSeparatorList();
            SeparatorList.DataBind();
            SeparatorList.SelectedValue = "1";
        }
        private DefineDocumentInitModel GetInitDataFromView()
        {
            return GetViewState(SessionConstants.DdInitData) as DefineDocumentInitModel ?? new DefineDocumentInitModel();
        }
        #endregion

        #region Cp Picture
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
            var ms = new MemoryStream();
            var fileType = "";
            var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
            //var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
                var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exists";
            }

            ErrPictureField.Text = errMsg;

        }

        #endregion

        #region Save, Update, Attach, Clear Buttons
        protected void OnSaveAsClick(object sender, EventArgs e)
        {


        }
        protected void OnBackToSkuClick(object sender, EventArgs e)
        {
            Session["AttachedReport"] = null;
            Session["BackToSku"] = "true";
            Response.Redirect("CustomerProgramNew.aspx");
        }
        protected void OnSaveClick(object sender, EventArgs e)
        {
            Page.Validate("DocumentValidatorGrp");
            if (!Page.IsValid)
            {
                PopupInfoDialog("Not all requirements are satisfied for documents", "Save", true);
                return;
            }
            var msg = QueryCpUtilsNew.UpdateDefineDocument(GetDocumentDetails(), this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Save document", true);
            }
            else
            {
                PopupInfoDialog("Document was successfully updated.", "Save document", false);
            }
        }
        private DocPredefinedModel GetDocumentDetails()
        {
            var document = new DocPredefinedModel
            {
                DocumentId = DocumentList.SelectedValue,
                DocumentName = DocumentList.SelectedItem.Text,
                UseDate = UseDateCb.Checked,
                UseVvNumber = UseVvNumberCb.Checked,
                BarCodeFixedText = BarCodeOptionList.SelectedIndex == 0 ? "" : FixedTextFld.Text,
                ImportTypeId = ImportList.SelectedValue,
                ExportTypeId = ExportList.SelectedValue,
                FormatTypeId = FormatList.SelectedValue,
                CorelFile = CorelFilesList.SelectedValue,
                DocumentTypeCode = GetInitDataFromView().DocumentType.DocumentTypeCode,
                DocumentValues = GetValuesFromGrid(DocumentList.SelectedValue, false)
            };
            return document;
        }
        protected void OnAttachClick(object sender, EventArgs e)
        {
            var document = GetCurrDocument();
            //alex 6/29
            //if (document.IsAttachedMyCp != "N")
            //{
            //    UpdateStateSaveButtons();
            //    return;
            //}
            //alex 6/29
            var initData = GetInitDataFromView();
            if (initData.Cp != null)
            {
                var msg = QueryCpUtilsNew.AttachDocumentToCp(document.DocumentId, initData.Cp.CpId, initData.Cp.CpOfficeId, this);
                if (!string.IsNullOrEmpty(msg))
                {
                    PopupInfoDialog(msg, "Attach document", true);
                }
                else
                {
                    AttachedRefresh();
                    PopupInfoDialog("Document was successfully attached.", "Attach document", false);
                }
            }
            var saveData = GetViewState(SessionConstants.CpEditModel) as CpEditModel;
            var cpDocId = Request.Params[ParamCpDocId];
            if (saveData != null && cpDocId != "")
            {      
                string attachedReport = DocumentList.SelectedValue;
                string attachedReportName = DocumentList.SelectedItem.Text;
                CpDocPrintModel extraModel = new CpDocPrintModel(); // new attached report
                CpDocPrintModel oldModel = new CpDocPrintModel(); // old attached report
                extraModel = saveData.Reports.Find(m => m.DocumentName == attachedReportName && m.OperationTypeOfficeId != null);
                extraModel.DocId = Convert.ToInt32(cpDocId);
                var reportType = document.DocumentTypeName;
                if (cpDocId != "" && extraModel !=null)
                {
                    var docAttached = saveData.CpDocs.Find(d => d.CpDocId == Convert.ToInt32(cpDocId)).AttachedReports;
                    oldModel = docAttached.Find(r => r.OperationTypeName.Contains(reportType));
                    docAttached.Remove(oldModel);
                    docAttached.Add(extraModel);
                    saveData.CpDocs.Find(d => d.CpDocId == Convert.ToInt32(cpDocId)).AttachedReports = docAttached;
                }
                OnCorelFileChange(attachedReport);
                var allOPers = QueryCpUtilsNew.GetAllOperations(this);
                saveData.OperationTypeOfficeId =
                    allOPers.FindAll(m => !string.IsNullOrEmpty(m.OperationTypeOfficeId))[0].OperationTypeOfficeId;
                var msg = QueryCpUtilsNew.SaveCustomerProgram(saveData, false, null, this);
                if (!msg.Contains("Error"))
                {
                    SetViewState(null, SessionConstants.CpEditModel);
                    //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
                }
                else 
                {
                    PopupInfoDialog(msg, "Save Program Error", true);
                }
            }
            else if (Session["CpReturnData"] != null)
            {
                Session["AttachedReport"] = document.DocumentName;
                Response.Redirect("CustomerProgramNew.aspx");
            }
            else if (Session["BatchReturnData"] != null)
            {
                Session["AttachedReport"] = document.DocumentName;
                Response.Redirect("CustomerProgramNew.aspx");
            }

        }
        private void AttachedRefresh()
        {
            var document = GetCurrDocument();
            var initData = GetInitDataFromView();
            bool attached = false;
            if (initData.CpId != null && initData.CpId != "")
                attached = QueryCpUtilsNew.IsAttachedDocument(document.DocumentId, initData.CpId, initData.CpOfficeId,
                                                           this);
            document.IsAttachedMyCp = (attached ? "Y" : "N");
            UpdateStateSaveButtons();
        }
        protected void OnNewClick(object sender, EventArgs e)
        {

        }
        #endregion

        #region Part Tree
        private void LoadPartsTree(IEnumerable<MeasurePartModel> parts)
        {
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PartTree.Nodes.Clear();
            PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Select();
            OnPartsTreeChanged(null, null);
        }

        //protected void OnPartsTreeChanged(object sender, EventArgs e)
        //{
        //    var selNode = PartTree.SelectedNode;
        //    if (selNode == null || string.IsNullOrEmpty(selNode.Value))
        //    {
        //        MeasureList.Items.Clear();
        //        return;
        //    }
        //    var partId = selNode.Value;
        //    var initData = GetInitDataFromView();
        //    var measures = initData.MeasuresByParts.FindAll(m => "" + m.PartId == partId);
        //    measures.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureName.ToUpper(), m2.MeasureName.ToUpper()));
        //    MeasureList.DataSource = measures;
        //    MeasureList.DataBind();
        //}
        protected void OnPartsTreeChanged(object sender, EventArgs e)
        {
            var selNode = PartTree.SelectedNode;
            if (selNode == null || string.IsNullOrEmpty(selNode.Value))
            {
                MeasureList.Items.Clear();
                return;
            }
            var partId = selNode.Value;
            var initData = GetInitDataFromView();
            var measuresShort = GetViewState(SessionConstants.CpCommonShortMeasures) as List<MeasureShortModel> ??
                           new List<MeasureShortModel>();
            var parts = initData.MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            var byType = measuresShort.FindAll(m =>"" + m.PartTypeId == part.PartTypeId.ToString());
            byType.Sort((m1, m2) => String.CompareOrdinal(m1.MeasureTitle.ToUpper(), m2.MeasureTitle.ToUpper()));
            MeasureList.DataSource = byType;
            MeasureList.DataBind();
        }
        #endregion

        #region Languages and DocumentTitles
        protected void OnLanguageChanged(object sender, EventArgs e)
        {
            var titles = QueryCpUtilsNew.GetDocumentTitles(LanguageList.SelectedValue, this);
            TitleList.DataSource = titles;
            TitleList.DataBind();

        }
        #endregion

        #region Documents
        private void LoadCorelFilesList(string docTypeCode)
        {
            string errMsg;
            var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                RepDirLabel.Text = errMsg;
            }
            else
            {
                RepDirLabel.Text = "";
            }
            var documents = GetInitDataFromView().Documents;

            foreach (var doc in documents)
            {
                if (doc.CorelFile != null && doc.CorelFile != "")
                    doc.CorelFile = (doc.CorelFile).ToLower();
                if (existsFiles.Find(m => m.Name == doc.CorelFile) == null)
                {
                    existsFiles.Add(new CorelFileModel { Name = doc.CorelFile, Exists = false });
                }
            }
            /*IvanB start*/
            //create and fill new blocklist
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CorelFiles, Page);
            //Remove items from a list of CorelFiles with a DisplayName equals value from block list 
            existsFiles = existsFiles.Where(x => !excluded.Exists(y => x.DisplayName.Equals(y.BlockedDisplayName))).ToList();
            List<CorelFileModel> finalExistsFiles = new List<CorelFileModel>();
            DataTable dt = QueryCpUtilsNew.GetCdrXlsFiles(docTypeCode, this);
            List<CorelFileModel> dbFiles = new List<CorelFileModel>();
            foreach (DataRow dr in dt.Rows)
            {
                string fileName = dr["CorelFile"].ToString();
                var dbFile = existsFiles.Find(x => x.DisplayName.ToLower() == fileName.ToLower());
                if (dbFile != null)
                    dbFiles.Add(dbFile);
            }
            List<CorelFileModel> dbSortedList = dbFiles.OrderBy(o => o.Name).ToList();
            CorelFilesList.DataSource = dbSortedList;
            //if (docTypeCode == "8")//LBL - use xls files only
            //    finalExistsFiles = existsFiles.FindAll(x => x.DisplayName.ToLower().Contains(".xls"));
            //else
            //{
                
            //    finalExistsFiles = existsFiles.FindAll(x => x.DisplayName.ToLower().Contains(".cdr"));
            //}
            //List<CorelFileModel> SortedList = finalExistsFiles.OrderBy(o => o.Name).ToList();
            //CorelFilesList.DataSource = SortedList;
            //CorelFilesList.DataSource = existsFiles;
            /*IvanB end*/

            CorelFilesList.DataBind();
        }
        private void LoadDocumentsList(string operationTypeId)
        {
            var data = GetInitDataFromView();
            /*IvanB start*/
            //create and fill blocklist
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Documents, Page);
            //Remove items from a list of documents with a DocumentName equals value from block list + remove doubles
            var result = data.Documents.Where(x => !excluded.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName))).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault());

            DocumentList.DataSource = result;
            /*IvanB end*/

            DocumentList.DataBind();
            if (!string.IsNullOrEmpty(operationTypeId))
            {
                var initDoc = data.Documents.Find(m => m.OperationTypeId == operationTypeId);
                if (initDoc != null)
                {
                    DocumentList.SelectedValue = initDoc.DocumentId;
                    OnDocumentChanged(null, null);
                    return;
                }
            }
            if (DocumentList.SelectedItem != null)
            {
                OnDocumentChanged(null, null);
                return;
            }
            if (DocumentList.SelectedItem == null && data.Documents.Count > 0)
            {
                DocumentList.SelectedIndex = 0;
                OnDocumentChanged(null, null);
            }
        }
        protected void OnDocumentChanged(object sender, EventArgs e)
        {
            SetDocumentDetails();
            OnCorelFileChange();
        }
        private void UpdateStateSaveButtons()
        {
            var document = GetCurrDocument();
            //alex 6/29
            //AttachBtn.Enabled = (document != null && document.DocumentId != "0" && document.IsAttachedMyCp != "Y");
            AttachBtn.Enabled = (document != null && document.DocumentId != "0" && access);
            //alex 6/29
            SaveBtn.Enabled = (document != null && document.DocumentId != "0" && access);
            SaveAsBtn.Enabled = (document != null && access);
        }
        private void SetDocumentDetails()
        {

            var document = GetCurrDocument();
            if (document == null)
            {
                UpdateStateSaveButtons();
                return;
            }

            ImportList.SelectedValue = document.ImportTypeId;
            ExportList.SelectedValue = document.ExportTypeId;
            FormatList.SelectedValue = document.FormatTypeId;
            /*IvanB start*/
            /*if saved selected colerFile for document is in block list*/
            /*we need to insert this item to CorelFilesList for show as selected*/
            var selV = CorelFilesList.Items.FindByValue(document.CorelFile);
            if (selV == null)
            {
                CorelFilesList.Items.Insert(0, new ListItem(document.CorelFile, document.CorelFile));
            }
            /*IvanB end*/
            CorelFilesList.SelectedValue = document.CorelFile;
            UseDateCb.Checked = document.UseDate;
            UseVvNumberCb.Checked = document.UseVvNumber;

            var barCodeIsFixed = !string.IsNullOrEmpty(document.BarCodeFixedText);
            FixedTextFld.Text = document.BarCodeFixedText;
            BarCodeOptionList.SelectedIndex = (barCodeIsFixed ? 1 : 0);
            OnBarCodeOptionChanged(null, null);

            if (document.DocumentValues.Count == 0)
            {
                document.DocumentValues = QueryCpUtilsNew.GetDocumentValues(document.DocumentId, this);
            }
            //-- Load Document Values
            LoadValuesGrid(document.DocumentValues);

            //-- Attached
            if (string.IsNullOrEmpty(document.IsAttachedMyCp))
            {
                AttachedRefresh();
            }
            UpdateStateSaveButtons();
        }
        private DocPredefinedModel GetCurrDocument()
        {
            var docId = DocumentList.SelectedValue;
            return GetInitDataFromView().Documents.Find(m => m.DocumentId == docId);
        }
        #endregion

        #region Document Values Grid
        private void LoadValuesGrid(List<DocumentValueModel> values)
        {
            foreach (DocumentValueModel value in values)//alex 05012024 enable all records
                value.Selected = true;
            values.Sort((m1, m2) => ComparerUtils.NumberCompare(m1.DocumentValueId, m2.DocumentValueId));
            ValuesGrid.DataSource = values;
            ValuesGrid.DataBind();
        }
        protected void OnValuesDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var valueModel = e.Item.DataItem as DocumentValueModel;
            if (valueModel == null) return;
            var document = GetCurrDocument();
            var minId = document.DocumentValues.Min(m => m.DocumentValueId);
            var maxId = document.DocumentValues.Max(m => m.DocumentValueId);
            var documentValueId = Convert.ToInt32(ValuesGrid.DataKeys[e.Item.ItemIndex]);

            var moveUpBtn = e.Item.FindControl("MoveUpBtn");
            if (moveUpBtn != null)
            {
                moveUpBtn.Visible = documentValueId != minId;
            }

            var moveDownBtn = e.Item.FindControl("MoveDownBtn");
            if (moveDownBtn != null)
            {
                moveDownBtn.Visible = documentValueId != maxId;
            }
            var selectFld = e.Item.FindControl(SelectCheckBox) as CheckBox;
            if (selectFld != null) selectFld.Checked = valueModel.Selected;

            var titleFld = e.Item.FindControl(TitleField) as TextBox;
            if (titleFld != null)
            {
                titleFld.Text = valueModel.Title;
                titleFld.Enabled = valueModel.Selected;
            }

            var valueFld = e.Item.FindControl(ValueField) as TextBox;
            if (valueFld != null)
            {
                valueFld.Text = valueModel.Value;
                valueFld.Enabled = valueModel.Selected;
            }

            var unitFld = e.Item.FindControl(UnitField) as TextBox;
            if (unitFld != null)
            {
                unitFld.Text = valueModel.Unit;
                unitFld.Enabled = valueModel.Selected;
            }

        }
        protected void OnCheckRowChanged(object sender, EventArgs e)
        {
            var chBox = sender as CheckBox;
            if (chBox == null) return;
            var dgItem = chBox.NamingContainer as DataGridItem;
            if (dgItem == null) return;
            var documentValueId = "" + ValuesGrid.DataKeys[dgItem.ItemIndex];

            var document = GetCurrDocument();
            if (document == null) return;

            var newValues = GetValuesFromGrid(document.DocumentId, false);
            var currValueModel = newValues.Find(m => "" + m.DocumentValueId == documentValueId);
            if (currValueModel != null) currValueModel.Selected = chBox.Checked;
            document.DocumentValues = newValues;
            LoadValuesGrid(newValues);

        }
        private List<DocumentValueModel> GetValuesFromGrid(string documentId, bool withSelected)
        {
            var result = new List<DocumentValueModel>();
            foreach (DataGridItem item in ValuesGrid.Items)
            {
                var documentValueId = ValuesGrid.DataKeys[item.ItemIndex];
                var valueModel = new DocumentValueModel { DocumentId = documentId, DocumentValueId = Convert.ToInt32(documentValueId) };

                var titleFld = item.FindControl(TitleField) as TextBox;
                if (titleFld != null)
                {
                    valueModel.Title = titleFld.Text;
                }

                var valueFld = item.FindControl(ValueField) as TextBox;
                if (valueFld != null)
                {
                    valueModel.Value = valueFld.Text;
                }

                var unitFld = item.FindControl(UnitField) as TextBox;
                if (unitFld != null)
                {
                    valueModel.Unit = unitFld.Text;
                }
                if (withSelected)
                {
                    var chBox = item.FindControl(SelectCheckBox) as CheckBox;
                    if (chBox != null) valueModel.Selected = chBox.Checked;
                }
                result.Add(valueModel);
            }
            return result;
        }

        #endregion


        #region Add Line button
        protected void OnAddLineClick(object sender, EventArgs e)
        {
            var document = GetCurrDocument();
            if (document == null) return;
            var maxId = (document.DocumentValues.Count == 0 ? 0 : document.DocumentValues.Max(m => m.DocumentValueId));
            var addLine = new DocumentValueModel { DocumentId = document.DocumentId, DocumentValueId = maxId + 1, Selected = true };
            var currValues = GetValuesFromGrid(document.DocumentId, false);
            currValues.Add(addLine);
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }
        #endregion

        #region Line Command: MoveUp, MoveDn, Delete, Insert
        private int GetCommandLineKey(object sender)
        {
            var btn = sender as ImageButton;
            if (btn == null) return 0;
            var dgItem = btn.NamingContainer as DataGridItem;
            if (dgItem == null) return 0;
            return Convert.ToInt32(ValuesGrid.DataKeys[dgItem.ItemIndex]);

        }
        protected void OnMoveUpRowClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var prevLine = currValues.Find(m => m.DocumentValueId == (lineKey - 1));
            var nowLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (prevLine != null) prevLine.DocumentValueId = prevLine.DocumentValueId + 1;
            if (nowLine != null) nowLine.DocumentValueId = nowLine.DocumentValueId - 1;
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnMoveDownRowClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var nextLine = currValues.Find(m => m.DocumentValueId == (lineKey + 1));
            var nowLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (nextLine != null) nextLine.DocumentValueId = nextLine.DocumentValueId - 1;
            if (nowLine != null) nowLine.DocumentValueId = nowLine.DocumentValueId + 1;
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnDelClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var delLine = currValues.Find(m => m.DocumentValueId == lineKey);
            if (delLine == null) return;
            currValues.Remove(delLine);
            var order = 0;
            foreach (var line in currValues)
            {
                order++;
                line.DocumentValueId = order;
            }
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }

        protected void OnInsertClick(object sender, ImageClickEventArgs e)
        {
            var lineKey = GetCommandLineKey(sender);
            if (lineKey == 0) return;
            var document = GetCurrDocument();
            if (document == null) return;
            var currValues = GetValuesFromGrid(document.DocumentId, true);
            var reorderValues = currValues.FindAll(m => m.DocumentValueId > lineKey);
            foreach (var line in reorderValues)
            {
                line.DocumentValueId++;
            }
            var insertLine = new DocumentValueModel
            { DocumentId = document.DocumentId, DocumentValueId = lineKey + 1, Selected = true };
            currValues.Add(insertLine);
            document.DocumentValues = currValues;
            LoadValuesGrid(currValues);
        }
        #endregion

        protected void OnBarCodeOptionChanged(object sender, EventArgs e)
        {

            var isFixedOption = (BarCodeOptionList.SelectedIndex == 1);
            FixedTextFld.Enabled = isFixedOption;
            BarCodeTextValidator.Enabled = isFixedOption;
            if (!isFixedOption)
            {
                FixedTextFld.Text = "";
            }
        }

        #region Update Button
        #endregion

        protected void OnDownloadClick(object sender, EventArgs e)
        {
            var fileName = CorelFilesList.SelectedItem.Text.Trim();
            var dirSource = "" + Session[SessionConstants.GlobalRepDir] + @"CorelFiles\";
            if (!(dirSource.EndsWith("\\") || dirSource.EndsWith("/")))
                dirSource = dirSource + "/";
            //-- Exists dir
            try
            {
                var dir = new DirectoryInfo(Server.MapPath(dirSource));
                if (!dir.Exists) return;
            }
            catch (Exception ex)
            {

                return;
            }
            var webClient = new WebClient();
            webClient.DownloadFile("file://" + Server.MapPath(dirSource) + fileName, @"c:\temp\MyCorelFiles\" + fileName);

        }
        #region Save As dialog
        protected void OnSaveAsOkClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(NewReportName.Text.Trim()))
            {
                SaveAsPopupExtender.Show();
                return;
            }
            //-- dublicate
            var editData = GetInitDataFromView();
            var eqDocs = editData.Documents.FindAll(m => m.DocumentName == NewReportName.Text.Trim());
            if (eqDocs.Count > 0)
            {
                SaveAsPopupExtender.Show();
                SaveAsMessageLbl.Text = "Document with this name already exists!";
                return;
            }
            var saveDoc = GetDocumentDetails();
            saveDoc.DocumentName = NewReportName.Text.Trim();

            string docId;
            var msg =
            QueryCpUtilsNew.InsertDefineDocument(saveDoc, "" + editData.Cp.ItemTypeId, editData.Cp.CpOfficeId,
                                              out docId, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Save as", true);
                return;
            }
            //-- Refresh Documents List

            editData.Documents = QueryCpUtilsNew.GetDocumentsByItemTypeAndDocumentTypeCode(
                "" + editData.Cp.ItemTypeId, editData.DocumentType.DocumentTypeCode, this);
            /*IvanB start*/
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Documents, Page);
            //Remove items from a list of documents with a DocumentName equals value from block list + remove doubles
            editData.Documents = editData.Documents.Where(x => !blockList.Exists(y => x.DocumentName.Equals(y.BlockedDisplayName))).GroupBy(x => x.DocumentName).Select(x => x.FirstOrDefault()).ToList();
            /*IvanB end*/
            DocumentList.DataSource = editData.Documents;
            DocumentList.DataBind();

            if (editData.Documents.Find(m => m.DocumentId == docId) != null)
            {
                DocumentList.SelectedValue = docId;
            }
            if (DocumentList.SelectedItem != null)
            {
                OnDocumentChanged(null, null);
            }
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string cdrName = CorelFilesList.SelectedValue;
        }

        protected void CorelFilesList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void OnAllCDRFiles_Changed(object sender, EventArgs e)
        {
            string errMsg;
            var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg);
            var documents = GetInitDataFromView().Documents;
            foreach (var doc in documents)
            {
                if (doc.CorelFile != null && doc.CorelFile != "")
                    doc.CorelFile = (doc.CorelFile).ToLower();
                if (existsFiles.Find(m => m.Name == doc.CorelFile) == null)
                {
                    existsFiles.Add(new CorelFileModel { Name = doc.CorelFile, Exists = false });
                }
            }
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CorelFiles, Page);
            existsFiles = existsFiles.Where(x => !excluded.Exists(y => x.DisplayName.Equals(y.BlockedDisplayName))).ToList();
            if (AllCDRFiles.Checked)
            {
                List<CorelFileModel> dbSortedList = existsFiles.OrderBy(o => o.Name).ToList();
                CorelFilesList.DataSource = dbSortedList;
                CorelFilesList.DataBind();
            }
            else
            {
                var docTypeCode = Request.Params[ParamDocTypeCode];
                DataTable dt = QueryCpUtilsNew.GetCdrXlsFiles(docTypeCode, this);
                List<CorelFileModel> dbFiles = new List<CorelFileModel>();
                foreach (DataRow dr in dt.Rows)
                {
                    string fileName = dr["CorelFile"].ToString();
                    var dbFile = existsFiles.Find(x => x.DisplayName.ToLower() == fileName.ToLower());
                    if (dbFile != null)
                        dbFiles.Add(dbFile);
                }
                List<CorelFileModel> dbSortedList = dbFiles.OrderBy(o => o.Name).ToList();
                CorelFilesList.DataSource = dbSortedList;
                CorelFilesList.DataBind();
            }
            var document = GetCurrDocument();
            CorelFilesList.SelectedValue = document.CorelFile;
        }

        //protected void OnAllCDRFiles_Changed(object sender, EventArgs e)
        //{
        //    if (AllCDRFiles.Checked)
        //    {
        //        string errMsg;
        //        var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg);
        //        var documents = GetInitDataFromView().Documents;

        //        foreach (var doc in documents)
        //        {
        //            if (doc.CorelFile != null && doc.CorelFile != "")
        //                doc.CorelFile = (doc.CorelFile).ToLower();
        //            if (existsFiles.Find(m => m.Name == doc.CorelFile) == null)
        //            {
        //                existsFiles.Add(new CorelFileModel { Name = doc.CorelFile, Exists = false });
        //            }
        //        }
        //        /*IvanB start*/
        //        //create and fill new blocklist
        //        var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CorelFiles, Page);
        //        //Remove items from a list of CorelFiles with a DisplayName equals value from block list 
        //        existsFiles = existsFiles.Where(x => !excluded.Exists(y => x.DisplayName.Equals(y.BlockedDisplayName))).ToList();
        //        List<CorelFileModel> dbSortedList = existsFiles.OrderBy(o => o.Name).ToList();
        //        CorelFilesList.DataSource = dbSortedList;
        //        CorelFilesList.DataBind();
        //    }
        //    else
        //    {
        //        var docTypeCode = Request.Params[ParamDocTypeCode];
        //        string errMsg;
        //        var existsFiles = Utlities.GetCorelDrawFilesAzure(this, out errMsg);
        //        var documents = GetInitDataFromView().Documents;

        //        foreach (var doc in documents)
        //        {
        //            if (doc.CorelFile != null && doc.CorelFile != "")
        //                doc.CorelFile = (doc.CorelFile).ToLower();
        //            if (existsFiles.Find(m => m.Name == doc.CorelFile) == null)
        //            {
        //                existsFiles.Add(new CorelFileModel { Name = doc.CorelFile, Exists = false });
        //            }
        //        }
        //        /*IvanB start*/
        //        //create and fill new blocklist
        //        var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CorelFiles, Page);
        //        //Remove items from a list of CorelFiles with a DisplayName equals value from block list 
        //        existsFiles = existsFiles.Where(x => !excluded.Exists(y => x.DisplayName.Equals(y.BlockedDisplayName))).ToList();
        //        List<CorelFileModel> finalExistsFiles = new List<CorelFileModel>();
        //        DataTable dt = QueryCpUtilsNew.GetCdrXlsFiles(docTypeCode, this);
        //        List<CorelFileModel> dbFiles = new List<CorelFileModel>();
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            string fileName = dr["CorelFile"].ToString();
        //            var dbFile = existsFiles.Find(x => x.DisplayName.ToLower() == fileName.ToLower());
        //            if (dbFile != null)
        //                dbFiles.Add(dbFile);
        //        }
        //        List<CorelFileModel> dbSortedList = dbFiles.OrderBy(o => o.Name).ToList();
        //        CorelFilesList.DataSource = dbSortedList;
        //        CorelFilesList.DataBind();
        //    }
        //}
    }
}