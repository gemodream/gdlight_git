﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CustomerProgramNew.aspx.cs" Inherits="Corpt.CustomerProgramNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%-- IvanB start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<%-- IvanB end --%>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black;/* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel {
	        padding-bottom:2px;
	        color:#5377A9;
	        font-family:Arial, Sans-Serif;
	        font-weight:bold;
	        font-size:1.0em;
        }
        .auto-style1 {
            height: 60px;
        }
        .FixedHeader {
            position: absolute;
            font-weight: bold;
            width: 650px;
        } 
        /*IvanB start*/
        input.buttonStyleReport1 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/1.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonStyleReport2 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/2.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonStyleReport3 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/3.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonStyleReport4 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/4.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonStyleReport5 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/5.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonStyleReport6 {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/6.jpg');
	        width: 134px;
	        height: 124px;
	        border: 3px solid #ffffff
        }

        input.buttonHighlighted {
	        border: 3px solid red;
        }

        input.buttonStyleDeleteReport {
	        color: transparent;
	        background-image: url('Images/reportTypeImages/DeleteRed.png');
	        background-color: #ffffff;
	        width: 32px;
	        height: 32px;
	        margin-left: 10px;
	        border: none;
        }

	    input.buttonStyleDeleteReport:disabled,
	    input.buttonStyleDeleteReport[disabled] {
		    color: transparent;
		    background-image: url('Images/reportTypeImages/clear.png');
		    background-color: #ffffff;
		    width: 32px;
		    height: 32px;
		    margin-left: 10px;
		    border: none;
	    }
        /*IvanB end*/
        
         #master_contentplaceholder{
            width:max-content;
        }

        .auto-style2 {
            font-size: 12px;
            color: #000000;
            vertical-align: middle;
            height: 29px;
            background-color: #FFFFFF;
        }
        .auto-style3 {
            height: 29px;
        }

        /* List of company group*/
        .chkGroupCompanyList > tbody > tr > td
        {
            padding:0px;
        }
        .chkGroupCompanyList
        {
            margin-bottom:0px !important
        }
        .chkGroupCompanyList > tbody > tr > td > input[type="checkbox"]{
            margin-bottom:8px !important;
            margin-left: 10px;
        }
        .auto-style4 {
            width: 199px;
        }
    </style>
    <script type="text/javascript">
        $(window).resize(function () {
            showHeight();
        });
        $(document).ready(function () {
            showHeight();
        });

        /*IvanB start*/

        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {
            $("#<%= CustomerList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });
            $("#<%= CpList.ClientID %>").select2({});
            $("#<%= VendorList.ClientID %>").select2({
                placeholder: {
                    id: '', 
                    text: ''
                }
            });
            $("#<%= SaCustomerList.ClientID %>").select2({
                placeholder: {
                    id: '', 
                    text: ' '
                },
                dropdownParent: $("#<%= SaveAsPanel.ClientID %>")
            });
            $("#<%= SaVendorList.ClientID %>").select2({
                placeholder: {
                    id: '', 
                    text: ' '
                },
                dropdownParent: $("#<%= SaveAsPanel.ClientID %>")
            });
            $(".filtered-select").select2({
                scrollAfterSelect: true,
            });
        }
        /*IvanB start*/
        //Getting ToolkitScriptManager instance
        var yPos;
        var senderControl = null;
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        //Handler to get Y position in Rules table when request begin
        function BeginRequestHandler(sender, args) {
            var rules = $('#<%=Rules.ClientID%>');
        if (rules.length > 0) {
            // Get Y positions of scrollbar before the partial postback
            if (rules.find(args._postBackElement).length > 0) {
                senderControl = args._postBackElement;
                yPos = rules.scrollTop();
            }
        }
    }
    //Handler to set Y
    function EndRequestHandler(sender, args) {

        if (senderControl == null) {
            return;
        }

            var rules = $('#<%=Rules.ClientID%>');
            var cntrIdToFocus = senderControl.id;
            var isCntrInsideRules = rules.find('#' + cntrIdToFocus).length > 0

            //check if rules table exists and control we need to scroll to is inside that table
            if (rules.length > 0 && isCntrInsideRules) {
                var targetControl;
                if (__nonMSDOMBrowser) {
                    targetControl = document.getElementById(cntrIdToFocus);
                }
                else {
                    targetControl = document.all[cntrIdToFocus];
                }
                var focused = targetControl;
                if (targetControl && (!WebForm_CanFocus(targetControl))) {
                    focused = WebForm_FindFirstFocusableChild(targetControl);
                }
                if (focused) {
                    try {
                        focused.focus();
                        if (__nonMSDOMBrowser) {
                            // Set Y positions back to the scrollbar
                            // after partial postback
                            rules.scrollTop(yPos);
                        }
                        if (window.__smartNav) {
                            window.__smartNav.ae = focused.id;
                        }
                    }
                    catch (e) {
                    }
                }

                //remove _controlIDToFocus so ToolkitScriptManager does not do wrong scroll
                Sys.WebForms.PageRequestManager.getInstance()._controlIDToFocus = null;
                senderControl = null;
            }
        }

        //adding handlers to begin and end request event
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
    /*IvanB end*/
        function ShowHideIframe() {
            var iframeVis = document.getElementById("SampleIframe");
            var shBtn = $("#<%= ShowHideIframeButton.ClientID %>");
            if (iframeVis.getAttribute("Style")!='') {
                shBtn.text("Close Sample");
                iframeVis.setAttribute("Style", "");
                iframeVis.setAttribute("src", "https://wg.gemscience.net/VRNewJS");
             }
            else {
                shBtn.text("Sample");
                iframeVis.setAttribute("Style", "display:none;");
                iframeVis.setAttribute("src", "");
            }
        }
        /*IvanB end*/

        function showHeight() {

            var h = parseInt(window.innerHeight) - 230;
            var div1 = document.getElementById('<%= ItemTypeGrpDiv.ClientID %>');
            div1.style.height = h + 'px';

            var div2 = document.getElementById('<%= ItemTypeDiv.ClientID %>');
            div2.style.height = h + 'px';

            var fld = document.getElementById('<%= ForHeightFld.ClientID %>');
            fld.value = h;
        };
    </script>
    
    <div class="demoarea">
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

        <div class="demoheading">Customer Program</div>
        <asp:UpdatePanel runat="server" ID="SearchPanel" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="VendorAsCustomer" 
                    EventName="CheckedChanged" />
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="UploadSkuBtn"></asp:PostBackTrigger>
            </Triggers>
            <ContentTemplate>
                
                <asp:Panel runat="server" DefaultButton="NoActionButton" ID="FindPanel">                    
                    <asp:HiddenField runat="server" ID="ForHeightFld" ></asp:HiddenField>
                    <table>
                        <tr>
                            <td>
                                Customer:
                            </td>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="CustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged1"
                                    ToolTip="Customers List" Width="300px" Height="26px" />
                                <%--<asp:DropDownList ID="CustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSearchClick"
                                    ToolTip="Customers List" Width="300px" Height="26px" />--%>
                            </td>
                            <td style="padding-top: 5px">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="CustomerLikeButton">
                                    <asp:TextBox runat="server" ID="CustomerLike" Width="100px" Height="18px"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" Style="display: none" />
                                </asp:Panel>
                            </td>
                            <td style="padding-left: 10px">
                                Vendor:
                            </td>
                            <td style="padding-top: -5px; padding-right: 5px">
                                <asp:DropDownList ID="VendorList" runat="server" AutoPostBack="True" CssClass=""
                                    DataTextField="CustomerName" DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnVendorSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Enabled="False" />
                            </td>
                            <td style="padding-top: 5px">
                                <asp:Panel ID="Panel4" runat="server" DefaultButton="VendorLikeButton">
                                    <asp:TextBox ID="VendorLike" runat="server" Width="100px" Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="VendorLikeButton" runat="server" ImageUrl="~/Images/ajaxImages/search.png"
                                        OnClick="OnVendorSearchClick" ToolTip="Filtering the list of customers" Style="display: none" />
                                </asp:Panel>
                            </td>
                            <td style="vertical-align: middle">
                                <asp:CheckBox ID="VendorAsCustomer" runat="server" Checked="True" Text="the same as Customer"
                                    CssClass="checkbox" Width="250px" OnCheckedChanged="OnVendorAsCustomerChanging"
                                    AutoPostBack="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style3">
                                Programs:
                            </td>
                            <td class="auto-style2">
                                <asp:DropDownList ID="CpList" runat="server" AutoPostBack="True" CssClass="" DataTextField="CustomerProgramName"
                                    DataValueField="CpId" OnSelectedIndexChanged="OnCpSelectedChanged" ToolTip="Customer Program List"
                                    Width="300px" />
                            </td>
                            <td class="auto-style3">
                                <asp:Button runat="server" ID="NewSkuBtn" Text="New SKU" OnClick="OnNewSkuClick"
                                    Enabled="False" CssClass="btn btn-small btn-info" />
                                <asp:Button runat="server" ID="HiddenNewSkuBtn" Style="display: none"/>
                            </td>
                            <td style="padding-left: 10px" class="auto-style3">Batch:</td>
                            <td colspan="2" class="auto-style3">
                                <asp:Panel runat="server" ID="BatchPanel" DefaultButton="SearchCpByBatchBtn" CssClass="form-inline">
                                    <asp:TextBox runat="server" ID="BatchNumberFld" MaxLength="9" Width="70px"
										ontextchanged="BatchNumberFld_TextChanged" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                                        ValidChars="0123456789-" TargetControlID="BatchNumberFld" />
                                    <asp:Button runat="server" ID="SearchCpByBatchBtn" OnClick="OnSearchCpByBatchClick"
                                        Style="display: none" />
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="ErrCP" ForeColor="Red" Width="200px" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <asp:Panel runat="server" ID="CpPanel" BorderStyle="Solid" BorderColor="Silver" BorderWidth="1px"
                    DefaultButton="NoActionButton" Style="margin: 5px; padding: 6px" Visible="False">
                    <asp:Button runat="server" ID="NoActionButton" Style="display: none" />
                    <table>
                        <tr>
                            <td style="vertical-align: top">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label4" runat="server" Text="Program Name"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label1" runat="server" Text="Style"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label2" runat="server" Text="Cust. ID"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label3" runat="server" Text="SRP"></asp:Label>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:Label ID="Label5" runat="server" Text="Path to picture"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="ProgramNameFld" OnTextChanged="CPNameClick"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="CpStyle"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="CustId" Width="100px"></asp:TextBox>
                                            <asp:HiddenField ID="hdnCustId" runat="server" />
                                            <asp:HiddenField ID="hdnItemTypeID" runat="server" />
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:TextBox runat="server" ID="Srp"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="SrpExtender" runat="server"
                                                ValidChars="0123456789." TargetControlID="Srp" Enabled="True" />
                                        </td>
                                        <td style="vertical-align: top; padding-left: 5px;">
                                            <asp:Panel runat="server" ID="PathToPicturePanel" CssClass="form-inline" DefaultButton="ApplyPath2PictureBtn">
                                                <asp:TextBox runat="server" ID="Path2PictureFld" Width="200px"></asp:TextBox>
                                                <asp:ImageButton runat="server" ID="ApplyPath2PictureBtn" OnClick="OnApplyPath2PictureClick"
                                                    ImageUrl="~/Images/ajaxImages/apply.png" ToolTip="Apply new Path to Picture" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="vertical-align: top; padding-left: 5px">
                                            <asp:Panel ID="Panel6" runat="server" CssClass="form-inline">
                                                <asp:Image ID="itGroupIcon" runat="server" ToolTip="Item Type Group" Width="16px" />
                                                <asp:Label runat="server" ID="itGroupName"></asp:Label>
                                                <asp:Image ID="itIcon" runat="server" ToolTip="Item Type" Width="16px" />
                                                <asp:Label runat="server" ID="itName"></asp:Label>
                                                <asp:TextBox runat="server" ID="itId" Style="display: none"></asp:TextBox>
                                                <asp:Button runat="server" ID="ClearAll" OnClick="OnClearAllClick" CssClass="btn btn-small btn-info"
                                                Style="margin-left: 70px" Text="Clear All" />
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top;text-align: center">
                                            <asp:Button runat="server" ID="SaveButton" OnClick="OnSaveClick" CssClass="btn btn-small btn-info"
                                               Enabled="False" Text="Save" ToolTip="Save Customer Program"/>
                                            <asp:Button runat="server" ID="SaveAsButton" OnClick="OnSaveAsClick" CssClass="btn btn-small btn-info"
                                                Enabled="False" Text="Save as" style="height: 22px" />
                                            <asp:Button runat="server" ID="btnSaveInCompanyGroup" OnClick="btnSaveInCompanyGroup_OnClick" CssClass="btn btn-small btn-info"
                                                Enabled="False" Text="Save in Company Group" />
                                            
                                            <asp:Button runat="server" ID="SaveAsHiddenBtn" Style="display: none"
                                                Enabled="False" Text="Save as" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="vertical-align: top">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; padding-left: 5px; text-align: left;width: 100px">
                                            <asp:Label runat="server" ID="CpPathToPicture"></asp:Label><br />
                                            <asp:Image ID="itemPicture" runat="server" Width="90px" Visible="False"></asp:Image><br />
                                            <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Width="90px"></asp:Label>
                                        </td>
                                        <%-- <td style="vertical-align: top;font-size: smaller;padding-left: 10px;font-size:medium">
                                            <div style="overflow-y: auto; overflow-x: hidden; height: 120px; border: silver solid 1px">
                                                <asp:TreeView ID="CpReportsTree" runat="server" Width="200px">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                                </asp:TreeView>
                                            </div>
                                        </td> --%>
                                        <%--<td  style="vertical-align: top;font-size: smaller;padding-left: 10px;font-size:medium">
                                            <div style="overflow-y: auto; overflow-x: hidden; height: 120px; border: silver solid 1px">
                                                <asp:ListBox runat="server" ID="CPTypesList" Rows="10" Width="250px" DataTextField="ItemTypeName"
                                                    DataValueField="ItemTypeName" OnSelectedIndexChanged="OnCPTypesSelection" AutoPostBack="True"/>
                                            </div>
                                        </td>--%>
                                        <td style="vertical-align: top; padding-left: 5px">
                                            <asp:ImageButton runat="server" ID="RefreshCpDocumentsTree" OnClick="OnRefreshCpDocumentsClick"
                                                ImageUrl="~/Images/ajaxImages/refresh24.png" ToolTip="Refresh Cp Reports" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="vertical-align: top">
                                              <asp:DropDownList ID="SKUTypesList" runat="server" AutoPostBack="True" CssClass="" DataTextField="ItemTypeName"
                                    DataValueField="ItemTypeID" OnSelectedIndexChanged="OnSKUTypesListSelectedChanged" ToolTip="Customer Program List"
                                    Width="300px" />
                            </td>
                            <td>
                                  <asp:TreeView ID="SKUTypesTreeView" runat="server">
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                  </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False"
                    ActiveTabIndex="2" Height="100%" Width="100%" ScrollBars="Auto" Visible="False">
                    <ajaxToolkit:TabPanel runat="server" HeaderText="Description" OnDemandMode="Once" ID="TabDescription" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="DescriptionPanel">
                                <asp:Panel runat="server" ID="PanelDescr1" ScrollBars="Auto" CssClass="collapsePanel">
                                    <table style="font-family: sans-serif">
                                        <tr>
                                            <td style="vertical-align: top; border-top: dotted silver 1px;font-family: sans-serif">
                                                <div>
                                                <asp:Label ID="Label8" runat="server" Text="Parts" CssClass="label"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TreeView ID="DescriptionPartsTreeView" runat="server" OnSelectedNodeChanged="OnDescripPartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                                </asp:TreeView>
                                                </div>
                                            </td>
                                            <td  style="vertical-align: top; padding-left: 20px;">
                                                <div>
                                                    <asp:Label ID="Label9" runat="server" Text="Characteristics" CssClass="label"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:ListBox runat="server" ID="DescripMeasuresField" DataTextField="MeasureTitle" DataValueField="MeasureId"
                                                        Rows="22" />
                                                </div>
												<div>
                                                    <asp:Label ID="FractionRangesLbl" runat="server" Text="Fractioin Ranges" CssClass="label"></asp:Label>
                                                    <asp:CheckBox ID="KeepRangeBox" runat="server" Text="Keep Range"/>
                                                </div>
                                                <div>
                                                    <asp:Button runat="server" ID="FractionRangesBtn" OnClick="OnFractionGroupClick" CssClass="btn btn-small btn-info" Text="Fraction Ranges" Width="90px" ToolTip="Fraction Ranges"/>
                                                    <asp:Button runat="server" ID="CopyFractionToCommentsBtn" OnClick="OnCopyRangeToDescriptionClick" CssClass="btn btn-small btn-info" Text="To Comments" Width="90px" ToolTip="Copy To Comments"/>
                                                    <asp:Button runat="server" ID="CopyFractionToDescriptionBtn" OnClick="OnCopyRangeToCommentsClick" CssClass="btn btn-small btn-info" Text="To Description" Width="90px" ToolTip="Copy To Description"/>
                                                    <asp:CheckBox ID="UpperCaseBox" runat="server" Text="Up" />
                                                 </div>
                                                <div>
                                                    <asp:DropDownList ID="FractionRangesList" runat="server" AutoPostBack="True" DataTextField="GroupName"
                                                        DataValueField="GroupId" OnSelectedIndexChanged="OnRangeGroupSelectedChanged" ToolTip="Range Group List"
                                                        Width="230px" />
                                                    <asp:DropDownList id="UnitList" runat="server" Width="80px" AutoPostBack="True" Visible="False">
	                                                    <asp:ListItem Selected="True" Value="ct(s)"> CT(S)</asp:ListItem>
                                                        <asp:ListItem Value="ct.t.w."> CT.T.W</asp:ListItem>
                                                        <asp:ListItem Value="ct(s) twt"> CT(S) TWT</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="FractionRangeFld" Width="300px"></asp:TextBox>
                                                    <asp:HiddenField ID="FullRangeFld" runat="server" />

                                                </div>
                                            </td>
                                            <td style="vertical-align: top; padding-left: 5px;">
                                                <div style="padding-top: 80px;">
                                                <asp:ImageButton ID="CopyT0CommentBtn" runat="server" ToolTip="Copy Characteristic to Description"
                                                    ImageUrl="~/Images/ajaxImages/rightArrowBl.png" OnClick="OnCopyCharToCommentClick" />
                                                </div>
                                                <div style="padding-top: 165px;" >
                                                <asp:ImageButton ID="CopyToDescripBtn" runat="server" ToolTip="Copy Characteristic to Comments"
                                                    ImageUrl="~/Images/ajaxImages/rightArrowBl.png" OnClick="OnCopyCharToDescripClick" />
                                                </div>
                                            </td>
                                            <td style="vertical-align: top; padding-left: 20px;font-size: medium">
                                                <div>
                                                    <asp:Label runat="server" Text="Description" CssClass="label"></asp:Label>
                                                </div>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="CpComments" TextMode="MultiLine" Width="550px" Height="150px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="SaveDescBtn" runat="server" CssClass="btn btn-small btn-info" Text="Save Description" Width="160px"                                         OnClick="SaveCommentsClick" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="DescNameBox" runat="server" Width="160px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="SavedDescList" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"                                            OnSelectedIndexChanged="OnCommentsNameSelectedChanged" ToolTip="Range Group List" Width="160px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="DeleteSavedDescBtn" runat="server"  CssClass="btn btn-small btn-info" Text="Delete Saved Descriptions"                                      Width="160px" OnClick="DeleteDescClick" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="padding-top:10px;">
                                                    <asp:Label ID="Label7" runat="server" Text="Comments" CssClass="label"></asp:Label>
                                                </div>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="CpDescrip" Width="550px" Height="150px" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="SaveCommentsBtn" runat="server" Text="Save Comments" Width="160px" CssClass="btn btn-small btn-info"                                        OnClick="SaveCommentsClick" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Textbox ID="CommentsNameBox" runat="server" Width="160px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DropDownList ID="SavedCommentsList" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"                                        OnSelectedIndexChanged="OnCommentsNameSelectedChanged" ToolTip="Range Group List" Width="160px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="DeleteSavedCommentsBtn" runat="server" CssClass="btn btn-small btn-info" Text="Delete Saved Comments"                                       Width="160px" OnClick="DeleteCommentsClick" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="ErrSaveDescComments" runat="server" TextMode="MultiLine" Columns="1" style="width: 160px; height: 60px;                                    float: left;" ForeColor="Red" Width="160px" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td style="padding-left: 20px;">
                                                <asp:Panel ID="NewPicturePanel" runat="server" style="text-align:center;">
                                                    <asp:Label runat="server" ID="NewPicture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                                                        <asp:ImageButton ID="NewPicture2PathButton" runat="server" Width="250px" BorderWidth="1px" ToolTip="New Report Picture button" OnClick="OnNewPictureClick" Visible="False" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        
</ContentTemplate>
                    


</ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Operations" OnDemandMode="Once" ID="TabOperations" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="OperationsPanel">
                                <asp:Panel runat="server" ID="PanelOper1" CssClass="collapsePanel">
                                    <asp:TreeView ID="TreeOpers" runat="server" ShowCheckBoxes="All" ExpandDepth="1"
                                        AfterClientCheck="CheckChildNodes();" OnTreeNodeCheckChanged="TreeOperationCheckChanged"
                                        OnSelectedNodeChanged="OnTreeOperationSelectedChanged" 
                                        onclick="OnTreeClick(event)">
                                        <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                        <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                        <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                    </asp:TreeView>
                                </asp:Panel>
                            </asp:Panel>
                        
</ContentTemplate>
                    


</ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Requirements" OnDemandMode="Once" ID="TabRequirements" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="RequirementsPanel">
                                <asp:Panel ID="PanelRequir1" runat="server">
                                    <table>
                                        <tr>
                                            <td style="vertical-align: top; padding-left: 20px" class="auto-style1">
                                                <asp:Panel ID="Panel2" runat="server" CssClass="form-inline" Height="58px">
                                                    <asp:Button runat="server" ID="AddDocumentBtn" Text="New Document" CssClass="btn btn-info btn-small" Enabled ="False"
                                                        OnClick="OnAddDocumentClick" />



                                                    <asp:Button runat="server" ID="DelDocumentBtn" Text="Delete Document" CssClass="btn btn-info btn-small" Enabled ="False"
                                                        OnClick="OnDelDocumentClick" />



                                                    <asp:HiddenField runat="server" ID="CpDocIdHidden" />



                                                </asp:Panel>



                                            </td>
                                            <td colspan="2" style="vertical-align: top;" class="auto-style1">
                                                <asp:RadioButtonList ID="CpDocsList" runat="server" RepeatDirection="Horizontal"
                                                    DataTextField="Title" DataValueField="CpDocId" BorderStyle="None" Style="margin-left: 5px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="OnCpDocsListChanged" CellPadding="1"
                                                    CellSpacing="1" RepeatColumns="6" Font-Size="X-Small"></asp:RadioButtonList>



                                            </td>
                                        </tr>
                                        <tr style="padding: 3px;">
                                            
                                            <td style="vertical-align: top; padding-left: 20px;width: 380px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                <asp:Button runat="server" ID="MDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="MDX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="FDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="FDX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="SDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="SDX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="GradingReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="Grading" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                        </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td>
                                                <asp:Button runat="server" ID="IDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="IDX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="WTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="WhiteTag" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="PCReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="Price Card" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="CNSLTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="CNSLT Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                        </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td>
                                                <asp:Button runat="server" ID="HDSReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="HDS_Back" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="GEMTAGReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="GEM Tag" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="GOLDTAGReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Font-Size="XX-Small" Enabled="False" Text="GoldTag Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="GMXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="GMX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                       </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td>
                                                <asp:Button runat="server" ID="CDXReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="CDX Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="LBLReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="LBL Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="TXTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="TXT Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                <asp:Button runat="server" ID="SRTReportButton" OnClick="LoadReportByTypeClick" CssClass="btn btn-small btn-info" Enabled="False" Text="SRT Report" Width="90px" Height="60px" OnClientClick="target ='_blank';" />



                                                            </td>
                                                        </tr>
                                                    </table>
                                                <table>
                                                    <tr>
                                                        <td colspan="2" style="padding-top: 0px">
                                                            <asp:Label runat="server" ID="Label6" Text="Internal Comments" CssClass="label"></asp:Label>



                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <asp:TextBox runat="server" ID="CommentsList" TextMode="MultiLine" Columns="1" style="width: 130px; height: 66px; float: left;"></asp:TextBox>



                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <!--
                                                <asp:DataGrid runat="server" ID="MeasureGroupGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                    DataKeyField="UniqueKey" OnItemDataBound="OnMeasureGroupDataBound">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="MeasureGroupId" HeaderText="MeasureGroupId" Visible="False" />
                                                        <asp:BoundColumn DataField="MeasureGroupName" HeaderText="Property Name"/>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>Rechecks</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Panel ID="Panel3" runat="server" DefaultButton="ApplyRechecksButton" >
                                                                    <asp:ImageButton runat="server" ID="ApplyRechecksButton" Style="display: none" OnClick="OnDocRechecksEnter"
                                                                        ImageUrl="" />
                                                                    <asp:TextBox runat="server" ID="RechecksField" ></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
                                                                        TargetControlID="RechecksField" FilterType="Numbers" />
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                </asp:DataGrid>
                                                                                                
                                                <asp:TextBox runat="server" ID="DocDescripField" TextMode="MultiLine" Width="250px"
                                                    ToolTip="Document description" Height="100px"></asp:TextBox><br/>

                                                <asp:CheckBox ID="IsReturnDoc" runat="server" Text="Return Item to Customer"
                                                    CssClass="checkbox" Width="200px" OnCheckedChanged="OnVendorAsCustomerChanging"
                                                    AutoPostBack="True" />
                                                    -->
                                            </td>
                                            <td style="vertical-align: top; padding-left: 10px;font-family: sans-serif">
                                                <table>
                                                    <tr >
                                                        <td colspan="2" style="padding-top: 0px">
                                                            <asp:Label runat="server" ID="Label16" Text="Attached Reports" CssClass="label"></asp:Label>



                                                        </td>
                                                        <td style="text-align: right;padding-top: 0px">
                                                            <asp:CheckBox runat="server" ID="ReplaceRptBox" Checked="True" Text="Replace"></asp:CheckBox>
                                                            <asp:ImageButton runat="server" ID="AddPrintDocToDocBtn" OnClick="AddPrintDocToDocClick"
                                                                ImageUrl="~/Images/ajaxImages/attachEditNew.png" ToolTip="Attach Print Doc" />

                                                            <asp:ImageButton runat="server" ID="DelDocPrintDocBtn" OnClick="OnDelDocPrintDocClick"
                                                                ImageUrl="~/Images/ajaxImages/deleteNew.png" ToolTip="Detach selected Print Doc" />

                                                            <a href="DefineDocumentNew.aspx" target="_blank" id="ViewLink" runat="server" >View</a>



                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" >
                                                            <asp:ListBox runat="server" ID="DocAttachedReportsList" Rows="5" Width="250px" DataTextField="OperationTypeName"
                                                                DataValueField="Key" OnSelectedIndexChanged="OnDocAttachPrintDocSelection" AutoPostBack="True"/>



                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <div>
                                                <asp:TreeView ID="DocPartTree" runat="server" ShowCheckBoxes="All" OnSelectedNodeChanged="OnDocPartsTreeChanged">
<SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
</asp:TreeView>



                                                </div>
                                                <div>
                                                    <asp:Panel ID="NewReportPicturePanel" runat="server" style="text-align:center;">
                                                    <asp:Label runat="server" ID="NewReportPictureField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>



                                                        <asp:ImageButton ID="NewReportPictureButton" runat="server" Width="250px" BorderWidth="1px" ToolTip="New Report Picture button" OnClick="OnNewPictureClick" />



                                                </asp:Panel>



                                                </div>
                                                <div>
                                                    <asp:Panel ID="NewreportPictureBatchPanel" runat="server" style="text-align:center;">
                                                        <asp:Label runat="server" ID="NewReportPictureBatchField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>



                                                        <asp:ImageButton ID="NewReportPictureBatchButton" runat="server" Width="250px" BorderWidth="1px" ToolTip="New Report Picture button" OnClick="OnNewPictureClick" />



                                                    </asp:Panel>



                                                </div>
                                            </td>
                                            <td style="vertical-align: top; padding-left: 10px;font-family: sans-serif">
                                                <table>
                                                    <tr>
                                            <td style="vertical-align: top; padding-left: 20px; width: 700px; height: 300px;" rowspan="2">
                                                <div>
                                                    <asp:Label runat="server" ID="PartName" CssClass="label" style="white-space: normal"></asp:Label>



                                                    <asp:Button runat="server" ID="BulkCopyButton" Text="Bulk Copy" OnClick="OnBulkCopyBtnClick" CssClass="btn btn-small btn-info"
                                                Style="margin-left: 70px" Width="100px"/>



                                                </div>
                                                <asp:Label runat="server" ID="RuleCountLabel" CssClass="label" ></asp:Label>



                                                
                                                <div id="HdrLbl" runat="server" style="border:double;font-weight:bold;">
        <table rules="all">
            <tr>
                <td style ="width:50px; border:hidden;"></td>
                <td style ="width:100px;text-align: center; border:hidden;">Measure</td>
                <td style ="width:230px;text-align: center; border:hidden;">Min Value</td>
                <td style ="width:220px;text-align: center; border:hidden;">Max Value</td>
                <td style ="width:30px;text-align: center; border:hidden;">Def</td>
                <td style ="width:40px;text-align: center; border:hidden;">NV</td>
                <td style ="width:40px;text-align: center; border:hidden;">Info</td>
            </tr>
        </table>
        </div>




                                                <div id="Rules" runat="server" style="width: 100%; height: 400px; overflow-y: scroll">
                                                
                                                <asp:DataGrid runat="server" ID="RulesGrid" AutoGenerateColumns="False" CssClass="table table-condensed" ShowHeader = "False"
                                                    DataKeyField="UniqueKey" OnItemDataBound="OnRuleItemDataBound" OnItemCommand="OnItemCommandClick" AutoPostBack="true" ><Columns>
<asp:TemplateColumn><ItemTemplate>
                                                                <asp:LinkButton ID="RuleResetBtn" runat="server" Text="Reset" OnClick="OnRuleResetBtnClick" style="width:44px;"></asp:LinkButton>
                                                            
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"></asp:BoundColumn>
<asp:BoundColumn DataField="MeasureName" HeaderText="Measure">
<ItemStyle Width="88px" />
</asp:BoundColumn>
<asp:BoundColumn DataField="MeasureClass" HeaderText="Measure Class" Visible="False"></asp:BoundColumn>
<asp:TemplateColumn><HeaderTemplate>
Min Value
</HeaderTemplate>
<ItemTemplate >
                                                                <asp:Panel runat="server" ID="RuleMinValuePanel" CssClass="form-inline">
                                                                    <asp:TextBox runat="server" ID="RuleMinNumericFld" Width="135px"></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMinExtender" runat="server" FilterType="Custom"
                                                                        ValidChars="0123456789." TargetControlID="RuleMinNumericFld" />
                                                                    <asp:DropDownList runat="server" ID="RuleMinEnumFld" DataTextField="MeasureValueName" OnSelectedIndexChanged="SelectedIndexChangedMin" AutoPostBack="true"
                                                                        DataValueField="MeasureValueId" Width="150px" />
                                                                </asp:Panel>
                                                            
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn><HeaderTemplate>
Max Value
</HeaderTemplate>
<ItemTemplate>
                                                                <asp:Panel runat="server" ID="RuleMaxValuePanel" CssClass="form-inline">
                                                                    <asp:TextBox runat="server" ID="RuleMaxNumericFld" Width="135px" ></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMaxExtender" runat="server" FilterType="Custom"
                                                                        ValidChars="0123456789." TargetControlID="RuleMaxNumericFld" />
                                                                    <asp:DropDownList runat="server" ID="RuleMaxEnumFld" DataTextField="MeasureValueName" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true"
                                                                        DataValueField="MeasureValueId" Width="150px"/>
                                                                    
                                                                </asp:Panel>
                                                            
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn><HeaderTemplate>
                                                                Is Default
                                                            
</HeaderTemplate>
<ItemTemplate>
                                                                <div style="text-align: center; width: 30px;">
                                                                    <asp:CheckBox runat="server" Text='' ID="CheckBoxIsDefault" Checked="false" OnCheckedChanged="OnCheckedBoxIsDefaultChanged" AutoPostBack="true"></asp:CheckBox>
                                                                </div>
                                                            
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn><HeaderTemplate>
                                                                Not Visible
                                                            
</HeaderTemplate>
<ItemTemplate>
                                                                <div style="text-align: center; width: 30px;">
                                                                    <asp:CheckBox runat="server" Text='' ID="CheckBoxNotVisibleInCcm" />
                                                                </div>
                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" />
</asp:TemplateColumn>
<asp:TemplateColumn><HeaderTemplate>
                                                                Info Only
                                                            
</HeaderTemplate>
<ItemTemplate>
                                                                <div style="text-align: center; width: 30px;">
                                                                    <asp:CheckBox runat="server" Text='' ID="CheckBoxInfoOnly" Checked="false" OnCheckedChanged="OnCheckedBoxInfoChanged" AutoPostBack="true"/>
                                                                </div>
                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" />
</asp:TemplateColumn>
</Columns>

<HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />

<ItemStyle Font-Names="Cambria" Font-Size="Small" />
</asp:DataGrid>



                                                    </div>



                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                        </tr>
                                    </table>
                                </asp:Panel>



                            </asp:Panel>



                        
</ContentTemplate>
                    


</ajaxToolkit:TabPanel>    

                    <ajaxToolkit:TabPanel runat="server" HeaderText="Pricing" OnDemandMode="Once" ID="TabPricing" >
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="PricingPanel" DefaultButton="PriceDefaultBtn">
                                <asp:Button runat="server" ID="PriceDefaultBtn" Style="display: none"/>
                                <asp:Panel runat="server" ID="PanelPrice1">
                                    <table style="font-family: sans-serif">
                                        <tr style="font-weight: bold;">
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                <asp:Label ID="Label11" runat="server" Text="Item Structure Tree" CssClass="label"></asp:Label>
                                                
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px">
                                                <asp:Label ID="Label12" runat="server" Text="Item Type Props" CssClass="label"></asp:Label>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px;">
                                                <asp:Label runat="server" Text="Pass" CssClass="label"></asp:Label>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            
                                           <td style="width: 10px">
                                            </td>
                                            				 
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; padding-top: 5px;font-family: sans-serif">
                                                <asp:TreeView ID="PricePartTree" runat="server" OnSelectedNodeChanged="OnPricePartsTreeChanged">
                                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                                </asp:TreeView>
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px;text-align:center;">
                                                <div>
                                                    <asp:ListBox runat="server" ID="PartMeasureList" DataTextField="MeasureName" DataValueField="MeasureId"
                                                    Rows="10" style="width: 200px"/>
                                                </div>
                                                <div style="padding-top: 100px;text-align:center;">
                                                    <asp:Button runat="server" ID="SavePriceRangeBtn" OnClick="SavePriceRangeNameChanged" CssClass="btn btn-small btn-info" Text="Save Price Range" Width="120px" Height="60px" />
                                                </div>
                                                <div style="padding-top: 20px">
                                                    <asp:TextBox runat="server" ID="SavePriceRangeFld" Width="180px" ontextchanged="SavePriceRangeNameChanged"  />
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="SavePriceRangeError" Width="200px" Visible="False" />
                                                </div>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px; border: dotted silver 1px">
                                                <table>
                                                    <tr>
                                                        <td rowspan="2">
                                                            <asp:RadioButtonList runat="server" ID="IsFixedFld" Width="100px" OnSelectedIndexChanged="OnPricingModeChanges" AutoPostBack="True">
                                                                <asp:ListItem Value="1" Text="Fixed" />
                                                                <asp:ListItem Value="0" Text="Dynamic" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            Price
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:TextBox runat="server" ID="PassFixedPrice" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender1" runat="server"
                                                                ValidChars="0123456789." TargetControlID="PassFixedPrice" Enabled="True" />
                                                        </td>
                                                        <td style="vertical-align: top; padding-left: 10px; text-align: left;margin-right: 10px">
                                                            Discount (%)
                                                            <asp:TextBox runat="server" ID="PassDiscount" Width="60px" ></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender2" runat="server"
                                                                ValidChars="0123456789." TargetControlID="PassDiscount" Enabled="True" />

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-right: 10px">
                                                            Delta Fix
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="DeltaFix" Width="90px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender3" runat="server"
                                                                ValidChars="0123456789." TargetControlID="DeltaFix" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="vertical-align: top">
                                                            <asp:ListBox runat="server" ID="PricePartMeasureList" DataTextField="PartNameMeasureName"
                                                                DataValueField="MeasureCode" Width="280px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label14" runat="server" Text="Price Ranges" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td style="text-align: right;padding-left: 10px">
                                                            <asp:Button runat="server" ID="AddPriceRangeBtn" Text="Add" OnClick="OnAddPriceRangeBtnClick"
                                                                CssClass="btn btn-small btn-default" />
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding-top: 6px;width: 220px;" align="center">
                                                            <asp:DataGrid runat="server" ID="PriceRangeGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                                OnItemDataBound="OnPriceRangeDataBound" OnItemCommand="OnRemovePriceRangeCommand" 
                                                                DataKeyField="Rownum">
                                                                <Columns>
                                                                    <asp:ButtonColumn Text="Del"/>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            From</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="FromField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FromExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="FromField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            To</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="ToField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ToExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="ToField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Price</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="PriceField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="PriceExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="PriceField" Enabled="True" />

                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                            </asp:DataGrid>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="vertical-align: top; padding-top: 5px; padding-left: 20px; margin-left: 5px;border: dotted silver 1px;">
                                                <table>
                                                    <tr>
                                                        <td style="vertical-align: top; padding-top: 5px;">
                                                            <asp:Label ID="Label10" runat="server" Text="Fail" CssClass="label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Fixed Price
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="FailFixedFld" Width="60px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender4" runat="server"
                                                                ValidChars="0123456789." TargetControlID="FailFixedFld" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Discount (%)
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="FailDiscountFld" Width="60px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="Extender5" runat="server"
                                                                ValidChars="0123456789." TargetControlID="FailDiscountFld" Enabled="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top; padding-top: 5px;">
                                                            <asp:Label ID="Label18" runat="server" Text="Currency" CssClass="label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <asp:DropDownList ID="ddlCurrency" runat="server" DataTextField="Currency" DataValueField="CurrencyID" ToolTip="Currency Name" Width="150px" style="margin-bottom:10px" />
                                                            <br />
                                                            <asp:Button runat="server" ID="btnChangeCurrency" CssClass="btn btn-small btn-default" Text="Set" OnClick="btnChangeCurrency_Click" />
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                             <td style="vertical-align: top; padding-top: 5px; padding-left: 20px; margin-left: 5px;border: dotted silver 1px;text-align:center;">
                                                 <div style="vertical-align: top; padding-top: 5px;">
                                                            <asp:Label ID="SampleListLbl" runat="server" Text="Sample List" CssClass="label"></asp:Label>
                                                 </div>
                                                 <div>
                                                     Sample Price List:
                                                 </div>
                                                 <div>
                                                        <asp:DropDownList ID="SamplePriceList" runat="server" AutoPostBack="True" DataTextField="SamplePriceName" Width="300px"
                                                        DataValueField="PriceRangeID" OnSelectedIndexChanged="OnSamplePriceSelectedChanged" ToolTip="Sample Price List"/>
                                                     <asp:Button runat="server" ID="DelRangeButton" OnClick="OnDelSamplePriceRangeClick" CssClass="btn btn-small btn-info" 
                                                         Text="Del" Width="40px" Height="30px" />

                                                 </div>
                                                 <div style="text-align:center;">
                                                     <asp:ListBox runat="server" ID="PriceSamplePartMeasureList" DataTextField="PartNameMeasureName"
                                                                DataValueField="MeasureCode" Width="280px" />
                                                 </div>
                                                 <div style="vertical-align: top; padding-top: 5px;" align="center">
                                                                <asp:DataGrid runat="server" ID="SamplePriceGrid" AutoGenerateColumns="False" CellPadding="5" CssClass="table table-condensed"
                                                                    CellSpacing="5" DataKeyField="Price" >
                                                                    <Columns>
                                                                    <asp:BoundColumn DataField="ValueFrom" HeaderText="From"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ValueTo" HeaderText="To"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Price" HeaderText="Price"></asp:BoundColumn>
                                                                    </Columns>
                                                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                                </asp:DataGrid>
                                                 </div>
                                                 <div style="vertical-align: top; padding-top: 5px;" align="center">
                                                     <asp:DataGrid runat="server" ID="SampleServicePriceGrid" AutoGenerateColumns="False" CellPadding="5" CssClass="table table-condensed"
                                                         CellSpacing="5" DataKeyField="Price" >
                                                         <Columns>
                                                             <asp:BoundColumn DataField="ServiceName" HeaderText="Service Name"></asp:BoundColumn>
                                                             <asp:BoundColumn DataField="Price" HeaderText="Service Price"></asp:BoundColumn>
                                                         </Columns>
                                                         <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                         <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                     </asp:DataGrid>
                                                 </div>
                                                 <div style="text-align:center">
                                                     <asp:Button runat="server" ID="LoadSamplePriceBtn" OnClick="OnLoadSamplePriceRangeClick" CssClass="btn btn-small btn-info" 
                                                         Text="Load Sample to Price Range" Width="200px" Height="60px" />
                                                 </div>
                                                 <div>
                                                     <asp:Label ID="LoadSampleError" ForeColor="Red" runat="server" Width="90px" Visible="False"></asp:Label>
                                                 </div>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td style="padding-left: 20px;padding-top: 10px">
                                                <table>
                                                    <tr style="padding-top: 10px">
                                                        <td colspan="2">
                                                            <asp:Label ID="Label13" runat="server" Text="Additional Services" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td  style="text-align: right;padding-left: 10px">
                                                            <asp:Button runat="server" ID="AddServiceBtn" OnClick="OnAddServiceBtnClick" CssClass="btn btn-small btn-default"
                                                                Text="Add" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="width: 280px">
                                                            <asp:DataGrid runat="server" ID="AddServicePriceGrid" AutoGenerateColumns="False"
                                                                CssClass="table table-condensed" OnItemDataBound="OnAddServicePriceDataBound"
                                                                DataKeyField="Rownum" OnItemCommand="OnDelAddServiceCommand">
                                                                <Columns>
                                                                    <asp:ButtonColumn Text="Del" />
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Service Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList runat="server" ID="AddServiceField" CssClass="" DataValueField="ServiceId"
                                                                                DataTextField="ServiceName" OnSelectedIndexChanged="OnAddServiceChanged" style="width: 150px">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Price</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="ServicePriceField" Width="50px"></asp:TextBox>
                                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ServPriceExtender" runat="server" ValidChars="0123456789."
                                                                                TargetControlID="ServicePriceField" Enabled="True" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        
</ContentTemplate>
                    


</ajaxToolkit:TabPanel>    

                </ajaxToolkit:TabContainer>
<%--IvanB start--%>
                <asp:Panel runat="server" ID="ReportTypeAssignmentPanel" Visible="false">
                    <table>
                        <tr>
                            <td>
                                <div runat="server" id="FractionsViewDiv" >
                                <div>    
                                    <asp:Label ID="ReqFRanges" runat="server" Text="Fractioin Ranges" CssClass="label"></asp:Label>
                                </div>
                                <div>
                                    <asp:Button runat="server" ID="ReqFRangesBtn" OnClick="OnReqFractionGroupClick" CssClass="btn btn-small btn-info" Text="Fraction Ranges" Width="150px" ToolTip="Fraction Ranges"/>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ReqFRangesGroupList" runat="server" runat="server" AutoPostBack="True" DataTextField="GroupName" OnSelectedIndexChanged="OnReqRangeGroupClick" ToolTip="Range Group List"></asp:DropDownList>
                                    <asp:Button runat="server" ID="ReqFDisplayBtn" OnClick="OnCloseReqRangeGroupClick" CssClass="btn btn-small btn-info" Text="Close" />
                                </div>
                                <div>
                                    <asp:ListBox runat="server" ID="ReqFRangeList" Rows="10" SelectionMode="Multiple" Visible ="False"></asp:ListBox>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="ReportType1Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport1" Text="1" />
                                <asp:Button runat="server" ID="ReportType2Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport2" Text="2" />
                                <asp:Button runat="server" ID="ReportType3Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport3" Text="3" />
                            </td>
                            <td rowspan="2" style="align-top: middle;">
                                <asp:Button runat="server" ID="DeleteReportTypeButton" OnClick="DeleteReportTypeClick" 
                                    CssClass="buttonStyleDeleteReport" Text="del" ToolTip="Delete Report Type Assignment" />
                            </td>
                            <td rowspan="2" class="auto-style4">
                                <table>
                                    <tr>
                                        <td>
                                            <div style="margin-left:270px;">
                                                <asp:Button ID="viewSku" runat="server" Text="View SKU File" ToolTip="Get SKU" CssClass="btn btn-info btn-large" OnClick="ViewSkuPdf" OnClientClick="window.document.forms[0].target='_self';"></asp:Button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group" style="margin-left:50px;border:5px solid brown" id="SkuUploadDiv" runat="server">
								                <label class="control-label" for="SkuFileUpload" style="font-weight: 600; float: left; margin-top: 4px;">SKU file:</label>
                                                <asp:FileUpload ID="SkuFileUploader1" runat="server" Width="200px" ToolTip="Please select file" />
                                                
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="UploadSkuBtn" Text="Upload SKU File" ToolTip="Upload SKU File" CssClass="btn btn-info btn-large" OnClick="UploadSkuBtnClick" Width="162px" />
                                            <asp:Button runat="server" ID="AddSkuOnlyBtn" Text="Add Min SKU" ToolTip="Add Min Sku" CssClass="btn btn-info btn-large" OnClick="AddSkuOnlyBtnClick" />
                                        </td>
                                    </tr>
                                </table>
                                <iframe src="#" id="iframeMemoSkuPDFViewer" width="1000" height="300" style="margin-left:100px; margin-top:50px;" runat="server" visible="true"></iframe>
                                <asp:Image ID="SkuImage" runat="server" Width="1000px" Height="300" Visible="False"></asp:Image><br />
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="ReportType4Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport4" Text="4" />
                                <asp:Button runat="server" ID="ReportType5Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport5" Text="5" />
                                <asp:Button runat="server" ID="ReportType6Button" OnClick="ReportTypeAssignClick" CssClass="buttonStyleReport6" Text="6" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="ShowHideIframeButton" Text="Sample"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <iframe id="SampleIframe"  width="1000" height="500" src = "" Style="display:none;"></iframe>
<%--IvanB start--%>                
                <asp:Button ID="btnModalPopUp" runat="server" Style="display: none" />
                
                <%--  On Attach Print Doc to Document --%>
                <asp:Panel runat="server" ID="AttachToDocPanel" CssClass="modalPopup"  Style="display: none">
                    <asp:Panel ID="AttachToDocDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div>
                            <p>Attach Print Doc to Document:</p>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="form-inline" Style="padding-top: 10px">
                        Print Docs:
                        <asp:DropDownList runat="server" ID="AvailPrintDocsForDocument" DataValueField="Key" DataTextField="OperationTypeName"/>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="AttachToDocOkBtn" runat="server" OnClick="OnAttachToDocOkClick" Text="Add"/>
                            <asp:Button ID="AttachToDocCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenBtnDoc" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenBtnDoc" PopupControlID="AttachToDocPanel" ID="AttachToDocPopupExtender"
                PopupDragHandleControlID="AttachToDocDragHandle" OkControlID="AttachToDocCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- New Sku parameters dialog --%>
                <asp:Panel runat="server" ID="ChoiceItemTypePanel" CssClass="modalPopup" Width="910px" Style="display: none;border: solid 2px #5377A9;" >
                    <asp:Panel ID="ItemTypePanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 30px">
                        <div style="text-align: left;color:#5377A9;font-weight: bold ">
                            <b>New SKU Parameters</b>
                        </div>
                    </asp:Panel>
                    <asp:Panel CssClass="form-inline" DefaultButton="NewProgramNameBtn" runat="server" Style="padding-top: 5px">
                        <strong>New Program Name*:</strong>
                        <asp:TextBox runat="server" ID="NewProgramNameFld"></asp:TextBox>
                        <asp:Button runat="server" ID="NewProgramNameBtn" Style="display: none"  OnClick="OnNewProgramNameClick"/>
                        <asp:Label runat="server" ID="ErrNewCpNameLabel" ForeColor="Red"></asp:Label>
                    </asp:Panel>
                    <table>
                        <tr>
                            <td><asp:Label Text="Item Type Groups" CssClass="label" runat="server"/></td>
                            <td style="padding-left: 2px"><asp:Label ID="Label15" Text="Item Types" CssClass="label" runat="server"/></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <div runat="server" id="ItemTypeGrpDiv" style="height: 200px;overflow-y: auto">
                                    <asp:RadioButtonList ID="ItemGroupsList" runat="server" DataValueField="ItemTypeGroupId"
                                        DataTextField="Title" Width="200px" RepeatDirection="Vertical" BorderStyle="None"
                                        Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnItemGroupsListChanged"
                                        CellPadding="1" CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                                    </asp:RadioButtonList>
                                </div>
                            </td>
<%--IvanB 20.02 start--%>   
                            <td style="vertical-align: top;padding-left: 2px">
                                <div style="overflow-y: auto;height: 300px;width: 400px" runat="server" id="ItemTypeDiv">
                                    <asp:RadioButtonList ID="ItemTypesList" runat="server" DataValueField="ItemTypeId" Width="390px"
                                        DataTextField="Title" RepeatDirection="Vertical" BorderStyle="None" Style="margin-left: 5px"
                                        AutoPostBack="True" OnSelectedIndexChanged="OnItemTypesListChanged" CellPadding="1"
                                        CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                            <td style="vertical-align: top;margin-right: 10px">
                                <div style="overflow-y: auto; height:300px; width: 300px" runat="server" id="ItemTreeDiv">
                                    <asp:TreeView ID="NewSKUTypesTreeView" runat="server">
                                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" BackColor="#00AACC" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalPadding="8px" VerticalPadding="2px" />
                                    </asp:TreeView>
                                </div>
                            </td>
<%--IvanB 20.02 end--%>   
                        </tr>
                    </table>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="ChoiceItemTypeOkButton" runat="server" OnClick="OnNewSkuDialogOkClick" Text="Ok" />
                            <asp:Button ID="ChoiceItemTypeCancelButton" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="NewSkuPopupExtender" runat="server" TargetControlID="HiddenNewSkuBtn"
                    PopupControlID="ChoiceItemTypePanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="ItemTypePanelDragHandle"
                    OkControlID="ChoiceItemTypeCancelButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Save As Dialog--%>
                <asp:Panel runat="server" ID="SaveAsPanel" CssClass="modalPopup"  Style="display: none;width: 430px">
                    <asp:Panel ID="SaveAsDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Gray; color: Black">
                        <div><p>Save Customer Program as</p>
                        </div>
                    </asp:Panel><br/>
                    <asp:Panel ID="Panel5" runat="server" DefaultButton="SaveAsNameBtn">
                        <table>
                            <tr >
                                <td>Save as</td>
                                <td><asp:TextBox runat="server" ID="SaveAsNameFld" Width="280px"></asp:TextBox></td>
                                <td><asp:Button runat="server" ID="SaveAsNameBtn" OnClick="OnSaveAsNameEnter" Style="display: none"/></td>
                            </tr>
                            <tr>
                                <td colspan="3"><asp:Label runat="server" ID="SaveAsErrLabel" ForeColor="DarkRed"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    Customer:
                                </td>
                                <td style="padding-top: -5px; padding-right: 5px">
                                    <asp:DropDownList ID="SaCustomerList" runat="server" DataTextField="CustomerName"
                                        AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnSaCustomerSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Height="26px" />
                                </td>
                                <td style="padding-top: 5px">
                                    <asp:Panel ID="Panel7" runat="server" DefaultButton="SaCustomerLikeButton">
                                        <asp:TextBox runat="server" ID="SaCustomerLike" Width="55px" Height="18px"></asp:TextBox>
                                        <asp:ImageButton ID="SaCustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                            ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnSaCustomerSearchClick" Style="display: none" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>Vendor:</td>
                                <td style="padding-top: -5px; padding-right: 5px">
                                    <asp:DropDownList ID="SaVendorList" runat="server" AutoPostBack="True" CssClass=""
                                        DataTextField="CustomerName" DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnSaVendorSelectedChanged"
                                        ToolTip="Customers List" Width="300px" Enabled="False" />
                                </td>
                                <td style="padding-top: 5px">
                                    <asp:Panel ID="Panel8" runat="server" DefaultButton="SaVendorLikeButton">
                                        <asp:TextBox ID="SaVendorLike" runat="server" Width="55px" Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="SaVendorLikeButton" runat="server" ImageUrl="~/Images/ajaxImages/search.png"
                                            OnClick="OnSaVendorSearchClick" ToolTip="Filtering the list of customers" Style="display: none" />
                                    </asp:Panel>
                                </td>

                            </tr>
                            <tr>
                                <td></td>
                                <td style="vertical-align: middle">
                                    <asp:CheckBox ID="SaVendorAsCustomer" runat="server" Checked="True" Text="the same as Customer"
                                        CssClass="checkbox" Width="250px" OnCheckedChanged="OnSaVendorAsCustomerChanging"
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="SaveAsOkBtn" runat="server" OnClick="OnSaveAsOkClick" Text="Save"/>
                            <asp:Button ID="SaveAsCancBtn" runat="server" Text="Cancel" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenSaveAsBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="SaveAsHiddenBtn" PopupControlID="SaveAsPanel" ID="SaveAsPopupExtender"
                PopupDragHandleControlID="SaveAsDragHandle" OkControlID="SaveAsCancBtn" >
                </ajaxToolkit:ModalPopupExtender>

                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>
                
                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <b>Save Customer Program</b>
                        </div>
                    </asp:Panel>
                    <div><asp:CheckBox runat="server" ID="ApplyAllCp" Text="Apply changes to all Customer Program copies?"/></div><br/>
                    <div style="color: black;padding-top: 10px">Are you sure you want to save this Customer Program?</div>
                    <div ><asp:HiddenField runat="server" ID="SaveMode"/></div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender"
                PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesSaveNoBtn" >
                </ajaxToolkit:ModalPopupExtender>

                  <%-- Save Company Group Question Dialog --%>
                <asp:Panel runat="server" ID="SaveGroupDialog" CssClass="modalPopup"  Style="width: 430px; height:500px; display: none">
                    <asp:Panel runat="server" ID="Panel10" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <b>Save Customer Program in Company Group - <span id="lblCompanyGroup" runat="server"></span></b>
                        </div>
                    </asp:Panel>

                     <div id="dvchkSisterCompanyList" runat="server">
                        <h6>List of group companies</h6>
                        <div style="height:300px;overflow-y:scroll;border:solid 1px black;">
                            <asp:CheckBoxList ID="chkGroupCompanyList" runat="server" Width="425px" CssClass="table table-condensed chkGroupCompanyList"
                                 RepeatLayout="Table" style="overflow-y: scroll;padding:5px;">
                            </asp:CheckBoxList>
                       </div>
                      </div>

                    <div><asp:CheckBox runat="server" ID="chkGroupApplyAllcp" Text="Apply changes to all Customer Program copies?"/></div>
                    <div style="color: black;padding-top: 10px">Are you sure you want to save this Customer Program?</div>
                    <div ><asp:HiddenField runat="server" ID="HiddenField1"/></div>
                    <div style="padding-top: 0px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="BtnQuesSaveYes" runat="server" OnClick="BtnQuesSaveYes_OnClick" Text="Yes"/>
                            <asp:Button ID="BtnQuesSaveNo" runat="server" Text="No" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="btnGroupPopupQuest" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="btnGroupPopupQuest" PopupControlID="SaveGroupDialog" ID="SaveGroupQPopupExtender"
                PopupDragHandleControlID="SaveQDragHandle" OkControlID="BtnQuesSaveNo" >
                </ajaxToolkit:ModalPopupExtender>
                
				<%--new picture popup --%>                
				<asp:Panel runat="server" ID="Panel3" CssClass="modalPopup" Height="850px" Width="600px" Style="display: none">
                	<asp:HiddenField runat="server" ID="HiddenUrl" ></asp:HiddenField>
                	<div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Image ID="NewReportPicture" runat="server" BorderWidth="1px" ></asp:Image>
                        </p>
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;">
                            <asp:Button ID="NewPictureOkButton" runat="server" Text="OK" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="HiddenNewPictureBtn" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenNewPictureBtn" PopupControlID="Panel3" ID="NewPicturePopupExtender" 
                    OkControlID="NewPictureOkButton" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>              
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="UploadSkuBtn" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

