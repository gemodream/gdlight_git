﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.Utilities;
using System.Linq;
using Corpt.TreeModel;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;

namespace Corpt
{
    public partial class CPRulesDisplay2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
                Page.Title = "GSI: CP Overview";
				BatchRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;

            if (!IsPostBack)
            {
                LoadCustomers();
                StateValidators(0);
                CustomerLike.Focus();
                GetSkuList();
                string roleId = Session["RoleID"].ToString();
                if (roleId == "1" || roleId == "45")
                    AddCertBtn.Visible = true;
                else
                    AddCertBtn.Visible = false;
                if (!string.IsNullOrEmpty("" + Request.Params["CustomerProgram"]))
                {
                    SkuTextBox.Text = Request.Params["CustomerProgram"];
                    OnLoadBySkuClick(null, null);
                    return;
                }
            }
        }
        #region By Customer And Customer Program
        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customersList == null || customersList.Count == 0) return;
            var customerModel = customersList.Find(m => m.CustomerId == lstCustomerList.SelectedValue);
            var cps = QueryUtils.GetCustomerPrograms(customerModel, this);
            SetViewState(cps, SessionConstants.CustomerProgramList);
            lstProgramList.DataSource = cps;
            lstProgramList.DataBind();
            if (!string.IsNullOrEmpty(lstProgramList.SelectedValue))
            {
                OnProgramSelectedChanged(null, null);
            } else
            {
                DisplayRuleDetails(SessionConstants.CpRulesByCp);
            }
        }

        protected void OnProgramSelectedChanged(object sender, EventArgs e)
        {
            SaveRulesByCustomerAndCp();
            DisplayRuleDetails(SessionConstants.CpRulesByCp);
        }
        private void SaveRulesByCustomerAndCp()
        {
            //-- Reset preview value
            SetViewState(null, SessionConstants.CpRulesByCp);
            
            //-- Find active Customer
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var customer = customers.Find(m => m.CustomerId == lstCustomerList.SelectedValue);
            if (customer == null) return;
            
            //-- Find active Customer Program
            var cps = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            if (cps == null) return;
            var cp = cps.Find(m => m.CpOfficeIdAndCpId == lstProgramList.SelectedValue);
            if (cp == null) return;

            //-- Get data and Save in ViewState
            SetViewState(QueryUtils.GetCpRulesByCustomerAndCp(customer, cp, this), SessionConstants.CpRulesByCp);
               
        }
        private void DisplayRuleDetails(string constName)
        {
            var cpRuleLookup = GetViewState(constName) as CpRuleLookupModel;
            if (cpRuleLookup == null)
            {
                RuleDetailPanel.Visible = false;
                return;
            }
            var cp = cpRuleLookup.Cp ?? new CustomerProgramModel();

            //-- Display Customer Program Details
            
            //-- CustomerId & CustomerName
            CpCustomerId.Text = string.IsNullOrEmpty(cpRuleLookup.Customer.CustomerId) ?
                    "" : string.Format("Customer ID: {0}", cpRuleLookup.Customer.CustomerId);

            CpCustomerName.Text = string.IsNullOrEmpty(cpRuleLookup.Customer.CustomerName) ?
                    "" : string.Format("Customer Name: {0}", cpRuleLookup.Customer.CustomerName);

            //-- CustomerProgram Name & Style & SRP(? )
            CpSrp.Text = string.IsNullOrEmpty(cp.Srp) ?
                    "" : string.Format("SRP: {0}", cp.Srp);
            CpStyle.Text = string.IsNullOrEmpty(cp.CustomerStyle) ? 
                    "" : string.Format("Customer Style: {0}", cp.CustomerStyle);
            CpName.Text = string.IsNullOrEmpty(cp.CustomerProgramName) ?
                    "" : string.Format("Program Name: {0}", cp.CustomerProgramName);
            
            
            //-- Load Picture
            ShowPicture(cp.Path2Picture);

            //-- Description & Comment
            CpDescription.Text = string.IsNullOrEmpty(cp.Description) ? 
                    "" : string.Format("<b>Comment:</b> {0}", cp.DescriptionDisplay);
            CpComments.Text = string.IsNullOrEmpty(cp.Comment) ? 
                    "" : string.Format("<b>Description:</b> {0}", cp.CommentDisplay);

            //-- Load CP Rules
            var cpRules = cpRuleLookup.CpRuleList;
            var dt = new DataTable("CpRules");
            dt.Columns.Add("Name");
            dt.Columns.Add("Measure");
            dt.Columns.Add("Minimum Value");
            dt.Columns.Add("Maximum Value");
            dt.Columns.Add("Ranges");
            dt.AcceptChanges();
            foreach (var cpRuleModel in cpRules)
            {
                dt.Rows.Add(new object[]
                {
                    cpRuleModel.Name, 
                    cpRuleModel.MeasureName, 
                    cpRuleModel.MinName, 
                    cpRuleModel.MaxName,
                    cpRuleModel.Ranges
                });
            }
            dt.AcceptChanges();
            grdByCust.DataSource = dt;
            grdByCust.DataBind();

            //-- Load Documents List
            var docs = cpRuleLookup.DocList;
            DocList.DataSource = docs;
            DocList.DataBind();
            RuleDetailPanel.Visible = true;
            SKUTypesList.Visible = true;
        }
        protected void OnLoadByCustomerClick(object sender, EventArgs e)
        {
            //viewSku.Visible = false;
            //UploadSkuBtn.Visible = false;
            //SkuUploadDiv.Visible = false;
            //iframeMemoSkuPDFViewer.Visible = false;
            if (TabContainer.ActiveTabIndex == 1)
            {
				OnLoadByBatchClick(null, null);
                return;
            }
            if (TabContainer.ActiveTabIndex == 2)
            {
                OnLoadByStyleClick(null, null);
                return;
            }
            SaveRulesByCustomerAndCp();
            DisplayRuleDetails(SessionConstants.CpRulesByCp);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {

            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }


        #endregion
        
        #region By Batch number
        protected void OnLoadByBatchClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BatchNumber.Text)) return;
            //viewSku.Visible = false;
            //UploadSkuBtn.Visible = false;
            //SkuUploadDiv.Visible = false;
            //iframeMemoSkuPDFViewer.Visible = false;
            var myResult = Utilities.QueryUtils.GetBatchNumberBy7digit(BatchNumber.Text, this.Page);
			if (myResult.Trim() != "") BatchNumber.Text = myResult;
            SetViewState(QueryUtils.GetCpRulesByBatch(BatchNumber.Text, this), SessionConstants.CpRulesByBatch);
            DisplayRuleDetails(SessionConstants.CpRulesByBatch);
            viewSku.Enabled = false;
            ViewSkuPdf(null, null);
        }
        #endregion

        #region By Style
        protected void OnLoadByStyleClick(object sender, EventArgs e)
        {
            StyleCertInfoLbl.Text = "";
            //viewSku.Visible = false;
            //UploadSkuBtn.Visible = false;
            //SkuUploadDiv.Visible = false;
            //iframeMemoSkuPDFViewer.Visible = false;
            if (string.IsNullOrEmpty(StyleTextBox.Text)) return;
            var models = QueryUtils.GetCpRulesByStyle(StyleTextBox.Text, this);
            models.Sort((m1, m2) => String.Compare(m1.DisplayValue, m2.DisplayValue, StringComparison.Ordinal));
            SetViewState(models, SessionConstants.CustomerCpByStyle);
            ComboByStyle.DataSource = models;
            ComboByStyle.DataBind();
            ComboByStyle.Visible = true;
            if (!string.IsNullOrEmpty(ComboByStyle.SelectedValue))
            {
                OnComboByStyleSelectedChanged(null, null);
            } else
            {
                SetViewState(null, SessionConstants.CpRulesByStyle);
                DisplayRuleDetails(SessionConstants.CpRulesByStyle);
            }
            viewSku.Enabled = true;
            ViewSkuPdf(null, null);
        }

        protected void OnComboByStyleSelectedChanged(object sender, EventArgs e)
        {
            var byStyleList = GetViewState(SessionConstants.CustomerCpByStyle) as List<CpRuleLookupModel>;
            if (byStyleList == null) return;
            var byStyle = byStyleList.Find(m => m.Cp.CpOfficeIdAndCpId == ComboByStyle.SelectedValue);
            if (byStyle == null) return;
            SetViewState(QueryUtils.GetCpRulesByStyle(byStyle, this), SessionConstants.CpRulesByStyle);
            DisplayRuleDetails(SessionConstants.CpRulesByStyle);
            ViewSkuPdf(null, null);
        }
        #endregion

        #region By SKU
        protected void OnLoadBySkuClick(object sender, EventArgs e)
        {
            iframeMemoSkuPDFViewer.Visible = false;
            CpRef.Visible = false;
            //SKUTypesList.Visible = false;
            viewSku.Visible = true;
            UploadSkuBtn.Visible = true;
            SkuUploadDiv.Visible = true;
            if (string.IsNullOrEmpty(SkuTextBox.Text)) return;
            string itemTypeID = hdnItemTypeID.Value;
            var models = QueryUtils.GetCpRulesBySku(SkuTextBox.Text, this, itemTypeID);
            models.Sort((m1, m2) => String.Compare(m1.DisplayValue, m2.DisplayValue, StringComparison.Ordinal));
            SetViewState(models, SessionConstants.CustomerCpBySku);
            ComboBySku.DataSource = models;
            ComboBySku.DataBind();
            ComboBySku.Visible = true;
            if (!string.IsNullOrEmpty(ComboBySku.SelectedValue))
            {
                OnComboBySkuSelectedChanged(null, null);
            } else
            {
                //-- nothing
                SetViewState(null, SessionConstants.CpRulesBySku);
                DisplayRuleDetails(SessionConstants.CpRulesBySku);
            }
            viewSku.Enabled = true;
            ViewSkuPdf(null, null);
        }
        private void GetSkuList()
        {
            DataTable dt = QueryCpUtilsNew.GetSKUAllTypesList(this);
            if (dt.Rows.Count > 0)
            {
                List<SKUTypesLIstModel> skuTypesList = (from DataRow row in dt.Rows select new SKUTypesLIstModel(row)).ToList();
                SKUTypesList.DataSource = skuTypesList;
                SKUTypesList.DataBind();
                SKUTypesList.SelectedIndex = -1;
                SKUTypesList.Items.Insert(0, new ListItem("All Types", ""));
            }
        }
        protected void OnSKUTypesListSelectedChanged(object sender, EventArgs e)
        {
            if (sender != null && e != null)
            {
                hdnItemTypeID.Value = SKUTypesList.SelectedValue;
                if (SKUTypesList.SelectedItem.Text != "All Types")
                {
                    var measureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(SKUTypesList.SelectedValue), this);
                    LoadItemTypesPartsTree(measureParts);
                }
            }
            
        }

        private void LoadItemTypesPartsTree(List<MeasurePartModel> parts)
        {
            //var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            SKUTypesTreeView.Nodes.Clear();
            SKUTypesTreeView.Nodes.Add(rootNode);
            SKUTypesTreeView.Nodes[0].Select();
            //OnDescripPartsTreeChanged(null, null);
        }
        protected void OnComboBySkuSelectedChanged(object sender, EventArgs e)
        {
            var bySkuList = GetViewState(SessionConstants.CustomerCpBySku) as List<CpRuleLookupModel>;
            if (bySkuList == null) return;
            var bySku = bySkuList.Find(m => m.Cp.CpOfficeIdAndCpId == ComboBySku.SelectedValue);
            if (bySku == null) return;
            SetViewState(QueryUtils.GetCpRulesByStyle(bySku, this), SessionConstants.CpRulesBySku);
            DisplayRuleDetails(SessionConstants.CpRulesBySku);
            CpRef.InnerHtml = "<strong>SKU: </strong>" + SkuTextBox.Text + string.Format(" (CustomerID = {0})", bySku.Customer.CustomerId);
            CpRef.HRef = "~/CustomerProgramNew.aspx?CPName=" + bySku.Cp.CustomerProgramName + @"&CustomerId=" + bySku.Customer.CustomerId;
            CpRef.Visible = true;
            ViewSkuPdf(null, null);
        }
        #endregion

        #region Others
        protected void OnActiveTabChanged(object sender, EventArgs e)
        {
            StateValidators(TabContainer.ActiveTabIndex);
            if (TabContainer.ActiveTabIndex == 0)
            {
                DisplayRuleDetails(SessionConstants.CpRulesByCp);
                BatchReq.Enabled = false;
                viewSku.Visible = true;
                UploadSkuBtn.Visible = true;
                SkuUploadDiv.Visible = true;
                iframeMemoSkuPDFViewer.Visible = true;
                iframeMemoSkuPDFViewer.Src = "";
                ErrMemoSkuField.Text = "";
                TabContainer.Height = 50;
            }
            if (TabContainer.ActiveTabIndex == 1)
            {
				BatchNumber.Focus();
				DisplayRuleDetails(SessionConstants.CpRulesByBatch);
                viewSku.Visible = true;
                UploadSkuBtn.Visible = true;
                SkuUploadDiv.Visible = true;
                iframeMemoSkuPDFViewer.Visible = true;
                iframeMemoSkuPDFViewer.Src = "";
                ErrMemoSkuField.Text = "";
                TabContainer.Height = 50;
            }
            if (TabContainer.ActiveTabIndex == 2)
            {
                DisplayRuleDetails(SessionConstants.CpRulesByStyle);
                viewSku.Visible = true;
                UploadSkuBtn.Visible = true;
                SkuUploadDiv.Visible = true;
                iframeMemoSkuPDFViewer.Visible = true;
                iframeMemoSkuPDFViewer.Src = "";
                ErrMemoSkuField.Text = "";
                TabContainer.Height = 50;
            }
            if (TabContainer.ActiveTabIndex == 3)
            {
                DisplayRuleDetails(SessionConstants.CpRulesBySku);
                viewSku.Visible = true;
                UploadSkuBtn.Visible = true;
                SkuUploadDiv.Visible = true;
                iframeMemoSkuPDFViewer.Visible = true;
                iframeMemoSkuPDFViewer.Src = "";
                ErrMemoSkuField.Text = "";
                TabContainer.Height = 200;
            }

        }
        private void StateValidators    (int tabIndex)
        {
            BatchReq.Enabled = tabIndex == 1;
            BatchRegExpr.Enabled = tabIndex == 1;
            StyleReq.Enabled = tabIndex == 2;
            SkuReq.Enabled = tabIndex == 3;
        }
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            } else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture"); //imgPath;
            ErrPictureField.Text = errMsg;

        }

        #endregion

        protected void OnAddStyleCertClick(object sender, EventArgs e)
        {
            if (StyleTextBox.Text == "")
            {
                StyleCertInfoLbl.Text = "No Customer Program Name";
                return;
            }
            var byStyleList = GetViewState(SessionConstants.CustomerCpByStyle) as List<CpRuleLookupModel>;
            var byStyle = byStyleList.Find(m => m.Cp.CpOfficeIdAndCpId == ComboByStyle.SelectedValue);
            string cpName = byStyle.Cp.CustomerProgramName;
            string styleAdded = QueryUtils.AddStyleToPrintedItems(StyleTextBox.Text, cpName, this);
            if (styleAdded != "missing")
            {
                byte[] certExists = null;
                try
                {
                    using (WebClient webClient = new WebClient())
                    {
                        string url = @"https://wg.gemscience.net/pdf/" + styleAdded;
                        certExists = webClient.DownloadData(url);
                    }
                    StyleCertInfoLbl.Text = "Record added for style " + StyleTextBox.Text;
                    
                    certExists = null;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    certExists = null;
                    StyleCertInfoLbl.Text = "No cert for sku " + CpName.Text;
                }
            }
            else
                StyleCertInfoLbl.Text = "No record added for style " + StyleTextBox.Text;
        }
        protected void ViewSkuPdf(object sender, EventArgs e)
        {
            if (CpName.Text == "")
            {
                ErrMemoSkuField.Text = "No sku file found";
                return;
            }
            string skuPdf = null;
            iframeMemoSkuPDFViewer.Src = null;
            string fileType = QueryCpUtils.FileUploaded(CpName.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(" ", "").Replace(@"/", "") + @"_" + CpCustomerId.Text.Trim().Replace("Customer ID: ", ""), this);
            if (fileType != null)
            {
                if (fileType.ToLower() != "jpg" && fileType.ToLower() != "png")
                {
                    SkuImage.Visible = false;
                    iframeMemoSkuPDFViewer.Visible = true;
                    string urlPrefix = "";
                    if (fileType == "xls" || fileType == "xlsx" || fileType == "xlsm")
                        urlPrefix = @"https://view.officeapps.live.com/op/embed.aspx?src=";
                    iframeMemoSkuPDFViewer.Src = urlPrefix + @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + CpName.Text.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + CpCustomerId.Text.Trim().Replace("Customer ID: ", "") + "." + fileType;
                }
                else
                {
                    SkuImage.Visible = true;
                    iframeMemoSkuPDFViewer.Visible = false;
                    SkuImage.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + CpName.Text.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + CpCustomerId.Text.Trim().Replace("Customer ID: ", "") + "." + fileType;

                }
            }
            else
            { 
                DataTable dt = QueryCpUtils.GetSkuMemoUrl(CpName.Text.Trim().Replace("Program Name: ", ""), CpCustomerId.Text.Replace("Customer ID: ", ""), this);
                if (dt != null && dt.Rows.Count > 0)
                {
                    skuPdf = dt.Rows[0]["SkuReceiptPDF"].ToString();
                    if (skuPdf != null && skuPdf != "")
                    {
                        iframeMemoSkuPDFViewer.Visible = true;
                        iframeMemoSkuPDFViewer.Src = skuPdf;
                    }
                    //iframeMemoSkuPDFViewer.Src = @"https://view.officeapps.live.com/op/embed.aspx?src=https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/testupload.xlsx";
                    //viewSku.Visible = false;
                }
                else
                    ErrMemoSkuField.Text = "No sku file found";
            }
        }
        //protected void ViewMemoPdf(object sender, EventArgs e)
        //{
        //    iframeMemoSkuPDFViewer.Src = null;
        //    DataTable dt = GetSkuMemoUrl(SkuTextBox.Text);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        string memoUrl = dt.Rows[0]["MemoReceiptPDF"].ToString();
        //        if (memoUrl == null || memoUrl == "")
        //        {
        //            ErrMemoSkuField.Text = "No memo file for " + SkuTextBox.Text;
        //            iframeMemoSkuPDFViewer.Visible = false;
        //            return;
        //        }
        //        iframeMemoSkuPDFViewer.Visible = true;
        //        iframeMemoSkuPDFViewer.Src = memoUrl;
        //        //viewSku.Visible = false;
        //    }
        //}
        protected DataTable GetSkuMemoUrl(string sku)
        {
            if (sku == null || sku == "")
                return null;
            string custId = CpCustomerId.Text.Replace("Customer ID: ", "");
            DataTable dt = QueryUtils.GetSkuMemoPerSku(sku, custId, this);
            if (dt != null)
                return dt;
            else
                return null;
        }

        protected void UploadSkuBtnClick(object sender, EventArgs e)
        {
            string skuFileLink = "";
            if (SkuFileUploader.HasFile)
            {
                UploadSKUFileToAzure();
                skuFileLink = GetSKUPath();
                ViewSkuPdf(null, null);
            }
            else
                ErrMemoSkuField.Text = "Please choose the file to upload.";
        }
        protected void UploadSKUFileToAzure()
        {
            int fileLen = SkuFileUploader.PostedFile.ContentLength;
            byte[] input = new byte[fileLen - 1];
            input = SkuFileUploader.FileBytes;
            string fileName = SkuFileUploader.FileName;
            //fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.:/]+", "", RegexOptions.Compiled);
            using (MemoryStream ms = new MemoryStream())
            {
                var msInput = new MemoryStream(input);
                ms.Write(input, 0, SkuFileUploader.FileBytes.Length);
                fileName = CpName.Text.Trim().Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + CpCustomerId.Text.Trim().Replace("Customer ID: ", "") + fileName.Substring(fileName.LastIndexOf(".")).ToLower() ;
                bool uploaded = QueryCpUtils.UploadImages(fileName, "gdlight/SkuReceipt", ms, this);
            }
        }
        //private static bool UploadImages(string fileName, string pathName, MemoryStream ms)
        //{
        //    try
        //    {
        //        string myAccountName = "gdlightstorage"; //ConfigurationManager.AppSettings["myAccountName"].ToString();
        //        string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";//ConfigurationManager.AppSettings["myAccountKey"].ToString();
        //        StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
        //        CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
        //        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
        //        CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
        //        CloudBlobContainer container = blobClient.GetContainerReference(pathName);
        //        var blockBlob = container.GetBlockBlobReference(fileName);
        //        blockBlob.Properties.ContentType = "application/pdf";
        //        ms.Position = 0;
        //        blockBlob.UploadFromStream(ms);
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }
        //    return true;
        //}
        public string GetSKUPath()
        {
            string oldFileName = SkuFileUploader.FileName;
            string fileName = CpName.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(@"/", "").Replace(" ", "") + SkuFileUploader.FileName.Substring(SkuFileUploader.FileName.LastIndexOf("."));
            string container = Session["AzureContainerName"].ToString();
            string directory = Session["SkuReceiptDirectory"].ToString();
            string url = @"https://gdlightstorage.blob.core.windows.net/" + container + @"/" + directory + @"/" + fileName;
            return url;
        }
        //protected string FileUploaded(string sku)
        //{
        //    if (Session["SkuFilesTypes"] == null)
        //        return null;
        //    //sku = sku.Replace(@"/", "").Replace(@".", "").Replace(" ", "");
        //    string[] types = Session["SkuFilesTypes"].ToString().Split(',');
        //    foreach (string type in types)
        //    {
        //        string url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @"." + type;
        //        WebRequest request = WebRequest.Create(new Uri(url));
        //        request.Method = "HEAD";
        //        try
        //        {
        //            using (WebResponse response = request.GetResponse())
        //            {
        //                return type;
        //                //long contentLength = response.ContentLength;
        //                //string contentType = response.ContentType;
        //            }
                    
        //        }
        //        catch
        //        {
        //            //url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".xlsx";
        //        }
        //    }
        //    //string url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".xls";
        //    //WebRequest request = WebRequest.Create(new Uri(url));
        //    //request.Method = "HEAD";
        //    //try
        //    //{
        //    //    using (WebResponse response = request.GetResponse())
        //    //    {
        //    //        long contentLength = response.ContentLength;
        //    //        string contentType = response.ContentType;
        //    //    }
        //    //    return "xls";
        //    //}
        //    //catch
        //    //{
        //    //    url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".xlsx";
        //    //}
        //    //request = WebRequest.Create(new Uri(url));
        //    //request.Method = "HEAD";
        //    //try
        //    //{
        //    //    using (WebResponse response = request.GetResponse())
        //    //    {
        //    //        long contentLength = response.ContentLength;
        //    //        string contentType = response.ContentType;
        //    //    }
        //    //    return "xlsx";
        //    //}
        //    //catch
        //    //{
        //    //    url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".pdf";
        //    //}
        //    //request = WebRequest.Create(new Uri(url));
        //    //request.Method = "HEAD";
        //    //try
        //    //{
        //    //    using (WebResponse response = request.GetResponse())
        //    //    {
        //    //        long contentLength = response.ContentLength;
        //    //        string contentType = response.ContentType;
        //    //    }
        //    //    return "pdf";
        //    //}
        //    //catch
        //    //{
        //    //    url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".jpg";
        //    //}
        //    //request = WebRequest.Create(new Uri(url));
        //    //request.Method = "HEAD";
        //    //try
        //    //{
        //    //    using (WebResponse response = request.GetResponse())
        //    //    {
        //    //        long contentLength = response.ContentLength;
        //    //        string contentType = response.ContentType;
        //    //    }
        //    //    return "jpg";
        //    //}
        //    //catch
        //    //{
        //    //    url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".png";
        //    //}
        //    //request = WebRequest.Create(new Uri(url));
        //    //request.Method = "HEAD";
        //    //try
        //    //{
        //    //    using (WebResponse response = request.GetResponse())
        //    //    {
        //    //        long contentLength = response.ContentLength;
        //    //        string contentType = response.ContentType;
        //    //    }
        //    //    return "png";
        //    //}
        //    //catch
        //    //{
        //    //    return null;
        //    //}
        //    return null;
        //}
    }
}