﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CustomerProgram : CommonPage
    {
        private const string CmdAddMeasure = "InsertMeasure";
        private const string CmdFocusComment = "CmdFocusComment";
        private const string CmdFocusDescrip = "CmdFocusDescrip";
        private const string CmdAddPriceMeasure = "CmdAddPriceMeasure";
        private const string CmdDelPriceMeasure = "CmdDelPriceMeasure";
        const string Dlg1DivFormat = "overflow-y: auto; height:{0}px; color: black; border: silver solid 1px; margin-top: 5px;weight: 300px";
        const string Dlg2DivFormat = "overflow-y: auto; height:{0}px; color: black; border: silver solid 1px; margin-top: 5px;weight: 600px";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Customer Program";
            if (IsPostBack)
            {
                PostBackHandler();
                return;
            }
            
            //-- Init events
            PartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PartMeasureList, CmdAddPriceMeasure));
            PricePartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PricePartMeasureList, CmdDelPriceMeasure));

            //-- ItemTypeGroups
            LoadItemTypeGroups();

            //-- Short Measures
            LoadMeasuresShort();

            //-- Additional Services
            LoadAdditionalServices();

            //-- EnumMeasures
            LoadEnumMeasures();

            //-- Customers List
            LoadCustomers();

            if (!string.IsNullOrEmpty("" + Request.Params["BatchNumber"]))
            {
                BatchNumberFld.Text = Request.Params["BatchNumber"];
                OnSearchCpByBatchClick(null, null);
                return;
            }
            CustomerLike.Focus();

            LoadCurrency();
        }

        #region PostBack handlers
        private void PostBackHandler()
        {
            if (Request.Params["__EVENTTARGET"] == "ctl00$SampleContent$ItemTypesTreeView")
            {
                NewSkuPopupExtender.Show();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddPriceMeasure)
            {
                OnAddCpPartMeasure();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelPriceMeasure)
            {
                OnRemoveCpPartMeasure();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddMeasure)
            {
                if (DescriptionPartsTreeView.SelectedNode != null && DescripMeasuresField.SelectedValue != null)
                {
                    var text = string.Format("[{0}].[{1}]", DescriptionPartsTreeView.SelectedNode.Text.Trim(),
                                             DescripMeasuresField.SelectedItem.Text.Trim());
                    var lastFocusIsComment = GetViewState(SessionConstants.CpLastFocusIsComment) as string ?? "Yes";
                    if (lastFocusIsComment == "Yes")
                    {
                        CpComments.Text = CpComments.Text + " " + text;
                    } else
                    {
                        CpDescrip.Text = CpDescrip.Text + " " + text;
                    }
                }
                
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdFocusComment)
            {
                SetViewState("Yes", SessionConstants.CpLastFocusIsComment);
                
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdFocusDescrip)
            {
                SetViewState("No", SessionConstants.CpLastFocusIsComment);
                
            }
            
        }
        #endregion

        #region Short Measures on Descrption
        private void LoadMeasuresShort()
        {
            SetViewState(QueryCpUtils.GetMeasuresShort(this), SessionConstants.CpCommonShortMeasures);
        }
        private List<MeasureShortModel> GetMeasuresShortByPartType(string partTypeId)
        {
            var measures = GetViewState(SessionConstants.CpCommonShortMeasures) as List<MeasureShortModel> ??
                           new List<MeasureShortModel>();
            var byType = measures.FindAll(m => m.PartTypeId == partTypeId);
            byType.Sort((m1, m2) => String.CompareOrdinal(m1.MeasureTitle.ToUpper(), m2.MeasureTitle.ToUpper()));
            return byType;
        }
        #endregion

        #region Enum Measures
        private void LoadEnumMeasures()
        {
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.CpCommonEnumMeasures);
        }
        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }
        #endregion

        #region Additional Services
        private void LoadAdditionalServices()
        {
            SetViewState(QueryCpUtils.GetAdditionalServices(this), SessionConstants.CpCommonAdditionalServices);
        }
        #endregion
        
        #region ItemTypeGroups & ItemTypes
        private void LoadItemTypeGroups()
        {
            var itemGroups = QueryCpUtils.GetItemTypeGroups(this);
            itemGroups.Sort((m1, m2) => string.CompareOrdinal(m1.ItemTypeGroupName.ToUpper(), m2.ItemTypeGroupName.ToUpper()));
            itemGroups.Insert(0, new ItemTypeGroupModel{ItemTypeGroupId = 0, ItemTypeGroupName = "Most Recently Used", Path2Icon = "D23.ico"});
            SetViewState(itemGroups, SessionConstants.CpCommonItemTypeGroups);
            ItemGroupsList.DataSource = itemGroups;
            ItemGroupsList.DataBind();
            ItemGroupsList.SelectedValue = "0";
        }
        private List<ItemTypeModel> GetItemTypes(string itemTypeGroup)
        {
            var itemTypes = GetViewState(SessionConstants.CpCommonItemTypes) as List<ItemTypeModel> ?? new List<ItemTypeModel>();
            if (itemTypeGroup == "0")
            {
                var types = itemTypes.FindAll(m => m.IsRecently);
                foreach(var type in types)
                {
                    itemTypes.Remove(type);
                }
                var recently = QueryCpUtils.GetItemTypesRecently(
                    GetCustomerModelFromView(CustomerList.SelectedValue),
                    GetCustomerModelFromView(VendorList.SelectedValue), this);
                itemTypes.AddRange(recently);
                SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
                return recently;
            }
            
            var itemTypesByGroup = itemTypes.FindAll(m => ""+m.ItemTypeGroupId == itemTypeGroup && !m.IsRecently);
            if (itemTypesByGroup.Count > 0) return itemTypesByGroup;
            
            itemTypesByGroup = QueryCpUtils.GetItemTypesByGroup(Convert.ToInt32(itemTypeGroup), this);
            itemTypes.AddRange(itemTypesByGroup);
            SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
            return itemTypesByGroup;
        }
        private ItemTypeGroupModel GetItemTypeGroup(CustomerProgramModel cpModel)
        {
            var groups = GetViewState(SessionConstants.CpCommonItemTypeGroups) as List<ItemTypeGroupModel> ??
                         new List<ItemTypeGroupModel>();
            return groups.Find(m => m.ItemTypeGroupId == cpModel.ItemTypeGroupId);
        }
        private ItemTypeModel GetItemType(string group, string type)
        {
            var types = GetItemTypes(group);
            return types.Find(m => ""+m.ItemTypeId == type);
        }
        private ItemTypeModel GetItemType(CustomerProgramModel cpModel)
        {
            var itemTypes = GetViewState(SessionConstants.CpCommonItemTypes) as List<ItemTypeModel> ?? new List<ItemTypeModel>();
            var itemTypesByGroup = itemTypes.FindAll(m => m.ItemTypeGroupId == cpModel.ItemTypeGroupId);
            if (itemTypesByGroup.Count == 0)
            {
                itemTypesByGroup = QueryCpUtils.GetItemTypesByGroup(cpModel.ItemTypeGroupId, this);
                itemTypes.AddRange(itemTypesByGroup);
                SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
            }
            return itemTypesByGroup.Find(m => m.ItemTypeId == cpModel.ItemTypeId);
        }
        #endregion
       
        #region Customer/Vendor Search Panel
        private CustomerModel GetCustomerModelFromView(string customerId)
        {
            if (string.IsNullOrEmpty(customerId)) return null;
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);

        }
        private List<CustomerModel> GetCustomersFromView()
        {
            return GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }
        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);

        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (VendorAsCustomer.Checked)
            {
                var customer = VendorList.Items.FindByValue(CustomerList.SelectedValue);
                if (customer != null)
                {
                    VendorList.SelectedValue = CustomerList.SelectedValue;
                }
            }
            LoadCpList();
            GetCurrencyByProgram();
        }
        protected void OnVendorSelectedChanged(object sender, EventArgs e)
        {
            LoadCpList();
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            CustomerList.DataSource = filtered;
            CustomerList.DataBind();
            if (filtered.Count == 1)
            {
                CustomerList.SelectedValue = filtered[0].CustomerId;
            } else
            {
                ResetCpList();
            }
            if (VendorAsCustomer.Checked)
            {
                VendorLike.Text = filterText;
                OnVendorSearchClick(null, null);
            }
            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
            NewSkuBtn.Enabled = !string.IsNullOrEmpty(CustomerList.SelectedValue);

        }
        protected void OnVendorSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = VendorLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            VendorList.DataSource = filtered;
            VendorList.DataBind();
            if (filtered.Count == 1) VendorList.SelectedValue = filtered[0].CustomerId;
            if (!string.IsNullOrEmpty(VendorList.SelectedValue))
            {
                OnVendorSelectedChanged(null, null);
            }

        }
        protected void OnVendorAsCustomerChanging(object sender, EventArgs e)
        {
            var asCustomer = VendorAsCustomer.Checked;
            VendorList.Enabled = !asCustomer;
            VendorLike.Enabled = !asCustomer;
        }
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            
            CustomerList.DataSource = customers;
            CustomerList.DataBind();
            CustomerList.SelectedIndex = 0;

            VendorList.DataSource = customers;
            VendorList.DataBind();
            VendorList.SelectedIndex = 0;

            //-- Save as dialog
            SaCustomerList.DataSource = customers;
            SaCustomerList.DataBind();

            SaVendorList.DataSource = customers;
            SaVendorList.DataBind();
        }

        #endregion

        #region Customer Program Section
        protected void OnNewSkuClick(object sender, EventArgs e)
        {
            NewProgramNameFld.Text = "";
            ItemGroupsList.SelectedValue = "0";
            OnItemGroupsListChanged(null, null);
            var h = Convert.ToInt32(ForHeightFld.Value);
            ItemTypeGrpDiv.Attributes.CssStyle.Value = string.Format(Dlg1DivFormat, h);
            ItemTypeDiv.Attributes.CssStyle.Value = string.Format(Dlg2DivFormat, h);
            NewSkuPopupExtender.Show();
            NewProgramNameFld.Focus();
        }

        private void HideCpDetailsSections()
        {
            CpPanel.Visible = false;
            TabContainer.Visible = false;
        }
        private void ShowCpDetailsSections()
        {
            CpPanel.Visible = true;
            TabContainer.Visible = true;
        }
        private void ResetCpList()
        {
            SetViewState(null, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.DataSource = null;
            CpList.DataBind();
            HideCpDetailsSections();
        }
        private void LoadCpList()
        {
            var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            if (customersList.Count == 0)
            {
                return;
            }
            var customerModel = customersList.Find(m => m.CustomerId == CustomerList.SelectedValue);
            var vendorModel = VendorAsCustomer.Checked ? null : customersList.Find(m => m.CustomerId == VendorList.SelectedValue);
            if (customerModel == null) return;
            var cpList = QueryUtils.GetCustomerPrograms(customerModel, vendorModel, this);
            //-- Add Vendor model
            SetViewState(cpList, SessionConstants.CustomerProgramList);
            
            CpList.DataSource = cpList;
            CpList.DataBind();
            if (cpList.Count == 0)
            {
                HideCpDetailsSections();
            } else
            {
                ShowCpDetailsSections();
            }
            OnCpSelectedChanged(null, null);
        }
        protected void OnCpSelectedChanged(object sender, EventArgs e)
        {
            try
            {
                var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                new List<CustomerProgramModel>();
                var cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
                ErrPictureField.Text = cp.Path2Picture;
                SetCpDetails(cp);

                if (!string.IsNullOrEmpty(CpList.SelectedValue) && !TabContainer.Visible)
                {
                    ShowCpDetailsSections();
                }
                SetEnabledSaveButtons();
                GetCurrencyByProgram();
            }
            catch (Exception ex)
            {
                ErrPictureField.Text = ex.Message;
            }
        }
        private void SetEnabledSaveButtons()
        {
            var cpEdit = GetCpEditFromView();
            SaveAsButton.Enabled = (cpEdit != null && cpEdit.Cp.CpId != "0" && !cpEdit.Cp.IsCopy);
            SaveButton.Enabled = (cpEdit != null);
        }
        protected void OnApplyPath2PictureClick(object sender, ImageClickEventArgs e)
        {
            ShowPicture(Path2PictureFld.Text);
        }

        private CpEditModel GetCpEditFromView()
        {
            return GetViewState(SessionConstants.CpEditModel) as CpEditModel;
        }
        private CpEditModel GetCpDetails()
        {
            var cpEdit = GetCpEditFromView();

            //-- Cp Details Panel
            cpEdit.Cp.CustomerProgramName = ProgramNameFld.Text;
            cpEdit.Cp.Path2Picture = Path2PictureFld.Text;
            cpEdit.Cp.CustomerStyle = CpStyle.Text;
            cpEdit.Cp.Srp = ""+ ConvertToDouble(Srp.Text);
            cpEdit.Cp.CpPropertyCustId = CustId.Text;
            cpEdit.Cp.Description = CpDescrip.Text;
            cpEdit.Cp.Comment = CpComments.Text;

            //-- Operations Tab
            var opChecked = new List<string>();
            foreach (TreeNode node in TreeOpers.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0) continue;
                var operKey = node.Value.Split('_'); //-- OperationTypeOfficeId_OperationTypeId
                if (operKey.Length > 1)
                {
                    opChecked.Add(operKey[1]);
                }
            }
            cpEdit.OperationTypes = opChecked;

            //-- Requirements Tab
            SaveCpDocChanges();

            //== Pricing Tab ========
            
            //-- Pricing, Fail Tab
            cpEdit.Cp.FailDiscount = ConvertToDouble(FailDiscountFld.Text);
            cpEdit.Cp.FailFixed = ConvertToDouble(FailFixedFld.Text);

            //-- Pricing, Pass Tab
            cpEdit.Cp.IsFixed = IsFixedFld.SelectedIndex == 0;
            if (cpEdit.Cp.IsFixed)
            {
                cpEdit.CpPrice.PricePartMeasures = new List<PricePartMeasureModel>();
                cpEdit.CpPrice.PriceRanges = new List<PriceRangeModel>();
                cpEdit.Cp.FixedPrice = ConvertToDouble(PassFixedPrice.Text);
                cpEdit.Cp.Discount = ConvertToDouble(PassDiscount.Text);
                cpEdit.Cp.DeltaFix = 0;
            } else
            {
                cpEdit.CpPrice.PriceRanges = GetPriceRangeValues();
                cpEdit.Cp.FixedPrice = 0;
                cpEdit.Cp.Discount = 0;
                cpEdit.Cp.DeltaFix = ConvertToDouble(DeltaFix.Text);
            }
            
            //-- Additinal Services
            var addServices = GetAddServicePriceValues();
            cpEdit.CpPrice.AddServicePrices =
                addServices.FindAll(m => !string.IsNullOrEmpty(m.ServiceId) && m.Price > 0);

            return cpEdit;
        }
        /// <summary>
        /// On CpList changing and New SKU action
        /// </summary>
        /// <param name="cpModel"></param>
        private void SetCpDetails(CustomerProgramModel cpModel)
        {
            if (cpModel != null)
            {
                var editCpModel = cpModel.CpId == "0" ? 
                    QueryCpUtils.InitCustomerProgram(cpModel,  GetEnumMeasure(), this) : 
                    QueryCpUtils.GetCustomerProgramFull(cpModel, GetEnumMeasure(), this);
                editCpModel.Customer = GetCustomerModelFromView(editCpModel.Cp.CustomerId);
                editCpModel.Vendor = GetCustomerModelFromView(editCpModel.Cp.VendorId);
                SetViewState(editCpModel, SessionConstants.CpEditModel);

                ProgramNameFld.Text = cpModel.CustomerProgramName;
                Path2PictureFld.Text = cpModel.Path2Picture;
                CpStyle.Text = cpModel.CustomerStyle;
                Srp.Text = cpModel.Srp;
                CustId.Text = cpModel.CpPropertyCustId;

                CpDescrip.Text = cpModel.Description;
                CpComments.Text = cpModel.Comment;
                //-- Load ItemTypeGroup Icon
                var group = GetItemTypeGroup(cpModel);
                if (group != null)
                {
                    itGroupName.Text = group.ItemTypeGroupName;
                    itGroupIcon.ImageUrl = "~/Images/ItemTypeImages/" + group.Path2Icon;
                }

                //-- Load ItemType Icon
                var itemType = GetItemType(cpModel);
                if (itemType != null)
                {
                    itName.Text = itemType.ItemTypeName;
                    itIcon.ImageUrl = "~/Images/ItemTypeImages/" + itemType.Path2Icon;
                    itId.Text = ""+cpModel.ItemTypeId;
                }
                //-- Load Picture
                ShowPicture(cpModel.Path2Picture);

                //-- Load Print Docs Attached to Cp
                LoadCpReportsTree();
                
                //-- Load Operations Tree
                LoadTreeOperations(editCpModel);

                //-- Load Parts Tree
                LoadDescripPartsTree(editCpModel);

                //-- Load Docs
                LoadCpDocsList(editCpModel, "");

                //-- Load Doc PartsTree
                LoadDocPartsTree(editCpModel);

                //-- Load Pricing
                LoadPricePartsTree(editCpModel);
                LoadPricingPanel(editCpModel);
                PricePartTree.Nodes[0].Select();
                OnPricePartsTreeChanged(null, null);

            }
        }
        private void ShowPicture(string dbPicture)
        {
            int picLength = dbPicture.Length;
            string tmpPic = dbPicture.ToLower();
            if (dbPicture.Substring(picLength-1,1) == @"/" || tmpPic.Contains("default"))
            {
                ErrPictureField.Text = "no pictures";
                return;
            }
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exists";
            }
            ErrPictureField.Text = errMsg;

        }

        #endregion

        #region Description tab
        private void LoadDescripPartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            DescriptionPartsTreeView.Nodes.Clear();
            DescriptionPartsTreeView.Nodes.Add(rootNode);
            DescriptionPartsTreeView.Nodes[0].Select();
            OnDescripPartsTreeChanged(null, null);
        }
        protected void OnDescripPartsTreeChanged(object sender, EventArgs e)
        {
            var selNode = DescriptionPartsTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var parts = GetCpEditFromView().MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            if (part != null)
            {
                var measures = GetMeasuresShortByPartType(part.PartTypeId);
                DescripMeasuresField.DataSource = measures;
                DescripMeasuresField.DataBind();
            }
        }
        private string GetDescripChars()
        {
            if (DescriptionPartsTreeView.SelectedNode == null || DescripMeasuresField.SelectedItem == null) return "";
            return string.Format("[{0}].[{1}]", DescriptionPartsTreeView.SelectedNode.Text.Trim(),
                                             DescripMeasuresField.SelectedItem.Text.Trim());
        }
        protected void OnCopyCharToCommentClick(object sender, ImageClickEventArgs e)
        {
            CpComments.Text = CpComments.Text + " " + GetDescripChars();
        }
        protected void OnCopyCharToDescripClick(object sender, ImageClickEventArgs e)
        {
            CpDescrip.Text = CpDescrip.Text + " " + GetDescripChars();
        }
        #endregion

        #region Available Operations
        protected void TreeOperationCheckChanged(object sender, TreeNodeEventArgs e)
        {


        }
        protected void OnTreeOperationSelectedChanged(object sender, EventArgs e)
        {

        }
        private void LoadTreeOperations(CpEditModel cpModel)
        {
            var allOPers = QueryCpUtils.GetAllOperations(this);
            cpModel.OperationTypeOfficeId =
                allOPers.FindAll(m => !string.IsNullOrEmpty(m.OperationTypeOfficeId))[0].OperationTypeOfficeId;
            foreach (var oper in allOPers.Where(oper => string.IsNullOrEmpty(oper.TreeParentId)))
            {
                oper.TreeParentId = "0";
            }
            var cpOpers = cpModel.OperationTypes;

            var cpTreeIds = cpOpers.Select(o => allOPers.Find(m => m.OperationTypeId == o).TreeId).ToList();

            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Operations" } };
            foreach (var item in allOPers)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.TreeId,
                        ParentId = item.TreeParentId,
                        DisplayName = item.TreeItemName
                       
                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            TreeUtils.CheckNodes(rootNode, cpTreeIds);
            rootNode.Expand();
            TreeOpers.Nodes.Clear();
            TreeOpers.Nodes.Add(rootNode);
        }
        #endregion

        #region Requirements TAB
        private void LoadDocPartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            DocPartTree.Nodes.Clear();
            DocPartTree.Nodes.Add(rootNode);
            if (DocPartTree.Visible)
            {
                DocPartTree.Nodes[0].Select();
            }
            OnDocPartsTreeChanged(null, null);
        }
        protected void OnDocPartsTreeChanged(object sender, EventArgs e)
        {
            ApplyRulesAll();
            LoadCpDocRules(CpDocsList.SelectedValue, DocPartTree.SelectedValue);
        }
        private void LoadCpDocsList(CpEditModel cpModel, string selectDocId)
        {
            var docs = cpModel.CpDocs;
            for (var i = 0; i < docs.Count; i++ )
            {
                docs[i].Title = docs[i].IsDefault ? "Default" : "Document " + (i);
            }
            CpDocsList.DataSource = docs;
            CpDocsList.DataBind();
            if (docs.Count <= 0) return;
            
            CpDocsList.SelectedValue = !string.IsNullOrEmpty(selectDocId) ? selectDocId : ""+docs[0].CpDocId;
            OnCpDocsListChanged(null, null);
        }
        protected void OnCpDocsListChanged(object sender, EventArgs e)
        {
            SaveCpDocChanges();
            CpDocIdHidden.Value = CpDocsList.SelectedValue;
            LoadCpDocRules(CpDocsList.SelectedValue, DocPartTree.SelectedValue);
            LoadDocMeasureGroups(CpDocsList.SelectedValue);
            LoadDocPrintDocs(CpDocsList.SelectedValue);
            var doc = GetCpEditFromView().CpDocs.Find(m => ""+m.CpDocId == CpDocsList.SelectedValue);

            DocDescripField.Text = doc == null ? "" : doc.Description;
            IsReturnDoc.Checked = doc != null && doc.IsReturn;
            DelDocumentBtn.Enabled = doc != null && !doc.IsDefault;
        }
        protected void OnAddDocumentClick(object sender, EventArgs e)
        {
            SaveCpDocChanges();
            var cpEditModel = GetCpEditFromView();
            var docId = QueryCpUtils.AddCpDocModel(cpEditModel, GetEnumMeasure(), false, this);
            LoadCpDocsList(cpEditModel, docId);
        }
        protected void OnDelDocumentClick(object sender, EventArgs e)
        {
            var cpDocId = CpDocsList.SelectedValue;
            if (string.IsNullOrEmpty(cpDocId) || CpDocsList.SelectedIndex == 0) return;
            CpDocIdHidden.Value = "";
            var cpEditModel = GetCpEditFromView();
            
            var docModel = cpEditModel.CpDocs.Find(m => ""+m.CpDocId == cpDocId);
            if (docModel == null) return;
            cpEditModel.CpDocs.Remove(docModel);
            var lastDoc = cpEditModel.CpDocs[cpEditModel.CpDocs.Count - 1];
            LoadCpDocsList(cpEditModel, ""+lastDoc.CpDocId);
        }
        private void SaveCpDocChanges()
        {
            var docId = CpDocIdHidden.Value;
            if (string.IsNullOrEmpty(docId)) return;
            var cpEditModel = GetCpEditFromView();
            var cpDocModel = cpEditModel.CpDocs.Find(m => ""+m.CpDocId == docId);
            if (cpDocModel == null) return;

            cpDocModel.Description = DocDescripField.Text;
            cpDocModel.IsReturn = IsReturnDoc.Checked;
            
            //-- Apply Recheck Measure Groups
            ApplyRechecksAll();

            //-- Apply Rules
            ApplyRulesAll();

            //-- Attached Reports - DONE

        }
        #endregion

        #region Doc rules DataGrid
        private void LoadCpDocRules(string docId, string partId)
        {
            var cpEditModel = GetCpEditFromView();
            var doc = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
            if (string.IsNullOrEmpty(docId) || string.IsNullOrEmpty(partId) || doc == null || doc.IsDefault)
            {
                RulesGrid.DataSource = null;
                RulesGrid.DataBind();
                RuleCountLabel.Text = "";
                return;
            }
            
            if (doc.IsDefault)
            {
                RulesGrid.DataSource = null;
                RulesGrid.DataBind();
                RuleCountLabel.Text = "";
                return;
            }
            var editRules = cpEditModel.CpDocs.Find(m => ""+m.CpDocId == docId).EditDocRules;
            var editRulesNow = editRules.FindAll(m => m.PartId == partId);
            editRulesNow.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureName.ToUpper(), m2.MeasureName.ToUpper()));
            RulesGrid.DataSource = editRulesNow;
            RulesGrid.DataBind();
            //RuleCountLabel.Text = editRulesNow.Count + " rules";
        }

        private const string RuleCheckBoxIsDefault = "CheckBoxIsDefault";
        private const string RuleCheckBoxNotVisible = "CheckBoxNotVisibleInCcm";
        private const string RuleNumericFldMin = "RuleMinNumericFld";
        private const string RuleNumericFldMax = "RuleMaxNumericFld";
        private const string RuleEnumFldMin = "RuleMinEnumFld";
        private const string RuleEnumFldMax = "RuleMaxEnumFld";
        protected void OnRuleItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rule = e.Item.DataItem as CpDocRuleEditModel;
                if (rule == null) return;
                if(rule.IsDefault)
                {
                    var chkBox = e.Item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
                    if (chkBox != null) chkBox.Checked = true;
                }
                if (rule.NotVisibleInCcm)
                {
                    var chkBox = e.Item.FindControl(RuleCheckBoxNotVisible) as CheckBox;
                    if (chkBox != null) chkBox.Checked = true;
                }
                var numFldMin = e.Item.FindControl(RuleNumericFldMin) as TextBox;
                var numFldMax = e.Item.FindControl(RuleNumericFldMax) as TextBox;
                var enumFldMin = e.Item.FindControl(RuleEnumFldMin) as DropDownList;
                var enumFldMax = e.Item.FindControl(RuleEnumFldMax) as DropDownList;
                if (numFldMin == null || numFldMax == null || enumFldMin == null || enumFldMax == null) return;
                var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
                numFldMin.Visible = isNumeric;
                numFldMax.Visible = isNumeric;
                enumFldMin.Visible = !isNumeric;
                enumFldMax.Visible = !isNumeric;
                if (!isNumeric)
                {
                    enumFldMax.DataSource = rule.EnumSource;
                    enumFldMax.DataBind();
                    enumFldMin.DataSource = rule.EnumSource;
                    enumFldMin.DataBind();
                    enumFldMax.SelectedValue = rule.MaxValue;
                    enumFldMin.SelectedValue = rule.MinValue;

                } else
                {
                    numFldMax.Text = rule.MaxValue;
                    numFldMin.Text = rule.MinValue;
                }
            }
        }
        private void ApplyRulesAll()
        {
            foreach (DataGridItem item in RulesGrid.Items)
            {
                ApplyRule(item);
            }
        }
        private void ApplyRule(DataGridItem item)
        {
            if (item == null) return;
            var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
            var rule = GetRuleByKey(uniqueKey);
            if (rule == null) return;
            
            //-- Is Default Measure
            var chkIsDefault = item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
            if (chkIsDefault != null) rule.IsDefault = chkIsDefault.Checked;
            
            //-- Is Not Visible in CCM
            var chkNotVisible = item.FindControl(RuleCheckBoxNotVisible) as CheckBox;
            if (chkNotVisible != null) rule.NotVisibleInCcm = chkNotVisible.Checked;

            //-- Max Value, Min Value
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                var minFld = item.FindControl(RuleNumericFldMin) as TextBox;
                var maxFld = item.FindControl(RuleNumericFldMax) as TextBox;
                if (minFld != null) rule.MinValue = minFld.Text;
                if (maxFld != null) rule.MaxValue = maxFld.Text;
            }
            else
            {
                var minFld = item.FindControl(RuleEnumFldMin) as DropDownList;
                var maxFld = item.FindControl(RuleEnumFldMax) as DropDownList;
                if (minFld != null) rule.MinValue = minFld.SelectedValue;
                if (maxFld != null) rule.MaxValue = maxFld.SelectedValue;
            }
        }

        private CpDocRuleEditModel GetRuleByKey(string key)
        {
            var keys = key.Split(';');
            if (keys.Length != 3) return null;
            var docId = keys[0];
            var partId = keys[1];
            var measureId = keys[2];
            var cpEditModel = GetCpEditFromView();
            var rules = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId).EditDocRules;
            return
                rules.Find(m => "" + m.PartId == partId && "" + m.MeasureId == measureId);
        }
        protected void OnRuleResetBtnClick(object sender, EventArgs e)
        {
            var item = ((LinkButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            //-- Reset Min Value, Max Value, Is Default

            var key = RulesGrid.DataKeys[item.ItemIndex];

            //-- Load Data on EditRulePanel
            var rule = GetRuleByKey(key.ToString());
            if (rule == null) return;
            var numFldMin = item.FindControl(RuleNumericFldMin) as TextBox;
            var numFldMax = item.FindControl(RuleNumericFldMax) as TextBox;
            var enumFldMin = item.FindControl(RuleEnumFldMin) as DropDownList;
            var enumFldMax = item.FindControl(RuleEnumFldMax) as DropDownList;
            var chbIsDefault = item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
            if (numFldMin == null || numFldMax == null || enumFldMin == null || enumFldMax == null || chbIsDefault == null) return;
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                numFldMin.Text = "";
                numFldMax.Text = "";
            }
            else
            {
                enumFldMax.SelectedValue = "0";
                enumFldMin.SelectedValue = "0";
            }
            chbIsDefault.Checked = false;
        }

        #endregion

        #region Doc Measure groups
        private const string RechecksField = "RechecksField";

        private void LoadDocMeasureGroups(string docId)
        {
            if (string.IsNullOrEmpty(docId))
            {
                MeasureGroupGrid.DataSource = null;
                MeasureGroupGrid.DataBind();
                return;
            }
            var cpEditModel = GetCpEditFromView();
            var docModel = cpEditModel.CpDocs.Find(m => ""+m.CpDocId == docId);
            MeasureGroupGrid.DataSource = docModel.MeasureGroups;
            MeasureGroupGrid.DataBind();
        }
        protected void OnMeasureGroupDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var group = e.Item.DataItem as MeasureGroupModel;
                var textBox = e.Item.FindControl(RechecksField) as TextBox;
                if (group != null && textBox != null)
                {
                    textBox.Text = "" + group.NoRecheck;
                }
            }

        }
        protected void OnDocRechecksEnter(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            if (item.ItemIndex < (MeasureGroupGrid.Items.Count - 1))
            {
                var itemNext = MeasureGroupGrid.Items[item.ItemIndex + 1];
                var nextTextBox = (TextBox)itemNext.FindControl(RechecksField);
                if (nextTextBox == null) return;
                nextTextBox.Focus();
            }
        }
        //-- On Changing CpDoc, Before Save
        private void ApplyRechecksAll()
        {
            foreach (DataGridItem item in MeasureGroupGrid.Items)
            {
                ApplyRecheck(item);
            }
        }
        private void ApplyRecheck(DataGridItem item)
        {
            if (item == null) return;
            var uniqueKey = MeasureGroupGrid.DataKeys[item.ItemIndex].ToString();
            var keys = uniqueKey.Split(';'); //-- CpDocId + ";" + MeasureGroupId
            var cpDocId = keys[0];  
            var measureGroupId = keys[1];
            var textBox = item.FindControl(RechecksField) as TextBox;
            if (textBox == null) return;
            var newVal = textBox.Text;

            var cpEditModel = GetCpEditFromView();
            var docModel = cpEditModel.CpDocs.Find(m => ""+m.CpDocId == cpDocId);
            if (docModel == null) return;
            var group = docModel.MeasureGroups.Find(m => "" + m.MeasureGroupId == "" + measureGroupId);
            if (group != null)
            {
                group.NoRecheck = string.IsNullOrEmpty(newVal) ? 0 : Convert.ToInt32(newVal);
            }
        }

        #endregion

        #region New Sku parameters dialog
        protected void OnNewSkuDialogOkClick(object sender, EventArgs e)
        {
            ErrNewCpNameLabel.Text = "";
            if (string.IsNullOrEmpty(NewProgramNameFld.Text))
            {
                NewProgramNameFld.Focus();
                OnNewSkuChangeParameters();
                return;
            }
            //-- Check unique new program name
            var exists = QueryCpUtils.ExistsCp(NewProgramNameFld.Text,
                                    GetCustomerModelFromView(CustomerList.SelectedValue),
                                    GetCustomerModelFromView(VendorList.SelectedValue), "", this);
            if (exists)
            {
                ErrNewCpNameLabel.Text = "Customer program with this name already exists";
                OnNewSkuChangeParameters();
                return;
            }

            //-- Create CpModel
            var itemType = GetItemType(ItemGroupsList.SelectedValue, ItemTypesList.SelectedValue);
            var cpModel = new CustomerProgramModel
            {
                CustomerId = CustomerList.SelectedValue, 
                VendorId = VendorList.SelectedValue,
                CustomerProgramName = NewProgramNameFld.Text,
                ItemTypeGroupId = itemType.ItemTypeGroupId,
                ItemTypeId = itemType.ItemTypeId,
                CpId = "0"
            };
            var editCp = QueryCpUtils.InitCustomerProgram(cpModel, GetEnumMeasure(), this);
            editCp.Customer = GetCustomerModelFromView(CustomerList.SelectedValue);
            editCp.Vendor = GetCustomerModelFromView(VendorList.SelectedValue);
            SetViewState(editCp, SessionConstants.CpEditModel);
            CpPanel.Visible = true;
            TabContainer.Visible = true;
            SetCpDetails(cpModel);
            SetEnabledSaveButtons();
        }
        private void OnNewSkuChangeParameters()
        {
            var h = Convert.ToInt32(ForHeightFld.Value);
            ItemTypeGrpDiv.Attributes.CssStyle.Value = string.Format(Dlg1DivFormat, h);
            ItemTypeDiv.Attributes.CssStyle.Value = string.Format(Dlg2DivFormat, h);

            NewSkuPopupExtender.Show();
        }

        protected void OnItemGroupsListChanged(object sender, EventArgs e)
        {
            var itemTypes = GetItemTypes(ItemGroupsList.SelectedValue);
            itemTypes.Sort((m1, m2) => string.CompareOrdinal(m1.ItemTypeName.ToUpper(), m2.ItemTypeName.ToUpper()));
            
            ItemTypesList.Items.Clear();
            ItemTypesList.DataSource = itemTypes;
            ItemTypesList.DataBind();
            ItemTypesList.SelectedValue = "" + itemTypes[0].ItemTypeId;
            
            OnNewSkuChangeParameters();
        }
        protected void OnItemTypesListChanged(object sender, EventArgs e)
        {
            OnNewSkuChangeParameters();
        }

        protected void OnNewProgramNameClick(object sender, EventArgs e)
        {
            OnNewSkuChangeParameters();
        }

        #endregion

        #region Save and SaveAs buttons
        protected void OnSaveClick(object sender, EventArgs e)
        {
            var saveData = GetCpDetails();
            var mode = ModeSaveExists;
            if (saveData.Cp.IsCopy)
            {
                mode = ModeSaveBranch;
            } else if (saveData.Cp.CpId == "0")
            {
                mode = ModeSaveNew;
            }
            PopupSaveQDialog(mode);
        }
        private void SaveRun(bool copyAll)
        {
            var saveData = GetCpDetails();
            // TODO - verify data
            if (string.IsNullOrEmpty(BatchNumberFld.Text))
            {
                var exists = QueryCpUtils.ExistsCp(saveData.Cp.CustomerProgramName, saveData.Customer, saveData.Vendor, saveData.Cp.CpId, this);
                if (exists)
                {
                    var err = string.Format("Customer program with '{0}' name already exists for this customer and vendor.", saveData.Cp.CustomerProgramName);
                    PopupInfoDialog(err, true);
                    return;
                }
            }
            var msg = QueryCpUtils.SaveCustomerProgram(saveData, copyAll, null, this);
            if (msg.Contains("Error"))
            {
                PopupInfoDialog(msg, true);
                return;
            }
            RefreshCpAfterSave(msg, saveData.Customer, saveData.Vendor);
            PopupInfoDialog("Customer program was saved successfully.", false);
        }
        protected void OnSaveAsClick(object sender, EventArgs e)
        {
            SaveAsNameFld.Text = "";
            SaCustomerLike.Text = "";
            SaCustomerList.DataSource = GetCustomersFromView();
            SaCustomerList.DataBind();

            SaVendorLike.Text = "";
            SaVendorList.DataSource = GetCustomersFromView();
            SaVendorList.DataBind();

            SaveAsOkBtn.Enabled = false;
            SaveAsPopupExtender.Show();
            SaveAsNameFld.Focus();
        }
        private void SaveAsRun()
        {
            var saveData = GetCpDetails();
            var cpFrom = new CustomerProgramModel {CpId = saveData.Cp.CpId, CpOfficeId = saveData.Cp.CpOfficeId};
            saveData.Customer = GetCustomerModelFromView(SaCustomerList.SelectedValue);
            saveData.Vendor = GetCustomerModelFromView(SaVendorList.SelectedValue);
            saveData.Cp.CpId = "";
            saveData.Cp.CpOfficeId = "";
            saveData.Cp.CustomerProgramName = SaveAsNameFld.Text.Trim();
            // TODO - verify data
            var msg = QueryCpUtils.SaveCustomerProgram(saveData, false, cpFrom, this);
            if (msg.Contains("Error"))
            {
                PopupInfoDialog(msg, true);
                return;
            }
            //-- Reload Saved Cp
            CustomerLike.Text = saveData.Customer.CustomerCode;
            CustomerList.DataSource = new List<CustomerModel> {saveData.Customer};
            CustomerList.DataBind();

            VendorLike.Text = saveData.Vendor.CustomerCode;
            VendorList.DataSource = new List<CustomerModel> {saveData.Vendor};
            VendorList.DataBind();

            RefreshCpAfterSave(msg, saveData.Customer, saveData.Vendor);
            PopupInfoDialog("Customer program was saved successfully.", false);
        }
        private void RefreshCpAfterSave(string cpKey, CustomerModel customer, CustomerModel vendor)
        {
            var saveCpId = cpKey.Split('_').Length > 1 ? cpKey.Split('_')[1] : "";
            if (string.IsNullOrEmpty(saveCpId)) return;

            //-- Refresh CpList
            var cpList = QueryUtils.GetCustomerPrograms(customer, vendor, this);
            SetViewState(cpList, SessionConstants.CustomerProgramList);

            CpList.DataSource = cpList;
            CpList.DataBind();
            if (cpList.Count == 0)
            {
                HideCpDetailsSections();
            }
            else
            {
                ShowCpDetailsSections();
            }
            if (cpList.Find(m => m.CpId == saveCpId) == null) return;
            CpList.SelectedValue = saveCpId;
            OnCpSelectedChanged(null, null);
        }
        #endregion

        #region Pricing Panel
        private void LoadPricingPanel(CpEditModel cpEdit)
        {
            FailFixedFld.Text = (cpEdit.Cp.FailFixed > 0 ? ""+cpEdit.Cp.FailFixed : "");
            FailDiscountFld.Text = (cpEdit.Cp.FailDiscount > 0 ? "" + cpEdit.Cp.FailDiscount : "");
            IsFixedFld.SelectedValue = (cpEdit.Cp.IsFixed ? "1" : "0");
            PassFixedPrice.Text = (cpEdit.Cp.FixedPrice > 0 ? "" + cpEdit.Cp.FixedPrice : "");
            PassDiscount.Text = (cpEdit.Cp.Discount > 0 ? "" + cpEdit.Cp.Discount : "");
            DeltaFix.Text = (cpEdit.Cp.DeltaFix > 0 ? "" + cpEdit.Cp.DeltaFix : "");

            LoadCpPartMeasures(cpEdit);
            LoadPriceRangeGrid(cpEdit);
            LoadAddServicePrices(cpEdit);
            OnPricingModeChanges(null, null);
        }
        protected void OnPricingModeChanges(object sender, EventArgs e)
        {
            var isFixed = IsFixedFld.SelectedValue == "1";
            PassFixedPrice.Enabled = isFixed;
            PassDiscount.Enabled = isFixed;
            DeltaFix.Enabled = !isFixed;
            PricePartMeasureList.Enabled = !isFixed;
            PriceRangeGrid.Enabled = !isFixed;
            AddPriceRangeBtn.Enabled = !isFixed;
            PartMeasureList.Enabled = !isFixed;
        }

        private void LoadCpPartMeasures(CpEditModel cpEdit)
        {
            PricePartMeasureList.DataSource = cpEdit.CpPrice.PricePartMeasures;
            PricePartMeasureList.DataBind();
        }
        private void OnAddCpPartMeasure()
        {
            if (PartMeasureList.SelectedItem == null || PricePartTree.SelectedNode == null) return;
            var name = string.Format("[{0}.{1}]", PricePartTree.SelectedNode.Text.Trim(), PartMeasureList.SelectedItem.Text.Trim());
            var cpEdit = GetCpEditFromView();
            var measures = cpEdit.CpPrice.PricePartMeasures;
            if (measures.Find(m => m.PartNameMeasureName == name) != null) return;
            //-- TODO Check HomogeneousClassId
            measures.Add(new PricePartMeasureModel { PartNameMeasureName = name, PartId = PricePartTree.SelectedValue, MeasureCode = PartMeasureList.SelectedValue });
            LoadCpPartMeasures(cpEdit);
        }
        private void OnRemoveCpPartMeasure()
        {
            if (PricePartMeasureList.SelectedItem == null) return;
            var forDel = PricePartMeasureList.SelectedItem.Text;
            var cpEdit = GetCpEditFromView();
            var measures = cpEdit.CpPrice.PricePartMeasures;
            var measure = measures.Find(m => m.PartNameMeasureName == forDel);
            if (measure == null) return;
            measures.Remove(measure);
            LoadCpPartMeasures(cpEdit);
        }
        private void LoadPricePartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = parts.Select(part => new TreeViewModel {Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName}).ToList();
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PricePartTree.Nodes.Clear();
            PricePartTree.Nodes.Add(rootNode);
        }
        protected void OnPricePartsTreeChanged(object sender, EventArgs e)
        {
            var cpEditModel = GetCpEditFromView();

            var selNode = PricePartTree.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var parts = cpEditModel.MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            if (part == null) return;
            var measures = cpEditModel.MeasuresByItemType.FindAll(m => m.Billable && m.PartTypeId == "" + part.PartTypeId);
            PartMeasureList.DataSource = measures;
            PartMeasureList.DataBind();
        }
        private static double ConvertToDouble(string src)
        {
            if (string.IsNullOrEmpty(src)) return 0;
            try
            {
                return Convert.ToDouble(src);
            } catch(Exception)
            {
                return 0;
            }
        }
        #endregion

        #region Price Range Grid

        private const string FromField = "FromField";
        private const string ToField = "ToField";
        private const string PriceField = "PriceField";
        private void LoadPriceRangeGrid(CpEditModel cpEdit)
        {
            PriceRangeGrid.DataSource = cpEdit.CpPrice.PriceRanges.Count == 0 ? null : cpEdit.CpPrice.PriceRanges;
            PriceRangeGrid.DataBind();
        }
        protected void OnPriceRangeDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as PriceRangeModel;
            if (priceModel == null) return;

            var fromFld = e.Item.FindControl(FromField) as TextBox;
            if (fromFld != null) fromFld.Text = "" + priceModel.ValueFrom;

            var toFld = e.Item.FindControl(ToField) as TextBox;
            if (toFld != null) toFld.Text = "" + priceModel.ValueTo;
            var priceFld = e.Item.FindControl(PriceField) as TextBox;
            if (priceFld != null) priceFld.Text = "" + priceModel.Price;
        }
        protected void OnRemovePriceRangeCommand(object source, DataGridCommandEventArgs e)
        {
            var rownum = "" + PriceRangeGrid.DataKeys[e.Item.ItemIndex];
            var result = GetPriceRangeValues();
            var forDel = result.Find(m => "" + m.Rownum == rownum);
            if (forDel != null)
            {
                result.Remove(forDel);
                GetCpEditFromView().CpPrice.PriceRanges = result;
                LoadPriceRangeGrid(GetCpEditFromView());
            }
        }
        private List<PriceRangeModel> GetPriceRangeValues()
        {
            var result = new List<PriceRangeModel>();
            foreach (DataGridItem item in PriceRangeGrid.Items)
            {
                var rownum = "" + PriceRangeGrid.DataKeys[item.ItemIndex];

                var fromFld = item.FindControl(FromField) as TextBox;
                var toFld = item.FindControl(ToField) as TextBox;
                var priceFld = item.FindControl(PriceField) as TextBox;
                if (fromFld == null || toFld == null || priceFld == null) continue;
                var priceRangeModel = new PriceRangeModel
                {
                    Rownum = Convert.ToInt32(rownum),
                    ValueFrom = ConvertToDouble(fromFld.Text),
                    ValueTo = ConvertToDouble(toFld.Text),
                    Price = ConvertToDouble(priceFld.Text),
                };
                result.Add(priceRangeModel);
            }
            return result;
        }
        protected void OnAddPriceRangeBtnClick(object sender, EventArgs e)
        {
            var result = GetPriceRangeValues();
            var rownum = (result.Count == 0 ? 0 : result.Max(m => m.Rownum));
            var lastRange = result.Find(m => m.Rownum == rownum);
            
            var newRow = new PriceRangeModel { Rownum = rownum + 1, Price = 0, ValueFrom = lastRange == null ? 0 : lastRange.ValueTo + 0.01, ValueTo = 0};
            result.Insert(result.Count, newRow);
            GetCpEditFromView().CpPrice.PriceRanges = result;
            LoadPriceRangeGrid(GetCpEditFromView());
        }
        #endregion

        #region Additional Services

        private const string ServicePriceField = "ServicePriceField";
        private const string AddServiceField = "AddServiceField";
        private void LoadAddServicePrices(CpEditModel cpEditModel)
        {
            AddServicePriceGrid.DataSource = cpEditModel.CpPrice.AddServicePrices.Count == 0 ? null : cpEditModel.CpPrice.AddServicePrices;
            AddServicePriceGrid.DataBind();
        }
        private List<AddServicePriceModel> GetAddServicePriceValues()
        {
            var result = new List<AddServicePriceModel>();
            foreach (DataGridItem item in AddServicePriceGrid.Items)
            {
                var rownum = "" + AddServicePriceGrid.DataKeys[item.ItemIndex];
                var addPriceModel = new AddServicePriceModel { Rownum = Convert.ToInt32(rownum) };

                var priceFld = item.FindControl(ServicePriceField) as TextBox;
                if (priceFld != null && !string.IsNullOrEmpty(priceFld.Text))
                {
                    try
                    {
                        addPriceModel.Price = Convert.ToDouble(priceFld.Text);
                    } catch (Exception x)
                    {
                        var msg = x.Message;
                        addPriceModel.Price = 0;
                    }
                }
                var serviceFld = item.FindControl(AddServiceField) as DropDownList;
                if (serviceFld != null)
                {
                    if (serviceFld.SelectedItem != null)
                    {
                        addPriceModel.ServiceId = serviceFld.SelectedItem.Value;
                        addPriceModel.ServiceName = serviceFld.SelectedItem.Text;
                    }
                }
                result.Add(addPriceModel);
            }
            return result;
        }
        protected void OnAddServiceChanged(object sender, EventArgs e)
        {
            var serviceFld = (DropDownList)sender;

            var item = serviceFld.NamingContainer as DataGridItem;

            if (item == null) return;
            var rownum = "" + AddServicePriceGrid.DataKeys[item.ItemIndex];

            var cpEdit = GetCpEditFromView();
            var serviceModel = cpEdit.CpPrice.AddServicePrices.Find(m => "" + m.Rownum == rownum);
            serviceModel.ServiceId = serviceFld.SelectedItem.Value;
            serviceModel.ServiceName = serviceFld.SelectedItem.Text;
        }
        protected void OnAddServicePriceDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as AddServicePriceModel;
            if (priceModel == null) return;

            var addServiceFld = e.Item.FindControl(AddServiceField) as DropDownList;
            if (addServiceFld != null)
            {
                addServiceFld.DataSource =
                    GetViewState(SessionConstants.CpCommonAdditionalServices) as List<AddServiceModel>;
                addServiceFld.DataBind();
                if (!string.IsNullOrEmpty(priceModel.ServiceId))
                {
                    addServiceFld.SelectedValue = priceModel.ServiceId;
                }
            }
            var priceFld = e.Item.FindControl(ServicePriceField) as TextBox;
            if (priceFld != null) priceFld.Text = "" + priceModel.Price;
        }
        protected void OnDelAddServiceCommand(object source, DataGridCommandEventArgs e)
        {
            //-- Remove line
            var rownum = "" + AddServicePriceGrid.DataKeys[e.Item.ItemIndex];
            var result = GetAddServicePriceValues();
            var forDel = result.Find(m => "" + m.Rownum == rownum);
            if (forDel != null)
            {
                result.Remove(forDel);
                GetCpEditFromView().CpPrice.AddServicePrices = result;
                LoadAddServicePrices(GetCpEditFromView());
            }
        }
        protected void OnAddServiceBtnClick(object sender, EventArgs e)
        {
            //-- Add new line
            var result = GetAddServicePriceValues();
            var rownum = (result.Count == 0 ? 0 : result.Max(m => m.Rownum)) + 1;
            var newRow = new AddServicePriceModel {Rownum = rownum, Price = 0};
            result.Insert(result.Count, newRow);
            GetCpEditFromView().CpPrice.AddServicePrices = result;
            LoadAddServicePrices(GetCpEditFromView());
        }
        #endregion
        
        #region Attached Print Docs on Documents (Tab Requirements)
        private void LoadDocPrintDocs(string docId)
        {
            DocAttachedReportsList.Items.Clear();
            if (!string.IsNullOrEmpty(docId))
            {
                var cpEditModel = GetCpEditFromView();
                var cpDocModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
                var attachedReports = cpDocModel.AttachedReports;
                attachedReports.Sort((m1, m2) => string.CompareOrdinal(m1.OperationTypeName.ToUpper(), m2.OperationTypeName.ToUpper()));
                DocAttachedReportsList.DataSource = attachedReports;
                DocAttachedReportsList.DataBind();
            }
            SetEnabledReportDelBtn();
            SetEnabledReportAddBtn();
        }
        protected void AddPrintDocToDocClick(object sender, ImageClickEventArgs e)
        {
            //-- Load Reports Attached to Cp

            AttachToDocPopupExtender.Show();
        }
        private void SetEnabledReportDelBtn()
        {
            var printDocId = DocAttachedReportsList.SelectedValue;
            var hasValue = !string.IsNullOrEmpty(printDocId);
            DelDocPrintDocBtn.Enabled = hasValue;

            ViewLink.HRef = "";
            if (!hasValue) return;
            var cpKey = GetCpEditFromView().Cp.CpOfficeIdAndCpId;
            var docKey = DocAttachedReportsList.SelectedValue;
            var cpDocId = CpDocsList.SelectedValue;
            var operationTypeId = "";
            if (docKey.Split('_').Length == 2)
            {
                operationTypeId = docKey.Split('_')[1];
            }
            if (string.IsNullOrEmpty(cpKey) || string.IsNullOrEmpty(docKey)) return;
            var docType = GetDocumentTypeCode(docKey);
            var format = "DefineDocument.aspx?" + 
                         DefineDocument.ParamCpKey + "={0}&" +
                         DefineDocument.ParamDocTypeCode + "={1}&" + 
                         DefineDocument.ParamCpDocId + "={2}&" + 
                         DefineDocument.ParamOperationTypeId + "={3}";
            ViewLink.HRef = string.Format(format, cpKey, docType, cpDocId, operationTypeId);
            DocAttachedReportsList.Focus();
        }
        /// <summary>
        /// After: Load DocAttachPrintDocs, Add Print Doc, Del Print Doc
        /// </summary>
        private void SetEnabledReportAddBtn()
        {
            var cpEditModel = GetCpEditFromView();
            var attCpDocs = GetAttachedToCpDocs();
            var cpDocument = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == CpDocsList.SelectedValue);
            var attDocDocs = cpDocument.AttachedReports;
            var canAttach = new List<CpDocPrintModel>();
            foreach(var cpDoc in attCpDocs)
            {
                var unAttach = attDocDocs.Find(m => m.Key == cpDoc.Key);
                if (unAttach == null) canAttach.Add(cpDoc);
            }
            AddPrintDocToDocBtn.Enabled = canAttach.Count > 0;
            AvailPrintDocsForDocument.DataSource = canAttach;
            AvailPrintDocsForDocument.DataBind();
            if (canAttach.Count > 0) AvailPrintDocsForDocument.SelectedIndex = 0;
        }
        private List<CpDocPrintModel> GetAttachedToCpDocs()
        {
            var cpEditModel = GetCpEditFromView();
            return cpEditModel.Reports.FindAll(m => m.IsAttachedToCp);
        }
        protected void OnAttachToDocOkClick(object sender, EventArgs e)
        {
            var docId = CpDocsList.SelectedValue;
            var document = GetCpDocSelected();
            if (document == null) return;
            var keyAdded = AvailPrintDocsForDocument.SelectedValue;
            var docAdded = GetAttachedToCpDocs().Find(m => m.Key == keyAdded);
            document.AttachedReports.Add(new CpDocPrintModel(docAdded));
            LoadDocPrintDocs(docId);
        }

        protected void OnDocAttachPrintDocSelection(object sender, EventArgs e)
        {
            SetEnabledReportDelBtn();
        }
        private CpDocModel GetCpDocSelected()
        {
            var cpEditModel = GetCpEditFromView();
            var docId = CpDocsList.SelectedValue;
            return cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
        }
        protected void OnDelDocPrintDocClick(object sender, ImageClickEventArgs e)
        {
            var docId = CpDocsList.SelectedValue;
            var document = GetCpDocSelected();
            if (document == null) return;
            var printDocKey = DocAttachedReportsList.SelectedValue;
            var printDoc = document.AttachedReports.Find(m => m.Key == printDocKey);
            if (printDoc == null) return;
            document.AttachedReports.Remove(printDoc);
            LoadDocPrintDocs(docId);
        }
        private string GetDocumentTypeCode(string id)
        {
            var cpEdit = GetCpEditFromView();
            var doc = cpEdit.Reports.Find(m => m.Key == id);
            return doc == null ? "" : doc.DocumentTypeCode;
        }
        #endregion

        #region Cp Reports Tree
        private void LoadCpReportsTree()
        {
            CpReportsTree.Nodes.Clear();
            var documents = GetCpEditFromView().Reports;
            var docTypes = documents.FindAll(m => !m.IsAttachedToCp);
            docTypes.Sort((m1, m2) => string.CompareOrdinal(m1.OperationTypeName.ToUpper(), m2.OperationTypeName.ToUpper()));
            foreach (var docType in docTypes)
            {
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = docType.DocumentTypeCode, DisplayName = docType.DisplayUrl } };
                var docsByType = documents.FindAll(m => m.IsAttachedToCp && m.DocumentTypeCode == docType.DocumentTypeCode);
                docsByType.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
                foreach (var docByType in docsByType)
                {
                    data.Add(new TreeViewModel { ParentId = docByType.DocumentTypeCode, Id = docByType.Key, DisplayName = docByType.DisplayUrl });
                }
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                CpReportsTree.Nodes.Add(rootNode);
            }
        }
        protected void OnRefreshCpDocumentsClick(object sender, ImageClickEventArgs e)
        {
            var editData = GetCpEditFromView();
            editData.Reports = QueryCpUtils.GetReportsByCp(editData.Cp, this);
            LoadCpReportsTree();
            SetEnabledReportAddBtn();
        }
        #endregion

        #region Cp By Batch Number
        protected void OnSearchCpByBatchClick(object sender, EventArgs e)
        {
			var myResult = Utilities.QueryUtils.GetBatchNumberBy7digit(BatchNumberFld.Text.Trim(), this.Page);
			if (myResult.Trim() != "") BatchNumberFld.Text = myResult;
			var batchNumber = BatchNumberFld.Text.Trim();
            if (string.IsNullOrEmpty(batchNumber))
            {
                CpByBatchModeOff();
                return;
            }
            if (batchNumber.Length < 8)
            {
                PopupInfoDialog(string.Format("The entered batch code {0} does not exist.", batchNumber), true);
                return;
            }
            var cp = QueryCpUtils.GetCustomerProgramByBatchNumber(batchNumber, this);
            if (cp == null)
            {
                PopupInfoDialog(string.Format("The entered batch code {0} does not exist.", batchNumber), true);
                return;
            }
            
            //-- Customer and Vendor
            var customer = GetCustomerModelFromView(cp.CustomerId);
            if (customer == null) return;
            var vendor = string.IsNullOrEmpty(cp.VendorId) ? customer : GetCustomerModelFromView(cp.VendorId);
            CustomerLike.Text = customer.CustomerCode;
            CustomerList.Items.Clear();
            CustomerList.Items.Add(new ListItem(customer.CustomerName, customer.CustomerId));
            CustomerList.SelectedValue = customer.CustomerId;
            VendorLike.Text = vendor.CustomerCode;
            VendorList.Items.Clear();
            VendorList.Items.Add(new ListItem(vendor.CustomerName, vendor.CustomerId));
            VendorList.SelectedValue = vendor.CustomerId;
            VendorAsCustomer.Checked = customer.CustomerId == vendor.CustomerId;
            OnVendorAsCustomerChanging(null, null);

            var cpList = new List<CustomerProgramModel> {cp};
            SetViewState(cpList, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.Items.Add(new ListItem(cp.CustomerProgramName, cp.CpId));
            CpList.SelectedValue = cp.CpId;
            CpByBatchModeOn();
            OnCpSelectedChanged(null, null);
        }
        private void CpByBatchModeOff()
        {
            CustomerList.Enabled = true;
            CustomerLike.Enabled = true;
            VendorAsCustomer.Enabled = true;
            NewSkuBtn.Enabled = CustomerList.SelectedItem != null;
            ProgramNameFld.Enabled = true;
            if (CustomerList.SelectedItem != null)
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        private void CpByBatchModeOn()
        {
            CustomerList.Enabled = false;
            CustomerLike.Enabled = false;
            VendorAsCustomer.Enabled = false;
            NewSkuBtn.Enabled = false;
            ProgramNameFld.Enabled = false;
        }
        #endregion

        #region SaveAs dialog
        private bool IsEnabledSaveAsBtn()
        {
            return SaveAsNameFld.Text.Trim().Length > 0 && SaCustomerList.SelectedItem != null;
        }
        protected void OnSaCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (SaVendorAsCustomer.Checked)
            {
                var customer = SaVendorList.Items.FindByValue(SaCustomerList.SelectedValue);
                if (customer != null)
                {
                    SaVendorList.SelectedValue = SaCustomerList.SelectedValue;
                }
            }
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();

        }

        protected void OnSaCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = SaCustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            SaCustomerList.DataSource = filtered;
            SaCustomerList.DataBind();
            if (filtered.Count == 1)
            {
                SaCustomerList.SelectedValue = filtered[0].CustomerId;
            }
            if (SaVendorAsCustomer.Checked)
            {
                SaVendorLike.Text = filterText;
                OnSaVendorSearchClick(null, null);
            }
            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnSaCustomerSelectedChanged(null, null);
            }
        }

        protected void OnSaVendorSelectedChanged(object sender, EventArgs e)
        {
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }

        protected void OnSaVendorSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = SaVendorLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            SaVendorList.DataSource = filtered;
            SaVendorList.DataBind();
            if (filtered.Count > 0) SaVendorList.SelectedValue = filtered[0].CustomerId;
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();

        }

        protected void OnSaVendorAsCustomerChanging(object sender, EventArgs e)
        {
            var asCustomer = SaVendorAsCustomer.Checked;
            SaVendorList.Enabled = !asCustomer;
            SaVendorLike.Enabled = !asCustomer;
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }

        protected void OnSaveAsOkClick(object sender, EventArgs e)
        {
            SaveAsErrLabel.Text = "";
            var exists = QueryCpUtils.ExistsCp(SaveAsNameFld.Text,
                                    GetCustomerModelFromView(SaCustomerList.SelectedValue),
                                    GetCustomerModelFromView(SaVendorList.SelectedValue), "", this);
            if (exists)
            {
                SaveAsErrLabel.Text = "Customer program with this name already exists for this customer and vendor.";
                SaveAsPopupExtender.Show();
                return;
            }
            SaveAsRun();
        }

        protected void OnSaveAsNameEnter(object sender, EventArgs e)
        {
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }
        #endregion

        #region Question dialog On Save

        private const string ModeSaveExists = "ModeSaveExists";
        private const string ModeSaveNew = "ModeSaveNew";
        private const string ModeSaveBranch = "ModeSaveBranch";
        private void PopupSaveQDialog(string mode)
        {
            SaveMode.Value = mode;
            ApplyAllCp.Visible = mode == ModeSaveBranch || mode == ModeSaveExists;
            ApplyAllCp.Checked = false;
            SaveQPopupExtender.Show();
        }
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            SaveRun(ApplyAllCp.Checked);
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Currency

        private void LoadCurrency()
        {
            List<CurrencyModel> currency = new List<CurrencyModel>();
            currency = GSIAppQueryUtils.GetCurrency(this);
            currency.Add(new CurrencyModel { CurrencyID = -1, Currency = "" });
            currency.Sort((m1, m2) => String.CompareOrdinal(m1.CurrencyID.ToString(), m2.CurrencyID.ToString()));
            currency.Add(new CurrencyModel { CurrencyID = 0, Currency = "---Remove Currency---" });
            SetViewState(currency, SessionConstants.CurrencyList);

            ddlCurrency.DataSource = currency;
            ddlCurrency.DataBind();
            ddlCurrency.SelectedIndex = 0;
        }

        private void GetCurrencyByProgram()
        {
            Int32 customerID = Int32.Parse(CustomerList.SelectedValue);
            string customerProgramName = CpList.SelectedItem.Text;
            var currency = GSIAppQueryUtils.GetCurrency(customerID, customerProgramName, this);
            if (currency != null)
                //ddlCurrency.SelectedItem.Text = currency.Currency;
                ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByText(currency.Currency));
            else
                ddlCurrency.SelectedIndex = -1;

        }

        protected void btnChangeCurrency_Click(object sender, EventArgs e)
        {
            Int32 customerID = Int32.Parse(CustomerList.SelectedValue);
            string customerProgramName = CpList.SelectedItem.Text;
            //Int32 cpid = ddlCpList.SelectedValue == "" ? 0 : Int32.Parse(ddlCpList.SelectedValue);
            Int32 currencyID = ddlCurrency.SelectedValue == "" ? 0 : Int32.Parse(ddlCurrency.SelectedValue);
            string result = GSIAppQueryUtils.SaveCurrency(customerID, customerProgramName, currencyID, this);
            GetCurrencyByProgram();
        }

        #endregion

        protected void BatchNumberFld_TextChanged(object sender, EventArgs e)
		{

		}
    }
}