using System;

namespace Corpt
{
	/// <summary>
	/// Summary description for SingleItem.
	/// </summary>
	public class SingleItem
	{
		public SingleItem()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private int orderCode;
		private int batchCode;
		private int itemCode;
		private int batchID;
		private int itemTypeID;
		private int stateID;

		public int StateID // Acutally I am using it as V0Item.StateID
		{
			get
			{
				return stateID;
			}
			set
			{
                stateID = value;
			}
		}

		public int OrderCode
		{
			get
			{
				return orderCode;
			}
			set
			{
				orderCode = value;
			}
		}

		public int BatchCode
		{
			get
			{
				return batchCode;
			}
			set
			{
				batchCode = value;
			}

		}
		public int ItemCode
		{
			get
			{
				return itemCode;
			}
			set
			{
				itemCode = value;
			}
		}
		public int BatchID
		{
			get
			{
				return batchID;
			}
			set
			{
				batchID = value;
			}
		}
		public int ItemTypeID
		{
			get
			{
				return itemTypeID;
			}
			set
			{
				itemTypeID = value;
			}
		}

		public override string ToString()
		{
			return Utils.FullItemNumber(orderCode.ToString(), batchCode.ToString(), itemCode.ToString());
		}

		private int newItemCode;
		private int newBatchID;

		public int NewItemCode
		{
			get
			{
				return newItemCode;
			}
			set
			{
				newItemCode = value;
			}
		}

		public int NewBatchID
		{
			get
			{
				return newBatchID;
			}
			set
			{
				newBatchID = value;
			}
		}
	}
}
