﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.AccountRepModels;
using Corpt.Constants;
using Corpt.Models;
using GemoDream.Database;
using Newtonsoft.Json;

namespace Corpt
{
    public partial class AccountRep
    {
        public enum TreeViewLevels
        {
            Group,
            Batch,
            Item,
            ItemDoc
        }

        public const string MemoTableName = "Memo";
        public const string PoTableName = "PO";

        private int _totalNodesCount;


        #region TreeLevelInfo 
        private static readonly TreeLevelInfo ItemDocLevel = new TreeLevelInfo
        {
            TableName = TreeViewLevels.ItemDoc,
            TreeNodeTextGetter = (row, accountRep) =>
            {
                var text =
                    row["OperationChar"] +
                    Utils.FillToFiveChars(Convert.ToString(row["GroupCode"])) + "." +
                    Utils.FullItemNumberWithDots(
                        Convert.ToInt32(row["GroupCode"]),
                        Convert.ToInt32(row["BatchCode"]),
                        Convert.ToInt32(row["ItemCode"]));

                return text;
            },
            TreeNodeValueGetter = row => JsonConvert.SerializeObject(new TreeNodeItemDocModel(row)),
            Child = null,
            ChildJoinCondition = null
        };

        private static readonly TreeLevelInfo ItemLevel = new TreeLevelInfo
        {
            TableName = TreeViewLevels.Item,
            TreeNodeTextGetter = (row, accountRep) =>
            {
                var text =
                    Utils.FillToFiveChars(Convert.ToString(row["GroupCode"])) + "." +
                    Utils.FullItemNumberWithDots(
                        Convert.ToInt32(row["GroupCode"]),
                        Convert.ToInt32(row["BatchCode"]),
                        Convert.ToInt32(row["ItemCode"]));

                string sItemNumber = text;
                if (row["Weight"] != null && row["Weight"] != DBNull.Value)
                {
                    text += " " + row["Weight"] + " " + row["WeightUnitName"];
                }
                if (row["Color"] != null && row["Color"] != DBNull.Value && row["Color"].ToString() != "" ||
                    row["Clarity"] != null && row["Clarity"] != DBNull.Value && row["Clarity"].ToString() != "" ||
                    row["KM"] != null && row["KM"] != DBNull.Value && row["KM"].ToString() != "" ||
                    row["LD"] != null && row["LD"] != DBNull.Value && row["LD"].ToString() != "")
                {
                    text += " (";
                    if (row["KM"] != null && row["KM"] != DBNull.Value && row["KM"].ToString() != "")
                    {
                        text += row["KM"] + " ";
                        //this.BackColor = System.Drawing.Color.Yellow;
                    }
                    if (row["LD"] != null && row["LD"] != DBNull.Value && row["LD"].ToString() != "")
                    {
                        text += row["LD"] + " ";
                        //this.ForeColor = System.Drawing.Color.Red;
                    }
                    if (row["Color"] != null && row["Color"] != DBNull.Value && row["Color"].ToString() != "")
                        text += row["Color"];
                    text += "/";
                    if (row["Clarity"] != null && row["Clarity"] != DBNull.Value && row["Clarity"].ToString() != "")
                        text += row["Clarity"];
                    text += ")";
                }
                string sNewItemNumber = "";
                string sPrevItemNumber = "";
                //string sItemNumber = "";
                if (row["NewOrderCode"] != DBNull.Value && row["NewBatchCode"] != DBNull.Value && row["NewItemCode"] != DBNull.Value)
                {
                    sNewItemNumber = Utils.FillToFiveChars(row["NewOrderCode"].ToString()) + "." +
                        Utils.FillToFiveChars(row["NewOrderCode"].ToString()) + "." +
                        Utils.FillToThreeChars(row["NewBatchCode"].ToString(), row["NewOrderCode"].ToString()) + "." +
                        Utils.FillToTwoChars(row["NewItemCode"].ToString());
                    if (sItemNumber == sNewItemNumber)
                        sNewItemNumber = "";
                }
                if (row["PrevOrderCode"] != DBNull.Value && row["PrevGroupCode"] != DBNull.Value && row["PrevBatchCode"] != DBNull.Value && row["PrevItemCode"] != DBNull.Value)
                {
                    sPrevItemNumber =
                        Utils.FillToFiveChars(row["PrevOrderCode"].ToString()) + "." +
                        Utils.FillToFiveChars(row["PrevGroupCode"].ToString()) + "." +
                        Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), row["PrevGroupCode"].ToString()) + "." +
                        Utils.FillToTwoChars(row["PrevItemCode"].ToString());
                }

                if (sNewItemNumber == "") sNewItemNumber = sItemNumber;
                if (sPrevItemNumber == "") sPrevItemNumber = sItemNumber;
                if (sItemNumber == sNewItemNumber & sItemNumber == sPrevItemNumber)
                {

                }
                else
                {
                    if (sItemNumber != sPrevItemNumber)// && sItemNumber == sNewItemNumber)
                    {
                        text += " (Old # " + sPrevItemNumber + ")";
                    }
                    //else
                    if (sItemNumber != sNewItemNumber)// && sItemNumber==sPrevItemNumber)
                    {
                        text += " (Current # " + sNewItemNumber + ")";
                    }
                }

                return text;
            },
            TreeNodeValueGetter = row => JsonConvert.SerializeObject(new TreeNodeItemModel(row)),
            Child = ItemDocLevel,
            ChildJoinCondition = row => $"BatchID = {row["BatchID"]} AND ItemCode = {row["ItemCode"]}"
        };

        private static readonly TreeLevelInfo BatchLevel = new TreeLevelInfo
        {
            TableName = TreeViewLevels.Batch,
            TreeNodeTextGetter = (row, accountRep) =>
            {
                var text =
                    Utils.FillToFiveChars(Convert.ToString(row["GroupCode"])) + "." +
                    Utils.FullItemNumberWithDots(Convert.ToInt32(row["GroupCode"]), Convert.ToInt32(row["BatchCode"]));

                if (row["MemoNumber"] != DBNull.Value)
                {
                    if (row["MemoNumber"].ToString().Trim() != "")
                        text += " (M: " + row["MemoNumber"].ToString().Trim() + ")";
                }
                return text;
            },
            TreeNodeValueGetter = row => JsonConvert.SerializeObject(new TreeNodeBatchModel(row)),
            Child = ItemLevel,
            ChildJoinCondition = row => $"BatchID = {row["BatchID"]}"
        };

        private static readonly TreeLevelInfo OrderLevel = new TreeLevelInfo
        {
            TableName = TreeViewLevels.Group,
            TreeNodeTextGetter = (row, accountRep) =>
            {
                var text =
                    Utils.FillToFiveChars(Convert.ToString(row["GroupCode"])) + "." +
                    Utils.FillToFiveChars(Convert.ToString(row["GroupCode"]));

                if (row["CreateDate"] != DBNull.Value)
                {
                    var authorOfficeId = row.ConvertToInt("AuthorOfficeID");
                    var createDate = row.ConvertToDateTime("CreateDate");
                    var timeZonedDate = accountRep.ApplyTimeZoneOffset(createDate, authorOfficeId);
                    text += " (" + timeZonedDate + ")";
                }
                else
                    text += " (" + "No Date" + ")";
                if (row["Memo"] != DBNull.Value)
                {
                    if (row["Memo"].ToString().Trim() != "")
                        text += " (M: " + row["Memo"].ToString().Trim() + ")";
                }
                return text;
            },
            TreeNodeValueGetter = row => JsonConvert.SerializeObject(new TreeNodeGroupModel(row)),
            Child = BatchLevel,
            ChildJoinCondition = row => $"GroupOfficeID_GroupID = '{row["GroupOfficeID_GroupID"]}'"
        };
        #endregion

        private void FillTreeView(DataSet dataSet,
            TreeLevelInfo levelInfo,
            string searchCondition,
            TreeNodeCollection treeNodeCollection,
            Func<TreeViewLevels, int, int, int, TreeNode, bool> isSelected)
        {
            treeNodeCollection.Clear();
            if (levelInfo.TableName == OrderLevel.TableName)
            {
                _totalNodesCount = 0;
            }

            var stateTargetModels = (List<StateTargetModel>)GetViewState(SessionConstants.StateTargetModels);

            foreach (var row in dataSet.Tables[levelInfo.TableName.ToString()].Select(searchCondition))
            {
                var iconIndex = Convert.ToString(row["IconIndex"]);
                var stateTargetModel = stateTargetModels.FirstOrDefault(model => model.IconIndex == iconIndex);
                var treeNode = new TreeNode(
                    levelInfo.TreeNodeTextGetter(row, this), levelInfo.TreeNodeValueGetter(row), stateTargetModel?.ImageUrl)
                {
                    ToolTip = GetNodeToolTip(stateTargetModel)
                };
                treeNode.Selected = isSelected(
                    levelInfo.TableName,
                    GetCodeColumnValue(row, "GroupCode"),
                    GetCodeColumnValue(row, "BatchCode"),
                    GetCodeColumnValue(row, "ItemCode"),
                    treeNode);

                if (_totalNodesCount + 1 > GetTreeViewTotalNodesLimit())
                {
                    return;
                }
                treeNodeCollection.Add(treeNode);
                _totalNodesCount++;


                if (levelInfo.Child != null)
                {
                    FillTreeView(dataSet, levelInfo.Child, levelInfo.ChildJoinCondition(row), treeNode.ChildNodes, isSelected);
                }

            }

        }

        private static string GetNodeToolTip(StateTargetModel model)
        {
            return $"{model.StateName} {model.TargetName}";
        }

        private static int GetCodeColumnValue(DataRow row, string columnName)
        {
            if (!row.Table.Columns.Contains(columnName))
                return 0;
            if (row[columnName] == DBNull.Value)
                return 0;
            return Convert.ToInt32(row[columnName]);
        }
    }
}