using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for TestRandom.
	/// </summary>
	public partial class TestRandom : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			SqlCommand command = new SqlCommand("ppGetRandomDecimal");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add("@Result", 1.2354);
			command.Parameters["@Result"].Direction = ParameterDirection.Output;

			command.Parameters.Add(new SqlParameter("@ItemCode", txtItemCode.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@BatchID", txtBatchID.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@ItemTypeID", txtItemTypeID.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@StartValue", txtMin.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@EndValue", txtMax.Text.Trim()));

			DataTable dt = new DataTable();
			dt.Columns.Add("Res");

			for(int i =0 ; i<int.Parse(txtAttempts.Text.Trim());i++)
			{
				DataRow dr = dt.NewRow();
				command.Connection.Open();
				command.ExecuteNonQuery();
				dr["Res"]=command.Parameters["@Result"].Value.ToString();
				dt.Rows.Add(dr);
				command.Connection.Close();
			}
			dgResult.DataSource=dt;
			dgResult.DataBind();
		}
	}
}
