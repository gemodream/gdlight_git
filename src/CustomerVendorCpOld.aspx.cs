using System;
using System.Data;
using System.Data.SqlClient;

namespace Corpt
{
	/// <summary>
	/// Summary description for CustomerVendorCpOld.
	/// </summary>
	public partial class CustomerVendorCpOld : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["ID"]==null)
				Response.Redirect("Login.aspx");
			if(!((Session["ID"].ToString()=="10")||(Session["ID"].ToString()=="12")||(Session["ID"].ToString()=="19")||(Session["ID"].ToString()=="17")||(Session["ID"].ToString()=="30")))
			{
				Response.Redirect("Middle.asx");
			}
			if(!Page.IsPostBack)
			{
				FillCustomerVendorList();
			}
			if(lstCustomer.SelectedIndex>-1)
			{
				cmdConnect.Enabled=true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void FillCustomerVendorList()
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				
			SqlCommand command = new SqlCommand("wspvvGetCustomerList");
			
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(dt);

			lstVendor.Items.Clear();
			lstVendor.DataSource=dt;
			lstVendor.DataValueField = "CustomerID";
			lstVendor.DataTextField = "CustomerName";
			lstVendor.DataBind();

			lstCustomer.Items.Clear();
			lstCustomer.DataSource=dt;
			lstCustomer.DataValueField = "CustomerID";
			lstCustomer.DataTextField = "CustomerName";
			lstCustomer.DataBind();
		}

		protected void lstVendor_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstVendor.SelectedIndex>-1)
			{
				//wspvvGetCustomerPrograms
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				
				SqlCommand command = new SqlCommand("wspvvGetCustomerPrograms");
			
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
				command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = int.Parse(lstVendor.SelectedValue);

				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);
				da.Fill(dt);

				cmdCustomerProgramList.DataSource=dt;
				cmdCustomerProgramList.DataValueField = "CustomerProgramName";
				cmdCustomerProgramList.DataTextField = "CustomerProgramName";
				cmdCustomerProgramList.DataBind();
			}
		}

		protected void cmdShowConnected_Click(object sender, System.EventArgs e)
		{
			if((lstVendor.SelectedIndex>-1)&&(lstCustomer.SelectedIndex>-1))
			{
				//wspvvGetCustomerVendorCP
				SqlCommand command = new SqlCommand("wspvvGetCustomerVendorCP");
			
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
				command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = int.Parse(lstCustomer.SelectedValue);
				command.Parameters.Add("@VendorID", SqlDbType.Int).Value = int.Parse(lstVendor.SelectedValue);

				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);
				da.Fill(dt);

				grdCpList.DataSource=dt;
				grdCpList.DataBind();

				grdCpList.Visible=true;
			}
			else
			{
				grdCpList.Visible=false;
			}
		}

		protected void lstCustomer_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cmdShowConnected_Click(sender,e);
		}

		protected void cmdCustomerProgramList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//wspvvGetCustomerVendorCPName
			if((lstVendor.SelectedIndex>-1)&&(lstCustomer.SelectedIndex>-1)&&(cmdCustomerProgramList.SelectedIndex>-1))
			{
				//wspvvGetCustomerVendorCP
				SqlCommand command = new SqlCommand("wspvvGetCustomerVendorCPName");
			
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
				command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = int.Parse(lstCustomer.SelectedValue);
				command.Parameters.Add("@VendorID", SqlDbType.Int).Value = int.Parse(lstVendor.SelectedValue);
				command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
				command.Parameters["@CustomerProgramName"].Size=255;
				command.Parameters["@CustomerProgramName"].Value=cmdCustomerProgramList.SelectedItem.Text;
				
				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);

				da.Fill(dt);

				if(dt.Rows.Count>0)
				{
					lblMessage.Text="Already Connected";
				}			
				else
				{
					lblMessage.Text="";
				}
			}
		}

		protected void cmdConnect_Click(object sender, System.EventArgs e)
		{
			//wspvvSetCustomerVendorCPName
			if((lstVendor.SelectedIndex>-1)&&(lstCustomer.SelectedIndex>-1)&&(cmdCustomerProgramList.SelectedIndex>-1))
			{
				SqlCommand command = new SqlCommand("wspvvSetCustomerVendorCPName");
			
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
				command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = int.Parse(lstCustomer.SelectedValue);
				command.Parameters.Add("@VendorID", SqlDbType.Int).Value = int.Parse(lstVendor.SelectedValue);
				command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
				command.Parameters["@CustomerProgramName"].Size=255;
				command.Parameters["@CustomerProgramName"].Value=cmdCustomerProgramList.SelectedItem.Text;

				try
				{
					conn.Open();
					command.ExecuteNonQuery();
					conn.Close();
					lblMessage.Text="Done: Connected.";
					cmdShowConnected_Click(sender,e);
				}
				catch(Exception ex)
				{
					lblMessage.Text="Error: " + ex.Message;
				}
			}
		}

		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Middle.aspx");
		}
	}
}
