﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="NewOrder.aspx.cs" Inherits="Corpt.NewOrder" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoheading">New order</div>
    <div class="navbar nav-tabs">
        <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-info" OnClick="OnSaveClick"/>
        <asp:Button runat="server" Text="Clear" CssClass="btn btn-info" OnClick="OnClearClick"/>
    </div>
    <table>
        <tr>
            <td><asp:Label runat="server" Text="Customer"></asp:Label></td>
            <td>
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="CustomerLikeButton">
                    <asp:TextBox runat="server" ID="CustomerLike" Style="width: 80%"></asp:TextBox>
                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                </asp:Panel>
            </td>
            <td style="width: 20px"></td>
            <td colspan="2">
                <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                    DataValueField="CustomerId" AutoPostBack="True" Style="font-family: Arial;
                    font-size: small;width: 100%" />
            </td>
        </tr>
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Order Memo"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="OrderField" ></asp:TextBox> </td>
            <td style="width: 20px"></td>
            <td><asp:Label ID="Label2" runat="server" Text="Number of Items"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="CountItemsField"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Memo Number"></asp:Label></td>
            <td>
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="addImageBUtton">
                    <asp:TextBox runat="server" ID="MemoNumberField" ></asp:TextBox>
                    <asp:ImageButton ID="addImageButton" runat="server" ToolTip="Add memo" ImageUrl="~/Images/ajaxImages/add.png"
                        OnClick="OnMemoAddClick" />
                </asp:Panel>
            </td>
            <td style="width: 20px"></td>
            <td><asp:Label ID="Label4" runat="server" Text="Memo File"></asp:Label></td>
            <td>
                <asp:Panel runat="server" ID="LoadXmlPanel" DefaultButton="LoadXmlButton">
                    <asp:TextBox runat="server" ID="MemoFileField"></asp:TextBox>
                    <asp:Button ID="LoadXmlButton" runat="server" OnClick="OnMemoAddFromFile" Text="Load" CssClass="btn btn-info" 
                    Style="margin-top: -10px; margin-left: 5px;"></asp:Button>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="vertical-align: top">
                <asp:Panel runat="server" Style="vertical-align: top">
                    <asp:ListBox runat="server" ID="MemoList" Rows="13" CssClass="control-list"
                        style="width: 88%;height: 100%"/>
                    <asp:ImageButton ID="DelImageButton" runat="server" ToolTip="Remove selected Memo" ImageUrl="~/Images/ajaxImages/del.png"
                        OnClick="OnMemoDelClick" Style="vertical-align: top" />
                </asp:Panel>
            </td>
            <td></td>
            <td style="vertical-align: top">Special <br/>Instructions</td>
            <td style="vertical-align: top">
                <asp:TextBox runat="server" ID="SpecIntrucField" 
                    Style="vertical-align: top; height: 100%; width: 100%" Wrap="True" 
                    TextMode="MultiLine" Rows="10"></asp:TextBox>
            </td>
        </tr>
    </table>

    <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
        TargetControlID="CountItemsField" FilterType="Numbers" />
</asp:Content>
