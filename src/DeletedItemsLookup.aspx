﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="DeletedItemsLookup.aspx.cs" Inherits="Corpt.DeletedItemsLookup" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
<style>
 .table tr:hover
            {
            background-color: #b8d1f3;    
            }
</style>

<div class="demoarea">
    <div class="demoheading">Deleted Order/Batch Lookup</div>
    <table>
        <tr>
            <td>By Order/Batch Number: </td>
            <td>
                <asp:Panel runat="server" DefaultButton="ByItemNumberBtn">
                    <asp:TextBox runat="server" ID="ItemNumberField"></asp:TextBox>
                    <asp:ImageButton ID="ByItemNumberBtn" runat="server" ToolTip="Search by Item Number" ImageUrl="~/Images/ajaxImages/search16.png"
                        OnClick="OnSearchByItemNumberClick" />
                </asp:Panel>
            </td>
            <td></td>
            <td style="padding-left: 20px"><asp:Label runat="server" ID="ErrLabel1" ForeColor="Red"></asp:Label></td>
            <td style="padding-left: 20px"><asp:Button runat="server" ID="ClearBtn" Text="CLEAR" ForeColor="Blue" OnClick="ClearBtn_Click"></asp:Button></td>
        </tr>
    </table>

    <!-- Error message-->
    <div class="control-group error">
        <asp:Label class="control-label" id="InfoLabel" runat="server" />
    </div>
        
    <!-- Item Numbers DataGrid-->
    <div style="font-size: small">
        <asp:DataGrid runat="server" ID="FileGrid"  AutoGenerateColumns="False" CellPadding="5">
            <Columns>
                <asp:BoundColumn HeaderText="ORDER" DataField="GroupCode">
                </asp:BoundColumn>
                <asp:BoundColumn HeaderText="BATCH" DataField="BatchCode">
                </asp:BoundColumn>
                <asp:BoundColumn HeaderText="ITEM" DataField="ItemCode">
                </asp:BoundColumn>
                <asp:BoundColumn HeaderText="EXPIRED DATE" DataField="ExpireDate">
                </asp:BoundColumn>
            </Columns>
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" Font-Size="Large" ForeColor="White" />
        </asp:DataGrid>
    </div>

    
    <div class="row" align="right">
        <asp:hyperlink id="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx" >Back</asp:hyperlink>
    </div>
</div>
</asp:Content>
