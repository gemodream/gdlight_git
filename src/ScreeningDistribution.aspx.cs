﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	public partial class ScreeningDistribution : System.Web.UI.Page
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");

			if (!IsPostBack)
			{
				LoadOrder();
				ClearForm();
			}
		}
	
		public void LoadOrder()
		{
			DataSet dsOrders= SyntheticScreeningUtils.GetSyntheticOrder(this);
			lstOrderIDs.DataTextField = "OrderCode";
			lstOrderIDs.DataValueField = "OrderCode";
			lstOrderIDs.DataSource = dsOrders.Tables[0];
			lstOrderIDs.DataBind();
			
			if (dsOrders.Tables[0].Rows.Count > 0)
			{
				lstOrderIDs_SelectedIndexChanged(null, null);
			}

		}
		
		public void LoadBatchStatus(int orderCode)
		{
			DataSet dsOrderBatchStatus = SyntheticScreeningUtils.GetSyntheticOrderBatchStatusByOrderCode(orderCode, this);

			DataTable dtOrderBatchStatus =  dsOrderBatchStatus.Tables[0];
			DataRow dr = null;
			DataColumn dc = new DataColumn("ID", typeof(System.Int16));
			dtOrderBatchStatus.Columns.Add(dc);
			int NewRowQty =Convert.ToInt16(0);
			for (int i = 0; i < NewRowQty; i++)
			{
				dr = dtOrderBatchStatus.NewRow();
				dr["ID"] = i.ToString();
				dtOrderBatchStatus.Rows.Add(dr);
			}
			gdvBatchQtyScreenerAllocation.DataSource = dtOrderBatchStatus;
			gdvBatchQtyScreenerAllocation.DataBind();
			if(dtOrderBatchStatus.Rows.Count>=1)
			{
				dvBatchAllocation.Visible = false;
			}
			else
			{
				dvBatchAllocation.Visible = true;
			}
			
		}
		#endregion

		#region SelectedIndex
		protected void lstOrderIDs_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lstOrderIDs.SelectedItem != null)
			{
				int orderCode = Convert.ToInt32(lstOrderIDs.SelectedItem.Value.ToString());
				DataSet dsOrders = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(orderCode, this);
				DataSet dsOrdersBags = new DataSet();
				if (dsOrders.Tables[0].Rows.Count>0)
				{
					//txtMemo.Text = dsOrders.Tables[0].Rows[0]["MemoNum"].ToString();
					txtTotalQty.Text = dsOrders.Tables[0].Rows[0]["TotalQty"].ToString();

					//txtSKUStyle.Text = dsOrders.Tables[0].Rows[0]["SKUName"].ToString() + " / " + dsOrders.Tables[0].Rows[0]["StyleName"].ToString();

                    if (dsOrders.Tables[0].Rows[0]["SKUName"].ToString() != "" && dsOrders.Tables[0].Rows[0]["StyleName"].ToString() != "")
                        txtSKUStyle.Text = dsOrders.Tables[0].Rows[0]["SKUName"].ToString() + " / " + dsOrders.Tables[0].Rows[0]["StyleName"].ToString();
                    else if (dsOrders.Tables[0].Rows[0]["SKUName"].ToString() != "")
                        txtSKUStyle.Text = dsOrders.Tables[0].Rows[0]["SKUName"].ToString();
                    else if (dsOrders.Tables[0].Rows[0]["StyleName"].ToString() != "")
                        txtSKUStyle.Text = dsOrders.Tables[0].Rows[0]["StyleName"].ToString();

                    txtOrderCode.Text = dsOrders.Tables[0].Rows[0]["OrderCode"].ToString(); ;
					txtRetailer.Text = dsOrders.Tables[0].Rows[0]["RetailerName"].ToString();
					txtJewelryType.Text= dsOrders.Tables[0].Rows[0]["JewelryTypeName"].ToString();
					//txtCustomer.Text = dsOrders.Tables[0].Rows[0]["CustomerName"].ToString();
					//txtPONumber.Text = dsOrders.Tables[0].Rows[0]["PONum"].ToString();
					//txtSKU.Text = ;

					LoadBatchStatus(orderCode);

					dsOrdersBags = SyntheticScreeningUtils.GetSyntheticOrderHistoryByOrderCode(orderCode, this);
				}
				else
				{
					ClearForm();
				}


			}
		}

		protected void drpScreeners_SelectedIndexChanged(object sender, EventArgs e)
		{
			DataTable dtUserID = new DataTable();
			dtUserID.Columns.Add("UserID", typeof(System.Int32));
			dtUserID.Columns.Add("StatusID", typeof(System.Int32));
			foreach (GridViewRow gvr in gdvBatchQtyScreenerAllocation.Rows)
			{
				var drpScreeners = (DropDownList)gvr.FindControl("drpScreeners");
				//var drpBatchStatus = (DropDownList)gvr.FindControl("drpBatchStatus");
				string UserID = drpScreeners.SelectedItem.Value;
				//string StatusID = drpBatchStatus.SelectedItem.Value;
				DataRow drnew = dtUserID.NewRow();
				if (UserID != "0" && drpScreeners.Enabled==true)
				{
					drnew["UserID"] = UserID;
					//drnew["StatusID"] = StatusID;
					dtUserID.Rows.Add(drnew);
				}

				var cnt = dtUserID.AsEnumerable().GroupBy(dr => dr.Field<int>("UserID")).Where(g => g.Count() > 1).Select(g => g.First()).ToList();
				if (cnt.Count >= 1)
				{
					drpScreeners.SelectedIndex = -1;
					PopupInfoDialog("User is already Assigned, Select another User", true);
					return;
				}
			}


		}
		#endregion

		#region GridEvent
		protected void gdvBatchQtyScreenerAllocation_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				
				//Load Batches 
				int OrderCode = Convert.ToInt32(lstOrderIDs.SelectedItem.Value.ToString());
				DataSet dsOrderBags = SyntheticScreeningUtils.GetSyntheticOrderBatchesByOrderCode(OrderCode, this);

				//Load Screening Instrument
				DataTable dtScreeningInstrument = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);
				var drpScreeningInstrument = (DropDownList)e.Row.FindControl("drpScreeningInstrument");
				drpScreeningInstrument.DataTextField = "InstrumentName";
				drpScreeningInstrument.DataValueField = "InstrumentCode";
				drpScreeningInstrument.DataSource = dtScreeningInstrument;
				drpScreeningInstrument.DataBind();
				ListItem liBlankInstrument = new ListItem();
				liBlankInstrument.Value = "0";
				liBlankInstrument.Text = "";
				drpScreeningInstrument.Items.Insert(0, liBlankInstrument);
				var lblScreeningInstrumentID = (Label)e.Row.FindControl("lblScreeningInstrumentID");
				if (lblScreeningInstrumentID.Text != "")
				{
					drpScreeningInstrument.Items.FindByValue(lblScreeningInstrumentID.Text).Selected = true;
				}
				//Load Screeners
				var drpScreeners = (DropDownList)e.Row.FindControl("drpScreeners");
				DataSet dtScreeners = SyntheticScreeningUtils.GetSyntheticUserStatus(this);
				drpScreeners.DataSource = dtScreeners;
				drpScreeners.DataBind();
				ListItem liBlank = new ListItem();
				liBlank.Value = "0";
				liBlank.Text = "";
				drpScreeners.Items.Insert(0, liBlank);

				Label lblAssignedTo = (Label)e.Row.FindControl("lblAssignedTo");
				if (lblAssignedTo.Text != "")
				{
					drpScreeners.Items.FindByValue(lblAssignedTo.Text).Selected = true;
				}
				var lblItemQty = (Label)e.Row.FindControl("lblItemQty");

				Label lblBatchCode = (Label)e.Row.FindControl("lblBatchCode");
				Label lblStatusID = (Label)e.Row.FindControl("lblStatusID");
				Label ScreeningInstrumentID = (Label)e.Row.FindControl("ScreeningInstrumentID");
				Button btnTask = (Button)e.Row.FindControl("btnTask");
				
				if (lblStatusID.Text != "")
				{
					if (Convert.ToInt32(lblStatusID.Text) == (int) SynhteticOrderStatus.BatchOpen)					{
						//load Screeners
						
						btnTask.Visible = true;
						drpScreeners.Enabled = true;
						drpScreeningInstrument.Enabled = true;
					}
					else
					{
						btnTask.Visible = false;
						drpScreeners.Enabled = false;
						drpScreeningInstrument.Enabled = false;
					}

				}
			}
		}

		protected void repScreener_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (string.Compare(e.CommandName, "UserID", false) == 0)
			{
				TextBox txtScrenerName = (TextBox)e.Item.FindControl("txtScrenerName");
				DropDownList drpScreeners = (DropDownList)e.Item.FindControl("drpScreeners");
				HiddenField hdnScreenerUserID = (HiddenField)e.Item.FindControl("hdnScreenerUserID");

				txtScrenerName.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
				string UserID = e.CommandArgument.ToString();
				hdnScreenerUserID.Value = UserID;

				drpScreeners.ClearSelection();
				drpScreeners.Items.FindByValue(UserID).Selected = true;
			}
		}

		protected void gdvBatchQtyScreenerAllocation_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "Task")
			{
				try
				{
					int rowIndex = Convert.ToInt32(e.CommandArgument);
					GridViewRow gvr = gdvBatchQtyScreenerAllocation.Rows[rowIndex];

					var lblBatchCode = (Label)gvr.FindControl("lblBatchCode");
					var drpScreeners = (DropDownList)gvr.FindControl("drpScreeners");
					var drpScreeningInstrument = (DropDownList)gvr.FindControl("drpScreeningInstrument");
					var lblItemQty = (Label)gvr.FindControl("lblItemQty");
					var btnTask = (Button)gvr.FindControl("btnTask");

					if (drpScreeners.SelectedItem.Value == "0")
					{
						PopupInfoDialog("Please Select Screener", true);
						return;
					}
					if (drpScreeningInstrument.SelectedItem.Value == "0")
					{
						PopupInfoDialog("Please Select Screening Instrument", true);
						return;
					}
					SyntheticBatchModel objBatch = new SyntheticBatchModel();
					objBatch.OrderCode = Convert.ToInt32(txtOrderCode.Text.ToString());
					objBatch.BatchCode = Convert.ToInt32(lblBatchCode.Text.ToString());
					objBatch.ScreenerID = Convert.ToInt32(drpScreeners.SelectedItem.Value);
					objBatch.StatusID = (int) SynhteticOrderStatus.ScreeningStart;
					objBatch.ScreeningInstrumentID= Convert.ToInt32(drpScreeningInstrument.SelectedItem.Value);
					objBatch.UpdateID = 1; // Allocate Screener
					bool msg = SyntheticScreeningUtils.SetSyntheticBatch(objBatch, this);
					///History Entry
					SyntheticOrderHistoryModel objSyntheticOrder=new SyntheticOrderHistoryModel();
					objSyntheticOrder.OrderCode = Convert.ToInt32(txtOrderCode.Text.ToString());
					objSyntheticOrder.BatchCode = Convert.ToInt32(lblBatchCode.Text.ToString());
					objSyntheticOrder.StatusId= (int) SynhteticOrderStatus.ScreeningStart;
					objSyntheticOrder.ItemQty = Convert.ToInt32(lblItemQty.Text.ToString().Trim());
					objSyntheticOrder.AssignedTo= Convert.ToInt32(drpScreeners.SelectedItem.Value);
					string historymsg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);

					drpScreeners.Enabled = false;
					btnTask.Visible = false;
					int orderCode = Convert.ToInt32(lstOrderIDs.SelectedItem.Value.ToString());
					LoadBatchStatus(orderCode);
					lblMsg.Text = "Task Allocated to User Successfully";
				}
				catch (Exception ex)
				{
					PopupInfoDialog(ex.Message.ToString(), true);
				}
			}
		}
		#endregion

		#region Popup Dialog

		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion

		#region Button Event

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				int orderCode = Convert.ToInt32(txtOrderCode.Text.ToString());
				DataSet dsOrdersHistory = SyntheticScreeningUtils.GetSyntheticOrderHistoryByOrderCode(orderCode, this);
				DataSet dsOrdersBags = SyntheticScreeningUtils.GetSyntheticOrderBatchesByOrderCode(orderCode, this);
				int bagsTotal = 0;
				int status = 0;
				string statusName = "";
				DataTable dtBatchQty = dsOrdersBags.Tables[0];
				dtBatchQty.Columns.Add("AssignedQty", typeof(System.Int32));
				foreach (GridViewRow gvr in gdvBatchQtyScreenerAllocation.Rows)
				{
					var drpOrderBatch = (DropDownList)gvr.FindControl("drpOrderBatch");
					int batchCode=Convert.ToInt32(drpOrderBatch.SelectedItem.Value);
					var txtQty = (TextBox)gvr.FindControl("txtQty");
					foreach (DataRow row in dtBatchQty.Rows)
					{
						if (row["BatchCode"].ToString() == batchCode.ToString())
						{
							row["AssignedQty"] = Convert.ToInt32(row["AssignedQty"].ToString()==""?"0": row["AssignedQty"].ToString()) + Convert.ToInt32(txtQty.Text);
						}
					}
					var drpBatchStatus = (DropDownList)gvr.FindControl("drpBatchStatus");
					int Qty = Convert.ToInt32(txtQty.Text == "" ? "0" : txtQty.Text);
					bagsTotal = bagsTotal + Convert.ToInt32(Qty);

					if (drpBatchStatus.SelectedIndex > 1)
					{
						//int PreviousIndex = drpBatchStatus.SelectedIndex - 1;//Get Last Status
						//int statusId = Convert.ToInt32(drpBatchStatus.Items[PreviousIndex].Value) + 1;
						int statusId = Convert.ToInt32(drpBatchStatus.SelectedItem.Value)-1;
						statusName = Convert.ToString(drpBatchStatus.SelectedItem.Text);
						status = ValidateCompleteStatus(dsOrdersHistory.Tables[0], statusId);
					}
				}
				foreach (DataRow row in dtBatchQty.Rows)
				{
					if (row["ItemQty"].ToString() != row["AssignedQty"].ToString())
					{
						string AssignedQty = row["AssignedQty"].ToString() == "" ? "0" : row["AssignedQty"].ToString();
						string msg = "BatchCode " + row["BatchCode"].ToString() + " has "+ AssignedQty + " Qty Allocated Out of "+ row["ItemQty"].ToString();
						PopupInfoDialog(msg, true);
						return;
					}
				}
				if (bagsTotal != Convert.ToInt32(txtTotalQty.Text.ToString()))
				{
					PopupInfoDialog("Total Quantity and Allocation Quantity not Match", true);
					return;
				}
				else if (status == 1)
				{
					PopupInfoDialog(statusName + " is Pending form User", true);
					return;
				}

				foreach (GridViewRow gvr in gdvBatchQtyScreenerAllocation.Rows)
				{
					var drpOrderBatch = (DropDownList)gvr.FindControl("drpOrderBatch");
					var drpScreeners = (DropDownList)gvr.FindControl("drpScreeners");
					var txtQty = (TextBox)gvr.FindControl("txtQty");
					var drpBatchStatus = (DropDownList)gvr.FindControl("drpBatchStatus");

					SyntheticOrderHistoryModel objSyntheticOrder = new SyntheticOrderHistoryModel();
					objSyntheticOrder.OrderCode = Convert.ToInt32(txtOrderCode.Text.ToString());
					objSyntheticOrder.AssignedTo = Convert.ToInt32(drpScreeners.SelectedItem.Value);
					objSyntheticOrder.StatusId = Convert.ToInt32(drpBatchStatus.SelectedItem.Value)+1; // Set to Next Status
					objSyntheticOrder.ItemQty = Convert.ToInt32(txtQty.Text);
					objSyntheticOrder.BatchCode = Convert.ToInt32(drpOrderBatch.SelectedItem.Value); 
					string msg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);
					drpScreeners.Enabled = false;
				}
				ClearForm();
				lblMsg.Text = "Allocation Successfully Inserted";

			}
			catch (Exception ex)
			{
				PopupInfoDialog(ex.Message.ToString(), true);
			}
		}
		
		private void ClearForm()
		{
			//txtMemo.Text = "";
			txtTotalQty.Text = "";
			txtSKUStyle.Text = "";
			txtOrderCode.Text = "";
			txtRetailer.Text = "";
			txtJewelryType.Text = "";
			//txtCustomer.Text = "";
			//txtPONumber.Text = "";
			//txtSKU.Text = "";

			lstOrderIDs.ClearSelection();
			gdvBatchQtyScreenerAllocation.DataSource = null;
			gdvBatchQtyScreenerAllocation.DataBind();
		}

		private int ValidateCompleteStatus(DataTable dtStatus, int statusId)
		{
			int statusID = 0;
			int totalQty = 0;
			int completeProcess = 0;
			foreach (DataRow row in dtStatus.Rows)
			{
				if (row["StatusID"].ToString() == "2")
				{
					totalQty += Convert.ToInt32(row["ItemQty"].ToString());
				}
				if (row["StatusID"].ToString() == statusId.ToString())
				{
					completeProcess += Convert.ToInt32(row["ItemQty"].ToString() == "" ? "0" : row["ItemQty"].ToString());
				}
			}
			if (totalQty != completeProcess)
			{
				statusID = 1;
			}
			return statusID;
		}
		#endregion

	}
}