﻿<%@ Page 
    Title="" 
    Language="C#" 
    MasterPageFile="~/DefaultMaster.Master" 
    AutoEventWireup="true" 
    CodeBehind="Middle.aspx.cs" 
    Inherits="Corpt.Middle" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>
    <style type="text/css">
        .table tr:hover
        {
            background-color: #b8d1f3;    
        }
        .styleLeft
        {
            width: 480px;
            text-align: left;
            vertical-align: top;
            border: thin solid silver;
            
        }
        .styleRight
        {
            width: 280px;
            text-align: left;
            vertical-align: sub;
            border: thin solid silver;
        }
        
    </style>
     <div class="demoarea" >
         <div class="demoheading" >Main Menu</div>
         <table style="font-family: 'MS Sans Serif'; font-size: small;">
             <!-- Server Name -->
             <tr>
                 <td colspan="2" style="border-width: 0px; text-align: left; font-weight: bold;">
                     <asp:Label ID="MyServerName" runat="server" Visible="True" Enabled="True"></asp:Label>
                 </td>
             </tr>
             <!-- RELOAD -->
             <tr>
                 <td colspan="2" style="border-width: 0px; text-align: left; font-weight: bold;">
                     <asp:HyperLink ID="lnkRelogin" runat="server" Visible="False" Enabled="False" NavigateUrl="Login.aspx">Could not find Access Rights for you, please try to re-login.</asp:HyperLink>
                 </td>
             </tr>
        </table>
        <table>
            <tr>
                <td style="padding-left: 5px;">
                </td>
                <td style="padding-left: 5px">
                     <asp:ImageButton ID="ClarityButton" runat="server" 
                        ImageUrl="~/Images/menuImages/clarity.jpg" 
                        PostBackUrl="~/Clarity2.aspx"  />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="ColorButton" runat="server" 
                        ImageUrl="~/Images/menuImages/color.jpg" PostBackUrl="~/Color2.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="MeasureButton" runat="server" ImageUrl="~/Images/menuImages/measure.jpg" PostBackUrl="~/Measure2.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="RemeasureButton" runat="server" ImageUrl="~/Images/menuImages/remeasure.jpg" PostBackUrl="~/Remeasure.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="AccountRepButton" runat="server" ImageUrl="~/Images/menuImages/accountRep.jpg" PostBackUrl="~/AccountRep.aspx" />
                </td>
            </tr>
            <tr style="padding-top: 5px">
                <td style="padding-left: 5px">
                   
                </td>
                <td style="padding-left: 50px">
                    &nbsp;</td>
            </tr>
            <tr style="padding-top: 5px">
                <td style="padding-left: 5px">
                    &nbsp;</td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="ItemiznButton" runat="server" ImageUrl="~/Images/menuImages/itemizn.jpg" PostBackUrl="~/Itemize1.aspx"/>
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="ReitemiznButton" runat="server" ImageUrl="~/Images/menuImages/reitemizn.jpg"  
                     PostBackUrl="~/ReItemizn.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="CpButton" runat="server" 
                        ImageUrl="~/Images/menuImages/customerProgram.jpg" PostBackUrl="~/CustomerProgramNew.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="CustomerButton" runat="server" ImageUrl="~/Images/menuImages/customer.jpg" PostBackUrl="~/CustomerEdit1.aspx" />
                </td>
                <td style="padding-left: 5px">
                    <asp:ImageButton ID="FrontButton" runat="server" ImageUrl="~/Images/menuImages/front.jpg" PostBackUrl="~/ScreeningFront.aspx" />
                </td>
            </tr>
        </table>
        <br/>
        <table style="font-family: 'MS Sans Serif'; font-size: small;width: 800px"  >
        
             <!-- Full Order -->
             <tr>
                 <td class="styleLeft">
                     Full Report
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink7" CssClass="btn-link" runat="server" Visible="True" Enabled="True" NavigateUrl="ShortReportII.aspx"
                         BorderStyle="None">Full Order</asp:HyperLink>
                 </td>
             </tr>
            
             <!-- Report Lookup -->
             <tr>
                 <td class="styleLeft">
                     To see previously printed report:<br>
                     Note: might not work with reports printed before March 3, 2006
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink1" CssClass="btn-link" runat="server" Visible="False" Enabled="False" NavigateUrl="ReportLookup.aspx"
                         BorderStyle="None">Report Lookup</asp:HyperLink>
                 </td>
             </tr>
             <!-- CPResult -->
             <tr>
                 <td class="styleLeft">
                     To check which items did not pass Customer Program requirements.
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink2" runat="server" Visible="False" Enabled="False" NavigateUrl="CPResult.aspx">CP Rules</asp:HyperLink>
                 </td>
             </tr>
             <!-- Short Report -->
             <tr>
                 <td class="styleLeft">
                     Online Short Report
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkShortReport" runat="server" Visible="False" Enabled="False"
                         NavigateUrl="ShortReport.aspx" >Short Report</asp:HyperLink>
                 </td>
             </tr>
             <!-- Lot Number-->
             <tr>
                 <td class="styleLeft">
                     To find an item by lot number
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink3" runat="server" Visible="False" Enabled="False" NavigateUrl="LotNumber.aspx"
                         >Find by Lot Number</asp:HyperLink>
                 </td>
             </tr>
             <!-- Memo Lookup-->
             <tr>
                 <td class="styleLeft">
                     To find orders and batches by a memo number<br />
                     Note: might not work for an older memo/order numbers
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink4" runat="server" Visible="False" Enabled="False" NavigateUrl="MemoLookup.aspx"
                         > Find by Memo Number</asp:HyperLink>
                 </td>
             </tr>
             <!-- Printed Not -->
             <tr>
                 <td class="styleLeft">
                     To see the list of all the items in a given order<br />To check which documents have
                     been ordered<br />To check if ordered documents have been printed
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink5" runat="server" Visible="False" Enabled="False" NavigateUrl="PrintedNot.aspx"
                         >Reports in an Order</asp:HyperLink>
                 </td>
             </tr>
             <!-- CP Overview -->
             <tr>
                 <td class="styleLeft">
                     Customer Program overview
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink6" runat="server" Visible="False" Enabled="False" NavigateUrl="CPRulesDisplay2.aspx"
                         >CP Overview</asp:HyperLink>
                 </td>
             </tr>
             <!--Report-Customer -->
             <tr>
                 <td class="styleLeft">
                     Report Ownership Information
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkReportCustomer" runat="server" Visible="False" Enabled="False"
                         NavigateUrl="BelongsTo.aspx" >Report-Customer</asp:HyperLink>
                 </td>
             </tr>
             <!--Billing Info -->
             <tr>
                 <td class="styleLeft">
                     Billing Info
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkBillingInfo" runat="server" Visible="False" Enabled="False"
                         NavigateUrl="BillingInfo.aspx" >Billing Info</asp:HyperLink>
                 </td>
             </tr>
             <!--GDLight -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkGDLight" runat="server" Visible="False" Enabled="False" NavigateUrl="GDLight.aspx" 
                         >GDLight</asp:HyperLink>
                 </td>
             </tr>
             <!-- Batch list -->
             <tr>
                 <td class="styleLeft">
                     Open batch list
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkOpenBatches" runat="server" Visible="False" Enabled="False"
                         NavigateUrl="ListOpenBatches.aspx" >OpenBatchList</asp:HyperLink>
                 </td>
             </tr>
             <!-- Sterling Stats -->
             <tr>
                 <td class="styleLeft">
                     Sterling Stats
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkStat" runat="server" Visible="False" Enabled="False" NavigateUrl="PrvStat.aspx">Stat</asp:HyperLink>
                 </td>
             </tr>
             <!-- Virtual Admin-->
             <tr>
                 <td class="styleLeft">
                     Virtual Vault Administration
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="cmdVVAdmin" runat="server" Visible="False" Enabled="False" NavigateUrl="VVAdmin1.aspx">VVAdmin</asp:HyperLink>
                 </td>
             </tr>
             <!-- Change PAssword 
<%--             <tr>--%>
<%--                 <td class="styleLeft">--%>
<%--                 </td>--%>
<%--                 <td class="styleRight">--%>
<%--                     <asp:HyperLink ID="lnkChangePassword" runat="server" Visible="False" Enabled="False"--%>
<%--                         NavigateUrl="ChangeUserPassword.aspx" >Change Password</asp:HyperLink>--%>
<%--                 </td>--%>
<%--             </tr>--%>
              Customer/Vendor -->
             <tr>
                 <td class="styleLeft">
                     Customer/Vendor Customer Programs
                 </td>
                 <td class="styleRight">
                     <a href="CustomerVendorCP.aspx"></a>
                     <asp:HyperLink ID="lnkCustomerVendorCP" runat="server" Visible="False" Enabled="False"
                         NavigateUrl="CustomerVendorCP.aspx" >Customer/Vendor</asp:HyperLink>
                 </td>
             </tr>
             <!-- Post to FM -->
             <tr>
                 <td class="styleLeft">
                     Post images for FM
                 </td>
                 <td class="styleRight">
                     <asp:LinkButton ID="cmdPostFM" runat="server" Visible="False" Enabled="False" >Post to FM</asp:LinkButton>
                 </td>
             </tr>
             <!-- Photospex -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkPhotospex" runat="server" Visible="False" Enabled="False">Photospex</asp:HyperLink>
                 </td>
             </tr>
             <!-- Kill Item -->
             <tr>
                 <td class="styleLeft">
                     KIll Item
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkKillItem" runat="server" Visible="False" Enabled="False" NavigateUrl="KillItem.aspx">Kill Item</asp:HyperLink>
                 </td>
             </tr>
             <!-- Open Orders -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkOpenOrders" Visible="False" Enabled="False" runat="server">Open Orders</asp:HyperLink>
                 </td>
             </tr>
             <!-- Tracking -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkTracking" Visible="False" Enabled="False" NavigateUrl="Tracking.aspx"
                         runat="server">Tracking</asp:HyperLink><br />
                     <asp:HyperLink ID="lnkTracking2" Visible="False" Enabled="False" NavigateUrl="Tracking2.aspx"
                         runat="server">Tracking II</asp:HyperLink>
                 </td>
             </tr>
             <!-- Get Invoice Number-->
             <tr>
                 <td class="styleLeft">
                     Get Invoice number by Order Number
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkInvoiceNumber" NavigateUrl="GetInvoiceNumberByOrderNumber.aspx"
                         runat="server">Get Invoice Number</asp:HyperLink>
                 </td>
             </tr>
             <!-- Mark Laser -->
             <tr>
                 <td class="styleLeft">
                     Mark LASER inscription
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkLAser" NavigateUrl="MarkLaser.aspx" runat="server">Mark LASER</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                 <td class="styleLeft">
                     Bulk Update New (AutoMeasure)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink20" NavigateUrl="BulkUpdateAsyncNew.aspx" runat="server">Bulk Update New (AutoMeasure)</asp:HyperLink>
                 </td>
             </tr>
			<!-- Synthetic Screening -->
            <%--<tr>
                 <td class="styleLeft">
                 Synthetic Screening data entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="lnkSyntheticScreening" Visible="True" Enabled="True" NavigateUrl="SyntheticScreening.aspx" runat="server">Synthetic Screening</asp:HyperLink>
                 </td>
             </tr>--%>
            <!-- Synthetic Screening -->
            <%--<tr>
                 <td class="styleLeft">
                 Synthetic Front Page
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink8" Visible="True" Enabled="True" NavigateUrl="ScreeningFrontEntries.aspx" runat="server">Synthetic Front Entries</asp:HyperLink>
                 </td>
             </tr>--%>
            <tr>
                 <td class="styleLeft">
                 Customer Program (Old)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink21" Visible="True" Enabled="True" NavigateUrl="CustomerProgramNew.aspx" runat="server">Customer Program (Old)</asp:HyperLink>
                 </td>
             </tr>
             <%--<tr>
                 <td class="styleLeft">
                 Synthetic Front Entries (New)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink19" Visible="True" Enabled="True" NavigateUrl="ScreeningFrontEntriesNew.aspx" runat="server">Synthetic Front Entries (New)</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                 <td class="styleLeft">
                 Synthetic Screening New Entries
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink9" Visible="True" Enabled="True" NavigateUrl="SyntheticNewEntries.aspx" runat="server">Synthetic Screening New Entries</asp:HyperLink>
                 </td>
             </tr>--%>
            <!--
            <tr>
                 <td class="styleLeft">
                 Synthetic Screening Customer Data Entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="LnkSyntheticCustomer" Visible="True" Enabled="True" NavigateUrl="SyntheticCustomerEntry.aspx" runat="server">Synthetic Screening Customer</asp:HyperLink>
                 </td>
             </tr>
                -->
            <tr>
                 <td class="styleLeft">
                 Screening Statistics
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="LnkSyntheticShortStats" Visible="True" Enabled="True" NavigateUrl="SyntheticShortStats.aspx" runat="server">Screening Statistics</asp:HyperLink>
                 </td>
             </tr>
            <!--
            <tr>
                 <td class="styleLeft">
                 Synthetic Screening Data Entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="LnkSyntheticDataStatistics" Visible="True" Enabled="True" NavigateUrl="SyntheticDataStatistics.aspx" runat="server">Synthetic Data Statistics</asp:HyperLink>
                 </td>
             </tr>
                -->
             <%--<tr>
                 <td class="styleLeft">
                 Synthetic Screening Data Entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="LnkSyntheticStatistics" Visible="True" Enabled="True" NavigateUrl="SyntheticStatistics.aspx" runat="server">Synthetic Statistics</asp:HyperLink>
                 </td>
             </tr>--%>
            
             <!-- Delivery Log -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink Style="z-index: 0" ID="lnkDeliveryLog" Visible="True" Enabled="True"
                         NavigateUrl="DeliveryLog.aspx" runat="server">Delivery Log</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                 <td class="styleLeft">
                Measure (New)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink15" Visible="True" Enabled="True" NavigateUrl="Measure2.aspx" runat="server">Measure (New)</asp:HyperLink>
                 </td>
             </tr>
            <%--<tr>
                 <td class="styleLeft">
                 Synthetic Screening (IND) data entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink10" Visible="True" Enabled="True" NavigateUrl="SyntheticScreeningIND.aspx" runat="server">Synthetic Screening (IND)</asp:HyperLink>
                 </td>
             </tr>--%>

             <tr>
                 <td class="styleLeft">
                 Additional Services
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink11" Visible="True" Enabled="True" NavigateUrl="AdditionalServices.aspx" runat="server">Additional Services</asp:HyperLink>
                 </td>
             </tr>

             <tr>
                 <td class="styleLeft">
                 Quality Control
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink12" Visible="True" Enabled="True" NavigateUrl="QualityControl.aspx" runat="server">Quality Control</asp:HyperLink>
                 </td>
             </tr>

             <tr>
                 <td class="styleLeft">
                 Tracking Order
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink13" Visible="True" Enabled="True" NavigateUrl="TrackingOrder.aspx" runat="server">Tracking Order</asp:HyperLink>
                 </td>
             </tr>
             <tr>
                 <td class="styleLeft">
                 Bulk Item Data Entry
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink14" Visible="True" Enabled="True" NavigateUrl="BulkItemDataEntry.aspx" runat="server">Bulk Item Data Entry</asp:HyperLink>
                 </td>
             </tr>

            <tr>
                 <td class="styleLeft">
                 Customer Edit (New)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink16" Visible="True" Enabled="True" NavigateUrl="CustomerEdit1.aspx" runat="server">Customer Edit (New)</asp:HyperLink>
                 </td>
             </tr>
			    <tr>
                 <td class="styleLeft">
                 Front (New)
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink18" Visible="True" Enabled="True" NavigateUrl="FrontNew.aspx" runat="server">Front (New)</asp:HyperLink>
                 </td>
             </tr>
             <tr>
                 <td class="styleLeft">
                 Notify Billing
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink17" Visible="True" Enabled="True" NavigateUrl="NotifyBilling.aspx" runat="server">Notify Billing</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                 <td class="styleLeft">
                 Color Stones
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink22" Visible="True" Enabled="True" NavigateUrl="ColorStones.aspx" runat="server">Color Stones(test)</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                 <td class="styleLeft">
                 Bulk Grading
                 </td>
                 <td class="styleRight">
                     <asp:HyperLink ID="HyperLink23" Visible="True" Enabled="True" NavigateUrl="ExpressGrading1.aspx" runat="server">Express Grading</asp:HyperLink>
                 </td>
             </tr>
            <tr>
                <td class="styleLeft">Everledger URL Generator
                </td>
                <td class="styleRight">
                    <asp:HyperLink ID="HyperLink8" Visible="True" Enabled="True" NavigateUrl="EverledgerURLGenerator.aspx" runat="server">Everledger URL Generator</asp:HyperLink>
                </td>
            </tr>
              <!-- LogOff -->
             <tr>
                 <td class="styleLeft">
                 </td>
                 <td class="styleRight">
                     <asp:LinkButton ID="LinkButton2" runat="server"  OnClick="LinkButton2Click">LogOff</asp:LinkButton>
                 </td>
             </tr>
           
        </table>
        <table style="font-family: 'MS Sans Serif'; font-size: small;">
             <tr>
                 <td colspan="2" style="text-align: left">
                     <asp:Label Style="z-index: 0" ID="lblDbInfo" runat="server"></asp:Label>
                 </td>
             </tr>
         </table>
     </div>
</asp:Content>
