﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ListOpenBatches.aspx.cs" Inherits="Corpt.ListOpenBatches" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ToolkitScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        table.mylist input {
            width: 20px;
            display: block;
            float: left;
        }

        table.mylist label {
            display: block;
            float: left;
        }
    </style>

    <script type="text/javascript">

        $().ready(function () {

            $("#<%=btnSearch.ClientID%>").click(function () {

                if ($('#<%=ddlCustomerList.ClientID%>').val().trim() == "" && $('#<%=ddlOfficeList.ClientID%>').val().trim() == "" && $('#<%=ddlInvoiceStatus.ClientID%>').val().trim() == "" && $('#<%=calFrom.ClientID%>').val().trim() == "" && $('#<%=calTo.ClientID%>').val().trim() == "") {
                    alert('Please select atleast one filter.');
                    $('#<%=ddlCustomerList.ClientID%>').focus();
                    return false;
                }
                else if (($('#<%=calFrom.ClientID%>').val().trim() != "" && $('#<%=calTo.ClientID%>').val().trim() == "") || ($('#<%=calFrom.ClientID%>').val().trim() == "" && $('#<%=calTo.ClientID%>').val().trim() != "")) {
                     alert('Please select date range for filter.');
                     $('#<%=calFrom.ClientID%>').focus();
                     return false;
                }
                else {
                    return true;
                }                
            });
           

         });

    </script>


    <div class="demoarea">
<%--        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
            </Triggers>
            <ContentTemplate>--%>
                <div class="demoheading">Batch List</div>
                <div style="border-bottom: solid; border-bottom-width: 1px; height: 45px;">
                    <p align="left">
                        <img src="Images/green_circle.jpg" alt="green" />&nbsp; Invoiced - Invoice in QuickBooks&nbsp;
                <img src="Images/yellow_circle.jpg" alt="yellow" />&nbsp; Invoiced - Invoice in Tally&nbsp;
                <img src="Images/red_circle.jpg" alt="red" />&nbsp; Not invoiced
                    </p>
                </div>

                <div class="container" style="margin-left: 0px; padding-left: 25px;">
                    <div class="row">
                        <div style="font-size: small; width: 100%;padding-top: 10px;">                            
                            <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; width: 530px;">
                                <legend style="width: 100px; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;" class="label">Filter Criteria</legend>
                                                            
                                <div style="padding-top: 10px;">
                                    <asp:Label ID="Label1" runat="server" Text="Customer:" Width="80px"></asp:Label>

                                    <asp:DropDownList ID="ddlCustomerList" runat="server" DataTextField="CustomerName" CssClass="form-control input-sm"
                                        AutoPostBack="false" DataValueField="CustomerId" ToolTip="Customers List" Width="298px" />
                                    <asp:TextBox runat="server" ID="CustomerLike" Width="100px" CssClass="form-control input-sm"
                                        Style="display: inline-block;"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/ajaxImages/search.png" Style="margin-left: 0px; display: inline-block; vertical-align: middle;"
                                        OnClick="OnCustomerSearchClick" />
                                </div>

                                <div>

                                    <asp:Label ID="Label2" runat="server" Text="Office Name:" Width="80px"></asp:Label>
                                    <asp:DropDownList ID="ddlOfficeList" runat="server" CssClass="form-control input-sm"
                                        AutoPostBack="false" DataTextField="OfficeCode" DataValueField="OfficeId" ToolTip="Office List" Width="100px" />

                                    &nbsp&nbsp&nbsp&nbsp<asp:Label ID="Label3" runat="server" Text="Invoice Status:" Width="80px"></asp:Label>
                                    <asp:DropDownList ID="ddlInvoiceStatus" runat="server" CssClass="form-control input-sm"
                                        AutoPostBack="false" DataTextField="Status" DataValueField="Status" ToolTip="Invoice Status List" Width="100px" />
                                </div>

                                <div >
                                    <asp:Label ID="Label4" runat="server" Text="Date Range:" Width="80px"></asp:Label>
                                  <%--  <asp:Panel ID="Panel1" runat="server" CssClass="form-inline " Width="400px">--%>
                                       
                                        <asp:Label ID="Label6" runat="server" Text="From" ></asp:Label>
                                        <asp:TextBox runat="server" ID="calFrom" Width="100px" CssClass="form-control input-sm"/>
                                        <asp:ImageButton runat="server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                            AlternateText="Click to show calendar" />                                       
                                         <asp:Label ID="Label7" runat="server" Text="To" style="padding-left: 5px;" ></asp:Label>
                                        <asp:TextBox runat="server" ID="calTo" Width="100px" CssClass="form-control input-sm"/>
                                        <asp:ImageButton runat="server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                            AlternateText="Click to show calendar" />
                                   <%-- </asp:Panel>--%>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                                        PopupButtonID="Image1" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                                        PopupButtonID="Image2" />

                                   <%-- <div style="float: right;">
                                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="50">
                                            <ProgressTemplate>
                                                &nbsp;&nbsp;<img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                                                <b>Please wait...</b>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </div>--%>
                                </div>

                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Select:" Width="80px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rblAllSelect" runat="server" RepeatDirection="Horizontal" Width="200px" CellPadding="0" CellSpacing="0" CssClass="mylist" AutoPostBack="True" OnSelectedIndexChanged="rblAllSelect_SelectedIndexChanged">
                                                    <asp:ListItem Value="2" Text="Filter Criteria" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Select All"></asp:ListItem>

                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="float: right;margin-left: 120px;">
                                                <asp:Button runat="server" ID="btnClear" Text="Reset" CssClass="btn btn-info" OnClick="btnClear_Click" />
                                                <asp:Button runat="server" ID="btnSearch" OnClick="OnSearchClick" Text="Search" CssClass="btn btn-info" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                               
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <p align="left" style="padding-top: 14px; padding-left: 0px;">
                            Total number of items:
                            <asp:Label ID="InfoLabel" runat="server" CssClass="control-label"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;Sum (Number of items):
                            <asp:Label ID="ClientSum" runat="server" CssClass="control-label"></asp:Label>
                             <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg" ToolTip="Download to Excel"
                                ImageAlign="AbsMiddle" Style="margin-left: 10px;" OnClick="btnExportToExcel_Click" />
                        </p>
                        <div style="font-size: small;">

                            <asp:GridView runat="server" ID="gvOpenBatches" AutoGenerateColumns="False" CellPadding="5" ShowHeaderWhenEmpty="True" EmptyDataText="No records Found">
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                
                                <Columns>
                                    <asp:BoundField DataField="DisplayOrder" HeaderText="Order Number"></asp:BoundField>
                                    <asp:BoundField DataField="DisplayBatch" HeaderText="Batch Number"></asp:BoundField>
                                    <asp:BoundField DataField="QBInvoiceNumber" HeaderText="QB Invoice Number"></asp:BoundField >
                                    <asp:BoundField DataField="NumberOfItems" HeaderText="Number Of Items"></asp:BoundField>
                                    <asp:BoundField DataField="Customer" HeaderText="Customer"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerOfficeName" HeaderText="Customer Office"></asp:BoundField>
                                    <asp:BoundField DataField="OrderDate" HeaderText="Order Date" ItemStyle-Width="150px"></asp:BoundField>
                                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>                                    
                                    <asp:TemplateField>
                                         <ItemTemplate>
                                            <asp:Image ID="imgStatus" ImageUrl='<%# Eval("IconStatus") %>' runat="server" ImageAlign="Middle" Width="20px" Height="20px" Border="0"/>
                                         </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                 <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                            </asp:GridView>
                        </div>

                    </div>
                </div>

         <%--   </ContentTemplate>

        </asp:UpdatePanel>--%>

    </div>
</asp:Content>
