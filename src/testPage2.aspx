﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="testPage2.aspx.cs" Inherits="Corpt.testPage2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
     <style type="text/css">

.GridviewScrollHeader TH, .GridviewScrollHeader TD 
{ 
    padding: 5px; 
    font-weight: bold; 
    white-space: nowrap; 
    border-right: 1px solid #AAAAAA; 
    border-bottom: 1px solid #AAAAAA; 
    background-color: #EFEFEF; 
    text-align: left; 
    vertical-align: bottom; 
} 
.GridviewScrollItem TD 
{ 
    padding: 5px; 
    white-space: nowrap; 
    border-right: 1px solid #AAAAAA; 
    border-bottom: 1px solid #AAAAAA; 
    background-color: #FFFFFF; 
} 
.GridviewScrollPager  
{ 
    border-top: 1px solid #AAAAAA; 
    background-color: #FFFFFF; 
} 
.GridviewScrollPager TD 
{ 
    padding-top: 3px; 
    font-size: 14px; 
    padding-left: 5px; 
    padding-right: 5px; 
} 
.GridviewScrollPager A 
{ 
    color: #666666; 
}

.GridviewScrollPager SPAN

{

    font-size: 16px;

    font-weight: bold;

}



    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
    <script src="Style/bootstrap2.0/js/gridviewScroll.min.js" type="text/javascript"></script>
  <!--  <script type="text/javascript">

        $(window).resize(function () {
            gridviewScrollPercent()
        });
        $(document).ready(function () {
            gridviewScrollPercent()
        
        });
        function gridviewScrollPercent() {
            var widthGrid = $('#gridContainer').width();
            var heightGrid = $('#gridContainer').height();
            $('#<%=dgResult.ClientID%>').gridviewScroll({
                width: widthGrid,
                height: heightGrid,
                freezesize: 1
            });
        }
        function gridviewScroll() {
            $('#<%=dgResult.ClientID%>').gridviewScroll({
                width: 660,
                height: 200,
                freezesize: 1
            });
        } 
</script> -->
<div id ="gridContainer" style="width: 90%">
      <asp:DataGrid ID="dgResult" runat="server" Width="100%" 
            AutoGenerateColumns="true" GridLines="None" 
          onselectedindexchanged="dgResult_SelectedIndexChanged" >
            <HeaderStyle CssClass="GridviewScrollHeader" /> 
            <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
            <PagerStyle CssClass="GridviewScrollPager" />
            <Columns>
            <asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
            </Columns> 
          </asp:DataGrid>
</div>
      <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" Visible ="false"
            AutoGenerateColumns="true" GridLines="None" >
            <HeaderStyle CssClass="GridviewScrollHeader" /> 
            <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
            <PagerStyle CssClass="GridviewScrollPager" /> 
          </asp:DataGrid>

</asp:Content>
