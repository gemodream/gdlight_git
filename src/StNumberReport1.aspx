﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="StNumberReport1.aspx.cs" Inherits="Corpt.StNumberReport1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>

    <div class="demoarea">
        <div class="demoheading">STMA Number Combined Report</div>
        <!-- Filter Panel -->
        <table>
            <!-- Date From, Date To -->
            <tr>
                <td>
                    <b>From:</b><br />
                    <asp:TextBox runat="server" ID="calFrom" />
                    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                        AlternateText="Click to show calendar" /><br />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                        PopupButtonID="Image1" />
                </td>
                <td>
                    <b>To:</b><br/>
                    <asp:TextBox runat="server" ID="calTo" />
                    <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                        AlternateText="Click to show calendar" /><br />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                        PopupButtonID="Image2" />
                </td>
            </tr>
            <!-- Customer List, Memo List-->
            <tr>
                <td colspan="2">
                    <b>Customers:</b><br/>
                    <asp:DropDownList ID="lstCustomerList" runat="server" Width="100%" 
                        onselectedindexchanged="OnCustomerSelectedChanged" 
                        DataTextField="CustomerName" DataValueField="CustomerOfficeID_CustomerID" 
                        AutoPostBack="True" />
                    <ajaxToolkit:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="lstCustomerList" Enabled="True"
                        PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains" QueryTimeout="2000" >
                    </ajaxToolkit:ListSearchExtender>
                </td>
                <td style="width: 140px">
                    <b>Memos:</b><br/>
                    <asp:DropDownList ID="lstMemos" runat="server" Width="100%" 
                        DataTextField="MemoNumber" DataValueField="MemoNumber" 
                        onselectedindexchanged="OnMemoSelectedChanged" AutoPostBack="True"/>
                    <ajaxToolkit:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="lstMemos" Enabled="True"
                        PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains" QueryTimeout="2000">
                    </ajaxToolkit:ListSearchExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <!-- Radio List Batch Numbers -->
                    <br/>
                    <asp:Panel ToolTip="Batch Numbers" ID="Panel1" runat="server" Width="95%" Height="120px"
                        CssClass="radio" BorderColor="Silver" BorderWidth="1px" ScrollBars="Vertical"
                        Wrap="True">
                        <asp:RadioButtonList ID="cblBatches" runat="server" 
                            RepeatDirection="Horizontal" RepeatLayout="Table" Width="100%"
                            AutoPostBack="True" 
                            OnSelectedIndexChanged="CblBatchesSelectedIndexChanged" CellPadding="1"
                            CellSpacing="1" RepeatColumns="2" DataTextField="BatchNumber" 
                            DataValueField="BatchID">
                        </asp:RadioButtonList>
                    </asp:Panel>
                </td>
                <!-- List Box Selected Batch Numbers -->
                <td style="margin: 0px; padding-left: 10px;">
                    <br/>
                    <asp:Panel ID="Panel2" ToolTip="Selected Batch numbers" runat="server" Width="220px"
                        Height="120px">
                        <asp:ListBox ID="lstLastBatchList" runat="server" Height="120px" padding="0" 
                            DataTextField="BatchNumber" DataValueField="BatchID"></asp:ListBox>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
