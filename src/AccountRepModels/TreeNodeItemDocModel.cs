﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.AccountRepModels
{
    [Serializable]
    public class TreeNodeItemDocModel
    {
        public TreeNodeItemDocModel()
        {
        }

        public TreeNodeItemDocModel(DataRow row)
        {
            GroupCode = row.ConvertToInt("GroupCode");
            BatchCode = row.ConvertToInt("BatchCode");
        }

        public int GroupCode { get; set; }

        public int BatchCode { get; set; }
    }
}