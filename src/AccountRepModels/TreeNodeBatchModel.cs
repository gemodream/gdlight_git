﻿using System;
using System.Data;
using Corpt.Models.AccountRepresentative.UI;
using GemoDream.Database;

namespace Corpt.AccountRepModels
{
    [Serializable]
    public class TreeNodeBatchModel : BatchShortModel
    {
        public TreeNodeBatchModel()
        {
        }

        public TreeNodeBatchModel(DataRow row)
        {
            BatchId = row.ConvertToInt("BatchID");
            GroupCode = row.ConvertToInt("GroupCode");
            BatchCode = row.ConvertToInt("BatchCode");
            MemoNumber = row.ConvertToString("MemoNumber");
            ItemsQuantity = row.ConvertToInt("ItemsQuantity");
            IconIndex = row.ConvertToInt("IconIndex");
            GroupOfficeIdGroupId = row.ConvertToString("GroupOfficeID_GroupID");
            Path2Picture = row.ConvertToString("Path2Picture");
        }

        public string Path2Picture { get; set; }
        public string GroupOfficeIdGroupId { get; set; }
        public int IconIndex { get; set; }
        public string MemoNumber { get; set; }
    }
}