﻿using System;
using System.Data;

namespace Corpt.AccountRepModels
{
    public class TreeLevelInfo
    {
        public AccountRep.TreeViewLevels TableName { get; set; }
        public Func<DataRow, AccountRep, string> TreeNodeTextGetter { get; set; }
        public Func<DataRow, string> TreeNodeValueGetter { get; set; }
        public TreeLevelInfo Child { get; set; }
        public Func<DataRow, string> ChildJoinCondition { get; set; }
    }
}