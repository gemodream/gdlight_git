﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.AccountRepModels
{
    [Serializable]
    public class TreeNodeItemModel
    {
        public TreeNodeItemModel()
        {
        }

        public TreeNodeItemModel(DataRow row)
        {
            BatchId = row.ConvertToInt("BatchID");
            GroupCode = row.ConvertToInt("GroupCode");
            BatchCode = row.ConvertToInt("BatchCode");
            ItemCode = row.ConvertToInt("ItemCode");
            NewBatchId = row.ConvertToInt("NewBatchID");
            NewItemCode = row.ConvertToInt("NewItemCode");

            PrevGroupCode = row.ConvertToInt("PrevGroupCode");
            PrevBatchCode = row.ConvertToInt("PrevBatchCode");
            PrevItemCode  = row.ConvertToInt("PrevItemCode");
            Shape = row.ConvertToInt("Shape");
            ItemTypeId = row.ConvertToInt("ItemTypeID");
            Path2Drawing = row.ConvertToString("Path2Drawing");
            LotNumber = row.ConvertToString("LotNumber");
            ItemComment = row.ConvertToString("ItemComment");

        }
        public string LotNumber { get; set; }

        public string ItemComment { get; set; }

        public string Path2Drawing { get; set; }

        public int ItemTypeId { get; set; }

        public int Shape { get; set; }

        public int PrevGroupCode { get; set; }

        public int PrevItemCode { get; set; }

        public int PrevBatchCode { get; set; }

        public int BatchCode { get; set; }

        public int GroupCode { get; set; }

        public int NewItemCode { get; set; }

        public int NewBatchId { get; set; }

        public int ItemCode { get; set; }

        public int BatchId { get; set; }
    }
}