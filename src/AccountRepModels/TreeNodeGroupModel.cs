﻿using System;
using System.Data;
using GemoDream.Database;

namespace Corpt.AccountRepModels
{
    [Serializable]
    public class TreeNodeGroupModel
    {
        public TreeNodeGroupModel()
        {
        }

        public TreeNodeGroupModel(DataRow row)
        {
            GroupId = row.ConvertToInt("GroupID");
            GroupCode = row.ConvertToInt("GroupCode");
            CreateDate = row.ConvertToDateTime("CreateDate");
            Memo = row.ConvertToString("Memo");
            GroupOfficeIdGroupId = row.ConvertToString("GroupOfficeID_GroupID");
            IconIndex = row.ConvertToInt("IconIndex");
            CustomerCode = row.ConvertToInt("CustomerCode");
            CustomerId = row.ConvertToInt("CustomerID");
            CustomerOfficeId = row.ConvertToInt("CustomerOfficeID");
            ServiceTypeId = row.ConvertToIntNullable("ServiceTypeID");
            RetailerId   = row.ConvertToInt("RetailerId");
            RetailerName = row.ConvertToString("RetailerName");
            SynthServiceTypeCode = row.ConvertToInt("SynthServiceTypeCode");
            SynthServiceTypeName = row.ConvertToString("SynthServiceTypeName");
            SynthTotalQty = row.ConvertToInt("SynthTotalQty");
            AuthorId = row.ConvertToInt("AuthorID");
            AuthorOfficeId = row.ConvertToInt("AuthorOfficeID");
            HasCustomerRequest = row.ConvertToBool("HasCustomerRequest");
            var decimalNullable = row.ConvertToDecimalNullable("NumberOfUnit"); //NUMERIC(14, 4)
            NumberOfUnit = decimalNullable == null ? null : (int?)decimalNullable;
            QCLab = row.ConvertToString("QC_LAB");
        }

        public string QCLab { get; set; }

        public bool HasCustomerRequest { get; set; }

        public int? NumberOfUnit { get; set; }

        public int AuthorOfficeId { get; set; }

        public int AuthorId { get; set; }

        public int SynthTotalQty { get; set; }

        public string SynthServiceTypeName { get; set; }

        public int? SynthServiceTypeCode { get; set; }

        public int CustomerOfficeId { get; set; }

        public int CustomerId { get; set; }

        public int? ServiceTypeId { get; set; }

        public int CustomerCode { get; set; }

        public int IconIndex { get; set; }

        public string GroupOfficeIdGroupId { get; set; }

        public string Memo { get; set; }

        public DateTime CreateDate { get; set; }

        public int GroupCode { get; set; }

        public int GroupId { get; set; }

        public int RetailerId { get; set; }

        public string RetailerName { get; set; }
    }
}