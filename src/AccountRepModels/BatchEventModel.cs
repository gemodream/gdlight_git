﻿using Corpt.Utilities;

namespace Corpt.AccountRepModels
{
    public class BatchEventModel
    {
        public int EventId => FrontUtils.BatchEvents.EndSession;
        public int FormId => FrontUtils.Codes.AccRep;
        public int BatchId { get; set; }
        public int ItemsAffected { get; set; }
        public int ItemsInBatch { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public int GroupCode { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public int BatchCode { get; set; }
    }
}