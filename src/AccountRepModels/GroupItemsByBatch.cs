﻿namespace Corpt.AccountRepModels
{
    public class GroupItemsByBatch
    {
        public GroupItemsByBatch(int newBatchId, int count)
        {
            NewBatchId = newBatchId;
            Count = count;
        }
        public int NewBatchId { get; }
        public int Count { get; }

    }
}