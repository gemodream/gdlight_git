﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Corpt.AccountRepModels
{
    static class TreeViewExtension
    {
        public static List<TreeNode> GetNodes(this TreeView treeView, int depth)
        {
            var nodes = new List<TreeNode>();
            GetNodes(treeView.Nodes, depth, nodes);
            return nodes;
        }

        private static void GetNodes(TreeNodeCollection treeViewNodes, int depth, List<TreeNode> result)
        {
            foreach (TreeNode treeNode in treeViewNodes)
            {
                if (treeNode.Depth == depth)
                {
                    result.Add(treeNode);
                }
                else if (treeNode.Depth < depth)
                {
                    GetNodes(treeNode.ChildNodes, depth, result);
                }
            }
        }
    }
}