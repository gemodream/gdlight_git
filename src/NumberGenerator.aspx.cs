﻿using System;
using System.Collections.Generic;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class NumberGenerator : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Number Generator";
            if (!IsPostBack)
            {
                NumberItems.Focus();
            }
        }

        protected void OnGenerateClick(object sender, EventArgs e)
        {
            Page.Validate("CountGroup");
            if (!Page.IsValid) return;
            var numbers = QueryUtils.NumberGenerator(PrefixField.Text.Trim(), Convert.ToInt32(NumberItems.Text), this);
            SetViewState(numbers, SessionConstants.NumberGeneratorList);
            dgrNumbers.DataSource = numbers;
            dgrNumbers.DataBind();
        }

        protected void OnDownloadClick(object sender, EventArgs e)
        {
            InfoLabel.Text = "";
            var numbers = GetViewState(SessionConstants.NumberGeneratorList) as List<LabelModel>;
            if (numbers == null || numbers.Count == 0) return;
            ExcelUtils.LabelsFileDownLoad1(numbers, this);
            //InfoLabel.Text = ExcelUtils.LabelsFileDownLoad(numbers, SessionConstants.GlobalItemizedLabel, this);
        }
    }
}