﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class TrackingBatchHistory : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var batchId = Request.Params["p1"];
            var trackings = QueryUtils.GetTrackingBatchHistory(batchId, this);
            SetViewState(trackings, SessionConstants.TrackBatchHistory);
            if (trackings.FindAll(m => m.IsCheckedOut).Count > 0)
            {
                ShowCarrierInfo(batchId);
            } else
            {
                CarrierInfoLabel.Text = "";
            }
            filterLabel.Text = "Tracking History, BatchId = " + batchId;
            ApplyCurrentSort();
        }
        private void ApplyCurrentSort()
        {
            var trackings = GetViewState(SessionConstants.TrackBatchHistory) as List<TrackingBatchHistoryModel> ??
              new List<TrackingBatchHistoryModel>();

            var table = new DataTable("TrackingBatchHistory");
            table.Columns.Add(new DataColumn { ColumnName = "Date", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Time", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Stage", DataType = typeof(string) });
            foreach (var item in trackings)
            {
                table.Rows.Add(new object[] { item.Date, item.Time, item.Stage});
            }
            table.AcceptChanges();

            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "Date";
                SetViewState(new SortingModel { ColumnName = "Date", Direction = true }, SessionConstants.CurrentSortModel);
            }

            // Rebind data 
            grdBatches.DataSource = dv;
            grdBatches.DataBind();
        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            ApplyCurrentSort();
        }
        private void ShowCarrierInfo(string batchId)
        {
            var carrierModel = QueryUtils.GetCarrierInfoByBatch(batchId, this);
            if (carrierModel == null)
            {
                CarrierInfoLabel.Text = "";
            }
            else
            {
                CarrierInfoLabel.Text = "<b>Delivery: </b>Sent out with " + carrierModel.CarrierDisplay;
            }
        }

    }
}