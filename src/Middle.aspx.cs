﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Corpt
{
    public partial class Middle : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Main Menu";
			// New Permission System
			MyServerName.Text = "";// "Server " + Session["OfficeIPGroup"];

            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);

#if DEBUG
            lblDbInfo.Text = conn.Database + " : " + conn.DataSource;
#else
			lblDbInfo.Text="";
#endif
            var command = new SqlCommand("sp_GetWebPermissions");

            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@UserID", "" + Session["ID"]);

            var dtPermission = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dtPermission);
            if (dtPermission.Rows.Count == 0)
            {   // no access
                lnkRelogin.Visible = true;
                lnkRelogin.Enabled = true;
            }
            else
            {
                //enable links 
                lnkRelogin.Visible = false;
                lnkRelogin.Enabled = false;

                // Report Lookup
                var b = dtPermission.Select("LinkID = 1").Length > 0;
                HyperLink1.Enabled = b;
                HyperLink1.Visible = b;

                // CP Rules
                b = dtPermission.Select("LinkID = 2").Length > 0;
                HyperLink2.Enabled = b;
                HyperLink2.Visible = b;

                // Short Report
                b = dtPermission.Select("LinkID = 3").Length > 0;
                lnkShortReport.Enabled = b;
                lnkShortReport.Visible = b;

                // Find by Lot Number
                b = dtPermission.Select("LinkID = 4").Length > 0;
                HyperLink3.Enabled = b;
                HyperLink3.Visible = b;

                // Find by Memo Number
                b = dtPermission.Select("LinkID = 5").Length > 0;
                HyperLink4.Enabled = b;
                HyperLink4.Visible = b;

                // Reports in an Order
                b = dtPermission.Select("LinkID = 6").Length > 0;
                HyperLink5.Enabled = b;
                HyperLink5.Visible = b;

                // CP Overview
                b = dtPermission.Select("LinkID = 7").Length > 0;
                HyperLink6.Enabled = b;
                HyperLink6.Visible = b;

                //Report-Customer
                b = dtPermission.Select("LinkID = 8").Length > 0;
                lnkReportCustomer.Enabled = b;
                lnkReportCustomer.Visible = b;

                // Billing Info
                b = dtPermission.Select("LinkID = 9").Length > 0;
                lnkBillingInfo.Enabled = b;
                lnkBillingInfo.Visible = b;

                // GDLight
                //b = dtPermission.Select("LinkID = 10").Length > 0;
                //lnkGDLight.Enabled = b;
                //lnkGDLight.Visible = b;

                // OpenBatchList
                b = dtPermission.Select("LinkID = 11").Length > 0;
                lnkOpenBatches.Enabled = b;
                lnkOpenBatches.Visible = b;

                // Stat
                b = dtPermission.Select("LinkID = 12").Length > 0;
                lnkStat.Enabled = b;
                lnkStat.Visible = b;

                // VVAdmin
                b = dtPermission.Select("LinkID = 13").Length > 0;
                cmdVVAdmin.Enabled = b;
                cmdVVAdmin.Visible = b;

                // Change Password
//                b = dtPermission.Select("LinkID = 14").Length > 0;
//                lnkChangePassword.Enabled = b;
//                lnkChangePassword.Visible = b;

                // CustomerVendorCP
                b = dtPermission.Select("LinkID = 15").Length > 0;
                lnkCustomerVendorCP.Enabled = b;
                lnkCustomerVendorCP.Visible = b;

                // Post FM
                b = dtPermission.Select("LinkID = 16").Length > 0;
                cmdPostFM.Enabled = b;
                cmdPostFM.Visible = b;

                // PhotoSpex 17
                b = dtPermission.Select("LinkID = 17").Length > 0;
                lnkPhotospex.Enabled = b;
                lnkPhotospex.Visible = b;
                if (b)
                {
                    lnkPhotospex.NavigateUrl = "PSX.aspx";
                }

                // OpenOrders
                b = dtPermission.Select("LinkID = 18").Length > 0;
                lnkOpenOrders.Visible = b;
                lnkOpenOrders.Enabled = b;

                if (b)
                {
                    lnkOpenOrders.NavigateUrl = "OpenOrders.aspx";
                }

                // Tracking
                b = dtPermission.Select("LinkID = 19").Length > 0;
                lnkTracking.Visible = b;
                lnkTracking.Enabled = b;

                // TrackingII
                b = dtPermission.Select("LinkID = 20").Length > 0;
                lnkTracking2.Visible = b;
                lnkTracking2.Enabled = b;

            }

        }

        protected void CmdVvAdminClick(object sender, EventArgs e)
        {
            Response.Redirect("VVAdmin.aspx");
        }

        protected void LinkButton2Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

    }
}