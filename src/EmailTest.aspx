﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="EmailTest.aspx.cs" Inherits="Corpt.EmailTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div class="demoheading">
        Email Test
    </div>
    <table>
        <tr>
            <td style="width: 100px">Settings for:</td>
            <td colspan="5" style="padding: 5px">
                <asp:RadioButtonList runat="server" ID="SettingFld"  OnSelectedIndexChanged="OnSettingModeChanges" Width="300px"
                    RepeatDirection="Horizontal" CssClass="radio" AutoPostBack="True">
                    <asp:ListItem Value="VvUser" Text="VV User" />
                    <asp:ListItem Value="ShortRpt" Text="Short Report" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="Host:"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="Host" Text="smtp.gmail.com" Width="200px"></asp:TextBox></td>
            <td><asp:Label runat="server" Text="Port: " Style="text-align: right"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="Port" Text="587"></asp:TextBox></td>
            
            <td><asp:CheckBox ID="UseDefault" Width="170px" runat="server" Checked="False" CssClass="checkbox" Style="padding-left: 15px;margin-left: 10px" Text="UseDefaultCredentials"/></td>
            <td><asp:CheckBox ID="EnableSsl" runat="server" Checked="True" CssClass="checkbox" Width="100px" Style="padding-left: 5px" Text="EnableSsl"/></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="From address: "/></td>
            <td><asp:TextBox runat="server" ID="FromAddress" Text="nataleb059@gmail.com" Width="200px"></asp:TextBox></td>
            <td> <asp:Label ID="Label2" runat="server" Text="From password:" ></asp:Label></td>
            <td> <asp:TextBox runat="server" ID="FromPassword" TextMode="Password"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="To address: "></asp:Label></td>
            <td> <asp:TextBox runat="server" ID="ToAddress" Text="natalebo@mail.ru" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="Subject:"></asp:Label></td>
            <td colspan="5" ><asp:TextBox runat="server" ID="Subject" Width="100%" Text="Message Subject"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="vertical-align: top"><asp:Label ID="Label4" runat="server" Text="Body:"></asp:Label></td>
            <td colspan="5" >
                <div id="BodyContent" runat="server" style="border: silver solid 1px"></div>
            </td>
        </tr>
    </table>
    <div class="navbar nav-tabs">
        <asp:Button ID="SendButton" runat="server" Text="Send Email" CssClass="btn btn-info" OnClick="OnSendClick" ToolTip="Send email"/>
        <asp:Label runat="server" ID="sendResult" ForeColor="Blue" Style="padding-left: 20px;"></asp:Label>
    </div>
    
   
</asp:Content>
