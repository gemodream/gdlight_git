﻿using System.Collections;
using System.Web.UI;

namespace Corpt
{
    public class CommonPage : Page
    {
        public interface IContentPlaceHolders
        {
            IList GetContentPlaceHolders();
        }
        public virtual string GetPageUniqueKey()
        {
            return "";
        }
        public void SetSessionData(string mapname, object value)
        {
            Session[mapname + GetPageUniqueKey()] = value;
        }
        public object GetSessionData(string mapname)
        {
            return Session[mapname + GetPageUniqueKey()];
        }
        #region Set & Get ViewState Parameters
        public void SetViewState(object value, string param)
        {
            ViewState[param] = value;
        }
        public object GetViewState(string param)
        {
            return ViewState[param];
        }
        #endregion

        #region Set & Get SessionState Parameters
        public void SetSessionState(object value, string param)
        {
            Session[param] = value;
        }

        public object GetSessionState(string param)
        {
            return Session[param];
        }
        #endregion


    }

}