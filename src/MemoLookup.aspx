﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="MemoLookup.aspx.cs" Inherits="Corpt.MemoLookup" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">
         <div class="demoheading">Find by Memo Number</div>
         <!-- Filter Panel -->
         <asp:Panel runat="server" DefaultButton="ActivateButton" CssClass="form-inline">
             <asp:TextBox type="text" ID="MemoNumber" MaxLength="100" placeholder="Memo Number"
                 runat="server" Style="font-weight: bold; width: 120px;" />
             <label class="checkbox" style="margin-left: 20px">
                 <asp:CheckBox ID="chkStrick" type="checkbox" runat="server" />
                 Strict Search</label>
             <asp:Button ID="ActivateButton" Style="margin-left: 20px" runat="server" CssClass="btn btn-primary"
                 Text="Lookup" OnClick="ActivateButtonClick"></asp:Button>
         </asp:Panel>
         <br/>
        <asp:RequiredFieldValidator runat="server" ID="VReq"
            ControlToValidate="MemoNumber"
            Display="None"
            
             ErrorMessage="<b>Required Field Missing</b><br />A Memo Number is required." 
             ValidationGroup="MemoGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="VReqE"
            TargetControlID="VReq"
            PopupPosition="BottomRight"
            HighlightCssClass="validatorCalloutHighlight"/>

        <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
            HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" Height="600px" Width="500px"
            TransitionDuration="250" AutoSize="Limit" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
           <Panes>
               
            <ajaxToolkit:AccordionPane ID="AccordionPaneOrdersHst" runat="server" Visible="false">
                <Header><asp:Label runat="server" ID="LblGrid1" Text="History Orders"/></Header>
                <Content>
                    <asp:DataGrid ID="tblOrderHst" runat="server" Style="font-size: small;" CellPadding="5"
                        BackColor="White" ForeColor="#555555" EnableViewState="False" GridLines="Both">
                        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" >
                        </HeaderStyle>
                    </asp:DataGrid>
                </Content>
            </ajaxToolkit:AccordionPane>

            <ajaxToolkit:AccordionPane ID="AccordionPaneOrders" runat="server" Visible="false">
                <Header><asp:Label runat="server" ID="LblGrid2" Text="Orders"/></Header>
                <Content>
                    <asp:DataGrid ID="tblOrder" runat="server" Style="font-size: small;" CellPadding="5" BackColor="White"
                        ForeColor="#555555" EnableViewState="False" GridLines="Both" >
                        <ItemStyle ForeColor="#555555" ></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White">
                        </HeaderStyle>
                    </asp:DataGrid>
                </Content>
            </ajaxToolkit:AccordionPane>

            <ajaxToolkit:AccordionPane ID="AccordionPaneBatchs" runat="server" Visible="False">
                <Header><asp:Label runat="server" ID="LblGrid3" Text="Batches"/></Header>
                    <Content>
                        <asp:DataGrid ID="tblBatch" runat="server" Style="font-size: small;" CellPadding="5" BackColor="White"
                            ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
                            EnableViewState="False" GridLines="Both" >
                            <ItemStyle ForeColor="#555555"></ItemStyle>
                            <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White">
                            </HeaderStyle>
                        </asp:DataGrid>
                    </Content>
                </ajaxToolkit:AccordionPane>
            </Panes>
        </ajaxToolkit:Accordion>
        

         <!-- Back link -->
         <div class="row" align="right">
             <asp:HyperLink ID="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx">Back</asp:HyperLink>
         </div>
     </div>
</asp:Content>
