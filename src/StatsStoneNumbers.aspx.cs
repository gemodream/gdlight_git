﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpt.Models.Stats;
using Corpt.Utilities;

namespace Corpt
{
    public partial class StatsStoneNumbers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            parentOfficeId.DataSource = QueryUtils.GetCountryOffices(this);
            parentOfficeId.DataBind();
            parentOfficeId.SelectedIndex = 0;

            SetFilterFromRequest();
            var p_page = Request.Params[StatsFilterModel.ParPage] ?? "";
            if (!string.IsNullOrEmpty(p_page))
            {
                var pageNo = Convert.ToInt32(p_page);
                PageNumberLoad(pageNo);
           }
        }
        protected void OnLookupClick(object sender, EventArgs e)
        {
            PageNumberLoad(1);
        }
        private void PageNumberLoad(int pageNo)
        {
            var filterModel = GetFilterFromPage();
            if (!filterModel.Valid) return;
            filterModel.PageNo = pageNo;

            int cnt = 0;
            var data = QueryUtils.GetStatsStoneNumber(filterModel, out cnt, this);
            GridView1.DataSource = data;
            GridView1.DataBind();
            if (cnt > 0)
            {
                var myPages = PaginatorUtils.GenerateStoneNumbersPages(cnt, pageNo, filterModel);
                var htmlPages = "";
                foreach (var page in myPages)
                {
                    htmlPages += page.PageUrl;
                }
                pagesTop.InnerHtml = htmlPages;
                pagesBotton.InnerHtml = htmlPages;
                var format = "Showing {0} to {1} of {2} entries";
                countLabel.Text = string.Format(format, 
                    (pageNo - 1) * PaginatorUtils.StoneNumbersCountOnPage + 1, 
                    pageNo * PaginatorUtils.StoneNumbersCountOnPage, cnt);
            }
            else
            {
                pagesTop.InnerHtml = "";
                pagesBotton.InnerHtml = "";
                countLabel.Text = "";
            }
        }
        #region Filter Panel
        private StatsFilterModel GetFilterFromPage()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime) dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.PeriodType = periodType.SelectedValue;
            filterModel.OfficeId = parentOfficeId.SelectedValue;
            return filterModel;
        }
        private void SetFilterFromRequest()
        {
            //-- Period Type
            var p_periodType = Request.Params[StatsFilterModel.ParPeriodType] ?? "";
            if (string.IsNullOrEmpty(p_periodType)){
                periodType.SelectedValue = PeriodWeekly;
                OnChangedPeriodType(null, null);
                return;
            }
            periodType.SelectedValue = p_periodType;

            //-- DateFrom, DateTo
            calFrom.Text = Request.Params[StatsFilterModel.ParDateFrom] ?? "";
            calTo.Text = Request.Params[StatsFilterModel.ParDateTo] ?? "";

            //-- Parent Office Id
            parentOfficeId.SelectedValue = Request.Params[StatsFilterModel.ParOfficeId] ?? "";
        }
        #endregion

        #region Period Type
        private static string PeriodMonthly = "m";
        private static string PeriodWeekly = "w";
        private static string PeriodRandom = "r";
        protected void OnChangedPeriodType(object sender, EventArgs e)
        {
            var type = periodType.SelectedValue;
            var isRandom = (type == PeriodRandom);
            calFrom.Enabled = isRandom;
            calTo.Enabled = isRandom;
            if (isRandom) return;
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var isWeekly = (type == PeriodWeekly);
            var dateFrom = PaginatorUtils.ConvertDateToString(isWeekly ? today.AddDays(-7) : today.AddMonths(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;
        }
        #endregion
    }
}