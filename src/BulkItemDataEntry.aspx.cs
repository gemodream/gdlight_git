using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using ExcelDataReader;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Corpt
{
    /// <summary>
    /// Summary description for BulkUpdate.
    /// </summary>
    public partial class BulkItemDataEntry : CommonPage
    {
		#region Page Laod
		protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null) Response.Redirect("Login.aspx");

            if (string.IsNullOrEmpty(UniqKeyFld.Text))
            {
                UniqKeyFld.Text = "" + DateTime.Now.Ticks;
            }

            Page.Title = "GSI: Item Data Bulk Update";

            if (!IsPostBack)
            {
                SetSessionData(SessionConstants.GradeMeasureControls, null);
                LoadCustomers();
            }
            //RenderControls();
        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
                
		#endregion

		#region Bulk Upload

		protected void btnUploadDataFile_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;
            string pathName = string.Empty;
            string extension = string.Empty;
            DataSet dsXls;

            //string folderPath = Server.MapPath(@"~\ExcelFiles\");
            if (fuDataFile.HasFile)
            {
                fileName = Path.GetFileName(fuDataFile.PostedFile.FileName);
                extension = Path.GetExtension(fuDataFile.PostedFile.FileName);
                switch (extension.ToLower())
                {
                    case ".xlsx":
                        break;
                    default:
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Unfortunately the selected file type is not currently supported, sorry...');", true);
                        return;
                }

                Stream fileStream = fuDataFile.PostedFile.InputStream;
                var excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                };

                dsXls = excelReader.AsDataSet(conf);
                excelReader.Close();
                StripEmptyRows(dsXls.Tables[0]);

                if (dsXls == null || dsXls.Tables.Count == 0 || dsXls.Tables[0].Rows.Count == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Records not found. Try Again ...');", true);
                    return;
                }
                else
                {
                    SetSessionState(dsXls, "ExcelData");
					OnLoadData(dsXls.Tables[0]);
                }
            }
            else
            {
                extension = Path.GetExtension(fuDataFile.PostedFile.FileName);
                if (extension == "")
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please select file...);", true);
                else if (extension != ".xlsx")
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Unfortunately the selected file type is not currently supported, sorry...');", true);
            }
        }
        public void OnLoadData(System.Data.DataTable dtExcel)
        {
            try
            {
                if (dtExcel.Rows.Count != 0)
                {
                    System.Data.DataTable dtData = new System.Data.DataTable();
					dtData.Columns.Add("ID", typeof(Int32));
					dtData.Columns.Add("ItemNumber", typeof(System.String));
					dtData.Columns.Add("ItemTitle", typeof(System.String));
					dtData.Columns.Add("ItemValue", typeof(System.String));

					DataTable dtItemStatus = new DataTable();
					dtItemStatus.Columns.Add("ItemNumber", typeof(string));
					dtItemStatus.Columns.Add("CellTitle", typeof(string));
					dtItemStatus.Columns.Add("CellValue", typeof(string));

					string ItemNumberList = String.Join(",", dtExcel.AsEnumerable().Select(row => row["ItemNumber"].ToString()));
                    List<SingleItemModel> item = QueryUtils.GetItemByCodeList(ItemNumberList, this.Page);
                    List<SingleItemModel> itemList = new List<SingleItemModel>();
					int k = 1;
                    int l = 0;
					for (int i = 0; i < dtExcel.Rows.Count; i++)
                    {
                        //ListItem ltsItem = new ListItem();
                        if (item != null)
                        {
                            string ItemNumber = dtExcel.Rows[i]["ItemNumber"].ToString();
                            var itemfound = item.Find(x => x.FullItemNumber == ItemNumber);
                            var itemOldfound = item.Find(x => x.FullOldItemNumber == ItemNumber);
                            if (itemfound == null && itemOldfound == null)
                            {
								DataRow dr = dtItemStatus.NewRow();
								dr["ItemNumber"] = ItemNumber;
                                dr["CellTitle"] = "";
								dr["CellValue"] = "ItemNumber is invalid";
								dtItemStatus.Rows.Add(dr);
							}
                            else if (itemfound == null && itemOldfound != null)
                            {
								DataRow dr = dtItemStatus.NewRow();
								dr["ItemNumber"] = itemOldfound.FullOldItemNumber;
								dr["CellTitle"] = "";
								dr["CellValue"] = "Unable to process old number. Use new number " + itemOldfound.FullItemNumber.ToString();
								dtItemStatus.Rows.Add(dr);
							}
                            else
                            {
                                lstItemList.Items.Add(ItemNumber);
                                itemList.Add(itemfound);
								for (int j = 0; j <= dtExcel.Columns.Count - 1; j++)
								{
									var cellTitle = dtExcel.Columns[j].ColumnName;
									var cellValue = dtExcel.Rows[i][j].ToString();

                                    if (cellValue.Length > 2000)
                                    {
                                        if (cellTitle == "IC_Description" || cellTitle == "IC_External Comment")
                                        {
                                            DataRow dr = dtItemStatus.NewRow();
                                            dr["ItemNumber"] = ItemNumber;
                                            dr["CellTitle"] = cellTitle;
                                            dr["CellValue"] = "String exceeds 2000 chars";
                                            dtItemStatus.Rows.Add(dr);
                                            l++;
                                        }
                                    }
                                    else if (cellValue != "" && cellTitle != "ItemNumber" && cellTitle != "SrNo" && cellTitle != "CustomerCode")
                                    {
                                        DataRow dr = dtData.NewRow();
                                        dr["ID"] = k;
                                        dr["ItemNumber"] = ItemNumber;
                                        dr["ItemTitle"] = cellTitle;
                                        dr["ItemValue"] = cellValue;
                                        dtData.Rows.Add(dr);
                                        k++;
                                    }
								}
                                gvItemStatus.DataSource = dtItemStatus;
                                gvItemStatus.DataBind();
							}
                        }
                    }
                    if (l == 0)
                        SetSessionState(dtData, "itemTitleValue");
                    else
                    {
                        //btnBulkInsert.Enabled = false;
						lblMsg.Text = "Bulk upload error,please update file and upload again.";
						lblMsg.ForeColor = Color.Red;
					}
				}
            }
            catch(Exception ex)
            {
				Console.WriteLine(ex.Message);
			}
		}
        protected void btnBulkInsert_Click(object sender, EventArgs e)
        {
            try
            {
                string msg = string.Empty;
                DateTime dtstart = DateTime.Now;
                lstItemStatus.Items.Clear();
                var itemTitleValue = GetSessionState("itemTitleValue") as DataTable;

				DataTable dtItemStatus = new DataTable();
				dtItemStatus.Columns.Add("ItemNumber", typeof(string));
				dtItemStatus.Columns.Add("CellTitle", typeof(string));
				dtItemStatus.Columns.Add("CellValue", typeof(string));

				if (itemTitleValue != null)
                {
     
                    if (lstItemStatus.Items.Count != 0)
                    {
                        msg = "Excel data file error";
                    }
                    else
                    {
                        DataTable resultTable = QueryUtils.SaveMeasuresBulkUpdate(itemTitleValue, this);

                        // Check if the DataTable has data
                        int i = 0;
                        if (resultTable != null && resultTable.Rows.Count > 0)
                        {                           
                            foreach (DataRow dr in resultTable.Rows)
                            {

								DataRow drnew = dtItemStatus.NewRow();
								drnew["ItemNumber"] = dr["ItemNumber"].ToString();
								drnew["CellTitle"] = dr["ItemTitle"].ToString();
								drnew["CellValue"] = dr["ItemValue"].ToString();
								dtItemStatus.Rows.Add(drnew);

								i++;
							}
                            if (i != 0)
                                msg = "Bulk upload error,please update file and upload again.";
							//msg = "Bulk upload successful but with some item errors listed";

							gvItemStatus.DataSource = dtItemStatus;
							gvItemStatus.DataBind();
						}
                    }
                }
                else 
                {
                    msg = "No record found";
                }

				Session["itemTitleValue"] = null;
				TimeSpan timeDiff = DateTime.Now - dtstart;

                if (msg != "")
                {
                    lblMsg.Text = msg;
                    lblMsg.ForeColor = Color.Red;

                }
                else
                {
                    lblMsg.Text = "Your bulk upload is in queue";
					lblMsg.ForeColor = Color.Green;
				}

            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
            }
		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			//btnBulkInsert.Enabled = true;
			gvItemStatus.DataSource = null;
            gvItemStatus.DataBind();    
			lstItemList.Items.Clear();
			pnlRight.Controls.Clear();
			lstItemStatus.Items.Clear();
			lblMsg.Text = "";
			SetSessionData(SessionConstants.GradeMeasureControls, null);
		}

		#endregion

		#region Download Template
		protected void OnCpSelectedChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataSet dsPartMeasures = new DataSet();
            dsPartMeasures.ReadXml(Server.MapPath("BulkItemEntryData.xml"));

            var cpList = GetSessionState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ?? new List<CustomerProgramModel>();
            var cpModel = cpList.Find(m => m.CpId == CpList.SelectedValue);
            Session["SelectedProgram"] = cpModel;

            if (cpModel != null)
            {
                List<MeasureValueCpModel> lstMeasureValueCpModel = GSIAppQueryUtils.GetMeasureValuesCpID(cpModel, this).ToList().OrderBy(s => s.PartId).ThenBy(s => s.MeasureName).ToList();
                ds.Tables.Add(new DataTable());
                //ds.Tables[0].Columns.Add("SrNo");
                //ds.Tables[0].Columns.Add("CustomerCode");
                ds.Tables[0].Columns.Add("ItemNumber");

                for (int i = 1; i <= 15; i++)
                    {
                        List<string> lstSelectedMeasure = new List<string>();
                        switch (i)
                        {
                            case 1:
                                //lstSelectedMeasure = ConfigurationSettings.AppSettings["Diamond"].ToString().Split(',').ToList(); 
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["Diamond"].ToString().Split(',').ToList();                            
                                break;
                            case 2:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["ColoredDiamond"].ToString().Split(',').ToList();
                                break;
                            case 3:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["ColorStone"].ToString().Split(',').ToList();
                                break;
                            case 4:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["Pearl"].ToString().Split(',').ToList();
                                break;
                            case 5:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["Mount"].ToString().Split(',').ToList();
                                break;
                            case 6:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["Metal"].ToString().Split(',').ToList();
                                break;
                            case 7:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["MetalBody"].ToString().Split(',').ToList();
                                break;
                            case 10:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["WhiteDiamondStoneSet"].ToString().Split(',').ToList();
                                break;
                            case 11:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["ColorDiamondStoneSet"].ToString().Split(',').ToList();
                                break;
                            case 12:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["ColorStoneSet"].ToString().Split(',').ToList();
                                break;
                            case 13:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["PearlSet"].ToString().Split(',').ToList();
                                break;
                            case 15:
                                lstSelectedMeasure = dsPartMeasures.Tables[0].Rows[0]["ItemContainer"].ToString().Split(',').ToList();
                                break;
                        }

                        lstMeasureValueCpModel.RemoveAll(x => !lstSelectedMeasure.Contains(x.MeasureId.ToString()) && x.PartTypeId == i);
                    }
                
                foreach (MeasureValueCpModel objMeasureModel in lstMeasureValueCpModel)
                {
					//string shortPartName = new String(objMeasureModel.PartName.Split(' ').Select(s => s.First()).ToArray());
					string shortPartName = string.Empty;
					for (int i = 0; i < objMeasureModel.PartName.Split(' ').Length; i++)
					{
						int j;
						bool isNum = int.TryParse(objMeasureModel.PartName.Split(' ')[i], out j);
						if (isNum == true)
						{
							shortPartName += objMeasureModel.PartName.Split(' ')[i];
						}
						else
						{
							shortPartName += objMeasureModel.PartName.Split(' ')[i].Substring(0, 1);
						}
					}
					ds.Tables[0].Columns.Add(shortPartName.ToUpper() + "_" + objMeasureModel.MeasureName);
                }
            }
			if (ds.Tables.Count != 0)
			{
				DataRow row = ds.Tables[0].NewRow();
				ds.Tables[0].Rows.Add(row);
				ds.AcceptChanges();
				Session["DsCPMeasure"] = ds;
			}
        }

        protected void OnDownloadTemplateClick(object sender, EventArgs e)
        {
            if (CustomerList.Text == "" && CpList.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please select Customer and Program.');", true);
                return;
            }
            DataSet dsMeasure = (DataSet)Session["DsCPMeasure"];
            // ExportDataSetToExcel(dsMeasure);
            ExportDataSetToExcel(dsMeasure,this);
        }

        private void ExportDataSetToExcel(DataSet ds, System.Web.UI.Page p)
        {
            IWorkbook wb = new XSSFWorkbook();
            var sheet = wb.CreateSheet("Data");
            string filename = "Template_" + CustomerList.SelectedItem.Text.Split('#')[1] + "_" + CpList.SelectedItem.Text;
            Export(ds, filename, false, p);            
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, System.Web.UI.Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }

            var filename = fname + ".xlsx";
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p);
            Memory.Flush();
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            if (!forHeader)
            {
                //style.WrapText = true;
            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            return style;
        }

        private void StripEmptyRows(System.Data.DataTable dt)
        {
            List<int> rowIndexesToBeDeleted = new List<int>();
            int indexCount = 0;
            foreach (var row in dt.Rows)
            {
                var r = (DataRow)row;
                int emptyCount = 0;
                int itemArrayCount = r.ItemArray.Length;
                foreach (var i in r.ItemArray) if (string.IsNullOrEmpty(i.ToString())) emptyCount++;

                if (emptyCount == itemArrayCount) rowIndexesToBeDeleted.Add(indexCount);

                indexCount++;
            }

            int count = 0;
            foreach (var i in rowIndexesToBeDeleted)
            {
                dt.Rows.RemoveAt(i - count);
                count++;
            }
        }

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetSessionState(customers, SessionConstants.CustomersList);

            CustomerList.DataSource = customers;
            CustomerList.DataBind();
            CustomerList.SelectedIndex = 0;

        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (CustomerList.Text != "")
            {
                CustomerLike.Text = "";
                LoadCpList();
            }
            else
            {
                CpList.Items.Clear();
            }
        }

        private void LoadCpList()
        {
            var customersList = GetSessionState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            if (customersList.Count == 0)
            {
                return;
            }
            var customerModel = customersList.Find(m => m.CustomerId == CustomerList.SelectedValue);
            if (customerModel == null) return;
            var cpList = QueryUtils.GetCustomerPrograms(customerModel, null, this);
            //-- Add Vendor model
            SetSessionState(cpList, SessionConstants.CustomerProgramList);

            CpList.DataSource = cpList;
            CpList.DataBind();

            OnCpSelectedChanged(null, null);
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);

            if (filtered.Count == 1)
            {
                CustomerList.SelectedValue = filtered[0].CustomerId;
            }
            else
            {
                ResetCpList();
            }

            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }

        private void ResetCpList()
        {
            SetSessionState(null, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.DataSource = null;
            CpList.DataBind();
            // HideCpDetailsSections();
        }

        private List<CustomerModel> GetCustomersFromView()
        {
            return GetSessionState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }

        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
        }

        protected void OnHomeClick(object sender, EventArgs e)
		{
           // SetViewState(null, SessionConstants.GradeMeasureControls);
			Response.Redirect("GDLight.aspx");
		}
  
		#endregion
	}
}
