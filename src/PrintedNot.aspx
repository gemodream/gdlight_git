<%@ Page Language="c#"  MasterPageFile="~/DefaultMaster.Master" CodeBehind="PrintedNot.aspx.cs" AutoEventWireup="True" Inherits="Corpt.PrintedNot" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server" >
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
<%--    <script type="text/javascript">--%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScrollPercent();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScrollPercent();--%>
<%----%>
<%--        });--%>
<%--        function gridviewScrollPercent() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=grdReports.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 1--%>
<%--            });--%>
<%--        }--%>
<%--    </script>--%>
   <div class="demoarea">
   <div class="demoheading">Reports in an Order</div>
    <!-- Order number and Lookup button -->
    <asp:Panel runat="server" DefaultButton="cmdLoad" CssClass="form-inline" style="padding: 10px">
        <!-- 'Order Number' textbox -->
        <strong>Order number: </strong>
        <asp:TextBox ID="txtOrderNumber" MaxLength="9" runat="server" placeholder="Order number"/>
        <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Lookup" OnClick="OnLoadClick">
        </asp:Button>
    </asp:Panel>

    <!-- Info labels -->
    <div style="font-family: Arial; font-size: 12px; font-weight: bold">
        <asp:Label ID="lblNumberOfItems" runat="server" CssClass="text"></asp:Label><br />
        <asp:Label ID="lblNumberOfReports" runat="server" CssClass="text"></asp:Label>
    </div>
    
    <!-- DataGrid -->
    <div id="gridContainer" style="font-size: small; ">
        <asp:DataGrid ID="grdReports" runat="server" OnItemDataBound="OnItemDataBound" CellPadding="5" CellSpacing="5">
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
        </asp:DataGrid>
    </div>
    </div>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderNumber"
        Display="None" 
        ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required." 
        ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderNumber"
        Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" 
        ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>" 
        ValidationGroup="OrderGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
</asp:Content>
