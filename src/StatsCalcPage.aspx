﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="StatsCalcPage.aspx.cs" Inherits="Corpt.StatsCalcPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .text_highlitedFail
        {
            background-color: #FFFF00;
        }
        .text_highlitedMiss
        {
            background-color: #7DFFFF;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }
    </style>
    <script language="C#" runat="server">
        int rownum = 1;
        int rownum2 = 1;
    </script>
    <div class="demoarea">
        <div class="demoheading">Rejected Stones</div>
        <div><a href="RejectionReason.aspx" target="_blank" id="A2" runat="server">Go on Report Struct</a></div>
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:Panel ID="UpdatePanel1" runat="server">
            <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False" ActiveTabIndex="0"
                Width="100%" Height="100%">
                <!-- Tab 'New Report'  -->
                <ajaxToolkit:TabPanel runat="server" HeaderText="New Report" OnDemandMode="Once"
                    ID="TabNewReport">
                    <ContentTemplate>
                        <asp:Panel ID="NewReportPanel" runat="server">
                            <div>
                                <table>
                                    <tr>
                                        <td style="padding-left: 20px">
                                            <asp:RadioButtonList ID="periodType" runat="server" CssClass="radio" OnSelectedIndexChanged="OnChangedPeriodType"
                                                RepeatDirection="Horizontal" Width="250px" AutoPostBack="True">
                                                <asp:ListItem Value="m">Monthly</asp:ListItem>
                                                <asp:ListItem Value="w" Selected="True">Weekly</asp:ListItem>
                                                <asp:ListItem Value="r">Random</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td style="padding-left: 30px;">
                                            <labeL>Customer</labeL>
                                        </td>
                                        <td style="padding-left: 30px">
                                            <labeL>Order State</labeL>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                                <label>
                                                    From:</label>
                                                <asp:TextBox runat="server" ID="calFrom" Width="80px" />
                                                <asp:ImageButton runat="server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                                    AlternateText="Click to show calendar" />
                                                <!-- Date To -->
                                                <label style="padding-left: 20px;">
                                                    To:</label>
                                                <asp:TextBox runat="server" ID="calTo" Width="80px" />
                                                <asp:ImageButton runat="server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                                    AlternateText="Click to show calendar" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                                                    PopupButtonID="Image1" Enabled="True" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                                                    PopupButtonID="Image2" Enabled="True" />
                                            </asp:Panel>
                                        </td>
                                        <td style="padding-left: 30px">
                                            <asp:Panel ID="Panel1" runat="server" CssClass="form-inline">
                                                <!-- Date From -->
                                                <asp:Panel runat="server" DefaultButton="AddCustomerBtn" ID="SearchPanelByCustomerCp"
                                                    CssClass="form_inline">
                                                    <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                                                        width: 80px"></asp:TextBox>
                                                    <asp:Button ID="AddCustomerBtn" runat="server" CssClass="btn btn-small btn-info"
                                                        OnClick="OnAddCustomerClick" Text="Add" Style="padding-left: 5px" />
                                                    <asp:Button ID="ClearCustomerBtn" runat="server" CssClass="btn btn-small btn-info"
                                                        OnClick="OnClearCustomerClick" Text="Clear" />
                                                </asp:Panel>
                                            </asp:Panel>
                                        </td>
                                        <td style="padding-left: 30px">
                                            <asp:DropDownList ID="OrderStateList" runat="server" DataTextField="OrderStateName" DataValueField="OrderStateCode" Width="150px"/>
                                            <asp:Button runat="server" ID="CalculateBtn" Text="Calculate" OnClick="OnCalculateClick"
                                                Style="margin-left: 10px" CssClass="btn btn-small btn-info" />

                                        </td>
                                    </tr>
                                    <tr style="padding-top: 10px">
                                    <td>
                                        <asp:Label ID="Label2" runat="server" class="label" Text="Report Measures"></asp:Label>
                                    </td>
                                        <td style="padding-left: 30px; vertical-align: top; height: 23px;">
                                            <asp:Label ID="Label1" runat="server" class="label" Text="Report Customers"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: small; text-align: center; vertical-align: top;">
                                            <asp:TreeView ID="ReasonTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="Small" ForeColor="Black"
                                                Font-Bold="False" ExpandDepth="2">
                                                <SelectedNodeStyle BackColor="#D7FFFF" />
                                            </asp:TreeView>
                                        </td>
                                        <td style="padding-left: 30px; font-size: small; vertical-align: top;">
                                            <asp:DataGrid ID="CustomerGrid" runat="server" AutoGenerateColumns="False" class="table-bordered"
                                                CellPadding="2" ShowHeader="False" DataKeyField="CustomerId" OnItemCommand="OnDelCustomer">
                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="Customer" DataField="CustomerName">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="CustomerId" DataField="CustomerId" Visible="False">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundColumn>
                                                    <asp:ButtonColumn Text="Del" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                
                <!-- Tab 'Reports'  -->
                <ajaxToolkit:TabPanel runat="server" HeaderText="Reports" OnDemandMode="Once" ID="TabReports">
                    <ContentTemplate>
                        <asp:Panel ID="ReportsPanel" runat="server">
                            <div>
                                <asp:DropDownList ID="ReportsControl" runat="server" DataTextField="ReportTitle"
                                    DataValueField="ReportId" OnSelectedIndexChanged="OnReportListChanged" Width="400px"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Button runat="server" ID="RemoveRptBtn" OnClick="OnRemoveRptClick" Text="Report delete"
                                    CssClass="btn btn-small btn-info" />
                            </div>
                            <div id="ReportsDiv" runat="server">
                            <table>
                                <tr>
                                    <!-- Report Parameters Panel  -->
                                    <td style="vertical-align: top;border: silver solid 1px;padding: 5px">
                                        <asp:Panel ID="ReportParamsPanel" runat="server">
                                            <asp:Label ID="ExecuteTimeLbl" runat="server" Font-Names="Tahoma" ForeColor="#5377A9" /><br /><br />
                                            <asp:Label ID="Label3" runat="server" Text="Report Parameters" CssClass="label" /><br />
                                            <asp:Label ID="ReportPeriodLbl" runat="server" Font-Names="Tahoma" ForeColor="#5377A9" Font-Bold="True"/><br />
                                            <asp:Label ID="ReportOrderStateLbl" runat="server" Font-Names="Tahoma" 
                                                ForeColor="Black" /><br /><br />
                                            <asp:TreeView ID="ReportRuleTree" runat="server" Font-Names="Tahoma" Font-Size="Small"
                                                ForeColor="Black" Font-Bold="False" ExpandDepth="2" NodeIndent="10">
                                                <SelectedNodeStyle BackColor="#D7FFFF" />
                                            </asp:TreeView><br />
                                            <asp:TreeView ID="ReportCustomerTree" runat="server" Font-Names="Tahoma" Font-Size="Small"
                                                ForeColor="Black" Font-Bold="False" ExpandDepth="2" NodeIndent="0" >
                                                <SelectedNodeStyle BackColor="#D7FFFF" />
                                            </asp:TreeView>
                                        </asp:Panel>
                                    </td>
                                    <!-- Report Totals Panel  -->
                                    <td style="vertical-align: top; padding-left: 30px">
                                        <asp:Panel ID="ReportTotalsPanel" runat="server">
                                            <asp:Label ID="Label4" runat="server" Text="Report Totals" CssClass="label" /><br />
                                            <asp:DataGrid ID="GridTotals" runat="server" AutoGenerateColumns="False" CellPadding="5">
                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Tahoma" Font-Size="Small"
                                                    ForeColor="White" />
                                                <ItemStyle BackColor="White" BorderColor="Silver" ForeColor="#0D0D0D" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="StoneQty" HeaderText="Quantity">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PassQty" HeaderText="Pass, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PassPercent" HeaderText="Pass, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailQty" HeaderText="Fail, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailPercent" HeaderText="Fail, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailDescr" HeaderText="Fail, Qty per reason">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissQty" HeaderText="Miss, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissPercent" HeaderText="Miss, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissDescr" HeaderText="Miss, Qty per reason">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <br />
                                             <asp:Label ID="TotalsByCustomerLbl" runat="server" Text="Totals per customer" CssClass="label" /><br />
                                            <asp:DataGrid ID="GridTotalsPerCustomer" runat="server" AutoGenerateColumns="False" CellPadding="5"
                                                DataKeyField="CustomerId">
                                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Tahoma" Font-Size="Small"
                                                    ForeColor="White" />
                                                <ItemStyle BackColor="White" BorderColor="Silver" ForeColor="#0D0D0D" Font-Size="Small"/>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="#">
                                                        <ItemTemplate>
                                                            <%# rownum++ %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                        <HeaderTemplate>
                                                            Details
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="DetailsBtn" ToolTip="Details" runat="server" ImageUrl="~/Images/ajaxImages/look24.png"
                                                                OnClick="OnDetailsBtnClick" Enabled="true" Style="text-align: center" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                        <HeaderTemplate>
                                                            Export
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                        
                                                            <asp:ImageButton ID="ExportBtn" ToolTip="Export to Excel" runat="server"
                                                                ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExportBtnClick"
                                                                Enabled="true" Style="text-align: center" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="CustomerName" HeaderText="Customer">
                                                        <ItemStyle VerticalAlign="Top" Width="300px" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="StoneQty" HeaderText="Quantity">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PassQty" HeaderText="Pass, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PassPercent" HeaderText="Pass, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailQty" HeaderText="Fail, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailPercent" HeaderText="Fail, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FailDescr" HeaderText="Fail, Qty per reason">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissQty" HeaderText="Miss, Qty">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissPercent" HeaderText="Miss, %">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MissDescr" HeaderText="Miss, Qty per reason">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                
                <!-- Tab 'Report per Customer Details'  -->
                <ajaxToolkit:TabPanel runat="server" HeaderText="Report Details" OnDemandMode="Once"
                    ID="TabReportDetail">
                    <ContentTemplate>
                        <asp:Panel ID="ReportDetailsPanel" runat="server" Visible="False">
                            <div>
                                <asp:HiddenField ID="CustomerIdFld" runat="server" />
                                <asp:HiddenField ID="ReportIdField" runat="server" />
                                <asp:HiddenField ID="FirstRowNum" runat="server" Value="1"/>
                                <table style="font-family: Tahoma;">
                                    <tr>
                                        <td>
                                            Report:
                                        </td>
                                        <td style="padding-left: 30px">
                                            <asp:Label ID="ReportTitleLbl" runat="server" Font-Names="Tahoma" ForeColor="#5377A9"
                                                Font-Bold="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Customer:
                                        </td>
                                        <td style="padding-left: 30px">
                                            <asp:Label ID="ReportCustomerLbl" runat="server" Font-Names="Tahoma" ForeColor="#5377A9" Font-Bold="False"/>
                                        </td>
                                    </tr>
                                </table>
                            </div><br/>
                            <!-- Paginator on Report Details  -->
                            
                            <table>
                                <tr>
                                    <!-- Filter Panel onTab Details  -->
                                    <td style="vertical-align: top;">
                                        <!-- Result Filter Panel onTab Details  -->
                                        <table >
                                            <tr>
                                                <td><label>By filter rows: </label><asp:TextBox ID="RowCountByCriteria" ReadOnly="True" runat="server" Width="40px"/></td>
                                                <td><label>pages: </label><asp:TextBox ID="PageCountByCriteria" ReadOnly="True"  runat="server" Width="30px"/></td>
                                            </tr>
                                        </table>
                                        <div style="overflow-y: auto; width: 250px; color: black; border: silver solid 1px;
                                            margin-top: 5px" id="EditItemDiv" runat="server">
                                            <asp:Label ID="Label5" runat="server" Text="Filter: " CssClass="label" /><br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label>Type:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="RejTypeFilterList" runat="server" Width="188px" DataTextField="RejectionTypeName"
                                                            DataValueField="RejectionTypeId" ToolTip="Rejection Type"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>SKU:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="CpFilterList" runat="server" Width="188px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>Item:</label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="ItemNumberFld" runat="server" ToolTip="Order/Batch/Item" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ItemExtender" runat="server" FilterType="Custom"
                                                            ValidChars="0123456789" TargetControlID="ItemNumberFld" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <asp:DataGrid runat="server" ID="ReasonFilterGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                                GridLines="None" ShowHeader="False" DataKeyField="ReasonId"  OnItemDataBound="OnFilterGridDataBound">
                                                <Columns>
                                                    <asp:BoundColumn DataField="ReasonName" HeaderText="ReasonName"></asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <HeaderTemplate>
                                                            Value</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList runat="server" ID="FilterValueFld" Width="135px" AutoPostBack="true"></asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                            </asp:DataGrid>
                                            <asp:Button ID="FilterResetBtn" runat="server" Text="Reset" OnClick="OnResetFilterClick" CssClass="btn btn-small btn-info"/>
                                            <asp:Button ID="FilterApplyBtn" runat="server" Text="Apply" OnClick="OnApplyFilterClick" CssClass="btn btn-small btn-info"/>
                                        </div>
                                    </td>
                                    <!-- Grid with Report Details on Tab Details  -->
                                    <td style="vertical-align: top; padding-left: 30px;color: black">
                                        <div>
                                            <label>Page size:</label>
                                            <asp:TextBox ID="PageSizeFld" runat="server" Width="60px" Text="500" />
                                            <label>Current page:</label>
                                            <asp:TextBox ID="PageNoFld" runat="server" Width="60px" />
                                            <asp:Button ID="PageGoBtn" runat="server" OnClick="OnPageGoClick" CssClass="btn btn-small"
                                                Text="Go" />
                                            <asp:Button ID="NextPageBtn" runat="server" OnClick="OnNextPageClick" CssClass="btn btn-small"
                                                Text="Next" />
                                            <asp:Button ID="PrevPageBtn" runat="server" OnClick="OnPrevPageClick" CssClass="btn btn-small"
                                                Text="Prev" />
                                            <label style="width: 100px"></label>
                                            <label>Qty on Page:</label>
                                            <asp:TextBox ID="DetailsQtyFld" runat="server" ForeColor="#5377A9" ReadOnly="true" Width="60px"/>
                                        </div>
                                        <div id="gridContainer" style="font-size: small;">
                                            <asp:DataGrid ID="grdDetails" runat="server" CellPadding="5" onitemdatabound="OnDetailsGridItemDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="#">
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="50px" />
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="GridviewScrollHeader" />
                                                <ItemStyle CssClass="GridviewScrollItem" HorizontalAlign="Left" 
                                                    VerticalAlign="Top">
                                                </ItemStyle>
                                                <PagerStyle CssClass="GridviewScrollPager" />
                                            </asp:DataGrid>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </asp:Panel>
    </div>
    <!-- Information Dialog  -->
    <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="display: none;width: 310px;border: solid 2px #5390D5">
        <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #E9EDF0;
            border: solid 1px Silver; color: black; text-align: left">
            <div>
                <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                <asp:Label runat="server" ID="InfoTitle"></asp:Label>
            </div>
        </asp:Panel>
        <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;color: black;text-align: center"
            id="MessageDiv" runat="server">Test
        </div>
        <div style="padding-top: 10px">
            <p style="text-align: center; font-family: sans-serif">
                <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
            </p>
        </div>
    </asp:Panel>
    <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
        PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
        OkControlID="InfoCloseButton">
    </ajaxToolkit:ModalPopupExtender>

</asp:Content>
