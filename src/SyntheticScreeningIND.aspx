﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="SyntheticScreeningIND.aspx.cs" Inherits="Corpt.SyntheticScreeningIND" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        .GridPager a, .GridPager span {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }

        .divCol {
            float: left;
            width: 200px;
        }

        .divBottom {
            clear: both;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>

    <script type="text/javascript">

        $().ready(function () {

            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtGsiOrder.ClientID%>').val().trim() == "") {
                    alert('Please fill out order field.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtGsiOrder.ClientID%>').val().trim().length > 7) {
                    alert('Please enter valid order code.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQtyPass.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity pass field..'); $('#<%=txtQtyPass.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQtyFail.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity fail field..'); $('#<%=txtQtyFail.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtFTQuantity.ClientID%>').val().trim() == "") {
                    alert('Please fill out further test quantity field..'); $('#<%=txtFTQuantity.ClientID%>').focus(); return false;
                }
                else if (parseInt($('#<%=lblTotalQTY.ClientID%>').text()) != parseInt($('#<%=txtQtyPass.ClientID%>').val()) + parseInt($('#<%=txtQtyFail.ClientID%>').val()) + parseInt($('#<%=txtFTQuantity.ClientID%>').val())) {
                    alert('Please fill out valid entry.\nTotal quantity is not equal to quantity pass plus quantity fail plus further test quantity.'); $('#<%=txtFTQuantity.ClientID%>').focus(); return false;
                }
                else {
                    return true;
                }

            });
            <%--#<%=txtGsiOrder.ClientID%>,--%>
            $(' #<%=gsiOrderFilter.ClientID%>, #<%=txtQtyPass.ClientID%>, #<%=txtQtyFail.ClientID%>, #<%=txtFTQuantity.ClientID%>, #<%=txtGSIItemNumber.ClientID%>, #<%=txtItemQTYFail.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

            $('#<%=txtCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();

            $('#<%=txtToCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();
        });

        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>

    <div class="demoarea">
        <h2 style="margin-left: 10px; margin-bottom: 10px; margin-top: 10px" class="demoheading">Synthetic Screening Data Entry
        </h2>
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">

                <div class="divCol">

                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                            GSI Order # :
                            <asp:TextBox type="text" ID="txtGsiOrder" runat="server" name="txtGsiOrder" TextMode="Number" min="100000" max="9999999"
                                placeholder="Order number" Style="width: 75px;" />
                            <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-small" runat="server" Text=""
                                OnClick="OnOrderCodeLoadClick" Style="margin-left: 10px; visibility: hidden; float: right;" UseSubmitBehavior="true"></asp:Button>
                            <asp:HiddenField ID="hdnScreeningID" runat="server" Value="0" />
                        </asp:Panel>
                    </div>
                                      

                    <div class="form-group" style="width: 170px;">
                        Customer Code :
                        <asp:Label ID="lblCustomerCode" runat="server"></asp:Label>
                    </div>

                    <div class="form-group" style="width: 170px;">
                        MO # :
                        <div>
                            <asp:Label ID="lblMemoNum" runat="server" style="white-space:nowrap;"></asp:Label>
                        </div>
                    </div>
                                       
                    <div class="form-group" style="width: 170px; margin-top: 20px;">
                        PO # :
                        <asp:TextBox ID="txtPONum" runat="server" Text="" MaxLength="200" Style="padding-top: 2px; padding-bottom: 2px; width: 136px;"></asp:TextBox>
                    </div>

                    <div class="form-group" style="margin-bottom: 2px;">
                        <label style="margin-bottom: 1px;">Destination :</label>
                        <asp:DropDownList ID="ddlDestination" runat="server" CssClass="form-control form-control-height selectpicker" Style=" padding-top: 2px; padding-bottom: 2px; width: 150px;"></asp:DropDownList>

                    </div>

                  
                    <div class="form-group" style="margin-bottom: 10px; ">
                        <label class="control-label">
                            Test:</label>
                        <div>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkQCHK" runat="server" type="checkbox" style="margin-top: 0px;margin-right:3px;" value="QCHK" />QCHK++
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkYehuda" runat="server" type="checkbox" style="margin-top: 0px; margin-right: 3px;" value="Yehuda" />Yehuda
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkDiamondView" runat="server" type="checkbox" style="margin-top: 0px; margin-right: 3px;"
                                    value="DiamondView" />Diamond View
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkRaman" runat="server" type="checkbox" style="margin-top: 0px; margin-right: 3px;" value="Raman" />Raman
                            </label>
                            <label class="checkbox-inline" style="padding-left: 15px;">
                                <input id="chkFTIR" runat="server" type="checkbox" style="margin-top: 0px; margin-right: 3px;" value="FTIR" />FTIR
                            </label>
                        </div>
                    </div>

                    <table>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyPass">
                                        Quantity Pass:</label>
                                    <div>
                                        <input id="txtQtyPass" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Pass" maxlength="5" style="width: 60px;" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyFail">
                                        Quantity Fail:</label>
                                    <div>
                                        <input id="txtQtyFail" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Fail" maxlength="5" style="width: 60px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <div class="form-group" style="width: 170px; margin-top: 60px;">
                        
                        <div>
                            <label class="control-label">
                                Add Fail Items for Order#:</label>
                        </div>
                        <div style="width: 350px;">
                            <input id="txtGSIItemNumber" runat="server" maxlength="11" style="width: 25%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="GSI Item Number" />
                            <input id="txtItemQTYFail" runat="server" maxlength="3" style="width: 20%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="Quantity Fail" />
                            <asp:Button ID="btnAddItem" runat="server" CssClass="btn btn-primary" Text="Add Item"
                                Style="height: 28px; margin-bottom: 10px;" OnClick="btnAddItem_Click"></asp:Button>
                        </div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" Style="margin-top: 5px;"
                                        OnRowDeleting="gvItems_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Item Number" HeaderStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGSIItemNumber" runat="server" Text='<%# Eval("GSIItemNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QTY Fail" HeaderStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemQTYFail" runat="server" Text='<%# Eval("ItemQTYFail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="true" DeleteImageUrl="Images/delete.png" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

                <div class="divCol" style="margin-right: 35px;">

                    <div class="form-group" style="margin-top: 5px; margin-bottom: 11px;">
                        Request # :
                        <asp:Label ID="lblRequestID" runat="server"></asp:Label>
                    </div>
                                     

                    <div class="form-group" >
                        Total QTY :
                        <asp:Label ID="lblTotalQTY" runat="server"></asp:Label>
                    </div>

                   

                    <div class="form-group" >
                        Style :
                        <div>
                            <asp:Label ID="lblStyle" runat="server" Style="max-height: 500px; white-space: nowrap;"></asp:Label>
                        </div>
                    </div>


                    <div class="form-group" style="margin-top: 20px;">
                        SKU :
                        <div>
                            <asp:TextBox ID="txtSKU" runat="server" MaxLength="200" Style="padding-top: 2px; padding-bottom: 2px; width: 136px;" Text=""></asp:TextBox>
                            
                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 2px; ">
                        Certified By :
                            <div>
                                <asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="form-control form-control-height selectpicker"
                                    Style="padding-top: 2px; padding-bottom: 2px; width: 150px;">
                                </asp:DropDownList>
                            </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 53px;">
                        Special Instructions :
                        <textarea id="txtNotes" runat="server" class="form-control form-control-height" rows="3"
                            placeholder="Enter Notes" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                    </div>

                    <table>
                         <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtFTQuantity">
                                        Further Test QTY:</label>
                                    <div>
                                        <input id="txtFTQuantity" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter FT QTY" maxlength="5" style="width: 60px;" />
                                    </div>
                                </div>

                            </td>
                            <td>
                                
                            </td>
                        </tr>

                    </table>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <div style="text-align: left;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" style="width:75px; height:40px" Text="Add New"
                                OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary" style="width:75px; height:40px" Text="Clear"
                                OnClick="btnClear_ServerClick"></asp:Button>
                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back" Style="display: none;"
                                OnClick="btnBack_Click"></asp:Button>
                        </div>
                    </div>
                </div>
                <div style="float: left; width: 600px;">
                    <div class="form-grouph" style="margin-bottom: 7px;">
                        <label class="control-label" for="gsiOrderFilter">
                            Filter By GSI Order # / Destination / Create Date:</label>
                        <div style="padding-bottom: 5px; display: block; width: 800px;">
                            <input id="gsiOrderFilter" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter Order#" maxlength="7" />
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnSearch_Click"></asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>

                            <asp:DropDownList ID="ddlDestinationSearch" runat="server" CssClass="form-control form-control-height selectpicker"
                                Style="width: 17%; display: inline">
                            </asp:DropDownList>

                            <asp:Button ID="btnDestSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;"
                                OnClick="btnDestSearch_Click"></asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                            <input id="txtCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="From Date"
                                readonly />
                            <input id="txtToCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="To Date"
                                readonly />
                            <asp:Button ID="btnCreateDateSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCreateDateSearch_Click"></asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;"></span>
                            <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px;" OnClick="btnExportToExcel_Click" />
                        </div>
                        <div class="form-group" style="margin-bottom: 7px;">
                            <div id="mainContainer" class="container" style="padding-left: 0px;">
                                <div class="shadowBox">
                                    <div class="page-container">
                                        <div class="container" style="padding-left: 0px;">
                                            <div style="padding-bottom: 5px;">
                                                <span id="gvTitle" runat="server" class="text-info"></span>
                                            </div>
                                            <div>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="grdScreening" runat="server" Width="1200" CssClass="table striped table-bordered"
                                                        AutoGenerateColumns="False" DataKeyNames="ScreeningID" EmptyDataText="There are no data records to display."
                                                        AllowPaging="true" RowStyle-Height="2px" OnPageIndexChanging="grdScreening_PageIndexChanging"
                                                        OnSelectedIndexChanged="grdScreening_SelectedIndexChanged"
                                                        Height="200px">
                                                        <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                            Height="25px" />
                                                        <RowStyle Font-Size="12px" />
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" SelectText="Select" HeaderStyle-Width="30px" />
                                                            <asp:BoundField DataField="RequestID" HeaderText="Request#" Visible="true" ReadOnly="True"
                                                                HeaderStyle-Width="30px" SortExpression="RequestID" ItemStyle-CssClass="visible-lg"
                                                                HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="CreatedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="GSIOrder" HeaderText="Order#" HeaderStyle-Width="60px"
                                                                SortExpression="GSIOrder" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CustomerCode" HeaderText="Customer Code" HeaderStyle-Width="100px"
                                                                SortExpression="CustomerCode" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Test" HeaderText="Test" HeaderStyle-Width="40px" ItemStyle-Wrap="true"
                                                                SortExpression="Test" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CertifiedBy" HeaderText="Certified By" HeaderStyle-Width="80px" ItemStyle-Wrap="false"
                                                                SortExpression="CertifiedBy" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Destination" HeaderText="Destination" HeaderStyle-Width="110px"
                                                                SortExpression="Destination" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="TotalQty" HeaderText="Total QTY" HeaderStyle-Width="30px"
                                                                SortExpression="TotalQty" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="FTQuantity" HeaderText="QTY FT" HeaderStyle-Width="30px"
                                                                SortExpression="FTQuantity" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtyPass" HeaderText="QTY Pass" HeaderStyle-Width="30px"
                                                                SortExpression="QtyPass" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtyFail" HeaderText="QTY Fail" HeaderStyle-Width="30px"
                                                                SortExpression="QtyFail" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="MemoNum" HeaderText="Memo#" HeaderStyle-Width="150px" SortExpression="MemoNum"
                                                                ItemStyle-CssClass="visible-lg" ItemStyle-Wrap="false" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="PONum" HeaderText="PO#" HeaderStyle-Width="150px" SortExpression="PONum"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="SKUName" HeaderText="SKU" HeaderStyle-Width="150px" ItemStyle-Wrap="true"
                                                                SortExpression="SKUName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="StyleName" HeaderText="Style" HeaderStyle-Width="150px" ItemStyle-Wrap="true"
                                                                SortExpression="StyleName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" ItemStyle-Wrap="false"
                                                                SortExpression="CustomerName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" HeaderStyle-Width="200" ItemStyle-Wrap="false"
                                                                HeaderStyle-CssClass="hidden-lg" ItemStyle-CssClass="hidden-lg" />

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
