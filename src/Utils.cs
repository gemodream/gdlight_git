using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;

using System.Data.SqlClient;
using System.Collections;
using System.Web.UI;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Collections.Generic;
using Corpt.FullItemCode;
using Corpt.Models;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class Utils
	{
		public Utils()
		{
			// 
			// TODO: Add constructor logic here
			//
		}

		public static int ParseOrderCode(string itemNumber,
            Func<string, bool> isBigBatchCode = null)
        {
            return new FullItemCodeParser(itemNumber, isBigBatchCode).Parse().OrderCode;
		}
		
		public static int ParseBatchCode(string itemNumber,
            Func<string, bool> isBigBatchCode = null)
		{
            return new FullItemCodeParser(itemNumber, isBigBatchCode).Parse().BatchCode;
		}

        public static int ParseItemCode(string itemNumber,
            Func<string, bool> isBigBatchCode = null)
		{
            return new FullItemCodeParser(itemNumber, isBigBatchCode).Parse().ItemCode;
		}

        public static bool IsShortReport(String batchId, Page page, out String documentId)
		{
			
			documentId ="0";
			var myDocList = new DataSet();

			var conn = new SqlConnection("" + page.Session["MyIP_ConnectionString"]);
			conn.Open();

			var command = new SqlCommand
			{
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "spGetDocumentTypeCodeByBatchID"
			};


		    command.Parameters.AddWithValue("@BatchID", batchId);
			command.Parameters.AddWithValue("@AuthorId", page.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", page.Session["AuthorOfficeID"].ToString());

			var dataAdapter = new SqlDataAdapter(command);
			dataAdapter.Fill(myDocList);

			var myDataView = new DataView(myDocList.Tables[0]) {RowFilter = "DocumentTypeCode = 10"};

		    if(myDataView.Count > 0)
			{
				documentId = myDataView[0]["DocumentID"].ToString();
			}
			myDataView.Dispose();
			dataAdapter.Dispose();
			command.Dispose();
			conn.Close();
		    return documentId != "0";
		}

		public static bool IsCustomLabel(String batchId, Page page, out String documentId)
		{
			documentId ="";

			var conn = new SqlConnection("" + page.Session["MyIP_ConnectionString"]);
			conn.Open();

			var command = new SqlCommand
			    {CommandText = "spGetDocumentTypeCodeByBatchID",Connection = conn, CommandType = CommandType.StoredProcedure};
		    command.Parameters.AddWithValue("@BatchID", batchId);
			command.Parameters.AddWithValue("@AuthorId", page.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", page.Session["AuthorOfficeID"].ToString());
			
            var dataAdapter = new SqlDataAdapter(command);
            var myDocList = new DataSet();
            dataAdapter.Fill(myDocList);

			var myDataView = new DataView(myDocList.Tables[0]) {RowFilter = "DocumentTypeCode = 8"};

		    if(myDataView.Count > 0)
			{
				documentId = myDataView[0]["DocumentID"].ToString();
			}
			myDataView.Dispose();
			dataAdapter.Dispose();
			command.Dispose();
			conn.Close();
		    return documentId != "";
		}

		public static String ParametersToValue(String myValue, String myBatchId, String myItemCode, Page page)
		{
			var r = new Regex(@"\[[\d,\w,/\\,\s,),(,#]*[.][\d,\w,/\\,\s,),(,#]*\]"); //accounts for numbers, dashes, spaces
		    var mc = r.Matches(myValue);

			if(mc.Count==0)
			{
				return myValue;
			}
			return ParametersToValue(r.Replace(myValue, ReplacementDataString(mc[0].Value, myBatchId, myItemCode, page), 1, 0), myBatchId, myItemCode, page);
		}

		public static String ParametersToValueNOHighlights(String myValue, String myBatchId, String myItemCode, Page page)
		{
			var r = new Regex(@"\[[\d,\w,/\\,\s,),(,#]*[.][\d,\w,/\\,\s,),(,#]*\]"); //accounts for numbers, dashes, spaces
			var mc = r.Matches(myValue);

			switch (mc.Count)
			{
			    case 0:
			        return myValue;
			    default:
			        return ParametersToValueNOHighlights(
                        r.Replace(myValue, ReplacementDataStringNoHighligths(mc[0].Value, myBatchId, myItemCode, page), 1, 0), 
                        myBatchId, myItemCode, page);
			}
		}

		public static String ReplacementDataStringNoHighligths(String myMetaData, String myBatchId, String myItemCode, Page page)
		{
			var conn = new SqlConnection("" + page.Session["MyIP_ConnectionString"]);
            conn.Open();

		    SqlCommand command; 
			SqlDataReader reader;


			if(myMetaData.Trim('[').Trim(']').Split('.')[1] == "Item #")
			{
				command = new SqlCommand { CommandText = "sp_GetOldNumberByBatchIDItemCode", CommandType = CommandType.StoredProcedure, Connection = conn };
			    command.Parameters.AddWithValue("@BatchID", myBatchId);
				command.Parameters.AddWithValue("@ItemCode", myItemCode);

				reader = command.ExecuteReader();
			    var myVal = "n/a";
				if(reader.HasRows)
				{
					reader.Read();
					myVal = reader.GetSqlValue(reader.GetOrdinal("Value")).ToString();
				}
				conn.Close();
				return myVal;
			}

            
            command = new SqlCommand{CommandText = "spGetMeasureValueByPart", Connection = conn, CommandType = CommandType.StoredProcedure};
		    command.Parameters.AddWithValue("@BatchID", myBatchId);
            command.Parameters.AddWithValue("@ItemCode", myItemCode);
            command.Parameters.AddWithValue("@PartName", myMetaData.Trim('[').Trim(']').Split('.')[0]);
            command.Parameters.AddWithValue("@MeasureTitle", myMetaData.Trim('[').Trim(']').Split('.')[1]);
            command.Parameters.AddWithValue("@CutGrade", "0");
            command.Parameters.AddWithValue("@AuthorId", page.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", page.Session["AUthorOfficeID"]);
			
			reader = command.ExecuteReader();
		    var myValue = "n/a";
			if(reader.HasRows)
			{
				reader.Read();
				myValue = NullValue(reader.GetSqlValue(reader.GetOrdinal("Value")));
			    var metaPart = myMetaData.Trim('[').Trim(']').Split('.')[1];
			    var noMess =
                    (metaPart != "KM") &&
                    (metaPart != "LD") &&
                    (metaPart != "FF") &&
                    (metaPart != "Black Pique") &&
                    (metaPart != "Broken Stone") &&
                    (metaPart != "Openings/Cavity") &&
                    (metaPart != "Missing Stone") &&
                    (metaPart != "Indented Natural") &&
                    (metaPart != "LI Present") &&
                    (metaPart != "Hearts and Arrows");
				if((myValue=="") && noMess)
				{
					((ArrayList) page.Session["LabelNoPrintList"]).Add(myMetaData + " is empty.");
				}
			}
			
			conn.Close();
		    return myValue;
		}

		public static String ReplacementDataString(String myMetaData, String myBatchId, String myItemCode, Page page)
		{

			//check for correct part name first
			var dtItemStructure = page.Session["BatchShortReportII.dtItemStructure"] as DataTable;
            if (dtItemStructure != null)
            {
                var blPartNotFound = true;
                foreach (DataRow dr in dtItemStructure.Rows)
                {
                    if (NullValue(dr["PartName"]).ToUpper() == myMetaData.Trim('[').Trim(']').Split('.')[0].ToUpper())
                    {
                        blPartNotFound = false;
                    }
                }
                if (blPartNotFound)
                {
                    return "<SPAN class=text_highlitedyellow>wrong structure</SPAN>";
                }
            }


            var conn = new SqlConnection("" + page.Session["MyIP_ConnectionString"]);
            conn.Open();

            SqlCommand command;
		    SqlDataReader reader;

		    var partName = myMetaData.Trim('[').Trim(']').Split('.')[0];
		    var measureTitle = myMetaData.Trim('[').Trim(']').Split('.')[1];
			
            if(measureTitle == "Item #")
			{
                //-- Get Old Number
				command = new SqlCommand
				{
				    CommandText = "sp_GetOldNumberByBatchIDItemCode", 
                    CommandType = CommandType.StoredProcedure, 
                    Connection = conn
				};

			    command.Parameters.AddWithValue("@BatchID", myBatchId);
				command.Parameters.AddWithValue("@ItemCode", myItemCode);

				reader = command.ExecuteReader();

                var myVal = "<bold>No measure</bold>";
				if(reader.HasRows)
				{
					reader.Read();
					myVal = reader.GetSqlValue(reader.GetOrdinal("Value")).ToString();
				}
                conn.Close();
			    return myVal;
			}

            command = new SqlCommand
            {
                CommandText = "spGetMeasureValueByPart", 
                Connection = conn, 
                CommandType = CommandType.StoredProcedure
            };
		    command.Parameters.AddWithValue("@BatchID", myBatchId);
			command.Parameters.AddWithValue("@ItemCode", myItemCode);
			command.Parameters.AddWithValue("@PartName", partName);
			command.Parameters.AddWithValue("@MeasureTitle", measureTitle);
			command.Parameters.AddWithValue("@CutGrade", "0");
			command.Parameters.AddWithValue("@AuthorId", page.Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", page.Session["AUthorOfficeID"]);
		    command.CommandTimeout = page.Session.Timeout;
			reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				reader.Read();
                var myValue = NullValue(reader.GetSqlValue(reader.GetOrdinal("Value")));
			    var myCp = reader.GetSqlValue(reader.GetOrdinal("CP")).ToString();
				if(Int32.Parse(myCp) != 1)
				{
                    myValue = "<SPAN class=text_highlitedyellow>" + myValue + "</SPAN>";
                }
                conn.Close();
                return myValue;

			}

            conn.Close();
			return "<bold>No measure</bold>";
		}

		public static bool SanPartName(string partName, out string sanPartName)
		{
			sanPartName = partName;
			return true;
		}

		public static String CleanPartName(String partName)
		{
			if((partName.IndexOf("Container")!=-1)||(partName.IndexOf("container")!=-1))
				partName="Item";
			if((partName.IndexOf("Body")!=-1)||(partName.IndexOf("body")!=-1))
				partName="Metal";
			return partName;
		}
		public static String CleanCD(String stringToClean)
		{
			bool deleteChars = false;
			StringBuilder workingString = new StringBuilder("");
			for(int i =0; i<stringToClean.Length;i++)
			{
				if(!deleteChars)
				{
					if(stringToClean[i]=='[')
					{
						deleteChars=true;
						workingString.Append("______");
						continue;
					}
					workingString.Append(stringToClean[i].ToString());
				}
				else
				{
					if(stringToClean[i]==']')
					{
						deleteChars=false;
						continue;
					}
				}
			}
			return workingString.ToString();								
		}

		public static string NullValue2(object obj)
		{
			if (obj == null || obj.ToString().ToLower() == "null" || obj.ToString().Trim() == "" )
				return "";

			return obj.ToString();
		}

		public static string NullValue(object obj)
		{
			if (obj == null || 
				obj.ToString().ToLower() == "null" || 
				obj.ToString().Trim() == "" || 
				obj.ToString().ToLower().Trim() == "exception" || 
				obj.ToString().ToLower().IndexOf("none") >= 0)
				return "";

			return obj.ToString();
		}

		public static String FormatCarrierString(String trackingNumber, int carrierCode)
		{
			if(trackingNumber.Trim()=="Scan Package Bar-Code Here")
				trackingNumber="N/A";
			switch(carrierCode)
			{
				case 1 :
				{
					if(trackingNumber.Trim().Length==16)
						return "FedEx with a tracking number <a href=\"http://www.fedex.com/Tracking?language=english&cntry_code=us&tracknumbers=" + trackingNumber.Trim().Substring(0,12) +"&track.x=0&track.y=0\">"+trackingNumber.Trim().Substring(0,12)+"</a>";
					if(trackingNumber.Trim().Length==32)
						return "FedEx with a fracking number <a href=\"http://www.fedex.com/Tracking?language=english&cntry_code=us&tracknumbers=" + trackingNumber.Trim().Substring(16,12) +"&track.x=0&track.y=0\">"+trackingNumber.Trim().Substring(16,12)+"</a>";
					if(trackingNumber.Trim()=="N/A")
						return "FedEx";
					return "";

					break;
				}
				case 2 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "UPS with a tracking number <a href=\"http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=" + trackingNumber.Trim() +"&track.x=0&track.y=0\">"+trackingNumber.Trim()+"</a>";
					else
						return "UPS";
					break;
				}
				case 3 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "USPS with a tracking number <a href=\"http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=" + trackingNumber.Trim() +"\">"+trackingNumber.Trim()+"</a>";
					else
						return "USPS";
					break;
				}
				case 4 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "Brinks with a traking number " + trackingNumber.Trim();
					else
						return "Brinks";
					break;
				}
				case 5 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "MalcaAmit with a tracking number " + trackingNumber.Trim();
					else 
						return "MalcaAmit";
					break;
				}
				case 6 :
				{
					return "GSI messenger.";
					break;
				}
				case 7 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "Dunbar with a tracking number " + trackingNumber.Trim();
					else
						return "Dunbar";
					break;
				}
				case 8 :
				{
					if(trackingNumber.Trim()!="N/A")
						return "K n S Armed Courier inc with a tracking number " + trackingNumber.Trim();
					else 
						return "K n S Armed Courier inc";
					break;
				}
				case 9 :
				{
                    return (trackingNumber.Trim() == "N/A" ?
                        "Parcel Pro" : "Parcel Pro with a tracking number " + trackingNumber.Trim());
				}
				case 10 :
				{
                    return (trackingNumber.Trim() == "N/A" ?
                        "DHL" : "DHL with a tracking number " + trackingNumber.Trim());
				}
				case 11 :
		        {
		            return (trackingNumber.Trim() == "N/A" ? 
                        "BAX" : "BAX with a tracking number " + trackingNumber.Trim());
				}
				case -1 :
				{
					return "messenger " + trackingNumber.Trim();
				}
				case -2 :
				{
					return "GSI messenger.";
				}
			}
			return "";

		}
		
		public static String NoMoreUnicode(String convertThis)
		{
			var r = new Regex(@"_x[A-F,0-9]{4}_"); //accounts for unicode charachters
			var mc = r.Matches(convertThis);
			for(var i=0; i < mc.Count; i++)
			{
				var repSubject = mc[i].Value.Trim('_').Trim('x');
				var repInt = Int32.Parse(repSubject,NumberStyles.AllowHexSpecifier);
				var repChar = Convert.ToChar(repInt);
				convertThis = r.Replace(convertThis, repChar.ToString(CultureInfo.InvariantCulture), 1, i);
			}
			return convertThis;
		}
		public static void SaveExcell(DataSet report,Page p)
		{
		    var srcFile = p.Session["ReportTemplate"].ToString();
		    var dstFile = p.Session["TempDir"] + p.Session.SessionID + @"\Report.xls";
			File.Copy( srcFile, dstFile, true);

            var streamWriter = new StreamWriter(dstFile, true);
			foreach(DataTable t in report.Tables)
			{
				//start worksheet
				streamWriter.Write("<Worksheet ss:Name = \""+t.TableName+"\"><Table x:FullColumns = \"1\" x:FullRows = \"1\">");
#if DEBUG
				streamWriter.Flush();
#endif
				//write column headers
				streamWriter.Write("<Row>");
				foreach(DataColumn c in t.Columns)
				{
					streamWriter.Write("<Cell><Data ss:Type = \"String\">" + NoMoreUnicode(c.ColumnName) + "</Data></Cell>");
				}
				//end column headers
				streamWriter.Write("</Row>");
#if DEBUG
				streamWriter.Flush();
#endif
			
				foreach(DataRow r in t.Rows)
				{
					streamWriter.Write("<Row>");
					foreach(DataColumn c in t.Columns)
					{
						var cellData = r[c].ToString();
						if(cellData.IndexOf("<SPAN", StringComparison.Ordinal) != -1)
						{
							cellData=Regex.Replace(cellData,@"<[^>]+>","");
							streamWriter.Write("<Cell ss:StyleID=\"s21\"><Data ss:Type = \"String\">"+cellData+"</Data></Cell>");
						}
						else
							streamWriter.Write("<Cell><Data ss:Type = \"String\">"+cellData+"</Data></Cell>");
					}
					streamWriter.Write("</Row>");
#if DEBUG
					streamWriter.Flush();
#endif
				}

				//end worksheet
				streamWriter.Write("</Table></Worksheet>");
			}
			streamWriter.Write("</Workbook>");
			streamWriter.Flush();
			streamWriter.Close();
		}

		public static void SaveExcell2(DataSet report,Page p)
		{
		    var dstPath = p.Session["TempDir"] + p.Session.SessionID + @"\Report.xls";
			File.Copy(p.Session["ReportTemplate"].ToString(), dstPath, true);
			
            var streamWriter = new StreamWriter(dstPath, true);
			streamWriter.Write("<Worksheet ss:Name = \"Worksheet1\"><Table x:FullColumns = \"1\" x:FullRows = \"1\">");

			foreach(DataTable t in report.Tables)
			{
				//start worksheet			
#if DEBUG
				streamWriter.Flush();
#endif
				//write column headers
				streamWriter.Write("<Row>");
				foreach(DataColumn c in t.Columns)
				{
					streamWriter.Write("<Cell><Data ss:Type = \"String\">" + NoMoreUnicode(c.ColumnName) + "</Data></Cell>");
				}
				//end column headers
				streamWriter.Write("</Row>");
#if DEBUG
				streamWriter.Flush();
#endif
			
				foreach(DataRow r in t.Rows)
				{
					streamWriter.Write("<Row>");
					foreach(DataColumn c in t.Columns)
					{
						var cellData = r[c].ToString();
						if(cellData.IndexOf("<SPAN", StringComparison.Ordinal)!=-1)
						{
							cellData = Regex.Replace(cellData,@"<[^>]+>", "");
							streamWriter.Write("<Cell ss:StyleID=\"s21\"><Data ss:Type = \"String\">" + cellData + "</Data></Cell>");
						}
						else
							streamWriter.Write("<Cell><Data ss:Type = \"String\">" + cellData + "</Data></Cell>");
					}
					streamWriter.Write("</Row>");
#if DEBUG
					streamWriter.Flush();
#endif
				}
				streamWriter.Write("<Row><Cell><Data ss:Type = \"String\"> </Data></Cell></Row>");
				//end worksheet				
			}
			
			streamWriter.Write("</Table></Worksheet>");
			streamWriter.Write("</Workbook>");
			streamWriter.Flush();
			streamWriter.Close();
		}

		public static DataSet PassFailDataSet(DataSet dataset)
		{
			var resultSet = new DataSet("Short Report");
            //-- Duplicate structure with an additional "Result" column
			foreach (DataTable dt in dataset.Tables) 
			{
				resultSet.Tables.Add(new DataTable(dt.TableName));
				resultSet.Tables[dt.TableName].Columns.Add(new DataColumn("Result"));
				foreach(DataColumn dc in dataset.Tables[dt.TableName].Columns)
				{
					resultSet.Tables[dt.TableName].Columns.Add(new DataColumn(dc.ColumnName));
				}
			}
			
            //-- Copy Data
			foreach(DataTable dt in dataset.Tables) 
			{
				foreach(DataRow dr in dataset.Tables[dt.TableName].Rows)
				{
					var resultRow = resultSet.Tables[dt.TableName].NewRow();
					foreach(DataColumn dc in dataset.Tables[dt.TableName].Columns)
					{
						resultRow[dc.ColumnName]=dr[dc].ToString();
					}
					resultSet.Tables[dt.TableName].Rows.Add(resultRow);
				}
			}
			
            //-- Fill in the "Result" column
			
			foreach(DataTable dt in resultSet.Tables)
			{
				foreach(DataRow dr in resultSet.Tables[dt.TableName].Rows)
				{
					foreach(DataColumn dc in resultSet.Tables[dt.TableName].Columns)
					{
						if(dr[dc].ToString().IndexOf("<SPAN", StringComparison.Ordinal) != -1)
						{
							dr[0]=@"<SPAN class=text_highlitedyellow>Fail</SPAN>";
						}
					}				
				}
			}
			return resultSet;
		}

		public static DataTable BatchShortReportWithOldItemNumber(String batchId, Page p)
		{
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
			
            String documentId;
            IsShortReport(batchId, p, out documentId);
            
            //-- Items By Batch
            var itemList = new DataSet("Item List");
            var command = new SqlCommand { CommandText = "wspvvGetItemsByBatch", CommandType = CommandType.StoredProcedure };
		    command.Parameters.AddWithValue("@BatchID",batchId);
			command.Connection=conn;
			var dataAdapter = new SqlDataAdapter(command);
			dataAdapter.Fill(itemList);

            //-- Doc Structure
            var myDocStructure = new DataSet("Doc Structure");
            command = new SqlCommand { CommandText = "spGetDocumentValue", Connection = conn, CommandType = CommandType.StoredProcedure };
		    command.Parameters.AddWithValue("@DocumentID",Int32.Parse(documentId));
			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"]);
			dataAdapter = new SqlDataAdapter(command);
			dataAdapter.Fill(myDocStructure);

			var myDocStructureView = new DataView(myDocStructure.Tables[0]);
            if (myDocStructureView.Count == 0)
            {
                conn.Close();
                return null;
            }
			var shortReport = new DataSet("Short Report");
			shortReport.Tables.Add(new DataTable("Report"));
			foreach(DataRow row in myDocStructureView.Table.Rows)
			{
				shortReport.Tables["Report"].Columns.Add(new DataColumn(row["Title"].ToString()));
			}
			shortReport.Tables["Report"].Columns.Add("OldItemNumnerColumnSpecial");
			shortReport.Tables["Report"].AcceptChanges();

			//item by item fill the "Report" table of shortReport DataSet;
			foreach(DataRow item in itemList.Tables[0].Rows)
			{
				var shrtRow = shortReport.Tables["Report"].NewRow();
				foreach(DataRow reportTitle in myDocStructureView.Table.Rows)
				{
					if((reportTitle[0].ToString()=="Cut Grade")||(reportTitle[0].ToString()=="CutGrade"))
					{
						shrtRow[reportTitle[0].ToString()] =
                            string.Format(
                                "<a href=\"CutGradeDetail.aspx?par1={0}&par2={1}&par3={2}\">{3}</a>", 
                                 item["orderCode"], 
                                 item["BatchCode"], 
                                 item["itemcode"], 
                                 ParametersToValue(reportTitle[1].ToString(), batchId, item[0].ToString(), p)
                            );
					}
					else
					{
						shrtRow[reportTitle[0].ToString()] = ParametersToValue(reportTitle[1].ToString(), batchId, item[0].ToString(), p);
					}
				}
				shortReport.Tables["Report"].Rows.Add(shrtRow);
			}
			shortReport = PassFailDataSet(shortReport);
			p.Session["ShortReport"] = shortReport;
			conn.Close();
			return shortReport.Tables["Report"];
		}

		public static string SpGetBatch(int intBatchId, Page p)
		{
			string res;
			var command = new SqlCommand
			{
                CommandText = "spGetBatch",
			    Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]),
			    CommandType = CommandType.StoredProcedure
			};

		    command.Parameters.Add(new SqlParameter("@BatchID", intBatchId));
			command.Parameters.Add(new SqlParameter("@AuthorID", p.Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString()));

			var dt = new DataTable();
			var da = new SqlDataAdapter(command);

			da.Fill(dt);
			if(dt.Rows.Count==1)
			{
				//get batch
				res = FullBatchNumber(dt.Rows[0]["GroupCode"].ToString(),dt.Rows[0]["BatchCode"].ToString());
			}
			else
			{
				var up = new Exception("Can't find full Batch Number by BatchID.");
				throw up;
			}
			return res;
		}

		public static DataTable GetShortReportFast(string strFullBatchNumber, Page p, DataTable dtDocStructure, DataTable dtItemList)
		{			
            //-- Item Data
			var command = new SqlCommand
			{
                CommandText = "spGetItemDataFromOrderbatchItem",
			    Connection = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString()),
			    CommandType = CommandType.StoredProcedure
			};

            DissectItemNumber(strFullBatchNumber, out string strGroupCode, out var strBatchCode, out _);
			            
			command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
			command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));
			command.Parameters.Add(new SqlParameter("@ItemCode", "0"));

            var da = new SqlDataAdapter(command);
            var dtItemValueList = new DataTable();
            da.Fill(dtItemValueList); //Items with Values;

			//-- Rules Tracking
            command = new SqlCommand
			{
                CommandText = "sp_RulesTracking24",
			    Connection = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString()),
			    CommandType = CommandType.StoredProcedure
			};

		    command.Parameters.Add(new SqlParameter("@BatchFullNumber", strFullBatchNumber + "00"));
			command.Parameters.Add(new SqlParameter("@ShowRules", "0"));

			da = new SqlDataAdapter(command);
			var dsCpResult = new DataSet();
			da.Fill(dsCpResult);        //Failed items with reasons;
            var dtCpResult = dsCpResult.Tables[1];

            //-- Remove '.' in ItemNumber & OldItemNumber
            foreach(DataRow dr in dtCpResult.Rows)
			{
				dr["ItemNumber"]    = dr["ItemNumber"].ToString().Replace(".","");
				dr["OldItemNumber"] = dr["OldItemNumber"].ToString().Replace(".","");
			}
			dtCpResult.AcceptChanges();

			var dtShortReport = new DataTable(); //Here we will build the short report for batch;			
			foreach(DataRow dr in dtDocStructure.Rows)
			{
				dtShortReport.Columns.Add(dr["Title"].ToString());
			}
			dtShortReport.AcceptChanges();

			foreach(DataRow dr in dtItemList.Rows)
			{
				var strNewGroupCode = dr["OrderCode"].ToString();
                var strNewBatchCode = dr["BatchCode"].ToString();
                var strNewItemCode  = dr["ItemCode"].ToString();

                var strNewItemNumber = FullItemNumber(strNewGroupCode, strNewBatchCode, strNewItemCode);

				var drShortReportRow = dtShortReport.NewRow();
				
				for(var i = 0; i < drShortReportRow.Table.Columns.Count; i++)
				{
					var strColumnName = drShortReportRow.Table.Columns[i].ColumnName.ToLower();
					if(
                        strColumnName.ToUpper().IndexOf("CUT", StringComparison.Ordinal) >= 0 && 
                        strColumnName.ToUpper().IndexOf("GRADE", StringComparison.Ordinal) >= 0)
					
					{//we need to do cutgrade here
						Console.WriteLine("need to do short report");
                        var sPartName = GetPartName(dtDocStructure.Rows[i]["Value"].ToString());
                        var sPartId = ParsePartIdValue(
                            dtDocStructure.Rows[i]["Value"].ToString(), 
                            strNewItemNumber, dtItemValueList, dsCpResult.Tables[1]);
                        var myRequest = 
                            "<a href=\"CutGradeDetail.aspx?par1=" + strNewGroupCode + 
                            "&par2=" + strNewBatchCode + 
                            "&par3=" + strNewItemCode + 
                            "&par4=" + sPartId + 
                            "&par5=" + sPartName + "\">" + 
                            ParseValue(dtDocStructure.Rows[i]["Value"].ToString(), strNewItemNumber, dtItemValueList, dsCpResult.Tables[1]) + 
                            //sPartName + 
                            "</a>";
						drShortReportRow[i] = myRequest; 
					}
					else
					{
						drShortReportRow[i] = ParseValue(
                            dtDocStructure.Rows[i]["Value"].ToString(), 
                            strNewItemNumber, dtItemValueList, dsCpResult.Tables[1]);
					}
				}
				dtShortReport.Rows.Add(drShortReportRow);
			}
			
			return dtShortReport;
		}


		public static string ParseValue(string strValue, string strItemNumber, DataTable dtItemValueList, DataTable dtCpResult)
		{
			var strResult="";
			var strPartName="";
			var strPropertyName="";
			var insidePartName = false;
			var insidePropertyName = false;

			foreach(char ch in strValue)
			{
				if((ch!='[') && (!insidePartName) && (!insidePropertyName))
				{
					strResult += ch;
					continue;
				}				
				
				if((ch == '['))
				{
					insidePartName = true;
					continue;
				}

				if((ch!='.') && insidePartName)
				{
					strPartName += ch;
					continue;
				}

				if((ch=='.') && insidePartName)
				{
					insidePartName = false;
					insidePropertyName = true;
					continue;
				}

				if((ch!=']') && insidePropertyName)
				{
					strPropertyName += ch;
					continue;
				}
				if((ch==']') && insidePropertyName)
				{
					insidePropertyName = false;
					strResult += ParseSingleValue(strPartName, strPropertyName ,strItemNumber, dtItemValueList, dtCpResult);
					strPartName = "";
					strPropertyName = "";
				}
			}
			return strResult;
		}

		public static string ParsePartIdValue(string strValue, string strItemNumber, DataTable dtItemValueList, DataTable dtCpResult)
		{
			var strResult="";
			var strPartName="";
			var strPropertyName="";
			var insidePartName = false;
			var insidePropertyName = false;

			foreach(var ch in strValue.ToCharArray())
			{
				if((ch!='[') &&  (!insidePartName) && (!insidePropertyName))
				{
					strResult += ch;
					continue;
				}				
				
				if((ch=='['))
				{
					insidePartName = true;
					continue;
				}

				if((ch!='.') && insidePartName)
				{
					strPartName += ch;
					continue;
				}

				if((ch=='.') && insidePartName)
				{
					insidePartName = false;
					insidePropertyName = true;
					continue;
				}

				if((ch!=']') && insidePropertyName)
				{
					strPropertyName += ch;
					continue;
				}
				if((ch==']') && insidePropertyName)
				{
					insidePropertyName=false;
					strResult += ParsePartId(strPartName, strPropertyName ,strItemNumber, dtItemValueList, dtCpResult);
					strPartName = "";
					strPropertyName = "";
				}
			}
			return strResult;
		}

		public static string GetPartName(string strValue) //, string strItemNumber, DataTable dtItemValueList, DataTable dtCpResult)
		{
			var strResult="";
			var strPartName="";
			var insidePartName = false;
			var insidePropertyName = false;

			foreach(var ch in strValue.ToCharArray())
			{
				if((ch!='[')&&(!insidePartName)&&(!insidePropertyName))
				{
					strResult+=ch;
					continue;
				}				
				
				if((ch=='['))
				{
					insidePartName=true;
					continue;
				}

				if((ch!='.') && insidePartName)
				{
					strPartName+=ch;
					continue;
				}

				if((ch=='.') && insidePartName)
				{
					insidePartName=false;
					insidePropertyName=true;
					continue;
				}

				if((ch!=']') && insidePropertyName)
				{
					continue;
				}
				if((ch==']') && insidePropertyName)
				{
					strResult = strPartName;
					break;
				}
			}
			return strResult;
		}


		public static string ParseSingleValue(string strPartName, string strPropertyName , string strItemNumber, DataTable dtItemValueList, DataTable dtCpResult)
		{
			if(strPropertyName.ToLower() == "item #")
			{
				//get old item number
				var itemNames = dtItemValueList.Select("[NewItemNumber] = '" + strItemNumber+"'");
				return itemNames[0]["OldItemNumber"].ToString();
			}
			var drValues = dtItemValueList.Select(
                "[NewItemNumber] = '" + strItemNumber + 
                "' AND [PartName] = '" + strPartName + 
                "' AND [MeasureName] = '" + strPropertyName + "'");
			if(drValues.Length == 0)
			{
				return "";
			}
			//check here for cprules and mark accordingly
			var strResultValue = drValues[0]["ResultValue"].ToString();
			var drCp = dtCpResult.Select(
                "[ItemNumber] = '" + strItemNumber + 
                "' AND [PartName] = '" + strPartName + 
                "' AND [MeasureName] = '"+strPropertyName + "'");
			if(drCp.Length > 0)
			{
				strResultValue = "<SPAN class=\"text_highlitedyellow\">" + strResultValue + "</SPAN>";
			}

			return strResultValue;
		}

		public static string ParsePartId(string strPartName, string strPropertyName , string strItemNumber, DataTable dtItemValueList, DataTable dtCpResult)
		{
			if(strPropertyName.ToLower() == "item #")
			{
				//get old item number
				var itemNames = dtItemValueList.Select("[NewItemNumber] = '" + strItemNumber + "'");
				return itemNames[0]["OldItemNumber"].ToString();
			}
			var drValues = dtItemValueList.Select(
                "[NewItemNumber] = '" + strItemNumber + 
                "' AND [PartName] = '" + strPartName + 
                "' AND [MeasureName] = '" + strPropertyName + "'");
			if(drValues.Length==0)
			{
				return "0";
			}
			if(drValues.Length>1)
			{
				var up = new Exception("Something Wrong with the parameter name.");
				throw up;
			}
			return drValues[0]["PartID"].ToString();
		}


		public static DataTable BatchShortReport(String batchId, Page p)
		{
            String documentId;
			var itemList = new DataSet("Item List");
            			
			IsShortReport(batchId, p, out documentId);

			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);

			//-- new version - now working only with items left in the batch
            var command = new SqlCommand
            {
                CommandText = "wspvvGetItemsByBatchExistingNumbersOnly", 
                CommandType = CommandType.StoredProcedure,
                Connection = conn
            };
		    command.Parameters.AddWithValue("@BatchID",batchId);
		
			var dataAdapter = new SqlDataAdapter(command) {SelectCommand = {CommandTimeout = p.Session.Timeout}};
		    var dsItemsByBatchExistingNumbersOnly = new DataSet();
			dataAdapter.Fill(dsItemsByBatchExistingNumbersOnly);

			//got only current ites
			itemList.Tables.Add(dsItemsByBatchExistingNumbersOnly.Tables[0].Copy());
			
			//need more code here to indicate that there are more items here
			
            //-- Session["dsMoveItems"] - DataSet, each DataTable.TableName = batchId
            //-- Session["MovedItems"] - message only

            var dtMovedItems = dsItemsByBatchExistingNumbersOnly.Tables[1].Copy();
			if(dtMovedItems.Rows.Count > 0)
			{
				//we have moved items.
				dtMovedItems.TableName = batchId;
				p.Session["MovedItems"] = "The batch has moved items. Please check with GemoDream.";
				if(NullValue(p.Session["dsMovedItems"])=="")
				{
					var dsMovedItems = new DataSet();
					try
					{
						dsMovedItems.Tables.Add(dtMovedItems.Copy());
					}
					catch(Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
					p.Session["dsMovedItems"] = dsMovedItems;
				}
				else
				{
					var dsMovedItems = p.Session["dsMovedItems"] as DataSet;
					try
					{
					    if (dsMovedItems != null) dsMovedItems.Tables.Add(dtMovedItems.Copy());
					}
					catch(Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
					p.Session["dsMovedItems"] = dsMovedItems;
				}
			}

            //-- Get Document
            var myDocStructure = new DataSet("Doc Structure");

            command = new SqlCommand
            {
                CommandText = "spGetDocumentValue", 
                Connection = conn, 
                CommandType = CommandType.StoredProcedure
            };
		    command.Parameters.AddWithValue("@DocumentID", Int32.Parse(documentId));
			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"]);

			dataAdapter = new SqlDataAdapter(command);
			dataAdapter.Fill(myDocStructure);

            //-- Session["KMMessage"]
            var strKmMessage = GetKmMessage(itemList.Tables[0], p);
			p.Session["KMMessage"] = strKmMessage;

            //-- Session["BatchNumberForEmail"]
            var strBatchNumberForEmail = SpGetBatch(int.Parse(batchId), p);
		    p.Session["BatchNumberForEmail"] = strBatchNumberForEmail;
			
            //-- Session["ShortReport"] - DataSet, where DataTable[0].Name = 'Report' (Structure * Value)
            var dtShortReport = GetShortReportFast(strBatchNumberForEmail, p, myDocStructure.Tables[0], itemList.Tables[0]);
			dtShortReport.TableName = "Report";
			
            var dsShortReport = new DataSet("Short Report");
			dsShortReport.Tables.Add(dtShortReport);

			//-----------Swithcoverpoint----
			dsShortReport = PassFailDataSet(dsShortReport);			
			p.Session["ShortReport"] = dsShortReport;
			conn.Close();
			return dsShortReport.Tables["Report"];

		}

		public static bool CompareColumnList(DataTable table1, DataTable table2)
		{
			if(table1.Columns.Count!=table2.Columns.Count)
			{
				return false;
			}
			for(int i = 0; i < table1.Columns.Count; i++)
			{
				if(table1.Columns[i].ColumnName!=table2.Columns[i].ColumnName)
				{
					return false;
				}
			}
			return true;
		}

        public static string FullItemNumberWithDots(int groupCode, int batchCode = 0, int itemCode = 0)
        {
            return FullItemNumber(groupCode, batchCode, itemCode, true);
        }

        public static string FullItemNumber(int groupCode, int batchCode = 0, int itemCode = 0, bool withDots = false)
        {
            return FillToFiveChars(groupCode) +
                   (batchCode == 0 ? "" : ((withDots ? "." : "") + FillToThreeChars(batchCode, groupCode))) +
                   (itemCode == 0 ? "" : ((withDots ? "." : "") + FillToTwoChars(itemCode)));
        }

        public static string FullItemNumber(string groupCode, string batchCode, string itemCode)
		{
			try
			{
				var intGroupCode = int.Parse(groupCode);
				var intBatchCode = int.Parse(batchCode);
				var intItemCode = int.Parse(itemCode);
				
				return FillToFiveChars(groupCode) + FillToThreeChars(batchCode, groupCode) + FillToTwoChars(itemCode);
			}
			catch(Exception)
			{
				return "-1";
			}
		}

        public static string FullBatchNumber(int groupCode, int batchCode, bool withDots = false)
        {
            return FullBatchNumber(groupCode.ToString(), batchCode.ToString(), withDots);
        }

        public static string FullBatchNumber(string groupCode, string batchCode, bool withDots = false)
		{
			try
			{
				var intGroupCode = int.Parse(groupCode);
				var strGroupCode = FillToFiveChars(groupCode); 

				var intBatchCode = int.Parse(batchCode);
				var strBatchCode = FillToThreeChars(batchCode, groupCode);

				return strGroupCode + (withDots ? "." : "") + strBatchCode;

			}
			catch(Exception)
			{
				return "-1";
			}
		}
		public static string FullGroupNumber(string groupCode)
		{
			try
			{
				var intGroupCode = int.Parse(groupCode);
				var strGroupCode = FillToFiveChars(groupCode); 
				return strGroupCode;
			}
			catch
			{
				return "-1";
			}

		}

        private static string FillToNChars(string sNumber, int n)
        {
            while (sNumber.Length < n)
                sNumber = "0" + sNumber;
            return sNumber;
        }

        public static string FillToFiveChars(string sNumber)
		{
			return FillToNChars(sNumber, 5);
		}

        private static string FillToFiveChars(int sNumber)
        {
            return FillToFiveChars(sNumber.ToString());
        }

        public static string FillToThreeChars(string batchCode, string orderCode)
		{
            if (orderCode.Trim().Length < 7)
            {
                return FillToNChars(batchCode, 3);
            }
            return FillToNChars(batchCode, 2);
		}

        private static string FillToThreeChars(int batchCode, int orderCode)
        {
            return FillToThreeChars(batchCode.ToString(), orderCode.ToString());
        }

        public static string FillToTwoChars(string sNumber)
		{
            return FillToNChars(sNumber, 2);
		}

        private static string FillToTwoChars(int sNumber)
        {
            return FillToTwoChars(sNumber.ToString());
        }

        public static string getPrevItemCode(DataTable dt, string newItemNumber)
        {
            foreach (DataRow dr in dt.Rows)
            {

                string prevOrder = dr["PrevGroupCode"].ToString();
                string prevBatch = dr["PrevBatchCode"].ToString();
                string prevItem = dr["PrevItemcode"].ToString();
                string prevNumber = Utils.FillToFiveChars(prevOrder) + Utils.FillToThreeChars(prevBatch, prevOrder) + Utils.FillToTwoChars(prevItem);

                string order = dr["GroupCode"].ToString();
                string batch = dr["BatchCode"].ToString();
                string item = dr["Itemcode"].ToString();
                string number = Utils.FillToFiveChars(order) + Utils.FillToThreeChars(batch, order) + Utils.FillToTwoChars(item);
                if (number == newItemNumber || prevNumber == newItemNumber)
                    return prevNumber;
            }
            return null;
        }
        public static string getPrevItemCodeNew(List<SingleItemModel> itemList, string newItemNumber, string matchedItemNumber = null)
        {
            if (matchedItemNumber != null)
            {
                foreach (SingleItemModel item in itemList)
                {
                    if (item.FullItemNumber == newItemNumber || item.FullOldItemNumber == newItemNumber || item.FullBatchNumber == newItemNumber || item.FullItemNumber == matchedItemNumber)
                        return item.FullOldItemNumber;
                }
            }
            else
            {
                foreach (SingleItemModel item in itemList)
                {
                    if (item.FullItemNumber == newItemNumber || item.FullOldItemNumber == newItemNumber || item.FullBatchNumber == newItemNumber)
                        return item.FullOldItemNumber;
                }
            }
                /*
                foreach (DataRow dr in dt.Rows)
                {

                    string prevOrder = dr["PrevGroupCode"].ToString();
                    string prevBatch = dr["PrevBatchCode"].ToString();
                    string prevItem = dr["PrevItemcode"].ToString();
                    string prevNumber = Utils.FillToFiveChars(prevOrder) + Utils.FillToThreeChars(prevBatch) + Utils.FillToTwoChars(prevItem);

                    string order = dr["GroupCode"].ToString();
                    string batch = dr["BatchCode"].ToString();
                    string item = dr["Itemcode"].ToString();
                    string number = Utils.FillToFiveChars(order) + Utils.FillToThreeChars(batch) + Utils.FillToTwoChars(item);
                    if (number == newItemNumber || prevNumber == newItemNumber)
                        return prevNumber;
                }
                */
                return null;
        }
        public static bool VerifyReport(string report, string virtualVault, System.Web.UI.WebControls.Label label, Page page)
		{

			label.Text = "";
			////////////////////////////REMOVE PROTECTION
			////////////////////////////			if (!Regex.IsMatch(report,@"^[A-Za-z](\d{10,10})$"))
			////////////////////////////			{
			////////////////////////////				label.Text = "Invalid Report Number";
			////////////////////////////				return false;
			////////////////////////////			}
			////////////////////////////
			////////////////////////////			if (!Regex.IsMatch(virtualVault,@"^[A-Za-z0-9]{10,10}$"))
			////////////////////////////			{
			////////////////////////////				label.Text = "Invalid Virtual Vault Number";
			////////////////////////////				return false;
			////////////////////////////			}


			page.Session["VirtualVaultNumber"] = virtualVault;
			page.Session["ReportNumber"] = report;

			////////////////////////////			int count = 0;
			////////////////////////////
			////////////////////////////			SqlConnection conn = new SqlConnection(page.Session["VVConnectionString"] as String);
			////////////////////////////			SqlCommand command = new SqlCommand("wspVerifyVirtualVault");
			////////////////////////////			command.Connection = conn;
			////////////////////////////
			////////////////////////////			/*
			////////////////////////////			 * 	@MeasureId	    int,
			////////////////////////////				@VirtualVaultNumber varchar(20),
			////////////////////////////				@OrderCode	    int,
			////////////////////////////				@BatchCode	    int,
			////////////////////////////				@ItemCode	    int,
			////////////////////////////				@OperationChar	    varchar(1)	
			////////////////////////////			*/
			////////////////////////////			command.CommandType = CommandType.StoredProcedure;
			////////////////////////////			command.Parameters.Add("@MeasureId",  Int32.Parse(page.Session["VirtualVaultID"] as string));
			////////////////////////////			command.Parameters.Add("@VirtualVaultNumber", virtualVault );
			////////////////////////////			command.Parameters.Add("@OrderCode", (Int32.Parse(report.Substring(1,5))) );
			////////////////////////////			command.Parameters.Add("@BatchCode", (Int32.Parse(report.Substring(6,3))) );
			////////////////////////////			command.Parameters.Add("@ItemCode", (Int32.Parse(report.Substring(9,2))) );
			////////////////////////////			command.Parameters.Add("@OperationChar", report.Substring(0,1) );
			////////////////////////////
			////////////////////////////			conn.Open();
			////////////////////////////			SqlDataReader reader = command.ExecuteReader();
			////////////////////////////
			////////////////////////////			if (reader.HasRows)
			////////////////////////////			{
			////////////////////////////				reader.Read();
			////////////////////////////				count = reader.GetInt32(0);
			////////////////////////////				if (count == 0)
			////////////////////////////					label.Text = "Incorrect ReportNumber/VirtualVault combination";
			////////////////////////////			}
			////////////////////////////
			////////////////////////////			reader.Close();

			// Ceck for report-existinguser disabled - Session["ID"] has different meaning then in the VV application.
			//////			if (page.Session["ID"] != null)
			//////			{
			//////				count = 1;
			//////
			//////				command.CommandText = "select * from VirtualVaultReportList where VVC_ID = @ID and ReportNumber = @ReportNumber and VirtualVaultNumber = @VirtualVaultNumber";
			//////				command.CommandType = CommandType.Text;
			//////
			//////				command.Parameters.Clear();
			//////				command.Parameters.Add("@ID", page.Session["ID"].ToString());
			//////				command.Parameters.Add("@ReportNumber", report );
			//////				command.Parameters.Add("@VirtualVaultNumber", virtualVault );
			//////
			//////				reader = command.ExecuteReader();
			//////
			//////				if (reader.HasRows)
			//////				{
			//////					count = 0;
			//////					label.Text = "You have already activated the VirtualVault for this report. Please follow <a href=\"RegisteredReports.aspx\">this</a> link to see the report in your VirtualVault.";
			//////				}
			//////
			//////				reader.Close();
			//////
			//////
			//////			}

			//////////////////////////////conn.Close();
			//////////////////////////////REMOVE PROTECTION
			//////////////////////////////			if (count == 0)
			//////////////////////////////			{
			//////////////////////////////				return false;
			//////////////////////////////			}
			//////////////////////////////
			//////////////////////////////			string fileName = page.Session["DocumentRoot"] + report.ToUpper() + "." + virtualVault.ToUpper() + ".PDF";
			//////////////////////////////			if (!File.Exists(fileName))
			//////////////////////////////			{
			//////////////////////////////				label.Text = "This report is temporary unavailable. Please check again later or contact GSI technical support";
			//////////////////////////////				return false;
			//////////////////////////////			}
			return true;
		}	
		
		public static DataTable DeDuper(DataTable dtS)
		{
			DataTable dtR = new DataTable();

			foreach(DataColumn dc in dtS.Columns)
			{
				dtR.Columns.Add(new DataColumn(dc.ColumnName,dc.DataType));
			}

			//			dtR.Rows.Add(dtS.Rows[0].ItemArray);

			foreach(DataRow dr in dtS.Rows)
			{
				bool exists = false;
				foreach(DataRow ddr in dtR.Rows)
				{
					if(copmareRows(ddr,dr))
					{
						exists = true;
					}
				}
				if(!exists)
				{
					dtR.Rows.Add(dr.ItemArray);
				}				
			}
			return dtR;
		}
		
		private static bool copmareRows(DataRow dr1, DataRow dr2)
		{
			for(int i=0; i<dr1.ItemArray.Length; i++)
			{
				if(dr1.ItemArray[i].ToString()!=dr2.ItemArray[i].ToString())
				{
					return false;
				}
			}
			return true;
		}
		
		public static string ReLink(string itemNumber)
		{
			return "<a href=\"ItemRedirector.aspx?ItemNumber="+itemNumber+"&mode=psx\">"+itemNumber+"</a>";
			//			return "";
		}

		public static bool DissectItemNumber(
            string itemNumber, 
            out int groupCode, 
            out int batchCode, 
            out int itemCode,
            Func<string, bool> isBigBatchCode = null)
		{
			try
			{
                var parser = new FullItemCodeParser(itemNumber, isBigBatchCode).Parse();
                groupCode = parser.OrderCode;
                batchCode = parser.BatchCode;
                itemCode = parser.ItemCode;
                return groupCode > 0;
			}
			catch(Exception e)
			{
				groupCode = 0;
				batchCode = 0;
				itemCode = 0;
				return false;
			}
		}

        public static bool DissectItemNumber(
            string itemNumber,
            out string groupCode,
            out string batchCode,
            out string itemCode,
            Func<string, bool> isBigBatchCode = null)
        {
            var isSuccess = DissectItemNumber(itemNumber, out int groupCodeInt, out int batchCodeInt, out int itemCodeInt, isBigBatchCode);
            groupCode = FillToFiveChars(groupCodeInt.ToString());
            batchCode = FillToThreeChars(batchCodeInt.ToString(), groupCodeInt.ToString());
            itemCode = FillToTwoChars(itemCodeInt.ToString());
            return isSuccess;
        }

        public static DataTable nbsp(DataTable dt)
		{
			return dt;
			foreach(DataRow dr in dt.Rows)
			{
				dr["Memo"]=dr["Memo"].ToString().Trim().Replace(" ","&nbsp;");
				dr["CustomerName"]=dr["CustomerName"].ToString().Trim().Replace(" ","&nbsp;");
			}



			//			return dt;

			DataTable rdt = new DataTable();
			
			foreach(DataColumn dc in dt.Columns)
			{
				switch(dc.ColumnName)
				{
					case "CreateDate":
					{
						rdt.Columns.Add(new DataColumn(dc.ColumnName));
						break;
					}
					case "LastModifiedDate":
					{
						rdt.Columns.Add(new DataColumn(dc.ColumnName));
						break;
					}
					default:
					{
						rdt.Columns.Add(new DataColumn(dc.ColumnName,dc.DataType));
						break;
					}
				}				
			}
			rdt.AcceptChanges();
	       
			foreach(DataRow dr in dt.Rows)
			{
				DataRow ddr = rdt.NewRow();
				foreach(DataColumn dc in dt.Columns)
				{
					switch(dc.ColumnName)
					{
						case "CreateDate":
						{
							ddr[dc.ColumnName]=dr[dc.ColumnName].ToString().Replace(" ","&nbsp;");
							break;
						}
						case "LastModifiedDate":
						{
							ddr[dc.ColumnName]=dr[dc.ColumnName].ToString().Replace(" ","&nbsp;");
							break;
						}
						default:
						{
							ddr[dc.ColumnName]=dr[dc.ColumnName];
							break;
						}
					}
				}
				rdt.Rows.Add(ddr);
			}
			return rdt;
		}

		public static string GetItemNumberColumnName(DataTable dtShortReportStructure)
		{
			var strItemNumberColumnName = "";
			foreach(DataRow drCp in dtShortReportStructure.Rows)
			{
				if(drCp["Value"].ToString().ToLower().Trim()=="[item container.item #]")
				{
					strItemNumberColumnName=drCp["Title"].ToString();
				}
			}
			return strItemNumberColumnName;
		}

		public static string GetKmMessage(DataTable dtItemList, Page p)
		{
			var strMessage = "";

			foreach(DataRow drItem in dtItemList.Rows)
			{
				var command = new SqlCommand
				{
                    CommandText = "spGetKMLDFFByItemNumber",
				    Connection = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString()),
				    CommandType = CommandType.StoredProcedure
                };

			    command.Parameters.Add(new SqlParameter("@GroupCode", drItem["OrderCode"].ToString()));
				command.Parameters.Add(new SqlParameter("@BatchCode", drItem["BatchCode"].ToString()));
				command.Parameters.Add(new SqlParameter("@ItemCode", drItem["ItemCode"].ToString()));

				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);

				foreach(DataRow drKm in dt.Rows)
				{
				    if (NullValue(drKm["MeasureValueName"]) == "") continue;
				    var itemNumber = FullItemNumber(
				        "" + drItem["OrderCode"],
				        "" + drItem["BatchCode"],
				        "" + drItem["ItemCode"]);
				    var myNumber = GetItemNumberWithDots(itemNumber);
				    strMessage += "Found: " + myNumber + ":" + drKm["MeasureValueName"]+"<br />";
				}
			}
			return strMessage;
		}		

		public static string GetCPIDbyBatchID(string strBatchID, Page p)
		{
			SqlCommand command = new SqlCommand("spGetCustomerProgramByBatchID");
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());

			command.Parameters.Add(new SqlParameter("@BatchID",strBatchID));
			command.Parameters.Add(new SqlParameter("@AuthorID", p.Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString()));

			SqlDataAdapter da = new SqlDataAdapter(command);

			DataTable dtCP = new DataTable();
			da.Fill(dtCP);

			//			dgDebug.DataSource=dtCP;
			//			dgDebug.DataBind();

			if(dtCP.Rows.Count>0)
			{
				string strCPID = dtCP.Rows[0]["CPID"].ToString();
				return strCPID;
			}
			else
			{
				Exception ex = new Exception("Can't Find CPID by BatchID");
			}
			return "";
		}

		public static DataTable GetCpDocsByCPID(string strCPID, Page p)
		{
			SqlCommand spGetCPDocs = new SqlCommand("spGetCPDocs");
			spGetCPDocs.CommandType = CommandType.StoredProcedure;
			spGetCPDocs.Connection = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());

			spGetCPDocs.Parameters.Add(new SqlParameter("@CPOfficeID", p.Session["AuthorOfficeID"].ToString()));
			spGetCPDocs.Parameters.Add(new SqlParameter("@CPID", strCPID));
			spGetCPDocs.Parameters.Add(new SqlParameter("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString()));
			spGetCPDocs.Parameters.Add(new SqlParameter("@AuthorID", p.Session["ID"].ToString()));

			SqlDataAdapter daspGetCPDocs = new SqlDataAdapter(spGetCPDocs);
			DataTable dtspGetCPDocs = new DataTable();
			daspGetCPDocs.Fill(dtspGetCPDocs);

			return dtspGetCPDocs;				
		}

		public static DataTable GetCPDocMeasuresByCPDoc(string strCPDocID, Page p)
		{
			SqlCommand spGetCPDocRule = new SqlCommand("spGetCPDocRule");
			spGetCPDocRule.CommandType=CommandType.StoredProcedure;
			spGetCPDocRule.Connection=new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());

			//string strCPDocID = dr["CPDocID"].ToString();

			spGetCPDocRule.Parameters.Add(new SqlParameter("@CPDocID", strCPDocID));
			spGetCPDocRule.Parameters.Add(new SqlParameter("@AuthorID", p.Session["ID"].ToString()));
			spGetCPDocRule.Parameters.Add(new SqlParameter("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString()));

			SqlDataAdapter daspGetCPDocRule = new SqlDataAdapter(spGetCPDocRule);
			DataTable dtCPDocRule = new DataTable();

			daspGetCPDocRule.Fill(dtCPDocRule);

			return dtCPDocRule;
		}
        /// <summary>
        /// Gets the full item number with dots.
        /// </summary>
        /// <param name="sFullItemNumber">The full item number(Order, Batch and Item codes concatenation.</param>
        /// <param name="isBigBatchCode">Function that answers on question: is GroupCode+BatchCode big batch code. For unit tests only</param>
        /// <returns></returns>
        public static string GetItemNumberWithDots(string sFullItemNumber,
            Func<string, bool> isBigBatchCode = null)
        {
            return GetFullItemNumber(sFullItemNumber, isBigBatchCode, true, false);
        }
        public static string GetItemNumberWithNoDots(string sFullItemNumber,
            Func<string, bool> isBigBatchCode = null)
        {
            return GetFullItemNumber(sFullItemNumber, isBigBatchCode, false, true);
        }

        private static string GetFullItemNumber(
            string sFullItemNumber, 
            Func<string, bool> isBigBatchCode, 
            bool isDotRequired,
            bool hasZeroCodes)
        {
            var separator = isDotRequired ? "." : "";
            var fullItemNumberText = sFullItemNumber.Trim().Replace(".", "");
            var parser = new FullItemCodeParser(fullItemNumberText, isBigBatchCode).Parse();
            if (!hasZeroCodes && (parser.OrderCode == 0 || parser.BatchCode == 0 || parser.ItemCode == 0))
            {
                return string.Empty;
            }

            return $"{FillToFiveChars(parser.OrderCode)}{separator}" +
                   $"{FillToThreeChars(parser.BatchCode, parser.OrderCode)}{separator}" +
                   $"{FillToTwoChars(parser.ItemCode)}";
        }

        public static string GetMyIP_Group()
		{
			string[] myIP1 = null;
			string sIPaddress = "";
			string[] sIP = null;
			string sIP_Group = "";
            
			Process p;
			try
			{
				p = new Process();
				p.StartInfo.FileName = "IPCONFIG";
				p.StartInfo.UseShellExecute = false;
				//p.StartInfo.Arguments = "/renew";
				//                //p.StartInfo.RedirectStandardOutput = true;
				//                p.Start();
				//                p.WaitForExit();
				//                p.Kill();
				//
				//                p = new Process();
				//                p.StartInfo.FileName = "IPCONFIG";
				p.StartInfo.UseShellExecute = false;
				//p.StartInfo.Arguments = "/all";
				p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				p.StartInfo.RedirectStandardOutput = true;
				p.Start();
				p.WaitForExit();
                
				myIP1 = Regex.Split(p.StandardOutput.ReadToEnd().ToString().Trim(), "\r\n");
				
				foreach (string line in myIP1)
				{
					//if (line.ToUpper().IndexOf("ETHERNET ADAPTER LOCAL") >= 0) bGetIP = true;
					if (line.ToUpper().IndexOf("IP ADDRESS") >= 0 || line.ToUpper().IndexOf("IPV4 ADDRESS") >= 0) // && bGetIP)
					{
						sIPaddress = line.Substring(line.IndexOf(":") + 1).Trim();
						if (sIPaddress.IndexOf("192.168")>= 0) 
							break;
					}
				}

				//                foreach (string line in myIP1)
				//                {
				//                  sIP_AddressTest = sIP_AddressTest + "\n" + line.Trim(); 
				//                }
				if (sIPaddress.Trim() != "")
				{
					sIP = sIPaddress.Split('.');
					sIP_Group = sIP[2].Trim();
					return sIP_Group;
				}
				return sIP_Group;
			}
			catch
			{
				throw new Exception("Can't find IP address. Restart program");
			}
		}
        public static List<string> GetOriginalItemListValues(DataTable dt, List<SingleItemModel> itemList, string newItemNumber, string matchedItemNumber = null)
        {
            bool oldMatch = false;
            try
            {
                foreach (SingleItemModel item in itemList)
                {
                    if (item.FullItemNumber == newItemNumber || item.FullBatchNumber == newItemNumber)//alex added batch
                    {
                        oldMatch = true;
                        break;
                    }
                }
                if (!oldMatch)
                {
                    foreach (SingleItemModel item in itemList)
                    {
                        if (item.FullOldItemNumber == newItemNumber)
                        {
                            oldMatch = true;
                            break;
                        }
                    }
                }
                if (!oldMatch && matchedItemNumber != null)
                {
                    foreach (SingleItemModel item in itemList)
                    {
                        if (item.FullItemNumber == matchedItemNumber)
                        {
                            oldMatch = true;
                            break;
                        }
                    }
                }
                if (oldMatch)
                {
                    List<string> newList = new List<string>();
                    foreach (SingleItemModel item in itemList)
                        newList.Add(item.FullOldItemNumber);
                    return newList;
                }
            }

            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            /*
            try
            {
                foreach (DataRow dr in dt.Rows)
                {

                    string Order = dr["GroupCode"].ToString();
                    string Batch = dr["BatchCode"].ToString();
                    string Item = dr["Itemcode"].ToString();
                    string Number = Utils.FillToFiveChars(Order) + Utils.FillToThreeChars(Batch) + Utils.FillToTwoChars(Item);
                    if (Number == newItemNumber)
                    {
                        oldMatch = true;
                        break;
                    }
                }
                if (!oldMatch)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string PrevOrder = dr["PrevGroupCode"].ToString();
                        string PrevBatch = dr["PrevBatchCode"].ToString();
                        string PrevItem = dr["PrevItemcode"].ToString();
                        string Number = Utils.FillToFiveChars(PrevOrder) + Utils.FillToThreeChars(PrevBatch) + Utils.FillToTwoChars(PrevItem);
                        if (Number == newItemNumber)
                        {
                            oldMatch = true;
                            break;
                        }
                    }
                }
                if (oldMatch)
                {
                    List<string> newList = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {

                        string prevOrder = dr["PrevGroupCode"].ToString();
                        string prevBatch = dr["PrevBatchCode"].ToString();
                        string prevItem = dr["PrevItemcode"].ToString();
                        string prevNumber = Utils.FillToFiveChars(prevOrder) + Utils.FillToThreeChars(prevBatch) + Utils.FillToTwoChars(prevItem);
                        newList.Add(prevNumber);
                    }
                    return newList;
                }
                return null;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            */
            return null;
        }
    }
}