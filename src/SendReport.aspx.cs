using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;
//using ExcelUtil;

namespace Corpt
{
	/// <summary>
	/// Summary description for SendReport.
	/// </summary>
	public partial class SendReport : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			if(Request.QueryString["CID"] !=null && Request.QueryString["COID"] !=null && Session["ListDone"] == null)
			{
				var conn = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
                conn.Open();

				Session["SendReport_CID"]   = Request.QueryString["CID"];
				Session["SendReport_COID"]  = Request.QueryString["COID"];

				var command = new SqlCommand {CommandText = "spGetPersonsByCustomer", CommandType = CommandType.StoredProcedure, Connection = conn};
			    command.Parameters.AddWithValue("@CustomerOfficeID",Request.QueryString["COID"]);
                command.Parameters.AddWithValue("@CustomerID", Request.QueryString["CID"]);
                command.Parameters.AddWithValue("@AuthorID", Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", Session["AuthorOfficeID"]);
				
				var reader = command.ExecuteReader();
				if(reader.HasRows)
				{
					PersonList = new ArrayList();
					PersonList.Clear();
					while(reader.Read())
					{
					    var person = new Person
                        {
					        FirstName   = Utils.NullValue(reader.GetSqlValue(reader.GetOrdinal("FirstName"))),
                            LastName    = Utils.NullValue(reader.GetSqlValue(reader.GetOrdinal("LastName"))),
                            Email       = Utils.NullValue(reader.GetSqlValue(reader.GetOrdinal("email"))),
                            PersonId    = Utils.NullValue(reader.GetSqlValue(reader.GetOrdinal("PersonID")))

					    };
						PersonList.Add(person);
					}
					lstPersons.Items.Clear();
					foreach(Person p in PersonList)
					{
						lstPersons.Items.Add(new ListItem(p.ToString()+" <"+p.Email+">", p.Email));
					}
					Session["ListDone"]="Done";
				}
				conn.Close();
				lstPersons_SelectedIndexChanged(sender, e);
			}
		}

		private void SendShortReport(String email, DataSet report)
		{
			Directory.CreateDirectory(Session["TempDir"].ToString() + Session.SessionID + @"\");
			
			Utils.SaveExcell(report,this);

			MailMessage myEmail;
			myEmail = new MailMessage();

			myEmail.From="CustomerCare@gemscience.net";
			myEmail.BodyFormat=MailFormat.Html;
			myEmail.BodyEncoding=System.Text.Encoding.UTF8;
			myEmail.Attachments.Add(new MailAttachment(Session["TempDir"].ToString() + Session.SessionID + @"\" + "Report.xls" ,MailEncoding.Base64));
#if DEBUG		
			myEmail.To=email;
#else
			myEmail.To=email;
#endif

			myEmail.Body="See attached files.<br><i>Gemological Science International.</i>";

			myEmail.Subject="Report from GSI";

			SmtpMail.SmtpServer="sbssql.gsi.local";
			SmtpMail.Send(myEmail);

			File.Delete(Session["TempDir"].ToString() + Session.SessionID + @"\"+"Report.xls");
			Directory.Delete(Session["TempDir"].ToString() + Session.SessionID + @"\");
		}
		
		
		private ArrayList PersonList;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		protected void cmdReList_Click(object sender, System.EventArgs e)
		{
			Session["ListDone"]=null;
			Response.Redirect("SendReport.aspx?CID="+Session["SendReport_CID"].ToString()+"&COID="+Session["SendReport_COID"].ToString());
		}

		protected void lstPersons_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lnkEmailThis.Text="Email to " + lstPersons.SelectedValue;
			lnkEmailThis.NavigateUrl="javascript:poptastic('EmailSent.aspx?Email=" + lstPersons.SelectedValue + "');";
		}

		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			cmdReList_Click(sender, e);
		}
	}
}
