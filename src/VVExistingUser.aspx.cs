using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for VVExistingUser.
	/// </summary>
	public partial class VVExistingUser : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["ID"]==null)
				Response.Redirect("Login.aspx");
			if (!IsPostBack)
				CreateStateList();

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.tblSearchResult.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.tblSearchResult_SortCommand);

		}
		#endregion

		private void DisplayMessage(String message)
		{
			pnlQ.Visible=true;
			pnlQ.Enabled=true;
			pnlQ.CssClass="MessageBackground";
			pnlM.Visible=true;
			pnlM.Enabled=true;
			lblMessage.Text=message;
		}
		
		private void HideMessage()
		{
			pnlQ.Visible=false;
			pnlQ.Enabled=false;
			
			pnlM.Visible=false;
			pnlM.Enabled=false;
			lblMessage.Text="";
		}
		
		private void DisplayQuestion(String question)
		{
			pnlQ.Visible=true;
			pnlQ.Enabled=true;
			pnlQ2.Visible=true;
			pnlQ2.Enabled=true;
			pnlQ.CssClass="MessageBackground";
			lblQuestion.Text= question;
		}
		private void HideQuestion()
		{
			pnlQ.Visible=false;
			pnlQ.Enabled=false;
			pnlQ2.Visible=false;
			pnlQ2.Enabled=false;			
			lblQuestion.Text="";
		}

		private void ClearForm()
		{
			FirstName.Text="";
			MiddleInitial.Text="";
			LastName.Text="";
			Address.Text="";
			City.Text="";
			ZipCode.Text="";
			State.SelectedIndex=-1;
			DayPhone.Text="";
			OtherPhone.Text="";
			EmailAddress.Text="";
		}

		protected void cmdNo_Click(object sender, System.EventArgs e)
		{
			HideQuestion();
			ClearForm();
		}

		protected void cmdYes_Click(object sender, System.EventArgs e)
		{
//			if(Session["ExistingUser"]!=null)
//			{
//				//DisplayUser(Session["ExistingUser"] as DataTable);
//			}
//			if(Session["NewUser"]!=null)
//			{
//				if(NewUser()==0)
//				{
//					HideQuestion();
//					DisplayMessage("Save Succeeded.");
//					//DisplayUser(Session["NewUser"] as DataTable);
//				}
//				else
//				{
//					Session["ExistingUser"]=null;
//					Session["NewUser"]=null;
//					HideQuestion();
//					DisplayMessage("Save Failed.");
//				}
//			}
		}

		protected void cmdOK_Click(object sender, System.EventArgs e)
		{
			HideMessage();
		}

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			DisplayQuestion("2b|!2b");
		}

		protected void Button2_Click(object sender, System.EventArgs e)
		{
			DisplayMessage("msg");
		}

		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("VVAdmin.aspx");
		}

		private void CreateStateList()
		{

			DataTable dt = new DataTable();
	         
			// Define the columns of the table.
			dt.Columns.Add(new DataColumn("StateCode", typeof(String)));
			dt.Columns.Add(new DataColumn("StateName", typeof(String)));


			dt.Rows.Add(CreateRow("","Select Your State",dt));
			dt.Rows.Add(CreateRow("AL","Alabama",dt));
			dt.Rows.Add(CreateRow("AK","Alaska",dt));
			dt.Rows.Add(CreateRow("AZ","Arizona",dt));
			dt.Rows.Add(CreateRow("AR","Arkansas",dt));
			dt.Rows.Add(CreateRow("CA","California",dt));
			dt.Rows.Add(CreateRow("CO","Colorado",dt));
			dt.Rows.Add(CreateRow("CT","Connecticut",dt));
			dt.Rows.Add(CreateRow("DE","Delaware",dt));
			dt.Rows.Add(CreateRow("DC","District of Columbia",dt));
			dt.Rows.Add(CreateRow("FL","Florida",dt));
			dt.Rows.Add(CreateRow("GA","Georgia",dt));
			dt.Rows.Add(CreateRow("HI","Hawaii",dt));
			dt.Rows.Add(CreateRow("ID","Idaho",dt));
			dt.Rows.Add(CreateRow("IL","Illinois",dt));
			dt.Rows.Add(CreateRow("IN","Indiana",dt));
			dt.Rows.Add(CreateRow("IA","Iowa",dt));
			dt.Rows.Add(CreateRow("KS","Kansas",dt));
			dt.Rows.Add(CreateRow("KY","Kentucky",dt));
			dt.Rows.Add(CreateRow("LA","Louisiana",dt));
			dt.Rows.Add(CreateRow("ME","Maine",dt));
			dt.Rows.Add(CreateRow("MD","Maryland",dt));
			dt.Rows.Add(CreateRow("MA","Massachusetts",dt));
			dt.Rows.Add(CreateRow("MI","Michigan",dt));
			dt.Rows.Add(CreateRow("MN","Minnesota",dt));
			dt.Rows.Add(CreateRow("MS","Mississippi",dt));
			dt.Rows.Add(CreateRow("MO","Missouri",dt));
			dt.Rows.Add(CreateRow("MT","Montana",dt));
			dt.Rows.Add(CreateRow("NE","Nebraska",dt));
			dt.Rows.Add(CreateRow("NV","Nevada",dt));
			dt.Rows.Add(CreateRow("NH","New Hampshire",dt));
			dt.Rows.Add(CreateRow("NJ","New Jersey",dt));
			dt.Rows.Add(CreateRow("NM","New Mexico",dt));
			dt.Rows.Add(CreateRow("NY","New York",dt));
			dt.Rows.Add(CreateRow("NC","North Carolina",dt));
			dt.Rows.Add(CreateRow("ND","North Dakota",dt));
			dt.Rows.Add(CreateRow("OH","Ohio",dt));
			dt.Rows.Add(CreateRow("OK","Oklahoma",dt));
			dt.Rows.Add(CreateRow("OR","Oregon",dt));
			dt.Rows.Add(CreateRow("PA","Pennsylvania",dt));
			dt.Rows.Add(CreateRow("PR","Puerto Rico",dt));
			dt.Rows.Add(CreateRow("RI","Rhode Island",dt));
			dt.Rows.Add(CreateRow("SC","South Carolina",dt));
			dt.Rows.Add(CreateRow("SD","South Dakota",dt));
			dt.Rows.Add(CreateRow("TN","Tennessee",dt));
			dt.Rows.Add(CreateRow("TX","Texas",dt));
			dt.Rows.Add(CreateRow("UT","Utah",dt));
			dt.Rows.Add(CreateRow("VT","Vermont",dt));
			dt.Rows.Add(CreateRow("VI","Virgin Islands",dt));
			dt.Rows.Add(CreateRow("VA","Virginia",dt));
			dt.Rows.Add(CreateRow("WA","Washington",dt));
			dt.Rows.Add(CreateRow("WV","West Virginia",dt));
			dt.Rows.Add(CreateRow("WI","Wisconsin",dt));
			dt.Rows.Add(CreateRow("WY","Wyoming",dt));

			//Canada
			dt.Rows.Add(CreateRow("AB","Alberta",dt));
			dt.Rows.Add(CreateRow("BC","British Columbia",dt));
			dt.Rows.Add(CreateRow("MB","Manitoba",dt));
			dt.Rows.Add(CreateRow("NB","New Brunswick",dt));
			dt.Rows.Add(CreateRow("NL","Newfoundland and Labrador",dt));
			dt.Rows.Add(CreateRow("NS","Nova Scotia",dt));
			dt.Rows.Add(CreateRow("ON","Ontario",dt));
			dt.Rows.Add(CreateRow("PE","Prince Edward Island",dt));
			dt.Rows.Add(CreateRow("QC","Quebec",dt));
			dt.Rows.Add(CreateRow("SK","Saskatchewan",dt));
			dt.Rows.Add(CreateRow("NT","Northwest Territories",dt));
			dt.Rows.Add(CreateRow("NU","Nunavut",dt));
			dt.Rows.Add(CreateRow("YT","Yukon",dt));			

			State.DataSource = new DataView(dt);
			State.DataTextField = "StateName";
			State.DataValueField = "StateCode";
			State.DataBind();
		}
		DataRow CreateRow(String Text, String Value, DataTable dt)
		{
			DataRow dr = dt.NewRow();
 
			dr[0] = Text;
			dr[1] = Value; 
			return dr;
		}

		protected void cmdSearch_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn = new SqlConnection(Session["VVConnectionString"] as String);

			SqlCommand command = new SqlCommand("spFind");
			command.Connection = conn;

			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@ReportNumber", SqlDbType.VarChar).Size = 10;
			if(txtSearchReportNumber.Text.Trim()=="")
			{
				command.Parameters["@ReportNumber"].Value=DBNull.Value;
			}
			else
			{
				command.Parameters["@ReportNumber"].Value=Regex.Replace(txtSearchReportNumber.Text.Trim(),"\\*","%");
			}

			command.Parameters.Add("@VirtualVaultNumber", SqlDbType.VarChar).Size = 10;
			if(txtSearchVirtualVaultNumber.Text.Trim()=="")
			{
				command.Parameters["@VirtualVaultNumber"].Value=DBNull.Value;
			}
			else
			{
				command.Parameters["@VirtualVaultNumber"].Value=Regex.Replace(txtSearchVirtualVaultNumber.Text.Trim(),"\\*","%");
			}

			command.Parameters.Add("@Email", SqlDbType.VarChar).Size = 255;
			if(txtSearchEmail.Text.Trim()=="")
			{
				command.Parameters["@Email"].Value=DBNull.Value;
			}
			else
			{
				command.Parameters["@Email"].Value=Regex.Replace(txtSearchEmail.Text.Trim(),"\\*","%");
			}

			command.Parameters.Add("@FirstName", SqlDbType.VarChar).Size = 255;
			if(txtSearchFirstName.Text.Trim()=="")
			{
				command.Parameters["@FirstName"].Value=DBNull.Value;
			}
			else
			{
				command.Parameters["@FirstName"].Value=Regex.Replace(txtSearchFirstName.Text.Trim(),"\\*","%");
			}
            
			command.Parameters.Add("@LastName", SqlDbType.VarChar).Size = 255;
			if(txtSearchLastName.Text.Trim()=="")
			{
				command.Parameters["@LastName"].Value=DBNull.Value;
			}
			else
			{
				command.Parameters["@LastName"].Value=Regex.Replace(LastName.Text.Trim(),"\\*","%");
			}

			try
			{
				DataTable dt = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);
				da.Fill(dt);
				if(dt.Rows.Count>0)
				{
					tblSearchResult.DataSource=dt;
					tblSearchResult.DataBind();
					DisplayMessage("Some uers found.");
				}
				else
				{
					DisplayMessage("Nothing found.");
					tblSearchResult.DataSource=null;
					tblSearchResult.DataBind();
				}
			}
			catch(Exception ex)
			{
				DisplayMessage("Error while searching.");
			}
	
		}

		protected void tblSearchResult_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void tblSearchResult_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			DisplayMessage(e.SortExpression+" "+e.CommandSource);
		}
	}
}
