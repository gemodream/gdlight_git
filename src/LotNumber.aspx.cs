﻿using System.Data;
using System.Data.SqlClient;

namespace Corpt
{
    public partial class LotNumber : CommonPage
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
			Page.Title = "GSI: Find by Lot Number";
            if (!IsPostBack)
            {
                txtLotNumber.Focus();
            }
        }

        protected void ActivateButtonClick(object sender, System.EventArgs e)
        {
            Page.Validate("LotGroup");
            if (!Page.IsValid) return;
            tblLotNumberResult.Visible = false;

            var conn = new SqlConnection(""+Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand("sp_FindItemByLotNumber")
                              {Connection = conn, CommandType = CommandType.StoredProcedure};

            if (chkStrict.Checked)
            {
                command.Parameters.AddWithValue("@LotNumber", txtLotNumber.Text.Trim());
            }
            else
            {
                command.Parameters.AddWithValue("@LotNumber", "%" + txtLotNumber.Text.Trim() + "%");
            }

            var reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                conn.Close();
                InfoLabel.Text = "No such Lot Numbers. Try entering a part of the number.";
                return;
            }
            tblLotNumberResult.DataSource = reader;
            tblLotNumberResult.DataBind();
            InfoLabel.Text = "";
            tblLotNumberResult.Visible = true;

            conn.Close();
        }

        protected void ShipButtonClick(object sender, System.EventArgs e)
        {
            InfoLabel.Text = "";
            string carrierName = "", trackingNumber = "", url = "";
            if (shipOrderBox.Text == "")
            {
                InfoLabel.Text = "Enter order.";
            }
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spFindShipInfoByOrderNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderNumber", shipOrderBox.Text.Trim());
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0].ItemArray[0].ToString() == "No Carrier")
                    InfoLabel.Text = "No carrier for " + shipOrderBox.Text;
                else
                {
                    carrierName = dt.Rows[0]["CarrierName"].ToString();
                    trackingNumber = dt.Rows[0]["CarrierTrackingNumber"].ToString();
                    if (carrierName.ToLower() == "fedex")
                    {
                        url = @"https://www.fedex.com/fedextrack/?trknbr=" + trackingNumber + @"&trkqual=";
                        Response.Redirect(url);
                    }
                    else if (carrierName.ToLower() == "ups")
                    {
                            url = @"https://www.ups.com/track?track=yes&trackNums=" + trackingNumber + @"&loc=en_US&requester=ST/trackdetails";
                            Response.Redirect(url);
                    }
                }
            }
        }
    }
}