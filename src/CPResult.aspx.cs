﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CPResult : CommonPage
    {
        private const string PageName = "CPResult.aspx";

        private const string ParamFlag = "Flag";
        private const string ParamBatch = "BatchId";
        private const string ParamItem = "ItemNumber";

        private const string FlagBatch = "NewBatch";
        private const string FlagItem = "OneItem";

        private const string StatusFailed = "Failed";
        private const string StatusOk = "OK to Print";
        private const string StatusMigrate = "Migrated to new #";

        const string HLinkDetailsOneItem = PageName + "?ItemNumber={0}" + "&Flag=OneItem";
        const string HLinkDetailsAllItems ="ItemView.aspx?BatchId={0}" + "&All=1";
        private const string HLinkBatchNumber = PageName + "?BatchId={0}" + "&Flag=NewBatch";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: CP Rules";
			BatchId.Focus();
            if (Request.QueryString[ParamFlag] != null)
            {
                switch (Request.QueryString[ParamFlag])
                {
                    case FlagBatch:
                        Session["batchNumber"] = Request.QueryString[ParamBatch];
                        BatchId.Text = "" + Session["batchNumber"];
                        btnOpenCP.Visible = true;
                        ShowShort();
                        break;
                    case FlagItem:

                        Session["batchNumber"] =
                                (Request.QueryString[ParamItem].Split('.'))[0] +
                                (Request.QueryString[ParamItem].Split('.'))[1];
                        if (BatchId.Text == "")
                        {
                            BatchId.Text = "" + Session["batchNumber"];
                        }
                        btnOpenCP.Visible = true;
                        ShowShort();
                        DisplayDetailsData(Request.QueryString[ParamItem]);
                       
                        break;
                }
            }
        }
        private void ShowShort()
        {
            if (Session["batchNumber"] == null || !Regex.IsMatch(Session["batchNumber"].ToString(), @"^\d{8}$|^\d{9}$"))
            {
                WrongInfoLabel.Text = "Invalid Batch Number";
                return;
            }
            WrongInfoLabel.Text = "";
            var batchNumber = "" + Session["batchNumber"];

            //check if batch exists
            var batchKey = QueryUtils.GetBatchId(batchNumber, this);
            if (batchKey == "")
            {
                WrongInfoLabel.Text = "This batch number is invalid. Please check the batch number and try again.";
                return;
            }

            var toShowShortReport = false;
            if (batchKey != "")
            {
                String someValue;
                toShowShortReport = Utils.IsShortReport(batchKey, this, out someValue);
            }

            var itemRulesList = QueryUtils.GetItemRules(batchNumber, this);
            if (itemRulesList.Count == 0)
            {
                WrongInfoLabel.Text = "Everything Pass.";
                var sLink = string.Format(HLinkDetailsAllItems, batchKey);
                Response.Redirect(sLink);
                return;
            }

            var table = new DataTable("CP Rules");
            table.Columns.Add(new DataColumn("Item Number"));
            table.Columns.Add(new DataColumn("Original Item Number"));
            table.Columns.Add(new DataColumn("Rule Number"));
            table.Columns.Add(new DataColumn("Status"));

            foreach(var itemRoleModel in itemRulesList)
            {
                var row = table.NewRow();
                //-- Column 'Item Number'
                if (toShowShortReport)
                {
                    //-- HyperLink on ItemView.aspx with parameters BatchId, Item Code, Item Number
                    var sLink = string.Format(PageConstants.LinkOnPageItemViewDetail,
                        batchKey, itemRoleModel.ItemNumber.Split('.')[2], itemRoleModel.ItemNumber);
                    row["Item Number"] = "<a href=" + sLink + ">" + itemRoleModel.ItemNumber + "</a>";
                }
                else
                {
                    //-- Item Number only
                    row["Item Number"] = itemRoleModel.ItemNumber;
                }

                row["Rule Number"] = itemRoleModel.RulesNumber;
                row["Original Item Number"] = itemRoleModel.OldItemNumber;

                //-- Column 'Status'
                if (itemRoleModel.IsFailed)
                {
                    row["Status"] = "<a href=" + string.Format(HLinkDetailsOneItem, itemRoleModel.ItemNumber) + ">" + itemRoleModel.Status + "</a>";
                }

                if(itemRoleModel.IsMigrate)
                {
                    var sLink = string.Format(HLinkDetailsOneItem, itemRoleModel.NewItemNumber);
                    row["Status"] = itemRoleModel.Status + "<br>" + "<a href=" + sLink + ">" + itemRoleModel.NewItemNumber + "</a>";
                }
                if (itemRoleModel.IsOk)
                {
                    row["Status"] = itemRoleModel.Status;
                }
                table.Rows.Add(row);
            }
            table.AcceptChanges();
            cpRulesTrackingResult.DataSource = new DataView{Table = table};
            cpRulesTrackingResult.DataBind();
            cpRulesTrackingResult.Visible = true;
            cmdDetail.Visible = true;
            CpResultDetail.Visible = false;


            if (toShowShortReport)
            {
                lnkShortReport.Visible = true;
                lnkShortReport.NavigateUrl = "ItemView.aspx?BatchId=" + batchKey + "&All=1";
            }
            else
            {
                lnkShortReport.Visible = false;
            }


        }

        protected void OnOpenCPClick(object sendier, EventArgs e)
        {
            if (Session["batchNumber"] == null || !Regex.IsMatch(Session["batchNumber"].ToString(), @"^\d{8}$|^\d{9}$"))
            {
                WrongInfoLabel.Text = "Invalid Batch Number";
                return;
            }
            var batchNumber = "" + Session["batchNumber"];
            Response.Redirect("CustomerProgramNew.aspx?BatchCode=" + batchNumber);
        }
        protected void OnDetailDataItemBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#F5F5F5'");
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'");
            }
        }
        protected void OnDataItemBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var status = "" + ((DataRowView)e.Item.DataItem).Row[3];
                if (e.Item.Cells.Count > 2)
                {
                    if (status.IndexOf(StatusFailed, StringComparison.Ordinal) != -1)
                    {
                        e.Item.Cells[3].BackColor = Color.FromArgb(252, 248, 227); //Color.FromArgb(242, 222, 222);
                    }
                    if (status.IndexOf(StatusOk, StringComparison.Ordinal) != -1)
                    {
                        e.Item.Cells[3].BackColor = Color.FromArgb(208, 233, 198);
                    }
                    if (status.IndexOf(StatusMigrate, StringComparison.Ordinal) != -1)
                    {
                        e.Item.Cells[3].BackColor = Color.FromArgb(196, 227, 243);
                    }

                }

                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#F5F5F5'");
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'");
                e.Item.Cells[1].CssClass = "text";
            }
        }

        protected void OnDetailAllClick(object sender, EventArgs e)
        {
            ShowShort();
            DisplayDetailsData("");
        }

        private void DisplayDetailsData(String itemNumber)
        {
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand { Connection = conn, CommandText = "sp_RulesTracking24", CommandType = CommandType.StoredProcedure };

            command.Parameters.AddWithValue("@BatchFullNumber", Session["batchNumber"] + "00");
            command.Parameters.AddWithValue("@ShowRules", "0");

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    reader.NextResult();
                    if (reader.HasRows)
                    {
                        var dataView = new DataView();
                        var dataTable = new DataTable("CP Rules Details");


                        var dataColumn = new DataColumn { ColumnName = "Item Number" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Part Name" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Measure Name" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Min Value" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Max Value" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Value" };
                        dataTable.Columns.Add(dataColumn);

                        dataColumn = new DataColumn { ColumnName = "Rule Number" };
                        dataTable.Columns.Add(dataColumn);

                        dataView.Table = dataTable;

                        var dataRow = dataTable.NewRow();

                        while (reader.Read())
                        {

                            if (reader.GetString(reader.GetOrdinal("ItemNumber")) == itemNumber)
                            {
                                dataRow["Item Number"] = reader.GetString(reader.GetOrdinal("ItemNumber"));
                                dataRow["Part Name"] = reader.GetString(reader.GetOrdinal("PartName"));
                                dataRow["Measure Name"] = reader.GetString(reader.GetOrdinal("MeasureName"));
                                dataRow["Min Value"] = reader.GetSqlValue(reader.GetOrdinal("Min_Value")).ToString();
                                dataRow["Max Value"] = reader.GetSqlValue(reader.GetOrdinal("Max_Value")).ToString();
                                dataRow["Value"] = reader.GetSqlValue(reader.GetOrdinal("RealValue")).ToString();
                                dataRow["Rule Number"] = reader.GetSqlValue(reader.GetOrdinal("RulesNumber")).ToString();

                                dataView.Table = dataTable;

                                dataTable.Rows.Add(dataRow);
                                dataRow = dataTable.NewRow();
                            }
                            else
                            {
                                if (itemNumber == "")
                                {
                                    dataRow["Item Number"] = reader.GetString(reader.GetOrdinal("ItemNumber"));
                                    dataRow["Part Name"] = reader.GetString(reader.GetOrdinal("PartName"));
                                    dataRow["Measure Name"] = reader.GetString(reader.GetOrdinal("MeasureName"));
                                    dataRow["Min Value"] = reader.GetSqlValue(reader.GetOrdinal("Min_Value")).ToString();
                                    dataRow["Max Value"] = reader.GetSqlValue(reader.GetOrdinal("Max_Value")).ToString();
                                    dataRow["Value"] = reader.GetSqlValue(reader.GetOrdinal("RealValue")).ToString();
                                    dataRow["Rule Number"] = reader.GetSqlValue(reader.GetOrdinal("RulesNumber")).ToString();

                                    dataView.Table = dataTable;

                                    dataTable.Rows.Add(dataRow);
                                    dataRow = dataTable.NewRow();
                                }
                            }
                        }

                        CpResultDetail.DataSource = dataTable;
                        CpResultDetail.DataBind();
                        CpResultDetail.Visible = true;
                        //cmdDetail.Visible=false;
                    }
                }
            }
        }

        protected void OnSubmitClick(object sender, EventArgs e)
        {
            Page.Validate("BatchGroup");
            if (!Page.IsValid) return;
			var myResult = Utilities.QueryUtils.GetBatchNumberBy7digit(BatchId.Text.Trim(), this.Page);
			if (myResult.Trim() != "") BatchId.Text = myResult;
            Session["batchNumber"] = BatchId.Text;
            btnOpenCP.Visible = true;
            ShowShort();
        }

        protected void OnBatchNumberChanged(object sender, EventArgs e)
        {
			var myResult = Utilities.QueryUtils.GetBatchNumberBy7digit(BatchId.Text.Trim(), this.Page);
			if (myResult.Trim() != "") BatchId.Text = myResult;
			var url = string.Format(HLinkBatchNumber, BatchId.Text);
            Response.Redirect(url);
        }
    }
}