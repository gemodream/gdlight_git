﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Corpt
{
    public partial class MemoLookup : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Find by Memo";
            if (!IsPostBack)
            {
                MemoNumber.Focus();
            }
        }
        private void ShowOrderHistory(Boolean show)
        {
            AccordionPaneOrdersHst.Visible = show;
        }
        private void ShowOrders(Boolean show)
        {
            AccordionPaneOrders.Visible = show;
        }
        private void ShowBatches(Boolean show)
        {
            AccordionPaneBatchs.Visible = show;
        }

        protected void ActivateButtonClickOld(object sender, EventArgs e)
        {
            Page.Validate("MemoGroup");
            if (!Page.IsValid) return;
            var paramMemoOrder = chkStrick.Checked ? MemoNumber.Text.Trim() : "%" + MemoNumber.Text.Trim() + "%";
            
            ShowOrderHistory(false);
            ShowOrders(false);
            ShowBatches(false);
            
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);

            var memoOrder = new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandText = "sp_FindMemoOrderEx" };
            memoOrder.Parameters.AddWithValue("@MemoNumber", paramMemoOrder);

            var ds = new DataSet();
            var da = new SqlDataAdapter(memoOrder);

            da.Fill(ds);
            
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                //-- 1 - Order(s) with the memo
                tblOrderHst.DataSource = ds.Tables[0];
                tblOrderHst.DataBind();
                ShowOrderHistory(true);
                LblGrid1.Text = string.Format("History Orders, {0} rows", ds.Tables[0].Rows.Count);
            }
            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                //-- 2 - Order(s) with the memo
                tblOrder.DataSource = ds.Tables[1];
                tblOrder.DataBind();
                ShowOrders(true);
                LblGrid2.Text = string.Format("Orders, {0} rows", ds.Tables[1].Rows.Count);
            }

            //-- Batches with memo
            conn.Open();
            var memoBatch = new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandText = "sp_FindMemoBatchEx" };
            memoBatch.Parameters.AddWithValue("@MemoNumber", paramMemoOrder);

            var breader = memoBatch.ExecuteReader();
            if (breader.HasRows)
            {
                tblBatch.DataSource = breader;
                tblBatch.DataBind();
                ShowBatches(true);
                LblGrid3.Text = string.Format("Batches, {0} rows", tblBatch.Items.Count);
            }
            conn.Close();
        }
        protected void ActivateButtonClick(object sender, EventArgs e)
        {
            Page.Validate("MemoGroup");
            if (!Page.IsValid) return;
            var paramMemoOrder = chkStrick.Checked ? MemoNumber.Text.Trim() : "%" + MemoNumber.Text.Trim() + "%";

            ShowOrderHistory(false);
            ShowOrders(false);
            ShowBatches(false);

            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);

            var memoOrder = new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandText = "sp_FindMemoBatchEx" };
            memoOrder.Parameters.AddWithValue("@MemoNumber", paramMemoOrder);

            var ds = new DataSet();
            var da = new SqlDataAdapter(memoOrder);

            da.Fill(ds);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                //-- 1 - Order(s) with the memo
                tblOrderHst.DataSource = ds.Tables[0];
                tblOrderHst.DataBind();
                ShowOrderHistory(true);
                LblGrid1.Text = string.Format("History Orders, {0} rows", ds.Tables[0].Rows.Count);
            }
            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                //-- 2 - Order(s) with the memo
                tblOrder.DataSource = ds.Tables[1];
                tblOrder.DataBind();
                ShowOrders(true);
                LblGrid2.Text = string.Format("Orders, {0} rows", ds.Tables[1].Rows.Count);
            }

            //-- Batches with memo
            conn.Open();
            var memoBatch = new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandText = "sp_FindMemoBatchEx" };
            memoBatch.Parameters.AddWithValue("@MemoNumber", paramMemoOrder);

            var breader = memoBatch.ExecuteReader();
            if (breader.HasRows)
            {
                tblBatch.DataSource = breader;
                tblBatch.DataBind();
                ShowBatches(true);
                LblGrid3.Text = string.Format("Batches, {0} rows", tblBatch.Items.Count);
            }
            conn.Close();
        }
    }
}