﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DefaultMaster : MasterPage, CommonPage.IContentPlaceHolders
    {
        public IList GetContentPlaceHolders()
        {
            return ContentPlaceHolders;
        }

        #region Info Panel
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack)
            {
                ShowHideSideMenu();
                return;
            }
            //loginModel.User.UserId
            
            
            BuildTreeMainMenu();
            
            UserName.Text = Session["FirstName"] + " " + Session["LastName"];

#if DEBUG
			ServerName.Text = "Server " + Session["OfficeIPGroup"];
            DataSource.Text = QueryUtils.GetDbName("" + Session["MyIP_ConnectionString"]);
#else
		   	ServerName.Text = "";
            DataSource.Text = "";

#endif
 			RevLabel.Text = PageConstants.RevNumber;
            ShowHideSideMenu();
        }
        #endregion

        #region Show/Hide Side menu, Main menu
        protected void OnHideMenuButton(object sender, ImageClickEventArgs e)
        {
            Session[SessionConstants.SideMenuState] = SessionConstants.SideMenuHide;
            ShowHideSideMenu();
        }

        protected void OnShowMenuButton(object sender, ImageClickEventArgs e)
        {
            Session[SessionConstants.SideMenuState] = SessionConstants.SideMenuShow;
            ShowHideSideMenu();

        }
        private void ShowHideSideMenu()
        {
            var state = Session[SessionConstants.SideMenuState] as string ?? "";
            bool show = state != SessionConstants.SideMenuHide;
            sidebar.Visible = show;
            //tempPanel.Visible = show;

            HideMenuButton.Visible = show;
            ShowMenuButton.Visible = !show;
        }
        protected void OnMainMenuBtnClick(object sender, EventArgs e)
        {
            Response.Redirect("Middle.aspx");

        }
        #endregion

        #region Tree Main Menu
        private bool ShowTestPages()
        {
            var loginModel = Session[SessionConstants.LoginModel] as LoginModel ?? new LoginModel();
            return loginModel.LoginName == "mrx16";
        }

        #region Access To Pages
        //public static string PrgBulkUpdate = "BulkUpdate";
        //public static string PrgReassembly = "Reassembly";
        //public static string PrgRemoteSession = "RemoteSession";
        public bool HasAccessToPage(string userId, string prg)
        {
            try
            {
                var ds = new DataSet();
                ds.ReadXml(Server.MapPath("Users.xml"));
                const string format = "{0}_Id='0' and Id='{1}'";
                return ds.Tables[1].Select(string.Format(format, prg, userId)).Length > 0;

            }
            catch (Exception ex)
            {
                ErrLoadLabel.Text = ex.Message;
            }
            return false;
        }

        #endregion
        public void BuildTreeMainMenu()
        {
            var currUser = "" + Session["ID"];
            var target = Request.RawUrl.Split('/')[Request.Path.Split('/').Length - 1];
            var data = TreeUtils.GetTreeMenuData(
                HasAccessToPage(currUser, AccessUtils.PrgBulkUpdate),
                HasAccessToPage(currUser, AccessUtils.PrgReassembly),
                HasAccessToPage(currUser, AccessUtils.PrgItemHistory),
                HasAccessToPage(currUser, AccessUtils.PrgRemoteSession),
				HasAccessToPage(currUser, AccessUtils.PrgAutoMeasure));
            
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.CenterId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.ShortReportId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.CustomerId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.CpId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.GdlId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.LookupsId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.HistoryId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.StatsId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.VvId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.OptionsId, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.Screening, data));
            AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.ScreeningNewId, data));
            if (ShowTestPages())
            {
                AddMenuNodes(TreeUtils.GetRootTreeModel(TreeUtils.TestId, data));
            }
            foreach(var node in treeMenu.Nodes.Cast<TreeNode>())
            {
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
            }
            
            var targetNode = data.Find(m => m.DisplayName.IndexOf(target, StringComparison.Ordinal) != -1);
            
            if (targetNode == null) return;
            //-- Find parent and expand it
            foreach (var node in treeMenu.Nodes.Cast<TreeNode>().Where(node => node.Value == targetNode.ParentId))
            {
                node.Expand();
                foreach (var child in node.ChildNodes.Cast<TreeNode>().Where(child => child.Value == targetNode.Id))
                {
                    child.Select();
                    break;
                }
                break;
            }
        }
        private void AddMenuNodes(NodeTreeViewModel root)
        {
            if (root == null) return;
            var treeNode = new TreeNode(root.DisplayName, root.Id);
            
            TreeUtils.FillNode(treeNode, root, false, TreeNodeSelectAction.Select);
            treeMenu.Nodes.Add(treeNode);
        }
        #endregion

    }
}