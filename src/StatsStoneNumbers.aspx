﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="StatsStoneNumbers.aspx.cs" Inherits="Corpt.StatsStoneNumbers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    
    <div class="demoarea">
        <div class="demoheading">Stone Numbers</div>
        <asp:Panel ID="Panel1" runat="server"  CssClass="form-inline">
            <asp:RadioButtonList ID="periodType" runat="server" CssClass="radio" OnSelectedIndexChanged="OnChangedPeriodType"
                RepeatDirection="Horizontal" Width="250px" AutoPostBack="true">
                <asp:ListItem Value="m" Selected="True">Monthly</asp:ListItem>
                <asp:ListItem Value="w">Weekly</asp:ListItem>
                <asp:ListItem Value="r">Random</asp:ListItem>
            </asp:RadioButtonList>
            <!-- Date From -->
            <label>
                From</label>
            <asp:TextBox runat="server" ID="calFrom" Width="100px" />
            <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <!-- Date To -->
            <label style="padding-left: 20px;">
                To</label>
            <asp:TextBox runat="server" ID="calTo" Width="100px"/>
            <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <label style="padding-left: 20px;">
                Office Location</label>
            <asp:DropDownList runat="server" ID="parentOfficeId" DataTextField="Country" DataValueField="ParentOfficeId" AutoPostBack="true" Width="100px"/>
            <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Lookup" OnClick="OnLookupClick"
                Style="margin-left: 15px"></asp:Button>
        </asp:Panel>
        <br />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
            PopupButtonID="Image1" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
            PopupButtonID="Image2" />
        <asp:Label runat="server" ID="countLabel" class="label" Style="text-align: left" />
        <ul class="pagination" id="pagesTop" runat="server">
        </ul>
        <div class="content">
            <div class="row">
                <div style="width: 800px; text-align: center;padding-left: 20px">
                    <div id="gridContainer" style="font-size: small;text-align: center">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" class="table-bordered"
                            CellPadding="5">
                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Stone Number" DataField="FullItemNumberWithDotes" />
                                <asp:BoundField HeaderText="Customer" DataField="CustomerName" />
                                <asp:BoundField HeaderText="Create Date" DataField="CreateDate" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <ul class="pagination" id="pagesBotton" runat="server">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                
            </div>
        </div>
        <br />
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $.datepicker.setDefaults($.datepicker.regional['en']);
            $("#pDateFrom").datepicker({
                dateFormat: "mm.dd.yy",
                changeMonth: true,
                changeYear: true,
                yearRange: '1930:2050',
                showOn: "both"
            });            
        })
    </script>
</asp:Content>
