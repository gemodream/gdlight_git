﻿using Corpt.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.Utilities;
using Corpt.TreeModel;
using System.Data;
using System.Xml;
using Corpt.Models.Remeas;
using System.IO;
using Corpt.Models;
using System.Net;

namespace Corpt
{
	public partial class Measure2 : CommonPage
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Measure(New)";

			OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
			OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

			if (!IsPostBack)
			{
				string measureIds = QueryUtils.GetGradeMeasureIds(this);
				Session["MeasureIds"] = measureIds;
				Session["TotalWeight"] = null;
				CleanUp();
				ResetField.Value = "false";
				calcBtn.Visible = false;
				SetViewState(QueryUtils.GetEnumMeasureUnion(this), SessionConstants.GradeMeasureEnums);
				SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
			}
			txtBatchNumber.Focus();
		}
		#endregion

		#region CleanUp (on PageLoad, on Load button click)
		private void CleanUp()
		{
			CleanItemListArea();
			CleanPartTreeArea();
			//HidePicturePanel();//TODO modify this - done, see line below
			//HideShapePanel();
			ShowHideImages(false);
			CleanValueGridArea();
			CleanViewState();

			InvalidLabel.Text = "";
		}
		private void CleanItemListArea()
		{
			lstItemList.Items.Clear();
			ItemEditing.Value = "";
			lstItemList.Visible = false;
			MetalParms.Visible = false;
			MountParms.Visible = false;
			MetalStampList.Visible = false;
			MetalColorList.Visible = false;
			MetalKaratageList.Visible = false;
			ICMetalColorList.Visible = false;
			ICMetalStampList.Visible = false;
			ICMetalKaratageList.Visible = false;
			ReqFRanges.Visible = false;
			ReqFRangesBtn.Visible = false;
			ReqFRangesGroupList.Visible = false;
			ReqFDisplayBtn.Visible = false;
			ReqFRangeList.Visible = false;
		}
		private void CleanPartTreeArea()
		{
			PartTree.Nodes.Clear();
			PartEditing.Value = "";
		}
		private void CleanValueGridArea()
		{
			//ItemValuesGrid.DataSource = null;
			//ItemValuesGrid.DataBind();

			//New code for unbinding data sources
			ElementPanel.DataSource = null;
			ElementPanel.DataBind();

			//Make Measurement Panel Invisible
			ShowHideItemDetails(false);
			/*
			MeasurementPanel.Visible = false;
			DataForLabel.Visible = false;
			ClarityRadio.Visible = false;
			Comments.Visible = false;
			WeightPanel.Visible = false;
			IntExtCommentPanel.Visible = false;
			LaserPanel.Visible = false;
			SarinPanel.Visible = false;
			*/
		}
		private void CleanViewState()
		{
			//-- Item numbers
			SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);

			//-- Measure Parts
			SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);

			//-- Measure Descriptions
			SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

			//-- Item Measure Values
			SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);

		}
		#endregion

		#region Load
		int ListMaxRow = 40;
		int ListMinRow = 10;
		protected void OnLoadClick(object sender, EventArgs e)
		{
			Session["ItemList"] = null;
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnLoadClick);
				return;
			}

			//if first time loading, hide side menu - not yet working
			/*
			if(MeasurementPanel.Visible == false)
			{
				Session[SessionConstants.SideMenuState] = SessionConstants.SideMenuHide;
				var state = Session[SessionConstants.SideMenuState] as string ?? "";
				bool show = state != SessionConstants.SideMenuHide;
				(this.Page.Master.FindControl("sidebar") as Control).Visible = show;
				(this.Page.Master.FindControl("HideMenuButton") as ImageButton).Visible = show;
				(this.Page.Master.FindControl("ShowMenuButton") as ImageButton).Visible = !show;
			}
			*/

			LoadExecute();
		}

		private void LoadExecute()
		{
			//Save current lstItemList temporarily
			Session["TMPNUMBER"] = null;
			Session["MatchItemNumber"] = null;
			hdnMeasureBatchEventID.Value = "";
			hdnMeasureItemEventID.Value = "";
			List<string> prevList = new List<string>();
			List<string> fullPrevList = new List<string>();
			string prevItemNumber = "";
			var prevModel = GetItemModelFromView(ItemEditing.Value);
			if (prevModel != null)
			{
				prevItemNumber = prevModel.FullBatchNumber;
				foreach (ListItem item in lstItemList.Items)
				{
					if (item.Text.StartsWith("*"))
					{
						prevList.Add(item.Text.Substring(1));
						fullPrevList.Add(item.Text.Substring(1));
					}
					else
						fullPrevList.Add(item.Text);
				}
			}

			CleanUp();
			if (txtBatchNumber.Text.Trim().Length == 0)
			{
				InvalidLabel.Text = "A Batch Number is required!";
				return;
			}
			//Check 7digit prefix
			//alex
			OldTextNumber.Value = txtBatchNumber.Text;
			//alex
			var myResult = QueryUtils.GetItemNumberBy7digit(txtBatchNumber.Text.Trim(), this.Page);
			if (myResult.Trim() != "")
			{
				txtBatchNumber.Text = myResult;
			}
			//-- Get ItemNumbers by BatchNumber
			//alex
			DataTable dt = new DataTable();
			dt = QueryUtils.GetItemsCpOldNew(txtBatchNumber.Text.Trim(), this.Page);
			var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
			Session["ItemList"] = dt;
			//var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
			//alex
			if (itemList.Count == 0)
			{
				InvalidLabel.Text = "Items not found";
				return;
			}
			SetViewState(itemList, SessionConstants.GradeItemNumbers);

			//-- Refresh batchNumber
			var tempTxtBatchNumber = txtBatchNumber.Text;//Sergey
			var itemNumber = itemList[0];
			string matchedItemNumber = null;
			if (txtBatchNumber.Text != itemNumber.FullBatchNumber)//not batch
			{
				RemeasParamModel matchItemNumber = QueryUtils.GetNewItemNumber(txtBatchNumber.Text, this);
				Session["MatchItemNumber"] = matchItemNumber.NewItemNumber;
				matchedItemNumber = matchItemNumber.NewItemNumber;
			}
			List<string> newItemList = Utils.GetOriginalItemListValues(dt, itemList, txtBatchNumber.Text, matchedItemNumber);

			/* alex
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}
            alex */
			//alex
			string prevItemCode = null;
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				int numberLength = txtBatchNumber.Text.Length;
				if (numberLength == 10 || numberLength == 11)//item
				{

					if (itemNumber.FullBatchNumber != txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2))//search by batch
					{

						prevItemCode = Utils.getPrevItemCodeNew(itemList, txtBatchNumber.Text, matchedItemNumber);//search by item
						if (prevItemCode != null)
							txtBatchNumber.Text = prevItemCode;
					}
				}
				else //batch
					txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}
			//alex
			//-- Loading ItemList control
			lstItemList.Items.Clear();
			if (newItemList == null)
			{
				foreach (var singleItemModel in itemList)
				{
					lstItemList.Items.Add(singleItemModel.FullItemNumber);
				}
			}
			else
			{
				txtBatchNumber.Text = Utils.getPrevItemCodeNew(itemList, tempTxtBatchNumber, matchedItemNumber);
				foreach (var item in newItemList)
					lstItemList.Items.Add(item);
			}
			/*
            foreach (var singleItemModel in itemList)
			{
				lstItemList.Items.Add(singleItemModel.FullItemNumber);
			}
			if (itemList.Count > ListMaxRow)
			{
				lstItemList.Rows = ListMaxRow;
			}
			else if (itemList.Count < ListMinRow)
			{
				lstItemList.Rows = ListMinRow;
			}
			else
			{
				lstItemList.Rows = itemList.Count + 1;
			}
            */
			if (itemList.Count > ListMaxRow)
			{
				lstItemList.Rows = ListMaxRow;
			}
			else if (itemList.Count < ListMinRow)
			{
				lstItemList.Rows = ListMinRow;
			}
			else
			{
				//alex oldnew
				if (newItemList == null)
					lstItemList.Rows = itemList.Count + 1;
				else
					lstItemList.Rows = itemList.Count + 1;
			}
			lstItemList.Visible = true;

			//-- Loading Parts by first ItemNumber and show on PartTree Control
			var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
			SetViewState(parts, SessionConstants.GradeMeasureParts);
			LoadPartsTree();

			//-- Loading Measures by Cp or ItemTypeId
			if (IgnoreCpFlag.Checked)
			{
				var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}
			else
			{
				var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}

			//Sergey: Selects the exact item on load, rather than the first
			if (newItemList == null)
			{
				var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
				if (selectListItem == null)
					selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
				if (selectListItem == null)
				{
					lstItemList.SelectedIndex = 0;
				}
				else
				{
					lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
				}
			}
			else
			{
				string selectListItem = null;
				//selectListItem = newItemList.Find(m => m == tempTxtBatchNumber);
				selectListItem = newItemList.Find(m => m == txtBatchNumber.Text);
				if (selectListItem == null)
				{
					lstItemList.SelectedIndex = 0;
				}
				else
					//lstItemList.SelectedValue = tempTxtBatchNumber;
					lstItemList.SelectedValue = txtBatchNumber.Text;
			}
			/*
			var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
            if (selectListItem == null)
                selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
            if (selectListItem == null)
			{
				lstItemList.SelectedIndex = 0;
			}
			else
			{
				lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
			}
            */
			//If same batch as previous, mark saved entries
			//if (prevItemNumber == txtBatchNumber.Text)
			//if ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)))
			bool inPrevList = false;
			string matchItem = null;
			if (fullPrevList.Count > 0 && ((matchItem = fullPrevList.Find(p => p == txtBatchNumber.Text)) != null))
				inPrevList = true;
			if ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)) || inPrevList)
			{
				foreach (ListItem item in lstItemList.Items)
				{
					string pItem = null;
					if ((pItem = prevList.Find(p => p == item.Text)) != null)
						item.Text = "*" + item.Text;
					//foreach (var prevItem in prevList)
					//{
					//	if (prevItem == item.Text)
					//	{
					//		item.Text = "*" + item.Text;
					//	}
					//}
				}
			}
			else//Otherwise reset the Radio Buttons
			{
				ClarityRadio.SelectedIndex = 0;
				SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
			}

			ShowHideItemDetails(true);
			/*
			ClarityRadio.Visible = true;
			Comments.Visible = true;
			WeightPanel.Visible = true;
			SarinPanel.Visible = true;
			IntExtCommentPanel.Visible = true;
			LaserPanel.Visible = true;
			*/
			CreateMeasurePanel();
			//if (txtBatchNumber.Text != tempTxtBatchNumber && txtBatchNumber.Text != tempTxtBatchNumber.Substring(0, tempTxtBatchNumber.Length - 2))
			if (tempTxtBatchNumber.Length >= 10 && (txtBatchNumber.Text != tempTxtBatchNumber && txtBatchNumber.Text != tempTxtBatchNumber.Substring(0, tempTxtBatchNumber.Length - 2)))
				Session["TMPNUMBER"] = tempTxtBatchNumber;
			OnItemListSelectedChanged(null, null);

			//*--------Nimesh Code--------------*/
			RptMeasureSarinList.DataSource = null;
			RptMeasureSarinList.DataBind();
			fieldsetSarin.Visible = false;
			//RptMeasureList.Items.Clear();
			//tracking
			int noInBatch = lstItemList.Items.Count;
			int eventID = 3;
			int formCode = 6;
			int noAffected = 0;
			long batchId = itemList[0].BatchId;
			if (tempTxtBatchNumber.Length < 10)//batch
			{

				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);
				if (batchEventId != "")
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
			else
			{
				//int noInBatch = lstItemList.Items.Count;
				//int eventID = 3;
				//int formCode = 4;
				//int noAffected = 0;
				//long batchId = itemList[0].BatchId;
				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);

				if (batchEventId != "")
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
		}

		/*
        private void LoadExecute()
        {
            //Save current lstItemList temporarily
            List<string> prevList = new List<string>();
            string prevItemNumber = "0";
            var prevModel = GetItemModelFromView(ItemEditing.Value);
            if (prevModel != null)
            {
                prevItemNumber = prevModel.FullBatchNumber;
                foreach (ListItem item in lstItemList.Items)
                {
                    if (item.Text.StartsWith("*"))
                    {
                        prevList.Add(item.Text.Substring(1));
                    }
                }
            }

            CleanUp();
            if (txtBatchNumber.Text.Trim().Length == 0)
            {
                InvalidLabel.Text = "A Batch Number is required!";
                return;
            }
            OldTextNumber.Value = txtBatchNumber.Text;
            //Check 7digit prefix
            var myResult = QueryUtils.GetItemNumberBy7digit(txtBatchNumber.Text.Trim(), this.Page);
            if (myResult.Trim() != "")
            {
                txtBatchNumber.Text = myResult;
            }
            //-- Get ItemNumbers by BatchNumber
            DataTable dt = new DataTable();
            dt = QueryUtils.GetItemsCpOldNew(txtBatchNumber.Text.Trim(), this.Page);
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row)).ToList();
            //var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
            if (itemList.Count == 0)
            {
                InvalidLabel.Text = "Items not found";
                return;
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);

            //-- Refresh batchNumber
            var tempTxtBatchNumber = txtBatchNumber.Text;//Sergey
            var itemNumber = itemList[0];
            /* alex
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}
            alex 
            string prevItemCode = null;
            if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
            {
                int numberLength = txtBatchNumber.Text.Length;
                if (numberLength == 10 || numberLength == 11)//item
                {

                    if (itemNumber.FullBatchNumber != txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2))//search by batch
                    {

                        prevItemCode = Utils.getPrevItemCode(dt, txtBatchNumber.Text);//search by item
                        txtBatchNumber.Text = prevItemCode;
                    }
                }
                else //batch
                    txtBatchNumber.Text = itemNumber.FullBatchNumber;
            }
            //-- Loading ItemList control
            lstItemList.Items.Clear();
            foreach (var singleItemModel in itemList)
            {
                lstItemList.Items.Add(singleItemModel.FullItemNumber);
            }
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                lstItemList.Rows = itemList.Count + 1;
            }
            lstItemList.Style["display"] = "";

            //-- Loading Parts by first ItemNumber and show on PartTree Control
            var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
            SetViewState(parts, SessionConstants.GradeMeasureParts);
            LoadPartsTree();

            //-- Loading Measures by Cp or ItemTypeId
            if (IgnoreCpFlag.Checked)
            {
                var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }
            else
            {
                var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }

            //Sergey: Selects the exact item on load, rather than the first
            var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
            if (selectListItem == null)
                selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
            if (selectListItem == null)
            {
                lstItemList.SelectedIndex = 0;
            }
            else
            {
                lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
            }

            //If same batch as previous, mark saved entries
            if (prevItemNumber == txtBatchNumber.Text)
            {
                foreach (ListItem item in lstItemList.Items)
                {
                    foreach (var prevItem in prevList)
                    {
                        if (prevItem == item.Text)
                        {
                            item.Text = "*" + item.Text;
                        }
                    }
                }
            }
            else//Otherwise reset the Radio Buttons
            {
                ClarityRadio.SelectedIndex = 0;
                SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
            }

            ShowHideItemDetails(true);
            /*
			ClarityRadio.Visible = true;
			Comments.Visible = true;
			WeightPanel.Visible = true;
			SarinPanel.Visible = true;
			IntExtCommentPanel.Visible = true;
			LaserPanel.Visible = true;
			
            CreateMeasurePanel();
            OnItemListSelectedChanged(null, null);
            RptMeasureSarinList.DataSource = null;
            RptMeasureSarinList.DataBind();
            fieldsetSarin.Visible = false;
        }//LoadExecute
    */
		#endregion

		#region Save
		protected void OnSaveClick(object sender, EventArgs e)
		{
			//-- no changes
			if (!HasUnSavedChanges())
			{
				InvalidLabel.Text = "No changes!";
				//PopupInfoDialog("No changes!", false);
				return;
			}
			var errMsg = SaveExecute();
			if (string.IsNullOrEmpty(errMsg))
			{
				//-- Reload Item Values
				OnItemListSelected();
				InvalidLabel.Text = "Changes were updated successfully.";
                //PopupInfoDialog("Changes were updated successfully.", false);
                if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                {
                    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                    OnItemListSelectedChanged(null, null);
                }
            }
			else
			{
				InvalidLabel.Text = errMsg;
				//PopupInfoDialog(errMsg, true);
			}
			return;
		}
		/* alex
		private string SaveExecute()
		{
			var itemModel = GetItemModelFromView(ItemEditing.Value);
			var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			var result = new List<MeasureValueModel>();
            bool valueIn = false;
			foreach (var itemValue in itemValues)
			{
				result.Add(new MeasureValueModel(itemValue, itemModel));
                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }
            }
			var errMsg = QueryUtils.SaveNewMeasures(result, this);
			if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
            {
                errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            }

			//Highlight after saved
			//lstItemList.Items.FindByText(itemModel.FullItemNumber).Attributes.Add("style", "color: green");//For some reason, this doesn't last!
			var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
			if (unsavedNum != null)
			{
				lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
			}

			return "";
		}
        alex */
		private string SaveExecute()
		{
			var itemModel = GetItemModelFromView(ItemEditing.Value);
			var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			if (itemValues.Count == 0)
				return "";
			long batchId = itemModel.BatchId;
			var result = new List<MeasureValueModel>();
			bool valueIn = false;
			List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel>;
			List<MeasureValueModel> toAdd = new List<MeasureValueModel>();
			List<MeasureValueModel> toRemove = new List<MeasureValueModel>();
			foreach (var itemValue in itemValues)
			{
				result.Add(new MeasureValueModel(itemValue, itemModel));
				toAdd.Add(new MeasureValueModel(itemValue, itemModel));
				foreach (var measure in measureValuesBatch)
				{
					if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString()
						&& measure.PartId.ToString() == itemValue.PartId)
					{
						toRemove.Add(measure);
						break;
					}
				}

				var measureId = itemValue.MeasureId.ToString();
				if (!valueIn)
				{
					string ids = (string)Session["MeasureIds"];
					valueIn = QueryUtils.IsValueInNew(ids, measureId);
				}


				/*
                foreach (var measure in measureValuesBatch)
                {
                    if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString() 
                        && measure.PartId.ToString() == itemValue.PartId)
                    {
                        measureValuesBatch.Remove(measure);
                        break;
                    }
                    measureValuesBatch.Add(new MeasureValueModel(itemValue, itemModel));
                }
                */
			}
			var errMsg = QueryUtils.SaveNewMeasures(result, this);
			if (ResetField.Value == "false")
			{
				List<MeasureValueModel> icTotalWeight = new List<MeasureValueModel>();
				if (Session["TotalWeight"] != null)
					icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
				var selItem = GetItemFromFullList(lstItemList.SelectedValue);
				var measures1 = QueryUtils.GetMeasureValuesCp(selItem.BatchId, selItem.ItemTypeId, this);
				var measures2 = measures1.Find(m => m.PartId == Convert.ToInt32(PartTree.SelectedNode.Value));
				//foreach (MeasureValueModel item in result)
				if (measures2.PartTypeId != 15)//not ic
					for (int i = 0; i <= result.Count - 1; i++)
					{
						if (((result[i].MeasureId == "5" && (measures2.PartTypeId == 1 || measures2.PartTypeId == 2 || measures2.PartTypeId == 3)) ||
							 (result[i].MeasureId == "2" && (measures2.PartTypeId == 11 || measures2.PartTypeId == 10)))
							 && result[i].PartId == measures2.PartId)
						//if ((result[i].MeasureId == "5" && (measures2.PartTypeId == 1 || measures2.PartTypeId == 2)))
						{
							MeasureValueModel oldItem = icTotalWeight.Find(m => m.PartId == result[i].PartId);
							if (oldItem == null)
								icTotalWeight.Add(result[i]);
							else
							{
								icTotalWeight.Remove(oldItem);
								icTotalWeight.Add(result[i]);
							}
						}
					}
				Session["TotalWeight"] = icTotalWeight;
			}
			if (!string.IsNullOrEmpty(errMsg)) return errMsg;
			//add tracking
			int noInBatch = lstItemList.Items.Count;
			int eventID = 2;
			int formCode = 6;
			int noAffected = 1;
			long batchIdInt = itemModel.BatchId;
			string batchEventId = "";
			if (hdnMeasureBatchEventID.Value == "") //fist item in batch
			{

				batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchIdInt, noAffected, noInBatch, 0, this);
				if (batchEventId != "")
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(batchEventId), 0, this);
					hdnMeasureBatchEventID.Value = batchEventId;
					hdnMeasureItemEventID.Value = itemEventId;
				}

			}
			else //next saved items
			{
				string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(hdnMeasureBatchEventID.Value), 1, this);
				hdnMeasureItemEventID.Value = itemEventId;
			}

			if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
			{
				errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
				if (!string.IsNullOrEmpty(errMsg)) return errMsg;
			}


			//Highlight after saved
			var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
			if (unsavedNum != null)
			{
				lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
			}
			else
			{
				unsavedNum = lstItemList.Items.FindByText(itemModel.FullOldItemNumber);
				if (unsavedNum != null)
				{
					lstItemList.Items.FindByText(itemModel.FullOldItemNumber).Text = "*" + unsavedNum.Text;
				}
			}
			bool shapeChanged = false;
			foreach (var measure in toAdd)
			{
				if (measure.MeasureId == "8")
				{
					measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
					shapeChanged = true;
					break;
				}
			}
			if (!shapeChanged)
			{
				measureValuesBatch.RemoveAll(x => toRemove.Contains(x));
				foreach (var measure in toAdd)
					measureValuesBatch.Add(measure);
			}
			SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
			return "";
		}
		#endregion

		#region ShortReportLink
		protected void OnShortReportClick(object sender, EventArgs e)
		{
			//Check if changes have been saved
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnShortReportClick);
				return;
			}
			//Redirect to short report
			var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
			Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
		}
		#endregion

		#region ItemNumber List
		protected void OnItemListSelectedChanged(object sender, EventArgs e)
		{
			/* alex
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnItemChanges);
				return;
			}
            */
			Session["MatchItemNumber"] = null;
			Session["TMPNUMBER"] = null;
			ICTotalWeightTxt.Text = "";
			OnItemListSelected();
		}

		private void OnItemListSelected()
		{
			string tmpBatchNumber = null, itemNumber = null, matchItemNumber = null;
			if (Session["MatchItemNumber"] != null)
				matchItemNumber = (string)Session["MatchItemNumber"];
			if (Session["TMPNUMBER"] != null)
			{
				tmpBatchNumber = (string)Session["TMPNUMBER"];
				Session["TMPNUMBER"] = null;
			}
			if (matchItemNumber != null)
				itemNumber = matchItemNumber;
			else if (tmpBatchNumber != null)
				itemNumber = tmpBatchNumber;
			else
			{
				DataTable dt = (DataTable)Session["ItemList"];
				var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
				foreach (SingleItemModel item in itemList)
				{

					string tmpSelectedValue = null;
					if (lstItemList.SelectedValue[0] == '*')
						tmpSelectedValue = lstItemList.SelectedValue.Substring(1);
					else
						tmpSelectedValue = lstItemList.SelectedValue;
					//if (item.FullOldItemNumber == lstItemList.SelectedValue)
					if (item.FullOldItemNumber == tmpSelectedValue)
					{
						itemNumber = item.FullItemNumber;
						break;
					}
				}

			}
			//var itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			var singleItemModel = GetItemModelFromView(itemNumber);
			if (singleItemModel == null) return;
			if (singleItemModel.StateId == DbConstants.ItemInvalidStateId)
			{
				InvalidLabel.Text = string.Format("Item {0} is invalid", singleItemModel.FullItemNumber);
			}
			else
			{
				InvalidLabel.Text = "";
			}
			ShowImagesNew(singleItemModel.Path2Picture, "Picture");//TODO implement this - done
			LoadMeasuresForEdit(itemNumber);
			PartEditing.Value = "";
			if (hdnMeasureBatchEventID != null && hdnMeasureBatchEventID.Value != "")//next item for existing batch event - touched
			{
				string batchEventId = hdnMeasureBatchEventID.Value;
				ItemEditing.Value = itemNumber;
				int noInBatch = lstItemList.Items.Count;
				int eventID = 3;
				int formCode = 4;
				long batchId = singleItemModel.BatchId;
				if (hdnMeasureItemEventID.Value != "")
					hdnMeasureItemEventID.Value = "";
				else
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, txtBatchNumber.Text, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
			/*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Select();
                */
			PartTree.Nodes[0].Selected = true;
			OnPartsTreeChanged(null, null);
			ItemEditing.Value = itemNumber;
		}

		private bool HasUnSavedChanges()
		{
			GetNewValuesFromGrid();
			var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			return itemValues.Count > 0;
		}
		#endregion

		#region Parts Tree
		protected void OnPartsTreeChanged(object sender, EventArgs e)
		{
			cmdSave.Enabled = true;
			ResetTotalWeightBtn.Visible = false;
			DiaParms.Visible = false;
			WDSSParms.Visible = false;
			CSSParms.Visible = false;
			MetalParms.Visible = false;
			MountParms.Visible = false;
			ICParms.Visible = false;
			AddShapeParms.Visible = false;
			WDSSNumberOfItems.Text = "";
			//WDSSNumberOfUnitsTxt.Text = "";
			WSSWeightPerItem.Text = "";
			//WDSSTotalWeightTxt.Text = "";
			TableTxt.Text = "";
			InvalidLabel.Text = "";
			if (PartTree.SelectedNode.Text.Contains("Stone Set"))
				ClarityRadio.SelectedIndex = 1;
			else
				ClarityRadio.SelectedIndex = 0;
			if (GetSelectedItemModel() == null) return;
			var currPart = PartTree.SelectedValue;
			var prevPart = PartEditing.Value;
			//-- Get Changes
			if (!string.IsNullOrEmpty(prevPart) && currPart != prevPart)
			{
				GetNewValuesFromGrid();
			}

			//Set Small Buttons to altered state on Item Container
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				var elemBtn = elem.FindControl("SmBtn") as Button;
				SetButtonState(elemBtn, PartTree.SelectedNode.Text);
			}

			//In Item Container, use SS Clarity instead of Clarity
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				if ((elem.FindControl("ElementName") as HiddenField).Value == "Clarity")
				{
					if (PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
				}
				else if ((elem.FindControl("ElementName") as HiddenField).Value == "SS Clarity")
				{
					if (PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
				}
			}

			//LoadDataForEditing();
			var ordernumber = lstItemList.SelectedValue;
			//alex if (sender != null)
			LoadShapeForEdit(ordernumber);
			PartEditing.Value = currPart;
			ClearButtonCategory();
			LodaMeasureforEditing();

		}
		private BatchModel GetFirstBatch()
		{
			var batches = GetViewState(SessionConstants.BulkBatches) as List<BatchModel>;
			return batches == null ? null : batches[0];
		}
		private List<EnumMeasureModel> GetEnumMeasure()
		{
			var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
						   new List<EnumMeasureModel>();
			return measures;
		}
		private const string ViewMeasures = "viewMeasures";
		private static string ViewBatchData = "viewBatchData";
		private void InitViewData()
		{
			SetViewState(new List<ViewMeasures>(), ViewMeasures);
			SetViewState(new List<ViewBatchData>(), ViewBatchData);

		}
		private IEnumerable<MeasureModel> GetMeasuresFromView(int itemTypeId)
		{
			var measures = GetViewState(ViewMeasures) as List<ViewMeasures> ?? new List<ViewMeasures>();
			var byItemType = measures.Find(m => m.ItemTypeId == itemTypeId);
			if (byItemType == null)
			{
				byItemType = new ViewMeasures { ItemTypeId = itemTypeId, Measures = QueryUtils.GetMeasures(new BatchModel { ItemTypeId = itemTypeId }, false, this) };
				measures.Add(byItemType);
			}
			return byItemType.Measures;
		}
		private void LodaMeasureforEditing()
		{
			//RptMeasureList.Items.Clear();
			if (PartTree.SelectedNode.Selected == false) return;
			var selItem = GetItemFromFullList(lstItemList.SelectedValue);

			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			var itemModel = GetItemFromFullList(lstItemList.SelectedValue);
			//var measures3 = QueryUtils.GetMeasure(itemModel, false, this);
			//var measures3 = QueryUtils.GetMeasureValues(itemModel,"6", this);

			var measures10 = QueryUtils.GetMeasures(new BatchModel { ItemTypeId = selItem.ItemTypeId }, false, this);
			//var measures11 = measures10.FindAll(m => m.PartId == Convert.ToInt32(PartTree.SelectedNode.Value));

			//var measures = GetMeasuresFromView(itemModel.ItemTypeId);

			var measures11 = GetMeasureListByMode(selItem, PartTree.SelectedNode.Value);
			var values = GetMeasureValuesByItem();
			DataTable MeasureDT = new DataTable();
			MeasureDT.Columns.Add("MeasureID", typeof(System.String));
			MeasureDT.Columns.Add("MeasureName", typeof(System.String));
			MeasureDT.Columns.Add("MeasureValue", typeof(System.String));
			var measures1 = QueryUtils.GetMeasureValuesCp(selItem.BatchId, selItem.ItemTypeId, this);
			var measures2 = measures1.Find(m => m.PartId == Convert.ToInt32(PartTree.SelectedNode.Value));
			int metalId = 0, stampId = 0, karatageId = 0, itemNameID = 0, mountId = 0;
			if (measures2.PartTypeId == 1 || measures2.PartTypeId == 2 || measures2.PartTypeId == 3)//diamond-colored diamond-color stone
			{
				DiaParms.Visible = true;
				MetalParms.Visible = false;
				MountParms.Visible = false;
				ICParms.Visible = false;
				WDSSParms.Visible = false;
				CSSParms.Visible = false;
				calcBtn.Visible = true;
				for (int i = 0; i < measures11.Count; i++)
				{
					if (measures11[i].MeasureId.ToString() == "11")//max
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								MaxTxt.Text = result.MeasureValue;
							else
								MaxTxt.Text = "";
						}
						catch
						{
							MaxTxt.Text = "";
						}
					}
					else if (measures11[i].MeasureId.ToString() == "12")//min
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								MinTxt.Text = result.MeasureValue;
							else
								MinTxt.Text = "";
						}
						catch
						{
							MinTxt.Text = "";

						}
					}
					else if (measures11[i].MeasureId.ToString() == "13")//H_x
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								H_xTxt.Text = result.MeasureValue;
							else
								H_xTxt.Text = "";
						}
						catch
						{
							H_xTxt.Text = "";
						}
					}
					else if (measures11[i].MeasureId.ToString() == "14")//Depth
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								TotaDepthTxt.Text = result.MeasureValue;
							else
								TotaDepthTxt.Text = "";
						}
						catch
						{
							TotaDepthTxt.Text = "";
						}
					}
					else if (measures11[i].MeasureId.ToString() == "15")//tab_d
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								Tab_DTxt.Text = result.MeasureValue;
							else
								Tab_DTxt.Text = "";
						}
						catch
						{
							Tab_DTxt.Text = "";
						}
					}
					else if (measures11[i].MeasureId.ToString() == "8")//shape
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								DiaShapeTxt.Text = result.MeasureValueName.ToString();
							else
								DiaShapeTxt.Text = "";
						}
						catch
						{
							DiaShapeTxt.Text = "";

						}
					}
					else if (measures11[i].MeasureId.ToString() == "54")//variety
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
							{
								CSVarietyTxt.Text = result.MeasureValueName.ToString();
								CSVarietyTxt.Visible = true;
							}
							else
							{
								CSVarietyTxt.Text = "";
								CSVarietyTxt.Visible = false;
							}
						}
						catch
						{
							DiaShapeTxt.Text = "";

						}
					}
					else if (measures11[i].MeasureId.ToString() == "5")//calculate weight
					{
						try
						{
							var cWeight = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (cWeight == null)
								DiaCalcWeightTxt.Text = "";
							else
							{
								DiaCalcWeightTxt.Text = cWeight.MeasureValue;
								if (Session["TotalWeight"] != null)
								{
									List<MeasureValueModel> icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
									MeasureValueModel oldItem = icTotalWeight.Find(m => m.PartId == measures11[i].PartId);
									if (oldItem == null)
									{
										icTotalWeight.Add(cWeight);
										Session["TotalWeight"] = icTotalWeight;
									}
								}
							}
							//DiaCalcWeightTxt.Text = GetCurrMeasureValue(measures11[i].MeasureId.ToString()).MeasureValue;
						}
						catch
						{
							continue;
						}
					}
					else if (measures11[i].MeasureId.ToString() == "4")//measured weight
					{
						try
						{
							var cWeight = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (cWeight == null)
								DiaMeasWeight.Text = "";
							else
							{
								DiaMeasWeight.Text = cWeight.MeasureValue;
								
							}
							//DiaCalcWeightTxt.Text = GetCurrMeasureValue(measures11[i].MeasureId.ToString()).MeasureValue;
						}
						catch
						{
							continue;
						}
					}
				}
			}
			if (measures2.PartTypeId == 15)//ic
			{
				DiaParms.Visible = false;
				MetalParms.Visible = false;
				MountParms.Visible = false;
				ICParms.Visible = true;
				WDSSParms.Visible = false;
				CSSParms.Visible = false;
				calcBtn.Visible = false;
				ICMetalColorList.Visible = true;
				ICMetalStampList.Visible = true;
				ICMetalKaratageList.Visible = true;
				ICItemNameList.Visible = true;

				var metalColor = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "44");
				var metalStamp = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "45");
				var metalKaratage = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "259");
				var blockKaratageList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Karatage, Page);
				var finalMetalKaratage = metalKaratage.Where(x => !blockKaratageList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName))));
				var itemNameList = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "109");
				var blockItemNameList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ItemName, Page);
				var finalItemNameList = itemNameList.Where(x => !blockItemNameList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName))));
				ICMetalColorList.DataSource = metalColor;
				ICMetalColorList.DataBind();
				ICMetalStampList.DataSource = metalStamp;
				ICMetalStampList.DataBind();
				ICMetalKaratageList.DataSource = finalMetalKaratage;
				ICMetalKaratageList.DataBind();
				ICItemNameList.DataSource = finalItemNameList;
				ICItemNameList.DataBind();
				ICMetalColorList.SelectedIndex = -1;
				ICMetalStampList.SelectedIndex = -1;
				ICMetalKaratageList.SelectedIndex = -1;

				for (int i = 0; i < measures11.Count - 1; i++)
				{
					var value = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
					if (value == null)
						continue;
					if (measures11[i].MeasureId == "44")//metal
						metalId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[i].MeasureId == "45")//stamp
						stampId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[i].MeasureId == "259")//karatage
						karatageId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[i].MeasureId == "109")//Item Name
						itemNameID = Convert.ToInt32(value.MeasureValueId);
					if (measures11[i].MeasureId.ToString() == "1")//weight grams
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								ICWeightTxt.Text = result.MeasureValue;
							else
								ICWeightTxt.Text = "";
						}
						catch
						{
							ICWeightTxt.Text = "";
						}
					}
					if (measures11[i].MeasureId.ToString() == "2")//weight cts
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								ICTotalWeightTxt.Text = result.MeasureValue;
							else
								ICTotalWeightTxt.Text = "";
							if (Session["TotalWeight"] != null && ResetField.Value == "false")//collect total weight from another parts calc weight
							{
								List<MeasureValueModel> icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
								if (icTotalWeight.Count > 0)
								{
									double icTW = 0;
									foreach (MeasureValueModel icTVItem in icTotalWeight)
									{
										double itemWeight = Convert.ToDouble(icTVItem.MeasureValue);
										icTW += itemWeight;
									}
									ICTotalWeightTxt.Text = icTW.ToString("0.##");
								}
							}

						}
						catch
						{
							ICTotalWeightTxt.Text = "";
						}
					}
					if (measures11[i].MeasureId.ToString() == "88")//weight grams
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								LotNumberTextBox.Text = result.StringValue;
							else
								LotNumberTextBox.Text = "";
						}
						catch
						{
							LotNumberTextBox.Text = "";
						}
					}
					for (int j = 0; j <= ICMetalColorList.Items.Count - 1; j++)
					{
						if (ICMetalColorList.Items[j].Value == metalId.ToString())
						{
							ICMetalColorList.SelectedIndex = j;
							break;
						}
					}
					for (int j = 0; j <= ICMetalStampList.Items.Count - 1; j++)
					{
						if (ICMetalStampList.Items[j].Value == stampId.ToString())
						{
							ICMetalStampList.SelectedIndex = j;
							break;
						}
					}
					for (int j = 0; j <= ICItemNameList.Items.Count - 1; j++)
					{
						if (ICItemNameList.Items[j].Value == itemNameID.ToString())
						{
							ICItemNameList.SelectedIndex = j;
							break;
						}
					}
					if (karatageId.ToString() != "0")
					{
						for (int j = 0; i <= ICMetalKaratageList.Items.Count - 1; j++)
						{
							if (ICMetalKaratageList.Items[j].Value == karatageId.ToString())
							{
								ICMetalKaratageList.SelectedIndex = j;
								break;
							}
						}
					}
					else
						ICMetalKaratageList.SelectedIndex = -1;
					if (measures11[i].MeasureId.ToString() == "210")//Engraving
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								ICEngravingBox.Text = result.StringValue;
							else
								ICEngravingBox.Text = "";
						}
						catch
						{
							ICEngravingBox.Text = "";
						}
					}
				}
				bool weightFound = false;
				foreach (MeasureValueCpModel weightMeasure in measures11)
                {
					if (weightMeasure.MeasureId == "2")//total weight
					{
						var result = GetCurrMeasureValue(weightMeasure.MeasureId.ToString(), values);
						if (result != null)
						{
							weightFound = true;
							break;
						}
					}
                }
				if (!weightFound)
                {
					if (Session["TotalWeight"] != null && ResetField.Value == "false")//collect total weight from another parts calc weight
					{
						List<MeasureValueModel> icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
						if (icTotalWeight.Count > 0)
						{
							double icTW = 0;
							foreach (MeasureValueModel icTVItem in icTotalWeight)
							{
								double itemWeight = Convert.ToDouble(icTVItem.MeasureValue);
								icTW += itemWeight;
							}
							ICTotalWeightTxt.Text = icTW.ToString("0.##");
						}
					}
				}
			}
			if (measures2.PartTypeId == 10 || measures2.PartTypeId == 11)//WDSS/CDSS
			{
				DiaParms.Visible = false;
				MetalParms.Visible = false;
				MountParms.Visible = false;
				ICParms.Visible = false;
				WDSSParms.Visible = true;
				CSSParms.Visible = false;
				calcBtn.Visible = false;
				for (int i = 0; i < measures11.Count; i++)
				{
					if (measures11[i].MeasureId.ToString() == "2")//weight
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
							{
								WDSSTotalWeightTxt.Text = result.MeasureValue;
								if (Session["TotalWeight"] != null)
								{
									List<MeasureValueModel> icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
									MeasureValueModel oldItem = icTotalWeight.Find(m => m.PartId == measures11[i].PartId);
									if (oldItem == null)
									{
										icTotalWeight.Add(result);
										Session["TotalWeight"] = icTotalWeight;
									}
								}
							}
							else
								WDSSTotalWeightTxt.Text = "";
						}
						catch
						{
							WDSSTotalWeightTxt.Text = "";
						}
					}
					if (measures11[i].MeasureId.ToString() == "65")//# of units
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								WDSSNumberOfUnitsTxt.Text = result.MeasureValue;
							else
								WDSSNumberOfUnitsTxt.Text = "";
						}
						catch
						{
							WDSSNumberOfUnitsTxt.Text = "";
						}
					}
				}
			}

			if (measures2.PartTypeId == 12)//CSS
			{
				DiaParms.Visible = false;
				MetalParms.Visible = false;
				MountParms.Visible = false;
				ICParms.Visible = false;
				WDSSParms.Visible = false;
				CSSParms.Visible = true;
				calcBtn.Visible = false;
				for (int i = 0; i < measures11.Count; i++)
				{
					if (measures11[i].MeasureId.ToString() == "2")//weight
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
							{
								CSSTotalWeightTxt.Text = result.MeasureValue;
								if (Session["TotalWeight"] != null)
								{
									List<MeasureValueModel> icTotalWeight = (List<MeasureValueModel>)Session["TotalWeight"];
									MeasureValueModel oldItem = icTotalWeight.Find(m => m.PartId == measures11[i].PartId);
									if (oldItem == null)
									{
										icTotalWeight.Add(result);
										Session["TotalWeight"] = icTotalWeight;
									}
								}
							}
							else
								CSSTotalWeightTxt.Text = "";
						}
						catch
						{
							CSSTotalWeightTxt.Text = "";
						}
					}
					if (measures11[i].MeasureId.ToString() == "65")//# of units
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[i].MeasureId.ToString(), values);
							if (result != null)
								CSSNumberOfUnitsTxt.Text = result.MeasureValue;
							else
								CSSNumberOfUnitsTxt.Text = "";
						}
						catch
						{
							CSSNumberOfUnitsTxt.Text = "";
						}
					}
				}
			}


			var ds = new DataSet();
			ds.ReadXml(Server.MapPath("Measure2Columns.xml"));

			if (measures2.PartTypeId == 6 || measures2.PartTypeId == 7)//metal
			{
				DiaParms.Visible = false;
				WDSSParms.Visible = false;
				CSSParms.Visible = false;
				ICParms.Visible = false;
				MetalParms.Visible = true;
				MetalStampList.Visible = true;
				MetalColorList.Visible = true;
				MetalKaratageList.Visible = true;
				calcBtn.Visible = false;
				var metalColor = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "44");
				var metalStamp = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "45");
				var metalKaratage = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "259");
				var blockKaratageList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Karatage, Page);
				var finalMetalKaratage = metalKaratage.Where(x => !blockKaratageList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName))));
				MetalColorList.DataSource = metalColor;
				MetalColorList.DataBind();
				MetalStampList.DataSource = metalStamp;
				MetalStampList.DataBind();
				MetalKaratageList.DataSource = finalMetalKaratage;
				MetalKaratageList.DataBind();
				MetalColorList.SelectedIndex = -1;
				MetalStampList.SelectedIndex = -1;
				MetalKaratageList.SelectedIndex = -1;

				for (int j = 0; j <= measures11.Count - 1; j++)
				{
					var value = GetCurrMeasureValue(measures11[j].MeasureId.ToString(), values);
					if (value == null)
						continue;
					if (measures11[j].MeasureId == "44")//metal
						metalId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[j].MeasureId == "45")//stamp
						stampId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[j].MeasureId == "259")//karatage
						karatageId = Convert.ToInt32(value.MeasureValueId);
					if (measures11[j].MeasureId.ToString() == "255")//chain length
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[j].MeasureId.ToString(), values);
							if (result != null)
								ChainLenghtTxt.Text = result.MeasureValue;
							else
								ChainLenghtTxt.Text = "";
						}
						catch
						{
							ChainLenghtTxt.Text = "";
						}
					}
					if (measures11[j].MeasureId.ToString() == "210")//Engraving
					{
						try
						{
							var result = GetCurrMeasureValue(measures11[j].MeasureId.ToString(), values);
							if (result != null)
								EngravingTxt.Text = result.StringValue;
							else
								EngravingTxt.Text = "";
						}
						catch
						{
							EngravingTxt.Text = "";
						}
					}
				}
				for (int i = 0; i <= MetalColorList.Items.Count - 1; i++)
				{
					if (MetalColorList.Items[i].Value == metalId.ToString())
					{
						MetalColorList.SelectedIndex = i;
						break;
					}
				}
				for (int i = 0; i <= MetalStampList.Items.Count - 1; i++)
				{
					if (MetalStampList.Items[i].Value == stampId.ToString())
					{
						MetalStampList.SelectedIndex = i;
						break;
					}
				}
				for (int i = 0; i <= MetalKaratageList.Items.Count - 1; i++)
				{
					if (MetalKaratageList.Items[i].Value == karatageId.ToString())
					{
						MetalKaratageList.SelectedIndex = i;
						break;
					}
				}
				metalId = 0;
				stampId = 0;
				karatageId = 0;
			}
			if (measures2.PartTypeId == 5)//mount
			{
				DiaParms.Visible = false;
				WDSSParms.Visible = false;
				CSSParms.Visible = false;
				ICParms.Visible = false;
				MetalParms.Visible = false;
				MountParms.Visible = true;
				MountMetalList.Visible = true;
				MountStampList.Visible = true;
				MountTypeList.Visible = true;
				MountKaratageList.Visible = true;
				var mountMetal = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "44");
				var mountStamp = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "45");
				var mountType = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "46");
				var mountKaratage = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == "259");
				var blockKaratageList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Karatage, Page);
				var finalMountKaratage = mountKaratage.Where(x => !blockKaratageList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName))));
				MountMetalList.DataSource = mountMetal;
				MountMetalList.DataBind();
				MountStampList.DataSource = mountStamp;
				MountStampList.DataBind();
				MountTypeList.DataSource = mountType;
				MountTypeList.DataBind();
				MountKaratageList.DataSource = finalMountKaratage;
				MountKaratageList.DataBind();
				MountMetalList.SelectedIndex = -1;
				MountStampList.SelectedIndex = -1;
				MountTypeList.SelectedIndex = -1;
				MountKaratageList.SelectedIndex = -1;

				for (int j = 0; j <= measures11.Count - 1; j++)
				{
					var value = GetCurrMeasureValue(measures11[j].MeasureId.ToString(), values);
					if (value == null)
						continue;
					if (measures11[j].MeasureId == "44")//metal
						metalId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[j].MeasureId == "45")//stamp
						stampId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[j].MeasureId == "46")//mount type
						mountId = Convert.ToInt32(value.MeasureValueId);
					else if (measures11[j].MeasureId == "259")//karatage
						karatageId = Convert.ToInt32(value.MeasureValueId);
					
				}
				for (int i = 0; i <= MountMetalList.Items.Count - 1; i++)
				{
					if (MountMetalList.Items[i].Value == metalId.ToString())
					{
						MountMetalList.SelectedIndex = i;
						break;
					}
				}
				for (int i = 0; i <= MountStampList.Items.Count - 1; i++)
				{
					if (MountStampList.Items[i].Value == stampId.ToString())
					{
						MountStampList.SelectedIndex = i;
						break;
					}
				}
				for (int i = 0; i <= MountTypeList.Items.Count - 1; i++)
				{
					if (MountTypeList.Items[i].Value == mountId.ToString())
					{
						MountTypeList.SelectedIndex = i;
						break;
					}
				}
				for (int i = 0; i <= MountKaratageList.Items.Count - 1; i++)
				{
					if (MountKaratageList.Items[i].Value == karatageId.ToString())
					{
						MountKaratageList.SelectedIndex = i;
						break;
					}
				}
				metalId = 0;
				stampId = 0;
				karatageId = 0;
				mountId = 0;
			}
			OldNumText.Text = QueryUtils.GetOldItemNumber(selItem.BatchId.ToString(), selItem.ItemCode, this);
			if ((selItem.FullItemNumber == OldNumText.Text) ||
				(selItem.FullItemNumber.Substring(0, selItem.FullItemNumber.Length - 2) == OldNumText.Text))
				DataForLabel.Text = "Data For # " + selItem.FullItemNumber;
			else
				DataForLabel.Text = "Data For # " + selItem.FullItemNumber + @"(" + OldNumText.Text + @")";
			sProgramm.Style.Add("visibility", "visible");
			var cpModel = QueryCpUtils.GetCustomerProgramByBatchNumber(selItem.FullItemNumber.ToString(), this);
			Programm.Text = cpModel.CustomerProgramName.ToString();
			Programm.Visible = true;
			PrefixText.Text = "";
			var prefixValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 112);
			if (prefixValue != null && prefixValue.Value != null)
			{
				PrefixText.Text = prefixValue.Value;
			}
			PrefixPanel.Visible = true;
			sCustomer.Style.Add("visibility", "visible");
			DataSet ds1 = AdditionalServicesUtils.GetBatchRecheckChangeSKU(selItem.FullBatchNumber, this);
			//DataTable dt1 = ds1.Tables[0].Select("BatchCode=" + measures11[0].ForItem.BatchCode.ToString()).CopyToDataTable();
			Customer.Text = ds1.Tables[0].Rows[0]["CustomerName"].ToString();
			Customer.Visible = true;
			return;
			//else
			//{
			//	fieldsetSarin.Visible = false;
			//}
			//RptMeasureList.Visible = false;
			DataTable dt = new DataTable();

			if (ClarityRadio.SelectedValue == "Loose")
			{

				//if (QueryUtils.GetPartTypeIDbyPartID(PartTree.SelectedNode.Value,Page) == "1") // Diamond
				if (measures2.PartTypeId == 1)//Diamond
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["Diamond"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 10)//WDSS
				{
					// dt = MeasureDT.Select(StrinbgtoSplit(Loose_WDSS)).CopyToDataTable();
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["WDSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 11)//CDSS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["CDSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 12) //CSS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["CSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 2) //CD
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["CD"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 3)//CS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[0]["CS"].ToString())).CopyToDataTable();
				}
			}
			else if (ClarityRadio.SelectedValue == "Mounted")
			{
				if (measures2.PartTypeId == 15)//em Container
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["IC"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 1)//Item
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["Diamond"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 10)//WDSS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["WDSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 11)//CDSS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["WDSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 12)//CSS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["CSS"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 2)//CD
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["CD"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 3)//CS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[1]["CS"].ToString())).CopyToDataTable();
				}
			}
			else if (ClarityRadio.SelectedValue == "India")
			{
				if (measures2.PartTypeId == 1)//Diamond
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[2]["Diamond"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 2)//CD
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[2]["CD"].ToString())).CopyToDataTable();
				}
				else if (measures2.PartTypeId == 3)//CS
				{
					dt = MeasureDT.Select(StrinbgtoSplit(ds.Tables[0].Rows[2]["CS"].ToString())).CopyToDataTable();
				}
			}
			// dt = MeasureDT.Select(" MeasureName like '%Weight%' or MeasureName like '%units%' ").CopyToDataTable();

			RptMeasureList.DataSource = dt;// MeasureDT.Select("MeasureName='Prefix' or MeasureName like '%Weight%'");
			int focusNumber = 0;
			for (int i = 0; i <= dt.Rows.Count - 1; i++)
			{
				if (dt.Rows[i]["MeasureID"].ToString() == "4")//measure weight
				{
					focusNumber = i;
					break;
				}

			}
			RptMeasureList.DataBind();

			/*Focus Measured Weight if available*/
			if (measures2.PartTypeId == 1)
			{
				var txtbx1 = RptMeasureList.Items[focusNumber].FindControl("RepeatTxt") as TextBox;
				if (txtbx1 != null)
				{
					txtbx1.Attributes.Add("onfocus", "this.select();");
					txtbx1.Focus();
				}
				else
				{
					foreach (RepeaterItem item in RptMeasureList.Items)
					{
						var txtbx = item.FindControl("RepeatTxt") as TextBox;
						if (txtbx != null)
						{
							txtbx.Attributes.Add("onfocus", "this.select();");
							txtbx.Focus();
							break;
						}
					}
				}
			}
			DataTable dt1 = (DataTable)Session["ItemList"];
			if (dt1 != null)
			{
				var prevNumber = Utils.getPrevItemCode(dt1, selItem.FullItemNumber);
				if (prevNumber != null)
					OldNumText.Text = prevNumber;
				else
					OldNumText.Text = QueryUtils.GetOldItemNumber(selItem.BatchId.ToString(), selItem.ItemCode, this);
			}
			//DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			//OldNumText.Text = QueryUtils.GetOldItemNumber(selItem.BatchId.ToString(), selItem.ItemCode, this);
			//if ((selItem.FullItemNumber == OldNumText.Text) ||
			//    (selItem.FullItemNumber.Substring(0, selItem.FullItemNumber.Length - 2) == OldNumText.Text))
			//    DataForLabel.Text = "Data For # " + selItem.FullItemNumber;
			//else
			//    DataForLabel.Text = "Data For # " + selItem.FullItemNumber + @"(" + OldNumText.Text + @")";
			//sProgramm.Style.Add("visibility", "visible");
			//var cpModel = QueryCpUtils.GetCustomerProgramByBatchNumber(selItem.FullItemNumber.ToString(), this);
			//Programm.Text =cpModel.CustomerProgramName.ToString();
			//Programm.Visible = true;
			//sProgramm.Visible = true;

			//sCustomer.Style.Add("visibility", "visible");
			//DataSet ds1 = AdditionalServicesUtils.GetBatchRecheckChangeSKU(Convert.ToInt32(selItem.FullBatchNumber.ToString()), this);
			////DataTable dt1 = ds1.Tables[0].Select("BatchCode=" + measures11[0].ForItem.BatchCode.ToString()).CopyToDataTable();
			//Customer.Text =  ds1.Tables[0].Rows[0]["CustomerName"].ToString();
			//Customer.Visible = true;
			//sCustomer.Visible = true;


			//Populate Prefix
			//PrefixText.Text = "";
			//var prefixValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 112);
			//if (prefixValue != null && prefixValue.Value != null)
			//{
			//    PrefixText.Text = prefixValue.Value;
			//}
			//PrefixPanel.Visible = true;
		}
		public string StrinbgtoSplit(string str)
		{
			string strlist = string.Empty;
			string[] firstNames = str.Split(',');
			foreach (string firstName in firstNames)
			{
				strlist = strlist + "MeasureName like '%" + firstName + "%' or ";
			}
			strlist = strlist.Substring(0, strlist.Length - 3);
			return strlist;
		}
		private MeasureValueModel GetCurrMeasureValue(string MID, List<MeasureValueModel>values)
		{
			var measure = GetMeasureSelected(MID);
			//var values = GetMeasureValuesByItem();
			return values.Find(m => m.MeasureId == measure.MeasureId && m.PartId == measure.PartId);
		}
		//private MeasureValueCpModel GetMeasureSelected()
		//{
		//    var measureId = lstMeasures.SelectedValue;
		//    var partId = lstParts.SelectedValue;
		//    if (string.IsNullOrEmpty(measureId)) return null;
		//    var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel>;
		//    if (measures == null) return null;
		//    return measures.Find(m => m.MeasureId == measureId && m.PartId == Convert.ToInt32(partId));
		//}

		private MeasureValueCpModel GetMeasureSelected(string MID)
		{
			var measureId = MID;
			var partId = PartTree.SelectedNode.Value;
			if (string.IsNullOrEmpty(measureId)) return null;
			var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel>;
			if (measures == null) return null;
			return measures.Find(m => m.MeasureId == measureId && m.PartId == Convert.ToInt32(partId));
		}
		private List<MeasureValueModel> GetMeasureValuesByItem()
		{
			SetViewState(null, SessionConstants.GradeMeasureValues);
			var itemModel = GetItemFromFullList(lstItemList.SelectedValue);
			var values = GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ??
						 new List<MeasureValueModel>();
			if (values.FindAll(m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode).Count == 0)
			{
				//values.AddRange(QueryUtils.GetMeasureValues(itemModel, "6", this));
				values.AddRange(QueryUtils.GetMeasureValues(itemModel, "", this));
				SetViewState(values, SessionConstants.GradeMeasureValues);
			}
			return
				values.FindAll(
					m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode);
		}
		private string SetEnumData(int measureId)
		{
			// var value = GetCurrMeasureValue();
			var enumMeasures = GetViewState(SessionConstants.BulkEnumMeasures) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
			if (enumMeasures.Count == 0)
			{
				enumMeasures = QueryUtils.GetEnumMeasure(this);
				SetViewState(enumMeasures, SessionConstants.BulkEnumMeasures);
			}
			var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
			//  var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
			//if (enums.Count == 0) return;
			//enums.Sort((m1, m2) => String.Compare(m1.ValueTitle, m2.ValueTitle, StringComparison.Ordinal));
			//if (value == null)
			//{
			//    enums.Insert(0, EnumMeasureModel.GetEmptyModel(measureId));
			//}


			//MeasureEnumField.DataSource = enums;
			//MeasureEnumField.DataBind();

			//lstMeasureEnumField.DataSource = enums;
			//lstMeasureEnumField.DataBind();
			return enums.Count == 0 ? "" : enums[0].MeasureValueName;
		}
		private SingleItemModel GetItemFromFullList(string itemNumber)
		{
			var list = GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel>;
			return list == null ? null : list.Find(m => (m.FullItemNumber == itemNumber) || (m.FullOldItemNumber == itemNumber));
		}
		private List<MeasureValueCpModel> GetMeasureListByMode(SingleItemModel itemModel, String partId)
		{
			// SetViewState(null, SessionConstants.GradeMeasureList);
			var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel> ??
						 new List<MeasureValueCpModel>();

			if (measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId).Count == 0)
			{
				//var itemMeasures = QueryUtils.GetMeasureListByAcces(
				//    itemModel, rblModeSelector.SelectedValue, IgnoreCpBox.Checked, this);
				//var itemMeasures = QueryUtils.GetMeasureListByAcces(itemModel, "6", IgnoreCpFlag.Checked, this);
				var itemMeasures = QueryUtils.GetMeasureListByAcces(itemModel, "", IgnoreCpFlag.Checked, this);
				measures.AddRange(itemMeasures);
				var groups = measures.GroupBy(m => m.PartId).ToList();
				var maxCount = 0;
				foreach (var group in groups)
				{
					maxCount = maxCount < group.Count() ? group.Count() : maxCount;
				}
				//lstMeasures.Rows = maxCount + 1;
				SetViewState(measures, SessionConstants.GradeMeasureList);
			}
			if (string.IsNullOrEmpty(partId))
			{
				//-- for all parts
				return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId);
			}
			return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId && m.PartId == Convert.ToInt32(partId));
		}
		private List<MeasureValueCpModel> GetMeasureListByMode1(SingleItemModel itemModel, String partId)
		{
			var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel> ??
						 new List<MeasureValueCpModel>();

			if (measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId).Count == 0)
			{
				//var itemMeasures = QueryUtils.GetMeasureListByAcces(
				//    itemModel, rblModeSelector.SelectedValue, IgnoreCpBox.Checked, this);
				var itemMeasures = QueryUtils.GetMeasureListByAcces(
					itemModel, "4", IgnoreCpFlag.Checked, this);
				measures.AddRange(itemMeasures);
				var groups = measures.GroupBy(m => m.PartId).ToList();
				var maxCount = 0;
				foreach (var group in groups)
				{
					maxCount = maxCount < group.Count() ? group.Count() : maxCount;
				}
				//lstMeasures.Rows = maxCount + 1;
				SetViewState(measures, SessionConstants.GradeMeasureList);
			}
			if (string.IsNullOrEmpty(partId))
			{
				//-- for all parts
				return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId);
			}
			return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId && m.PartId == Convert.ToInt32(partId));
		}
		private void LoadPartsTree()
		{
			PartTree.Nodes.Clear();
			var parts = GetPartsFromView();
			if (parts.Count == 0)
			{
				return;
			}
			var data = new List<TreeViewModel>();
			foreach (var part in parts)
			{
				data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
			}
			var root = TreeUtils.GetRootTreeModel(data);
			var rootNode = new TreeNode(root.DisplayName, root.Id);

			TreeUtils.FillNode(rootNode, root);
			rootNode.Expand();

			PartTree.Nodes.Add(rootNode);
			/*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Select();
                */
			PartTree.Nodes[0].Selected = true;
			OnPartsTreeChanged(null, null);
		}
		#endregion

		#region Elements
		private List<String> commentNums = new List<String> { "113", "48", "105", "49", "51", "86", "97", "102", "103", "104" };
		private void CreateMeasurePanel()
		{
			List<ExpandableControlModel> Elements = new List<ExpandableControlModel>();

			XmlDocument doc = new XmlDocument();
			doc.Load(Server.MapPath("ClarityData.xml"));

			foreach (XmlNode node in doc.SelectNodes("//Element"))
			{
				ExpandableControlModel elem;
				elem = ExpandableControlModel.CreateNew(node);

				if (elem.DropDownPanelVis)
				{
					elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == elem.ElemMeasure);
				}

				if (elem.ElemType == "Standard")
				{
					var buttonList = ButtonDetailModel.CreateNewStandard(elem.DropDownEnums);
					if (elem.ElemMeasure == "18" || elem.ElemMeasure == "19")
					{
						var noneEntry = buttonList[0];
						buttonList.Remove(buttonList[0]);
						buttonList.Insert(buttonList.Count() - 1, noneEntry);
					}
					elem.BtnRepeaterSource = buttonList;
				}
				else if (elem.ElemType == "Filter" || elem.ElemType == "Custom")
				{
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNew(node);
				}
				else if (elem.ElemType == "Comments")
				{
					/*
					foreach (XmlNode item in node.SelectNodes("CommentSource/Comments"))
					{
						commentNums.Add(item.InnerText);
					}
					*/
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNewComments(GetMeasureEnumsFromView(), node);
				}

				Elements.Add(elem);
			}

			ElementPanel.DataSource = Elements.Where(item => item.ElemName == "Picture");
			ElementPanel.DataBind();

			//Make filtering dropdown work
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				if ((element.FindControl("ElementType") as HiddenField).Value == "Filter")
				{
					(element.FindControl("BigTxt") as TextBox).Attributes.Add("onKeyUp", "FilterShapes(this)");
					(element.FindControl("BigTxt") as TextBox).Attributes.Add("onFocus", "FilterShapes(this)");
				}
			}

			//Apply styles to particular elements
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementType") as HiddenField).Value.StartsWith("Pic"))
				{
					(item.FindControl("Pic") as Panel).Style["display"] = "inline-block";
					//(item.FindControl("Pic") as Panel).Style["clear"] = "right";
				}
				else
				{
					(item.FindControl("Pic") as Panel).Style["display"] = "none";
				}
			}
		}
		//On selecting item from dropdown list
		protected void OnDropDownSelectedIndexChanged(object sender, EventArgs e)
		{
			var listBox = sender as ListBox;
			var textBox = listBox.Parent.Parent.FindControl("BigTxt") as TextBox;
			var smTextBox = listBox.Parent.Parent.FindControl("SmTxt") as TextBox;
			textBox.Text = listBox.SelectedItem.Text;
			smTextBox.Text = listBox.SelectedItem.Text;
		}

		protected void OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
			var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
			bool changed = false;

			foreach (ListItem item in listBox.Items)
			{
				if (textBox.Text == item.Text)
				{
					listBox.SelectedValue = item.Value;
					smTextBox.Text = smTextBox.Text;
					changed = true;
					break;
				}
			}

			if (!changed)
			{
				if (listBox.SelectedItem != null)
				{
					textBox.Text = listBox.SelectedItem.Text;
				}
				else
				{
					textBox.Text = "";
				}
			}
		}

		protected void OnButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("RepeatButton") as Button;
			var val = e.Item.FindControl("BtnValue") as HiddenField;
			var redir = e.Item.FindControl("BtnLink") as HiddenField;
			var cat = e.Item.FindControl("BtnCat") as HiddenField;

			if ((e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value == "Comments")
			{
				var mea = e.Item.FindControl("BtnMeasure") as HiddenField;
				var buttonStyle = btn.Style["border-bottom-color"];//Comment button styles

				if (mea.Value != "0")
				{
					var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
					if (itemValues != null)
					{
						var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
						if (itemValue != null)
						{
							var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
							EnumMeasureModel enumValue;
							if (buttonStyle == "#821717" || buttonStyle == "0")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
							{
								enumValue = enumValues.Find(m => m.ValueTitle == "");
								//btn.Style["background-color"] = "rgb(73,175,205)";
							}
							else
							{
								enumValue = enumValues.Find(m => m.MeasureValueName == val.Value);
								//btn.Style["background-color"] = "red";

								//Find control that is red, if any, for that measure value, and change it to default
								/*
								var buttonList = e.Item.Parent as Repeater;
								foreach (RepeaterItem button in buttonList.Items)
								{
									if ((button.FindControl("BtnMeasure") as HiddenField).Value == mea.Value)
									{
										if (btn != button.FindControl("RepeatButton") as Button && (button.FindControl("RepeatButton") as Button).Style["background-color"] == "red")
										{
											SetCommentButtonState(button.FindControl("RepeatButton") as Button, false);
											//(button.FindControl("RepeatButton") as Button).Style["background-color"] = "rgb(73,175,205)";
											break;
										}
									}
								}
								*/
							}

							if (itemValue != null && enumValue != null)
							{
								var newValue = enumValue.MeasureValueId.ToString();
								itemValue.Value = newValue;
							}
							PopulateComments();
						}
					}
				}
			}
			else if (val.Value != "0")
			{
				var ddl = e.Item.Parent.Parent.FindControl("DropDownListBox") as ListBox;
				var elemMeasure = e.Item.Parent.Parent.Parent.FindControl("ElementMeasure") as HiddenField;
				ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == val.Value).MeasureValueId.ToString();
				OnDropDownSelectedIndexChanged(ddl, null);
			}

			if (redir.Value != "0")
			{
				if (redir.Value == "Small")
				{
					ElementEditing.Value = "None";
				}
				CategoryEditing.Value = redir.Value;
				SetButtonCategory();
			}
		}

		protected void OnSmBtnClick(object sender, EventArgs e)
		{
			var btn = sender as Button;
			var smallPanel = btn.Parent as Panel;
			var bigPanel = smallPanel.Parent.FindControl("BigPanel") as Panel;
			var eleName = bigPanel.Parent.FindControl("ElementName") as HiddenField;

			ElementEditing.Value = eleName.Value;

			if (ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
			{
				CategoryEditing.Value = ClarityRadio.SelectedValue;
			}
			else
			{
				CategoryEditing.Value = "Root";
			}

			SetButtonCategory();
		}

		private void SetButtonCategory()
		{
			Panel activeBigPanel = null;

			//Display big panel for the current element
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					activeBigPanel = item.FindControl("BigPanel") as Panel;

					//(item.FindControl("SmallPanel") as Panel).Visible = false;
					activeBigPanel.Visible = true;
				}
				else
				{
					//(item.FindControl("SmallPanel") as Panel).Visible = true;
					(item.FindControl("BigPanel") as Panel).Visible = false;
				}
			}

			//Display buttons in the big panel for the current category
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					foreach (RepeaterItem btnItem in (item.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if ((btnItem.FindControl("BtnCat") as HiddenField).Value == CategoryEditing.Value)
						{
							btnItem.FindControl("RepeatButton").Visible = true;
						}
						else
						{
							btnItem.FindControl("RepeatButton").Visible = false;
						}
					}
					break;
				}
			}
		}
		private void ClearButtonCategory()
		{
			ElementEditing.Value = "";
			CategoryEditing.Value = "";
			SetButtonCategory();
		}

		protected void ClarityRadioChanged(object sender, EventArgs e)
		{
			//if(ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
			//{
			//    CategoryEditing.Value = ClarityRadio.SelectedValue;
			//    SetButtonCategory();
			//}


			LodaMeasureforEditing();

		}

		private void ShowImages(string path, string option)
		{
			string imgPath;
			string errMsg;

			Panel imgPanel = null;
			ShowHideImages(true);
			if (option == "Shape")
			{
				//string pngPath;
				if (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\")
				{
					path = path.Substring(1);
				}
				var pngStream = new MemoryStream();
				var result = Utlities.GetShapeImageUrlAzure(path, out imgPath, out errMsg, this, /*out pngPath, */ out pngStream);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PictureShape")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).Visible = true;
				}
				else
				{
					(imgPanel.FindControl("PicImg") as Image).Visible = false;
				}
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Shape");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}
			else if (option == "Picture")
			{
				var ms = new MemoryStream();
				var fileType = "";
				//var result = Utlities.GetPictureImageUrl(path, out imgPath, out errMsg, this);
				//GetPictureImageUrl(string dbPicture, out MemoryStream ms, out string errMsg, out string fileType, Page p)
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PicturePicture")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					//var webClient = new WebClient();
					//var stream = webClient.OpenRead(imgPath);
					//var fileType = imgPath.Substring(imgPath.Length - 3, 3);
					//var ms = new MemoryStream();
					//stream.CopyTo(ms);
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;
					(imgPanel.FindControl("PicImg") as Image).Visible = true;
				}
				else
				{
					(imgPanel.FindControl("PicImg") as Image).Visible = false;
				}
				//-- Path to picture
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Picture");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}
		}
		private void ShowImagesNew(string path, string option)
		{
			//<asp:Image runat="server" ID="PicImg" Width="180px" Visible='<%# Eval("PicImgVis")%>'></asp:Image>
			//<asp:Image runat="server" ID="PicImg" Width="180px"></asp:Image>
			Panel imgPanel = null;
			var imgPanelName = "Picture" + option;
			var imgUrl = Utlities.GetPictureImageUrl(path, this);

			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				if ((elem.FindControl("ElementType") as HiddenField).Value == imgPanelName)
				{
					imgPanel = elem.FindControl("Pic") as Panel;
					break;
				}
			}
			if (imgPanel != null)
			{
				var imgControl = imgPanel.FindControl("Image") as Image;
				var imgLabel = imgPanel.FindControl("PicLbl") as Label;
				if (option == "Shape")
				{
					imgUrl = imgUrl.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
					path = (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\") ? path.Substring(1) : path;
					path = path.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
				}

				imgControl.ImageUrl = imgUrl;
				imgControl.Style["opacity"] = "1";
				imgControl.AlternateText = "NoImage";
				imgLabel.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, option);
			}
			ShowHideImages(true);
		}
		private void ShowHideImages(bool show)
		{
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				(elem.FindControl("Pic") as Panel).Visible = show;
			}
		}
		#endregion

		#region IntExtComments
		protected void OnIntExtCommentChanged(object sender, EventArgs e)
		{
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var commentBox = sender as TextBox;
			int measureId = 0;

			if (commentBox.ID == "InternalComments")
			{
				measureId = 26;
			}
			else if (commentBox.ID == "ExternalComments")
			{
				measureId = 9;
			}

			if (measureId != 0)
			{
				var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == measureId);
				if (itemValue != null)
				{
					itemValue.Value = commentBox.Text;
				}
			}
		}
		#endregion

		#region Laser Inscription
		protected void OnLaserInscriptionChanged(object sender, EventArgs e)
		{
			var liscBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 67);
			if (itemValue != null)
			{
				itemValue.Value = liscBox.Text;
			}
		}
		#endregion

		#region Prefix
		protected void OnPrefixChanged(object sender, EventArgs e)
		{
			var prefBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 112);
			if (itemValue != null)
			{
				itemValue.Value = prefBox.Text;
			}
		}
		#endregion

		/*
		#region Shapes
		protected void OnShapesChanged(object sender, EventArgs e)
		{
			ShapesTextBox.Text = ShapesList.SelectedItem.Text;
		}

		protected void OnShapesButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ShapesButton") as Button;
			var val = e.Item.FindControl("ShapesButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ShapesButtonRedirect") as HiddenField;

			if(val.Value != "0")
			{
				//Input selected value into textbox
				ShapesList.SelectedValue = val.Value;
				OnShapesChanged(null, null);
			}

			if(redir.Value != "0")
			{
				//Redirect to new button menu
				ShapesCategoryEditing.Value = redir.Value;
				InitShapesButtons();
			}
		}

		private void InitShapesButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ShapesList.Items;

			if (ShapesCategoryEditing.Value == "Root")
			{
				//List of Categories
				btnCategories.Add(new ButtonDetailUtils("Generic Type 1", "Generic Type 1"));
				btnCategories.Add(new ButtonDetailUtils("Generic Type 2", "Generic Type 2"));
				btnCategories.Add(new ButtonDetailUtils("Signet Branded", "Signet Branded"));
				btnCategories.Add(new ButtonDetailUtils("Non Signet", "Non Signet"));
				btnCategories.Add(new ButtonDetailUtils("Shapes For Plot", "Shapes For Plot"));
				btnCategories.Add(new ButtonDetailUtils("Color Stones", "Color Stones"));
				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if(ShapesCategoryEditing.Value == "Generic Type 1")
			{
				//List of Shape Names
				names.Add("Emerald");
				names.Add("Heart Brill");
				names.Add("Marquise Brill");
				names.Add("Oval Brill");
				names.Add("Pear Brill");
				names.Add("Princess Regular");
				names.Add("Radiant");
				names.Add("Rectangular Brilliant");
				names.Add("Round Brill Default");
				names.Add("Sq. Emerald");
				names.Add("Triangular Brilliant");
				names.Add("Triangular Step Cut");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if(ShapesCategoryEditing.Value == "Generic Type 2")
			{
				//List of Shape Names
				names.Add("Half Moon");
				names.Add("Kite");
				names.Add("Mod. Heart Brilliant");
				names.Add("Mod. Rectangular Brilliant");
				names.Add("Modified Oval Brill");
				names.Add("Octagonal Mod. Brill");
				names.Add("Old European");
				names.Add("Old Mine");
				names.Add("Pear Mod. Brilliant");
				names.Add("Radiant Square");
				names.Add("Rectangular Modified Brilliant");
				names.Add("Round 108 Facets");
				names.Add("Round Mod. Brilliant");
				names.Add("Shield");
				names.Add("Single");
				names.Add("Sq. Cushion Mod.Brill");
				names.Add("Square Step");
				names.Add("Transitional");
				names.Add("Trilliant");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Signet Branded")
			{
				//List of Shape Names
				names.Add("Celebration Cushion");
				names.Add("Celebration Princess");
				names.Add("Celebration Radiant");
				names.Add("Celebration Round");
				names.Add("Leo Cushion");
				names.Add("Leo Emerald");
				names.Add("Leo Marq_6_old");
				names.Add("Leo Marquise");
				names.Add("Leo PR");
				names.Add("Leo PR Rect");
				names.Add("Leo PR/Leo RBC");
				names.Add("Leo Princess");
				names.Add("Leo Radiant");
				names.Add("Leo RBC");
				names.Add("Leo Round");
				names.Add("preTolk_PR");
				names.Add("preTolk_RBC");
				names.Add("Princess Sterling");
				names.Add("TOLKOWSKY IDEAL CUT-PR");
				names.Add("TOLKOWSKY IDEAL CUT-RBC");
				names.Add("Tolkowsky Legacy Princess");
				names.Add("Tolkowsky Legacy Round");
				names.Add("Zales Princess");
				names.Add("Zales Princess Rect");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Non Signet")
			{
				//List of Shape Names
				names.Add("Preferred105");
				names.Add("Radiant Star Cush");
				names.Add("Radiant Star Oval");
				names.Add("Radiant Star PR");
				names.Add("Radiant Star RBC");
				names.Add("Shane Cushion");
				names.Add("Shane PR");
				names.Add("Shane Princess");
				names.Add("Sitara Cushion");
				names.Add("Sitara Princess");
				names.Add("Sitara Round");
				names.Add("Sitara Round(RMB)");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Shapes For Plot")
			{
				//List of Shape Names
				
				//This is likely a mistake...
				//names.Add("Preferred105");
				//names.Add("Radiant Star Cush");
				//names.Add("Radiant Star Oval");
				//names.Add("Radiant Star PR");
				//names.Add("Radiant Star RBC");
				//names.Add("Shane Cushion");
				//names.Add("Shane PR");
				//names.Add("Shane Princess");
				//names.Add("Sitara Cushion");
				//names.Add("Sitara Princess");
				//names.Add("Sitara Round");
				//names.Add("Sitara Round(RMB)");

				//foreach (var name in names)
				//{
				//	btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				//}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Color Stones")
			{
				//List of Shape Names
				names.Add("Heart Mixed");
				names.Add("Marquise Mixed");
				names.Add("Mod.Trapezoid/Triangle Mix");
				names.Add("Octagon Mix");
				names.Add("Oval Cab");
				names.Add("Oval Cabochon");
				names.Add("Oval Double Cabochon");
				names.Add("Oval Mixed");
				names.Add("Pear Mixed");
				names.Add("Rectangular Mix");
				names.Add("Round Cabochon");
				names.Add("Round Mixed");
				names.Add("Square Mixed");
				names.Add("Square Mixed Cut");
				names.Add("Trapezoid Mixed");
				names.Add("Triangular Cabochon");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Shape", "Root"));
			}

			ShapesButtonList.DataSource = btnCategories;
			ShapesButtonList.DataBind();
		}
		#endregion

		#region Polish
		protected void OnPolishChanged(object sender, EventArgs e)
		{
			PolishTextBox.Text = PolishList.SelectedItem.Text;
		}

		protected void OnPolishButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("PolishButton") as Button;
			var val = e.Item.FindControl("PolishButtonValue") as HiddenField;
			var redir = e.Item.FindControl("PolishButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				PolishList.SelectedValue = val.Value;
				OnPolishChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				PolishCategoryEditing.Value = redir.Value;
				InitPolishButtons();
			}
		}

		private void InitPolishButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = PolishList.Items;

			if (PolishCategoryEditing.Value == "Root")
			{
				for(int c = 1; c < PolishList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(PolishList.Items[c].Text, "Collapsed", int.Parse(PolishList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (PolishCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Polish", "Root"));
			}

			PolishButtonList.DataSource = btnCategories;
			PolishButtonList.DataBind();
		}
		#endregion

		#region Symmetry
		protected void OnSymmetryChanged(object sender, EventArgs e)
		{
			SymmetryTextBox.Text = SymmetryList.SelectedItem.Text;
		}

		protected void OnSymmetryButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("SymmetryButton") as Button;
			var val = e.Item.FindControl("SymmetryButtonValue") as HiddenField;
			var redir = e.Item.FindControl("SymmetryButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				SymmetryList.SelectedValue = val.Value;
				OnSymmetryChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				SymmetryCategoryEditing.Value = redir.Value;
				InitSymmetryButtons();
			}
		}

		private void InitSymmetryButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = SymmetryList.Items;

			if (SymmetryCategoryEditing.Value == "Root")
			{
				for (int c = 1; c < SymmetryList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(SymmetryList.Items[c].Text, "Collapsed", int.Parse(SymmetryList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (SymmetryCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Symmetry", "Root"));
			}

			SymmetryButtonList.DataSource = btnCategories;
			SymmetryButtonList.DataBind();
		}
		#endregion

		#region Reflection
		protected void OnReflectionChanged(object sender, EventArgs e)
		{
			ReflectionTextBox.Text = ReflectionList.SelectedItem.Text;
		}

		protected void OnReflectionButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ReflectionButton") as Button;
			var val = e.Item.FindControl("ReflectionButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ReflectionButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				ReflectionList.SelectedValue = val.Value;
				OnReflectionChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				ReflectionCategoryEditing.Value = redir.Value;
				InitReflectionButtons();
			}
		}

		private void InitReflectionButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ReflectionList.Items;

			if (ReflectionCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(ReflectionList.Items[0].Value)));
				btnCategories.Add(new ButtonDetailUtils(ReflectionList.Items[1].Text, "Collapsed", int.Parse(ReflectionList.Items[1].Value)));
				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (ReflectionCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Reflection", "Root"));
			}

			ReflectionButtonList.DataSource = btnCategories;
			ReflectionButtonList.DataBind();
		}
		#endregion

		#region Clarity
		protected void OnClarityChanged(object sender, EventArgs e)
		{
			ClarityTextBox.Text = ClarityList.SelectedItem.Text;
		}

		protected void OnClarityButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ClarityButton") as Button;
			var val = e.Item.FindControl("ClarityButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ClarityButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				ClarityList.SelectedValue = val.Value;
				OnClarityChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				ClarityCategoryEditing.Value = redir.Value;
				InitClarityButtons();
			}
		}

		private void InitClarityButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ClarityList.Items;

			if (ClarityCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(ClarityList.Items[0].Value)));

				for (int c = 1; c < ClarityList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(ClarityList.Items[c].Text, "Collapsed", int.Parse(ClarityList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (ClarityCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Clarity", "Root"));
			}

			ClarityButtonList.DataSource = btnCategories;
			ClarityButtonList.DataBind();
		}
		#endregion

		#region Inclusion
		protected void OnInclusionChanged(object sender, EventArgs e)
		{
			InclusionTextBox.Text = InclusionList.SelectedItem.Text;
		}

		protected void OnInclusionButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("InclusionButton") as Button;
			var val = e.Item.FindControl("InclusionButtonValue") as HiddenField;
			var redir = e.Item.FindControl("InclusionButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				InclusionList.SelectedValue = val.Value;
				OnInclusionChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				InclusionCategoryEditing.Value = redir.Value;
				InitInclusionButtons();
			}
		}

		private void InitInclusionButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = InclusionList.Items;

			if (InclusionCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(InclusionList.Items[0].Value)));

				for (int c = 1; c < InclusionList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(InclusionList.Items[c].Text, "Collapsed", int.Parse(InclusionList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (InclusionCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Inclusion", "Root"));
			}

			InclusionButtonList.DataSource = btnCategories;
			InclusionButtonList.DataBind();
		}
		#endregion
		*/

		/*
		#region Shape Panel
		private void ShowShape(string dbShape)
		{
			string imgPath;
			string errMsg;
			string pngPath;
			ShapePanel.Visible = true;
			if (dbShape.Substring(0, 1) == "/" || dbShape.Substring(0, 1) == "\\")
			{
				dbShape = dbShape.Substring(1);
			}
			var result = Utlities.GetShapeImageUrl(dbShape, out imgPath, out errMsg, this, out pngPath);
			if (result)
			{
				itemShape.ImageUrl = pngPath;// imgPath;
				itemShape.Visible = true;
			}
			else
			{
				itemShape.Visible = false;
			}
			//-- Path to picture
			ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", imgPath, "Shape");
			ErrShapeField.Text = errMsg;

		}
		private void HideShapePanel()
		{
			ShapePanel.Visible = false;
		}
		#endregion

		#region Picture Panel
		private void ShowPicture(string dbPicture)
		{
			string imgPath;
			string errMsg;
			var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
			if (result)
			{
				itemPicture.ImageUrl = imgPath;
				itemPicture.Visible = true;
			}
			else
			{
				itemPicture.Visible = false;
			}
			//-- Path to picture
			PicturePathAbbr.Text = ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", imgPath, "Picture");
			ErrPictureField.Text = errMsg;

		}
		private void HidePicturePanel()
		{
			itemPicture.Visible = false;
			PicturePathAbbr.Text = "";
			ErrPictureField.Text = "";
		}
		#endregion
		*/

		#region Data
		private void LoadMeasuresForEdit(string itemNumber)
		{
			var itemModel = GetItemModelFromView(itemNumber);
			var enumsMeasure = GetMeasureEnumsFromView();
			var parts = GetPartsFromView();
			string itemCode = itemNumber.Substring(itemNumber.Length - 2);
			List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
			List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
			if (measureValuesBatch.Count == 0)
			{
				measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
				SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
			}
			foreach (var measure in measureValuesBatch)
				if (Utils.FillToTwoChars(measure.ItemCode) == itemCode && itemModel.BatchId == measure.BatchId)
					measureValues.Add(measure);
			//alex var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
			/* TODO replace this code with something that works with our Element objects - done */
			var currPart = PartTree.SelectedValue;
			var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
			if (withShapes.Count > 0)
			{
				var path = withShapes[0].ShapePath2Drawing;
				if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
				else ShowHideImages(false);
			}
			else
			{
				ShowHideImages(false);
			}
			// */
			var measures = GetMeasuresCpFromView();
			var result = new List<ItemValueEditModel>();
			foreach (var measure in measures)
			{
				if (measure.MeasureClass > 3) continue;
				var partModel = parts.Find(m => m.PartId == measure.PartId);
				if (partModel == null) continue;
				var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
				var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
						? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
				result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
			}
			result.Sort(new ItemValueEditComparer().Compare);
			SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);

		}

		private void LoadShapeForEdit(string itemNumber)
		{
			itemNumber = itemNumber.TrimStart('*');

			var itemModel = GetItemModelFromView(itemNumber);
			var enumsMeasure = GetMeasureEnumsFromView();
			var parts = GetPartsFromView();
			string itemCode = itemNumber.Substring(itemNumber.Length - 2);
			//alex var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
			List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
			List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
			if (measureValuesBatch.Count == 0)
			{
				measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
				SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
			}
			foreach (var measure in measureValuesBatch)
				if (Utils.FillToTwoChars(measure.ItemCode) == itemCode)
					measureValues.Add(measure);
			//var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
			/* TODO replace this code with something that works with our Element objects - done */
			var currPart = PartTree.SelectedValue;
			var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
			if (withShapes.Count > 0)
			{
				var path = withShapes[0].ShapePath2Drawing;
				if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
				else ShowHideImages(false);
			}
			else
			{
				ShowHideImages(false);
			}
		}

		private void LoadDataForEditing()
		{
			var itemModel = GetSelectedItemModel();
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			var enumValues = GetMeasureEnumsFromView();
			string matchedItemNumber = null, tmpBatchNumber = null;
			if (Session["MatchItemNumber"] != null)
			{
				matchedItemNumber = (string)Session["MatchItemNumber"];
				Session["MatchItemNumber"] = null;
			}
			if (Session["TMPNUMBER"] != null)
			{
				tmpBatchNumber = (string)Session["TMPNUMBER"];
				Session["TMPNUMBER"] = null;
			}
			//Test
			//var test = enumValues.FindAll(m => m.MeasureValueMeasureId == 180);//EXISTS!

			//New code
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				var smallTxt = element.FindControl("SmTxt") as TextBox;
				var bigTxt = element.FindControl("BigTxt") as TextBox;
				smallTxt.Text = "";
				bigTxt.Text = "";

				var dropDownList = element.FindControl("DropDownListBox") as ListBox;
				dropDownList.SelectedIndex = -1;

				//Populate dropdownlist and text boxes if appropriate
				var dropDownMeasure = element.FindControl("ElementMeasure") as HiddenField;
				var dropDownValue = itemValues.Find(m => m.MeasureId.ToString() == dropDownMeasure.Value);
				if (dropDownValue != null)
				{
					var dropDownEnumValue = enumValues.Find(m => m.MeasureValueMeasureId.ToString() == dropDownMeasure.Value
						&& m.MeasureValueId.ToString() == dropDownValue.Value);
					if (dropDownEnumValue != null)
					{
						smallTxt.Text = dropDownEnumValue.ValueTitle.ToString();
						bigTxt.Text = smallTxt.Text;
						dropDownList.SelectedValue = dropDownValue.Value;
					}
				}
				else//Disable button if ElementMeasure is not empty
				{
					var smallButton = element.FindControl("SmBtn") as Button;
					if (smallButton.Text == "Comments")//Disable buttons in comments
					{
						bool allDisabled = true;
						foreach (RepeaterItem bigBtn in (element.FindControl("ButtonRepeater") as Repeater).Items)
						{
							var commentMeasure = bigBtn.FindControl("BtnMeasure") as HiddenField;
							var commentValue = itemValues.Find(m => m.MeasureId.ToString() == commentMeasure.Value);
							var bigBtnBtn = bigBtn.FindControl("RepeatButton") as Button;
							if (commentValue == null && commentMeasure.Value != "0" && commentMeasure.Value != "")
							{
								SetButtonState(bigBtnBtn, "Disable");
							}
							else
							{
								SetButtonState(bigBtnBtn, "");

								if (commentMeasure.Value != "0" && commentMeasure.Value != "")
								{
									allDisabled = false;
								}
							}
						}
						if (allDisabled)
						{
							SetButtonState(smallButton, "Disabled");
						}
					}
					else
					{
						SetButtonState(smallButton, "Disable");
					}
				}
			}

			PopulateComments();

			//Populate Prefix
			PrefixText.Text = "";
			var prefixValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 112);
			if (prefixValue != null && prefixValue.Value != null)
			{
				PrefixText.Text = prefixValue.Value;
			}

			//Populate Weight
			WeightValue.Text = "";
			if (PartTree.SelectedNode.Text.ToLower().Contains("stone set"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 2);
				if (weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Total Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
			}
			else if (PartTree.SelectedNode.Text.ToLower().Contains("diamond"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 4);
				if (weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Measured Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
				else
				{
					weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 5);
					if (weightValue != null && weightValue.Value != "")
					{
						WeightTxt.Text = "Calculated Weight (ct):";
						WeightValue.Text = weightValue.Value;
					}
				}
			}

			//Populate Sarin Panel
			SarinRatio.Text = "";
			SarinMeasure.Text = "";
			var sarinMaxValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 11);
			var sarinMinValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 12);
			var sarinDepthValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 13);//Depth is wrong, need height! H_x is 13
			if (sarinMaxValue != null && sarinMinValue != null && sarinDepthValue != null)
			{
				//if(sarinMaxValue.Value != null && sarinMinValue.Value != null && sarinDepthValue.Value != null)
				if (!String.IsNullOrEmpty(sarinMaxValue.Value) && !String.IsNullOrEmpty(sarinMinValue.Value) && !String.IsNullOrEmpty(sarinDepthValue.Value))
				{
					double sarinRatio = double.Parse(sarinMaxValue.Value) / double.Parse(sarinMinValue.Value);
					SarinRatio.Text = sarinRatio.ToString("0.0000");
					SarinMeasure.Text = sarinMaxValue.Value + " x " + sarinMinValue.Value + " x " + sarinDepthValue.Value;
				}
			}

			//Populate Int/Ext Comments
			InternalComments.Text = "";
			ExternalComments.Text = "";
			var intComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 26);
			if (intComments != null && intComments.Value != null)
			{
				InternalComments.Text = intComments.Value;
			}
			var extComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 9);
			if (extComments != null && extComments.Value != null)
			{
				ExternalComments.Text = extComments.Value;
			}

			//Populate Laser Inscribed
			LaserInscription.Text = "";
			var linsc = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 67);
			if (linsc != null && linsc.Value != null)
			{
				LaserInscription.Text = linsc.Value;
			}
			DataTable dt = (DataTable)Session["ItemList"];
			if (dt != null)
			{
				var prevNumber = Utils.getPrevItemCode(dt, itemModel.FullItemNumber);
				if (prevNumber != null)
					OldNumText.Text = prevNumber;
				else
					OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
			}
			if (matchedItemNumber != null)
				matchedItemNumber = matchedItemNumber.Trim();
			if (tmpBatchNumber != null)
				tmpBatchNumber = tmpBatchNumber.Trim();
			if ((itemModel.FullItemNumber == OldNumText.Text) ||
				(itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldNumText.Text))
				DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			else
			{
				if (matchedItemNumber == null || tmpBatchNumber == null || (String.Equals(matchedItemNumber, tmpBatchNumber) == true))
					DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldNumText.Text + @")";
				else
					DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + tmpBatchNumber + @")";
			}
			//DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			//OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
			//if ((itemModel.FullItemNumber == OldTextNumber.Value) ||
			//    (itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldTextNumber.Value))
			//    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			//else
			//    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldTextNumber.Value + @")";

			//Get Old Number
			//var test = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
			ShowHideItemDetails(true);
			/*
			cmdSave.Visible = true;
			shortrpt.Visible = true;
			*/

			/* Old code for LoadDataForEditing
			//Load Shapes in ListBox
			var shapeEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 8);
			ShapesList.DataSource = shapeEnums;
			ShapesList.DataBind();

			//Load text of Shapes ListBox and Selected Index of Dropdown
			ShapesTextBox.Text = "";
			ShapesList.SelectedIndex = -1;
			ShapesPanel.Visible = false;
			var shapeValue = itemValues.Find(m => m.MeasureId == 8);
			if (shapeValue != null)
			{
				ShapesPanel.Visible = true;
				var shapeEnum = shapeEnums.Find(m => m.MeasureValueId.ToString() == shapeValue.Value);
				if (shapeEnum != null)
				{
					ShapesTextBox.Text = shapeEnum.ValueTitle.ToString();
					ShapesList.SelectedValue = shapeValue.Value;
				}
			}

			//Load Polish in ListBox
			var polishEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 18);
			PolishList.DataSource = polishEnums;
			PolishList.DataBind();

			//Load text of Polish ListBox and Selected Index of Dropdown
			PolishTextBox.Text = "";
			PolishList.SelectedIndex = -1;
			PolishPanel.Visible = false;
			var polishValue = itemValues.Find(m => m.MeasureId == 18);
			if(polishValue != null)
			{
				PolishPanel.Visible = true;
				var polishEnum = polishEnums.Find(m => m.MeasureValueId.ToString() == polishValue.Value);
				if(polishEnum != null)
				{
					PolishTextBox.Text = polishEnum.ValueTitle.ToString();
					PolishList.SelectedValue = polishValue.Value;
				}
			}

			//Load Symmetry in ListBox
			var symmetryEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 19);
			SymmetryList.DataSource = symmetryEnums;
			SymmetryList.DataBind();

			//Load text of Symmetry ListBox and Selected Index of Dropdown
			SymmetryTextBox.Text = "";
			SymmetryList.SelectedIndex = -1;
			SymmetryPanel.Visible = false;
			var symmetryValue = itemValues.Find(m => m.MeasureId == 19);
			if (symmetryValue != null)
			{
				SymmetryPanel.Visible = true;
				var symmetryEnum = symmetryEnums.Find(m => m.MeasureValueId.ToString() == symmetryValue.Value);
				if (symmetryEnum != null)
				{
					SymmetryTextBox.Text = symmetryEnum.ValueTitle.ToString();
					SymmetryList.SelectedValue = symmetryValue.Value;
				}
			}

			//Load Reflection in ListBox
			var reflectionEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 177);
			ReflectionList.DataSource = reflectionEnums;
			ReflectionList.DataBind();

			//Load text of Reflection ListBox and Selected Index of Dropdown
			ReflectionTextBox.Text = "";
			ReflectionList.SelectedIndex = -1;
			ReflectionPanel.Visible = false;
			var reflectionValue = itemValues.Find(m => m.MeasureId == 177);
			if (reflectionValue != null)
			{
				ReflectionPanel.Visible = true;
				var reflectionEnum = reflectionEnums.Find(m => m.MeasureValueId.ToString() == reflectionValue.Value);
				if (reflectionEnum != null)
				{
					ReflectionTextBox.Text = reflectionEnum.ValueTitle.ToString();
					ReflectionList.SelectedValue = reflectionValue.Value;
				}
			}

			//Load Weight textbox(es) and populate
			WeightPanel.Visible = false;
			var weightValues = itemValues.FindAll(m => m.MeasureTitle.ToLower().Contains("weight") && !m.MeasureTitle.ToLower().Contains("sarin") && 
															m.Value != null && m.Value != "" && m.Value != "0");
			if(weightValues != null)
			{
				WeightPanel.Visible = true;
				WeightPanel.DataSource = weightValues;
				WeightPanel.DataBind();
			}

			//Load Clarity in ListBox
			var clarityEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 29);
			ClarityList.DataSource = clarityEnums;
			ClarityList.DataBind();

			//Load text of Clarity ListBox and Selected Index of Dropdown
			ClarityTextBox.Text = "";
			ClarityList.SelectedIndex = -1;
			ClarityPanel.Visible = false;
			var clarityValue = itemValues.Find(m => m.MeasureId == 29);
			if (clarityValue != null)
			{
				ClarityPanel.Visible = true;
				var clarityEnum = clarityEnums.Find(m => m.MeasureValueId.ToString() == clarityValue.Value);
				if (clarityEnum != null)
				{
					ClarityTextBox.Text = clarityEnum.ValueTitle.ToString();
					ClarityList.SelectedValue = clarityValue.Value;
				}
			}

			//Load Inclusion in ListBox
			var inclusionEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 198);
			InclusionList.DataSource = inclusionEnums;
			InclusionList.DataBind();

			//Load text of Inclusion ListBox and Selected Index of Dropdown
			InclusionTextBox.Text = "";
			InclusionList.SelectedIndex = -1;
			InclusionPanel.Visible = false;
			var inclusionValue = itemValues.Find(m => m.MeasureId == 198);
			if (inclusionValue != null)
			{
				InclusionPanel.Visible = true;
				var inclusionEnum = inclusionEnums.Find(m => m.MeasureValueId.ToString() == inclusionValue.Value);
				if (inclusionEnum != null)
				{
					InclusionTextBox.Text = inclusionEnum.ValueTitle.ToString();
					InclusionList.SelectedValue = inclusionValue.Value;
				}
			}

			//Unimplemented Panels
			SarinPanel.Visible = false;
			CommentsPanel.Visible = false;
			PlotPanel.Visible = false;

			//Enable and Populate Data Label and Measure Panel
			DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			ShowHideItemDetails(true);
			cmdSave.Visible = true;
			ShapesCategoryEditing.Value = "Collapsed";
			PolishCategoryEditing.Value = "Collapsed";
			SymmetryCategoryEditing.Value = "Collapsed";
			ReflectionCategoryEditing.Value = "Collapsed";
			ClarityCategoryEditing.Value = "Collapsed";
			InclusionCategoryEditing.Value = "Collapsed";
			InitShapesButtons();
			InitPolishButtons();
			InitSymmetryButtons();
			InitReflectionButtons();
			InitClarityButtons();
			InitInclusionButtons();
			*/
		}
		private void PopulateComments()
		{
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			CommentsList.Text = "";
			if (itemValues.Count == 0)
			{
				return;
			}

			//Find comments element
			RepeaterItem commentElement = null;
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				if ((element.FindControl("ElementType") as HiddenField).Value == "Comments")
				{
					commentElement = element;
					break;
				}
			}

			foreach (var measureId in commentNums)
			{
				var commentValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId.ToString() == measureId);
				if (commentValue != null)
				{
					//Collapse button
					if (commentValue.Value == "0")
					{
						foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
						{
							if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
							{
								SetButtonState(btn.FindControl("RepeatButton") as Button, "");
							}
						}
					}
					var enumValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == measureId && m.MeasureValueId.ToString() == commentValue.Value);
					if (enumValue != null)
					{
						if (enumValue.ValueTitle != "")
						{
							CommentsList.Text += enumValue.MeasureValueName;
							CommentsList.Text += "\n";
						}

						//Update style of buttons
						if (commentElement != null)
						{
							foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
							{
								if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString() /*enumValue.MeasureValueMeasureId.ToString()*/)
								{
									var rBtn = btn.FindControl("RepeatButton") as Button;
									if (rBtn.Enabled)
									{
										if (enumValue.MeasureValueName == (btn.FindControl("BtnValue") as HiddenField).Value)
										{
											SetButtonState(rBtn, "Active");
										}
										else
										{
											SetButtonState(rBtn, "");
										}
									}
								}

							}
						}
					}
				}
				else//If commentvalue is null, clear all styles
				{
					foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
						{
							var rBtn = btn.FindControl("RepeatButton") as Button;
							if (rBtn.Enabled)
							{
								SetButtonState(rBtn, "");
							}
						}

					}
				}
			}
		}
		private void SetButtonState(Button btn, string state)
		{
			btn.Enabled = true;
			if (state.ToUpper().Contains("ITEM CONTAINER"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else if (state.Contains("Active"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else if (state.Contains("Disable"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #b3b3b3, #4d4d4d)";
				btn.Style["border-top-color"] = "#4d4d4d";
				btn.Style["border-left-color"] = "#4d4d4d";
				btn.Style["border-right-color"] = "#4d4d4d";
				btn.Style["border-bottom-color"] = "#333333";
				btn.Enabled = false;
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}

		private void SetCommentButtonState(Button btn, bool active)
		{
			if (active)
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}
		private void SetSmallButtonState(Button btn, string part)
		{
			if (part.ToUpper().Contains("ITEM CONTAINER"))
			{
				/* Too bright!
				btn.Style["background"] = "linear-gradient(to bottom, #cc0099, #990073)";
				btn.Style["border-top-color"] = "#990073";
				btn.Style["border-left-color"] = "#990073";
				btn.Style["border-right-color"] = "#990073";
				btn.Style["border-bottom-color"] = "#66004d";
				*/
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}

		void ShowHideItemDetails(bool show)
		{
			DataForLabel.Visible = show;
			PrefixPanel.Visible = show;
			OldNumPanel.Visible = show;
			MeasurementPanel.Visible = show;
			ClarityRadio.Visible = false;
			Comments.Visible = show;
			WeightPanel.Visible = show;
			IntExtCommentPanel.Visible = show;
			LaserPanel.Visible = show;
			SarinPanel.Visible = show;
			cmdSave.Visible = show;
			ReqFRanges.Visible = show;
			ReqFRangesBtn.Visible = show;
			ReqFRangesGroupList.Visible = show;
			ReqFDisplayBtn.Visible = show;
			ReqFRangeList.Visible = show;

		}

		private void GetNewValuesFromGrid()
		{
			//var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValues = GetMeasuresForEditFromView("");
			//var parts = itemValues.Select(x => x.PartName.Contains("Metal")).Distinct();
			var metalItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("metal")).Select(x => x.PartId).Distinct();
			var mountItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("mount")).Select(x => x.PartId).Distinct();
			var icItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("item container")).Select(x => x.PartId).Distinct();
			var diaItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("diamond") && !x.PartName.ToLower().Contains("set")).Select(x => x.PartId).Distinct();
			var ssItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("diamond") && x.PartName.ToLower().Contains("set")).Select(x => x.PartId).Distinct();
			var colStoneItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("color stone") && !x.PartName.ToLower().Contains("set")).Select(x => x.PartId).Distinct();
			var colStoneSetItems = itemValues.FindAll(x => x.PartName.ToLower().Contains("color stone set")).Select(x => x.PartId).Distinct();
			//var metalParts = metalItems.Select(x => x.PartId).Distinct();
			foreach (string icPart in icItems)
			{
				if (icPart == PartEditing.Value)
				{
					var icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "1");//weight(g)
					if (icValue != null)
					{
						var newDiaValue = ICWeightTxt.Text;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "2");//total weight(cts)
					if (icValue != null)
					{
						var newDiaValue = ICTotalWeightTxt.Text;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "44");//color
					if (icValue != null)
					{
						var newDiaValue = ICMetalColorList.SelectedValue;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "45");//stamp
					if (icValue != null)
					{
						var newDiaValue = ICMetalStampList.SelectedValue;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "259");//karatage
					if (icValue != null)
					{
						var newDiaValue = ICMetalKaratageList.SelectedValue;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "109");//Item Name
					if (icValue != null)
					{
						var newDiaValue = ICItemNameList.SelectedValue;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "88");//Item Name
					if (icValue != null)
					{
						var newDiaValue = LotNumberTextBox.Text;
						icValue.Value = newDiaValue;
					}
					icValue = itemValues.Find(m => m.PartId == icPart && "" + m.MeasureId == "210");//Item Name
					if (icValue != null)
					{
						var newDiaValue = ICEngravingBox.Text;
						icValue.Value = newDiaValue;
					}
				}
			}
			foreach (string ssPart in ssItems)
			{
				if (ssPart == PartEditing.Value)
				{
					var ssValue = itemValues.Find(m => m.PartId == ssPart && "" + m.MeasureId == "2");//total weight
					if (ssValue != null)
					{
						var newDiaValue = WDSSTotalWeightTxt.Text;
						ssValue.Value = newDiaValue;
					}
					ssValue = itemValues.Find(m => m.PartId == ssPart && "" + m.MeasureId == "65");//# of units
					if (ssValue != null)
					{
						var newDiaValue = WDSSNumberOfUnitsTxt.Text;
						ssValue.Value = newDiaValue;
					}
				}
			}
			foreach (string diaPart in diaItems)
			{
				if (diaPart == PartEditing.Value)
				{
					var itemValuesDia = GetMeasuresForEditFromView(diaPart);
					var diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "11");//max
					if (diaValue != null)
					{
						var newDiaValue = MaxTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "12");//min
					if (diaValue != null)
					{
						var newDiaValue = MinTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "13");//h_x
					if (diaValue != null)
					{
						var newDiaValue = H_xTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "14");//total depth
					if (diaValue != null)
					{
						var newDiaValue = TotaDepthTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "15");//tab_d
					if (diaValue != null)
					{
						var newDiaValue = Tab_DTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "5");//calc weight
					if (diaValue != null)
					{
						var newDiaValue = DiaCalcWeightTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "4");//measured weight
					if (diaValue != null)
					{
						var newDiaValue = DiaMeasWeight.Text;
						diaValue.Value = newDiaValue;
					}
					break;
				}
			}

			foreach (string diaPart in colStoneItems)
			{
				if (diaPart == PartEditing.Value)
				{
					var itemValuesDia = GetMeasuresForEditFromView(diaPart);
					var diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "11");//max
					if (diaValue != null)
					{
						var newDiaValue = MaxTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "12");//min
					if (diaValue != null)
					{
						var newDiaValue = MinTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "13");//h_x
					if (diaValue != null)
					{
						var newDiaValue = H_xTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "14");//total depth
					if (diaValue != null)
					{
						var newDiaValue = TotaDepthTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "15");//tab_d
					if (diaValue != null)
					{
						var newDiaValue = Tab_DTxt.Text;
						diaValue.Value = newDiaValue;
					}
					diaValue = itemValues.Find(m => m.PartId == diaPart && "" + m.MeasureId == "5");//tab_d
					if (diaValue != null)
					{
						var newDiaValue = DiaCalcWeightTxt.Text;
						diaValue.Value = newDiaValue;
					}
					break;
				}
			}

			foreach (string ssPart in colStoneSetItems)
			{
				if (ssPart == PartEditing.Value)
				{
					var ssValue = itemValues.Find(m => m.PartId == ssPart && "" + m.MeasureId == "2");//total weight
					if (ssValue != null)
					{
						var newDiaValue = CSSTotalWeightTxt.Text;
						ssValue.Value = newDiaValue;
					}
					ssValue = itemValues.Find(m => m.PartId == ssPart && "" + m.MeasureId == "65");//# of units
					if (ssValue != null)
					{
						var newDiaValue = CSSNumberOfUnitsTxt.Text;
						ssValue.Value = newDiaValue;
					}
				}
			}
			//foreach (RepeaterItem element in ElementPanel.Items)
			//         {
			//             var eleValue = element.FindControl("ElementMeasure") as HiddenField;
			//             var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
			//             if (itemValue != null)
			//             {
			//                 var eleList = element.FindControl("DropDownListBox") as ListBox;
			//                 var newValue = eleList.SelectedValue;
			//                 itemValue.Value = newValue;
			//             }
			//         }
			//         foreach (RepeaterItem element in RptMeasureList.Items)
			//         {
			//             var eleValue = element.FindControl("MeasureID") as HiddenField;
			//             var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
			//             if (itemValue != null)
			//             {
			//                 var eleList = element.FindControl("RepeatTxt") as TextBox;
			//                 var newValue = eleList.Text;
			//                 itemValue.Value = newValue;
			//             }
			//         }
			foreach (string metalPart in metalItems)
			{
				if (metalPart == PartEditing.Value)
				{
					var itemValuesMetal = GetMeasuresForEditFromView(metalPart);
					var metalValue = itemValues.Find(m => m.PartId == metalPart && "" + m.MeasureId == "44");
					if (metalValue != null)
					{
						var newMetalValue = MetalColorList.SelectedValue;
						metalValue.Value = newMetalValue;
					}
					metalValue = itemValues.Find(m => m.PartId == metalPart && "" + m.MeasureId == "45");
					if (metalValue != null)
					{
						var newMetalValue = MetalStampList.SelectedValue;
						metalValue.Value = newMetalValue;
					}
					metalValue = itemValues.Find(m => m.PartId == metalPart && "" + m.MeasureId == "259");
					if (metalValue != null)
					{
						var newMetalValue = MetalKaratageList.SelectedValue;
						metalValue.Value = newMetalValue;
					}
					metalValue = itemValues.Find(m => m.PartId == metalPart && "" + m.MeasureId == "255");
					if (metalValue != null)
					{
						var newMetalValue = ChainLenghtTxt.Text;
						metalValue.Value = newMetalValue;
					}
					metalValue = itemValues.Find(m => m.PartId == metalPart && "" + m.MeasureId == "210");
					if (metalValue != null)
					{
						var newMetalValue = EngravingTxt.Text;
						metalValue.Value = newMetalValue;
					}
					break;
				}
			}
			foreach (string mountPart in mountItems)
			{
				if (mountPart == PartEditing.Value)
				{
					var itemValuesMount = GetMeasuresForEditFromView(mountPart);
					var mountValue = itemValues.Find(m => m.PartId == mountPart && "" + m.MeasureId == "44");
					if (mountValue != null)
					{
						var newMountValue = MountMetalList.SelectedValue;
						mountValue.Value = newMountValue;
					}
					mountValue = itemValues.Find(m => m.PartId == mountPart && "" + m.MeasureId == "45");
					if (mountValue != null)
					{
						var newMountValue = MountStampList.SelectedValue;
						mountValue.Value = newMountValue;
					}
					mountValue = itemValues.Find(m => m.PartId == mountPart && "" + m.MeasureId == "46");
					if (mountValue != null)
					{
						var newMountValue = MountTypeList.SelectedValue;
						mountValue.Value = newMountValue;
					}
					mountValue = itemValues.Find(m => m.PartId == mountPart && "" + m.MeasureId == "259");
					if (mountValue != null)
					{
						var newMountValue = MountKaratageList.SelectedValue;
						mountValue.Value = newMountValue;
					}
					break;
				}
			}
			/* Old code for GetNewValuesFromGrid
			//Shapes
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "8");
			if (itemValue != null)
			{
				var newValue = ShapesList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Polish
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "18");
			if(itemValue != null)
			{
				var newValue = PolishList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Symmetry
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "19");
			if (itemValue != null)
			{
				var newValue = SymmetryList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Clarity
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "29");
			if(itemValue != null)
			{
				var newValue = ClarityList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Inclusion
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "198");
			if(itemValue != null)
			{
				var newValue = InclusionList.SelectedValue;
				itemValue.Value = newValue;
			}
			*/
		}
		#endregion

		#region Get Data From View

		//-- Measure Values
		private List<MeasureValueModel> GetMeasureValuesFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
		}

		//-- Measure Enums
		private List<EnumMeasureModel> GetMeasureEnumsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
		}

		//-- Loading Measures Description
		private List<MeasureValueCpModel> GetMeasuresCpFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
		}

		//-- Loading Item Numbers
		private List<SingleItemModel> GetItemNumbersFromView()
		{

			return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
		}
		private SingleItemModel GetItemModelFromView(string itemNumber)
		{
			return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber || m.FullOldItemNumber == itemNumber || m.FullBatchNumber == itemNumber);
		}
		private SingleItemModel GetSelectedItemModel()
		{
			var itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			if (string.IsNullOrEmpty(itemNumber)) return null;
			return GetItemModelFromView(itemNumber);
		}
		private List<MeasurePartModel> GetPartsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
		}
		private List<ItemValueEditModel> GetMeasuresForEditFromView(string partId)
		{
			var data = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ?? new List<ItemValueEditModel>();
			if (string.IsNullOrEmpty(partId)) return data;
			else return data.FindAll(m => m.PartId == partId);
		}
		#endregion

		#region Information Dialog
		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}

		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{
			if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
			{
				lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
				OnItemListSelectedChanged(null, null);
			}

		}

		#endregion

		#region Question Dialog
		static string ModeOnItemChanges = "item";
		static string ModeOnLoadClick = "load";
		static string ModeOnShortReportClick = "report";
		private void PopupSaveQDialog(string src)
		{
			var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
			SaveDlgMode.Value = src;
			QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
			SaveQPopupExtender.Show();
		}
		protected void OnQuesSaveYesClick(object sender, EventArgs e)
		{
			var errMsg = SaveExecute();
			if (!string.IsNullOrEmpty(errMsg))
			{
				// Return on prev item
				txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
				lstItemList.SelectedValue = ItemEditing.Value;
				InvalidLabel.Text = errMsg;
				//PopupInfoDialog(errMsg, true);
				return;
			}

			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				LoadExecute();
			}
			if (SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
		}
		protected void OnQuesSaveNoClick(object sender, EventArgs e)
		{
			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				LoadExecute();
			}
			if (SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
		}
		protected void OnQuesSaveCancelClick(object sender, EventArgs e)
		{
			lstItemList.SelectedValue = ItemEditing.Value;
			txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
		}
		#endregion

		//protected void OnMeasureChanged(object sender, EventArgs e)
		//{

		//}

		protected void OnMetalStampChanged(object sender, EventArgs e)
		{

		}

		protected void OnMetalColorChanged(object sender, EventArgs e)
		{

		}

		protected void OnCalclick(object sender, EventArgs e)
		{
			string maxvalue = "", minvalue = "", hxvalue = "";
			int tab_d = 0;
			if (DiaShapeTxt.Text == "")
            {
				InvalidLabel.Text = "Missing shape";
				return;
            }
			if (MaxTxt.Text != "" && MinTxt.Text != "" && H_xTxt.Text != "")
			{
				decimal max = Decimal.Parse(MaxTxt.Text);
				decimal min = Decimal.Parse(MinTxt.Text);
				decimal h_x = Decimal.Parse(H_xTxt.Text);
				string shape = DiaShapeTxt.Text;
				string variety = "";
				if (PartTree.SelectedNode.Text.ToLower().Contains("color stone"))
				{
					if (CSVarietyTxt.Text == "")
					{
						InvalidLabel.Text = "Missing Variety";
						return;
					}
					variety = CSVarietyTxt.Text;
					CalculateWeightCS(max, min, h_x, shape.ToLower(), variety);
				}
				else
				{
					if (TableTxt.Text == "0")
						InvalidLabel.Text = "Missing data";
					else
						CalculateWeight(max, min, h_x, shape.ToLower());
				}
			}
			else
				InvalidLabel.Text = "Missing data";
			//if (tab_d != 0 && totalDepth != 0)
			//{
			//foreach (RepeaterItem element in RptMeasureList.Items)
			//{
			//	var itemValues = GetMeasuresForEditFromView("");
			//	var eleValue = element.FindControl("MeasureID") as HiddenField;
			//	var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
			//	if (eleValue.Value == "14")//totaldepth
			//	{
			//		if (itemValue != null)
			//		{
			//			var eleList = element.FindControl("RepeatTxt") as TextBox;
			//			var newValue = eleList.Text;
			//			itemValue.Value = totalDepth.ToString();
			//			maxvalue = newValue;
			//		}
			//	}
			//	else if (eleValue.Value == "15")//tab_D
			//	{
			//		if (itemValue != null)
			//		{
			//			var eleList = element.FindControl("RepeatTxt") as TextBox;
			//			var newValue = eleList.Text;
			//			itemValue.Value = tab_d.ToString();
			//			minvalue = newValue;
			//		}
			//	}
			//}
			//	foreach (RepeaterItem element in RptMeasureList.Items)
			//{
			//	var itemValues = GetMeasuresForEditFromView("");
			//	var eleValue = element.FindControl("MeasureID") as HiddenField;
			//	var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
			//	if (eleValue.Value == "11")//max
			//	{
			//		if (itemValue != null)
			//		{
			//			var eleList = element.FindControl("RepeatTxt") as TextBox;
			//			var newValue = eleList.Text;
			//			itemValue.Value = newValue;
			//			maxvalue = newValue;
			//		}
			//	}
			//	else if (eleValue.Value == "12")//min
			//	{
			//		if (itemValue != null)
			//		{
			//			var eleList = element.FindControl("RepeatTxt") as TextBox;
			//			var newValue = eleList.Text;
			//			itemValue.Value = newValue;
			//			minvalue = newValue;
			//		}
			//	}
			//	else if (eleValue.Value == "13")//h_x
			//	{
			//		if (itemValue != null)
			//		{
			//			var eleList = element.FindControl("RepeatTxt") as TextBox;
			//			var newValue = eleList.Text;
			//			itemValue.Value = newValue;
			//			hxvalue = newValue;
			//		}
			//	}
			//}

			//}
		}//onCalcClick

		protected void CalculateWeight(decimal max, decimal min, decimal h_x, string shape)
		{
			var dsShape = new DataSet();
			//dsShape.ReadXml(Server.MapPath("Measure2ShapeTypes.xml"));
			dsShape = QueryUtils.GetMeasure2ShapeList(this);
			string select = @"Shape = '" + shape + @"'";
			DataRow[] dr = dsShape.Tables[0].Select(select);
			if (dr.Length == 0)//shape not found
			{
				InvalidLabel.Text = "Missing shape " + shape;
				AddShapeParms.Visible = true;
				NewShapeTxt.Text = shape;
				NewShapeTypeList.DataSource = null;
				NewShapeTypeList.DataBind();
				DataTable newShapeDt = new DataTable();
				DataColumn newShapeDc = new DataColumn("ShapeID", typeof(int));
				newShapeDt.Columns.Add(newShapeDc);
				newShapeDc = new DataColumn("ShapeType", typeof(string));
				newShapeDt.Columns.Add(newShapeDc);
				DataRow newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 1;
				newShapeDr["ShapeType"] = "01";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 2;
				newShapeDr["ShapeType"] = "ec";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 3;
				newShapeDr["ShapeType"] = "cushion";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 4;
				newShapeDr["ShapeType"] = "heart";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 5;
				newShapeDr["ShapeType"] = "oe";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 6;
				newShapeDr["ShapeType"] = "mq";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 7;
				newShapeDr["ShapeType"] = "om";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 8;
				newShapeDr["ShapeType"] = "ov";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 9;
				newShapeDr["ShapeType"] = "pear";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 10;
				newShapeDr["ShapeType"] = "pr";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 11;
				newShapeDr["ShapeType"] = "ps";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 12;
				newShapeDr["ShapeType"] = "rect";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 13;
				newShapeDr["ShapeType"] = "round";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 14;
				newShapeDr["ShapeType"] = "triangular";
				newShapeDt.Rows.Add(newShapeDr);
				NewShapeTypeList.DataSource = newShapeDt;
				NewShapeTypeList.DataBind();


				return;
			}
			else
			{
				AddShapeParms.Visible = false;
			}
			string type = dr[0]["Type"].ToString();
			double L = (double)max;
			double W = (double)min;
			double D = (double)h_x;
			double WCF = Convert.ToDouble(dr[0]["WCF"].ToString());
			double LWRF = L / W;
			double result = 0;
			decimal table = Decimal.Parse(TableTxt.Text);
			if (type == "round")//round
			{
				decimal totalDepth = h_x / ((max + min) / 2) * 100;
				TotaDepthTxt.Text = totalDepth.ToString("0.##");
				int tab_d = Convert.ToInt16(table / ((min + max) / 2) * 100);
				Tab_DTxt.Text = tab_d.ToString();
				WCF = 0.0061;
				result = ((L + W) / 2) * ((L + W) / 2) * D * WCF;
			}
			else
			{
				decimal totalDepth = h_x / min * 100;
				TotaDepthTxt.Text = totalDepth.ToString("0.##");
				int tab_d = Convert.ToInt16(table / min * 100);
				Tab_DTxt.Text = tab_d.ToString();
				DataSet dsShapeWCF = new DataSet();
				dsShapeWCF.ReadXml(Server.MapPath("Measure2ShapeAdjustment.xml"));
				select = @"Type = '" + type + @"'";
				DataRow[] drWCF = dsShapeWCF.Tables[0].Select(select);
				if (drWCF.Length == 0)
				{
					InvalidLabel.Text = "No entries for adjustment for " + shape;
				}
				else
				{
					double ratio = (double)max / (double)min;
					foreach (DataRow shRow in drWCF)
					{
						double minRatio = Convert.ToDouble(shRow["min"].ToString());
						double maxRatio = Convert.ToDouble(shRow["max"].ToString());
						if (ratio <= maxRatio && ratio >= minRatio)
						{
							WCF = Convert.ToDouble(shRow["WCF"].ToString());
							break;
						}
					}

				}
				result = L * W * D * WCF;
			}
			DiaCalcWeightTxt.Text = result.ToString("0.##");
		}//CalculateWeight

		protected void CalculateWeightCS(decimal max, decimal min, decimal h_x, string shape, string stoneName)
		{
			var dsShape = new DataSet();
			//dsShape.ReadXml(Server.MapPath("Measure2ShapeTypes.xml"));
			if (CSVarietyTxt.Text == "")
            {
				InvalidLabel.Text = "Missing variety";
				return;
            }
			dsShape = QueryUtils.GetMeasure2ShapeList(this);
			string select = @"Shape = '" + shape + @"'";
			DataRow[] dr = dsShape.Tables[0].Select(select);
			if (dr.Length == 0)//shape not found
			{
				InvalidLabel.Text = "Missing shape " + shape;
				AddShapeParms.Visible = true;
				NewShapeTxt.Text = shape;
				NewShapeTypeList.DataSource = null;
				NewShapeTypeList.DataBind();
				DataTable newShapeDt = new DataTable();
				DataColumn newShapeDc = new DataColumn("ShapeID", typeof(int));
				newShapeDt.Columns.Add(newShapeDc);
				newShapeDc = new DataColumn("ShapeType", typeof(string));
				newShapeDt.Columns.Add(newShapeDc);
				DataRow newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 1;
				newShapeDr["ShapeType"] = "01";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 2;
				newShapeDr["ShapeType"] = "ec";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 3;
				newShapeDr["ShapeType"] = "cushion";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 4;
				newShapeDr["ShapeType"] = "heart";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 5;
				newShapeDr["ShapeType"] = "oe";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 6;
				newShapeDr["ShapeType"] = "mq";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 7;
				newShapeDr["ShapeType"] = "om";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 8;
				newShapeDr["ShapeType"] = "ov";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 9;
				newShapeDr["ShapeType"] = "pear";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 10;
				newShapeDr["ShapeType"] = "pr";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 11;
				newShapeDr["ShapeType"] = "ps";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 12;
				newShapeDr["ShapeType"] = "rect";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 13;
				newShapeDr["ShapeType"] = "round";
				newShapeDt.Rows.Add(newShapeDr);
				newShapeDr = newShapeDt.NewRow();
				newShapeDr["ShapeID"] = 14;
				newShapeDr["ShapeType"] = "triangular";
				newShapeDt.Rows.Add(newShapeDr);
				NewShapeTypeList.DataSource = newShapeDt;
				NewShapeTypeList.DataBind();


				return;
			}
			else
			{
				AddShapeParms.Visible = false;
			}
			string type = dr[0]["Type"].ToString();
			string shapeCS = dr[0]["Shape"].ToString();
			double L = (double)max;
			double W = (double)min;
			double D = (double)h_x;
			double WCF = Convert.ToDouble(dr[0]["WCF"].ToString());
			double LWRF = L / W;
			double result = 0, csAdjustment = 0;
			decimal table = 0;
			if (TableTxt.Text != "")
			table = Decimal.Parse(TableTxt.Text);
			if (shapeCS == "round")//round
			{
				decimal totalDepth = h_x / ((max + min) / 2) * 100;
				TotaDepthTxt.Text = totalDepth.ToString("0.##");
				int tab_d = 0;
				if (table != 0)
				{
					tab_d = Convert.ToInt16(table / ((min + max) / 2) * 100);
					Tab_DTxt.Text = tab_d.ToString();
				}
				else
					Tab_DTxt.Text = "0";
				WCF = 0.0061;
				result = ((L + W) / 2) * ((L + W) / 2) * D * WCF;
			}
			else
			{
				decimal totalDepth = h_x / min * 100;
				TotaDepthTxt.Text = totalDepth.ToString("0.##");
				int tab_d = 0;
				if (table != 0)
				{
					tab_d = Convert.ToInt16(table / min * 100);
					Tab_DTxt.Text = tab_d.ToString();
				}
				else
					Tab_DTxt.Text = "0";
				DataSet dsShapeWCF = new DataSet();
				dsShapeWCF.ReadXml(Server.MapPath("Measure2ShapeAdjustment.xml"));
				select = @"Type = '" + type + @"'";
				DataRow[] drWCF = dsShapeWCF.Tables[0].Select(select);
				if (drWCF.Length == 0)
				{
					InvalidLabel.Text = "No entries for adjustment for " + shape;
				}
				else
				{
					double ratio = (double)max / (double)min;
					foreach (DataRow shRow in drWCF)
					{
						double minRatio = Convert.ToDouble(shRow["min"].ToString());
						double maxRatio = Convert.ToDouble(shRow["max"].ToString());
						if (ratio <= maxRatio && ratio >= minRatio)
						{
							WCF = Convert.ToDouble(shRow["WCF"].ToString());
							break;
						}
					}

				}
				result = L * W * D * WCF;
			}
			DataTable dtCS = QueryUtils.GetMeasureColorStone(CSVarietyTxt.Text, this);
			if (dtCS != null && dtCS.Rows.Count > 0)
            {
				csAdjustment = Convert.ToDouble(dtCS.Rows[0]["WA"].ToString());
				result *= csAdjustment;
            }
			else
            {
				InvalidLabel.Text = "No stone name found for " + CSVarietyTxt.Text;
            }
			DiaCalcWeightTxt.Text = result.ToString("0.##");
		}//CalculateWeightCS
		 //protected void CalculateWeight(decimal max, decimal min, decimal h_x, string shape)
		 //{
		 //	var dsShape = new DataSet();
		 //	dsShape.ReadXml(Server.MapPath("Measure2ShapeTypes.xml"));
		 //	string shape = "Marquise";
		 //	string select = @"Shape = '" + shape + @"'";
		 //	DataRow[] dr = dsShape.Tables[0].Select(select);
		 //	double L = (double)max;
		 //	double W = (double)min;
		 //	double D = (double)h_x;
		 //	double WCF = 0;
		 //	double LWRF = L / W;
		 //	double result = 0;

		//	if (shape.ToLower().Contains("round brill"))
		//	{
		//		WCF = 0.0061;
		//		result = ((L + W) / 2) * ((L + W) / 2) * D * WCF;
		//	}
		//	else
		//	{
		//		if (shape.ToLower().Contains("oval brill"))
		//		{
		//			WCF = 0.0068;
		//			LWRF = L / W;
		//		}
		//		else if (shape.ToLower().Contains("pear brill"))
		//		{
		//			WCF = 0.0058;
		//			LWRF = 1;//L / W;
		//		}
		//		else if (shape.ToLower().Contains("marquise brill") || shape.ToLower().Contains("mq bezel"))
		//		{
		//			WCF = 0.0058;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("heart brill"))
		//		{
		//			WCF = 0.0062;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("emerald step"))
		//		{
		//			WCF = 0.0061;
		//			LWRF = 1;// L / W;
		//		}
		//		else if (shape.ToLower().Contains("princess rect"))
		//		{
		//			WCF = 0.0061;
		//			LWRF = 1;// L / W * 1.05;
		//		}
		//		else if (shape.ToLower().Contains("cc rect. mod"))
		//		{
		//			WCF = 0.0061;
		//			LWRF = 1;// L / W * 1.04;
		//		}
		//		else if (shape.ToLower().Contains("cushion"))
		//		{
		//			WCF = 0.0061;
		//			LWRF = L / W * 1.04;
		//		}
		//		else if (shape.ToLower().Contains("princess regular"))
		//		{
		//			WCF = 0.0082;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("sq. emerald"))
		//		{
		//			WCF = 0.008;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("square cushion"))
		//		{
		//			WCF = 0.00815;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("triangle brilliant"))
		//		{
		//			WCF = 0.0057;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("old european"))
		//		{
		//			WCF = 0.0063;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("old mine"))
		//		{
		//			WCF = 0.0066;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("round rose"))
		//		{
		//			WCF = 0.0043;
		//			LWRF = 1;
		//		}
		//		else if (shape.ToLower().Contains("oval rose"))
		//		{
		//			WCF = 0.00815;
		//			LWRF = 1;
		//		}
		//		result = L * W * D * LWRF * WCF;
		//	}
		//	DiaCalcWeightTxt.Text = result.ToString("0.##");
		//}

		protected void OnMetalKaratageChanged(object sender, EventArgs e)
		{

		}

		protected void OnAddNewShapeBtn_Click(object sender, EventArgs e)
		{
			if (NewShapeTxt.Text == "" || NewWCFTxt.Text == "" || NewShapeTypeList.SelectedIndex < 0)
			{
				InvalidLabel.Text = "Missing data to add the shape";
				return;
			}
			bool added = QueryUtils.InsertMeasure2ShapeType(NewShapeTxt.Text, NewWCFTxt.Text, NewShapeTypeList.SelectedItem.Text, this);
			if (!added)
				InvalidLabel.Text = "Error Adding shape " + NewShapeTxt.Text;
			else
			{
				InvalidLabel.Text = NewShapeTxt.Text + " shape was added";
				AddShapeParms.Visible = false;
			}
		}

		protected void OnShapeTypeListChanged(object sender, EventArgs e)
		{

		}

		protected void ICTotalWeightChanged(object sender, EventArgs e)
		{
			ResetTotalWeightBtn.Visible = true;
			cmdSave.Enabled = false;
		}

		protected void OnResetTotalWeightClick(object sender, EventArgs e)
		{
			ResetField.Value = "true";
			OnSaveClick(null, null);
			ResetField.Value = "false";
			ResetTotalWeightBtn.Visible = false;
			cmdSave.Enabled = true;
		}

		protected void WDSSSingleWeightTxt_TextChanged(object sender, EventArgs e)
		{
			InvalidLabel.Text = "";
			if (WDSSSingleWeightTxt.Text == "")
				return;
			if (WDSSSingleWeightTxt.Text.Any(x => char.IsLetter(x)))
			{
				InvalidLabel.Text = "Enter number for Single Weight";
				return;
			}
			if (WDSSNumberOfUnitsTxt.Text == "")
			{
				InvalidLabel.Text = "Enter number of units";
				return;
			}
			double totalWeight = 0;
			double singleWeight = Convert.ToDouble(WDSSSingleWeightTxt.Text);
			double nOfUnits = Convert.ToDouble(WDSSNumberOfUnitsTxt.Text);
			totalWeight = singleWeight * nOfUnits;
			WDSSTotalWeightTxt.Text = totalWeight.ToString("0.##");
		}
		protected void CSSingleWeightTxt_TextChanged(object sender, EventArgs e)
		{
			InvalidLabel.Text = "";
			if (CSSingleWeightTxt.Text == "")
				return;
			if (CSSingleWeightTxt.Text.Any(x => char.IsLetter(x)))
			{
				InvalidLabel.Text = "Enter number for Single Weight";
				return;
			}
			if (CSSNumberOfUnitsTxt.Text == "")
			{
				InvalidLabel.Text = "Enter number of units";
				return;
			}
			double totalWeight = 0;
			double singleWeight = Convert.ToDouble(CSSingleWeightTxt.Text);
			double nOfUnits = Convert.ToDouble(CSSNumberOfUnitsTxt.Text);
			totalWeight = singleWeight * nOfUnits;
			CSSTotalWeightTxt.Text = totalWeight.ToString("0.##");
		}

		protected void WDSSItemWeight_TextChanged(object sender, EventArgs e)
		{

		}

		protected void OnAddWSSItemsWeightClick(object sender, EventArgs e)
		{
			if (WDSSTotalWeightTxt.Text.Any(x => char.IsLetter(x)) || WDSSNumberOfUnitsTxt.Text.Any(x => char.IsLetter(x))
				|| WDSSNumberOfItems.Text.Any(x => char.IsLetter(x)) || WSSWeightPerItem.Text.Any(x => char.IsLetter(x)))
				return;
			double totalUnits = 0, itemsNumber = 0;
			double totalWeight = 0, weightPerItem = 0, itemsWeight = 0;
			if (WDSSNumberOfUnitsTxt.Text != "")
				totalUnits = Convert.ToDouble(WDSSNumberOfUnitsTxt.Text);
			if (WDSSNumberOfItems.Text != "")
				itemsNumber = Convert.ToDouble(WDSSNumberOfItems.Text);
			WDSSNumberOfUnitsTxt.Text = (totalUnits + itemsNumber).ToString();
			if (WDSSTotalWeightTxt.Text != "")
				totalWeight = Convert.ToDouble(WDSSTotalWeightTxt.Text);
			if (WDSSNumberOfItems.Text != "" && WSSWeightPerItem.Text != "")
            {
				weightPerItem = Convert.ToDouble(WSSWeightPerItem.Text);
				itemsNumber = Convert.ToDouble(WDSSNumberOfItems.Text);
				totalUnits += itemsNumber;
				WDSSNumberOfUnitsTxt.Text = Convert.ToInt32(totalUnits).ToString();
				itemsWeight = weightPerItem * itemsNumber;
				totalWeight += itemsWeight;
				WDSSTotalWeightTxt.Text = totalWeight.ToString();
            }
        }

        protected void OnMountMetalChanged(object sender, EventArgs e)
        {

        }

        protected void OnMountTypeChanged(object sender, EventArgs e)
        {

        }

        protected void OnMountKaratageChanged(object sender, EventArgs e)
        {

        }

        protected void OnMountStampChanged(object sender, EventArgs e)
        {

        }
		protected void OnReqFractionGroupClick(object sender, EventArgs e)
		{
			ReqFRangesGroupList.Visible = true;
			//FullRangeFld.Value = "";
			var dt = new DataTable();
			dt = QueryCpUtilsNew.GetRangeGroupList(this);
			if (dt != null && dt.Rows.Count != 0)
			{
				List<FractionRangesGroupModel> rangeGroups = (from DataRow row in dt.Rows select new FractionRangesGroupModel(row)).ToList();
				//foreach (FractionRangesGroupModel group in rangeGroups)
				//{
				//    ReqFRangesGroupList.Items.Add(group.GroupName + "(" + group.GroupId + ")");
				//}
				ReqFRangesGroupList.DataSource = rangeGroups;
				ReqFRangesGroupList.DataBind();
				ReqFRangesGroupList.SelectedIndex = 0;
				ReqFRangesGroupList.SelectedIndex = -1;
				ReqFRangesGroupList.Items.Insert(0, new ListItem("Please Select Range Group", ""));

			}
		}
		protected void OnReqRangeGroupClick(object sender, EventArgs e)
		{
			ReqFRangeList.Visible = true;
			ReqFDisplayBtn.Visible = true;
			var rangeGroup = ReqFRangesGroupList.SelectedItem.Text;

			DataTable dt = QueryCpUtilsNew.GetFractionRangeList(rangeGroup, this);
			List<string> dataList = new List<string>();
			if (dt != null && dt.Rows.Count > 0)
			{
				ReqFRangeList.Items.Add("Fraction  Range Name");
				foreach (DataRow dr in dt.Rows)
				{
					ReqFRangeList.Items.Add(dr["Fraction"].ToString() + "   " + dr["RangeName"].ToString());
				}
			}
		}
		protected void OnCloseReqRangeGroupClick(object sender, EventArgs e)
		{
			ReqFRangeList.Items.Clear();
			ReqFRangeList.Visible = false;
			ReqFRangesBtn.Visible = false;
		}
	}
}