﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="LotNumber.aspx.cs" Inherits="Corpt.LotNumber" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .table tr:hover
        {
            background-color: #b8d1f3;
        }
    </style>
    <div class="demoarea">
        <div class="demoheading">Find by Lot Number</div>
        <!-- Filter Panel -->
        <table>
            <tr>
                <td>
                    <asp:Panel runat="server" DefaultButton="ActivateButton" CssClass="form-inline" Visible="false">
                        <asp:TextBox id="txtLotNumber" maxlength="100" placeholder="Lot Number" runat="server"
                            style="font-weight: bold; width: 120px;" />
                        <label class="checkbox" style="margin-left: 20px">
                            <asp:CheckBox ID="chkStrict" type="checkbox" runat="server" />
                            Strict Search</label>
                        <!-- Lookup button -->
                        <asp:Button ID="ActivateButton" Style="margin-left: 20px" runat="server" CssClass="btn btn-primary"
                            Text="Lookup" OnClick="ActivateButtonClick"></asp:Button>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" CssClass="form-inline">
                        <asp:Label Text="ENTER GSI ORDER" runat="server"></asp:Label>
                        <asp:TextBox id="shipOrderBox" maxlength="100" placeholder="Shipping Number" runat="server"
                            style="font-weight: bold; width: 120px;" />
                        <!-- Lookup button -->
                        <asp:Button ID="ShipBtn" Style="margin-left: 20px" runat="server" CssClass="btn btn-primary"
                            Text="Lookup" OnClick="ShipButtonClick"></asp:Button>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        
        <!-- Error message-->
        <div class="control-group error">
            <asp:Label class="control-label" id="InfoLabel" runat="server" />
        </div>

        <asp:RequiredFieldValidator runat="server" ID="VReq" ControlToValidate="txtLotNumber"
            Display="None" 
            ErrorMessage="<b>Required Field Missing</b><br />A Lot Number is required." 
            ValidationGroup="LotGroup" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="VReqE" TargetControlID="VReq"
            PopupPosition="BottomRight" HighlightCssClass="validatorCalloutHighlight" />
        <!-- Item Numbers DataGrid-->
        <div style="font-size: small; margin-top: 20px;">
            <asp:DataGrid ID="tblLotNumberResult" runat="server" CellPadding="5">
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
            </asp:DataGrid>
        </div>
        <div class="row" align="right">
            <asp:HyperLink ID="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx">Back</asp:HyperLink>
        </div>
    </div>
</asp:Content>
