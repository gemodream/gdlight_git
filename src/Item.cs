using System;
using System.Collections;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class Item
	{
		public Item()
		{
			// 
			// TODO: Add constructor logic here
			//
			Items = new ArrayList();
		}
		public ArrayList Items;
		private SortedList StringValues;
		private SortedList EnumValues;
		private SortedList IntValues;

		private int orderCode;
		private int batchCode;
		private int itemCode;

		private int officeID;

		private int batchID;
		private int partID;

		private int itemTypeID;
		private String partName;
		private String partTypeName;


		
		/// <summary>
		/// Adds New Item as SubItem
		/// </summary>
		/// <param name="item">SubItem to add.</param>
		public void AddItem(Item item)
		{
			if(item!=null)
				Items.Add(item);
		}

		public void AddMeasure(String MeasureName, String MeasureValue)
		{
			StringValues.Add(MeasureName,MeasureValue);
		}

		public void AddMeasure(String MeasureName, int MeasureValue)
		{
			IntValues.Add(MeasureName,MeasureValue);
		}

		public void AddMeasure(String MeasureName, EnumMeasure MeasureValue)
		{
			EnumValues.Add(MeasureName, MeasureValue);
		}

		public int BatchCode
		{
			get
			{
				return batchCode;
			}
			set
			{
				batchCode = value;
			}
		}

		public int ItemCode
		{
			get
			{
				return itemCode;
			}
			set
			{
				itemCode = value;
			}
		}

		public int OfficeID
		{
			get
			{
				return officeID;
			}
			set
			{
				officeID = value;
			}
		}

		public int OrderCode
		{
			get
			{
				return orderCode;
			}
			set
			{
				orderCode = value;
			}
		}

		public int BatchID
		{
			get
			{
				return batchID;
			}
			set
			{
				batchID = value;
			}
		}

		public int ItemTypeID
		{
			get
			{
				return itemTypeID;
			}
			set
			{
				itemTypeID = value;
			}
		}

		public int PartID
		{
			get
			{
				return partID;
			}
			set
			{
				partID=value;
			}
		}

		public String PartName
		{
			get
			{
				return partName;
			}
			set
			{
				partName = value;
			}
		}

		public String PartTypeName
		{
			get
			{
				return partTypeName;
			}
			set
			{
				partTypeName = value;
			}
		}

		public override string ToString()
		{
			return partTypeName;
		}

	}

	/// <summary>
	/// This is a measure;
	/// </summary>
	public class EnumMeasure
	{
		public EnumMeasure()
		{}
		public String MeasureValue;
		public int MeasureCode;
		public int MeasureID;
		public int MeasureValueMEasureID;
	}
}
