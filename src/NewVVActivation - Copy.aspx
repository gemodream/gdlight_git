﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="NewVVActivation.aspx.cs" Inherits="Corpt.NewVvActivation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: #555555;
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel {
	        padding-bottom:2px;
	        color:#5377A9;
	        font-family:Arial, Sans-Serif;
	        font-weight:bold;
	        font-size:1.0em;
        }
    </style>
    <div class="demoarea">
        <div class="demoheading">New User Activation</div>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <Triggers>
<%--                <asp:PostBackTrigger ControlID="CheckButton" />--%>
<%--                <asp:PostBackTrigger ControlID="SearchByEmalButton" />--%>
<%--                <asp:PostBackTrigger ControlID="AddButton" />--%>
<%--
                <asp:AsyncPostBackTrigger ControlID="CheckButton" EventName="Click" />
--%>
                
            </Triggers>
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:Panel runat="server" ID="SearchPanel" DefaultButton="CheckButton">
                                <table>
                                    <tr>
                                        <td>
                                            Report Number:
                                            <asp:CustomValidator ID="ReportNumberValidator" runat="server" ControlToValidate="ReportNumberFld"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpSearch" ToolTip="Ten or eleven numeric characters"
                                                onservervalidate="OnReportNumberValidate"></asp:CustomValidator>
                                        </td>
                                        <td ><asp:TextBox runat="server" ID="ReportNumberFld" Width="150px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            VirtualVault™ Number:
                                            <asp:RequiredFieldValidator ID="VirtualVaultValidator" Text="*"  runat="server" ValidationGroup="ValGrpSearch" ErrorMessage="" 
                                                                        ToolTip="Required field" ControlToValidate="VirtualNumberFld"/>
                                            
                                        </td>
                                        <td><asp:TextBox runat="server" ID="VirtualNumberFld" Width="150px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><asp:Label runat="server" ID="ResultCheckLabel" ForeColor="DarkRed"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <a href="CustomerHistory.aspx" target="_blank" id="DocumentLink" runat="server" 
                                                title="Document" name="Document"></a>
<%--                                            <asp:HyperLink ID="DocumentLink" runat="server"></asp:HyperLink>--%>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="SearhBtnsPanel">
                                <asp:Button runat="server" ID="CheckButton" Text="Check Report" CssClass="btn btn-info"
                                    OnClick="OnCheckButtonClick" />
                                <asp:Button runat="server" ID="NextCheckButton" Text="Next Report" CssClass="btn btn-info"
                                    OnClick="OnNextCheckButtonClick" Visible="False" />
                            </asp:Panel>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:Panel runat="server" ID="UserPanel" DefaultButton="HiddenBtn" Visible="False">
                                <table>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            E-mail address
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="PersonEmail"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="E-mail address is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="3">
                                            <asp:Panel ID="MailPanel" DefaultButton="SearchByEmalButton" runat="server">
                                                <asp:TextBox runat="server" ID="PersonEmail" MaxLength="100" Width="70%" ClientIDMode="Static"></asp:TextBox>
                                                <asp:ImageButton ID="SearchByEmalButton" runat="server" ToolTip="Search user by email"
                                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnSearchByEmailClick" />
                                                <asp:ImageButton ID="EmailButton" runat="server" ToolTip="Send email"
                                                    ImageUrl="~/Images/ajaxImages/email_go.png" OnClick="OnSendEmailClick" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            First Name
                                            <asp:RequiredFieldValidator ID="ValFirstName" runat="server" ControlToValidate="PersonFirstName"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="First Name is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="PersonFirstName" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td style="padding-left: 10px">
                                            M.I.
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="PersonMi" MaxLength="1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            Last Name
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="PersonLastName"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Last Name is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td >
                                            <asp:TextBox runat="server" ID="PersonLastName" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            Address
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PersonAddress"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Address is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="PersonAddress" MaxLength="100"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            City
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PersonCity"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="City is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="PersonCity"></asp:TextBox>
                                        </td>
                                        <td style="padding-left: 10px">
                                            Zip
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PersonZip"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="Zip is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="PersonZip" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            State
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="PersonUsState"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson" ToolTip="State is required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="PersonUsState" runat="server" DataTextField="State" DataValueField="State"
                                                              Width="100px" Height="25px"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            Day Phone
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="DayPhone" MaxLength="20"></asp:TextBox>
                                        </td>
                                        <td style="padding-left: 15px">
                                            Other<br/> Phone
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="OtherPhone" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 30px">
                                            Password
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="WebPassword"
                                                ErrorMessage="" Text="*" ValidationGroup="ValGrpPerson"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="WebPassword" MaxLength="12"></asp:TextBox>
                                        </td>
                                        <td style="padding-left: 15px"></td>
                                        <td><asp:TextBox runat="server" ID="CustomerId" Enabled="False" Visible="False"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <asp:Button runat="server" ID="AddButton" Text="Add" CssClass="btn btn-info" OnClick="OnAddClick" OnClientClick = 'return confirm("Are you sure you want to save?");' />
                                            <asp:Button runat="server" ID="ClearButton" Text="Clear" CssClass="btn btn-info" OnClick="OnClearClick"/>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="HiddenBtn" Style="display: none" Enabled="False"></asp:Button>
                                        </td>
                                    </tr>

                                </table>
<%--                                <div style="padding-left: 30px">--%>
<%--                                    <asp:Label runat="server" ID="MsgLabel" Width="700px"></asp:Label>--%>
<%--                                </div>--%>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <asp:Label runat="server" ID="InfoTitle"></asp:Label>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" >
                </ajaxToolkit:ModalPopupExtender>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
