﻿using System;
using System.Collections.Generic;
using System.IO;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class EndSession2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: End Session";
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
            var itemList = QueryUtils.GetItemsCp(BatchNumber.Text.Trim(), this);
            lstItemList.Items.Clear();
            var gradeItemsList = new List<MeasureGradeItemModel>();
            foreach (var singleItemModel in itemList)
            {
                lstItemList.Items.Add(singleItemModel.FullItemNumber);
                gradeItemsList.Add(new MeasureGradeItemModel(singleItemModel));
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);
            if (lstItemList.Items.Count > 0)
            {
                lstItemList.SelectedIndex = 0;
                OnItemListSelectedChanged(null, null);
            }
            
            resultList.Items.Clear();
             foreach (var singleItemModel in itemList)
             {
                 var res = QueryUtils.SetEndSession(singleItemModel, this);
                 resultList.Items.Add(res);
             }
        }

        protected void OnItemListSelectedChanged(object sender, EventArgs e)
        {
            var itemNumber = lstItemList.SelectedValue;
            var singleItemModel = GetItemNumbers().Find(m => m.FullItemNumber == itemNumber);
            if (singleItemModel == null) return;
            ShowPicture(singleItemModel.Path2Picture);
        }

        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            } else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = dbPicture;
            ErrPictureField.Text = errMsg;

        }

        private List<SingleItemModel> GetItemNumbers()
        {
            var itemsList = GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel>;
            if (itemsList == null)
            {
                return new List<SingleItemModel>();
            }
            return itemsList;
        }

    }
}