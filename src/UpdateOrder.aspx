﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="UpdateOrder.aspx.cs" Inherits="Corpt.UpdateOrder" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Update Order</div>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
             <ContentTemplate>
                 <div class="navbar nav-tabs">
                     <asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
                         <asp:Label ID="Label1" runat="server" Text="Order: " CssClass="label" Style=""></asp:Label>
                         <asp:TextBox runat="server" ID="OrderField" MaxLength="7" Style="margin-top: 10px;margin-left: 5px"></asp:TextBox>
                         <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                             ValidChars="0123456789" TargetControlID="OrderField" />
                         <asp:ImageButton ID="SearchButton" runat="server" ToolTip="Load Order" ImageUrl="~/Images/ajaxImages/search.png"
                             OnClick="OnSearchClick" Style="margin-right: 15px" />
                         <asp:Button runat="server" ID="AddButton" Text="Add" CssClass="btn btn-info" 
                             Visible="False" OnClick="OnAddClick" />
                         <asp:Button runat="server" ID="UpdateButton" Text="Update" CssClass="btn btn-info" OnClientClick = 'return confirm("Are you sure you want to update?");'
                             Visible="False" OnClick="OnUpdateClick" ToolTip="Set changes to db" />
                         <asp:Button runat="server" ID="ClearButton" Text="Clear" CssClass="btn btn-info" 
                             Visible="False" OnClick="OnClearClick" />
                     </asp:Panel>
                 </div>
                 <table>
                     <tr>
                         <td style="vertical-align: top">
                             <asp:TreeView ID="TreeHistory" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                 onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                 Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                 ShowLines="False" ShowExpandCollapse="true">
                             </asp:TreeView>
                         </td>
                         <td style="width: 60px"></td>
                         <td style="vertical-align: top">
                             <asp:DataGrid runat="server" ID="ItemsGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                 DataKeyField="UniqueKey" OnItemDataBound="OnItemDataBound">
                                 <Columns>
                                     <asp:BoundColumn DataField="FullItemNumber" HeaderText="Item Number"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="CpName" HeaderText="Customer program"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="ItemCode" HeaderText="ItemCode" Visible="False"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="BatchId" HeaderText="BatchId" Visible="False"></asp:BoundColumn>
                                     <asp:TemplateColumn>
                                         <HeaderTemplate>
                                             Report</HeaderTemplate>
                                         <ItemTemplate>
                                             <asp:DropDownList runat="server" ID="DocItemFld" DataTextField="OperationTypeName" Font-Size="small" Font-Names="Cambria"
                                                 DataValueField="Key" Width="250px" />
                                         </ItemTemplate>
                                     </asp:TemplateColumn>
                                     <asp:TemplateColumn>
                                         <HeaderTemplate>Delete</HeaderTemplate>
                                         <ItemTemplate>
                                             <asp:ImageButton ID="DeleteBtn" runat="server" ToolTip="Delete Line" ImageUrl="~/Images/ajaxImages/delete.png"
                                                 OnClick="OnDelItemClick" />
                                         </ItemTemplate>
                                     </asp:TemplateColumn>
                                 </Columns>
                                 <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                 <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                             </asp:DataGrid>
                         </td>
                     </tr>
                 </table>

                 <%-- Choice  document --%>
                 <asp:Panel runat="server" ID="ChoiceDocPanel" CssClass="modalPopup" Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="ChoiceDocDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <b> Common Details</b>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="ChoiceBatchIdFld"></asp:HiddenField>
                    <table style="margin-top: 10px">
                        <tr>
                            <td style="padding-right: 10px">Customer program: </td>
                            <td ><asp:TextBox runat="server" ID="CpNameFld" ReadOnly="True" Style="width: 100%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Select Report Type: </td>
                            <td><asp:DropDownList runat="server" ID="DocsFld" DataTextField="OperationTypeName" DataValueField="Key"/></td>
                        </tr>
                    </table> 
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="ChoiceAddButton" runat="server" Text="Add" OnClick="OnChoiceAddClick"/>
                            <asp:Button ID="ChoiceCancButton" runat="server" Text="Cancel"/>
                        </p>
                    </div>
                 </asp:Panel>
                 <asp:Button runat="server" ID="ChoiceHiddenBtn" Style="display: none" />
                 <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="ChoiceHiddenBtn"
                     PopupControlID="ChoiceDocPanel" ID="ChoiceDocPopupExtender" PopupDragHandleControlID="ChoiceDocDragHandle"
                     OkControlID="ChoiceCancButton">
                 </ajaxToolkit:ModalPopupExtender>
                 <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup"  Style="width: 410px;display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b> Information</b>
                            
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto;max-width: 400px;max-height: 300px;margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                 <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                     PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                     OkControlID="InfoCloseButton">
                 </ajaxToolkit:ModalPopupExtender>
             </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
