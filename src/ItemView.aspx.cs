﻿//using System;
//using System.Collections.Generic;
//using System.Data;
using System.Drawing;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using Corpt.Constants;
//using Corpt.Models;
//using Corpt.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;
using System.IO;


namespace Corpt
{
    public partial class ItemView : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Items View";
            lnkMovedItems.Visible = false;

            if ((Request.QueryString["BatchID"] != null) && (Request.QueryString["All"] != null))
            {
                //call Short Report for the whole batch;
                var batchModel = QueryUtils.SpGetBatch(int.Parse(Request.QueryString["BatchID"]), this);
                var shortReportModel = DisplayBatchShortReport(batchModel);
                SetViewState(shortReportModel, SessionConstants.ShortReportModel);
                SetViewState(shortReportModel.MovedItems, SessionConstants.MovedItems);

                if (batchModel.CustomerId.Length > 0 && batchModel.CustomerOfficeId.Length > 0)
                {
                    if (!IsPostBack)
                    {
                        var addresses = QueryUtils.GetPersons(batchModel.CustomerId, batchModel.CustomerOfficeId, this);
                        MailAddressList.DataSource = addresses.FindAll(m => !string.IsNullOrEmpty(m.Email));
                        MailAddressList.DataBind();
                        OnChoiceMailFromContacts(null,null);
                    }

                    GoEmailButton.Visible = true;
                    MailAddressList.Visible = true;
                }
                else
                {
                    GoEmailButton.Visible = false;
                    MailAddressList.Visible = false;
                }

                btnMovedItems.Visible = shortReportModel.MovedItems.Count > 0;
                SetMovedItemsData(shortReportModel);

            }
            else
            {
                if ((Request.QueryString["BatchId"] != null) && (Request.QueryString["ItemCode"] != null))
                {
                    DisplayItemProperties(Request.QueryString["BatchID"], Request.QueryString["ItemCode"],
                                          Request.QueryString["ItemNumber"]);
                }
            }
            cmdDownloadExcel.Visible = GetViewState(SessionConstants.ShortReportModel) != null;
            //lblInfo.Text = Utils.NullValue(Session["KMMessage"]); //TODO
        }
        private void SetMovedItemsData(ShortReportModel shortReportModel)
        {
            //-- Create Table for display moved items
            var moveTable = new DataTable("MovedItems");
            moveTable.Columns.Add(new DataColumn("Old Item Number"));
            moveTable.Columns.Add(new DataColumn("New Item Number"));
            moveTable.AcceptChanges();
            foreach (var movedItemModel in shortReportModel.MovedItems)
            {
                moveTable.Rows.Add(new object[] { movedItemModel.FullItemNumberWithDotes, movedItemModel.NewFullItemNumberWithDotes });
            }
            moveTable.AcceptChanges();
            dgPopup.DataSource = moveTable;
            dgPopup.DataBind();
            
        }

        private ShortReportModel DisplayBatchShortReport(BatchModel batchModel)
        {
            ShortReportPanel.Visible = true;

            //-- Get Short Report
            headerShortReport.Text = "Short Report for the whole batch " + batchModel.FullBatchNumber;
            var shortReportModel = Utlities.GetShortReportData(batchModel, this);
            new ShortReportUtils(false).CreateMergedItemStructValueTable(shortReportModel);
            var param = "" + Session["RemoveEmptyCols"];
            if (param == "1")
            {
                List<string> colList = Utlities.GetEmptyColumns(shortReportModel.ReportView);
                if (colList.Count > 0)
                {
                    foreach (var col in colList)
                        shortReportModel.ReportView.Columns.Remove(col);
                }
            }
            
            tblFullReport.DataSource = shortReportModel.ReportView.Copy();
            
            tblFullReport.DataBind();
            return shortReportModel;
        }

        private void DisplayItemProperties(String batchId, String itemCode, String itemNumber)
        {
            var batchModel = QueryUtils.SpGetBatch(Int32.Parse(batchId), this);
            var documentId = QueryUtils.GetDocumentId(batchModel, this);

            var docStructureList = QueryUtils.GetDocStructure(documentId, this);
            var itemStructList = QueryUtils.GetItemStructureByBatch(batchModel, this);
            var itemValueList = QueryUtils.GetItemValuesAndCp(batchModel, itemCode, this);
            
            var myTable = new DataTable("Item Properties");
            myTable.Columns.Add(new DataColumn("Name"));
            myTable.Columns.Add(new DataColumn("Value"));

            if (docStructureList.Count > 0)
            {
                foreach (DocStructModel docModel in docStructureList)
                {
                    var datarow = myTable.NewRow();

                    datarow["Name"] = docModel.Title;
                    datarow["Value"] = Utlities.ParametersToValue(docModel.Value, batchId, itemCode,
                                                               itemStructList, itemValueList, this);
                    myTable.Rows.Add(datarow);
                }
            }
            //display myTable
            if (myTable.Rows.Count > 0)
            {
                WrongInfoLabel.Visible = false;
                headerDocStruct.Text = string.Format("Item {0} Properties", itemNumber);
                PropertiesPanel.Visible = true;

                tblDocStructure.DataSource = myTable;
                tblDocStructure.DataBind();
            }
            else
            {
                PropertiesPanel.Visible = false;
                WrongInfoLabel.Visible = true;
                WrongInfoLabel.Text = "Short Data Not Found";
            }
        }

        protected void CmdDownloadExcelClick(object sender, ImageClickEventArgs e)
        {
            var shortReportModel = GetViewState(SessionConstants.ShortReportModel) as ShortReportModel;
            if (shortReportModel == null)
            {
                return;
            }
            var fileName = shortReportModel.BatchModel.FullBatchNumber;
            ExcelUtils.ExportThrouPoi(shortReportModel.ReportView, fileName, this);

        }

        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    if (cell.Text.IndexOf("text_highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        cell.BackColor = Color.FromArgb(252, 248, 227);
                    }
                }
            }
        }

        protected void OnPropertiesDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    if (cell.Text.IndexOf("text_highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        cell.BackColor = Color.FromArgb(252, 248, 227);
                    }
                }
            }
        }

        protected void OnSendEmailClick(object sender, ImageClickEventArgs e)
        {
            var addressTo = AddressTo.Text.Trim();
            if (string.IsNullOrEmpty(addressTo)) return;
            var shortReportModel = GetViewState(SessionConstants.ShortReportModel) as ShortReportModel;
            if (shortReportModel == null)
            {
                return;
            }
            new ShortReportUtils(true).CreateMergedItemStructValueTable(shortReportModel);
            var fileName = shortReportModel.BatchModel.FullBatchNumber + "_" + DateTime.Now.Ticks;
            var dss = new DataSet();
            dss.Tables.Add(shortReportModel.ReportView.Copy());
            ExcelUtils.ExportThrouPoi(dss, fileName, true, this);
            ExcelUtils.SendMailWithAttachExcel("" + addressTo, fileName, this);
            //ExcelUtils.DownloadExcelFile(fileName + ".xlsx", this);

        }
        protected void OnChoiceMailFromContacts(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MailAddressList.SelectedValue)) return;
            AddressTo.Text = MailAddressList.SelectedValue;
        }

    }
}