﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="BulkUpdate2.aspx.cs" Inherits="Corpt.BulkUpdate2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Bulk Update</div>
        <div class="navbar nav-tabs">
            <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadButton"
                Style="font-size: small; padding-bottom: 5px">
                <strong>Order: </strong>
                <asp:TextBox runat="server" ID="OrderField" MaxLength="7"></asp:TextBox>
                <strong>Batch: </strong>
                <asp:TextBox runat="server" ID="BatchField" MaxLength="3"></asp:TextBox>
                <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item Numbers" ImageUrl="~/Images/ajaxImages/search16.png"
                    OnClick="OnLoadClick" />
                <asp:Button runat="server" ID="NextOrderButton" Text="Next Order" OnClick="OnNextOrderClick"
                    Enabled="False" CssClass="btn btn-info" />
                <asp:Button runat="server" ID="UpdateButton" Text="Update" OnClick="OnUpdateButtonClick"
                    CssClass="btn btn-info" Enabled="False" />
                <asp:Button runat="server" ID="UndoAllButton" Text="Undo All" OnClick="OnUndoAllButtonClick"
                    CssClass="btn btn-info" Enabled="False" />
            </asp:Panel>
        </div>
        <div>
            <asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small; color: Red" ></asp:Label>
            <asp:Label ID="LoadMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
        </div>
        <asp:Panel runat="server" ID="InfoPanel">
            <table>
                <tr>
                    <td style="vertical-align: top;font-size: smaller; font-family: Calibri;width: 250px;">
                        Batches with the same structure as the batch above
                     </td>
                    <td style="vertical-align: top">
                        <asp:CheckBox ID="IgnoreCpBox" runat="server" CssClass="checkbox" Text="Ignore Customer Program"
                            Width="220px" />
                    </td>
                    <td style="padding-left: 20px;vertical-align: top;font-size: small;font-weight: bold"><asp:Label runat="server" ID="EnteredLabel" Text="Entered Values" Visible="False"></asp:Label></td>
                     <td style="padding-left: 20px;vertical-align: top">
                        <asp:Label runat="server" ID="SelectedItemNumber" Style="font-family: Cambria;font-weight: bold"></asp:Label>
                     </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;width: 250px;">
                            <asp:TreeView ID="tvwItems" runat="server" 
                            ShowCheckBoxes="All" ExpandDepth="1" AfterClientCheck="CheckChildNodes();"
                                PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeNodeCheckChanged"
                                OnSelectedNodeChanged="OnTreeSelectedChanged" onclick="OnTreeClick(event)">
                                <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                            </asp:TreeView>
                    </td>
                    <td style="padding-left: 20px;margin-left: 10px;vertical-align: top;border-style: none">
                        <asp:Panel runat="server" ID="EditPanel" Style="font-family: Cambria;font-size: small" CssClass="form-horizontal" Visible="False">
                            <strong>Part</strong>
                            <div>
                                <asp:DropDownList runat="server" ID="PartsListField" OnSelectedIndexChanged="OnPartChanged"
                                    AutoPostBack="True" DataTextField="PartName" DataValueField="PartId" />
                            </div><br/>
                            <strong>Measure</strong>
                            <div>
                                <asp:DropDownList runat="server" ID="MeasureListField" OnSelectedIndexChanged="OnMeasureChanged" 
                                AutoPostBack="True" DataValueField="MeasureId" DataTextField="MeasureName"/>
                            </div><br/>
                            <strong>New Value</strong>
                            <div class="form-horizontal">
                                <asp:DropDownList runat="server" ID="MeasureEnumField" OnSelectedIndexChanged="OnEnumMeasureChanged"
                                    Visible="False" AutoPostBack="True" Width="100%" DataValueField="MeasureValueId" DataTextField="ValueTitle"/>
                                <asp:TextBox ID="MeasureNumericField" runat="server" onkeypress="return IsOneDecimalPoint(event);" OnTextChanged="OnNumericFieldChanged"
                                    AutoPostBack="True" Visible="False" Width="90%" ToolTip="Numeric Value" BackColor="#CCFFCC" />
                                <asp:TextBox ID="MeasureTextField" runat="server" Visible="False" Width="90%" OnTextChanged="OnTextFieldChanged" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </asp:Panel>
                    </td>
                    <td style="padding-left: 20px;vertical-align: top">
                        <asp:DataGrid runat="server" ID="EnteredGrid" AutoGenerateColumns="False" CellPadding="5" CellSpacing="5" onitemcommand="OnDelCommand" DataKeyField="UniqueKey">
                            <Columns>
                                <asp:ButtonColumn Text="Del" ButtonType="LinkButton" HeaderText="Details" HeaderImageUrl="Images/ajaxImages/del.png"/>
                                <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DisplayValue" HeaderText="Value"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" /> 
                            <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                        </asp:DataGrid>
                    </td>
                    <td style="padding-left: 20px;vertical-align: top">
                        <div id="gridContainer" style="vertical-align: top; ">
                            <asp:DataGrid runat="server" ID="dgMeasures" AutoGenerateColumns="False" CellPadding="5" CellSpacing="5">
                                <Columns>
                                    <asp:BoundColumn DataField="PartName" HeaderText="Part Name"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureName" HeaderText="Grade"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ResultValue" HeaderText="Value"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="OrderField"
        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order Code is required." />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="OrderField"
        Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>Five, six or seven numeric characters</strong>" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />

    <asp:RequiredFieldValidator runat="server" ID="BatchReq" ControlToValidate="BatchField"
        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Code is required." />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqE" TargetControlID="BatchReq"
        HighlightCssClass="validatorCalloutHighlight" />
    <asp:RegularExpressionValidator runat="server" ID="BatchRegExpr" ControlToValidate="BatchField"
        Display="None" ValidationExpression="^\d{3}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch code in the format:<br /><strong>Three numeric characters</strong>" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqExpr" TargetControlID="BatchRegExpr"
        HighlightCssClass="validatorCalloutHighlight" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server"
        FilterType="Custom" ValidChars="01234567890." TargetControlID="MeasureNumericField">
    </ajaxToolkit:FilteredTextBoxExtender>

    </div>
</asp:Content>
