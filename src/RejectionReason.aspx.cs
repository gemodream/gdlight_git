﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.TreeModel;
using Corpt.Models.Stats;
using Corpt.Models;

namespace Corpt
{
    public partial class RejectionReason : CommonPage
    {
        private static string ReasonList = "ReasonList";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Report Struct";
            if (!IsPostBack)
            {
                LoadRuleTree();
            }
        }

        #region UnAttached Measures
        private void LoadUnAttachedMeasures(List<MeasureModel> measures)
        {
            grdMeasure.DataSource = measures;
            grdMeasure.DataBind();
        }
        private List<string> GetMeasuresForAdd()
        {
            var items = new List<string>();
            foreach (DataGridItem item in grdMeasure.Items)
            {
                var checkBox = item.FindControl("Checkbox1") as CheckBox;
                if (checkBox == null || !checkBox.Checked) continue;
                var measureId = "" + grdMeasure.DataKeys[item.ItemIndex];
                items.Add(measureId);
            }
            return items;
        }

        #endregion

        #region Rules Tree (Load tree)
        private void SelectReasonOnTree(string reasonId)
        {
            if (RuleTree.Nodes.Count == 0) return;
            var root = RuleTree.Nodes[0];
            
            foreach(TreeNode node in root.ChildNodes) 
            {
                if (node.Value == reasonId)
                {
                    node.Selected = true;
                    if (node.ChildNodes.Count > 0)
                    {
                        node.Expand();
                        return;
                    }
                }
            }
        }
        private void SelectReasonOnTreeByName(string reasonName)
        {
            if (RuleTree.Nodes.Count == 0) return;
            var root = RuleTree.Nodes[0];

            foreach (TreeNode node in root.ChildNodes)
            {
                if (node.Text.Contains(reasonName)) node.Selected = true;
            }
        }
        private void LoadRuleTree()
        {
            List<MeasureModel> measures;
            var reasons = QueryUtils.GetRejectionReasons(this);
            SetViewState(reasons, ReasonList);

            var rules = QueryUtils.GetReasonMeasureRules(out measures, this);
            
            RuleTree.Nodes.Clear();

            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Report Struct" } };
            foreach (var reason in reasons)
            {
                //-- Attached Measures
                var linkMeasures = rules.FindAll(m => m.ReasonId == reason.ReasonId);
                //if (linkMeasures.Count == 0) continue;

                //-- Reason level
                data.Add(new TreeViewModel { ParentId = "0", Id = reason.ReasonId, DisplayName = "R: " + reason.ReasonName });

                //-- Measures level
                foreach (var linkMeasure in linkMeasures)
                {
                    data.Add(new TreeViewModel { ParentId = reason.ReasonId, Id = reason.ReasonId + "_" + linkMeasure.MeasureId, DisplayName = linkMeasure.MeasureName });
                }
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            RuleTree.Nodes.Add(rootNode);
            rootNode.Expand();

            LoadUnAttachedMeasures(measures);
        }
        private string GetSelectedReason()
        {
            if (string.IsNullOrEmpty(RuleTree.SelectedValue)) return "";
            var depth = RuleTree.SelectedNode.Depth;
            if (depth == 0) return "";
            if (depth == 1) return RuleTree.SelectedValue;
            if (depth == 2) return RuleTree.SelectedNode.Parent.Value;
            return "";
        }
        private List<string> GetMeasureForRemove()
        {
            var measureIds = new List<string>();
            if (string.IsNullOrEmpty(RuleTree.SelectedValue)) return measureIds;
            var depth = RuleTree.SelectedNode.Depth;
            if (depth == 0 || depth == 1) return measureIds;
            var node = RuleTree.SelectedNode;
            measureIds.Add(node.Value.Split('_')[1]);
            return measureIds;
        }
        #endregion

        #region Button Add Reason
        //-- Popup 'Add Reason' dialog
        protected void OnAddReasonClick(object sender, EventArgs e)
        {
            AddReasonFld.Text = "";
            AddReasonPopupExtender.Show();
        }

        private List<RejectionReasonModel> GetReasons()
        {
            return GetViewState(ReasonList) as List<RejectionReasonModel>;
        }
        protected void OnAddReasonOkClick(object sender, EventArgs e)
        {
            var msg = NewReasonVerify();
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, "Add Reason", true);
                AddReasonPopupExtender.Show();
                return;
            }
            QueryUtils.AddRejectReason(AddReasonFld.Text.Trim(), this);
            LoadRuleTree();
            var reason = AddReasonFld.Text.Trim();
            SelectReasonOnTreeByName(reason);
        }
        private string NewReasonVerify()
        {
            var reason = AddReasonFld.Text.Trim();
            if (reason.Length == 0)
            {
                return "Reason Name is required!";
            }
            var reasonExists = GetReasons().Find(m => m.ReasonName.ToUpper() == reason.ToUpper());
            return reasonExists == null ? "" : "Reason with this name already exists!";
        }


        #endregion

        #region Attach Measures to Reason Button
        
        protected void OnAddMeasureClick(object sender, EventArgs e)
        {
            var measureIds = GetMeasuresForAdd();
            if (measureIds.Count == 0)
            {
                PopupInfoDialog("Checked Measures Not Found!", "Add Measures", true);
                return;
            }
            var reasonId = GetSelectedReason();
            if (reasonId == "")
            {
                PopupInfoDialog("Select Reason, please!", "Add Measures", true);
                return;
            }
            QueryUtils.SetReasonMeasureRule(measureIds, reasonId, this);
            LoadRuleTree();
            SelectReasonOnTree(reasonId);
        }

        #endregion

        #region Button Remove Measure

        protected void OnDelMeasureClick(object sender, EventArgs e)
        {
            var measureIds = GetMeasureForRemove();
            if (measureIds.Count == 0) 
            {
                PopupInfoDialog("Select Measure on Rule Tree, please!", "Remove Measure", true);
                return;
            }
            var reasonId = GetSelectedReason();
            QueryUtils.SetReasonMeasureRule(measureIds, "", this);
            LoadRuleTree();
            SelectReasonOnTree(reasonId);
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion


    }
}