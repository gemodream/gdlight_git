﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Globalization;
using System.Data.SqlClient;

namespace Corpt
{
    [ScriptService]
    public partial class TrackingOrder : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!Page.IsPostBack)
            {
                hdnAuthorOfficeID.Value = Session["AuthorOfficeID"].ToString();
                LoadCustomers();
                GetOffice();
            }
        }

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            //Customer Filter by Office
            string AuthorOfficeID = Session["AuthorOfficeID"].ToString();
            //var customersOffice=customers.Where(c=>c.OfficeId== AuthorOfficeID).ToList();
            var customersOffice = customers.ToList();

            customersOffice.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customersOffice.Add(new CustomerModel { CustomerId = "0", CustomerName = "--All--" });
            customersOffice.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            
            lstCustomerList.Items.Clear();
            lstCustomerList.DataSource = customersOffice;
            lstCustomerList.DataBind();
        }

        public void GetOffice()
        {
            DataTable dtOffice = QueryUtils.GetAllOfficeList();
            ddlOffice.DataSource = dtOffice;
            ddlOffice.DataTextField = "OfficeName";
            ddlOffice.DataValueField = "OfficeID";
            ddlOffice.DataBind();

            ListItem liBlank = new ListItem();
            liBlank.Value = "";
            liBlank.Text = "";
            ddlOffice.Items.Insert(0, liBlank);

            ListItem liSelect = new ListItem();
            liSelect.Value = "0";
            liSelect.Text = "--All--";
            ddlOffice.Items.Insert(1, liSelect);
            
        }

        #region Elements
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetOrdersDetail(int CustomerID, DateTime TakeinDateFrom, DateTime TakeinDateTo)
        {
            string conn;
            if (HttpContext.Current.Session["UseRoDb"].ToString() == "1")
                conn = HttpContext.Current.Session["RO_MyIP_ConnectionString"].ToString();
            else
                conn = HttpContext.Current.Session["MyIP_ConnectionString"].ToString();

            List<OrderTracking> lstOrderTracking = new List<OrderTracking>();
            OrderTracking objOrderTracking = new OrderTracking();
            TrackingOrderUtils objOrderTrackingUtils = new TrackingOrderUtils();
            objOrderTracking.CustomerID = CustomerID;
            objOrderTracking.TakeinDateFrom = TakeinDateFrom;
            objOrderTracking.TakeinDateTo = TakeinDateTo;
            lstOrderTracking = objOrderTrackingUtils.GetOrderTracking(objOrderTracking, conn);
            var cols = lstOrderTracking.Where(w => w.GSIOrder == w.GSIOrder);
            foreach (var item in cols)
            {
                string onclick = "onclick='openTrakingOrderData(" + item.GSIOrder + ");'";
                item.GSIOrder = "<a " + onclick + ">" + item.GSIOrder + "</a>";
            }
            string Json = JsonConvert.SerializeObject(cols);
            return Json;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetOrderTrackingByLocation(int CustomerID, int AuthorOfficeID, DateTime TakeinDateFrom, DateTime TakeinDateTo, int RealOfficeID)
        {
            string conn;
            if (HttpContext.Current.Session["UseRoDb"].ToString() == "1")
                conn = HttpContext.Current.Session["RO_MyIP_ConnectionString"].ToString();
            else
                conn = HttpContext.Current.Session["MyIP_ConnectionString"].ToString();

            List<OrderTracking> lstOrderTracking = new List<OrderTracking>();
            OrderTracking objOrderTracking = new OrderTracking();
            TrackingOrderUtils objOrderTrackingUtils = new TrackingOrderUtils();

            objOrderTracking.CustomerID = CustomerID;
            objOrderTracking.RealOfficeID = RealOfficeID;
            objOrderTracking.AuthorOfficeID = AuthorOfficeID;
            objOrderTracking.TakeinDateFrom = TakeinDateFrom;
            objOrderTracking.TakeinDateTo = TakeinDateTo;
            lstOrderTracking = objOrderTrackingUtils.GetOrderTrackingByLocation(objOrderTracking, conn);
            var cols = lstOrderTracking.Where(w => w.GSIOrder == w.GSIOrder);
            foreach (var item in cols)
            {
                string onclick = "onclick='openTrakingOrderData(" + item.GSIOrder + ");'";
                item.GSIOrder = "<a " + onclick + ">" + item.GSIOrder + "</a>";
            }
            string Json = JsonConvert.SerializeObject(cols);
            return Json;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetOrderTrackingDetail(int OrderCoder)
        {
            string conn;
            if (HttpContext.Current.Session["UseRoDb"].ToString() == "1")
                conn = HttpContext.Current.Session["RO_MyIP_ConnectionString"].ToString();
            else
                conn = HttpContext.Current.Session["MyIP_ConnectionString"].ToString();

            DataSet ds = TrackingOrderUtils.GetOrderHistory(OrderCoder, conn);
			string Json = JsonConvert.SerializeObject(ds.Tables[0], new JsonSerializerSettings() { DateFormatString = "dd MMM yyyy hh:mm tt" });
            return Json;
        }
        #endregion

    }
}