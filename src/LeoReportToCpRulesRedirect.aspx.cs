using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for LeoReportToCpRulesRedirect.
	/// </summary>
	public partial class LeoReportToCpRulesRedirect : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			else
			{
				lblInfo.Text="";
				PerformRedirect();
			}
		}

		private void PerformRedirect()
		{
			var itemParts = Request.Params["ItemNumber"].Split('.');
		    var itemModel = new ItemModel
		                        {OrderCode = itemParts[0], BatchCode = itemParts[1], ItemCode = itemParts[2]};
		    var newModel = QueryUtils.GetNewItemNumberFromOld(itemModel, this);
			if(newModel == null)
			{
				lblInfo.Text="Can't find info in DB. Call IT";
			    return;
			}
			lblInfo.Text="";


			var strRedirectString = "CPResult.aspx?ItemNumber="+ newModel.FullItemNumberWithDotes +"&Flag=OneItem";
			lblInfo.Text=strRedirectString;
			Response.Redirect(strRedirectString);
	    }

	}
}
