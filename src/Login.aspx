﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Login.aspx.cs" Inherits="Corpt.Login" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GSI: Login</title>
    <link rel="icon" type="image/png" href="logo@2x.png" />    
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="Style/bootstrap2.0/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="Style/bootstrap2.0/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="Style/bootstrap2.0/js/jquery.js" type="text/javascript"></script>
   
</head>
<body>

     <script>

		 //Get Office function
         getOffice();

		 async function getCity() {
			 var city = "";
			 if (navigator.geolocation) {
				 try {
					 const position = await new Promise((resolve, reject) => {
						 navigator.geolocation.getCurrentPosition(resolve, reject);
					 });
					 //alert(position);
					 // The user's location is available in the 'position' object
					 const latitude = position.coords.latitude;
					 const longitude = position.coords.longitude;

					 const apiKey = 'AIzaSyCckVZIgXAjLVx-LkfFfemHumals1U2CiA';

					 // Construct the API request URL for reverse geocoding
					 const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${apiKey}`;

					 // Send a GET request to the Geocoding API
					 await $.getJSON(apiUrl, function (data) {
						 const addressComponents = data.results[0].address_components;
						 for (const component of addressComponents) {
							 if (component.types.includes("locality")) {
								 city = component.long_name;
							 }
						 }
					 });
				 } catch (error) {
					 city = "";
				 }
			 }
			 else {
				 x.innerHTML = "Geolocation is not supported by this browser.";
			 }
			
			 return city;
         }

		 async function getIP() {
			 var ip = "";
			 await $.getJSON('https://httpbin.org/ip', function (data) {
				 ip = data['origin'];
			 });
			 return ip;
         }

         async function getOffice() {
			 var objCoOrds = new Object();
			 objCoOrds.ip = await getIP();
			 objCoOrds.city = '';
			 var city = await getCity();
			 //objCoOrds.ip = '49.248.14.123';
			 if (objCoOrds.ip != "") {
				 $.ajax({
					 type: "POST",
					 url: 'login.aspx/GetOffice',
					 data: JSON.stringify(objCoOrds),
					 contentType: "application/json; charset=utf-8",
					 dataType: "json",
					 cache: false,
					 success: function (data) {
						 const myObj = JSON.parse(data.d);
						 var IPAddress = "";
						 var i = 0;
						 var cityLocation = "";
						 if (city != "")
							 cityLocation = myObj.filter(item => item.City === city);
						 else
							 cityLocation = myObj;

						 for (let x in cityLocation) {
							 var OfficeID = cityLocation[x].OfficeID;
							 var OfficeName = cityLocation[x].OfficeName;
							 IPAddress = cityLocation[x].IPAddress;
							 $('#ddlOffice').append('<option value="' + OfficeID + '">' + OfficeName + '</option>');
							 i++;
						 }
						 if (IPAddress != null && i == 1) {
							 $('#trOffice').hide();
						 }
						 else if (i == 0) {
							 $('#ddlOffice').append('<option value="0">None of Above</option>');
							 $('#trOffice').hide();
						 }
						 else {
							 $('#ddlOffice').prepend('<option value="-1" selected>-Select-</option>');
							 $('#ddlOffice').append('<option value="0">None of Above</option>');
							 $('#trOffice').show();
						 }
					 },
					 error: function (request, status, error) {
						 alert(request.responseText);
					 }
				 });
			 }
         }
	 </script>

    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
        </ajaxToolkit:ToolkitScriptManager>
        <div class="demoarea">
            <div class="demoheading">Log In </div>
             <asp:Panel ID="Panel1" runat="server" DefaultButton="Activate" CssClass="form-horizontal">
            <table style="margin-left: 100px;"cellpadding="5px">
                <tr>
                    <td style="vertical-align: top">Login:</td>
                    <td><asp:TextBox runat="server" ID="LoginTxt" Width="170px" /></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">Password: </td>
                    <td>
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" Width="170px" />
                    </td>
                </tr>
			    <tr id="trOffice" style="display:none;">
				    <td style="vertical-align: top; width: 60px;">
					    <asp:Label ID="lblOffice" Text="Office:" runat="server"></asp:Label>
				    </td>
				    <td>
					    <asp:DropDownList ID="ddlOffice" runat="server" Style="width: 185px;"></asp:DropDownList>
				    </td>
			    </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                         <asp:Button ID="Activate" runat="server" Text="Login" OnClick="ActivateClick" class="btn btn-primary" Style="margin-top: 0px"/>
                    </td>
                </tr>
            </table>
                 </asp:Panel>
            <br/>
            <asp:RequiredFieldValidator runat="server" ID="NReq"
                ControlToValidate="LoginTxt"
                Display="None"
                ErrorMessage="<b>Required Field Missing</b><br />A Login is required." />
            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="NReqE"
                TargetControlID="NReq"
                HighlightCssClass="validatorCalloutHighlight"/>

            <asp:RequiredFieldValidator runat="server" ID="PReq"
                ControlToValidate="Password"
                Display="None"
                ErrorMessage="<b>Required Field Missing</b><br />A Password is required." />
            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="PReqE"
                TargetControlID="PReq"
                HighlightCssClass="validatorCalloutHighlight"/>

             <asp:RequiredFieldValidator runat="server" ID="LReq"
                ControlToValidate="ddlOffice" InitialValue="-1"
                Display="None"
                ErrorMessage="<b>Required Field Missing</b><br />Please select Office." />
             <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="LReqE"
                TargetControlID="LReq"
                />
            <br /><br />
            <asp:Label ID="LabelInfo" runat="server" />
            <div id="output"></div>
        </div>
    </form>
</body>
</html>
