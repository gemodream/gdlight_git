﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="EverledgerURLGenerator.aspx.cs" Inherits="Corpt.EverledgerURLGenerator" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <div class="demoarea">

        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

        <div class="demoheading">Everledger URL Generator</div>

        <asp:UpdatePanel runat="server" ID="MainPanel">
            <ContentTemplate>

                <div class="navbar nav-tabs">
                    <asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
                        <asp:Label ID="Label1" runat="server" Text="New Order Number: " CssClass="label" Style=""></asp:Label>
                        <asp:TextBox runat="server" ID="OrderField" MaxLength="7" Style="margin-top: 10px; margin-left: 5px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                            ValidChars="0123456789" TargetControlID="OrderField" />
                        <asp:ImageButton ID="SearchButton" runat="server" ToolTip="Load Order" ImageUrl="~/Images/ajaxImages/search.png"
                            OnClick="OnSearchClick" Style="margin-right: 15px" />
                        <asp:Button runat="server" ID="btnGenerateURL" Text="Generate URL" CssClass="btn btn-info" OnClick="OnGenerateURL" />
                       
                        <asp:Button runat="server" ID="ClearButton" Text="Clear" CssClass="btn btn-info"
                            Visible="False" OnClick="OnClearClick" />
                    </asp:Panel>
                </div>

                <table>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:TreeView ID="TreeHistory" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                ShowLines="False" ShowExpandCollapse="true">
                            </asp:TreeView>
                        </td>
                        <td style="width: 100px"></td>
                        <td style="vertical-align: top; text-align: right;">
                            <asp:DataGrid runat="server" ID="ResultGrid" AutoGenerateColumns="False" CssClass="table table-condensed" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="ItemNumber" HeaderText="Item Number">
                                        <ItemStyle Width="20px" Wrap="false" />
                                        <HeaderStyle Width="20px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="APIStatus" HeaderText="Item Status">
                                        <%--  <ItemStyle Width="50px" Wrap="false" />
                                        <HeaderStyle Width="50px" Wrap="false" />--%>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CreateItemStatus" HeaderText="Create Item Status">
                                        <ItemStyle Width="50px" Wrap="True" />
                                        <HeaderStyle Width="50px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AssociateDiamondStatus" HeaderText="Associate Diamond Status">
                                        <ItemStyle Width="10px" Wrap="True" />
                                        <HeaderStyle Width="10px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="URLGenStatus" HeaderText="URL Generation Status">
                                        <ItemStyle Width="10px" Wrap="True" />
                                        <HeaderStyle Width="10px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AddGradingStatus" HeaderText="Add Grading Status">
                                        <ItemStyle Width="10px" Wrap="True" />
                                        <HeaderStyle Width="10px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="URL" HeaderText="URL">
                                        <ItemStyle Width="50px" Wrap="True" />
                                        <HeaderStyle Width="50px" Wrap="True" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ShortCode" HeaderText="Short Code">
                                        <ItemStyle Width="50px" Wrap="True" />
                                        <HeaderStyle Width="50px" Wrap="false" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DBSaveURL" HeaderText="DB Save URL">
                                        <ItemStyle Width="50px" Wrap="True" />
                                        <HeaderStyle Width="50px" Wrap="True" />
                                    </asp:BoundColumn>

                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>


                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>

                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OkControlID="InfoCloseButton">
                </ajaxToolkit:ModalPopupExtender>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
