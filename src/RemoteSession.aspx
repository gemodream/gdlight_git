﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="RemoteSession.aspx.cs" Inherits="Corpt.RemoteSession" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Remote Sessions</div>
                <asp:Panel ID="Panel1" runat="server"  CssClass="form-inline">
            <!-- Date From -->
            <label>
                From</label>
            <asp:TextBox runat="server" ID="calFrom" Width="100px" />
            <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <!-- Date To -->
            <label style="padding-left: 20px;">
                To</label>
            <asp:TextBox runat="server" ID="calTo" Width="100px"/>
            <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <label style="padding-left: 20px;">
                User</label>
            <asp:DropDownList runat="server" ID="UserList" DataTextField="DisplayName" DataValueField="UserId" AutoPostBack="true" Width="150px"/>
            <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Lookup" OnClick="OnLookupClick"
                Style="margin-left: 15px"></asp:Button>
        </asp:Panel>
        <br />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
            PopupButtonID="Image1" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
            PopupButtonID="Image2" />
        <asp:Label runat="server" ID="countLabel" class="label" Style="text-align: left" />
        <ul class="pagination" id="pagesTop" runat="server">
        </ul>
        <div class="content">
            <div class="row">
                <div style="width: 800px; text-align: center;padding-left: 20px">
                    <div id="gridContainer" style="font-size: small;text-align: center">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" class="table-bordered"
                            CellPadding="5">
                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="User ID" DataField="UserId" />
                                <asp:BoundField HeaderText="User Name" DataField="DisplayName" />
                                <asp:BoundField HeaderText="Ip" DataField="Ip" />
                                <asp:BoundField HeaderText="Country" DataField="CountryCode" />
                                <asp:BoundField HeaderText="Region" DataField="RegionCode" />
                                <asp:BoundField HeaderText="Login Date" DataField="LoginDate" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <ul class="pagination" id="pagesBotton" runat="server">
        </ul>
    </div>
</asp:Content>
