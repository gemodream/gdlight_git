using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for Expire.
	/// </summary>
	public partial class Expire : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdExpire_Click(object sender, System.EventArgs e)
		{

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

			Label6.Text=conn.Database + " : " + conn.DataSource;
			
			SqlCommand command = new SqlCommand("SELECT * from V0group where groupcode = @GroupCode; Select * from v0item where groupcode = @GroupCode");
			command.Connection = conn;
			command.CommandType=CommandType.Text;

			command.Parameters.Add("@GroupCode",txtOrderNumber.Text);

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			Label6.Text=ds.Tables.Count.ToString();

			DataGrid1.DataSource=ds.Tables[0];
			DataGrid1.Visible=true;
			DataGrid1.DataBind();

			DataGrid2.DataSource=ds.Tables[1];
			DataGrid2.Visible=true;
			DataGrid2.DataBind();

		}
	}
}
