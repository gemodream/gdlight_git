﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Itemize1.aspx.cs" Inherits="Corpt.Itemize1" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" runat="server" contentplaceholderid="PageHead">
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
    <style type="text/css">
        .auto-style1 {
            width: 459px;
        }
        .auto-style2 {
            width: 577px;
        }
        .auto-style3 {
            width: 134px;
            margin-bottom: 0;
        }
        .auto-style5 {
            width: 1099px;
        }
        .auto-style6 {
            height: 35px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1"></ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">
        $( document ).ready( function ()
        {
            $( "#<%= ChangeCPListField.ClientID %>" ).select2( {
                placeholder: {
                    id: '',
                    text: ' '
                }
            } );
            $( "#<%= CpListField.ClientID %>" ).select2( {
                placeholder: {
                    id: '',
                    text: ' '
                }
            } );
        } );
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        function EndRequestHandler( sender, args )
        {
            var itemCount = $("#<%= ItemsToAdd.ClientID %>" ) != null ? $("#<%= ItemsToAdd.ClientID %>").val() + "" : "";
            if (itemCount != "")
            {
                $( "#<%= ItemField.ClientID %>" ).focus();
            }
        }

        prm.add_pageLoaded( EndRequestHandler );
        function ShowPage()
        {
            aspnetForm.target = "_blank";
            setTimeout( function ()
            {
                aspnetForm.target = "_self";
            }, 1000 );
        }
        function DataPrint()
        {
            var prtContent = document.getElementById('<%= GridNewBatches.ClientID %>' );
            prtContent.border = 0; //set no border here
            var Print = window.open( '', '', 'left=100,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1' );
            Print.document.write( prtContent.outerHTML );
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
        function LabelOldPrint()
        {
            var prtContent = document.getElementById('<%= LabelOldGrid.ClientID %>' );
            prtContent.border = 0; //set no border here
            var Print = window.open( '', '', 'left=10,top=10,width=1000,height=1000,toolbar=1,scrollbars=0,status=0,resizable=1' );
            Print.document.write( prtContent.outerHTML );
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
        function LabelOld2Print()
        {
            var prtContent = document.getElementById('<%= LabelOld2Grid.ClientID %>' );
            prtContent.border = 0; //set no border here
            var Print = window.open( '', '', 'left=10,top=10,width=1000,height=1000,toolbar=1,scrollbars=0,status=0,resizable=1' );
            Print.document.write( prtContent.outerHTML );
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }

    </script>
    <div class="demoarea">
        <div class="demoheading">Itemize</div>
        <div style="border-style: double; border-color: inherit; border-width: medium;" class="auto-style1">
        <table class="auto-style2">
            <tr>
                            <td>
                                <asp:Label runat="server" Text="Order Number"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="OrderSearchButton">
                                    <asp:TextBox runat="server" ID="OrderField"></asp:TextBox>
                                    <asp:ImageButton ID="OrderSearchButton" runat="server" ToolTip="Search Order" ImageUrl="~/Images/ajaxImages/search16.png"
                                        OnClick="OnOrderSearchClick" />
                                    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="OrderField"
                                        Display="None"
                                        ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required."
                                        ValidationGroup="OrderGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="OrderReqE" TargetControlID="OrderReq"
                                        HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
                                    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="OrderField"
                                        Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$"
                                        ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>"
                                        ValidationGroup="OrderGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                        HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
                                </asp:Panel>
                            </td>
                            <td colspan="3" style="font-weight: bold"></td>

                        </tr>
            <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Memo Number"></asp:Label></td>
                            <td>
                                <asp:DropDownList runat="server" ID="MemoListField" /></td>
                        </tr>
        </table>
            </div>
        <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False"
            ActiveTabIndex="2" Height="100%" Width="100%" ScrollBars="Auto" Visible="true" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
            <ajaxToolkit:TabPanel runat="server" HeaderText="Change CP" OnDemandMode="Once" ID="ChangeCPPanel" Enabled="false" BorderWidth="15px">
                <ContentTemplate>
                    <div style="padding-top: 10px; border:double">
                        <div class="navbar nav-tabs">
                            <asp:DropDownList runat="server" ID="ChangeCPListField" OnSelectedIndexChanged="OnChangeCpCpChanged" AutoPostBack="True" />
                            <asp:HiddenField ID="ChangeCPItemTypeId" runat="server" />
                            <asp:Button runat="server" ID="CPCopyButton" Text="SELECT" OnClick="OnCPCopyClick" CssClass="btn btn-info"></asp:Button>
                            <asp:Button runat="server" ID="CPClearButton" Text="CANCEL" OnClick="OnCPClearClick" CssClass="btn btn-info"></asp:Button>
                        </div>
                        <table border="1">
                            <tr>
                                <td>
                                    <asp:Label runat="server" Style="width: 350px" ID="OldCP" Text="Old CP:" ForeColor="Black"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Style="width: 350px; padding-left: 20px;" ID="NewCP" ForeColor="Black" Text="New CP:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old customer id -->
                                <td>
                                    <asp:Panel runat="server" ID="OldCustomerIDPanel">
                                        <asp:Label runat="server" ID="OldCustomerIDLabel" Text="Old Customer ID" ForeColor="Black" ToolTip="Old Customer ID"></asp:Label>
                                        <asp:TextBox ID="OldCustomerIDBox" runat="server"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                                <!-- New Customer ID -->
                                <td style="padding-left: 20px;">
                                    <asp:Panel ID="NewCustomerIDPanel" runat="server">
                                        <asp:Label runat="server" ID="NewCustomerIDLabel" Text="New Customer ID" ForeColor="Black" ToolTip="New Customer ID"></asp:Label>
                                        <asp:TextBox runat="server" ID="NewCustomerIDBox"></asp:TextBox>
                                        <asp:HiddenField ID="ChangeCPNewOrder" runat="server" />
                                        <asp:HiddenField ID="ChangeCPOldItem" runat="server" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old Customer Style -->
                                <td>
                                    <asp:Panel runat="server" ID="OldCustomerStylePanel">
                                        <asp:Label runat="server" ID="OldCustomerStyleLabel" Text="Old Customer Style" ForeColor="Black" ToolTip="Old Customer Style"></asp:Label>
                                        <asp:TextBox ID="OldCustomerStyleBox" runat="server"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                                <!-- New Customer Style -->
                                <td style="padding-left: 20px;">
                                    <asp:Panel ID="NewCustomerStylePanel" runat="server">
                                        <asp:Label runat="server" ID="NewCustomerStyleLabel" Text="New Customer Style" ForeColor="Black" ToolTip="New Customer Style"></asp:Label>
                                        <asp:TextBox runat="server" ID="NewCustomerStyleBox"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old SRP -->
                                <td>
                                    <asp:Panel runat="server" ID="OldSRPPanel">
                                        <asp:Label runat="server" ID="OldSRPLabel" Text="Old SRP" ForeColor="Black" ToolTip="Old SRP"></asp:Label>
                                        <asp:TextBox ID="OldSRPBox" runat="server"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                                <!-- New SRP -->
                                <td style="padding-left: 20px;">
                                    <asp:Panel ID="NewSRPPanel" runat="server">
                                        <asp:Label runat="server" ID="NewSRPLabel" Text="New SRP" ForeColor="Black" ToolTip="New SRP"></asp:Label>
                                        <asp:TextBox runat="server" ID="NewSRPBox"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old Description -->
                                <td>
                                    <asp:Panel runat="server" ID="OldDescPanel">
                                        <asp:Label runat="server" ID="OldDescLabel" Text="Old Description" ForeColor="Black" ToolTip="Old SRP"></asp:Label>
                                        <asp:TextBox ID="OldDescBox" TextMode="MultiLine" Columns="50" Rows="5" runat="server"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                                <!-- New Description -->
                                <td style="padding-left: 20px;">
                                    <asp:Panel ID="NewDescPanel" runat="server">
                                        <asp:Label runat="server" ID="NewDescLabel" Text="New Description" ForeColor="Black" ToolTip="New SRP"></asp:Label>
                                        <asp:TextBox runat="server" TextMode="MultiLine" Columns="50" Rows="5" ID="NewDescBox"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old Comment -->
                                <td style="height: 94px">
                                    <asp:Panel runat="server" ID="OldCommentPanel">
                                        <asp:Label runat="server" ID="OldCommentLabel" Text="Old Comments" ForeColor="Black" ToolTip="Old Comment"></asp:Label>
                                        <asp:TextBox ID="OldCommentBox" TextMode="MultiLine" Columns="50" Rows="5" runat="server"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                                <!-- New Comment -->
                                <td style="padding-left: 20px; height: 94px;">
                                    <asp:Panel ID="NewCommentPanel" runat="server">
                                        <asp:Label runat="server" ID="NewCommentLabel" Text="New Comments" ForeColor="Black" ToolTip="New Comment"></asp:Label>
                                        <asp:TextBox runat="server" TextMode="MultiLine" Columns="50" Rows="5" ID="NewCommentBox"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <!-- Old Picture -->
                                <td>
                                    <asp:Panel runat="server" ID="OldPicturePanel">
                                        <asp:Label runat="server" ID="OldPicture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                                        <asp:Image ID="OldItemPicture" runat="server" Width="200px" BorderWidth="1px"></asp:Image>
                                    </asp:Panel>
                                </td>
                                <!-- New Picture -->
                                <td style="padding-left: 20px;">
                                    <asp:Panel ID="NewPicturePanel" runat="server">
                                        <asp:Label runat="server" ID="NewPicture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label>
                                        <asp:Image ID="NewItemPicture" ImageUrl="~/Images/ajaxImages/information24.png" runat="server" Width="200px" BorderWidth="1px"></asp:Image>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="OldNew" OnDemandMode="Once" ID="OldNewPanel" Enabled="false">
                <ContentTemplate>
                    <div style="padding-top: 10px">
                    </div>
                    <div class="navbar nav-tabs" style=" border-style: solid; border-color: black; border-width: 2px; left: 0px; top: 0px; width: 1003px;">
                        <table class="auto-style5">
                            <tr>
                                <td style="padding-left: 50px" class="auto-style6"></td>
                                <td class="auto-style6">
                                    <asp:Button ID="Button5" runat="server" Text="Save" CssClass="btn btn-info" OnClick="OnOldNewSaveClick" ToolTip="Add new Items"/></td>
                                <td style="padding-left: 50px" class="auto-style6"></td>
                                <td class="auto-style6">
                                    <asp:Button ID="ChangeCPButton" runat="server" Text="Change CP" CssClass="btn btn-info" OnClick="OnChangeCPButtonClick" OnClientClick="target ='_self';" /></td>
                                
                                <td style="padding-left: 50px" class="auto-style6"></td>
                                
                                <td class="auto-style6">
                                    <asp:Button ID="DeleteBatchButton" runat="server" Text="Delete Batch" CssClass="btn btn-info" OnClick="OnDeleteBatchButtonClick" /></asp:button>
                                    </td>
                                <td style="padding-left: 50px" class="auto-style6"><td class="auto-style6">
                                    <asp:Button ID="Button6" runat="server" CssClass="btn btn-info" OnClick="OnClearButtonClick" Text="Clear" />
                                    </td>
                                    <td style="padding-left: 50px" class="auto-style6"></td>
                                    <td class="auto-style6">
                                        <asp:Button ID="Button7" runat="server" CssClass="btn btn-info" OnClick="OnPrintButtonClick" Text="Print" Width="110px" />
                                    </td>
                                    <td style="padding-left: 10px">
                                    <asp:CheckBox BorderStyle="Double" BackColor="LightYellow" CssClass="checkbox" runat="server" ID="PrintBatchLabelBox" AutoPostBack="True" Width="150px" Text="PrintLabels" OnCheckedChanged="OnChangeOldNewPrintBatchLabelClick"></asp:CheckBox>
                                </td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="nnavbar nav-tabs" style="border-style: solid; border-color: black; border-width: 2px; left: 0px; top: 0px; width: 1003px;">
                        <table class="auto-style5">
                            <tr>
                                                <td>
                                                    <asp:Label runat="server" Text="Item Number"></asp:Label>
                                                    <asp:TextBox runat="server" ID="ItemField" OnTextChanged="OnItemAddClick" Style="margin-left:10px;" CssClass="auto-style3"></asp:TextBox>
                                                    &nbsp;
                                                    <asp:ImageButton ID="AddItemButton" runat="server" ToolTip="Add Item" ImageUrl="~/Images/ajaxImages/addbar.png"
                                                        OnClick="OnItemAddClick" Height="37px" Width="106px" Style="margin-left:10px;" />
                                                    &nbsp;
                                                    <asp:Label runat="server" Text="New Order"></asp:Label>
                                                    <asp:TextBox runat="server" ID="OldNewNewOrder" Style="margin-left:10px;" ></asp:TextBox>
                                                    &nbsp;
                                                    <asp:Label runat="server" Text="Available items"></asp:Label>
                                                    <asp:TextBox runat="server" ID="ItemsAvailableBox" Style="width: 30px;margin-left:10px;" Enabled="False"></asp:TextBox>
                                                    &nbsp;
                                                    <asp:Label runat="server" Text="Items to add"></asp:Label>
                                                    <asp:TextBox runat="server" ID="ItemsToAdd" Style="width: 30px;margin-left:10px;" OnTextChanged="OnItemCountClick"><</asp:TextBox>
                                                    <asp:HiddenField ID="ItemsAvailable" runat="server" />
                                                    <asp:Label runat="server" ID="OldNewErrorMsg" Style="color: red; margin-left: 100px;"></asp:Label>
                                                </td>
                                               </tr>
                        </table>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel ID="NewMemo" runat="server" CssClass="form-inline">
                                        <table style="width: 1200px;">
                                            <tr>
                                                <td colspan="3" style="font-weight: bold"></td>
                                            </tr>
                                                
                                            
                                            </table>
                                        <table>
                                            <tr>
                                                <td style="width=300px;">
                                                    <asp:ListBox runat="server" ID="OrderListBox" Rows="30" CssClass="control-list" Width="110px" Style="border: solid 2px;" />
                                                    <asp:ImageButton ID="RemoveItemButton" runat="server" ToolTip="Remove selected Item"
                                                        ImageUrl="~/Images/ajaxImages/deletebar.png" OnClick="OnItemDelClick" Style="vertical-align: top" Height="43px" Width="119px" />
                                                    <asp:ImageButton ID="GoItemButton" runat="server" ToolTip="Copy Items"
                                                        ImageUrl="~/Images/ajaxImages/rightArrow36.png" OnClick="OnItemsCopyClick" AutoPostBack="True" Style="vertical-align: top; margin-left: 0px; margin-top:50px;" Width="33px" />
                                                </td>
                                                <td style="vertical-align: top">
                                                    <div id="DragAndDropButtons" runat="server">
                                                        <asp:Button ID="DragItems" runat="server" CssClass="btn btn-info" OnClick="OnDragButtonClick" Text="Drag" Width="110px" />
                                                        <asp:Button ID="DropItems" runat="server" CssClass="btn btn-info" OnClick="OnDropButtonClick" Text="Drop" Width="110px" Style="margin-left:20px;" />
                                                        <asp:Button ID="RollbackItems" runat="server" CssClass="btn btn-info" OnClick="OnRollbackButtonClick" Text="Rollback" Width="110px" Style="margin-left:20px;"/>
                                                    </div>                                                    
                                                    <asp:Panel ID="OldItemsTreePanel" runat="server" Style="vertical-align: top; width: 380px; border: solid 2px; margin-left: 0px;">
                                                        <asp:TreeView ID="OldItemsTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                                            onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="Large" ForeColor="Black"
                                                            Font-Bold="False" ExpandDepth="3" Width="380px">
                                                            <SelectedNodeStyle BackColor="#D7FFFF" />
                                                        </asp:TreeView>
                                                    </asp:Panel>
                                                </td>
                                                <td style="vertical-align: top">
                                                    <asp:ListBox runat="server" ID="DragedItems" Rows="10" Visible="False" />
                                                </td>
                                                <td style="vertical-align: top">
                                                    <asp:ListBox runat="server" ID="lstMovedItems" Rows="10" DataValueField="ID" AutoPostBack="True"
                                                        DataTextField="RequestId" Visible="False" Width="240px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>

                            </tr>


                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel runat="server" HeaderText="Itemizing" OnDemandMode="Once" ID="ItemizingPanel" Enabled="true">
                <ContentTemplate>
                    <div class="navbar nav-tabs">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-info" OnClick="OnSaveClick" ToolTip="Add new Items" /></td>
                                <td>
                                    <asp:Button ID="ClearButton" runat="server" Text="Clear" CssClass="btn btn-info" OnClick="OnClearClick" /></td>
                                <td>
                                    <asp:Button ID="RefreshButton" runat="server" Text="Refresh" CssClass="btn btn-info" OnClick="OnRefreshClick" /></td>
                                <td>
                                    <asp:Button ID="OldNumber" runat="server" Text="Old Number" CssClass="btn btn-info" OnClick="OnOldNumberClick" OnClientClick="target ='_self';" /></td>
                                <td>
                                    <asp:Button ID="PrintButton" runat="server" Text="Print" CssClass="btn btn-info" OnClick="OnPrintClick" Enabled="False" ToolTip="Print new batch numbers" /></td>
                                <td>
                                    <asp:Button ID="ReprintButton" runat="server" Text="Reprint Labels" CssClass="btn btn-info" OnClick="OnReprintClick" ToolTip="Print new batch numbers" /></td>
                                <td style="padding-left: 10px">
                                    <asp:CheckBox CssClass="checkbox" runat="server" ID="PrintBox" Checked="True" AutoPostBack="True" Width="120px" Text="Print Labels" OnCheckedChanged="OnPrintLabelsCheck"></asp:CheckBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="CustomerNameField"></asp:Label>
                                </td>
                            </tr>
                        </table>





                    </div>
                    <table>
                        
                        <tr>
                            <td></td>
                            <td style="padding-left: 20px;">
                                <asp:RadioButtonList ID="modeSelector" runat="server" AutoPostBack="True" CssClass="radio" Width="150px"
                                    RepeatDirection="Horizontal" OnSelectedIndexChanged="OnModeChanged">
                                    <asp:ListItem Value="0" Selected="True">BLK</asp:ListItem>
                                    <asp:ListItem Value="1">Lot List</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="auto-style7">
                                <div runat="server" id="ZenPrefixDiv" style="box-sizing: border-box; border:solid #969696; background-color:cornsilk">
                                <asp:CheckBox ID="ZenCheckBox" runat="server" CssClass="checkbox" AutoPostBack="True" Text="Zen Select" Width="110px" OnCheckedChanged="ZenBoxCheckedChanged"/>
                                <asp:Label runat="server" Text="Zen Prefix"></asp:Label>
                                <asp:TextBox ID="ZenPrefixText" runat="server" MaxLength="10" Width="70px" Enabled="False" ></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" Text="Customer Program"></asp:Label></td>
                            <td>
                                <asp:DropDownList runat="server" ID="CpListField" OnSelectedIndexChanged="OnCpChanged" AutoPostBack="True" DataTextField = "CustomerProgramName" DataValueField = "CpOfficeIdAndCpId" class="filtered-select"/>
                            </td>
                            <td style="padding-left: 20px">
                                <asp:Label ID="Label2" runat="server" Text="Lot Numbers" Visible="False"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" CssClass="form-inline" DefaultButton="AddLotButton" Visible="False">
                                    <asp:TextBox runat="server" ID="LotNumberField" Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="AddLotButton" runat="server" ToolTip="Add Lot" ImageUrl="~/Images/ajaxImages/add.png"
                                        OnClick="OnLotAddClick" Enabled="False" />
                                </asp:Panel>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td style="height: 100px; vertical-align: top">
                                <asp:Label runat="server" ID="Picture2PathField" Style="font-size: smaller" Enabled="False" Wrap="True"></asp:Label><br />
                                <asp:Image ID="itemPicture" runat="server" Width="100px" Visible="False"></asp:Image>
                                <br />
                            </td>
                            <td></td>
                            <td rowspan="3" style="vertical-align: top">
                                <asp:Panel ID="LotPanel" runat="server" Style="vertical-align: top" Visible="False">
                                    <asp:ListBox runat="server" ID="LotList" Rows="8" CssClass="control-list" Style="width: 88%; height: 100%" />
                                    <asp:ImageButton ID="DelImageButton" runat="server" ToolTip="Remove selected Lot"
                                        ImageUrl="~/Images/ajaxImages/del.png" OnClick="OnLotDelClick" Style="vertical-align: top" />
                                </asp:Panel>
                            </td>
                            <td rowspan="3" style="vertical-align: top">
                                <asp:Panel ID="ReprintPanel" runat="server" Style="vertical-align: top" Visible="False">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="ReprintItemNumber" runat="server" Text="Enter Batch/Item" Width="150px" ToolTip="Enter Batch/Item" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="ItemTextBox" Width="150px"></asp:TextBox>
                                                <asp:Label runat="server" ID="ErrPrintLbl" ForeColor="Red" ></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ReprintBatch" runat="server" Text="Reprint Batch" Width="150px" CssClass="btn btn-info" OnClick="OnReprintBatchClick" ToolTip="Reprint Batch" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ReprintItem" runat="server" Text="Reprint Item" Width="150px" CssClass="btn btn-info" OnClick="OnReprintItemClick" ToolTip="Reprint Item" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ReprintBatchItem" runat="server" Text="Reprint Batch/Items" Width="150px" CssClass="btn btn-info" OnClick="OnReprintBatchItemClick" ToolTip="Reprint Batch/Items" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label runat="server" Style="font-size: small" ID="ErrorPictureField" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Not Inspected<br />Items</td>
                            <td>
                                <asp:TextBox runat="server" ID="NotInspectedItems"></asp:TextBox></td>
                            <ajaxToolkit:FilteredTextBoxExtender ID="NotInspectedItemsTextBoxExtender" runat="server"
                                TargetControlID="NotInspectedItems" FilterType="Numbers" Enabled="True" />
                        </tr>
                        <tr>
                            <td>Inspected Items</td>
                            <td>
                                <asp:TextBox runat="server" ID="InspectedItems" OnClick="OnSaveInspectedClick" OnTextChanged="OnSaveInspectedClick"></asp:TextBox></td>
                            <ajaxToolkit:FilteredTextBoxExtender ID="InspectedItemsTextBoxExtender" runat="server"
                                TargetControlID="InspectedItems" FilterType="Numbers" Enabled="True" />
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Itemized Items</td>
                            <td>
                                <asp:TextBox runat="server" ID="itemizedItems"></asp:TextBox></td>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ItemizedItemsExtender" runat="server"
                                TargetControlID="ItemizedItems" FilterType="Numbers" Enabled="True" />
                        </tr>
                        <tr>
                            <td>Number of items
                                <br />
                                in the division</td>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" CssClass="form-inline" DefaultButton="AddBatchButton">
                                    <asp:TextBox runat="server" ID="CountItemsField" ></asp:TextBox>
                                    <asp:Button ID="AddBatchButton" runat="server" ToolTip="Add Batch" Text="Add New Batch"  CssClass="btn btn-info"
                                        OnClick="OnBatchAddClick" Enabled="False" />
                                </asp:Panel>
                            </td>
                            <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
                                TargetControlID="CountItemsField" FilterType="Numbers" Enabled="True" />
                        </tr>
                        <tr id="EditBatchesTr" runat="server" Visible="true">
                            <td colspan="4">
                                <asp:DataGrid ID="GridEditNewBatches" runat="server" CssClass="table" OnItemCommand="ItemsGrid_Command" AutoGenerateColumns="False" AllowingPaging="True">
                                    <Columns>
                                        <asp:ButtonColumn HeaderText="" ButtonType="LinkButton" Text="Delete" CommandName="Delete"/>  
                                        <asp:BoundColumn HeaderText="Items" ReadOnly="True" DataField="Items"/>
                                        <asp:BoundColumn HeaderText="Memo" ReadOnly="True" DataField="Memo"/>
                                        <asp:BoundColumn HeaderText="Sku" ReadOnly="True" DataField="Sku"/>
                                        <asp:BoundColumn HeaderText="ItemsSet" ReadOnly="True" DataField="ItemsSet"/>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="SavedBatchesTr" runat="server" Visible="true">
                            <td colspan="4">
                                <asp:DataGrid ID="GridNewBatches" runat="server" CssClass="table" AllowingPaging="True">
                                </asp:DataGrid>
                            </td>
                        </tr>

                    </table>

                    <asp:Panel runat="server" ID="LabelOldPanel" CssClass="modalPopup" Style="display: none; border: solid 2px #5377A9">
                        <asp:Panel ID="LabelOldDragHandle" runat="server" Style="cursor: move; background-color: white;">
                            <div style="color: #5377A9; font-weight: bold; text-align: center">
                                <asp:Label ID="LabelOldTitle" Text="Preview Labels" runat="server" />
                            </div>
                        </asp:Panel>
                        <div runat="server" id="LabelOldDiv" style="overflow-y: auto; overflow-x: hidden; height: 350px; width: 260px; text-align: left">
                            <asp:DataGrid runat="server" ID="LabelOldGrid" ShowHeader="False" AutoGenerateColumns="False"
                                DataKeyField="NewItemNumber" OnItemDataBound="OnPreviewLabelDataBound" GridLines="None" Style="text-align: left; max-width: 260px">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="LabelOldPrintPanel" Style="font-family: 'Arial Narrow'; font-weight: bold; font-size: 10px; color: black; text-align: center; background-color: white; height: 87.5pt; margin: 0pt; vertical-align: top">
                                                <asp:Panel ID="Panel6" runat="server" Height="24px">
                                                    <asp:Label runat="server" ID="Barcode" Style="font-family: 'Code 128'; font-size: 24px; font-weight: normal"
                                                        ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel7" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="BarcodeNum" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel8" runat="server" CssClass="form-inline" Height="14px">
                                                    <asp:Label runat="server" ID="IsPrinted"></asp:Label>
                                                    <asp:Label runat="server" ID="Value1" Style="padding-left: 10px"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel9" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value2_3" ForeColor="black"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel10" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value4"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel11" runat="server" Height="12px">
                                                    <asp:Label runat="server" ID="NewBatchNumber" Style="font-size: 10px"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div>
                        </div>
                        <div style="padding-top: 10px">
                            <p style="text-align: center;">
                                <asp:Button ID="PrintLblOldBtn" runat="server" CssClass="btn btn-small btn-default"
                                    OnClick="OnLabelOldPrintClick" OnClientClick="LabelOldPrint()" Text="Print" />
                                <asp:Button ID="CloseLblOldBtn" runat="server" CssClass="btn btn-small btn-default"
                                    Text="Close" />
                            </p>
                        </div>

                    </asp:Panel>
                    <asp:Button ID="HiddenLblOldPrintBtn" runat="server" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblOldPrintBtn" PopupControlID="LabelOldPanel" ID="LabelOldPopupExtender"
                        PopupDragHandleControlID="LabelOldDragHandle" OkControlID="CloseLblOldBtn" DynamicServicePath="" Enabled="True">
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Panel runat="server" ID="LabelOld2Panel" CssClass="modalPopup" Style="display: none; border: solid 2px #5377A9" Width="600px">
                        <asp:Panel ID="LabelOld2DragHandle" runat="server" Style="cursor: move; background-color: white;">
                            <div style="color: #5377A9; font-weight: bold; text-align: center">
                                <asp:Label ID="LabelOld2Title" Text="Preview Labels" runat="server" />
                            </div>
                        </asp:Panel>
                        <div runat="server" id="LabelOld2Div" style="overflow-y: auto; overflow-x: hidden; height: 350px; width: 600px; text-align: left">
                            <asp:DataGrid runat="server" ID="LabelOld2Grid" ShowHeader="False" AutoGenerateColumns="False"
                                DataKeyField="UniqueKey" OnItemDataBound="OnPreviewLabelDataBound2" GridLines="None" Width="550px">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="LabelOldPrintPanel_l" Style="font-family: 'Arial Narrow'; font-weight: bold; font-size: 10px; color: black; text-align: center; background-color: white; height: 87.5pt; margin: 0pt; vertical-align: top">
                                                <asp:Panel ID="Panel6_l" runat="server" Height="24px">
                                                    <asp:Label runat="server" ID="Barcode_l" Style="font-family: 'Code 128'; font-size: 24px; font-weight: normal"
                                                        ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel7_l" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="BarcodeNum_l" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel8_l" runat="server" CssClass="form-inline" Height="14px">
                                                    <asp:Label runat="server" ID="IsPrinted_l"></asp:Label>
                                                    <asp:Label runat="server" ID="Value1_l" Style="padding-left: 10px"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel9_l" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value2_3_l" ForeColor="black"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel10_l" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value4_l"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel11_l" runat="server" Height="12px">
                                                    <asp:Label runat="server" ID="NewBatchNumber_l" Style="font-size: 10px"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="LabelOldPrintPanel_r" Style="font-family: 'Arial Narrow'; font-weight: bold; font-size: 10px; color: black; text-align: center; background-color: white; height: 87.5pt; margin: 0pt; vertical-align: top">
                                                <asp:Panel ID="Panel6" runat="server" Height="24px">
                                                    <asp:Label runat="server" ID="Barcode_r" Style="font-family: 'Code 128'; font-size: 24px; font-weight: normal"
                                                        ForeColor="Black" BackColor="White" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel7_r" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="BarcodeNum_r" CssClass=""></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel8_r" runat="server" CssClass="form-inline" Height="14px">
                                                    <asp:Label runat="server" ID="IsPrinted_r"></asp:Label>
                                                    <asp:Label runat="server" ID="Value1_r" Style="padding-left: 10px"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel9_r" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value2_3_r" ForeColor="black"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel10_r" runat="server" Height="14px">
                                                    <asp:Label runat="server" ID="Value4_r"></asp:Label>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel11_r" runat="server" Height="12px">
                                                    <asp:Label runat="server" ID="NewBatchNumber_r" Style="font-size: 10px"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div>
                        </div>
                        <div style="padding-top: 10px">
                            <p style="text-align: center;">
                                <asp:Button ID="PrintLblOld2Btn" runat="server" CssClass="btn btn-small btn-default"
                                    OnClick="OnLabelOld2PrintClick" OnClientClick="LabelOld2Print()" Text="Print" />
                                <asp:Button ID="CloseLblOld2Btn" runat="server" CssClass="btn btn-small btn-default"
                                    Text="Close" />
                            </p>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
        <asp:Button ID="HiddenLblOld2PrintBtn" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="HiddenLblOld2PrintBtn" PopupControlID="LabelOld2Panel" ID="LabelOld2PopupExtender"
            PopupDragHandleControlID="LabelOld2DragHandle" OkControlID="CloseLblOld2Btn">
        </ajaxToolkit:ModalPopupExtender>

    </div>

</asp:Content>


