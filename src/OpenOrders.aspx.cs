﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class OpenOrders : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!IsPostBack)
            {
                LoadCustomers();
                CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
                CustomerLike.Focus();
            }
        }
        #region Customers
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this, true);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {

            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        #endregion
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
        }

        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            var result = QueryUtils.GetOpenOrders(CalendarExtenderFrom.SelectedDate,
                CalendarExtenderTo.SelectedDate,
                string.IsNullOrEmpty(lstCustomerList.SelectedValue) ? 0 : Int32.Parse("" + lstCustomerList.SelectedValue), this);
            var dt = new DataTable("Orders");
            dt.Columns.Add(new DataColumn("Order", typeof(Int32)));
            dt.Columns.Add(new DataColumn("Create Date", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Not inspected", typeof(Int32)));
            dt.Columns.Add(new DataColumn("Memo", typeof(string)));
            dt.Columns.Add(new DataColumn("Customer Name", typeof(string)));
            dt.AcceptChanges();
            foreach (var orderModel in result)
            {
                dt.Rows.Add(new object[]
                {
                    orderModel.OrderCode, 
                    orderModel.CreateDate, 
                    orderModel.NotInspectedQuantity,
                    orderModel.Memo, 
                    orderModel.CustomerName
                });
            }
            dt.AcceptChanges();
            SetViewState(dt, SessionConstants.OpenOrdersList);
            UpdateDataView(dt);
            lblNumberOfItems.Text = dt.Rows.Count == 0 ? "" : string.Format("Total number of orders {0}", dt.Rows.Count);

        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);

            UpdateDataView(GetViewState(SessionConstants.OpenOrdersList) as DataTable);
        }
        void UpdateDataView(DataTable table)
        {
            if (table == null)
            {
                grdOrders.DataSource = null;
                grdOrders.DataBind();
                return;
            }
            var dv = new DataView { Table = table };
            if (grdOrders.AllowSorting)
            {
                // Apply sort information to the view
                var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
                if (sortModel != null)
                {
                    dv.Sort = sortModel.ColumnName;
                    if (!sortModel.Direction) dv.Sort += " DESC";
                }
                else
                {
                    dv.Sort = "Order";
                    SetViewState(new SortingModel { ColumnName = "Order", Direction = true }, SessionConstants.CurrentSortModel);
                }
            }

            // Rebind data 
            grdOrders.DataSource = dv;
            grdOrders.DataBind();
        }
    }
}