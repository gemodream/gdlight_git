﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Retailer.aspx.cs" Inherits="Corpt.Retailer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        .GridPager a, .GridPager span {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }

        .divCol {
            float: left;
            width: 290px;
        }

        .divBottom {
            clear: both;
        }

        .auto-style1 {
            //float: right;
            width: 400px;
            //height: 521px;
            //height: 800px;
        }

        .auto-style2 {
            font-size: 14px;
            color: #FFFFFF;
            vertical-align: middle;
            background-color: #0044CC;
            background-repeat: repeat-x;
        }

        .text-style {
            padding-bottom: 3px;
            padding-top: 1px;
            padding-left: 3px;
            padding-right: 3px;
            margin-left: 10px;
            margin-bottom: 2px;
        }

        .btnfront {
            font: 'Forgotten Futurist';
            width: 110px;
            font-size: 15px;
            padding-top: 4px;
            padding-bottom: 4px;
        }
    </style>

    <div class="demoarea" style="text-align: center;">
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">
                <div style="left: 460px; position: absolute; top: 60px">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                        <ProgressTemplate>
                            <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                            <b>Please, wait....</b>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <asp:UpdatePanel runat="server" ID="EnteredValuesPanel">
                    <ContentTemplate>
                        <div class="auto-style1" style="text-align: left;">
                            <h2 style="text-align: left; margin-bottom: 0px; margin-top: 0px" class="demoheading">Customers Retailer
                            </h2>
                            <table class="form-group" style="margin-bottom: 7px; width: 860px;" border="0">
                                <tr>
                                    <td>
                                        <label class="control-label" for="txtCustomerName" style="float: left; margin-top: 2px; margin-bottom: 0px;">
                                            Customer Name:</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCutomer" runat="server" DataTextField="CustomerName"
                                            AutoPostBack="True" DataValueField="CustomerCode" CssClass="text-style" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                            ToolTip="Customers List" Width="300px" Height="30px">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*" ErrorMessage="Please Select Customer" ValidationGroup="vgRetailerToCustomer" ControlToValidate="ddlCutomer" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtCustomerCode1" class="form-control form-control-height text-style" Style="width: 90px; margin-top: 8px; margin-left: 0px;" runat="server" OnClick="OnCustCodeClick" OnTextChanged="OnCustCodeClick"
                                            placeholder="CustomerCode" autocomplete="off" CausesValidation="false" AutoPostBack="true"></asp:TextBox>

                                        <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                            ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustCodeClick" CausesValidation="false" />
                                    </td>
                                    <td rowspan="4" style="vertical-align: top; padding-left: 10px;width:280px;" >
										<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vgRetailerToCustomer" HeaderText="You must enter a value in the following fields:" />
                                     

                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label class="control-label" for="txtStyle" style="float: left; margin-top: 4px;">
                                            Select Retailer:</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRetailer" runat="server" DataTextField="RetailerName" DataValueField="RetailerId"
                                            Width="180px" Height="30px" CssClass="text-style">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="vgRetailerToCustomer" runat="server" Text="*" ErrorMessage="Please Select Retailer" ControlToValidate="ddlRetailer" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>

                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Blue" Style="margin-left: 10px;"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="AddRetailerToCustomer" runat="server" class="btn btn-info btn-small" ValidationGroup="vgRetailerToCustomer" Style="font: 'Forgotten Futurist'; width: auto; font-size: 15px; vertical-align: middle; margin-left: 10px; margin-top: 10px;"
                                            Text="Add Retailer To Customer" OnClick="AddRetailerToCustomer_Click" />
                                        <asp:Button ID="btnClearAll" runat="server" class="btn btn-info btn-small" Text="Clear" OnClick="btnClear_Click" Style="margin-left: 0px; font: 'Forgotten Futurist'; font-size: 15px; margin-top: 10px;"
                                            CausesValidation="false" />

                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding-left: 0px; vertical-align: top; font-size: 14px;" colspan="2">
                                        <br />
                                        <asp:Label runat="server" ID="EnteredLabel" Text="Customers Retailer:" Visible="False"></asp:Label>


                                        <asp:DataGrid runat="server" ID="EnteredGrid" AutoGenerateColumns="False" CellPadding="5"
                                            CellSpacing="5" OnItemCommand="OnDelCommand">
                                            <Columns>
                                                <asp:TemplateColumn>
                                                    <HeaderTemplate>Delete</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this Retailer?');">Delete </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="RetailerId" HeaderText="Retailer" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RetailerName" HeaderText="Retailer"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Names"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="CustomerCode" HeaderText="Customer Codes"></asp:BoundColumn>

                                            </Columns>

                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                            <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                        </asp:DataGrid>
                                        <div style="border: black solid 1px !important; width: 345px;" id="lblNoRecord" runat="server" visible="false">
                                            <asp:Label runat="server" ID="lblNoRecord1" Text="No records found." Style="padding-left: 100px;"></asp:Label>
                                        </div>



                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-right: 7px;" colspan="2">
                                        <div class="form-group" style="margin-bottom: 2px;">
                                        </div>
                                    </td>

                                    <td style="padding-right: 7px;">
                                        <div class="form-group" style="margin-bottom: 2px;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%-- Information Dialog --%>
                        <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none; border: solid 2px lightgray;">
                            <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <div>
                                    <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                    <b>Information</b>

                                </div>
                            </asp:Panel>
                            <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                            </div>
                            <div style="padding-top: 10px">
                                <p style="text-align: center; font-family: sans-serif">
                                    <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                            PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton">
                        </ajaxToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
