using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for CPRulesDisplay.
	/// </summary>
	public partial class CPRulesDisplay : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataList DataList1;
		protected System.Data.DataTable dt;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
#if DEBUG
			LinkButton4.Visible=true;
			LinkButton4.Enabled=true;
#endif

			if((Utils.NullValue(Request.QueryString["Row"])!="")&&(Session["cpList"]!=null))
			{
				//Set customerlist and cp list and cal show
				DataSet ds = Session["cpList"] as DataSet;
				ShowProgram(ds.Tables[0].Rows[Int32.Parse(Request.QueryString["Row"].ToString())]["customerprogramname"].ToString(),Int32.Parse(ds.Tables[0].Rows[Int32.Parse(Request.QueryString["Row"].ToString())]["CustomerOfficeID_CustomerID"].ToString().Split('_')[0]),Int32.Parse(ds.Tables[0].Rows[Int32.Parse(Request.QueryString["Row"].ToString())]["CustomerOfficeID_CustomerID"].ToString().Split('_')[1]),ds.Tables[0].Rows[Int32.Parse(Request.QueryString["Row"].ToString())]["CPOfficeID_CPID"].ToString());
				Session["cpList"]=null;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ActivateButton_Click(object sender, System.EventArgs e)
		{
			lstCustomerList.Items.Clear();
			lstProgramList.Items.Clear();
			itemPicture.ImageUrl="";
			tblCPRules.Visible=false;
			lblComments.Text="";
			lblDescription.Text="";
			SRP.Text="";
			CustomerStyle.Text="";
			CustomerID.Text="";

			tblDocList.Visible=false;
			string sFullItemNumber = "";

			if (!Regex.IsMatch(txtBatchNumber.Text.Trim(), @"^\d{8}$|^\d{9}$"))
			{
				InfoLabel.Text= "Invalid Batch Number";
				lblCustomerProgramName.Text="";
				lblCustomerProgramName.Visible=false;
				return;
			}
			else
			{
				sFullItemNumber = txtBatchNumber.Text.Trim() + "00";
				tblCPRules.Visible=false;
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				conn.Open();
			
				SqlCommand command = new SqlCommand("sp_GetCPRules");
				
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
			
				command.Parameters.Add("@OrderCode", Utils.ParseOrderCode(sFullItemNumber).ToString());  //    txtBatchNumber.Text.Substring(0,5));
				command.Parameters.Add("@BatchCode", Utils.ParseBatchCode(sFullItemNumber).ToString());  //    txtBatchNumber.Text.Substring(5));

//				InfoLabel.Text=txtBatchNumber.Text.Substring(0,5) + " " + txtBatchNumber.Text.Substring(5);

				SqlDataReader reader = command.ExecuteReader();

				if(reader.HasRows)
				{
				//	InfoLabel.Text="";
					
					DataView dataView = new DataView();
					String prevCPDocID = "";
					String currCPDocID = "";
					String partName = "";
					String measureName = "";
					object minValue = "";
					object maxValue = "";
					String customerProgramName = "";
					String customerName = "";
					
					// Custom DataTable Thing:
					DataTable dataTable = new DataTable("CP Rules");
					// Declare variables for DataColumn and DataRow objects.
					DataColumn dataColumn;
					DataRow dataRow;
					DataRow blank;

					// Create new DataColumn, set DataType, ColumnName and add to DataTable.    
					dataColumn = new DataColumn();
					dataColumn.ColumnName = "Name";
					// Add the Column to the DataColumnCollection.
					dataTable.Columns.Add(dataColumn);

					dataColumn = new DataColumn();
					dataColumn.ColumnName = "Measure";
					// Add the Column to the DataColumnCollection.
					dataTable.Columns.Add(dataColumn);

					dataColumn = new DataColumn();
					dataColumn.ColumnName = "Mimimun Value";
					// Add the Column to the DataColumnCollection.
					dataTable.Columns.Add(dataColumn);

					dataColumn = new DataColumn();
					dataColumn.ColumnName = "Maximum Value";
					// Add the Column to the DataColumnCollection.
					dataTable.Columns.Add(dataColumn);


					// Add the new DataTable to the DataSet.
					//dataView.Table = dataTable;

					dataRow = dataTable.NewRow();
					
					dataRow["Name"]	= "Rule:";
					dataRow["Measure"]	= "";
					dataRow["Mimimun Value"] = "";
					dataRow["Maximum Value"] = "";
					dataTable.Rows.Add(dataRow);

					while(reader.Read())
					{
						dataRow = dataTable.NewRow();
						itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString().Replace(@"\",@"/");
                        partName = reader.GetString(reader.GetOrdinal("PartName"));
						measureName = reader.GetString(reader.GetOrdinal("MeasureName"));
						if(Convert.IsDBNull(reader.GetValue(reader.GetOrdinal("MinName")))) minValue = "";
						else minValue = reader.GetValue(reader.GetOrdinal("MinName"));//reader.GetString(reader.GetOrdinal("MinName"));
						if(Convert.IsDBNull(reader.GetValue(reader.GetOrdinal("MaxName")))) maxValue = "";
						else maxValue = reader.GetValue(reader.GetOrdinal("MaxName"));
						currCPDocID = reader.GetSqlValue(reader.GetOrdinal("CPDocID")).ToString();
						customerProgramName = reader.GetString(reader.GetOrdinal("customerprogramname"));
						customerName = reader.GetSqlValue(reader.GetOrdinal("customername")).ToString();
						Session["CustomerID"]=reader.GetSqlValue(reader.GetOrdinal("customerid")).ToString();
						Session["CustomerOfficeID"]=reader.GetSqlValue(reader.GetOrdinal("customerofficeid")).ToString();
						Session["CPID"]=reader.GetSqlValue(reader.GetOrdinal("CPID")).ToString();
						lblComments.Text="Comments: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("comment")).ToString());
						lblDescription.Text="Description: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("description")).ToString());

						if(prevCPDocID=="")
							prevCPDocID=currCPDocID;

						if(prevCPDocID!=currCPDocID)
						{
							blank = dataTable.NewRow();
							blank["Name"]	= "Rule:";
							blank["Measure"] = "";
							blank["Mimimun Value"] = "";
							blank["Maximum Value"] = "";
							dataTable.Rows.Add(blank);
							prevCPDocID=currCPDocID;
						}

						partName=Utils.CleanPartName(partName);
						dataRow["Name"]	= partName;
						dataRow["Measure"] = measureName;
						dataRow["Mimimun Value"] = minValue;
						dataRow["Maximum Value"] = maxValue;
						//dataView.Table = dataTable;
						dataTable.Rows.Add(dataRow);

						dataRow = dataTable.NewRow();
					}

					tblCPRules.DataSource = dataTable;
					tblCPRules.DataBind();
					tblCPRules.Visible=true;

					lblCustomerProgramName.Text="Customer: " + customerName+ "<br>Customer Program: " + customerProgramName;
					lblCustomerProgramName.Visible=true;


					conn.Close();

					//pull document for batch
					conn.Open();
					command = new SqlCommand("oleg_sp_GetDocList");
					command.Connection=conn;
					command.CommandType=CommandType.StoredProcedure;
			
					command.Parameters.Add("@CustomerID", Session["CustomerID"].ToString());
					command.Parameters.Add("@CustomerOfficeID", Session["CustomerOfficeID"].ToString());
					command.Parameters.Add("@CustomerProgramName",customerProgramName);
			
					//WrongInfoLabel.Text=Session["CustomerID"].ToString() + " " + Session["CustomerOfficeID"].ToString() + " " + customerProgramName;
					reader=command.ExecuteReader();

					//datatable for the document list

					DataTable myDocList;
					myDocList = new DataTable("Documents");
					dataColumn = new DataColumn();
					dataColumn.ColumnName = "Document";
					myDocList.Columns.Add(dataColumn);

					if(reader.HasRows)
					{
						while(reader.Read())
						{
							dataRow = myDocList.NewRow();
//							dataRow["Document"]="<A HREF=\"ShowReportStructure.aspx?CPOffice_CPID=" + Session["CustomerOfficeID"].ToString()+ "_" + Session["CPID"].ToString() + "&documentID=" + reader.GetSqlValue(reader.GetOrdinal("DocumentID")) + "\">"  + reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString() + "</a>"; 
							dataRow["Document"]=reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString();
							myDocList.Rows.Add(dataRow);
						}
						tblDocList.DataSource=myDocList;
						tblDocList.DataBind();
						tblDocList.Visible=true;
					}
					conn.Close();
	
				}
				else
				{
					InfoLabel.Text = "Could not find CP Rules for the batch, please check the batch nuber and try again.";
					tblCPRules.Visible=false;
					conn.Close();
				}
			}
			pnlResult.Visible=true;
		}

		protected void lstCustomerList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			tblDocList.Visible=false;
			lstProgramList.Items.Clear();
			String customerOfficeID;
			String customerID;
			itemPicture.ImageUrl="";
			tblCPRules.Visible=false;
			lblComments.Text="";
			lblDescription.Text="";
			SRP.Text="";
			CustomerStyle.Text="";
			CustomerID.Text="";


			customerOfficeID = lstCustomerList.Items[lstCustomerList.SelectedIndex].Value.Split('_')[0];
			customerID = lstCustomerList.Items[lstCustomerList.SelectedIndex].Value.Split('_')[1];

			if(Session["CustomerID"]==null)
				Session["CustomerID"]=customerID;
			else
				if(Session["CustomerID"].ToString()==customerID)
					return;
				else
					Session["CustomerID"]=customerID;

			Session["CustomerOfficeID"]=customerOfficeID;

			Int32 intCustomerOfficeID = Int32.Parse(customerOfficeID.Trim());
			Int32 intCustomerID = Int32.Parse(customerID.Trim());

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
			
			SqlCommand command = new SqlCommand("spGetCustomerProgramsPerCustomer");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			customerOfficeID = lstCustomerList.SelectedValue;
			customerID = lstCustomerList.SelectedValue;

			command.Parameters.Add("@CustomerOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@CustomerID", intCustomerID);
			command.Parameters.Add("@VendorOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@VendorID", intCustomerID);
			
			command.Parameters.Add("@AuthorID", Session["ID"].ToString().Trim());
			command.Parameters.Add("@AuthorOfficeID", Session["AuthorOfficeID"].ToString().Trim());
				
			SqlDataReader reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				lstProgramList.DataSource=reader;
				lstProgramList.DataTextField="CustomerProgramName";
				lstProgramList.DataValueField="CPOfficeID_CPID";
				lstProgramList.DataBind();
			}

			conn.Close();
		}

		protected void lstProgramList_SelectedIndexChanged(object sender, System.EventArgs e)
		{//this is where we start loading cuaatomer program
			InfoLabel.Text=lstProgramList.SelectedValue+ " " + lstProgramList.SelectedItem.Text;
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
			

			Int32 intCustomerOfficeID = Int32.Parse(Session["CustomerOfficeID"].ToString());
			Int32 intCustomerID = Int32.Parse(Session["CustomerID"].ToString());

			SqlCommand command = new SqlCommand("spGetCustomerProgramByNameAndCustomer");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			String customerOfficeID = lstCustomerList.SelectedValue;
			String customerID = lstCustomerList.SelectedValue;

			command.Parameters.Add("@CustomerProgramName", lstProgramList.SelectedItem.Text);

			command.Parameters.Add("@CustomerOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@CustomerID", intCustomerID);
			command.Parameters.Add("@VendorOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@VendorID", intCustomerID);
			
			command.Parameters.Add("@AuthorID", Session["ID"].ToString().Trim());
			command.Parameters.Add("@AuthorOfficeID", Session["AuthorOfficeID"].ToString().Trim());
				
			SqlDataReader reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				while(reader.Read())
				{
					SRP.Text="SRP: " + reader.GetSqlValue(reader.GetOrdinal("SRP")).ToString();
					if(SRP.Text.EndsWith("Null"))
						SRP.Text="";
                    CustomerStyle.Text="Customer Style: " + reader.GetSqlValue(reader.GetOrdinal("CustomerStyle")).ToString();
					if(CustomerStyle.Text.EndsWith("Null"))
						CustomerStyle.Text="";
					CustomerID.Text="Customer ID: " + reader.GetSqlValue(reader.GetOrdinal("CPPropertyCustomerID")).ToString();
					if(CustomerID.Text.EndsWith("Null"))
						CustomerID.Text="";
					lblComments.Text="Comments: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("Description")).ToString());
					lblDescription.Text="Description: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("Comment")).ToString());
					itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString().Replace(@"\",@"/");
				}
			}

			conn.Close();

			tblCPRules.Visible=false;
			
			conn.Open();
			
			command = new SqlCommand("sp_GetCPRulesByCPOfficeID_CPID");
				
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@CPOfficeID_CPID", lstProgramList.SelectedValue);

			command.CommandTimeout = 0;

			reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				InfoLabel.Text="";
					
				DataView dataView = new DataView();
				String prevCPDocID = "";
				String currCPDocID = "";
				String partName = "";
				String measureName = "";
				String minValue = "";
				String maxValue = "";
				String customerProgramName = "";
					
				// Custom DataTable Thing:
				DataTable dataTable = new DataTable("CP Rules");
				// Declare variables for DataColumn and DataRow objects.
				DataColumn dataColumn;
				DataRow dataRow;
				DataRow blank;

				// Create new DataColumn, set DataType, ColumnName and add to DataTable.    
				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Name";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Measure";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Mimimun Value";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Maximum Value";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);


				// Add the new DataTable to the DataSet.
				dataView.Table = dataTable;

				dataRow = dataTable.NewRow();
					
				dataRow["Name"]	= "Rule:";
				dataRow["Measure"]	= "";
				dataRow["Mimimun Value"] = "";
				dataRow["Maximum Value"] = "";
				dataTable.Rows.Add(dataRow);

				while(reader.Read())
				{
					dataRow = dataTable.NewRow();
//					partName = reader.GetString(reader.GetOrdinal("PartName"));
//					measureName = reader.GetString(reader.GetOrdinal("MeasureName"));
//					minValue = reader.GetString(reader.GetOrdinal("MinName"));
//					maxValue = reader.GetString(reader.GetOrdinal("MaxName"));
//					currCPDocID = reader.GetSqlValue(reader.GetOrdinal("CPDocID")).ToString();
//					customerProgramName = reader.GetString(reader.GetOrdinal("customerprogramname"));

					partName = Utils.NullValue(reader["PartName"]);
					measureName = Utils.NullValue(reader["MeasureName"]);
					minValue = Utils.NullValue(reader["MinName"]);
					maxValue = Utils.NullValue(reader["MaxName"]);
					currCPDocID = Utils.NullValue(reader["CPDocID"]);
					customerProgramName = Utils.NullValue(reader["customerprogramname"]);

					if(prevCPDocID=="")
						prevCPDocID=currCPDocID;

					if(prevCPDocID!=currCPDocID)
					{
						blank = dataTable.NewRow();
						blank["Name"]	= "Rule:";
						blank["Measure"]	= "";
						blank["Mimimun Value"] = "";
						blank["Maximum Value"] = "";
						dataTable.Rows.Add(blank);
						prevCPDocID=currCPDocID;
					}
                    
					partName=Utils.CleanPartName(partName);
					dataRow["Name"]	= partName;
					dataRow["Measure"]	= measureName;
					dataRow["Mimimun Value"] = minValue;
					dataRow["Maximum Value"] = maxValue;
					dataTable.Rows.Add(dataRow);

				}

				tblCPRules.DataSource = dataView;
				tblCPRules.DataBind();
				tblCPRules.Visible=true;

				lblCustomerProgramName.Text="Customer Program: " + customerProgramName;
				lblCustomerProgramName.Visible=true;


				conn.Close();
		// pull the doclist
				conn.Open();
				command = new SqlCommand("oleg_sp_GetDocList");
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
			
				command.Parameters.Add("@CustomerID", Session["CustomerID"].ToString());
				command.Parameters.Add("@CustomerOfficeID", Session["CustomerOfficeID"].ToString());
				command.Parameters.Add("@CustomerProgramName",customerProgramName);
			
				//WrongInfoLabel.Text=Session["CustomerID"].ToString() + " " + Session["CustomerOfficeID"].ToString() + " " + customerProgramName;
				reader=command.ExecuteReader();

				//datatable for the document list

				DataTable myDocList;
				myDocList = new DataTable("Documents");
				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Document";
				myDocList.Columns.Add(dataColumn);

				if(reader.HasRows)
				{
					while(reader.Read())
					{
						dataRow = myDocList.NewRow();
						//dataRow["Document"]="<A HREF=\"ShowReportStructure.aspx?CPOffice_CPID=" + lstProgramList.SelectedValue + "&documentID=" + reader.GetSqlValue(reader.GetOrdinal("DocumentID")) + "\">"  + reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString() + "</a>"; 
						dataRow["Document"]=reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString();
						myDocList.Rows.Add(dataRow);
					}
					tblDocList.DataSource=myDocList;
					tblDocList.DataBind();
					tblDocList.Visible=true;
				}
				conn.Close();
				pnlResult.Visible=true;
			}
		}

		private void ShowProgram(String cpName, int customerOfficeID, int customerID, String cpOffice_CPID)
		{
//			InfoLabel.Text=lstProgramList.SelectedValue+ " " + lstProgramList.SelectedItem.Text;
			InfoLabel.Text=lstProgramList.SelectedValue+ " " + cpName;
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
			

//			Int32 intCustomerOfficeID = Int32.Parse(Session["CustomerOfficeID"].ToString());
//			Int32 intCustomerID = Int32.Parse(Session["CustomerID"].ToString());

			int intCustomerOfficeID = customerOfficeID;
			int intCustomerID = customerID;


			SqlCommand command = new SqlCommand("spGetCustomerProgramByNameAndCustomer");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
//			String customerOfficeID = lstCustomerList.SelectedValue;
//			String customerID = lstCustomerList.SelectedValue;

//			command.Parameters.Add("@CustomerProgramName", lstProgramList.SelectedItem.Text);
			command.Parameters.Add("@CustomerProgramName", cpName);

			command.Parameters.Add("@CustomerOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@CustomerID", intCustomerID);
			command.Parameters.Add("@VendorOfficeID", intCustomerOfficeID);
			command.Parameters.Add("@VendorID", intCustomerID);
			
			command.Parameters.Add("@AuthorID", Session["ID"].ToString().Trim());
			command.Parameters.Add("@AuthorOfficeID", Session["AuthorOfficeID"].ToString().Trim());
				
			SqlDataReader reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				while(reader.Read())
				{
					SRP.Text="SRP: " + reader.GetSqlValue(reader.GetOrdinal("SRP")).ToString();
					if(SRP.Text.EndsWith("Null"))
						SRP.Text="";
					CustomerStyle.Text="Customer Style: " + reader.GetSqlValue(reader.GetOrdinal("CustomerStyle")).ToString();
					if(CustomerStyle.Text.EndsWith("Null"))
						CustomerStyle.Text="";
					CustomerID.Text="Customer ID: " + reader.GetSqlValue(reader.GetOrdinal("CPPropertyCustomerID")).ToString();
					if(CustomerID.Text.EndsWith("Null"))
						CustomerID.Text="";
					lblComments.Text="Comments: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("Description")).ToString());
					lblDescription.Text="Description: "+Utils.CleanCD(reader.GetSqlValue(reader.GetOrdinal("Comment")).ToString());
					itemPicture.ImageUrl="https://www.gemscience.net/trackingNET/batchpicture/" + reader.GetSqlValue(reader.GetOrdinal("Path2Picture")).ToString().Replace(@"\",@"/");
				}
			}

			conn.Close();

			tblCPRules.Visible=false;
			
			conn.Open();
			
			command = new SqlCommand("sp_GetCPRulesByCPOfficeID_CPID");
				
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@CPOfficeID_CPID", cpOffice_CPID);

			reader = command.ExecuteReader();

			if(reader.HasRows)
			{
				InfoLabel.Text="";
					
				DataView dataView = new DataView();
				String prevCPDocID = "";
				String currCPDocID = "";
				String partName = "";
				String measureName = "";
				String minValue = "";
				String maxValue = "";
				String customerProgramName = "";
					
				// Custom DataTable Thing:
				DataTable dataTable = new DataTable("CP Rules");
				// Declare variables for DataColumn and DataRow objects.
				DataColumn dataColumn;
				DataRow dataRow;
				DataRow blank;

				// Create new DataColumn, set DataType, ColumnName and add to DataTable.    
				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Name";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Measure";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Mimimun Value";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);

				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Maximum Value";
				// Add the Column to the DataColumnCollection.
				dataTable.Columns.Add(dataColumn);


				// Add the new DataTable to the DataSet.
				dataView.Table = dataTable;

				dataRow = dataTable.NewRow();
					
				dataRow["Name"]	= "Rule:";
				dataRow["Measure"]	= "";
				dataRow["Mimimun Value"] = "";
				dataRow["Maximum Value"] = "";
				dataTable.Rows.Add(dataRow);

				while(reader.Read())
				{
					dataRow = dataTable.NewRow();
					partName = reader.GetString(reader.GetOrdinal("PartName"));
					measureName = reader.GetString(reader.GetOrdinal("MeasureName"));
					minValue = reader.GetString(reader.GetOrdinal("MinName"));
					maxValue = reader.GetString(reader.GetOrdinal("MaxName"));
					currCPDocID = reader.GetSqlValue(reader.GetOrdinal("CPDocID")).ToString();
					customerProgramName = reader.GetString(reader.GetOrdinal("customerprogramname"));

					if(prevCPDocID=="")
						prevCPDocID=currCPDocID;

					if(prevCPDocID!=currCPDocID)
					{
						blank = dataTable.NewRow();
						blank["Name"]	= "Rule:";
						blank["Measure"]	= "";
						blank["Mimimun Value"] = "";
						blank["Maximum Value"] = "";
						dataTable.Rows.Add(blank);
						prevCPDocID=currCPDocID;
					}
                    
					partName=Utils.CleanPartName(partName);
					dataRow["Name"]	= partName;
					dataRow["Measure"]	= measureName;
					dataRow["Mimimun Value"] = minValue;
					dataRow["Maximum Value"] = maxValue;
					dataTable.Rows.Add(dataRow);

				}

				tblCPRules.DataSource = dataView;
				tblCPRules.DataBind();
				tblCPRules.Visible=true;

				lblCustomerProgramName.Text="Customer Program: " + customerProgramName;
				lblCustomerProgramName.Visible=true;


				conn.Close();
				// pull the doclist
				conn.Open();
				command = new SqlCommand("oleg_sp_GetDocList");
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
			
//				command.Parameters.Add("@CustomerID", Session["CustomerID"].ToString());
//				command.Parameters.Add("@CustomerOfficeID", Session["CustomerOfficeID"].ToString());
				command.Parameters.Add("@CustomerID", customerID);
				command.Parameters.Add("@CustomerOfficeID", customerOfficeID);

				command.Parameters.Add("@CustomerProgramName",customerProgramName);
			
				//WrongInfoLabel.Text=Session["CustomerID"].ToString() + " " + Session["CustomerOfficeID"].ToString() + " " + customerProgramName;
				reader=command.ExecuteReader();

				//datatable for the document list

				DataTable myDocList;
				myDocList = new DataTable("Documents");
				dataColumn = new DataColumn();
				dataColumn.ColumnName = "Document";
				myDocList.Columns.Add(dataColumn);

				if(reader.HasRows)
				{
					while(reader.Read())
					{
						dataRow = myDocList.NewRow();
						//dataRow["Document"]="<A HREF=\"ShowReportStructure.aspx?CPOffice_CPID=" + lstProgramList.SelectedValue + "&documentID=" + reader.GetSqlValue(reader.GetOrdinal("DocumentID")) + "\">"  + reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString() + "</a>"; 
						dataRow["Document"]=reader.GetSqlValue(reader.GetOrdinal("DocumentName")).ToString();
						myDocList.Rows.Add(dataRow);
					}
					tblDocList.DataSource=myDocList;
					tblDocList.DataBind();
					tblDocList.Visible=true;
				}
				conn.Close();
				pnlResult.Visible=true;
			}
		}


		protected void cmdListProgram_Click(object sender, System.EventArgs e)
		{
			lstProgramList_SelectedIndexChanged(sender, e);
		}

		protected void cmdLookupStyle_Click(object sender, System.EventArgs e)
		{
			if(txtStyleNumber.Text=="")
				return;

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();

			SqlCommand command = new SqlCommand("sp_GetSkuListByStyle2");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@CustomerStyle", "%" + txtStyleNumber.Text.Trim() + "%");
			command.Parameters.Add("@AuthorID", Session["ID"].ToString());

			DataSet cpList = new DataSet();
			SqlDataAdapter dataAdapter;

			dataAdapter = new SqlDataAdapter(command);

			if(dataAdapter.Fill(cpList)>0) //search returned some results.
			{
				lblStyleNotFound.Text="";
				DataTable customerSKU = new DataTable("Found SKU'S");
				customerSKU.Columns.Add(new DataColumn("Customer"));
				customerSKU.Columns.Add(new DataColumn("Customer Program"));
				customerSKU.Columns.Add(new DataColumn("Customer Style"));
				
				Session["cpList"]=cpList;

				int i=0;
				foreach(DataRow row in cpList.Tables[0].Rows)
				{
					DataRow rrow = customerSKU.NewRow();
					rrow["Customer"]="<a href='CPRulesDisplay.aspx?Row="+i+"'>"+row["CustomerName"].ToString()+"</a>";
					rrow["Customer Program"]="<a href='CPRulesDisplay.aspx?Row="+i+"'>"+row["customerprogramname"].ToString()+"</a>";
					rrow["Customer Style"]="<a href='CPRulesDisplay.aspx?Row="+i+++"'>"+row["customerstyle"].ToString()+"</a>";
					customerSKU.Rows.Add(rrow);
				}

				tblCustomerSKU.DataSource=customerSKU;
				tblCustomerSKU.DataBind();
				tblCustomerSKU.Visible=true;
				pnlResult.Visible=true;
			}
			else
			{
				lblStyleNotFound.Text="Style Not Found";
			}
			conn.Close();
		}

		protected void LinkButton1_Click(object sender, System.EventArgs e)
		{
			pnlFindByCustomer.Visible=true;
			pnlFindByBatch.Visible=false;
			pnlFindByStyle.Visible=false;
			pnlSKU.Visible=false;
			pnlResult.Visible=false;
			ReFillCustomerList();
			CleanUp();
		}

		protected void LinkButton2_Click(object sender, System.EventArgs e)
		{
			pnlFindByCustomer.Visible=false;
			pnlFindByBatch.Visible=true;
			pnlFindByStyle.Visible=false;
			pnlSKU.Visible=false;
			pnlResult.Visible=false;
			CleanUp();
		}

		protected void LinkButton3_Click(object sender, System.EventArgs e)
		{
			pnlFindByCustomer.Visible=false;
			pnlFindByBatch.Visible=false;
			pnlFindByStyle.Visible=true;
			pnlSKU.Visible=false;
			pnlResult.Visible=false;
			CleanUp();
		}

		protected void LinkButton4_Click(object sender, System.EventArgs e)
		{
			pnlFindByCustomer.Visible=false;
			pnlFindByBatch.Visible=false;
			pnlFindByStyle.Visible=false;
			pnlSKU.Visible=true;
			pnlResult.Visible=false;
			CleanUp();		
		}

		private void CleanUp()
		{
			itemPicture.ImageUrl="";
			lblCustomerProgramName.Text="";
			CustomerStyle.Text="";
			CustomerID.Text="";
			SRP.Text="";
			tblCPRules.DataSource=null;
			tblCPRules.DataBind();
			lblDescription.Text="";
			lblComments.Text="";
			tblDocList.DataSource=null;
			tblDocList.DataBind();
			tblCustomerSKUII.DataSource=null;
			tblCustomerSKUII.DataBind();
			Session["cpList"] = null;
		}
		private void ReFillCustomerList()
		{
			lstCustomerList.Items.Clear();
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
				
			SqlCommand command = new SqlCommand("spGetCustomers");
			
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@AuthorID",Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			SqlDataReader reader = command.ExecuteReader();
			if(reader.HasRows)
			{
				lstCustomerList.DataSource=reader;
				lstCustomerList.DataTextField="CustomerName";
				lstCustomerList.DataValueField="CustomerOfficeID_CustomerID";
				lstCustomerList.DataBind();
				Session["CustomerListDone"]="Done";
			}
			conn.Close();
		}

		protected void cmdFindBySKU_Click(object sender, System.EventArgs e)
		{
			if(txtSKU.Text=="")
				return;

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();

			SqlCommand command = new SqlCommand("sp_GetSkuListBySKU2");
						
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@SKU", "%" + txtSKU.Text.Trim() + "%");
			command.Parameters.Add("@AuthorID", Session["ID"].ToString());

			DataSet cpList = new DataSet();
			SqlDataAdapter dataAdapter;

			dataAdapter = new SqlDataAdapter(command);

			if(dataAdapter.Fill(cpList)>0) //search returned some results.
			{
				lblStyleNotFound.Text="";
				DataTable customerSKU = new DataTable("Found SKU'S");
				customerSKU.Columns.Add(new DataColumn("Customer"));
				customerSKU.Columns.Add(new DataColumn("Customer Program"));
				customerSKU.Columns.Add(new DataColumn("Customer Style"));
				
				Session["cpList"]=cpList;

				int i=0;
				foreach(DataRow row in cpList.Tables[0].Rows)
				{
					DataRow rrow = customerSKU.NewRow();
					rrow["Customer"]="<a href='CPRulesDisplay.aspx?Row="+i+"'>"+row["CustomerName"].ToString()+"</a>";
					rrow["Customer Program"]="<a href='CPRulesDisplay.aspx?Row="+i+"'>"+row["customerprogramname"].ToString()+"</a>";
					rrow["Customer Style"]="<a href='CPRulesDisplay.aspx?Row="+i+++"'>"+row["customerstyle"].ToString()+"</a>";
					customerSKU.Rows.Add(rrow);
				}

				tblCustomerSKUII.DataSource=customerSKU;
				tblCustomerSKUII.DataBind();
				tblCustomerSKUII.Visible=true;
				pnlResult.Visible=true;
			}
			else
			{
				lblStyleNotFound.Text="Style Not Found";
			}
			conn.Close();
		}
	}
}
