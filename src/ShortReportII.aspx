<%@ Page language="c#"  MasterPageFile="~/DefaultMaster.Master" Codebehind="ShortReportII.aspx.cs" AutoEventWireup="True" Inherits="Corpt.ShortReportII" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <style>
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
    </style>
        <script type="text/javascript">
            function downloadExcelFile(base64String, fileName, minute) {
                //alert("Report generated in " + minute + " seconds");
                var a = document.createElement('a');
                a.href = 'data:application/vnd.ms-excel;base64,' + base64String;
                a.download = fileName;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }
        </script>
<div class="demoarea">
    <div class="demoheading">
        Full Order
        <asp:Label ID="lblCustomerName" class="control-label" runat="server" Font-Bold="True"></asp:Label>
    </div>
    <div class="navbar nav-tabs" style="margin-bottom: 5px">
        <table style="margin-bottom: 5px;">
            <tr>
                <td>
                    <asp:ImageButton ID="cmdSaveExcel" ToolTip="Selected batch numbers to Excel" runat="server"
                        ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExcelClick" Enabled="False"
                        Style="margin-left: 0px" />
                    <asp:ImageButton ID="cmdSaveExcelQR" ToolTip="Selected batch numbers to Excel with Report Link" runat="server"
                    ImageUrl="~/Images/ajaxImages/Excelweb.png" OnClick="OnExcelClickQR" Enabled="False" Visible="false"
                    Style="margin-left: 5px" />
                </td>
               
                <td>
                    <asp:Button ID="cmdFullAuto" runat="server" CssClass="btn btn-info" Text="Full AUTO"
                        Style="margin-left: 5px" OnClick="OnFullAutoClick" ToolTip="All batch numbers to Excel"
                        Enabled="False" Visible="false"></asp:Button>
                </td>
                <td>
                    <asp:Button ID="btnFullOrderWithImage" runat="server" CssClass="btn btn-info" Text="Full Order With Image"
                        Style="margin-left: 5px" ToolTip="All batch numbers ith Image to Excel" Enabled="False" OnClick="btnFullOrderWithImage_Click" Visible="false"></asp:Button>
                </td>
                
                <td>
<%--                    <asp:Button ID="btnMovedItems" runat="server" OnClientClick="return false;" Text="Moved Items"--%>
<%--                        Style="margin-left: 5px" CssClass="btn btn-info" Visible="false" />--%>
                <asp:LinkButton ID="btnMovedItems" runat="server" OnClientClick="return false;" Visible="False" Style="margin-left: 5px">Items moved from this batch to another batches</asp:LinkButton>                
                    <asp:TextBox ID="reorientNowFld" runat="server" AutoPostBack="true" Visible="False"
                        Text="H" Width="20"></asp:TextBox>
                </td>
                <td style="padding-left: 5px">
                    <!-- Email Panel -->
                    <asp:Panel ID="Panel3" class="form-inline"  runat="server" DefaultButton="GoEmailButton">
                        <asp:CheckBox ID="SendFullCheckbox" runat="server" Text="Send Full Report" Checked="True"
                            CssClass="checkbox" Width="140px" OnCheckedChanged="OnSendFullChecked" />
                        <asp:DropDownList runat="server" ID="MailAddressList" DataValueField="Email" DataTextField="DisplayName"
                            OnSelectedIndexChanged="OnChoiceMailFromContacts" AutoPostBack="True" CssClass="dropdown" />
                        <asp:Label ID="Label1" runat="server" Text="Address To: " Style="padding-left: 10px"></asp:Label>
                        <asp:TextBox runat="server" ID="AddressTo" Width="200px" placeholder="Email"></asp:TextBox>
                        <asp:ImageButton ID="GoEmailButton" ToolTip="Email Short Report" runat="server" ImageUrl="~/Images/ajaxImages/email_go.png"
                            OnClick="OnSendEmailClick" style="margin-top: 5px;" />
                        <asp:Label runat="server" ID="SendEmailMessage" CssClass="control-label" ForeColor="#990000"></asp:Label>
                    </asp:Panel>
                </td>
                   <td>
                    <asp:UpdatePanel ID="upnlExport" runat="server">
                        <ContentTemplate>
                                <div style="display:flex;align-items: center;padding-left:50px;">
                                      <label style="margin-left: 10px; margin-right: 5px; text-align: right;margin-top: 5px;">
                                        Export Report:</label>
                                    <asp:DropDownList ID="ddlReportType" runat="server" Width="200px" style="margin-bottom:0px;">
                                        <asp:ListItem value="0" Text=""></asp:ListItem>
                                        <asp:ListItem value="5" Text="Selected Batch Numbers"></asp:ListItem>
                                        <asp:ListItem value="6" Text="Report Link"></asp:ListItem>
                                        <asp:ListItem value="1" Text="QR Code + Report Link"></asp:ListItem>
                                        <asp:ListItem value="2" Text="Image from SKU"></asp:ListItem>
                                        <asp:ListItem value="3" Text="QR Code + Image from Prefix" ></asp:ListItem>
                                        <asp:ListItem value="4" Text="Image from Prefix + Report Link" ></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvReportType" ControlToValidate="ddlReportType" InitialValue="0" runat="server" ErrorMessage="*" ValidationGroup="grpReportType"></asp:RequiredFieldValidator>
                                     <asp:Button ID="btnExport" class="btn btn-primary" runat="server" Text="Export" style="margin-top: 0px;margin-bottom: 0px;margin-left: 0px;" 
                                         OnClick="btnExport_Click" ValidationGroup="grpReportType" Enabled="false"></asp:Button>
                                </div>
                             </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnExport" EventName="click"/>
                        </Triggers>
                    </asp:UpdatePanel>
					<asp:UpdateProgress ID="Up1" runat="Server" AssociatedUpdatePanelID="upnlExport">
						<ProgressTemplate>
							<span style="width: -webkit-fill-available; height: -webkit-fill-available;position: absolute; background-color: rgba(0, 0, 0, 0.4);top: 0;left: 0;">
								<img src="Images/ajaxImages/loader.gif" alt="Please wait" width="100px" style="position: absolute;top: 40%;left: 50%;" />
								Please wait ...</span>
						</ProgressTemplate>
					</asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>
    
    <div style="font-family: Cambria; font-size: small; margin-left: 5px; border-color: silver; border-style: none; border-width: 1px; ">
        Search by
        <div >
            <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False" ActiveTabIndex="1" AutoPostBack="True" Height="40px"
                OnActiveTabChanged="OnActiveTabChanged">
                <ajaxToolkit:TabPanel runat="server" HeaderText="Order" OnDemandMode="Once" ID="TabOrder">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="OrderFilterPanel" CssClass="form-inline" DefaultButton="cmdLoad" >
                            <!-- 'Order Number' textbox -->
                            <asp:TextBox type="text" ID="txtOrderNumber" runat="server" name="txtOrderNumber"/>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderNumber"
                                Display="None" 
                                ErrorMessage="<b>Required Field Missing</b><br />A Order Number is required." 
                                ValidationGroup="OrderGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderNumber"
                                Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" 
                                ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>" 
                                ValidationGroup="OrderGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" Enabled="True" />
                            <!-- 'Load' button -->
                            <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Load" OnClick="OnLoadClick">
                            </asp:Button>
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" HeaderText="STMA" OnDemandMode="Once" ID="TabMemo">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="MemoFilterPanel" CssClass="form-inline" Style="font-size: small">
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="CustomerLikePanel" DefaultButton="CustomerLikeButton"
                                        CssClass="form-inline">
                                        <asp:TextBox runat="server" ID="CustomerLike"></asp:TextBox>
                                        <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                            ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                                    </asp:Panel>
                                </td>
                                <td>
                                    <!-- Customer -->
                                    <asp:DropDownList ID="lstCustomerList" runat="server" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                                        Width="300px" Style="font-family: Cambria; font-size: 13px" />
                                    <ajaxToolkit:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="lstCustomerList"
                                        Enabled="True" PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains"
                                        QueryTimeout="2000">
                                    </ajaxToolkit:ListSearchExtender>
                                </td>
                                <td>
                                    <!-- Date From -->
                                    <label style="margin-left: 5px; margin-right: 5px; text-align: right">
                                        From</label>
                                    <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
                                        AutoPostBack="True" Style="margin-left: 3px; height: 21px" />
                                    <asp:ImageButton runat="server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                        AlternateText="Click to show calendar" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                                        PopupButtonID="Image1" Enabled="True" />
                                </td>
                                <td>
                                    <!-- Date To -->
                                    <label style="margin-left: 5px; margin-right: 5px; text-align: right">
                                        To</label>
                                    <asp:TextBox runat="server" ID="calTo" Width="100px" OnTextChanged="OnChangedDateTo"
                                        AutoPostBack="True" />
                                    <asp:ImageButton runat="server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                        AlternateText="Click to show calendar" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                                        PopupButtonID="Image2" Enabled="True" />
                                </td>
                                <td>
                                    <!-- Memo Number -->
                                    <label style="margin-left: 10px; margin-right: 5px; text-align: right">
                                        Memo</label>
                                    <asp:DropDownList ID="lstMemos" runat="server" DataTextField="MemoNumber" DataValueField="MemoNumber"
                                        OnSelectedIndexChanged="OnMemoSelectedChanged" AutoPostBack="True" Style="margin-left: 5px;
                                        font-family: Cambria; font-size: 13px" Width="150px" />
                                    <ajaxToolkit:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="lstMemos"
                                        Enabled="True" PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains"
                                        QueryTimeout="2000">
                                    </ajaxToolkit:ListSearchExtender>
                                    <!-- 'Load' button -->
                                    <asp:Button ID="LoadByMemoButton" class="btn btn-primary" runat="server" Text="Load"
                                        OnClick="OnLoadByMemoClick"></asp:Button>
                                </td>
                            </tr>
                        </table>
                            
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </div>
    </div>

    <!-- Moved Items region -->
    <div style="margin: 0px; margin: 5px;">
        
        <!-- "Wire frame" div used to transition from the button to the info panel -->
        <div id="flyout" style="display: none; overflow: hidden; z-index: 2; background-color: #FFFFFF; border: solid 1px #D0D0D0;"></div>
        
        <!-- Info panel to be displayed as a flyout when the button is clicked -->
        <div id="info" style="display: none; width:400px; z-index: 2; opacity: 0; 
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); 
            font-size: 12px; font-family: Cambria; border: solid 1px #CCCCCC; background-color: #FFFFFF; padding: 5px;">
            <!-- Close Button -->
            <div id="btnCloseParent" style="float: right; opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);">
                <asp:LinkButton id="btnClose" runat="server" OnClientClick="return false;" Text="X" ToolTip="Close"
                    Style="background-color: #666666; color: #FFFFFF; text-align: center; font-weight: bold; text-decoration: none; border: outset thin #FFFFFF; padding: 2px;" />
            </div>
			<!-- Data Grid with Removed Items -->
            <div style="overflow-x: auto; overflow-y: auto; padding: 2px;height: 400px; width: 400px;">
                <asp:DataGrid runat="server" ID="dgPopup" CssClass="table table-bordered" >
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:DataGrid>
            </div>
        </div>
        
        <script type="text/javascript" language="javascript">
            // Move an element directly on top of another element (and optionally
            // make it the same size)
            function Cover(bottom, top, ignoreSize) {
                var location = Sys.UI.DomElement.getLocation(bottom);
                top.style.position = 'absolute';
                top.style.top = location.y + 'px';
                top.style.left = location.x + 'px';
                if (!ignoreSize) {
                    top.style.height = bottom.offsetHeight + 'px';
                    top.style.width = bottom.offsetWidth + 'px';
                }
            }
        </script>
        <ajaxToolkit:AnimationExtender id="OpenAnimation" runat="server" TargetControlID="btnMovedItems">
            <Animations>
                <OnClick>
                    <Sequence>
                        <%-- Disable the button so it can't be clicked again --%>
                        <EnableAction Enabled="false" />
                        
                        <%-- Position the wire frame on top of the button and show it --%>
                        <ScriptAction Script="Cover($get('ctl00_SampleContent_btnMovedItems'), $get('flyout'));" />
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="block"/>
                        
                        <%-- Move the wire frame from the button's bounds to the info panel's bounds --%>
                        <Parallel AnimationTarget="flyout" Duration=".3" Fps="25">
                            <Move Horizontal="150" Vertical="-50" />
                            <Resize Width="400" Height="400" />
                            <Color PropertyKey="backgroundColor" StartValue="#AAAAAA" EndValue="#FFFFFF" />
                        </Parallel>
                        
                        <%-- Move the info panel on top of the wire frame, fade it in, and hide the frame --%>
                        <ScriptAction Script="Cover($get('flyout'), $get('info'), true);" />
                        <StyleAction AnimationTarget="info" Attribute="display" Value="block"/>
                        <FadeIn AnimationTarget="info" Duration=".2"/>
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="none"/>
                        
                        <%-- Flash the text/border red and fade in the "close" button --%>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#666666" EndValue="#FF0000" />
                            <Color PropertyKey="borderColor" StartValue="#666666" EndValue="#FF0000" />
                        </Parallel>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#FF0000" EndValue="#666666" />
                            <Color PropertyKey="borderColor" StartValue="#FF0000" EndValue="#666666" />
                            <FadeIn AnimationTarget="btnCloseParent" MaximumOpacity=".9" />
                        </Parallel>
                    </Sequence>
                </OnClick>
            </Animations>
        </ajaxToolkit:AnimationExtender>
        <ajaxToolkit:AnimationExtender id="CloseAnimation" runat="server" TargetControlID="btnClose">
            <Animations>
                <OnClick>
                    <Sequence AnimationTarget="info">
                        <%--  Shrink the info panel out of view --%>
                        <StyleAction Attribute="overflow" Value="hidden"/>
                        <Parallel Duration=".3" Fps="15">
                            <Scale ScaleFactor="0.05" Center="true" ScaleFont="true" FontUnit="px" />
                            <FadeOut />
                        </Parallel>
                        
                        <%--  Reset the sample so it can be played again --%>
                        <StyleAction Attribute="display" Value="none"/>
                        <StyleAction Attribute="width" Value="400px"/>
                        <StyleAction Attribute="height" Value=""/>
                        <StyleAction Attribute="fontSize" Value="12px"/>
                        <OpacityAction AnimationTarget="btnCloseParent" Opacity="0" />
                        
                        <%--  Enable the button so it can be played again --%>
                        <EnableAction AnimationTarget="btnMovedItems" Enabled="true" />
                    </Sequence>
                </OnClick>
                <OnMouseOver>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FFFFFF" EndValue="#FF0000" />
                </OnMouseOver>
                <OnMouseOut>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FF0000" EndValue="#FFFFFF" />
                </OnMouseOut>
             </Animations>
        </ajaxToolkit:AnimationExtender>


    </div>
    <!-- Batch numbers List -->
    <div style="margin: 5px" >
        <asp:Panel runat="server" ID="batchNumbersPanel" Visible="False">
        <table>
            <tr>
                <td style="padding-left: 5px;">
                    <asp:CheckBox class="checkbox" runat="server" Text="Check All" ID="checkAll" AutoPostBack="True" Width="70px" Font-Size="12px"
                        OnCheckedChanged="OnCheckAllClick"></asp:CheckBox>
                </td>
            </tr>
            <tr >
                <td style="vertical-align: top">
                    <asp:Panel ToolTip="Batch Codes" ID="Panel2" runat="server" CssClass="checkbox"
                        Style="height: 105px;vertical-align: top;min-width: 200px" BorderColor="Silver" BorderStyle="Dotted"
                        BorderWidth="1px" ScrollBars="Vertical" Wrap="True" >
                        <asp:CheckBoxList ID="fullBatchList" runat="server" style="font-size: small;font-family: Calibri;width: 320px"
                            RepeatDirection="Horizontal"
                            BorderStyle="None" AutoPostBack="True" OnSelectedIndexChanged="OnFullBatchListChanged"
                            CellPadding="2" CellSpacing="2" RepeatColumns="3" 
                            DataTextField="FullBatchNumber"
                            DataValueField="BatchId">
                        </asp:CheckBoxList>
                    </asp:Panel>
                </td>
                <td style="padding-left: 20px; vertical-align: top">
                    <asp:Panel ToolTip="Selected Batch Numbers" ID="Panel1" runat="server" CssClass="radio"
                        Style="height: 105px;min-width: 200px" BorderColor="Silver" BorderStyle="Dotted"
                        BorderWidth="1px" ScrollBars="Vertical" Wrap="True" Font-Names="Calibri" Font-Size="Small">
                        <asp:RadioButtonList ID="checkedBatchList" runat="server" RepeatDirection="Horizontal"
                            BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnCheckedBatchListChanged"
                            CellPadding="1" CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                        </asp:RadioButtonList>
                       <!-- <ajaxToolkit:ResizableControlExtender ID="Panel1_ResizableControlExtender" runat="server"
                            Enabled="True" TargetControlID="Panel1" HandleCssClass="handleText" ResizableCssClass="resizingImage"
                            >
                        </ajaxToolkit:ResizableControlExtender> -->
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
    
    <div>
        <asp:ImageButton ID="cmdReorient" ToolTip="Change direction table" runat="server"
            ImageUrl="~/Images/ajaxImages/reorient1.png" OnClick="OnReorientClick" Visible="False"
            Style="margin-left: 5px;" />
    </div>
    <div>
        <asp:Panel runat="server" ID="gdPanel" Visible="False">
            <div>
                <asp:Label ID="LblRowCounts" runat="server" />
            </div>
            <div id="gridContainer" style="font-size: small;">
                <asp:DataGrid ID="grdShortReport" runat="server" CellPadding="5"
                    AllowSorting="True" OnSortCommand="GrdShortReportSortCommand">
                    <HeaderStyle CssClass="GridviewScrollHeader" />
                    <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
                    <PagerStyle CssClass="GridviewScrollPager" />
                </asp:DataGrid>
            </div>
        </asp:Panel>
    </div>
</div>
</asp:Content>
