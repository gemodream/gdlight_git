﻿<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="SyntheticNewEntries.aspx.cs" AutoEventWireup="True" Inherits="Corpt.SyntheticNewEntries" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
	<style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
        .auto-style1 {
            height: 200px;
        }
        .auto-style2 {
            width: 220px;
            height: 200px;
        }
        .auto-style3 {
            width: 587px;
            height: 90px;
        }
    </style>
	<script type="text/javascript">
		function FilterShapes(caller) {
			var textbox = caller;
			filter = textbox.value.toLowerCase();
			var dropDownArray = document.getElementsByTagName("select");
			var list = dropDownArray[1];
			for (var c = 0; c < list.options.length; c++) {
				if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
					list.options.item(c).style.display = "block";
				}
				else {
					list.options.item(c).style.display = "none";
				}
			}
		}
		function StayFocused() {
			var textbox = document.GetElementById('<%=txtOrderNumber.ClientID%>');
			textbox.focus();
        }
        function StayFocused1() {
			var textbox = document.GetElementById('<%=SynthNumberBox.ClientID%>');
			textbox.focus();
		}
	</script>
	<div class="demoarea">
		<div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
		<asp:UpdatePanel runat="server" ID="MainPanel">
			<ContentTemplate>
                <div class="demoheading">
                    Synthetic New Entries
                    <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 20px"></asp:Label>
                </div>
				<table style="width: 100%;">
					<tr>
                        <td style="vertical-align: top; white-space: nowrap;" colspan="2">
                            <!-- 'Batch Number' textbox -->
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                                <asp:TextBox type="text" ID="txtOrderNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                    placeholder="Order number" Style="width: 115px;" OnFocus="this.select();"/>
                                <!-- 'Load' button -->
                                <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-large" runat="server" Text="Load"
                                    OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
                                <asp:Button ID="cmdSaveBatch" class="btn btn-info btn-large" runat="server" Text="Save"
                                    OnClick="OnSaveClick" Style="margin-left: 10px"></asp:Button>
                                </asp:Panel>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrderNumber"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrderNumber"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                        </td>
                        <!--<td style="vertical-align: top; width: 80%;"> -->
                            <!--<asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel" Visible="false" Style="float: left; font-size: 16px;"></asp:Label> -->
                            <!-- 'Save' button -->
							<!-- Short Report Button -->
                        <!--</td> -->
                        <%-- <td style="text-align: left; padding-right: 40px; padding-left: 20px; vertical-align: top">
                        </td> --%>
                    </tr>
					<tr>
                        <td style="padding-top: 0px; vertical-align: top; height: 25px;" class="auto-style1">
                            <br />
                        </td>
                        <%-- %><td style="vertical-align: top;padding-right: 20px;font-size: 19px; height: 25px;" class="auto-style2">
                            </td>--%>
						<td style="vertical-align: top;" rowspan="2">
							<%--New Measurement Panel--%>
							<asp:Panel runat="server" ID="MeasurementPanel" Visible="false" Style="position: relative;">
								<div style="height: 700px;">
                                    
                        <table>
                        <tr>
                            <td>
                                    <asp:Panel runat="server" ID="Destination">
                                        <asp:Label runat="server" Text="Destination" style="float: left; clear:both;"></asp:Label>
								        <asp:TextBox runat="server" ID="DestinationBox" OnTextChanged="OnSynthAddClick" style="width: 150px; float:left; clear:both; margin-bottom: 55px;" autocomplete="off"></asp:TextBox>                                       
                                        <ajaxToolkit:DropDownExtender runat="server" TargetControlID="DestinationBox" DropDownControlID="PanelDest" />
                                        <asp:Panel runat="server" ID="PanelDest">
                                                        <asp:ListBox runat="server" ID="DestinationListBox" DataSource='<%# Eval("DropDownEnums")%>'
                                                            DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
                                                            OnSelectedIndexChanged="OnDestinationDropDownSelectedIndexChanged"></asp:ListBox>
                                        </asp:Panel>
                                        <!-- alex 
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="DestinationButton" class="btn btn-info btn-large" runat="server" Text="Dest Delete"
                                                    Style="margin-left: 10px; margin-top: 10px; float:left; clear:both;"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                            -->
							        </asp:Panel>
                            </td>
                            <td>
                                    <asp:Panel runat="server" ID="SynthNumber">
                                        <asp:Label runat="server" Text="Synthetic" style="float: left; clear:both;"></asp:Label>
								        <asp:TextBox runat="server" ID="SynthNumberBox" OnTextChanged="OnSynthAddClick" style="width: 150px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>                                       
                                        <ajaxToolkit:DropDownExtender runat="server" TargetControlID="SynthNumberBox" DropDownControlID="PanelSynth" />
                                        <asp:Panel runat="server" ID="PanelSynth">
                                                        <asp:ListBox runat="server" ID="SynthListBox" DataSource='<%# Eval("DropDownEnums")%>'
                                                            DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
                                                            OnSelectedIndexChanged="OnSynthDropDownSelectedIndexChanged"></asp:ListBox>
                                        </asp:Panel>
                                        
                                        <table>
                                            <tr>
                                                <!--
                                                <td>
                                                    <asp:Button ID="SynthNumberAddBtn" class="btn btn-info btn-large" runat="server" Text="Synth Add" visible="false"
                                                    OnClick="OnSynthAddClick" Style="margin-left: 10px; float:left; clear:both;"></asp:Button>
                                                </td>
                                                -->
                                                <td>
                                                    <asp:Button ID="SynthNumberDelBtn" class="btn btn-info btn-large" runat="server" Text="Synth Delete" 
                                                    OnClick="OnSynthDelClick" Style="margin-left: 10px; float:left; clear:both;"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        
							        </asp:Panel>
                            </td>
                            <td>
                                    <asp:Panel runat="server" ID="SuspNumber">
                                        <asp:Label runat="server" Text="Suspect" style="float: left; clear:both;"></asp:Label>
								        <asp:TextBox runat="server" ID="SuspNumberBox" OnFocus="this.select();" OnTextChanged="OnSuspAddClick" style="width: 150px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
                                        <ajaxToolkit:DropDownExtender runat="server" TargetControlID="SuspNumberBox" DropDownControlID="Panel2" />
                                        <asp:Panel runat="server" ID="Panel2">
                                                        <asp:ListBox runat="server" ID="SuspListBox" DataSource='<%# Eval("DropDownEnums")%>'
                                                            DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
                                                            OnSelectedIndexChanged="OnSuspDropDownSelectedIndexChanged"></asp:ListBox>
                                                    </asp:Panel>
                                
                                        <table>
                                            <tr>
                                                <!--
                                                <td>
                                                    <asp:Button ID="SuspNumberAddBtn" class="btn btn-info btn-large" runat="server" Text="Susp Add" visible="false"
                                                    OnClick="OnSuspAddClick" Style="margin-left: 10px; float:left; clear:both;"></asp:Button>
                                                </td>
                                                -->
                                                <td>
                                                    <asp:Button ID="SuspNumberDelBtn" class="btn btn-info btn-large" runat="server" Text="Susp Delete" 
                                                    OnClick="OnSuspDelClick" Style="margin-left: 10px; float:left; clear:both;"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                
							        </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btnPrintLabel" runat="server" CssClass="btn btn-primary" Text="Print Label"
                                Style="height: 25px; margin-bottom: 12px; margin-left: 20px; padding-top: 2px;" OnClick="btnPrintLabel_Click" Enabled="false">
                            </asp:Button>
                                </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" id="labelCountLbl" Text="Small Labels Count" style="float:left; clear:both;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" ID="labelCount" style="width: 200px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnPrintItemLabel" runat="server" CssClass="btn btn-primary" Text="Print Small Label"
                                                Style="height: 25px; margin-bottom: 12px; margin-left: 20px; padding-top: 2px;" OnClick="btnPrintItemLabel_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnPrintNewItemLabel" runat="server" CssClass="btn btn-primary" Text="Print Item Label"
                                                Style="margin-bottom: 12px; margin-left: 20px; padding-top: 2px;" OnClick="btnPrintNewItemLabel_Click"/>
                                        </td>
                                    </tr>
                                </table>
                                
                                
                                
                            </asp:Button>
                            </td>
                        </tr>
                        </table>
									<asp:HiddenField runat="server" ID="PartEditing" />
									<asp:HiddenField runat="server" ID="ItemEditing" />
									<asp:HiddenField runat="server" ID="ElementEditing" />
									<asp:HiddenField runat="server" ID="CategoryEditing" />
									<asp:Repeater runat="server" ID="ElementPanel" >
										<ItemTemplate>
											<asp:HiddenField runat="server" ID="ElementName" Value='<%# Eval("ElemName")%>' />
											<asp:HiddenField runat="server" ID="ElementType" Value='<%# Eval("ElemType") %>' />
											<asp:HiddenField runat="server" ID="ElementMeasure" Value='<%# Eval("ElemMeasure") %>' />
											<asp:Panel runat="server" ID="SmallPanel" visible='<%# Eval("SmVis")%>'>
												<div style="width: 150px; float: left;  margin-bottom: 20px; margin-right: 20px;">
													<asp:Label runat="server" ID="SmLbl" Text='<%# Eval("SmLbl")%>' visible='<%# Eval("SmLblVis")%>'></asp:Label>
													<asp:TextBox runat="server" ID="SmTxt" Text='<%# Eval("SmTxt")%>' readonly='<%# Eval("SmTxtRO")%>' visible='<%# Eval("SmTxtVis")%>' Enabled="false" Style="width: 135px;"></asp:TextBox>
													<asp:Button runat="server" ID="SmBtn" CssClass="btn btn-info btn-large" OnClick="OnSmBtnClick"
														Text='<%# Eval("SmBtn")%>' visible='<%# Eval("SmBtnVis")%>' Style="width: 150px;"></asp:Button>
												</div>
											</asp:Panel>
											<asp:Panel runat="server" ID="BigPanel" visible='<%# Eval("BigVis")%>' Style="position: absolute; width: 492px; top: 200px; background-color: white; border: 2px solid gray; padding: 5px;">
												<asp:Panel runat="server" ID="DropDownPanel" visible='<%# Eval("DropDownPanelVis")%>'>
													<asp:Label runat="server" ID="BigLbl" Text='<%# Eval("BigLbl")%>' visible='<%# Eval("BigLblVis")%>'></asp:Label>
													<asp:TextBox runat="server" ID="BigTxt" Text='<%# Eval("BigTxt")%>' visible='<%# Eval("BigTxtVis")%>' OnTextChanged="OnTextChanged"></asp:TextBox>
													<ajaxToolkit:DropDownExtender runat="server" TargetControlID="BigTxt" DropDownControlID="DropDownListPanel" />
													<asp:Panel runat="server" ID="DropDownListPanel">
														<asp:ListBox runat="server" ID="DropDownListBox" DataSource='<%# Eval("DropDownEnums")%>'
															DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
															OnSelectedIndexChanged="OnDropDownSelectedIndexChanged"></asp:ListBox>
													</asp:Panel>
												</asp:Panel>
												<asp:Repeater runat="server" ID="TextRepeater" DataSource='<%# Eval("TxtRepeaterSource")%>' visible='<%# Eval("TxtRepeaterVis")%>'>
													<ItemTemplate>
														<div style="display: flex; justify-content: flex-end;">
															<asp:Label runat="server" ID="RepeatLabel" Text='<%# Eval("RepeatLbl")%>' Style="text-align: right;"></asp:Label>
															<asp:TextBox runat="server" ID="RepeatTxt" Value='<%# Eval("RepeatTxt")%>' readonly="true" Style="margin-left: 5px; width: 50px;"/>
														</div>
													</ItemTemplate>
												</asp:Repeater>
												<asp:Repeater runat="server" ID="ButtonRepeater" DataSource='<%# Eval("BtnRepeaterSource")%>' visible='<%# Eval("BtnRepeaterVis")%>'
													OnItemCommand="OnButtonListClick">
													<ItemTemplate>
														<asp:Button runat="server" ID="RepeatButton" CssClass="btn btn-info btn-large"
																Text='<%# Eval("BigBtn")%>'  style="margin: 10px; width: 100px; font-size: 16px; padding-left: 0px; padding-right: 0px; border-bottom-color: #1f6377"/>
														<asp:HiddenField runat="server" ID="BtnValue" Value='<%# Eval("BtnValue")%>' />
														<asp:HiddenField runat="server" ID="BtnLink" Value='<%# Eval("BtnLink")%>' />
														<asp:HiddenField runat="server" ID="BtnCat" Value='<%# Eval("BtnCat")%>' />
														<asp:HiddenField runat="server" ID="BtnMeasure" Value='<%# Eval("BtnMeasure") %>' />
                                                        <asp:HiddenField runat="server" ID="BigBtn" Value='<%# Eval("BigBtn") %>' />
													</ItemTemplate>
												</asp:Repeater>
											</asp:Panel>
											<asp:Panel runat="server" ID="Pic" CssClass="form-horizontal" Visible='<%# Eval("PicVis")%>' Style="width: 200px; word-wrap: break-word; margin-bottom: 60px;">
												<asp:Label runat="server" ID="PicLbl" Width="200px" Text='<%# Eval("PicLblTxt") %>'></asp:Label>
												<asp:Image runat="server" ID="PicImg" Width="180px" Visible='<%# Eval("PicImgVis")%>'></asp:Image>
												<asp:Label runat="server" ID="PicErrLbl" ForeColor="Red" Width="200px" Text='<%# Eval("PicErrLblTxt") %>' Visible='<%# Eval("PicErrLblVis")%>'></asp:Label>
											</asp:Panel>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</asp:Panel>
						</td>
					</tr>
					<tr>
                        
						<td style="vertical-align: top; padding-right:150px; width:300px;">
							<asp:Panel runat="server" ID="MemoPanel" Visible="false">
								<!-- alex <asp:Label runat="server" ID="MemoTxt" Text="Memo" style="float:left; clear:both;"></asp:Label> alex -->
                                <asp:Label runat="server" Text="Memo" style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="MemoValue" style="width: 200px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
							    <asp:HiddenField ID="SynthScreeningID" runat="server" />
                            </asp:Panel>
                            
							<asp:Panel runat="server" ID="PONumPanel" Visible="false">
								<asp:Label runat="server" ID="PONumText" Text="PO Number" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="PONumValue" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
							</asp:Panel>
                            <!--
                            <asp:Panel runat="server" ID="SKUPanel" Visible="false">
                                <asp:Label runat="server" Text="SKU/Style" Style="float:left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="SKUValue" Style="width: 200px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
							</asp:Panel>
                                -->
							<asp:Panel runat="server" ID="VendorPanel" Visible="false">
								<asp:Label runat="server" Text="Vendor" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="VendorValue" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off">
									</asp:TextBox>
							</asp:Panel>
                            <asp:Panel runat="server" ID="TotalQtyPanel" Visible="false">
								<asp:Label runat="server" Text="Total Quantity" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="TotalQtyBox" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off">
									</asp:TextBox>
							</asp:Panel>
                             <asp:Panel runat="server" ID="FurtherTestingPanel" Visible="false">
								<asp:Label runat="server" Text="FurtherTesting" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="FurtherTestingBox" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off">
									</asp:TextBox>
							</asp:Panel>
                            <asp:Panel runat="server" ID="QtyPassPanel" Visible="false">
								<asp:Label runat="server" Text="Quantity Pass" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="txtQtyPass" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnVendorChanged"></asp:TextBox>
							</asp:Panel>
                            <asp:Panel runat="server" ID="QtySuspPanel" Visible="false">
								<asp:Label runat="server" Text="Quantity Suspect" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="txtQtySusp" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnVendorChanged"></asp:TextBox>
							</asp:Panel>
                            <asp:Panel runat="server" ID="QtySynthPanel" Visible="false">
								<asp:Label runat="server" Text="Quantity Synthetic" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="txtQtySynth" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnVendorChanged"></asp:TextBox>
							</asp:Panel>
                            <table id="BagsTable" runat="server" visible="false">
                                <tr>
                                    <td>Items in bag</td>
                                </tr>
                                <tr>
                                    <td><asp:TextBox runat="server" ID="ItemsInBag" OnTextChanged="OnBagAddClick"></asp:TextBox>
                                        <asp:ImageButton ID="AddBagButton" runat="server" ToolTip="Add Batch" ImageUrl="~/Images/ajaxImages/add.png"
                                        OnClick="OnBagAddClick" Enabled="True"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DataGrid ID="GridNewBags" runat="server" CssClass="table" AllowingPaging="True" PageSize ="10">
                                            
                                        </asp:DataGrid>
                                    </td>
                                </tr> 
                            </table>
                            <!--
                            <asp:Panel runat="server" ID="QtySynthSuspPanel" Visible="false">
								<asp:Label runat="server" Text="Quantity Synthetic/Suspect" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="txtQtySynthSusp" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnVendorChanged"></asp:TextBox>
							</asp:Panel>
                            
                            <asp:Panel runat="server" ID="FTQuantity" Visible="false">
								<asp:Label runat="server" Text="FT Quantity" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="FTQty" style="width: 200px; float: left; clear:both; margin-bottom: 10px;" autocomplete="off"
									OnTextChanged="OnVendorChanged"></asp:TextBox>
							</asp:Panel>
                            -->
                            <asp:Panel runat="server" ID="IntExtCommentPanel" Visible="false" Height="50px" Width="192px">
								<asp:Label runat="server" ID="notesLbl" Text="Notes" Style="float:left; clear:both; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="InternalComments" TextMode="MultiLine" Columns="1" Style="width: 150px; height: 25px; margin-bottom: 10px;"
									OnTextChanged="OnIntExtCommentChanged" AutoPostBack="true"></asp:TextBox>
                                <asp:Label runat="server" id="GSIItemLabel" Text="GSI ITEM" style="float: left; clear:both;"></asp:Label>
								<asp:TextBox runat="server" ID="gsiNumberTextBox" style="width: 200px; float: left; clear:both; margin-top: 10px;" autocomplete="off"
									OnTextChanged="OnLoadItemPartsClick" AutoPostBack="True"></asp:TextBox>
                                <asp:Button ID="itemLoadBtn" class="btn btn-info btn-large" runat="server" Text="Load Item Parts"
                                    OnClick="OnLoadItemPartsClick" Style="margin-top: 20px;"></asp:Button>
                                <asp:Label runat="server" ID="ItemPartsLabel" Text="Item Parts" style="float: left; clear:both; margin-top:20px;"></asp:Label>
                                <asp:DropDownList ID="ItemPartsList" runat="server" AutoPostBack="True" DataTextField="PartName" Width="200px" style="margin-top: 10px;"
                                                        DataValueField="PartID" ToolTip="Parts List"/>
                                <asp:Button ID="synthItemBtn" class="btn btn-info btn-large" runat="server" Text="Update Item Data"
                                    OnClick="OnUpdateFailedItemClick" Style="margin-top: 20px;"></asp:Button>
							</asp:Panel>
                            
						</td>
                        <td>
                            <asp:Panel runat="server" ID="Panel3" Visible="false" >
								
							</asp:Panel>
                        </td>
					</tr>
                    
				</table>
				<%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px;display: none;border: solid 2px Gray">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px;color: black;text-align:center"
                        id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif;">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black;padding-top: 10px">Do you want to save the item changes?</div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" OnClick="OnQuesSaveNoClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" OnClick="OnQuesSaveCancelClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close"  Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender" Y="0"
                    PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn" >
                </ajaxToolkit:ModalPopupExtender>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>