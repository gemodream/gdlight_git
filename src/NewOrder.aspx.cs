﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class NewOrder : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: New Order";
            if(!IsPostBack)
            {
                LoadCustomers();
                SetViewState(new List<string>(), SessionConstants.NewOrderMemoList);
                SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
                CustomerLike.Focus();
            }
        }

        #region Customers
        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();


        }
        #endregion

        #region Memo Numbers
        protected void OnMemoAddClick(object sender, ImageClickEventArgs e)
        {
            var memo = MemoNumberField.Text.Trim();
            if (memo.Length == 0) return;
            var memoList = GetViewState(SessionConstants.NewOrderMemoList) as List<string> ?? new List<string>();
            if (memoList.Contains(memo)) return;

            memoList.Add(memo);
            SetViewState(memoList, SessionConstants.NewOrderMemoList);

            MemoList.DataSource = memoList;
            MemoList.DataBind();
            MemoList.SelectedValue = memo;
            MemoNumberField.Text = "";
            MemoNumberField.Focus();
        }

        protected void OnMemoAddFromFile(object sender, EventArgs e)
        {
            var file = MemoFileField.Text.Trim();
            if (string.IsNullOrEmpty(file)) return;
            if (!file.EndsWith(".xml"))
            {
                file += ".xml";
            }
            var dsXml = new DataSet();
            try
            {
                dsXml.ReadXml(Session[SessionConstants.GlobalXmlMemo] + file);
            } catch (FileNotFoundException ex)
            {
                var msg = ex.Message.Replace("\\", "/");
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + ex.Message + "\")</SCRIPT>");
                return;
            }
            if (dsXml.Tables["Data"].Rows.Count > 0)
            {
                var memoList = GetViewState(SessionConstants.NewOrderMemoList) as List<string> ?? new List<string>();
                memoList.AddRange(from DataRow drMemo in dsXml.Tables["Data"].Rows select drMemo[1].ToString());
                SetViewState(memoList, SessionConstants.NewOrderMemoList);
                MemoList.DataSource = memoList;
                MemoList.DataBind();
            
            }


        }
        protected void OnMemoDelClick(object sender, ImageClickEventArgs e)
        {
            var memo = MemoList.SelectedValue;
            if (string.IsNullOrEmpty(memo)) return;
            var memoList = GetViewState(SessionConstants.NewOrderMemoList) as List<string> ?? new List<string>();
            var idxNext = MemoList.SelectedIndex;
            memoList.Remove(memo);
            SetViewState(memoList, SessionConstants.NewOrderMemoList);
            MemoList.DataSource = memoList;
            MemoList.DataBind();
            if (idxNext >= memoList.Count && memoList.Count > 0)
            {
                MemoList.SelectedIndex = 0;
            }
            if (idxNext < memoList.Count)
            {
                MemoList.SelectedIndex = idxNext;
            }
        }
        #endregion

        #region Save & Clear Buttons
        private void InitPageFields()
        {
            OrderField.Text = "";
            CountItemsField.Text = "";
            SpecIntrucField.Text = "";

            MemoNumberField.Text = "";
            MemoList.Items.Clear();
            SetViewState(new List<string>(), SessionConstants.NewOrderMemoList);
        }
        protected void OnSaveClick(object sender, EventArgs e)
        {
            var newOrder = OrderField.Text.Trim();
            if (string.IsNullOrEmpty(newOrder) || string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var customer = customers.Find(m => m.CustomerId == lstCustomerList.SelectedValue);
            if (customer == null) return;
            var countItems = 0;
            if (!string.IsNullOrEmpty(CountItemsField.Text))
            {
                countItems = Int32.Parse(CountItemsField.Text);
            }
            var orderModel = new NewOrderModel
            {
                OrderMemo = newOrder,
                MemoNumbers = GetViewState(SessionConstants.NewOrderMemoList) as List<string>,
                Customer =  customer,
                NumberOfItems = countItems,
                SpecialInstructions = SpecIntrucField.Text
                
            };
            string errMsg;
            var result = QueryUtils.CreateOrder(orderModel, this, out errMsg);
            if (result)
            {
                var okMsg = 
                    "New order number " +  orderModel.OrderNumber + " " +
                    "has been created for customer " + orderModel.Customer.CustomerName + "!";
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + okMsg + "\")</SCRIPT>");
                InitPageFields();
            } else
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + errMsg + "\")</SCRIPT>");
            }
        }
        protected void OnClearClick(object sender, EventArgs e)
        {
            InitPageFields();
        }

        #endregion

    }
}