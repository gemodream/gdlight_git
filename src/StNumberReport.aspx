﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="StNumberReport.aspx.cs" Inherits="Corpt.StNumberReport" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" >
    </ajaxToolkit:ToolkitScriptManager>

    <div class="demoarea">
        <div class="demoheading">STMA Number Combined Report</div>
        <!-- Filter Panel -->
        <div class="form-inline" style="font-family: Cambria;font-size: 13px;">
            <!-- Date From -->
                <label style="margin-right: 5px">From</label>
                <asp:TextBox runat="server" ID="calFrom"  Width="100px" 
                ontextchanged="OnChangedDateFrom" AutoPostBack="True" />
                <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png" 
                    AlternateText="Click to show calendar" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom" 
                    PopupButtonID="Image1" />
            <!-- Date To -->
                <label style="margin-left: 15px; margin-right: 5px; text-align: right" >To</label>
                <asp:TextBox runat="server" ID="calTo" Width="100px" 
                ontextchanged="OnChangedDateTo" AutoPostBack="True" />
                <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                    AlternateText="Click to show calendar" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                    PopupButtonID="Image2" />
                <!-- Customer -->
                <asp:DropDownList ID="lstCustomerList" runat="server" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                    DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True" Width="300px"  style="margin-left: 25px;"/>
                <ajaxToolkit:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="lstCustomerList"
                    Enabled="True" PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains"
                    QueryTimeout="2000">
                </ajaxToolkit:ListSearchExtender>
                <!-- Memo Number -->
                <asp:DropDownList ID="lstMemos" runat="server" DataTextField="MemoNumber" 
                    DataValueField="MemoNumber" OnSelectedIndexChanged="OnMemoSelectedChanged" AutoPostBack="True"  style="margin-left: 15px;"/>
                <ajaxToolkit:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="lstMemos"
                    Enabled="True" PromptCssClass="ListSearchExtenderPrompt" QueryPattern="Contains"
                    QueryTimeout="2000">
                </ajaxToolkit:ListSearchExtender>
                <!-- Excel button -->
                <asp:ImageButton ID="cmdSaveExcel" ToolTip="Selected batch numbers to Excel" runat="server"
                    ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="OnExcelClick" Enabled="False" />
        </div>
        <!-- Batch numbers List -->
        <div style="margin: 0px; margin-top: 10px; margin-bottom: 10px;">
            <asp:Panel ToolTip="Batch Numbers" ID="Panel1" runat="server" Height="40px" CssClass="radio" 
                BorderColor="Silver" BorderStyle="Dotted" BorderWidth="1px" ScrollBars="Vertical"
                Wrap="True" Font-Names="Calibri" Font-Size="Small">
                <asp:RadioButtonList ID="cblBatches" runat="server" RepeatDirection="Horizontal" 
                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="OnChangedBatchGroup"
                    CellPadding="1" CellSpacing="1" RepeatColumns="1" Font-Size="X-Small">
                </asp:RadioButtonList>
            </asp:Panel>
            <ajaxToolkit:ResizableControlExtender ID="Panel1_ResizableControlExtender" runat="server"
                Enabled="True" TargetControlID="Panel1" HandleCssClass="handleText" ResizableCssClass="resizingImage">
            </ajaxToolkit:ResizableControlExtender>
        </div>
        <div>
            <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                FadeTransitions="false" FramesPerSecond="40" TransitionDuration="250" AutoSize="Fill"
                RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                <Panes>
                    <ajaxToolkit:AccordionPane ID="AccordionPane" runat="server" Visible="false">
                        <Header>
                            <asp:Label ID="LblRowCounts" runat="server" />
                        </Header>
                        <Content>
                            <div style="overflow-x: auto; overflow-y: auto; margin-bottom: 5px; height: 450px; font-family: Cambria;font-size: small">
                                <asp:DataGrid ID="grdShortReport" runat="server" Style="font-family: 'MS Sans Serif';
                                    font-size: small;" class="table" CellPadding="1" BackColor="White" ForeColor="#555555"
                                    BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD" CssClass="table table-bordered"
                                    Font-Names="Arial,Microsoft Sans Serif" EnableViewState="True" AllowSorting="False"
                                    GridLines="Both" Font-Size="10px" OnItemDataBound="OnItemDataBound" OnSortCommand="GrdShortReportSortCommand">
                                    <ItemStyle ForeColor="#555555" Font-Names="Arial" Font-Size="12px"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="12px">
                                    </HeaderStyle>
                                </asp:DataGrid>
                            </div>
                        </Content>
                    </ajaxToolkit:AccordionPane>
                </Panes>
            </ajaxToolkit:Accordion>
        </div>
        <div class="row" align="right">
        <asp:hyperlink id="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx" >Back</asp:hyperlink>
    </div>

    </div>
</asp:Content>
