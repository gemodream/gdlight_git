﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="GetFullOrder.aspx.cs" Inherits="Corpt.GetFullOrder" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <asp:Panel runat="server" CssClass="form-inline" DefaultButton="LoadButton">
        <asp:TextBox runat="server" ID="OrderField" placeholder="Order code"></asp:TextBox>
        <!-- Order is required -->
        <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="OrderField"
            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order Code is required." />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
            HighlightCssClass="validatorCalloutHighlight" />
        
        <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="OrderField"
            Display="None" ValidationExpression="^\d{5}$|^\d{6}$|^\d{7}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a order number in the format:<br /><strong>Five, six or seven numeric characters</strong>" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
            HighlightCssClass="validatorCalloutHighlight" />
        <asp:Button runat="server" ID="LoadButton" OnClick="OnLoadClick" Text="Load" CssClass="btn btn-primary"/>
        <asp:DropDownList ID="BatchCombo" runat="server" Width="50%" OnSelectedIndexChanged="OnBatchChanged" AutoPostBack="True">
        </asp:DropDownList>
        <asp:Label runat="server" ID="BatchIdLabel"></asp:Label>
    </asp:Panel>
    <br/>
    <ajaxToolkit:TabContainer ID="TabContainer" runat="server" OnDemand="False"
            ActiveTabIndex="0" AutoPostBack="True" Height="100%"
            onactivetabchanged="OnActiveTabChanged" 
        UseVerticalStripPlacement="False">
        <ajaxToolkit:TabPanel runat="server" HeaderText="Batch" ID="TabBatch"
                OnDemandMode="Once">
            <ContentTemplate>
                <asp:DataGrid ID="gridBatch" runat="server" Width="100%">
                </asp:DataGrid>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Failed Items" ID="TabFailedItems"
                OnDemandMode="Once">
            <ContentTemplate>
                <table>
                    <tr>
                        <td   style="vertical-align: top; width: 50%;border: thin silver solid">
                            <asp:Label ID="spOldLabel" runat="server"></asp:Label>
                        </td>
                        <td style="vertical-align: top; width: 50%; margin-left: 10px;border: thin silver solid;padding-left: 10px;">
                            <asp:Label ID="spNewLabel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="failedOldLbl" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:label id="failedNewLbl" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridFaildOld" runat="server" Height="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundColumn DataField="ItemNumber" HeaderText="ItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OldItemNumber" HeaderText="OldItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartName" HeaderText="PartName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureName" HeaderText="MeasureName"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridFaildNew" runat="server" Height="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundColumn DataField="ItemNumber" HeaderText="ItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OldItemNumber" HeaderText="OldItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartName" HeaderText="PartName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureName" HeaderText="MeasureName"></asp:BoundColumn>
                                </Columns>
                                
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Moved Items" ID="TabMovedItems"
                OnDemandMode="Once">
                <ContentTemplate>
                <table>
                    <tr>
                        <td  style="vertical-align: top; width: 50%;border: thin silver solid">
                            <asp:Label ID="spMovedOldLabel" runat="server"></asp:Label>
                        </td>
                        <td style="vertical-align: top; width: 50%; margin-left: 10px;border: thin silver solid;padding-left: 10px;">
                            <asp:Label ID="spMovedNewLabel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="movedOldLbl" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:label id="movedNewLbl" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridMovedOld" runat="server" Height="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundColumn DataField="FullItemNumber" HeaderText="FullItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewFullItemNumber" HeaderText="NewFullItemNumber"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridMovedNew" runat="server" Height="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundColumn DataField="FullItemNumber" HeaderText="FullItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewFullItemNumber" HeaderText="NewFullItemNumber"></asp:BoundColumn>
                                </Columns>
                                
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                    
                </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Item Values" ID="TabItemValues"
                OnDemandMode="Once">
                <ContentTemplate>
                <table>
                    <tr>
                        <td  style="vertical-align: top; width: 50%;border: thin silver solid">
                            <asp:Label ID="spValuesOldLabel" runat="server"></asp:Label>
                        </td>
                        <td style="vertical-align: top; width: 50%; margin-left: 10px;border: thin silver solid;padding-left: 10px;">
                            <asp:Label ID="spValuesNewLabel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="valuesOldLbl" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:label id="valuesNewLbl" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridValuesOld" runat="server" Height="100%" AutoGenerateColumns="False" Style="font-size: smaller">
                                <Columns>
                                    <asp:BoundColumn DataField="PartId" HeaderText="PartId"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewItemNumber" HeaderText="NewItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OldItemNumber" HeaderText="OldItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartName" HeaderText="PartName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureName" HeaderText="MeasureName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ResultValue" HeaderText="ResultValue"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="vertical-align: top">
                            <asp:DataGrid ID="gridValuesNew" runat="server" Height="100%" AutoGenerateColumns="False" Style="font-size: smaller">
                                <Columns>
                                    <asp:BoundColumn DataField="PartId" HeaderText="PartId"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewItemNumber" HeaderText="NewItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OldItemNumber" HeaderText="OldItemNumber"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartName" HeaderText="PartName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MeasureName" HeaderText="MeasureName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ResultValue" HeaderText="ResultValue"></asp:BoundColumn>
                                </Columns>
                                
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                    
                </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
