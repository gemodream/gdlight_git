﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" Title="Screening Manager Allocation" AutoEventWireup="true" CodeBehind="ScreeningDistribution.aspx.cs" Inherits="Corpt.ScreeningDistribution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <script src="Style/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="Style/jquery-ui.js" type="text/javascript"></script>
    <script src="Style/bootstrap2.0/js/bootstrap.js" type="text/javascript"></script>

    <link rel="icon" type="image/png" href="logo@2x.png" />
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="Style/bootstrap2.0/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Style/jquery-ui.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .split {
            margin-top: 0px;
        }

        .bagtext {
            margin-bottom: 0px !important;
        }

        .bagLable {
            text-align: center;
            margin-left: 6px;
        }

        .ajax__tab_body {
            font-family: unset !important;
            padding: 0px !important;
        }

        td > Label {
            display: unset !important;
            margin-left: 5px !important;
        }

        input[type="text"] {
            margin-bottom: 0px !important;
        }

        .auto-style1 {
            height: 23px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }
    </style>
    <script type="text/javascript">
        function textboxMultilineMaxNumber(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1))
                    return false;
            } catch (e) {
            }
        }
    </script>
    <div class="demoarea" style="text-align: left;">
        <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True"></ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="demoarea">
                    <div style="font-size: smaller; height: 25px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                            <ProgressTemplate>
                                <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                                <b>Please, wait....</b>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <table>
                        <tr>
                            <td>
                                <div class="demoheading">Distribution</div>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <table style="width: auto;" border="0">
                        <tr>
                            <td rowspan="2" style="vertical-align: top;">
                                <fieldset style="border: 1px groove #ddd !important; padding: 10px 10px 10px 10px; border-radius: 8px; width: 100px;">
                                    <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                        class="label">Select Order </legend>
                                    <table style="width: auto;">

                                        <tr>
                                            <td rowspan="9" style="vertical-align: top; padding-right: 5px;">

                                                <asp:ListBox ID="lstOrderIDs" runat="server" Height="195px" Width="100px" OnSelectedIndexChanged="lstOrderIDs_SelectedIndexChanged" AutoPostBack="True" Style="margin-bottom: 0px !important;"></asp:ListBox>
                                            </td>
                                        </tr>


                                    </table>
                                </fieldset>
                            </td>
                            <td style="padding-left: 15px;">
                                <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; width: 200px;">
                                    <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                        class="label">Order Code Detail</legend>
                                    <table style="width: 740px;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Order Code"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="SKU / Style"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="Jewelry Type"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="Retailer"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Total Qty" Width="70px"></asp:Label>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtOrderCode" runat="server" Width="120px" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSKUStyle" runat="server" Width="120px" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtJewelryType" runat="server" Enabled="false" Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRetailer" runat="server" Enabled="false" Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTotalQty" runat="server" Enabled="false" Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>


                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>

                            <td style="vertical-align: top; padding-top: 15px; padding-left: 15px;">
                                <fieldset style="border: 1px groove #ddd !important; padding: 10px 10px 10px 10px; border-radius: 8px; width: max-content;">
                                    <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                        class="label">Batch Allocation</legend>
                                    <table style="width: 740px;">
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlBatchQtyScreenerAllocation" runat="server" ScrollBars="Vertical">
                                                    <asp:GridView ID="gdvBatchQtyScreenerAllocation" runat="server" AutoGenerateColumns="False" OnRowDataBound="gdvBatchQtyScreenerAllocation_RowDataBound" CellPadding="4" ForeColor="#333333" OnRowCommand="gdvBatchQtyScreenerAllocation_RowCommand" Width="700px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Batch">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBatchCode" runat="server" Text='<%# Eval("BatchCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Quantity" ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemQty" runat="server" Text='<%# Eval("ItemQty") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="Screener Pass Items" ItemStyle-Width="50px">
														<ItemTemplate>
															<asp:Label ID="lblScreenerPassItems" runat="server" Text='<%# Eval("ScreenerPassItems") %>' ></asp:Label> 
														</ItemTemplate>
														<ItemStyle Width="50px" />
													</asp:TemplateField>
														<asp:TemplateField HeaderText="Screener Fail Items" ItemStyle-Width="50px">
														<ItemTemplate>
															<asp:Label ID="lblScreenerFailItems" runat="server" Text='<%# Eval("ScreenerFailItems") %>' ></asp:Label> 
														</ItemTemplate>
														<ItemStyle Width="50px" />
													</asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBatchStatus" runat="server" Text='<%# Eval("StatusName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Select Screener">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="drpScreeners" runat="server" Width="200px" Style="margin-bottom: 0px;"
                                                                        DataTextField="UserName" DataValueField="UserID"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Screener" SetFocusOnError="true"
                                                                        Display="None" ForeColor="Red" ValidationGroup="vgSubmit" ControlToValidate="drpScreeners" InitialValue="0"></asp:RequiredFieldValidator>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Select Screening Instrument">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="drpScreeningInstrument" runat="server" Width="200px" Style="margin-bottom: 0px;">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Screening Instrument" SetFocusOnError="true"
                                                                        Display="None" ForeColor="Red" ValidationGroup="vgSubmit" ControlToValidate="drpScreeningInstrument" InitialValue="0"></asp:RequiredFieldValidator>
                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Task Allocation">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnTask" runat="server" Text="Submit" CommandName="Task" CommandArgument='<%# Container.DataItemIndex %>' CssClass="btn btn-info btn-large" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="lblStatusID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatusID" runat="server" Text='<%# Eval("StatusID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ScreeningInstrumentID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblScreeningInstrumentID" runat="server" Text='<%# Eval("ScreeningInstrumentID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AssignedTo" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                    <div id="dvBatchAllocation" runat="server" style="border: 1px solid black; padding-left: 10%; width: 700px; padding: 10px; text-align: center; v">
                                                        No Batch Allocation Found
                                                    </div>

                                                    <%-- Screener Select Dialog --%>
                                                    <asp:Panel runat="server" ID="pnlScreeners" CssClass="modal-dialog" Width="460px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Label4" Text="Screeners"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Repeater ID="repScreener" runat="server" OnItemCommand="repScreener_ItemCommand">
                                                                        <ItemTemplate>
                                                                            <asp:Button ID="btnScreener" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                                                                CommandName="Id"
                                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID")%>'
                                                                                Text='<%# DataBinder.Eval(Container.DataItem, "UserName")%>'
                                                                                CausesValidation="false" />
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>

                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vgSubmit" DisplayMode="BulletList" ShowSummary="false" ShowMessageBox="true" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td style="text-align: right;">
                                                <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Blue" Style="padding-right: 10px;"></asp:Label>

                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>



                    </table>


                    <%-- Information Dialog --%>
                    <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray; margin-top: 25%;">
                        <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                            <div>
                                <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                <b>Information</b>
                            </div>
                        </asp:Panel>
                        <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
                            id="MessageDiv" runat="server">
                        </div>
                        <div style="padding-top: 10px">
                            <p style="text-align: center; font-family: sans-serif;">
                                <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                        PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
                    </ajaxToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
