﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="TrackingOrder.aspx.cs" Inherits="Corpt.TrackingOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        body {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }

        #dvTrackingData th {
            color: White;
            background-color: #5377A9;
            font-family: Cambria;
            font-size: 13px;
        }

        a {
            cursor: pointer;
        }

        .dt-button {
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4) !important;
            border-color: #e4e7eb !important;
            color: white !important;
            padding: 10px 20px;
            font-size: 12px !important;
            /*
		font-weight: bold !important;
		border-radius: 5px !important;*/
        }

        .dataTables_filter input, select {
            width: 100px !important;
        }

        #filterTable thead th, thead th:hover {
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4) !important;
            border-color: #e4e7eb !important;
        }

        .table.dataTable tbody td {
            padding: 2px !important;
        }

        .table.dataTable.display tbody tr td {
            text-align: center;
        }

        table th {
            background-color: #5377A9 !important;
            color: white !important;
        }

        .dataTables_wrapper .dataTables_filter {
            float: left !important;
            text-align: left !important;
        }

        .dataTables_info {
            float: none !important;
            font-weight: bold;
        }

        .dataTables_paginate {
            float: left !important;
            font-weight: bold;
        }

        #master_contentplaceholder {
            width: 150% !important;
        }
    </style>

    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet" />
    <script src="Style/select2.min.js"></script>

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.24/themes/start/jquery-ui.css" />
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>

    <div class="demoarea">
        
        <div id="loader" style="display:none;">
            <img src="Images/ajaxImages/loader.gif" width="20px" alt="Loading..." />
            <b>Please, wait...</b>
        </div>

        <div class="demoheading">
            Tracking Order
        </div>
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left;">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td colspan="2" style="float: left;">
                                            <fieldset class="border p-2" style="margin-bottom: 10px; float: right; border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; text-align: left;">
                                                <legend class="w-auto" style="width: max-content; padding-left: 0px; padding-right: 0px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;">
                                                    <label style="font-size: 12px; margin-bottom: 0px;">Search</label></legend>

                                                <div class="input-group" style="margin-bottom: 4px;">
                                                    <label class="form-control" style="padding-right: 43px;">Office Name:</label>
                                                    <asp:DropDownList ID="ddlOffice" runat="server" DataTextField="OfficeName"
                                                        DataValueField="OfficeID" CssClass="input-group-text"
                                                        ToolTip="Offices List" Width="170" Height="26px" />
                                                </div>

                                                <div class="input-group" style="margin-top: 4px;">
                                                    <label class="form-control" style="padding-right: 23px;">Customer Name:</label>
                                                    <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                                        DataValueField="CustomerId" CssClass="input-group-text"
                                                        ToolTip="Customers List" Width="255px" Height="26px" />
                                                </div>
                                                <div class="input-group">
                                                    <label class="form-control">Request Date:</label>
                                                    <span class="input-group-text" style="border-radius: 0px;">From</span>
                                                    <input id="txtTakeInDateFrom" type="text" class="form-control" placeholder="From Date" aria-label="From Date" style="width: 105px; flex: inherit;">
                                                    <span class="input-group-text" style="border-radius: 0px;">To</span>
                                                    <input id="txtTakeInDateTo" type="text" class="form-control" placeholder="To Date" aria-label="To Date" style="width: 105px; flex: inherit;">
                                                    <input id="btnSubmit" type="button" value="Submit" class="dt-button form-control " style="width: 100px; height: 30px; flex: inherit; padding: 0px;" />
                                                </div>
                                                <div>
                                                    <label id="lblMsg" style="color: red;"></label>
                                                    <asp:HiddenField ID="hdnAuthorOfficeID" runat="server" />
                                                </div>
                                            </fieldset>
                                        </td>
                                        <td style="vertical-align: top; padding-left: 10px;">
                                            <fieldset class="border p-2" style="margin-bottom: 10px; float: right; border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; text-align: left;">
                                                <legend class="w-auto" style="width: max-content; padding-left: 0px; padding-right: 0px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;">
                                                    <label style="font-size: 12px; margin-bottom: 0px;">Search</label></legend>
                                                <div class="input-group">
                                                    <label class="form-control" style="padding-right: 15px;">OrderCode:</label>
                                                    <input id="txtOrderCode" type="text" class="form-control" />
                                                    <input id="btnOrderSubmit" type="button" value="Submit" class="dt-button form-control " style="width: 100px; height: 30px; flex: inherit; padding: 0px;" />

                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left: 20px; vertical-align: top;">
                                <div class="row justify-content-center" style="width: -webkit-fill-available; visibility: hidden; width: 90% !important;" id="dvDataGrid">
                                    <div class="category-filter">
                                        <select id="categoryFilter" class="form-control text-left">
                                            <option value="">Show All</option>
                                            <option value="Not Take-In">Not Take-In</option>
                                            <option value="Ready to Pickup">Ready to Pickup</option>
                                            <option value="Account">Account</option>
                                            <option value="Billing">Billing</option>
                                            <option value="Cancel">Cancel</option>
                                            <option value="Clarity">Clarity</option>
                                            <option value="Closed">Closed</option>
                                            <option value="Color">Color</option>
                                            <option value="Color Stones">Color Stones</option>
                                            <option value="Counting">Counting</option>
                                            <option value="Dia">Dia</option>
                                            <option value="Distribution">Distribution</option>
                                            <option value="EndSession">EndSession</option>
                                            <option value="Front">Front</option>
                                            <option value="FTD">FTD</option>
                                            <option value="Further Testing">Further Testing</option>
                                            <option value="GiveOut">GiveOut</option>
                                            <option value="Grading">Grading</option>
                                            <option value="Inscription">Inscription</option>
                                            <option value="Invoice">Invoice</option>
                                            <option value="Itemising">Itemising</option>
                                            <option value="Lab">Lab</option>
                                            <option value="Measure">Measure</option>
                                            <option value="Order Update">Order Update</option>
                                            <option value="Packing">Packing</option>
                                            <option value="PSX">PSX</option>
                                            <option value="QC">QC</option>
                                            <option value="R&D">R&D</option>
                                            <option value="Repacking">Repacking</option>
                                            <option value="Sarin">Sarin</option>
                                            <option value="Screening">Screening</option>
                                            <option value="ShipIn">ShipIn</option>
                                            <option value="ShipOut">ShipOut</option>
                                            <option value="Weight">Weight</option>

                                        </select>
                                    </div>
                                    <table id="filterTable" class="table table-striped table-bordered table-hover display nowrap">
                                        <thead class="text-white" style="">
                                            <tr>
                                                <th scope="col" style="color: white;">Sr.No.</th>
                                                <th scope="col" style="color: white;">Request ID</th>
                                                <th scope="col" style="color: white;">Request Date</th>
                                                <th scope="col" style="color: white;">Order Code</th>
                                                <th scope="col" style="color: white;">TakeIn Date</th>
                                                <th scope="col" style="color: white;">Packing Date</th>
                                                <th scope="col" style="color: white;">GiveOut Date</th>
                                                <th scope="col" style="color: white;">Service Type</th>
                                                <th scope="col" style="color: white;">Status</th>
                                                <th scope="col" style="color: white;">Total Qty</th>
                                                <th scope="col" style="color: white;">Total Weight</th>
                                                <th scope="col" style="color: white;">Measure Unit</th>
                                                <th scope="col" style="color: white;">SKU Name</th>
                                                <th scope="col" style="color: white;">Style Name</th>
                                                <th scope="col" style="color: white;">Customer Name</th>
                                                <th scope="col" style="color: white;">Retailer Name</th>
                                                <th scope="col" style="color: white;">SpecialInstructions</th>
                                                <th scope="col" style="color: white;">OfficeName</th>

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; float: left;">
                                <div id="dvTrackingOrderData">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
        <script type="text/javascript">

            $(document).ready(function () {
                // Show loader when an AJAX request starts
                $(document).ajaxStart(function () {
                    $("#loader").show();
                });

                // Hide loader when an AJAX request completes
                $(document).ajaxStop(function () {
                    $("#loader").hide();
                });
            });

            $("#txtTakeInDateFrom").datepicker({
                numberOfMonths: 1,
                dateFormat: 'mm/dd/yy',
                onSelect: function (selectedDate) {
                    var selectedDateParse = $.datepicker.parseDate('mm/dd/yy', selectedDate);
                    var minDate = new Date("2022-01-01 00:00:00");
                    if (selectedDateParse < minDate) {
                        alert("Selected date must be equal to or later than " + minDate.toDateString());
                        $(this).datepicker("setDate", minDate);
                    }
                    var dt = new Date(selectedDate);
                    dt.setDate(dt.getDate());
                    $("#txtTakeInDateTo").datepicker("option", "minDate", dt);
                }
            }).datepicker('setDate', '0');

            $("#txtTakeInDateTo").datepicker({
                numberOfMonths: 1,
                dateFormat: 'mm/dd/yy',
                onSelect: function (selectedDate) {
                    var selectedDateParse = $.datepicker.parseDate('mm/dd/yy', selectedDate);
                    var minDate = new Date("2022-01-01 00:00:00");
                    if (selectedDateParse < minDate) {
                        alert("Selected date must be equal to or later than " + minDate.toDateString());
                        $(this).datepicker("setDate", minDate);
                    }
                    var dt = new Date(selectedDate);
                    dt.setDate(dt.getDate());
                    $("#txtTakeInDateFrom").datepicker("option", "maxDate", dt);
                }
            }).datepicker("setDate", "0");

            $("#<%=lstCustomerList.ClientID%>").select2();
            $("#<%=ddlOffice.ClientID%>").select2();

            function openTrakingOrderData(orderCode) {
                //alert(orderCode);
                var objOrderTracking = new Object();
                objOrderTracking.OrderCoder = orderCode;
                //objOrderTracking.TimezoneOffset = new Date().getTimezoneOffset();
                $.ajax({
                    type: "POST",
                    url: 'TrackingOrder.aspx/GetOrderTrackingDetail',
                    data: JSON.stringify(objOrderTracking),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        //alert(JSON.stringify(JSON.parse(data.d)));
                        const myObj = JSON.parse(data.d);
                        let text = "<br/><div class='demoheading' style='text-align:center;'>" + orderCode + " Tracking order detail</div>";
                        text += "<table border = '1' class='table table-striped table-bordered table-hover' > "
                        text += "<th>OrderEventName</th>";
                        text += "<th>RecordTimeStamp</th>";
                        text += "<th>UserName</th>";
                        text += "<th>OfficeCode</th>";
                        text += "<th>Status</th>";
                        for (let x in myObj) {
                            text += "<tr>";
                            text += "<td>" + myObj[x].OrderEventName + "</td>";
                            text += "<td>" + myObj[x].RecordTimeStamp + "</td>";
                            text += "<td>" + myObj[x].UserName + "</td>";
                            text += "<td>" + myObj[x].OfficeCode + "</td>";
                            text += "<td>Done</td>";
                            text += "</tr > ";
                        }
                        text += "</table>"
                        document.getElementById('dvTrackingOrderData').innerHTML = text;
                        $("#dvTrackingOrderData").show();
                    }
                });
            }
            $(document).ready(function () {
                $("#btnOrderSubmit").click(function () {
                    var orderCode = $.trim($("#txtOrderCode").val());
                    if (orderCode == "") {
                        $('#lblMsg').html("Please enter ordercode");
                        return;
                    }
                    else {
                        openTrakingOrderData(orderCode);
                        $("#dvTrackingOrderData").show();
                        $("#dvDataGrid").hide();
                    }
                });
                $("#btnSubmit").click(function () {
                    //alert('hi');
                    $("#dvTrackingOrderData").hide();
                    $("#dvDataGrid").show();
                    $('#lblMsg').html("");
                    var fromDate = $.trim($("#txtTakeInDateFrom").val());
                    var toDate = $.trim($("#txtTakeInDateTo").val());
                    var officeID = $.trim($("#<%=ddlOffice.ClientID%>").val());
                    var customerID = $.trim($("#<%=lstCustomerList.ClientID%>").val());
                    var authorOfficeID = $.trim($("#<%=hdnAuthorOfficeID.ClientID%>").val());
                    //alert(customerID);
                    if (officeID == "") {
                        $('#lblMsg').html("Please select office name");
                        return;
                    }
                    if (customerID == "") {
                        $('#lblMsg').html("Please select customer name");
                        return;
                    }
                    else if (fromDate == "") {
                        $('#lblMsg').html("Please select from date");
                        return;
                    }
                    else if (toDate == "") {
                        $('#lblMsg').html("Please select to date");
                        return;
                    }
                    else {
                        //alert("All customer");
                        var objOrderTracking = new Object();
                        objOrderTracking.TakeinDateFrom = fromDate;
                        objOrderTracking.TakeinDateTo = toDate;
                        objOrderTracking.AuthorOfficeID = authorOfficeID;
                        objOrderTracking.CustomerID = customerID;
                        objOrderTracking.RealOfficeID = officeID;
                        var filename = "GSI-OrderTracking_" + new Date().toISOString().slice(0, 10);
                        //alert(filename);
                        //alert(JSON.stringify(objOrderTracking));
                        $.ajax({
                            type: "POST",
                            url: 'TrackingOrder.aspx/GetOrderTrackingByLocation',
                            data: JSON.stringify(objOrderTracking),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            cache: false,
                            success: function (data) {
                                $('#dvDataGrid').css('visibility', 'visible');
                                if ($.fn.dataTable.isDataTable('#filterTable')) {
                                    $('#filterTable').DataTable().clear().draw();
                                    $('#filterTable').DataTable().rows.add(JSON.parse(data.d));
                                }
                                else {
                                    //alert(JSON.stringify(JSON.parse(data.d)));
                                    $('#filterTable').DataTable({
                                        data: JSON.parse(data.d),
                                        columns: [

                                            { data: 'SrNo' },
                                            { data: 'RequestID' },
                                            { data: 'RequestDate' },
                                            { data: 'GSIOrder' },
                                            { data: 'TakeinDate' },
                                            { data: 'PackingDate' },
                                            { data: 'GiveOutDate' },
                                            { data: 'ServiceTypeName' },
                                            { data: 'Status' },
                                            { data: 'TotalQty' },
                                            { data: 'TotalWeight' },
                                            { data: 'MeasureUnit' },
                                            { data: 'SKUName' },
                                            { data: 'StyleName' },
                                            { data: 'CustomerName' },
                                            { data: 'RetailerName' },
                                            { data: 'SpecialInstructions' },
                                            { data: 'OfficeName' }

                                        ],
                                        rowCallback: function (row, data, index) {
                                            var serialNumber = this.api().page() * this.api().page.len() + (index + 1);
                                            $('td:eq(0)', row).html(serialNumber);
                                        },
                                        dom: 'iBfrtp',
                                        lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
                                        buttons: [
                                            'pageLength',
                                            {
                                                extend: 'copy', text: 'Copy', filename: filename, title: filename
                                            },
                                            {
                                                extend: 'csv', text: 'CSV', filename: filename, title: filename
                                            },
                                            {
                                                extend: 'excel', text: 'Excel', filename: filename, title: filename
                                            },
                                            {
                                                extend: 'pdf', text: 'PDF', filename: filename, title: filename
                                            },
                                            {
                                                extend: 'print', text: 'Print', filename: filename, title: filename
                                            }
                                        ],
                                        order: [[3, 'desc']],
                                        "searching": true,
                                        "destroy": true
                                    });
                                }
                                var table = $('#filterTable').DataTable();

                                table.buttons().container().appendTo('#filterTable_wrapper .col-sm-6:eq(0)');
                                //Take the category filter drop down and append it to the datatables_filter div.
                                //You can use this same idea to move the filter anywhere withing the datatable that you want.
                                $("#filterTable_filter.dataTables_filter").append($("#categoryFilter"));

                                //Get the column index for the Category column to be used in the method below ($.fn.dataTable.ext.search.push)
                                //This tells datatables what column to filter on when a user selects a value from the dropdown.
                                //It's important that the text used here (Category) is the same for used in the header of the column to filter
                                var categoryIndex = 0;
                                $("#filterTable th").each(function (i) {
                                    if ($($(this)).html() == "Status") {
                                        categoryIndex = i; return false;
                                    }
                                });
                                //Use the built in datatables API to filter the existing rows by the Category column
                                $.fn.dataTable.ext.search.push(
                                    function (settings, data, dataIndex) {
                                        var selectedItem = $('#categoryFilter').val()
                                        var category = data[categoryIndex];
                                        if (selectedItem === "" || category.includes(selectedItem)) {
                                            return true;
                                        }
                                        return false;
                                    }
                                );
                                //Set the change event for the Category Filter dropdown to redraw the datatable each time
                                //a user selects a new filter.
                                $("#categoryFilter").change(function (e) {
                                    table.draw();
                                });
                                table.draw();
                                table.column(14).visible(true);
                            },
                            error: function (req, status, errorObj) {
                                alert(errorObj);
                                //alert('The server encountered an error processing the request.');
                            }
                        });
                    }

                });
            });
        </script>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
