using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.IO;
using System.Web.Mail;

namespace Corpt
{
	/// <summary>
	/// Summary description for LeoList.
	/// </summary>
	public partial class LeoList : Page
	{
		protected DataGrid DataGrid1;
		protected Panel Panel1;
		protected Panel Panel2;
		
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");

			var connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
#if DEBUG
			lblDebug.Text=connection.DataSource+":"+connection.Database;
#endif
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdAdd_Click(object sender, System.EventArgs e)
		{
			string strOrderNumber;
			strOrderNumber = txtOrderNumber.Text.Trim();
			if(Regex.IsMatch(strOrderNumber, @"^\d{5}$|^\d{6}$|^\d{7}$"))
			{
				if(lstOrderNumber.Items.FindByText(strOrderNumber)==null)
				{
					lstOrderNumber.Items.Add(strOrderNumber);
				}
			}
			txtOrderNumber.Text="";
		}

		protected void cmdClear_Click(object sender, System.EventArgs e)
		{
			lstOrderNumber.Items.Clear();
			cblBatches.Items.Clear();
			grdShortReport.DataSource=null;
			grdShortReport.DataBind();
			Session["batchList"]=null;
            Session["shortReports"]=null;
            Session["combinedReport"] =null;
			lblInfo.Text="";
		}

		protected void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if(lstOrderNumber.SelectedIndex!=-1)
			{
				lstOrderNumber.Items.RemoveAt(lstOrderNumber.SelectedIndex);
			}
		}

		protected void cmdShowShortReport_Click(object sender, System.EventArgs e)
		{
            cblBatches.Items.Clear();
			grdShortReport.DataSource=null;
			grdShortReport.DataBind();
			
			DataTable batchList = new DataTable();
			batchList.Columns.Add(new DataColumn("BatchID"));
			batchList.Columns.Add(new DataColumn("BatchNumber"));
			batchList.Columns.Add(new DataColumn("GroupCode"));
			batchList.Columns.Add(new DataColumn("BatchCode"));
			batchList.Columns.Add(new DataColumn("GroupID"));
			batchList.Columns.Add(new DataColumn("CPID"));
			batchList.AcceptChanges();

			foreach(ListItem li in lstOrderNumber.Items)
			{
				string strOrderNumber;
				strOrderNumber = li.Text;

				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("spGetBatchByCode");
				command.CommandType=CommandType.StoredProcedure;
				command.Connection=conn;
	
				command.Parameters.Add("@CustomerCode",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EGroupState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BGroupState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EState",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BDate",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@EDate",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@BatchCode",SqlDbType.Int).Value=DBNull.Value;
				command.Parameters.Add("@GroupCode",SqlDbType.Int).Value=Int32.Parse(li.Text);
				command.Parameters.Add("@AuthorID",SqlDbType.Int).Value=Int32.Parse(Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeID",SqlDbType.Int).Value=Int32.Parse(Session["AuthorOfficeID"].ToString());

				conn.Open();
				SqlDataAdapter da = new SqlDataAdapter(command);
				command.CommandTimeout=0;
				DataTable dt = new DataTable();
				da.Fill(dt);

				if(dt.Rows.Count<1)
				{
					lblInfo.Text+=" Could not load batches for order "+li.Text;
					return;
				}


				foreach(DataRow dr in dt.Copy().Rows)
				{
					DataRow ddr = batchList.NewRow();
					ddr["BatchID"]=dr["BatchID"];
					ddr["BatchNumber"] = Utils.FillToFiveChars(dr["GroupCode"].ToString()) + Utils.FillToThreeChars(dr["BatchCode"].ToString(), dr["GroupCode"].ToString());
					ddr["GroupCode"] = dr["GroupCode"];
					ddr["BatchCode"] = dr["BatchCode"];
					ddr["GroupID"] = dr["GroupID"];
					ddr["CPID"] = dr["CPID"];
					batchList.Rows.Add(ddr);
				}
			}

			cblBatches.Items.Clear();

			cblBatches.DataSource= batchList;
			cblBatches.DataTextField="BatchNumber";
			cblBatches.DataValueField="BatchID";
			cblBatches.DataBind();
			Session["batchList"]=batchList.Copy(); // I have the list of batches here.

			Hashtable shortReports = new Hashtable();			
			
			foreach(ListItem li in cblBatches.Items) ///clean out batches without short report
			{
				string b = "";
				if(!Utils.IsShortReport(li.Value, this, out b))
				{
					(Session["batchList"] as DataTable).Rows.Remove((Session["batchList"] as DataTable).Select("BatchID="+li.Value)[0]);
					lblInfo.Text="Some batches were discarded - Short report is not attached to the customer program.";
					return;
				}
				else
				{

					SqlCommand commandCheck = new SqlCommand("spGetItemStructureByBatchID");
					commandCheck.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					commandCheck.CommandType=CommandType.StoredProcedure;
				
					commandCheck.Parameters.Add(new SqlParameter("@BatchID", li.Value));
					SqlDataAdapter da = new SqlDataAdapter(commandCheck);
					DataTable dtItemStructure = new DataTable();
					da.Fill(dtItemStructure);

					Session["BatchShortReportII.dtItemStructure"] = dtItemStructure;
					DataTable sdt = Utils.BatchShortReport(li.Value, this); 
					Session["BatchShortReportII.dtItemStructure"] = null;
					
					//check here if the short report was sucsessfull
					try
					{
						if(sdt.Rows.Count<1)
						{
							Exception up = new Exception("Zero-row Short Report.");
							throw(up);
						}
					}
					catch(Exception ex)
					{
						lblInfo.Text+="Failed while doing short report: "+ex.Message;
					}
					
					shortReports.Add(li.Value, sdt); // hashtable that has all the tables of short reports.
				}
			}
			cblBatches.DataSource=(Session["batchList"] as DataTable).Copy();
			cblBatches.DataBind();
//			dgDebug1.DataSource=(Session["batchList"] as DataTable).Copy();
//			dgDebug1.DataBind();
			Session["shortReports"] = shortReports;

			DataTable combinedReport = new DataTable(); 
			foreach(DataColumn dc in Utils.BatchShortReportWithOldItemNumber(cblBatches.Items[0].Value, this).Columns) //change BatchShortReportWithOldItemNumber vs BatchShortReport
			{
				combinedReport.Columns.Add(dc.ColumnName);
			}

			// new - adding CPResult to the picture
			DataTable dtCPRulesResult = new DataTable();
			dtCPRulesResult=GetCPRulesForListOfBatches((Session["batchList"] as DataTable).Copy());

			foreach(DataRow drCPRules in dtCPRulesResult.Rows)
			{
				drCPRules["OldItemNumber"] = drCPRules["OldItemNumber"].ToString().Replace(".","");
			}
			dtCPRulesResult.AcceptChanges();
			
			//show cpresult for debug 
//			dgDebug2.DataSource=dtCPRulesResult;
//			dgDebug2.DataBind();
			//-----------------------------


			foreach(ListItem li in cblBatches.Items)
			{                
				SqlCommand commandCheck = new SqlCommand("spGetItemStructureByBatchID");
				commandCheck.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				commandCheck.CommandType=CommandType.StoredProcedure;
				
				commandCheck.Parameters.Add(new SqlParameter("@BatchID", li.Value));
				SqlDataAdapter da = new SqlDataAdapter(commandCheck);
				DataTable dtItemStructure = new DataTable();
				da.Fill(dtItemStructure);

				Session["BatchShortReportII.dtItemStructure"] = dtItemStructure;
//				DataTable dt = Utils.BatchShortReport(li.Value,this); //this is where we get the sort report we use later.
				DataTable dt = Utils.BatchShortReportWithOldItemNumber(li.Value,this); //this is where we get the sort report we use later -  now with oldItemNumberColumn.
				Session["BatchShortReportII.dtItemStructure"] = null;
				foreach(DataRow dr in dt.Rows)
				{
					combinedReport.Rows.Add(dr.ItemArray);
				}
			}
						
			grdShortReport.DataSource = combinedReport;

			combinedReport = FailByCpRules(combinedReport,dtCPRulesResult);

			combinedReport.Columns.Add(new DataColumn("LineChecked",System.Type.GetType("System.Boolean")));
			combinedReport.AcceptChanges();
			foreach(DataRow dr in combinedReport.Rows)
			{
				dr["LineChecked"]=!CheckedOrNotChecked(dr["Result"]);
			}
			
			Session["combinedReport"] = combinedReport;
			grdShortReport.DataBind();
		}

		public string ExtendUtils(object o)
		{
			return Utils.NullValue(o);
		}

		public bool CheckedOrNotChecked(object o)
		{
			if(Utils.NullValue(o)=="")
			{
				return false;
			}
			return true;
		}

		protected void ItemCheckBoxClicked(object sender, System.EventArgs e)
		{
			System.Web.UI.WebControls.DataGridItem diSender;
			diSender = ((sender as System.Web.UI.WebControls.CheckBox).Parent as System.Web.UI.WebControls.TableCell).Parent as System.Web.UI.WebControls.DataGridItem;

			DataTable dtCombinedReport = new DataTable();
			dtCombinedReport = Session["combinedReport"] as DataTable;

			dtCombinedReport.Rows[diSender.DataSetIndex]["LineChecked"]=(sender as System.Web.UI.WebControls.CheckBox).Checked;
			grdShortReport.DataSource=dtCombinedReport;
			Session["combinedReport"]=dtCombinedReport;
			grdShortReport.DataBind();
			
		}

		private DataTable FailByCpRules(DataTable dtShortReport, DataTable dtCPrulesResult)
		{
			return dtShortReport;
		}

		protected void cmdSaveExcel_Click(object sender, System.EventArgs e)
		{
			DataTable dtShorReport = (Session["combinedReport"] as DataTable).Copy();
		
			if(Directory.Exists(Session["TempDir"].ToString()+Session.SessionID + @"\"))
				Directory.Delete(Session["TempDir"].ToString()+Session.SessionID + @"\",true);
			Directory.CreateDirectory(Session["TempDir"].ToString()+Session.SessionID + @"\");
			DataSet ds = new DataSet();
			ds.Tables.Add(dtShorReport);
			Utils.SaveExcell(ds, this);
			
			string strNewFileName = DateTime.Now.Ticks.ToString();

			File.Move(Session["TempDir"].ToString()+Session.SessionID + @"\Report.xls",Session["TempDir"].ToString()+Session.SessionID + @"\" + strNewFileName +".xls");
			Response.Redirect("Temp/"+Session.SessionID+"/"+ strNewFileName + ".xls");

		}

		protected void cmdSendToSasha_Click(object sender, System.EventArgs e)
		{
			DataTable dtIncoming = Session["combinedReport"] as DataTable;
			DataTable dtSend = new DataTable();
			dtSend.Columns.Add(new DataColumn("ReportNumber"));
			dtSend.AcceptChanges();

			foreach(DataRow dr in dtIncoming.Rows)
			{
				if(bool.Parse(dr["LineChecked"].ToString()))
				{
					DataRow drSend = dtSend.NewRow();
					drSend["ReportNumber"]=dr["Cert #"].ToString();
					dtSend.Rows.Add(drSend);
				}
			}

			//prepare folder and excel file for email
			string strDirectoryName = Session["TempDir"].ToString()+Session.SessionID + @"\";
			if(Directory.Exists(strDirectoryName))
			{
				Directory.Delete(strDirectoryName,true);
			}
			Directory.CreateDirectory(strDirectoryName);
			DataSet ds = new DataSet();
			ds.DataSetName = "FilesToPrintList";
			ds.Tables.Add(dtSend);
			Utils.SaveExcell(ds, this);

			//prepare xml file to send
			DataSet dsXml = new DataSet("ItemToPrint");
			DataTable dtXml = new DataTable();
			dtXml=dtSend.Copy();			
			dtXml.TableName="Item";
			dtXml.Columns[0].ColumnName="ItemsList";
			dtXml.AcceptChanges();
            dsXml.Tables.Clear();
			dsXml.Tables.Add(dtXml);
			dsXml.WriteXml(strDirectoryName+"ItemsToPrint.xml");

			SendEmail(strDirectoryName+"Report.xls", strDirectoryName+"ItemsToPrint.xml");

			DataSet dtItemSet = new DataSet("ItemSet");			
			DataTable dtItems = new DataTable("Items");
			dtItemSet.Tables.Add(dtItems);

			dtItems.Columns.Add(new DataColumn("SRP"));
			dtItems.Columns.Add(new DataColumn("SIDD_PRICE"));
			dtItems.Columns.Add(new DataColumn("NEW_SKU"));
			dtItems.Columns.Add(new DataColumn("XREF"));
			dtItems.Columns.Add(new DataColumn("REPORT_TYPE"));
			dtItems.Columns.Add(new DataColumn("ITEM"));
			dtItems.Columns.Add(new DataColumn("GroupCode"));
			dtItems.Columns.Add(new DataColumn("BatchCode"));
			dtItems.Columns.Add(new DataColumn("ItemCode"));

			dtItems.AcceptChanges();

			foreach(DataRow dr in dtSend.Rows)
			{
				DataRow drDest = dtItems.NewRow();
				drDest["SRP"]=DBNull.Value;
				drDest["SIDD_PRICE"]=DBNull.Value;
				drDest["NEW_SKU"]=DBNull.Value;
				drDest["XREF"]=DBNull.Value;
				drDest["REPORT_TYPE"]=DBNull.Value;
				drDest["ITEM"]=dr["ReportNumber"].ToString();

				Utils.DissectItemNumber(dr["ReportNumber"].ToString(), out int intGroupCode, out int intBatchCode, out int intItemCode);
				drDest["GroupCode"]= intGroupCode;
				drDest["BatchCode"]= intBatchCode;
				drDest["ItemCode"]= intItemCode;
				
				dtItems.Rows.Add(drDest);
			}

			SqlCommand command = new SqlCommand("sp_GetItemCodeBatchIDPartID");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@ItemsList",dtItemSet.GetXml()));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataSet dsCo = new DataSet();
			da.Fill(dsCo);

			DataSet dsOrderDocs = new DataSet("SID_Docs");
			DataTable dtOrderDocs = new DataTable("Operation");
			dsOrderDocs.Tables.Add(dtOrderDocs);

			dtOrderDocs.Columns.Add(new DataColumn("AuthorId"));
			dtOrderDocs.Columns.Add(new DataColumn("AuthorOfficeId"));
			dtOrderDocs.Columns.Add(new DataColumn("CurrentOfficeId"));
			dtOrderDocs.Columns.Add(new DataColumn("OperationTypeOfficeID"));
			dtOrderDocs.Columns.Add(new DataColumn("OperationTypeID"));
			dtOrderDocs.Columns.Add(new DataColumn("BatchID"));
			dtOrderDocs.Columns.Add(new DataColumn("ItemCode"));
			dtOrderDocs.AcceptChanges();

//			dgDebug.DataSource=dsCo.Tables[1];
//			dgDebug.DataBind();

			DataRow[] drsSelected = dsCo.Tables[1].Select("OperationChar = 'R' or OperationChar = 'T' or OperationChar = 'D'");

			foreach(DataRow dr in drsSelected)
			{
				DataRow drToOrder = dtOrderDocs.NewRow();
				drToOrder["AuthorId"]= Session["ID"].ToString();
				drToOrder["AuthorOfficeId"]=Session["AuthorOfficeId"].ToString();
				drToOrder["CurrentOfficeId"]=dr["OperationTypeOfficeID"].ToString();
				drToOrder["OperationTypeOfficeID"]=dr["OperationTypeOfficeID"].ToString();
				drToOrder["OperationTypeID"]=dr["OperationTypeID"].ToString();
				drToOrder["BatchID"]=dr["BatchID"].ToString();
				drToOrder["ItemCode"]=dr["NewItemCode"].ToString();
				dtOrderDocs.Rows.Add(drToOrder);
			}

			OrderReports(dsOrderDocs);
			PushTheQueue(dtSend);
			lblInfo.Text="<h3>Reports ordered.</h3>";
		}

		private void PushTheQueue(DataTable dtOrder)
		{
			DataSet ds = new DataSet("dsItems");
			ds.Tables.Add(dtOrder.Copy());
			ds.Tables[0].TableName="dtItems";

			SqlCommand command = new SqlCommand("sp_EnqueueLeo");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@ItemNumbers",ds.GetXml()));
			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
		}

		private void OrderReports(DataSet dsOrder)
		{
			SqlCommand command = new SqlCommand("sp_SetItemsDocsFromList");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@ItemsDocs",dsOrder.GetXml()));
			//execute command here and hope for the best:
			try
			{
				SqlDataAdapter da= new SqlDataAdapter(command);
				DataSet ds = new DataSet();
				da.Fill(ds);
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(ex.Message);
				lblInfo.Text = ex.Message;
			}
//			Exception up = new Exception("Something went wrong");
//			throw up; //just because i can:)
		}

		private void SendEmail(string strFileName)
		{
			try
			{
				MailMessage myEmail = new MailMessage();

				myEmail.From = "noreply@gemscience.net";
				myEmail.BodyFormat=MailFormat.Html;
				myEmail.BodyEncoding=System.Text.Encoding.UTF8;
				myEmail.Attachments.Add(new MailAttachment(strFileName));

				myEmail.To="oleg@gemscience.net; sasha@gemscience.net; debbie@gemscience.net";
				//myEmail.To="oleg@gemscience.net";

				myEmail.Body="List of files to sent to Leo Schachter is attached.";
				myEmail.Body+="<br />Logged in user "+Session["FirstName"].ToString()+" "+Session["LastName"].ToString()+".";
				myEmail.Subject="List for Leo Shcackter";

				SmtpMail.SmtpServer="testserver.gsi.local";
				SmtpMail.Send(myEmail);

//				lblInfo.Text = "Email sent successfully.";
			}
			catch(Exception ex)
			{
				lblInfo.Text = "There was a problem sending the email. Please try again.";
			}
		}

		private void SendEmail(string strFileName, string strSecondFileName)
		{
			try
			{
				MailMessage myEmail = new MailMessage();

				myEmail.From = "noreply@gemscience.net";
				myEmail.BodyFormat=MailFormat.Html;
				myEmail.BodyEncoding=System.Text.Encoding.UTF8;
				myEmail.Attachments.Add(new MailAttachment(strFileName));
				myEmail.Attachments.Add(new MailAttachment(strSecondFileName));

				myEmail.To="oleg@gemscience.net; sasha@gemscience.net; debbie@gemscience.net";
				//myEmail.To="oleg@gemscience.net";

				myEmail.Body="List of files to sent to Leo Schachter is attached.";
				myEmail.Body+="<br />Logged in user "+Session["FirstName"].ToString()+" "+Session["LastName"].ToString()+".";
				myEmail.Subject="List for Leo Schachter";

				SmtpMail.SmtpServer="testserver.gsi.local";
				SmtpMail.Send(myEmail);

				//				lblInfo.Text = "Email sent successfully.";
			}
			catch(Exception ex)
			{
				lblInfo.Text = "There was a problem sending the email. Please try again.";
			}
		}

		protected void txtOrderNumber_TextChanged(object sender, System.EventArgs e)
		{
			cmdAdd_Click(sender, e);
		}

		protected void cmdEmailOnly_Click(object sender, System.EventArgs e)
		{
			DataTable dtIncoming = Session["combinedReport"] as DataTable;
			DataTable dtSend = new DataTable();
			dtSend.Columns.Add(new DataColumn("ReportNumber"));
			dtSend.AcceptChanges();

			foreach(DataRow dr in dtIncoming.Rows)
			{
				if(bool.Parse(dr["LineChecked"].ToString()))
				{
					DataRow drSend = dtSend.NewRow();
					drSend["ReportNumber"]=dr["Cert #"].ToString();
					dtSend.Rows.Add(drSend);
				}
			}

			string strDirectoryName = Session["TempDir"].ToString()+Session.SessionID + @"\";
			if(Directory.Exists(strDirectoryName))
			{
				Directory.Delete(strDirectoryName,true);
			}
			Directory.CreateDirectory(strDirectoryName);
			DataSet ds = new DataSet();
			ds.DataSetName = "FilesToPrintList";
			ds.Tables.Add(dtSend);
			Utils.SaveExcell(ds, this);
			SendEmail(strDirectoryName+"Report.xls");
		}

		private DataTable GetCPRulesForListOfBatches(DataTable dtBatchList)
		{
			DataTable dtCombinedCpRules = new DataTable();
			DataSet dsRules = new DataSet();

			foreach(DataRow dr in dtBatchList.Rows)
			{
				string strPreparedBatchNumber = dr["BatchNumber"].ToString()+"00";

				SqlCommand command = new SqlCommand("sp_RulesTracking24");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter("@BatchFullNumber", strPreparedBatchNumber));
				command.Parameters.Add(new SqlParameter("@ShowRules", "0"));

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtTemp = new DataTable();

				da.Fill(dtTemp);
				dsRules.Tables.Add(dtTemp);
			}


			//dtCombinedCpRules.Columns.AddRange(dsRules.Tables[0].Columns);
			foreach(DataColumn dc in dsRules.Tables[0].Columns)
			{
				dtCombinedCpRules.Columns.Add(dc.ColumnName);
			}

			dtCombinedCpRules.AcceptChanges();

			foreach(DataTable dt in dsRules.Tables)
			{
				foreach(DataRow dr in dt.Rows)
				{
					 dtCombinedCpRules.Rows.Add(dr.ItemArray);
				}
			}
			return dtCombinedCpRules;
		}
	}
}
