﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" 
    CodeBehind="SyntheticShortStats.aspx.cs" Inherits="Corpt.SyntheticShortStats" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style type="text/css">
        .ChkBoxClass input {width:30px; height:40px;}
        .GridPager a, .GridPager span
        {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }
        
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        
        .GridPager span
        {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }
        
        .divCol
        {
            float: left;
            width: 190px;
        }
        
        .divBottom
        {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        $().ready(function () {
        
            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtGsiOrder.ClientID%>').val().trim() == "") {
                    alert('Please fill out order field.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtVendorNum.ClientID%>').val().trim() == "") {
                    alert('Please fill out vendor number field.'); $('#<%=txtVendorNum.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtMemoNum.ClientID%>').val().trim() == "") {
                    alert('Please fill out Memo number field..'); $('#<%=txtMemoNum.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQtyPass.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity pass field..'); $('#<%=txtQtyPass.ClientID%>').focus(); return false;
                }
                    else if (parseInt($('#<%=txtTotalQTY.ClientID%>').val()) != parseInt($('#<%=txtQtyPass.ClientID%>').val()) + parseInt($('#<%=txtQtySynth.ClientID%>').val()) + parseInt($('#<%=txtQtySusp.ClientID%>').val())) {
                    alert('Please fill out valid entry.\nTotal quantity is not equal to quantity pass plus quantity Synth plus quantity Susp.'); $('#<%=txtQtyPass.ClientID%>').focus(); return false;
                }
                else {
                    return true;
                }
            });

            $('#<%=txtCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();

            $('#<%=txtToCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();
        });

        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>

    <div class="demoarea">
        <h2 style="margin-left: 10px; margin-bottom: 10px; margin-top: 10px" class="demoheading">
            Short Synthetic Statistics
        </h2>
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">
                <div class="divCol" style="width: 170px;">
                    
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtGsiOrder">
                            GSI Order #:</label>
                        <div>
                            <input id="hdnScreeningID" runat="server" type="hidden" />
                            <input id="txtGsiOrder" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter GSI Order #" maxlength="7" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label " for="txtVendorNum">
                            Vendor #:</label>
                        <div>
                            <input id="txtVendorNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter Vendor #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtMemoNum">
                            MEMO #:</label>
                        <div>
                            <input id="txtMemoNum" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter Memo #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="width: 170px; margin-top: 25px;">
                    </div>
                </div>
                <div class="divCol" style="margin-right: 55px;">
                    <div class="form-group" style="margin-bottom: 7px; width: 170px;">
                        <label class="control-label" for="txtRetailer">
                            Retailer #:</label>
                        <div>
                            <input id="txtRetailer" runat="server" type="text" class="form-control form-control-height"
                                placeholder="Enter Retailer #" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtNotes">
                            Notes:</label>
                        <div>
                            <textarea id="txtNotes" runat="server" class="form-control form-control-height" rows="3"
                                placeholder="Enter Notes" maxlength="1000" style="resize: none; width: 205px;"></textarea>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtTotalQTY">
                                        Total QTY:</label>
                                    <div>
                                        <input id="txtTotalQTY" runat="server" type="text" class="form-control form-control-height" readonly="true"
                                            placeholder="Enter Total QTY" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyPass">
                                        Quantity Pass:</label>
                                    <div>
                                        <input id="txtQtyPass" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Pass" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <!--
                        <tr>
                            
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtyFail">
                                        Quantity Fail:</label>
                                    <div>
                                        <input id="txtQtyFail" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Fail" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td style="padding-right: 7px;">
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtySynth">
                                        Quantity Synthetic:</label>
                                    <div>
                                        <input id="txtQtySynth" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Synth" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtQtySusp">
                                        Quantity Suspect:</label>
                                    <div>
                                        <input id="txtQtySusp" runat="server" type="text" class="form-control form-control-height"
                                            placeholder="Enter QTY Susp" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtBagQty">
                                        QTY per BAG:</label>
                                    <div>
                                        <input id="txtBagQty" runat="server" type="text" class="form-control form-control-height" 
                                            placeholder="QTY per BAG" ontextchanged="changeBagQty" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin-bottom: 7px;">
                                    <label class="control-label" for="txtBagNum">
                                        Number of Bags:</label>
                                    <div>
                                        <input id="txtBagNum" runat="server" type="text" class="form-control form-control-height" readonly="true"
                                            placeholder="Number of Bags" maxlength="5" style="width: 90px;" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    
                    <div class="form-group" style="margin-bottom: 7px;">
                        <div style="text-align: left;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save"
                                OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="btnLoad" runat="server" CssClass="btn btn-primary" Text="Load"
                                OnClick="btnLoad_ServerClick"></asp:Button>
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary" Text="Clear"
                                OnClick="btnShort_ServerClick"></asp:Button>
                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back" Style="display: none;"
                                OnClick="btnBack_Click"></asp:Button>
                            
                            <div style="margin-top: 20px;">
                            <asp:TreeView ID="OfficeTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="large" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                ShowLines="False" ShowExpandCollapse="true">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <div style="float: left; width: 600px;">
                    <div class="form-grouph" style="margin-bottom: 7px;">
                        <label class="control-label" for="gsiOrderFilter">
                            Filter By GSI Order# / Create Date:</label>
                        <div style="padding-bottom: 5px; display: block;width:800px;">
                            <input id="gsiOrderFilter" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter Order#" maxlength="7" />
                            
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnSearch_Click">
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                            <input id="retailerSearch" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter Retailer#" maxlength="6" />
                             <asp:Button ID="btnDestSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" 
                                onclick="btnDestSearch_Click" >
                            </asp:Button>
                            
                            <!--
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                            <input id="userSearch" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Enter User" maxlength="6" />
                            <asp:Button ID="btnUserSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" 
                                onclick="btnDestSearch_Click" >
                            </asp:Button>
                            -->
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"></span>
                            <input id="txtCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="From Date"
                                readonly />
                            <input id="txtToCreateDateSearch" runat="server" style="width: 12%; display: inline;"
                                type="text" class="form-control form-control-height" placeholder="To Date"
                                readonly />
                            <asp:Button ID="btnCreateDateSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCreateDateSearch_Click">
                            </asp:Button>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"></span>
                            <asp:CheckBox ID="ClosedOnly" runat="server" Text="Closed Only" CssClass="ChkBoxClass" style="margin-left: 50px; float: right;"
                                    Checked="False" />
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;"></span>
                            <asp:Button ID="btnClosedSearch" runat="server" CssClass="btn btn-primary" Text="Closed Today Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCLosedSearch_Click">
                            </asp:Button> 
                            <asp:Button ID="btnYesterdaySearch" runat="server" CssClass="btn btn-primary" Text="Yesterday Search"
                                Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnYesterdaySearch_Click">
                            </asp:Button>
                            <!--CssClass="radio-inline"checkbox-->
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;">
                            </span>
                            <!--
                            <asp:Panel runat="server" ID="SuspNumber">
                                        <asp:Label runat="server" Text="Suspect" style="float: left; clear:both;"></asp:Label>
								        <asp:TextBox runat="server" ID="SuspNumberBox" OnTextChanged="OnKeyPress" style="width: 250px; float:left; clear:both; margin-bottom: 10px;" autocomplete="off"></asp:TextBox>
                                        <ajaxToolkit:DropDownExtender runat="server" TargetControlID="SuspNumberBox" DropDownControlID="Panel2" />
                                        <asp:Panel runat="server" ID="Panel2">
                                                        <asp:ListBox runat="server" ID="SuspListBox" DataSource='<%# Eval("DropDownEnums")%>'
                                                            DataTextField="ValueTitle" DataValueField="MeasureValueId" AutoPostBack="true"
                                                            OnSelectedIndexChanged="OnKeyPress"></asp:ListBox>
                                        </asp:Panel>
                            </asp:Panel>
                            -->
                            <div class="form-group" style="margin-bottom: 7px;">
                        <label class="control-label" for="txtTotalQTY">
                            Certified By:</label>
                        <div>
                            <asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="form-control form-control-height selectpicker"
                                Style="padding-top: 2px; padding-bottom: 2px;">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Not Certified</asp:ListItem>
                                <asp:ListItem>GSI Certified</asp:ListItem>
                                <asp:ListItem>GIA Certified</asp:ListItem>
                                <asp:ListItem>GSL Certified</asp:ListItem>
                                <asp:ListItem>IGI Certified</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                            <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;">
                            </span>
                            <input id="totalRecNumber" runat="server" style="width: 12%; display: inline" type="text"
                                class="form-control form-control-height" placeholder="Total # Found" maxlength="6" />
                            <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px;" OnClick="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToExcelSorted" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px;" OnClick="btnExportToExcelSorted_Click" />
                        </div>
                        <div class="form-group" style="margin-bottom: 7px;">
                            <div id="mainContainer" class="container" style="padding-left: 0px;">
                                <div class="shadowBox">
                                    <div class="page-container">
                                        <div class="container" style="padding-left: 0px;">
                                            <div style="padding-bottom: 5px;">
                                                <span id="gvTitle" runat="server" class="text-info"></span>
                                            </div>
                                            <div>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="grdScreening" runat="server" Width="1200" CssClass="table striped table-bordered"
                                                        AutoGenerateColumns="False" DataKeyNames="GSIOrder" EmptyDataText="There are no data records to display."
                                                        AllowPaging="true" PageSize="20" RowStyle-Height="2px" OnPageIndexChanging="grdScreening_PageIndexChanging"
                                                        OnRowDeleting="grdScreening_RowDeleting" OnSelectedIndexChanged="grdScreening_SelectedIndexChanged"
                                                        AllowSorting="true" OnSorting="sort_table"
                                                        Height="200px">
                                                        <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                            Height="25px" />
                                                        <RowStyle Font-Size="12px" />
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" SelectText="Select" HeaderStyle-Width="30px" />
                                                            <asp:BoundField DataField="OfficeID" HeaderText="OfficeID" HeaderStyle-Width="100px"
                                                                SortExpression="OfficeID" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Memo" HeaderText="Memo" HeaderStyle-Width="40px" ItemStyle-Wrap="true"
                                                                SortExpression="Memo" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="GSIOrder" HeaderText="Order #" HeaderStyle-Width="60px"
                                                                SortExpression="GSIOrder" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Destination" HeaderText="Retailer" HeaderStyle-Width="110px"
                                                                SortExpression="Destination" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Created Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="CreatedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="ReadyDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Ready Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="ReadyDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="LastModifiedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Last Modified Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="LastModifiedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="PickupDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Pickup Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="PickupDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="80px"
                                                                SortExpression="Status" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Style" HeaderText="Style" HeaderStyle-Width="80px"
                                                                SortExpression="Style" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="TotalQty" HeaderText="Total QTY" HeaderStyle-Width="30px"
                                                                SortExpression="TotalQty" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtyPass" HeaderText="QTY Pass" HeaderStyle-Width="30px"
                                                                SortExpression="QtyPass" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtySynth" HeaderText="QTY Synthetic" HeaderStyle-Width="30px"
                                                                SortExpression="QtySynth" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="QtySusp" HeaderText="QTY Suspect" HeaderStyle-Width="30px"
                                                                SortExpression="QtySusp" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CustomerCode" HeaderText="Customer Code" HeaderStyle-Width="30px"
                                                                SortExpression="CustomerCode" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CompanyName" HeaderText="Company Name" HeaderStyle-Width="30px"
                                                                SortExpression="CompanyName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-Width="30px"
                                                                SortExpression="UserName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Notes" HeaderText="Notes" HeaderStyle-Width="30px"
                                                                SortExpression="Notes" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:GridView ID="grdClosedScreening" runat="server" Width="1200" CssClass="table striped table-bordered"
                                                        AutoGenerateColumns="False" DataKeyNames="GSIOrder" EmptyDataText="There are no data records to display."
                                                        AllowPaging="true" PageSize="20" RowStyle-Height="2px" 
                                                        AllowSorting="true" OnSorting="sort_table"
                                                        Height="200px">
                                                        <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                            Height="25px" />
                                                        <RowStyle Font-Size="12px" />
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" SelectText="Select" HeaderStyle-Width="30px" />
                                                            <asp:BoundField DataField="Memo" HeaderText="Memo" HeaderStyle-Width="40px" ItemStyle-Wrap="true"
                                                                SortExpression="Memo" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="GSIOrder" HeaderText="Order #" HeaderStyle-Width="60px"
                                                                SortExpression="GSIOrder" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Created Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="CreatedDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="80px"
                                                                SortExpression="Status" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="PickupDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Pickup Date"
                                                                HeaderStyle-Width="70px" ItemStyle-Wrap="false" SortExpression="PickupDate"
                                                                ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="TotalQty" HeaderText="Total QTY" HeaderStyle-Width="30px"
                                                                SortExpression="TotalQty" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            <asp:BoundField DataField="CompanyName" HeaderText="Company Name" HeaderStyle-Width="30px"
                                                                SortExpression="CompanyName" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
							                                <asp:BoundField DataField="Destination" HeaderText="Retailer" HeaderStyle-Width="110px"
                                                                SortExpression="Destination" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                            </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
