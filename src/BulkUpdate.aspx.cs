using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for BulkUpdate.
	/// </summary>
	public partial class BulkUpdate : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				LoadMeasures();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void LoadMeasures()
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("sp_GetEnumMeasures");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;
			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(dt);
			lstMeasure.DataSource=dt;
			lstMeasure.DataTextField = "MeasureTitle";
			lstMeasure.DataValueField="MeasureCode";
			lstMeasure.DataBind();
		}

		protected void rblMeasure_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(rblMeasure.SelectedIndex!=-1)
			{
				if(rblMeasure.SelectedValue!="2")
				{
					SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
					SqlCommand command = new SqlCommand("sp_GetMeasureValueListByMeasureCode");
					command.Connection = conn;
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@MeasureCode", SqlDbType.Int).Value=int.Parse(rblMeasure.SelectedValue);
					SqlDataAdapter da = new SqlDataAdapter(command);
					DataTable dt = new DataTable();
					da.Fill(dt);
					lstEnum.Items.Clear();
					lstEnum.DataSource=dt;
					lstEnum.DataTextField="MeasureValueName";
					lstEnum.DataValueField="MeasureValueID";
					lstEnum.DataBind();
				}
			}
		}

		protected void cmdGet_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("SELECT v0Part.PartID, v0Part.PartName FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID WHERE (v0Batch.GroupCode = '"+ order.Text.Trim()+"') AND (v0Batch.BatchCode = '"+ batch.Text.Trim()+ "')");
			command.Connection = conn;
			command.CommandType = CommandType.Text;

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			lstPartName.DataSource=dt;
			lstPartName.DataTextField  = "PartName";
			lstPartName.DataValueField = "PartID";
			lstPartName.DataBind();
			txtOrder.Text=order.Text;
		}

		protected void cmdGo_Click(object sender, System.EventArgs e)
		{
			if((txtOrder.Text!="")&&(lstPartName.SelectedIndex!=-1)&&(txtBatchFrom.Text!="")&&(txtBatchTo.Text!=""))
			{
				if((rblMeasure.SelectedValue!="1")&&(rblMeasure.SelectedValue!="2")&&(rblMeasure.SelectedValue!="10")&&(rblMeasure.SelectedValue!="96")&&(rblMeasure.SelectedValue!="107")&&(rblMeasure.SelectedValue!="124"))
				{
					SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
					SqlCommand command = new SqlCommand("sp_SetMeasuresBulk");
					command.Connection = conn;
					command.CommandType = CommandType.StoredProcedure;
					//@PartId int,
					//					@SetMeasureCode int,
					//					@SetMeasureValueID int,
					//					@SetMeasureValue float,
					//					@StringValue varchar(100),
					//					@OrderCodeIN int,
					//					@BatchCodeFrom int,
					//					@BatchCodeTo int

					command.Parameters.Add("@PartId", SqlDbType.Int).Value=int.Parse(lstPartName.SelectedValue);
					command.Parameters.Add("@SetMeasureCode", SqlDbType.Int).Value=int.Parse(rblMeasure.SelectedValue);
					command.Parameters.Add("@SetMeasureValueID", SqlDbType.Int).Value = int.Parse(lstEnum.SelectedValue);
					command.Parameters.Add("@SetMeasureValue", SqlDbType.Float).Value = DBNull.Value;
					command.Parameters.Add("@StringValue", SqlDbType.VarChar);
					command.Parameters["@StringValue"].Size=100;
					command.Parameters["@StringValue"].Value=DBNull.Value;
					command.Parameters.Add("@OrderCodeIN", SqlDbType.Int).Value = int.Parse(txtOrder.Text.Trim());
					command.Parameters.Add("@BatchCodeFrom", SqlDbType.Int).Value = int.Parse(txtBatchFrom.Text.Trim());
					command.Parameters.Add("@BatchCodeTo", SqlDbType.Int).Value = int.Parse(txtBatchTo.Text.Trim());

					conn.Open();
					command.ExecuteNonQuery();
					conn.Close();
				}
				else 
				{ 
					if((rblMeasure.SelectedValue=="1")||(rblMeasure.SelectedValue=="2")||(rblMeasure.SelectedValue=="10")||(rblMeasure.SelectedValue=="96")||(rblMeasure.SelectedValue=="107"))
					{
						SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
						SqlCommand command = new SqlCommand("sp_SetMeasuresBulk");
						command.Connection = conn;
						command.CommandType = CommandType.StoredProcedure;
						//@PartId int,
						//					@SetMeasureCode int,
						//					@SetMeasureValueID int,
						//					@SetMeasureValue float,
						//					@StringValue varchar(100),
						//					@OrderCodeIN int,
						//					@BatchCodeFrom int,
						//					@BatchCodeTo int

						command.Parameters.Add("@PartId", SqlDbType.Int).Value=int.Parse(lstPartName.SelectedValue);
						command.Parameters.Add("@SetMeasureCode", SqlDbType.Int).Value=int.Parse(rblMeasure.SelectedValue);
						command.Parameters.Add("@SetMeasureValueID", SqlDbType.Int).Value = DBNull.Value;
						command.Parameters.Add("@SetMeasureValue", SqlDbType.Float).Value = float.Parse(txtInt.Text.Trim());
						command.Parameters.Add("@StringValue", SqlDbType.VarChar);
						command.Parameters["@StringValue"].Size=100;
						command.Parameters["@StringValue"].Value=DBNull.Value;
						command.Parameters.Add("@OrderCodeIN", SqlDbType.Int).Value = int.Parse(txtOrder.Text.Trim());
						command.Parameters.Add("@BatchCodeFrom", SqlDbType.Int).Value = int.Parse(txtBatchFrom.Text.Trim());
						command.Parameters.Add("@BatchCodeTo", SqlDbType.Int).Value = int.Parse(txtBatchTo.Text.Trim());

						conn.Open();
						command.ExecuteNonQuery();
						conn.Close();
					}
					else
					{
						if(rblMeasure.SelectedValue=="124")
						{
							SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
							SqlCommand command = new SqlCommand("sp_SetMeasuresBulk");
							command.Connection = conn;
							command.CommandType = CommandType.StoredProcedure;
							//@PartId int,
							//					@SetMeasureCode int,
							//					@SetMeasureValueID int,
							//					@SetMeasureValue float,
							//					@StringValue varchar(100),
							//					@OrderCodeIN int,
							//					@BatchCodeFrom int,
							//					@BatchCodeTo int

							command.Parameters.Add("@PartId", SqlDbType.Int).Value=int.Parse(lstPartName.SelectedValue);
							command.Parameters.Add("@SetMeasureCode", SqlDbType.Int).Value=int.Parse(rblMeasure.SelectedValue);
							command.Parameters.Add("@SetMeasureValueID", SqlDbType.Int).Value = DBNull.Value;
							command.Parameters.Add("@SetMeasureValue", SqlDbType.Float).Value = DBNull.Value;
							command.Parameters.Add("@StringValue", SqlDbType.VarChar);
							command.Parameters["@StringValue"].Size=100;
							command.Parameters["@StringValue"].Value=txtInt.Text.Trim();
							command.Parameters.Add("@OrderCodeIN", SqlDbType.Int).Value = int.Parse(txtOrder.Text.Trim());
							command.Parameters.Add("@BatchCodeFrom", SqlDbType.Int).Value = int.Parse(txtBatchFrom.Text.Trim());
							command.Parameters.Add("@BatchCodeTo", SqlDbType.Int).Value = int.Parse(txtBatchTo.Text.Trim());

							conn.Open();
							command.ExecuteNonQuery();
							conn.Close();
						}
					}
				}
			}
		}

		protected void lstMeasure_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			rblMeasure.Items.Clear();
			rblMeasure.Items.Add(new ListItem(lstMeasure.SelectedItem.Text,lstMeasure.SelectedItem.Value));
			rblMeasure.SelectedIndex=0;
			rblMeasure_SelectedIndexChanged(sender,e);
		}
	}
}
