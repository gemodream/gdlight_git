﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class BulkUpdate2Old : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            //-- Load Enum Measures
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.BulkEnumMeasures);
            SaveEnumButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            OrderField.Focus();
        }

        #region Load button
        protected void OnLoadClick(object sender, EventArgs e)
        {
            ShowValuePanels(0);
            PartsListField.Items.Clear();
            MeasureListField.Items.Clear();
            MeasureEnumField.Items.Clear();

            //-- Load Similar Batches
            var batches = QueryUtils.GetSimilarBatches(OrderField.Text, BatchField.Text, this);
            BatchList.DataSource = batches;
            BatchList.DataBind();
            SetViewState(batches, SessionConstants.BulkBatches);

            //-- Load Document Item Types
            var parts = QueryUtils.GetPartsMeasure(OrderField.Text.Trim(), BatchField.Text, this);
            PartsListField.DataTextField = "PartName";
            PartsListField.DataValueField = "PartId";
            PartsListField.DataSource = parts;
            PartsListField.DataBind();

            //-- Clear preview measures
            SetViewState(null, SessionConstants.BulkMeasures);
            if (!string.IsNullOrEmpty(PartsListField.SelectedValue))
            {
                OnPartChanged(null, null);
            }
        }
        #endregion

        #region Mode section
        protected void OnModeChanged(object sender, EventArgs e)
        {
            //-- Reload Measures Combo
            //-- Clear preview measures
            SetViewState(null, SessionConstants.BulkMeasures);
            if (!string.IsNullOrEmpty(PartsListField.SelectedValue))
            {
                OnPartChanged(null, null);
            } else
            {
                MeasureListField.Items.Clear();
            }
            
        }
        #endregion

        #region Document Parts Combo
        protected void OnPartChanged(object sender, EventArgs e)
        {
            var batch = GetFirstBatch();
            if (batch == null)
            {
                MeasureListField.Items.Clear();
                return;
            }
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel>;
            if (measures == null || measures.Count == 0)
            {
                measures = QueryUtils.GetMeasures(batch, IsByCpMode(), this);
                SetViewState(measures, SessionConstants.BulkMeasures);
            }
            var byType =
                measures.FindAll(m => m.PartId == GetPartIdSelected());
            
            byType.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));
            MeasureListField.DataTextField = "MeasureName";
            MeasureListField.DataValueField = "MeasureId";

            MeasureListField.DataSource = byType;
            MeasureListField.DataBind();
            if (!string.IsNullOrEmpty(MeasureListField.SelectedValue))
            {
                OnMeasureChanged(null, null);
            } else
            {
                ShowValuePanels(0);
            }
        }
        #endregion

        #region Batch
        protected void OnBatchListChanged(object sender, EventArgs e)
        {
            

        }
        protected void OnCheckAllClick(object sender, EventArgs e)
        {
            foreach (ListItem item in BatchList.Items)
            {
                item.Selected = checkAll.Checked;
            }

        }
        #endregion

        #region Measure Combo
        protected void OnMeasureChanged(object sender, EventArgs e)
        {
            var measure = GetMeasureSelected();
            if (measure == null) return;
            ShowValuePanels(measure.MeasureClass);
            if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                SetEnumData(measure.MeasureId);
            }
        }
        #endregion

        #region Measure Enum Value
        private void SetEnumData(int measureId)
        {
            var enumMeasures = GetViewState(SessionConstants.BulkEnumMeasures) as List<EnumMeasureModel>;
            if (enumMeasures == null) return;
            var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
            if (enums.Count == 0) return;
            enums.Sort((m1, m2) => m1.NOrder.CompareTo(m2.NOrder));
            MeasureEnumField.DataValueField = "MeasureValueID";
            MeasureEnumField.DataTextField = "ValueTitle";

            MeasureEnumField.DataSource = enums;
            MeasureEnumField.DataBind();
        }
        protected void OnEnumMeasureChanged(object sender, EventArgs e)
        {
        }
        #endregion

        #region Save button
        protected void OnSaveMeasureClick(object sender, EventArgs e)
        {
            //-- Get Checked Batches
            var batches = (from ListItem item in BatchList.Items where item.Selected select Convert.ToInt32(item.Value)).ToList();
            if (batches.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Batches Not Found!" + "\")</SCRIPT>");
            }
            var measure = GetMeasureSelected();
            if (measure == null)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Measure is required !" + "\")</SCRIPT>");
                return;
            }
            var updateModel = new BulkUpdateMeasureModel
            {
                Batches = batches, 
                Measure = GetMeasureSelected(), 
                PartId = GetPartIdSelected(),
                ItemTypeId = GetFirstBatch().ItemTypeId,
                //MeasureEnumValue = Convert.ToInt32(MeasureEnumField.SelectedValue)
            };
            if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                updateModel.MeasureEnumValue = Convert.ToInt32(MeasureEnumField.SelectedValue);
            }
            if (measure.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                var value = MeasureNumericField.Text;
                if (!string.IsNullOrEmpty(value))
                {
                    updateModel.MeasureFloatValue = float.Parse("" + value);    
                }
                
            }
            if (measure.MeasureClass == MeasureModel.MeasureClassText)
            {
                updateModel.MeasureTextValue = MeasureTextField.Text.Trim();
            }
            var msg = QueryUtils.SaveMeasureValues(updateModel, this);
            SaveMsgLabel.Text = msg;
        }
        #endregion

        #region Utils
        private bool IsByCpMode()
        {
            return ModeField.SelectedValue == "cpEnabled";
        }
        private BatchModel GetFirstBatch()
        {
            var batches = GetViewState(SessionConstants.BulkBatches) as List<BatchModel>;
            return batches == null ? null : batches[0];
        }
        private void ShowValuePanels(int measureClass)
        {
            ValueLabel.Visible = measureClass != 0;
            if (measureClass == 0)
            {
                //-- Hide All
                MeasureEnumField.Visible = false;
                MeasureNumericField.Visible = false;
                MeasureTextField.Visible = false;
            }
            MeasureEnumField.Visible = measureClass == MeasureModel.MeasureClassEnum;
            MeasureNumericField.Visible = measureClass == MeasureModel.MeasureClassNumeric;
            if (!MeasureNumericField.Visible) MeasureNumericField.Text = "";
            MeasureTextField.Visible = measureClass == MeasureModel.MeasureClassText;
            if (!MeasureTextField.Visible) MeasureTextField.Text = "";
        }
        private MeasureModel GetMeasureSelected()
        {
            var value = MeasureListField.SelectedValue;
            if (string.IsNullOrEmpty(value)) return null;
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel>;
            if (measures == null) return null;
            return measures.Find(m => m.MeasureId == Convert.ToInt32(value));
        }
        private int GetPartIdSelected()
        {
            var value = PartsListField.SelectedValue;
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
        }
        #endregion

        #region Clear button
        protected void OnClearClick(object sender, EventArgs e)
        {
            ShowValuePanels(0);
            BatchList.Items.Clear();
            checkAll.Checked = false;
            PartsListField.Items.Clear();
            MeasureListField.Items.Clear();
            SaveMsgLabel.Text = "";
        }
        #endregion

        #region PreFill Measure
        private void PreFillMeasure()
        {
            //-- Find CpId for checked Batch
            //-- Get Checked Batches
            var batches = (from ListItem item in BatchList.Items where item.Selected select Convert.ToInt32(item.Value)).ToList();
            if (batches.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Batches Not Found!" + "\")</SCRIPT>");
            }

        }
        #endregion
    }
}