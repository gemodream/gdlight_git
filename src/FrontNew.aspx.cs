﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace Corpt
{
	public partial class FrontNew : CommonPage
	{
		#region Page Load
		
		private DataTable dtotGiveOutVal;

		public string imgTransperant = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Front";
			if (!IsPostBack)
			{
				//-- Customers List
				LoadCustomers();

				//--Load Weight Type
				DataView dv = FrontUtils.GetMeasureUnits(this).DefaultView;
				dv.RowFilter = "MeasureUnitCode = 2";
				ddlWeightType.DataSource = dv;
				ddlWeightType.DataBind();

				ddlWeightTypeShipRecieving.DataSource = dv;
				ddlWeightTypeShipRecieving.DataBind();

				//--Load Service Type
				var ServiceType = FrontUtils.GetServiceTypes(this);

				rdlServiceTypeTakeIn.DataSource = ServiceType;
				rdlServiceTypeTakeIn.DataBind();
                rdlServiceTypeTakeIn.SelectedIndex = 3;


                rdlServiceTypeShipReceiving.DataSource = ServiceType;
				rdlServiceTypeShipReceiving.DataBind();
                rdlServiceTypeShipReceiving.SelectedIndex = 3;

                //ddlServiceTypeEditOrder.DataSource = ServiceType;
                //	ddlServiceTypeEditOrder.DataBind();


                //-- Load Carries
                var dtCarriers = QueryCustomerUtils.GetCarriers(this);
				dtCarriers.RemoveAt(0);
				//ddlCarriersShipOut.DataSource = dtCarriers;
				//ddlCarriersShipOut.DataBind();

				rdlCarriersShipReceiving.DataSource = dtCarriers;
				rdlCarriersShipReceiving.DataBind();
				rdlCarriersShipOut.DataSource = dtCarriers;
				rdlCarriersShipOut.DataBind();


				//--Desable All Controls
				TakeInControlsEnableDesable(false);
				GiveOutControlsEnableDesable(false);
				ShipRecievingControlsEnableDesable(false);
				ShipOutControlsEnableDesable(false);
				//-- Country List
				/*  var country = QueryCustomerUtils.GetCountry(this);
                  CustomerCountry.DataSource = country;
                  CustomerCountry.DataBind();

                  dlPersonCounty.DataSource = country;
                  dlPersonCounty.DataBind();





                  //ddlPersonCounty


                  ////-- US States
                  //var states = QueryCustomerUtils.GetUsStates(this);
                  //CompanyUsState.DataSource = states;
                  //CompanyUsState.DataBind();

                  //PersonUsState.DataSource = states;
                  //PersonUsState.DataBind();

                  //-- Business Type
                  BusinessType.DataSource = QueryCustomerUtils.GetBusinessTypes(this);
                  BusinessType.DataBind();

                  //-- IndustryMembers
                  IndustryMembers.DataSource = QueryCustomerUtils.GetIndustryMemberships(this);
                  IndustryMembers.DataBind();

                  //-- Permissions
                  CompanyPermissionsList.DataSource = new PermissionsModel().Items;
                  CompanyPermissionsList.DataBind();

                  PersonPermissionsList.DataSource = new PermissionsModel().Items;
                  PersonPermissionsList.DataBind();

                  //-- Goods Movement
                  MovementsList.DataSource = new GoodsMovementModel().Items;
                  MovementsList.DataBind();

                  //-- Carriers
                  CarrierList.DataSource = QueryCustomerUtils.GetCarriers(this);
                  CarrierList.DataBind();

                  //-- Preferred Methods of Communication
                  SetTreeMethodValue("f0p0e0m0", CompanyMethods);
                  SetTreeMethodValue("f0p0e0m0", PersonMethods);

                  //-- Person Position Types
                  PersonPosition.DataSource = QueryCustomerUtils.GetPositions(this);
                  PersonPosition.DataBind();

                  CompanyDetailsPanel.Visible = false;
                  PersonListPanel.Visible = false;
                  PersonDetailsPanel.Visible = false;

                  PersonUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this person information?');");
                  PersonDeleteBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this person information?');");
                  CustomerUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this customer information?');");
                  */
				//btnSubmitTakeIn.Attributes.Add("OnClientClick", "return confirm('Are you sure you want to save ?');");
				//btnSubmitGiveOut.Attributes.Add("OnClientClick", "return confirm('Are you sure you want to save ?');");
				//btnSubmitShipReceiving.Attributes.Add("OnClientClick", "return confirm('Are you sure you want to save ?');");

				//btnSubmitsh.Attributes.Add("OnClientClick", "return confirm('Are you sure you want to save and Create New Order?');");

				txtCustomerLookupTakeIn.Attributes.Add("onKeyUp", "FilterShapes_TakeIn(this)");
				txtCustomerLookupTakeIn.Attributes.Add("onFocus", "FilterShapes_TakeIn(this)");

				txtCustomerLookupShipRecieving.Attributes.Add("onKeyUp", "FilterShapes_ShipRecieving(this)");
				txtCustomerLookupShipRecieving.Attributes.Add("onFocus", "FilterShapes_ShipRecieving(this)");

				txtSpecialInstructions.Attributes.Remove("MaxLength");
				txtSpecialInstructions.Attributes.Add("MaxLength", "2000");
				txtMemoTakeIn.Attributes.Remove("MaxLength");
				txtMemoTakeIn.Attributes.Add("MaxLength", "99");

				txtSpecialInstructionEditOrder.Attributes.Remove("MaxLength");
				txtSpecialInstructionEditOrder.Attributes.Add("MaxLength", "2000");

				btnCaptureSignGiveOut.Attributes.Add("onclick", "GetSignaturePadTest('dvSignaturepadGiveOut','" + imgCaptureSignatureGiveOut.ClientID + "', '" + hdnimgSignatureGiveOut.ClientID + "', 'ctl00_SampleContent_ModalPopupExtender8_foregroundElement');");
				btnCaptureSignatureTakeIn.Attributes.Add("onclick", "GetSignaturePadTest('dvSignaturepadTakeIn','" + imgCaptureSignatureTakeIn.ClientID + "', '" + hdnimgSignatureTakeIn.ClientID + "', 'ctl00_SampleContent_ModalPopupExtender5_foregroundElement');");
				// txtCustomerLike.Focus();

				Page.Validate(ValidatorGroupCompany);
				Page.Validate(ValidatorGroupPerson);

                rbtNoOfItems_SelectedIndexChanged(this, e);
                rbtnTotalWeight_SelectedIndexChanged(this, e);
                rbtnNoOfItemsShipReceiving_SelectedIndexChanged(this, e);
                rbtnTotalWeightShipReceiving_SelectedIndexChanged(this, e);
                rdbServiceTypeTakeIn_IndexChanged(this, e);
                rdlServiceTypeShipReceiving_IndexChanged(this, e);

                return;
			}
			else
			{
				if (hdnimgSignatureTakeIn.Value != "")
				{
					imgCaptureSignatureTakeIn.ImageUrl = hdnimgSignatureTakeIn.Value;
				}
				if (hdnimgSignatureGiveOut.Value != "")
				{
					imgCaptureSignatureGiveOut.ImageUrl = hdnimgSignatureGiveOut.Value;
				}
				if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == "move")
				{
					int idx = lstMNsTakeIn.SelectedIndex;
					ListItem item = lstMNsTakeIn.SelectedItem;
					lstMNsTakeIn.Items.Remove(item);
				}
				lstMNsTakeIn.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(lstMNsTakeIn, "move"));
				//iFramePlaceOrder.Src = "";
			}

		}

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);

            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "   ";

            ddlCustomerListTakeIn.DataSource = customers;
            ddlCustomerListTakeIn.DataBind();
            ddlCustomerListTakeIn.Items.Insert(0, li);

            ddlCustomerLookupShipRecieving.DataSource = customers;
            ddlCustomerLookupShipRecieving.DataBind();
            ddlCustomerLookupShipRecieving.Items.Insert(0, li);

            lstCustomerlookupTakeIn.DataSource = customers;
            lstCustomerlookupTakeIn.DataBind();

            lstCustomerLookupShipRecieving.DataSource = customers;
            lstCustomerLookupShipRecieving.DataBind();

            ddlVendorDepartureSettingShipRecieving.DataSource = customers;
            ddlVendorDepartureSettingShipRecieving.DataBind();

            ddlVendorDepartureSettingTakeIn.DataSource = customers;
            ddlVendorDepartureSettingTakeIn.DataBind();

            ddlVendorDepartureSettingGiveOut.DataSource = customers;
            ddlVendorDepartureSettingGiveOut.DataBind();

            ddlVendorDepartureSettingShipOut.DataSource = customers;
            ddlVendorDepartureSettingShipOut.DataBind();

            ListItem vendroLookup = new ListItem();
            vendroLookup.Value = "0";
            vendroLookup.Text = "Vendor Lookup";
            ddlVendorDepartureSettingTakeIn.Items.Insert(0, vendroLookup);
            ddlVendorDepartureSettingShipRecieving.Items.Insert(0, vendroLookup);
            ddlVendorDepartureSettingGiveOut.Items.Insert(0, vendroLookup);
            ddlVendorDepartureSettingShipOut.Items.Insert(0, vendroLookup);
        }

		#endregion

		#region TakeIn (Tab-1)
		protected void txtMNTakeIn_TextChanged(object sender, EventArgs e)
		{
			if (lstMNsTakeIn.Items.FindByText(txtMNTakeIn.Text.ToString().Trim()) != null)
			{
				PopupInfoDialog("Memo Number Repeated", true);
				txtMNTakeIn.Text = "";
				return;
			}
			if (txtMNTakeIn.Text != "")
			{
				lstMNsTakeIn.Items.Add(txtMNTakeIn.Text.ToString().Trim());
				txtMNTakeIn.Text = "";
			}
			txtMNTakeIn.Focus();
		}

		protected void chkPickedUpByOurMessenger_TakeIn_CheckedChanged(object sender, EventArgs e)
		{
			if (chkPickedUpByOurMessenger_TakeIn.Checked == true)
			{
				txtMessengerTakeIn.Text = "";
				hdnMessengerTakeIn.Value = "";
				imgCaptureSignatureTakeIn.ImageUrl = imgTransperant;
				imgStoredPhoto.ImageUrl = imgTransperant;
				imgStoredSignature.ImageUrl = imgTransperant;
				lblMessengerNameTakeIn.Text = "";
				rdbMessengerTakeIn.ClearSelection();
			}
		}

		public void TakeInControlsEnableDesable(bool Enabled)
		{
			btnMessengerlistTakeIn.Enabled = Enabled;
			//txtNoOfItems.Enabled = Enabled;
			rdlNoOfItems.Enabled = Enabled;
			//txtTotalWeight.Enabled = Enabled;
			//ddlWeightType.Enabled = Enabled;
			rdlTotalWeight.Enabled = Enabled;
			btnServiceTypeTakeIn.Enabled = Enabled;
			txtSpecialInstructions.Enabled = Enabled;
			txtMemoTakeIn.Enabled = Enabled;
			txtMNTakeIn.Enabled = Enabled;
			lstMNsTakeIn.Enabled = Enabled;
			btnSubmitTakeIn.Enabled = Enabled;
			btnCaptureSignatureTakeIn.Enabled = Enabled;
			btnDepartureSettingTakeIn.Enabled = Enabled;
			btnNewMessengerTakeIn.Enabled = Enabled;
		}

		private CustomerModel GetCustomerFromView(string customerId)
		{
			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
							new List<CustomerModel>();
			return customers.Find(m => m.CustomerId == customerId);
		}

		private PersonExModel GetPersonFromView(int personId)
		{
			var persons = GetViewState(SessionConstants.PersonEditData) as List<PersonExModel> ??
							new List<PersonExModel>();
			return persons.Find(m => m.PersonId == personId);
		}

		protected void txtCustomerLookupTakeIn_OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			//var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
			//var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
			bool changed = false;

			foreach (ListItem item in ddlCustomerListTakeIn.Items)
			{
				//if (textBox.Text == item.Text)
				//if (item.Text.Contains(textBox.Text))
				string custNumbers = new String(textBox.Text.Where(Char.IsDigit).ToArray());
				if (item.Text.Contains(custNumbers == "" ? "$" : custNumbers))
				{
					ddlCustomerListTakeIn.SelectedValue = item.Value;
					txtCustomerLookupTakeIn.Text = txtCustomerLookupTakeIn.Text;
					changed = true;
					break;
				}
			}

			if (!changed)
			{
				if (ddlCustomerListTakeIn.SelectedItem != null)
				{
					textBox.Text = ddlCustomerListTakeIn.SelectedItem.Text;
				}
				else
				{
					textBox.Text = "";
				}
			}
		}

		protected void OnCustomerSelectedChanged(object sender, EventArgs e)
		{
			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
			ddlCustomerListTakeIn.Items.Clear();
			ddlCustomerListTakeIn.DataSource = customers;
			ddlCustomerListTakeIn.DataBind();

			ddlCustomerListTakeIn.ClearSelection();
			ddlCustomerListTakeIn.Items.FindByValue(lstCustomerlookupTakeIn.SelectedItem.Value).Selected = true;

			//HideCustomerDetails();
			if (string.IsNullOrEmpty(ddlCustomerListTakeIn.SelectedValue)) return;

			var customer = GetCustomerFromView(ddlCustomerListTakeIn.SelectedValue);
			if (customer == null) return;
			//HistoryRef.HRef = "~/CustomerHistory.aspx?CustomerCode=" + customer.CustomerCode;
			var infoModel = QueryCustomerUtils.GetCustomer(customer, this);

			///-Customer Popup Edit mode
			lblModelHeader.Text = "Edit Customer/Messenger";
			btnNewCustomerTakeIn.Text = "Edit Customer";
			btnNewMessengerTakeIn.Text = "Edit Messenger";
			iFrameNewCustomer.Src = "CustomerEdit1.aspx?CustomerCode=" + customer.CustomerCode;

			SetViewState(infoModel, SessionConstants.CustomerEditData);
			if (infoModel == null) return;

			//SetCompanyInfo(infoModel);

			//Load Carriers according customer
			var customerModel = GetCustomerFromView(ddlCustomerListTakeIn.SelectedValue);
			//CustomerModel customerModel = new CustomerModel();
			//customerModel.CustomerId = lstCustomerList.SelectedValue;
			//customerModel.OfficeId = lstCustomerList.OfficeId;

			//var Carriers = QueryCustomerUtils.GetCarriersByCustomer(customerModel, Page);
			var Persons = QueryCustomerUtils.GetPersonsByCustomer(customerModel, Page);
			SetViewState(Persons, SessionConstants.PersonEditData);
			//ddlMessengerListTakeIn.DataSource = Persons;
			//ddlMessengerListTakeIn.DataBind();
			if (Persons.Count >= 1)
			{
				rdbMessengerTakeIn.DataSource = Persons;
				rdbMessengerTakeIn.DataBind();
				lblMessengerMsgTakeIn.Visible = false;
			}
			else
			{
				rdbMessengerTakeIn.Items.Clear();
				rdbMessengerTakeIn.DataSource = null;
				rdbMessengerTakeIn.DataBind();
				lblMessengerMsgTakeIn.Visible = true;
			}


			if (ddlCustomerListTakeIn.SelectedValue == "0")
				TakeInControlsEnableDesable(false);
			else
				TakeInControlsEnableDesable(true);

			txtMessengerTakeIn.Text = "Messenger Lookup";
			imgStoredPhoto.ImageUrl = imgTransperant;
			imgStoredSignature.ImageUrl = imgTransperant;
			lblCustomerNameTakeIn.Text = ddlCustomerListTakeIn.SelectedItem.Text.ToString();
			txtCustomerLookupTakeIn.Text = ddlCustomerListTakeIn.SelectedItem.Text;
			lblOrderNumbeTakeIn.Text = "";
		}

		protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
		{
			OnCustomerLikeClick(null, null);
		}

		protected void OnCustomerLikeClick(object sender, EventArgs e)
		{
			//HideCustomerDetails();
			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
			var filterText = txtCustomerLookupTakeIn.Text.Trim().ToLower();
			var filtered = string.IsNullOrEmpty(filterText) ? customers :
				customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
			//ddlCustomerListTakeIn.DataSource = filtered;
			//ddlCustomerListTakeIn.DataBind();
			//lstCustomerlookupTakeIn.DataSource = filtered.Where(user => user.OfficeId == AuthorOfficeID).ToList();
			//lstCustomerlookupTakeIn.DataSource = filtered;
			//lstCustomerlookupTakeIn.DataBind();
			if (filtered.Count == 1)
			{
				ddlCustomerListTakeIn.SelectedValue = filtered[0].CustomerId;
				lstCustomerlookupTakeIn.SelectedValue = filtered[0].CustomerId;
			}
			if (!string.IsNullOrEmpty(ddlCustomerListTakeIn.SelectedValue))
			{
				OnCustomerSelectedChanged(null, null);
			}
		}

		protected void rdbServiceTypeTakeIn_IndexChanged(object sender, EventArgs e)
		{
			txtServiceTypeTakeIn.Text = rdlServiceTypeTakeIn.SelectedItem.Text.ToString();
			hdnServiceTypeTakeIn.Value = rdlServiceTypeTakeIn.SelectedItem.Value.ToString();
			lblServiceTypeSelecedTakeIn.Text = rdlServiceTypeTakeIn.SelectedItem.Text.ToString();
		}

		protected void rdbMessengerTakeIn_Changed(object sender, EventArgs e)
		{
			//InitPageFields();
			lblMessengerNameTakeIn.Text = rdbMessengerTakeIn.SelectedItem.Text.ToString();
			txtMessengerTakeIn.Text = rdbMessengerTakeIn.SelectedItem.Text;
			hdnMessengerTakeIn.Value = rdbMessengerTakeIn.SelectedItem.Value;
			chkPickedUpByOurMessenger_TakeIn.Checked = false;
			//var customerModel = GetCustomerFromView(ddlCustomerListTakeIn.SelectedValue);
			//var Persons = QueryCustomerUtils.GetPersonsByCustomer(customerModel, Page);
			var Person = GetPersonFromView(Convert.ToInt32(rdbMessengerTakeIn.SelectedItem.Value));

			//var Person= Persons.Find(m => m.PersonId == Convert.ToInt32(rdbMessengerTakeIn.SelectedItem.Value));
			//imgStoredSignature.Src = Person.Path2Signature==null?"": Person.Path2Signature;

			if (Person.Path2Signature == null)
			{
				imgStoredSignature.ImageUrl = imgTransperant;
			}
			else
			{
				imgStoredSignature.ImageUrl = Person.Path2Signature;
			}

			if (Person.Path2Photo == null)
			{
				imgStoredPhoto.ImageUrl = imgTransperant;
			}
			else
			{
				imgStoredPhoto.ImageUrl = Person.Path2Photo;
			}
		}
		
		protected void btnClearTakeIn_Click(object sender, EventArgs e)
		{
			TakeInDefaultFields();
		}

		private void TakeInDefaultFields()
		{
			lstCustomerlookupTakeIn.ClearSelection();
			txtCustomerLookupTakeIn.Text = "";
			txtMessengerTakeIn.Text = "Messenger Lookup";
			imgCaptureSignatureTakeIn.ImageUrl = imgTransperant;
			imgStoredPhoto.ImageUrl = imgTransperant;
			imgStoredSignature.ImageUrl = imgTransperant;
			hdnimgSignatureTakeIn.Value = "";

			txtNoOfItemsTakeIn.Text = "";
			txtTotalWeightTakeIn.Text = "";
			ddlWeightType.SelectedIndex = 0;
			txtServiceTypeTakeIn.Text = "5 days";
			hdnServiceTypeTakeIn.Value = "4";

			rdlServiceTypeTakeIn.SelectedIndex = 3;
			txtMemoTakeIn.Text = "";
			txtSpecialInstructions.Text = "";

			rdlNoOfItems.SelectedIndex = 1;
			rdlTotalWeight.SelectedIndex = 1;
			txtMNTakeIn.Text = "";
			lstMNsTakeIn.Items.Clear();

			rbtNoOfItems_SelectedIndexChanged(null, null);
			rbtnTotalWeight_SelectedIndexChanged(null, null);

			lblCustomerNameTakeIn.Text = "";
			lblMessengerNameTakeIn.Text = "";
			lblNumberOfItemsValueTakeIn.Text = "";
			lblTotalWeightValueTakeIn.Text = "";
			lblServiceTypeSelecedTakeIn.Text = "";
			lblMemoNoTakeIn.Text = "";
			lblVendorTakeIn.Text = "";
			lblOrderNumbeTakeIn.Text = "";
			//txtOrderNumberTakeIn.Text = "";

			chkPickedUpByOurMessenger_TakeIn.Checked = false;
            chkNoGoods.Checked = false;
            btnSubmitTakeIn.Enabled = false;
			TakeInControlsEnableDesable(false);
		}

		protected void btnSubmitTakeIn_Click(object sender, EventArgs e)
		{
			if (hdnMessengerTakeIn.Value == "" && chkPickedUpByOurMessenger_TakeIn.Checked == false)
			{
				PopupInfoDialog("Please select Messenger or Picked-Up-By-Our-Messenger", false);
				return;
			}

			if (rdlNoOfItems.Items[2].Selected == false && string.IsNullOrEmpty(txtNoOfItemsTakeIn.Text.Trim()) == true)
			{
				PopupInfoDialog("Please enter number of items.", false);
				return;
			}

			SaveQPopupExtender.Show();

		}

		private string SubmitTakeIn()
        {
            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

            objNewOrderModelExt.NumberOfItems = txtNoOfItemsTakeIn.Text.ToString() == "" ? 0 : Convert.ToInt32(txtNoOfItemsTakeIn.Text.ToString());
            objNewOrderModelExt.TotalWeight = txtTotalWeightTakeIn.Text.ToString() == "" ? 0 : Convert.ToDecimal(txtTotalWeightTakeIn.Text.ToString());
            objNewOrderModelExt.IsIQInspected = rdlNoOfItems.Items[0].Selected == true ? true : false;
            objNewOrderModelExt.IsTWInspected = rdlTotalWeight.Items[0].Selected == true ? true : false;
            
            objNewOrderModelExt.MeasureUnitID = Convert.ToInt32(ddlWeightType.SelectedValue.ToString());
            objNewOrderModelExt.ServiceTypeID = Convert.ToInt32(hdnServiceTypeTakeIn.Value.ToString());
            objNewOrderModelExt.Memo = txtMemoTakeIn.Text.ToString();
            objNewOrderModelExt.SpecialInstructions = txtSpecialInstructions.Text.ToString();
            objNewOrderModelExt.PersonID = hdnMessengerTakeIn.Value == "" ? 0 : Convert.ToInt32(hdnMessengerTakeIn.Value.ToString());
            objNewOrderModelExt.VendorID = Convert.ToInt32(ddlVendorDepartureSettingTakeIn.SelectedItem.Value.ToString() == "" ? "0" : ddlVendorDepartureSettingTakeIn.SelectedItem.Value.ToString());
            
            string vendorId = ddlVendorDepartureSettingTakeIn.SelectedValue.ToString() == "" ? "0" : ddlVendorDepartureSettingTakeIn.SelectedValue.ToString();
            if (vendorId != "0")
            {
                var vendors = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
                objNewOrderModelExt.VendorOfficeID = Convert.ToInt32(vendors.Find(m => m.CustomerId == vendorId).OfficeId == "" ? "0" : vendors.Find(m => m.CustomerId == vendorId).OfficeId);
            }
            if (lstMNsTakeIn.Items.Count == 0 && objNewOrderModelExt.Memo != "")
            {
                lstMNsTakeIn.Items.Add(objNewOrderModelExt.Memo);
            }
            objNewOrderModelExt.MemoNumbers = lstMNsTakeIn.Items.Cast<ListItem>().Select(x => x.Value).ToList<string>();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();

            string customerId = ddlCustomerListTakeIn.SelectedValue.ToString();
            objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);

            string personId = hdnMessengerTakeIn.Value.ToString();
            objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
            objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).OfficeId);

            string errMsg = string.Empty;

            var result = FrontUtils.CreateOrder(objNewOrderModelExt, "TakeIn", this, out errMsg);
                      
            if (result != null)
            {
                objNewOrderModelExt.PickedUpByOurMessenger = chkPickedUpByOurMessenger_TakeIn.Checked == true ? true : false;
                if (chkPickedUpByOurMessenger_TakeIn.Checked == true)
                {
                    var result_Messenger = FrontUtils.SetPickedUpByOurMessenger(objNewOrderModelExt, this, out errMsg);
                }

                objNewOrderModelExt.IsNoGoods = chkNoGoods.Checked == true ? true : false;
                var resultNoGoods = FrontUtils.SetNoGoods(objNewOrderModelExt, this, out errMsg);
                
                txtOrderNumberTakeIn.Text = objNewOrderModelExt.OrderNumber;
                //iFramePlaceOrder.Src = "XmlDownload.aspx?OrderCode="+ objNewOrderModelExt.OrderNumber;
                //var okMsg =
                //    "New order number " + objNewOrderModelExt.OrderNumber + " " +
                //    "has been created for customer " + objNewOrderModelExt.Customer.CustomerName + "!";

                TakeInDefaultFields();
                lblOrderNumbeTakeIn.Text = "Last Order# \n\r" + Environment.NewLine + objNewOrderModelExt.OrderNumber;
				//GroupIDTakeIn.Value = objNewOrderModelExt.GroupID.ToString();
				//CustomerCodeTakeIn.Value = objNewOrderModelExt.Customer.CustomerCode.ToString();
				//GroupOfficeIDTakeIn.Value = objNewOrderModelExt.GroupOfficeID.ToString();
				btnViewRecieptTakeIn.Enabled = true;
				btnPrintLabelTakeIn.Enabled = true;
				btnPrintReceiptTakeIn.Enabled = true;
				hdnOrderCode.Value = objNewOrderModelExt.OrderNumber;
				//if (rblLabelsTakeIn.SelectedValue == "0")
				//{
				//    iFramePlaceOrder.Src = "XmlDownload.aspx?Type=Label&OrderCode=" + txtOrderNumberTakeIn.Text;
				//}
			}            
            return errMsg;
        }

		protected void rbtNoOfItems_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rdlNoOfItems.Items[0].Selected == true)
			{
				txtNoOfItemsTakeIn.Enabled = true;
				lblNumberOfItemTakeIn.Text = "Number Of Items (Inspected): ";
			}
			else if (rdlNoOfItems.Items[1].Selected == true)
			{
				txtNoOfItemsTakeIn.Enabled = true;
				lblNumberOfItemTakeIn.Text = "Number Of Items (Not Inspected): ";
			}
			else
			{
				txtNoOfItemsTakeIn.Enabled = false;
				lblNumberOfItemTakeIn.Text = "Number Of Items (N/A): ";
			}
		}

		protected void rbtnTotalWeight_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rdlTotalWeight.Items[0].Selected == true)
			{
				txtTotalWeightTakeIn.Enabled = true;
				ddlWeightType.Enabled = true;
				lblTotalWeightTakeIn.Text = "Total Weight (Inspected): ";
			}
			else if (rdlTotalWeight.Items[1].Selected == true)
			{
				txtTotalWeightTakeIn.Enabled = true;
				ddlWeightType.Enabled = true;
				lblTotalWeightTakeIn.Text = "Total Weight (Not Inspected): ";
			}
			else
			{
				txtTotalWeightTakeIn.Enabled = false;
				ddlWeightType.Enabled = false;
				lblTotalWeightTakeIn.Text = "Total Weight (N/A): ";
			}
		}

		protected void txtNoOfItems_TextChanged(object sender, EventArgs e)
		{
			lblNumberOfItemsValueTakeIn.Text = txtNoOfItemsTakeIn.Text.ToString();
		}

		protected void txtTotalWeight_TextChanged(object sender, EventArgs e)
		{
			lblTotalWeightValueTakeIn.Text = txtTotalWeightTakeIn.Text.ToString();
		}

		protected void ddlServiceType_SelectedIndexChanged(object sender, EventArgs e)
		{
			lblServiceTypeSelecedTakeIn.Text = rdlServiceTypeTakeIn.SelectedItem.Text.ToString();
		}

		protected void txtOrderMemo_TextChanged(object sender, EventArgs e)
		{
			lblMemoNoTakeIn.Text = txtMemoTakeIn.Text.ToString();
		}

		private void LoadCheckBoxList(string values, CheckBoxList control)
		{
			foreach (ListItem item in control.Items)
			{
				item.Selected = false;
			}
			if (string.IsNullOrEmpty(values)) return;
			var keys = values.Split(',');
			foreach (ListItem item in control.Items)
			{

				foreach (var key in keys)
				{
					if (key == item.Value) item.Selected = true;
				}
			}
		}

		private string GetCheckBoxListValue(CheckBoxList control)
		{
			var value = "";
			foreach (ListItem item in control.Items)
			{
				if (item.Selected) value += item.Value + ",";
			}
			return value;
		}

		protected void btnSetDepartureSettingTakeIn_OnClick(object sender, EventArgs e)
		{
			lblVendorTakeIn.Text = "Vendor: " + ddlVendorDepartureSettingTakeIn.SelectedItem.Text.ToString().Trim();
		}

		protected void ddlVendorDepartureSettingTakeIn_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			txtVendorDepartureSettingTakeIn.Text = ddlVendorDepartureSettingTakeIn.SelectedItem.Value.ToString();
			string customerName = ddlVendorDepartureSettingTakeIn.SelectedItem.Text.ToString();
			txtVendorDepartureSettingTakeIn.Text = customerName.Substring(customerName.Length - 4, 4);
			ModalPopupExtender13.Show();
		}

		protected void txtVendorDepartureSettingTakeIn_OnTextChanged(object sender, EventArgs e)
		{
			ddlVendorDepartureSettingTakeIn.ClearSelection();
			ddlVendorDepartureSettingTakeIn.Items.Cast<ListItem>().Where(x => x.Text.Contains(txtVendorDepartureSettingTakeIn.Text.ToString().Trim())).LastOrDefault().Selected = true;
			ModalPopupExtender13.Show();
		}
		#endregion

		#region Give Out (Tab-2)
		protected void btnSubmitGiveOut_Click(object sender, EventArgs e)
		{
			if (hdnMessengerLookupGiveOut.Value == "" && chkTakenOutByOurMessengerGiveOut.Checked == false)
			{
				PopupInfoDialog("Please select Messenger or Taken-Out-By-Our-Messenger", false);
				return;
			}

			SaveQPopupExtender.Show();

		}

		public string SubmitGiveOut()
		{
			DataTable dtVendor = FrontUtils.GetVendorStruct(this);//Procedure dbo.spGetGroupVendorTypeOf
			DataSet otGiveOut = GetViewState("otGiveOut") as DataSet;

			NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
			objNewOrderModelExt.GroupOfficeID = otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[0].ToString());
			objNewOrderModelExt.GroupID = otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[1].ToString());
			objNewOrderModelExt.VendorOfficeID = otGiveOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString());
			objNewOrderModelExt.VendorID = ddlVendorDepartureSettingGiveOut.SelectedItem.Value == "" ? 0 : Convert.ToInt32(ddlVendorDepartureSettingGiveOut.SelectedItem.Value.ToString());
			//otGiveOut.Tables[0].Rows[0]["CustomerID"].ToString() == "" ? 0 : Convert.ToInt32(otGiveOut.Tables[0].Rows[0]["CustomerID"].ToString());
			objNewOrderModelExt.ShipmentCharge = 0;
			objNewOrderModelExt.CarrierTrackingNumber = "";
			objNewOrderModelExt.CarrierID = 0;

			string errMsg;
			var result_GroupVendor = FrontUtils.SetGroupVendor(objNewOrderModelExt, this, out errMsg); ////spSetGroupVendor


			DataTable dtTemp = new DataTable();
			dtTemp = otGiveOut.Tables["tblItem"].Clone();

			if (chkAutoCheckOutGiveOut.Checked)
			{
				dtTemp = otGiveOut.Tables["tblItem"].Copy();
			}
			else
			{
				dtotGiveOutVal = new DataTable();
				dtotGiveOutVal = otGiveOut.Tables["tblItem"].Clone();
				dtTemp = GetCheckedNodes(trvOrderTreeGiveOut.Nodes, otGiveOut.Tables["tblItem"]);
			}

			DataTable dtItemOut = FrontUtils.GetItemOutStruct(this);

			if (dtTemp.Rows.Count == 0)
			{
				PopupInfoDialog("Please select atleast one item.", false);
				return string.Empty;
			}

			foreach (DataRow row in dtTemp.Rows)
			{
				DataRow drItem = dtItemOut.NewRow();
				drItem["BatchID_ItemCode"] = row["BatchID_ItemCode"];
				dtItemOut.Rows.Add(drItem);
			}


			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
			string customerId = hfCustomerGiveOut.Value.ToString();
			objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);
			objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
			objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).OfficeId);

			//objNewOrderModelExt.Customer.CustomerId = hfCustomerGiveOut.Value;
			//objNewOrderModelExt.Customer.CustomerName = txtCustomerGiveOut.Text;
			objNewOrderModelExt.PersonID = hdnMessengerLookupGiveOut.Value == "" ? 0 : Convert.ToInt32(hdnMessengerLookupGiveOut.Value);
					
			objNewOrderModelExt.GroupCode =Convert.ToInt32(txtItemNoGiveOut.Text);

			//Procedure dbo.spSetCloseGroupStateByCode and loop with procedure dbo.spSetCloseBatchStateByCode and dbo.spSetCloseItemStateByCode
			var result_setclosegroup = FrontUtils.SetCloseGroupStateByCode(objNewOrderModelExt, this, out errMsg);

			objNewOrderModelExt.TakenOutByOurMessenger = chkTakenOutByOurMessengerGiveOut.Checked == true ? true : false;

			//Procedure dbo.spSetCloseOrderStateByCode and for close detail to tblOrderOut Close table 
			var result_setcloseorder = FrontUtils.SetOrderOut(objNewOrderModelExt, this, out errMsg);


			//	#region BatchTracking
			DataSet dsTemp1 = otGiveOut;
			dsTemp1.Tables["tblItem"].Clear();
			dsTemp1.Tables["tblItem"].Merge(dtTemp);

			if (dsTemp1.Tables["tblBatch"].Rows.Count > 0)
			{
				foreach (DataRow dr in dsTemp1.Tables["tblBatch"].Rows)
				{

					DataRow[] drSet = dsTemp1.Tables["tblItem"].Select("NewBatchID = '" + dr["BatchID"].ToString() + "'");

					if (drSet.Length > 0 && drSet.Length > 0)
					{
						DataTable dtitem = drSet.CopyToDataTable();

						int BatchID = Convert.ToInt32(dr["BatchID"]);
						int EventID = FrontUtils.BatchEvents.TakeOut;
						int ItemsAffected = drSet.Length;
						int ItemsInBatch = Convert.ToInt32(dr["ItemsQuantity"]);
						int FormID = FrontUtils.Codes.AccRep;
						FrontUtils.SetBatchEvent(EventID, BatchID, FormID, ItemsAffected, ItemsInBatch, this);//Procedure dbo.spSetBatchEvents

						foreach (DataRow dritem in dtitem.Rows)
						{
							objNewOrderModelExt.ItemCode = Convert.ToInt32(dritem["ItemCode"]);// dsTemp1.Tables["tblItem"].Rows[0]["ItemCode"]
							objNewOrderModelExt.BatchCode = Convert.ToInt32(dritem["BatchCode"].ToString());//Convert.ToInt32(dsTemp1.Tables["tblItem"].Rows[0]["BatchCode"].ToString());
							FrontUtils.SetCloseItemStateByCode(objNewOrderModelExt, this, out errMsg); //spSetCloseItemStateByCode

							objNewOrderModelExt.BatchID = Convert.ToInt32(dr["BatchID"]);

							if (chkTakenOutByOurMessengerGiveOut.Checked == true)
							{
								FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
								FrontUtils.SetItemOutByOurMessenger(objNewOrderModelExt, this, out errMsg);
							}
							else
							{
								FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
							}
						}
					}
					FrontUtils.SetCloseBatchStateByCode(objNewOrderModelExt, this, out errMsg);
				}
			}
			ClearGiveOut();
			txtItemNoGiveOut.Text = "";
			if (errMsg == "")
				lblMsgGiveOut.Text = "Data was successfully added";
			else
				lblMsgGiveOut.Text = "Error in GiveOut";
			return errMsg;

		}

		public void GiveOutControlsEnableDesable(bool Enabled)
		{
			btnMessengerLookupGiveOut.Enabled = Enabled;
			btnNewMessengerGiveOut.Enabled = Enabled;
			btnCaptureSignGiveOut.Enabled = Enabled;
			//btnClearGiveOut.Enabled = Enabled;
			btnSubmitGiveOut.Enabled = Enabled;
			btnDepartureSettingGiveOut.Enabled = Enabled;
		}

		protected void rblMessengerLookupGiveOut_Changed(object sender, EventArgs e)
		{
			txtMessengerLookupGiveOut.Text = rblMessengerLookupGiveOut.SelectedItem.Text;
			hdnMessengerLookupGiveOut.Value = rblMessengerLookupGiveOut.SelectedItem.Value;

			var Person = GetPersonFromView(Convert.ToInt32(rblMessengerLookupGiveOut.SelectedItem.Value));

			imgStoredSignGiveOut.ImageUrl = string.IsNullOrEmpty(Person.Path2Signature) ? imgTransperant : Person.Path2Signature;
			imgStoredPhotoGiveOut.ImageUrl = string.IsNullOrEmpty(Person.Path2Photo) ? imgTransperant : Person.Path2Photo;
			chkTakenOutByOurMessengerGiveOut.Checked = false;
		}

		protected void btnClearGiveOut_Click(object sender, EventArgs e)
		{
			GiveOutControlsEnableDesable(false);
			ClearGiveOut();
			txtItemNoGiveOut.Text = "";
		}

		public void ClearGiveOut()
		{
			GiveOutControlsEnableDesable(false);
			txtCustomerGiveOut.Text = "";
			hfCustomerGiveOut.Value = "";

			txtMessengerLookupGiveOut.Text = "";
			hdnMessengerLookupGiveOut.Value = "";
			imgCaptureSignatureGiveOut.ImageUrl = imgTransperant;
			imgStoredSignGiveOut.ImageUrl = imgTransperant;
			imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
			trvOrderTreeGiveOut.Nodes.Clear();
			btnSubmitGiveOut.Enabled = false;
			lblVendorGiveOut.Text = "";
			chkTakenOutByOurMessengerGiveOut.Checked = false;
			lblMsgGiveOut.Text = "";

		}

		protected void btnItemGiveOut_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //bool bOpenOrder = false;
                if (txtItemNoGiveOut.Text.Trim().Length > 4)
                {
                    ClearGiveOut();
                    lblMsgGiveOut.Text = "";
                    string[] sCodesArray = txtItemNoGiveOut.Text.Split('.');
                    DataSet dsOrders = new DataSet();

                    int GroupCode = Convert.ToInt32(sCodesArray[0]);
                    NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
                    objNewOrderModelExt.GroupCode = GroupCode;
                    objNewOrderModelExt.BatchCode = 0;
                    objNewOrderModelExt.ItemCode = 0;
                    objNewOrderModelExt.OperationChar = "";

                    //Procedures spGetGroupByCode, spGetBatchByCode, spGetItemByCode, spGetItemDocByCode
                    DataTable tblOrder = FrontUtils.GetGroupByCode(objNewOrderModelExt, this);
                    DataTable tblBatch = FrontUtils.GetBatchbyCode(objNewOrderModelExt, this);
                    DataTable tblItem = FrontUtils.GetItembyCode(objNewOrderModelExt, this);
                    DataTable tblDoc = FrontUtils.GetItemDocByCode(objNewOrderModelExt, this);
                    tblOrder.TableName = "tblOrder";
                    tblBatch.TableName = "tblBatch";
                    tblItem.TableName = "tblItem";
                    tblDoc.TableName = "tblDoc";
                    dsOrders.Tables.Add(tblOrder.Copy());
                    dsOrders.Tables.Add(tblBatch.Copy());
                    dsOrders.Tables.Add(tblItem.Copy());
                    dsOrders.Tables.Add(tblDoc.Copy());

                    SetViewState(dsOrders, "otGiveOut");
                    if (dsOrders.Tables[0].Rows.Count == 0)
                    {
                        lblMsgGiveOut.Text = "Order # " + sCodesArray[0] + " doesn't exists";
                        GiveOutControlsEnableDesable(false);
                    }
                    else
                    {
                        GiveOutControlsEnableDesable(true);
                    }

				

                    //if (System.Convert.ToInt32(dsOrders.Tables[0].Rows[0]["StateCode"]) == 1)
                    //{
                    //    if (MessageBox.Show("Order # " + sCodesArray[0] + " is closed. Would you like to open it temporary?", "Order status", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    //    {
                    //        bOpenOrder = true;
                    //    }
                    //    else throw new Exception("Order# " + sCodesArray[0] + " is closed");
                    //}
                    //if (System.Convert.ToInt32(dsOrders.Tables[0].Rows[0]["StateCode"]) >= 2 || bOpenOrder == true)
                    //{
                    BatchModel batchModel = new BatchModel();
                    batchModel.CustomerId = dsOrders.Tables[0].Rows[0]["CustomerID"].ToString();
                    batchModel.CustomerOfficeId = dsOrders.Tables[0].Rows[0]["CustomerOfficeID"].ToString();

                    var dtCustomer = QueryUtils.GetCustomerName(batchModel, this); //Procedure spGetCustomer
                    txtCustomerGiveOut.Text = dtCustomer.CustomerName;
                    hfCustomerGiveOut.Value = dtCustomer.CustomerId;

                    var customerModel = GetCustomerFromView(batchModel.CustomerId);
                    var Persons = QueryCustomerUtils.GetPersonsByCustomer(customerModel, Page);
                    SetViewState(Persons, SessionConstants.PersonEditData);
                    rblMessengerLookupGiveOut.DataSource = Persons;
                    rblMessengerLookupGiveOut.DataBind();

                    //Load Order Tree
                    LoadItemNumbersTree1(txtItemNoGiveOut.Text.ToString().Trim(), trvOrderTreeGiveOut);
                    if (tblItem.Rows.Count == 0)
                    {
                        btnSubmitGiveOut.Enabled = false;
                    }
					hdnOrderCode.Value = txtItemNoGiveOut.Text.ToString().Trim();
					btnPrintLabelGiveOut.Enabled = true;
					btnPrintReceiptGiveOut.Enabled = true;
					//Check All Tree Nodes
					CheckAllNodes(trvOrderTreeGiveOut.Nodes);
					trvOrderTreeGiveOut.Enabled = false;

					if (dsOrders.Tables[0].Rows[0]["StateName"].ToString() == "closed")
					{
						btnSubmitGiveOut.Enabled = false;
						lblMsgGiveOut.Text = "Giveout for this order already done!";
						lblMsgGiveOut.ForeColor = System.Drawing.Color.Red;
					}
					else
					{
						btnSubmitGiveOut.Enabled = true;
						lblMsgGiveOut.Text = "";
						lblMsgGiveOut.ForeColor = System.Drawing.Color.Blue;
					}
				}
            }
            catch (Exception exc)
            {
                PopupInfoDialog("Data wasn't Loaded. Error: " + exc.Message, true);
            }

            if (chkAutoCheckOutGiveOut.Checked == true)
            {
                var errMsg = SubmitGiveOut();
                PopupInfoDialog("Data wasn't added. Error: " + errMsg, true);
            }
        }

		public void CheckAllNodes(TreeNodeCollection nodes)
		{
			foreach (TreeNode node in nodes)
			{
				node.Checked = true;
				CheckChildren(node, true);
				//node.SelectAction = TreeNodeSelectAction.None;
			}
		}

		private void CheckChildren(TreeNode rootNode, bool isChecked)
		{
			foreach (TreeNode node in rootNode.ChildNodes)
			{
				CheckChildren(node, isChecked);
				node.Checked = isChecked;
				//node.SelectAction = TreeNodeSelectAction.None;
			}
		}

		protected void chkTakenOutByOurMessengerGiveOut_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTakenOutByOurMessengerGiveOut.Checked == true)
            {
                txtMessengerLookupGiveOut.Text = "";
                hdnMessengerLookupGiveOut.Value = "";
                imgCaptureSignatureGiveOut.ImageUrl = imgTransperant;
                imgStoredPhotoGiveOut.ImageUrl = imgTransperant;
                imgStoredSignGiveOut.ImageUrl = imgTransperant;
                rblMessengerLookupGiveOut.ClearSelection();
            }
        }

        public DataTable GetCheckedNodes(TreeNodeCollection nodes, DataTable dt)
		{

			foreach (TreeNode aNode in nodes)
			{
				//edit
				if (aNode.Checked)
				{
					//Console.WriteLine(aNode.Text);
					//dt.Rows.Add(aNode.Value);

					if (aNode.Value.Length >= 18)
					{
						string itemNumber = aNode.Value.Substring(5, 13);
                        Utils.DissectItemNumber(itemNumber.Replace(".", ""), out string groupCode, out var batchCode, out var itemCode);
						//DataRow[] filterdrows =dt.Select("GroupCode = " + GroupCode + " AND BatchCode = " + BatchCode + " AND ItemCode = " + ItemCode);
						dtotGiveOutVal.Merge(dt.Select("GroupCode = " + groupCode + " AND BatchCode = " + batchCode + " AND ItemCode = " + itemCode).CopyToDataTable());
						//dtVal = filterdrows.CopyToDataTable();
					}
				}
				if (aNode.ChildNodes.Count != 0)
					GetCheckedNodes(aNode.ChildNodes, dt);

			}
			return dtotGiveOutVal;
		}

		private void LoadItemNumbersTree1(string OrderCode, TreeView trv)
		{

			var items = QueryItemiznUtils.GetOrderHistory(OrderCode, false, this);
			if (items.Count == 0)
			{
				PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", OrderCode), true);

			}
			SetViewState(items);
			ShowTreeHistory(items, trv);
		}

		private const string OrderHistoryItems = "orderHistoryItems";

		void SetViewState(List<CustomerHistoryModel> items)
		{
			SetViewState(items, OrderHistoryItems);
		}

		private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items, TreeView trv)
		{
			TreeUtils.ShowTreeHistory(items, trv);
		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}

		protected void ddlVendorDepartureSettingGiveOut_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			txtVendorDepartureSettingGiveOut.Text = ddlVendorDepartureSettingGiveOut.SelectedItem.Value.ToString();
			string customerName = ddlVendorDepartureSettingGiveOut.SelectedItem.Text.ToString();
			txtVendorDepartureSettingGiveOut.Text = customerName.Substring(customerName.Length - 4, 4);
			ModalPopupExtender15.Show();
		}

		protected void txtVendorDepartureSettingGiveOut_OnTextChanged(object sender, EventArgs e)
		{
			ddlVendorDepartureSettingGiveOut.ClearSelection();
			ddlVendorDepartureSettingGiveOut.Items.Cast<ListItem>().Where(x => x.Text.Contains(txtVendorDepartureSettingGiveOut.Text.ToString().Trim())).LastOrDefault().Selected = true;
			ModalPopupExtender15.Show();
		}

		protected void btnSetDepartureSettingGiveOut_OnClick(object sender, EventArgs e)
		{
			lblVendorGiveOut.Text = "Vendor: " + ddlVendorDepartureSettingGiveOut.SelectedItem.Text.ToString().Trim();
		}

		#endregion

		#region Ship Receiving (Tab-3)
		private string SubmitShipReceiving()
		{
			string errMsg = string.Empty;

			try
			{
				NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
				objNewOrderModelExt.NumberOfItems = Convert.ToInt32(txtNoOfItemsShipReceiving.Text.ToString() == "" ? "0" : txtNoOfItemsShipReceiving.Text.ToString());
				objNewOrderModelExt.TotalWeight = Convert.ToInt32(txtTotalWeightShipReceiving.Text.ToString() == "" ? "0" : txtTotalWeightShipReceiving.Text.ToString());
				objNewOrderModelExt.IsIQInspected = rdlNoOfItemsShipReceiving.Items[0].Selected == true ? true : false;
				objNewOrderModelExt.IsTWInspected = rdlTotalWeightShipReceiving.Items[0].Selected == true ? true : false;
				objNewOrderModelExt.MeasureUnitID = Convert.ToInt32(ddlWeightTypeShipRecieving.SelectedValue.ToString());
				objNewOrderModelExt.ServiceTypeID = Convert.ToInt32(hdnServiceTypeShipReceiving.Value.ToString());
				objNewOrderModelExt.Memo = txtMemoShipReceiving.Text.ToString();
				objNewOrderModelExt.SpecialInstructions = txtSpecialInstructionsShipReceiving.Text.ToString();
				objNewOrderModelExt.CarrierID = Convert.ToInt32(hdnCarriersShipReceiving.Value.ToString() == "" ? "0" : hdnCarriersShipReceiving.Value.ToString());
				objNewOrderModelExt.CarrierTrackingNumber = txtScanPackageBarcodeShipRecieving.Text.ToString();
				objNewOrderModelExt.VendorID = Convert.ToInt32(ddlVendorDepartureSettingShipRecieving.SelectedItem.Value.ToString() == "" ? "0" : ddlVendorDepartureSettingShipRecieving.SelectedItem.Value.ToString());

				string vendorId = ddlVendorDepartureSettingShipRecieving.SelectedValue.ToString() == "" ? "0" : ddlVendorDepartureSettingShipRecieving.SelectedValue.ToString();
				if (vendorId != "0")
				{
					var vendors = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
					objNewOrderModelExt.VendorOfficeID = Convert.ToInt32(vendors.Find(m => m.CustomerId == vendorId).OfficeId);
				}
				else
				{ objNewOrderModelExt.VendorOfficeID = 0; }

				if (lstMNsShipReceiving.Items.Count == 0 && objNewOrderModelExt.Memo != "")
				{
					lstMNsShipReceiving.Items.Add(objNewOrderModelExt.Memo);
				}
				objNewOrderModelExt.MemoNumbers = lstMNsShipReceiving.Items.Cast<ListItem>().Select(x => x.Value).ToList<string>();
				var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();

				string customerId = ddlCustomerLookupShipRecieving.SelectedValue.ToString();
				objNewOrderModelExt.Customer = customers.Find(m => m.CustomerId == customerId);


				objNewOrderModelExt.PersonCustomerID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).CustomerId);
				objNewOrderModelExt.PersonCustomerOfficeID = Convert.ToInt32(customers.Find(m => m.CustomerId == customerId).OfficeId);
				var result = FrontUtils.CreateOrder(objNewOrderModelExt, "ShipIn", this, out errMsg);
				objNewOrderModelExt.PickedUpByOurMessenger = chkPickedUpByOurMessenger_ShipReceiving.Checked == true ? true : false;
				if (chkPickedUpByOurMessenger_ShipReceiving.Checked == true)
				{
					var result_Messenger = FrontUtils.SetPickedUpByOurMessenger(objNewOrderModelExt, this, out errMsg);
				}
				if (result != null)
				{
					txtOrderNumberShipReceiving.Text = objNewOrderModelExt.OrderNumber;
					var okMsg =
						"New order number " + objNewOrderModelExt.OrderNumber + " " +
						"has been created for customer " + objNewOrderModelExt.Customer.CustomerName + "!";
					//System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + okMsg + "\")</SCRIPT>");
					//ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + okMsg + "');", true);
					ShipReceivingDefaultFields();
					lblOrderNumbeShipReceiving.Text = "Last Order# \n\r" + Environment.NewLine + objNewOrderModelExt.OrderNumber;
					btnPrintLabelShipIn.Enabled = true;
					btnPrintReceiptShipIn.Enabled = true;
					hdnOrderCode.Value = objNewOrderModelExt.OrderNumber;
					//if (rblLabelsShipReceiving.SelectedValue == "0")
					//{
					//    iFramePlaceOrder.Src = "XmlDownload.aspx?Type=Label&OrderCode=" + txtOrderNumberShipReceiving.Text;
					//}
				}
				else
				{
					PopupInfoDialog(errMsg, true);
					//System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + errMsg + "\")</SCRIPT>");
					//ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + errMsg + "');", true);
				}
			}
			catch (Exception ex)
			{
				errMsg = ex.Message;
			}
			return errMsg;
		}

		protected void OnCustomerSearchClickShipRecieving(object sender, EventArgs e)
		{
			//HideCustomerDetails();
			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
			var filterText = txtCustomerLookupShipRecieving.Text.Trim().ToLower();
			var filtered = string.IsNullOrEmpty(filterText) ? customers :
				customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
			//ddlCustomerLookupShipRecieving.DataSource = filtered;
			//ddlCustomerLookupShipRecieving.DataBind();

			//lstCustomerLookupShipRecieving.DataSource = filtered;
			//lstCustomerLookupShipRecieving.DataBind();
			if (filtered.Count == 1)
			{
				ddlCustomerLookupShipRecieving.SelectedValue = filtered[0].CustomerId;
				lstCustomerLookupShipRecieving.SelectedValue = filtered[0].CustomerId;
			}
			if (!string.IsNullOrEmpty(ddlCustomerLookupShipRecieving.SelectedValue))
			{
				OnCustomerSelectedChangedShipReceiving(null, null);
			}
		}

		protected void OnCustomerSelectedChangedShipReceiving(object sender, EventArgs e)
		{
			ddlCustomerLookupShipRecieving.ClearSelection();
			ddlCustomerLookupShipRecieving.Items.FindByValue(lstCustomerLookupShipRecieving.SelectedItem.Value).Selected = true;
			//HideCustomerDetails();
			if (string.IsNullOrEmpty(ddlCustomerLookupShipRecieving.SelectedValue)) return;

			var customer = GetCustomerFromView(ddlCustomerLookupShipRecieving.SelectedValue);
			if (customer == null) return;
			//HistoryRef.HRef = "~/CustomerHistory.aspx?CustomerCode=" + customer.CustomerCode;
			var infoModel = QueryCustomerUtils.GetCustomer(customer, this);

			///-Customer Popup Edit mode
			lblModelHeader.Text = "Edit Customer/Messenger";
			btnNewCustomerTakeIn.Text = "Edit Customer";
			btnNewMessengerTakeIn.Text = "Edit Messenger";
			iFrameNewCustomer.Src = "CustomerEdit.aspx?CustomerCode=" + customer.CustomerCode;


			if (ddlCustomerLookupShipRecieving.SelectedValue == "0")
				ShipRecievingControlsEnableDesable(false);
			else
				ShipRecievingControlsEnableDesable(true);

			lblCustomerShipReceiving.Text = ddlCustomerLookupShipRecieving.SelectedItem.Text.ToString();
			txtCustomerLookupShipRecieving.Text = ddlCustomerLookupShipRecieving.SelectedItem.Text.ToString();
			//lblCustomerNameDepartureSettingShipRecieving.Text = ddlCustomerLookupShipRecieving.SelectedItem.Text.ToString();
			lblOrderNumbeShipReceiving.Text = "";
		}

		protected void txtCustomerLookupShipRecieving_OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			//var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
			//var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
			bool changed = false;

			foreach (ListItem item in ddlCustomerLookupShipRecieving.Items)
			{
				//if (textBox.Text == item.Text)
				//if (item.Text.Contains(textBox.Text))
				string custNumbers = new String(textBox.Text.Where(Char.IsDigit).ToArray());
				if (item.Text.Contains(custNumbers == "" ? "$" : custNumbers))
				{
					ddlCustomerLookupShipRecieving.SelectedValue = item.Value;
					txtCustomerLookupShipRecieving.Text = txtCustomerLookupShipRecieving.Text;
					changed = true;
					break;
				}
			}

			if (!changed)
			{
				if (ddlCustomerLookupShipRecieving.SelectedItem != null)
				{
					textBox.Text = ddlCustomerLookupShipRecieving.SelectedItem.Text;
				}
				else
				{
					textBox.Text = "";
				}
			}
		}

		protected void btnSubmitShipReceiving_Click(object sender, EventArgs e)
		{
			if (hdnCarriersShipReceiving.Value == "" && chkPickedUpByOurMessenger_ShipReceiving.Checked == false)
			{
				PopupInfoDialog("Please select Carrier or Picked_Up_By_Our_Messenger", false);
				return;
			}
			SaveQPopupExtender.Show();
		}

		public void ShipRecievingControlsEnableDesable(bool Enabled)
		{
			//lstMessengerList.Enabled = Enabled;
			//txtNoOfItems.Enabled = Enabled;
			rdlNoOfItemsShipReceiving.Enabled = Enabled;
			//txtTotalWeight.Enabled = Enabled;
			//ddlWeightType.Enabled = Enabled;
			rdlTotalWeightShipReceiving.Enabled = Enabled;
			btnServiceTypeShipReceiving.Enabled = Enabled;
			txtSpecialInstructionsShipReceiving.Enabled = Enabled;
			txtMemoShipReceiving.Enabled = Enabled;
			txtMNShipReceiving.Enabled = Enabled;
			lstMNsShipReceiving.Enabled = Enabled;
			btnSubmitShipReceiving.Enabled = Enabled;
			btnCarriersShipReceiving.Enabled = Enabled;

			btnDepartureSettingShipRecieving.Enabled = Enabled;
		}

		protected void btnClearShipReceiving_Click(object sender, EventArgs e)
		{
			ShipReceivingDefaultFields();
		}

		protected void rbtnNoOfItemsShipReceiving_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rdlNoOfItemsShipReceiving.Items[0].Selected == true)
			{
				txtNoOfItemsShipReceiving.Enabled = true;
				lblNumberOfItemsShipReceiving.Text = "Number Of Items (Inspected): ";
			}
			else if (rdlNoOfItemsShipReceiving.Items[1].Selected == true)
			{
				txtNoOfItemsShipReceiving.Enabled = true;
				lblNumberOfItemsShipReceiving.Text = "Number Of Items (Not Inspected): ";
			}
			else
			{
				txtNoOfItemsShipReceiving.Enabled = false;
				lblNumberOfItemsShipReceiving.Text = "Number Of Items (N/A): ";
			}
		}

		protected void rbtnTotalWeightShipReceiving_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rdlTotalWeightShipReceiving.Items[0].Selected == true)
			{
				txtTotalWeightShipReceiving.Enabled = true;
				ddlWeightTypeShipRecieving.Enabled = true;
				lblTotalWeightShipReceiving.Text = "Total Weight (Inspected): ";
			}
			else if (rdlTotalWeightShipReceiving.Items[1].Selected == true)
			{
				txtTotalWeightShipReceiving.Enabled = true;
				ddlWeightTypeShipRecieving.Enabled = true;
				lblTotalWeightShipReceiving.Text = "Total Weight (Not Inspected): ";
			}
			else
			{
				txtTotalWeightShipReceiving.Enabled = false;
				ddlWeightTypeShipRecieving.Enabled = false;
				lblTotalWeightShipReceiving.Text = "Total Weight (N/A): ";
			}
		}

		protected void txtNoOfItemsShipReceiving_TextChanged(object sender, EventArgs e)
		{
			lblNumberOfItemsValueShipReceiving.Text = txtNoOfItemsShipReceiving.Text.ToString();
		}

		protected void txtTotalWeightShipReceiving_TextChanged(object sender, EventArgs e)
		{
			lblTotalWeightValueShipReceiving.Text = txtTotalWeightShipReceiving.Text.ToString();
		}

		protected void txtMemoShipReceiving_TextChanged(object sender, EventArgs e)
		{
			lblMemoShipReceiving.Text = txtMemoShipReceiving.Text.ToString();
		}

		protected void rdlServiceTypeShipReceiving_IndexChanged(object sender, EventArgs e)
		{
			txtServiceTypeShipReceiving.Text = rdlServiceTypeShipReceiving.SelectedItem.Text.ToString();
			hdnServiceTypeShipReceiving.Value = rdlServiceTypeShipReceiving.SelectedItem.Value.ToString();
			lblServiceTypeShipReceiving.Text = rdlServiceTypeShipReceiving.SelectedItem.Text.ToString();
		}

		protected void rdlCarriersShipReceiving_Changed(object sender, EventArgs e)
		{
			txtCarriersShipReceiving.Text = rdlCarriersShipReceiving.SelectedItem.Text;
			hdnCarriersShipReceiving.Value = rdlCarriersShipReceiving.SelectedItem.Value;
			lblCarrierShipReceiving.Text = rdlCarriersShipReceiving.SelectedItem.Text;
			chkPickedUpByOurMessenger_ShipReceiving.Checked = false;

		}

		private void ShipReceivingDefaultFields()
		{
			lstCustomerLookupShipRecieving.ClearSelection();
			txtCustomerLookupShipRecieving.Text = "";

			txtNoOfItemsShipReceiving.Text = "";
			txtTotalWeightShipReceiving.Text = "";
			ddlWeightTypeShipRecieving.SelectedIndex = 0;
			txtServiceTypeShipReceiving.Text = "5 days";
			hdnServiceTypeShipReceiving.Value = "4";

			rdlServiceTypeShipReceiving.SelectedIndex = 3;
			txtMemoShipReceiving.Text = "";
			txtSpecialInstructionsShipReceiving.Text = "";

			rdlNoOfItemsShipReceiving.SelectedIndex = 1;
			rdlTotalWeightShipReceiving.SelectedIndex = 1;
			txtMNShipReceiving.Text = "";
			lstMNsShipReceiving.Items.Clear();

			rbtnNoOfItemsShipReceiving_SelectedIndexChanged(null, null);
			rbtnTotalWeightShipReceiving_SelectedIndexChanged(null, null);

			txtCarriersShipReceiving.Text = "";
			hdnCarriersShipReceiving.Value = "";

			lblCustomerShipReceiving.Text = "";
			lblOrderNumbeShipReceiving.Text = "";
			lblNumberOfItemsValueShipReceiving.Text = "";
			lblTotalWeightValueShipReceiving.Text = "";
			lblServiceTypeShipReceiving.Text = "";
			lblMemoShipReceiving.Text = "";
			lblVendorShipRecieving.Text = "";
			txtScanPackageBarcodeShipRecieving.Text = "";
			lblOrderNumbeShipReceiving.Text = "";
			//txtOrderNumberShipReceiving.Text = "";

			chkPickedUpByOurMessenger_ShipReceiving.Checked = false;
			btnSubmitShipReceiving.Enabled = false;
			ShipRecievingControlsEnableDesable(false);
			btnPrintLabelShipIn.Enabled = false;
			btnPrintReceiptShipIn.Enabled = false;
		}

		protected void txtMNShipReceiving_TextChanged(object sender, EventArgs e)
		{
			if (lstMNsShipReceiving.Items.FindByText(txtMNShipReceiving.Text.ToString().Trim()) != null)
			{
				PopupInfoDialog("Memo Number Repeated", true);
				txtMNShipReceiving.Text = "";
				return;
			}
			if (txtMNShipReceiving.Text != "")
			{
				lstMNsShipReceiving.Items.Add(txtMNShipReceiving.Text.ToString().Trim());
				txtMNShipReceiving.Text = "";
			}
			txtMNShipReceiving.Focus();
		}

		protected void ddlVendorDepartureSettingShipRecieving_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			txtVendorDepartureSettingShipRecieving.Text = ddlVendorDepartureSettingShipRecieving.SelectedItem.Value.ToString();
			string customerName = ddlVendorDepartureSettingShipRecieving.SelectedItem.Text.ToString();
			txtVendorDepartureSettingShipRecieving.Text = customerName.Substring(customerName.Length - 4, 4);
			ModalPopupExtender7.Show();
		}

		protected void txtVendorDepartureSettingShipRecieving_OnTextChanged(object sender, EventArgs e)
		{
			ddlVendorDepartureSettingShipRecieving.ClearSelection();
			ddlVendorDepartureSettingShipRecieving.Items.Cast<ListItem>().Where(x => x.Text.Contains(txtVendorDepartureSettingShipRecieving.Text.ToString().Trim())).LastOrDefault().Selected = true;
			ModalPopupExtender7.Show();
		}

		protected void btnSetDepartureSettingShipRecieving_OnClick(object sender, EventArgs e)
		{
			lblVendorShipRecieving.Text = "Vendor: " + ddlVendorDepartureSettingShipRecieving.SelectedItem.Text.ToString().Trim();
		}

        protected void chkPickedUpByOurMessenger_ShipReceiving_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPickedUpByOurMessenger_ShipReceiving.Checked == true)
            {
                txtCarriersShipReceiving.Text = "";
                hdnCarriersShipReceiving.Value = "";
                rdlCarriersShipReceiving.ClearSelection();
                lblCarrierShipReceiving.Text = "";
            }
        }
		#endregion

		#region Ship Out (Tab-4)
		public void ShipOutControlsEnableDesable(bool Enabled)
		{
			btnDepartureSettingShipOut.Enabled = Enabled;
			btnCarriersShipOut.Enabled = Enabled;
			txtBarcodeShipOut.Enabled = Enabled;
			txtShippingChargesShipOut.Enabled = Enabled;
			//btnClearShipOut.Enabled = Enabled;
			btnSubmitShipOut.Enabled = Enabled;
		}

		protected void rdlCarriersShipOut_Changed(object sender, EventArgs e)
		{
			txtCarriersShipOut.Text = rdlCarriersShipOut.SelectedItem.Text;
			hdnCarriersShipOut.Value = rdlCarriersShipOut.SelectedItem.Value;
			chkTakenOutByOurMessengerShipOut.Checked = false;
		}

		protected void btnItemNoShipOut_Click(object sender, EventArgs e)
		{
			try
			{
				bool bOpenOrder = false;
				if (txtItemNoShipOut.Text.Trim().Length > 4)
				{
					ClearShipOut();
					lblMsgShipOut.Text = "";
					string[] sCodesArray = txtItemNoShipOut.Text.Split('.');
					DataSet dsOrders = new DataSet();

					int GroupCode = Convert.ToInt32(sCodesArray[0]);
					NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
					objNewOrderModelExt.GroupCode = GroupCode;
					objNewOrderModelExt.BatchCode = 0;
					objNewOrderModelExt.ItemCode = 0;
					objNewOrderModelExt.OperationChar = "";

					//Procedures spGetGroupByCode, spGetBatchByCode, spGetItemByCode, spGetItemDocByCode
					DataTable tblOrder = FrontUtils.GetGroupByCode(objNewOrderModelExt, this);
					DataTable tblBatch = FrontUtils.GetBatchbyCode(objNewOrderModelExt, this);
					DataTable tblItem = FrontUtils.GetItembyCode(objNewOrderModelExt, this);
					DataTable tblDoc = FrontUtils.GetItemDocByCode(objNewOrderModelExt, this);
					tblOrder.TableName = "tblOrder";
					tblBatch.TableName = "tblBatch";
					tblItem.TableName = "tblItem";
					tblDoc.TableName = "tblDoc";
					dsOrders.Tables.Add(tblOrder.Copy());
					dsOrders.Tables.Add(tblBatch.Copy());
					dsOrders.Tables.Add(tblItem.Copy());
					dsOrders.Tables.Add(tblDoc.Copy());

					SetViewState(dsOrders, "otShipOut");
					if (dsOrders.Tables[0].Rows.Count == 0)
					{
						lblMsgShipOut.Text = "Order # " + sCodesArray[0] + " doesn't exists";
						ShipOutControlsEnableDesable(false);
					}
					else
					{
						ShipOutControlsEnableDesable(true);
					}

					//if (System.Convert.ToInt32(dsOrders.Tables[0].Rows[0]["StateCode"]) == 1)
					//{
					//    if (MessageBox.Show("Order # " + sCodesArray[0] + " is closed. Would you like to open it temporary?", "Order status", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					//    {
					//        bOpenOrder = true;
					//    }
					//    else throw new Exception("Order# " + sCodesArray[0] + " is closed");
					//}
					//if (System.Convert.ToInt32(dsOrders.Tables[0].Rows[0]["StateCode"]) >= 2 || bOpenOrder == true)
					//{
					BatchModel batchModel = new BatchModel();
					batchModel.CustomerId = dsOrders.Tables[0].Rows[0]["CustomerID"].ToString();
					batchModel.CustomerOfficeId = dsOrders.Tables[0].Rows[0]["CustomerOfficeID"].ToString();

					var dtCustomer = QueryUtils.GetCustomerName(batchModel, this); //Procedure spGetCustomer
					txtCustomerNameShipOut.Text = dtCustomer.CustomerName;

					//Load Order Tree
					LoadItemNumbersTree1(txtItemNoShipOut.Text.ToString().Trim(), trvOrderTreeShipOut);
					if (tblItem.Rows.Count == 0)
					{
						btnSubmitShipOut.Enabled = false;
					}
					hdnOrderCode.Value = txtItemNoShipOut.Text.ToString().Trim();
					btnPrintLabelShipOut.Enabled = true;
					btnPrintReceiptShipOut.Enabled = true;
					//Check All Tree Nodes
					CheckAllNodes(trvOrderTreeShipOut.Nodes);
					trvOrderTreeShipOut.Enabled = false;

					if (dsOrders.Tables[0].Rows[0]["StateName"].ToString() == "closed")
					{
						btnSubmitShipOut.Enabled = false;
						lblMsgShipOut.Text = "Shipout for this order already done!";
						lblMsgShipOut.ForeColor = System.Drawing.Color.Red;
					}
					else
					{
						btnSubmitShipOut.Enabled = true;
						lblMsgShipOut.Text = "";
						lblMsgShipOut.ForeColor = System.Drawing.Color.Blue;
					}
				}
			}
			catch (Exception exc)
			{
				PopupInfoDialog("Data wasn't Loaded. Error: " + exc.Message, true);
			}

		}

        protected void btnSubmitShipOut_Click(object sender, EventArgs e)
        {
            if (hdnCarriersShipOut.Value == "" && chkTakenOutByOurMessengerShipOut.Checked == false)
            {
                PopupInfoDialog("Please select Carriers or Taken-Out-By-Our-Messenger", false);
                return;
            }

            SaveQPopupExtender.Show();
            
        }

        protected void btnClearShipOut_Click(object sender, EventArgs e)
        {
            ClearShipOut();
            txtItemNoShipOut.Text = "";
        }

        protected void chkTakenOutByOurMessengerShipOut_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTakenOutByOurMessengerShipOut.Checked == true)
            {
                txtCarriersShipOut.Text = "";
                hdnCarriersShipOut.Value = "";
                rdlCarriersShipOut.ClearSelection();
            }
        }

        private string SubmitShipOut()
        {
            string errMsg;
           
            DataTable dtVendor = FrontUtils.GetVendorStruct(this);//Procedure dbo.spGetGroupVendorTypeOf
            DataSet otShipOut = GetViewState("otShipOut") as DataSet;

            NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();
            objNewOrderModelExt.GroupOfficeID = otShipOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otShipOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[0].ToString());
            objNewOrderModelExt.GroupID = otShipOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString() == "" ? 0 : Convert.ToInt32(otShipOut.Tables[0].Rows[0]["GroupOfficeID_GroupID"].ToString().Split('_')[1].ToString());
            objNewOrderModelExt.GroupCode = otShipOut.Tables[0].Rows[0]["GroupCode"].ToString() == "" ? 0 : Convert.ToInt32(otShipOut.Tables[0].Rows[0]["GroupCode"].ToString());
            objNewOrderModelExt.VendorOfficeID = otShipOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString() == "" ? 0 : Convert.ToInt32(otShipOut.Tables[0].Rows[0]["CustomerOfficeID"].ToString());
            objNewOrderModelExt.VendorID = ddlVendorDepartureSettingShipOut.SelectedItem.Value == "" ? 0 : Convert.ToInt32(ddlVendorDepartureSettingShipOut.SelectedItem.Value.ToString());
            //otShipOut.Tables[0].Rows[0]["CustomerID"].ToString() == "" ? 0 : Convert.ToInt32(otShipOut.Tables[0].Rows[0]["CustomerID"].ToString());
            objNewOrderModelExt.ShipmentCharge = txtShippingChargesShipOut.Text.ToString().Trim() == "" ? 0 : Convert.ToInt32(txtShippingChargesShipOut.Text.ToString().Trim());
            objNewOrderModelExt.CarrierTrackingNumber = txtBarcodeShipOut.Text.ToString().Trim() == "" ? "" : txtBarcodeShipOut.Text.ToString().Trim();
            objNewOrderModelExt.CarrierID = hdnCarriersShipOut.Value == "" ? 0 : Convert.ToInt32(hdnCarriersShipOut.Value.ToString().Trim());
              
            var result_GroupVendor = FrontUtils.SetGroupVendor(objNewOrderModelExt, this, out errMsg); ////spSetGroupVendor

            objNewOrderModelExt.TakenOutByOurMessenger = chkTakenOutByOurMessengerShipOut.Checked == true ? true : false;


            DataTable dtTemp = new DataTable();
            dtTemp = otShipOut.Tables["tblItem"].Clone();
                   
            dtotGiveOutVal = new DataTable();
            dtotGiveOutVal = otShipOut.Tables["tblItem"].Clone();
            dtTemp = GetCheckedNodes(trvOrderTreeShipOut.Nodes, otShipOut.Tables["tblItem"]);

            DataTable dtItemOut = FrontUtils.GetItemOutStruct(this);

            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow drItem = dtItemOut.NewRow();
                drItem["BatchID_ItemCode"] = row["BatchID_ItemCode"];
                dtItemOut.Rows.Add(drItem);
            }
            objNewOrderModelExt.PersonID = 0;

            //Procedure dbo.spSetCloseGroupStateByCode and loop with procedure dbo.spSetCloseBatchStateByCode and dbo.spSetCloseItemStateByCode
            var result_setclosegroup = FrontUtils.SetCloseGroupStateByCode(objNewOrderModelExt, this, out errMsg);

			objNewOrderModelExt.GroupCode = Convert.ToInt32(txtItemNoShipOut.Text);

			//Procedure dbo.spSetCloseOrderStateByCode and for close detail to tblOrderOut Close table 
			var result_setcloseorder = FrontUtils.SetOrderOut(objNewOrderModelExt, this, out errMsg);

			//	#region BatchTracking
			DataSet dsTemp1 = otShipOut;
            dsTemp1.Tables["tblItem"].Clear();
            dsTemp1.Tables["tblItem"].Merge(dtTemp);

            

            if (dsTemp1.Tables["tblBatch"].Rows.Count > 0)
            {
                foreach (DataRow dr in dsTemp1.Tables["tblBatch"].Rows)
                {

                    DataRow[] drSet = dsTemp1.Tables["tblItem"].Select("NewBatchID = '" + dr["BatchID"].ToString() + "'");

                    if (drSet.Length > 0 && drSet.Length > 0)
                    {
                        DataTable dtitem = drSet.CopyToDataTable();

                        int BatchID = Convert.ToInt32(dr["BatchID"]);
                        int EventID = FrontUtils.BatchEvents.ShipOut;
                        int ItemsAffected = drSet.Length;
                        int ItemsInBatch = Convert.ToInt32(dr["ItemsQuantity"]);
                        int FormID = FrontUtils.Codes.AccRep;
                        FrontUtils.SetBatchEvent(EventID, BatchID, FormID, ItemsAffected, ItemsInBatch, this);//Procedure dbo.spSetBatchEvents

                        foreach (DataRow dritem in dtitem.Rows)
                        {
                            objNewOrderModelExt.ItemCode = Convert.ToInt32(dritem["ItemCode"]);// dsTemp1.Tables["tblItem"].Rows[0]["ItemCode"]
                            objNewOrderModelExt.BatchCode = Convert.ToInt32(dritem["BatchCode"].ToString());//Convert.ToInt32(dsTemp1.Tables["tblItem"].Rows[0]["BatchCode"].ToString());
                            FrontUtils.SetCloseItemStateByCode(objNewOrderModelExt, this, out errMsg); //spSetCloseItemStateByCode

                            objNewOrderModelExt.BatchID = Convert.ToInt32(dr["BatchID"]);

                            if (chkTakenOutByOurMessengerShipOut.Checked == true)
                            {
                                FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
                                FrontUtils.SetItemOutByOurMessenger(objNewOrderModelExt, this, out errMsg); //spSetItemOutByOurMessenger
                            }
                            else
                            {
                                FrontUtils.SetItemOut(objNewOrderModelExt, this, out errMsg); ////spSetItemOut
                            }

                        }
                    }
                    FrontUtils.SetCloseBatchStateByCode(objNewOrderModelExt, this, out errMsg);
                }
            }

            ClearShipOut();
            txtItemNoShipOut.Text = "";
            if (errMsg == "")
                lblMsgShipOut.Text = "Data was successfully added";
            else
                lblMsgShipOut.Text = "Error in ShipOut";
            return errMsg;

        }
        
        public void ClearShipOut()
		{
			ShipOutControlsEnableDesable(false);
			txtCustomerNameShipOut.Text = "";
			txtCarriersShipOut.Text = "";
			hdnCarriersShipOut.Value = "0";
			txtBarcodeShipOut.Text = "";
			txtShippingChargesShipOut.Text = "";
			chkTakenOutByOurMessengerShipOut.Checked = false;
			trvOrderTreeShipOut.Nodes.Clear();
			btnSubmitShipOut.Enabled = false;
			rdlCarriersShipOut.ClearSelection();
			lblVendorShipOut.Text = "";

		}

		protected void ddlVendorDepartureSettingShipOut_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			txtVendorDepartureSettingShipOut.Text = ddlVendorDepartureSettingShipOut.SelectedItem.Value.ToString();
			string customerName = ddlVendorDepartureSettingShipOut.SelectedItem.Text.ToString();
			txtVendorDepartureSettingShipOut.Text = customerName.Substring(customerName.Length - 4, 4);
			ModalPopupExtender16.Show();
		}

		protected void txtVendorDepartureSettingShipOut_OnTextChanged(object sender, EventArgs e)
		{
			ddlVendorDepartureSettingShipOut.ClearSelection();
			ddlVendorDepartureSettingShipOut.Items.Cast<ListItem>().Where(x => x.Text.Contains(txtVendorDepartureSettingShipOut.Text.ToString().Trim())).LastOrDefault().Selected = true;
			ModalPopupExtender16.Show();
		}

		protected void btnSetDepartureSettingShipOut_OnClick(object sender, EventArgs e)
		{
			lblVendorShipOut.Text = "Vendor: " + ddlVendorDepartureSettingShipOut.SelectedItem.Text.ToString().Trim();
		}
		#endregion

		#region EditOrder (Tab-5)

		public void EnableDesableEditOrderControls(bool Enabled)
		{
			txtNoOFItemsEditOrder.Visible = Enabled;
			txtTotalWeightEditOrder.Visible = Enabled;
			txtSpecialInstructionEditOrder.Visible = Enabled;
			txtMemoEditOrder.Visible = Enabled;
			lblEditOrderQty.Visible = Enabled;
			lblEditOrderTW.Visible = Enabled;
			lblEditOrderMemo.Visible = Enabled;
			lblEditOrderSP.Visible = Enabled;
            lblEditNoGoods.Visible = Enabled;
            chkEditNoGood.Visible = Enabled;
            btnSubmitEditOrder.Visible = Enabled;
			btnClearEditOrder.Visible = Enabled;

		}

		protected void btnEditOrder_Click(object sender, EventArgs e)
		{
			EnableDesableEditOrderControls(true);
			//ClearEditOrder();
			DataTable orderDetails = FrontUtils.GetOrderFullInfo(txtOrderCodeEditOrder.Text, this);
			var batchModel = QueryUtils.GetBatches(txtOrderCodeEditOrder.Text, "", this);
			if (batchModel.Count >= 1)
			{
				trEditOrderMemo.Visible = false;
			}
			else
			{
				trEditOrderMemo.Visible = true;
			}
			if (orderDetails != null)
			{
				txtNoOFItemsEditOrder.Text = orderDetails.Rows[0]["NotInspectedQuantity"].ToString();
				txtTotalWeightEditOrder.Text = orderDetails.Rows[0]["NotInspectedTotalWeight"].ToString();
				txtSpecialInstructionEditOrder.Text = orderDetails.Rows[0]["SpecialInstruction"].ToString();
				txtMemoEditOrder.Text = orderDetails.Rows[0]["Memo"].ToString();
				hdnMemoEditOrder.Value = orderDetails.Rows[0]["Memo"].ToString();
				lblMsgEditOrder.Text = "";
				lblEditOrderSearch.Text = "";
                chkEditNoGood.Checked = orderDetails.Rows[0]["IsNoGoods"].ToString() == "Yes" ? true : false;
            }
			else
			{
				EnableDesableEditOrderControls(false);
				lblEditOrderSearch.Text = "No Order Found !";
			}
		}
		protected void btnSubmitEditOrder_Click(object sender, EventArgs e)
		{
			NewOrderModel itemModel = new NewOrderModel();
			itemModel.OrderNumber = txtOrderCodeEditOrder.Text.ToString();
			itemModel.NumberOfItems = txtNoOFItemsEditOrder.Text.ToString().Trim().Length != 0 ? Int32.Parse(txtNoOFItemsEditOrder.Text.ToString().Trim()) : (int?)null;
			itemModel.TotalWeight = txtTotalWeightEditOrder.Text.ToString().Trim().Length != 0 ? Decimal.Parse(txtTotalWeightEditOrder.Text.ToString().Trim()) : (Decimal?)null;
			itemModel.OrderMemo = txtMemoEditOrder.Text.ToString().Trim();
			itemModel.SpecialInstructions = txtSpecialInstructionEditOrder.Text.ToString().Trim();
            itemModel.IsNoGoods = chkEditNoGood.Checked;

			if (hdnMemoEditOrder.Value.Trim().Length != 0 && txtMemoEditOrder.Text.Trim().Length == 0)
			{
				lblMsgEditOrder.Text = "Please enter memo";
				return;
			}

			var result = FrontUtils.UpdateGroupDetail(itemModel, this);
			if (result == "")
			{
				lblMsgEditOrder.Text = "Order Update successfully";
				ClearEditOrder();
			}
			else
			{
				lblMsgEditOrder.Text = result.ToString();
			}
		}

		protected void btnClearEditOrder_Click(object sender, EventArgs e)
		{
			ClearEditOrder();
			txtOrderCodeEditOrder.Text = "";
			txtOrderCodeEditOrder.Focus();
		}

		public void ClearEditOrder()
		{
			txtNoOFItemsEditOrder.Text = "";
			txtTotalWeightEditOrder.Text = "";
			txtSpecialInstructionEditOrder.Text = "";
			txtMemoEditOrder.Text = "";

			EnableDesableEditOrderControls(false);
		}



		#endregion

		#region View Order (Tab-6)
		public void ClearViewOrder()
		{
			txtViewOrder.Text = "";
			lblViewOrderCustomer.Text = "";
            lblNoGoods.Text = "";
            lblViewOrderVendorName.Text = "";
			
			lblViewOrderNumOfItemsNotInspected.Text = "";
			lblViewOrderTotalWeightNotInspected.Text = "";
			lblViewOrderNumOfItemsInspected.Text = "";
			lblViewOrderTotalWeightInspected.Text = "";
			lblViewOrderServiceType.Text = "";
			lblViewOrderMemo.Text = "";

			//lblViewTakeInOrderMessenger.Text = "";
			lblViewTakeInOrderCarrier.Text = "";
			lblViewTakeInOrderCarrierTrackingNumber.Text = "";

			//lblViewGiveOutOrderMessenger.Text = "";
			lblViewGiveOutOrderCarrier.Text = "";
			lblViewGiveOutOrderCarrierTrackingNumber.Text = "";

			lblViewOrderOrderState.Text = "";
			lblViewOrderCreateDate.Text = "";
			lblViewOrderStatus.Text = "";
			//imgMessanger.ImageUrl = imgTransperant;
			//imgSignature.ImageUrl = imgTransperant;

			lblViewTakeInSignatureTimeStamp.Text = "";
			lblViewGiveOutSignatureTimeStamp.Text = "";

			lblViewTakeInRecieptUploadedTimeStamp.Text = "";
			lblViewGiveOutRecieptUploadedTimeStamp.Text = "";

			lblRecieptTakeIn.Text = "";
			lblRecieptGiveOut.Text = "";
			lblViewOrderSearch.Text = "";

			btnUploadSignedOrderReciept.Enabled = false;
			btnPrintLabelViewOrder.Enabled = false;
			btnPrintReceiptViewOrder.Enabled = false;

			trViewOrderSummary.Visible = false;
			trViewOrderOperation.Visible = false;
			btnUploadSignedOrderReciept.Visible = false;
			btnPrintLabelViewOrder.Visible = false;
			btnPrintReceiptViewOrder.Visible = false;
			btnClearViewOrder.Visible = false;
		}

		protected void btnClearViewOrder_Click(object sender, EventArgs e)
		{
			ClearViewOrder();
		}

		protected void btnViewOrder_Click(object sender, EventArgs e)
        {
			try
			{
				DataTable orderDetails = FrontUtils.GetOrderSummery(txtViewOrder.Text, this);
				if (orderDetails != null)
				{
					trViewOrderSummary.Visible = true;
					trViewOrderOperation.Visible = true;
					btnUploadSignedOrderReciept.Visible = true;
					btnPrintLabelViewOrder.Visible = true;
					btnPrintReceiptViewOrder.Visible = true;
					btnClearViewOrder.Visible = true;

					hdnOrderCode.Value = orderDetails.Rows[0]["OrderCode"].ToString(); //For Print Receipt

					lblViewOrderCustomer.Text = orderDetails.Rows[0]["CustomerName"].ToString();
                    lblNoGoods.Text = orderDetails.Rows[0]["IsNoGoods"].ToString();

                    lblViewOrderVendorName.Text = orderDetails.Rows[0]["VendorName"].ToString();

					lblViewOrderNumOfItemsNotInspected.Text = orderDetails.Rows[0]["NotInspectedQuantity"].ToString();
					lblViewOrderTotalWeightNotInspected.Text = orderDetails.Rows[0]["NotInspectedTotalWeight"].ToString();
					lblViewOrderNumOfItemsInspected.Text = orderDetails.Rows[0]["InspectedQuantity"].ToString();
					lblViewOrderTotalWeightInspected.Text = orderDetails.Rows[0]["InspectedTotalWeight"].ToString();
					lblViewOrderServiceType.Text = orderDetails.Rows[0]["ServiceTypeName"].ToString();
					lblViewOrderMemo.Text = orderDetails.Rows[0]["Memo"].ToString();

					lblViewOrderOrderState.Text = orderDetails.Rows[0]["StateName"].ToString();
					lblViewOrderCreateDate.Text = orderDetails.Rows[0]["CreateDate"].ToString();
					lblViewOrderStatus.Text = orderDetails.Rows[0]["OrderStatus"].ToString();

					//hdnTakeInPersonID.Value = orderDetails.Rows[0]["PersonID"].ToString();
					//hdnGiveOutPersonID.Value = orderDetails.Rows[0]["PersonID"].ToString();
					hdnViewOrder.Value = orderDetails.Rows[0]["OrderCode"].ToString();

					//btnViewOrderReciept.Enabled = true;
					btnUploadSignedOrderReciept.Enabled = true;
					btnPrintLabelViewOrder.Enabled = true;
					btnPrintReceiptViewOrder.Enabled = true;
					lblViewOrderSearch.Text = "";


					var PickedUpByOurMessenger = orderDetails.Rows[0]["PickedUpByOurMessenger"].ToString();
					var TakenOutByOurMessenger = orderDetails.Rows[0]["TakenOutByOurMessenger"].ToString();
					if (PickedUpByOurMessenger=="1")
					{
						lblTakeInPickedUpByOurMessenger.Visible = true;

						trTakeInImages.Visible = false;
						trViewTakeInSignatureTimeStamp.Visible = false;
						trViewTakeInRecieptUploadedTimeStamp.Visible = false;
						trViewTakeInRecieptDownload.Visible = false;

						trViewTakeInOrderCarrier.Visible = false;
						trViewTakeInOrderCarrierTrackingNumber.Visible = false;
						lblTakeInSubmittedbytext.Text = "Submitted By:";
					}
					else if(orderDetails.Rows[0]["CarrierName"].ToString() != "")
					{
						lblTakeInPickedUpByOurMessenger.Visible = false;
						trTakeInImages.Visible = false;
						trViewTakeInSignatureTimeStamp.Visible = false;
						trViewTakeInRecieptUploadedTimeStamp.Visible = false;
						trViewTakeInRecieptDownload.Visible = false;

						trViewTakeInOrderCarrier.Visible = true;
						trViewTakeInOrderCarrierTrackingNumber.Visible = true;
						lblTakeInSubmittedbytext.Text = "Shipped By:";
					}
					else
					{
						lblTakeInPickedUpByOurMessenger.Visible = false;

						trTakeInImages.Visible = true;
						trViewTakeInSignatureTimeStamp.Visible = true;
						trViewTakeInRecieptUploadedTimeStamp.Visible = true;
						trViewTakeInRecieptDownload.Visible = true; ;

						trViewTakeInOrderCarrier.Visible = false;
						trViewTakeInOrderCarrierTrackingNumber.Visible = false;
						lblTakeInSubmittedbytext.Text = "Submitted By:";
					}

					if (TakenOutByOurMessenger == "1")
					{
						trGiveOutPickedUpByOurMessenger.Visible = true;

						trGiveOutImages.Visible = false;
						trViewGiveOutSignatureTimeStamp.Visible = false;
						trViewGiveOutRecieptUploadedTimeStamp.Visible = false;
						trViewGiveOutRecieptDownload.Visible = false;

						trViewGiveOutOrderCarrier.Visible = false;
						trViewGiveOutOrderCarrierTrackingNumber.Visible = false;

						lblViewGiveOutSubmittedBytext.Text = "Accepted By:";
					}
					else if(orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() != "")
					{

						trGiveOutPickedUpByOurMessenger.Visible = false;

						trGiveOutImages.Visible = false;
						trViewGiveOutSignatureTimeStamp.Visible = false;
						trViewGiveOutRecieptUploadedTimeStamp.Visible = false;
						trViewGiveOutRecieptDownload.Visible = false;

						trViewGiveOutOrderCarrier.Visible = true;
						trViewGiveOutOrderCarrierTrackingNumber.Visible = true;
						lblViewGiveOutSubmittedBytext.Text = "Shipped By:";
					}
					else
					{
						trGiveOutPickedUpByOurMessenger.Visible = false;

						trGiveOutImages.Visible = true;
						trViewGiveOutSignatureTimeStamp.Visible = true;
						trViewGiveOutRecieptUploadedTimeStamp.Visible = true;
						trViewGiveOutRecieptDownload.Visible = true;

						trViewGiveOutOrderCarrier.Visible = false;
						trViewGiveOutOrderCarrierTrackingNumber.Visible = false;
						lblViewGiveOutSubmittedBytext.Text = "Accepted By:";
					}

					///Carrier For Take In
					lblViewTakeInOrderCarrier.Text = orderDetails.Rows[0]["CarrierName"].ToString();
					lblViewTakeInOrderCarrierTrackingNumber.Text = orderDetails.Rows[0]["CarrierTrackingNumber"].ToString();
					lblTakeInSubmittedBy.Text = orderDetails.Rows[0]["Person"].ToString();
					lblViewTakeInAcceptedBy.Text = orderDetails.Rows[0]["TakeInUserName"].ToString();
					if (orderDetails.Rows[0]["CarrierName"].ToString() != "")
					{
						lblTakeInViewOrderLabel.Text = "Ship In";
						
					}
					else
					{
						lblTakeInViewOrderLabel.Text = "Take In";
						
					}

					///Carrier For Give Out
					lblViewGiveOutOrderCarrier.Text = orderDetails.Rows[0]["GiveOutCarrierName"].ToString();
					lblViewGiveOutOrderCarrierTrackingNumber.Text = orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
					lblViewGiveOutSubmittedBy.Text = orderDetails.Rows[0]["GiveOutPerson"].ToString();
					lblViewGiveOutAcceptedBy.Text = orderDetails.Rows[0]["GiveOutUserName"].ToString();

					if (orderDetails.Rows[0]["GiveOutCarrierTrackingNumber"].ToString() != "")
					{
						lblGiveOutViewOrderLabel.Text = "Ship Out";
					}
					else
					{
						lblGiveOutViewOrderLabel.Text = "Give Out";
					}

					if (orderDetails.Rows[0]["OrderStatus"].ToString() ==""|| orderDetails.Rows[0]["OrderStatus"].ToString() == "TakeIn" || orderDetails.Rows[0]["OrderStatus"].ToString() == "ShipIn" || orderDetails.Rows[0]["OrderStatus"].ToString() == "Itemizing")
					{
						tdGiveOut.Visible = false;
					}
					else
					{
						tdGiveOut.Visible = true; 
					}
					DataTable orderInOutDetails = FrontUtils.GetOrderInOutDetails(txtViewOrder.Text, this);
					if (orderInOutDetails != null)
					{
						string TakeInRecieptImage = orderInOutDetails.Rows[0]["TakeInRecieptImage"].ToString();
						string GiveOutRecieptImage = orderInOutDetails.Rows[0]["GiveOutRecieptImage"].ToString();

						lblViewTakeInSignatureTimeStamp.Text = orderInOutDetails.Rows[0]["TakeInSignatureTimeStamp"].ToString();
						lblViewGiveOutSignatureTimeStamp.Text = orderInOutDetails.Rows[0]["GiveOutSignatureTimeStamp"].ToString();

						lblViewTakeInRecieptUploadedTimeStamp.Text = orderInOutDetails.Rows[0]["TakeInRecieptUploadedTimeStamp"].ToString();
						lblViewGiveOutRecieptUploadedTimeStamp.Text = orderInOutDetails.Rows[0]["GiveOutRecieptUploadedTimeStamp"].ToString();

						lblRecieptTakeIn.Text = "";
						lblRecieptGiveOut.Text = "";
						// Take In
						//lblViewTakeInOrderMessenger.Text = orderInOutDetails.Rows[0]["TakeInPersonName"].ToString();
						if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["TakeInPersonPhoto"].ToString()))
						{
							imgTakeInMessanger.ImageUrl = orderInOutDetails.Rows[0]["TakeInPersonPhoto"].ToString();
						}
						else
						{
							imgTakeInMessanger.ImageUrl = imgTransperant;
						}
						if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["TakeInCaptureSignature"].ToString()))
						{
							imgTakeInSignature.ImageUrl = orderInOutDetails.Rows[0]["TakeInCaptureSignature"].ToString();
						}
						else
						{
							imgTakeInSignature.ImageUrl = imgTransperant;
						}

						// Give out 
						//lblViewGiveOutOrderMessenger.Text = orderInOutDetails.Rows[0]["GiveOutPersonName"].ToString();
						

						if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["GiveOutPersonPhoto"].ToString()))
						{
							imgGiveOutMessanger.ImageUrl = orderInOutDetails.Rows[0]["GiveOutPersonPhoto"].ToString();
						}
						else
						{
							imgGiveOutMessanger.ImageUrl = imgTransperant;
						}
						if (!string.IsNullOrEmpty(orderInOutDetails.Rows[0]["GiveOutCaptureSignature"].ToString()))
						{
							imgGiveOutSignature.ImageUrl = orderInOutDetails.Rows[0]["GiveOutCaptureSignature"].ToString();
						}
						else
						{
							imgGiveOutSignature.ImageUrl = imgTransperant;
						}

						if (!string.IsNullOrEmpty(TakeInRecieptImage))
						{
							lnkRecieptTakeInDownloadfrm.Attributes.Add("href", TakeInRecieptImage);
							lnkRecieptTakeInDownloadfrm.Visible = true;
						}
                        else
                        {
                            lnkRecieptTakeInDownloadfrm.Attributes.Remove("href");
                            lnkRecieptTakeInDownloadfrm.Visible = false;
                        }

                        if (!string.IsNullOrEmpty(GiveOutRecieptImage))
						{
							lnkRecieptGiveOutDownloadfrm.Attributes.Add("href", GiveOutRecieptImage);
							lnkRecieptGiveOutDownloadfrm.Visible = true;
						}
                        else
                        {
                            lnkRecieptGiveOutDownloadfrm.Attributes.Remove("href");
                            lnkRecieptGiveOutDownloadfrm.Visible = false;
                        }
                    }
					else
					{
						lblRecieptTakeIn.Text = "";
						lblRecieptGiveOut.Text = "";
					}
				}
				else
				{
					trViewOrderSummary.Visible = false;
					trViewOrderOperation.Visible = false;

					btnUploadSignedOrderReciept.Visible = false;
					btnPrintLabelViewOrder.Visible = false;
					btnPrintReceiptViewOrder.Visible = false;
					btnClearViewOrder.Visible = false;
					btnPrintLabelViewOrder.Enabled = false;
					btnPrintReceiptViewOrder.Enabled = false;
					lblViewOrderSearch.Text = "No Order Found !";
				}
			}
			catch(Exception ex)
			{
				PopupInfoDialog(ex.Message.ToString(), true);
			}
        }

		protected void AsyncFileUploadSignedOrderRecieptTakeIn_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadSignedOrderRecieptTakeIn.HasFile)
			{
				Session["AsyncFileUploadSignedOrderRecieptTakeIn"] = Convert.ToBase64String(AsyncFileUploadSignedOrderRecieptTakeIn.FileBytes);
				lblRecieptTakeIn.Text = "";
			}
		}

		protected void AsyncFileUploadSignedOrderRecieptGiveOut_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadSignedOrderRecieptGiveOut.HasFile)
			{
				Session["AsyncFileUploadSignedOrderRecieptGiveOut"] = Convert.ToBase64String(AsyncFileUploadSignedOrderRecieptGiveOut.FileBytes);
				lblRecieptGiveOut.Text = "";
			}
		}

		protected void btnUploadSignedOrderRecieptTakeIn_Click(object sender, EventArgs e)
		{
			int orderCode = Convert.ToInt32(hdnViewOrder.Value);
			string TakeInRecieptImage = Session["AsyncFileUploadSignedOrderRecieptTakeIn"].ToString();
            string fileName = AzureStorageBlob.GenerateFrontInOutFileName(orderCode.ToString(), FrontInOutFile.TakeInReceipt, this);
            string fileImagePath = AzureStorageBlob.UploadFileToBlob(TakeInRecieptImage, fileName, this);

            var msg = FrontUtils.SetOrderInOutDetails(orderCode, 0, null, fileImagePath, 0, null, null, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}

            lblRecieptTakeIn.Text = "Signed receipt uploaded successfully!";
			
			ModalPopupExtender17.Show();
			MainPanel.Update();
		}

		protected void btnUploadSignedOrderRecieptGiveOut_Click(object sender, EventArgs e)
		{
			int orderCode = Convert.ToInt32(hdnViewOrder.Value);
			string GiveOutRecieptImage = Session["AsyncFileUploadSignedOrderRecieptGiveOut"].ToString();
            string fileName = AzureStorageBlob.GenerateFrontInOutFileName(orderCode.ToString(), FrontInOutFile.GiveOutReceipt, this);
            string fileImagePath = AzureStorageBlob.UploadFileToBlob(GiveOutRecieptImage, fileName, this);

            var msg = FrontUtils.SetOrderInOutDetails(orderCode, 0, null, null, 0, null, fileImagePath, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}
			lblRecieptGiveOut.Text = "Signed receipt uploaded successfully!";
			
			ModalPopupExtender17.Show();
			MainPanel.Update();
		}
		#endregion

		#region ActiveTab Event
		protected void OnQuesSaveYesClick(object sender, EventArgs e)
		{
			var errMsg = "";

			//var errMsg = SaveExecute();

			if (TabContainer1.ActiveTabIndex == 0)  //Take In
			{
				errMsg = SubmitTakeIn();
			}
			else if (TabContainer1.ActiveTabIndex == 1) //Give Out
			{
				errMsg = SubmitGiveOut();
			}
			else if (TabContainer1.ActiveTabIndex == 2) //Ship receiving
			{
				errMsg = SubmitShipReceiving();
			}
			else if (TabContainer1.ActiveTabIndex == 3) //Ship Out
			{
				errMsg = SubmitShipOut();
			}

			if (!string.IsNullOrEmpty(errMsg))
			{
				PopupInfoDialog(errMsg, true);
				return;
			}
		}
		protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
		{
			TakeInDefaultFields();
			ClearGiveOut();
			ShipReceivingDefaultFields();
			ClearShipOut();
			ClearEditOrder();
			ClearViewOrder();

			txtItemNoGiveOut.Text = "";
			txtItemNoShipOut.Text = "";
			txtOrderCodeEditOrder.Text = "";

			

		}
		#endregion

		#region TreeView Methods Utils
		private static void LoadTreeMethods(List<CommunicationModel> items, TreeView tree)
		{
			items.Sort((m1, m2) => m1.Order.CompareTo(m2.Order));
			tree.Nodes.Clear();
			var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Communications" } };
			data.AddRange(items.Select(item => new TreeViewModel
			{
				ParentId = "0",
				Id = item.Code,
				DisplayName = item.Name,
			}));

			var root = TreeUtils.GetRootTreeModel(data);
			var rootNode = new TreeNode(root.DisplayName, root.Id);
			TreeUtils.FillNode(rootNode, root);
			tree.Nodes.Add(rootNode);
			foreach (TreeNode node in rootNode.ChildNodes)
			{
				var src = items.Find(m => m.Code == node.Value && m.Checked);
				node.Checked = src != null;
			}

		}
		private static void ReorderMethods(bool up, TreeView tree)
		{
			if (tree.SelectedNode == null || tree.SelectedNode.Value == "0") return;
			var nowInd = 0;
			var items = new List<CommunicationModel>();
			for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
			{
				var node = tree.Nodes[0].ChildNodes[i];
				if (node.Value == tree.SelectedValue) nowInd = i;
				items.Add(new CommunicationModel { Checked = node.Checked, Code = node.Value, Name = node.Text, Order = i });
			}
			if (up && nowInd == 0) return;
			if (!up && nowInd == (items.Count - 1)) return;
			var newInd = up ? (nowInd - 1) : (nowInd + 1);
			var item1 = items.Find(m => m.Order == nowInd);
			var item2 = items.Find(m => m.Order == newInd);
			if (item1 != null) item1.Order = newInd;
			if (item2 != null) item2.Order = nowInd;
			LoadTreeMethods(items, tree);
			//tree.Focus();
			tree.Nodes[0].ChildNodes[newInd].Selected = true;
		}
		private static void OnTreeMethodCheck(TreeView tree)
		{
			if (tree.CheckedNodes.Count > 0)
			{
				// the selected nodes.
				foreach (TreeNode node in tree.CheckedNodes)
				{
					if (node.ChildNodes.Count <= 0) continue;
					foreach (TreeNode childNode in node.ChildNodes)
					{
						childNode.Checked = true;
					}
				}
			}
		}
		private static string GetTreeMethodValue(TreeView tree)
		{
			var result = "";
			for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
			{
				var node = tree.Nodes[0].ChildNodes[i];
				result += node.Value + (node.Checked ? "1" : "0");
			}
			return result;
		}
		private static void SetTreeMethodValue(string value, TreeView tree)
		{
			var communications = new CommunicationsModel();
			communications.SetDbValue(value);
			LoadTreeMethods(communications.Items, tree);
		}
		protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
		{
			//int index = Int32.Parse(e.Item.Value);
			//MultiView1.ActiveViewIndex = index;
		}
		#endregion

		#region Validators

		private const string ValidatorGroupCompany = "ValGrpCompany";
		private const string ValidatorGroupPerson = "ValGrpPerson";

		protected bool IsGroupValid(string sValidationGroup)
		{
			Page.Validate(sValidationGroup);
			foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
			{
				if (!validator.IsValid)
				{
					return false;
				}
			}
			return true;
		}
		private string GetErrMessage(string sValidationGroup)
		{
			var msg = "";
			foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
			{
				if (!validator.IsValid)
				{
					msg += "<br/>" + validator.ToolTip;
				}
			}
			return msg;
		}
		#endregion

		#region Print Data in PDF
		protected void btnPrintReceiptTakeIn_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}
		protected void btnPrintLabelTakeIn_Click(object sender, EventArgs e)
		{
			PrintPDFLabel();
		}
		protected void btnPrintReceiptGiveOut_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}
		protected void btnPrintLabelGiveOut_Click(object sender, EventArgs e)
		{
			PrintPDFLabel();
		}
		protected void btnPrintLabelShipIn_Click(object sender, EventArgs e)
		{
			PrintPDFLabel();
		}
		protected void btnPrintReceiptShipIn_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}
		protected void btnPrintLabelShipOut_Click(object sender, EventArgs e)
		{
			PrintPDFLabel();
		}
		protected void btnPrintReceiptShipOut_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}
		protected void btnPrintLabelViewOrder_Click(object sender, EventArgs e)
		{
			PrintPDFLabel();
		}
		protected void btnPrintReceiptViewOrder_Click(object sender, EventArgs e)
		{
			PrintPDFReceipt();
		}

		public void PrintPDFReceipt()
		{
			string CustomerCode = "";
			string requestID = "";
			string orderCode = hdnOrderCode.Value.ToString().Trim();

			NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

			DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
			if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
			{
				objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
				objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
				CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
			}

			DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);

			objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
			objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
			CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
			var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
			var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
			var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
			var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
			objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

			string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
			string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
			string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
			string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
			string notInspectedQuantity = totalQ;
			string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
			string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
			string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
			string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
			string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
			string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
			string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();

			string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
			string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
			string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
			string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
			string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
			string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
			string inspectedUnit = "", notInspectedUnit = "";
			string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();

			//Take In
			string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
			string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
			string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
			string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
			string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
			string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
			string TakeInOrShipReceivingBy = string.Empty;
			if (TakeInCarrierName != "")
				TakeInOrShipReceivingBy = "Ship Receiving";
			else
				TakeInOrShipReceivingBy = "Take In";


			//Give Out
			string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
			string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
			string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
			string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
			string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
			string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
			string GiveOutOrShipOutBy = string.Empty;
			if (GiveOutCarrierName != "")
				GiveOutOrShipOutBy = "Ship Out";
			else
				GiveOutOrShipOutBy = "Take Out";

			OrderModel objOrderModel = new OrderModel();
			objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
			var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
			objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

			string groupMemoNumbers = null;
			if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
				groupMemoNumbers = "";
			else
			{
				for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
					groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
			}
			groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);

			string groupCode = orderCode;
			string filename = @"label_FrontNew_Take_In_" + groupCode + ".xml";

			var custCode = CustomerCode;
			string OrderBy = "";
			if (TabContainer1.ActiveTabIndex == 0)
			{
				if (!string.IsNullOrEmpty(PickedUpByOurMessenger))
					OrderBy = "TakeInPickedUpByOurMessenger";
				else
					OrderBy = "TakeIn";
			}
			else if (TabContainer1.ActiveTabIndex == 1)
			{
				OrderBy = "TakeOut";
			}

			if (inspectedUnitID == "1")
				inspectedUnit = "g";
			else if (inspectedUnitID == "2")
				inspectedUnit = "ct.";
			else if (inspectedUnitID == "3")
				inspectedUnit = "abst.";
			else if (inspectedUnitID == "4")
				inspectedUnit = "enum";
			else if (inspectedUnitID == "5")
				inspectedUnit = "string";
			if (notInspectedUnitID == "1")
				notInspectedUnit = "g";
			else if (notInspectedUnitID == "2")
				notInspectedUnit = "ct.";
			else if (notInspectedUnitID == "3")
				notInspectedUnit = "abst.";
			else if (notInspectedUnitID == "4")
				notInspectedUnit = "enum";
			else if (notInspectedUnitID == "5")
				notInspectedUnit = "string";

			// Create a Document object
			var document = new Document(PageSize.A4, 15, 15, 15, 15);
			float PageWidth = 95f;
			// Create a new PdfWrite object, writing the output to a MemoryStream
			var output = new MemoryStream();
			var writer = PdfWriter.GetInstance(document, output);

			// Open the Document for writing
			document.Open();

			// First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
			var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
			var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
			var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
			var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
			var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
			var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
			var bodyFontSmall = FontFactory.GetFont("Arial", 8, Font.NORMAL);

			// Finally, add an image in the upper right corner
			var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/logo.gif"));
			logo.SetAbsolutePosition(30, 760);
			logo.ScalePercent(60);
			document.Add(logo);

			// Add the Receipt title
			var rptHeader = new Paragraph("Gemological Science International", headerFont);

			rptHeader.Alignment = Element.ALIGN_CENTER;
			document.Add(rptHeader);

			var repSubHeader = new Paragraph("www.gemscience.net", bodyFont);
			repSubHeader.Alignment = Element.ALIGN_CENTER;
			document.Add(repSubHeader);
			document.Add(Chunk.NEWLINE);

			PdfPTable table = new PdfPTable(3);
			table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
			PdfPCell cellNY = new PdfPCell(new Phrase("New York Headquarters", FontFactory.GetFont("Arial", 12, Font.BOLD)));
			cellNY.Border = iTextSharp.text.Rectangle.NO_BORDER;
			PdfPCell cellMB = new PdfPCell(new Phrase("Mumbai", FontFactory.GetFont("Arial", 12, Font.BOLD)));
			cellMB.Border = iTextSharp.text.Rectangle.NO_BORDER;
			PdfPCell cellRG = new PdfPCell(new Phrase("Ramat Gan", FontFactory.GetFont("Arial", 12, Font.BOLD)));
			cellRG.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPCell cellNYadd = new PdfPCell(new Phrase("581 5th Avenue, Fourth floor,\nNew York, New York, 10017 \nPhone: +1(212) 207 - 4140 \nFax: +1(212) 207 - 4156", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellNYadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPCell cellMBadd = new PdfPCell(new Phrase("Gemological Science International Pvt. Ltd. \n601 - B, 6th Floor, Trade \nCenter BKC, Bandra(East) \nMumbai - 400051 \nPhone: +91(022) 26520354 \nFax: +91(022) 26524091 / 26527209", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMBadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPCell cellRGadd = new PdfPCell(new Phrase("GW Gemological Science Israel Ltd \n54 Bezalel Street, \nRamat Gan, 52521, Israel \nPhone: +972 03 - 752 - 3272", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellRGadd.Border = iTextSharp.text.Rectangle.NO_BORDER;

			table.SetTotalWidth(new float[] { 300f, 410f, 340f });
			table.AddCell(cellNY);
			table.AddCell(cellMB);
			table.AddCell(cellRG);
			table.AddCell(cellNYadd);
			table.AddCell(cellMBadd);
			table.AddCell(cellRGadd);
			document.Add(new Paragraph("\n"));
			table.WidthPercentage = PageWidth;
			document.Add(table);

			// Add the Order Barcode
			Barcode128 bc = new Barcode128();
			bc.TextAlignment = Element.ALIGN_LEFT;
			bc.Code = orderCode;
			bc.StartStopText = false;
			bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
			bc.Extended = true;
			bc.AltText = "";
			bc.X = 1.5f;
			//bc.BarHeight = 10f;
			bc.BarHeight = (bc.BarcodeSize.Width) * (float)(0.15);


			PdfContentByte cb = writer.DirectContent;
			iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
			patImage.SetAbsolutePosition(30, 600);
			patImage.ScaleAbsolute(100, 40);
			//patImage.ScalePercent(100);
			document.Add(patImage);

			PdfPCell barcodeBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
			barcodeBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;

			// Add the Receipt Order
			var rptOrder = new Paragraph("Order   " + orderCode, FontFactory.GetFont("Arial", 16, Font.BOLD));
			rptOrder.SpacingBefore = 10;

			// Add the Memo
			var rptMemo = new Paragraph("Main Memo:   " + memo, FontFactory.GetFont("Arial", 14, Font.BOLD));
			rptMemo.SpacingBefore = 10;

			PdfPTable tableBarcode = new PdfPTable(3);
			tableBarcode.SetTotalWidth(new float[] { 300f, 310f, 440f });
			tableBarcode.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
			tableBarcode.AddCell(barcodeBlank);
			tableBarcode.AddCell(rptOrder);
			tableBarcode.AddCell(rptMemo);
			document.Add(new Paragraph("\n"));
			document.Add(new Paragraph("\n"));
			tableBarcode.WidthPercentage = PageWidth;
			document.Add(tableBarcode);


			//Add Customer Detail
			string customerAddress = customerName + ",\n" + address + ",\n" + city + ", " + stateName + ",\n" + country + "-" + zip + ",\n" + phone + ",\n" + fax;
			PdfPCell cell = new PdfPCell(new Phrase(customerAddress, FontFactory.GetFont("Arial", 11, Font.NORMAL)));
			cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
			cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
			cell.SetLeading(1, 1.2f);

			PdfPTable tableNoOfItem = new PdfPTable(3);

			PdfPCell cellAcceptAt = new PdfPCell(new Phrase("Accepted at: " + orderCreateDate, FontFactory.GetFont("Arial", 11, Font.NORMAL)));
			cellAcceptAt.Colspan = 3;
			cellAcceptAt.HorizontalAlignment = 0;
			cellAcceptAt.FixedHeight = 25f;
			cellAcceptAt.Border = Rectangle.BOTTOM_BORDER;

			PdfPCell cellNoOfItems = new PdfPCell(new Phrase("Number Of Items:\n(Not Inspected)", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
			cellNoOfItems.Colspan = 2;
			cellNoOfItems.FixedHeight = 30f;
			cellNoOfItems.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
			PdfPCell cellNoOfItemsVal = new PdfPCell(new Phrase(totalQ, FontFactory.GetFont("Arial", 13, Font.BOLDITALIC)));
			cellNoOfItemsVal.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
			cellNoOfItemsVal.Padding = 5;

			PdfPCell cellTotalWeight = new PdfPCell(new Phrase("Total Weight:\n(Not Inspected)", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
			cellTotalWeight.Colspan = 2;
			cellTotalWeight.FixedHeight = 30f;
			cellTotalWeight.Border = Rectangle.TOP_BORDER;
			PdfPCell cellTotalWeightVal = new PdfPCell(new Phrase(totalW + "  ct.", FontFactory.GetFont("Arial", 13, Font.BOLDITALIC)));
			cellTotalWeightVal.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
			cellTotalWeightVal.Padding = 5;


			//tableNoOfItem.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
			tableNoOfItem.AddCell(cellAcceptAt);
			tableNoOfItem.AddCell(cellNoOfItems);
			tableNoOfItem.AddCell(cellNoOfItemsVal);
			tableNoOfItem.AddCell(cellTotalWeight);
			tableNoOfItem.AddCell(cellTotalWeightVal);


			PdfPTable tableDetail = new PdfPTable(2);
			tableDetail.SetTotalWidth(new float[] { 600f, 400f });
			tableDetail.AddCell(cell);
			tableDetail.AddCell(tableNoOfItem);
			document.Add(new Paragraph("\n"));
			tableDetail.WidthPercentage = PageWidth;
			document.Add(tableDetail);

			//Add Instruction and memo

			PdfPCell cellInstruction = new PdfPCell(new Phrase("Special Instructions:", new Font(bodyFont)));
			cellInstruction.FixedHeight = 70f;
			cellInstruction.VerticalAlignment = Element.ALIGN_MIDDLE;
			PdfPCell cellInstructionVal = new PdfPCell(new Phrase(specialInstr, new Font(bodyFont)));

			PdfPCell cellMemo = new PdfPCell(new Phrase("Memo Numbers:", new Font(bodyFont)));
			cellMemo.FixedHeight = 70f;
			cellMemo.VerticalAlignment = Element.ALIGN_MIDDLE;
			PdfPCell cellMemoVal = new PdfPCell(new Phrase(memo, new Font(bodyFont)));

			PdfPTable tableInstruction = new PdfPTable(2);
			tableInstruction.SetTotalWidth(new float[] { 200f, 800f });
			tableInstruction.AddCell(cellInstruction);
			tableInstruction.AddCell(cellInstructionVal);
			tableInstruction.AddCell(cellMemo);
			tableInstruction.AddCell(cellMemoVal);
			document.Add(new Paragraph("\n"));
			tableInstruction.WidthPercentage = PageWidth;
			document.Add(tableInstruction);

			//Add Takein TakeOut
			PdfPTable tableInOut = new PdfPTable(2);
			tableInOut.SetTotalWidth(new float[] { 500f, 500f });
			tableInOut.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPTable tableIn = new PdfPTable(2);
			tableIn.SetTotalWidth(new float[] { 130f, 370f });
			PdfPTable tableOut = new PdfPTable(2);
			tableOut.SetTotalWidth(new float[] { 130f, 370f });
			PdfPCell cellTakeIn = new PdfPCell(new Phrase(TakeInOrShipReceivingBy, new Font(bodyFont)));
			cellTakeIn.Border = Rectangle.RIGHT_BORDER;
			cellTakeIn.Colspan = 2;
			cellTakeIn.HorizontalAlignment = 1;


			PdfPCell cellTakeOut = new PdfPCell(new Phrase(GiveOutOrShipOutBy, new Font(bodyFont)));
			cellTakeOut.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellTakeOut.HorizontalAlignment = 1;
			cellTakeOut.Colspan = 2;


			PdfPCell cellTakeInrow = new PdfPCell(new Phrase("\n\n", new Font(bodyFont)));
			cellTakeInrow.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellTakeInrow.Border = Rectangle.RIGHT_BORDER;
			cellTakeInrow.HorizontalAlignment = 1;
			PdfPCell cellTakeOutrow = new PdfPCell(new Phrase("\n\n", new Font(bodyFont)));
			cellTakeOutrow.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellTakeOutrow.HorizontalAlignment = 1;


			PdfPCell cellSubmitedBy = new PdfPCell(new Phrase("Submitted By:", new Font(bodyFont)));
			cellSubmitedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellSubmitedBy.VerticalAlignment = Element.ALIGN_MIDDLE;
			cellSubmitedBy.FixedHeight = 50f;

			PdfPCell cellAcceptedBy = new PdfPCell(new Phrase("Accepted By:", new Font(bodyFont)));
			cellAcceptedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellAcceptedBy.VerticalAlignment = Element.ALIGN_TOP;
			cellAcceptedBy.FixedHeight = 50f;
			cellAcceptedBy.PaddingTop = 20f;

			tableIn.AddCell(cellTakeIn);
			tableOut.AddCell(cellTakeOut);
			tableIn.AddCell(cellSubmitedBy);
			tableOut.AddCell(cellAcceptedBy);

			if (PickedUpByOurMessenger != "")
			{
				PdfPCell cellPickedUp = new PdfPCell(new Phrase("\n\nPicked Up By Our Messenger\n\n\n", new Font(bodyFont)));
				cellPickedUp.Border = Rectangle.RIGHT_BORDER;
				tableIn.AddCell(cellPickedUp);
			}
			else if (TakeInPersonName != "")
			{
				if (TakeInCaptureSignature != "")
				{
					iTextSharp.text.Image SubmitedBySign = iTextSharp.text.Image.GetInstance(TakeInCaptureSignature);
					SubmitedBySign.SetAbsolutePosition(120, 230);
					SubmitedBySign.ScaleAbsolute(100, 100);
					document.Add(SubmitedBySign);
					PdfPCell SubmitedBySignBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
					SubmitedBySignBlank.Border = Rectangle.RIGHT_BORDER;
					SubmitedBySignBlank.FixedHeight = 70f;
					tableIn.AddCell(SubmitedBySignBlank);
				}
				else
				{
					PdfPCell SubmitedBySign = new PdfPCell(new Phrase("", new Font(bodyFont)));
					SubmitedBySign.Border = Rectangle.RIGHT_BORDER;
					tableIn.AddCell(SubmitedBySign);
				}
				PdfPCell CarrierName = new PdfPCell(new Phrase("", new Font(bodyFont)));
				CarrierName.Border = iTextSharp.text.Rectangle.NO_BORDER;
				tableIn.AddCell(CarrierName);

				PdfPCell TakeInPersonNamecell = new PdfPCell(new Phrase("\n" + TakeInPersonName + "\n\n\n", new Font(bodyFont)));
				TakeInPersonNamecell.Border = Rectangle.RIGHT_BORDER;
				TakeInPersonNamecell.VerticalAlignment = Element.ALIGN_MIDDLE;
				TakeInPersonNamecell.PaddingLeft = 45f;
				tableIn.AddCell(TakeInPersonNamecell);

			}
			else if (TakeInCarrierName != "")
			{
				PdfPCell TrackingNumber = new PdfPCell(new Phrase("\n\n" + TakeInCarrierName + "\nTracking Number: " + TakeInCarrierTrackingNumber + "\n\n\n", new Font(bodyFont)));
				TrackingNumber.Border = Rectangle.RIGHT_BORDER;
				tableIn.AddCell(TrackingNumber);
			}
			else
			{
				PdfPCell TrackingNumber = new PdfPCell(new Phrase("", new Font(bodyFont)));
				TrackingNumber.Border = Rectangle.RIGHT_BORDER;
				tableIn.AddCell(TrackingNumber);
			}
			//Give Out /Ship Out
			//if (TakenOutByOurMessenger != "")
			if (!(String.IsNullOrEmpty(TakenOutByOurMessenger) || TakenOutByOurMessenger == "0"))
			{
				PdfPCell cellPickedUp = new PdfPCell(new Phrase("\n\nTaken Out By Our Messenger\n\n\n", new Font(bodyFont)));
				cellPickedUp.Border = iTextSharp.text.Rectangle.NO_BORDER;
				tableOut.AddCell(cellPickedUp);
			}
			else if (GiveOutPersonName != "")
			{
				if (GiveOutCaptureSignature != "")
				{
					var AcceptedBySign = iTextSharp.text.Image.GetInstance(GiveOutCaptureSignature);
					AcceptedBySign.SetAbsolutePosition(390, 240);
					AcceptedBySign.ScaleAbsolute(100, 100);
					document.Add(AcceptedBySign);
					PdfPCell AcceptedBySignBlank = new PdfPCell(new Phrase("", new Font(bodyFont)));
					AcceptedBySignBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
					AcceptedBySignBlank.FixedHeight = 70f;
					tableOut.AddCell(AcceptedBySignBlank);
				}
				else
				{
					PdfPCell AcceptedBySign = new PdfPCell(new Phrase("", new Font(bodyFont)));
					tableOut.AddCell(AcceptedBySign);
				}
				PdfPCell CarrierName = new PdfPCell(new Phrase("", new Font(bodyFont)));
				CarrierName.Border = iTextSharp.text.Rectangle.NO_BORDER;
				tableOut.AddCell(CarrierName);

				PdfPCell GiveOutPersonNamecell = new PdfPCell(new Phrase("\n" + GiveOutPersonName + "\n\n\n", new Font(bodyFont)));
				GiveOutPersonNamecell.Border = iTextSharp.text.Rectangle.NO_BORDER;
				GiveOutPersonNamecell.VerticalAlignment = Element.ALIGN_MIDDLE;
				GiveOutPersonNamecell.PaddingLeft = 40f;
				tableOut.AddCell(GiveOutPersonNamecell);
			}
			else if (GiveOutCarrierName != "")
			{

				PdfPCell TrackingNumber = new PdfPCell(new Phrase("\n\n" + GiveOutCarrierName + "\nTracking Number: " + GiveOutCarrierTrackingNumber + "\n\n\n", new Font(bodyFont)));
				TrackingNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
				tableOut.AddCell(TrackingNumber);
			}
			else
			{
				PdfPCell TrackingNumber = new PdfPCell(new Phrase("", new Font(bodyFont)));
				TrackingNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
				tableOut.AddCell(TrackingNumber);
			}

			PdfPCell cellTakeInAcceptedBy = new PdfPCell(new Phrase("Accepted By:", new Font(bodyFont)));
			cellTakeInAcceptedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;

			tableIn.AddCell(cellTakeInAcceptedBy);
			PdfPCell cellTakeInAcceptedByVal = new PdfPCell(new Phrase(TakeInUserName, new Font(bodyFont)));
			cellTakeInAcceptedByVal.Border = Rectangle.RIGHT_BORDER;
			tableIn.AddCell(cellTakeInAcceptedByVal);

			PdfPCell cellGiveOutSubmittedBy = new PdfPCell(new Phrase("Submitted By:", new Font(bodyFont)));
			cellGiveOutSubmittedBy.Border = iTextSharp.text.Rectangle.NO_BORDER;
			tableOut.AddCell(cellGiveOutSubmittedBy);
			PdfPCell cellGiveOutSubmittedByVal = new PdfPCell(new Phrase(GiveOutUserName, new Font(bodyFont)));
			cellGiveOutSubmittedByVal.Border = iTextSharp.text.Rectangle.NO_BORDER;
			tableOut.AddCell(cellGiveOutSubmittedByVal);



			tableInOut.AddCell(tableIn);
			tableInOut.AddCell(tableOut);

			document.Add(new Paragraph("\n"));
			tableInOut.WidthPercentage = PageWidth;
			document.Add(tableInOut);
			document.Add(new Paragraph("\n"));
			document.Add(new Paragraph("\n"));

			string Note = "The diamonds  have been purchased from legitimate sources not involved in funding conflict and in compliance with United Nations ";
			Note += "resolutions.The submitter hereby guarantees that these diamonds are conflict free, based on personal knowledge and/ or written ";
			Note += "guarantees provided by the supplier of these diamonds.";

			PdfPTable tableNote = new PdfPTable(1);
			PdfPCell cellNote = new PdfPCell(new Phrase(Note, new Font(bodyFont)));
			cellNote.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellNote.SetLeading(1, 1.2f);
			tableNote.AddCell(cellNote);
			tableNote.WidthPercentage = PageWidth;
			document.Add(tableNote);
			//document.Add(new Paragraph(Note, bodyFont));

			//Add Customer Detail


			document.Close();

			this.Response.Clear();
			this.Response.ContentType = "application/pdf";
			this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=External_Receipt_Order-{0}.pdf", orderCode));
			this.Response.BinaryWrite(output.ToArray());
			this.Response.End();
		}

		public void PrintPDFLabel()
		{
			string CustomerCode = "";
			string requestID = "";
			string orderCode = hdnOrderCode.Value.ToString().Trim();

			NewOrderModelExt objNewOrderModelExt = new NewOrderModelExt();

			DataSet dsGroupDetail = FrontUtils.GetGroupDetail(orderCode, this);
			if (dsGroupDetail != null && dsGroupDetail.Tables[0].Rows.Count != 0)
			{
				objNewOrderModelExt.GroupID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][0].ToString());
				objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsGroupDetail.Tables[0].Rows[0][1].ToString());
				CustomerCode = dsGroupDetail.Tables[0].Rows[0][2].ToString();
			}

			DataTable dsOrderSummery = FrontUtils.GetOrderSummery(orderCode, this);

			objNewOrderModelExt.GroupID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupID"].ToString());
			objNewOrderModelExt.GroupOfficeID = Convert.ToInt32(dsOrderSummery.Rows[0]["GroupOfficeID"].ToString());
			CustomerCode = dsGroupDetail.Tables[0].Rows[0]["CustomerCode"].ToString();
			var specialInstr = dsOrderSummery.Rows[0]["SpecialInstruction"].ToString();
			var memo = dsOrderSummery.Rows[0]["Memo"].ToString();
			var totalQ = dsOrderSummery.Rows[0]["NotInspectedQuantity"].ToString();
			var totalW = dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString();
			objNewOrderModelExt.TotalWeight = Convert.ToDecimal(string.IsNullOrEmpty(dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString()) ? "0" : dsOrderSummery.Rows[0]["NotInspectedTotalWeight"].ToString());

			string isNoGoods = dsOrderSummery.Rows[0]["IsNoGoods"].ToString();
			string serviceTypeName = dsOrderSummery.Rows[0]["ServiceTypeName"].ToString();
			string customerOfficeCode = dsOrderSummery.Rows[0]["CustomerOfficeCode"].ToString();
			string customerName = dsOrderSummery.Rows[0]["CustomerName"].ToString();
			string notInspectedQuantity = totalQ;
			string address = dsOrderSummery.Rows[0]["CustomerAddress1"].ToString() + " " + dsOrderSummery.Rows[0]["CustomerAddress2"].ToString();
			string city = dsOrderSummery.Rows[0]["CustomerCity"].ToString();
			string zip = dsOrderSummery.Rows[0]["CustomerZip"].ToString();
			string stateName = dsOrderSummery.Rows[0]["CustomerState"].ToString();
			string country = dsOrderSummery.Rows[0]["CustomerCountry"].ToString();
			string phone = dsOrderSummery.Rows[0]["CustomerPhone"].ToString();
			string fax = dsOrderSummery.Rows[0]["CustomerFax"].ToString();

			string sPersonID = dsOrderSummery.Rows[0]["PersonID"].ToString();
			string sPersonCustomerID = dsOrderSummery.Rows[0]["PersonCustomerID"].ToString();
			string sPersonCustomerOfficeID = dsOrderSummery.Rows[0]["PersonCustomerOfficeID"].ToString();
			string orderCreateDate = Convert.ToDateTime(dsOrderSummery.Rows[0]["CreateDate"]).ToString("M/d/yyyy  hh:mm tt", CultureInfo.InvariantCulture);
			string inspectedUnitID = (dsOrderSummery.Rows[0]["InspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["InspectedWeightUnitID"].ToString();
			string notInspectedUnitID = (dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"] == DBNull.Value) ? "" : dsOrderSummery.Rows[0]["NotInspectedWeightUnitID"].ToString();
			string inspectedUnit = "", notInspectedUnit = "";
			string StateName = dsOrderSummery.Rows[0]["OrderStatus"].ToString();

			//Take In
			string TakeInPersonName = dsOrderSummery.Rows[0]["Person"].ToString();
			string TakeInPositionName = dsOrderSummery.Rows[0]["PositionName"].ToString();
			string TakeInPersonPhone = dsOrderSummery.Rows[0]["Phone"].ToString();

			string TakeInCaptureSignature = dsOrderSummery.Rows[0]["TakeInCaptureSignature"].ToString();
			string TakeInCarrierName = dsOrderSummery.Rows[0]["CarrierName"].ToString();
			string TakeInCarrierTrackingNumber = dsOrderSummery.Rows[0]["CarrierTrackingNumber"].ToString();
			string PickedUpByOurMessenger = dsOrderSummery.Rows[0]["PickedUpByOurMessenger"].ToString();
			string TakeInUserName = dsOrderSummery.Rows[0]["TakeInUserName"].ToString();
			string TakeInOrShipReceivingBy = string.Empty;
			if (TakeInCarrierName != "")
				TakeInOrShipReceivingBy = "Ship Receiving";
			else
				TakeInOrShipReceivingBy = "Take In";


			//Give Out
			string GiveOutPersonName = dsOrderSummery.Rows[0]["GiveOutPerson"].ToString();
			string GiveOutCaptureSignature = dsOrderSummery.Rows[0]["GiveOutCaptureSignature"].ToString();
			string GiveOutCarrierName = dsOrderSummery.Rows[0]["GiveOutCarrierName"].ToString();
			string GiveOutCarrierTrackingNumber = dsOrderSummery.Rows[0]["GiveOutCarrierTrackingNumber"].ToString();
			string TakenOutByOurMessenger = dsOrderSummery.Rows[0]["TakenOutByOurMessenger"].ToString();
			string GiveOutUserName = dsOrderSummery.Rows[0]["GiveOutUserName"].ToString();
			string GiveOutOrShipOutBy = string.Empty;
			if (GiveOutCarrierName != "")
				GiveOutOrShipOutBy = "Ship Out";
			else
				GiveOutOrShipOutBy = "Take Out";

			OrderModel objOrderModel = new OrderModel();
			objOrderModel.GroupId = Convert.ToInt32(objNewOrderModelExt.GroupID);
			var OrderMemos = QueryUtils.GetOrderMemos(objOrderModel, this);
			objNewOrderModelExt.MemoNumbers = OrderMemos.Select(x => x.MemoNumber).ToList<string>();

			string groupMemoNumbers = null;
			if (objNewOrderModelExt.MemoNumbers == null || objNewOrderModelExt.MemoNumbers.ToString() == "" || objNewOrderModelExt.MemoNumbers.Count == 0)
				groupMemoNumbers = "";
			else
			{
				for (int i = 0; i <= objNewOrderModelExt.MemoNumbers.Count - 1; i++)
					groupMemoNumbers += "," + objNewOrderModelExt.MemoNumbers[i];
			}
			groupMemoNumbers = groupMemoNumbers == "" ? "" : groupMemoNumbers.Substring(1);

			string groupCode = orderCode;
			string filename = @"label_FrontNew_Take_In_" + groupCode + ".xml";

			var custCode = CustomerCode;
			string OrderBy = "";
			if (TabContainer1.ActiveTabIndex == 0)
			{
				if (!string.IsNullOrEmpty(PickedUpByOurMessenger))
					OrderBy = "TakeInPickedUpByOurMessenger";
				else
					OrderBy = "TakeIn";
			}
			else if (TabContainer1.ActiveTabIndex == 1)
			{
				OrderBy = "TakeOut";
			}

			if (inspectedUnitID == "1")
				inspectedUnit = "g";
			else if (inspectedUnitID == "2")
				inspectedUnit = "ct.";
			else if (inspectedUnitID == "3")
				inspectedUnit = "abst.";
			else if (inspectedUnitID == "4")
				inspectedUnit = "enum";
			else if (inspectedUnitID == "5")
				inspectedUnit = "string";
			if (notInspectedUnitID == "1")
				notInspectedUnit = "g";
			else if (notInspectedUnitID == "2")
				notInspectedUnit = "ct.";
			else if (notInspectedUnitID == "3")
				notInspectedUnit = "abst.";
			else if (notInspectedUnitID == "4")
				notInspectedUnit = "enum";
			else if (notInspectedUnitID == "5")
				notInspectedUnit = "string";

			// Create a Document object
			var document = new Document(PageSize.A4, 15, 15, 15, 15);
			float PageWidth = 95f;
			// Create a new PdfWrite object, writing the output to a MemoryStream
			var output = new MemoryStream();
			var writer = PdfWriter.GetInstance(document, output);

			// Open the Document for writing
			document.Open();

			// First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
			var headerFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
			var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
			var subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
			var boldTableFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
			var endingMessageFont = FontFactory.GetFont("Arial", 8, Font.ITALIC);
			var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
			var bodyFontSmall = FontFactory.GetFont("Arial", 8, Font.NORMAL);

			// Add the Order Barcode
			Barcode128 bc = new Barcode128();
			bc.TextAlignment = Element.ALIGN_LEFT;
			bc.Code = orderCode;
			bc.StartStopText = false;
			bc.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
			bc.Extended = true;
			//bc.AltText = "";
			bc.X = 1.5f;
			//bc.BarHeight = 10f;
			bc.BarHeight = (bc.BarcodeSize.Width) * (float)(0.15);


			PdfContentByte cb = writer.DirectContent;
			iTextSharp.text.Image patImage = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
			patImage.SetAbsolutePosition(30, 795);
			patImage.ScaleAbsolute(70, 30);
			document.Add(patImage);

			PdfPTable table = new PdfPTable(2);
			table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
			table.SetTotalWidth(new float[] { 200f, 800f });

			PdfPCell cellBlank = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.BOLD)));
			cellBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
			PdfPCell cellDate = new PdfPCell(new Phrase(orderCreateDate + "\n" + TakeInPositionName + "\n" + TakeInPersonPhone, FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellDate.Border = iTextSharp.text.Rectangle.NO_BORDER;

			PdfPCell cellCustomerID = new PdfPCell(new Phrase("\nCustomer ID #" + CustomerCode, FontFactory.GetFont("Arial", 18, Font.BOLD)));
			cellCustomerID.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellCustomerID.Colspan = 2;

			PdfPCell cellNoOfItems = new PdfPCell(new Phrase("Number of items:  " + totalQ + "_____not inspected", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellNoOfItems.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellNoOfItems.Colspan = 2;

			PdfPCell cellTotalWeight = new PdfPCell(new Phrase("Total weight:         " + totalW + "ct.   not inspected", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellTotalWeight.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellTotalWeight.Colspan = 2;

			PdfPCell cellMainMemo = new PdfPCell(new Phrase("Main Memo\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMainMemo.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMainMemo.Colspan = 2;

			PdfPCell cellMemoNumber = new PdfPCell(new Phrase("  Memo Number \n  " + memo + "\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMemoNumber.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMemoNumber.Colspan = 2;

			PdfPCell cellMemoNumbers = new PdfPCell(new Phrase("  " + groupMemoNumbers.Replace(",", "\n  ") + "\n\n\n\n\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellMemoNumbers.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellMemoNumbers.Colspan = 2;


			PdfPCell cellSpecialInstruction = new PdfPCell(new Phrase("Special Instructions: \n" + specialInstr + "\n\n\n\n\n\n", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellSpecialInstruction.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellSpecialInstruction.Colspan = 2;

			PdfPCell cellDue = new PdfPCell(new Phrase("Due:", FontFactory.GetFont("Arial", 10, Font.NORMAL)));
			cellDue.Border = iTextSharp.text.Rectangle.NO_BORDER;
			cellDue.Colspan = 2;

			table.AddCell(cellBlank);
			table.AddCell(cellDate);
			table.AddCell(cellCustomerID);
			table.AddCell(cellNoOfItems);
			table.AddCell(cellTotalWeight);
			table.AddCell(cellMainMemo);
			table.AddCell(cellMemoNumber);
			table.AddCell(cellMemoNumbers);
			table.AddCell(cellSpecialInstruction);
			table.AddCell(cellDue);
			table.WidthPercentage = 95f;
			document.Add(table);

			document.Close();

			this.Response.Clear();
			this.Response.ContentType = "application/pdf";
			this.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Label_Front-{0}.pdf", orderCode));
			this.Response.BinaryWrite(output.ToArray());
			this.Response.End();
		}

		#endregion
	}
}