﻿using System;
using System.Data;

namespace Corpt.Models
{
    [Serializable]
    public class SyntheticShortStatsModel
    {
        public SyntheticShortStatsModel() { }
        public SyntheticShortStatsModel(DataRow row)
        {
            CompanyName = "" + row["CompanyName"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            Memo = "" + row["Memo"];
            Style = "" + row["Style"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            Destination = "" + row["Destination"];
            OfficeID = "" + row["OfficeID"];
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            ReadyDate = row["ReadyDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ReadyDate"]);
            LastModifiedDate = row["LastModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["LastModifiedDate"]);
            PickupDate = row["PickupDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["PickupDate"]);
            Status = "" + row["Status"];
            QtyPass = row["QtyPass"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyPass"]);
            QtySynth = row["QtySynth"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySynth"]);
            QtySusp = row["QtySusp"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySusp"]);
            CustomerCode = row["CustomerCode"] == DBNull.Value ? (int?)null : int.Parse("" + row["CustomerCode"]);
            UserName = "" + row["UserName"];
            Notes = "" + row["Notes"];
        }
        public string CompanyName { get; set; }
        public int? GSIOrder { get; set; }
        public string Memo { get; set; }
        public string Style { get; set; }
        public int? TotalQty { get; set; }
        public string Destination { get; set; }
        public string OfficeID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ReadyDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? PickupDate { get; set; }
        public string Status { get; set; }
        public int? QtyPass { get; set; }
        public int? QtySynth { get; set; }
        public int? QtySusp { get; set; }
        public int? CustomerCode { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
    }
    
    [Serializable]
    public class SyntheticClosedStatsModel
    {
        public SyntheticClosedStatsModel() { }
        public SyntheticClosedStatsModel(DataRow row)
        {
            Memo = "" + row["Memo"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            PickupDate = row["PickupDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["PickupDate"]);
            Status = "" + row["Status"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            CompanyName = "" + row["CompanyName"];
            Destination = "" + row["Destination"];
        }
        
        public string Memo { get; set; }
        public int? GSIOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? PickupDate { get; set; }
        public string Status { get; set; }
        public int? TotalQty { get; set; }
        public string CompanyName { get; set; }
        public string Destination { get; set; }
    }

    [Serializable]
    public class SyntheticBlueNileStatsModel
    {
        public SyntheticBlueNileStatsModel() { }
        public SyntheticBlueNileStatsModel(DataRow row)
        {
            
            
            CreatedDate = DateTime.Today; //row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            CompanyName = "" + row["CompanyName"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            Memo = "" + row["Memo"];
            Sku = "     "; 
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            Destination = "" + row["Destination"];
        }
        public string Memo { get; set; }
        public int? GSIOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Sku { get; set; }
        public int? TotalQty { get; set; }
        public string CompanyName { get; set; }
        public string Destination { get; set; }
    }

    [Serializable]
    public class SyntheticMiscStatsModel
    {
        public SyntheticMiscStatsModel() { }
        public SyntheticMiscStatsModel(DataRow row)
        {
            
            CreatedDate = DateTime.Today; //row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            CompanyName = "" + row["CompanyName"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            Memo = "" + row["Memo"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            Destination = "" + row["Destination"];
        }
        public string Memo { get; set; }
        public int? GSIOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? TotalQty { get; set; }
        public string CompanyName { get; set; }
        public string Destination { get; set; }
    }

    [Serializable]
    public class SyntheticShortStatsQCModel
    {
        public SyntheticShortStatsQCModel() { }
        public SyntheticShortStatsQCModel(DataRow row)
        {
            CompanyName = "" + row["CompanyName"];
            GSIOrder = row["GSIOrder"] == DBNull.Value ? (int?)null : int.Parse("" + row["GSIOrder"]);
            Memo = "" + row["Memo"];
            Style = "" + row["Style"];
            TotalQty = row["TotalQty"] == DBNull.Value ? (int?)null : int.Parse("" + row["TotalQty"]);
            Destination = "" + row["Destination"];
            OfficeID = "" + row["OfficeID"];
            CreatedDate = row["CreatedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["CreatedDate"]);
            ReadyDate = row["ReadyDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["ReadyDate"]);
            LastModifiedDate = row["LastModifiedDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["LastModifiedDate"]);
            PickupDate = row["PickupDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse("" + row["PickupDate"]);
            Status = "" + row["Status"];
            BNCategory = "" + row["BNCATEGORY"];
            BNDestination = "" + row["BNDESTINATION"];
            QtyPass = row["QtyPass"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtyPass"]);
            QtySynth = row["QtySynth"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySynth"]);
            QtySusp = row["QtySusp"] == DBNull.Value ? (int?)null : int.Parse("" + row["QtySusp"]);
            CustomerCode = row["CustomerCode"] == DBNull.Value ? (int?)null : int.Parse("" + row["CustomerCode"]);
            UserName = "" + row["UserName"];
            Notes = "" + row["Notes"];
        }
        public string CompanyName { get; set; }
        public int? GSIOrder { get; set; }
        public string Memo { get; set; }
        public string Style { get; set; }
        public int? TotalQty { get; set; }
        public string Destination { get; set; }
        public string OfficeID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ReadyDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? PickupDate { get; set; }
        public string Status { get; set; }
        public int? QtyPass { get; set; }
        public int? QtySynth { get; set; }
        public int? QtySusp { get; set; }
        public int? CustomerCode { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
        public string BNCategory { get; set; }
        public string BNDestination { get; set; }
    }
}