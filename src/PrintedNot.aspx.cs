using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using Corpt.Utilities;


namespace Corpt
{
	/// <summary>
	/// Summary description for PrintedNot.
	/// </summary>
	public partial class PrintedNot : CommonPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!IsPostBack) txtOrderNumber.Focus();
		}
        private void LoadData()
        {
            var result = QueryUtils.GetPrintedNot(txtOrderNumber.Text, this);
            if (result.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + string.Format("Order number {0} is invalid!", txtOrderNumber.Text) + "\")</SCRIPT>");
            }
            var dt = new DataTable();
            dt.Columns.Add("Batch #");
            dt.Columns.Add("Item #");
            dt.Columns.Add("Report #");
            dt.Columns.Add("Report(s), Status");
            dt.Columns.Add("Report(s) Date");
            dt.AcceptChanges();
            foreach (var rptModel in result)
            {
                dt.Rows.Add(new object[]
                {
                    rptModel.BatchNumber, 
                    rptModel.ItemNumber, 
                    rptModel.OldItemNumber, 
                    rptModel.ReportsDisplay, 
                    rptModel.ReportsDate
                });
            }
            dt.AcceptChanges();
            var c1 = result.FindAll(m => !m.IsFooter).Count;
            var c2 = result.FindAll(m => !m.IsFooter && m.IsPassed).Count;
            lblNumberOfItems.Text = (c1 == 0 ? "" : string.Format("Total number of items in the order: {0}", c1));
            lblNumberOfReports.Text = (c1 == 0 ? "" : string.Format("Total number of documents in the order: {0}", c2));

            grdReports.DataSource = dt;
            grdReports.DataBind();
            grdReports.Visible = dt.Rows.Count > 0;
        }
	    protected void OnLoadClick(object sender, EventArgs e)
	    {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
	        LoadData();    
	    }

	    protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
	    {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                
                foreach (TableCell cell in e.Item.Cells)
                {
                    if (cell.Text.IndexOf("##", StringComparison.Ordinal) != -1)
                    {
                        cell.Font.Bold = true;
                        cell.ForeColor = Color.Black;// Blue;
                    }
                }
            }
        }
	}
}
