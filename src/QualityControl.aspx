﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="QualityControl.aspx.cs" Inherits="Corpt.QualityControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style type="text/css">
        .GridPager a, .GridPager span
        {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }
        
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        
        .GridPager span
        {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }
        
        .divCol
        {
            float: left;
            width: 190px;
        }
        
        .divBottom
        {
            clear: both;
        }
    </style>
    <script type="text/javascript">

        $().ready(function () {

            $("#_ctl0_SampleContent_btnSave").click(function () {
                if ($('#<%=txtGsiOrder.ClientID%>').val().trim() == "") {
                    alert('Please fill out order field.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtGsiOrder.ClientID%>').val().trim().length != 6 && $('#<%=txtGsiOrder.ClientID%>').val().trim().length != 7) {
                    alert('Please enter valid six or seven digit order code.'); $('#<%=txtGsiOrder.ClientID%>').focus(); return false;
                }
                else if ($('#<%=ddlProgram.ClientID%>').val().trim() == "") {
                    alert('Please select program field.'); $('#<%=ddlProgram.ClientID%>').focus(); return false;
                }
                else if ($('#<%=CustomerList.ClientID%>').val().trim() == "") {
                    alert('Please select customer field..'); $('#<%=CustomerList.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtSKU.ClientID%>').val().trim() == "") {
                    alert('Please fill out SKU field..'); $('#<%=txtSKU.ClientID%>').focus(); return false;
                }
                else if ($('#<%=txtQuantity.ClientID%>').val().trim() == "") {
                    alert('Please fill out quantity field..'); $('#<%=txtQuantity.ClientID%>').focus(); return false;
                }                
                else {
                    return true;
                }

            });

            $('#<%=txtGsiOrder.ClientID%>, #<%=gsiOrderFilter.ClientID%>, #<%=CustomerLike.ClientID%>, #<%=txtQuantity.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

            $('#<%=txtCreateDateSearch.ClientID%>').datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker();


        });

        function isNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>
    
	  <div class="demoarea">
        <h2 style="margin-left: 10px;margin-bottom: 0px;margin-top: 0px;padding-bottom: 0px;" class="demoheading">
            Quality Control
        </h2>
        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row" style="border: 1px solid #dddddd; border-radius: 5px; padding-left: 10px;padding-top: 5px;width: 1079px;">
                <div class="form-group" style="margin-bottom: 0px; width: 130px; float: left;">
                    <label class="control-label " for="txtGsiOrder">
                        GSI Order #:</label>
                    <div>
                        <input id="hdnQualityControlID" runat="server" type="hidden" />
                        <input id="txtGsiOrder" runat="server" type="text" class="form-control form-control-height"
                            placeholder="Enter GSI Order #" maxlength="7" style="width: 100px;" />
                    </div>
                </div>

                 <div class="form-group" style="margin-bottom: 0px; display: inline; width: 490px; float: left;">
                    <label class="control-label" for="CustomerLike">
                        Customer:</label>
                    <div>
                        <asp:DropDownList ID="CustomerList" runat="server" DataTextField="CustomerName" CssClass="form-control input-sm"
                            AutoPostBack="True" DataValueField="CustomerId" ToolTip="Customers List" Width="300px"
                            OnSelectedIndexChanged="OnCustomerSelectedChanged" />
                        <asp:TextBox runat="server" ID="CustomerLike" Width="130px" CssClass="form-control input-sm"
                            Style="display: inline-block;"></asp:TextBox>
                        <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                            ImageUrl="~/Images/ajaxImages/search.png" Style="margin-left: 0px; display: inline-block;
                            vertical-align: middle;" OnClick="OnCustomerSearchClick" />
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 0px; display: inline;">
                    <label class="control-label" for="txtSKU">
                        SKU:</label>
                    <div>
                        <asp:DropDownList ID="CpList" runat="server" AutoPostBack="True" DataTextField="CustomerProgramName"
                            CssClass="form-control input-sm" DataValueField="CpId" ToolTip="Customer Program List"
                            Width="300px" OnSelectedIndexChanged="OnCpSelectedChanged" />
                        <input id="txtSKU" runat="server" type="text" class="form-control form-control-height"
                            maxlength="100" />
                    </div>
                </div>
                             

                <div style="clear: both;">
                </div>

                 <div class="form-group" style="margin-bottom: 0px; display: inline; float: left;">
                    <label class="control-label" for="ddlProgram">
                        Program:</label>
                    <div>
                        <asp:DropDownList ID="ddlProgram" runat="server" DataTextField="ProgramName" DataValueField="ProgramID"
                          CssClass="form-control input-sm" ToolTip="Program List" Width="300px" />
                    </div>
                </div>

                <div class="form-group" style="margin-left: 15px; margin-bottom: 0px; width: 120px; float: left;">
                    <label class="control-label" for="lblQuantity">
                        Quantity:</label>
                    <div>
                        <input id="txtQuantity" runat="server" type="text" class="form-control form-control-height"
                            placeholder="Enter Total QTY" maxlength="5" style="width: 90px;" />
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 0px; float: left;width: 330px;">
                    <label class="control-label" for="txtNotes">
                        Comments:</label>
                    <div style="float: left; margin-right: 25px;">
                        <textarea id="txtNotes" runat="server" class="form-control form-control-height" rows="1"
                            placeholder="Enter Comments" maxlength="1000" style="resize: none; width: 300px;">
                            </textarea>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 0px; float: left;">
                    <label class="control-label" for="chkIsQCDone" style="float: left;">
                        Is QC Done:</label>
                    <div>
                        <input id="chkIsQCDone" runat="server" type="checkbox" class="form-control form-control-height"
                            style="width: 17px;margin-bottom: 10px;height: 18px;" />
                    </div>
                </div>

                <div id="dvIsDisable" runat="server" class="form-group" style="margin-left: 30px; margin-bottom: 0px; float: left;">
                    <label class="control-label" for="chkIsDisable" style="float: left;">
                        Is Disable:</label>
                    <div>
                        <input id="chkIsDisable" runat="server" type="checkbox" class="form-control form-control-height"
                            style="width: 17px;margin-bottom: 10px;height: 18px;" />
                    </div>
                </div>

                <div id="dvSave" runat="server" class="form-group" style="margin-left: 89px; float: left; padding-top: 26px; float: left;">
                    <div style="text-align: center;">
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Add New" style="width:65px; padding-left:7px;"
                            OnClick="btnSave_Click"></asp:Button>
                        
                    </div>
                </div>

                <div style="clear: both;">
                </div>
            </div>
            <div class="row">
                <div class="form-grouph " style="margin-bottom: 7px; float: left;">
                    <label class="text-info" for="gsiOrderFilter" style="padding-top: 14px;">
                        Filter By GSI Order# / Create Date:</label>
                    <div style="padding-bottom: 5px; display: block;">
                        <input id="gsiOrderFilter" runat="server" style="width: 100px; display: inline" type="text"
                            class="form-control form-control-height" placeholder="Enter GSI Order#" maxlength="7" />
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                            Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnSearch_Click">
                        </asp:Button>
                        <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px;">or</span>
                        <input id="txtCreateDateSearch" runat="server" style="width: 100px; display: inline;"
                            type="text" class="form-control form-control-height" placeholder="Enter Created Date"
                            readonly />
                        <asp:Button ID="btnCreateDateSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                            Style="height: 25px; margin-bottom: 12px; padding-top: 2px;" OnClick="btnCreateDateSearch_Click">
                        </asp:Button>

                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary" Text="ReSet"  Style="height: 25px; margin-bottom: 12px; padding-top: 2px; margin-left: 25px; "
                            OnClick="btnClear_ServerClick"></asp:Button>
                    </div>
                    <div class="form-group" style="margin-bottom: 7px;">
                        <div id="mainContainer" class="container" style="padding-left: 0px;">
                            <div class="shadowBox">
                                <div class="page-container">
                                    <div class="container" style="padding-left: 0px;">
                                        <div style="padding-bottom: 5px;">
                                            <span id="gvTitle" runat="server" class="text-info"></span>
                                              <span class="vertical-divider" style="padding-left: 10px; padding-right: 10px; float: right;">
                                            </span>
                                            <asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                                                ImageAlign="AbsMiddle" Style="margin-bottom: 10px; margin-left: 10px; margin-right: 10px;" OnClick="btnExportToExcel_Click" />
                                        </div>
                                        <div>
                                            <div class="table-responsive">
                                                <asp:GridView ID="grdQualityControl" runat="server" Width="1400px" CssClass="table striped table-bordered "
                                                    AutoGenerateColumns="False" DataKeyNames="QCID" EmptyDataText="There are no data records to display."
                                                    AllowPaging="true" RowStyle-Height="2px" OnPageIndexChanging="grdQualityControl_PageIndexChanging"
                                                    OnSelectedIndexChanged="grdQualityControl_SelectedIndexChanged" Height="200px">
                                                    <HeaderStyle BackColor="#5377A9" Font-Names="Cambria" ForeColor="White" Font-Size="12.5px"
                                                        Height="25px" />
                                                    <RowStyle Font-Size="12px" />
                                                    <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                    <SelectedRowStyle BackColor="LightSkyBlue" Font-Bold="True" ForeColor="Black" />
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectText="Select" HeaderStyle-Width="30px" />
                                                        <asp:BoundField DataField="QCID" HeaderText="Sr.No." Visible="true" ReadOnly="True"
                                                            HeaderStyle-Width="30px" SortExpression="QCID" ItemStyle-CssClass="visible-lg"
                                                            HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HeaderText="Created Date"
                                                            HeaderStyle-Width="110px" ItemStyle-Wrap="false" SortExpression="CreatedDate"
                                                            ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="OrderCode" HeaderText="Order #" HeaderStyle-Width="70px"
                                                            SortExpression="GSIOrder" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="CreatedBy" HeaderText="Created By" HeaderStyle-Width="120px"
                                                            SortExpression="CreatedBy" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="CustomerName" HeaderText="Customer" HeaderStyle-Width="350px"
                                                            ItemStyle-Wrap="true" SortExpression="CustomerName" HeaderStyle-CssClass="visible-lg"
                                                            ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="CustomerProgram" HeaderText="SKU" HeaderStyle-Width="250px"
                                                            ItemStyle-Wrap="true" SortExpression="CustomerProgram" HeaderStyle-CssClass="visible-lg"
                                                            ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="ProgramName" HeaderText="Program" HeaderStyle-Width="550px"
                                                            SortExpression="ProgramName" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-Width="30px"
                                                            SortExpression="Quantity" ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="ModifiedDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HeaderText="Modified Date"
                                                            HeaderStyle-Width="130px" ItemStyle-Wrap="false" SortExpression="ModifiedDate"
                                                            ItemStyle-CssClass="visible-lg" HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By" HeaderStyle-Width="120px"
                                                            SortExpression="ModifiedBy" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="QCDoneOn" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HeaderText="QCDone On"
                                                            HeaderStyle-Width="100px" ItemStyle-Wrap="false" SortExpression="QCDoneOn" ItemStyle-CssClass="visible-lg"
                                                            HeaderStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="QCDoneBy" HeaderText="QCDone By" HeaderStyle-Width="120px"
                                                            SortExpression="QCDoneBy" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                        <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="Comments"
                                                            HeaderStyle-Width="250" HeaderStyle-CssClass="hidden-lg" ItemStyle-CssClass="hidden-lg" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
