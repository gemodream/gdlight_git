﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ReportQuantities.aspx.cs" Inherits="Corpt.ReportQuantities" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" AsyncPostBackTimeout="360000">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Reports Quantity</div>
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="padding-left: 20px">
                            <asp:RadioButtonList ID="periodType" runat="server" CssClass="radio" OnSelectedIndexChanged="OnChangedPeriodType"
                                RepeatDirection="Horizontal" Width="250px" AutoPostBack="true">
                                <asp:ListItem Value="m">Monthly</asp:ListItem>
                                <asp:ListItem Value="w" Selected="True">Weekly</asp:ListItem>
                                <asp:ListItem Value="r">Random</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="padding-left: 50px">
                            Customer Like
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                <label>
                                    From</label>
                                <asp:TextBox runat="server" ID="calFrom" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                                <!-- Date To -->
                                <label style="padding-left: 20px;">
                                    To</label>
                                <asp:TextBox runat="server" ID="calTo" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                            </asp:Panel>
                        </td>
                        <td style="padding-left: 50px">
                            <asp:Panel ID="Panel1" runat="server" CssClass="form-inline">
                                <!-- Date From -->
                                <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="SearchPanelByCustomerCp"
                                    CssClass="form_inline">
                                    <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                                        width: 80px"></asp:TextBox>
                                    <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                        ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                                    <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                        DataValueField="CustomerId" AutoPostBack="True" Width="350px" Style="font-family: Arial;
                                        font-size: 12px" />
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                        <td style="padding-left: 50px">
                            <asp:DropDownList ID="DocumentTypeList" runat="server" DataTextField="DocumentTypeName"
                                DataValueField="DocumentTypeCode" AutoPostBack="True" Width="250px" Style="font-family: Arial;
                                font-size: 12px" Visible="false"/>

                        </td>
                    </tr>
                </table>
                <br />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                    PopupButtonID="Image1" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                    PopupButtonID="Image2" />
        <table>
            <tr>
                <td style="padding-left: 40px;vertical-align: top">
                    <asp:Label ID="Label1" runat="server" class="label" Text="Reports By Document Type"></asp:Label>
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Report quantities by Document type"
                        ImageUrl="~/Images/ajaxImages/search.png" 
                        OnClick="OnLookupByDocumentTypeClick" />
                </td>
                <td style="padding-left: 40px;vertical-align: top">
                    <asp:Label ID="Label2" runat="server" class="label" Text="Reports By Customer"></asp:Label>
                    <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Report quantities by Customer"
                        ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnLookupByCustomerClick" />
                </td>
            </tr>
            <tr>
                <td style="padding-left: 40px;vertical-align: top">
                    <asp:Label ID="ByDocumentTotal" runat="server"></asp:Label>
                </td>
                <td style="padding-left: 40px;vertical-align: top">
                    <asp:Label ID="ByCustomerTotal" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 40px;font-size: small; text-align: center;vertical-align: top">
                    <asp:GridView ID="GridDocuments" runat="server" AutoGenerateColumns="false" class="table-bordered"
                        CellPadding="5">
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Document Type" DataField="DocumentTypeName" ItemStyle-HorizontalAlign="Left"/>
                            <asp:BoundField HeaderText="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right"/>
                        </Columns>
                    </asp:GridView>
                </td>
                <td style="font-size: small; text-align: center; vertical-align: top; padding-left: 40px">
                    <asp:GridView ID="GridCustomers" runat="server" AutoGenerateColumns="false" class="table-bordered"
                        CellPadding="5">
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Customer" DataField="CustomerName" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField HeaderText="Quantity by Document Type" DataField="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                        </Columns>
                    </asp:GridView>
                </td>

            </tr>
        </table>

            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
</asp:Content>
