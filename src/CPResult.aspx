﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CPResult.aspx.cs" Inherits="Corpt.CPResult" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
    <div class="demoheading">CP Rules</div>
        <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" defaultbutton="LookupButton">
            <asp:Label ID="Label1" Text="Batch Number:" CssClass="control-label" runat="server"/>
            <asp:TextBox type="text" ID="BatchId" MaxLength="9" runat="server" name="BatchId"
                style="font-weight: bold; width: 100px;" placeholder="Batch Number" OnTextChanged="OnBatchNumberChanged" />
            <!-- 'Lookup' button -->
            <asp:Button ID="LookupButton" runat="server" type="submit" class="btn btn-info" OnClick="OnSubmitClick"
                Text="Lookup" postbackurl="~/CPResult.aspx"></asp:Button>
            <!-- 'Full Details' button-->
            <asp:Button runat="server" CssClass="btn btn-info" ID="cmdDetail" Text="Full Details"
                Visible="False" OnClick="OnDetailAllClick" usesubmitbehavior="False" postbackurl="~/CPResult.aspx">
            </asp:Button>
            <asp:Button runat="server" CssClass="btn btn-info" ID="btnOpenCP" Text="Open CP"
                Visible="false" OnClick="OnOpenCPClick" OnClientClick="target ='_blank';" />
            <!-- 'Show Report' link -->
            <asp:HyperLink ID="lnkShortReport" runat="server" CssClass="btn btn-link" Visible="False"
                style="margin-left: 10px" align="right" Font-Underline="True">Short Report</asp:HyperLink>
        </asp:Panel>
        <!-- Error message-->
        <div class="control-group error">
            <asp:Label class="control-label" ID="WrongInfoLabel" runat="server" />
        </div>
    
        <!-- Hidden Field -->
        <input value="NewBatch" type="hidden" name="Flag" />
    
    
        <!-- Item Numbers DataGrid-->
        <table>
            <tr>
                <td style="vertical-align: top">
                    <div style="font-size: small; margin-top: 20px;">
                        <asp:DataGrid ID="cpRulesTrackingResult" runat="server" style="font-size: small;"
                            CellPadding="5" AutoGenerateColumns="False" OnItemDataBound="OnDataItemBound">
                            <columns>
                                <asp:BoundColumn DataField="Item Number" HeaderText="Item Number"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Original Item Number" HeaderText="Original Item Number">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Rule Number" HeaderText="Rule Number"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
                            </columns>
                            <headerstyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        </asp:DataGrid>
                    </div>
                </td>
                <td style="vertical-align: top;padding-left: 30px">
                    <!-- Item Details DataGrid -->
                    <div style="font-size: small; margin-top: 20px;">
                        <asp:DataGrid ID="CpResultDetail" runat="server" style="font-size: small;" CellPadding="5"
                            GridLines="Both" AutoGenerateColumns="true" OnItemDataBound="OnDetailDataItemBound">
                            <headerstyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</div>
    <asp:RequiredFieldValidator runat="server" ID="BReq" ControlToValidate="BatchId"
        Display="None" 
        ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." 
        validationgroup="BatchGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BReqE" TargetControlID="BReq"
        HighlightCssClass="validatorCalloutHighlight" />
</asp:Content>
