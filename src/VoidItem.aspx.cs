using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for VoidItem.
	/// </summary>
	public partial class VoidItem : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			SqlCommand stageCommand = new SqlCommand();
			SqlConnection connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			stageCommand.Connection=connection;
			stageCommand.CommandText="spSetItemStateByCode";
			stageCommand.CommandType=CommandType.StoredProcedure;

			stageCommand.Parameters.Add(new SqlParameter("@rId",SqlDbType.VarChar,255));
			stageCommand.Parameters["@rId"].Direction=ParameterDirection.Output;
			
			stageCommand.Parameters.Add(new SqlParameter("@StateCode",3));

			stageCommand.Parameters.Add(new SqlParameter("@GroupCode",txtGroupCode.Text.Trim()));
			stageCommand.Parameters.Add(new SqlParameter("@BatchCode",txtBatchCode.Text.Trim()));
			stageCommand.Parameters.Add(new SqlParameter("@ItemCode",txtItemCode.Text.Trim()));

			stageCommand.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));
			stageCommand.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
			stageCommand.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

			try
			{
				connection.Open();
				stageCommand.ExecuteNonQuery();
				connection.Close();
			}
			catch(Exception ex)
			{
				lblInfo.Text=ex.Message;
				return;
			}

			string message="";
			message="@GroupCode: "+stageCommand.Parameters["@GroupCode"].Value.ToString();
			message+="; @BatchCode: "+stageCommand.Parameters["@BatchCode"].Value.ToString();
			message+="; @ItemCode: "+stageCommand.Parameters["@ItemCode"].Value.ToString();
			message+="; @rID: "+stageCommand.Parameters["@rId"].Value.ToString();
			lblInfo.Text=message;
		}
	}
}
