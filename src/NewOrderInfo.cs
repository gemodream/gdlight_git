using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Collections;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class NewOrderInfo
	{
		public NewOrderInfo()
		{
			// 
			// TODO: Add constructor logic here
			//
			CustomerID = -1;
			CustomerOfficeID=-1;
			MemoNumbers=new DataTable();
			NumberOfItems = -1;
			SpecialInstructions="";
		}
		public NewOrderInfo(int customerID, int customerOfficeID)
		{
			CustomerID=customerID;
			CustomerOfficeID=customerOfficeID;
		}
		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers) : this(customerID, customerOfficeID)
		{
			MemoNumbers=memoNumbers;
		}
		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers, int numberOfItems) : this(customerID, customerOfficeID, memoNumbers)
		{
			NumberOfItems = numberOfItems;
		}

		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers, int numberOfItems, String specialInstructions) : this(customerID, customerOfficeID, memoNumbers, numberOfItems)
		{
			SpecialInstructions = specialInstructions;
		}
		
		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers, int numberOfItems, String specialInstructions, String customerName) : this(customerID, customerOfficeID, memoNumbers, numberOfItems, specialInstructions)
		{
			CustomerName = customerName;
		}																			  
		
		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers, int numberOfItems, String specialInstructions, String customerName, float totalWeight) : this(customerID, customerOfficeID, memoNumbers, numberOfItems, specialInstructions, customerName)
		{
			TotalWeight = totalWeight;
		}

		public NewOrderInfo(int customerID, int customerOfficeID, DataTable memoNumbers, int numberOfItems, String specialInstructions, String customerName, float totalWeight, String orderMemo) : this(customerID, customerOfficeID, memoNumbers, numberOfItems, specialInstructions, customerName, totalWeight)
		{
			OrderMemo = orderMemo;
		}

		public int CustomerID;
		public int CustomerOfficeID;
		public float TotalWeight;
		public String CustomerName;
		public DataTable MemoNumbers;
		public int NumberOfItems;
		public String SpecialInstructions;
		public String OrderMemo;
	}
}
