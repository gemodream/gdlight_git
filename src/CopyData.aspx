<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Page language="c#" Codebehind="CopyData.aspx.cs" AutoEventWireup="True" Inherits="Corpt.CopyData" aspCompat="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CopyData</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iewc:treeview style="Z-INDEX: 102; POSITION: absolute; TOP: 144px; LEFT: 104px" id="sourceTree"
				runat="server" Width="225px" Height="264px" AutoPostBack="True"></iewc:treeview><asp:label style="Z-INDEX: 115; POSITION: absolute; TOP: 88px; LEFT: 552px" id="lblDestCustomer"
				runat="server" Width="352px" CssClass="text"></asp:label><asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 120px; LEFT: 560px" id="lblDest" runat="server"
				CssClass="text" Font-Bold="True"></asp:label>
			<asp:button style="Z-INDEX: 108; POSITION: absolute; TOP: 0px; LEFT: 0px" id="cmdHome" runat="server"
				Width="16px" Height="16px" CssClass="buttonStyle" Text="H" onclick="cmdHome_Click"></asp:button>
			<asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 16px; LEFT: 568px" id="Label2" runat="server"
				CssClass="text">Target Item</asp:label>
			<asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 248px; LEFT: 424px" id="cmdCopy" runat="server"
				CssClass="buttonStyle" Text="COPY" onclick="cmdCopy_Click"></asp:button>
			<iewc:treeview style="Z-INDEX: 103; POSITION: absolute; TOP: 144px; LEFT: 560px" id="destTree"
				runat="server" Width="248px" Height="264px" AutoPostBack="True"></iewc:treeview><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 216px; LEFT: 416px" id="lblCopy" runat="server"
				CssClass="text"></asp:label><asp:panel style="Z-INDEX: 113; POSITION: absolute; TOP: 472px; LEFT: 280px" id="Panel1" runat="server"
				Width="240px" Height="100px" CssClass="text" BackColor="#E0E0E0" Visible="False" BorderColor="Gray" BorderStyle="Solid"
				BorderWidth="2px">
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
				<P align="center">&nbsp;</P>
			</asp:panel><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 16px; LEFT: 120px" id="Label1" runat="server"
				CssClass="text">Source Item</asp:label><asp:image style="Z-INDEX: 101; POSITION: absolute; TOP: 144px; LEFT: 432px" id="Image1" runat="server"
				ImageUrl="Images/left2right.jpg"></asp:image><asp:label style="Z-INDEX: 109; POSITION: absolute; TOP: 120px; LEFT: 120px" id="lblSource"
				runat="server" CssClass="text" Font-Bold="True"></asp:label><asp:datagrid style="Z-INDEX: 111; POSITION: absolute; TOP: 376px; LEFT: 120px" id="sourceDataGrid"
				runat="server" CssClass="text">
				<HeaderStyle Font-Bold="True" BackColor="#E0E0E0"></HeaderStyle>
			</asp:datagrid><asp:datagrid style="Z-INDEX: 112; POSITION: absolute; TOP: 376px; LEFT: 560px" id="DestDataGrid"
				runat="server" CssClass="text">
				<HeaderStyle Font-Bold="True" BackColor="#E0E0E0"></HeaderStyle>
			</asp:datagrid><asp:label style="Z-INDEX: 114; POSITION: absolute; TOP: 80px; LEFT: 120px" id="lblSourceCustomer"
				runat="server" Width="352px" CssClass="text"></asp:label><asp:label style="Z-INDEX: 116; POSITION: absolute; TOP: 96px; LEFT: 328px" id="lblMessage"
				runat="server" CssClass="text" Font-Bold="True" Font-Size="Smaller"></asp:label><asp:textbox style="Z-INDEX: 117; POSITION: absolute; TOP: 32px; LEFT: 120px" id="txtItemNumber"
				runat="server" Width="160px" Height="24px" CssClass="inputStyleCC" ontextchanged="txtItemNumber_TextChanged"></asp:textbox>
			<asp:button style="Z-INDEX: 118; POSITION: absolute; TOP: 40px; LEFT: 304px" id="cmdLoad" tabIndex="10000"
				runat="server" CssClass="buttonStyle" Text="Load" onclick="cmdLoad_Click"></asp:button>
			<asp:textbox style="Z-INDEX: 119; POSITION: absolute; TOP: 32px; LEFT: 568px" id="txtDest" runat="server"
				Width="160px" Height="24px" CssClass="inputStyleCC" ontextchanged="txtDest_TextChanged"></asp:textbox>
			<asp:button style="Z-INDEX: 120; POSITION: absolute; TOP: 40px; LEFT: 752px" id="cmdLoadDest"
				tabIndex="1000" runat="server" CssClass="buttonStyle" Text="Dest" onclick="cmdLoadDest_Click"></asp:button>
			<asp:label style="Z-INDEX: 121; POSITION: absolute; TOP: 352px; LEFT: 120px" id="lblSourcePartName"
				runat="server" Width="168px" Height="24px" CssClass="text" Font-Bold="True"></asp:label><asp:label style="Z-INDEX: 122; POSITION: absolute; TOP: 352px; LEFT: 568px" id="lblDestPartName"
				runat="server" Width="160px" Height="16px" CssClass="text" Font-Bold="True"></asp:label>
			<P align="center">
				<asp:Button style="Z-INDEX: 123; POSITION: absolute; TOP: 40px; LEFT: 0px" id="cmdOK" runat="server"
					CssClass="buttonStyle" Text="Back" Width="56px" onclick="cmdOK_Click"></asp:Button></P>
		</form>
	</body>
</HTML>
