using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using Corpt.TreeModel;
using System.IO;
//IvanB 14/03
using System.Linq;
//IvanB 14/03

namespace Corpt
{
	/// <summary>
	/// Summary description for GradeII.
	/// </summary>
    public partial class Grade2 : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Grade II";

			OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
			OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

			if (!IsPostBack)
            {
                string measureIds = QueryUtils.GetGradeMeasureIds(this);
                Session["MeasureIds"] = measureIds;
                CleanUp();
                SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
                txtBatchNumber.Focus();
            }

		}
        #endregion

        #region CleanUp (on PageLoad, on Load button click)
        private void CleanUp()
        {
            CleanItemListArea();
            CleanPartTreeArea();
            HidePicturePanel();
            HideShapePanel();
            CleanValueGridArea();
            CleanViewState();

            cmdSave.Visible = false;
            InvalidLabel.Text = "";
        }
        private void CleanItemListArea()
        {
            lstItemList.Items.Clear();
            ItemEditing.Value = "";
            lstItemList.Visible = false;
        }
        private void CleanPartTreeArea() 
        {
            PartTree.Nodes.Clear();
            PartEditing.Value = "";
        }
        private void CleanValueGridArea()
        {
            ItemValuesGrid.DataSource = null;
            ItemValuesGrid.DataBind();
            MeasurementPanel.Visible = false;
            DataForLabel.Visible = false;
			dvDescWithComment.Visible = false;
		}
        private void CleanViewState()
        {
            //-- Item numbers
            SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);

            //-- Measure Parts
            SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);
            
            //-- Measure Descriptions
            SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

            //-- Item Measure Values
            SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);
        }
        #endregion

        #region ItemNumber List 
        private void OnItemListSelected()
        {
            var itemNumber = lstItemList.SelectedValue;
            var singleItemModel = GetItemModelFromView(itemNumber);
            if (singleItemModel == null) return;
            if (singleItemModel.StateId == DbConstants.ItemInvalidStateId)
            {
                InvalidLabel.Text = string.Format("Item {0} is invalid", singleItemModel.FullItemNumber);
            }
            else
            {
                InvalidLabel.Text = "";
            }
            ShowPicture(singleItemModel.Path2Picture);
            LoadMeasuresForEdit(itemNumber);
            PartEditing.Value = "";
            PartTree.Nodes[0].Selected = true;
            OnPartsTreeChanged(null, null);
            ItemEditing.Value = itemNumber;
        }
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
		{
            if (HasUnSavedChanges())
            {
                PopupSaveQDialog(ModeOnItemChanges);
                return;
            }
            OnItemListSelected();
		}
        private bool HasUnSavedChanges()
        {
            GetNewValuesFromGrid();
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            return itemValues.Count > 0;
        }
        #endregion

        #region Load Button
        int ListMaxRow = 40;
        int ListMinRow = 10;
        private void LoadExecute()
        {
            CleanUp();
            if (txtBatchNumber.Text.Trim().Length == 0)
            {
                InvalidLabel.Text = "A Batch Number is required!";
                return;
            }
            //-- Get ItemNumbers by BatchNumber
            var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
            if (itemList.Count == 0)
            {
                InvalidLabel.Text = "Items not found";
                return;
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);

			//-- Refresh batchNumber
			var tempTxtBatchNumber = txtBatchNumber.Text;//Sergey
            var itemNumber = itemList[0];
            if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
            {
                txtBatchNumber.Text = itemNumber.FullBatchNumber;
            }

            //-- Loading ItemList control
            lstItemList.Items.Clear();
            foreach (var singleItemModel in itemList)
            {
                lstItemList.Items.Add(singleItemModel.FullItemNumber);
            }
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                lstItemList.Rows = itemList.Count + 1;
            }
            lstItemList.Visible = true;

            //-- Loading Parts by first ItemNumber and show on PartTree Control
            var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
            SetViewState(parts, SessionConstants.GradeMeasureParts);
            LoadPartsTree();

            //-- Loading Measures by Cp or ItemTypeId
            if (IgnoreCpFlag.Checked)
            {
                var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }
            else
            {
                var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }

			//Sergey: Selects the exact item on load, rather than the first
			int index = 0;
			foreach(var item in itemList)
			{
				if(item.FullItemNumber == tempTxtBatchNumber)
				{
					lstItemList.SelectedIndex = index;
					break;
				}
				index++;
			}

			//lstItemList.SelectedIndex = 0;
            OnItemListSelectedChanged(null, null);

        }
        protected void OnLoadClick(object sender, EventArgs e)
		{
            if (HasUnSavedChanges())
            {
                PopupSaveQDialog(ModeOnLoadClick);
                return;
            }
            LoadExecute();
		}

        #endregion

        #region Shape Picture
        private string GetShapePicturePath(MeasureValueCpModel measureCpModel, int strPartId, String strMeasureId)
		{
			string itemNumber = lstItemList.SelectedValue;

            var measureValuesList = GetMeasureValuesFromView();
            if (measureValuesList == null) return "";
			var drPartValue = measureValuesList.FindAll(m => m.PartId == strPartId && m.MeasureId == strMeasureId);
			if (drPartValue.Count > 0)
			{
                var pathToShape = drPartValue[0].ShapePath2DrawingGif;
				return @"ShapeView.aspx?itemNumber=" + itemNumber + ":"+measureCpModel.PartName + @"&shapePath=Images\"+pathToShape;
			}
			return @"ShapeView.aspx?itemNumber=" + itemNumber + @"&shapePath=Images\Shapes\Test\NotShape.gif";

		}
        #endregion

        #region Get Data From View
        
        //-- Measure Values
        private List<MeasureValueModel> GetMeasureValuesFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
        }

        //-- Measure Enums
        private List<EnumMeasureModel> GetMeasureEnumsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
        }
        
        //-- Loading Measures Description
        private List<MeasureValueCpModel> GetMeasuresCpFromView() 
        {
            return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
        }
        
        //-- Loading Item Numbers
        private List<SingleItemModel> GetItemNumbersFromView()
        {
            
            return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
        }
        private SingleItemModel GetItemModelFromView(string itemNumber)
        {
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber);
        }
        private SingleItemModel GetSelectedItemModel()
        {
            var itemNumber = lstItemList.SelectedValue;
            if (string.IsNullOrEmpty(itemNumber)) return null;
            return GetItemModelFromView(itemNumber);
        }
        private List<MeasurePartModel> GetPartsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
        }
        private List<ItemValueEditModel> GetMeasuresForEditFromView(string partId)
        {
            var data = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ?? new List<ItemValueEditModel>();
            if (string.IsNullOrEmpty(partId)) return data;
            else return data.FindAll(m => m.PartId == partId);
        }
        #endregion

        #region Save Button
        private string SaveExecute() 
        {
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            var result = new List<MeasureValueModel>();
            bool valueIn = false;
            foreach(var itemValue in itemValues) 
            {
                result.Add(new MeasureValueModel(itemValue, itemModel));
                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }
            }
            var errMsg = QueryUtils.SaveNewMeasures(result, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
            {
                errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            }

            /*
            if (!CheckForSarinDiscrepancy(result))
            {
                //Sarin weight did not match. Mark the item in the db
                InvalidLabel.Text = string.Format( "#{0}, Sarin weight did not match!", ItemEditing.Value);
                itemModel.StateId = DbConstants.ItemInvalidStateId;
                QueryUtils.MarkItemInvalid(itemModel, this);
            }
             */ 
            return "";

        }
        protected void OnSaveClick(object sender, EventArgs e)
		{
			/*NimeshP start */
			bool hasChange = false;
			var itemModel = GetItemModelFromView(ItemEditing.Value);
			string strExtDesc = txtExtDesc.Text.Trim();
			string strhdnExtDesc = hdnExtDesc.Value.Trim();
			string strExtComment = txtExtComment.Text.Trim();
			string strhdnExtComment = hdnExtComment.Value.Trim();
			lblExtDescLimit.Visible = false;
			lblExtCommentLimit.Visible = false;
			if (strExtDesc.Length > 2000)
			{
				lblExtDescLimit.Visible = true;
				return;
			}
			if (strExtComment.Length > 2000)
			{
				lblExtCommentLimit.Visible = true;
				return;
			}
			//var errMsg = "";
			if (strExtDesc != strhdnExtDesc || strExtComment != strhdnExtComment)
			{
				hasChange = true;
				var errMsgnew = QueryUtils.SetExtendedDescription(strExtDesc, strExtComment, int.Parse(PartTree.SelectedValue), itemModel.BatchId, int.Parse(itemModel.ItemCode), this); // Desc
				if (!string.IsNullOrEmpty(errMsgnew))
				{
					PopupInfoDialog(errMsgnew, true);
					return;
				}
				else
				{
					txtExtDesc.Text = "";
					hdnExtDesc.Value = "";
					txtExtComment.Text = "";
					hdnExtComment.Value = "";
				}
			}

			//var errMsg = "";
		/*	if (strExtComment != strhdnExtComment)
			{
				hasChange = true;
				var errMsgnew = QueryUtils.SetExtendedDescription(strExtComment, int.Parse(PartTree.SelectedValue), itemModel.BatchId, int.Parse(itemModel.ItemCode), 9, this); // Comment
				if (!string.IsNullOrEmpty(errMsgnew))
				{
					PopupInfoDialog(errMsgnew, true);
					return;
				}
				else
				{
					txtExtComment.Text = "";
					hdnExtComment.Value = "";
				}
			}*/
			//-- no changes
			if (!HasUnSavedChanges() && hasChange == false)
			{
				PopupInfoDialog("No changes!", false);
				return;
			}
			/*NimeshP end 
			//-- no changes
			/*if (!HasUnSavedChanges())
            {
                PopupInfoDialog("No changes!", false);
                return;
            }*/
			var errMsg = SaveExecute();
            if (string.IsNullOrEmpty(errMsg))
            {
                //-- Reload Item Values
                OnItemListSelected();
                PopupInfoDialog("Changes were updated successfully.", false);
                
            }
            else
            {
                PopupInfoDialog(errMsg, true);
            }
            return;
            
        }
        
        private bool CheckNumericData(ArrayList controls)
        {
            var errs = 0;
            foreach (WebControl wc in controls)
            {
                if (wc.GetType() != typeof(Panel)) continue;
                foreach (WebControl iwc in wc.Controls)
                {
                    var myControlUniqueId = iwc.ID;
                    if (myControlUniqueId != null && myControlUniqueId.StartsWith("Measure"))
                    {
                        var measureId = myControlUniqueId.Split('.')[2];
                        var partId = Convert.ToInt32(myControlUniqueId.Split('.')[1]);
                        var cpMeasures = GetMeasuresCpFromView();
                        if (cpMeasures == null) continue;
                        var cpMeasure = cpMeasures.FindAll(m => m.MeasureId == measureId && m.PartId == partId);
                        foreach (MeasureValueCpModel cp in cpMeasure)
                        {
                            var measureClass = cp.MeasureClass;
                            if (measureClass != 3) continue;
                            var textBox = iwc as TextBox;
                            if (textBox.Text.Trim() == "") continue;
                            try
                            {
                                var val = float.Parse(textBox.Text.Trim());
                            }
                            catch (Exception x)
                            {
                                textBox.BorderStyle = BorderStyle.Solid;
                                textBox.ToolTip = x.Message;
                                textBox.BorderColor = Color.Red;
                                errs++;
                            }
                        }
                    }
                }
            }
            return errs == 0;
        }


        #endregion

        #region Sarin Discrepancy
        private bool CheckForSarinDiscrepancy(List<MeasureValueModel> newValueList)
        {
            var fullList = GetMeasureValuesFromView();
            if (fullList == null || newValueList.Count == 0) return true;

            var sarinMeasures = fullList.FindAll(m => m.MeasureId == "6");
            if (sarinMeasures.Count == 0) return true;

            Console.Write("Have SARIN Will Travel.");
            foreach (var sarinModel in sarinMeasures)
            {
                var measure4List = newValueList.FindAll(m => m.PartId == sarinModel.PartId && m.MeasureId == "4");
                var sw = (!string.IsNullOrEmpty(sarinModel.MeasureValue) ? double.Parse(sarinModel.MeasureValue) : double.Parse("0"));
                foreach (var valueModel in measure4List)
                {
                    var mw = (!string.IsNullOrEmpty(valueModel.MeasureValue) ? double.Parse(valueModel.MeasureValue) : double.Parse("0"));
                    if (Math.Abs(mw - sw) > 0.011)
                    {
                        return false;
                    }
                }
            }

            return true;

        }
        #endregion

        #region Parts Tree
        protected void OnPartsTreeChanged(object sender, EventArgs e)
        {
            if (GetSelectedItemModel() == null) return;
            var currPart = PartTree.SelectedValue;
            var prevPart = PartEditing.Value;
            //-- Get Changes
            if (!string.IsNullOrEmpty(prevPart) && currPart != prevPart)
            {
                GetNewValuesFromGrid();
            }

            LoadDataForEditing();
            PartEditing.Value = currPart;

			/*NimeshP Start*/
			if (PartTree.SelectedNode.Text.Trim() == "Item Container")
			{
				txtExtComment.Visible = true;
				txtExtDesc.Visible = true;
				lblExtDesc.Visible = true;
				lblExtComment.Visible = true;
			}
			else
			{
				txtExtComment.Visible = false;
				txtExtDesc.Visible = false;
				lblExtDesc.Visible = false;
				lblExtComment.Visible = false;
			}
			/*NimeshP end*/
		}
		private void LoadPartsTree()
        {
            PartTree.Nodes.Clear();
            var parts = GetPartsFromView();
            if (parts.Count == 0)
            {
                return;
            }
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();
           
            PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Select();
            
            OnPartsTreeChanged(null, null);

        }
        #endregion
        
        #region Grid For Item Editing
        private const string ValueStringFld = "ValueStringFld";
        private const string ValueNumericFld = "ValueNumericFld";
        private const string ValueEnumFld = "ValueEnumFld";

        protected void OnNumericFldChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var item = (textBox.BindingContainer as DataGridItem);
            var parsePrev = string.IsNullOrEmpty(textBox.ToolTip) ? 0 : Convert.ToDecimal(textBox.ToolTip);
            var parseNew = string.IsNullOrEmpty(textBox.Text.Trim()) ? 0 : Convert.ToDecimal(textBox.Text.Trim());
            var isChanged = (parsePrev != parseNew);
            MarkGridItemAsChanged(item, isChanged);
        }
        protected void OnTextFldChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var item = (textBox.BindingContainer as DataGridItem);
            var isChanged = textBox.ToolTip != textBox.Text;
            MarkGridItemAsChanged(item, isChanged);
        }

		//Triggers on selecting an option from a dropdown menu
        protected void OnEnumMeasureChoice(object sender, EventArgs e) 
        {
            var dropList = sender as DropDownList;
            var item = (dropList.BindingContainer as DataGridItem);
            bool isChanged = (dropList.ToolTip != dropList.SelectedItem.Text);
            MarkGridItemAsChanged(item, isChanged);
        }
        private void MarkGridItemAsChanged(DataGridItem item, bool changed)
        {
            if (changed)
            {
                item.Style.Add("background-color","LightGoldenrodYellow");
                //item.Cells[2].BackColor = Color.LightGoldenrodYellow;
                //item.Cells[2].Font.Italic = true;
            }
            else
            {
                item.Style.Remove("background-color");
                //item.Cells[2].BackColor = Color.White;
                //item.Cells[2].Font.Italic = false;
            }
        }
        protected void OnEditItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //-- Load data on controls
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;
            var editModel = e.Item.DataItem as ItemValueEditModel;
            if (editModel == null) return;
            if (editModel.HasChange)
            {
                MarkGridItemAsChanged(e.Item, true);
            }
            var stringFld = e.Item.FindControl(ValueStringFld) as TextBox;
            var numericFld = e.Item.FindControl(ValueNumericFld) as TextBox;
            var enumFld = e.Item.FindControl(ValueEnumFld) as DropDownList;

            if (stringFld == null || numericFld == null || enumFld == null) return;
            if (editModel.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                /*IvanB 15/03 start*/
                if (editModel.MeasureName.Equals("Shape (cut)"))
                {
                    enumFld.Attributes.Add("class", "filtered-select");
                }
                /*IvanB 15/03 end*/
                enumFld.DataSource = editModel.EnumSource;
                enumFld.DataBind();
                enumFld.ToolTip = editModel.PrevValue;
                if (string.IsNullOrEmpty(editModel.Value))
                {
                    enumFld.SelectedIndex = -1;
                    enumFld.ToolTip = "";
                }
                else
                {
                    enumFld.SelectedValue = editModel.Value;
                    var prevValue = editModel.EnumSource.Find(m => "" + m.MeasureValueId == editModel.PrevValue);
                    enumFld.ToolTip = prevValue == null ? "" : prevValue.ValueTitle;
                }
                enumFld.Visible = true;
                stringFld.Visible = false;
                numericFld.Visible = false;
                return;
            }
            if (editModel.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                numericFld.Text = editModel.Value;
                numericFld.ToolTip = editModel.PrevValue;
                enumFld.Visible = false;
                stringFld.Visible = false;
                numericFld.Visible = true;
                return;
            }
            stringFld.Text = editModel.Value;
            stringFld.ToolTip = editModel.PrevValue;
            enumFld.Visible = false;
            stringFld.Visible = true;
            numericFld.Visible = false;

        }
        private void LoadDataForEditing() 
        {
            var itemModel = GetSelectedItemModel();
            var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
            ItemValuesGrid.DataSource = itemValues; // itemModel.IsInvalid ? null : itemValues;
            ItemValuesGrid.DataBind();
            ItemValuesGrid.Enabled = !itemModel.IsInvalid;
			dvDescWithComment.Visible = true;
			DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            ShowHideItemDetails(true);
            cmdSave.Visible = true;
            

        }
        void ShowHideItemDetails(bool show)
        {
            DataForLabel.Visible = show;
            MeasurementPanel.Visible = show;
			dvDescWithComment.Visible = show;
		}
        private void GetNewValuesFromGrid()
        {
            var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[item.ItemIndex];
                var partId = key.Split(';')[0];
                var measureId = key.Split(';')[1];
                var itemValue = itemValues.Find(m => m.PartId == partId && "" + m.MeasureId == measureId);
                if (itemValue == null) continue;
                var newValue = "";
                if (itemValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enumFld = item.FindControl(ValueEnumFld) as DropDownList;
                    if (enumFld != null) newValue = enumFld.SelectedValue;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                    if (numericFld != null) newValue = numericFld.Text;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassText)
                {
                    var stringFld = item.FindControl(ValueStringFld) as TextBox;
                    if (stringFld != null) newValue = stringFld.Text;
                }
                //if (!itemValue.HasChanged(newValue)) continue;
                itemValue.Value = newValue;
            }
        }
        private void LoadMeasuresForEdit(string itemNumber)
        {
            var itemModel = GetItemModelFromView(itemNumber);
            var enumsMeasure = GetMeasureEnumsFromView();
            var parts = GetPartsFromView();
            var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            var currPart = PartTree.SelectedValue;
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
            if (withShapes.Count > 0)
            {
                var path = withShapes[0].ShapePath2Drawing;
                if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowShape(path);
                else HideShapePanel();
            }
            else
            {
                HideShapePanel();
            }
            var measures = GetMeasuresCpFromView();
            var result = new List<ItemValueEditModel>();
            foreach (var measure in measures)
            {
                if (measure.MeasureClass > 3) continue;
                var partModel = parts.Find(m => m.PartId == measure.PartId);
                if (partModel == null) continue;
                var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
                var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                        ? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
                result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
            }
            result.Sort(new ItemValueEditComparer().Compare);
            //IvanB 14/03 start
            var blockShapesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
            var shapes = result.FindAll(m => m.MeasureName.Equals("Shape (cut)"));
            if (shapes != null && shapes.Count > 0)
            {
                foreach (var shapeEnum in shapes)
                {
                    var selectedShape = shapeEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(shapeEnum.Value)).ValueTitle;
                    shapeEnum.EnumSource = shapeEnum.EnumSource.Where(x => !blockShapesList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedShape.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    shapeEnum.EnumSource = shapeEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
			/*Nimeshp Strat */
			DataTable dtExtData= QueryUtils.GetExtendedDescription(int.Parse(PartTree.SelectedValue), itemModel.BatchId, int.Parse(itemModel.ItemCode), this);
            if (dtExtData.Rows.Count == 1)
            {
                txtExtDesc.Text = dtExtData.Rows[0]["ExtDescription"].ToString();
                hdnExtDesc.Value = txtExtDesc.Text;

                txtExtComment.Text = dtExtData.Rows[0]["ExtExternalComment"].ToString();
                hdnExtComment.Value = txtExtComment.Text;
            }
            else
            {
                txtExtDesc.Text = "";
                hdnExtDesc.Value = "";
                txtExtComment.Text = "";
                hdnExtComment.Value = "";
			}
			/*Nimeshp End */

			//IvanB 14/03 end
			SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);

        }


        #endregion

        #region Shape Panel
        private void ShowShape(string dbShape)
        {
            string imgPath;
            string errMsg;
            string pngPath;
			

			ShapePanel.Visible = true;
            if (dbShape.Substring(0, 1) == "/" || dbShape.Substring(0, 1) == "\\")
            {
                dbShape = dbShape.Substring(1);
            }
			var pngStream = new MemoryStream();
			var myResultAzure = Utlities.GetShapeImageUrlAzure(dbShape, out pngPath, out errMsg, this, /*out imgPath, */ out pngStream);
			if (myResultAzure)
			{
				//itemShape.ImageUrl = pngPath;
				
				itemShape.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length);
				itemShape.Visible = true;

			}

			//var result = Utlities.GetShapeImageUrl(dbShape, out imgPath, out errMsg, this, out pngPath);
			//if (result)
			//{
			//	itemShape.ImageUrl = pngPath;// imgPath;
			//	itemShape.Visible = true;
			//}
			else
			{
                itemShape.Visible = false;
            }
            //-- Path to picture
            ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbShape, "Shape");
            ErrShapeField.Text = errMsg;

        }
        private void HideShapePanel()
        {
            ShapePanel.Visible = false;
        }
        #endregion

        #region Picture Panel
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            PicturePathAbbr.Text = ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            ErrPictureField.Text = errMsg;

        }
        private void HidePicturePanel()
        {
            itemPicture.Visible = false;
            PicturePathAbbr.Text = "";
            ErrPictureField.Text = "";
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Question Dialog
        static string ModeOnItemChanges = "item";
        static string ModeOnLoadClick = "load";
        private void PopupSaveQDialog(string src)
        {
            var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
            SaveDlgMode.Value = src;
            QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
            SaveQPopupExtender.Show();
        }
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            var errMsg = SaveExecute();
            if (!string.IsNullOrEmpty(errMsg))
            {
                // Return on prev item
                txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
                lstItemList.SelectedValue = ItemEditing.Value;
                PopupInfoDialog(errMsg, true);
                return;
            }

            if (SaveDlgMode.Value == ModeOnItemChanges)
            {
                OnItemListSelected();
            }
            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
        }
        protected void OnQuesSaveNoClick(object sender, EventArgs e)
        {
            if (SaveDlgMode.Value == ModeOnItemChanges)
            {
                OnItemListSelected();
            }
            if (SaveDlgMode.Value == ModeOnLoadClick)
            {
                LoadExecute();
            }
        }
        protected void OnQuesSaveCancelClick(object sender, EventArgs e)
        {
            lstItemList.SelectedValue = ItemEditing.Value;
            txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
        }
        #endregion


    }
}
