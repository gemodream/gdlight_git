﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="testPage.aspx.cs" Inherits="Corpt.testPage" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
<script language="javascript" type="text/javascript">
    function MakeStaticHeader(gridId, height, width, headerHeight) {
        var tbl = document.getElementById(gridId);
        if (tbl) {
            var DivHR = document.getElementById('DivHeaderRow');
            var DivMC = document.getElementById('DivMainContent');

            //*** Set divheaderRow Properties ****
            DivHR.style.height = headerHeight + 'px';
            DivHR.style.width = (parseInt(width) - 16) + 'px';
            DivHR.style.position = 'relative';
            DivHR.style.top = '0px';
            DivHR.style.zIndex = '10';
            DivHR.style.verticalAlign = 'top';

            //*** Set divMainContent Properties ****
            DivMC.style.width = width + 'px';
            DivMC.style.height = height + 'px';
            DivMC.style.position = 'relative';
            DivMC.style.top = -headerHeight + 'px';
            DivMC.style.zIndex = '1';

            //****Copy Header in divHeaderRow****
            DivHR.appendChild(tbl.cloneNode(true));
        }
    }

    function OnScrollDiv(Scrollablediv) {
        document.getElementById('DivHeaderRow').scrollLeft = Scrollablediv.scrollLeft;
        document.getElementById('DivFooterRow').scrollLeft = Scrollablediv.scrollLeft;
    }


</script>

<div id="DivRoot" align="left">
    <div style="overflow: hidden;" id="DivHeaderRow">
    </div>

    <div style="overflow:scroll;" onscroll="OnScrollDiv(this)" id="DivMainContent">
       
     <asp:DataGrid ID="dgResult" runat="server" Style="font-family: 'MS Sans Serif';
            font-size: small;" class="table" Width="100%" CellPadding="1" BackColor="White"
            ForeColor="#555555" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DDDDDD"
            CssClass="table" Font-Names="Cambria" Enabled="false"
            Visible="true" GridLines="Both" Font-Size="12px"
            UseAccessibleHeader = "true" >
            <ItemStyle ForeColor="#555555" Font-Names="Cambria" Font-Size="13px"></ItemStyle>
            <HeaderStyle Font-Bold="True" ForeColor="#555555" BackColor="White" Font-Size="12px">
            </HeaderStyle>
          </asp:DataGrid>
      
    </div>
</div>

</asp:Content>