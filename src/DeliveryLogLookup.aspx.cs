using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Corpt
{
	/// <summary>
	/// Summary description for DeliveryLogLookup.
	/// </summary>
	public partial class DeliveryLogLookup : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				LoadItemsOut();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		private void LoadItemsOut()
		{
			SqlCommand command = new SqlCommand("trkGetItemsOut");
			command.Connection=new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@StageOut", 14));
			command.Parameters.Add(new SqlParameter("@StageIn", 15));
			
			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();

			da.Fill(dt);

			dgCurrentBatchDisplay.DataSource=dt;
			dgCurrentBatchDisplay.DataBind();
		}
		
		
		protected void cmdLookup_Click(object sender, System.EventArgs e)
		{
			
			string strBatchNumber = txtBatchNumber.Text.Trim();
			string strGroupCode = "";
			string strBatchCode = "";
			string strItemCode = "";
			string sFullItemNumber = "";
			if(Regex.IsMatch(strBatchNumber, @"^\d{10}$|^\d{11}$")) // Full Item Number entered.
			{
//				strGroupCode = strBatchNumber.Substring(0,5);
//				strBatchCode = strBatchNumber.Substring(5,3);
//				strItemCode = strBatchNumber.Substring(8,2);

				strGroupCode =	Utils.FillToFiveChars(Utils.ParseOrderCode(strBatchNumber).ToString());
				strBatchCode =	Utils.FillToThreeChars(Utils.ParseBatchCode(strBatchNumber).ToString(), strGroupCode);
				strItemCode =	Utils.FillToTwoChars(Utils.ParseItemCode(strBatchNumber).ToString());

		
				SqlCommand command = new SqlCommand("spGetItemByCode");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter("@CustomerCode",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BDate",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EDate",DBNull.Value));

				command.Parameters.Add(new SqlParameter("@GroupCode",strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode",strBatchCode));
				command.Parameters.Add(new SqlParameter("@ItemCode",strItemCode));

				command.Parameters.Add("@AuthorId",Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());

				command.Parameters.Add(new SqlParameter("@IsNew", 1));
                
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtItemCheck = new DataTable();

				da.Fill(dtItemCheck);
				if(dtItemCheck.Rows.Count>0)
				{
					string strPrevGroupCode = dtItemCheck.Rows[0]["prevGroupCode"].ToString();
					string strPrevBatchCode = dtItemCheck.Rows[0]["prevBatchCode"].ToString();
					string strPrevItemCode = dtItemCheck.Rows[0]["prevItemCode"].ToString();

					string strFullItemNumber = Utils.FullItemNumber(strPrevGroupCode, strPrevBatchCode, strPrevItemCode);

					DislpayItemDeliveryHistory(strFullItemNumber);
				}
				else
				{
					lblInfo.Text="Please enter batch number or item number";
				}

			}
			else
			{
				if(Regex.IsMatch(strBatchNumber, @"^\d{8}$|^\d{9}$")) //Full Batch Number entered
				{
					sFullItemNumber = strBatchNumber + "00";
					strGroupCode =	Utils.FillToFiveChars(Utils.ParseOrderCode(sFullItemNumber).ToString());
					strBatchCode =	Utils.FillToThreeChars(Utils.ParseBatchCode(sFullItemNumber).ToString(), strGroupCode);

//					strGroupCode = strBatchNumber.Substring(0,5);
//					strBatchCode = strBatchNumber.Substring(5,3);

					SqlCommand command = new SqlCommand("spGetBatchByCode");
					command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add(new SqlParameter("@CustomerCode", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@BGroupState", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@EGroupState", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@BState", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@EState", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@BDate", DBNull.Value));
					command.Parameters.Add(new SqlParameter("@EDate", DBNull.Value));

					command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
					command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));

					command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
					command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

					SqlDataAdapter da = new SqlDataAdapter(command);
					DataTable dt = new DataTable();
					da.Fill(dt);

//					dgDebug.DataSource=dt;
//					dgDebug.DataBind();

					if(dt.Rows.Count>0) // have batchid
					{
						string strBatchID = dt.Rows[0]["BatchID"].ToString();
						DislpayCurrentBatchDeliveryHistory(int.Parse(strBatchID));
					}
					else
					{
						lblInfo.Text="Please enter batch number or item number";
					}
				}
				else				
				{
					lblInfo.Text="Please enter batch number or item number";
				}
			}
		}

		private void DislpayCurrentBatchDeliveryHistory(int intBatchID)
		{
			SqlCommand command = new SqlCommand("trkSpGetDeliveryLog");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@BatchID",intBatchID));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dtRaw = new DataTable();
			da.Fill(dtRaw);

			if(dtRaw.Rows.Count>0)
			{
				DataTable dtDisplay = new DataTable();
				dtDisplay.Columns.Add("Description");
				dtDisplay.Columns.Add("Date/Time");
				dtDisplay.Columns.Add("Item Number");
				dtDisplay.Columns.Add("Batch");
				dtDisplay.AcceptChanges();

				foreach(DataRow dr in dtRaw.Rows)
				{
					DataRow drDisplay = dtDisplay.NewRow();
					drDisplay["Description"]=dr["EventName"].ToString();
					drDisplay["Date/Time"]=dr["RecordTimeStamp"].ToString();
					drDisplay["Item Number"]=dr["FullItemNumber"].ToString();

					string strOutBatchNumber = "";
					string strGroupCode = dr["GroupCode"].ToString();
					string strBatchCode = dr["BatchCode"].ToString();
					strOutBatchNumber = Utils.FullBatchNumber(strGroupCode, strBatchCode);
					drDisplay["Batch"]=strOutBatchNumber;
					dtDisplay.Rows.Add(drDisplay);
				}

				dtDisplay.AcceptChanges();
				dgCurrentBatchDisplay.DataSource=dtDisplay;
				dgCurrentBatchDisplay.DataBind();
			}
			else
			{
				dgCurrentBatchDisplay.DataSource=null;
				dgCurrentBatchDisplay.DataBind();
			}
		}

		private void DislpayItemDeliveryHistory(string strItemNumber)
		{
			SqlCommand command = new SqlCommand("trkSpGetItemDeliveryLog");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@ItemNumber", strItemNumber));
			
			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();

			da.Fill(dt);

			if(true)
			{
				DataTable dtDisplay = new DataTable();
				dtDisplay.Columns.Add("Description");
				dtDisplay.Columns.Add("Date/Time");
				dtDisplay.Columns.Add("Item Number");
				dtDisplay.Columns.Add("Batch");
				dtDisplay.AcceptChanges();

				foreach(DataRow dr in dt.Rows)
				{
					DataRow drDisplay = dtDisplay.NewRow();
					drDisplay["Description"]= dr["EventName"].ToString();
					drDisplay["Date/Time"]=dr["RecordTimeStamp"].ToString();
					drDisplay["Item Number"] = dr["FullItemNumber"].ToString();

					string strOutBatchNumber = "";
					string strGroupCode = dr["GroupCode"].ToString();
					string strBatchCode = dr["BatchCode"].ToString();
					strOutBatchNumber = Utils.FullBatchNumber(strGroupCode, strBatchCode);
					drDisplay["Batch"]=strOutBatchNumber;
					dtDisplay.Rows.Add(drDisplay);					
				}

				dgCurrentBatchDisplay.DataSource = dtDisplay ;
				dgCurrentBatchDisplay.DataBind();
			}			
		}

		protected void txtBatchNumber_TextChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}
