﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DeliveryLog2 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Delivery Log";
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            var records = QueryUtils.GetDeliveryLog(this, lstEvents.SelectedValue);
            SetViewState(records, SessionConstants.TrkEventList);
            UpdateDataView();
        }
        void UpdateDataView()
        {
            var events = GetViewState(SessionConstants.TrkEventList) as List<DeliveryLogModel>;


            if (events == null || events.Count == 0)
            {
                dgrLog.DataSource = null;
                dgrLog.DataBind();
                return;
            }
            var table = new DataTable("DeliveryEvents");
            table.Columns.Add("RecordId");
            table.Columns.Add("FullItemNumber");
            table.Columns.Add("RecordTimeStamp", typeof(DateTime));
            table.Columns.Add("EventName");
            foreach(var row in events)
            {
                table.Rows.Add(new object[] {row.RecordId, row.DisplayFullItemNumber, row.RecordTimeStamp, row.EventName});
            }
            table.AcceptChanges();
            
            var dv = new DataView { Table = table };
            // Apply sort information to the view
            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "RecordTimeStamp";
                SetViewState(new SortingModel { ColumnName = "RecordTimeStamp", Direction = true }, SessionConstants.CurrentSortModel);
            }

            // Rebind data 
            dgrLog.DataSource = dv;
            dgrLog.DataBind();
        }

        protected void OnEventSelectedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            UpdateDataView();

        }
    }
}