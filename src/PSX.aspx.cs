﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class PSX : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            CalendarExtenderFrom.SelectedDate = DateTime.Now.Date;
            CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
            ScanNumberFld.Focus();
        }
        #region DateFrom, DateTo
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
        }
        protected void OnChangedDateTo(object sender, EventArgs e)
        {

            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
        }

        #endregion

        #region Lookup
        protected void OnLookupClick(object sender, EventArgs e)
        {
            var filterModel = new PsxFilterModel
            {
                DateFrom = CalendarExtenderFrom.SelectedDate,
                DateTo = CalendarExtenderTo.SelectedDate,
                ScanLike = ScanNumberFld.Text.Trim(),
                Xfactor = Convert.ToInt32(XFactorList.SelectedValue)
            };
            var data = QueryUtils.GetScanData(filterModel, this);
            RowsCountLabel.Text = (data.Count == 0 ? "" : data.Count + " rows");
            SetViewState(data, SessionConstants.ScanData);
            ApplySorting();
        }
        private void ApplySorting()
        {
            var scanData = GetViewState(SessionConstants.ScanData) as List<PsxNormDataModel>;
            if (scanData == null) return;
            
            var table = CreateTable(scanData);
            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "Scan";
                SetViewState(new SortingModel { ColumnName = "Scan", Direction = true }, SessionConstants.CurrentSortModel);
            }

            ScanGrid.DataSource = dv;
            ScanGrid.DataBind();
        }
        private DataTable CreateTable(IEnumerable<PsxNormDataModel> dataList)
        {
            var table = new DataTable("ScanData");
            table.Columns.Add(new DataColumn { ColumnName = "Scan", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Old Item Number", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "New Item Number", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Brilliance", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Fire", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Scintillation", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Efficiency", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Contrast", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "X-fctr", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Scan Date", DataType = typeof(DateTime) });
            foreach(var data in dataList)
            {
                table.Rows.Add(new object[]
                {
                    data.ScanId,
                    data.ReLinkOldItemNumber,
                    data.ReLinkNewItemNumber,
                    data.DisplayBrilliance,
                    data.DisplayFire,
                    data.DisplayScintillation,
                    data.DisplayEfficiency,
                    data.DisplayConrtast,
                    data.DisplayXFactor,
                    data.ScanDate
                });
            }
            table.AcceptChanges();
            return table;
        }
        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            ApplySorting();
        }

        #endregion
    }
}