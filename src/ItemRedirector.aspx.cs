using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for ItemRedirector.
	/// </summary>
	public partial class ItemRedirector : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Utils.NullValue(Session["ID"])=="")
			{
				Response.Redirect("login.aspx");
			}
			else
			{
				if((Utils.NullValue(Request.QueryString["itemNumber"])!="")&&Regex.IsMatch(Request.QueryString["itemNumber"],@"^\d{10}$|^\d{11}$"))
				{
					Response.Redirect(PrepareResponse(Request.QueryString["itemNumber"]));
				}
			}
		}								 
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	
		private string PrepareResponse(string itemNumber)
		{
			int iItemCode;
			int iBatchCode;
			int iGroupCode;

			if(Utils.DissectItemNumber(itemNumber, out iGroupCode, out iBatchCode, out iItemCode))
			{
				SqlCommand command = new SqlCommand("wspvvGetBatchIDItemCodeByItemNumber");
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.Connection=conn;
				command.CommandType=CommandType.StoredProcedure;
                
				command.Parameters.Add("@GroupCode",iGroupCode);
				command.Parameters.Add("@BatchCode",iBatchCode);
				command.Parameters.Add("@ItemCode",	iItemCode);

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dt = new DataTable();

				da.Fill(dt);

				if(dt.Rows.Count>0)
				{
					return "ItemView.aspx?BatchID="+dt.Rows[0]["NewBatchID"].ToString()+"&ItemCode="+dt.Rows[0]["NewItemCode"].ToString() + "&ItemNumber=" + itemNumber;
				}

			}
			else
			{
				return "PSX.aspx";
			}
			return "PSX.aspx";
		}
	}
}
