﻿using Corpt.Utilities;

namespace Corpt.FullItemCode
{
    /// <summary>
    /// Contains utility methods for parsing and union Full Item Code
    /// </summary>
    public static class FullItemCodeUtils
    {
        /// <summary>
        /// Determines whether the specified batch has code > 99.
        /// </summary>
        /// <param name="groupBatchCode">The group + batch codes: [Group Code] * 10^3 + [Batch Code]</param>
        public static bool IsBigBatchCode(long groupBatchCode)
        {
            return QueryUtils.IsBigBatchCode(groupBatchCode);
        }

        /// <summary>
        /// Determines whether the specified batch has code > 99.
        /// </summary>
        /// <param name="groupBatchCode">Concatenation of the  group(4/5/6 chars) and batch(3 chars) codes</param>
        public static bool IsBigBatchCode(string groupBatchCode)
        {
            return IsBigBatchCode(int.Parse(groupBatchCode));
        }

        /// <summary>
        /// Determines whether the specified group is group with batches with codes > 99.
        ///
        /// To generate this code run:
        /// SELECT CONCAT('if (groupCode == ', g.GroupCode, ') return true; if (groupCode &gt; ', g.GroupCode, ') return false;')
        /// FROM tblBatch b
        /// INNER JOIN tblGroup g ON b.GroupID = g.GroupID
        /// WHERE b.BatchCode &gt; 99
        /// GROUP BY g.GroupCode
        /// ORDER BY g.GroupCode DESC;
        /// 
        /// </summary>
        /// <param name="groupCode">The group code.</param>
        /// <returns>
        ///   <c>true</c> if the specified group is group with batches with codes > 99; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsGroupHasBigBatchCodes(long groupCode)
        {
            if (groupCode == 895118) return true; if (groupCode > 895118) return false;
            if (groupCode == 872238) return true; if (groupCode > 872238) return false;
            if (groupCode == 795948) return true; if (groupCode > 795948) return false;
            if (groupCode == 795876) return true; if (groupCode > 795876) return false;
            if (groupCode == 776533) return true; if (groupCode > 776533) return false;
            if (groupCode == 776532) return true; if (groupCode > 776532) return false;
            if (groupCode == 768619) return true; if (groupCode > 768619) return false;
            if (groupCode == 760138) return true; if (groupCode > 760138) return false;
            if (groupCode == 754521) return true; if (groupCode > 754521) return false;
            if (groupCode == 754034) return true; if (groupCode > 754034) return false;
            if (groupCode == 745797) return true; if (groupCode > 745797) return false;
            if (groupCode == 740973) return true; if (groupCode > 740973) return false;
            if (groupCode == 664017) return true; if (groupCode > 664017) return false;
            if (groupCode == 663836) return true; if (groupCode > 663836) return false;
            if (groupCode == 655864) return true; if (groupCode > 655864) return false;
            if (groupCode == 478553) return true; if (groupCode > 478553) return false;
            if (groupCode == 478434) return true; if (groupCode > 478434) return false;
            if (groupCode == 478112) return true; if (groupCode > 478112) return false;
            if (groupCode == 413187) return true; if (groupCode > 413187) return false;
            if (groupCode == 413155) return true; if (groupCode > 413155) return false;
            if (groupCode == 412735) return true; if (groupCode > 412735) return false;
            if (groupCode == 412332) return true; if (groupCode > 412332) return false;
            if (groupCode == 411487) return true; if (groupCode > 411487) return false;
            if (groupCode == 403851) return true; if (groupCode > 403851) return false;
            if (groupCode == 401726) return true; if (groupCode > 401726) return false;
            if (groupCode == 400854) return true; if (groupCode > 400854) return false;
            if (groupCode == 327801) return true; if (groupCode > 327801) return false;
            if (groupCode == 275324) return true; if (groupCode > 275324) return false;
            if (groupCode == 261313) return true; if (groupCode > 261313) return false;
            if (groupCode == 261312) return true; if (groupCode > 261312) return false;
            if (groupCode == 261310) return true; if (groupCode > 261310) return false;
            if (groupCode == 261309) return true; if (groupCode > 261309) return false;
            if (groupCode == 261308) return true; if (groupCode > 261308) return false;
            if (groupCode == 261307) return true; if (groupCode > 261307) return false;
            if (groupCode == 261305) return true; if (groupCode > 261305) return false;
            if (groupCode == 261304) return true; if (groupCode > 261304) return false;
            if (groupCode == 261303) return true; if (groupCode > 261303) return false;
            if (groupCode == 261302) return true; if (groupCode > 261302) return false;
            if (groupCode == 261301) return true; if (groupCode > 261301) return false;
            if (groupCode == 261299) return true; if (groupCode > 261299) return false;
            if (groupCode == 220234) return true; if (groupCode > 220234) return false;
            if (groupCode == 219649) return true; if (groupCode > 219649) return false;
            if (groupCode == 210282) return true; if (groupCode > 210282) return false;
            if (groupCode == 209777) return true; if (groupCode > 209777) return false;
            if (groupCode == 209593) return true; if (groupCode > 209593) return false;
            if (groupCode == 208868) return true; if (groupCode > 208868) return false;
            if (groupCode == 208685) return true; if (groupCode > 208685) return false;
            if (groupCode == 208359) return true; if (groupCode > 208359) return false;
            if (groupCode == 206628) return true; if (groupCode > 206628) return false;
            if (groupCode == 205807) return true; if (groupCode > 205807) return false;
            if (groupCode == 201041) return true; if (groupCode > 201041) return false;
            if (groupCode == 197444) return true; if (groupCode > 197444) return false;
            if (groupCode == 197433) return true; if (groupCode > 197433) return false;
            if (groupCode == 197134) return true; if (groupCode > 197134) return false;
            if (groupCode == 188732) return true; if (groupCode > 188732) return false;
            if (groupCode == 183532) return true; if (groupCode > 183532) return false;
            if (groupCode == 176066) return true; if (groupCode > 176066) return false;
            if (groupCode == 175686) return true; if (groupCode > 175686) return false;
            if (groupCode == 173626) return true; if (groupCode > 173626) return false;
            if (groupCode == 172258) return true; if (groupCode > 172258) return false;
            if (groupCode == 171812) return true; if (groupCode > 171812) return false;
            if (groupCode == 171765) return true; if (groupCode > 171765) return false;
            if (groupCode == 171193) return true; if (groupCode > 171193) return false;
            if (groupCode == 171069) return true; if (groupCode > 171069) return false;
            if (groupCode == 171022) return true; if (groupCode > 171022) return false;
            if (groupCode == 170959) return true; if (groupCode > 170959) return false;
            if (groupCode == 170582) return true; if (groupCode > 170582) return false;
            if (groupCode == 169985) return true; if (groupCode > 169985) return false;
            if (groupCode == 169912) return true; if (groupCode > 169912) return false;
            if (groupCode == 169907) return true; if (groupCode > 169907) return false;
            if (groupCode == 169756) return true; if (groupCode > 169756) return false;
            if (groupCode == 169318) return true; if (groupCode > 169318) return false;
            if (groupCode == 169315) return true; if (groupCode > 169315) return false;
            if (groupCode == 169088) return true; if (groupCode > 169088) return false;
            if (groupCode == 168267) return true; if (groupCode > 168267) return false;
            if (groupCode == 168122) return true; if (groupCode > 168122) return false;
            if (groupCode == 167278) return true; if (groupCode > 167278) return false;
            if (groupCode == 167068) return true; if (groupCode > 167068) return false;
            if (groupCode == 167004) return true; if (groupCode > 167004) return false;
            if (groupCode == 166548) return true; if (groupCode > 166548) return false;
            if (groupCode == 165583) return true; if (groupCode > 165583) return false;
            if (groupCode == 164921) return true; if (groupCode > 164921) return false;
            if (groupCode == 163428) return true; if (groupCode > 163428) return false;
            if (groupCode == 162496) return true; if (groupCode > 162496) return false;
            if (groupCode == 162443) return true; if (groupCode > 162443) return false;
            if (groupCode == 162355) return true; if (groupCode > 162355) return false;
            if (groupCode == 162027) return true; if (groupCode > 162027) return false;
            if (groupCode == 161978) return true; if (groupCode > 161978) return false;
            if (groupCode == 161749) return true; if (groupCode > 161749) return false;
            if (groupCode == 161682) return true; if (groupCode > 161682) return false;
            if (groupCode == 160872) return true; if (groupCode > 160872) return false;
            if (groupCode == 160677) return true; if (groupCode > 160677) return false;
            if (groupCode == 160621) return true; if (groupCode > 160621) return false;
            if (groupCode == 160565) return true; if (groupCode > 160565) return false;
            if (groupCode == 160216) return true; if (groupCode > 160216) return false;
            if (groupCode == 160091) return true; if (groupCode > 160091) return false;
            if (groupCode == 160046) return true; if (groupCode > 160046) return false;
            if (groupCode == 159764) return true; if (groupCode > 159764) return false;
            if (groupCode == 159735) return true; if (groupCode > 159735) return false;
            if (groupCode == 159685) return true; if (groupCode > 159685) return false;
            if (groupCode == 159627) return true; if (groupCode > 159627) return false;
            if (groupCode == 159556) return true; if (groupCode > 159556) return false;
            if (groupCode == 159508) return true; if (groupCode > 159508) return false;
            if (groupCode == 159425) return true; if (groupCode > 159425) return false;
            if (groupCode == 159256) return true; if (groupCode > 159256) return false;
            if (groupCode == 159112) return true; if (groupCode > 159112) return false;
            if (groupCode == 159001) return true; if (groupCode > 159001) return false;
            if (groupCode == 158954) return true; if (groupCode > 158954) return false;
            if (groupCode == 158919) return true; if (groupCode > 158919) return false;
            if (groupCode == 158802) return true; if (groupCode > 158802) return false;
            if (groupCode == 158629) return true; if (groupCode > 158629) return false;
            if (groupCode == 158567) return true; if (groupCode > 158567) return false;
            if (groupCode == 158472) return true; if (groupCode > 158472) return false;
            if (groupCode == 158446) return true; if (groupCode > 158446) return false;
            if (groupCode == 158352) return true; if (groupCode > 158352) return false;
            if (groupCode == 158271) return true; if (groupCode > 158271) return false;
            if (groupCode == 158216) return true; if (groupCode > 158216) return false;
            if (groupCode == 157251) return true; if (groupCode > 157251) return false;
            if (groupCode == 157142) return true; if (groupCode > 157142) return false;
            if (groupCode == 157081) return true; if (groupCode > 157081) return false;
            if (groupCode == 157004) return true; if (groupCode > 157004) return false;
            if (groupCode == 156991) return true; if (groupCode > 156991) return false;
            if (groupCode == 156935) return true; if (groupCode > 156935) return false;
            if (groupCode == 156839) return true; if (groupCode > 156839) return false;
            if (groupCode == 156778) return true; if (groupCode > 156778) return false;
            if (groupCode == 156725) return true; if (groupCode > 156725) return false;
            if (groupCode == 156285) return true; if (groupCode > 156285) return false;
            if (groupCode == 156179) return true; if (groupCode > 156179) return false;
            if (groupCode == 155815) return true; if (groupCode > 155815) return false;
            if (groupCode == 155776) return true; if (groupCode > 155776) return false;
            if (groupCode == 155321) return true; if (groupCode > 155321) return false;
            if (groupCode == 155125) return true; if (groupCode > 155125) return false;
            if (groupCode == 154605) return true; if (groupCode > 154605) return false;
            if (groupCode == 154538) return true; if (groupCode > 154538) return false;
            if (groupCode == 154074) return true; if (groupCode > 154074) return false;
            if (groupCode == 154017) return true; if (groupCode > 154017) return false;
            if (groupCode == 153969) return true; if (groupCode > 153969) return false;
            if (groupCode == 153371) return true; if (groupCode > 153371) return false;
            if (groupCode == 153299) return true; if (groupCode > 153299) return false;
            if (groupCode == 152408) return true; if (groupCode > 152408) return false;
            if (groupCode == 152086) return true; if (groupCode > 152086) return false;
            if (groupCode == 151615) return true; if (groupCode > 151615) return false;
            if (groupCode == 150878) return true; if (groupCode > 150878) return false;
            if (groupCode == 150836) return true; if (groupCode > 150836) return false;
            if (groupCode == 149695) return true; if (groupCode > 149695) return false;
            if (groupCode == 148727) return true; if (groupCode > 148727) return false;
            if (groupCode == 148495) return true; if (groupCode > 148495) return false;
            if (groupCode == 147993) return true; if (groupCode > 147993) return false;
            if (groupCode == 147936) return true; if (groupCode > 147936) return false;
            if (groupCode == 147241) return true; if (groupCode > 147241) return false;
            if (groupCode == 147128) return true; if (groupCode > 147128) return false;
            if (groupCode == 145841) return true; if (groupCode > 145841) return false;
            if (groupCode == 145023) return true; if (groupCode > 145023) return false;
            if (groupCode == 144903) return true; if (groupCode > 144903) return false;
            if (groupCode == 144762) return true; if (groupCode > 144762) return false;
            if (groupCode == 143554) return true; if (groupCode > 143554) return false;
            if (groupCode == 143552) return true; if (groupCode > 143552) return false;
            if (groupCode == 143433) return true; if (groupCode > 143433) return false;
            if (groupCode == 143372) return true; if (groupCode > 143372) return false;
            if (groupCode == 143288) return true; if (groupCode > 143288) return false;
            if (groupCode == 143236) return true; if (groupCode > 143236) return false;
            if (groupCode == 143162) return true; if (groupCode > 143162) return false;
            if (groupCode == 143108) return true; if (groupCode > 143108) return false;
            if (groupCode == 143002) return true; if (groupCode > 143002) return false;
            if (groupCode == 142829) return true; if (groupCode > 142829) return false;
            if (groupCode == 142753) return true; if (groupCode > 142753) return false;
            if (groupCode == 142264) return true; if (groupCode > 142264) return false;
            if (groupCode == 142214) return true; if (groupCode > 142214) return false;
            if (groupCode == 141904) return true; if (groupCode > 141904) return false;
            if (groupCode == 141007) return true; if (groupCode > 141007) return false;
            if (groupCode == 139471) return true; if (groupCode > 139471) return false;
            if (groupCode == 138679) return true; if (groupCode > 138679) return false;
            if (groupCode == 138265) return true; if (groupCode > 138265) return false;
            if (groupCode == 138194) return true; if (groupCode > 138194) return false;
            if (groupCode == 138147) return true; if (groupCode > 138147) return false;
            if (groupCode == 137964) return true; if (groupCode > 137964) return false;
            if (groupCode == 137893) return true; if (groupCode > 137893) return false;
            if (groupCode == 137535) return true; if (groupCode > 137535) return false;
            if (groupCode == 137487) return true; if (groupCode > 137487) return false;
            if (groupCode == 137183) return true; if (groupCode > 137183) return false;
            if (groupCode == 137016) return true; if (groupCode > 137016) return false;
            if (groupCode == 136921) return true; if (groupCode > 136921) return false;
            if (groupCode == 136799) return true; if (groupCode > 136799) return false;
            if (groupCode == 136471) return true; if (groupCode > 136471) return false;
            if (groupCode == 135588) return true; if (groupCode > 135588) return false;
            if (groupCode == 134318) return true; if (groupCode > 134318) return false;
            if (groupCode == 131467) return true; if (groupCode > 131467) return false;
            if (groupCode == 130982) return true; if (groupCode > 130982) return false;
            if (groupCode == 129931) return true; if (groupCode > 129931) return false;
            if (groupCode == 128975) return true; if (groupCode > 128975) return false;
            if (groupCode == 128908) return true; if (groupCode > 128908) return false;
            if (groupCode == 128682) return true; if (groupCode > 128682) return false;
            if (groupCode == 128252) return true; if (groupCode > 128252) return false;
            if (groupCode == 128055) return true; if (groupCode > 128055) return false;
            if (groupCode == 127877) return true; if (groupCode > 127877) return false;
            if (groupCode == 127709) return true; if (groupCode > 127709) return false;
            if (groupCode == 127256) return true; if (groupCode > 127256) return false;
            if (groupCode == 125467) return true; if (groupCode > 125467) return false;
            if (groupCode == 124939) return true; if (groupCode > 124939) return false;
            if (groupCode == 123727) return true; if (groupCode > 123727) return false;
            if (groupCode == 123182) return true; if (groupCode > 123182) return false;
            if (groupCode == 120277) return true; if (groupCode > 120277) return false;
            if (groupCode == 117361) return true; if (groupCode > 117361) return false;
            if (groupCode == 117309) return true; if (groupCode > 117309) return false;
            if (groupCode == 116282) return true; if (groupCode > 116282) return false;
            if (groupCode == 114282) return true; if (groupCode > 114282) return false;
            if (groupCode == 112555) return true; if (groupCode > 112555) return false;
            if (groupCode == 110593) return true; if (groupCode > 110593) return false;
            if (groupCode == 107729) return true; if (groupCode > 107729) return false;
            if (groupCode == 102119) return true; if (groupCode > 102119) return false;
            if (groupCode == 101162) return true; if (groupCode > 101162) return false;
            if (groupCode == 100367) return true; if (groupCode > 100367) return false;
            if (groupCode == 100001) return true; if (groupCode > 100001) return false;
            if (groupCode == 81006) return true; if (groupCode > 81006) return false;
            if (groupCode == 80423) return true; if (groupCode > 80423) return false;
            if (groupCode == 79448) return true; if (groupCode > 79448) return false;
            if (groupCode == 73941) return true; if (groupCode > 73941) return false;
            if (groupCode == 72868) return true; if (groupCode > 72868) return false;
            if (groupCode == 70548) return true; if (groupCode > 70548) return false;
            if (groupCode == 69280) return true; if (groupCode > 69280) return false;
            if (groupCode == 68740) return true; if (groupCode > 68740) return false;
            if (groupCode == 67717) return true; if (groupCode > 67717) return false;
            if (groupCode == 66943) return true; if (groupCode > 66943) return false;
            if (groupCode == 66213) return true; if (groupCode > 66213) return false;
            if (groupCode == 65451) return true; if (groupCode > 65451) return false;
            if (groupCode == 64387) return true; if (groupCode > 64387) return false;
            if (groupCode == 35688) return true; if (groupCode > 35688) return false;
            if (groupCode == 35676) return true; if (groupCode > 35676) return false;
            if (groupCode == 35663) return true; if (groupCode > 35663) return false;
            if (groupCode == 35642) return true; if (groupCode > 35642) return false;
            if (groupCode == 35598) return true; if (groupCode > 35598) return false;
            if (groupCode == 32294) return true; if (groupCode > 32294) return false;
            if (groupCode == 30982) return true; if (groupCode > 30982) return false;
            if (groupCode == 24350) return true; if (groupCode > 24350) return false;
            if (groupCode == 20143) return true; if (groupCode > 20143) return false;
            if (groupCode == 16135) return true; if (groupCode > 16135) return false;
            if (groupCode == 15489) return true; if (groupCode > 15489) return false;
            if (groupCode == 15481) return true; if (groupCode > 15481) return false;
            if (groupCode == 14827) return true; if (groupCode > 14827) return false;
            if (groupCode == 5321) return true; if (groupCode > 5321) return false;
            if (groupCode == 5314) return true; if (groupCode > 5314) return false;
            return false;
        }
    }
}