using System;

namespace Corpt.FullItemCode
{
    /// <summary>
    /// Parses string that can contains OrderCode/OrderCode+BatchCode/OrderCode+BatchCode+ItemCode
    /// and provides properties - parsing result: OrderCode, BatchCode, ItemCode
    /// </summary>
    public class FullItemCodeParser
    {
        private readonly string _itemNumber;
        private readonly Func<string, bool> _isBigBatchCode = FullItemCodeUtils.IsBigBatchCode;

        /// <summary>
        /// Initializes a new instance of the <see cref="FullItemCodeParser"/> class.
        /// </summary>
        /// <param name="itemNumber">The item number.</param>
        /// <param name="isBigBatchCode">isBigBatchCode function - for Unit Testing only</param>
        public FullItemCodeParser(string itemNumber,
            Func<string, bool> isBigBatchCode = null)
        {
            _itemNumber = itemNumber;
            if (isBigBatchCode != null)
            {
                _isBigBatchCode = isBigBatchCode;
            }
        }

        /// <summary>
        /// Gets or sets the order code.
        /// </summary>
        public int OrderCode { get; private set; }

        /// <summary>
        /// Gets or sets the batch code.
        /// </summary>
        public int BatchCode { get; private set; }

        /// <summary>
        /// Gets or sets the item code.
        /// </summary>
        public int ItemCode { get; private set; }

        public FullItemCodeParser Parse()
        {
            switch (_itemNumber.Length)
            {
                case 5:
                case 6:
                case 7:
                    ItemNumberLengthLe07();
                    break;
                case 8:
                    ItemNumberLengthEq08();
                    break;
                case 9:
                    ItemNumberLengthEq09();
                    break;
                case 10:
                    ItemNumberLengthEq10();
                    break;
                case 11:
                    ItemNumberLengthEq11();
                    break;
                default:
                    ItemNumberUnexpected();
                    break;
            }
            return this;
        }

        private void ItemNumberUnexpected()
        {
            OrderCode = 0;
            BatchCode = 0;
            ItemCode = 0;
        }

        private void ItemNumberLengthLe07()
        {
            OrderCode = int.Parse(_itemNumber);
            BatchCode = 0;
            ItemCode = 0;
        }

        private void ItemNumberLengthEq08()
        {
            //12345678 = 12345 678, 12345078 = 12345 078 as well as 35598100 = 35598 100 - when 35598 is order code with big batch code
            OrderCode = int.Parse(_itemNumber.Substring(0, 5));
            BatchCode = int.Parse(_itemNumber.Substring(5));
            ItemCode = 0;
        }

        private void ItemNumberLengthEq09()
        {

            if (_isBigBatchCode(_itemNumber))
            {
                //158472187 = 158472 187 - old  (35688 is order code with big batch code)
                OrderCode = int.Parse(_itemNumber.Substring(0, 6));
                BatchCode = int.Parse(_itemNumber.Substring(6, 3));
                ItemCode = 0;
            }
            else if (_itemNumber.Substring(6, 1) == "0")
            {
                //123456089 = 123456 089 - old
                OrderCode = int.Parse(_itemNumber.Substring(0, 6));
                BatchCode = int.Parse(_itemNumber.Substring(6, 3));
                ItemCode = 0;
            }
            else
            {
                //123456789 = 1234567 89 - new
                OrderCode = int.Parse(_itemNumber.Substring(0, 7));
                BatchCode = int.Parse(_itemNumber.Substring(7));
                ItemCode = 0;
            }
        }

        private void ItemNumberLengthEq10()
        {
            // it caanot be
            // 10 = 6 + 3 + 1 - because ItemCode should have 2 digits, not 1,
            // 10 = 7 + 3 - because BatchCode should have 2 digits, not 3, with new 7-digits OrderCode,
            // 10 = 7 + 2 + 1 - because ItemCode should have 2 digits, not 1
            if (_isBigBatchCode(_itemNumber.Substring(0, 8)))
            {
                //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
                OrderCode = int.Parse(_itemNumber.Substring(0, 5));
                BatchCode = int.Parse(_itemNumber.Substring(5, 3));
                ItemCode = int.Parse(_itemNumber.Substring(8, 2));
            }
            else if (_itemNumber.Substring(5, 1) == "0")
            {
                //1234507890 = 12345 078 90 - old - 10-chars returns first 5 chars only if 6th char = 0, otherwise - it is new (7 digits) OrderCode.
                OrderCode = int.Parse(_itemNumber.Substring(0, 5));
                BatchCode = int.Parse(_itemNumber.Substring(5, 3));
                ItemCode = int.Parse(_itemNumber.Substring(8, 2));
            }
            else
            {
                //1234507890 = 12345 178 90 - old - 6th char != 0 => but 7-digits OrderCode is not suitable, see above
                //so this can be violation of a convention, ok let it be so:
                OrderCode = int.Parse(_itemNumber.Substring(0, 5));
                BatchCode = int.Parse(_itemNumber.Substring(5, 3));
                ItemCode = int.Parse(_itemNumber.Substring(8, 2));
            }
        }

        private void ItemNumberLengthEq11()
        {
            if (_isBigBatchCode(_itemNumber.Substring(0, 9)))
            {
                //15847211101 = 158472 111 01 - old (158472 is order code with big batch code)
                OrderCode = int.Parse(_itemNumber.Substring(0, 6));
                BatchCode = int.Parse(_itemNumber.Substring(6, 3));
                ItemCode = int.Parse(_itemNumber.Substring(9, 2));
            }
            else if (_itemNumber.Substring(6, 1) == "0")
            {
                //12345608901 = 123456 089 01 - old
                OrderCode = int.Parse(_itemNumber.Substring(0, 6));
                BatchCode = int.Parse(_itemNumber.Substring(6, 3));
                ItemCode = int.Parse(_itemNumber.Substring(9, 2));
            }
            else
            {
                //12345678901 = 1234567 89 01 - new
                OrderCode = int.Parse(_itemNumber.Substring(0, 7));
                BatchCode = int.Parse(_itemNumber.Substring(7, 2));
                ItemCode = int.Parse(_itemNumber.Substring(9, 2));
            }
        }
    }
}