﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="DeliveryLog2.aspx.cs" Inherits="Corpt.DeliveryLog2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Delivery Log</div>
        <div>
            Event: 
            <asp:dropdownlist  id="lstEvents"
				runat="server" CssClass="dropdown" AutoPostBack="True" onselectedindexchanged="OnEventSelectedChanged">
				<asp:ListItem Value="14" Selected="True">Sent to Avi Polish</asp:ListItem>
				<asp:ListItem Value="16">Sent to Photoscribe Engraving</asp:ListItem>
			</asp:dropdownlist>
            <asp:HyperLink ID="BatchLog" runat="server" NavigateUrl="DeliveryLogBatch.aspx">Batch Delivery Log</asp:HyperLink>
        </div>
        <div style="margin-top: 10px;font-size: small">
            <asp:DataGrid runat="server" ID="dgrLog" CellPadding="5" AllowSorting="True"  OnSortCommand="OnSortCommand">
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
            </asp:DataGrid>
        </div>

    </div>
</asp:Content>
