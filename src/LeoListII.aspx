<%@ Page Language="c#" CodeBehind="LeoListII.aspx.cs" AutoEventWireup="True" Inherits="Corpt.LeoListII" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>LeoListII</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body onload="document.Form1.txtOrderNumber.focus();">
    <form id="Form1" method="post" runat="server">
    <p>
        <asp:Label Style="z-index: 0" ID="lblDebug" runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblInfo" runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;
        <asp:Label Style="z-index: 0" ID="lblExistingReports" runat="server" CssClass="text_highlitedyellow"></asp:Label></p>
    <p style="z-index: 0">
        <asp:TextBox Style="z-index: 0" ID="txtOrderNumber" runat="server" CssClass="inputStyleCC"
            AutoPostBack="True" OnTextChanged="txtOrderNumber_TextChanged"></asp:TextBox><asp:Button
                Style="z-index: 0" ID="cmdAdd" runat="server" CssClass="buttonStyle" Text="Add"
                Width="96px" OnClick="cmdAdd_Click"></asp:Button></p>
    <p>
        <asp:ListBox Style="z-index: 0" ID="lstOrderNumber" runat="server" CssClass="text"
            Width="88px" Height="78px"></asp:ListBox>
        <asp:Button Style="z-index: 0" ID="cmdDelete" runat="server" CssClass="buttonStyle"
            Text="Delete" Width="96px" OnClick="cmdDelete_Click"></asp:Button></p>
    <p>
        <asp:Button Style="z-index: 0" ID="cmdClear" runat="server" CssClass="buttonStyle"
            Text="Clear" Width="96px" OnClick="cmdClear_Click"></asp:Button></p>
    <p>
        <asp:Button Style="z-index: 0" ID="cmdShowShortReport" runat="server" CssClass="buttonStyle"
            Text="Short Report" Width="96px" OnClick="cmdShowShortReport_Click"></asp:Button><br>
        <asp:HyperLink Style="z-index: 0" ID="lblMovedItems" runat="server" CssClass="text"
            NavigateUrl="ShowMovedItems.aspx" Visible="False" Enabled="False">Some of the items<br>have been moved<br>to different batches.<br>Click here to see the list.</asp:HyperLink>
    </p>
    <asp:DataGrid Style="z-index: 0" ID="dgPass" runat="server" CssClass="text" AutoGenerateColumns="True">
        <Columns>
            <asp:ButtonColumn ButtonType="PushButton" CommandName="btnKillPass" HeaderText="Move"
                Text="To Fail" ItemStyle-CssClass="buttonStyle345"></asp:ButtonColumn>
        </Columns>
    </asp:DataGrid>
    <p>
    </p>
    <p>
        <!-- Fail: -->
    </p>
    <p>
        <asp:DataGrid Style="z-index: 0" ID="dgFail" runat="server" CssClass="text">
            <Columns>
                <asp:ButtonColumn ButtonType="PushButton" CommandName="btnKillFail" HeaderText="Move"
                    Text="To Pass" ItemStyle-CssClass="buttonStyle345"></asp:ButtonColumn>
            </Columns>
        </asp:DataGrid><br>
        <asp:Label ID="lblKm" CssClass="text_highlitedyellow" runat="server"></asp:Label>
        <asp:DataGrid ID="dgCombined" runat="server" CssClass="text">
            <Columns>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                        Order(Yes/No)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox runat="server" Text='' Checked='<%# DataBinder.Eval(Container.DataItem, "LineChecked") %>'
                            AutoPostBack="True" OnCheckedChanged="ItemCheckBoxClicked" ID="Checkbox1"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid></p>
    <p>
        <asp:Button Style="z-index: 0" ID="cmdOrderReports" runat="server" CssClass="buttonStyle"
            Text="Order Reports" Width="96px" OnClick="cmdOrderReports_Click"></asp:Button></p>
    <p>
        <asp:HyperLink ID="lnkMiddleAspx" runat="server"  NavigateUrl="Middle.aspx">Back</asp:HyperLink></p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    </form>
</body>
</html>
