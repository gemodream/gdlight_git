using System;

namespace Corpt
{
	/// <summary>
	/// Summary description for CurrentOrder.
	/// </summary>
	public class CurrentOrder
	{
		public CurrentOrder()
		{
			//
			// TODO: Add constructor logic here
			//
			_GroupCode = 0;
			_GroupID = 0;
			_CustomerOfficeID = 0;
			_CustomerID = 0;
			_CustomerName = "";
			_CustomerCode = 0;
		}

		private int _GroupCode;
		private int _GroupID;
		private int _CustomerOfficeID;
		private int _CustomerID;
		private String _CustomerName;
		private int _CustomerCode;
		private int _GroupOfficeID;

		public int GroupOfficeID
		{
			get
			{
				return _GroupOfficeID;
			}
			set
			{
				_GroupOfficeID = value;
			}
		}

		public int GroupCode
		{
			get
			{
				return _GroupCode;
			}
			set
			{
				_GroupCode = value;
			}
		}
		public int GroupID
		{
			get
			{
				return _GroupID;
			}
			set
			{
				_GroupID = value;
			}
		}
		public int CustomerOfficeID
		{
			get
			{
				return _CustomerOfficeID;
			}
			set
			{
				_CustomerOfficeID = value;
			}
		}
		public int CustomerID
		{
			get
			{
				return _CustomerID;
			}
			set
			{
				_CustomerID = value;
			}
		}
		public String CustomerName
		{
			get
			{
				return _CustomerName;
			}
			set
			{
				_CustomerName = value;
			}
		}
		public int CustomerCode
		{
			get
			{
				return _CustomerCode;
			}
			set
			{
				_CustomerCode = value;
			}
		}		
	}
}
