using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for GradeII.
	/// </summary>
    public partial class GradeII : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (string.IsNullOrEmpty(UniqKeyFld.Text))
            {
                UniqKeyFld.Text = "" + DateTime.Now.Ticks;
            }
			Page.Title = "GSI: Grade II";

			OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
			OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

			if (!IsPostBack)
            {
	
                SetSessionData(SessionConstants.GradeMeasureControls, null);
                SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
               
                var batchNumber = Request.Params["BatchNumber"] ?? "";
                if (!string.IsNullOrEmpty(batchNumber))
                {
                    txtBatchNumber.Text = batchNumber;
                    OnLoadClick(null, null);

                }
                var itemNumber = Request.Params["ItemNumber"] ?? "";
                if (!string.IsNullOrEmpty(itemNumber))
                {
                    lstItemList.SelectedValue = itemNumber;
                    OnItemListSelectedChanged(null, null);
                }
                txtBatchNumber.Focus();
            }

            RenderControls();
            if (!IsPostBack) txtBatchNumber.Focus();
		}
        #endregion

        public override string GetPageUniqueKey()
        {
            return UniqKeyFld.Text;
        }

        #region CleanUp
        private void CleanUp()
        {
            lstItemList.Items.Clear();
            CpPathToPicture.Text = "";
            ErrPictureField.Text = "";
            itemPicture.Visible = false;
            ClearControls();
            SetSessionData(SessionConstants.GradeMeasureControls, null);
        }
        private void ClearControls()
        {
            pnlRight.Controls.Clear();
        }
        #endregion

        #region ItemNumber List 
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
		{
            
            var itemNumber = lstItemList.SelectedValue;
		    var singleItemModel = GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber);
            if (singleItemModel == null) return;
            if (singleItemModel.StateId == DbConstants.ItemInvalidStateId)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + string.Format("Item {0} is invalid", singleItemModel.FullItemNumber) + "\")</SCRIPT>");
            }
            ShowPicture(singleItemModel.Path2Picture);
            
            var myControls = new ArrayList();
            
			var lblItemNumber = new Label {CssClass = "text", Text = lstItemList.SelectedValue};
			myControls.Add(lblItemNumber);

            SetViewState(QueryUtils.GetMeasureParts(singleItemModel.ItemTypeId, this), SessionConstants.GradeMeasureParts);
            SetViewState(QueryUtils.GetMeasureValues(singleItemModel, "", this), SessionConstants.GradeMeasureValues);

            if (IgnoreCpFlag.Checked)
            {
                SetViewState(QueryUtils.GetMeasureListByAcces(singleItemModel, "", true, this), SessionConstants.GradeMeasureValuesCp);
            } else
            {
                SetViewState(QueryUtils.GetMeasureValuesCp(singleItemModel, this), SessionConstants.GradeMeasureValuesCp); 
            }
			
			CreateMeasureControls();
		}
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = dbPicture;
            ErrPictureField.Text = errMsg;

        }
        #endregion

        #region Load Button
        protected void OnLoadClick(object sender, EventArgs e)
		{
			CleanUp();

		    var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
			
			if (itemList.Count > 0)
			{
				{
					if (itemList[0].FullItemNumber.ToString().Length == 10 || itemList[0].FullItemNumber.ToString().Length == 11)
					{
						txtBatchNumber.Text = itemList[0].FullItemNumber.ToString().Substring(0, itemList[0].FullItemNumber.ToString().Length - 2);
					}
				}
			}

            lstItemList.Items.Clear();
            var gradeItemsList = new List<MeasureGradeItemModel>();
		    foreach (var singleItemModel in itemList)
		    {
		        lstItemList.Items.Add(singleItemModel.FullItemNumber);
                gradeItemsList.Add(new MeasureGradeItemModel(singleItemModel));
		    }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);
            if (lstItemList.Items.Count > 0)
            {
                lstItemList.SelectedIndex = 0;
                OnItemListSelectedChanged(null, null);
            }
		}
        #endregion

        #region Create Controls 
        private void CreateMeasureControls()
		{
            //-- Resit Controls
			var myControls = new ArrayList();

			//One Panel Per Part
			var partsList = GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel>;
            if (partsList == null) return;

            var measureCpList = GetMeasuresCpFromView();
            if (measureCpList == null) return;

            var enumMeasureList = GetMeasureEnumsFromView();
		    
            foreach (var itemPartTypeModel in partsList)
			{
				var pnl = new Panel();
                
                var l = new Label { CssClass = "control-label", Text = itemPartTypeModel.PartName + "<br />" };
			    l.Font.Bold = true;
			    l.Font.Size = 13;
			    l.ForeColor = Color.FromArgb(83, 119, 169);
				pnl.Controls.Add(l);
			    
                var measureCpByPart = measureCpList.FindAll(m => m.PartId == itemPartTypeModel.PartId);
                measureCpByPart.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName));
				foreach (var measureCpModel in measureCpByPart)
				{
                    var lblMeasureName = new Label { CssClass = "control-label", Text = measureCpModel.MeasureName + " : ", Width = 150};  
				    pnl.Controls.Add(lblMeasureName);
					
				    var webControl = MeasureToControl(measureCpModel, itemPartTypeModel.PartId, measureCpModel.MeasureId,
				                                      enumMeasureList);
                    if (webControl != null)
                    {
                        pnl.Controls.Add(webControl);
                    }
                    if (lblMeasureName.Text == "Shape (cut) : ")
                    {
                        var hp = new HyperLink
                        {
                            Text = "View", 
                            NavigateUrl = GetShapePicturePath(measureCpModel, itemPartTypeModel.PartId, "" + measureCpModel.MeasureId)
                        };
                        pnl.Controls.Add(hp);
                    }
                    pnl.Controls.Add(new Label { Text = "<br>" });
                }

				myControls.Add(pnl);
			}
            SetSessionData(SessionConstants.GradeMeasureControls, myControls);
            RenderControls();
		}
        private WebControl MeasureToControl(MeasureValueCpModel measureCpModel, int strPartId, String strMeasureId, List<EnumMeasureModel> enumMeasureList)
        {
            const int width = 250;
            var greenColor = Color.FromArgb(204, 255, 204);
            var grayColor = Color.FromArgb(226, 226, 226);
            var measureValueList = GetMeasureValuesFromView();
            if (measureValueList == null) return null;
            var measureValueModel = measureValueList.Find(m => m.PartId == strPartId && m.MeasureId == strMeasureId);

            switch (measureCpModel.MeasureClass)
            {
                case 1: //enum
                    {
                        var lstMeasureValues = new DropDownList
                        {
                            AutoPostBack = false,
                            CssClass = "combobox",
                            BackColor = (measureValueModel != null ? greenColor : grayColor),
                            ID = "Measure." + strPartId + "." + strMeasureId,
                            Width = width + 15
                        };
                        //Check for '(Value not set)' and if not there - add
                        if (enumMeasureList.FindAll(m => m.MeasureValueMeasureId == Int32.Parse(measureCpModel.MeasureId) && m.NOrder == -10).Count == 0)
                        {
                            enumMeasureList.Add(new EnumMeasureModel(measureCpModel.MeasureId));
                        }
                        var dataSource =
                            enumMeasureList.FindAll(m => m.MeasureValueMeasureId == Int32.Parse(measureCpModel.MeasureId));
                        dataSource.Sort((c1, c2) => c1.NOrder.CompareTo(c2.NOrder));

                        lstMeasureValues.DataSource = dataSource;
                        lstMeasureValues.DataValueField = "MeasureValueID";
                        lstMeasureValues.DataTextField = "ValueTitle";
                        lstMeasureValues.DataBind();

                        //DataRow drMeeasureValue;
                        if (measureValueModel != null)
                        {
                            lstMeasureValues.SelectedIndex = lstMeasureValues.Items.IndexOf(lstMeasureValues.Items.FindByValue(measureValueModel.MeasureValueId));
                        }
                        //NB ?? TODO lstMeasureValues.AutoPostBack=false;
                        return lstMeasureValues;

                    }
                case 2: //String // StringValue
                    {
                        var measureTextBox = new TextBox
                        {
                            AutoPostBack = false,
                            CssClass = "controls",
                            BackColor = (measureValueModel != null ? greenColor : grayColor),
                            ID = "Measure." + strPartId + "." + strMeasureId,
                            Width = width
                        };
                        if (measureValueModel != null)
                        {
                            measureTextBox.Text = measureValueModel.StringValue;
                        }
                        return measureTextBox;
                    }
                case 3: //Numeric Value //MeasureValue
                    {
                        var measureTextBox = new TextBox
                        {
                            AutoPostBack = false,
                            CssClass = "controls",
                            BackColor = (measureValueModel != null ? greenColor : grayColor),
                            ID = "Measure." + strPartId + "." + strMeasureId,
                            Width = width
                        };
                        if (measureValueModel != null)
                        {
                            measureTextBox.Text = measureValueModel.MeasureValue;
                        }
                        // NB ?? TODO AutoPostBack = false
                        return measureTextBox;
                    }
                case 4:
                    {
                        var measureLabel = new Label { CssClass = "control-label" };
                        try
                        {
                            measureLabel.Text = measureValueModel.StringValue;
                        }
                        catch (Exception ex)
                        {
                            measureLabel.Text = ex.Message;
                            measureLabel.Text = "";
                        }
                        return measureLabel;
                    }
                case 5:
                    {
                        var measureLabel = new Label { Text = "Strange - case 5", CssClass = "control-label" };
                        return measureLabel;
                    }
            }
            var lblError = new Label { Text = "You dont see this.", CssClass = "control-label" };
            return lblError;

        }
        private void RenderControls()
        {
            ClearControls();
            var controls = GetSessionData(SessionConstants.GradeMeasureControls) as ArrayList;
            if (controls == null) return;
            foreach (object o in controls)
            {
                if ((o as WebControl) != null)
                {
                    pnlRight.Controls.Add(o as WebControl);
                }
            }
        }



        #endregion

        #region Shape Picture
        private string GetShapePicturePath(MeasureValueCpModel measureCpModel, int strPartId, String strMeasureId)
		{
			string itemNumber = lstItemList.SelectedValue;

            var measureValuesList = GetMeasureValuesFromView();
            if (measureValuesList == null) return "";
			var drPartValue = measureValuesList.FindAll(m => m.PartId == strPartId && m.MeasureId == strMeasureId);
			if (drPartValue.Count > 0)
			{
                var pathToShape = drPartValue[0].ShapePath2DrawingGif;
				return @"ShapeView.aspx?itemNumber=" + itemNumber + ":"+measureCpModel.PartName + @"&shapePath=Images\"+pathToShape;
			}
			return @"ShapeView.aspx?itemNumber=" + itemNumber + @"&shapePath=Images\Shapes\Test\NotShape.gif";

		}
        #endregion

        #region Get Data From View
        
        //-- Measure Values
        private List<MeasureValueModel> GetMeasureValuesFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
        }

        //-- Measure Enums
        private List<EnumMeasureModel> GetMeasureEnumsFromView()
        {
            return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
        }
        
        //-- Loading Measures Description
        private List<MeasureValueCpModel> GetMeasuresCpFromView() 
        {
            return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
        }
        
        //-- Loading Item Numbers
        private List<SingleItemModel> GetItemNumbersFromView()
        {
            return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
        }

        #endregion

        #region Save Button
        protected void OnSaveClick(object sender, EventArgs e)
		{
            var controls = GetSessionData(SessionConstants.GradeMeasureControls) as ArrayList;
            var enumsMeasure = GetMeasureEnumsFromView();

            var itemList = GetItemNumbersFromView();
            if (itemList.Count == 0) return;
            
            var itemModel = itemList.Find(m => m.FullItemNumber == lstItemList.SelectedValue);
            if (enumsMeasure.Count == 0 || itemModel == null) return;

            var result = new List<MeasureValueModel>();
            if (!CheckNumericData(controls)) return;

            foreach (WebControl wc in controls)
			{
                if (wc.GetType() != typeof(Panel)) continue;
				foreach(WebControl iwc in wc.Controls)
				{
				    var myControlUniqueId = iwc.ID;
					if(myControlUniqueId != null && myControlUniqueId.StartsWith("Measure"))
					{
					    //This is our control that has our info.
						var measureId = myControlUniqueId.Split('.')[2];
						var partId = Convert.ToInt32(myControlUniqueId.Split('.')[1]);
                        var cpMeasures = GetMeasuresCpFromView();
                        if (cpMeasures == null) continue;
						
                        var cpMeasure = cpMeasures.FindAll(m => m.MeasureId == measureId && m.PartId == partId);
                        var prevValue = GetMeasureValuesFromView().Find(m => m.PartId == partId && m.MeasureId == measureId && m.ForItem.FullItemNumber == itemModel.FullItemNumber);
                        foreach (MeasureValueCpModel cp in cpMeasure)							
						{
						    
							int measureClass = cp.MeasureClass;
							switch (measureClass)
							{
								case 1: //enum
								{
									try
									{
										var measureValueId = int.Parse(((DropDownList) iwc).SelectedItem.Value);
                                        var prevValueId = prevValue == null ? "" : prevValue.MeasureValueId;
                                        if (measureValueId > 0 && ("" + measureValueId) != prevValueId)
										{
                                            result.Add(new MeasureValueModel
                                            {
                                                PartId = partId, 
                                                MeasureId = measureId, 
                                                MeasureValueId = "" + measureValueId,
                                                MeasureValue = null, 
                                                StringValue = null,
                                                BatchId = itemModel.NewBatchId,
                                                ItemCode = itemModel.NewItemCode,
                                                MeasureClass = MeasureModel.MeasureClassEnum
                                            });
										}
									}
									catch(Exception ex)
									{
                                           Console.WriteLine(ex.Message); 
									}									
									break;
								}
								case 2: //String // StringValue
								{
                                    var prevStringValue = prevValue == null ? "" : prevValue.StringValue.Trim();
                                    var nowStringValue = ((TextBox)iwc).Text.Trim();
                                    if (nowStringValue != prevStringValue)
									{
                                        result.Add(new MeasureValueModel
                                        {
                                            PartId = partId,
                                            MeasureId = measureId,
                                            MeasureValueId = null,
                                            MeasureValue = null,
                                            StringValue = nowStringValue,
                                            BatchId = itemModel.NewBatchId,
                                            ItemCode = itemModel.NewItemCode,
                                            MeasureClass = MeasureModel.MeasureClassText
                                        });
									}
									break;
								}
								case 3: //Numeric Value //MeasureValue
						        {
						            var textBox = iwc as TextBox;
                                    if (textBox == null) break;
                                    var prevNumValue = prevValue == null ? "0" : prevValue.MeasureValue;
                                    var nowNumValue = textBox.Text.Trim().Length == 0 ? "0" : textBox.Text.Trim();
                                    
                                    try
									{
                                        if (nowNumValue != prevNumValue)
										{
                                            var val = float.Parse(nowNumValue);
                                            result.Add(new MeasureValueModel
                                            {
                                                PartId = Convert.ToInt32(partId),
                                                MeasureId = measureId,
                                                MeasureValueId = null,
                                                MeasureValue = "" + val,
                                                StringValue = null,
                                                BatchId = itemModel.NewBatchId,
                                                ItemCode = itemModel.NewItemCode,
                                                MeasureClass = MeasureModel.MeasureClassNumeric
                                            });

										}
									}
									catch(Exception ex)
									{
									    textBox.BorderColor = Color.Red;
									    textBox.ToolTip = ex.Message;
									}
									break;
								}
								case 4:
								{
									break;
								}
								case 5:
								{
									break;
								}
							}
						}
					}
				}
																	  
			}
            if (result.Count == 0) return;
		    
            cmdSave.Enabled = false;
            
            //check if there any rows in dtNewMeasureValues and push them to the db.
		    QueryUtils.SaveNewMeasures(result, this);

            QueryUtils.SetEstimatedValues(itemModel, this);

			if(!CheckForSarinDiscrepancy(result))
			{
				//Sarin weight did not match.
				// mark the item in the db
                var msg = "Sarin weight did not match!";
                itemModel.StateId = DbConstants.ItemInvalidStateId;
                QueryUtils.MarkItemInvalid(itemModel, this);
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");

			}
			else
			{
				itemModel.StateId=11;
			}
			OnItemListSelectedChanged(sender,e);
		    cmdSave.Enabled = true;
		}
        private bool CheckForSarinDiscrepancy(List<MeasureValueModel> newValueList)
        {
            var fullList = GetMeasureValuesFromView();
            if (fullList == null || newValueList.Count == 0) return true;

            var sarinMeasures = fullList.FindAll(m => m.MeasureId == "6");
            if (sarinMeasures.Count == 0) return true;

            Console.Write("Have SARIN Will Travel.");
            foreach (var sarinModel in sarinMeasures)
            {
                var measure4List = newValueList.FindAll(m => m.PartId == sarinModel.PartId && m.MeasureId == "4");
                var sw = (!string.IsNullOrEmpty(sarinModel.MeasureValue) ? double.Parse(sarinModel.MeasureValue) : double.Parse("0"));
                foreach (var valueModel in measure4List)
                {
                    var mw = (!string.IsNullOrEmpty(valueModel.MeasureValue) ? double.Parse(valueModel.MeasureValue) : double.Parse("0"));
                    if (Math.Abs(mw - sw) > 0.011)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        private bool CheckNumericData(ArrayList controls)
        {
            var errs = 0;
            foreach (WebControl wc in controls)
            {
                if (wc.GetType() != typeof(Panel)) continue;
                foreach (WebControl iwc in wc.Controls)
                {
                    var myControlUniqueId = iwc.ID;
                    if (myControlUniqueId != null && myControlUniqueId.StartsWith("Measure"))
                    {
                        var measureId = myControlUniqueId.Split('.')[2];
                        var partId = Convert.ToInt32(myControlUniqueId.Split('.')[1]);
                        var cpMeasures = GetMeasuresCpFromView();
                        if (cpMeasures == null) continue;
                        var cpMeasure = cpMeasures.FindAll(m => m.MeasureId == measureId && m.PartId == partId);
                        foreach (MeasureValueCpModel cp in cpMeasure)
                        {
                            var measureClass = cp.MeasureClass;
                            if (measureClass != 3) continue;
                            var textBox = iwc as TextBox;
                            if (textBox.Text.Trim() == "") continue;
                            try
                            {
                                var val = float.Parse(textBox.Text.Trim());
                            }
                            catch (Exception x)
                            {
                                textBox.BorderStyle = BorderStyle.Solid;
                                textBox.ToolTip = x.Message;
                                textBox.BorderColor = Color.Red;
                                errs++;
                            }
                        }
                    }
                }
            }
            return errs == 0;
        }


        #endregion

	}
}
