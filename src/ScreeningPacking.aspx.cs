﻿using Corpt.Models;
using Corpt.Utilities;
using System;
using System.Data;
using System.Linq;

namespace Corpt
{
    public partial class ScreeningPacking : System.Web.UI.Page
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");

			this.Form.DefaultFocus = txtOrderCode.ClientID;
			this.Form.DefaultButton = btnLoad.UniqueID;
		}
		#endregion

		#region Other Events
		protected void btnLoad_Click(object sender, EventArgs e)
		{
			//Get Order and memo details
			var orderModel = QueryUtils.GetOrderInfo(txtOrderCode.Text.Trim(), this);

			if (orderModel == null)
			{
				var message = string.Format("Order {0} not found.", txtOrderCode.Text.Trim());
				lblInvalidLabel.Text = message;
				tblDetail.Visible = false;
				return;
			}

			var cpList = QueryUtils.GetOrderCp(orderModel, this);
			if (cpList.Count(cp => cp.CustomerProgramName == "TND") == 0)
			{
				lblInvalidLabel.Text = "Customer Program Name TND not found.";
				tblDetail.Visible = false;
				return;
			}
			DataSet ds = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			if (ds.Tables[0].Rows.Count == 0)
			{
				lblInvalidLabel.Text = "Order is not associated with Synthetic Screening";
				return;
			}
			else
			{
				lblNoofItems.Text = ds.Tables[0].Rows[0]["TotalQty"].ToString();
				txtTotalPassItemsPerOrder.Text = ds.Tables[1].Compute("Sum(TesterPassItems)", string.Empty).ToString();
                txtTotalFailItemsPerOrder.Text = ds.Tables[1].Compute("Sum(TesterFailItems)", string.Empty).ToString();
                txtTotalFaildStonePerOrder.Text = ds.Tables[1].Compute("Sum(TesterFailedStones)", string.Empty).ToString();
            }
			DataSet dsBatches = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
			DataTable dtBatch = dsBatches.Tables[1].Select("StatusID = MIN(StatusID)").CopyToDataTable();
			if (ds.Tables[0].Rows.Count >= 1)
			{
				int StatusID = Convert.ToInt16(dtBatch.Rows[0]["StatusID"].ToString());
				if (StatusID == (int) SynhteticOrderStatus.TestingComplete)
				{
					tblDetail.Visible = true;
					btnSave.Visible = true;
					btnClear.Visible = true;
					lblInvalidLabel.Text = "";
                    txtBlueBagsPerOrder.Text = "0";
                    txtBillingInfo.Text = "";
                    txtComment.Text = "";
                }
				else if (StatusID <= (int) SynhteticOrderStatus.ScreeningComplete)
				{
					lblInvalidLabel.Text = "Testing of all batches of this order is not completed";
					tblDetail.Visible = false;
					btnSave.Visible = false;
					btnClear.Visible = false;
				}
                else if (StatusID == (int)SynhteticOrderStatus.PackingFinished)
                {
                    tblDetail.Visible = true;
                    btnSave.Visible = true;
                    btnClear.Visible = true;
                    lblInvalidLabel.Text = "";
                    txtBlueBagsPerOrder.Text = ds.Tables[0].Rows[0]["PackingTotalBlueBagsPerOrder"].ToString();
                    txtBillingInfo.Text = ds.Tables[0].Rows[0]["PackingBillingInfo"].ToString();
                    txtComment.Text = ds.Tables[0].Rows[0]["PackingComment"].ToString();
                }
                else
				{
					lblInvalidLabel.Text = "Order packing is already completed.";
					tblDetail.Visible = false;
					btnSave.Visible = false;
					btnClear.Visible = false;
				}
			}
			else
			{
				lblInvalidLabel.Text = "Items not found";
				tblDetail.Visible = false;
			}
		}
		#endregion

		#region Save Button
		protected void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (Convert.ToInt32(txtTotalPassItemsPerOrder.Text.ToString().Trim()) > Convert.ToInt32(lblNoofItems.Text.ToString().Trim()))
				{
					lblInvalidLabel.Text = "Pass item should not be more then batch total";
					return;
				}
				else if ( txtBlueBagsPerOrder.Text.ToString().Trim() == "")
				{
					lblInvalidLabel.Text = "Please enter number of blue bags per order";
					txtBlueBagsPerOrder.Focus();
					return;
				}

				SyntheticBatchModel objBatch = new SyntheticBatchModel();
				objBatch.OrderCode = Utils.ParseOrderCode(txtOrderCode.Text);
				
				objBatch.PackingTotalItemsFailPerOrder = Convert.ToInt32(txtTotalFailItemsPerOrder.Text.ToString().Trim());
				objBatch.PackingTotalStonesFailPerOrder = Convert.ToInt32(txtTotalFaildStonePerOrder.Text.ToString().Trim());
                objBatch.PackingTotalBlueBagsPerOrder = Convert.ToInt32(txtBlueBagsPerOrder.Text.ToString().Trim());              
				objBatch.PackingBillingInfo = txtBillingInfo.Text.ToString().Trim();
                objBatch.PackingComment = txtComment.Text.ToString().Trim();
                objBatch.StatusID = (int)SynhteticOrderStatus.PackingFinished;
				objBatch.PackerID = Convert.ToInt32(this.Session["ID"].ToString());
				bool result = SyntheticScreeningUtils.SetSyntheticOrderPacking(objBatch, this);

				///History Entry
				DataSet dsBatches = SyntheticScreeningUtils.GetSyntheticOrderByOrderCode(Convert.ToInt32(txtOrderCode.Text), this);
				for (int i = 0; i < dsBatches.Tables[1].Rows.Count; i++)
				{
					SyntheticOrderHistoryModel objSyntheticOrder = new SyntheticOrderHistoryModel();
					objSyntheticOrder.OrderCode = Utils.ParseOrderCode(txtOrderCode.Text);
					objSyntheticOrder.BatchCode = Convert.ToInt32( dsBatches.Tables[1].Rows[i]["BatchCode"].ToString());
					objSyntheticOrder.StatusId = (int)SynhteticOrderStatus.PackingFinished;
					objSyntheticOrder.ItemQty = Convert.ToInt32(lblNoofItems.Text.ToString().Trim());
					objSyntheticOrder.AssignedTo = Convert.ToInt32(this.Session["ID"].ToString());
					string historymsg = SyntheticScreeningUtils.SetSyntheticOrderHistory(objSyntheticOrder, this);
				}
				if (result == true)
				{
					//Clear();
					lblInvalidLabel.Text = "";
					PopupInfoDialog("Packing save successfully", false);
				}
			}
			catch (Exception ex)
			{
				lblInvalidLabel.Text = ex.Message.ToString();
			}
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			Clear();
		}
		public void Clear()
		{
			//txtItemwithFailedStonePerOrder.Text = "0";
			txtBlueBagsPerOrder.Text = "0";
			//txtTotalPassItemsPerOrder.Text = "0";
			//txtTotalFaildStonePerOrder.Text = "0";
			txtComment.Text = "";
			txtBillingInfo.Text = "";
			lblInvalidLabel.Text = "";
		}
		#endregion

		#region Popup Dialog

		protected void OnInfoCloseButtonClick(object sender, EventArgs e)
		{

		}

		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion
	}
}