﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class VvAdmin1 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: VV Registration";
        }

        #region Search By Report NUmber
        protected void OnSearchClick(object sender, ImageClickEventArgs e)
        {
            if (string.IsNullOrEmpty(ReportNumberFld.Text.Trim()))
            {
                PopupInfoDialog("Report Number is empty", true);
                return;
            }
            var items = new List<CustomerInfoModel>();
            try
            {
                items = QueryUtils.SearchUsersByReportNumber(ReportNumberFld.Text.Trim(), this);
            } catch (Exception ex)
            {
                PopupInfoDialog(ex.Message, true);
                return;
            }
             
            SetViewState(items, SessionConstants.VvUsers);
            UsersGrid.DataSource = items.Count == 0 ? null : items;
            UsersGrid.DataBind();
            if (items.Count == 0)
            {
                PopupInfoDialog(string.Format("GSI # {0} is not registered with VV", ReportNumberFld.Text), false);
            }
        }
        #endregion

        protected void OnEditUserCommand(object source, DataGridCommandEventArgs e)
        {
            var id = "" + UsersGrid.DataKeys[e.Item.ItemIndex];
            var items = GetViewState(SessionConstants.VvUsers) as List<CustomerInfoModel> ??
                        new List<CustomerInfoModel>();
            var user = items.Find(m => m.Id == id);
            if (user == null) return;
            UserIdFld.Value = id;
            OldPasswordFld.Text = user.Password;
            NewPasswordFld.Text = "";
            ChangePwdPopupExtender.Show();
            NewPasswordFld.Focus();
        }

        protected void OnNewUserClick(object sender, EventArgs e)
        {
            Response.Redirect("NewVVActivation.aspx");
        }

        protected void OnChangePwdOkClick(object sender, EventArgs e)
        {
            var msg = QueryUtils.UpdateVvPassword("" + UserIdFld.Value, OldPasswordFld.Text, NewPasswordFld.Text, this);
            if (string.IsNullOrEmpty(msg))
            {
                OnSearchClick(null, null);
                return;
            }
            PopupInfoDialog(msg, true);
            ChangePwdPopupExtender.Show();

        }
        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    }
}