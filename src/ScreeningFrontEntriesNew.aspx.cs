﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using System.Collections;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;
using System.Net;

namespace Corpt
{
    public partial class ScreeningFrontEntriesNew : System.Web.UI.Page
    {
        public string memoNum = null, poNum = null, skuName = null, style = null;
        public string totalQty = null, bagQty = null, retailer = null;
        string loginName = null;
        public enum ResponseEndEnum //alex
        {
            EndResponseEndResponse,
            ContinueResponse
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
            if (IsPostBack)
                return;
            if (false/*Session["ID"].ToString()!="10"*/)
                Response.Redirect("Login.aspx");
            btnPrint.Enabled = false;
            btnPrint1.Enabled = false;
            CustomerCodeList.Visible = false;
            CustomerIdList.Visible = false;
            txtCustomerName.Visible = false;
            //txtCustomerCode.Visible = false;
           LoadCustomers();
            //this.Controls.Add(btnPrint1);
            // ClearText();
            //SyntheticScreeningModel data = new SyntheticScreeningModel();
            //data = (SyntheticScreeningModel)Session["ScreeningData"];
            //txtSku.Text = data.SKUName;
            //txtCustomerCode.Text = data.CustomerName;
        }//Page_Load

        private void LoadCustomers()
        {
            if (CustomerList2.Items.Count > 0)
                return;
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            CustomerList2.Items.Clear();
            
            foreach (var items in customers)
            {
                CustomerList2.Items.Add(items.CustomerName);
            }
            foreach (var items in customers)
            {
                CustomerIdList.Items.Add(items.CustomerId);
            }
            CommonPage commonPage = new CommonPage();
            commonPage.SetViewState(customers, SessionConstants.CustomersList);
            
            CustomerList2.DataSource = customers;
            CustomerList2.DataBind();
            CustomerList2.SelectedIndex = 0;
            CustomerCodeList.DataSource = customers;
            CustomerCodeList.DataBind();
            CustomerCodeList.SelectedIndex = 0;
            CustomerIdList.DataSource = customers;
            CustomerIdList.DataBind();
            CustomerIdList.SelectedIndex = 0;
            
        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
           
            var customer = CustomerList2.Items.FindByValue(CustomerList2.SelectedValue);
            if (customer != null)
            {
                CustomerCodeList.SelectedIndex = CustomerList2.SelectedIndex;
                txtCustomerID.Value = CustomerIdList.Items[CustomerCodeList.SelectedIndex].Value;
                CustomerIdList.SelectedIndex = CustomerList2.SelectedIndex;

                //CustomerList2.SelectedValue = CustomerList2.SelectedValue;
            }
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            ListItem item = CustomerList2.Items.FindByText(txtCustomerCode1.Text);
            if (item != null)
                CustomerList2.SelectedValue = item.Value;
            /*

            var filterText = txtCustomerCode1.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            CustomerList2.DataSource = filtered;
            CustomerList2.DataBind();
            */
            //if (filtered.Count == 1)
            //{
                //CustomerList2.SelectedValue = filtered[0].CustomerId;
            //}
            //else
            //{
            //    ResetCpList();
            //}
            
        }

        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);

        }

        private List<CustomerModel> GetCustomersFromView()
        {
            CommonPage commonPage = new CommonPage();
            return commonPage.GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }
        protected void OnLoadClick(object sender, EventArgs e)
        {
            ClearText("requestId");
            LoadExecute();
        }

        protected void OnCustCodeClick(object sender, EventArgs e)
        {
            bool codeFound = false;
            for (int i=0; i<=CustomerCodeList.Items.Count-1; i++)
            {
                if (txtCustomerCode1.Text == CustomerCodeList.Items[i].Text)
                {
                    CustomerList2.SelectedIndex = i;
                    CustomerIdList.SelectedIndex = i;
                    txtCustomerID.Value = CustomerIdList.Items[i].Value;
                    codeFound = true;
                    break;
                    /*
                    codeFound = true;
                    for (int j=0; j<= CustomerList2.Items.Count-1; j++)
                    {
                        if (CustomerList2.Items[j].Text.Contains(txtCustomerCode1.Text))
                        {
                            CustomerList2.SelectedIndex = j;
                            nameFound = true;
                            return;
                        }
                        
                    }
                    break;
                    */
                }
            }
            if (codeFound == false)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer Code not found');", true);
                return;
            }
            //var filterText = txtCustomerCode1.Text.Trim().ToLower();
            //var filtered = GetCustomersFromView(filterText);
            //CustomerList2.DataSource = filtered;
            //CustomerList2.DataBind();
            //if (filtered.Count == 1)
            //{
            //CustomerList2.SelectedValue = filtered[0].CustomerId;
            //string tmpcode = txtCustomerCode1.Text;
            //ClearText("all");
            //txtCustomerCode1.Text = tmpcode;
            //txtCustomerCode1.Text = CustomerList2.SelectedValue;
            //LoadExecute();
            /*
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Value, this);
            if (ds != null && ds.Tables[0].Rows.Count != 0)
            {
                DataRow custDr = ds.Tables[0].Rows[0];
                txtCustomerName.Value = custDr["CompanyName"].ToString();
                txtCustomerID.Value = custDr["CustomerID"].ToString();
            }
            */
            SetMessengersList();

        }

        private void LoadExecute()
        {
            string requestId = RequestIDBox1.Text;
            if (requestId == null || requestId == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter request ID properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticCustomerEntries(requestId, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this request ID');", true);
            else
                PopulateFrontScreenRequestID(ds);

            //Response.Redirect("ScreeningFrontEntries.aspx");
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            //txtCustomerID.Value = @"c:\temp\abc.pdf";
            return;
        }
        protected void btnSavefront_Click(object sender, EventArgs e)
        {
            btnSave.UseSubmitBehavior = false; //alex
            btnSave.OnClientClick = "_spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true; ";
            bool multibatch = false;
            if (txtMemoNum.Value == "" || txtMemoNum.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Value == "" || txtQuantity.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode1.Text == "" || txtCustomerCode1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            //if (txtCustomerName.Value == "" || txtCustomerName.Value == null)
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Name');", true);
            //    return;
            //}
            if (messengerListBox.SelectedItem.Text == "" || messengerListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Messenger');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string[] batchMemos = null;
            if (Textarea1.Value != null && Textarea1.Value != "")
            {
                string memoStr = Textarea1.Value.Replace("\r", "");
                batchMemos = memoStr.Split('\n');
                int batchTotal = 0;
                foreach (string batch in batchMemos)
                {
                    string[] batchElements = batch.Split('/');
                    string batchItemsNumber = batchElements[1];
                    batchTotal += Convert.ToInt16(batchElements[1]);
                }
                int total = Convert.ToInt16(txtQuantity.Value);
                if (total != batchTotal)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Total quantity does not match with total batches.');", true);
                    return;
                }
                multibatch = true;
            }
            string rid = null;
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int currentOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int itemsQuantity = Convert.ToInt32(txtQuantity.Value);
            int serviceTypeId = 7;
            string memo = txtMemoNum.Value;
            string specialInstruction = specialInstructionsListBox.SelectedItem.Text;
            int personCustomerOfficeId = authorOfficeId;
            int personCustomerId = Convert.ToInt32(txtCustomerID.Value);
            int? personId = GSIAppQueryUtils.GetPersonID(txtCustomerCode1.Text, messengerListBox.SelectedItem.Text, this);
            int customerOfficeId = authorOfficeId;
            int customerId = Convert.ToInt32(txtCustomerID.Value);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.CreateScreeningNewOrder(authorId, authorOfficeId, currentOfficeId, groupOfficeId,
                itemsQuantity, serviceTypeId, memo, specialInstruction,
                personCustomerOfficeId, personCustomerId, personId, customerOfficeId,
                customerId, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order not created.');", true);
            else
            {
                string sGroupCode = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                txtOrder1.Text = sGroupCode;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Group Code " + sGroupCode + " created.');", true);
                string groupId = null;
                DataSet dsGroupId = new DataSet();
                dsGroupId = GSIAppQueryUtils.GetGroupId(sGroupCode, this);
                if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
                {
                    groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                bool added = GSIAppQueryUtils.SetMemoNumber(groupId, memo, authorId, authorOfficeId, currentOfficeId, this);
                if (!added)
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo was not added.');", true);
                else
                {
                    bool synthOrderAdded = true;
                    synthOrderAdded = GSIAppQueryUtils.AddOrderToSynthScreening(sGroupCode, specialInstruction, txtPONum1.Value, txtQuantity.Value, txtSku1.Value,
                        txtStyle.Value, txtMemoNum.Value, txtCustomerCode1.Text, this);

                }
                //if batch area is not empty add batches
                if (batchMemos != null)
                {
                    foreach (string batch in batchMemos)
                    {
                        string[] batchElements = batch.Split('/');
                        string batchNumber = batchElements[0];
                        string batchItemsNumber = batchElements[1];
                        string customerCodeName = "Screening";
                        DataSet cpDs = GSIAppQueryUtils.GetCPInfo(customerCodeName, txtCustomerCode1.Text, this);
                        if (cpDs != null && cpDs.Tables[0].Rows.Count != 0)
                        {
                            DataRow dr = cpDs.Tables[0].Rows[0];
                            string itemTypeId = dr["ItemTypeID"].ToString();
                            string cpid = dr["CPID"].ToString();

                            string batchId = GSIAppQueryUtils.AddSyntheticBatch(authorId, authorOfficeId, currentOfficeId, groupId,
                                itemTypeId, batchItemsNumber, cpid, null, this);
                            //itemTypeId, batchItemsNumber, cpid, batchMemo, this);
                            if (batchId == null)
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Batch was not created');", true);
                            else
                            {
                                bool synthBatchAdded = GSIAppQueryUtils.AddBatchToSynthScreening(sGroupCode, specialInstruction, txtPONum1.Value, batchItemsNumber, txtSku1.Value,
                                    txtStyle.Value, txtMemoNum.Value, batchNumber, txtCustomerCode1.Text, this);
                            }
                        }
                    }
                }
                if (RequestIDBox1.Text != "" && RequestIDBox1.Text != null)
                {
                    string result = GSIAppQueryUtils.UpdateCustomerData(Convert.ToInt32(RequestIDBox1.Text), memo, Convert.ToInt32(sGroupCode),
                        Convert.ToInt32(txtCustomerCode1.Text), txtPONum1.Value,
                        txtSku1.Value, Convert.ToInt32(txtQuantity.Value), txtStyle.Value,
                        specialInstructionsListBox.SelectedItem.Text, messengerListBox.SelectedItem.Text, this);
                    if (result == "success")
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer Data was updated.');", true);
                    else if (result == "failed")
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Failed to upodate Customer Data.');", true);
                }
                btnPrint.Enabled = true;
                btnPrint1.Enabled = true;
                onOrderUpdate();
                PrintTakeInLabel();
                //PrintTwoLabels();
                // LoadExecute();
            }


        }//savefront

        protected void btnAddItem_Click(object sender, EventArgs e)
        {

        }
        protected void gvItems_RowDeleting(object sender, EventArgs e)
        {

        }
        

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearText("all");
        }//btnClear_Click

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(@"Middle.aspx");
        }
        protected void btnSearch1_Click(object sender, EventArgs e)
        {

        }
        /*
            protected void btnSave_Click(object sender, EventArgs e)
        {
            var personId = GSIAppQueryUtils.GetPersonID(txtCustomerCode.Text, messengerListBox.SelectedItem.Text, this);
            DataSet ds = new DataSet();
            //ds = GSIAppQueryUtils.CreateScreeningNewOrder();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int customerOfficeId = authorOfficeId;
            int personCustomerOfficeId = authorOfficeId;
            int personCustomerId = Convert.ToInt32(txtCustomerID.Value);



            
             [12/5/2018 5:24:06 PM]EntryBatch: PersonCustomerOfficeID_PersonCustomerID_PersonID=1_23_54; 
             CustomerOfficeID_CustomerID=1_23; VendorOfficeID_VendorID=; PersonID=; PersonCustomerOfficeID=; 
             PersonCustomerID=; GroupOfficeID=; GroupID=; GroupCode=; OrderCode=; IsIQInspected=0; IsTWInspected=2; 
             IsMemo=1; ItemsQuantity=1000; TotalWeight=; MeasureUnitID=; ServiceTypeID=7; Memo=ALEXTEST; 
             SpecialInstruction=FM; CarrierID=; CarrierTrackingNumber=; CustomerOfficeID=; CustomerID=; VendorOfficeID=; VendorID=; 
             
             [12/5/2018 5:24:06 PM]dbo.spsetEntryBatch: @AuthorId=261; @AuthorOfficeId=1; @CurrentOfficeId=1; @rId=; 
             @PersonID=54; @PersonCustomerOfficeID=1; @PersonCustomerID=23; @GroupOfficeID=; @GroupID=; @GroupCode=; 
             @OrderCode=; @IsIQInspected=0; @IsTWInspected=2; @IsMemo=1; @ItemsQuantity=1000; @TotalWeight=; @MeasureUnitID=;
             @ServiceTypeID=7; @Memo=ALEXTEST; @SpecialInstruction=FM; @CarrierID=; @CarrierTrackingNumber=; @CustomerOfficeID=1; 
             @CustomerID=23; @VendorOfficeID=; @VendorID=; 
             *
            /* alex
            loginName = (string)HttpContext.Current.Session["LoginName"];
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            bagQty = txtBagQty.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            int orderCount = IsOrderExist(txtMemoNum.Text);
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 2)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo already exists. Press UPDATE');", true);
            else if (orderCount == 0)
            {
                string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                if (custCode != null)
                {
                    string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, bagQty, retailer, this, 0, custCode, null);
                    if (status == "failed")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                        if (status == "failed")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                            bool printed = true;// GSIAppQueryUtils.PrintCustomerLabel(memoNum, poNum, skuName, totalQty, bagQty, retailer, custCode, this);
                            if (printed)
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                            else
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                            //ClearText();
                        }

                        //ClearText();
                    }
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer does not exist. Contact GSI administrator');", true);
            }
            
    }//btnSave_Click
    
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            //bagQty = txtBagQty.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            style = txtStyle.Text.Trim();
            bagQty = null;
            int orderCount = IsOrderExist(memoNum);
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 0)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo does not exist. Press ADD NEW');", true);
            else if (orderCount == 2)//save record
            {
                string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, style, retailer, this, 1, loginName, null);
                if (status == "failed")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                    string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                    bool printed = true;// GSIAppQueryUtils.PrintCustomerLabel(memoNum, poNum, skuName, totalQty, bagQty, retailer, custCode, this);
                    if (printed)
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                    //ClearText();
                }
            }
        }//btnUpdate_Click
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            memoNum = txtMemoNum.Text.Trim();
            loginName = (string)HttpContext.Current.Session["LoginName"];
            int orderCount = IsOrderExist(memoNum);
            if (orderCount > 0)
            {
                string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                DataSet ds = new DataSet();
                bool found = false;
                ds = GSIAppQueryUtils.GetSyntheticRecordByMemo(memoNum, this);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    DataRow dr;
                    dr = ds.Tables[0].Rows[0];
                    txtPONum.Text = dr["PONum"].ToString();
                    txtSku.Text = dr["SKUName"].ToString();
                    txtQuantity.Text = dr["TotalQty"].ToString();
                    //txtBagQty.Text = dr["BagQty"].ToString();
                    txtRetailer.Text = dr["Retailer"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Record found.');", true);
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error finding customer data.');", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No order found.');", true);

        }//search
        */
        protected void specialInstructionsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRetailer.Value = specialInstructionsListBox.SelectedItem.Text;
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //PrintExternalLabel();
            PrintTakeInLabel();
        }//btnPrint_Click
        protected void btnPrint1_Click(object sender, EventArgs e)
        {
            //PrintTwoLabels();
            PrintExternalLabel();
            //PrintTakeInLabel();
        }//btnPrint_Click
         /* alex
         ClearText();
         txtCreateDateSearch.Value = string.Empty;
         txtToCreateDateSearch.Value = string.Empty;
         gsiOrderFilter.Value = string.Empty;
         grdScreening.PageIndex = 0;
         grdScreening.SelectedIndex = -1;
         BindGrid(ddlDestinationSearch.SelectedItem.Value.Trim());
         gvTitle.InnerText = "Screening Result - For orders having destination " + ddlDestinationSearch.SelectedItem.Value;
         alex */
        private void PrintTwoLabels()
        {
            //PrintExternalLabel();
            //PrintTakeInLabel();
            //return;
            if (txtOrder1.Text == "" || txtOrder1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Value == "" || txtMemoNum.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Value == "" || txtQuantity.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode1.Text == "" || txtCustomerCode1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string groupId = null;
            var gsiOrder = txtOrder1.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_TwoLabels_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;

            var totalQ = txtQuantity.Value;
            var memo = txtMemoNum.Value;
            var custCode = txtCustomerCode1.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            DataSet dsGroupId = new DataSet();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            dsGroupId = GSIAppQueryUtils.GetGroupId(gsiOrder, this);
            if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
            {
                groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            DataSet dsGroup = new DataSet();
            dsGroup = GSIAppQueryUtils.GetGroupWithCustomer(groupId, authorId, authorOfficeId, groupOfficeId, this);
            DataRow rGroup = null;
            if (dsGroup != null && dsGroup.Tables[0].Rows.Count != 0)
                rGroup = dsGroup.Tables[0].Rows[0];
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer');", true);
                return;
            }
            string officeCode = rGroup["OfficeCode"].ToString();
            string customerName = rGroup["CustomerName"].ToString();
            string notInspectedQuantity = txtQuantity.Value;
            string address = rGroup["Address1"].ToString() + " " + rGroup["Address2"].ToString();
            string city = rGroup["City"].ToString();
            string zip = rGroup["Zip1"].ToString();
            string stateName = rGroup["USStateName"].ToString();
            string country = rGroup["Country"].ToString();
            string phone = rGroup["CountryPhoneCode"].ToString() + " " + rGroup["Phone"].ToString();
            string fax = rGroup["CountryFaxCode"].ToString() + " " + rGroup["Fax"].ToString();
            DataSet dsAuthor = GSIAppQueryUtils.GetAuthorData(groupId, authorId, groupOfficeId, authorOfficeId, this);
            if (dsAuthor == null || dsAuthor.Tables[0].Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Author');", true);
                return;
            }
            string name = dsAuthor.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsAuthor.Tables[0].Rows[0]["LastName"];
            string sPersonID = dsGroup.Tables[0].Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsGroup.Tables[0].Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsGroup.Tables[0].Rows[0]["PersonCustomerOfficeID"].ToString();
            string messenger = messengerListBox.SelectedItem.Text;
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("labels");
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");//6
                writer.WriteElementString("customerName", customerName);//7
                writer.WriteElementString("notInspectedQuantity", notInspectedQuantity);//8
                writer.WriteElementString("address", address);//9
                writer.WriteElementString("city", city);//10
                writer.WriteElementString("zip", zip);//11
                writer.WriteElementString("stateName", stateName);//12
                writer.WriteElementString("country", country);//13
                writer.WriteElementString("phone", phone);//14
                writer.WriteElementString("fax", fax);//15
                writer.WriteElementString("messenger", messenger);//16
                writer.WriteElementString("name", name);//17

                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();

                writer.WriteStartElement("label1");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");
                if (Textarea1.Value != null && Textarea1.Value != "")
                {
                    string[] batchMemos = null;
                    string memoStr = Textarea1.Value.Replace("\r", "");
                    batchMemos = memoStr.Split('\n');
                    string batchInfo = null;
                    foreach (string batch in batchMemos)
                    {
                        string[] batchElements = batch.Split('/');
                        string batchItemsNumber = batchElements[1];
                        batchInfo += batchItemsNumber + ",";
                    }
                    writer.WriteElementString("batches", batchInfo);//6
                }
                else
                    writer.WriteElementString("batches", "no batches");//6
                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            DownloadExcelFile(filename, this);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
        }//PrintTwoLabels()
        private void PrintExternalLabel()
        {
            if (txtOrder1.Text == "" || txtOrder1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Value == "" || txtMemoNum.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Value == "" || txtQuantity.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode1.Text == "" || txtCustomerCode1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string groupId = null;
            var gsiOrder = txtOrder1.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_External_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;

            var totalQ = txtQuantity.Value;
            var memo = txtMemoNum.Value;
            var custCode = txtCustomerCode1.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            DataSet dsGroupId = new DataSet();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            dsGroupId = GSIAppQueryUtils.GetGroupId(gsiOrder, this);
            if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
            {
                groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            DataSet dsGroup = new DataSet();
            dsGroup = GSIAppQueryUtils.GetGroupWithCustomer(groupId, authorId, authorOfficeId, groupOfficeId, this);
            DataRow rGroup = null;
            if (dsGroup != null && dsGroup.Tables[0].Rows.Count != 0)
                rGroup = dsGroup.Tables[0].Rows[0];
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer');", true);
                return;
            }
            string officeCode = rGroup["OfficeCode"].ToString();
            string customerName = rGroup["CustomerName"].ToString();
            string notInspectedQuantity = txtQuantity.Value;
            string address = rGroup["Address1"].ToString() + " " + rGroup["Address2"].ToString();
            string city = rGroup["City"].ToString();
            string zip = rGroup["Zip1"].ToString();
            string stateName = rGroup["USStateName"].ToString();
            string country = rGroup["Country"].ToString();
            string phone = rGroup["CountryPhoneCode"].ToString() + " " + rGroup["Phone"].ToString();
            string fax = rGroup["CountryFaxCode"].ToString() + " " + rGroup["Fax"].ToString();
            DataSet dsAuthor = GSIAppQueryUtils.GetAuthorData(groupId, authorId, groupOfficeId, authorOfficeId, this);
            if (dsAuthor == null || dsAuthor.Tables[0].Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Author');", true);
                return;
            }
            string name = dsAuthor.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsAuthor.Tables[0].Rows[0]["LastName"];
            string sPersonID = dsGroup.Tables[0].Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsGroup.Tables[0].Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsGroup.Tables[0].Rows[0]["PersonCustomerOfficeID"].ToString();
            string messenger = messengerListBox.SelectedItem.Text;
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");//6
                writer.WriteElementString("customerName", customerName);//7
                writer.WriteElementString("notInspectedQuantity", notInspectedQuantity);//8
                writer.WriteElementString("address", address);//9
                writer.WriteElementString("city", city);//10
                writer.WriteElementString("zip", zip);//11
                writer.WriteElementString("stateName", stateName);//12
                writer.WriteElementString("country", country);//13
                writer.WriteElementString("phone", phone);//14
                writer.WriteElementString("fax", fax);//15
                writer.WriteElementString("messenger", messenger);//16
                writer.WriteElementString("name", name);//17

                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }

            DownloadExcelFile(filename, this);
        }//PrintExternalLabel
        private void PrintTakeInLabel()
        {
            if (txtOrder1.Text == "" || txtOrder1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Value == "" || txtMemoNum.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Value == "" || txtQuantity.Value == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode1.Text == "" || txtCustomerCode1.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            var gsiOrder = txtOrder1.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_Take_In_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;
            var groupCode = txtOrder1.Text;
            var totalQ = txtQuantity.Value;
            var memo = txtMemoNum.Value;
            var custCode = txtCustomerCode1.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", groupCode);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");
                if (Textarea1.Value != null && Textarea1.Value != "")
                {
                    string[] batchMemos = null;
                    string memoStr = Textarea1.Value.Replace("\r", "");
                    batchMemos = memoStr.Split('\n');
                    string batchInfo = null;
                    foreach (string batch in batchMemos)
                    {
                        batchInfo += batch + ";";
                    }
                    writer.WriteElementString("batchInfo", batchInfo.Substring(0, batchInfo.Length - 1));//6
                }
                else
                    writer.WriteElementString("batchInfo", "no batches");//6
                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndDocument();

                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            stream.Position = 0;
            //FileStream fs = new FileStream(filePath, FileMode.Open, ileAccess.Read);
            //int length = Convert.ToInt32(fs.Length);
            //byte[] data = new byte[length];
            //fs.Read(data, 0, length);
            //fs.Close();
            using (var fs = new FileStream(filename, FileMode.CreateNew, FileAccess.ReadWrite))
            {
                stream.CopyTo(fs); // fileStream is not populated
                //int lnth = (int)(fs.length);
                byte[] data = new byte[10000];
                fs.Read(data, 0, 10000);
                //BinaryReader r = new BinaryReader(fs);
                //byte[] postArray = r.ReadBytes((int)fs.Length);
                using (var client = new WebClient())
                {
                    string webServiceUrl = @"http://localhost/dell/" + filename;
                    //client.Headers.Add("Authorization", "Basic " + "YYY");
                    var abc = client.UploadData(webServiceUrl, data);
                    client.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
                    using (var postStream = client.OpenWrite(webServiceUrl, "POST"))
                    {
                        postStream.Write(data, 0, data.Length);
                    }
                }
            }

            //WebClient client = new WebClient();

            //client.UploadFile("http://localhost/dell", FileStream);
            //var Memory = new MemoryStream();
            //Memory.Position = 0;
            //writer.Write(Memory);
            //this.Session["Memory"] = Memory;
            //DownloadExcelFile(filename, this);

        }//PrintTakeInLabel

        public void DownloadExcelFile1(string dir, String filename, Page p)
        {

            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            //p.Response.AddHeader("Refresh", "0.1");
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            //p.Response.TransmitFile(filename);
            //p.Response.End();
        }
        public void DownloadExcelFile(String filename, Page p)
        {
            WebClient client = new WebClient();
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            
            string myFile = filename;
            client.Credentials = CredentialCache.DefaultCredentials;
            using (var postStream = client.OpenWrite(@"http://localhost/dell/" + filename))
            {
                postStream.Write(msMemory.ToArray(), 0, msMemory.ToArray().Length);
            }
            // client.UploadFile(@"http://mywebserver/myFile", "PUT", myFile);
            //client.UploadFile(@"http://wg.gemscience.net/PDF", "PUT", myFile);
            client.Dispose();
            //p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
            //string redirect = "<script>location.assign(window.location);</script>";
            //p.Response.Write(redirect);
            p.Response.Clear();
            //p.Response.Buffer = true; //alex
            //MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.OutputStream.Flush();//alex
            //p.Response.OutputStream.Close();//alex
            //p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.Flush(); //alex
            //p.Response.Redirect("ScreeningFrontEntries.aspx");
            try
            {
                //SyntheticScreeningModel data = new SyntheticScreeningModel();
                //data.ScreeningID = 123;
                //data.SKUName = txtSku.Text;
                //data.CustomerName = txtCustomerName.Text;
                //Session["ScreeningData"] = data;
                // Context.Items.Add("abc", "xyz");
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("ScreeningFrontEntries.aspx", true);
                //Response.Redirect(Request.RawUrl);
                //Context.Items.Add(txtGSIOrder, "abc");
                //Server.Transfer("ScreeningFrontEntries.aspx", false);
                //string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
                //Response.Redirect(navigate);
                p.Response.End();
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
                txtOrder1.Text = "abc";

            }
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        protected void txtOrder_TextChanged(object sender, EventArgs e)
        {
            ClearText("order");
            string groupCode = txtOrder1.Text;
            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }
        protected void OnOrderClick(object sender, EventArgs e)
        {
            ClearText("order");
            string groupCode = txtOrder1.Text;
            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }

        protected void onOrderUpdate()
        {
            string groupCode = txtOrder1.Text;

            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }//onOrderUpdate
        private void PopulateFrontScreen(DataSet ds)
        {
            string batch = null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr.Table.Columns.Contains("Batch"))
                    batch = dr["Batch"].ToString();
                if (batch == null || batch == "")
                {
                    if (!dr.IsNull("Memo"))
                        txtMemoNum.Value = dr["Memo"].ToString();
                    if (!dr.IsNull("PONum"))
                        txtPONum1.Value = dr["PONum"].ToString();
                    if (!dr.IsNull("TotalQty"))
                        txtQuantity.Value = dr["TotalQty"].ToString();
                    //if (!dr.IsNull("BagQty"))
                    //    txtBagQty.Text = dr["BagQty"].ToString();
                    if (!dr.IsNull("SKUName"))
                        txtSku1.Value = dr["SKUName"].ToString();
                    if (!dr.IsNull("StyleName"))
                        txtStyle.Value = dr["StyleName"].ToString();
                    if (ds.Tables[0].Columns.Contains("Destination"))
                    {
                        if (!dr.IsNull("Destination"))
                        {
                            string retailer = dr["Destination"].ToString();
                            for (int i = 0; i <= specialInstructionsListBox.Items.Count - 1; i++)
                            {
                                if (specialInstructionsListBox.Items[i].Value == retailer)
                                {
                                    specialInstructionsListBox.SelectedIndex = i;
                                    break;
                                }

                            }
                        }
                    }
                    //specialInstructionsListBox.SelectedItem.Text = dr["Destination"].ToString();
                    //specialInstructionsListBox.Items.Add(dr["Destination"].ToString());
                    //specialInstructionsListBox.SelectedItem.Value = specialInstructionsListBox.Items[0].Value;
                    if (!dr.IsNull("GSIOrder"))
                        txtOrder1.Text = dr["GSIOrder"].ToString();
                    //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                    string custCode = null;
                    if (ds.Tables[0].Columns.Contains("VendorNum") && !dr.IsNull("VendorNum"))
                        custCode = dr["VendorNum"].ToString();
                    else if (ds.Tables[0].Columns.Contains("CustomerCode") && !dr.IsNull("CustomerCode"))
                        custCode = dr["CustomerCode"].ToString();
                    if (custCode != null)
                    {
                        txtCustomerCode1.Text = custCode;
                        for (int i=0; i<=CustomerCodeList.Items.Count-1; i++)
                        {
                            if (CustomerCodeList.Items[i].Text == txtCustomerCode1.Text)
                            {
                                CustomerList2.SelectedIndex = i;
                                CustomerIdList.SelectedIndex = i;
                                txtCustomerID.Value = CustomerIdList.Items[i].Value;
                            }
                        }
                        /*
                        DataSet custDs = new DataSet();
                        custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode1.Text, this);
                        if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                        {
                            DataRow custDr = custDs.Tables[0].Rows[0];
                            if (!custDr.IsNull("CompanyName"))
                                txtCustomerName.Value = custDr["CompanyName"].ToString();
                            if (!custDr.IsNull("CustomerID"))
                                txtCustomerID.Value = custDr["CustomerID"].ToString();
                        }
                        */
                        SetMessengersList();
                        if (ds.Tables.Count > 1)
                        {
                            string messenger = ds.Tables[1].Rows[0].ItemArray[0].ToString();
                            for (int j = 0; j <= messengerListBox.Items.Count - 1; j++)
                            {
                                if (messengerListBox.Items[j].Text == messenger)
                                    messengerListBox.SelectedIndex = j;
                            }
                        }
                    }
                    /*
                    if (!dr.IsNull("Destination"))
                    {
                        foreach (var specInstr in specialInstructionsListBox.Items)
                        {
                            if (specInstr.ToString() == dr["Retailer"].ToString())
                            {
                                specialInstructionsListBox.SelectedItem.Text = specInstr.ToString();
                                break;
                            }
                        }
                    }
                    */
                }
                else
                {
                    //string batchNum = dr["Batch"].ToString();
                    //string batchItemsNum = dr["TotalQty"].ToString();
                    //Textarea1.Value += batchNum + @"/" + batchItemsNum + "\n";
                }

                /*
                if (!dr.IsNull("CustomerCode"))
                    txtCustomerCode.Text = dr["CustomerCode"].ToString();
                DataSet custDs = new DataSet();
                custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Text, this);
                if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                {
                    DataRow custDr = custDs.Tables[0].Rows[0];
                    if (!custDr.IsNull("CompanyName"))
                        txtCustomerName.Text = custDr["CompanyName"].ToString();
                    if (!custDr.IsNull("CustomerID"))
                        txtCustomerID.Value = custDr["CustomerID"].ToString();
                }
                */
            }//foreach
            //SetMessengersList();
            /*
            DataSet messengerDs = new DataSet();
            messengerDs = GSIAppQueryUtils.GetMessengersList(txtCustomerCode.Text, this);
            if (messengerDs != null && messengerDs.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow messengerDr in messengerDs.Tables[0].Rows)
                {
                    string firstName = messengerDr["FirstName"].ToString();
                    string lastName = messengerDr["LastName"].ToString();
                    messengerListBox.Items.Add(firstName + " " + lastName);
                }
            }
            */
        }//PopulateFrontScreen

        private void PopulateFrontScreenRequestID(DataSet ds)
        {
            string batch = null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (!dr.IsNull("MemoNum"))
                    txtMemoNum.Value = dr["MemoNum"].ToString();
                if (!dr.IsNull("PONum"))
                    txtPONum1.Value = dr["PONum"].ToString();
                if (!dr.IsNull("TotalQty"))
                    txtQuantity.Value = dr["TotalQty"].ToString();
                //if (!dr.IsNull("BagQty"))
                //    txtBagQty.Text = dr["BagQty"].ToString();
                if (!dr.IsNull("SKUName"))
                    txtSku1.Value = dr["SKUName"].ToString();
                if (!dr.IsNull("StyleName"))
                    txtStyle.Value = dr["StyleName"].ToString();

                if (!dr.IsNull("Retailer"))
                {
                    string retailer = dr["Retailer"].ToString();
                    for (int i = 0; i <= specialInstructionsListBox.Items.Count - 1; i++)
                    {
                        if (specialInstructionsListBox.Items[i].Value.ToLower() == retailer.ToLower())
                        {
                            specialInstructionsListBox.SelectedIndex = i;
                            break;
                        }

                    }
                    //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                    //specialInstructionsListBox.Items.Add(dr["Retailer"].ToString());
                    //specialInstructionsListBox.SelectedItem.Value = specialInstructionsListBox.Items[0].Value;
                }
                if (!dr.IsNull("GSIOrder"))
                    txtOrder1.Text = dr["GSIOrder"].ToString();
                //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                if (!dr.IsNull("CustomerCode"))
                {
                    txtCustomerCode1.Text = dr["CustomerCode"].ToString();
                    for (int i=0; i<= CustomerCodeList.Items.Count-1; i++)
                    {
                        if (CustomerCodeList.Items[i].Text == txtCustomerCode1.Text)
                        {
                            CustomerList2.SelectedIndex = i;
                            txtCustomerID.Value = CustomerIdList.Items[i].Value;
                        }
                    }
                    /*
                    DataSet custDs = new DataSet();
                    custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Value, this);
                    if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                    {
                        DataRow custDr = custDs.Tables[0].Rows[0];
                        if (!custDr.IsNull("CompanyName"))
                            txtCustomerName.Value = custDr["CompanyName"].ToString();
                        if (!custDr.IsNull("CustomerID"))
                            txtCustomerID.Value = custDr["CustomerID"].ToString();
                    }
                    */
                }
                SetMessengersList();
                if (!dr.IsNull("Messenger"))
                {
                    string messenger = dr["Messenger"].ToString();
                    for (int i = 0; i <= messengerListBox.Items.Count - 1; i++)
                    {
                        if (messengerListBox.Items[i].Value.ToLower() == messenger.ToLower())
                        {
                            messengerListBox.SelectedIndex = i;
                            break;
                        }

                    }
                }
            }//foreach


        }//PopulateFrontScreenRequestID
        private void SetMessengersList()
        {
            DataSet messengerDs = new DataSet();
            messengerDs = GSIAppQueryUtils.GetMessengersList(txtCustomerCode1.Text, this);
            if (messengerDs != null && messengerDs.Tables[0].Rows.Count != 0)
            {
                messengerListBox.Items.Add("");
                foreach (DataRow messengerDr in messengerDs.Tables[0].Rows)
                {
                    string firstName = null, lastName = null;
                    if (!messengerDr.IsNull("FirstName"))
                        firstName = messengerDr["FirstName"].ToString();
                    if (!messengerDr.IsNull("LastName"))
                        lastName = messengerDr["LastName"].ToString();
                    messengerListBox.Items.Add(firstName + " " + lastName);
                }
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No messengers found.');", true);
        }
        private int IsOrderExist(string memoNum)
        {
            return GSIAppQueryUtils.GetScreeningByMemo(memoNum, this);
        }

        private void ClearText(string allSet)
        {
            txtMemoNum.Value = string.Empty;
            txtPONum1.Value = string.Empty;
            //txtBagQty.Text = string.Empty;
            txtQuantity.Value = string.Empty;
            txtSku1.Value = string.Empty;
            txtStyle.Value = string.Empty;
            txtCustomerCode1.Text = string.Empty;
            txtCustomerName.Value = string.Empty;
            txtCustomerID.Value = string.Empty;
            if (allSet != "requestId")
                RequestIDBox1.Text = string.Empty;
            Textarea1.Value = string.Empty;
            if (allSet != "order")
                txtOrder1.Text = string.Empty;
            specialInstructionsListBox.SelectedIndex = -1;
            messengerListBox.Items.Clear();
            CustomerList2.SelectedIndex = -1;
            CustomerIdList.SelectedIndex = -1;
            btnPrint.Enabled = false;
            btnPrint1.Enabled = false;
            
            //CustomerList2.Items.Clear();
            //CustomerCodeList.Items.Clear();
        }
    }
}