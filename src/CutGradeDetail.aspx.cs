﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CutGradeDetail : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if ((Request.QueryString["par1"] != null) || (Request.QueryString["par2"] != null) || (Request.QueryString["par3"] != null))
            {
                DisplayDetails(Request.QueryString["par1"], Request.QueryString["par2"], Request.QueryString["par3"], Request.QueryString["par4"], Request.QueryString["par5"]);
            }

        }
        private void DisplayDetails(String orderCode, String batchCode, String itemCode, String partId, String sPartName)
        {
            Page.Title = "GSI: CutGrade " + Utils.FullItemNumber(orderCode, batchCode, itemCode);
            var cutGradeList = QueryUtils.GetCutGradeDetails(orderCode, batchCode, itemCode, partId, this);
            var failedList = cutGradeList.FindAll(m => m.MeasureName == "CutGrade Type" && !m.IsPass);
            System.Collections.Generic.List<Models.CutGradeModel> badGradeList = new System.Collections.Generic.List< Models.CutGradeModel > (cutGradeList);
            
            foreach(var failModel in failedList)
            {
                cutGradeList.RemoveAll(m => m.Comment == failModel.Comment);
            }
            var myDataTable = new DataTable();
            myDataTable.Columns.Add("CG Name");
            myDataTable.Columns.Add("Measure");
            myDataTable.Columns.Add("Pass/Fail");
            myDataTable.Columns.Add("Min Value");
            myDataTable.Columns.Add("Max Value");
            myDataTable.Columns.Add("Actual Value");
            myDataTable.AcceptChanges();
            
            if (cutGradeList.Count == 0)
            {
                /*
                foreach (var cg in failedList)
                {
                    if (!cg.Comment.ToLower().Contains(" upd")&& !cg.Comment.ToLower().Contains("cutgrade ext"))
                    {
                        myDataTable.Rows.Add(new object[]
                        {
                        "Cut Grade Failed", cg.MeasureName, cg.PassFail, cg.DisplayMinValue, cg.DisplayMaxValue, cg.DisplayActualValue
                        });
                        break;
                    }
                }
                */
            }
            else
            {
                foreach (var cg in cutGradeList)
                {
                    myDataTable.Rows.Add(new object[]
                    {
                    cg.Comment, cg.MeasureName, cg.PassFail, cg.DisplayMinValue, cg.DisplayMaxValue, cg.DisplayActualValue
                    });
                }
            }
            myDataTable.AcceptChanges();

            tblCutGradeDetail.DataSource = myDataTable;
            tblCutGradeDetail.DataBind();
            if (myDataTable.Rows.Count > 0)
            {
                 tblCutGradeDetail.Visible = true;
                 InfoLabel.Text = Utils.FullItemNumber(orderCode, batchCode, itemCode) + "  (" + sPartName + ")";
             }
             else
             {
                 tblCutGradeDetail.Visible = false;
                 InfoLabel.Text = Utils.FullItemNumber(orderCode, batchCode, itemCode) + ": Cut Grade could not be calculated for this number.";
             }
             dgPanel.Visible = myDataTable.Rows.Count > 0;
             LblRowCount.Text = "" + myDataTable.Rows.Count + " rows";
        }
        
        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    if (cell.Text.IndexOf("text_highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        cell.BackColor = Color.FromArgb(252, 248, 227);
                    }
                }
            }
        }

        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            var data = (DataTable) tblCutGradeDetail.DataSource;
            ExcelUtils.ExportThrouPoi(data, "cutGrade", this);
        }
    }
}