﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="BulkUpdateAsyn.aspx.cs" Inherits="Corpt.BulkUpdateAsyn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">
            Bulk Update</div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="navbar nav-tabs">
                    <asp:Panel runat="server" ID="NavPanel">
                        <%--                <Triggers>--%>
                        <%--                    <asp:AsyncPostBackTrigger ControlID="LoadButton" EventName="Click" />--%>
                        <%--                </Triggers>--%>
                        <table>
                            <tr>
                                <td style="padding-top: 10px; padding-bottom: 5px; font-size: small; width: 400px">
                                    <asp:Panel ID="Panel" runat="server" DefaultButton="LoadButton" Width="380px">
                                        Order:
                                        <asp:TextBox runat="server" ID="OrderField" MaxLength="7" Width="100px"></asp:TextBox>
                                        Batch:
                                        <asp:TextBox runat="server" ID="BatchField" MaxLength="3" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Item Numbers" ImageUrl="~/Images/ajaxImages/search.png"
                                            OnClick="OnLoadClick" />
                                    </asp:Panel>
                                </td>
                                <td style="padding-top: 0px; font-size: small">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="UpdateButton" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:Button runat="server" ID="UpdateButton" Text="Update" OnClick="OnUpdateButtonClick"
                                                CssClass="btn btn-info" Enabled="False" />
                                            <asp:Button runat="server" ID="NextOrderButton" Text="Next Order" OnClick="OnNextOrderClick"
                                                Enabled="False" CssClass="btn btn-info" />
                                            <asp:Button runat="server" ID="UndoAllButton" Text="Undo All" OnClick="OnUndoAllButtonClick"
                                                CssClass="btn btn-info" Enabled="False" />
                                             <asp:Button runat="server" ID="ResetButton" Text="Reset" OnClick="ResetButton_OnClick"
                                                CssClass="btn btn-info"  Enabled="False"/>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                    <asp:Label ID="LoadMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; font-size: smaller; font-family: Calibri; width: 250px;">
                                    Batches with the same structure as the batch above
                                </td>
                                <td style="vertical-align: top">
                                    <asp:CheckBox ID="IgnoreCpBox" runat="server" CssClass="checkbox" Text="Ignore Customer Program"
                                        Width="220px" />
                                   <asp:CheckBox ID="AutoMeasureCpBox" runat="server" CssClass="checkbox" Text="Auto Measure Run Mode"
                                                 Checked="False"
                                                 Width="220px" OnCheckedChanged="AutoMeasureCpBox_OnCheckedChanged"/>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <div runat="server" id="InfoPanel1">
                    <table>
                        <tr>
                            <td style="vertical-align: top; width: 250px;">
                                <asp:UpdatePanel runat="server" ID="BatchTreePanel">
                                    <%--                            <Triggers>--%>
                                    <%--                                <asp:AsyncPostBackTrigger ControlID="LoadButton" EventName="Click" />--%>
                                    <%--                            </Triggers>--%>
                                    <ContentTemplate>
                                        <asp:TreeView ID="tvwItems" runat="server" ShowCheckBoxes="All" ExpandDepth="1" AfterClientCheck="CheckChildNodes();"
                                            PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeNodeCheckChanged"
                                            OnSelectedNodeChanged="OnTreeSelectedChanged" onclick="OnTreeClick(event)">
                                            <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                            <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                        </asp:TreeView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="padding-left: 20px; vertical-align: top; font-size: small;">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:UpdatePanel runat="server" ID="PartsPanel" Visible="False">
                                            <ContentTemplate>
                                                Parts<br />
                                                <asp:DropDownList runat="server" ID="PartsListField" OnSelectedIndexChanged="OnPartChanged"
                                                    AutoPostBack="True" DataTextField="PartName" DataValueField="PartId" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="MeasuresPanel" Visible="False">
                                            <ContentTemplate>
                                                Measures<br />
                                                <asp:DropDownList runat="server" ID="MeasureListField" OnSelectedIndexChanged="OnMeasureChanged"
                                                    AutoPostBack="True" DataValueField="MeasureId" DataTextField="MeasureName" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="InputControlsPanel" Visible="False">
                                            <ContentTemplate>
                                               <asp:Label runat="server" ID="NewValueLabel" Text="New Value"/>
                                                <asp:DropDownList runat="server" ID="MeasureEnumField" OnSelectedIndexChanged="OnEnumMeasureChanged"
                                                    Visible="False" AutoPostBack="True" Width="100%" DataValueField="MeasureValueId"
                                                    DataTextField="MeasureValueName" />
                                                <asp:TextBox ID="MeasureNumericField" runat="server" onkeypress="return IsOneDecimalPoint(event);"
                                                    OnTextChanged="OnNumericFieldChanged" AutoPostBack="True" Visible="False" Width="90%"
                                                    ToolTip="Numeric Value" BackColor="#CCFFCC" />
                                                <asp:TextBox ID="MeasureTextField" runat="server" Visible="False" Width="90%" OnTextChanged="OnTextFieldChanged"
                                                    AutoPostBack="True"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="InputMaxControlsPanel" Visible="False">
                                           <ContentTemplate>
                                              To<br />
                                              <asp:DropDownList runat="server" ID="MeasureEnumFieldMax" OnSelectedIndexChanged="OnEnumMeasureChangedMax"
                                                                Visible="False" AutoPostBack="True" Width="100%" DataValueField="MeasureValueId"
                                                                DataTextField="MeasureValueName" />
                                              <asp:TextBox ID="MeasureNumericFieldMax" runat="server" onkeypress="return IsOneDecimalPoint(event);"
                                                           OnTextChanged="OnNumericFieldChangedMax" AutoPostBack="True" Visible="False" Width="90%"
                                                           ToolTip="Maximum Numeric Value" BackColor="#CCFFCC" />
                                           </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div id="InvalidItemsBlock" runat="server" Visible="False">
                                            <asp:Label runat="server" CssClass="" ForeColor="Red" Font-Bold="True" Text="Invalid Items:"></asp:Label><br/>
                                            <asp:ListBox runat="server" ID="InvalidItems" DataTextField="FullItemNumberWithDotes"/>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="padding-left: 20px; vertical-align: top; font-size: small;">
                                <asp:UpdatePanel runat="server" ID="EnteredValuesPanel">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="EnteredLabel" Text="Entered Values" Visible="False"></asp:Label>
                                        <asp:DataGrid runat="server" ID="EnteredGrid" AutoGenerateColumns="False" CellPadding="5"
                                            CellSpacing="5" OnItemCommand="OnDelCommand" DataKeyField="UniqueKey">
                                            <Columns>
                                                <%--HeaderImageUrl="Images/ajaxImages/del.png" --%>
                                                <asp:ButtonColumn Text="Del" ButtonType="LinkButton" HeaderText="Del" />
                                                <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DisplayValue" HeaderText="Value"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DisplayValueMax" HeaderText="To" Visible="False"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                            <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="padding-left: 20px; vertical-align: top">
                                <asp:UpdatePanel runat="server" ID="SelectedBatchValues">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="tvwItems" EventName="SelectedNodeChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="SelectedItemNumber" Style="font-family: Cambria; font-weight: bold"></asp:Label>
                                        <asp:DataGrid runat="server" ID="dgMeasures" AutoGenerateColumns="False" CellPadding="5"
                                            CellSpacing="5">
                                            <Columns>
                                                <asp:BoundColumn DataField="PartName" HeaderText="Part Name"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="MeasureName" HeaderText="Grade"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ResultValue" HeaderText="Value"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                            <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
