﻿using System;
using System.Collections.Generic;
using System.Data;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class DownLoad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var cmd = "" + Request.QueryString[SessionConstants.DownLoadCmd];
            var par = "" + Request.QueryString["p2"];
            if (cmd == SessionConstants.DownLoadCmdGrp)
            {
                LoadGrp();
            }
            if (cmd == SessionConstants.DownLoadCmdAutoFull)
            {
                LoadFull();
            }
            if (cmd == SessionConstants.ShortReportPrettyTable)
            {
                LoadPretty();
            }
        }

        private void LoadPretty()
        {
            var table = (Session[SessionConstants.ShortReportPrettyTable] as DataTable);
            if (table == null) return;
            ExcelUtils.ExportThrouPoi(table, "" + DateTime.Now.Ticks, this);
        }

        private void LoadGrp()
        {
            //-- Check Group
            var grp = "" + Session[SessionConstants.ShortReportActiveGroup];
            if (string.IsNullOrEmpty(grp)) return;

            //-- Check ShortReportModel
            var fullList = Session[SessionConstants.ShortReportModelList] as List<ShortReportModel>;
            if (fullList == null || fullList.Count == 0) return;
            var fname = fullList[0].BatchModel.GroupCode;
            var dataTable = Utlities.GetReportViewByGroup(Int32.Parse(grp), PageConstants.DirHorizontal, this);
            if (dataTable != null)
            {
                var table = dataTable.Copy();
                var dv = new DataView {Table = table};

                // Apply sort information to the view
                var sortCol = "" + Session[SessionConstants.ShortReportSortExpression];
                if (sortCol != "" && dv.Table.Columns.Contains(sortCol))
                {
                    dv.Sort = sortCol;
                    if (Session[SessionConstants.ShortReportSortExpressionDir] != null &&
                        Session[SessionConstants.ShortReportSortExpressionDir].ToString() == "no")
                    {
                        dv.Sort += " DESC";
                    }
                }

                var result = table.Copy();
                result.Rows.Clear();
                result.AcceptChanges();
                for (var i = 0; i < dv.Count; i++)
                {
                    result.Rows.Add(dv[i].Row.ItemArray);
                    result.AcceptChanges();
                }

                ExcelUtils.ExportThrouPoi(result, fname, this);
            }
        }
        private void LoadFull()
        {
            var fullList = Session[SessionConstants.ShortReportModelList] as List<ShortReportModel>;
            if (fullList == null) return;

            var grpReportList = fullList.FindAll(model => model.DocStructGrp != 0);
            if (grpReportList.Count == 0) return;

            grpReportList.Sort((c1, c2) => c1.DocStructGrp.CompareTo(c2.DocStructGrp));

            //-- Get file name & max group
            int cnt = grpReportList[grpReportList.Count - 1].DocStructGrp;
            var fname = "FullAuto_" + grpReportList[0].BatchModel.GroupCode;

            var dss = new DataSet(fname);
            for (int i = 0; i < cnt; i++)
            {
                var table = Utlities.GetReportViewByGroup(i + 1, PageConstants.DirHorizontal, this);
                if (table != null)
                {
                    table.TableName = "" + (i + 1);
                    dss.Tables.Add(table);
                }
            }
            ExcelUtils.ExportThrouPoi(dss, fname, this);

        }
    }
}