﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="NumberGenerator.aspx.cs" Inherits="Corpt.NumberGenerator" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">Number Generator</div>
        <asp:Panel runat="server" CssClass="form-inline" DefaultButton="GenerateBtn">
            Prefix
            <asp:TextBox runat="server" ID="PrefixField"></asp:TextBox>
            # of items
            <asp:TextBox runat="server" ID="NumberItems"></asp:TextBox>

            <asp:Button runat="server" Text="Generate" ID="GenerateBtn" CssClass="btn btn-info" OnClick="OnGenerateClick"/>
            <asp:Button runat="server" Text="DownLoad" ID="DownloadBtn" CssClass="btn btn-info" OnClick="OnDownloadClick"/>
            <div><asp:Label runat="server" ID="InfoLabel" ForeColor="Red"></asp:Label></div>
            <div style="margin-top: 10px">
                <asp:DataGrid runat="server" ID="dgrNumbers" AutoGenerateColumns="False" CssClass="table table-bordered" Width="400px">
                    <Columns>
                        <asp:BoundColumn DataField="BarCode" HeaderText="Bar Code"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CombinedNumber" HeaderText="Combined Number">
                        </asp:BoundColumn>
                    </Columns>
                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                </asp:DataGrid>

            </div>
        </asp:Panel>
    </div>
    <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
        TargetControlID="NumberItems" FilterType="Numbers" />
    <asp:RequiredFieldValidator runat="server" ID="CountReq" ControlToValidate="NumberItems"
        Display="None" 
        ErrorMessage="<b>Required Field Missing</b><br />A '# of items' is required." 
        ValidationGroup="CountGroup" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="CountReqE" TargetControlID="CountReq"
        HighlightCssClass="validatorCalloutHighlight" />

</asp:Content>
