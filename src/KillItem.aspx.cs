using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for KillItem.
	/// </summary>
	public partial class KillItem : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			Response.Redirect("Middle.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			
			if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{10}$|^\d{11}$")) return;

			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			string strItemNumber = txtItemNumber.Text.Trim();
			string strGroupCode = Utils.FillToFiveChars(Utils.ParseOrderCode(strItemNumber).ToString());
			string strBatchCode = Utils.FillToFiveChars(Utils.ParseBatchCode(strItemNumber).ToString());
			string strItemCode = Utils.FillToFiveChars(Utils.ParseItemCode(strItemNumber).ToString());

			lblDebug1.Text = strGroupCode;
			lblDebug2.Text = strBatchCode;
			lblDebug3.Text = strItemCode;

			command.Parameters.Add(new SqlParameter("@CustomerCode", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BGroupState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EGroupState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BDate", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EDate", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
			command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));
			command.Parameters.Add(new SqlParameter("@ItemCode", strItemCode));
			command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));
			command.Parameters.Add(new SqlParameter("@IsNew", 1));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dtItem = new DataTable();
			da.Fill(dtItem);

			dgDebug.DataSource = dtItem;
			dgDebug.DataBind();
		}
	}
}
