﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Tracking.aspx.cs" Inherits="Corpt.Tracking" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
<style>
.text_highlitedyellow {background-color: #FFFF00}
</style>
<%--    <script type="text/javascript">--%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScrollBatch();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScrollBatch();--%>
<%----%>
<%--        });--%>
<%--        function gridviewScrollBatch() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=grdBatches.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 1--%>
<%--            });--%>
<%--        }--%>
<%--    </script>--%>
    <div class="demoarea">
    <div class="demoheading">Tracking</div>  
        <!-- Filter Panel: Customer List, DateFrom, Date To, -->
    <div class="form-inline">
        <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="FilterPanelByCustomerAndRabgeDate"
            class="form-inline">
            <!-- Customers -->
            <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                width: 60px;"></asp:TextBox>
            <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
            <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                DataValueField="CustomerId" Width="300px" Style="font-family: Arial; font-size: 12px"
                ToolTip="Customers List" />
            <!-- Date From -->
            <label style="margin-right: 5px">
                From</label>
            <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
                AutoPostBack="True" />
            <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                PopupButtonID="Image1" />
            <!-- Date To -->
            <label style="margin-left: 15px; margin-right: 5px; text-align: right">
                To</label>
            <asp:TextBox runat="server" ID="calTo" Width="100px" OnTextChanged="OnChangedDateTo"
                AutoPostBack="True" />
            <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                AlternateText="Click to show calendar" />
            <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                PopupButtonID="Image2" />
            <asp:Button runat="server" ID="LoadButton" OnClick="OnLoadClick" Text="Load Activity"
                class="btn btn-primary" Style="margin-left: 10px;" />
        </asp:Panel>
    </div>
        <asp:Panel ID="OrderPanel" runat="server" Style="font-size: small; font-weight: bold;"
            Visible="False">
            <asp:Label runat="server" Text="Orders" CssClass="control-label"></asp:Label>
            <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; padding: 2px">
                <asp:Label runat="server" ID="OrderLabel" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
            </div>
        </asp:Panel>
        <asp:Panel ID="CpPanel" runat="server" Style="width: 100%; font-size: small;
            font-weight: bold; " Visible="False">
            Customer Programs:<br />
            <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; margin-top: 10px;
                padding: 2px; border: silver thin solid; margin-top: 10px; padding: 2px">
                <asp:Label runat="server" ID="CpLabel" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
            </div>
        </asp:Panel>
        <asp:Panel ID="MemoPanel" runat="server" Style="width: 100%; font-size: small; font-weight: bold; " Visible="False">
            Memo Numbers:<br />
            <div style="overflow-x: hidden; overflow-y: auto; border: silver thin solid; 
                padding: 2px; border: silver thin solid; padding: 2px">
                <asp:Label runat="server" ID="MemoLabel" Width="100%" Wrap="True" Height="80px" Style="font-weight: normal;"></asp:Label>
            </div>
        </asp:Panel>
    <asp:Label ID="lblCurrentRows" runat="server" Style="font-family: Arial; font-size: small;
        font-weight: bold" />
    <div id="gridContainer" style="font-size: small; padding-top: 10px;" >
        <asp:DataGrid ID="grdBatches" runat="server" Enabled="True" AllowSorting="true" CellPadding="5"
            OnSortCommand="OnSortCommand" AutoGenerateColumns="True">
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
        </asp:DataGrid>
    </div>
    </div>
</asp:Content>
