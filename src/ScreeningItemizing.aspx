﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningItemizing.aspx.cs" Inherits="Corpt.ScreeningItemizing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1"></ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

            input[type="checkbox"], label {
                margin-top: 0;
                margin-bottom: 0;
                line-height: normal;
            }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }
    </style>

    <%--<script type="text/javascript">

        $().ready(function () {

            $('#<%=txtOrder.ClientID%>, #<%=txtAllocateItems.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

        });

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }

    </script>--%>

    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

        <div class="demoheading">
            Itemizing
            <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 47px"></asp:Label>
        </div>
        <table style="width: 100%;">

            <tr>
                <td style="vertical-align: top; white-space: nowrap;">
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnOrderSearch" CssClass="form-inline">
                        <asp:TextBox type="text" ID="txtOrder" MaxLength="7" runat="server" name="txtOrder"
                            placeholder="Order code" Style="width: 115px;" OnFocus="this.select();" autocomplete="off"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
                        <!-- 'Load' button -->
                        <asp:Button ID="btnOrderSearch" runat="server" class="btn btn-info btn-large" Text="Load" Style="margin-left: 10px" OnClick="btnOrderSearch_Click" ValidationGroup="BatchGroup" />
                    </asp:Panel>

                    <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrder"
                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
                        ValidationGroup="BatchGroup" />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                        HighlightCssClass="validatorCalloutHighlight" />
                    <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrder"
                        Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>six or seven numeric characters</strong>"
                        ValidationGroup="BatchGroup" />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                        HighlightCssClass="validatorCalloutHighlight" />
					<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers" TargetControlID="txtOrder"></ajaxToolkit:FilteredTextBoxExtender>
                </td>

            </tr>
        </table>

        <div id="divResult" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
				 <Triggers>
                    <asp:PostBackTrigger ControlID="btnClear" />
                </Triggers>
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; white-space: nowrap;">

                                <table>
                                    <tr>
                                        <td style="height: 35px;">Total Quantity :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotQty" runat="server" Style="height: 35px;"></asp:Label>
                                        </td>
                                    </tr>

                                    <%--                       <tr>
                                <td>Certified By
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="form-control form-control-height selectpicker"
                                        Style="padding-top: 2px; padding-bottom: 2px;">
                                    </asp:DropDownList>

                                </td>
                            </tr>

                            <tr>
                                <td style="width: 120px;">Screening Instrument</td>
                                <td>

                                    <asp:CheckBoxList ID="chkLstScreeningInstrument" runat="server" AutoPostBack="false" RepeatDirection="Horizontal" CssClass="checkboxSingle">
                                    </asp:CheckBoxList>

                                </td>
                            </tr>--%>
                                </table>

                            </td>

                        </tr>

                        <tr>

                            <td style="vertical-align: -webkit-baseline-middle;">
                                <div style="padding-left: 5px; padding-bottom: 25px;">
                                    <h5>Item allocation in batches</h5>

                                    <div>


                                        <%-- <table>
                                            <tr>
                                                <td style="vertical-align: bottom;">No. of Items Left</td>
                                                <td style="vertical-align: bottom;">Allocate Items in Batch</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtItemsLeft" runat="server" MaxLength="6" Width="100" Text="" ReadOnly="true"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtAllocateItems" runat="server" MaxLength="6" Width="100" autocomplete="off"></asp:TextBox></td>
                                                <td>
                                                    <asp:Button ID="btnAddBatch" runat="server" Text="Add Batch" CssClass="btn btn-sm btn-info" OnClick="btnAddBatch_Click" /></td>
                                            </tr>

                                        </table>--%>

                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" Style="margin-top: 5px;" CellPadding="10">

                                            <HeaderStyle HorizontalAlign="Left" />
                                            <RowStyle HorizontalAlign="Left" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Batch Code" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBatchCode" runat="server" Text='<%# Eval("BatchCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Item Qty" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAllocateItemQty" runat="server" Text='<%# Eval("AllocateItemQty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Fail Item Qty" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemQtyFail" runat="server" Text='<%# Eval("TesterFailItems") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="120">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:CommandField ShowDeleteButton="true" DeleteImageUrl="Images/delete.png" />
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div style="padding-top: 15px;">
                                    <%--<asp:Button ID="btnCreateBatches" runat="server" Text="Create Batches" CssClass="btn btn-sm btn-info" OnClick="btnCreateBatches_Click" />--%>
                                    <asp:Button ID="btnItemize" runat="server" Text="Itemize Fail Items" CssClass="btn btn-info btn-large" OnClick="btnItemize_Click" />
                                    <asp:Button ID="btnPrint" runat="server" Text="Print Batch Label" CssClass="btn btn-info btn-large" OnClick="btnPrint_Click" />
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" ClientIDMode="Static"  CssClass="btn btn-info btn-large" OnClick="btnClear_Click" />
                                </div>
                            </td>
                        </tr>

                        <%--<tr>
                    <td>
                        <div style="visibility: hidden;">
                            <asp:Label ID="lblMemo" runat="server" Style="height: 35px;"></asp:Label>
                            <asp:Label ID="lblServiceType" runat="server" Style="height: 35px;"></asp:Label>
                            <asp:Label ID="lblRetailer" runat="server" Style="height: 35px;"></asp:Label>
                        </div>
                    </td>
                </tr>--%>
                    </table>

                    <%-- Information Dialog --%>
                    <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray; margin-top: 25%;">
                        <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                            <div>
                                <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                <b>Information</b>
                            </div>
                        </asp:Panel>
                        <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
                            id="MessageDiv" runat="server">
                        </div>
                        <div style="padding-top: 10px">
                            <p style="text-align: center; font-family: sans-serif;">
                                <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
                            </p>
                        </div>
                    </asp:Panel>
                    <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
                        PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
                    </ajaxToolkit:ModalPopupExtender>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
