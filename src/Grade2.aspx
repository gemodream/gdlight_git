<%@ Page Language="c#" MasterPageFile="~/DefaultMaster.Master" CodeBehind="Grade2.aspx.cs" AutoEventWireup="True" Inherits="Corpt.Grade2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%-- IvanB 15/03 start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID ="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet"/>
</asp:Content>
<%-- IvanB 15/03 end --%>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }.text_nohighlitedyellow
        {
            background-color: white;
        }
    </style>
    <script type="text/javascript">
        /*IvanB 15/03 start*/
        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {
            $(".filtered-select").select2({
                scrollAfterSelect: true,
            });
        }
            /*IvanB 15/03 end*/
        function jsSetEditMode() {
            var doc = document.getElementById('<%= ItemValuesGrid.ClientID %>');
        }
    </script>
    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <ContentTemplate>
                <div class="demoheading">
                    Grade II
                    <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 20px"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td style="vertical-align: top; white-space: nowrap;" colspan="2">
                            <!-- 'Batch Number' textbox -->
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoadBatch" CssClass="form-inline">
                                <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="15" runat="server" name="txtOrderNumber"
                                    placeholder="Batch number" Style="width: 115px;" />
                                <!-- 'Load' button -->
                                <asp:Button ID="cmdLoadBatch" class="btn btn-info btn-small" runat="server" Text="Load"
                                    OnClick="OnLoadClick" Style="margin-left: 10px"></asp:Button>
                                <asp:CheckBox ID="IgnoreCpFlag" runat="server" CssClass="radio-inline" Text="Ignore Cp" style="margin-left: 10px;"
                                    Checked="True" />
                            </asp:Panel>
                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtBatchNumber"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtBatchNumber"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                        </td>
                        <td style="vertical-align: top">
                            <asp:Label runat="server" Text="Data For" CssClass="label" ID="DataForLabel" Visible="false"></asp:Label>
                            <!-- 'Save' button -->
                            <asp:Button ID="cmdSave" CssClass="btn btn-info btn-small" runat="server" Text="Save"
                                Style="text-align: center;margin-left:20px" OnClick="OnSaveClick" Visible="false"></asp:Button>

                        </td>
                        <td style="text-align: left; padding-right: 40px; padding-left: 20px; vertical-align: top">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0px; vertical-align: top">
                            <asp:ListBox ID="lstItemList" runat="server" Width="130px" CssClass="text" Style="vertical-align: top"
                                 AutoPostBack="True" Rows="5" OnSelectedIndexChanged="OnItemListSelectedChanged">
                            </asp:ListBox>
                            <br />
                        </td>
                        <td style="vertical-align: top;padding-right: 20px;width: 220px;">
                            <asp:TreeView ID="PartTree" runat="server" 
                                OnSelectedNodeChanged="OnPartsTreeChanged" NodeIndent="15">
                                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                            </asp:TreeView>
                        </td>
                        <td style="vertical-align: top">
                            <asp:Panel runat="server" ID="MeasurementPanel" >

                                <asp:HiddenField runat="server" ID="PartEditing" />
                                <asp:HiddenField runat="server" ID="ItemEditing" />
                                
                                <div style="overflow-y: scroll; width: 500px; height:500px; color: black; border: silver solid 1px;
                                    margin-top: 5px;" id="EditItemDiv" runat="server">
                                    <asp:DataGrid runat="server" ID="ItemValuesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                        DataKeyField="UniqueKey" OnItemDataBound="OnEditItemDataBound" GridLines="None">
                                        <Columns>
                                            <asp:BoundColumn DataField="UniqueKey" HeaderText="UniqueKey" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PartName" HeaderText="Part" ItemStyle-Width="160px"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MeasureName" HeaderText="Measure" ItemStyle-Width="140px"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MeasureClass" HeaderText="Measure Class" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    Value</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel runat="server" ID="ItemValuePanel" CssClass="form-inline" DefaultButton="EditHiddenBtn">
                                                        <asp:Button runat="server" ID="EditHiddenBtn" Style="display: none" Enabled="False" />
                                                        <asp:TextBox runat="server" ID="ValueStringFld" Width="135px" onchange="jsSetEditMode();" MaxLength="200" ontextchanged="OnTextFldChanged" AutoPostBack="true"></asp:TextBox>
                                                        <asp:TextBox runat="server" ID="ValueNumericFld" Width="135px" onchange="jsSetEditMode();" ontextchanged="OnNumericFldChanged" AutoPostBack="true"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ValueNumericExtender" runat="server" FilterType="Custom"
                                                            ValidChars="0123456789." TargetControlID="ValueNumericFld" />
                                                        <asp:DropDownList runat="server" ID="ValueEnumFld" DataTextField="ValueTitle" DataValueField="MeasureValueId"
                                                            Width="150px" onchange="jsSetEditMode();" OnSelectedIndexChanged="OnEnumMeasureChoice" AutoPostBack="true"/>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                    </asp:DataGrid>
                                </div>
                            </asp:Panel>
                        </td>
                        <td style="vertical-align: top;padding-left: 20px">
                            <asp:Label runat="server" ID="PicturePathAbbr" Width="200px"></asp:Label><br />
                            <asp:Image ID="itemPicture" runat="server" Width="150px" Visible="False"></asp:Image><br />
                            <asp:Label ID="ErrPictureField" ForeColor="Red" runat="server" Width="200px" Style="font-size: small;
                                width: 200px; word-wrap: break-word"></asp:Label><br />
                            <asp:Panel runat="server" ID="ShapePanel" CssClass="form-horizontal" Visible="False"
                                Style="padding-top: 20px">
                                <asp:Label runat="server" ID="ShapePathAbbr" Width="200px" Font-Size="small"></asp:Label><br />
                                <asp:Image ID="itemShape" runat="server" Width="180px" Visible="False"></asp:Image><br />
                                <asp:Label ID="ErrShapeField" ForeColor="Red" runat="server" Width="200px" Style="font-size: small;
                                    width: 200px; word-wrap: break-word"></asp:Label><br />
                            </asp:Panel>
                              <div id="dvDescWithComment" runat="server" visible="false">
                                <asp:Label runat="server" ID="lblExtDesc"  style="font-weight: bold;font-size: larger;" >Extended Description</asp:Label>
                                <asp:HiddenField ID="hdnExtDesc" runat="server" /><br /> 
                                <asp:TextBox ID="txtExtDesc" runat="server" TextMode="MultiLine" MaxLength="2000" style="height:185px;width:300px;" ></asp:TextBox><br />
                                <asp:Label runat="server" ID="lblExtDescLimit"  style="font-weight: normal;font-size: larger;color:red;" Visible="false" >Max 2000 character limit exceed.</asp:Label><br />

                                <asp:Label runat="server" ID="lblExtComment"  style="font-weight: bold;font-size: larger;" >Extended External Comment</asp:Label>
                                <asp:HiddenField ID="hdnExtComment" runat="server" /><br />
                                <asp:TextBox ID="txtExtComment" runat="server" TextMode="MultiLine" MaxLength="2000" style="height:185px;width:300px;" ></asp:TextBox><br />
                                <asp:Label runat="server" ID="lblExtCommentLimit"  style="font-weight: normal;font-size: larger;color:red;" Visible="false" >Max 2000 character limit exceed.</asp:Label><br />
                            </div>
                        </td>
                    </tr>
                </table>
                <%-- Information Dialog --%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px;display: none;border: solid 2px Gray">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <b>Information</b>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px;color: black;text-align:center"
                        id="MessageDiv" runat="server">
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif;">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small"/>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                    PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OkControlID="InfoCloseButton">
                </ajaxToolkit:ModalPopupExtender>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup"  Style="width: 430px;display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD;border: solid 1px Silver; color: black;text-align: left">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black;padding-top: 10px">Do you want to save the item changes?</div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center;font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" OnClick="OnQuesSaveNoClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" OnClick="OnQuesSaveCancelClick" class="btn btn-info btn-small"/>
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close"  Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog" ID="SaveQPopupExtender"
                    PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn" >
                </ajaxToolkit:ModalPopupExtender>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
