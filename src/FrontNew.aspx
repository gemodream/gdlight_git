﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="FrontNew.aspx.cs" Inherits="Corpt.FrontNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: #555555;
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        /*body {
			font-family: Tahoma,Arial,sans-serif;
			font-size: 75%;
		}*/

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .modalProgress {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .centerProgress {
            z-index: 1000;
            /*margin: 300px auto;*/
            margin: 20% 40%;
            padding: 10px;
            width: 70px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
            text-align: center;
            color: black;
            font-size: 9px;
        }

            .centerProgress img {
                height: 50px;
                width: 50px;
            }
    </style>
    <style type="text/css">
        .modal-header {
            background-color: #f0f0f0;
            text-align: left;
            padding: 7px 14px !important;
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
        }

        .modal-title {
            margin: -7px -13px 0px 0 !important;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.5em;
            height: 20px;
        }

        .modal-footer {
            background-color: #f0f0f0;
            padding: 17px 5px !important;
            text-align: right;
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=0);
            opacity: 0;
        }

        .modal-dialog {
            vertical-align: middle;
            background-color: white;
            border-radius: 5px;
            /*width: 1000px;*/
            background-color: white;
            border: 2px solid gray;
            box-shadow: 10px 10px 5px rgba(0,0,0,0.6);
            /*padding: 10px 10px 10px 10px;*/
        }

        .modal-body {
            padding: 5px 5px 0px 5px;
            position: relative;
            overflow: hidden;
            /*max-height: 488px !important;*/
            max-height: 730px !important;
        }

        .RadioButtonList input {
            vertical-align: top;
        }

        .DropDownListBoxStyle {
            font-size: 15px;
            font-family: Tahoma,Arial,sans-serif;
            padding: 1px 1px 1px 1px;
        }


        .radioButtonList input[type="radio"] {
            width: auto;
            float: left;
            width: 20px;
            height: 20px;
            margin-top: 7px;
            margin-left: 7px;
            margin-left: 2px;
            position: absolute;
            z-index: 1;
            font-size: 15px;
        }

        .checkboxSingle input[type="checkbox"] {
            width: auto;
            float: left;
            width: 20px;
            height: 20px;
            margin-top: 5px;
            margin-left: 7px;
            margin-left: 10px;
            position: absolute;
            z-index: 1;
            font-size: 15px;
        }

        .radioButtonList label {
            color: white;
            font-weight: bold;
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            *background-color: #2f96b4;
            background-repeat: repeat-x;
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            border-left-color: #2f96b4;
            border-right-color: #2f96b4;
            border-top-color: #2f96b4;
            border-bottom-color: #1f6377;
            /*height: 30px;*/
            padding-left: 22px;
            width: max-content;
            line-height: 25px;
            /* margin: 5px 5px 5px 5px;
            padding-top: 6px;
            padding-bottom: 6px;*/
        }

        .checkboxSingle label {
            color: white;
            font-weight: bold;
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            *background-color: #2f96b4;
            background-repeat: repeat-x;
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            border-left-color: #2f96b4;
            border-right-color: #2f96b4;
            border-top-color: #2f96b4;
            border-bottom-color: #1f6377;
            width: max-content;
            line-height: 25px;
            padding-bottom: 6px;
            padding-top: 2px;
            height: 20px;
            margin-bottom: 2px;
            margin-top: 0px;
            padding-left: 32px;
        }

        .DropDownListBoxStyle option:hover {
            font-size: 15px;
            font-family: Tahoma,Arial,sans-serif;
            padding: 1px 1px 1px 1px;
            font-weight: bold;
            background-color: #ddd;
        }

        .auto-style1 {
            height: 30px;
        }


        ul, li, body {
            margin: 0;
            padding: 0;
        }



        /* MultiView Tab Using Menu Control */



        .tabs {
            position: relative;
            top: 1px;
            z-index: 2;
        }



        .tab {
            /*

        background-image:url(images/navigation.jpg);

        background-repeat:repeat-x;

        color:White;      
		   padding:2px 10px; 
       */
            border: 1px solid black;
            padding: 2px 10px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }



        .selectedtab {
            /*background:none;

    background-repeat:repeat-x;

    color:black;*/
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            text-decoration: none;
        }

        .tabcontents {
            border: 1px solid black;
            padding: 10px;
            width: 760px;
            height: auto;
            background-color: white;
        }

        .filedSetBorder {
            border: 1px groove black !important;
            padding: 10px 10px 10px 10px;
            border-radius: 8px;
            margin: 0px 5px 5px 5px;
            float: left;
            vertical-align: top;
            /*width: -webkit-fill-available;*/
        }

            .filedSetBorder legend {
                padding-left: 7px;
                padding-right: 7px;
                margin-bottom: 0px;
                border-bottom: 0px !important;
                font-family: verdana,tahoma,helvetica;
                width: max-content;
            }

        .ajax__tab_xp .ajax__tab_body {
            font-size: 12px;
            font-family: Tahoma,Arial,sans-serif;
        }

        #master_content {
            padding-top: 20px;
        }
    </style>
    <script src="Style/signature_pad.umd.js"></script>
    <script type="text/javascript">
        function GetSignaturePadTest(dvSPad, imgSign, hdnImg, cntHide) {
            var wrapper = document.getElementById(dvSPad);
            var canvas = wrapper.querySelector("canvas");
            var signaturePad = new SignaturePad(canvas);
            var clearButton = wrapper.querySelector("[data-action=clear]");
            var undoButton = wrapper.querySelector("[data-action=undo]");
            var savePNGButton = wrapper.querySelector("[data-action=save-png]");
            var imgSignature = document.getElementById(imgSign);
            clearButton.addEventListener("click", function (event) {
                signaturePad.clear();
            });
            undoButton.addEventListener("click", function (event) {
                var data = signaturePad.toData();
                if (data) {
                    data.pop();
                    signaturePad.fromData(data);
                }
            });
            savePNGButton.addEventListener("click", function (event) {
                if (signaturePad.isEmpty()) {
                    alert("Please provide a signature first.");
                } else {
                    document.getElementById(cntHide).style.display = "none";

                    document.getElementById(cntHide.replace("foregroundElement", "backgroundElement")).style.display = "none";

                    var dataURL = signaturePad.toDataURL();
                    imgSignature.src = dataURL;
                    document.getElementById(hdnImg).value = imgSignature.src;
                }
            });
            return false;
        }
        function ClearRBList() {

            var elementRef = document.getElementById('<%= rdbMessengerTakeIn.ClientID %>');
		    var inputElementArray = elementRef.getElementsByTagName('input');

		    for (var i = 0; i < inputElementArray.length; i++) {
		        var inputElement = inputElementArray[i];

		        inputElement.checked = false;
		        inputElement.selected = false;
		    }
		    return false;
		}
    </script>
    <script type="text/javascript">
        function FilterShapes_TakeIn(caller) {
            var textbox = caller;
            filter = textbox.value.toLowerCase();
            var dropDownArray = document.getElementById('<%=lstCustomerlookupTakeIn.ClientID%>');
			var list = dropDownArray;
			for (var c = 0; c < list.options.length; c++) {
			    if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
			        list.options.item(c).style.display = "block";
			    }
			    else {
			        list.options.item(c).style.display = "none";
			    }
			}
        }
        function FilterShapes_ShipRecieving(caller) {
            var textbox = caller;
            filter = textbox.value.toLowerCase();
            var dropDownArray = document.getElementById('<%=lstCustomerLookupShipRecieving.ClientID%>');
			var list = dropDownArray;
			for (var c = 0; c < list.options.length; c++) {
			    if (list.options.item(c).text.toLowerCase().indexOf(filter) > -1) {
			        list.options.item(c).style.display = "block";
			    }
			    else {
			        list.options.item(c).style.display = "none";
			    }
			}
        }
        ///-----------------Upload Control Validation--------------------------///
        var ifIgnoreError = false;
        function UpLoadStarted(sender, e) {
            var fileName = e.get_fileName();
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'png' || fileExtension == 'PNG') {
                //file is good -- go ahead
                document.getElementById('<%=btnUploadTakeIn.ClientID%>').disabled = true;
			    document.getElementById('<%=btnUploadGiveOut.ClientID%>').disabled = true;
			}
			else {
			    //stop upload
			    ifIgnoreError = true;
			    sender._stopLoad();
			}
        }
        function UploadError(sender, e) {
            if (ifIgnoreError) {
                alert("Wrong file type, Only JPG/PNG file extension allowed");
            }
            else {
                alert(e.get_message());
            }
        }
        function UploadCompleteTakeIn(sender, e) {
            document.getElementById('<%=btnUploadTakeIn.ClientID%>').disabled = false;
		}
		function UploadCompleteGiveOut(sender, e) {
		    document.getElementById('<%=btnUploadGiveOut.ClientID%>').disabled = false;
		}




    </script>
    <div class="demoarea">
        <div class="demoheading">
            Front
        </div>
        <div style="height: 25px; position: absolute; top: 28px; left: 290px;">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <div class="modalProgress">
                        <div class="centerProgress">
                            <img alt="" src="Images/ajaxImages/loader.gif" />
                            <br />
                            <b>Please wait...</b>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

        <asp:UpdatePanel runat="server" ID="MainPanel" UpdateMode="Conditional" ChildrenAsTrigger="false">
            <Triggers>
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$lstCustomerlookupTakeIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel3$lstCustomerLookupShipRecieving" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$btnPrintLabelTakeIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel2$btnPrintLabelGiveOut" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel3$btnPrintLabelShipIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel4$btnPrintLabelShipOut" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel6$btnPrintLabelViewOrder" />

				<asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$btnPrintReceiptTakeIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel2$btnPrintReceiptGiveOut" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel3$btnPrintReceiptShipIn" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel4$btnPrintReceiptShipOut" />
                <asp:PostBackTrigger ControlID="TabContainer1$TabPanel6$btnPrintReceiptViewOrder" />
            </Triggers>
            <ContentTemplate>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="850px" Font-Size="12px" ActiveTabIndex="0" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                    <ajaxToolkit:TabPanel ID="TabPanel1" HeaderText="Take In" runat="server">
                        <ContentTemplate>
                            <asp:Panel runat="server" CssClass="form-inline" ID="CustomerSearchPanel" Width="1000px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Customer Status</legend>
                                    <table border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCustomerlookup" runat="server" Text="Customer lookup" Width="125px" Style="float: left"></asp:Label>
                                            </td>
                                            <td style="padding-top: -5px; padding-right: 5px" colspan="2" rowspan="1">
                                                <asp:TextBox runat="server" ID="txtCustomerLookupTakeIn" Width="350px" OnTextChanged="txtCustomerLookupTakeIn_OnTextChanged" autocomplete="off" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:DropDownExtender runat="server" TargetControlID="txtCustomerLookupTakeIn" DropDownControlID="DropDownListPanel" ID="DropDownExtender1" HighlightBackColor="WindowFrame" Enabled="True" DynamicServicePath="" />
                                                <asp:Panel runat="server" ID="DropDownListPanel" Style="visibility: hidden;">
                                                    <asp:ListBox runat="server" ID="lstCustomerlookupTakeIn"
                                                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                                                        OnSelectedIndexChanged="OnCustomerSelectedChanged" Rows="20" Width="365px" CssClass="DropDownListBoxStyle"></asp:ListBox>
                                                    <br />
                                                    <asp:DropDownList ID="ddlCustomerListTakeIn" runat="server" AutoPostBack="True" DataTextField="CustomerName"
                                                        DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnCustomerSelectedChanged" ToolTip="Customers List" Width="30px" Visible="False">
                                                    </asp:DropDownList>
                                                </asp:Panel>
                                            </td>
                                            <td style="padding-top: 10px">
                                                <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnNewCustomerTakeIn" runat="server" Text="New Customer" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right;" />
                                            </td>
                                            <td style="width: 70px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblVendorTakeIn" runat="server" Style="float: left; font-weight: bold;"></asp:Label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnDepartureSettingTakeIn" runat="server" Text="Departure Setting" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnMessengerlistTakeIn" runat="server" Text="Messenger" Width="150px" class="btn btn-info"
                                                    Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:Button>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="txtMessengerTakeIn" Width="350px" autocomplete="off" Height="20px" Enabled="False" Text="Messenger Lookup"></asp:TextBox>
                                                <asp:Label runat="server" ID="MessageLabel" ForeColor="Blue"></asp:Label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnNewMessengerTakeIn" runat="server" Text="New Messenger" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <table border="0">
                                                    <tr>
                                                        <td style="display:none;">
                                                            <asp:Button ID="btnCaptureSignatureTakeIn" runat="server" Text="Capture Signature" class="btn btn-info" Width="150px"
                                                                Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 3px;"></asp:Button>

                                                        </td>
                                                        <td style="border: 1px solid lightgray;display:none;">
                                                            <asp:HiddenField ID="hdnimgSignatureTakeIn" runat="server" Value="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" />
                                                            <asp:Image ID="imgCaptureSignatureTakeIn" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                        </td>
                                                        <td style="width: 2px;"></td>
                                                        <td style="border: 1px solid lightgray;">
                                                            <asp:Image ID="imgStoredSignature" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                        </td>
                                                        <td style="width: 2px;"></td>
                                                        <td style="border: 1px solid lightgray;">
                                                            <asp:Image ID="imgStoredPhoto" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;display:none;">
                                                            <asp:Label ID="Label28" runat="server" Text="Captured Signature" Style="float: none; width: 100px; padding: 0 10px 0 10px;"></asp:Label>

                                                        </td>
                                                        <td></td>
                                                        <td style="text-align: center;">
                                                            <asp:Label ID="lblStoredSignature" runat="server" Text="Stored Signature" Style="float: none; width: 100px; padding: 0 10px 0 10px;"></asp:Label></td>
                                                        <td></td>
                                                        <td style="text-align: center;">
                                                            <asp:Label ID="lblPhoto" runat="server" Text="Photo" Style="float: none; width: 50px; padding: 0 10px 0 12px;"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>


                                    </table>

                                </fieldset>
                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel5" Width="1000px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Order</legend>
                                    <table width="400px" style="float: left; margin-right: 0px;" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNumberOfItems" runat="server" Text="Number of Items" Style="float: left"></asp:Label>

                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtNoOfItemsTakeIn" Width="100px" OnTextChanged="txtNoOfItems_TextChanged" AutoPostBack="True" Enabled="False" MaxLength="5" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers" TargetControlID="txtNoOfItemsTakeIn" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdlNoOfItems" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" OnSelectedIndexChanged="rbtNoOfItems_SelectedIndexChanged">
                                                    <asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>

                                            </td>
                                            <td rowspan="5" style="vertical-align: top;">
                                                <fieldset class="filedSetBorder" style="padding-top: 2px;">
                                                    <legend class="label">Batches Memo Numbers</legend>
                                                    <table style="float: right; padding-left: 10px;" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOrderMemo" runat="server" Style="float: left" Text="Order Memo" Width="70px"></asp:Label>
                                                                <asp:TextBox ID="txtMemoTakeIn" runat="server" AutoPostBack="True" OnTextChanged="txtOrderMemo_TextChanged" Width="115px" Height="20px"></asp:TextBox>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtMNTakeIn" runat="server" Width="185px" AutoPostBack="True" OnTextChanged="txtMNTakeIn_TextChanged" Height="20px" MaxLength="100"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ListBox ID="lstMNsTakeIn" runat="server" Height="70px" Width="200px"></asp:ListBox>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTotalWeigh" runat="server" Text="Total Weight" Width="120px" Style="float: left"></asp:Label>

                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>


                                                            <asp:TextBox runat="server" ID="txtTotalWeightTakeIn" Width="45px" OnTextChanged="txtTotalWeight_TextChanged" AutoPostBack="True" Enabled="False" MaxLength="7" Height="20px"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom, Numbers" ValidChars="." TargetControlID="txtTotalWeightTakeIn" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td style="padding-bottom: 5px;">
                                                            <asp:DropDownList ID="ddlWeightType" runat="server" Width="45px" Height="30px" Style="padding: 0px;" DataTextField="MeasureUnitName" DataValueField="MeasureUnitID" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td>
                                                <asp:RadioButtonList ID="rdlTotalWeight" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" OnSelectedIndexChanged="rbtnTotalWeight_SelectedIndexChanged" AutoPostBack="True">
                                                    <asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Button ID="btnServiceTypeTakeIn" runat="server" Text="Service Type" Width="150px" class="btn btn-info"
                                                    Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 3px;"></asp:Button>


                                            </td>
                                            <td colspan="2" class="auto-style1">

                                                <asp:TextBox runat="server" ID="txtServiceTypeTakeIn" Width="360px" Enabled="False" Height="20px" Text="5 days"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdnServiceTypeTakeIn" Value="4"></asp:HiddenField>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSpecialInstructions" runat="server" Text="Special Instructions" Width="120px" Style="float: left"></asp:Label>

                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="txtSpecialInstructions" Width="360px" Height="50px" TextMode="MultiLine" MaxLength="2000"></asp:TextBox>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                        </tr>
                                    </table>
                                    &nbsp;
                                </fieldset>
                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel6" Width="760px">
                                <table style="width: 760px;">
                                    <tr>
                                        <td colspan="3">
                                            <fieldset class="filedSetBorder" style="width: 774px;">
                                                <legend class="label">Order Summary</legend>
                                                <table style="float: left; width: 750px;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCustomer" runat="server" Text="Customer:"></asp:Label>

                                                            <asp:Label ID="lblCustomerNameTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td rowspan="6" style="text-align: right;">

                                                            <asp:Label ID="lblOrderNumbeTakeIn" runat="server" Font-Bold="True" Width="150px" Style="font-size: large; text-align: center;"></asp:Label>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMessengerTakeIn" runat="server" Text="Messenger:"></asp:Label>

                                                            <asp:Label ID="lblMessengerNameTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNumberOfItemTakeIn" runat="server" Text="Number Of Items:"></asp:Label>

                                                            <asp:Label ID="lblNumberOfItemsValueTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTotalWeightTakeIn" runat="server" Text="Total Weight:"></asp:Label>

                                                            <asp:Label ID="lblTotalWeightValueTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblServiceTypesTakeIn" runat="server" Text="Service Type:"></asp:Label>

                                                            <asp:Label ID="lblServiceTypeSelecedTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMemoTakeIn" runat="server" Text="Memo:"></asp:Label>

                                                            <asp:Label ID="lblMemoNoTakeIn" runat="server"></asp:Label>

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtOrderNumberTakeIn" runat="server" Visible="False"></asp:TextBox>
                                                            <asp:Button ID="btnViewRecieptTakeIn" runat="server" Text="View Order Receipt" CssClass="btn btn-sm btn-info" Style="float: right;" Enabled="False" Visible="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr class="checkboxSingle">
                                        <td style="padding-left: 7px;" colspan="2">
                                            <asp:CheckBox ID="chkNoGoods" runat="server" Text="No Goods" style="float:left; margin-right:1vmin;"/>
                                            <asp:CheckBox ID="chkPickedUpByOurMessenger_TakeIn" runat="server" Text="Picked Up By GSI Messenger" OnCheckedChanged="chkPickedUpByOurMessenger_TakeIn_CheckedChanged" AutoPostBack="True" />
                                            <asp:HiddenField ID="hdnOrderCode" runat="server" />

                                            <div style="float: right;">
												<asp:Button ID="btnPrintReceiptTakeIn" runat="server" Text="Print Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintReceiptTakeIn_Click" Enabled="False" />
												<asp:Button ID="btnPrintLabelTakeIn" runat="server" Text="Print Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintLabelTakeIn_Click" Enabled="False" />
                                                    <asp:Button ID="btnSubmitTakeIn" runat="server" Text="Submit" CssClass="btn btn-sm btn-info" Style="float: right;" OnClick="btnSubmitTakeIn_Click" Width="65px" />
                                                <asp:Button ID="btnClearTakeIn" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: right; margin-right: 5px;" OnClick="btnClearTakeIn_Click" Width="65px" />
                                            </div>

                                        </td>


                                    </tr>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel2" HeaderText="Give Out" runat="server">
                        <ContentTemplate>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel2" Width="630px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Customer Detail</legend>
                                    <table style="width: 600px;" border="0">
                                        <tr>
                                            <td style="padding-left: 5px;">
                                                <asp:Label runat="server" ID="Label26" Text="Customer Name"></asp:Label>
                                            </td>
                                            <td colspan="4">
                                                <asp:TextBox ID="txtCustomerGiveOut" runat="server" Width="300px" Enabled="False" Height="20px"></asp:TextBox>
                                                <asp:HiddenField ID="hfCustomerGiveOut" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>

                                        <tr>

                                            <td colspan="5">

                                                <asp:Label ID="lblVendorGiveOut" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td colspan="4">
                                                <asp:Button ID="btnDepartureSettingGiveOut" runat="server" CssClass="btn btn-sm btn-info" Style="float: right;" Text="Departure Setting" Width="150px" />

                                            </td>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnMessengerLookupGiveOut" runat="server" class="btn btn-info" OnClientClick="ClearRBList();" Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" Text="Messenger" Width="150px" />
                                                </td>
                                                <td colspan="5">
                                                    <asp:TextBox ID="txtMessengerLookupGiveOut" runat="server" autocomplete="off" Enabled="False" Height="20px" Text="Messenger Lookup" Width="300px"></asp:TextBox>
                                                </td>
                                                <td align="right" colspan="3">
                                                    <asp:Button ID="btnNewMessengerGiveOut" runat="server" CssClass="btn btn-sm btn-info" Style="float: right;" Text="New Messenger" Width="150px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7">
                                                    <table border="0">
                                                        <tr>
                                                            <td style="display:none;">
                                                                <asp:Button ID="btnCaptureSignGiveOut" runat="server" class="btn btn-info" Style="background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 13px;display:none;" Text="Capture Signature" Width="150px" />
                                                            </td>
                                                            <td style="border: 1px solid lightgray; float: none;display:none;">
                                                                <asp:Image ID="imgCaptureSignatureGiveOut" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px!important; height: 100px!important; " />
                                                            </td>
                                                            <td style="width: 2px;"></td>
                                                            <td style="border: 1px solid lightgray;float: none; padding-left: 2px;">
                                                                <asp:Image ID="imgStoredSignGiveOut" runat="server" BorderStyle="None" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px!important; height: 100px!important; " />
                                                            </td>
                                                            <td style="width: 2px;"></td>
                                                            <td style="border: 1px solid lightgray; float: none; padding-left: 2px; ">
                                                                <asp:Image ID="imgStoredPhotoGiveOut" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px!important; height: 100px!important; " />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                           <%-- <td></td>--%>
                                                            <td style="text-align: center;display:none;">
                                                                <asp:Label ID="Label27" runat="server" Style="float: none; padding: 0 10px 0 10px;" Text="Captured Signature" Width="50px"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td style="text-align: center;">
                                                                <asp:Label ID="Label8" runat="server" Style="float: none; padding: 0 10px 0 10px;" Text="Stored Signature" Width="100px"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td style="text-align: center;">
                                                                <asp:Label ID="Label12" runat="server" Style="float: none; width: 50px; padding: 0 10px 0 12px;" Text="Photo" Width="50px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tr>


                                    </table>
                                </fieldset>
                                <br />
                                <table width="660px" border="0">

                                    <tr>
                                        <td colspan="2">

                                            <table style="width: 676px;">
                                                <tr>
                                                    <td style="padding-bottom: 7px; width: 80px;">
                                                        <asp:Label ID="Label5" runat="server" Text="Order Number"></asp:Label>
                                                    </td>
                                                    <td style="width: 170px;">
                                                        <asp:TextBox ID="txtItemNoGiveOut" runat="server" Width="150px" Height="20px" autocomplete="off"></asp:TextBox>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:ImageButton ID="btnItemGiveOut" runat="server" ImageUrl="~/Images/ajaxImages/search.png" OnClick="btnItemGiveOut_Click" />
                                                    </td>
                                                    <td class="checkboxSingle" style="float: right;">
                                                        <asp:CheckBox ID="chkAutoCheckOutGiveOut" runat="server" Text="Auto Check Out" Visible="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Panel runat="server" ID="Panel3" Width="675px" Height="200px" ScrollBars="Auto" BorderStyle="Inset" BorderWidth="1px">
                                                            <asp:TreeView ID="trvOrderTreeGiveOut" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="Small" ForeColor="Black"
                                                                Font-Bold="False" ExpandDepth="2">
                                                                <SelectedNodeStyle BackColor="#D7FFFF" />
                                                            </asp:TreeView>
                                                        </asp:Panel>

                                                    </td>
                                                </tr>

                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 0px; vertical-align: bottom; width: 680px;">
                                            <table border="0" style="width: 100%;">
                                                <tr>
                                                    <td colspan="5" style="text-align: right;">
                                                        <asp:Label ID="lblMsgGiveOut" runat="server" ForeColor="Blue" Width="334px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxSingle" style="">
                                                        <asp:CheckBox ID="chkTakenOutByOurMessengerGiveOut" runat="server" Text="Taken Out By GSI Messenger" Width="173px" OnCheckedChanged="chkTakenOutByOurMessengerGiveOut_CheckedChanged" AutoPostBack="True" />
                                                    </td>

                                                     <td colspan="1" style="width: 215px;text-align:right;">
														<asp:Button ID="btnPrintReceiptGiveOut" runat="server" Text="Print Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintReceiptGiveOut_Click" Enabled="False" />
														<asp:Button ID="btnPrintLabelGiveOut" runat="server" Text="Print Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintLabelGiveOut_Click" Enabled="False" />
                                                 </td>
                                                    <td style="width: 50px;">


                                                        <asp:Button ID="btnClearGiveOut" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: right; margin-right: 5px;" OnClick="btnClearGiveOut_Click" />
                                                    </td>

                                                    <td style="width: 50px;">
                                                        <asp:Button ID="btnSubmitGiveOut" runat="server" Text="Submit" CssClass="btn btn-sm btn-info" Style="" OnClick="btnSubmitGiveOut_Click" Enabled="False" />
                                                        <%--<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Are you sure you want to save ?" TargetControlID="btnSubmitGiveOut" Enabled="True">
														</ajaxToolkit:ConfirmButtonExtender>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel3" HeaderText="Ship Receiving" runat="server">
                        <ContentTemplate>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel8" Width="1000px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Customer Status</legend>
                                    <table border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Customer lookup" Width="125px" Style="float: left"></asp:Label>
                                            </td>
                                            <td style="padding-top: -5px; padding-right: 5px">
                                                <asp:TextBox runat="server" ID="txtCustomerLookupShipRecieving" Width="350px" OnTextChanged="txtCustomerLookupShipRecieving_OnTextChanged" autocomplete="off" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:DropDownExtender runat="server" TargetControlID="txtCustomerLookupShipRecieving" DropDownControlID="DropDownListPanel1" ID="DropDownExtender2" HighlightBackColor="WindowFrame" Enabled="True" />
                                                <asp:Panel runat="server" ID="DropDownListPanel1" Style="z-index: 1000; visibility: visible; position: absolute; left: 375px; top: 141px;">
                                                    <asp:ListBox runat="server" ID="lstCustomerLookupShipRecieving"
                                                        DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="True"
                                                        OnSelectedIndexChanged="OnCustomerSelectedChangedShipReceiving" Rows="20" Width="365px" CssClass="DropDownListBoxStyle"></asp:ListBox>
                                                    <asp:DropDownList ID="ddlCustomerLookupShipRecieving" runat="server" AutoPostBack="True" DataTextField="CustomerName"
                                                        DataValueField="CustomerId" Height="26px" OnSelectedIndexChanged="OnCustomerSelectedChangedShipReceiving" ToolTip="Customers List" Width="30px" Visible="False">
                                                    </asp:DropDownList>
                                                </asp:Panel>


                                            </td>
                                            <td style="padding-top: 10px">
                                                <asp:ImageButton ID="CustomerLikeButtonShipRecieving" runat="server" ToolTip="Filtering the list of customers"
                                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClickShipRecieving" />
                                            </td>

                                            <td>
                                                <asp:Button ID="btnCustomerShipRecieving" runat="server" Text="New Customer" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right;" />
                                            </td>
                                            <td style="width: 55px;"></td>
                                        </tr>

                                        <tr>
                                            <td align="left" colspan="2">
                                                <asp:Label ID="lblVendorShipRecieving" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td></td>
                                            <td align="right">
                                                <asp:Button ID="btnDepartureSettingShipRecieving" runat="server" CssClass="btn btn-sm btn-info" Style="float: right;" Text="Departure Setting" Width="150px" />
                                            </td>
                                            <tr>
                                                <td colspan="2">
                                                    <fieldset class="filedSetBorder" style="margin-left: 0px; width: 330px;">
                                                        <legend class="label">Carrier</legend>
                                                        <asp:button id="btnCarriersShipReceiving" runat="server" class="btn btn-info" style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 5px; margin-bottom: 4px;" text="Carrier" width="100px" xmlns:asp="#unknown" />
                                                        <asp:TextBox ID="txtCarriersShipReceiving" runat="server" autocomplete="off" Enabled="False" Height="20px" Width="200px"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnCarriersShipReceiving" runat="server" Value="0" />
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                                                    <asp:TextBox ID="txtScanPackageBarcodeShipRecieving" runat="server" Height="20px" Width="450px"></asp:TextBox>
                                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" Enabled="True" TargetControlID="txtScanPackageBarcodeShipRecieving" WatermarkText="Scan Package Bar-Code Here">
                                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                                </td>
                                            </tr>
                                        </tr>


                                    </table>

                                </fieldset>




                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel4" Width="1000px">
                                <fieldset class="filedSetBorder">
                                    <legend class="label">Order</legend>
                                    <table width="400px" style="float: left; margin-right: 0px;">
                                        <tr>
                                            <td style="padding-top: -5px; padding-right: 5px">
                                                <asp:Label ID="Label10" runat="server" Text="Number of Items" Style="float: left"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtNoOfItemsShipReceiving" Width="100px" AutoPostBack="True" Enabled="False" OnTextChanged="txtNoOfItemsShipReceiving_TextChanged" MaxLength="5" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers" TargetControlID="txtNoOfItemsShipReceiving" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdlNoOfItemsShipReceiving" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" OnSelectedIndexChanged="rbtnNoOfItemsShipReceiving_SelectedIndexChanged">
                                                    <asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td rowspan="5" style="vertical-align: top;">

                                                <fieldset class="filedSetBorder" style="padding-top: 2px;">
                                                    <legend class="label">Batches Memo Numbers</legend>
                                                    <table style="float: right; padding-left: 10px;" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label14" runat="server" Style="float: left" Text="Order Memo" Width="70px"></asp:Label>
                                                                <asp:TextBox ID="txtMemoShipReceiving" runat="server" AutoPostBack="True" OnTextChanged="txtMemoShipReceiving_TextChanged" Width="115px" Height="20px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtMNShipReceiving" runat="server" Width="185px" Height="20px" OnTextChanged="txtMNShipReceiving_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ListBox ID="lstMNsShipReceiving" runat="server" Height="70px" Width="200px"></asp:ListBox>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Text="Total Weight" Width="110px" Style="float: left"></asp:Label>
                                            </td>
                                            <td style="padding-bottom: 5px;">
                                                <asp:TextBox runat="server" ID="txtTotalWeightShipReceiving" Width="50px" AutoPostBack="True" Enabled="False" OnTextChanged="txtTotalWeightShipReceiving_TextChanged" MaxLength="7" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom, Numbers" ValidChars="." TargetControlID="txtTotalWeightShipReceiving" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                                <asp:DropDownList ID="ddlWeightTypeShipRecieving" runat="server" Width="40px" Height="30px" Style="padding: 0px; float: right;" Enabled="False" DataTextField="MeasureUnitName" DataValueField="MeasureUnitID">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdlTotalWeightShipReceiving" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="True" OnSelectedIndexChanged="rbtnTotalWeightShipReceiving_SelectedIndexChanged">
                                                    <asp:ListItem Text="Inspected" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Not Inspected" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:Button ID="btnServiceTypeShipReceiving" runat="server" Text="Service Type" Width="100px" class="btn btn-info"
                                                    Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 0px;"></asp:Button>
                                            </td>
                                            <td colspan="2">



                                                <asp:TextBox runat="server" ID="txtServiceTypeShipReceiving" Width="360px" Enabled="False" Height="20px" Text="5 days"></asp:TextBox>
                                                <asp:HiddenField ID="hdnServiceTypeShipReceiving" runat="server" Value="4" />


                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label13" runat="server" Text="Special Instructions" Width="110px" Style="float: left"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="txtSpecialInstructionsShipReceiving" Width="360px" Height="50px" TextMode="MultiLine" MaxLength="2000"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                        </tr>
                                    </table>
                                &nbsp;
                            </asp:Panel>

                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel7" Width="760px">
                                <table style="width: 760px;">
                                    <tr>
                                        <td>
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Order Summary</legend>
                                                <table style="float: left; width: 732px;" colspan="3">
                                                    <tr>
                                                        <td style="width: 530px;">
                                                            <asp:Label ID="Label15" runat="server" Text="Customer:"></asp:Label>
                                                            <asp:Label ID="lblCustomerShipReceiving" runat="server"></asp:Label>
                                                        </td>
                                                        <td rowspan="6">
                                                            <asp:Label ID="lblOrderNumbeShipReceiving" runat="server" Font-Bold="True" Width="150px" Style="font-size: large; text-align: center;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label17" runat="server" Text="Carrier:"></asp:Label>
                                                            <asp:Label ID="lblCarrierShipReceiving" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNumberOfItemsShipReceiving" runat="server" Text="Number Of Items :"></asp:Label>
                                                            <asp:Label ID="lblNumberOfItemsValueShipReceiving" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTotalWeightShipReceiving" runat="server" Text="Total Weight:"></asp:Label>
                                                            <asp:Label ID="lblTotalWeightValueShipReceiving" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label21" runat="server" Text="Service Type:"></asp:Label>
                                                            <asp:Label ID="lblServiceTypeShipReceiving" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label23" runat="server" Text="Memo:"></asp:Label>
                                                            <asp:Label ID="lblMemoShipReceiving" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <asp:TextBox ID="txtOrderNumberShipReceiving" runat="server" Visible="false"></asp:TextBox>
                                                            <asp:Button ID="btnViewRecieptShipReceiving" runat="server" Text="View Order Receipt" CssClass="btn btn-sm btn-info" Style="float: right;" Enabled="False" Visible="false" /></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                        <%--		<td colspan="2" style="vertical-align: bottom;">
											<table style="width: 180px;" align="right">
												<tr>

													<td style="float: right;">
														<fieldset class="filedSetBorder">
															<legend class="label">Labels</legend>
															<asp:RadioButtonList ID="rblLabelsShipReceiving" runat="server">
																<asp:ListItem Text="Print Label" Value="0" Selected="True"></asp:ListItem>
																<asp:ListItem Text="Skip Label" Value="1"></asp:ListItem>
															</asp:RadioButtonList>
														</fieldset>
													</td>
												</tr>
											</table>
										</td>--%>
                                    </tr>
                                    <tr>
                                        <td class="checkboxSingle" style="padding-left: 5px;" colspan="2">
                                            <asp:CheckBox ID="chkPickedUpByOurMessenger_ShipReceiving" runat="server" Text="Picked Up By GSI Messenger" OnCheckedChanged="chkPickedUpByOurMessenger_ShipReceiving_CheckedChanged" AutoPostBack="true" Visible="false" />

                                            <%--					<asp:HiddenField ID="GroupOfficeIDShipIn" runat="server" />
											<asp:HiddenField ID="CustomerCodeShipIn" runat="server" />
											<asp:HiddenField ID="GroupIDShipIn" runat="server" />--%>
                                            <div style="float: right;padding-right:7px;">
                                                <asp:HiddenField ID="hdnOrderCodeShipReceiving" runat="server" />
												<asp:Button ID="btnPrintReceiptShipIn" runat="server" Text="Print Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintReceiptShipIn_Click" Enabled="False" />
												<asp:Button ID="btnPrintLabelShipIn" runat="server" Text="Print Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintLabelShipIn_Click" Enabled="False" />
                                           
                                                <asp:Button ID="btnSubmitShipReceiving" runat="server" Text="Submit" CssClass="btn btn-sm btn-info" Style="float: right;" OnClick="btnSubmitShipReceiving_Click" />
                                                <asp:Button ID="btnClearShipReceiving" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: right; margin-right: 5px;" OnClick="btnClearShipReceiving_Click" />
                                                <%--<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" ConfirmText="Are you sure you want to save ?" TargetControlID="btnSubmitShipReceiving" Enabled="True">
											</ajaxToolkit:ConfirmButtonExtender>--%>
                                            </div>
                                        </td>


                                    </tr>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel4" HeaderText="Ship Out" runat="server">
                        <ContentTemplate>


                            <asp:Panel runat="server" CssClass="form-inline" ID="Panel1" Width="1000px">
                                <fieldset class="filedSetBorder" style="width: auto;">
                                    <legend class="label">Customer Detail</legend>
                                    <table style="width: 640px;" border="0">
                                        <tr>
                                            <td colspan="0">
                                                <asp:Label ID="Label33" runat="server" Text="Customer Name" Width="100px"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCustomerNameShipOut" runat="server" Width="350px" Height="20px" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td></td>

                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblVendorShipOut" runat="server" Text="" Font-Bold="true" Style="float: left;"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Button ID="btnDepartureSettingShipOut" runat="server" Text="Departure Setting" CssClass="btn btn-sm btn-info" Width="150px" Style="float: right;" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <fieldset class="filedSetBorder" style="margin-left: 0px; width: 330px;">
                                                    <legend class="label">Carrier</legend>
                                                    <asp:Button ID="btnCarriersShipOut" runat="server" Text="Carrier" CssClass="btn btn-sm btn-info" Width="100px"
                                                        Style="padding-left: 0px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-right: 5px; margin-bottom: 4px;"></asp:Button>

                                                    <asp:TextBox runat="server" ID="txtCarriersShipOut" Width="200px" autocomplete="off" Height="20px" Enabled="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnCarriersShipOut" runat="server" Value="0" />

                                                </fieldset>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                                                <asp:TextBox ID="txtBarcodeShipOut" runat="server" Width="350px" placeholder="" Height="20px"></asp:TextBox>
                                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkText="Scan Package Bar-Code Here" TargetControlID="txtBarcodeShipOut"></ajaxToolkit:TextBoxWatermarkExtender>
                                                <asp:Label ID="Label1" runat="server" Text="Shipping Charges" Style="float: right; padding-right: 5px; padding-top: 2px;"></asp:Label>
                                            </td>
                                            <td>

                                                <asp:TextBox ID="txtShippingChargesShipOut" runat="server" Width="150px" Height="20px"></asp:TextBox></td>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <br />
                                <table width="670px" border="0">

                                    <tr>
                                        <td colspan="2">
                                            <fieldset class="filedSetBorder">
                                                <legend class="label">Order Detail</legend>
                                                <table width="600px">
                                                    <tr>
                                                        <td style="text-align: left; width: 85px;">
                                                            <asp:Label ID="Label2" runat="server" Text="Order Number"></asp:Label>
                                                        </td>
                                                        <td style="text-align: left; width: 120px;">
                                                            <asp:TextBox ID="txtItemNoShipOut" runat="server" Width="100px" Height="20px" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:ImageButton ID="btnItemNoShipOut" runat="server" ImageUrl="~/Images/ajaxImages/search.png" OnClick="btnItemNoShipOut_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Panel runat="server" ID="Panel10" Width="630px" Height="200px" ScrollBars="Auto" BorderStyle="Inset" BorderWidth="1px">
                                                                <asp:TreeView ID="trvOrderTreeShipOut" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                                                    onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="Small" ForeColor="Black"
                                                                    Font-Bold="False" ExpandDepth="2">
                                                                    <SelectedNodeStyle BackColor="#D7FFFF" />
                                                                </asp:TreeView>
                                                            </asp:Panel>

                                                        </td>
                                                    </tr>

                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td colspan="2">
                                            <table border="0" style="width: 99%;">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblMsgShipOut" runat="server" Text="" ForeColor="Blue" Style="float: right;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxSingle" style="">
                                                        <asp:CheckBox ID="chkTakenOutByOurMessengerShipOut" runat="server" Text="Taken Out By GSI Messenger" Width="173px" AutoPostBack="true" OnCheckedChanged="chkTakenOutByOurMessengerShipOut_CheckedChanged" Visible="false" />
                                                    </td>

                                                    <td style="width: 215px;text-align:right;">
												<asp:Button ID="btnPrintReceiptShipOut" runat="server" Text="Print Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintReceiptShipOut_Click" Enabled="False" />
												<asp:Button ID="btnPrintLabelShipOut" runat="server" Text="Print Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 5px;" OnClick="btnPrintLabelShipOut_Click" Enabled="False" />
    </td>
                                                    <td style="width: 50px;">
                                                        <asp:Button ID="btnClearShipOut" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: left; margin-right: 5px;" OnClick="btnClearShipOut_Click" />
                                                    </td>
                                                    <td style="width: 50px;">
                                                        <asp:Button ID="btnSubmitShipOut" runat="server" Text="Submit" CssClass="btn btn-sm btn-info" Style="float: right;" OnClick="btnSubmitShipOut_Click" />
                                                        <%--<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" ConfirmText="Are you sure you want to save ?" TargetControlID="btnSubmitShipOut" OnClientCancel="">
														</ajaxToolkit:ConfirmButtonExtender>--%>
                                                    </t>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                </table>

                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel5" HeaderText="Edit Order" runat="server">
                        <ContentTemplate>


                            <fieldset class="filedSetBorder" style="float: none; width: max-content;">
                                <legend class="label">Order</legend>
                                <table width="500px" style="float: left; margin-right: 15px;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text="Order Number" Width="100px" Style="float: left" Height="20px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtOrderCodeEditOrder" autocomplete="off" Height="20px" MaxLength="7"></asp:TextBox>
                                            <asp:ImageButton ID="OrderLikeButton" runat="server" ToolTip="find Order" ImageUrl="~/Images/ajaxImages/search.png" OnClick="btnEditOrder_Click" />
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers" TargetControlID="txtOrderCodeEditOrder"></ajaxToolkit:FilteredTextBoxExtender>
                                        </td>

                                        <td colspan="0">

                                            <asp:Label ID="lblEditOrderSearch" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: -5px; padding-right: 5px">
                                            <asp:Label ID="lblEditOrderQty" runat="server" Text="Not Inspected Quantity" Width="150px" Style="float: left" Visible="False" Height="20px"></asp:Label>

                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox runat="server" ID="txtNoOFItemsEditOrder" Visible="False" MaxLength="5" Height="20px"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtNoOFItemsEditOrder"></ajaxToolkit:FilteredTextBoxExtender>
                                        </td>
                                        <td></td>
                                        <td rowspan="4" style="vertical-align: top;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEditOrderTW" runat="server" Text="Not Inspected Total Weight" Style="float: left" Visible="False"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox runat="server" ID="txtTotalWeightEditOrder" Visible="False" MaxLength="7" Height="20px"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtTotalWeightEditOrder"></ajaxToolkit:FilteredTextBoxExtender>
                                        </td>

                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEditOrderSP" runat="server" Text="Special Instructions" Width="120px" Style="float: left" Visible="False"></asp:Label>

                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox runat="server" ID="txtSpecialInstructionEditOrder" Width="300px" Height="70px" TextMode="MultiLine" Visible="False" MaxLength="2000"></asp:TextBox>

                                        </td>

                                    </tr>
                                    <tr runat="server" id="trEditOrderMemo" visible="false">
                                        <td class="auto-style1">
                                            <asp:Label ID="lblEditOrderMemo" runat="server" Style="float: left" Text="Order Memo" Width="120px" Visible="False"></asp:Label>
                                        </td>
                                        <td colspan="2" class="auto-style1">
                                            <asp:HiddenField ID="hdnMemoEditOrder" runat="server" />
                                            <asp:TextBox ID="txtMemoEditOrder" runat="server" Width="300px" Visible="False" MaxLength="99" Height="20px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">
                                            <asp:Label ID="lblEditNoGoods" runat="server" Style="float: left" Text="No Goods" Width="120px" Visible="False"></asp:Label>
                                        </td>
                                        <td colspan="2" class="auto-style1">
                                            <asp:CheckBox ID="chkEditNoGood" runat="server" Width="300px" Visible="False" Height="20px" style="margin-top:0px;"/>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td colspan="3" style="padding-right: 15px;">

                                            <asp:Button ID="btnSubmitEditOrder" runat="server" CssClass="btn btn-sm btn-info" OnClick="btnSubmitEditOrder_Click" Style="float: right;" Text="Submit" Visible="False" />
                                            <asp:Button ID="btnClearEditOrder" runat="server" CssClass="btn btn-sm btn-info" Style="float: right; margin-right: 5px;" Text="Clear" Visible="False" OnClick="btnClearEditOrder_Click" />
                                            <asp:Label ID="lblMsgEditOrder" runat="server" Text="" ForeColor="Blue" Style="float: right; padding-right: 5px;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel6" HeaderText="View Order" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td colspan="2">

                                        <fieldset class="filedSetBorder" style="float: none; width: max-content;">
                                            <legend class="label">Order</legend>
                                            <table style="float: left; margin-right: 15px; width: 500px;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label41" runat="server" Text="Order Number" Width="100px" Style="float: left" Height="20px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtViewOrder" autocomplete="off" Height="20px" MaxLength="7"></asp:TextBox>
                                                        <asp:HiddenField runat="server" ID="hdnViewOrder"></asp:HiddenField>
                                                        <asp:ImageButton ID="imgViewOrder" runat="server" ToolTip="find Order" ImageUrl="~/Images/ajaxImages/search.png" OnClick="btnViewOrder_Click" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers" TargetControlID="txtViewOrder"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </td>

                                                    <td colspan="0">
                                                        <asp:Label ID="lblViewOrderSearch" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr runat="server" id="trViewOrderSummary" visible="false">
                                    <td colspan="2">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Order Summary</legend>
                                            <table style="float: left; width: 650px;" border="0">
                                                <tr>
                                                    <td colspan="1">
                                                        <asp:Label ID="Label42" runat="server" Text="Customer:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderCustomer" runat="server"></asp:Label>
                                                    </td>
                                                     <td>
                                                        <asp:Label ID="Label43" runat="server" Text="No Goods:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblNoGoods" runat="server" Text=""></asp:Label>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <asp:Label ID="Label55" runat="server" Text="Vendor:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderVendorName" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label53" runat="server" Text="Memo:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderMemo" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label47" runat="server" Text="Number Of Items (Not Inspected):" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderNumOfItemsNotInspected" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label46" runat="server" Text="Create Date:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderCreateDate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label49" runat="server" Text="Total Weight (Not Inspected):" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderTotalWeightNotInspected" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label52" runat="server" Text="Order State:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderOrderState" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label44" runat="server" Text="Number Of Items (Inspected):" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderNumOfItemsInspected" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label59" runat="server" Text="Order Status:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderStatus" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label48" runat="server" Text="Total Weight (Inspected):" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderTotalWeightInspected" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label51" runat="server" Text="Service Type:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewOrderServiceType" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr runat="server" id="trViewOrderOperation" visible="false">
                                    <td style="vertical-align: top;">
                                        <fieldset class="filedSetBorder" style="width: 340px; height: 270px;">
                                            <legend class="label">
                                                <asp:Label ID="lblTakeInViewOrderLabel" runat="server" Text="Take In" Font-Bold="true"></asp:Label></legend>
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblTakeInSubmittedbytext" runat="server" Text="Submitted By:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblTakeInSubmittedBy" runat="server" Text="" Font-Bold="false"></asp:Label>
                                                         <asp:Label ID="lblTakeInPickedUpByOurMessenger" runat="server" Text="Picked Up By GSI Messenger" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <%--<tr runat="server" id="trTakeInPickedUpByOurMessenger" >
                                                    <td>
                                                       
                                                    </td>
                                                </tr>--%>
                                                
                                                <tr runat="server" id="trTakeInImages">
                                                    <td>
                                                        <asp:Image ID="imgTakeInMessanger" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                        <asp:Image ID="imgTakeInSignature" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                    </td>
                                                </tr>
                                                <%--	<tr runat="server" id="trTakeInMessenger">
													<td>
														<asp:Label ID="Label45" runat="server" Text="Messenger:" Font-Bold="true"></asp:Label>
														<asp:Label ID="lblViewTakeInOrderMessenger" runat="server"></asp:Label>
														<asp:HiddenField ID="hdnTakeInPersonID" runat="server" />
													</td>
												</tr>--%>
                                                <tr runat="server" id="trViewTakeInOrderCarrier">
                                                    <td>
                                                        <asp:Label ID="Label50" runat="server" Text="Carrier:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInOrderCarrier" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInOrderCarrierTrackingNumber">
                                                    <td>
                                                        <asp:Label ID="Label54" runat="server" Text="Carrier Tracking Number:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInOrderCarrierTrackingNumber" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="Label56" runat="server" Text="Accepted By:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInAcceptedBy" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInSignatureTimeStamp">
                                                    <td colspan="2">
                                                        <asp:Label ID="Label60" runat="server" Text="Signature TimeStamp:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInSignatureTimeStamp" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="trViewTakeInRecieptUploadedTimeStamp">
                                                    <td colspan="2">
                                                        <asp:Label ID="Label63" runat="server" Text="Receipt Uploaded TimeStamp:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewTakeInRecieptUploadedTimeStamp" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewTakeInRecieptDownload">
                                                    <td>
                                                        <asp:LinkButton ID="lnkRecieptTakeInDownloadfrm" runat="server" href="#" target="_blank" Text="View Signed Receipt" Style="font-weight: bold;" Visible="false"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>

                                        </fieldset>
                                    </td>

                                    <td style="vertical-align: top;" runat="server" id="tdGiveOut">
                                        <fieldset class="filedSetBorder" style="width: 340px; height: 270px;">
                                            <legend class="label">
                                                <asp:Label ID="lblGiveOutViewOrderLabel" runat="server" Text="Give Out" Font-Bold="true"></asp:Label></legend>
                                            <table border="0">
                                                <tr>

                                                    <td>

                                                        <asp:Label ID="lblViewGiveOutSubmittedBytext" runat="server" Text="Accepted By:" Font-Bold="true"></asp:Label>
                                                        <%--</td>
													<td>--%>
                                                        <asp:Label ID="lblViewGiveOutSubmittedBy" runat="server" Text=""></asp:Label>

                                                    </td>

                                                </tr>
                                                <tr runat="server" id="trGiveOutPickedUpByOurMessenger">
                                                    <td>
                                                        <asp:Label ID="lblGiveOutPickedUpByOurMessenger" runat="server" Text="Taken Out By GSI Messenger" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trGiveOutImages">
                                                    <td>
                                                        <asp:Image ID="imgGiveOutMessanger" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                        <asp:Image ID="imgGiveOutSignature" runat="server" ImageUrl="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" Style="float: right; width: 150px; height: 100px;" />
                                                    </td>
                                                </tr>
                                                <%--<tr runat="server" id="trGiveOutMessenger">
													<td>
														<asp:Label ID="Label681" runat="server" Text="Messenger:" Font-Bold="true"></asp:Label>
														<asp:Label ID="lblViewGiveOutOrderMessenger" runat="server"></asp:Label>
														<asp:HiddenField ID="hdnGiveOutPersonID" runat="server" />
													</td>
												</tr>--%>
                                                <tr runat="server" id="trViewGiveOutOrderCarrier">
                                                    <td>
                                                        <asp:Label ID="Label69" runat="server" Text="Carrier:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewGiveOutOrderCarrier" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewGiveOutOrderCarrierTrackingNumber">
                                                    <td>
                                                        <asp:Label ID="Label71" runat="server" Text="Carrier Tracking Number:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewGiveOutOrderCarrierTrackingNumber" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="Label57" runat="server" Text="Submitted By:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewGiveOutAcceptedBy" runat="server" Text="" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewGiveOutSignatureTimeStamp">
                                                    <td colspan="2">
                                                        <asp:Label ID="Label61" runat="server" Text="Signature TimeStamp" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewGiveOutSignatureTimeStamp" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="trViewGiveOutRecieptUploadedTimeStamp">
                                                    <td colspan="2">
                                                        <asp:Label ID="Label65" runat="server" Text="Receipt Uploaded TimeStamp:" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblViewGiveOutRecieptUploadedTimeStamp" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trViewGiveOutRecieptDownload">
                                                    <td>
                                                        <asp:LinkButton ID="lnkRecieptGiveOutDownloadfrm" runat="server" href="#" target="_blank" Text="View Signed Receipt" Style="font-weight: bold;" Visible="false"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>

                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:right;">
										<asp:Button ID="btnUploadSignedOrderReciept" runat="server" Text="Upload Signed Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnPrintReceiptViewOrder" runat="server" Text="Print Receipt" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnPrintReceiptViewOrder_Click" Enabled="false" Visible="false" />
                                        <asp:Button ID="btnPrintLabelViewOrder" runat="server" Text="Print Label" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 2px;" OnClick="btnPrintLabelViewOrder_Click" Enabled="false" Visible="false" />
                                      <asp:Button ID="btnClearViewOrder" runat="server" Text="Clear" CssClass="btn btn-sm btn-info" Style="float: none; margin-right: 10px;" OnClick="btnClearViewOrder_Click" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                </ajaxToolkit:TabContainer>

                <%----------------------Upload Company Latterhead ----------------------%>
                <asp:Panel runat="server" ID="pnlUploadSignedOrderReciept" CssClass="modal-dialog"
                    Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel1">
                                <asp:Button Style="float: right" ID="Button4" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" />
                                <asp:Label ID="Label58" runat="server" Text="Upload / Download Signed Receipt"
                                    Style="float: none; padding-top: 5px; font-size: 15px; line-height: 25px;"></asp:Label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Take In</legend>
                                            <table border="1" cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label76" runat="server" Text="Select Scanned File (JPG/PNG Only) :-" Style="float: none; padding-top: 5px;"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadSignedOrderRecieptTakeIn" runat="server" Width="175px" OnUploadedComplete="AsyncFileUploadSignedOrderRecieptTakeIn_UploadedComplete"
                                                            ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" OnClientUploadComplete="UploadCompleteTakeIn" />

                                                    </td>
                                                    <td>
                                                       <%-- <asp:Label ID="lblkycMsg" runat="server" Text="" ForeColor="Blue"></asp:Label>--%>
                                                        <asp:Button Style="float: right" ID="btnUploadTakeIn" runat="server" Text="Upload" CssClass="btn btn-info" CausesValidation="false" OnClick="btnUploadSignedOrderRecieptTakeIn_Click" Enabled="false" />
                                                    </td>
                                                </tr>

                                                <tr>


                                                    <td colspan="3">
                                                        <asp:Label ID="lblRecieptTakeIn" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Blue"></asp:Label></td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="filedSetBorder">
                                            <legend class="label">Give Out
                                            </legend>
                                            <table border="1" cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label68" runat="server" Text="Select Scanned File (JPG/PNG Only) :-" Style="float: none; padding-top: 5px;"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolkit:AsyncFileUpload ID="AsyncFileUploadSignedOrderRecieptGiveOut" runat="server" Width="175px" OnUploadedComplete="AsyncFileUploadSignedOrderRecieptGiveOut_UploadedComplete"
                                                            ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" OnClientUploadComplete="UploadCompleteGiveOut" />

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label67" runat="server" Text="" ForeColor="Blue"></asp:Label>
                                                        <asp:Button Style="float: right" ID="btnUploadGiveOut" runat="server" Text="Upload" CssClass="btn btn-info" CausesValidation="false" OnClick="btnUploadSignedOrderRecieptGiveOut_Click" Enabled="false" />
                                                    </td>
                                                </tr>

                                                <tr>

                                                    <td colspan="3">
                                                        <asp:Label ID="lblRecieptGiveOut" runat="server" Text="" Style="float: none; padding-top: 5px;" ForeColor="Blue"></asp:Label>
                                                    </td>

                                                </tr>

                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender17" runat="server" PopupControlID="pnlUploadSignedOrderReciept" TargetControlID="TabContainer1$TabPanel6$btnUploadSignedOrderReciept" CancelControlID="ctl00_SampleContent_ModalPopupExtender17_backgroundElement"
                    Enabled="True" DynamicServicePath="" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <%--Error Message popup for Font All Tabs Start--%>
                <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none; border: solid 2px lightgray;">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />

                            <b>Information</b>

                        </div>
                    </asp:Panel>

                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                    </div>

                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />

                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                    PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                <%--End Error Message popup for Font All Tabs --%>

                <%-- Save Question Dialog --%>
                <asp:Panel runat="server" ID="SaveQDialog" CssClass="modalPopup" Style="width: 430px; display: none">
                    <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                            <asp:Label ID="QTitle" runat="server" />
                        </div>
                    </asp:Panel>
                    <div style="color: black; padding-top: 10px">
                        Do you want to save the order changes?
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="QuesSaveYesBtn" runat="server" OnClick="OnQuesSaveYesClick" Text="Yes"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesCancelBtn" runat="server" Text="Cancel" Style="display: none"
                                class="btn btn-info btn-small" />
                            <asp:Button ID="QuesCloseBtn" runat="server" Text="Close" Style="display: none" />
                        </p>
                    </div>
                    <asp:HiddenField ID="SaveDlgMode" runat="server" />
                </asp:Panel>
                <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupQuestBtn" PopupControlID="SaveQDialog"
                    ID="SaveQPopupExtender" PopupDragHandleControlID="SaveQDragHandle" OkControlID="QuesCloseBtn">
                </ajaxToolkit:ModalPopupExtender>
                <%-- End Save Question Dialog --%>

                <%--Common Customer Edit Page for All Tabs Start--%>
                <asp:Panel ID="pnlCustomerEdit" runat="server" CssClass="modal-dialog" Width="1800px" Height="800px" Style="display: none;">

                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel">
                                <asp:Label ID="lblModelHeader" runat="server" Text="New Customer" Style="float: left; padding-top: 5px;"></asp:Label>

                                <asp:Button Style="float: right" ID="btnClosex" runat="server" Text="X" class="btn btn-danger" />

                            </div>
                        </div>
                        <div class="modal-body">
                            <iframe id="iFrameNewCustomer" runat="server" src="CustomerEdit1.aspx" width="2000px" height="807px" style="margin-left: -206px; margin-top: -30px; overflow: scroll;"></iframe>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="pnlCustomerEdit" TargetControlID="TabContainer1$TabPanel1$btnNewCustomerTakeIn"
                    CancelControlID="btnClosex" BackgroundCssClass="modalBackground" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="pnlCustomerEdit" TargetControlID="TabContainer1$TabPanel1$btnNewMessengerTakeIn"
                    CancelControlID="btnClosex" BackgroundCssClass="modalBackground" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender12" runat="server" PopupControlID="pnlCustomerEdit" TargetControlID="TabContainer1$TabPanel3$btnCustomerShipRecieving"
                    CancelControlID="btnClosex" BackgroundCssClass="modalBackground" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender11" runat="server" PopupControlID="pnlCustomerEdit" TargetControlID="TabContainer1$TabPanel2$btnNewMessengerGiveOut"
                    CancelControlID="btnClosex" BackgroundCssClass="modalBackground" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>



                <%--Common Customer Edit Page for All Tabs End--%>
                <%--Take In Start--%>
                <asp:Panel runat="server" ID="pnlMessengerlistTakeIn" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label7" Text="Messenger" Font-Bold="true"> </asp:Label>

                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField runat="server" ID="hdnMessengerTakeIn"></asp:HiddenField>
                                <asp:RadioButtonList ID="rdbMessengerTakeIn" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rdbMessengerTakeIn_Changed"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="PersonId"
                                    DataTextField="FullName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblMessengerMsgTakeIn" runat="server" Text="No Messenger available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender10" runat="server" PopupControlID="pnlMessengerlistTakeIn" TargetControlID="TabContainer1$TabPanel1$btnMessengerlistTakeIn" CancelControlID="ctl00_SampleContent_ModalPopupExtender10_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlCaptureSignatureTakeIn" CssClass="modal-dialog" Style="display: none;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel1">
                                <asp:Button Style="float: right" ID="Button1" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender5_backgroundElement').style.display = 'none';" />
                                <asp:Label ID="Label16" runat="server" Text="Signature Capture" Style="float: none; padding-top: 5px;"></asp:Label>
                            </div>
                        </div>
                        <div class="modal-body">

                            <div id="dvSignaturepadTakeIn" class="m-signature-pad">
                                <div class="m-signature-pad--body">
                                    <canvas style="background-color: white;" width="400" height="300"></canvas>
                                </div>

                                <div class="signature-pad--footer">
                                    <div class="signature-pad--actions">
                                        <div align="center">
                                            <button type="button" class="button clear" data-action="clear">Clear</button>
                                            <button type="button" class="button" data-action="undo">Undo</button>
                                            <button id="btnSave" type="button" class="button save" data-action="save-png">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender5" runat="server" PopupControlID="pnlCaptureSignatureTakeIn" TargetControlID="TabContainer1$TabPanel1$btnCaptureSignatureTakeIn" CancelControlID="ctl00_SampleContent_ModalPopupExtender5_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" ID="pnlDepartureSettingTakeIn" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel2">
                                <asp:Label ID="Label30" runat="server" Text="Departure Setting" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="Button6" runat="server" Text="X" class="btn btn-danger" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label31" Text="" Font-Bold="true"> </asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label32" Text="Scan Bag/Receipt Number" Font-Bold="true"> </asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlVendorDepartureSettingTakeIn" runat="server" DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorDepartureSettingTakeIn_OnSelectedIndexChanged" Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVendorDepartureSettingTakeIn" runat="server" Width="70px" Height="20px" OnTextChanged="txtVendorDepartureSettingTakeIn_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="float: right;">
                                        <asp:Button ID="btnSetDepartureSettingTakeIn" runat="server" Text="Set" CssClass="btn btn-info" OnClick="btnSetDepartureSettingTakeIn_OnClick" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>

                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender13" runat="server" PopupControlID="pnlDepartureSettingTakeIn" TargetControlID="TabContainer1$TabPanel1$btnDepartureSettingTakeIn" CancelControlID="ctl00_SampleContent_ModalPopupExtender13_backgroundElement"
                    Enabled="True" DropShadow="true" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="pnlServiceTypeTakeIn" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label4" Text="Service Type" Font-Bold="True"></asp:Label>

                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2" class="radioButtonList">
                                <asp:RadioButtonList ID="rdlServiceTypeTakeIn" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rdbServiceTypeTakeIn_IndexChanged"
                                    CellPadding="3" CellSpacing="0" RepeatColumns="3" Font-Size="X-Small" DataValueField="ServiceTypeID"
                                    DataTextField="ServiceTypeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="pnlServiceTypeTakeIn"
                    TargetControlID="TabContainer1$TabPanel1$btnServiceTypeTakeIn" CancelControlID="ctl00_SampleContent_ModalPopupExtender4_backgroundElement"
                    Enabled="True" DropShadow="True">
                </ajaxToolkit:ModalPopupExtender>
                <%--Take In End--%>

                <%--Give Out Start--%>
                <asp:Panel runat="server" ID="pnlDepartureSettingGiveOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabelGiveOut">
                                <asp:Label ID="Label37" runat="server" Text="Departure Setting" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="Button7" runat="server" Text="X" class="btn btn-danger" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblVendorDepartureSettingGiveOut" Text="" Font-Bold="true"> </asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label39" Text="Scan Bag/Receipt Number" Font-Bold="true"> </asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlVendorDepartureSettingGiveOut" runat="server" DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorDepartureSettingGiveOut_OnSelectedIndexChanged" Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVendorDepartureSettingGiveOut" runat="server" Width="70px" Height="20px" OnTextChanged="txtVendorDepartureSettingGiveOut_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="float: right;">
                                        <asp:Button ID="Button8" runat="server" Text="Set" CssClass="btn btn-info" OnClick="btnSetDepartureSettingGiveOut_OnClick" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>

                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender15" runat="server" PopupControlID="pnlDepartureSettingGiveOut" TargetControlID="TabContainer1$TabPanel2$btnDepartureSettingGiveOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender15_backgroundElement"
                    Enabled="True" DropShadow="true" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" ID="pnlMessengerLookupGiveOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label22" Text="Messenger" Font-Bold="true"> </asp:Label>
                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField runat="server" ID="hdnMessengerLookupGiveOut"></asp:HiddenField>
                                <asp:RadioButtonList ID="rblMessengerLookupGiveOut" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rblMessengerLookupGiveOut_Changed"
                                    CellPadding="0" CellSpacing="0" RepeatColumns="2" Font-Size="X-Small" DataValueField="PersonId"
                                    DataTextField="DisplayName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="Label24" runat="server" Text="No Messenger available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender9" runat="server" PopupControlID="pnlMessengerLookupGiveOut" TargetControlID="TabContainer1$TabPanel2$btnMessengerLookupGiveOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender9_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlCaptureSignGiveOut" CssClass="modal-dialog" Style="display: none;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel11">
                                <asp:Button Style="float: right" ID="Button3" runat="server" Text="X" class="btn btn-danger" CausesValidation="false" OnClientClick="document.getElementById('ctl00_SampleContent_ModalPopupExtender15_foregroundElement').style.display = 'none';" />
                                <asp:Label ID="Label6" runat="server" Text="Signature Capture" Style="float: none; padding-top: 5px;"></asp:Label>
                            </div>
                        </div>
                        <div class="modal-body">

                            <div id="dvSignaturepadGiveOut" class="m-signature-pad">
                                <div class="m-signature-pad--body">
                                    <canvas style="background-color: white;" width="400" height="300"></canvas>
                                </div>

                                <div class="signature-pad--footer">
                                    <div class="signature-pad--actions">
                                        <div align="center">
                                            <button type="button" class="button clear" data-action="clear">Clear</button>
                                            <button type="button" class="button" data-action="undo">Undo</button>
                                            <button id="btnSave" type="button" class="button save" data-action="save-png">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnimgSignatureGiveOut" runat="server" Value="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" />
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender8" runat="server" PopupControlID="pnlCaptureSignGiveOut" TargetControlID="TabContainer1$TabPanel2$btnCaptureSignGiveOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender8_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
                <%--Give Out End--%>

                <%--Ship Recieving Start--%>
                <asp:Panel runat="server" ID="pnlDepartureSettingShipRecieving" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel">
                                <asp:Label ID="Label25" runat="server" Text="Departure Setting" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="Button2" runat="server" Text="X" class="btn btn-danger" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblCustomerNameDepartureSettingShipRecieving" Text="" Font-Bold="true"> </asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label29" Text="Scan Bag/Receipt Number" Font-Bold="true"> </asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlVendorDepartureSettingShipRecieving" runat="server" DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorDepartureSettingShipRecieving_OnSelectedIndexChanged" Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVendorDepartureSettingShipRecieving" runat="server" Width="70px" Height="20px" OnTextChanged="txtVendorDepartureSettingShipRecieving_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="float: right;">
                                        <asp:Button ID="btnSetDepartureSettingShipRecieving" runat="server" Text="Set" CssClass="btn btn-info" OnClick="btnSetDepartureSettingShipRecieving_OnClick" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>

                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender7" runat="server" PopupControlID="pnlDepartureSettingShipRecieving" TargetControlID="TabContainer1$TabPanel3$btnDepartureSettingShipRecieving" CancelControlID="ctl00_SampleContent_ModalPopupExtender7_backgroundElement"
                    Enabled="True" DropShadow="true" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" ID="pnlCarriersShipReceiving" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label18" Text="Carriers" Font-Bold="true"> </asp:Label>
                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlCarriersShipReceiving" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rdlCarriersShipReceiving_Changed"
                                    CellPadding="2" CellSpacing="0" RepeatColumns="4" Font-Size="X-Small" DataValueField="ID"
                                    DataTextField="Name" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="Label19" runat="server" Text="No Carriers available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender6" runat="server" PopupControlID="pnlCarriersShipReceiving" TargetControlID="TabContainer1$TabPanel3$btnCarriersShipReceiving" CancelControlID="ctl00_SampleContent_ModalPopupExtender6_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>


                <asp:Panel runat="server" ID="pnlServiceTypeShipReceiving" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label20" Text="Service Type" Font-Bold="True"></asp:Label>

                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td colspan="2" class="radioButtonList">
                                <asp:HiddenField runat="server" ID="HiddenField1"></asp:HiddenField>
                                <asp:RadioButtonList ID="rdlServiceTypeShipReceiving" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rdlServiceTypeShipReceiving_IndexChanged"
                                    CellPadding="3" CellSpacing="0" RepeatColumns="3" Font-Size="X-Small" DataValueField="ServiceTypeID"
                                    DataTextField="ServiceTypeName" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlServiceTypeShipReceiving"
                    TargetControlID="TabContainer1$TabPanel3$btnServiceTypeShipReceiving" CancelControlID="ctl00_SampleContent_ModalPopupExtender3_backgroundElement"
                    Enabled="True" DropShadow="True">
                </ajaxToolkit:ModalPopupExtender>

                <%--Ship Recieving End--%>

                <%--Ship Out Strat--%>
                <asp:Panel runat="server" ID="pnlDepartureSettingShipOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6);">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLabel">
                                <asp:Label ID="Label36" runat="server" Text="Departure Setting" Style="float: left; padding-top: 5px;"></asp:Label>
                                <asp:Button Style="float: right" ID="Button9" runat="server" Text="X" class="btn btn-danger" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label38" Text="" Font-Bold="true"> </asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label40" Text="Scan Bag/Receipt Number" Font-Bold="true"> </asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlVendorDepartureSettingShipOut" runat="server" DataTextField="CustomerName" DataValueField="CustomerId" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorDepartureSettingShipOut_OnSelectedIndexChanged" Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVendorDepartureSettingShipOut" runat="server" Width="70px" Height="20px" OnTextChanged="txtVendorDepartureSettingShipOut_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="float: right;">
                                        <asp:Button ID="btnSetDepartureSettingShipOut" runat="server" Text="Set" CssClass="btn btn-info" OnClick="btnSetDepartureSettingShipOut_OnClick" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>

                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender16" runat="server" PopupControlID="pnlDepartureSettingShipOut" TargetControlID="TabContainer1$TabPanel4$btnDepartureSettingShipOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender16_backgroundElement"
                    Enabled="True" DropShadow="true" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" ID="pnlCarriersShipOut" CssClass="modal-dialog" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="Label34" Text="Messenger" Font-Bold="true"> </asp:Label>
                            </td>
                            <td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdlCarriersShipOut" runat="server" RepeatDirection="Vertical"
                                    BorderStyle="None" Style="margin-left: 5px" AutoPostBack="True" OnSelectedIndexChanged="rdlCarriersShipOut_Changed"
                                    CellPadding="2" CellSpacing="0" RepeatColumns="4" Font-Size="X-Small" DataValueField="ID"
                                    DataTextField="Name" Visible="true" class="btn btn-info" CssClass="radioButtonList">
                                </asp:RadioButtonList>
                                <asp:Label ID="Label35" runat="server" Text="No Messenger available." Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender14" runat="server" PopupControlID="pnlCarriersShipOut" TargetControlID="TabContainer1$TabPanel4$btnCarriersShipOut" CancelControlID="ctl00_SampleContent_ModalPopupExtender14_backgroundElement"
                    Enabled="True" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>
              <%--  <iframe id="iFramePlaceOrder" runat="server" src="" width="0" height="0" style="border: none; visibility: hidden;"></iframe>--%>

                <%--  <asp:Panel ID="pnlPrintReciept" runat="server" CssClass="modal-dialog"  >

                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title" id="exampleModalLabel20">
                                    <asp:Label ID="Label56" runat="server" Text="Place an Order" Style="float: left; padding-top: 5px;"></asp:Label>
                                    <asp:Button Style="float: right; margin-bottom:0px;" ID="btnClosePlaceOrder" runat="server" Text="X" class="btn btn-danger" />
                                </div>
                            </div>
                            <div class="modal-body">
                               
                            </div>
                           
                        </div>
                    </asp:Panel>
				 <ajaxToolkit:ModalPopupExtender runat="server" PopupControlID="pnlPrintReciept" TargetControlID="TabContainer1$TabPanel1$btnViewRecieptTakeIn"
                        CancelControlID="btnClosePlaceOrder" BackgroundCssClass="modalBackground" Enabled="True">
                    </ajaxToolkit:ModalPopupExtender>--%>
                <%--Ship Out End--%>

				</div>
				
            </ContentTemplate>


        </asp:UpdatePanel>

    </div>
</asp:Content>
