using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Corpt
{
	/// <summary>
	/// Summary description for TrackingIIBatchDetail.
	/// </summary>
	public partial class TrackingIIBatchDetail : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			else
			{
				if(!IsPostBack)
				{
					HyperLink1.NavigateUrl = Request.Params["HTTP_REFERER"].ToString();
				}

				SqlCommand command = new SqlCommand("sp_GetWebPermissions");
				
				command.Connection=new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType=CommandType.StoredProcedure;
			
				command.Parameters.Add("@UserID", Session["ID"].ToString());

				DataTable dtPermission = new DataTable();
				SqlDataAdapter da = new SqlDataAdapter(command);
				da.Fill(dtPermission);
				if(dtPermission.Select("LinkID = 21").Length>0)
				{
					DisplayBatchDetail();
				}
				else
				{
					string strBack = Request.Params["HTTP_REFERER"].ToString();
					Response.Redirect(strBack);
				}				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void DisplayBatchDetail()
		{
			string strBatchNumber = "";
			
			try
			{
				strBatchNumber = Request.Params["BatchNumber"];
				lblInfo.Text="";
			}
			catch(Exception ex)
			{
				lblInfo.Text = "Not enough parameters: "+ex.Message;
				dgBatchDetail.DataSource = null;
				dgBatchDetail.DataBind();
				return;
			}

            Utils.DissectItemNumber(strBatchNumber, out string strGroupCode, out var strBatchCode, out _);

			SqlCommand command = new SqlCommand("spGetBatchByCode");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@CustomerCode", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BGroupState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EGroupState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EState", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@BDate", DBNull.Value));
			command.Parameters.Add(new SqlParameter("@EDate", DBNull.Value));

			command.Parameters.Add(new SqlParameter("@GroupCode", strGroupCode));
			command.Parameters.Add(new SqlParameter("@BatchCode", strBatchCode));

			command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);

			if(dt.Rows.Count == 1)
			{
				int intBatchID = 0;
				intBatchID = int.Parse(dt.Rows[0]["BatchID"].ToString());
				lblInfo.Text = strBatchNumber;
				GetAndDisplayDetails(intBatchID);
			}
			else
			{
				lblInfo.Text="No history for batch "+strBatchNumber;
			}

		}

		private void GetAndDisplayDetails(int intBatchID)
		{
			SqlCommand command = new SqlCommand("trkSpGetBatchHistoryDetails");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@BatchID", intBatchID));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);

			if(dt.Rows.Count>0)
			{
				lblInfo.Text+=" - " + dt.Rows[0]["NumberOfItemsInBatch"].ToString() + " items.";

				DataTable dtDisplay = new DataTable();
			
				dtDisplay.Columns.Add(new DataColumn("Time Stamp"));
				dtDisplay.Columns.Add(new DataColumn("Action"));
				dtDisplay.Columns.Add(new DataColumn("Form"));
				dtDisplay.Columns.Add(new DataColumn("First Name"));
				dtDisplay.Columns.Add(new DataColumn("Last Name"));
				dtDisplay.Columns.Add(new DataColumn("Items Affected"));

				dtDisplay.Columns["Time Stamp"].DataType = System.Type.GetTypeFromHandle(Type.GetTypeHandle(new DateTime()));
			
				dtDisplay.AcceptChanges();


				dtDisplay.AcceptChanges();

				foreach(DataRow dr in dt.Rows)
				{
					DataRow drDisplay = dtDisplay.NewRow();
					drDisplay["Time Stamp"]=DateTime.Parse(dr["RecordTimeStamp"].ToString());
					drDisplay["Action"]=dr["EventName"].ToString();
					drDisplay["Form"]=dr["VewAccessName"].ToString();
					drDisplay["First Name"]=dr["FirstName"].ToString();
					drDisplay["Last Name"]=dr["LastName"].ToString();
					drDisplay["Items Affected"]=dr["NumberOfItemsAffected"].ToString();
					dtDisplay.Rows.Add(drDisplay);
				}

				dgBatchDetail.DataSource = dtDisplay;
				dgBatchDetail.DataBind();

			}
			else
			{
				lblInfo.Text+=" No tracking records.";
			}


		}
	}
}
