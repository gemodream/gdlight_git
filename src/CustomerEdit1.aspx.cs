﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CustomerEdit1 : CommonPage
    {
		#region Page Load
		
		protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Customer Maintenance";
            if (IsPostBack)
            {
                LoadMovmentList();
                LoadCompanyMethod();
                LoadCompanyPermissions();

                LoadPersonMethod();
                LoadPersonPermissions();
				LoadPersonServices();
				BindShippingCourierGrid();
				BindDeliveryMethodGrid();

				Page.Validate(ValidatorGroupCompany);
                Page.Validate(ValidatorGroupPerson);
                return;
            }
          
            if (!IsPostBack)
            {
                //-- Customers List
                LoadCustomers();

				var country = QueryCustomerUtils.GetCountry(this);
                ddlCustomerCountry.DataSource = country;
                ddlCustomerCountry.DataBind();
                ddlCustomerCountry.Items.Insert(0,"");

                repCustomerCountry.DataSource = country;
                repCustomerCountry.DataBind();

                ddlPersonCountry.DataSource = country;
                ddlPersonCountry.DataBind();
                ddlPersonCountry.Items.Insert(0, "");

                repPersonCountry.DataSource = country;
                repPersonCountry.DataBind();

                //-- Business Type
                var BusinessType=QueryCustomerUtils.GetBusinessTypes(this);
                ddlCustomerBusinessType.DataSource = BusinessType;
                ddlCustomerBusinessType.DataBind();

                BusinessType.RemoveAll(r => r.BusinessTypeId == "");
                repCustomerBusinessType.DataSource = BusinessType;
                repCustomerBusinessType.DataBind();
                
               //-- Permissions
                CompanyPermissionsList.DataSource = new PermissionsModel().Items;
                CompanyPermissionsList.DataBind();
                
                cblPersonPermissionsList.DataSource = new PermissionsModel().Items;
                cblPersonPermissionsList.DataBind();

				//-- Goods Movement
				DataTable deliveryMethod = SyntheticScreeningUtils.GetSyntheticDeliveryMethod(this);
				rblMovementsList.DataSource = deliveryMethod;
				rblMovementsList.DataBind();
				
				//-- Carriers
				var carriers=QueryCustomerUtils.GetCarriers(this);
                ddlCarrierList.DataSource = carriers;
                ddlCarrierList.DataBind();

                carriers.RemoveAll(r => r.Name == "");
                repCarrier.DataSource = carriers;
                repCarrier.DataBind();

                //-- Preferred Methods of Communication
                SetTreeMethodValue("f0p0e0m0", CompanyMethods);
                SetTreeMethodValue("f0p0e0m0", PersonMethods);

				//--Person Services
				var personsServices = QueryCustomerUtils.GetPersonsServicesList(this);
				cblPersonServicesList.DataSource = personsServices;
				cblPersonServicesList.DataBind();

				//-- Person Position Types
				var positions = QueryCustomerUtils.GetPositions(this);
                ddlPersonPosition.DataSource = positions;
                ddlPersonPosition.DataBind();

                positions.RemoveAll(r => r.Name == "");
                repPersonPosition.DataSource = positions;
                repPersonPosition.DataBind();

                CompanyDetailsPanel.Visible = false;
                PersonListPanel.Visible = false;
                PersonDetailsPanel.Visible = false;
                CarrierDetailsPanel.Visible = false;

                PersonUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this person information?');");
                PersonDeleteBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this person information?');");
                CustomerUpdateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to save this customer information?');");

               CustomerLike.Attributes.Add("onKeyUp", "FilterShapes(this)");
               CustomerLike.Attributes.Add("onFocus", "FilterShapes(this)");

                CustomerLike.Focus();

                if (Request.QueryString["CustomerCode"] != null)
                {
                    CustomerLike.Text = Request.QueryString["CustomerCode"].ToString();
                    OnCustomerLikeClick(null, null);
                }

				CustomerLike.Focus();
				
			}
        }
	
		private void LoadCustomers()
        {

            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);//alex 11102022
            customers = customers.Where(x => !excluded.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
            SetViewState(customers, SessionConstants.CustomersList);

			lstCustomerList.Items.Clear();
			lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();

			DropDownListBox.Items.Clear();
			DropDownListBox.DataSource = customers;
            DropDownListBox.DataBind();
        }

        protected void OnDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = sender as ListBox;
            CustomerLike.Text = DropDownListBox.SelectedItem.Text;

			var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
			lstCustomerList.Items.Clear();
			lstCustomerList.DataSource = customers;
			lstCustomerList.DataBind();

			lstCustomerList.ClearSelection();
            lstCustomerList.Items.FindByValue(DropDownListBox.SelectedItem.Value).Selected = true;

            HideCustomerDetails();
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;

            var customer = GetCustomerFromView(lstCustomerList.SelectedValue);
            if (customer == null) return;
            HistoryRef.HRef = "~/CustomerHistory.aspx?CustomerCode=" + customer.CustomerCode;
            var infoModel = QueryCustomerUtils.GetCustomerExt(customer, this);

            SetViewState(infoModel, SessionConstants.CustomerEditDataExt);
            if (infoModel == null) return;

            SetCompanyInfo(infoModel);
			
			
			CustomerUpdateBtn.Enabled = true;
			MessageLabel.Text = "";
			CarrierUpdateBtn.Enabled = true;
			UpdateCarrierLabel.Text = "";
			PersonUpdateBtn.Enabled = true;
			UpdatePersonLabel.Text = "";

		}
        protected void OnTextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            bool changed = false;

            foreach (ListItem item in DropDownListBox.Items)
            {
				string custNumbers = new String(textBox.Text.Where(Char.IsDigit).ToArray());
				if (item.Text.Contains(custNumbers==""?"$": custNumbers))
				{
						DropDownListBox.SelectedValue = item.Value;
						CustomerLike.Text = CustomerLike.Text;
						changed = true;
						break;
                }
            }

            if (!changed)
            {
                if (DropDownListBox.SelectedItem != null)
                {
                    textBox.Text = DropDownListBox.SelectedItem.Text;
                }
                else
                {
                    textBox.Text = "";
                }
            }
        }
        #endregion

        #region Search Customer
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            HideCustomerDetails();
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);


            string AuthorOfficeID = this.Session["AuthorOfficeID"].ToString();
			lstCustomerList.DataSource = filtered.ToList();

            if (filtered.Count == 1) lstCustomerList.SelectedValue = filtered[0].CustomerId;
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            HideCustomerDetails();
            if (string.IsNullOrEmpty(lstCustomerList.SelectedValue)) return;
            
            var customer = GetCustomerFromView(lstCustomerList.SelectedValue);
            if (customer == null) return;
            HistoryRef.HRef = "~/CustomerHistory.aspx?CustomerCode=" + customer.CustomerCode;
            var infoModel = QueryCustomerUtils.GetCustomerExt(customer, this);

            SetViewState(infoModel, SessionConstants.CustomerEditDataExt);
            if (infoModel == null) return;
            
            SetCompanyInfo(infoModel);
            
        }
        private CustomerModel GetCustomerFromView(string customerId)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ??
                            new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);
        }
        private void HideCustomerDetails()
        {
            MessageLabel.Text = "";
            CompanyDetailsPanel.Visible = PersonDetailsPanel.Visible = PersonListPanel.Visible = false;
        }
        #endregion

        #region Company Details Panel
        private CustomerEditModelExt GetCompanyInfo()
        {
            var customerModel = GetViewState(SessionConstants.CustomerEditDataExt) as CustomerEditModelExt ??
                                    new CustomerEditModelExt();
            
            var infoModel = new CustomerEditModelExt();
            infoModel.StateId = customerModel.StateId;
            infoModel.StateTargetId = customerModel.StateTargetId;
            infoModel.CustomerOfficeId = customerModel.CustomerOfficeId;
            infoModel.CustomerHistoryId = customerModel.CustomerHistoryId;
            infoModel.CustomerId = customerModel.CustomerId;
            infoModel.CustomerCode = customerModel.CustomerCode;
            infoModel.Company.Address.AddressId = customerModel.Company.Address.AddressId;

            infoModel.Company.CompanyName = CompanyName.Text.Trim();
            infoModel.Company.ShortName = ShortName.Text.Trim();
            infoModel.Company.Address.Address1 = CompanyAddress1.Text.Trim();
            infoModel.Company.Address.Address2 = CompanyAddress2.Text.Trim();

            infoModel.Company.Address.Zip1 = CompanyZip1.Text.Trim();
            infoModel.Company.Address.Zip2 = CompanyZip2.Text.Trim();
            infoModel.Company.Address.City = CompanyCity.Text.Trim();
            infoModel.Company.Address.Country = ddlCustomerCountry.SelectedItem.Text;

            infoModel.Company.Address.UsStateId = ddlCustomerState.SelectedValue;

            infoModel.Company.Address.CountryPhoneCode = CompanyPhoneCode.Text;
            infoModel.Company.Address.Phone = CompanyPhone.Text.Trim();

            infoModel.Company.Address.CountryFaxCode = CompanyFaxCode.Text;
            infoModel.Company.Address.Fax = CompanyFax.Text.Trim();

            infoModel.Company.Address.Email = CompanyEmail.Text.Trim();
            infoModel.Company.Communication = GetTreeMethodValue(CompanyMethods);
            infoModel.Company.Permissions = GetCheckBoxListValue(CompanyPermissionsList);
            infoModel.Company.IndustryMembership = "";

            infoModel.Company.BusinessTypeId = ddlCustomerBusinessType.SelectedValue;
            infoModel.Company.GoodsMovement = GetGoodsMovement();
            infoModel.Company.UseTheirAccount = UseAccountFlag.Checked;
            infoModel.Company.Account = txtCarrierAccount.Text.Trim();
            infoModel.Company.CarrierId = ddlCarrierList.SelectedValue;
            return infoModel;
        }

        private void SetCompanyInfo(CustomerEditModelExt infoModel)
        {

            CompanyDetailsPanel.Visible = true;
            
            CompanyName.Text = infoModel.Company.CompanyName;
            ShortName.Text = infoModel.Company.ShortName;
            CompanyAddress1.Text = infoModel.Company.Address.Address1;
            CompanyAddress2.Text = infoModel.Company.Address.Address2;
            CompanyCity.Text = infoModel.Company.Address.City;
                                 
            if (ddlCustomerCountry.Items.FindByText(infoModel.Company.Address.Country) != null)
            {
                ddlCustomerCountry.ClearSelection();
                ddlCustomerCountry.Items.FindByText(infoModel.Company.Address.Country).Selected = true;
                txtCustomerCountry.Text = infoModel.Company.Address.Country;
                ddlCustomerCountry_SelectedIndexChanged(this, EventArgs.Empty);

                if (ddlCustomerState.Items.FindByValue(infoModel.Company.Address.UsStateId) != null)
                {
                    ddlCustomerState.SelectedValue = infoModel.Company.Address.UsStateId;
                    txtCustomerState.Text = ddlCustomerState.SelectedItem.Text.ToString();
                }
                else
                {
                    ddlCustomerState.SelectedIndex = -1;
                    
                }                                
                OnCompanyCountryChanging(null, null);
            }
            else
            {
                ddlCustomerCountry.SelectedIndex = -1;
                ddlCustomerState.Items.Clear();
                btnCustomerState.Text = "State";
                CompanyPhoneCode.Text = "";                
                CompanyFaxCode.Text = "";
                txtCustomerCountry.Text = "";
                txtCustomerState.Text = "";
            }
            
            CustomerCode.Text = infoModel.CustomerCode;
            CustomerId.Text = infoModel.CustomerId;
            CustomerStartDate.Text = String.Format("{0:G}", infoModel.CreateDate);
            if (!string.IsNullOrEmpty(infoModel.Company.BusinessTypeId))
            {
                ddlCustomerBusinessType.SelectedValue = infoModel.Company.BusinessTypeId;
                txtCustomerBusinessType.Text = ddlCustomerBusinessType.SelectedItem.Text.ToString();
            } else
            {
                ddlCustomerBusinessType.SelectedIndex = -1;
                txtCustomerBusinessType.Text = "";
            }
            
            CompanyZip1.Text = infoModel.Company.Address.Zip1;
            CompanyZip2.Text = infoModel.Company.Address.Zip2;

            CompanyPhoneCode.Text = infoModel.Company.Address.CountryPhoneCode;
            CompanyPhone.Text =infoModel.Company.Address.Phone;

            CompanyFaxCode.Text = infoModel.Company.Address.CountryFaxCode;
            CompanyFax.Text = infoModel.Company.Address.Fax;
            CompanyEmail.Text = infoModel.Company.Address.Email;

            LoadCheckBoxList(infoModel.Company.Permissions, CompanyPermissionsList);
            

            SetGoodsMovement(infoModel.Company.GoodsMovement);
            UseAccountFlag.Checked = infoModel.Company.UseTheirAccount;
            OnCheckedMovement(null, null);

            if (!string.IsNullOrEmpty(infoModel.Company.CarrierId))
            {
                ddlCarrierList.SelectedValue = infoModel.Company.CarrierId;
                txtCarrierList.Text = ddlCarrierList.SelectedItem.Text.ToString();
            } else
            {
                ddlCarrierList.SelectedIndex = -1;
            }
            txtCarrierAccount.Text = infoModel.Company.Account;
            SetTreeMethodValue(infoModel.Company.Communication, CompanyMethods);

            //-- Load Persons List
            PersonsGrid.DataSource = infoModel.Persons;
            PersonsGrid.DataBind();
            PersonListPanel.Visible = true;

            //-- Load Carrier List
            CarrierGrid.DataSource = infoModel.Company.Carriers;
            CarrierGrid.DataBind();
            CarrierGrid.Visible = true;

            if (infoModel.Persons.Count > 0)
            {
                SetPersonInfo(infoModel.Persons[0]);
            }
            if (infoModel.Company.Carriers.Count > 0)
            {
                SetCarrierInfo(infoModel.Company.Carriers[0]);
            }
			else
			{
				CarrierDetailsPanel.Visible = false;
			}
            NewPersonButton.Enabled = !string.IsNullOrEmpty(infoModel.CustomerId);
            Page.Validate("ValGrpCompany");
            MessageLabel.Text = "";

            LoadCompanyPermissions();
            LoadCompanyMethod();
            LoadMovmentList();

            SetKYCCompanyInfo(infoModel);
        }
		#endregion

		#region Company Country
			protected void OnCompanyCountryChanging(object sender, EventArgs e)
        {
            ddlCustomerState.Enabled = true;
            CompUsStateValidator.Enabled = true;
        }
        #endregion

        #region CheckBoxList Utils
        private void LoadCheckBoxList(string values, CheckBoxList control)
        {
            foreach (ListItem item in control.Items)
            {
                item.Selected = false;
            }
            if (string.IsNullOrEmpty(values)) return;
            var keys = values.Split(',');
            foreach (ListItem item in control.Items)
            {
                foreach (var key in keys)
                {
                    if (key == item.Value) item.Selected = true;
                }
            }
        }
        private string GetCheckBoxListValue(CheckBoxList control)
        {
            var value = "";
            foreach (ListItem item in control.Items)
            {
                if (item.Selected) value += item.Value + ",";
            }
            return value;
        }
        #endregion

        #region Goods Movement
        private void SetGoodsMovement(GoodsMovementModel movement)
        {
            foreach (ListItem item in rblMovementsList.Items)
            {
                item.Selected = false;
            }
            if (movement.WeCarry) rblMovementsList.SelectedValue = "0";
            if (movement.TheyCarry) rblMovementsList.SelectedValue = "1";
            if (movement.WeShipCarry) rblMovementsList.SelectedValue = "2";
            HiddenMovements.Text = movement.HasChecked ? "Y" : "";
        }
        protected void OnCheckedMovement(object sender, EventArgs e)
        {
            var isShip = (rblMovementsList.SelectedValue == "2");
            UseAccountFlag.Enabled = isShip;
            if (!isShip)
            {
                UseAccountFlag.Checked = false;
            }
            OnUseAccountChecked(null, null);

            HiddenMovements.Text = GetGoodsMovement().HasChecked ? "Y" : "";
            LoadMovmentList();
        }
        private GoodsMovementModel GetGoodsMovement()
        {
            var movement = new GoodsMovementModel();
            foreach (ListItem item in rblMovementsList.Items)
            {
                if (!item.Selected) continue;
                if (item.Value == "0") movement.WeCarry = true;
                if (item.Value == "1") movement.TheyCarry = true;
                if (item.Value == "2") movement.WeShipCarry = true;
            }
            return movement;
        }
        #endregion

        #region Use Account Flag
        protected void OnUseAccountChecked(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Company: Methods of Communication
        protected void OnCompanyMethodsCheckChanged(object sender, TreeNodeEventArgs e)
        {
            OnTreeMethodCheck(CompanyMethods);
        }

        protected void OnCompanyDownClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(false, CompanyMethods);
        }

        protected void OnCompanyUpClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(true, CompanyMethods);
        }
        #endregion

        #region Person: Methods of Communication
        protected void OnPersonMethodsCheckChanged(object sender, TreeNodeEventArgs e)
        {
            OnTreeMethodCheck(PersonMethods);
        }

        protected void OnPersonDownClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(false, PersonMethods);
        }

        protected void OnPersonUpClick(object sender, ImageClickEventArgs e)
        {
            ReorderMethods(true, PersonMethods);
        }
        #endregion

        #region TreeView Methods Utils
        private static void LoadTreeMethods(List<CommunicationModel> items, TreeView tree)
        {
            items.Sort((m1, m2) => m1.Order.CompareTo(m2.Order));
            tree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Communications" } };
            data.AddRange(items.Select(item => new TreeViewModel
            {
                ParentId = "0",
                Id = item.Code,
                DisplayName = item.Name,
            }));

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tree.Nodes.Add(rootNode);
            foreach (TreeNode node in rootNode.ChildNodes)
            {
                var src = items.Find(m => m.Code == node.Value && m.Checked);
                node.Checked = src != null;
            }

        }
        private static void ReorderMethods(bool up, TreeView tree)
        {
            if (tree.SelectedNode == null || tree.SelectedNode.Value == "0") return;
            var nowInd = 0;
            var items = new List<CommunicationModel>();
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                if (node.Value == tree.SelectedValue) nowInd = i;
                items.Add(new CommunicationModel { Checked = node.Checked, Code = node.Value, Name = node.Text, Order = i });
            }
            if (up && nowInd == 0) return;
            if (!up && nowInd == (items.Count - 1)) return;
            var newInd = up ? (nowInd - 1) : (nowInd + 1);
            var item1 = items.Find(m => m.Order == nowInd);
            var item2 = items.Find(m => m.Order == newInd);
            if (item1 != null) item1.Order = newInd;
            if (item2 != null) item2.Order = nowInd;
            LoadTreeMethods(items, tree);
            tree.Nodes[0].ChildNodes[newInd].Selected = true;
        }
        private static void OnTreeMethodCheck(TreeView tree)
        {
            if (tree.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tree.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        private static string GetTreeMethodValue(TreeView tree)
        {
            var result = "";
            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
            {
                var node = tree.Nodes[0].ChildNodes[i];
                result += node.Value + (node.Checked ? "1" : "0");
            }
            return result;
        }
        private static void SetTreeMethodValue(string value, TreeView tree)
        {
            var communications = new CommunicationsModel();
            communications.SetDbValue(value);
            LoadTreeMethods(communications.Items, tree);
        }
        #endregion

        #region Persons
        protected void OnAddPersonClick(object sender, EventArgs e)
        {
            var customer = GetCustomerShort();

            var person = new PersonExModel { Customer = customer };
            SetPersonInfo(person);
            PersonFirstName.Focus();
            ddlPersonState.Items.Clear();
            ddlPersonState.DataBind();
			PersonUpdateBtn.Enabled = true;
			UpdatePersonLabel.Text = "";
		}

        protected void OnAddCarrierClick(object sender, EventArgs e)
        {
            CarrierDetailsPanel.Visible = true;

            txtCarrierAccount.Enabled = true;
            ddlCarrierList.Enabled = true;
            LowerLimit.Enabled = true;
            UpperLimit.Enabled = true;
            CarrierValidator.Enabled = true;
            AccountValidator.Enabled = true;
            btnCarrier.Enabled = true;
            txtCarrierAccount.Text = "";
            ddlCarrierList.SelectedIndex = -1;
            LowerLimit.Text = "";
            UpperLimit.Text = "";
            txtCarrierList.Text = "";
			UpdateCarrierLabel.Text = "";
			CarrierUpdateBtn.Enabled = true;
			CarrierDeleteBtn.Enabled = false;
		}

        private CustomerModel GetCustomerShort()
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            var customer = customers.Find(m => m.CustomerId == CustomerId.Text);
            if (customer != null) return customer;
            //-- Refresh short Customer List
            LoadCustomers();
            lstCustomerList.SelectedValue = CustomerId.Text;
            customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == CustomerId.Text) ?? new CustomerModel();
        }

        protected void OnPersonItemDataBound(object sender, DataGridItemEventArgs e)
        {
        }

        protected void OnViewPersonCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + PersonsGrid.DataKeys[e.Item.ItemIndex]; 

            var customer = QueryCustomerUtils.GetCustomerExt(GetCustomerShort(), this);

            var person = customer.Persons.Find(m => ("" + m.PersonId) == uniqueKey);
            if (person != null) SetPersonInfo(person);

			PersonUpdateBtn.Enabled = true;
			UpdatePersonLabel.Text = "";
		}

        protected void OnViewCarrierCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + CarrierGrid.DataKeys[e.Item.ItemIndex];
            //-- Load Person Details Panel
            var customer = QueryCustomerUtils.GetCustomerExt(GetCustomerShort(), this);
            var Account = e.Item.Cells[3].Text;
            var carrier = customer.Company.Carriers.Find(m => ("" + m.CarrierID) == uniqueKey && m.Account== Account);
            if (carrier != null) SetCarrierInfo(carrier);

			UpdateCarrierLabel.Text = "";
			CarrierUpdateBtn.Enabled = true;
		}
        
        #endregion

        #region Carrier Detail Panel

        private LimitCarrierModel GetCarrierInfo()
        {
            var exists = !string.IsNullOrEmpty(ddlCarrierList.SelectedValue);
            var limitCarrierModel = new LimitCarrierModel();
            if (exists)
            {
                var customerModel = GetViewState(SessionConstants.CustomerEditData) as CustomerEditModelExt ??
                                    new CustomerEditModelExt();
                var carrier = customerModel.Company.Carriers.Find(m => m.CarrierID == ddlCarrierList.SelectedValue);
                if (carrier != null)
                {
                    limitCarrierModel.CarrierID = carrier.CarrierID;
                    limitCarrierModel.Account = carrier.Account;
                    limitCarrierModel.LowerLimit = carrier.LowerLimit;
                    limitCarrierModel.UpperLimit = carrier.UpperLimit;
                }
            }

            limitCarrierModel.CarrierID = ddlCarrierList.SelectedValue;
            limitCarrierModel.Account = txtCarrierAccount.Text;
            limitCarrierModel.LowerLimit = LowerLimit.Text;
            limitCarrierModel.UpperLimit = UpperLimit.Text;
            return limitCarrierModel;
        }
        private void SetCarrierInfo(LimitCarrierModel limitCarrierModel)
        {
			CarrierDeleteBtn.Enabled = Convert.ToInt16(limitCarrierModel.CarrierID) > 0;
			CarrierDetailsPanel.Visible = true;
            ddlCarrierList.SelectedValue= limitCarrierModel.CarrierID;
            txtCarrierList.Text = ddlCarrierList.SelectedItem.Text.ToString();
            txtCarrierAccount.Text= limitCarrierModel.Account;
            LowerLimit.Text= limitCarrierModel.LowerLimit;
            UpperLimit.Text= limitCarrierModel.UpperLimit;
            Page.Validate("ValGrpCarrier");
        }
            #endregion

        #region Person Details Panel
        private PersonExModel GetPersonInfo()
        {
            var exists = !string.IsNullOrEmpty(txtPersonCode.Text);
            var infoModel = new PersonExModel();
            if (exists)
            {
                var customerModel = GetViewState(SessionConstants.CustomerEditDataExt) as CustomerEditModelExt ??
                                   new CustomerEditModelExt();
                var person = customerModel.Persons.Find(m => m.PersonCode == txtPersonCode.Text);

                if (person != null)
                {
                    infoModel.PersonId = person.PersonId;
                    infoModel.PersonHistoryId = person.PersonHistoryId;
                    infoModel.Address.AddressId = person.Address.AddressId;
                    infoModel.CreateDate = person.CreateDate;
                    infoModel.WebLogin = person.WebLogin;
                }
            }

            infoModel.WebLogin.LoginName = WebLogin.Text.Trim();
            infoModel.WebLogin.Password = WebPassword.Text.Trim();
            infoModel.Customer = GetCustomerShort();

            infoModel.PersonCode = txtPersonCode.Text;
            infoModel.PersonId = Convert.ToInt32( hdnPersonID.Value);
            
            infoModel.FirstName = PersonFirstName.Text.Trim();
            infoModel.LastName = PersonLastName.Text.Trim();
            if (CalendarExt.SelectedDate != null)
            {
                infoModel.BirthDate = CalendarExt.SelectedDate;
            }
            infoModel.Address.Address1 = PersonAddress1.Text.Trim();
            infoModel.Address.Address2 = PersonAddress2.Text.Trim();
            infoModel.Address.Zip1 = PersonZip1.Text.Trim();
            infoModel.Address.Zip2 = PersonZip2.Text.Trim();
            infoModel.Address.City = PersonCity.Text.Trim();
            infoModel.Address.Country = ddlPersonCountry.SelectedItem.Text.Trim();

            infoModel.Address.UsStateId = ddlPersonState.SelectedValue;
            infoModel.Address.CountryPhoneCode = PersonPhoneCode.Text;
            infoModel.Address.Phone = PersonPhone.Text.Trim();
            infoModel.Address.ExtPhone = PersonPhoneExt.Text.Trim();

            infoModel.Address.CountryFaxCode = PersonFaxCode.Text;
            infoModel.Address.Fax = PersonFax.Text.Trim();

            infoModel.Address.CountryCellCode = PersonCellCode.Text;
            infoModel.Address.Cell = PersonCell.Text.Trim();

            infoModel.Address.Email = PersonEmail.Text.Trim();
            infoModel.Communication = GetTreeMethodValue(PersonMethods);
            infoModel.Permissions = GetCheckBoxListValue(cblPersonPermissionsList);
			
			infoModel.Position = new PositionModel{Id = ddlPersonPosition.SelectedValue};
			infoModel.Path2Photo= hdfimgPhoto.Value;
			infoModel.Path2Signature = hdfimgSignature.Value;
			return infoModel;
        }

        private void SetPersonInfo(PersonExModel infoModel)
        {
            PersonDeleteBtn.Enabled = infoModel.PersonId > 0;
            SetViewState(infoModel, SessionConstants.PersonEditData);
            PersonDetailsPanel.Visible = true;
            PersonFirstName.Text = infoModel.FirstName;
            PersonLastName.Text = infoModel.LastName;
            if (infoModel.BirthDate != null)
            {
                PersonBirthDate.Text = String.Format("{0:m/d/yyyy}", infoModel.BirthDate);
                CalendarExt.SelectedDate = infoModel.BirthDate;
            } else
            {
                PersonBirthDate.Text = "";
                CalendarExt.SelectedDate = null;
                //PersonBirthDate.Text = DateTime.Now.ToString("{0:m/d/yyyy}");
                //CalendarExt.SelectedDate= DateTime.Now;
            }
            PersonAddress1.Text = infoModel.Address.Address1;
            PersonAddress2.Text = infoModel.Address.Address2;
            PersonCity.Text = infoModel.Address.City;

            ddlPersonCountry.ClearSelection();

            if (ddlPersonCountry.Items.FindByText(infoModel.Address.Country) != null)
            {
                ddlPersonCountry.Items.FindByText(infoModel.Address.Country).Selected = true;
                txtPersonCountry.Text = infoModel.Address.Country;

                ddlPersonCountry_SelectedIndexChanged(this, EventArgs.Empty);
                if (ddlPersonState.Items.FindByValue(infoModel.Address.UsStateId) != null)
                {
                    ddlPersonState.SelectedValue = infoModel.Address.UsStateId;
                    txtPersonState.Text = ddlPersonState.SelectedItem.Text.ToString();
                }
                else
                {
                    ddlPersonState.SelectedIndex = -1;
                    
                }
            }
            else
            {
                ddlPersonState.Items.Clear();
                btnPersonState.Text = "State";
                PersonPhoneCode.Text = "";
                PersonCellCode.Text = "";
                PersonFaxCode.Text = "";
                txtPersonCountry.Text = "";
                txtPersonState.Text = "";
            }

            txtPersonCode.Text = "" + infoModel.PersonCode;
            hdnPersonID.Value = infoModel.PersonId.ToString();
            PersonStartDate.Text = String.Format("{0:G}", infoModel.CreateDate);

            PersonZip1.Text = infoModel.Address.Zip1;
            PersonZip2.Text = infoModel.Address.Zip2;

            PersonPhoneCode.Text = infoModel.Address.CountryPhoneCode;
            PersonPhone.Text = infoModel.Address.Phone;
            PersonPhoneExt.Text = infoModel.Address.ExtPhone;

            PersonFaxCode.Text = infoModel.Address.CountryFaxCode;
            PersonFax.Text = infoModel.Address.Fax;

            PersonCellCode.Text = infoModel.Address.CountryCellCode;
            PersonCell.Text = infoModel.Address.Cell;

            PersonEmail.Text = infoModel.Address.Email;

            LoadCheckBoxList(infoModel.Permissions, cblPersonPermissionsList);
            SetTreeMethodValue(infoModel.Communication, PersonMethods);

			DataTable dtPersonsServices = QueryCustomerUtils.GetPersonServices(infoModel.PersonId, Page);
			if (dtPersonsServices != null)
			{if (dtPersonsServices.Rows.Count > 0)
				{
					LoadCheckBoxList(dtPersonsServices.Rows[0]["Services"].ToString(), cblPersonServicesList);
				}
				else
				{
					LoadCheckBoxList(null, cblPersonServicesList);
				}
			}
			if (!string.IsNullOrEmpty(infoModel.Position.Name))
            {
                ddlPersonPosition.SelectedValue = "" + infoModel.Position.Id;
                txtPersonPosition.Text = ddlPersonPosition.SelectedItem.Text.ToString();
            }
            else
            {
                ddlPersonPosition.SelectedIndex = -1;
                txtPersonPosition.Text = "";
            }
            AsCompanyAddress.Checked = false;

            WebLogin.Text = infoModel.WebLogin.LoginName;
            WebPassword.Text = infoModel.WebLogin.Password;

			RePassword.Text = WebPassword.Text;

			if (infoModel.Path2Photo != "" && infoModel.Path2Photo != null)
			{
				imgPhoto.Src = infoModel.Path2Photo + "?" + DateTime.Now.Ticks;
				hdfimgPhoto.Value = infoModel.Path2Photo;
			}
			else
			{
				imgPhoto.Src = "Images/NoImage.jpg";
				hdfimgPhoto.Value = "";
			}
			if (infoModel.Path2Signature != "" && infoModel.Path2Signature != null)
			{
				imgSignature.Src = infoModel.Path2Signature + "?" + DateTime.Now.Ticks;
				hdfimgSignature.Value = infoModel.Path2Signature;
			}
			else
			{
				imgSignature.Src = "Images/NoSignature.jpg";
				hdfimgSignature.Value = "";
			}

            Page.Validate("ValGrpPerson");
            UpdatePersonLabel.Text = "";

            LoadPersonPermissions();
            LoadPersonMethod();
			LoadPersonServices();

			DataTable dtPersonAuthForm = QueryUtils.GetPersonAuthForm(infoModel, this);
			if (dtPersonAuthForm.Rows.Count > 0)
			{
				btnAuthorizationForm.Text = "Authorization Form";
				btnAuthorizationForm.Attributes.Add("class", "btn btn-info");
			}
			else
			{
				btnAuthorizationForm.Text = "Authorization Form - Pending ";
				btnAuthorizationForm.Attributes.Add("class", "btn btn-danger");
			}
			lblkycMsg.Text = "";
        }
       
            #endregion

        #region Person Birth Date
            protected void OnChangedBirthDate(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (PersonBirthDate.Text != "")
            {
                try
                {
                    date = DateTime.Parse(PersonBirthDate.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExt.SelectedDate = date;
        }
        #endregion

        #region Same As Company Address
        protected void OnAsCompanyChecked(object sender, EventArgs e)
        {
            var asCompany = AsCompanyAddress.Checked;
            PersonAddress1.Enabled = PersonAddress2.Enabled = PersonCity.Enabled = ddlPersonState.Enabled = !asCompany;
            if (!asCompany) return;
            PersonAddress1.Text = CompanyAddress1.Text;
            PersonAddress2.Text = CompanyAddress2.Text;
            PersonCity.Text = CompanyCity.Text;
            ddlPersonCountry.ClearSelection();
            ddlPersonCountry.Items.FindByText(ddlCustomerCountry.SelectedItem.Text).Selected = true;
            ddlPersonCountry_SelectedIndexChanged(this, EventArgs.Empty);

			ddlPersonState.Items.Clear();
			if (txtPersonCountry.Text.Trim() != "")
			{
				ddlPersonState.Items.Clear();
				var state = QueryCustomerUtils.GetStateByCountry(ddlPersonCountry.SelectedValue.ToString(), this);
				ddlPersonState.DataSource = state;
				ddlPersonState.DataBind();
				ddlPersonState.Items.Insert(0, "");
				ddlPersonState.Items.FindByText(ddlCustomerState.SelectedItem.Text).Selected = true;
				ddlPersonState_SelectedIndexChanged(this, EventArgs.Empty);
			}

			if (ddlPersonState.Items.FindByValue(ddlCustomerState.SelectedValue) != null)
            {
                ddlPersonState.SelectedValue = ddlCustomerState.SelectedValue;
            }
            else
            {
                ddlPersonState.SelectedIndex = -1;
            }
            ddlPersonState.Enabled = true;

        }
        #endregion

        #region Refresh Persons List after Update, Delete
        private void RefreshCompanyInfo(string selPersonId)
        {
            var customerModel = QueryCustomerUtils.GetCustomerExt(GetCustomerShort(), this);
           SetCompanyInfo(customerModel);
            if (selPersonId == "0" || selPersonId== "") return;
            var person = customerModel.Persons.Find(m => m.PersonId == Convert.ToInt32(selPersonId));
            if (person != null) SetPersonInfo(person);
        }
		#endregion

		#region Refresh carrier List after Update, Delete
		private void RefreshCarrierInfo(string selCarrierId)
		{
			var customerModel = QueryCustomerUtils.GetCustomerExt(GetCustomerShort(), this);
			SetCompanyInfo(customerModel);
			if (selCarrierId == "0" || selCarrierId == "") return;
			var carrier = customerModel.Company.Carriers.Find(m => m.CarrierID == selCarrierId);
			if (carrier != null) SetCarrierInfo(carrier);
		}
		#endregion

		#region Validators

		private const string ValidatorGroupCompany = "ValGrpCompany";
        private const string ValidatorGroupPerson = "ValGrpPerson";
        private const string ValidatorGroupCarrier = "ValGrpCarrier";
		private const string ValidatorGroupKYC = "ValGrpKYC";
		private const string ValidatorGroupKYCPage2 = "ValGrpKYC2";

		protected bool IsGroupValid(string sValidationGroup)
        {
            Page.Validate(sValidationGroup);
            foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
            {
                if (!validator.IsValid)
                {
                    return false;
                }
            }
            return true;
        }
        private string GetErrMessage(string sValidationGroup)
        {
            var msg = "";
            foreach (BaseValidator validator in Page.GetValidators(sValidationGroup))
            {
                if (!validator.IsValid)
                {
                    msg += "<br/>" + validator.ToolTip;
                }
            }
            return msg;
        }
        #endregion

        #region Save Company, Person
        private string GetErrMessageOnUpdateCompany()
        {
            var msg = "";
            if (!RequiredFieldValidatorMovements.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorMovements.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyName.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyName.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyShortName.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyShortName.ToolTip;
            }
            if (!RequiredFieldValidatorAddr1.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorAddr1.ToolTip;
            }
            if (!RequiredFieldValidatorCompanyCity.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorCompanyCity.ToolTip;
            }
            if (!CompUsStateValidator.IsValid)
            {
                msg += "<br/>" + CompUsStateValidator.ToolTip;
            }
            if (!RequiredFieldValidatorZip.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidatorZip.ToolTip;
            }
            if (!RequiredFieldValidator1BusinessType.IsValid)
            {
                msg += "<br/>" + RequiredFieldValidator1BusinessType.ToolTip;
            }
            if (!CarrierValidator.IsValid)
            {
                msg += "<br/>" + CarrierValidator.ToolTip;
            }
            if (!AccountValidator.IsValid)
            {
                msg += "<br/>" + AccountValidator.ToolTip;
            }

            return msg;
        }
        protected void OnUpdateCompanyClick(object sender, EventArgs e)
        {
            MessageLabel.Text = "";
            Page.Validate(ValidatorGroupCompany);
            if (!IsGroupValid(ValidatorGroupCompany))
            {
                PopupInfoDialog(GetErrMessage(ValidatorGroupCompany), true);
                return;
            }
            var customerInfo = GetCompanyInfo();
            var msg = QueryCustomerUtils.UpdateCompanyExt(customerInfo, this);
			if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true);
                return;
            }
            CustomerId.Text = customerInfo.CustomerId;
            RefreshCompanyInfo("0");
            MessageLabel.Text = "Customer was saved successfully!";
			CustomerUpdateBtn.Enabled = false;
		}
        protected void OnUpdateCarrierClick(object sender, EventArgs e)
        {
            UpdateCarrierLabel.Text = "";
            Page.Validate(ValidatorGroupCarrier);
            if (!IsGroupValid(ValidatorGroupCarrier))
            {
                PopupInfoDialog(GetErrMessage(ValidatorGroupCarrier), true);
                return;
            }
            
            var customerModel = GetCompanyInfo();
            var limitCarrierModel=GetCarrierInfo();
            var msg = QueryCustomerUtils.UpdateCarrier(customerModel, limitCarrierModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true);
                return;
            }
			RefreshCarrierInfo(""+ limitCarrierModel.CarrierID);
			UpdateCarrierLabel.Text = "Carrier was saved successfully!";
			CarrierUpdateBtn.Enabled = false;

		}

        protected void OnUpdatePersonClick(object sender, EventArgs e)
        {

            UpdatePersonLabel.Text = "";
            Page.Validate(ValidatorGroupPerson);
            if (!IsGroupValid(ValidatorGroupPerson))
            {
                PopupInfoDialog(GetErrMessage(ValidatorGroupPerson), true);
                return;
            }
            
            if (WebPassword.Text != RePassword.Text)
            {
                PopupInfoDialog("Password doesn't match retyped password. <br/>Please, type and retype again.", true);
                return;
            }
            var personInfo = GetPersonInfo();

			if (QueryCustomerUtils.IsWebLoginExists(personInfo.WebLogin, this))
            {
                PopupInfoDialog("Person with this login and password already exists.", true);
                return;
            }

            string fileName, fileImageString;
			if (hdfimgSignature.Value.Contains("data:image/png;base64"))
			{
				fileName = AzureStorageBlob.GeneratePersonFileName(personInfo.PersonId.ToString(), PersonFile.Signature, this);
				fileImageString = AzureStorageBlob.UploadFileToBlob(hdfimgSignature.Value.ToString(), fileName, this);
				imgSignature.Src = fileImageString;
				personInfo.Path2Signature = fileImageString;
				hdfimgSignature.Value = "";
			}
			if (hdfimgPhoto.Value.Contains("data:image/png;base64"))
			{
				fileName = AzureStorageBlob.GeneratePersonFileName(personInfo.PersonId.ToString(), PersonFile.Photo, this);
				fileImageString = AzureStorageBlob.UploadFileToBlob(hdfimgPhoto.Value.ToString(), fileName, this);
				imgPhoto.Src = fileImageString;
				personInfo.Path2Photo = fileImageString;
				hdfimgPhoto.Value = "";
			}
            var msg = QueryCustomerUtils.UpdatePerson(personInfo, this);
			       
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true);
                return;
            }
			//Update Person Service
			PersonServiceModel objServiceModel = new PersonServiceModel();
			objServiceModel.PersonID = personInfo.PersonId;
			objServiceModel.ServiceIDList = GetCheckBoxListValue(cblPersonServicesList);
			msg = SetPersonServices(objServiceModel);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}
			//-- Refresh Persons List

			RefreshCompanyInfo("" + personInfo.PersonId);

            UpdatePersonLabel.Text = "Person was saved successfully!";
			//PersonUpdateBtn.Enabled = false;
		}
		public string SetPersonServices(PersonServiceModel objPersonServiceModel)
		{
			var msg = "";
			msg = QueryCustomerUtils.DeletePersonServices(objPersonServiceModel, this);
			string[] ServiceIDList = objPersonServiceModel.ServiceIDList.Split(',');
			foreach (string ServiceID in ServiceIDList)
			{
				if (ServiceID != "")
				{
					objPersonServiceModel.ServiceID = Convert.ToInt32(ServiceID);
					msg += QueryCustomerUtils.SetPersonsServices(objPersonServiceModel, this);
				}
			}
			return msg;
		}
        protected void OnDeletePersonClick(object sender, EventArgs e)
        {
            var infoModel = GetPersonInfo();
            var msg = QueryCustomerUtils.DeletePerson(infoModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                MessageLabel.Text = msg;
                return;
            }
            RefreshCompanyInfo("" + infoModel.PersonId);
            UpdatePersonLabel.Text = "Person was deleted successfully!";
			PersonUpdateBtn.Enabled = true;
		}
        protected void OnDeleteCarrierClick(object sender, EventArgs e)
        {
            var customerModel = GetCompanyInfo();
            var limitCarrierModel = GetCarrierInfo();

            var msg = QueryCustomerUtils.DeleteCarrierFromCustomer(customerModel, limitCarrierModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                MessageLabel.Text = msg;
                return;
            }
			RefreshCarrierInfo("" + limitCarrierModel.CarrierID);
			UpdateCarrierLabel.Text = "Carrier was deleted successfully!";
			CarrierUpdateBtn.Enabled = true;
		}
		#endregion

		#region New Customer Button
		protected void OnNewCustomerClick(object sender, EventArgs e)
        {
            HideCustomerDetails();
            var customer = new CustomerEditModelExt();
            SetViewState(customer, SessionConstants.CustomerEditDataExt);
            SetCompanyInfo(customer);
            CompanyName.Focus();
            ddlCustomerState.Items.Clear();
            ddlCustomerState.DataBind();

			DropDownListBox.SelectedIndex = -1;
			CustomerLike.Text = "";
			CustomerUpdateBtn.Enabled = true;
			
		}
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerHtml = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
		#endregion

		#region Combo box To Touch screen Events

		protected void ddlPersonCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
			PersonPhoneCode.Enabled = false;
			PersonFaxCode.Enabled = false;
			PersonCellCode.Enabled = false;
			if (ddlPersonCountry.SelectedItem.Text == "India")
            {
                btnPersonState.Text = "State";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Hong Kong")
            {
                btnPersonState.Text = "Region";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "UAE")
            {
                btnPersonState.Text = "Emirate";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Israel")
            {
                btnPersonState.Text = "Region";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Botswana")
            {
                btnPersonState.Text = "District";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Belgium")
            {
                btnPersonState.Text = "Province";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "USA")
            {
                btnPersonState.Text = "State";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Canada")
            {
                btnPersonState.Text = "Province";
            }
            else if (ddlPersonCountry.SelectedItem.Text == "Other")
            {
                btnPersonState.Text = "State";
                ddlPersonState.Items.Clear();
                ddlPersonState.DataBind();
				PersonPhoneCode.Enabled = true;
				PersonFaxCode.Enabled = true;
				PersonCellCode.Enabled = true;
            }
			else
			{
				btnPersonState.Text = "State";
			}
            ddlPersonState.Items.Clear();
            var state = QueryCustomerUtils.GetStateByCountry(ddlPersonCountry.SelectedValue.ToString(), this);
            ddlPersonState.DataSource = state;
            ddlPersonState.SelectedValue = null;
            ddlPersonState.DataBind();
            ddlPersonState.Items.Insert(0, "");


            repPersonState.DataSource = state;
            repPersonState.DataBind();

            txtPersonCountry.Text = ddlPersonCountry.SelectedItem.Text;
            txtPersonState.Text = "";
        }

        protected void ddlCustomerCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
			CompanyPhoneCode.Enabled = false;
			CompanyFaxCode.Enabled = false;
			if (ddlCustomerCountry.SelectedItem.Text == "India")
            {
                btnCustomerState.Text = "State";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Hong Kong")
            {
                btnCustomerState.Text = "Region";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "UAE")
            {
                btnCustomerState.Text = "Emirate";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Israel")
            {
                btnCustomerState.Text = "Region";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Botswana")
            {
                btnCustomerState.Text = "District";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Belgium")
            {
                btnCustomerState.Text = "Province";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "USA")
            {
                btnCustomerState.Text = "State";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Canada")
            {
                btnCustomerState.Text = "Province";
            }
            else if (ddlCustomerCountry.SelectedItem.Text == "Other")
            {
                btnCustomerState.Text = "State";
                ddlCustomerState.Items.Clear();
                ddlCustomerState.DataBind();
				CompanyPhoneCode.Enabled = true;
				CompanyFaxCode.Enabled = true;
            }
			else
			{
				btnCustomerState.Text = "State";
			}
            
            ddlCustomerState.Items.Clear();
            var state = QueryCustomerUtils.GetStateByCountry(ddlCustomerCountry.SelectedValue.ToString(), this);
            ddlCustomerState.DataSource = state;
            ddlCustomerState.SelectedValue = null;
            ddlCustomerState.DataBind();
            ddlCustomerState.Items.Insert(0, "");

            repCustomerState.DataSource = state;
            repCustomerState.DataBind();
            
            txtCustomerCountry.Text = ddlCustomerCountry.SelectedItem.Text;
            txtCustomerState.Text = "";
        }

        protected void repCustomerCountry_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "CountryID", false) == 0)
            {
                txtCustomerCountry.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlCustomerCountry.ClearSelection();
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
				string CountryCode = commandArgs[0];
				string PhoneCode = commandArgs[1];
				ddlCustomerCountry.Items.FindByValue(CountryCode).Selected = true;
				CompanyPhoneCode.Text = "+" + PhoneCode;
				CompanyFaxCode.Text = "+" + PhoneCode;
				ddlCustomerCountry_SelectedIndexChanged(this, EventArgs.Empty);
            }
        }

        protected void repCustomerState_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "StateId", false) == 0)
            {
                txtCustomerState.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlCustomerState.ClearSelection();
                ddlCustomerState.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
            }
        }

        protected void  repCustomerBusinessType_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "BusinessTypeId", false) == 0)
            {
                txtCustomerBusinessType.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlCustomerBusinessType.ClearSelection();
                ddlCustomerBusinessType.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
            }
        }
        
        protected void repPersonCountry_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "CountryID", false) == 0)
            {
                txtPersonCountry.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlPersonCountry.ClearSelection();
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
				string CountryCode = commandArgs[0];
				string PhoneCode = commandArgs[1];
				ddlPersonCountry.Items.FindByValue(CountryCode).Selected = true;
				PersonFaxCode.Text = "+" + PhoneCode;
				PersonCellCode.Text = "+" + PhoneCode;
				PersonPhoneCode.Text = "+" + PhoneCode;
				ddlPersonCountry_SelectedIndexChanged(this, EventArgs.Empty);
            }
        }

        protected void repPersonState_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "StateId", false) == 0)
            {
                txtPersonState.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlPersonState.ClearSelection();
                ddlPersonState.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
            }
        }

        protected void repPersonPosition_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "Id", false) == 0)
            {
                txtPersonPosition.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlPersonPosition.ClearSelection();
                ddlPersonPosition.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
            }
        }

        protected void repCarrier_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "Id", false) == 0)
            {
                txtCarrierList.Text = ((System.Web.UI.WebControls.Button)e.CommandSource).Text;
                ddlCarrierList.ClearSelection();
                ddlCarrierList.Items.FindByValue(e.CommandArgument.ToString()).Selected = true;
				txtCarrierAccount.Text = "";
            }
        }
        

        protected void CompanyUsState_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCustomerState.Text = ddlCustomerState.SelectedItem.Text;
        }

        protected void ddlPersonState_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPersonState.Text = ddlPersonState.SelectedItem.Text;
        }

        protected void ddlPersonPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPersonPosition.Text= ddlPersonPosition.SelectedItem.Text;
        }

        protected void ddlCarrierList_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCarrierList.Text = ddlCarrierList.SelectedItem.Text;
        }

        protected void btnSetCompanyPermissions_Click(object sender, EventArgs e)
        {
        }
        protected void btnSetCompanyMethods_Click(object sender, EventArgs e)
        {
        }
        protected void btnSetPersonPermissions_Click(object sender, EventArgs e)
        {
        }
        protected void btnSetPersonMethods_Click(object sender, EventArgs e)
        {
        }
		protected void btnSetPersonService_Click(object sender, EventArgs e)
		{
		}
		private void LoadCompanyPermissions()
        {
            string strPermissions = string.Empty;
            dvCustomerPermissions.Controls.Clear();
            foreach (ListItem li in CompanyPermissionsList.Items)
            {
                if (li.Selected == true)
                {
                    Label lb = new Label();
                    lb.Text = li.Text;
                    lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
                    dvCustomerPermissions.Controls.Add(lb);
                }
            }
        }

        private void LoadCompanyMethod()
        {
            dvCompanyCommunication.Controls.Clear();
            string strPermissions = string.Empty;
            foreach (TreeNode node in CompanyMethods.CheckedNodes)
            {
                if (node.Checked == true && node.Text != " Communications")
                {
                    Label lb = new Label();
                    lb.Text = node.Text;
                    lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
                    dvCompanyCommunication.Controls.Add(lb);
                }
            }
        }

        private void LoadMovmentList()
        {
            dvGoodsMovement.Controls.Clear();
            string strPermissions = string.Empty;
            foreach (ListItem li in rblMovementsList.Items)
            {
                if (li.Selected == true)
                {
                    Label lb = new Label();
                    lb.Text = li.Text;
                    lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
                    dvGoodsMovement.Controls.Add(lb);
                }
            }
        }
        private void LoadPersonPermissions()
        {
            string strPermissions = string.Empty;
            dvPersonPermission.Controls.Clear();
            foreach (ListItem li in cblPersonPermissionsList.Items)
            {
                if (li.Selected == true)
                {
                    Label lb = new Label();
                    lb.Text = li.Text;
                    lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
                    dvPersonPermission.Controls.Add(lb);
                }
            }
        }

        private void LoadPersonMethod()
        {
            dvPersonMethod.Controls.Clear();
            string strPermissions = string.Empty;
            foreach (TreeNode node in PersonMethods.CheckedNodes)
            {
                if (node.Checked == true && node.Text != " Communications")
                {
                    Label lb = new Label();
                    lb.Text = node.Text;
                    lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
                    dvPersonMethod.Controls.Add(lb);
                }
            }
        }
		private void LoadPersonServices()
		{
			dvPersonService.Controls.Clear();
			string strPermissions = string.Empty;
			foreach (ListItem li in cblPersonServicesList.Items)
			{
				if (li.Selected == true)
				{
					Label lb = new Label();
					lb.Text = li.Text;
					lb.Attributes.Add("style", "background-color:#5377A9;color:white;font-weight:bold;margin:5px;padding-left: 3px;padding-right: 3px;");
					dvPersonService.Controls.Add(lb);
				}
			}
		}

		#endregion

		#region KYC Details Panel
		protected void AsyncFileUploadAuthForm_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadAuthForm.HasFile)
			{
				Session["AsyncFileUploadAuthForm"] = Convert.ToBase64String(AsyncFileUploadAuthForm.FileBytes);
				lblAuthFormMsg.Text = "";
			}
		}

		protected void AsyncFileUploadImgKycPhoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
		{
			if (AsyncFileUploadImgKycPhoto.HasFile)
			{
				Session["AsyncFileUploadImgKycPhoto"] = Convert.ToBase64String(AsyncFileUploadImgKycPhoto.FileBytes);
			}
		}

		private void SetKYCCompanyInfo(CustomerEditModelExt infoModel)
		{
			var kycModel = QueryUtils.GetKYC("0", this);
			if (infoModel.CustomerId != "0")
			{
				if (infoModel.CustomerId != null)
				{
					kycModel = QueryUtils.GetKYC(infoModel.CustomerId.ToString(), this);
					btnKYC.Enabled = true;
				}
				else
				{
					btnKYC.Enabled = false;
					return;
				}
			}
			if (kycModel != null)
			{
				ClearKYCForm();
				SetKYCInfo(kycModel);
				btnKYC.Text = "KYC";
				btnKYC.Attributes.Add("class", "btn btn-info");
			}
			else
			{
				ClearKYCForm();
				SetKYCFormCompany();
				btnKYC.Text = "KYC - Pending ";
				btnKYC.Attributes.Add("class", "btn btn-danger");
			}
		}
		private void SetKYCFormCompany()
		{
			txtkycCompany.Text = CompanyName.Text.ToString();
			txtkycAddress1.Text = CompanyAddress1.Text.ToString();
			txtkycAddress2.Text = CompanyAddress2.Text.ToString();
			txtkycCity.Text = CompanyCity.Text.ToString();
			txtkycZip.Text = CompanyZip1.Text.ToString();
			txtkycState.Text = txtCustomerState.Text.ToString();
			txtkycTel1.Text = CompanyPhoneCode.Text.ToString() + CompanyPhone.Text.ToString();
			txtkycFAX.Text = CompanyFaxCode.Text.ToString() + CompanyFax.Text.ToString();
			txtkycEmail.Text = CompanyEmail.Text.ToString();
		}
		private void ClearKYCForm()
		{ 
			txtkycDate.Text = DateTime.Now.ToString("yyyy-MM-dd"); ;
			txtkycYOE.Text = "";
			rdlCustomerInfo.ClearSelection();
			rdlExistingCustomer.ClearSelection();
			rdlTypeOfAddress.ClearSelection();

			txtkycCompany.Text = "";
			txtkycOwnerName.Text = "";
			txtkycAddress1.Text = "";
			txtkycAddress2.Text = "";
			txtkycCity.Text = "";
			txtkycZip.Text = "";

			txtkycState.Text = "";
			txtkycCityCode.Text = "";
			txtkycTel1.Text = "";
			txtkycTel2.Text = "";
			txtkycMobile.Text = "";

			txtkycFAX.Text = "";
			txtkycEmail.Text = "";
			txtkycWebsite.Text = "";
			txtkycRepresentative.Text = "";
			txtkycDesignation.Text = "";
			txtkycMemberOf.Text = "";

			txtkycRef1.Text = "";
			txtkycContactPerson1.Text = "";
			txtkycRef2.Text = "";
			txtkycContactPerson2.Text = "";
			txtkycPAN.Text = "";
			txtkycGSTNo.Text = "";
			txtkycGSTNowef.Text = "";

			cblDocProof.ClearSelection();

			rdlCompanyType.ClearSelection();
			rdlBusinessType.ClearSelection();
			rdlDistribution.ClearSelection();

			txtkycCompanyBrief.Text = "";

			ltlKycPhoto.Text = "";
			ImgKycPDFDownload.Visible = false;
		}

		private KYCModel GetKYCInfo()
		{

			var kycModel = new KYCModel();
			kycModel.KYCDate = txtkycDate.Text.Trim();
			kycModel.YearofEstd = txtkycYOE.Text.Trim();
			kycModel.CustomerID = Convert.ToInt32(CustomerId.Text.Trim());
			kycModel.IsDomestic = rdlCustomerInfo.SelectedItem.Value == "0" ? false : true;
			kycModel.IsExistingCustomer = rdlExistingCustomer.SelectedItem.Value == "0" ? false : true;
			kycModel.AddressType = rdlTypeOfAddress.SelectedItem.Value.ToString();

			kycModel.CompanyName = txtkycCompany.Text.Trim();
			kycModel.OwnerName = txtkycOwnerName.Text.Trim();
			kycModel.Address1 = txtkycAddress1.Text.Trim();
			kycModel.Address2 = txtkycAddress2.Text.Trim();
			kycModel.City = txtkycCity.Text.Trim();
			kycModel.Zip = txtkycZip.Text.Trim();

			kycModel.State = txtkycState.Text.Trim();
			kycModel.CityCode = txtkycCityCode.Text.Trim();
			kycModel.Tel1 = txtkycTel1.Text.Trim();
			kycModel.Tel2 = txtkycTel2.Text.Trim();
			kycModel.Mobile = txtkycMobile.Text.Trim();

			kycModel.FAX = txtkycFAX.Text.Trim();
			kycModel.Email = txtkycEmail.Text.Trim();
			kycModel.Website = txtkycWebsite.Text.Trim();
			kycModel.Representative = txtkycRepresentative.Text.Trim();
			kycModel.Designation = txtkycDesignation.Text.Trim();
			kycModel.Memberof = txtkycMemberOf.Text.Trim();

			kycModel.Ref1 = txtkycRef1.Text.Trim();
			kycModel.RefContact1 = txtkycContactPerson1.Text.Trim();
			kycModel.Ref2 = txtkycRef2.Text.Trim();
			kycModel.RefContact2 = txtkycContactPerson2.Text.Trim();
			kycModel.PAN = txtkycPAN.Text.Trim();
			kycModel.GSTNo = txtkycGSTNo.Text.Trim();

			kycModel.GSTwef = txtkycGSTNowef.Text.Trim();
			txtkycGSTNowef.Text = kycModel.GSTwef;
			string cblDocProofval = string.Empty;
			if (txtkycPAN.Text.Trim() != "") { cblDocProof.Items[0].Selected = true; }
			if (txtkycGSTNo.Text.Trim() != "") { cblDocProof.Items[1].Selected = true; }
			for (int i = 0; i < cblDocProof.Items.Count; i++)
			{
				if (cblDocProof.Items[i].Selected)
					cblDocProofval += cblDocProof.Items[i].Value.ToString() + ",";
			}
		

			kycModel.PhotoIDProof = cblDocProofval;
			kycModel.CompanyTypeID = Convert.ToInt16(rdlCompanyType.SelectedItem.Value);
			kycModel.BusinessTypeID = Convert.ToInt16(rdlBusinessType.SelectedItem.Value);
			kycModel.DistributionID = Convert.ToInt16(rdlDistribution.SelectedItem.Value);

			kycModel.CompanyBrief = txtkycCompanyBrief.Text.ToString();

			string CustomerImg = string.Empty;
			if (Session["AsyncFileUploadImgKycPhoto"] != null)
			{
				string CustCode = CustomerCode.Text.ToString().Trim();
				CustomerImg = Session["AsyncFileUploadImgKycPhoto"].ToString();
				string fileName = AzureStorageBlob.GenerateCustomerKYCFileName(CustCode.ToString(), this);
				string fileImagePath = AzureStorageBlob.UploadPDFFileToBlob(CustomerImg, fileName, this);
				kycModel.CustomerImg = fileImagePath;

				string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"210px\" height=\"215px\">";
				embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
				embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
				embed += "</object>";
				ltlKycPhoto.Text = string.Format(embed, fileImagePath +"?" + DateTime.Now.Ticks);
				ImgKycPDFDownload.NavigateUrl = kycModel.CustomerImg + "?" + DateTime.Now.Ticks;
				ImgKycPDFDownload.Visible = true;
				MainPanel.Update();
				Session["AsyncFileUploadImgKycPhoto"] = null;
			}
			else
			{
				kycModel.CustomerImg = hdnKycPhoto.Value;
			}

			return kycModel;
		}

		private void SetKYCInfo(KYCModel kycModel)
		{
			txtkycDate.Text = Convert.ToDateTime(kycModel.KYCDate).ToString("yyyy-MM-dd");
			txtkycYOE.Text = kycModel.YearofEstd;
			rdlCustomerInfo.Items.FindByValue(kycModel.IsDomestic == false ? "0" : "1").Selected = true;
			rdlExistingCustomer.Items.FindByValue(kycModel.IsExistingCustomer == false ? "0" : "1").Selected = true;
			rdlTypeOfAddress.Items.FindByValue(kycModel.AddressType).Selected = true;

			txtkycCompany.Text = kycModel.CompanyName;
			txtkycOwnerName.Text = kycModel.OwnerName;
			txtkycAddress1.Text = kycModel.Address1;
			txtkycAddress2.Text = kycModel.Address2;
			txtkycCity.Text = kycModel.City;
			txtkycZip.Text = kycModel.Zip;

			txtkycState.Text = kycModel.State;
			txtkycCityCode.Text = kycModel.CityCode;
			txtkycTel1.Text = kycModel.Tel1;
			txtkycTel2.Text = kycModel.Tel2;
			txtkycMobile.Text = kycModel.Mobile;

			txtkycFAX.Text = kycModel.FAX;
			txtkycEmail.Text = kycModel.Email;
			txtkycWebsite.Text = kycModel.Website;
			txtkycRepresentative.Text = kycModel.Representative;
			txtkycDesignation.Text = kycModel.Designation;
			txtkycMemberOf.Text = kycModel.Memberof;

			txtkycRef1.Text = kycModel.Ref1;
			txtkycContactPerson1.Text = kycModel.RefContact1;
			txtkycRef2.Text = kycModel.Ref2;
			txtkycContactPerson2.Text = kycModel.RefContact2;
			txtkycPAN.Text = kycModel.PAN;
			txtkycGSTNo.Text = kycModel.GSTNo;
			txtkycGSTNowef.Text = kycModel.GSTwef;

			if (kycModel.CustomerImg != "")
			{
				string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"210px\" height=\"215px\">";
				embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
				embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
				embed += "</object>";
				ltlKycPhoto.Text = string.Format(embed, kycModel.CustomerImg + "?" + DateTime.Now.Ticks);
				ImgKycPDFDownload.NavigateUrl = kycModel.CustomerImg + "?" + DateTime.Now.Ticks;
				ImgKycPDFDownload.Visible = true;
			}
			if (kycModel.PhotoIDProof != "")
			{
				cblDocProof.ClearSelection();
				string[] cblDocProofval = kycModel.PhotoIDProof.Substring(0, kycModel.PhotoIDProof.Length - 1).Split(',');
				for (int i = 0; i <= cblDocProofval.Length - 1; i++)
				{
					cblDocProof.Items.FindByValue(cblDocProofval[i].ToString()).Selected = true;
				}
			}
			else
			{
				cblDocProof.ClearSelection();
			}
			rdlCompanyType.Items.FindByValue(kycModel.CompanyTypeID.ToString()).Selected = true;
			rdlBusinessType.Items.FindByValue(kycModel.BusinessTypeID.ToString()).Selected = true;
			rdlDistribution.Items.FindByValue(kycModel.DistributionID.ToString()).Selected = true;

			txtkycCompanyBrief.Text = kycModel.CompanyBrief;
			hdnKycPhoto.Value = kycModel.CustomerImg;
		}

		//private void ValidateDocProof()
		//{
		//	int chkCnt = 0;
		//	rfvkycPAN.Enabled = false;
		//	rfvkycGST.Enabled = false;
		//	for (int i = 0; i < cblDocProof.Items.Count; i++)
		//	{
		//		if (cblDocProof.Items[i].Selected == true)
		//		{
		//			chkCnt++;
		//			if (i == 0)
		//			{
		//				rfvkycPAN.Enabled = true;
		//			}
		//			if (i == 1)
		//			{
		//				rfvkycGST.Enabled = true;
		//			}
		//		}
		//	}
		//	if (chkCnt < 2)
		//	{
		//		ModalPopupExtender13.Show();
		//		ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(1);", true);
		//	}
		//}

		//public void DocProofCheckBoxListServerValidate(object sender, ServerValidateEventArgs args)
		//{
		//	var isValid = false;
		//	var cnt = 0;
		//	for (var i = 0; i < cblDocProof.Items.Count; i++)
		//	{
		//		if (cblDocProof.Items[i].Selected == true)
		//		{
		//			cnt = cnt + 1;
		//			if (cnt == 2)
		//			{
		//				isValid = true;
		//				break;
		//			}
		//		}
		//	}
		//	args.IsValid = isValid;
		//}

		protected void btnKYCSubmit_Click(object sender, EventArgs e)
		{
			lblkycMsg.Text = "";
			//ValidateDocProof();
			Page.Validate(ValidatorGroupKYCPage2);
			if (!IsGroupValid(ValidatorGroupKYCPage2))
			{
				ModalPopupExtender13.Show();
				ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(1);", true);
				//PopupInfoDialog(GetErrMessage(ValidatorGroupKYC), true);
				return;
			}
			var kycModel = GetKYCInfo();
			var msg = QueryUtils.SetKYC(kycModel, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}
			btnKYC.Text = "KYC";
			btnKYC.Attributes.Add("class", "btn btn-info");
			lblkycMsg.Text = "KYC was saved successfully!";
			ModalPopupExtender13.Show();
			MainPanel.Update();

			ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(1);", true);
		}

		protected void btnKYC_OnClick(object sender, EventArgs e)
		{
			lblkycMsg.Text = "";
			var customerModel = GetCompanyInfo();
			SetKYCCompanyInfo(customerModel);
			Page.Validate(ValidatorGroupKYC);
			MainPanel.Update();
			ModalPopupExtender13.Show();
			ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(0);", true);
			btnNextKYC.Visible = true;
			btnPrevKYC.Visible = false;
			btnKYCSubmit.Visible = false;
		}

		protected void btnAuthorizationForm_OnClick(object sender, EventArgs e)
		{
			lblAuthFormMsg.Text = "";
			MainPanel.Update();
			ModalPopupExtender16.Show();
			ltlAuthorizationForm.Visible = false;
			btnAuthFormView.Text = "View";
		}

		protected void btnPersonAuthForm_Click(object sender, EventArgs e)
		{
			lblkycMsg.Text = "";
			var personModel = GetPersonInfo();
			string fileImagePath = string.Empty;
			if (Session["AsyncFileUploadAuthForm"] != null)
			{
				string AuthFormImg = Session["AsyncFileUploadAuthForm"].ToString();
				string fileName = AzureStorageBlob.GeneratePersonFileName(personModel.PersonId.ToString(), PersonFile.AuthorizationForm, this);
				fileImagePath = AzureStorageBlob.UploadPDFFileToBlob(AuthFormImg, fileName, this);
				Session["AsyncFileUploadAuthForm"] = null;

				string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"600px\" height=\"300px\">";
				embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
				embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
				embed += "</object>";
				ltlAuthorizationForm.Text = string.Format(embed, fileImagePath + "?" + DateTime.Now.Ticks);
				lnkAuthorizationFormDownload.NavigateUrl = fileImagePath + "?" + DateTime.Now.Ticks;
				lnkAuthorizationFormDownload.Visible = true;
			}
			else
			{
				lblAuthFormMsg.Text = "Please select file for upload";
				lblAuthFormMsg.ForeColor = System.Drawing.Color.Red;
				ModalPopupExtender16.Show();
				return;
			}
			
			var msg = QueryUtils.SetPersonAuthForm(personModel.PersonId.ToString(), fileImagePath, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
				return;
			}

			btnAuthorizationForm.Text = "Authorization Form";
			btnAuthorizationForm.Attributes.Add("class", "btn btn-info");
			lblAuthFormMsg.Text = "Person Auth Form was saved successfully!";
			lblAuthFormMsg.ForeColor = System.Drawing.Color.Blue;
			ModalPopupExtender16.Show();
			MainPanel.Update();
		}

		protected void btnAuthFormView_Click(object sender, EventArgs e)
		{
			if (ltlAuthorizationForm.Visible == false)
			{
				//imgAuthorizationForm.Visible = true;
				ltlAuthorizationForm.Visible = true;
				var personModel = GetPersonInfo();
				DataTable dtPersonAuthForm = QueryUtils.GetPersonAuthForm(personModel, this);
				if (dtPersonAuthForm.Rows.Count > 0)
				{
					string AuthorizationFormImageUrl = dtPersonAuthForm.Rows[0]["AuthFormImg"].ToString();

					string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"600px\" height=\"300px\">";
					embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
					embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
					embed += "</object>";
					ltlAuthorizationForm.Text = string.Format(embed, AuthorizationFormImageUrl + "?" + DateTime.Now.Ticks);
					lnkAuthorizationFormDownload.NavigateUrl = AuthorizationFormImageUrl + "?" + DateTime.Now.Ticks;
					lnkAuthorizationFormDownload.Visible = true;
					btnAuthFormView.Text = "Hide";
				}
				else
				{
					ltlAuthorizationForm.Text = "";
					lnkAuthorizationFormDownload.NavigateUrl = "#";
					lnkAuthorizationFormDownload.Visible = false;
					lblAuthFormMsg.Text = "File is not uploaded";
					lblAuthFormMsg.ForeColor = System.Drawing.Color.Red;
				}
			}
			else
			{
				btnAuthFormView.Text = "View";
				ltlAuthorizationForm.Visible = false;
				lnkAuthorizationFormDownload.Visible = false;
			}
			ModalPopupExtender16.Show();
		}

		protected void btnNextKYC_OnClick(object sender, EventArgs e)
		{
			Page.Validate(ValidatorGroupKYC);
			if (!IsGroupValid(ValidatorGroupKYC))
			{
				ModalPopupExtender13.Show();
				ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(0);", true);
			}
			else
			{
				ModalPopupExtender13.Show();
				ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(1);", true);
				btnPrevKYC.Visible = true;
				btnNextKYC.Visible = false;
				btnKYCSubmit.Visible = true;
			}
			lblkycMsg.Text = "";
		}

		protected void btnPrevKYC_OnClick(object sender, EventArgs e)
		{
			ModalPopupExtender13.Show();
			ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "tab", "javascript:showTab(0);", true);
			btnNextKYC.Visible = true;
			btnPrevKYC.Visible = false;
			btnKYCSubmit.Visible = false;
			lblkycMsg.Text = "";
		}

		#endregion

		#region ShippingCourier  / Carriers
		private string SortDirection
		{
			get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		public void BindShippingCourierGrid(string sortExpression = null)
		{
			DataTable dtShippingCourier = SyntheticScreeningUtils.GetCarriers(this);

			if (sortExpression != null)
			{
				DataView dv = dtShippingCourier.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvShippingCourier.DataSource = dv;
			}
			else
			{
				gvShippingCourier.DataSource = dtShippingCourier;
			}
			gvShippingCourier.DataBind();
			
		}

		protected void OnSortingCarrierName(object sender, GridViewSortEventArgs e)
		{
			this.BindShippingCourierGrid(e.SortExpression);
			modelAddShippingCourier.Show();
		}

		protected void EditShippingCourier(object sender, EventArgs e)
		{
			ClearShippingCourier();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			Label lblShippingCourierName = (Label)clickedRow.FindControl("lblShippingCourierName");
			HiddenField hdnShippingCourierID = (HiddenField)clickedRow.FindControl("hdnShippingCourierID");

			txtShippingCourierName.Text = lblShippingCourierName.Text;
			hdnShippingCourierId.Value = hdnShippingCourierID.Value;
			modelAddShippingCourier.Show();
		}

		private void ClearShippingCourier()
		{
			hdnShippingCourierId.Value = "0";
			txtShippingCourierName.Text = "";
			lblShippingCourierMsg.Text = "";
		}

		protected void btnAddShippingCourier_Click(object sender, EventArgs e)
		{
			try
			{
				modelAddShippingCourier.Show();
				SyntheticShippingCourierModel shippingCourier = new SyntheticShippingCourierModel();
				shippingCourier.CarrierID = hdnShippingCourierId.Value.Trim();
				shippingCourier.CarrierName = txtShippingCourierName.Text.Trim();
				DataTable dtShippingCourier = SyntheticScreeningUtils.SetCarriers(shippingCourier, this);
				if (dtShippingCourier.Rows.Count >= 1)
				{

					lblShippingCourierMsg.Text = "ShippingCourier already exist.";
					lblShippingCourierMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindShippingCourierGrid();
				ClearShippingCourier();
				lblShippingCourierMsg.Text = "ShippingCourier Data Added successfully!";
				lblShippingCourierMsg.ForeColor = System.Drawing.Color.Blue;
			}
			catch (Exception ex)
			{
				lblShippingCourierMsg.Text = ex.Message.ToString();
				lblShippingCourierMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearShippingCourier_Click(object sender, EventArgs e)
		{
			modelAddShippingCourier.Show();
			ClearShippingCourier();
		}


		#endregion

		#region DeliveryMethod
		protected void btnPopupAddDeliveryMethod_OnClick(object sender, EventArgs e)
		{
			modelAddDeliveryMethod.Show();
		}	
		public void BindDeliveryMethodGrid(string sortExpression = null)
		{
			DataTable deliveryMethod = SyntheticScreeningUtils.GetSyntheticDeliveryMethod(this);

			if (sortExpression != null)
			{
				DataView dv = deliveryMethod.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvDeliveryMethod.DataSource = dv;
			}
			else
			{
				gvDeliveryMethod.DataSource = deliveryMethod;
			}
			gvDeliveryMethod.DataBind();
			
		}

		protected void OnSortingDeliveryMethod(object sender, GridViewSortEventArgs e)
		{
			this.BindDeliveryMethodGrid(e.SortExpression);
			modelAddDeliveryMethod.Show();
		}

		protected void EditDeliveryMethod(object sender, EventArgs e)
		{
			ClearDeliveryMethod();

			GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
			Label lblDeliveryMethodName = (Label)clickedRow.FindControl("lblDeliveryMethodName");
			HiddenField hdnDeliveryMethodID = (HiddenField)clickedRow.FindControl("hdnDeliveryMethodID");

			txtDeliveryMethodName.Text = lblDeliveryMethodName.Text;
			hdnDeliveryMethodId.Value = hdnDeliveryMethodID.Value;
			modelAddDeliveryMethod.Show();
		}

		private void ClearDeliveryMethod()
		{
			hdnDeliveryMethodId.Value = "0";
			txtDeliveryMethodName.Text = "";
			lblDeliveryMethodMsg.Text = "";
		}

		protected void btnAddDeliveryMethod_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticDeliveryMethodModel deliveryMethod = new SyntheticDeliveryMethodModel();
				deliveryMethod.DeliveryMethodId = hdnDeliveryMethodId.Value.Trim();
				deliveryMethod.DeliveryMethodName = txtDeliveryMethodName.Text.Trim();
				DataTable dtDeliveryMethod = SyntheticScreeningUtils.SetSyntheticDeliveryMethod(deliveryMethod, this);
				if (dtDeliveryMethod.Rows.Count >= 1)
				{

					lblDeliveryMethodMsg.Text = "DeliveryMethod already exist.";
					lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Red;
					return;
				}
				BindDeliveryMethodGrid();
				ClearDeliveryMethod();
				lblDeliveryMethodMsg.Text = "DeliveryMethod Data Added successfully!";
				lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Blue;
				modelAddDeliveryMethod.Show();
			}
			catch (Exception ex)
			{
				lblDeliveryMethodMsg.Text = ex.Message.ToString();
				lblDeliveryMethodMsg.ForeColor = System.Drawing.Color.Red;
			}
		}

		protected void btnClearDeliveryMethod_Click(object sender, EventArgs e)
		{
			modelAddDeliveryMethod.Show();
			ClearDeliveryMethod();
		}


		#endregion

		//USA  +1   -- State
		//India +91  -- State
		//---HK +852  -- Region
		//UAE +971    -- Emirate
		//Israel  +972   -- Region
		//Botswana +267  -- District
		//Belgium  +32   -- Province
		//UK +44
	}
}