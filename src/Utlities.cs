﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.Utilities;
using System.Net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System.Linq;

namespace Corpt
{
    public class Utlities
    {
        #region Create ShortReportModel 
        public static DataTable CreateTableVertic(DataTable table)
        {
            var result = new DataTable("Short Report");
            result.Columns.Add(new DataColumn("Title"));
            foreach (DataRow row in table.Rows)
            {
                var colName = ("" + row[0]);
                colName = Regex.Replace(colName, @"<[^>]+>", "");
                result.Columns.Add(new DataColumn(colName));
            }
            for (var i = 1; i < table.Columns.Count; i++)
            {
                var newRow = result.NewRow();
                newRow["Title"] = table.Columns[i].Caption;
                result.Rows.Add(newRow);
            }
            result.AcceptChanges();
            for (int r = 0; r < table.Rows.Count; r++)
            {
                for (int c = 1; c < table.Columns.Count; c++)
                {
                    var val = table.Rows[r][c];
                    result.Rows[c - 1][r + 1] = val;
                }
            }
            result.AcceptChanges();
            return result;
        }
        public static ShortReportModel CreateShortReportModelWithDocStruct(BatchModel batchModel, Page p)
        {
            var docId = QueryUtils.GetDocumentId(batchModel, p);
            if (docId == "0")
            {
                return null;
            }
            return new ShortReportModel
                      {
                        BatchModel = batchModel,
                        DocumentId = docId,
                        DocStructure = QueryUtils.GetDocStructure(docId, p)
                      };
        }
        public static ShortReportModel GetShortReportData(BatchModel batchModel, Page p)
        {
            var shortReportModel = new ShortReportModel {BatchModel = batchModel};

            //-- DocumentId
            shortReportModel.DocumentId = QueryUtils.GetDocumentId(batchModel, p);
            
            //-- Document Structure
            shortReportModel.DocStructure = QueryUtils.GetDocStructure(shortReportModel.DocumentId, p);

            //-- Failed Items
            shortReportModel.ItemsFailed = QueryUtils.GetItemsFailed(shortReportModel.BatchModel, p);

            //-- Items and MovedItems
            QueryUtils.GetItemsAndMovedItems(shortReportModel, p);

            //-- Values Items
            shortReportModel.ItemValues = QueryUtils.GetItemValues(shortReportModel.BatchModel, p);

            //--  Report View
            CreateMergedItemStructValueTable(shortReportModel);

            return shortReportModel;
        }
        public static void CreateMergedItemStructValueTable(ShortReportModel shortReportModel)
        {
            var table = new DataTable();
            //-- Create HLink column on ViewItem Details page
            table.Columns.Add(PageConstants.ColNameLinkOnItemView);

            //-- Create Columns by Document Structure
            table.Columns.Add(new DataColumn("Result"));
            foreach (DocStructModel docModel in shortReportModel.DocStructure)
            {
                if (!table.Columns.Contains(docModel.Title))
                {
                    table.Columns.Add(new DataColumn(docModel.Title));
                }
            }
            table.AcceptChanges();

            //-- Set data in columns
            foreach(ItemModel itemModel in shortReportModel.Items)
            {
                var strNewItemNumber = itemModel.FullItemNumber;
                var row = table.NewRow();
                //-- Add Hlink on ItemView
                var sLink = string.Format(PageConstants.LinkOnPageItemViewDetail,
                    shortReportModel.BatchModel.BatchId, itemModel.ItemCode, itemModel.FullItemNumberWithDotes);
                row[PageConstants.ColNameLinkOnItemView] = "<a href=" + sLink + ">" + itemModel.FullItemNumberWithDotes + "</a>";

                foreach(DocStructModel docStructModel in shortReportModel.DocStructure)
                {
                    if(docStructModel.IsCutGrade)
                    {
                        var sPartName = GetPartName(docStructModel.Value);
                        var sPartId = ParsePartIdValue(
                            docStructModel.Value, strNewItemNumber, 
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);
                        var myRequest =
                            "<a href=\"CutGradeDetail.aspx?par1=" + itemModel.OrderCode +
                            "&par2=" + itemModel.BatchCode +
                            "&par3=" + itemModel.ItemCode +
                            "&par4=" + sPartId +
                            "&par5=" + sPartName + "\">" +
                            ParseValue(docStructModel.Value, strNewItemNumber, shortReportModel.ItemValues, shortReportModel.ItemsFailed) +
                            "</a>";
                        row[docStructModel.Title] = myRequest; 
                    } else
                    {
                        row[docStructModel.Title] = ParseValue(
                            docStructModel.Value, strNewItemNumber, 
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);

                    }
                }
                
                table.Rows.Add(row);
            }
            //-- Result column value
            SetFailColumn(table);
            shortReportModel.ReportView = table.Copy();
        }
        
        public static void SetFailColumn(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                for(int i=0; i < table.Columns.Count; i++)
                {
                    string val = row[i].ToString().ToLower();
                    if (val.IndexOf("highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        row["Result"] =  @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                        row.AcceptChanges();
                        break;
                    }
                }
            }
        }
        #endregion

        #region Parse Document Structure Value
        public static string ParseSingleValue(string strPartName, string strPropertyName, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            if (strPropertyName.ToLower() == "item #")
            {
                //get old item number
                var itemNames = itemValueList.FindAll(model => model.NewItemNumber == strItemNumber);
                return itemNames[0].OldItemNumber;
            }
            var drValues = itemValueList.FindAll( model =>
                model.NewItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drValues.Length == 0 || drValues[0].MeasureCode == DbConstants.MeasureCodeInternalComment) //-- TODO
            {
                return "";
            }
            //check here for cprules and mark accordingly
            var strResultValue = drValues[0].ResultValue;
            var drCp = itemFailedList.FindAll(model =>
                model.ItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drCp.Length > 0)
            {
                strResultValue = "<SPAN class=\"text_highlitedyellow\">" + strResultValue + "</SPAN>";
            }

            return strResultValue;
        }

        public static string ParseValue(string strValue, string strItemNumber, List<ItemValueModel> dtItemValueList, List<ItemFailedModel> dtCpResult)
        {
            var strResult = "";
            var strPartName = "";
            var strPropertyName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (char ch in strValue)
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    strPropertyName += ch;
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    insidePropertyName = false;
                    strResult += ParseSingleValue(strPartName, strPropertyName, strItemNumber, dtItemValueList, dtCpResult);
                    strPartName = "";
                    strPropertyName = "";
                }
            }
            return strResult;
        }
        public static string ParseCutGrade(ItemModel itemModel, string strValue, string strItemNumber, List<ItemValueModel> dtItemValueList, List<ItemFailedModel> dtCpResult)
        {
            var result = "";

         //   [Diamond 1.CutGrade] / [Diamond 2.CutGrade]
            var items = new List<CutGradeParseModel>();
            var p = 0;
            while (p < strValue.Length)
            {
                var i_s = strValue.IndexOf('[', p);
                if (i_s == -1) break;
                var i_e = strValue.IndexOf(']', i_s);
                if (i_e == -1) break;
                var pair = strValue.Substring(i_s + 1, i_e - i_s - 1);
                var a_pair = pair.Split(new char[]{'.'}, System.StringSplitOptions.RemoveEmptyEntries);
                if (a_pair.Length != 2) break;
                var parseModel = new CutGradeParseModel { PartName = a_pair[0], MeasureName = a_pair[1] };
                parseModel.PartId = ParsePartId(parseModel.PartName, parseModel.MeasureName, strItemNumber, dtItemValueList, dtCpResult);
                parseModel.MeasureValue = ParseSingleValue(parseModel.PartName, parseModel.MeasureName, strItemNumber, dtItemValueList, dtCpResult);
                items.Add(parseModel);
                p = i_s + 1;
            }
            foreach (var item in items)
            {
                result += (result.Length > 0 ? " " : "") +
                    "<a target='_blank' href='CutGradeDetail.aspx" +
                        "?par1=" + itemModel.OrderCode +
                        "&par2=" + itemModel.BatchCode +
                        "&par3=" + itemModel.ItemCode +
                        "&par4=" + item.PartId +
                        "&par5=" + item.PartName + "'>" +
                        item.MeasureValue +
                    "</a>";
            }
            return result;
        }
        public static List<string> GetPartsNameForCutGrade(string strValue)
        {
            var parts = new List<string>();
            char[] separatingChars = {'/' };
            string[] words = strValue.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);
            foreach (var word in words)
            {
                parts.Add(GetPartName(word));
            }
            return parts;
        }
        public static string GetPartName(string strValue) //, string strItemNumber, DataTable itemValueList, DataTable itemFailedList)
        {
            var strResult = "";
            var strPartName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (var ch in strValue.ToCharArray())
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    strResult = strPartName;
                    break;
                }
            }
            return strResult;
        }
       // public static List<string> 
        public static string ParsePartIdValue(string strValue, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            var strResult = "";
            var strPartName = "";
            var strPropertyName = "";
            var insidePartName = false;
            var insidePropertyName = false;

            foreach (var ch in strValue.ToCharArray())
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    strPropertyName += ch;
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    insidePropertyName = false;
                    strResult += ParsePartId(strPartName, strPropertyName, strItemNumber, itemValueList, itemFailedList);
                    strPartName = "";
                    strPropertyName = "";
                }
            }
            return strResult;
        }
        public static string ParsePartId(string strPartName, string strPropertyName, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            if (strPropertyName.ToLower() == "item #")
            {
                //get old item number
                var itemNames = itemValueList.FindAll(model => model.NewItemNumber == strItemNumber).ToArray();
                return itemNames[0].OldItemNumber;
            }
            var drValues = itemValueList.FindAll(model =>
                model.NewItemNumber == strItemNumber &&
                model.PartName == strPartName &&
                model.MeasureName == strPropertyName).ToArray();
            if (drValues.Length == 0)
            {
                return "0";
            }
            if (drValues.Length > 1)
            {
                var msg = string.Format("Duplication item value: NewItemNumber {0}, PartName {1}, MeasureName {2}",
                                        strItemNumber, strPartName, strPropertyName);
                throw new Exception(msg);
            }
            return drValues[0].PartId;
        }

        public static String ParametersToValue(String myValue, String myBatchId, String myItemCode, List<ItemStructModel> itemStructList, List<ItemValueModel> itemValueList, Page p)
        {
            var r = new Regex(@"\[[\d,\w,/\\,\s,),(,#]*[.][\d,\w,/\\,\s,),(,#]*\]"); //accounts for numbers, dashes, spaces
            var mc = r.Matches(myValue);

            if (mc.Count == 0)
            {
                return myValue;
            }
            var replacement = ReplacementDataString(mc[0].Value, myBatchId, myItemCode, p, itemStructList, itemValueList);
            var newValue = r.Replace(myValue, replacement, 1, 0);
            return ParametersToValue(newValue, myBatchId, myItemCode, itemStructList, itemValueList, p);
        }
       
        public static String ReplacementDataString(String myMetaData, String batchId, String itemCode, Page p,
            List<ItemStructModel> itemStructList, List<ItemValueModel> itemValueList)
        {
            //check for correct part name first
            var metaPartName = myMetaData.Trim('[').Trim(']').Split('.')[0].ToUpper();
            var metaMeasure = myMetaData.Trim('[').Trim(']').Split('.')[1].ToUpper();
            
            if (itemStructList.FindAll(model => model.PartName.ToUpper() == metaPartName).Count == 0)
            {
                return "<SPAN class=text_highlitedyellow>wrong structure</SPAN>";
            }


            if (metaMeasure == "Item #")
            {
                return QueryUtils.GetOldItemNumber(batchId, itemCode, p);
            }

            var myValueList =
                itemValueList.FindAll(
                    model => model.PartName.ToUpper() == metaPartName && model.MeasureName.ToUpper() == metaMeasure);
            if (myValueList.Count == 0)
            {
                return "";// "<strong>No measure </strong>";
            }
            if (!myValueList[0].IsCpOne)
            {
                return "<SPAN class=text_highlitedyellow>" + myValueList[0].ResultValue + "</SPAN>";
            }
            return myValueList[0].ResultValue;
        }

        public static List<string> GetEmptyColumns(DataTable dt)
        {

            List<string> colList = new List<string>();
            foreach (var col in dt.Columns)
            {
                string colName = col.ToString();
                bool emptyCol = (from DataRow row in dt.Rows select row[colName]).Where(m => m.Equals(DBNull.Value) || m.Equals("")).Count() == dt.Rows.Count;
                if (emptyCol)
                    colList.Add(colName);
            }
            return colList;
        }

        public static string GetPictureImageUrl(string dbPicture, Page p)
        {
            var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
            dbPicture = dbPicture.ToLower().Replace(@"\\", "/").Replace(@"\", "/").Replace(@"/pictures", @"pictures");
            //static value for testing
            //dbPicture = "pictures/earrings/studs/er50rdw4cgsiza.png";
            return rootDir + dbPicture;
        }

        
        public static bool GetPictureImageUrl(string dbPicture, out string imagePath, out string errMsg, Page p)
        {
			var webClient = new WebClient();
			var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
			dbPicture = dbPicture.ToLower().Replace(@"\\", "/").Replace(@"\", "/");
			dbPicture = dbPicture.Replace(@"/pictures", @"pictures");
			imagePath = rootDir + dbPicture;
			
			try
			{
				var stream = webClient.OpenRead(imagePath);
				//if (File.Exists(imagePath)) //      p.MapPath(imagePath)))
				//{
				errMsg = "";
				return true;
				//}
				//errMsg = "File " + imagePath + " does not exist.";
				//return false;
			}
			catch (Exception ex)
			{
				var eMessage = ex.Message;
				errMsg = "File " + "\"" + dbPicture +"\"" + " does not exist.";
				return false;
			}
        }

        public static bool GetUrlLink(string dbPicture)
        {
            var webClient = new WebClient();
            try
            {
                var stream = webClient.OpenRead(dbPicture);
            }
            catch (Exception ex)
            {
                var eMessage = ex.Message;
                return false;
            }
            return true;
        }
        public static bool GetPictureImageUrl(string dbPicture, out MemoryStream ms, out string errMsg, out string fileType, Page p)
		{
			var webClient = new WebClient();
			var imgPath = GetImgPath(dbPicture, p);
            ms = new MemoryStream();
			fileType = "";
			try
			{
				Stream stream = webClient.OpenRead(imgPath);
				//if (File.Exists(imagePath)) //      p.MapPath(imagePath)))
				//{
				fileType = imgPath.Substring(imgPath.Length - 3, 3);
				stream.CopyTo(ms);
				errMsg = "";
				return true;
				//}
				//errMsg = "File " + imagePath + " does not exist.";
				//return false;
			}
			catch (Exception ex)
			{
				var eMessage = ex.Message;
				errMsg = "File " + "\"" + dbPicture + "\"" + " does not exist.";
				return false;
			}
		}

        public static string GetImgPath(string path2Picture, Page p,
            string root = SessionConstants.WebPictureShapeRoot)
        {
            var rootDir = "" + p.Session[root];
            var path = path2Picture.ToLower().Replace(@"\\", "/").Replace(@"\", "/");
            path = path.Replace(@"/pictures", @"pictures");
            var imgPath = rootDir + path;
            return imgPath;
        }


        public static Stream GetPDFFromAzure(string dbPicture, out MemoryStream ms, out string errMsg, out string fileType, Page p)
        {
            var webClient = new WebClient();
            var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
            //dbPicture = dbPicture.ToLower().Replace(@"\\", "/").Replace(@"\", "/");
            //dbPicture = dbPicture.Replace(@"/pictures", @"pictures");
            var imgPath = rootDir + @"FrontInOut/" + dbPicture;
            ms = new MemoryStream();
            fileType = "";
            try
            {
                Stream stream = webClient.OpenRead(imgPath);
                //if (File.Exists(imagePath)) //      p.MapPath(imagePath)))
                //{
                fileType = imgPath.Substring(imgPath.Length - 3, 3);
                stream.CopyTo(ms);
                errMsg = "";
                return stream;
                //}
                //errMsg = "File " + imagePath + " does not exist.";
                //return false;
            }
            catch (Exception ex)
            {
                var eMessage = ex.Message;
                errMsg = "File " + "\"" + dbPicture + "\"" + " does not exist.";
                return null;
            }
        }


        private static void ConvertWmfToPng(string path, string pathGif, out string saveErr)
        {
            saveErr = "";
            Image img = Image.FromFile(path);

            int width = Convert.ToInt32(img.Width * 0.01 * 10 );
            int height = Convert.ToInt32(img.Height * 0.01 * 10);
            Image bitmap = new Bitmap(width, height, img.PixelFormat);
            
            Graphics g = Graphics.FromImage(bitmap);
            g.Clear(Color.White);
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var rect = new Rectangle(0, 0, width, height);
            g.DrawImage(img, rect);
            try
            {
                Thread.Sleep(500);
                bitmap.Save(pathGif, ImageFormat.Png);
                saveErr = "";
            } catch (Exception ex)
            {
                saveErr = ex.Message;
            }
//            bitmap.Dispose();
//            GC.Collect();
        }

		private static void ConvertWmfToPngAzure(Stream wmf, string pathGif, Page p, out string saveErr, out MemoryStream bitmapStream)
		{
			//////var myAccountName = p.Session[SessionConstants.AzureAccount].ToString();
			//////var myAccountKey = p.Session[SessionConstants.AccountKey].ToString();
			//////StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
			//////CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
			//////CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
			//////CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
			//////CloudBlobContainer container = blobClient.GetContainerReference(p.Session[SessionConstants.WebShapePngRoot].ToString());
			//////var blockBlob = container.GetBlockBlobReference(pathGif);

			saveErr = "";
			Image img = Image.FromStream(wmf);

			int width = Convert.ToInt32(img.Width * 0.01 * 10);
			int height = Convert.ToInt32(img.Height * 0.01 * 10);
			Image bitmap = new Bitmap(width, height, img.PixelFormat);

			/* Does not work on Azure */
			Graphics g = Graphics.FromImage(bitmap);
			g.Clear(Color.White);
			g.CompositingQuality = CompositingQuality.HighQuality;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			var rect = new Rectangle(0, 0, width, height);
			g.DrawImage(img, rect);
			/**/

			//Stream bitmapStream = new MemoryStream();
			bitmapStream = new MemoryStream();
			try
			{
				//Thread.Sleep(500);
				bitmap.Save(bitmapStream, ImageFormat.Png);
				//////////var bytes = ((MemoryStream)bitmapStream).ToArray();
				//////////using
				//////////(var inputStream = new MemoryStream(bytes))	  blockBlob.UploadFromStream(inputStream);
				////Bitmap fromStream = new Bitmap(inputStream);
				//saveErr = "";
			}
			catch (Exception ex)
			{
				saveErr = ex.Message;
			}
			//            bitmap.Dispose();
			//            GC.Collect();

		}

		public static bool GetShapeImageUrlAzure(string dbShape,  out string imagePath, out string errMsg,  Page p,/* out string pngPath,*/ out MemoryStream ms)
		{
			string saveErr = "";
			ms = new MemoryStream();
			var webClient = new WebClient();
			var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
			//var rootDir = "" + p.Session[SessionConstants.GlobalShapeRoot];
			//var rootPngDir = "" + p.Session[SessionConstants.GlobalShapePngRoot];
			dbShape = dbShape.ToLower().Replace(@"\\", "/").Replace(@"\", "/");
			dbShape = dbShape.Replace(@"/shapes", @"shapes");
			imagePath = rootDir + dbShape;
			var pngPath = "";
			try
			{
				var stream = webClient.OpenRead(imagePath);
				errMsg = "";
				if (stream != null)
				{
					var ps = dbShape.Split('/');
					foreach (var sTemp in ps)
					{
						if (sTemp.Contains(".wmf"))
						{
							pngPath = sTemp.Replace(".wmf", ".png");
							ConvertWmfToPngAzure(stream, pngPath, p, out saveErr, out ms);
							imagePath = rootDir + p.Session[SessionConstants.WebTempFolder].ToString() + pngPath;
  							return true;
						}
					}
				}
				errMsg = "File " + dbShape + " does not exist.";
				return false;
			}
			catch (Exception ex)
			{
				var exMess = ex.Message;
				errMsg = "Convert dir " + imagePath + " does not exists.";
				return false;
			}
		}

        public static bool GetJpgUrlAzure(string pdfName, Page p, out string errMsg, out MemoryStream ms)
        {
            try
            {
                errMsg = "";
                var webClient = new WebClient();
                var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
                string pdfPath = rootDir + @"cdr_to_jpg/" + pdfName;
                Stream stream = webClient.OpenRead(pdfPath);
                string saveErr = "";
                ms = null;
                //ms = new MemoryStream();
                //stream.CopyTo(ms);
                return true;
            }
            catch (Exception ex)
            {
                var exMess = ex.Message;
                errMsg = "Convert dir " + pdfName + " does not exists.";
                ms = null;
                return false;
            }
        }

        public static bool GetSkuJpgUrlAzure(string pdfName, Page p, out string errMsg, out MemoryStream ms)
        {
            try
            {
                errMsg = "";
                var webClient = new WebClient();
                var rootDir = "" + p.Session[SessionConstants.WebPictureShapeRoot];
                string pdfPath = rootDir + @"sku_jpg/" + pdfName;
                Stream stream = webClient.OpenRead(pdfPath);
                string saveErr = "";
                ms = null;
                //ms = new MemoryStream();
                //stream.CopyTo(ms);
                return true;
            }
            catch (Exception ex)
            {
                var exMess = ex.Message;
                errMsg = "Convert dir " + pdfName + " does not exists.";
                ms = null;
                return false;
            }
        }
        public static bool GetShapeImageUrl(string dbShape, out string imagePath, out string errMsg, Page p, out string pngPath)
		{
			string saveErr;
			var rootDir = "" + p.Session[SessionConstants.GlobalShapeRoot];
			var rootPngDir = "" + p.Session[SessionConstants.GlobalShapePngRoot];
			imagePath = rootDir + dbShape;
			if (!Directory.Exists(p.MapPath(rootPngDir)))
			{
				errMsg = "Convert dir " + rootPngDir + " does not exists.";
				pngPath = imagePath;
				return false;
			}

			if (File.Exists(p.MapPath(imagePath)))
			{
				var ps = dbShape.Split('\\');
				var wmfName = ps[ps.Length - 1];
				if (wmfName.IndexOf(".wmf", StringComparison.Ordinal) != -1)
				{
					wmfName = wmfName.Split('.')[0] + ".png";
				}
				pngPath = rootPngDir + "/" + wmfName;

				ConvertWmfToPng(p.MapPath(imagePath), p.MapPath(pngPath), out errMsg);
				if (!string.IsNullOrEmpty(errMsg))
				{
					return false;
				}
				errMsg = "";
				return true;
			}
			errMsg = "File " + imagePath + " does not exist.";
			pngPath = "";
			return false;
		}
		#endregion

		#region Define Document, CorelDraw files
		public static List<CorelFileModel> GetCorelDrawFiles(Page p, out string errMsg)
        {
            var fileList = new List<CorelFileModel>();
            var corelFileDir = p.Session[SessionConstants.GlobalRepDir] + "CorelFiles\\";
            if (!Directory.Exists(p.MapPath(corelFileDir)))
            {
                errMsg = String.Format("Can't load corel files. Directory {0} is not exists or access denied. Please, check the directory.",
                    p.MapPath(corelFileDir));
                return fileList;

            }
            errMsg = "";
            var di = new DirectoryInfo(p.MapPath(corelFileDir));
            var files = di.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Extension.ToUpper() == ".JPG" || file.Extension.ToUpper() == ".DB") continue;
                fileList.Add(new CorelFileModel(file.Name, true));
            }

            return fileList;

            //ArrayList lstFileNames = gemoDream.Service.GetFileList(sCorelFilesPath);
        }

        public static List<CorelFileModel> GetCorelDrawFilesAzure(Page p, out string errMsg)
        {
            var fileList = new List<CorelFileModel>();
            errMsg = "";
            var myAccountName = p.Session[SessionConstants.AzureAccount].ToString();
            var myAccountKey = p.Session[SessionConstants.AccountKey].ToString();
            var containerName = p.Session[SessionConstants.AzureContainerName].ToString();
            //string containerName = @"gdlight";
            //var myAccountName = @"gdlightstorage";
            //var myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                //var zzz = cloudBlobClient.ListContainers();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                var items = cloudBlobContainer.ListBlobs();
                foreach (var dir in items.OfType<CloudBlobDirectory>())
                {
                    string uriStr = dir.Uri.ToString();
                    if (!uriStr.Contains("CorelFiles"))
                        continue;
                    var dirs = dir.ListBlobs();
                    var files = dirs.OfType<CloudBlockBlob>().Where(b => (Path.GetExtension(b.Name).Equals(".cdr") || Path.GetExtension(b.Name).Equals(".xls")));
                    //var ddd = yyy.OfType<CloudBlockBlob>();
                    foreach (var cdrfile in files)
                    {
                        var cdrname = ((CloudBlockBlob)cdrfile).Name;
                        cdrname = cdrname.Replace(@"CorelFiles/", "");
                        if (cdrname.Contains("backup"))
                            continue;
                        fileList.Add(new CorelFileModel(cdrname, true));
                    }
                    //var xxx = yyy.OfType<CloudBlockBlob>.Where(b => Path.GetExtension(b.Name).Equals(".png"));

                    Console.WriteLine("\t{0}", dir.Uri);

                }
                /*
                var blobs = items.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".cdr"));
                foreach (var item in blobs)
                {

                    string name = ((CloudBlockBlob)item).Name;
                    fileList.Add(new CorelFileModel(name, true));
                    //CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(name);
                    //string path = (@"C:\Users\mbcrump\Downloads\test\" + name);
                    //blockBlob.DownloadToFile(path, FileMode.OpenOrCreate);
                }
                */
            }
            catch (Exception ex)
            {
                errMsg = String.Format("Can't load corel files.");
                return fileList;
            }
            /*alex
            Console.WriteLine("Directories:");
            foreach (var dir in items.OfType<CloudBlobDirectory>())
            {
                var yyy = dir.ListBlobs();
                var xxx = yyy.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".jpg"));
                var ddd = yyy.OfType<CloudBlockBlob>();
                foreach (var jpgfile in xxx)
                {
                    var jname = ((CloudBlockBlob)jpgfile).Name;
                }
                //var xxx = yyy.OfType<CloudBlockBlob>.Where(b => Path.GetExtension(b.Name).Equals(".png"));

                Console.WriteLine("\t{0}", dir.Uri);

            }
            */
            /*
            var corelFileDir = p.Session[SessionConstants.GlobalRepDir] + "CorelFiles\\";
            if (!Directory.Exists(p.MapPath(corelFileDir)))
            {
                errMsg = String.Format("Can't load corel files. Directory {0} is not exists or access denied. Please, check the directory.",
                    p.MapPath(corelFileDir));
                return fileList;

            }
            errMsg = "";
            var di = new DirectoryInfo(p.MapPath(corelFileDir));
            var files = di.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Extension.ToUpper() == ".JPG" || file.Extension.ToUpper() == ".DB") continue;
                fileList.Add(new CorelFileModel(file.Name, true));
            }
            */
            return fileList;

            //ArrayList lstFileNames = gemoDream.Service.GetFileList(sCorelFilesPath);
        }
        

        public static List<CorelFileModel> GetSkuDrawFilesAzure(string filter, Page p, out string errMsg)
        {
            var fileList = new List<CorelFileModel>();
            errMsg = "";
            var myAccountName = p.Session[SessionConstants.AzureAccount].ToString();
            var myAccountKey = p.Session[SessionConstants.AccountKey].ToString();
            var containerName = p.Session[SessionConstants.AzureContainerName].ToString();
            //string containerName = @"gdlight";
            //var myAccountName = @"gdlightstorage";
            //var myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                //var zzz = cloudBlobClient.ListContainers();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                var items = cloudBlobContainer.ListBlobs();
                foreach (var dir in items.OfType<CloudBlobDirectory>())
                {
                    string uriStr = dir.Uri.ToString();
                    //if (!uriStr.Contains("sku_images"))
                    if (!uriStr.Contains("sku_jpg"))
                        continue;
                    var dirs = dir.ListBlobs();
                    //var files = dirs.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".jpg"));
                    var files = dirs.OfType<CloudBlockBlob>().Where(b => Path.GetFileName(b.Name).Contains(filter));
                    //var ddd = yyy.OfType<CloudBlockBlob>();
                    foreach (var cdrfile in files)
                    {
                        var cdrname = ((CloudBlockBlob)cdrfile).Name;
                        //cdrname = cdrname.Replace(@"sku_images/", "");
                        cdrname = cdrname.Replace(@"sku_jpg/", "");
                        if (cdrname.Contains("backup"))
                            continue;
                        fileList.Add(new CorelFileModel(cdrname, true));
                    }
                    //var xxx = yyy.OfType<CloudBlockBlob>.Where(b => Path.GetExtension(b.Name).Equals(".png"));

                    Console.WriteLine("\t{0}", dir.Uri);

                }
                /*
                var blobs = items.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".cdr"));
                foreach (var item in blobs)
                {

                    string name = ((CloudBlockBlob)item).Name;
                    fileList.Add(new CorelFileModel(name, true));
                    //CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(name);
                    //string path = (@"C:\Users\mbcrump\Downloads\test\" + name);
                    //blockBlob.DownloadToFile(path, FileMode.OpenOrCreate);
                }
                */
            }
            catch (Exception ex)
            {
                errMsg = String.Format("Can't load corel files.");
                return fileList;
            }
            /*alex
            Console.WriteLine("Directories:");
            foreach (var dir in items.OfType<CloudBlobDirectory>())
            {
                var yyy = dir.ListBlobs();
                var xxx = yyy.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".jpg"));
                var ddd = yyy.OfType<CloudBlockBlob>();
                foreach (var jpgfile in xxx)
                {
                    var jname = ((CloudBlockBlob)jpgfile).Name;
                }
                //var xxx = yyy.OfType<CloudBlockBlob>.Where(b => Path.GetExtension(b.Name).Equals(".png"));

                Console.WriteLine("\t{0}", dir.Uri);

            }
            */
            /*
            var corelFileDir = p.Session[SessionConstants.GlobalRepDir] + "CorelFiles\\";
            if (!Directory.Exists(p.MapPath(corelFileDir)))
            {
                errMsg = String.Format("Can't load corel files. Directory {0} is not exists or access denied. Please, check the directory.",
                    p.MapPath(corelFileDir));
                return fileList;

            }
            errMsg = "";
            var di = new DirectoryInfo(p.MapPath(corelFileDir));
            var files = di.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Extension.ToUpper() == ".JPG" || file.Extension.ToUpper() == ".DB") continue;
                fileList.Add(new CorelFileModel(file.Name, true));
            }
            */
            return fileList;

            //ArrayList lstFileNames = gemoDream.Service.GetFileList(sCorelFilesPath);
        }

        #endregion
    }


}