using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Corpt
{
	/// <summary>
	/// Summary description for TrackingStart.
	/// </summary>
	public partial class TrackingStart : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				PrepareForm();
			}
		}

		private void PrepareForm()
		{
			//prepare date ranges
			for(int i=1; i<32; i++)
			{
				lstDayFrom.Items.Add(new ListItem(i.ToString(),i.ToString()));
				lstDayTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}

			for(int i=2008;i<2015;i++)
			{
				lstYearFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
				lstYearTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}


			//prepare customer list
			lstCustomerList.Items.Clear();
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			conn.Open();
				
			SqlCommand command = new SqlCommand("spGetCustomers");
			
			command.Connection=conn;
			command.CommandType=CommandType.StoredProcedure;
			
			command.Parameters.Add("@AuthorID",Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
			SqlDataReader reader = command.ExecuteReader();
			if(reader.HasRows)
			{
				lstCustomerList.DataSource=reader;
				lstCustomerList.DataTextField="CustomerName";
				lstCustomerList.DataValueField="CustomerOfficeID_CustomerID";
				lstCustomerList.DataBind();
			}
			conn.Close();

			if((Utils.NullValue(Session["CustomerID"])!="")&&(Utils.NullValue(Session["CustomerOfficeID"])!=""))
			{
				//set the prev. selected customer
				lstCustomerList.SelectedIndex=lstCustomerList.Items.IndexOf(lstCustomerList.Items.FindByValue(Session["CustomerOfficeID"].ToString()+'_'+Session["CustomerID"].ToString()));
			}

			if((Utils.NullValue(Session["dtFrom"])!="")&&(Utils.NullValue(Session["dtTo"])!=""))
			{
				//set the prev. selected date                
				DateTime dtFrom = new DateTime();
				dtFrom = DateTime.Parse(Session["dtFrom"].ToString());
				lstMonthFrom.SelectedIndex=lstMonthFrom.Items.IndexOf(lstMonthFrom.Items.FindByValue(dtFrom.Month.ToString()));
				lstDayFrom.SelectedIndex=lstDayFrom.Items.IndexOf(lstDayFrom.Items.FindByValue(dtFrom.Day.ToString()));
				lstYearFrom.SelectedIndex = lstDayFrom.Items.IndexOf(lstYearFrom.Items.FindByValue(dtFrom.Year.ToString()));

				DateTime dtTo = new DateTime();
				dtTo = DateTime.Parse(Session["dtTo"].ToString());
				lstMonthTo.SelectedIndex= lstMonthTo.Items.IndexOf(lstMonthTo.Items.FindByValue(dtTo.Month.ToString()));
				lstDayTo.SelectedIndex=lstDayTo.Items.IndexOf(lstDayTo.Items.FindByValue(dtTo.Day.ToString()));
				lstYearTo.SelectedIndex=lstYearTo.Items.IndexOf(lstYearTo.Items.FindByValue(dtTo.Year.ToString()));				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdMiddle_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Middle.aspx");
		}

		protected void cmdLoadActivity_Click(object sender, System.EventArgs e)
		{
			if(lstCustomerList.SelectedIndex!=-1)
			{
				SqlCommand command = new SqlCommand("wspvvGetMemoDateRange");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType=CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter("@CustomerID",lstCustomerList.SelectedValue.Split('_')[1]));
				command.Parameters.Add(new SqlParameter("@CustomerOfficeID",lstCustomerList.SelectedValue.Split('_')[0]));

//Setting Selected Customer Info
				Session["CustomerID"]=lstCustomerList.SelectedValue.Split('_')[1];
				Session["CustomerOfficeID"]=lstCustomerList.SelectedValue.Split('_')[0];
				
				DateTime dtFrom = new DateTime(int.Parse(lstYearFrom.SelectedValue),int.Parse(lstMonthFrom.SelectedValue),int.Parse(lstDayFrom.SelectedValue));
				DateTime dtTo = new DateTime(int.Parse(lstYearTo.SelectedValue),int.Parse(lstMonthTo.SelectedValue),int.Parse(lstDayTo.SelectedValue));

//Setting Selected Date Range 
				Session["dtFrom"]=dtFrom;
				Session["dtTo"]=dtTo;

				command.Parameters.Add(new SqlParameter("@DateFrom", dtFrom));
				command.Parameters.Add(new SqlParameter("@DateTo", dtTo));

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dt = new DataTable();

				da.Fill(dt);

				dgDebug.DataSource=dt;
				dgDebug.DataBind();

				if(dt.Rows.Count>0)
				{
//					dgDisplay.DataSource=dt;
//					dgDisplay.DataBind();
					lblInfo.Text="";

					PrepareOrderList(dt);
					PrepareCustomerProgramList(dt);
					PrepareMemoList(dt);
				}
				else
				{
//					dgDisplay.DataSource=null;
//					dgDisplay.DataBind();
					lblInfo.Text="No activity.";
				}
			}
			else
			{	
				lblInfo.Text="Select a customer.";
			}
		}

		private void PrepareOrderList(DataTable dt)
		{
			//<span class='text_highlitedgray'>00000</span> - closed order.<br><span class='text_highlitedyellow'>00000</span> - open order.
			SortedList sl = new SortedList();

			foreach(DataRow dr in dt.Rows)
			{
				try
				{
					string strOrderCodeH = dr["OrderCode"].ToString();
					string strOrderCode = dr["OrderCode"].ToString();
					if(dr["StateID"].ToString()=="4")
					{
						strOrderCodeH="<span class='text_highlitedgray'>"+strOrderCode+"</span>";
					}
					else
					{
						strOrderCodeH="<span class='text_highlitedyellow'>"+strOrderCode+"</span>";
					}
					sl.Add(strOrderCode,strOrderCodeH);
				}
				catch
				{}
			}

			int i =0;
			foreach(DictionaryEntry o in sl)
			{
				lblOrders.Text+="<a href=\"TrackingStep2.aspx?p1=Order&p2=" + HttpUtility.UrlEncode(o.Key.ToString()) + "\">"+ o.Value.ToString() +"</a>,&nbsp;";
				if(i++>15)
				{
					lblOrders.Text+="<br />";
					i=0;
				}
			}
		}

		private void PrepareCustomerProgramList(DataTable dt)
		{
			SortedList sl = new SortedList();

			foreach(DataRow dr in dt.Rows)
			{
				try
				{
					sl.Add(dr["CustomerProgramName"].ToString(),dr["CustomerProgramName"].ToString());
				}
				catch
				{}	
			}

			int i=0;
			foreach(DictionaryEntry di in sl)
			{
				lblCustomerPrograms.Text+="<a href=\"TrackingStep2.aspx?p1=CP&p2=" +HttpUtility.UrlEncode(di.Value.ToString()) + "\">"+ di.Value.ToString() +"</a>,&nbsp;";
				if(i++>10)
				{
					lblCustomerPrograms.Text+="<br />";
					i=0;
				}
			}
		}

		private void PrepareMemoList(DataTable dt)
		{
			foreach(DataRow dr in dt.Rows)
			{
				if(Utils.NullValue(dr["MemoNumber"])=="")
				{
					dr["MemoNumber"]="No Memo";
				}
			}
			
			SortedList sl = new SortedList();

			foreach(DataRow dr in dt.Rows)
			{
				try
				{
					sl.Add(dr["MemoNumber"].ToString(),dr["MemoNumber"].ToString());
				}
				catch
				{}
			}

			int i=0;
			foreach(DictionaryEntry di in sl)
			{
				lblMemos.Text+="<a href=\"TrackingStep2.aspx?p1=Memos&p2=" + HttpUtility.UrlEncode(di.Value.ToString()) + "\">"+ di.Value.ToString() +"</a>,&nbsp;";
				if(i++>10)
				{
					lblMemos.Text+="<br />";
					i=0;
				}
			}			
		}		
	}
}
