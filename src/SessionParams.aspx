﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="SessionParams.aspx.cs" Inherits="Corpt.SessionParams" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div>
        Application Path
        <asp:Label runat="server" ID="AppPath"></asp:Label>
    </div>
    <div>
    <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label5" runat="server" Text="Change Session Parameter" CssClass="label"></asp:Label>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td><asp:Label ID="Label1" runat="server" Text="Name" CssClass="control-label "/></td>
            <td><asp:TextBox ID="ParamName" runat="server" ></asp:TextBox></td>
            <td><asp:Label ID="Label2" runat="server" Text="Value: " ></asp:Label></td>
            <td><asp:TextBox ID="ParamValue" runat="server" ></asp:TextBox></td>
            <td><asp:Button ID="ApplyBtn" OnClick="OnApplyClick" Text="Apply" runat="server" CssClass="btn btn-info"/></td>
        </tr>
    </table>
    <asp:Panel runat="server" DefaultButton="ShowDirBtn">
    <table>
        <tr>
            <td colspan="2" style="vertical-align: top">
                <asp:Label ID="Label3" runat="server" Text="Show directory (virtual)" CssClass="label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;padding: 5px">
                <asp:Label ID="Label4" runat="server" Text="Directory: " ></asp:Label>
            </td>
            <td style="vertical-align: top;padding: 5px">
                <asp:TextBox ID="DirField" runat="server"></asp:TextBox>
            </td>
            <td style="vertical-align: top;padding: 5px">
                <asp:Button ID="ShowDirBtn" OnClick="OnShowDirClick" runat="server" Text="Show" CssClass="btn btn-info"/>
            </td>
            <td style="vertical-align: top">
                <asp:ListBox ID="FilesList" runat="server" DataValueField="DisplayName" Rows="10"/>
            </td>
            <td style="vertical-align: top">
                <asp:Label runat="server" ID="PhysicalPathLabel" ToolTip="Physical Path"></asp:Label><br/>
                <asp:Label runat="server" ID="ErrorLabel" ForeColor="DarkRed"></asp:Label>
            </td>
        </tr>
        
    </table>
    </asp:Panel>
        
    </div>
    <div>
        <asp:DataGrid runat="server" ID="ParamsGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
            <Columns>
                <asp:BoundColumn DataField="Name" HeaderText="Param Name"></asp:BoundColumn>
                <asp:BoundColumn DataField="Value" HeaderText="Param Value"></asp:BoundColumn>
                <asp:BoundColumn DataField="MapFile" HeaderText="Physical Path"/>
            </Columns>
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
            <ItemStyle Font-Names="Cambria" Font-Size="Small" />
        </asp:DataGrid>
    </div>

</asp:Content>
