﻿using Corpt.Models.SyntheticScreening;
using Corpt.Utilities;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Corpt
{
    public partial class ScreeningDataView : System.Web.UI.Page
    {
		#region Page Load 
		protected void Page_Load(object sender, EventArgs e)
        {
			if(!IsPostBack)
			{
				BindRetailers();
				BindServiceType();
				BindCertifiedBy();
				BindCategory();
				BindServiceTime();
				BindDeliveryMethod();
				BindShippingCourier();
				BindScreeningInstrument();
				BindPosition();
				BindJewelryType();
			}
        }
		private void BindPosition()
		{

			DataSet dsUserStatus = SyntheticScreeningUtils.GetSyntheticOrderStatus(1, this);
			ddlPosition.DataTextField = "StatusName";
			ddlPosition.DataValueField = "StatusID";
			ddlPosition.DataSource = dsUserStatus.Tables[0];
			ddlPosition.DataBind();
			ListItem liPosition = new ListItem();
			liPosition.Text = "";
			liPosition.Value = "0";
			ddlPosition.Items.Insert(0, liPosition);

		}

		private void BindRetailers()
		{
			var retailers = SyntheticScreeningUtils.GetRetailers(this);
			ddlRetailer.DataSource = retailers;
			ddlRetailer.DataTextField = "RetailerName";
			ddlRetailer.DataValueField = "RetailerID";
			ddlRetailer.DataBind();
			ListItem liRetailer = new ListItem();
			liRetailer.Text = "";
			liRetailer.Value = "0";
			ddlRetailer.Items.Insert(0, liRetailer);
		}

		public void BindServiceType()
		{
			DataTable dt = SyntheticScreeningUtils.GetSyntheticServiceType(this);
			ddlServiceType.DataSource = dt;
			ddlServiceType.DataTextField = "ServiceTypeName";
			ddlServiceType.DataValueField = "ServiceTypeCode";
			ddlServiceType.DataBind();
			ListItem liServiceType = new ListItem();
			liServiceType.Text = "";
			liServiceType.Value = "0";
			ddlServiceType.Items.Insert(0, liServiceType);
		}

		public void BindCertifiedBy()
		{
			DataTable dtCertifiedBy = SyntheticScreeningUtils.GetSyntheticCertifiedBy(this);
			ddlCertifiedBy.DataSource = dtCertifiedBy;
			ddlCertifiedBy.DataTextField = "CertifiedBy";
			ddlCertifiedBy.DataValueField = "CertifiedCode";
			ddlCertifiedBy.DataBind();
			ListItem liCertifiedBy = new ListItem();
			liCertifiedBy.Text = "";
			liCertifiedBy.Value = "0";
			ddlCertifiedBy.Items.Insert(0, liCertifiedBy);
		}

		public void BindCategory()
		{
			DataTable dtCategory = SyntheticScreeningUtils.GetSyntheticCategory(this);
			ddlCategory.DataSource = dtCategory;
			ddlCategory.DataTextField = "CategoryName";
			ddlCategory.DataValueField = "CategoryCode";
			ddlCategory.DataBind();
			ListItem liCategory = new ListItem();
			liCategory.Text = "";
			liCategory.Value = "0";
			ddlCategory.Items.Insert(0, liCategory);
		}

		public void BindServiceTime()
		{
			DataTable dtServiceTime = SyntheticScreeningUtils.GetSyntheticServiceTime(this);
			ddlServiceTime.DataSource = dtServiceTime;
			ddlServiceTime.DataTextField = "ServiceTimeName";
			ddlServiceTime.DataValueField = "ServiceTimeCode";
			ddlServiceTime.DataBind();
			ListItem liServiceTime = new ListItem();
			liServiceTime.Text = "";
			liServiceTime.Value = "0";
			ddlServiceTime.Items.Insert(0, liServiceTime);
		}

		public void BindDeliveryMethod()
		{
			DataTable dtDeliveryMethod = SyntheticScreeningUtils.GetSyntheticDeliveryMethod(this);
			ddlDeliveryMethod.DataSource = dtDeliveryMethod;
			ddlDeliveryMethod.DataTextField = "DeliveryMethodName";
			ddlDeliveryMethod.DataValueField = "DeliveryMethodCode";
			ddlDeliveryMethod.DataBind();
			ListItem liDeliveryMethod = new ListItem();
			liDeliveryMethod.Text = "";
			liDeliveryMethod.Value = "0";
			ddlDeliveryMethod.Items.Insert(0, liDeliveryMethod);
		}

		public void BindShippingCourier()
		{
			DataTable dtShippingCourier = SyntheticScreeningUtils.GetCarriers(this);
			ddlShippingCurrier.DataSource = dtShippingCourier;
			ddlShippingCurrier.DataTextField = "CarrierName";
			ddlShippingCurrier.DataValueField = "CarrierCode";
			ddlShippingCurrier.DataBind();
			ListItem liShippingCurrier = new ListItem();
			liShippingCurrier.Text = "";
			liShippingCurrier.Value = "0";
			ddlShippingCurrier.Items.Insert(0, liShippingCurrier);
		}

		public void BindScreeningInstrument()
		{
			DataTable dtScreeningInstrument = SyntheticScreeningUtils.GetSyntheticScreeningInstrument(this);
			ddlScreeningInstrument.DataSource = dtScreeningInstrument;
			ddlScreeningInstrument.DataTextField = "InstrumentName";
			ddlScreeningInstrument.DataValueField = "InstrumentCode";
			ddlScreeningInstrument.DataBind();
			ListItem liScreeningInstrument = new ListItem();
			liScreeningInstrument.Text = "";
			liScreeningInstrument.Value = "0";
			ddlScreeningInstrument.Items.Insert(0, liScreeningInstrument);
		}

		public void BindJewelryType()
		{
			DataTable dtJewelryType = SyntheticScreeningUtils.GetSyntheticJewelryType(this);
			ddlJewelryType.DataSource = dtJewelryType;
			ddlJewelryType.DataTextField = "JewelryTypeName";
			ddlJewelryType.DataValueField = "JewelryTypeCode";
			ddlJewelryType.DataBind();
			ListItem liJewelryType = new ListItem();
			liJewelryType.Text = "";
			liJewelryType.Value = "0";
			ddlJewelryType.Items.Insert(0, liJewelryType);
		}
		#endregion

		#region Datatview Filter
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			BindDataViewGrid();
		}
		protected void OnSortingDataView(object sender, GridViewSortEventArgs e)
		{
			this.BindDataViewGrid(e.SortExpression);
		}
		private string SortDirection
		{
			get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		public void BindDataViewGrid(string sortExpression = null)
		{
			SyntheticDataViewModel objDataView = new SyntheticDataViewModel();
			objDataView.CustomerCode = Convert.ToInt16(txtCustomerCode.Text.ToString().Trim()==""?"0": txtCustomerCode.Text.ToString().Trim());
			objDataView.RetailerID = Convert.ToInt16(ddlRetailer.SelectedValue.ToString().Trim());
			objDataView.PONum = txtPONumber.Text.ToString().Trim();
			objDataView.MemoNum = txtMemo.Text.ToString().Trim();
			objDataView.StyleName = txtStyleName.Text.ToString().Trim();
			objDataView.SKUName = txtSKUName.Text.ToString().Trim();
			objDataView.Status = Convert.ToInt16(ddlPosition.SelectedValue.ToString().Trim());
			objDataView.CertifiedBy = Convert.ToInt16(ddlCertifiedBy.SelectedValue.ToString().Trim());
			objDataView.ServiceTypeCode = Convert.ToInt16(ddlServiceType.SelectedValue.ToString().Trim());
			objDataView.CategoryCode = Convert.ToInt16(ddlCategory.SelectedValue.ToString().Trim());
			objDataView.ServiceTimeCode = Convert.ToInt16(ddlServiceTime.SelectedValue.ToString().Trim());
			objDataView.DeliveryMethodCode = Convert.ToInt16(ddlDeliveryMethod.SelectedValue.ToString().Trim());
			objDataView.CarrierCode = Convert.ToInt16(ddlShippingCurrier.SelectedValue.ToString().Trim());
			objDataView.InstrumentCode = Convert.ToInt16(ddlScreeningInstrument.SelectedValue.ToString().Trim());
			objDataView.JewelryTypeCode = Convert.ToInt16(ddlJewelryType.SelectedValue.ToString().Trim());
			if (txtDate.Text != "")
			{
				DateTime dtDate = DateTime.ParseExact(txtDate.Text.ToString().Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
				objDataView.CreateDate = dtDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
			}
			else
			{
				objDataView.CreateDate = "";
			}
			DataTable dtDataView = SyntheticScreeningUtils.GetSyntheticDataView(objDataView, this);

			if (sortExpression != null)
			{
				DataView dv = dtDataView.AsDataView();
				this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

				dv.Sort = sortExpression + " " + this.SortDirection;
				gvDataView.DataSource = dv;
				Session["ScreeningDataView"] = dv.ToTable();
				EmptyDataGrid.Visible = false;
			}
			else
			{
				gvDataView.DataSource = dtDataView;
				Session["ScreeningDataView"] = dtDataView;
				EmptyDataGrid.Visible = false;
			}
			gvDataView.DataBind();
			gvDataView.Visible = true;
			btnimgExcelExport.Visible = true;
			tdExportButton.Visible = true;
		}
		protected void gvDataView_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvDataView.PageIndex = e.NewPageIndex;
			BindDataViewGrid();
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			txtCustomerCode.Text = "";
			ddlRetailer.SelectedIndex = 0;
			txtPONumber.Text = "";
			txtMemo.Text = "";
			txtStyleName.Text = "";
			txtSKUName.Text = "";
			ddlPosition.SelectedIndex = 0;
			ddlCertifiedBy.SelectedIndex = 0;
			ddlServiceType.SelectedIndex = 0;
			ddlCategory.SelectedIndex = 0;
			ddlServiceTime.SelectedIndex = 0;
			ddlDeliveryMethod.SelectedIndex = 0;
			ddlShippingCurrier.SelectedIndex = 0;
			ddlScreeningInstrument.SelectedIndex = 0;
			ddlJewelryType.SelectedIndex=0;
			txtDate.Text = "";
			EmptyDataGrid.Visible = true;
			gvDataView.Visible = false;
			btnimgExcelExport.Visible = false;
			tdExportButton.Visible = false;
			//gvDataView.DataSource = null;
			//gvDataView.DataBind();
		}
		#endregion

		#region ExcelExport
		protected void btnimgExcelExport_Click(object sender, System.EventArgs e)
		{
			DataSet dsScr = new DataSet();
			dsScr.Merge((DataTable)Session["ScreeningDataView"]);
			Export(dsScr, "SyntheticScreening DataView", false, this);
		}

		public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
		{
			IWorkbook workbook = new XSSFWorkbook();
			var sheet = workbook.CreateSheet("Report");
			//sheet.CreateFreezePane(0, 1, 0, 1);

			//-- Create Style for column headers
			var headerStyle = CreateStyle(workbook, true, 0);
			var cellStyle = CreateStyle(workbook, false, 0);

			var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

			var currrow = 0;
			foreach (DataTable dataTable in dataSet.Tables)
			{
				//-- Remove work column
				//foreach (var hiddenColumn in GetHiddenColumns())
				//{
				//    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
				//}

				//-- Create Columns
				var headerRow = sheet.CreateRow(currrow);
				headerRow.RowStyle = headerStyle;
				for (var i = 0; i < dataTable.Columns.Count; i++)
				{
					var cell = headerRow.CreateCell(i);
					cell.CellStyle = headerStyle;
					cell.SetCellValue(dataTable.Columns[i].ColumnName);

					sheet.SetColumnWidth(i, 20 * 256);
				}
				if (dataSet.Tables.Count == 1)
				{
					var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
					sheet.RepeatingRows = range;
				}

				currrow++;
				//-- Rows
				for (var j = 0; j < dataTable.Rows.Count; j++)
				{
					var row = sheet.CreateRow(currrow);
					row.RowStyle = cellStyle;
					for (var i = 0; i < dataTable.Columns.Count; i++)
					{
						var cell = row.CreateCell(i);
						var data = dataTable.Rows[j][i].ToString();
						if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyleYellow;
						}
						else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyle;
						}
						else
						{
							cell.CellStyle = cellStyle;
						}
						cell.SetCellValue(data);
					}
					currrow++;
				}
				currrow++;
			}


			//-- Save File
			//var dir = GetExportDirectory(p);

			//var filename = fname + ".xlsx";
			//var sw = File.Create(dir + filename);
			//workbook.Write(sw);
			//sw.Close();
			//if (!skipDownload) DownloadExcelFile(filename, p);

			var filename = fname + ".xlsx";
			var Memory = new MemoryStream();
			Memory.Position = 0;
			workbook.Write(Memory);
			p.Session["Memory"] = Memory;
			if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p);

			Memory.Flush();
		}

		private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
		{
			//-- Font
			IFont font = workbook.CreateFont();
			font.Color = IndexedColors.Black.Index;
			if (forHeader)
			{
				font.Boldweight = (short)FontBoldWeight.Bold;
			}

			font.FontHeight = 10;
			font.FontName = "Calibri";


			ICellStyle style = workbook.CreateCellStyle();
			style.SetFont(font);
			if (fillColor != 0)
			{
				style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
				style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

			}
			if (!forHeader)
			{
				//style.WrapText = true;
			}
			//-- Border
			style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
			style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
			style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
			style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
			style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
			style.RightBorderColor = IndexedColors.Grey50Percent.Index;
			style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
			style.TopBorderColor = IndexedColors.Grey50Percent.Index;

			style.Alignment = HorizontalAlignment.Center;
			return style;
		}
		#endregion


	}
}