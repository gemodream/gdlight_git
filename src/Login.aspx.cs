﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;
using Newtonsoft.Json;

namespace Corpt
{
    [ScriptService]
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string machineName = null, pName = null;
            // Put user code to initialize the page here
            if (IsPostBack)
            {
                machineName = Page.Request.Params["p"];
                pName = Page.Request.Params["PName"];
                //machineName = "gsi-ny-6";
                if (machineName != null)
                {
                    Session["MachineName"] = machineName;
                    LabelInfo.Text = machineName;
                }
                else
                    LabelInfo.Text = "No machine name";
                if (pName != null)
                    Session["PName"] = pName;
                return;
            }
            //if (!IsPostBack)
            //    GetOffice(this);

            Session.Abandon();
        }
        //alex
        private string GetIPAddress()
        {
            string IPAddress = null;
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            return IPAddress;
        }
        protected void ActivateClick(object sender, EventArgs e)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;
            var loginModel = new LoginModel { LoginName = LoginTxt.Text, Password = Password.Text };
            loginModel = QueryUtils.GetLogin(loginModel, this);
            string roleId = (string)HttpContext.Current.Session["RoleID"];
            Session["RoleID"] = roleId;

            //--Set Session for Order Creation Real Office ID in ScreeningFront.aspx
            if (HttpContext.Current.Session["RealOfficeID"] == null || HttpContext.Current.Session["RealOfficeID"].ToString() == "")
                HttpContext.Current.Session["RealOfficeID"] = Request.Form["ddlOffice"];

            if (roleId == "33")
            {
                try
                {
                    Response.Redirect("SyntheticDataEntry.aspx");
                }
                catch (Exception ex)
                {
                    LabelInfo.Text = "Login Failed";
                    LabelInfo.ForeColor = Color.Red;
                    Response.Redirect("Login.aspx");
                    //return;
                }
            }
            if (roleId == "32" && loginModel.AuthorOfficeId == 1)
                return;
            if (loginModel.Error)
            {
                LabelInfo.Text = loginModel.ErrorText;
                LabelInfo.ForeColor = Color.Red;
                Session.Clear();
                Session.Abandon();
                return;
            }
            Session["AuthorName"] = loginModel.User.FirstName + " " + loginModel.User.LastName;
            Response.Redirect("Middle.aspx");
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetOffice(string city, string ip)
        {
            HttpContext.Current.Session["IPAddress"] = ip;
            HttpContext.Current.Session["RealOfficeID"] = "";
            string Json = "";
            if (ip != "")
            {
                DataTable dtOffice = QueryUtils.GetOfficeList(ip, city);
                if (dtOffice.Rows.Count == 1)
                    HttpContext.Current.Session["RealOfficeID"] = dtOffice.Rows[0]["OfficeID"].ToString();
                Json = JsonConvert.SerializeObject(dtOffice);
            }
            return Json;
        }
    }
}