﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using System.Xml;
using System.Linq;
using Corpt.TreeModel;

namespace Corpt
{
    public partial class ItemizeOldNew : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Itemize Old/New";
            var tmp = Request.UrlReferrer;
            
            if (IsPostBack) return;
            if (Request.QueryString["OrderCode"] != null)
            {
                NewOrder.Text = (string)Request.QueryString["OrderCode"];
                ItemsAvailable.Value = (string)Request.QueryString["MaxItems"];
                NewOrder.Enabled = false;
                ItemsToAdd.Text = "";
            }
            var orderModel = QueryUtils.GetOrderInfo(NewOrder.Text.Trim(), this);
            Session["OrderModel"] = orderModel;
            var memoList = QueryUtils.GetOrderMemos(orderModel, this);
            SetViewState(memoList, SessionConstants.ItemizeMemoList);
            MemoListField.DataTextField = "MemoNumber";
            MemoListField.DataValueField = "MemoId";
            MemoListField.DataSource = memoList;
            MemoListField.DataBind();
            OldItemsTree.Visible = false;
            OldItemsTreePanel.Visible = false;
            ItemField.Enabled = false;
            Session["SaveBatchList"] = null;
            /*
            SetViewState(new List<string>(), SessionConstants.ItemizeLotList);
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            GridNewBatches.DataSource = null;
            GridNewBatches.DataBind();
            Session["BatchData"] = null;
            Session["BatchDataLot"] = null;
            */
            ItemField.Focus();
        }

        #region Save Button
        protected void OnSaveClick(object sender, EventArgs e)
        {
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            if (OldItemsTree.CheckedNodes.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items Found!" + "\")</SCRIPT>");
                return;
            }
            if (MemoListField.SelectedItem.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Memo not found!" + "\")</SCRIPT>");
                return;
            }
            List<ItemizedModel> batchList = new List<ItemizedModel>();
            List<string> oldItemsList = new List<string>();

            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                int j = 1;
                if (node.Text.Contains("Batch"))
                {
                    int i = 0;
                    string nodeId = node.Text;
                    int idx1 = nodeId.IndexOf('(');
                    int idx2 = nodeId.IndexOf(')');
                    nodeId = nodeId.Substring(idx1 + 1, idx2 - idx1 -1);
                    string sku = nodeId;

                    foreach (TreeNode child in OldItemsTree.CheckedNodes)
                    {
                        if (child.Text.Contains("Item") && child.Parent.Text == node.Text)
                        {
                            int idx3 = child.Text.IndexOf('(');
                            int idx4 = child.Text.IndexOf(')');
                            string oldItem = child.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList.Add(oldItem);
                            i++;
                        }
                    }
                    ItemizedModel batch = new ItemizedModel();
                    batch.Items = i.ToString();
                    batch.Memo = MemoListField.SelectedItem.Text;
                    batch.Sku = sku;
                    batch.ItemsSet = j.ToString();
                    batchList.Add(batch);
                    j++;
                }
                
            }
            OrderModel orderModel = null;
            if (Session["OrderModel"] != null)
                orderModel = (OrderModel)Session["OrderModel"];
            else
                orderModel = QueryUtils.GetOrderInfo(NewOrder.Text.Trim(), this);
            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            //var cpModel = cpList.Find(m => m.CpOfficeIdAndCpId == cp);
            var memoList = QueryUtils.GetOrderMemos(orderModel, this);

            List<BatchNewModel> saveBatchList = new List<BatchNewModel>();
            foreach (var item in batchList)
            {
                List<BatchNewModel> fullBatchList = new List<BatchNewModel>();
                //item.
                //-- Customer Program is required
                //ItemizedModel item = new ItemizedModel();//alex temp
                //var cp = CpListField.SelectedValue;
                var cp = item.Sku;
                if (string.IsNullOrEmpty(cp))
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Customer Program is required!" + "\")</SCRIPT>");
                    return;
                }
                       
                    //var cpModel = cps.Find(m => m.CpOfficeIdAndCpId == cp);
                var cpModel = cpList.Find(m => m.CustomerProgramName == cp);
                if (cpModel == null) return;
                    
                        //-- Memo Number is required
                        //var memo = MemoListField.SelectedValue;
                var memo = item.Memo;
                if (string.IsNullOrEmpty(memo))
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                    return;
                }
                        //var memos = GetViewState(SessionConstants.ItemizeMemoList) as List<MemoModel>;
                        //if (memos == null) return;

                        //var memoModel = memos.Find(m => m.MemoId == memo);
                var memoModel = memoList.Find(m => m.MemoNumber == memo);
                if (memoModel == null) return;

                var model = new ItemizeGoModel
                {
                    Cp = cpModel,
                    Memo = memoModel,
                    //NumberOfItems = GetNumberItems(),
                    NumberOfItems = Convert.ToInt16(item.Items),
                    Order = orderModel,//GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel
                    LotNumbers = oldItemsList //new List<string>()
                        };
                        var newBatches = QueryUtils.ItemizeAddOldNewItemsByCount(model, this);
                for (int y=0; y<= newBatches.Count-1; y++)
                //foreach (var batch in newBatches)
                {
                    var batch = newBatches[y];
                    fullBatchList.Add(batch);
                    saveBatchList.Add(batch);
                    int x = (y * 25);
                    //foreach (var lot in model.LotNumbers)
                    int batchItems = Convert.ToInt16(batch.ItemsAffected);
                    for (int z=0; z<=batchItems-1; z++)
                    {
                        var lot = model.LotNumbers[z + x];
                        foreach (TreeNode node in OldItemsTree.CheckedNodes)
                        {
                            if (node.Text.Contains("Batch"))
                            {
                                foreach (TreeNode childnode in node.ChildNodes)
                                {
                                    if (childnode.Text.Contains(lot))
                                    {
                                        string newItem = batch.OrderCode + @"." + batch.BatchCode + @"." + Utils.FillToTwoChars((z+1).ToString());
                                        childnode.Text += @"(" + newItem + @")";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                //Itemize1 itemize1 = new Itemize1();
                //bool printed = PrintLabels(fullBatchList);
                //if (!printed)
                //    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                //                "Error printing labels!" + "\")</SCRIPT>");
            }//foreach
            Session["SaveBatchListOldNew"] = saveBatchList;
        }
        protected void OnItemCountClick(object sender, EventArgs e)
        {
            if (ItemsToAdd.Text == "")
            {
                ItemField.Enabled = false;
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Enter items number to add. " + "\")</SCRIPT>");
                return;
            }
            int added = Convert.ToInt16(ItemsToAdd.Text);
            int avail = Convert.ToInt16(ItemsAvailable.Value);
            if (added > avail)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Available items: " + ItemsAvailable.Value + "\")</SCRIPT>");
                ItemsToAdd.Text = "";
            }
            ItemField.Enabled = true;
        }
        protected void OnItemsCopyClick(object sender, EventArgs e)
        {
            OnItemsCopy();
        }
        protected void OnItemsCopy()
        { 
            // LoadOfficesTree();
            //LoadOfficesTreeOld();
            //return;
            if (OrderListBox.Items.Count == 0)
                return;
            OldItemsTree.Visible = true;
            OldItemsTreePanel.Visible = true;
            OldItemsTree.Nodes.Clear();
            try
            {
                int i = 1;
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "" } };
                var list = OrderListBox.Items.Cast<ListItem>().OrderBy(item => item.Text).ToList();
                OrderListBox.Items.Clear();
                foreach (ListItem listItem in list)
                {
                    OrderListBox.Items.Add(listItem);
                }
                string prevordercode = null, prevbatchcode = null, previtemcode = null;
                string sku = null;
                foreach (var item in OrderListBox.Items)
                {
                    Utils.DissectItemNumber(item.ToString(), out string ordercode, out var batchcode, out var itemcode);

                    bool newTree = false, newBatch = false;
                    if (prevordercode == null)
                    {
                        prevordercode = ordercode;
                        prevbatchcode = batchcode;
                        previtemcode = itemcode;
                        newTree = true;
                    }
                    if ((ordercode != prevordercode) || (ordercode == prevordercode && prevbatchcode != batchcode))
                        newBatch = true;
                    if (newTree || newBatch)
                    {
                        DataRow dr = QueryUtils.GetNewCustomerProgramInstanceByBatchCode(Convert.ToInt32(ordercode), Convert.ToInt16(batchcode), Convert.ToInt16(itemcode), this);
                        if (dr != null)
                        {
                            sku = dr["CustomerProgramName"].ToString();
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + item.ToString() + "\")</SCRIPT>");
                            return;
                        }
                    }
                    if (newTree)
                    {
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                    if (newBatch)
                    {
                        i++;
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                        int orderNumber = OrderListBox.Items.Count;
                        data.Add(new TreeViewModel { ParentId = i.ToString(), Id = (orderNumber + i).ToString(), DisplayName = @"Item(" + Utils.FillToFiveChars(ordercode) + @"." + Utils.FillToThreeChars(batchcode, ordercode) + @"." + Utils.FillToTwoChars(itemcode) + @")" });
                }
                    //data.Add(new TreeViewModel { ParentId = "1", Id = "2", DisplayName = itemcode });
                    //string order = 
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                OldItemsTree.Nodes.Add(rootNode);
                
                rootNode.ExpandAll();
                OldItemsTree.Enabled = true;
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + msg + "\")</SCRIPT>");
                return;
            }
            
        }

        private string GetCheckedItems()
        {
            int i = 0;
            string item = null;
            string oldSku = null;
                //return null;
            
            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                if (node.Depth == 1)
                {
                    int skuStart = node.Text.IndexOf("(");
                    int skuStop = node.Text.IndexOf(")");
                    oldSku = node.Text.Substring(skuStart+1, node.Text.Length - skuStart - 2);
                    continue;//-- 0 - all, 1 - country, 2 - office
                }
                else if (node.Depth == 2)
                {
                    item = node.Text;
                    return item + @"," + oldSku;
                    //i++;
                }
            }
            /* alex
            if (i > 1)
                return "all";
            else
                return item;
                */
            return null;
        }
        /*
        protected void OnItemsCopyClickOld(object sender, EventArgs e)
        {
            // LoadOfficesTree();
            //LoadOfficesTreeOld();
            //return;
            OldItemsTree.Nodes.Clear();
            try
            {
                int i = 1;
                foreach (var item in OrderListBox.Items)
                {
                    Utils.DissectItemNumber(item.ToString(), out string ordercode, out var batchcode, out var itemcode);
                    var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Batch" + FillToThreeChars(i.ToString()) } };
                    i++;
                    data.Add(new TreeViewModel { ParentId = "0", Id = "1", DisplayName = FillToFiveChars(ordercode) + @"." + FillToThreeChars(batchcode) + @"." + FillToTwoChars(itemcode) });
                    //data.Add(new TreeViewModel { ParentId = "1", Id = "2", DisplayName = itemcode });
                    //string order = 
                    var root = TreeUtils.GetRootTreeModel(data);
                    var rootNode = new TreeNode(root.DisplayName, root.Id);

                    TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                    OldItemsTree.Nodes.Add(rootNode);
                    rootNode.Expand();
                    OldItemsTree.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                return;
            }

        }
        */
        protected void OnChangeCPButtonClick(object sender, EventArgs e)
        {
            string checkedItem = GetCheckedItems();
            if (checkedItem == null)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items found for \")</SCRIPT>");
                return;
            }
            string[] items = checkedItem.Split(',');
            string item = items[0];
            string oldSku = items[1];
            int idx1 = item.IndexOf('(');
            int idx2 = item.IndexOf(')');
            item = item.Substring(idx1 + 1, idx2 - idx1 - 1);
            Response.Redirect("ItemizingChangeCP.aspx?Item=" + item + "&OrderCode=" + NewOrder.Text);
        }//OnChangeCPButtonClick

        protected void OnPreloadButtonClick(object sender, EventArgs e)
        {
            if (Session["BackToItemizing"] == null)
                return;
            ItemizedChangedCPModel changeResults = new ItemizedChangedCPModel();
            changeResults = (ItemizedChangedCPModel)Session["BackToItemizing"];
            
            bool reloaded = ReloadTree(changeResults);
            
        }
        protected void OnPrintButtonClick(object sender, EventArgs e)
        {
            List<BatchNewModel> saveBatchList = new List<BatchNewModel>();
            saveBatchList = (List<BatchNewModel>)Session["SaveBatchListOldNew"];
            bool printed = PrintLabels(saveBatchList);
            if (!printed)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Error printing labels!" + "\")</SCRIPT>");
        }
        private bool ReloadTree(ItemizedChangedCPModel changeResults)
        {
            string newOrder = changeResults.newOrder;
            string oldItem = changeResults.oldItem;
            string oldSku = changeResults.oldSku;
            string newSku = changeResults.newSku;
            
            try
            {
                //OldItemsTree.Nodes.Add(new TreeNode(newSku));
                
                int count = 0, itemCount = 0;
                foreach (TreeNode node in OldItemsTree.Nodes)//find the number of batches
                {
                    count = node.ChildNodes.Count;
                }
                foreach (TreeNode node in OldItemsTree.CheckedNodes)//find the number of items in selected batch
                {
                    if (node.Text.Contains("Batch") && !node.Text.Contains("New"))
                    {
                        itemCount = node.ChildNodes.Count;
                        break;
                    }
                    else if (node.Text.Contains("Item"))
                    {
                        itemCount++;
                    }
                }
                int z = 0;
                string[] oldItemsList = new string[itemCount];
                foreach (TreeNode node in OldItemsTree.CheckedNodes)
                {
                    if (node.Text.Contains("Batch"))
                    {
                        foreach (TreeNode childnode in node.ChildNodes)
                        {
                            int idx3 = childnode.Text.IndexOf('(');
                            int idx4 = childnode.Text.IndexOf(')');
                            string item = childnode.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList[z] = item;
                            z++;
                        }
                    }
                }
                    RemoveCheckedNodes();
                
                TreeNode tn = new TreeNode("New Batch " + (count+1).ToString() + @"(" + newSku + @")");
                tn.Checked = true;
                tn.Text.Replace(@"color=""Black""", @"color=""Red""");
                for (int i = 1; i <= itemCount; i++)
                {
                    TreeNode child = new TreeNode(@"Item_" + i.ToString() + @"(" + oldItemsList[i-1] + @")");
                    child.Checked = true;
                    tn.ChildNodes.Add(child);
                }
                tn.Expand();
                OldItemsTree.Nodes.Add(tn);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
            return true;

            /*
            if (node.Depth == 1)
                {
                    int skuStart = node.Text.IndexOf("(");
                    int skuStop = node.Text.IndexOf(")");
                    oldSkuTree = node.Text.Substring(skuStart + 1, node.Text.Length - skuStart - 2);
                    continue;//-- 0 - all, 1 - country, 2 - office
                }
                else if (node.Depth == 2)
                {
                    item = node.Text;
                    //return item + @"," + oldSku;
                    //i++;
                }
            }
            return true;
            */
        }

        private void RemoveCheckedNodes()
        {
            foreach (TreeNode node in OldItemsTree.CheckedNodes)//uncheck old number
            {
                if (node != null)
                    node.Checked = false;
                else
                    return;
                RemoveCheckedNodes();
                int count = OldItemsTree.CheckedNodes.Count;
                if (count == 0)
                    return;
                //node.SelectAction = TreeNodeSelectAction.None;
            }
        }
        protected void OnDeleteBatchButtonClick(object sender, EventArgs e)
        {
            for (int j = 2; j >= 0; j--)
            {
                var checkedNodes = OldItemsTree.CheckedNodes;
                int count = checkedNodes.Count;
                if (count == 0)
                    break;
                var abc = checkedNodes[0];
                for (int i = 0; i < count; i++)
                {
                    if (checkedNodes.Count == 0)
                        continue;
                    var node = checkedNodes[0];
                    if (node.Depth == j)
                        node.Parent.ChildNodes.Remove(node);
                }
            }
            /*
           foreach (TreeNode node in OldItemsTree.CheckedNodes)
           {
               if (node.Parent != null)
                   node.Parent.ChildNodes.Remove(node);
               else
               {
                   OldItemsTree.Nodes.Remove(node);
                   break;
               }

           }

           foreach (TreeNode node in OldItemsTree.CheckedNodes)//uncheck old number
           {
               if (node.Depth == 2)
               {
                   OldItemsTree.Nodes.Remove(node);
               }
               //node.SelectAction = TreeNodeSelectAction.None;
           }
           foreach (TreeNode node in OldItemsTree.CheckedNodes)//find the number of items in selected batch
           {
               if (node.Depth == 1)
               {
                   OldItemsTree.Nodes.Remove(node);
                   break;
               }
           }
           */
        }//OnDeleteBatchButtonClick
        /*
        private void LoadOfficesTree()
        {
            var countries = QueryUtils.GetCountryOffices(this);
            OldItemsTree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Offices" } };
            foreach (var country in countries)
            {
                if (country.Country == "USA" || country.Country == "India" || country.Country == "China")
                {
                    data.Add(new TreeViewModel { ParentId = "0", Id = country.Country, DisplayName = country.Country });
                    foreach (var office in country.Offices)
                    {
                        data.Add(new TreeViewModel { ParentId = country.Country, Id = office.OfficeId, DisplayName = office.DisplayOffice });
                    }
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            OldItemsTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }
        
        private void LoadOfficesTreeOld()
        {
            var countries = QueryUtils.GetCountryOffices(this);
            OfficeTree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Offices" } };
            foreach (var country in countries)
            {
                if (country.Country == "USA" || country.Country == "India" || country.Country == "China")
                {
                    data.Add(new TreeViewModel { ParentId = "0", Id = country.Country, DisplayName = country.Country });
                    foreach (var office in country.Offices)
                    {
                        data.Add(new TreeViewModel { ParentId = country.Country, Id = office.OfficeId, DisplayName = office.DisplayOffice });
                    }
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            OfficeTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }
        */
        protected void OnItemAddClick(object sender, EventArgs e)
        {
            if (ItemsToAdd.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No items to add \")</SCRIPT>");
                return;
            }
            int itemAvailable = Convert.ToInt16(ItemsAvailable.Value);
            int itemsNew = Convert.ToInt16(ItemsToAdd.Text);
            if (itemAvailable == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No available items \")</SCRIPT>");
                return;
            }
            var item = ItemField.Text.Trim();
            if (item.Length == 0)
                return;
            //var itemList = new List<string>();
            //if (itemList.Contains(item)) return;
            var itemList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            if (itemList.Contains(item)) return;
            itemList.Add(item);
            SetViewState(itemList, SessionConstants.ItemizeLotList);

            OrderListBox.DataSource = itemList;
            OrderListBox.DataBind();
            OrderListBox.SelectedValue = item;
            ItemField.Text = "";
            ItemField.Focus();
            ItemsAvailable.Value = (itemAvailable - 1).ToString();
            ItemsToAdd.Text = (itemsNew - 1).ToString();
            if ((itemsNew - 1) == 0)
                OnItemsCopy();
        }//OnItemAddClick

        protected void OnClearButtonClick(object sender, EventArgs e)
        {
            //ItemField.Text = "";
            ItemsToAdd.Text = "";
            OrderListBox.Items.Clear();
            OldItemsTree.Nodes.Clear();
            OldItemsTreePanel.Visible = false;
            MemoListField.SelectedIndex = -1;
            ItemField.Enabled = false;

        }
        protected void OnItemDelClick(object sender, EventArgs e)
        {
            var item = OrderListBox.SelectedValue;
            if (string.IsNullOrEmpty(item)) return;
            var itemList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            var idxNext = OrderListBox.SelectedIndex;

            itemList.Remove(item);
            ItemsAvailable.Value = (Convert.ToInt16(ItemsAvailable.Value) + 1).ToString();
            ItemsToAdd.Text = (Convert.ToInt16(ItemsToAdd.Text) + 1).ToString();
            SetViewState(itemList, SessionConstants.ItemizeLotList);
            OrderListBox.DataSource = itemList;
            OrderListBox.DataBind();
            if (idxNext >= itemList.Count && itemList.Count > 0)
            {
                OrderListBox.SelectedIndex = 0;
            }
            if (idxNext < itemList.Count)
            {
                OrderListBox.SelectedIndex = idxNext;
            }
            if (OrderListBox.Items.Count == 0)
            {
                OldItemsTree.Visible = false;
                OldItemsTreePanel.Visible = false;
            }
            else
            {
                bool removed = RemoveTreeNode(OldItemsTree.Nodes, item);
                /*
                foreach (TreeNode node in OldItemsTree.Nodes)
                {
                    string nodeid = getNodeValue(node.Text);
                    if (nodeid == item)
                    {
                        OldItemsTree.Nodes.Remove(node);
                        break;
                    }
                }
                */
            }

        }

        private string getNodeValue (string nodeText)
        {
            try
            {
                if (nodeText.Length == 0)
                    return null;
                int idx1 = nodeText.IndexOf('(');
                int idx2 = nodeText.IndexOf(')');
                return nodeText.Substring(idx1 + 1, idx2 - idx1 - 1);
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
        
        private bool RemoveTreeNode(TreeNodeCollection itemsTreeNodes, string item)
        {
            foreach (TreeNode node in itemsTreeNodes)
            {
                string nodeid = getNodeValue(node.Text);
                if (nodeid != null)
                    nodeid = nodeid.Replace(".", "");
                
                if (nodeid == item)
                {
                    itemsTreeNodes.Remove(node);
                    return true;
                }
                else
                {
                    if (node.ChildNodes.Count > 0)
                    {
                        bool removed = RemoveTreeNode(node.ChildNodes, item);
                        if (removed)
                            return true;
                    }
                }
            }
            return false;
        }

            /* alex protected void OnSaveInspectedClick(object sender, EventArgs e)
             {
                 var Order = GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel;
                 int itemNumber = Convert.ToInt16(InspectedItems.Text);
                 bool updated = QueryUtils.UpdateInspectedItems(Order, itemNumber, this);
                 if (!updated)
                     System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                             "Please, Set Lot Numbers!" + "\")</SCRIPT>");
             }
             alex */
            /*
           protected void OnBatchAddClick(object sender, EventArgs e)
               {
                   //CountItemsField.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
                   int inspected = Convert.ToInt16(InspectedItems.Text);
                   int itemized = Convert.ToInt16(itemizedItems.Text);
                   int newItems = Convert.ToInt16(CountItemsField.Text);
                   int availableItems = inspected - itemized;
                   if (availableItems > 0)
                   {
                       if (availableItems < newItems)
                           System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Cannot itemize. Available items: " + availableItems.ToString() + "\")</SCRIPT>");
                   }
                   else
                   {
                       System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Cannot itemize. Inspected items less than itemized." + "\")</SCRIPT>"); ;
                   }

                       List<ItemizedModel> batchList = new List<ItemizedModel>();
                       ItemizedModel item = new ItemizedModel();
                       if (Session["BatchData"] != null)
                           batchList = (List<ItemizedModel>)Session["BatchData"];
                       item.Items = CountItemsField.Text;
                       item.Memo = MemoListField.SelectedItem.Text;
                       int setNumber = batchList.Count;
                       if (setNumber == 0)
                       {
                           item.ItemsSet = "1";
                           batchList.Add(item);
                           GridNewBatches.DataSource = batchList;
                           GridNewBatches.DataBind();
                           Session["BatchData"] = batchList;
                       }
                       else
                       {
                           item.ItemsSet = (setNumber + 1).ToString();
                           //batchList = (List<ItemizedModel>)GridNewBatches.DataSource;
                           batchList.Add(item);
                           GridNewBatches.DataSource = batchList;
                           GridNewBatches.DataBind();
                           Session["BatchData"] = batchList;
                       }


                       item.Items = CountItemsField.Text;
                       item.Memo = MemoListField.SelectedItem.Text;



                       if (setNumber == 0)
                       {
                           item.ItemsSet = "1";
                           batchList.Add(item);
                           GridNewBatches.DataSource = batchList;
                           GridNewBatches.DataBind();
                           Session["BatchDataLot"] = batchList;
                       }
                       else
                       {
                           item.ItemsSet = (setNumber + 1).ToString();
                           //batchList = (List<ItemizedModel>)GridNewBatches.DataSource;
                           batchList.Add(item);
                           GridNewBatches.DataSource = batchList;
                           GridNewBatches.DataBind();
                           Session["BatchDataLot"] = batchList;
                       }

               }
               */
            #endregion
            #region Refresh Button
            protected void OnRefreshClick(object sender, EventArgs e)
            {
                List<ItemizedModel> batchList = new List<ItemizedModel>();
                List<ItemizedLotModel> batchListLot = new List<ItemizedLotModel>();
                
                
                    batchList = (List<ItemizedModel>)Session["BatchData"];
                
            }
            #endregion



            #region Order search, Cp changes, Picture show
            

            private void ShowPicture(string dbPicture)
            {
                //string imgPath;
                string errMsg;
                var ms = new MemoryStream();
                var fileType = "";
                var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
                //var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
                if (result)
                {
                    var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                    itemPicture.ImageUrl = ImageUrl;
                    itemPicture.Visible = true;
                }
                //-- Path to picture
                Picture2PathField.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
                ErrorPictureField.Text = errMsg;
            }
            #endregion

            #region Print labels


            protected void OnPreviewLabelDataBound(object sender, DataGridItemEventArgs e)
            {
                if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
                var labelModel = e.Item.DataItem as PrintLabelModel;
                if (labelModel == null) return;
                foreach (var val in labelModel.PaintValues)
                {
                    var fld = e.Item.FindControl(val.CellName) as Label;
                    if (fld != null) fld.Text = val.CellValue;
                }
            }
            protected void OnPreviewLabelDataBound2(object sender, DataGridItemEventArgs e)
            {
                if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
                var label2Model = e.Item.DataItem as PrintLabel2Model;
                if (label2Model == null) return;
                foreach (var val in label2Model.LabelOne.PaintValues)
                {
                    var fld = e.Item.FindControl(val.CellName + "_l") as Label;
                    if (fld != null) fld.Text = val.CellValue;
                }
                foreach (var val in label2Model.LabelTwo.PaintValues)
                {
                    var fld = e.Item.FindControl(val.CellName + "_r") as Label;
                    if (fld != null) fld.Text = val.CellValue;
                }
            }
        public bool PrintLabels(List<BatchNewModel> newbatches)
        {
            string sReportPath = null;
            string sReportsDir = @"\\mars\reports";
            string filename = null; 

            var stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Labels");
                foreach (var batch in newbatches)
                {
                    DataSet dsBatch = QueryUtils.GetBatchWithCustomer(batch.BatchId, this);
                    if (dsBatch == null || dsBatch.Tables[0].Rows.Count == 0)
                    {
                        return false;
                    }
                    DataSet dsItems = QueryUtils.GetItemByCode(dsBatch.Tables[0].Rows[0]["GroupCode"].ToString(),
                    dsBatch.Tables[0].Rows[0]["BatchCode"].ToString(), this);
                    DataSet dsGroup = QueryUtils.GetGroupWithCustomer(dsBatch.Tables[0].Rows[0]["GroupOfficeID"].ToString(), dsBatch.Tables[0].Rows[0]["GroupID"].ToString(), this);
                    DataRow rGroup = dsGroup.Tables[0].Rows[0];
                    DataSet dsServiceType = QueryUtils.GetServiceType(rGroup["ServiceTypeID"].ToString(), this);
                    StringBuilder sbOldNumbers = new StringBuilder("");
                    if (dsItems.Tables[0].Rows[0]["PrevGroupCode"].ToString() != "")
                    {
                        sReportPath = sReportsDir + @"batch_label_on.xls";

                        foreach (DataRow drItem in dsItems.Tables[0].Rows)
                        {
                            if (sbOldNumbers.Length != 0) sbOldNumbers.Append(" ");
                            var prevGroupCode = Utils.FillToFiveChars(drItem["PrevGroupCode"].ToString());
                            var prevBatchCode = Utils.FillToThreeChars(drItem["PrevBatchCode"].ToString(), prevGroupCode);
                            var fillToTwoChars = Utils.FillToTwoChars(drItem["PrevItemCode"].ToString());
                            sbOldNumbers.Append(prevGroupCode + "." + prevBatchCode + "." + fillToTwoChars + @",");
                        }
                    }
                    else
                        sReportPath = sReportsDir + @"Labal_Batch.xls";
                    DataRow rBatch = dsBatch.Tables[0].Rows[0];
                    string sDeliveryMethod = null, sAccount = null, sCarrier = null;
                    if (rBatch["WeShipCarry"].ToString() == "1")
                    {
                        sDeliveryMethod = "We Ship Carry";
                        try
                        {
                            sAccount = rBatch["Account"].ToString();
                            //DataSet dsCustomerTypeEx = gemoDream.Service.GetCustomerTypeEx();
                            DataSet tblCarriers = QueryUtils.GetCarriers(this);
                            DataRow[] rCarrier = tblCarriers.Tables[0].Select("CarrierID=" + rBatch["CarrierID"]);
                            sCarrier = rCarrier[0]["CarrierName"].ToString();
                            sDeliveryMethod = "We Use Their Account To Ship";
                        }
                        catch
                        {
                            sAccount = "";
                            sCarrier = "";
                        }
                    }
                    string gsiOrder = Utils.FillToFiveChars(rBatch["GroupCode"].ToString());
                    string gsiBatch = Utils.FillToThreeChars(rBatch["BatchCode"].ToString(), gsiOrder);

                    string sOrderDate = rBatch["CreateDate"].ToString();
                    filename = @"itemizing_batch_label_on_" + gsiOrder + @"." + gsiBatch + @"_" + DateTime.Now.ToString() + ".xml";
                    System.DateTime dOrderDate = System.DateTime.Parse(sOrderDate);
                    writer.WriteStartElement("Batch");
                    writer.WriteElementString("GsiBatch", gsiBatch); //0
                    writer.WriteElementString("GroupCode", gsiOrder);//1
                    writer.WriteElementString("CustomerName", rBatch["CustomerName"].ToString()); //2
                    writer.WriteElementString("ItemsQuantity", rBatch["ItemsQuantity"].ToString());//3
                    if (sbOldNumbers.Length > 0)
                        writer.WriteElementString("OldNumbers", sbOldNumbers.ToString());//4
                    else
                        writer.WriteElementString("OldNumbers", "");
                    writer.WriteElementString("DeliveryMethod", sDeliveryMethod);//5
                    writer.WriteElementString("Carrier", sCarrier);//6
                    writer.WriteElementString("Account", sAccount);//8
                    writer.WriteElementString("Sku", rBatch["CustomerProgramName"].ToString());
                    writer.WriteElementString("MemoNumber", rBatch["MemoNumber"].ToString());//9
                    if (rBatch["ItemsWeight"].ToString() != "")
                        writer.WriteElementString("ItemWeight", rBatch["ItemsWeight"].ToString() + " ct.");//10
                    else
                        writer.WriteElementString("ItemWeight", "");
                    writer.WriteElementString("DdDate", dsBatch.Tables[0].Rows[0]["CreateDate"].ToString());//11
                    writer.WriteElementString("OrderDate", dOrderDate.Date.ToShortDateString());//12
                    writer.WriteElementString("TimeOfDay", dOrderDate.TimeOfDay.ToString());//13
                    writer.WriteElementString("ExtPhone", rBatch["ExtPhone"].ToString());//14
                    if (!(sbOldNumbers.Length > 0))
                    {
                        DataSet dsCheckedOperations = QueryUtils.GetCheckedOperations(batch.BatchId, this);
                        if (dsCheckedOperations.Tables[0].Rows.Count > 0)
                        {
                            string operationTypeName = null;
                            for (int i = 0; i < System.Math.Min(dsCheckedOperations.Tables[0].Rows.Count, 10); i++)
                            {
                                if (i == 0)
                                {
                                    operationTypeName = dsCheckedOperations.Tables[0].Rows[i][0].ToString();
                                }
                                else //if(i>0 && i<10)
                                {
                                    operationTypeName += dsCheckedOperations.Tables[0].Rows[i][0].ToString() + @",";
                                }
                            }
                            writer.WriteElementString("OperationTypeName", operationTypeName);
                        }
                    }
                    if (dsServiceType.Tables[0].Rows.Count > 0)
                        writer.WriteElementString("ServiceType", dsServiceType.Tables[0].Rows[0]["ServiceTypeName"].ToString());
                    else
                        writer.WriteElementString("ServiceType", "");
                    writer.WriteEndElement();
                    DataSet dsRecvSet = QueryUtils.GetItem_New_1(batch.BatchId, this);
                    int itemCount = dsRecvSet.Tables[0].Rows.Count;
                    if (dsRecvSet != null && itemCount > 0)
                    {
                        DataRow sarinRow = dsRecvSet.Tables[0].Rows[0];
                        string sarinLRID = sarinRow["SarinLRID"].ToString();
                        if (sarinLRID == "2000")
                        {
                            /*
                            DataSet dsResults = QueryUtils.GetCIDInfo()
                            dsResults.Tables.Add("CIDInfo");
                            dsResults.Tables[0].Columns.Add("GroupCode");
                            dsResults.Tables[0].Columns.Add("Batch");
                            dsResults.Tables[0].Columns.Add("Item");
                            dsResults.Tables[0].Rows.Add(dsResults.Tables[0].NewRow());

                            dsResults.Tables[0].Rows[0]["GroupCode"] = sGroupCode;
                            dsResults.Tables[0].Rows[0]["Batch"] = sBatchID;
                            dsResults.Tables[0].Rows[0]["Item"] = sItemCode;

                            dsResults = Service.ProxyGenericGet(dsResults);
                            */
                        }
                        int i = 1;
                        foreach (DataRow row in dsRecvSet.Tables[0].Rows)
                        {
                            if (sarinLRID == "2000")//CID
                            {
                                writer.WriteStartElement("CID");
                                string groupCode = Utils.FillToFiveChars(row["GroupCode"].ToString());
                                DataSet dsresults = QueryUtils.GetCIDInfo(
                                    groupCode, 
                                    Utils.FillToThreeChars(row["BatchCode"].ToString(), groupCode), 
                                    Utils.FillToTwoChars(row["ItemCode"].ToString()), this);
                                if (dsresults != null && dsresults.Tables[0].Rows.Count != 0)
                                {
                                    var cidRow = dsresults.Tables[0].Rows[0];
                                    i++;
                                    writer.WriteElementString("GroupCode", Utils.FillToFiveChars(row["GroupCode"].ToString()));
                                    writer.WriteElementString("BatchCode", Utils.FillToThreeChars(row["BatchCode"].ToString(), Utils.FillToFiveChars(row["GroupCode"].ToString())));
                                    writer.WriteElementString("ItemCode", Utils.FillToTwoChars(row["ItemCode"].ToString()));
                                    int itemCode = Convert.ToInt16(row["ItemCode"].ToString());
                                    if (itemCode == itemCount)
                                        writer.WriteElementString("EndOfBatch", "Y");
                                    else
                                        writer.WriteElementString("EndOfBatch", "N");
                                    writer.WriteElementString("PrevGroupCode", Utils.FillToFiveChars(row["PrevGroupCode"].ToString()));
                                    writer.WriteElementString("PrevBatchCode", Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), Utils.FillToFiveChars(row["PrevGroupCode"].ToString())));
                                    writer.WriteElementString("PrevItemCode", Utils.FillToTwoChars(row["PrevItemCode"].ToString()));
                                    writer.WriteElementString("Prefix", cidRow["Prefix"].ToString());
                                    writer.WriteElementString("Weight", row["Weight"].ToString());
                                    writer.WriteElementString("SarinLRID", row["SarinLRID"].ToString());
                                    writer.WriteElementString("SarinID", row["SarinID"].ToString());
                                    writer.WriteEndElement();
                                }
                            }
                            else
                            {
                                writer.WriteStartElement("Item");
                                i++;
                                writer.WriteElementString("GroupCode", Utils.FillToFiveChars(row["GroupCode"].ToString()));
                                writer.WriteElementString("BatchCode", Utils.FillToThreeChars(row["BatchCode"].ToString(), Utils.FillToFiveChars(row["GroupCode"].ToString())));
                                writer.WriteElementString("ItemCode", Utils.FillToTwoChars(row["ItemCode"].ToString()));
                                int itemCode = Convert.ToInt16(row["ItemCode"].ToString());
                                if (itemCode == itemCount)
                                    writer.WriteElementString("EndOfBatch", "Y");
                                else
                                    writer.WriteElementString("EndOfBatch", "N");
                                writer.WriteElementString("PrevGroupCode", Utils.FillToFiveChars(row["PrevGroupCode"].ToString()));
                                writer.WriteElementString("PrevBatchCode", Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), Utils.FillToFiveChars(row["PrevGroupCode"].ToString())));
                                writer.WriteElementString("PrevItemCode", Utils.FillToTwoChars(row["PrevItemCode"].ToString()));
                                writer.WriteElementString("Sku", row["CustomerProgramName"].ToString());
                                writer.WriteElementString("Memo", row["MemoNumber"].ToString());
                                writer.WriteElementString("Prefix", row["Prefix"].ToString());
                                writer.WriteElementString("Weight", row["Weight"].ToString());
                                writer.WriteElementString("SarinLRID", row["SarinLRID"].ToString());
                                writer.WriteElementString("SarinID", row["SarinID"].ToString());
                                writer.WriteEndElement();
                            }
                        }
                    }



                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            DownloadExcelFile(filename, this);
            return true;
        }
        public void DownloadExcelFile(String filename, Page p)
        {

            //p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
            //string redirect = "<script>location.assign(window.location);</script>";
            //p.Response.Write(redirect);
            p.Response.Clear();
            //p.Response.Buffer = true; //alex
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.OutputStream.Flush();//alex
            //p.Response.OutputStream.Close();//alex
            //p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.Flush(); //alex
            //p.Response.Redirect("ScreeningFrontEntries.aspx");
            try
            {
                //SyntheticScreeningModel data = new SyntheticScreeningModel();
                //data.ScreeningID = 123;
                //data.SKUName = txtSku.Text;
                //data.CustomerName = txtCustomerName.Text;
                //Session["ScreeningData"] = data;
                // Context.Items.Add("abc", "xyz");
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("ScreeningFrontEntries.aspx", true);
                //Response.Redirect(Request.RawUrl);
                //Context.Items.Add(txtGSIOrder, "abc");
                //Server.Transfer("ScreeningFrontEntries.aspx", false);
                //string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
                //Response.Redirect(navigate);
                p.Response.End();
            }
            catch (Exception ex)
            {
                // string abc = ex.Message;
                // txtGSIOrder.Text = "abc";

            }
        }

        /*alex
           protected void OnLabelOldPrintClick(object sender, EventArgs e)
           {
               LabelOldPopupExtender.Show();
           }

           protected void OnLabelOld2PrintClick(object sender, EventArgs e)
           {
               LabelOld2PopupExtender.Show();
           }
           private void LoadLabels2Grid(List<PrintLabelModel> labelModels)
           {
               var labels2 = PrintUtils.Convert2ColumnsFormat(labelModels);
               LabelOld2Grid.DataSource = labels2;
               LabelOld2Grid.DataBind();
               LabelOld2PopupExtender.Show();
           }
   
        private void LoadLabelsGrid(List<PrintLabelModel> labelModels)
            {
                LabelOldGrid.DataSource = labelModels;
                LabelOldGrid.DataBind();
                LabelOldPopupExtender.Show();
            }
            alex*/
    }
    #endregion

} 