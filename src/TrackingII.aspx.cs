using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for TrackingII.
	/// </summary>
	public partial class TrackingII : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				PrepareForm();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgUnitemizedOrders.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgUnitemizedOrders_SortCommand);
			this.dgBatchStatus.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgBatchStatus_SortCommand);

		}
		#endregion
        
        #region Prepare Form
        private void PrepareForm()
		{
			LoadCustomerList();
			LoadLstAction();
			LoadLstForm();
		}

		private void LoadCustomerList()
		{
			lstCustomerList.Items.Clear();
		    var customers = QueryUtils.GetCustomers(this, true);
            
			lstCustomerList.DataSource=customers;
			lstCustomerList.DataTextField="CustomerName";
			lstCustomerList.DataValueField="CustomerId";
			lstCustomerList.DataBind();
		}

		private void LoadLstAction()
		{
			lstAction.Items.Clear();
			lstAction.DataSource = QueryUtils.GetEventsList(this);
			lstAction.DataTextField="EventName";
			lstAction.DataValueField="EventId";
			lstAction.DataBind();
		}
		
		private void LoadLstForm()
		{
			lstForm.Items.Clear();
			lstForm.DataSource = QueryUtils.GetFormsList(this);
			lstForm.DataTextField = "ViewAccessName";
			lstForm.DataValueField= "ViewAccessId";
			lstForm.DataBind();

		}
        #endregion
        
        
        protected void cmdLookup_Click(object sender, EventArgs e)
		{
			lblInfo.Text = "cmdLookup_Click";
			//return;
            int intUnItemizedOrderAlertHours = 5;// int.Parse(Session["UnItemizedOrderAlertHours"].ToString());

			DateTime dateFrom = new DateTime();
			DateTime dateTo = new DateTime();

			dateFrom = calFrom.SelectedDate;
			dateTo = calTo.SelectedDate;

			//lblInfo.Text = dateFrom.ToString();
		

			SqlCommand command = new SqlCommand("trkSpGetAgedBatches");
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandTimeout = 0;

			if(DateTime.Compare(dateFrom,dateTo)<0)
			{
			}
			else
			{
				dateFrom = DateTime.Parse("2010-06-01");
				dateTo = DateTime.Now.AddDays(1);
			}			

			command.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
			command.Parameters.Add(new SqlParameter("@DateTo", dateTo));
			
			if(!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
			{
				command.Parameters.Add(new SqlParameter("@CustomerID", lstCustomerList.SelectedValue));
			}
			else
			{
				command.Parameters.Add(new SqlParameter("@CustomerID", DBNull.Value));
			}

			lblInfo.Text = "Actual selected tade range: " + dateFrom.ToShortDateString()+" - "+dateTo.ToShortDateString();

			
			DataTable dtList = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);

			da.Fill(dtList);

			DataTable dtShow = new DataTable();

			DateTime dsss = new DateTime();

			dtShow.Columns.Add(new DataColumn("Customer Name"));
			dtShow.Columns.Add(new DataColumn("Order Number"));
			dtShow.Columns.Add(new DataColumn("Order Came at"));
			dtShow.Columns["Order Came at"].DataType= System.Type.GetTypeFromHandle(Type.GetTypeHandle(new DateTime()));
			dtShow.AcceptChanges();

			foreach(DataRow dr in dtList.Rows)
			{
				try
				{
					if(int.Parse(dr["ageInHrs"].ToString())>3)
					{
						DataRow drShow = dtShow.NewRow();
						drShow["Customer Name"]	=	dr["CompanyName"].ToString();
						drShow["Order Number"]	=	dr["GroupCode"].ToString();
						drShow["Order Came at"]	=	dr["CreateDate"].ToString();
						dtShow.Rows.Add(drShow);
					}
				}
				catch
				{}						
			}
			dtShow.AcceptChanges();
			dgUnitemizedOrders.DataSource=dtShow;
			dgUnitemizedOrders.DataBind();
			Session["dtShow"]=dtShow;

			DoTheBatchDetail(dateFrom, dateTo);
		}

		private void DoTheBatchDetail(DateTime dateFrom, DateTime dateTo)
		{
			SqlCommand command = new SqlCommand("trkSpGetAgedBatchHistoryWithFilters");
			command.Connection= new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
			command.Parameters.Add(new SqlParameter("@DateTo", dateTo));

			if(lstCustomerList.SelectedValue!="0_0")
			{
				command.Parameters.Add(new SqlParameter("@CustomerID", lstCustomerList.SelectedValue));
			}
			else
			{
				command.Parameters.Add(new SqlParameter("@CustomerID", DBNull.Value));
			}

			if(lstAction.SelectedValue!="0")
			{
				command.Parameters.Add(new SqlParameter("@EventID", lstAction.SelectedValue));
			}
			else
			{
				command.Parameters.Add(new SqlParameter("@EventID", DBNull.Value));
			}

			if(lstForm.SelectedValue!="0")
			{
				command.Parameters.Add(new SqlParameter("@ViewAccessID", lstForm.SelectedValue));
			}
			else
			{
				command.Parameters.Add(new SqlParameter("@ViewAccessID", DBNull.Value));
			}

			
			if(chkHideCheckedOut.Checked)
			{
				command.Parameters.Add(new SqlParameter("@ShowCheckedOut", DBNull.Value));
			}
			else
			{
				command.Parameters.Add(new SqlParameter("@ShowCheckedOut", 1));
			}
			
		SqlDataAdapter da = new SqlDataAdapter(command);

			DataTable dt = new DataTable();
			command.CommandTimeout = 0;
			da.Fill(dt);

			dt = ReTableBatchDetails(dt);
			dt = GetLastStamps(dt);

////			dgDebug.DataSource=dt;
////			dgDebug.DataBind();

			dgBatchStatus.DataSource=dt;
			dgBatchStatus.DataBind();			

			//debug
			//return;

			DataTable dtDisplay = new DataTable();

			dtDisplay.Columns.Add(new DataColumn("Customer Name"));
			dtDisplay.Columns.Add(new DataColumn("Time Stamp"));
			dtDisplay.Columns.Add(new DataColumn("Action"));
			dtDisplay.Columns.Add(new DataColumn("Batch"));
			dtDisplay.Columns.Add(new DataColumn("Form"));

			dtDisplay.Columns["Time Stamp"].DataType = System.Type.GetTypeFromHandle(Type.GetTypeHandle(new DateTime()));
			
			dtDisplay.AcceptChanges();

			foreach(DataRow dr in dt.Rows)
			{
				DataRow drDisplay =  dtDisplay.NewRow();
				string strAgeInHrs = dr["ageInHrs"].ToString();
				if((int.Parse(strAgeInHrs)>4)||(!chkHideCurrent.Checked))
				{
					drDisplay["Time Stamp"]=DateTime.Parse(dr["RecordTimeStamp"].ToString());
					drDisplay["Customer Name"]=dr["CompanyName"].ToString();
					drDisplay["Action"]=dr["EventName"].ToString();
					drDisplay["Batch"] = "<a href='TrackingIIBatchDetail.aspx?BatchNumber="+dr["FullBatchNumber"].ToString()+"' >" + dr["FullBatchNumber"].ToString()+"</a>";
					drDisplay["Form"] = dr["VewAccessName"].ToString();
                    dtDisplay.Rows.Add(drDisplay);
				}
			}
            dtDisplay.AcceptChanges();
			dgBatchStatus.DataSource=dtDisplay;
			Session["dgBatchStatus"] = dtDisplay;
			dgBatchStatus.DataBind();
		}

		private void dgUnitemizedOrders_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			DataTable dtShow = new DataTable();
			dtShow = Session["dtShow"] as DataTable;

			DataView dw = new DataView(dtShow);
			dw.Sort=e.SortExpression;

			dgUnitemizedOrders.DataSource=dw;
			dgUnitemizedOrders.DataBind();
		}

		private DataTable GetLastStamps(DataTable dt)
		{
			DataTable dtRet = new DataTable();

			SortedList sl = new SortedList();
			
			foreach(DataRow dr in dt.Rows)
			{
				DateTime timeStampNew = DateTime.Parse(dr["RecordTimeStamp"].ToString());
				string strBatchNew = dr["FullBatchNumber"].ToString();
				if(sl.ContainsKey(strBatchNew))
				{
					DataRow drOld = sl[strBatchNew] as DataRow;
					DateTime timeOld = DateTime.Parse(drOld["RecordTimeStamp"].ToString());
					if(timeStampNew>=timeOld)
					{
						sl.Remove(strBatchNew);
						sl.Add(strBatchNew, dr);
					}
				}   
				else
				{
					sl.Add(strBatchNew,dr);
				}
			}
			
			DataTable dtReturn = new DataTable();
			dtReturn = dt.Copy();
			dtReturn.Rows.Clear();
			foreach(DataRow dr in sl.Values)
			{
				dtReturn.Rows.Add(dr.ItemArray);
			}
			dt.AcceptChanges();
			return dtReturn;
		}

		private DataTable ReTableBatchDetails(DataTable dt)
		{

			DataTable dtResult = new DataTable();
						
			dtResult = dt.Copy();
			dtResult.Columns.Add(new DataColumn("FullBatchNumber"));
			dtResult.AcceptChanges();

			foreach(DataRow dr in dtResult.Rows)
			{
				dr["FullBatchNumber"]=Utils.FullBatchNumber(dr["GroupCode"].ToString(), dr["BatchCode"].ToString());
				if(Utils.NullValue(dr["ageInHrs"])=="")
				{
					dr["ageInHrs"]=0;
				}
			}
			dtResult.AcceptChanges();
			return dtResult;
		}

		private void dgBatchStatus_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			DataTable dtDisplay = Session["dgBatchStatus"] as DataTable;
			DataView dwDisplay = new DataView(dtDisplay);
			
			dwDisplay.Sort=e.SortExpression;

			dgBatchStatus.DataSource = dwDisplay;
			dgBatchStatus.DataBind();
		}
	}
}
