using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Security.Cryptography;


namespace Corpt
{
	/// <summary>
	/// Summary description for CorptS.
	/// </summary>
	public class CorptS : System.Web.Services.WebService
	{
		public CorptS()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		// WEB SERVICE EXAMPLE
		// The HelloWorld() example service returns the string Hello World
		// To build, uncomment the following lines then save and build the project
		// To test this web service, press F5

		[WebMethod(EnableSession=true)]
		public void LogOff()
		{
			Session.Clear();
			Session.Abandon();
		}

		[WebMethod(EnableSession=true)]
		public long Login(string Machine, String token)
		{
			if((Machine.Length>25)||(token.Length>20))
			{
				Session["Machine"] = null;
				return -1;
			}			
			else
			{
				SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
				SqlCommand command = new SqlCommand("wsGetMachineTokenByName");
				command.Connection = conn;
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.Add("@MachineName", SqlDbType.VarChar);
				command.Parameters["@MachineName"].Size=25;
				command.Parameters["@MachineName"].Value=Machine;
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dt = new DataTable();
				da.Fill(dt);
				if(dt.Rows.Count<1)
				{
					return -1;
				}
				else
				{				
					if(dt.Rows[0][0].ToString()==token)
					{
						Session["Machine"]=Machine;
						return 1;
					}
					else
					{
						Session["Machine"] = null;
						return -1;
					}
				}
			}
		}

		[WebMethod(EnableSession=true)]
		public int UpSARIN(string secret, byte[] s, string PartName, out byte[] b)
		{
			b = null;
			if(Session["Machine"]==null)
				return -3;

			MD5 md5 = new MD5CryptoServiceProvider();

			string sanPartName;
			if(Utils.SanPartName(PartName, out sanPartName))
			{
				string fileName = Session["SARINDir"].ToString() + sanPartName;
				try
				{
					MemoryStream ms = new MemoryStream(s);
					FileStream fs = new FileStream(fileName, FileMode.Create);					
					ms.WriteTo(fs);
					ms.Close();
					fs.Close();
					ms = null;
					fs = null;
					b = md5.ComputeHash(s);
					return 0;
				}
				catch(Exception e)
				{				
					return -2;
				}
			}			
			return -1;
		}	

		[WebMethod()]
		public string GetTheString()
		{
			return DateTime.Now.ToString();
		}

		[WebMethod]
		public string GetReport(string strReportNumber, string strVirtualVaultNumber)
		{
			return strReportNumber + "." + strVirtualVaultNumber;
		}
	}
}
