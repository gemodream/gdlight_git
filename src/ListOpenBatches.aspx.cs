﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Reflection;
using Corpt.Models.Stats;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace Corpt
{
    public partial class ListOpenBatches : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");

            if (!IsPostBack)
            {
                LoadCustomers();
                LoadInvoiceStatusList();
                LoadOfficeList();
            }
        }

        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            ddlCustomerList.DataSource = customers;
            ddlCustomerList.DataBind();
            ddlCustomerList.SelectedIndex = 0;
        }

        private void LoadInvoiceStatusList()
        {
            var invoiceStatusList = GSIAppQueryUtils.GetInvoiceStatusList(this);
            //    SetViewState(customers, SessionConstants.CustomersList);        
            ddlInvoiceStatus.DataSource = invoiceStatusList;
            ddlInvoiceStatus.DataBind();
            ddlInvoiceStatus.Items.Insert(0, "");
            ddlInvoiceStatus.SelectedIndex = 0;
        }

        private void LoadOfficeList()
        {
            var countryOffices = GSIAppQueryUtils.GetCountryOffices(this);
            //    SetViewState(customers, SessionConstants.CustomersList);
            countryOffices.OrderByDescending(m => m.OfficeCode).ToList<OfficeModel>();
            countryOffices.Insert(0, new OfficeModel { OfficeId = "", OfficeCode = "" });
            ddlOfficeList.DataSource = countryOffices;
            ddlOfficeList.DataBind();
            ddlOfficeList.SelectedIndex = 0;
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);

            if (filtered.Count == 1)
            {
                ddlCustomerList.SelectedValue = filtered[0].CustomerId;
            }
        }

        private List<CustomerModel> GetCustomersFromView()
        {
            return GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }

        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
        }

        public void BindGrid()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var batches = QueryUtils.GetOpenBatchList(ddlCustomerList.SelectedValue.ToString(), ddlOfficeList.SelectedValue.ToString(), ddlInvoiceStatus.SelectedValue.ToString(), dateFrom, dateTo, this);

            DataSet dsOpenBatch = new DataSet();
            dsOpenBatch.Tables.Add(ToDataTable<OpenBatchModel>(batches.Item1));
            dsOpenBatch.Tables[0].Columns.RemoveAt(dsOpenBatch.Tables[0].Columns.Count - 1);
            Session["ListOpenBatches"] = dsOpenBatch;

            //var spSum = QueryUtils.GetOpenBatchSum(this);
            //var sum = batches.Item1.Sum(batch => batch.NumberOfItems);
            //var spSum= batches.Sum(batch => batch.ItemsQuantity);
            //InfoLabel.Text = "" + spSum;

            InfoLabel.Text = "" + batches.Item2.Rows[0]["TotalNumberOfItems"].ToString();
            ClientSum.Text = "" + batches.Item2.Rows[0]["NumberOfItems"].ToString();
            gvOpenBatches.Visible = true;
            if (batches.Item1.Count > 0)
            {
                gvOpenBatches.DataSource = batches.Item1;
                gvOpenBatches.DataBind();
            }
        }

        protected void OnSearchClick(object sender, EventArgs e)
        {
            if (rblAllSelect.SelectedValue == "2")
            {
                var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
                var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
                
                BindGrid();
            }
            else
            {
                ddlCustomerList.SelectedIndex = 0;
                ddlOfficeList.SelectedIndex = 0;
                ddlInvoiceStatus.SelectedIndex = 0;
                calFrom.Text = "";
                calTo.Text = "";
                CustomerLike.Text = "";
                BindGrid();
            }
        }

        protected void rblAllSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblAllSelect.SelectedValue == "1")
            {
                ddlCustomerList.SelectedIndex = 0;
                ddlOfficeList.SelectedIndex = 0;
                ddlInvoiceStatus.SelectedIndex = 0;
                calFrom.Text = "";
                calTo.Text = "";
                CustomerLike.Text = "";
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlCustomerList.SelectedIndex = 0;
            ddlOfficeList.SelectedIndex = 0;
            ddlInvoiceStatus.SelectedIndex = 0;
            calFrom.Text = "";
            calTo.Text = "";
            CustomerLike.Text = "";
            gvOpenBatches.DataSource = null;
            gvOpenBatches.DataBind();
            gvOpenBatches.Visible = false;
            InfoLabel.Text = "";
            ClientSum.Text = "";
        }

        protected void btnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = (DataSet)Session["ListOpenBatches"];         
            Export(ds, "ListOpenBatches", false, this);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            // dataTable.Columns.Remove("Comments");
            return dataTable;
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }

            var filename = fname + ".xlsx";
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            if (!skipDownload) ExcelUtils.DownloadExcelFile(filename, p);

            Memory.Flush();
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            if (!forHeader)
            {
                //style.WrapText = true;
            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Center;
            return style;
        }

       
    }
}