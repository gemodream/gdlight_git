﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SyntheticDataEntry.aspx.cs" Inherits="Corpt.SyntheticDataEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GSI Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function print() {
            var iframe = document.getElementById('textfile');
            iframe.contentWindow.print();
        }
        function print12() {
            var w = window.open("test13.pdf");
            w.print();
        }
        function print1() {
            var fileName = "\\mercury\c$\temp\" + document.getElementById(txtMemoNum).value + ".pdf";
            var w = window.open(fileName);
            w.print();
        }
        function print3() {
            var fileName = getElementById("txtMemoNum")
            var w = window.open("test13.pdf");
            w.print();
        }
</script>
    <style>
        .bg {
            background-image: url("Images/background_land.png");
            height: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .fullscreen {
            position: absolute;
            left: 0;
            top: 0;
            width: 99%;
            height: 99%;
        }


        /*Screen of Mobile for 480 Resolution*/
        @media only screen and (max-width:480px) {
            p {
                font-size: 11px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 5px;
                border-radius: 5px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:9px; 
                padding-top: 1px; 
                width:75px
            }

            .header {
                text-align: center; 
                font-size: 11px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 1px; 
                font-size: 14px;
            }

            .btnFont {
                font-size: 8px;
            }

            .lblStyle {
                height: 32px;
                width: 648px; 
            }
        }

        /*Screen of Mobile for 640 Resolution*/
        @media only screen and (min-width : 481px) and (max-width:640px) {
            p {
                font-size: 12px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 30px;
            }

            .text {
                -moz-border-radius: 6px;
                border-radius: 6px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:12px; 
                padding-top: 1px; 
                width:100px
            }

            .header {
                text-align: center; 
                font-size: 12px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 18px;
            }

            .btnFont {
                font-size: 11px;
            }

            .lblStyle {
                height: 13px;
                width: 258px; 
            }
        }

        @media only screen and (min-device-width: 641px) and (max-device-width: 700px) {
            p {
                font-size: 12.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 65px;
            }

            .text {
                -moz-border-radius: 6px;
                border-radius: 6px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:13px; 
                padding-top: 1px; 
                width:109px
            }

            .header {
                text-align: center; 
                font-size: 11px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 20px;
            }

            .btnFont {
                font-size: 12px;
            }

            .lblStyle {
                height: 14px;
                width: 283px; 
            }      
        }

        @media only screen and (min-width : 701px) and (max-width:800px) {
            p {
                font-size: 13.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 7px;
                border-radius: 7px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 1px; 
                font-size:15px; 
                padding-top: 1px; 
                width:125px
            }

            .header {
                text-align: center; 
                font-size: 16px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 2px; 
                font-size: 23px;
            }

            .btnFont {
                font-size: 14px;
            }

            .lblStyle {
                height: 16px;
                width: 323px; 
            }
        }

        /*Screen of Laptop for 1024 Resolution*/
        @media only screen and (min-width : 801px) and (max-width:1024px) {
             p {
                font-size: 15px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 75px;
            }

            .text {
                -moz-border-radius: 10px;
                border-radius: 10px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 2px; 
                font-size:19px; 
                padding-top: 2px; 
                width:150px
            }

            .header {
                text-align: center; 
                font-size: 17px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 3px; 
                font-size: 29px;
            }

            .btnFont {
                font-size: 18px;
            }

            .lblStyle {
                height: 20px;
                width: 413px; 
            }
        }

        /*Screen of Laptop for 1366 Resolution*/
        @media only screen and (min-width: 1025px) and (max-width: 1366px) {
             p {
                font-size: 20.4px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 10px;
            }

            .text {
                -moz-border-radius: 13px;
                border-radius: 13px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 2.5px; 
                font-size:25px; 
                padding-top: 2.5px; 
                width:213px
            }

            .header {
                text-align: center; 
                font-size: 22px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 4px; 
                font-size: 39px;
            }

            .btnFont {
                font-size: 24px;
            }

            .lblStyle {
                height: 27px;
                width: 551px; 
            }

        }

         /*Screen of Desktop for 1605 Resolution*/
        @media only screen and (min-width: 1367px) and (max-width: 1605px) {
            p {
                font-size: 23.5px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 150px;
            }

            .text {
                -moz-border-radius: 15px;
                border-radius: 15px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 3px; 
                font-size:30px; 
                padding-top: 3px; 
                width:250px
            }

            .header {
                text-align: center; 
                font-size: 26px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 5px; 
                font-size: 46px;
            }

            .btnFont {
                font-size: 28px;
            }

            .lblStyle {
                height: 32px;
                width: 648px; 
            }

        }

        /*Screen of 1920 Resolution*/
        @media only screen and (min-width: 1606px) {
            p {
                font-size: 29px;
                font-family: 'Forgotten Futurist';
                font-weight: normal;
                color: #3a6d92;
            }

            .headTop {
                padding-top: 175px;
            }

            .text {
                -moz-border-radius: 18px;
                border-radius: 18px;
                border: solid 1px #3a6d92;
                outline: none;
                color: black;
                text-align: center;
                letter-spacing: 4px; 
                font-size:36px; 
                padding-top: 4px; 
                width:300px
            }

            .header {
                text-align: center; 
                font-size: 31px; 
                color: #0a517d; 
                font-weight: bold;
            }

            .spanStyle {
                padding-top: 6px; 
                font-size: 55px;
            }

            .btnFont {
                font-size: 34px;
            }

            .lblStyle {
                height: 38px;
                width: 777px; 
            }
        }


        .auto-style1 {
            margin-top: 0px;
        }


    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container-fluid">
                <div class="col-md-12 fullscreen bg" >

                    <div class="col-sm-8 col-md-8 col-lg-8 headTop" style="padding:0%" >
                        <%--<p>
                            Simply key in below your Grading Report Number (It's a good idea to record this number in a place                                
                                you can easily access it in the event of report loss.)
                        </p>--%>

                        <p style="text-align: center;  ">
                            <span class="spanStyle" style="background-color: lightblue; color: #0a517d;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Create Request &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label7" text="REQUEST ID" runat="server" class="lblStyle" Font-Bold="True" Visible="false" ></asp:Label>
                            <asp:TextBox ID="txtRequestID" class="text" runat="server" placeholder="REQUEST ID" Visible="false" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label8" text="CUSTOMER CODE" runat="server" class="lblStyle" Font-Bold="True" Visible="false"></asp:Label>
                            <asp:TextBox ID="txtCustomerCode" class="text" runat="server" placeholder="Customer Code" autocomplete="off" Visible="false"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label1" text="MEMO" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="txtMemoNum" class="text" runat="server" placeholder="Enter Memo" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label2" text="PO NUMBER" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="txtPONum" class="text" runat="server" placeholder="Enter PO Number" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label3" text="SKU" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="txtSku" class="text" runat="server" placeholder="Enter SKU" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label5" text="Style" runat="server" class="lblStyle" Font-Bold="True"></asp:Label>
                            <asp:TextBox ID="txtStyle" class="text" runat="server" placeholder="Enter Style" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label4" text="Total Number of Items" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="txtQuantity" class="text" runat="server" placeholder="Enter Quantity" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%;">
                            <asp:Label ID="Label6" Text="Retailer" runat="server" class="lblStyle" Font-Bold="True" ></asp:Label>
                            <asp:TextBox ID="txtRetailer" class="text" runat="server" placeholder="Enter Retailer" autocomplete="off"></asp:TextBox>
                        </p>
                        <p style="text-align: center; padding-top: 1%; ">
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist';" Text="SAVE" />
                            <!--<asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist';" Text="UPDATE ITEM" />-->
                            <asp:Button ID="btnPrint" runat="server" onclientclick="print1()" onClick="PrintOnClick" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist';" Text="REPRINT FORM" />
                            <!--<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn btn-primary btnFont" Style="font:'Forgotten Futurist';" Text="SEARCH" /> -->
                        </p>

                       <%-- <p>
                            If you’re having technical difficulties registering or accessing your GSI report,<br />
                            please contact us at info@gemscience.net or call: 212-207-4140 or (toll free): 800-720-2018
                        </p>--%>
                        <p style="text-align: center;">
                            <asp:Label ID="lblError" runat="server" class="lblStyle" ForeColor="Red" Font-Bold="True" ></asp:Label>
                        </p>
                    </div>

                    <div class="col-sm-2 col-md-2 col-lg-2 "></div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
