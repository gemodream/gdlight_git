﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class LeoReport2 : CommonPage
    {
        #region Load Page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Leo Report II";
            if (IsPostBack) return;
            SetViewState(new List<ShortReportModel>(), SessionConstants.LeoShortReports);
            SetViewState(new List<string>(), SessionConstants.LeoItemNumbersForOrder);
            
            OrderButton.Attributes.Add("onclick", "return confirm('Are you sure you want to order checked items?');");
            OrderField.Focus();
        }
        #endregion

        #region Orders List
        protected void OnDelOrderClick(object sender, ImageClickEventArgs e)
        {
            var selOrder = OrderList.SelectedValue;
            if (string.IsNullOrEmpty(selOrder)) return;
            selOrder = Utils.FillToFiveChars(selOrder);

            var allOrders = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel>;
            if (allOrders == null) return;
            while (allOrders.Find(m => m.BatchModel.GroupCodeFiveChars == selOrder) != null)
            {
                var model = allOrders.Find(m => m.BatchModel.GroupCodeFiveChars == selOrder);
                allOrders.Remove(model);
            }
            var index = OrderList.SelectedIndex;
            OrderList.Items.RemoveAt(index);
            if (index < OrderList.Items.Count) OrderList.SelectedIndex = index;
            else if (OrderList.Items.Count > 0) OrderList.SelectedIndex = 0;
            OnShowReportClick(null, null);
        }
        protected void OnAddOrderClick(object sender, ImageClickEventArgs e)
        {
            if (OrderField.Text.Trim().Length == 0) return; 
            var order = Utils.FillToFiveChars(OrderField.Text.Trim());
            if (ExistsOrder(order)) return;
            var orderItem = new ListItem { Value = order, Text = order };
            if (OrderList.Items.FindByValue(order) != null) return;

            AddOrderButton.Enabled = false;
            string errMsg;
            List<MovedItemModel> orderMovedItems;
            var reports = QueryUtils.GetLeoFullOrder(order, out errMsg, this, out orderMovedItems);
            AddOrderButton.Enabled = true;

            if (reports == null || reports.Count == 0) return;

            OrderList.Items.Add(orderItem);

            var allReports = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel>;
            if (allReports == null) return;
            allReports.AddRange(reports);
            SetViewState(allReports, SessionConstants.LeoShortReports);
            OrderField.Text = "";
            OnShowReportClick(null, null);
        }
        private bool ExistsOrder(string order)
        {
            return OrderList.Items.Cast<ListItem>().Any(item => Convert.ToInt32(item.Text) == Convert.ToInt32(order));
        }

        #endregion
        
        #region DataGrid
        protected void OnShowReportClick(object sender, EventArgs e)
        {
            btnMovedItems.Visible = false;
            ExcelButton.Enabled = false;
            GridButtonsPanel.Visible = false;
            SaveErrLabel.Text = "";

            var srList = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel>;
            if (srList == null || srList.Count == 0)
            {
                LblRowCounts.Text = "";
                grdShortReport.DataSource = null;
                grdShortReport.DataBind();
                return;
            }
            var hideEmpty = HideEmptyCheckBox.Checked && reorientNowFld.Text == PageConstants.DirHorizontal; 
            var dt = new ShortReportUtils(false).GetLeoReport(srList, reorientNowFld.Text, hideEmpty, false);
            SetViewState(dt, SessionConstants.LeoDataTable);
            UpdateDataView(dt);
            LblRowCounts.Text = dt == null ? "" : dt.Rows.Count + " rows";
            ExcelButton.Enabled = (dt != null) ;
            btnMovedItems.Visible = HasMovedItems();
            GridButtonsPanel.Visible = (dt != null && dt.Rows.Count > 0);
        }
        void UpdateDataView(DataTable table)
        {
            if (table == null)
            {
                grdShortReport.DataSource = null;
                grdShortReport.DataBind();
                return;
            }
            var dv = new DataView { Table = table };
            if (grdShortReport.AllowSorting)
            {
                // Apply sort information to the view
                var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
                if (sortModel != null && table.Columns.Contains(sortModel.ColumnName))
                {
                    dv.Sort = sortModel.ColumnName;
                    if (!sortModel.Direction) dv.Sort += " DESC";
                }
                else
                {
                    dv.Sort = PageConstants.ColNameLinkOnItemView;
                    SetViewState(new SortingModel { ColumnName = PageConstants.ColNameLinkOnItemView, Direction = true }, SessionConstants.CurrentSortModel);
                }
            }

            // Rebind data 
            grdShortReport.DataSource = dv;
            grdShortReport.DataBind();
        }
        protected void OnCheckAllClick(object sender, EventArgs e)
        {
            if (!IsDirHorizontal()) return;
            foreach (DataGridItem item in grdShortReport.Items)
            {
                var checkBox = item.Cells[0].Controls[1] as CheckBox;
                if (checkBox == null) return;
                checkBox.Checked = OrderCheckBox.Checked;
            }
        }
        #endregion

        #region Sort DataGrid Command
        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);

            UpdateDataView(GetViewState(SessionConstants.LeoDataTable) as DataTable);

        }
        #endregion

        #region Excel button
        private DataTable GenerateShortReportTable(bool forEmail)
        {
            var srList = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel>;
            if (srList == null || srList.Count == 0) return null;
            var dataTable = new ShortReportUtils(forEmail).GetLeoReport(srList, PageConstants.DirHorizontal, false, false);
            if (dataTable == null) return null;

            var table = dataTable.Copy();
            if (table.Columns.Contains(PageConstants.ColNameLeoOrdered))
            {
                table.Columns.Remove(PageConstants.ColNameLeoOrdered);
            }
            if (table.Columns.Contains(PageConstants.ColNameLeoPrinted))
            {
                table.Columns.Remove(PageConstants.ColNameLeoPrinted);
            }
            if (table.Columns.Contains("Order"))
            {
                table.Columns.Remove("Order");
            }
            var dv = new DataView { Table = table };

            // Apply sort information to the view
            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null && dv.Table.Columns.Contains(sortModel.ColumnName))
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }

            var result = table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < dv.Count; i++)
            {
                result.Rows.Add(dv[i].Row.ItemArray);
            }
            result.AcceptChanges();
            return result;
        }
        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {

            var result = GenerateShortReportTable(false);
            ExcelUtils.ExportThrouPoi(result, "LeoReport", this);
        }
        #endregion

        #region Moved Items
        private bool HasMovedItems()
        {
            //-- Make table
            var movedItemsList = new List<MovedItemModel>();
            var shortReportList = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel>;
            if (shortReportList == null || shortReportList.Count == 0) return false;
            //-- Create Table for display moved items
            var moveTable = new DataTable("MovedItems");
            moveTable.Columns.Add(new DataColumn("Old Item Number"));
            moveTable.Columns.Add(new DataColumn("New Item Number"));
            moveTable.AcceptChanges();
            foreach (var shortReportModel in shortReportList)
            {
                foreach (var movedItemModel in shortReportModel.MovedItems)
                {
                    movedItemsList.Add(movedItemModel);
                    moveTable.Rows.Add(new object[] { movedItemModel.FullItemNumberWithDotes, movedItemModel.NewFullItemNumberWithDotes });
                }
            }
            moveTable.AcceptChanges();
            dgPopup.DataSource = moveTable;
            dgPopup.DataBind();

            return movedItemsList.Count > 0;
        }

        #endregion

        #region Reorient button
        private bool IsDirHorizontal()
        {
            return PageConstants.DirHorizontal == reorientNowFld.Text;
        }
        protected void OnReorientClick(object sender, ImageClickEventArgs e)
        {
            var nowDir = reorientNowFld.Text;
            var newDir = (nowDir == PageConstants.DirHorizontal ? PageConstants.DirVertical : PageConstants.DirHorizontal);
            reorientNowFld.Text = newDir;
            //-- Reset sort data
            SetViewState(null, SessionConstants.CurrentSortModel);
            //-- remove sorting data
            grdShortReport.AllowSorting = (newDir == PageConstants.DirHorizontal);
            OnShowReportClick(null, null);
            
            //-- Hide Order Column
            grdShortReport.Columns[0].Visible = IsDirHorizontal();
        }
        #endregion

        #region Clear button
        protected void OnClearClick(object sender, EventArgs e)
        {
            OrderField.Text = "";
            OrderList.Items.Clear();
            SetViewState(new List<ShortReportModel>(), SessionConstants.LeoShortReports);
            SetViewState(new List<string>(), SessionConstants.LeoItemNumbersForOrder);
            //OrderButton.Enabled = false;
            OnShowReportClick(null, null);
        }
        #endregion

        #region Hide Empty Columns Button
        protected void OnHideEmptyClick(object sender, EventArgs e)
        {
            OnShowReportClick(null, null);
        }
        #endregion

        #region ItemCheckBox
        protected void ItemCheckBoxClicked(object sender, EventArgs e)
        {
            SaveErrLabel.Text = "";
            var checkBox = sender as CheckBox;
            if (checkBox == null) return;
            if (!(checkBox.Parent is TableCell)) return;
            var dgItem = (checkBox.Parent as TableCell).Parent as DataGridItem;
            if (dgItem == null) return;

            var itemNumber = grdShortReport.Items[dgItem.DataSetIndex].Cells[1].Text;
            itemNumber = Regex.Replace(itemNumber, @"<[^>]+>", "");
            var items = GetViewState(SessionConstants.LeoItemNumbersForOrder) as List<string> ?? new List<string>();
            if (checkBox.Checked && !items.Contains(itemNumber))
            {
                items.Add(itemNumber);
            }
            if (!checkBox.Checked && items.Contains(itemNumber))
            {
                items.Remove(itemNumber);
            }
            SetViewState(items, SessionConstants.LeoItemNumbersForOrder);
        }
        #endregion

        #region Order Reports button
        protected void OnOrderReportsClick(object sender, EventArgs e)
        {
            if (!IsDirHorizontal())
            {
                SaveErrLabel.Text = "Nothing to order!";
                return;
            }
            SaveErrLabel.Text = "";
            var items = new List<string>();
            foreach (DataGridItem item in grdShortReport.Items)
            {
                var checkBox = item.Cells[0].Controls[1] as CheckBox;
                if (checkBox == null) continue;
                if (checkBox.Checked)
                {
                    var itemNumber = item.Cells[1].Text;
                    itemNumber = Regex.Replace(itemNumber, @"<[^>]+>", "");
                    items.Add(itemNumber);
                }
            }
            if (items.Count == 0)
            {
                //Nothing to order
                SaveErrLabel.Text = "Nothing to order!";
                return;
            }
            var msg = QueryUtils.SetLeoOrderReports(items, this);
            SaveErrLabel.Text = string.IsNullOrEmpty(msg) ? "Reports ordered." : msg;
        }
        #endregion

        #region Send Email with attached Short report
        protected void OnSendEmailClick(object sender, ImageClickEventArgs e)
        {
            SendEmailMessage.Text = "";
            var toAddress = AddressTo.Text.Trim();
            if (string.IsNullOrEmpty(toAddress))
            {
                SendEmailMessage.Text = "Please enter a 'Address to'!";
                return;
            }
            var result = GenerateShortReportTable(true);
            if (result == null || result.Rows.Count == 0)
            {
                SendEmailMessage.Text = "A Short report is empty!";
                return;
            }
            var dss = new DataSet();
            dss.Tables.Add(result);
            
            //-- Generate excel file
            var fname = "LeoReport_" + DateTime.Now.Ticks;
            ExcelUtils.ExportThrouPoi(dss, fname, true, this);

            var msg = ExcelUtils.SendMailWithAttachExcel(toAddress, fname, this);
            SendEmailMessage.Text = msg;

        }
        #endregion
    }
}