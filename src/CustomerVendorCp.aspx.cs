﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class CustomerVendorCp : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            LoadData();
        }
        private void LoadData()
        {
            var customers = QueryUtils.GetAllCustomers(this);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();

            var cps = QueryUtils.GetCustomerVendorCps(customers, this);
            SetViewState(cps, SessionConstants.CustomerVendorCp);
            lstVendorList.DataSource = customers;
            lstVendorList.DataBind();

            var programs = new List<CustomerProgramModel>();
            foreach(var cp in cps)
            {
                if (programs.Find(m=> m.CustomerProgramName == cp.ProgramName) != null) continue;
                programs.Add(new CustomerProgramModel{CustomerProgramName = cp.ProgramName});
            }
            programs.Sort((m1, m2) => String.Compare(m1.CustomerProgramName, m2.CustomerProgramName, StringComparison.Ordinal));
            programs.Insert(0, new CustomerProgramModel{CustomerProgramName = "(All)"});
            SetViewState(programs, SessionConstants.CpList);
            
            CpList.DataSource = programs;
            CpList.DataBind();

            ApplyFilter();
        }
        private void ApplyFilter()
        {
            grdCps.DataSource = new List<CustomerVendorCpModel>();
            grdCps.DataBind();
            RowsCount.Text = "";
            
            var data = GetViewState(SessionConstants.CustomerVendorCp) as List<CustomerVendorCpModel>;
            if (data == null) return;
            
            if (lstCustomerList.SelectedValue != "")
            {
                data = data.FindAll(m => m.CustomerId == lstCustomerList.SelectedValue);
            }
            if (data.Count == 0) return;
            if (lstVendorList.SelectedValue != "")
            {
                data = data.FindAll(m => m.VendorId == lstVendorList.SelectedValue);
            }
            if (data.Count == 0) return;
            if (CpList.SelectedValue != "(All)")
            {
                data = data.FindAll(m => m.ProgramName == CpList.SelectedValue);
            }
            
            grdCps.DataSource = data;
            grdCps.DataBind();
            RowsCount.Text = data.Count + " rows";

        }
        #region Customers
        protected void OnCustomerSearchClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (!string.IsNullOrEmpty(lstCustomerList.SelectedValue))
            {
                ApplyFilter();
            }
        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        #endregion

        #region Vendors
        protected void OnVendorSearchClick(object sender, ImageClickEventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = VendorLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstVendorList.DataSource = filtered;
            lstVendorList.DataBind();
            if (!string.IsNullOrEmpty(lstVendorList.SelectedValue))
            {
                ApplyFilter();
            }
        }

        protected void OnVendorSelectedChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }
        #endregion

        #region Programs
        protected void OnCpChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }
        protected void OnCpSearchClick(object sender, ImageClickEventArgs e)
        {
            var cps = GetViewState(SessionConstants.CpList) as List<CustomerProgramModel>;
            if (cps == null) return;
            var filterText = CpLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? cps :
                cps.FindAll(m => m.CustomerProgramName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            CpList.DataSource = filtered;
            CpList.DataBind();
            if (!string.IsNullOrEmpty(CpList.SelectedValue))
            {
                ApplyFilter();
            }

        }
        #endregion

        protected void OnFilterClick(object sender, ImageClickEventArgs e)
        {
            ApplyFilter();
        }
    }
}