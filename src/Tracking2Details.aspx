﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="Tracking2Details.aspx.cs" Inherits="Corpt.Tracking2Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
<%--    <script type="text/javascript">--%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScroll();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScroll();--%>
<%--        });--%>
<%--        function gridviewScroll() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=grdDetails.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 1--%>
<%--            });--%>
<%--        }--%>
<%--        --%>
<%--    </script>--%>
    <div class="demoheading">
        Tracking II Details</div>
    <asp:Label runat="server" ID="batchNumberFld" CssClass="label"></asp:Label>
    <br/>
    <asp:Label runat="server" ID="infoLabel" CssClass="label-warning"></asp:Label>
    <asp:Label ID="lblHistoryRows" runat="server" Style="font-family: Arial; font-size: small;
        font-weight: bold"></asp:Label>
    <div id="gridContainer" style="padding-top: 10px;font-size: small">
        <asp:DataGrid ID="grdDetails" runat="server" AutoGenerateColumns="True" AllowSorting="True" OnSortCommand="OnSortCommand" CellPadding="5">
            <HeaderStyle CssClass="GridviewScrollHeader" />
            <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
            <PagerStyle CssClass="GridviewScrollPager" />
        </asp:DataGrid>
    </div>
    <div class="row" align="right">
        <asp:HyperLink ID="HyperLinkBack" runat="server" CssClass="btn-link" NavigateUrl="Middle.aspx">Back</asp:HyperLink>
    </div>
</asp:Content>
