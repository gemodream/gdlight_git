﻿using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Corpt
{
    public partial class Retailer : System.Web.UI.Page
    {
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
			if (!IsPostBack)
			{
				LoadCustomers("");
				LoadRetailers();
				BindDataGrid();
			}
			if (IsPostBack)
                return;
		}
		#endregion

		#region Load Data

		
		private void LoadCustomers(string filter)
        {
           
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
			if(filter!="")
			{
				customers=customers.Where(x => x.CustomerName.Contains(filter)).ToList();
			}
			ddlCutomer.Items.Clear();
			ddlCutomer.DataSource = customers;
			ddlCutomer.DataBind();
            
        }
		private void LoadRetailers()
		{
			var retailers = SyntheticScreeningUtils.GetRetailers(this);
			ddlRetailer.DataSource = retailers;
			ddlRetailer.DataBind();
			ListItem liRetailer = new ListItem();
			liRetailer.Text = "";
			liRetailer.Value = "0";
			ddlRetailer.Items.Insert(0,liRetailer);
		}
		private void BindDataGrid()
		{
			EnteredGrid.Visible = true;
			EnteredLabel.Visible = true;
			SyntheticRetailerModel retailer = new SyntheticRetailerModel();
			retailer.customerCode = ddlCutomer.SelectedItem.Value;
			var retailers = SyntheticScreeningUtils.GetSyntheticCustomerRetailer(retailer, this);
			EnteredGrid.DataSource = retailers;
			EnteredGrid.DataBind();
			if (retailers.Count > 0)
			{
				lblNoRecord.Visible = false;
			}
			else
			{
				lblNoRecord.Visible = true;
			}
		}
		#endregion

		#region Events	
		protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
			var customer = ddlCutomer.Items.FindByValue(ddlCutomer.SelectedValue);

			if (customer != null)
			{
				txtCustomerCode1.Text = ddlCutomer.SelectedItem.Value;
				BindDataGrid();
			}
		}

		protected void OnCustCodeClick(object sender, EventArgs e)
        {
			LoadCustomers(txtCustomerCode1.Text);
			for (int i = 0; i <= ddlCutomer.Items.Count - 1; i++)
			{
				if (ddlCutomer.Items[i].Text.Contains(txtCustomerCode1.Text.Trim()))
				{
					ddlCutomer.SelectedIndex = i;
					BindDataGrid();
					break;
				}
			}
			if (ddlCutomer.Items.Count==0)
			{
				ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('Customer Code not found');", true);
				return;
			}
		}
		#endregion

		#region Save/Delete Button

	
		protected void AddRetailerToCustomer_Click(object sender, EventArgs e)
		{
			try
			{
				SyntheticRetailerModel retailerGet = new SyntheticRetailerModel();
				retailerGet.customerCode = ddlCutomer.SelectedItem.Value;
				var retailers = SyntheticScreeningUtils.GetSyntheticCustomerRetailer(retailerGet, this);

				if (retailers.Count > 0)
				{
					foreach (var retailer in retailers)
						if (retailer.retailerId.ToString().Trim().ToLower() == ddlRetailer.SelectedItem.Value.Trim().ToLower())
						{
							ScriptManager.RegisterStartupScript(EnteredValuesPanel, EnteredValuesPanel.GetType(), "alert", "alert('retailer already exists for customer.');", true);
							return;
						}
				}
				SyntheticRetailerModel retailerSet = new SyntheticRetailerModel();
				retailerSet.retailerId = ddlRetailer.SelectedItem.Value;
				retailerSet.customerCode = ddlCutomer.SelectedItem.Value;
				retailerSet.customerName = ddlCutomer.SelectedItem.Text;
				bool added = SyntheticScreeningUtils.SetSyntheticCustomerRetailer(retailerSet, this);
				BindDataGrid();
				ddlRetailer.SelectedIndex = 0;
				lblMsg.Text = "Retailer added to Customer successfully";
			}
			catch (Exception ex)
			{
				lblMsg.Text =ex.Message.ToString();
				lblMsg.ForeColor = System.Drawing.Color.Red;
			}
		}
		protected void OnDelCommand(object source, DataGridCommandEventArgs e)
        {
            int itemNumber = e.Item.ItemIndex;
			List<SyntheticRetailerModel> retailers = new List<SyntheticRetailerModel>();
			SyntheticRetailerModel retailer = new SyntheticRetailerModel();
			retailer.customerCode = ddlCutomer.SelectedItem.Value;
			retailer.retailerId = EnteredGrid.Items[itemNumber].Cells[1].Text;
			bool removed = SyntheticScreeningUtils.DelSyntheticCustomerRetailer(retailer, this);
			BindDataGrid();
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			//lblRetailerMsg.Text = "";
			lblMsg.Text = "";
			txtCustomerCode1.Text = "";
			//txtRetailerName.Value = "";
			LoadCustomers("");
			ddlCutomer.SelectedIndex = -1;
			BindDataGrid();
		}
		#endregion

		#region Information Dialog
		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerHtml = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}
		#endregion
	}
}