﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class Grade1 : CommonPage
    {
        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) {
                Page.Title = "GSI: " + TitleLabel.Text;
				return;
			}
            var mode = Request.Params["Mode"] ?? "";
            switch (mode)
            {
                case "Color":
                    rblModeSelector.SelectedValue = "" + DbConstants.ViewAccessColorCode;
                    TitleLabel.Text = "Color";
                    Page.Title = "GSI: Color";
                    break;
                case "Clarity":
                    rblModeSelector.SelectedValue = "" + DbConstants.ViewAccessClarityCode;
                    TitleLabel.Text = "Clarity";
                    Page.Title = "GSI: Clarity";
                    break;
                case "Measure":
                    rblModeSelector.SelectedValue = "" + DbConstants.ViewAccessMeasureCode;
                    TitleLabel.Text = "Measure";
                    Page.Title = "GSI: Measure";
                    break;
                default:
                    rblModeSelector.SelectedValue = "" + DbConstants.ViewAccessColorCode;
                    Page.Title = "GSI: Color";
                    break;
            }
            rblModeSelector.Enabled = false;
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");

            ViewKeyLink.NavigateUrl = "HotKeysView.aspx";
            ViewKeyLink.Target = "_blank";
            KeysPanel.Visible = false;

            var batchNumber = Request.Params["BatchNumber"] ?? "";
//            if (!string.IsNullOrEmpty(batchNumber))
//            {
//                BatchItemField.Text = batchNumber;
//                OnLoadClick(null, null);
//            }
            var itemNumber = Request.Params["ItemNumber"] ?? "";
            if (!string.IsNullOrEmpty(itemNumber))
            {
                BatchItemField.Text = itemNumber;
                OnLoadClick(null, null);
//                lstItemList.SelectedValue = itemNumber;
//                OnItemListSelectedChanged(null, null);
            }
            var partId = Request.Params["PartId"] ?? "";
            if (!string.IsNullOrEmpty(partId))
            {
                lstParts.SelectedValue = partId;
                OnPartListSelectedChanged(null,null);
            }
            var measureId = Request.Params["MeasureId"] ?? "";
            if (!string.IsNullOrEmpty(measureId))
            {
                lstMeasures.SelectedValue = measureId;
                OnMeasureSelected(null, null);
            }
            if (string.IsNullOrEmpty(batchNumber + itemNumber + partId + measureId))
            {
                //-- not parameters
                BatchItemField.Focus();
            }
        }
        #endregion

        #region Load Button
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("BatchGroup");
            if (!Page.IsValid) return;
            InvalidLabel.Text = "";
            var batchItem = BatchItemField.Text.Trim();
			var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(batchItem, this.Page);
			if (myResult.Trim() != "")
			{
				BatchItemField.Text = myResult;
				batchItem = myResult;
			}
            var batch = (batchItem.Length < 10 ? batchItem : batchItem.Substring(0, batchItem.Length - 2));
            var itemList = QueryUtils.GetItemsCp(batch, this);
            if (itemList.Count == 0)
            {
                InvalidLabel.Text = "#" + batchItem + " not found.";
                return;
            }
            if (batchItem.Length > 9)
            {
                //-- Item only
                itemList = itemList.FindAll(m => m.FullItemNumber == batchItem);
                if (itemList.Count == 0)
                {
                    InvalidLabel.Text = "#" + batchItem + " not found.";
                    return;
                }
            }
            lstItemList.Rows = (itemList.Count < 4 ? 4 :  itemList.Count) + 1;
            SetViewState(itemList, SessionConstants.GradeItemNumbers);
           
            lstItemList.DataSource = itemList;
            lstItemList.DataBind();

            BatchItemField.Enabled = false;
            LoadButton.Enabled = false;
            IgnoreCpBox.Enabled = false;
            if (Items.Count <= 0) return;
            
            lstItemList.SelectedIndex = 0;
            OnItemListSelectedChanged(null, null);
            KeysField.Focus();
        }
        #endregion

        #region Item Numbers list
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
        {
            if (lstItemList.SelectedIndex == -1)
            {
                ClearingParts();
                HidePicturePanel();
                HideShapePanel();
                return;
            }
            var selItem = GetItemFromFullList(lstItemList.SelectedValue);
            if (selItem == null) return;
            ShowItemState(selItem);
            if (selItem.IsInvalid)
            {
                ClearingParts();
                HidePicturePanel();
                HideShapePanel();
                return;
            }
            ShowPicture(selItem.Path2Picture);

            //-- Get Measures
            var measures = GetMeasureListByMode(selItem, "");

            //-- Get MeasureValues
            var values = GetMeasureValuesByItem();
            if (values != null)
            {
                var withShapes = values.FindAll(m => m.ShapePath2Drawing.Length > 0);
                if (withShapes.Count > 0)
                {
                    var path = withShapes[0].ShapePath2Drawing;
                    if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowShape(path);
                    else HideShapePanel();
                } else
                {
                    HideShapePanel();
                }
            }


            //-- Set Measure Parts
            var itemParts = GetMeasureParts(selItem.ItemTypeId);
            
            //-- Remove not used parts
            var unusedParts = itemParts.Where(part => measures.FindAll(m => m.PartId == part.PartId).Count == 0).ToList();

            foreach (var part in unusedParts)
            {
                itemParts.Remove(part);
            }
            lstParts.DataSource = itemParts;
            lstParts.DataBind();

            //-- Select 'Diamond'
            var diamondPart = itemParts.Find(m => m.PartName.ToLower().IndexOf("diamond", StringComparison.Ordinal) != -1);
            if (diamondPart != null)
            {
                lstParts.SelectedValue = "" + diamondPart.PartId;
                
            } else
            {
                ClearingMeasures();
            }
            //-- Create Measure Values Table
            SetMeasureValuesGrid(measures, values);
            OnPartListSelectedChanged(null, null);
            ShowHotKeysPanel(true);
        }
        private SingleItemModel GetItemFromFullList(string itemNumber)
        {
            var list = GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel>;
            return list == null ? null : list.Find(m => m.FullItemNumber == itemNumber);
        }
        #endregion

        #region Measure and Values Grid
        private void SetMeasureValuesGrid(IEnumerable<MeasureValueCpModel> measures, List<MeasureValueModel> values)
        {
            var items = new List<GradeEditModel>();
            foreach (var measure in measures)
            {
                var value = values.Find(m => m.PartId == measure.PartId && m.MeasureId == measure.MeasureId);
                items.Add(new GradeEditModel(measure, value));
            }
            items.Sort(new GradeEditComparer().Compare);
            SetViewState(items, SessionConstants.GradeMeasureValuesMap);
            UpdateMeasureValuesGrid(items);

        }
        private void UpdateMeasureValuesGrid(List<GradeEditModel> items)
        {
            MeasureValuesGrid.DataSource = items;
            MeasureValuesGrid.DataBind();
            var notEntered = items.FindAll(m => m.NotEntered).Count;
            NotEnteredLabel.Text = (notEntered == 0 ? "" : notEntered + " fields not entered.");
            UpdateButtonsState();
        }
        private void UndoRowModel(string uniqueKey)
        {
            var partId = Convert.ToInt32(uniqueKey.Split(';')[0]);
            var measureId = uniqueKey.Split(';')[1];
            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel> ?? new List<GradeEditModel>();
            var item = map.Find(m => m.PartId == partId && m.MeasureId == measureId);
            if (item == null) return;
            item.UndoChanges();
            UpdateMeasureValuesGrid(map);
        }
        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var item = e.Item.DataItem as GradeEditModel;
                if (item == null) return;
                if (item.NotEntered)
                {
                    e.Item.Cells[3].ForeColor = Color.DarkRed;
                    e.Item.Cells[3].Font.Bold = true;
                }
                if (!item.HasSavingMeasure)
                {
                    //e.Item.BackColor = Color.LightGoldenrodYellow;// Color.FromArgb(242, 242, 242);
                }
                if (item.HasChanges)
                {
                    e.Item.Cells[5].BackColor = Color.LightGoldenrodYellow;
                    e.Item.Cells[5].Font.Bold = true;
                }
                
            }
        }
        protected void OnUndoCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + MeasureValuesGrid.DataKeys[e.Item.ItemIndex]; 
            UndoRowModel(uniqueKey);
        }

        #endregion

        #region Measure List
        protected void OnMeasureSelected(object sender, EventArgs e)
        {
            var measure = GetMeasureSelected();
            if (measure == null) return;
            ShowMeasureValuePanel(measure.MeasureClass);
            if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                SetEnumData(Convert.ToInt32(measure.MeasureId));
            }
            //-- set value
            SetMeasureValuePanel();
        }
        private MeasureValueCpModel GetMeasureSelected()
        {
            var measureId = lstMeasures.SelectedValue;
            var partId = lstParts.SelectedValue;
            if (string.IsNullOrEmpty(measureId)) return null;
            var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel>;
            if (measures == null) return null;
            return measures.Find(m => m.MeasureId == measureId && m.PartId == Convert.ToInt32(partId));
        }

        private List<MeasureValueModel> GetMeasureValuesByItem()
        {
            var itemModel = GetItemFromFullList(lstItemList.SelectedValue);
            var values = GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ??
                         new List<MeasureValueModel>();
            if (values.FindAll(m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode).Count == 0)
            {
                values.AddRange(QueryUtils.GetMeasureValues(itemModel, rblModeSelector.SelectedValue, this));
                SetViewState(values, SessionConstants.GradeMeasureValues);
            }
            return
                values.FindAll(
                    m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode);
        }
        private List<MeasureValueCpModel> GetMeasureListByMode(SingleItemModel itemModel, String partId)
        {
            var measures = GetViewState(SessionConstants.GradeMeasureList) as List<MeasureValueCpModel> ??
                         new List<MeasureValueCpModel>();
            if (measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId).Count == 0)
            {
                var itemMeasures = QueryUtils.GetMeasureListByAcces(
                    itemModel, rblModeSelector.SelectedValue, IgnoreCpBox.Checked, this);
                measures.AddRange(itemMeasures);
                var groups = measures.GroupBy(m => m.PartId).ToList();
                var maxCount = 0;
                foreach (var group in groups)
                {
                    maxCount = maxCount < group.Count() ? group.Count() : maxCount;
                }
                lstMeasures.Rows = maxCount + 1;
                SetViewState(measures, SessionConstants.GradeMeasureList);
            }
            if (string.IsNullOrEmpty(partId))
            {
                //-- for all parts
                return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId);
            }
            return measures.FindAll(m => m.ForItem.BatchId == itemModel.BatchId && m.PartId == Convert.ToInt32(partId));
        }
        #endregion

        #region Measure Value Panel
        private MeasureValueModel GetCurrMeasureValue()
        {
            var measure = GetMeasureSelected();
            var values = GetMeasureValuesByItem();
            return values.Find(m => m.MeasureId == measure.MeasureId && m.PartId == measure.PartId);
        }
        private void SetMeasureValuePanel()
        {
            var measure = GetMeasureSelected();
            //var values = GetMeasureValuesByItem();
            var value = GetCurrMeasureValue();
            if (value == null)
            {
                ValueLabel.Text = "<b>" + lstMeasures.SelectedItem.Text + "</b>";
            }
            if (measure.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                if (value != null) MeasureNumericField.Text = value.MeasureValue;
                MeasureNumericField.Focus();
            }
            if (measure.MeasureClass == MeasureModel.MeasureClassText)
            {
                if (value != null) MeasureTextField.Text = value.StringValue;
                MeasureTextField.Focus();
            }
            if (measure.MeasureClass != MeasureModel.MeasureClassEnum) return;
            if (value != null)
            {
                var item = MeasureEnumField.Items.FindByValue(value.MeasureValueId);
                if (item != null)
                {
                    item.Selected = true;
                }
            }
            MeasureEnumField.Focus();
        }
        private void ShowMeasureValuePanel(int measureClass)
        {
            SaveInfoLabel.Text = "";
            ValueLabel.Visible = measureClass != 0;
            if (ValueLabel.Visible)
            {
                ValueLabel.Text = "<b>" + lstMeasures.SelectedItem.Text + "</b>";
            }
            if (measureClass == 0)
            {
                //-- Hide All
                MeasureEnumField.Visible = false;
                MeasureNumericField.Visible = false;
                MeasureTextField.Visible = false;
                return;
            }

            MeasureEnumField.Visible = measureClass == MeasureModel.MeasureClassEnum;
            
            MeasureNumericField.Visible = measureClass == MeasureModel.MeasureClassNumeric;
            MeasureNumericField.Text = "";
            
            MeasureTextField.Visible = measureClass == MeasureModel.MeasureClassText;
            MeasureTextField.Text = "";

        }

        #endregion

        #region Enum Measure Control
        private void SetEnumData(int measureId)
        {
            var value = GetCurrMeasureValue();
            var enumMeasures = GetViewState(SessionConstants.BulkEnumMeasures) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
            if (enumMeasures.Count == 0)
            {
                enumMeasures = QueryUtils.GetEnumMeasure(this);
                SetViewState(enumMeasures, SessionConstants.BulkEnumMeasures);
            }
            var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
            if (enums.Count == 0) return;
            enums.Sort((m1, m2) => String.Compare(m1.ValueTitle, m2.ValueTitle, StringComparison.Ordinal));
            if (value == null)
            {
                enums.Insert(0, EnumMeasureModel.GetEmptyModel(measureId));
            }

            MeasureEnumField.DataSource = enums;
            MeasureEnumField.DataBind();
        }
        protected void OnEnumMeasureChoice(object sender, EventArgs e)
        {
            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel>;
            if (map == null) return;
            var measure = GetMeasureSelected();
            if (measure == null) return;
            var item = map.Find(m => m.PartId == measure.PartId && m.MeasureId == measure.MeasureId);
            if (item == null) return;
            item.ValueEnumIdCurr = MeasureEnumField.SelectedValue;
            item.ValueEnumTitleCurr = MeasureEnumField.SelectedItem.Text;
            UpdateMeasureValuesGrid(map);
        }
        #endregion

        #region Numeric Measure Control
        protected void OnMeasureNumbChanged(object sender, EventArgs e)
        {
            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel>;
            if (map == null) return;
            var measure = GetMeasureSelected();
            if (measure == null) return;
            var item = map.Find(m => m.PartId == measure.PartId && m.MeasureId == measure.MeasureId);
            if (item == null) return;
            item.ValueNumbCurr = MeasureNumericField.Text;
            UpdateMeasureValuesGrid(map);
        }
        #endregion

        #region Text Measure Control
        protected void OnMeasureTextChanged(object sender, EventArgs e)
        {
            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel>;
            if (map == null) return;
            var measure = GetMeasureSelected();
            if (measure == null) return;
            var item = map.Find(m => m.PartId == measure.PartId && m.MeasureId == measure.MeasureId);
            if (item == null) return;
            item.ValueTextCurr = MeasureTextField.Text;
            UpdateMeasureValuesGrid(map);
        }
        #endregion
       
        #region Clearing Controls
        protected void ClearingAll()
        {
            BatchItemField.Text = "";
            BatchItemField.Enabled = true;
            LoadButton.Enabled = true;
            IgnoreCpBox.Enabled = true;

            lstItemList.Items.Clear();
            SetViewState(null, SessionConstants.GradeItemNumbers);

            ClearingParts();
            SetViewState(null, SessionConstants.GradeMeasureParts);

            SetViewState(null, SessionConstants.GradeMeasureList);
            SetViewState(null, SessionConstants.GradeMeasureValuesMap);

            SetViewState(null, SessionConstants.GradeEditedItemList);

            HidePicturePanel();
            HideShapePanel();
            ShowHotKeysPanel(false);
        }
        private void ClearingParts()
        {
            lstParts.Items.Clear();
            ClearingMeasures();
        }
        private void ClearingMeasures()
        {
            lstMeasures.Items.Clear();
            ShowMeasureValuePanel(0);   // Hide
            NotEnteredLabel.Text = "";
            MeasureValuesGrid.DataSource = null;
            MeasureValuesGrid.DataBind();
        }
        #endregion

        #region Parts Measure
        protected void OnPartListSelectedChanged(object sender, EventArgs e)
        {
            lstMeasures.Items.Clear();
            if (lstParts.SelectedIndex == -1) return;
            var selItem = GetItemFromFullList(lstItemList.SelectedValue);

            var measures = GetMeasureListByMode(selItem, lstParts.SelectedValue);
            lstMeasures.DataSource = measures;
            lstMeasures.DataBind();
            if (measures.Count == 0) return;
            lstMeasures.SelectedIndex = 0;
            OnMeasureSelected(null, null);

            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel> ?? new List<GradeEditModel>();
            UpdateMeasureValuesGrid(map);
        }
        private List<MeasurePartModel> GetMeasureParts(int itemTypeId)
        {
            var parts = GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ??
                        new List<MeasurePartModel>();
            if (parts.FindAll(m=> m.ItemTypeId == itemTypeId).Count == 0)
            {
                parts.AddRange(QueryUtils.GetMeasureParts(itemTypeId, this));
                SetViewState(parts, SessionConstants.GradeMeasureParts);
            }
            return parts.FindAll(m => m.ItemTypeId == itemTypeId);
        }
        private MeasurePartModel GetPartSelected()
        {
            var partId = lstParts.SelectedValue;
            if (string.IsNullOrEmpty(partId)) return null;
            var parts = GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ??
                        new List<MeasurePartModel>();
            return parts.Find(m => m.PartId == Convert.ToInt32(partId));
        }
        #endregion

        #region Picture
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            PicturePathAbbr.Text = ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture"); 
            ErrPictureField.Text = errMsg;

        }
        private void HidePicturePanel()
        {
            itemPicture.Visible = false;
            PicturePathAbbr.Text = "";
            ErrPictureField.Text = "";
        }
        #endregion

        #region Shape
        private string GetShapePath()
        {
            var values = GetMeasureValuesByItem();
            if (values == null) return "";
            var withShapes = values.FindAll(m => m.ShapePath2Drawing.Length > 0);
            if (withShapes.Count > 0) return  withShapes[0].ShapePath2Drawing;
            return "";
        }
        private void ShowShape(string dbShape)
        {
            string imgPath;
            string errMsg;
            //string pngPath;

			ShapePanel.Visible = true;
            if (dbShape.Substring(0,1) == "/" || dbShape.Substring(0,1) == "\\")
            {
                dbShape = dbShape.Substring(1);
            }
			var pngStream = new MemoryStream();
			var result = Utlities.GetShapeImageUrlAzure(dbShape, out imgPath, out errMsg, this,/* out pngPath, */ out pngStream);
            if (result)
            {
                itemShape.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length); // imgPath;
				itemShape.Visible = true;
            }
            else
            {
                itemShape.Visible = false;
            }
            //-- Path to picture
            ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbShape, "Shape");
            ErrShapeField.Text = errMsg;
            
        }
        private void HideShapePanel()
        {
            ShapePanel.Visible = false;
        }
        #endregion

        #region InvalidLabel
        private void ShowItemState(SingleItemModel itemModel)
        {
            //-- Invalid Item
            InvalidLabel.Text = "";
            if (itemModel.IsInvalid)
            {
                InvalidLabel.Text = string.Format("Item {0} is Invalid. Please check the weight and/or ld/ff/km.",
                                                  itemModel.FullItemNumber);
            }
        }
        #endregion

        #region 'Next Batch' Button
        protected void OnNextBatchClick(object sender, EventArgs e)
        {
            var items = GetViewState(SessionConstants.GradeEditedItemList) as List<SingleItemModel> ??
                        new List<SingleItemModel>();
            if (items.Count > 0)
            {
                SaveTrackingInfo(items.ToArray()[0].NewBatchId, items.Count);
            }
            ClearingAll();
            BatchItemField.Focus();
        }
        private void SaveTrackingInfo(int batchId, int cnt)
        {
            QueryUtils.UpdateTracking(
                    new TrackingUpdateModel
                    {
                        BatchId = batchId,
                        EventId = DbConstants.TrkEventTouched,
                        ViewAccess = DbConstants.ViewAccessGdLightGradeId,
                        ItemsAffected = cnt,
                        ItemsInBatch = 0
                    }, this);
            QueryUtils.UpdateTracking(
                    new TrackingUpdateModel
                    {
                        BatchId = batchId,
                        EventId = DbConstants.TrkEventUpdated,
                        ViewAccess = DbConstants.ViewAccessGdLightGradeId,
                        ItemsAffected = cnt,
                        ItemsInBatch = 0
                    }, this);
        }

        #endregion

        #region 'Next Item' Button
        protected void OnNextItemClick()
        {
            lstItemList.Enabled = true;
            var itemNumber = lstItemList.SelectedValue;
            if (string.IsNullOrEmpty(itemNumber)) return;
            var li = lstItemList.Items.FindByValue(itemNumber);
            li.Text = li.Value + "*";
            var ind = lstItemList.Items.IndexOf(li);
            lstItemList.SelectedIndex = ind;
            OnItemListSelectedChanged(null, null);
        }
        #endregion


        #region Save Button
        protected void OnSaveClick(object sender, EventArgs e)
        {
            var selItem = GetItemFromFullList(lstItemList.SelectedValue);
            var items = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel> ?? new List<GradeEditModel>();
            var msgSave = "";
            foreach (var item in items)
            {
                if (!item.HasChanges) continue;
                var updateModel = new UpdateMeasureModel { Item = selItem };
                var chgMeasure = new MeasureValueModel(item);
                updateModel.MeasureValue = chgMeasure;
                msgSave += (msgSave.Length == 0 ? "" : "<br/>") + QueryUtils.UpdateMeasureValue(updateModel, this);
            }
            SaveInfoLabel.Text = string.IsNullOrEmpty(msgSave) ? "OK" : msgSave;

            
            //-- Reset data
            SetViewState(null, SessionConstants.GradeMeasureValues);
            SetViewState(null, SessionConstants.GradeMeasureValuesMap);
            OnNextItemClick();
        }
        #endregion

        #region Undo Button
        protected void OnUndoClick(object sender, EventArgs e)
        {
            //-- Reload Measure Values
            var items = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel> ?? new List<GradeEditModel>();
            foreach (var item in items)
            {
                item.UndoChanges();
            }
            UpdateMeasureValuesGrid(items);
        }
        #endregion

        #region Compare 'Measured Weight'(4) with 'Sarin Weight'(6) 
//        private bool CheckForSarinDiscrepancy(UpdateMeasureModel updateModel)
//        {
//            //-- Find measureId = 6 Sarin Weight
//            var values = GetMeasureValuesByItem(updateModel.Item);
//            var sarinWeight = values.Find(m => m.MeasureId == "6" && m.PartId == updateModel.MeasureValue.PartId);
//            if (sarinWeight == null) return true;
//            var w1 = updateModel.MeasureValue.MeasureValue;
//            var w2 = sarinWeight.MeasureValue;
//            if (string.IsNullOrEmpty(w1) || string.IsNullOrEmpty(w2)) return true;
//            var f1 = float.Parse(w1);
//            var f2 = float.Parse(w2);
//            return Math.Abs(f1 - f2) <= 0.011;
//        }
        #endregion

        private void UpdateButtonsState()
        {
            var items = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel> ?? new List<GradeEditModel>();
            var hasChanged = items.FindAll(m => m.HasChanges).Count > 0;
            SaveButton.Enabled = hasChanged;
            UndoButton.Enabled = hasChanged;
            lstItemList.Enabled = !hasChanged;
            NextBatchButton.Enabled = !hasChanged;

        }
        
        #region Apply Keys Button
        private List<MeasureHotKeyModel> GetHotKeys()
        {
            var keys = GetViewState(SessionConstants.GradeMeasureHotKeys) as List<MeasureHotKeyModel> ??
            new List<MeasureHotKeyModel>();
            if (keys.Count == 0)
            {
                keys.AddRange(QueryUtils.GetMeasureHotKeys(this));
                SetViewState(keys, SessionConstants.GradeMeasureHotKeys);
            }
            return keys;
        }
        protected void OnApplyKeysClick(object sender, ImageClickEventArgs e)
        {
            //-- Split keys
            var keys = KeysField.Text.Trim();
            if (string.IsNullOrEmpty(keys)) return;
            var elems = keys.Split(' ');
            var allKey = GetHotKeys().FindAll(m => m.Access.AccessCode == Convert.ToInt32(rblModeSelector.SelectedValue));
            var map = GetViewState(SessionConstants.GradeMeasureValuesMap) as List<GradeEditModel>;
            if (map == null) return;

            foreach (var elem in elems)
            {
                var setting = allKey.Find(m => m.HotKey.ToLower() == elem.ToLower());
                if (setting == null) continue;
                var measure = setting.Measure;
                var item = map.Find(m => m.MeasureId == "" + measure.MeasureId);
                if (item == null) return;
                item.ValueEnumIdCurr = "" + setting.EnumValueId;
                item.ValueEnumTitleCurr = setting.EnumValueTitle;
            }
            UpdateMeasureValuesGrid(map);
        }
        private void ShowHotKeysPanel(bool show)
        {
            KeysPanel.Visible = show;
        }
        #endregion

        protected void OnShapeEditClick(object sender, EventArgs e)
        {
            PlottingLabel.Text = "";
            var part = GetPartSelected();
            if (part == null) return;
            var selItem = GetItemFromFullList(lstItemList.SelectedValue);
            if (selItem == null) return;
            var shapePath = GetShapePath();
            if (string.IsNullOrEmpty(shapePath))
            {
                PlottingLabel.Text = "ShapePath2Drawing is empty";
                return;
            }
            var msg = PlottingUtils.CallPlotting(this, GetShapePath(), selItem.FullItemNumber, part.PartName);
            if(!string.IsNullOrEmpty(msg))
            {
                PlottingLabel.Text = msg;
            }
        }
    }
}