﻿using System;
using System.Collections.Generic;
using System.IO;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class SessionParams : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            AppPath.Text = AppUtils.MappedApplicationPath;
            var parms = new List<SessionParamModel>();
            foreach(var parm in SessionConstants.GlobalAll)
            {
                parms.Add(new SessionParamModel(parm, Convert.ToString(Session[parm])));
            }
            var dirParams = parms.FindAll(m => m.Name.ToLower().Contains("root") || m.Name.ToLower().Contains("dir"));
            foreach(var param in dirParams)
            {
                try
                {
                    var dirName = Server.MapPath(param.Value);
                    param.MapFile = dirName;
                } catch (Exception x)
                {
                    param.MapFile = x.Message;
                }
            }
            ParamsGrid.DataSource = parms;
            ParamsGrid.DataBind();
        }

        protected void OnShowDirClick(object sender, EventArgs e)
        {
            PhysicalPathLabel.Text = "";
            ErrorLabel.Text = "";
            if (string.IsNullOrEmpty(DirField.Text.Trim())) return;
            try
            {
                PhysicalPathLabel.Text = Server.MapPath(DirField.Text.Trim());
                var dir = new DirectoryInfo(Server.MapPath(DirField.Text.Trim()));
                if (!dir.Exists)
                {
                    ErrorLabel.Text = "Directory does not exists";
                    return;
                }
                var subDirs = dir.GetDirectories();
                var files = dir.GetFiles();
                var items = new List<DirFileModel>();
                foreach(var subDir in subDirs)
                {
                    items.Add(new DirFileModel(subDir.Name, true));
                }
                foreach (var file in files)
                {
                    items.Add(new DirFileModel(file.Name, false));    
                }
                items.Sort((m1, m2) => string.CompareOrdinal(m1.DisplayName, m2.DisplayName));
                FilesList.DataSource = items;
                FilesList.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
            

        }

        protected void OnApplyClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ParamName.Text.Trim())) return;
            Session[ParamName.Text] = ParamValue.Text.Trim();
        }

    }
}