using System;
using System.Text.RegularExpressions;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
	/// <summary>
	/// Summary description for ShortReport.
	/// </summary>
	public partial class ShortReport : CommonPage
	{
	
		protected void Page_Load(object sender, EventArgs e)
		{			
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Short Report";
            if (!IsPostBack)
            {
                BatchNumber.Focus();
            }
		}

        private void SetWrongInfo(string info)
        {
            WrongInfoLabel.Text = info;
        }
		protected void LookupClick(object sender, EventArgs e)
		{
		    SetWrongInfo("");
		    var batchNumber = BatchNumber.Text.Trim();
			if(!Regex.IsMatch(batchNumber, @"^\d{8}$|^\d{9}$"))
			{
				SetWrongInfo("Not a batch number");
				return;
			}
            var myBatchId = QueryUtils.GetBatchId(batchNumber, this);
            if (string.IsNullOrEmpty(myBatchId))
            {
                SetWrongInfo("This batch number is invalid. Please check the batch number and try again.");
                return;
            }
		    var docId = QueryUtils.GetDocumentId(new BatchModel {BatchId = myBatchId}, this);
            if (docId == "0")
            {
                SetWrongInfo("No Short Report for this batch");
                return;
            }
            Response.Redirect("ItemView.aspx?BatchId=" + myBatchId + "&All=1");					
		}

	}
}
