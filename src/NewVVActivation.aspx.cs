﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class NewVvActivation : CommonPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var onBlurScript = Page.ClientScript.GetPostBackEventReference(PersonEmail, "OnBlur");
            PersonEmail.Attributes.Add("onblur", onBlurScript);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: New VV User";
            if (IsPostBack)
            {
                Page.Validate("ValGrpSearch");
                Page.Validate("ValGrpPerson");
                PostBackHandler();
                return;
            }

            //-- US States
            var states = QueryCustomerUtils.GetUsStates(this);
            PersonUsState.DataSource = states;
            PersonUsState.DataBind();

        }

        private void PostBackHandler()
        {
            var ctrlName = Request.Params[postEventSourceID];
            var args = Request.Params[postEventArgumentID];
            if (ctrlName == PersonEmail.UniqueID && args == "OnBlur")
            {
                OnSearchByEmailClick(null, null);
            }
        }

        #region Check Report parameters
        protected void OnCheckButtonClick(object sender, EventArgs e)
        {
            Page.Validate("ValGrpSearch");
            if (!Page.IsValid) return;
            try
            {
                var exists = QueryUtils.IsExistsReport(ReportNumberFld.Text.Trim(), VirtualNumberFld.Text.Trim(), this);
                exists = true;
                SearchPanel.Enabled = !exists;
                UserPanel.Visible = exists;
                CheckButton.Visible = !exists;
                NextCheckButton.Visible = exists;
                if (exists) FindPdfDocument();
            }
            catch (Exception ex)
            {
                PopupInfoDialog(ex.Message, "Error", true);
            }
        }
        protected void OnNextCheckButtonClick(object sender, EventArgs e)
        {
            SearchPanel.Enabled = true;
            UserPanel.Visible = false;
            CheckButton.Visible = true;
            NextCheckButton.Visible = false;
            ResultCheckLabel.Text = "";
            DocumentLink.Visible = false;
            OnClearClick(null, null);
        }
        protected void OnReportNumberValidate(object source, ServerValidateEventArgs args)
        {
            var itemNumber = args.Value.Trim();
            if (itemNumber.Length == 0 || !Regex.IsMatch(itemNumber, @"^\d{10}$|^\d{11}$"))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        private void FindPdfDocument()
        {
            ResultCheckLabel.Text = "";
            DocumentLink.Visible = false;
            //-- From Global.asax
            var dirName = "" + Session[SessionConstants.GlobalDocumentRoot];
            if (!(dirName.EndsWith("\\") || dirName.EndsWith("/")))
                dirName= dirName + "/";
            
            //-- Exists dir
            try
            {
                var dir = new DirectoryInfo(Server.MapPath(dirName));
                if (!dir.Exists)
                {
                    ResultCheckLabel.Text = "Dir " + dir.FullName + " does not exist.";
                    return ;
                }
            }
            catch (Exception ex)
            {
                ResultCheckLabel.Text = ex.Message;
                return ;
            }
            var findModel = new ReportFindModel
                                {ItemNumber = ReportNumberFld.Text, VirtualVaultNumber = VirtualNumberFld.Text};
            string[] sDocumentType = { "H", "D", "S", "T", "C", "Q", "Z" };
            var al0 = new List<string>();

            //al0.Add("*.pdf");
            al0.Add("367_" + findModel.ItemNumber + " T.pdf");
            al0.Add("133_" + findModel.ItemNumber + " T.pdf");
            al0.Add(findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
            al0.Add(findModel.ItemNumber + ".pdf");

            foreach (var type in sDocumentType)
            {
                al0.Add(type + findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
            }

            foreach (var obj in al0)
            {
                var strTempFileName = "" + Server.MapPath(dirName) + obj;
                var fi = new FileInfo(strTempFileName);
                if (fi.Exists)
                {
                    var fileModel = new FileInfoModel { FileName = fi.Name, LastWriteTimeUtc = fi.LastWriteTimeUtc, Exists = true };
                    DocumentLink.Visible = true;
                    DocumentLink.HRef = fileModel.RedirectUrl;
                    DocumentLink.InnerText = fileModel.FileName;
                    return;
                }
            }
            ResultCheckLabel.Text = "PDF not found.";

        }
        #endregion


        protected void OnAddClick(object sender, EventArgs e)
        {
            Page.Validate("ValGrpPerson");
            if (!Page.IsValid)
            {
                PopupInfoDialog("Please validate user data first!", "Information", true);
                return;
            }

            var msg = QueryUtils.NewUserActivation(GetCustomerInfo(), this);
            if (string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog("Save Succeeded!", "Save", false);
            } else
            {
                PopupInfoDialog(msg, "Save", true);
            }

        }

        protected void OnClearClick(object sender, EventArgs e)
        {
            SetCustomerInfo(new CustomerInfoModel());
            SetEditModeOnUserPanel(true);
        }
        private void SetCustomerInfo(CustomerInfoModel customerInfo)
        {
            PersonFirstName.Text = customerInfo.FirstName;
            PersonLastName.Text = customerInfo.LastName;
            PersonMi.Text = customerInfo.MiddleName;
            PersonAddress.Text = customerInfo.Address;
            PersonCity.Text = customerInfo.City;
            PersonZip.Text = customerInfo.Zip;
            PersonUsState.SelectedValue = customerInfo.State;
            DayPhone.Text = customerInfo.DayPhone;
            OtherPhone.Text = customerInfo.OtherPhone;
            PersonEmail.Text = customerInfo.Email;
            WebPassword.Text = customerInfo.Password;
            CustomerId.Text = customerInfo.Id;
        }
        private CustomerInfoModel GetCustomerInfo()
        {
            var user = new CustomerInfoModel
            {
                FirstName = PersonFirstName.Text.Trim(),
                LastName = PersonLastName.Text.Trim(),
                MiddleName = PersonMi.Text.Trim(),
                Address = PersonAddress.Text.Trim(),
                City = PersonCity.Text.Trim(),
                Zip = PersonZip.Text.Trim(),
                State = PersonUsState.SelectedValue,
                DayPhone = DayPhone.Text.Trim(),
                OtherPhone = OtherPhone.Text.Trim(),
                Email = PersonEmail.Text.Trim(),
                Password = WebPassword.Text,
                ReportNumber = ReportNumberFld.Text.Trim(),
                VirtualVaultNumber = VirtualNumberFld.Text.Trim()
            };
            return user;
        }
        protected void OnSearchByEmailClick(object sender, ImageClickEventArgs e)
        {
            var users = new List<CustomerInfoModel>();
            try
            {
                users = QueryUtils.FindUserByEmail(PersonEmail.Text.Trim(), this);
            } catch (Exception ex)
            {
                PopupInfoDialog(ex.Message, "Error", true);
                return;
            }
             
            if (users.Count == 1)
            {
                SetCustomerInfo(users.ElementAt(0));
            }
            SetEditModeOnUserPanel(users.Count == 0);
        }
        private void SetEditModeOnUserPanel(bool edit)
        {
            PersonFirstName.Enabled = edit;
            PersonLastName.Enabled = edit;
            PersonMi.Enabled = edit;
            PersonAddress.Enabled = edit;
            PersonCity.Enabled = edit;
            PersonZip.Enabled = edit;
            PersonUsState.Enabled = edit;
            DayPhone.Enabled = edit;
            OtherPhone.Enabled = edit;
            WebPassword.Enabled = edit;
        }

        protected void OnSendEmailClick(object sender, ImageClickEventArgs e)
        {
            var msg = ExcelUtils.SendMailVirtulaVault(GetCustomerInfo(), this);
            if (msg.Contains("OK"))
            {
                PopupInfoDialog(msg.Split(';')[1], "Information", false);
            } else
            {
                PopupInfoDialog(msg, "Information", true);
            }

        }
        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    }
}