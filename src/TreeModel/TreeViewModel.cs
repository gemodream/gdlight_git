﻿using System;

namespace Corpt.TreeModel
{
    [Serializable]
    public class TreeViewModel
    {
        public TreeViewModel()
        {
        }

        public string ParentId { get; set; }
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public bool HasParent { get { return !string.IsNullOrEmpty(ParentId); } }
    }
}