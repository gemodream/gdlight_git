﻿using System;
using System.Collections.Generic;

namespace Corpt.TreeModel
{
    [Serializable]
    public class NodeTreeViewModel
    {
        public NodeTreeViewModel()
        {
            Childs = new List<NodeTreeViewModel>();
        }
        public NodeTreeViewModel(TreeViewModel data)
        {
            Data = data;
            Childs = new List<NodeTreeViewModel>();
        }

        public List<NodeTreeViewModel> Childs { get; set; }
        public TreeViewModel Data { get; set; }
        public string DisplayName { get { return " " + Data.DisplayName; } }
        public string Id { get { return Data.Id; } }
        public NodeTreeViewModel Parent { get; set; }
    }
}