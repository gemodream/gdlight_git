﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDataList.aspx.cs" Inherits="Corpt.TestDataList" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DataList Select Example</title>
<script runat="server">



   </script>

</head>
<body>

   <form id="form1" runat="server">

      <h3>DataList Select Example</h3>

      Click <b>Select</b> to select an item.
      <br/>
      <asp:Button runat="server" Text="Reload" OnClick="OnReloadClick"/>
      <br /><br />
   <div>
       <asp:DataList ID="ItemsList" GridLines="Both" CellPadding="3" CellSpacing="0" OnItemCommand="Item_Command"
           runat="server">
           <HeaderStyle BackColor="#aaaadd"></HeaderStyle>
           <AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
           <SelectedItemStyle BackColor="Yellow"></SelectedItemStyle>
           <HeaderTemplate>
               Items
           </HeaderTemplate>
           <ItemTemplate>
               <asp:LinkButton ID="SelectButton" Text="Select" CommandName="Select" runat="server" />
               Item
               <%# DataBinder.Eval(Container.DataItem, "Item") %>
           </ItemTemplate>
           <SelectedItemTemplate>
               Item:
               <asp:Label ID="ItemLabel" Text='<%# DataBinder.Eval(Container.DataItem, "Item") %>'
                   runat="server" />
               <br />
               Quantity:
               <asp:Label ID="QtyLabel" Text='<%# DataBinder.Eval(Container.DataItem, "Qty") %>'
                   runat="server" />
               <br />
               Price:
               <asp:Label ID="PriceLabel" Text='<%# DataBinder.Eval(Container.DataItem, "Price", "{0:c}") %>'
                   runat="server" />
           </SelectedItemTemplate>
       </asp:DataList>
   </div>
   
   <div>
       <br/> DataView and Select button
       <asp:GridView runat="server" ID="MyGridView" AutoGenerateSelectButton="True"
           OnSelectedIndexChanged="OnGridViewSelectedChanged" 
           AutoGenerateColumns="False">
           <Columns>
               <asp:BoundField DataField="Item" HeaderText="Item" />
               <asp:BoundField DataField="Qty" HeaderText="Qty" />
               <asp:BoundField DataField="Price" HeaderText="Price" />
           </Columns>
       </asp:GridView>
   </div>
   <div>
       <br/> DataGrid and select 
       <asp:DataGrid runat="server" ID="MyDataGrid" 
           onitemcommand="MyDataGrid_ItemCommand" 
           onitemdatabound="OnDataGridItemDataBound" 
           onselectedindexchanged="OnDataGridSelectedChanged"></asp:DataGrid>
   </div>
   </form>

</body>
</html>
