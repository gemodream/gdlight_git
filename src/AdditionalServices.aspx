﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="AdditionalServices.aspx.cs" Inherits="Corpt.AdditionalServices" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ToolkitScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=txtSKUChangePrice.ClientID%>, #<%=txtRecheckPrice.ClientID%>, #<%=txtUrgentServicePrice.ClientID%>, #<%=txtColorStonePrice.ClientID%>, #<%=txtRePrintPrice.ClientID%>, #<%=txtUrgentService48Price.ClientID%>, #<%=txtLaserInscriptionPrice.ClientID%>, #<%=txtTNDPrice.ClientID%>, #<%=txtLaserRemovalPrice.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

        });

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }


        function CheckAll(CellNo) {
            var GridView = document.getElementById('<%=gvAdditionalServicesResult.ClientID %>');
            if (GridView.rows[0].cells[CellNo].getElementsByTagName("input")[0].checked == true) {
                if (GridView.rows.length > 0) {
                    for (var Row = 1; Row <= GridView.rows.length; Row++) {

                        if (GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].disabled == false) {
                            GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].checked = true;
                        }
                        //     else { GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].checked = false; }	
                    }
                }
            }
            else {
                if (GridView.rows.length > 0) {
                    for (var Row = 1; Row <= GridView.rows.length; Row++) {
                        // if (GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].checked == false) {
                        GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].checked = false;
                        //} else { GridView.rows[Row].cells[CellNo].getElementsByTagName("input")[0].checked = false; }	
                    }
                }
            }
        }

    </script>


    <div class="demoarea">
        <div class="demoheading">
            Additional Services
        </div>
        <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadButton">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" Text="Batch Number:" CssClass="control-label" runat="server" />
                        <asp:TextBox type="text" ID="txtBatchNumber" MaxLength="9" runat="server" name="txtBatchNumber"
                            Style="font-weight: bold; width: 100px;" placeholder="Batch Number" />
                        <!-- 'Lookup' button -->
                        <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Batch Numbers" ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnLoadClick" />
                    </td>
                    <td>
                        <table id="divDetail" runat="server" visible="false" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: 300; height: 15px !important;">
                                    <asp:Label ID="lblCustomer" runat="server" Text="Customer:" Font-Size="Smaller"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: 300; height: 15px !important;">
                                    <asp:Label ID="lblSKU" runat="server" Text="SKU:" Font-Size="Smaller"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>


        </asp:Panel>
        <div class="control-group error">
            <asp:Label class="control-label" ID="WrongInfoLabel" runat="server" />
        </div>

        <!-- Batch Numbers DataGrid-->
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="vertical-align: top; padding-left: 0px">
                    <!-- Item Details DataGrid -->
                    <div id="divSetPrice" runat="server" style="font-size: small; margin-top: 0px;" align="left" visible="false">
                        <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px;">
                            <legend style="width: 50px; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;" class="label">Pricing</legend>

                            <table cellpadding="0" cellspacing="0" border="0" style="width: 750px;">
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>

                                    <td colspan="2"></td>
                                </tr>

                                <%--     <tr> 
                                  <td>
                                    <asp:Label ID="lblBatchNumber" runat="server" Text="Batch Number:"></asp:Label>
                                </td>
                               

                            </tr>--%>
                                <tr>
                                    <td>

                                        <asp:Label ID="Label2" runat="server" Text="SKU Change Price:" ForeColor="Black"></asp:Label>

                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSKUChangePrice" runat="server" MaxLength="4" placeholder="SKU Change Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="RePrint Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRePrintPrice" runat="server" MaxLength="4" placeholder="RePrint Price " Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text=" Laser Inscription Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLaserInscriptionPrice" runat="server" MaxLength="4" placeholder="Laser Inscription Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Recheck Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRecheckPrice" runat="server" MaxLength="4" placeholder="Recheck Price" Width="50px " Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Color Stone Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtColorStonePrice" runat="server" MaxLength="4" placeholder="Color Stone Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Laser Removal Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLaserRemovalPrice" runat="server" MaxLength="4" placeholder="Laser Removal Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Urgent Service(24 Hrs) Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUrgentServicePrice" runat="server" MaxLength="4" placeholder="Urgent Service Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="Urgent Service(48 Hrs) Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUrgentService48Price" runat="server" MaxLength="4" placeholder="Urgent Service 48 Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="TND Price:" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTNDPrice" runat="server" MaxLength="4" placeholder="TND Price" Width="50px" Height="13px"></asp:TextBox>
                                    </td>
                                    <td style="padding: 0 10px 10px 10px;">
                                        <asp:Button runat="server" ID="btnSaveButton" OnClick="OnSaveClick" Text="Save" CssClass="btn btn-info" /></td>
                                    <td></td>
                                </tr>

                            </table>
                        </fieldset>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <br />
                    <div id="divSetService" runat="server" visible="false" style="font-size: small; margin-top: 00px;">
                        <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px;">
                            <legend style="width: 85px; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;" class="label">Account Rep</legend>


                            <asp:GridView ID="gvAdditionalServicesResult" runat="server" Style="font-size: small;" CellPadding="5" AutoGenerateColumns="False" Width="1000px" OnRowDataBound="gvAdditionalServicesResult_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Report Number" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblReportNumber" Text='<%# Eval("ReportNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is SKU Change /Price" runat="server" ID="label1"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsSKUChange" runat="server" onclick="CheckAll(2);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsSKUChange" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChangeSKU")) %>' />
                                            <asp:HiddenField ID="hfBatchID" runat="server" Value='<%# Eval("BatchID") %>' />
                                            <asp:HiddenField ID="hfGroupID" runat="server" Value='<%# Eval("GroupID") %>' />
                                            <asp:HiddenField ID="hfCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />
                                            <asp:HiddenField ID="hfCPID" runat="server" Value='<%# Eval("CPID") %>' />
                                            <asp:HiddenField ID="hfCPName" runat="server" Value='<%# Eval("CustomerProgramName") %>' />

                                            &nbsp;&nbsp;/&nbsp;&nbsp; 
                                        <asp:Label ID="lblSKUChangePrice" runat="server" Text='<%# Eval("ChangeSKUPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Is Recheck /Price" ItemStyle-Width="100px">

                                        <HeaderTemplate>
                                            <asp:Label Text="Is Recheck /Price" runat="server" ID="label2"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsRecheck" runat="server" onclick="CheckAll(3);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsRecheck" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsRecheck")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblRecheckPrice" runat="server" Text='<%# Eval("RecheckPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Is Urgent Service(24 Hrs) /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is Urgent Service (24 Hrs)/Price" runat="server" ID="label3"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsUrgentService" runat="server" onclick="CheckAll(4);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsUrgentService" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsUrgent")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblUrgentServicePrice" runat="server" Text='<%# Eval("UrgentServicePrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Urgent Service(48 Hrs) /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is Urgent Service (48 Hrs)/Price" runat="server" ID="label34"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsUrgent48Service" runat="server" onclick="CheckAll(5);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsUrgent48Service" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsUrgent48")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblUrgent48ServicePrice" runat="server" Text='<%# Eval("UrgentService48Price") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Is Color Stone /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is Color Stone /Price" runat="server" ID="label4"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsColorStone" runat="server" onclick="CheckAll(6);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsColorStone" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsColorStone")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblColorStonePrice" runat="server" Text='<%# Eval("ColorStonePrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is RePrint /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is RePrint /Price" runat="server" ID="label5"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsRePrint" runat="server" onclick="CheckAll(7);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsRePrint" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsRePrint")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblRePrintPrice" runat="server" Text='<%# Eval("RePrintPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Laser Inscription /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is Laser Inscription /Price" runat="server" ID="label6"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsLaserInscription" runat="server" onclick="CheckAll(8);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsLaserInscription" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsLaserInscription")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblLaserInscriptionPrice" runat="server" Text='<%# Eval("LaserInscriptionPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Is Laser Removal /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is Laser Removal /Price" runat="server" ID="label7"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsLaserRemoval" runat="server" onclick="CheckAll(9);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsLaserRemoval" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsLaserRemoval")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblLaserRemovalPrice" runat="server" Text='<%# Eval("LaserRemovalPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is TND /Price" ItemStyle-Width="100px">
                                        <HeaderTemplate>
                                            <asp:Label Text="Is TND /Price" runat="server" ID="label8"></asp:Label><br />
                                            <asp:CheckBox ID="checkAllIsTND" runat="server" onclick="CheckAll(9);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkIsTND" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsTND")) %>' />
                                            &nbsp;&nbsp;/&nbsp;&nbsp;
                                      <asp:Label ID="lblTNDPrice" runat="server" Text='<%# Eval("TNDPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                            </asp:GridView>

                            <br />
                            <asp:Button runat="server" ID="btnSetService" OnClick="OnSaveService" Text="Set Service" CssClass="btn btn-info" Visible="false" />
                        </fieldset>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>

            </tr>

        </table>
    </div>


</asp:Content>
