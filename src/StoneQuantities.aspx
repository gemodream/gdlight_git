﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="StoneQuantities.aspx.cs" Inherits="Corpt.StoneQuantities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .text_highlitedFail
        {
            background-color: #FFFF00;
        }
        .text_highlitedMiss
        {
            background-color: #7DFFFF;
        }
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }
    </style>

    <div class="demoarea">
        <div class="demoheading">Stone Quantities</div>
        <div style="height: 20px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <table>
                    <!-- Period Type, Labels: Order State, Customer Like  -->
                    <tr>
                        <td style="padding-left: 20px">
                            <asp:RadioButtonList ID="periodType" runat="server" CssClass="radio" OnSelectedIndexChanged="OnChangedPeriodType"
                                RepeatDirection="Horizontal" Width="250px" AutoPostBack="true">
                                <asp:ListItem Value="m">Monthly</asp:ListItem>
                                <asp:ListItem Value="w" Selected="True">Weekly</asp:ListItem>
                                <asp:ListItem Value="r">Random</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="padding-left: 30px">
                            <label>
                                Order State</label>
                        </td>
                        <td style="padding-left: 30px">
                            <label>
                                Customer Like</label>
                        </td>
                    </tr>
                    <!-- Panel: Date From, Date To; Order State, Panel: Customer Like  -->
                    <tr>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                <label>
                                    From</label>
                                <asp:TextBox runat="server" ID="calFrom" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                                <label style="padding-left: 20px;">
                                    To</label>
                                <asp:TextBox runat="server" ID="calTo" Width="100px" />
                                <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                    AlternateText="Click to show calendar" />
                            </asp:Panel>
                        </td>
                        <td style="padding-left: 30px">
                            <asp:DropDownList ID="OrderStateList" runat="server" DataTextField="OrderStateName"
                                DataValueField="OrderStateCode" Width="150px" />
                        </td>
                        <td style="padding-left: 30px">
                            <asp:Panel runat="server" DefaultButton="CustomerLikeButton" ID="SearchPanelByCustomerCp"
                                CssClass="form_inline">
                                <asp:TextBox runat="server" ID="CustomerLike" Style="font-family: Arial; font-size: 12px;
                                    width: 80px"></asp:TextBox>
                                <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                    ImageUrl="~/Images/ajaxImages/search16.png" OnClick="OnCustomerSearchClick" />
                                <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                    DataValueField="CustomerId" AutoPostBack="True" Width="350px" Style="font-family: Arial;
                                    font-size: 12px" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <!-- Offices Tree  -->
                        <td style="vertical-align: top; font-family: sans-serif">
                            <asp:TreeView ID="OfficeTree" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                ShowLines="False" ShowExpandCollapse="true">
                                <SelectedNodeStyle BackColor="#D7FFFF" />
                            </asp:TreeView>
                        </td>
                        <!-- Result by Offices Filter  -->
                        <td style="vertical-align: top; padding-left: 40px;">
                            <asp:Label ID="Label1" runat="server" class="label" Text="Stones by Offices"></asp:Label>
                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Stone quentities by Location"
                                ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnLookupByLocationClick" />
                            <br />
                            <label id="ByCountryTotal" runat="server" />
                            <asp:DataGrid ID="GridCountries" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                DataKeyField="OfficeId">
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Tahoma" Font-Size="Small"
                                    ForeColor="White" />
                                <ItemStyle BackColor="White" BorderColor="Silver" ForeColor="#0D0D0D" Font-Size="Small" />
                                <Columns>
                                    <asp:BoundColumn DataField="Country" HeaderText="Country" />
                                    <asp:BoundColumn DataField="OfficeCode" HeaderText="Office" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Quantity" DataField="Quentity" ItemStyle-HorizontalAlign="Right" />
                                    <asp:TemplateColumn>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DetailsBtn" ToolTip="Customer stone quantity" runat="server"
                                                ImageUrl="~/Images/ajaxImages/look24.png" OnClick="OnDetailsBtnClick" Enabled="true"
                                                Style="text-align: center" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <br />
                            <label id="OfficeLabel" runat="server" />
                            <div style="overflow-y: auto; overflow-x: hidden; height: 500px; width: 400px; color: black">
                            <asp:GridView ID="GridOfficeCustomers" runat="server" AutoGenerateColumns="false"
                                class="table-bordered" CellPadding="5" ForeColor="Black">
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Customer" DataField="CustomerName" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            </div>
                        </td>
                        <!-- Result by Customer Filter  -->
                        <td style="vertical-align: top; padding-left: 40px;">
                            <asp:Label ID="Label2" runat="server" class="label" Text="Stones By Customer"></asp:Label>
                            <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Stone quentities by Customer"
                                ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnLookupByCustomerClick" />
                            <br />
                            <label id="ByCustomerTotal" runat="server" />
                            <asp:GridView ID="GridCustomers" runat="server" AutoGenerateColumns="false" class="table-bordered"
                                CellPadding="5" ForeColor="Black">
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Customer" DataField="CustomerName" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                    PopupButtonID="Image1" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                    PopupButtonID="Image2" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
