using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for BulkUpdateII.
	/// </summary>
	public partial class BulkUpdateII : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button Button1;
//		protected System.Web.UI.WebControls.DataGrid dgDebug;
//		protected System.Web.UI.WebControls.DataGrid dgDebug2;
		protected System.Web.UI.WebControls.CheckBox chkWeight;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if(!IsPostBack)
			{
				LoadMeasures();
			}
#if DEBUG
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			lblDebug.Text=conn.DataSource+"&nbsp;:&nbsp;"+conn.Database;
#endif

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		
		private void LoadMeasures()
		{
		}

		protected void cmdGet_Click(object sender, System.EventArgs e)
		{
			string strOrderCode = Utils.FillToFiveChars(order.Text.Trim());
			string strBatchCode = Utils.FillToThreeChars(batch.Text.Trim(), strOrderCode);
				if(Regex.IsMatch(strOrderCode, @"^\d{5}$|^\d{6}$|^\d{7}$") && Regex.IsMatch(strBatchCode, @"^\d{3}$"))
				{
					order.Text = strOrderCode;
					batch.Text = strBatchCode;

					SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
					SqlCommand command = new SqlCommand("SELECT v0Part.PartID, v0Part.PartName FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID WHERE (v0Batch.GroupCode = '"+ strOrderCode +"') AND (v0Batch.BatchCode = '"+ strBatchCode+ "')");
					command.Connection = conn;
					command.CommandType = CommandType.Text;

					SqlDataAdapter da = new SqlDataAdapter(command);
					DataTable dt = new DataTable();
					try
					{
						da.Fill(dt);
						lblInfo.Text="";
					}
					catch(Exception ex)
					{
						lblInfo.Text="Fail:&nbsp;"+ex.Message;
					}
					if(dt.Rows.Count>0)
					{
						lstPartName.DataSource=dt;
						lstPartName.DataTextField  = "PartName";
						lstPartName.DataValueField = "PartID";
						lstPartName.DataBind();

#if DEBUG
						//				dgItemSttructure.DataSource = dt;
						//				dgItemSttructure.DataBind();
#endif

						//			txtOrder.Text=order.Text;
						lblInfo.Text="";
						command.CommandText="sp_getSimialrBatches";
						command.CommandType=CommandType.StoredProcedure;
				
						command.Parameters.Add(new SqlParameter("@GroupCode",order.Text.Trim()));
						command.Parameters.Add(new SqlParameter("@BatchCode",batch.Text.Trim()));

						SqlDataAdapter daSimilarBatches = new SqlDataAdapter(command);
						DataTable dtSimilarBatches = new DataTable();
						daSimilarBatches.Fill(dtSimilarBatches);
						if(dtSimilarBatches.Rows.Count>0)
						{
							chklstBatches.DataSource=dtSimilarBatches;
							chklstBatches.DataTextField="BatchCode";
							chklstBatches.DataValueField="BatchID";
							chklstBatches.DataBind();
							Session["sp_getSimialrBatches"]=dtSimilarBatches;
#if DEBUG
							//					dgDebugBatchInfo.DataSource=dtSimilarBatches;
							//					dgDebugBatchInfo.DataBind();
#endif
						}
					}
					else

					{
						lblInfo.Text="Could not load item. Check the numbers.";
					}
				}
				else
				{
					lblInfo.Text = "Check Order/Batch format";
				}
		}

		protected void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem li in chklstBatches.Items)
			{
				li.Selected=true;
			}
		}

		protected void cmdDeselectAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem li in chklstBatches.Items)
			{
				li.Selected=false;
			}		
		}

		protected void cmdLoadMeasures_Click(object sender, System.EventArgs e)
		{
			lstMeasures.Items.Clear();

			DataTable dtSimialrBatches = new DataTable();
			dtSimialrBatches = Session["sp_getSimialrBatches"] as DataTable; 

			string strBatchID;
			strBatchID = dtSimialrBatches.Rows[0]["BatchID"].ToString();
			
			string strItemTypeID;
			strItemTypeID = dtSimialrBatches.Rows[0]["ItemTypeID"].ToString();


			SqlCommand command = new SqlCommand("spGetItemMeasuresByCP");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@ItemTypeID", strItemTypeID));
			command.Parameters.Add(new SqlParameter("@BatchID", strBatchID));
			command.Parameters.Add(new SqlParameter("@AuthorId", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeId", Session["AuthorOfficeID"].ToString()));

			if(rblMeasureMode.SelectedValue=="cpDisabled")
			{
				command.Parameters.RemoveAt("@BatchID");
				command.CommandText="spGetMeasuresByItemTypePartID";
			}

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);

			da.Fill(dt);
			
			if(dt.Rows.Count>0)
			{
				Session["spGetItemMeasuresByCP"] = dt;

				string strItemPartID;
				strItemPartID = lstPartName.SelectedValue;

				DataRow[] drsPartMeasures = dt.Select("PartID = '"+strItemPartID+"'");

				if(drsPartMeasures.Length>0)
				{
					DataTable dtPartMeasures = new DataTable();
					dtPartMeasures = dt.Copy();
					dtPartMeasures.Rows.Clear();
					dtPartMeasures.AcceptChanges();

					foreach(DataRow dr in drsPartMeasures)
					{
						dtPartMeasures.Rows.Add(dr.ItemArray);
					}
					dtPartMeasures.AcceptChanges();
	                
					if(dtPartMeasures.Rows.Count>0)
					{
						lstMeasures.DataSource = dtPartMeasures;
						lstMeasures.DataValueField="MeasureID";
						lstMeasures.DataTextField="MeasureName";
						lstMeasures.DataBind();

						Session["dtPartMeasures"] = dtPartMeasures;

//						dgDebug.DataSource=dtPartMeasures;
//						dgDebug.DataBind();
					}
					else
					{
						lblInfo.Text="Could not load measures for part.";
					}
				}
				else
				{
					lblInfo.Text="Could not load measures";
				}

			}
			else
			{
				lblInfo.Text="Can't get any measures.(spGetItemMeasuresByCP)";
			}


		/*
					lstMeasures.DataSource=dtGrade_MeasureList;
					lstMeasures.DataTextField="MeasureName";
					lstMeasures.DataValueField="MeasureID";
					lstMeasures.DataBind();
					*/
		}

		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			DataTable dt = new DataTable("Batches");
			dt.Columns.Add("BatchID");
			foreach(ListItem l in chklstBatches.Items)
			{
				if(l.Selected)
				{
					DataRow dr = dt.NewRow();
					dr["BatchID"]=l.Value;
					dt.Rows.Add(dr);
				}
			}
			DataSet dsBatches = new DataSet("Batches");
			dsBatches.Tables.Add(dt);

			SqlCommand command = new SqlCommand("sp_SetMeasuresBulk2");
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

			command.Parameters.Add(new SqlParameter("@AuthorId", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeId", Session["AuthorOfficeID"].ToString()));
			command.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));

			command.Parameters.Add(new SqlParameter("@ItemTypeID", (Session["sp_getSimialrBatches"] as DataTable).Rows[0]["ItemTypeID"].ToString()));
			command.Parameters.Add(new SqlParameter("@PartId", lstPartName.SelectedValue));
			command.Parameters.Add(new SqlParameter("@SetMeasureID", lstMeasures.SelectedValue));

			//prepare the selected/entered measure here.
			command.Parameters.Add(GetMeasuredParameter());

			command.Parameters.Add(new SqlParameter("@BatchList", dsBatches.GetXml()));

			lblDebug.Text=dsBatches.GetXml();
			string strDebugXMLPath = System.Environment.GetEnvironmentVariable("TEMP")+"\\"+DateTime.Now.Ticks.ToString()+".xml";
			dsBatches.WriteXml(strDebugXMLPath);

			SqlDataAdapter da = new SqlDataAdapter(command);
			command.CommandTimeout=0;
			DataTable dtResult = new DataTable();
			da.Fill(dtResult);

			if(dtResult.Rows.Count>0)
			{
				lblInfo.Text = "Updated&nbsp;"+dtResult.Rows.Count.ToString()+"&nbsp;items.";
			}
			else
			{
				lblInfo.Text="Update failed";
			}
//			dgDebug.DataSource=dtResult;
//			dgDebug.DataBind();
		}

		private SqlParameter GetMeasuredParameter(out string retStrMeasureClass)
		{
			DataTable dtPartMeasures = Session["dtPartMeasures"] as DataTable;
			string strSelectedMeasureID = lstMeasures.SelectedValue;

			DataRow[] drsMeasures = dtPartMeasures.Select("MeasureID = "+strSelectedMeasureID);
			if(drsMeasures.Length==1)
			{
				string strMeasureClass = drsMeasures[0]["MeasureClass"].ToString();
				retStrMeasureClass = strMeasureClass;
				switch(strMeasureClass)
				{
					case "1": //enum
					{
						SqlParameter param = new SqlParameter("@SetMeasureValueID", lstEnumValues.SelectedValue);
						return param;
						break;
					}
					case "2": //String // StringValue
					{
						SqlParameter param = new SqlParameter("@SetStringValue", txtSetValue.Text.Trim());
						return param;
						break;
					}
					case "3": //Numeric Value //MeasureValue
					{
						SqlParameter param = new SqlParameter("@SetMeasureValue", txtSetValue.Text.Trim());
						return param;
						break;
					}
				}
			}
			else
			{
				Exception up = new Exception();
				throw up;
			}
			return null;
		}

		private SqlParameter GetMeasuredParameter()
		{
			string outStrMeasureClass;

			return GetMeasuredParameter(out outStrMeasureClass);

////			DataTable dtPartMeasures = Session["dtPartMeasures"] as DataTable;
////			string strSelectedMeasureID = lstMeasures.SelectedValue;
////
////			DataRow[] drsMeasures = dtPartMeasures.Select("MeasureID = "+strSelectedMeasureID);
////			if(drsMeasures.Length==1)
////			{
////				string strMeasureClass = drsMeasures[0]["MeasureClass"].ToString();
////				switch(strMeasureClass)
////				{
////					case "1": //enum
////					{
////						SqlParameter param = new SqlParameter("@SetMeasureValueID", lstEnumValues.SelectedValue);
////						return param;
////						break;
////					}
////					case "2": //String // StringValue
////					{
////						SqlParameter param = new SqlParameter("@SetStringValue", txtSetValue.Text.Trim());
////						return param;
////						break;
////					}
////					case "3": //Numeric Value //MeasureValue
////					{
////						SqlParameter param = new SqlParameter("@SetMeasureValue", txtSetValue.Text.Trim());
////						return param;
////						break;
////					}
////				}
////			}
////			else
////			{
////				Exception up = new Exception();
////				throw up;
////			}
////			return null;
		}

		protected void lstMeasures_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lstMeasures.SelectedIndex!=-1)
			{
				//show the panel (and load the measurelist)
				DataTable dtMeasures = new DataTable();
				dtMeasures = Session["dtPartMeasures"] as DataTable;

				DataRow[] drs = dtMeasures.Select("MeasureID = " + lstMeasures.SelectedValue);
				string strPartID = drs[0]["PartID"].ToString();
				string strMeasureID = drs[0]["MeasureID"].ToString();
////				string strDebug = "";
////				foreach(DataRow ddd in drs)
////				{
////					for(int i =0; i<ddd.ItemArray.Length;i++)
////					{
////						strDebug+=ddd.ItemArray[i].ToString()+", ";
////					}
////					strDebug+="<br />";
////				}
////				Label1.Text=strDebug;
				if(drs.Length==1)
				{
					//we have selected a measure
					string strMeasureClass = drs[0]["MeasureClass"].ToString();
					switch(strMeasureClass)
					{
						case "1": //enum
						{
							//show panel
							ShowPnlEnum();

							//get enum measure list
							SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
							SqlCommand command = new SqlCommand("spGetMeasureValues");
							command.Connection = conn;
							command.CommandType = CommandType.StoredProcedure;
							command.Parameters.Add("@AuthorID", Session["ID"].ToString());
							command.Parameters.Add("@AuthorOfficeID",Session["AuthorOfficeID"].ToString());
							DataTable dt = new DataTable();
							SqlDataAdapter da;
							da = new SqlDataAdapter(command);
							da.Fill(dt);
							Session["GDLight_EnumMeasures"]=dt.Copy();

							//MeasureValueMeasureID

							DataRow[] drsSelect = dt.Select("MeasureValueMeasureID = " + lstMeasures.SelectedValue);
							DataTable dtSelectedMeasureValues = new DataTable();
							dtSelectedMeasureValues = dt.Copy();
							dtSelectedMeasureValues.Rows.Clear();
							dtSelectedMeasureValues.AcceptChanges();

							foreach(DataRow dr in drsSelect)
							{
								dtSelectedMeasureValues.Rows.Add(dr.ItemArray);								
							}
							dtSelectedMeasureValues.AcceptChanges();

							lstEnumValues.DataSource=dtSelectedMeasureValues;
							lstEnumValues.DataTextField="MeasureValueName";
							lstEnumValues.DataValueField="MeasureValueID";
							lstEnumValues.DataBind();

							Session["dtSelectedMeasureValues"]=dtSelectedMeasureValues;

                            //Set CP measure here:
                            SetCPEnum(lstEnumValues, strPartID, strMeasureID);

//							dgEnum.DataSource=dt;
//							dgEnum.DataBind();
							break;
						}
						case "2": //String // StringValue
						{
							ShowPnlString();							
							break;
						}
						case "3": //Numeric Value //MeasureValue
						{
							ShowPnlString();
							SetCPMeasureValue(txtSetValue, strPartID, strMeasureID);
							break;
						}
					}

				}
				else
				{
					lblInfo.Text = "Can not select the measure.";
				}
			}
			else
			{}
		}

		private void SetCPEnum(DropDownList lstEnumValue, string strPartID, string strMeasureID)
		{
			//exec sp_GetCPRulesWithPreFill @ordercode = 63210, @BatchCode = 1
			SqlCommand command = new SqlCommand("sp_GetCPRulesWithPreFill");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@OrderCode", order.Text.Trim()));
			command.Parameters.Add(new SqlParameter("@BatchCode", batch.Text.Trim()));

			SqlDataAdapter da = new SqlDataAdapter(command);

			DataTable dt = new DataTable();

			da.Fill(dt);
//			dgDebug.DataSource=dt;
//			dgDebug.DataBind();
		}
		
		private void SetCPMeasureValue(TextBox txtSetValue, string strPartID, string strMeasureID)
		{

		}

		private void ShowPnlEnum()
		{
			pnlEnum.Visible=true;
			pnlEnum.Enabled=true;

			pnlString.Visible=false;
			pnlString.Enabled=false;

		}

		private void ShowPnlString()
		{
			pnlEnum.Visible=false;
			pnlEnum.Enabled=false;

			pnlString.Visible=true;
			pnlString.Enabled=true;

		}

		protected void cmdClear_Click(object sender, System.EventArgs e)
		{
			lstEnumValues.Items.Clear();
			lstMeasures.Items.Clear();
			chklstBatches.Items.Clear();
			lstPartName.Items.Clear();
			txtSetValue.Text="";
			order.Text="";
			batch.Text="";
			Session["dtPartMeasures"]=null;
			Session["GDLight_EnumMeasures"]=null;
			Session["dtSelectedMeasureValues"]=null;

			pnlEnum.Visible=false;
			pnlEnum.Enabled=false;

			pnlString.Visible=false;
			pnlString.Enabled=false;

			if((sender as WebControl).ID!="rblMeasureMode")
			{
				rblMeasureMode.SelectedIndex=0;
			}			
		}

		protected void rblMeasureMode_SelectedIndexChanged(object sender, System.EventArgs e)
		{			
			cmdClear_Click(sender, e);
		}

		protected void cmdPrefill_Click(object sender, System.EventArgs e)
		{
			lblInfo.Text="";
			foreach(ListItem l in chklstBatches.Items)
			{
				if(l.Selected)
				{
					DoThePreFill(l.Value, lstPartName.SelectedValue, lstMeasures.SelectedValue);
				}
			}
			lblInfo.Text="Done";
		}

		private void DoThePreFill(string strBatchID, string strPartName, string strMeasure)
		{
			SqlCommand command = new SqlCommand("spGetCustomerProgramByBatchID");
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

			command.Parameters.Add(new SqlParameter("@BatchID",strBatchID));
			command.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
			command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

			SqlDataAdapter da = new SqlDataAdapter(command);

			DataTable dtCP = new DataTable();
			da.Fill(dtCP);

//			dgDebug.DataSource=dtCP;
//			dgDebug.DataBind();

			if(dtCP.Rows.Count>0)
			{
				string strCPID = dtCP.Rows[0]["CPID"].ToString();
				string strCPOfficeID = dtCP.Rows[0]["CPOfficeID"].ToString();

				SqlCommand spGetCPDocs = new SqlCommand("spGetCPDocs");
				spGetCPDocs.CommandType = CommandType.StoredProcedure;
				spGetCPDocs.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

				spGetCPDocs.Parameters.Add(new SqlParameter("@CPOfficeID", strCPOfficeID));
				spGetCPDocs.Parameters.Add(new SqlParameter("@CPID", strCPID));
				spGetCPDocs.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));
				spGetCPDocs.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));

				SqlDataAdapter daspGetCPDocs = new SqlDataAdapter(spGetCPDocs);
				DataTable dtspGetCPDocs = new DataTable();
				daspGetCPDocs.Fill(dtspGetCPDocs);
				
//				dgDebug.DataSource=dtspGetCPDocs;
//				dgDebug.DataBind();

				foreach(DataRow dr in dtspGetCPDocs.Rows)
				{
					SqlCommand spGetCPDocRule = new SqlCommand("spGetCPDocRule");
					spGetCPDocRule.CommandType=CommandType.StoredProcedure;
					spGetCPDocRule.Connection=new SqlConnection(Session["MyIP_ConnectionString"].ToString());

					string strCPDocID = dr["CPDocID"].ToString();

					spGetCPDocRule.Parameters.Add(new SqlParameter("@CPDocID", strCPDocID));
					spGetCPDocRule.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
					spGetCPDocRule.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

					SqlDataAdapter daspGetCPDocRule = new SqlDataAdapter(spGetCPDocRule);
					DataTable dtCPDocRule = new DataTable();

					daspGetCPDocRule.Fill(dtCPDocRule);
					
					if(dtCPDocRule.Rows.Count>0)
					{
//						dgDebug.DataSource = dtCPDocRule;
//						dgDebug.DataBind();

						DataRow[] drSelectedPreFill = dtCPDocRule.Select("[IsDefaultMeasureValue] = 1 AND [PartID] = '"+strPartName+"' AND [MeasureID] = '" + strMeasure + "' ");
//						dgDebug2.DataSource = drSelectedPreFill;
//						dgDebug2.DataBind();

						string strMeasureValue = drSelectedPreFill[0]["MaxMeasure"].ToString();

						if(drSelectedPreFill.Length==1)
						{//found pre-fill
							FillBatch(strBatchID, strPartName, strMeasure, strMeasureValue);
						}
					}
				}
			}
		}

		private void FillBatch(string strBatchID, string strPartName, string strMeasure, string strMeasureValue)
		{
            SqlCommand cmItemList = new SqlCommand("wspvvGetItemsByBatchExistingNumbersOnly");
			cmItemList.CommandType= CommandType.StoredProcedure;
			cmItemList.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

			cmItemList.Parameters.Add(new SqlParameter("@BatchID", strBatchID));

			SqlDataAdapter da = new SqlDataAdapter(cmItemList);
			DataTable dtItemList = new DataTable();

			da.Fill(dtItemList);
            
//			dgDebug2.DataSource = dtItemList;
//			dgDebug2.DataBind();

			foreach(DataRow drItem in dtItemList.Rows)
			{
				SqlCommand spSetPartValue = new SqlCommand("spSetPartValue");
				spSetPartValue.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				spSetPartValue.CommandType = CommandType.StoredProcedure;

				/*
					 * @rId varchar(150) output,
						@ItemCode dnItemCode,
						@BatchID dnID, 
						@PartID dnSmallID, 
						@MeasureCode dnTinyID,
						@MeasureValue dnMeasureExt,
						@MeasureValueID dnSmallID,
						@StringValue dsName,
						@UseClosedRecheckSession int,
						@CurrentOfficeID dnTinyID,
						@AuthorID dnSmallID, 
						@AuthorOfficeID dnTinyID) 
*/
				spSetPartValue.Parameters.Add(new SqlParameter("@rId",SqlDbType.VarChar));
				spSetPartValue.Parameters["@rId"].Size=150;
				spSetPartValue.Parameters["@rId"].Direction = ParameterDirection.Output;

				spSetPartValue.Parameters.Add(new SqlParameter("@ItemCode",drItem["ItemCode"].ToString()));
				spSetPartValue.Parameters.Add(new SqlParameter("@BatchID", strBatchID));
				spSetPartValue.Parameters.Add(new SqlParameter("@PartID", strPartName));
				spSetPartValue.Parameters.Add(new SqlParameter("@MeasureCode", GetMeasureCodeFromMeasureID(strMeasure)));



				//decide wich type we have here

				string retStrMeasureClass;
				SqlParameter parMeasureValue = GetMeasuredParameter(out retStrMeasureClass);
				switch(retStrMeasureClass)
				{
					case "1": //enum
					{
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValueID", strMeasureValue));
						spSetPartValue.Parameters.Add(new SqlParameter("@StringValue", DBNull.Value));						
						break;
					}
					case "2": //String // StringValue
					{
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
						spSetPartValue.Parameters.Add(new SqlParameter("@StringValue", strMeasureValue));		
						break;
					}
					case "3": //Numeric Value //MeasureValue
					{
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValue", strMeasureValue));
						spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
						spSetPartValue.Parameters.Add(new SqlParameter("@StringValue", DBNull.Value));		
						break;
					}

				}

////				spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
////				spSetPartValue.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
////				spSetPartValue.Parameters.Add(new SqlParameter("@StringValue", strLaserName));
				spSetPartValue.Parameters.Add(new SqlParameter("@UseClosedRecheckSession", DBNull.Value));
				spSetPartValue.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));
				spSetPartValue.Parameters.Add(new SqlParameter("@AuthorID", Session["ID"].ToString()));
				spSetPartValue.Parameters.Add(new SqlParameter("@AuthorOfficeID", Session["AuthorOfficeID"].ToString()));

				spSetPartValue.Connection.Open();
				spSetPartValue.ExecuteNonQuery();
				spSetPartValue.Connection.Close();
			}
		}

		private string GetMeasureCodeFromMeasureID(string strMeasure)
		{
			SqlCommand command = new SqlCommand("spGetMeasureCodeFromMeasureID");
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());

			command.Parameters.Add(new SqlParameter("@MeasureID", strMeasure));
			command.Parameters.Add(new SqlParameter("@MeasureCode", SqlDbType.Int, 8));
			command.Parameters["@MeasureCode"].Direction = ParameterDirection.Output;

			command.Connection.Open();
			command.ExecuteNonQuery();
			command.Connection.Close();

			return command.Parameters["@MeasureCode"].Value.ToString();
		}

		protected void cmdRandomizeMeasurements_Click(object sender, System.EventArgs e)
		{
			lblInfo.Text="";
			foreach(ListItem l in chklstBatches.Items)
			{
				if(l.Selected)
				{
					DoRandomWeights(l.Value, lstPartName.SelectedValue);
				}
			}
			lblInfo.Text="Done";
		}		

		private void DoRandomWeights(string strBatchID, string strPartName)
		{
			string strCPID = Utils.GetCPIDbyBatchID(strBatchID, this);

			DataTable dtCpDocs = Utils.GetCpDocsByCPID(strCPID, this);
			
//			dgDebug.DataSource = dtCpDocs;
//			dgDebug.DataBind();

			foreach(DataRow drCPDoc in dtCpDocs.Rows)
			{
				DataTable dtCpDocMeasures = new DataTable();
				dtCpDocMeasures = Utils.GetCPDocMeasuresByCPDoc(drCPDoc["CPDocID"].ToString(), this);
//				dgDebug2.DataSource = dtCpDocMeasures;
//				dgDebug2.DataBind();
                
				if(dtCpDocMeasures.Rows.Count>0)
				{
					//min
					string strMinTop;
					string strMinBottom;

					DataRow[] drsSelected = dtCpDocMeasures.Select("[IsDefaultMeasureValue] = 1 AND [PartID] = '"+strPartName+"' AND [MeasureID] = '12' ");
				
					strMinTop = drsSelected[0]["MaxMeasure"].ToString();
					strMinBottom = drsSelected[0]["MinMeasure"].ToString();

                
					//max
					string strMaxTop;
					string strMaxBottom;

					drsSelected = dtCpDocMeasures.Select("[IsDefaultMeasureValue] = 1 AND [PartID] = '"+strPartName+"' AND [MeasureID] = '11' ");
					strMaxTop = drsSelected[0]["MaxMeasure"].ToString();
					strMaxBottom = drsSelected[0]["MinMeasure"].ToString();

					//H_x
					string strH_xTop;
					string strH_xBottom;

					drsSelected = dtCpDocMeasures.Select("[IsDefaultMeasureValue] = 1 AND [PartID] = '"+strPartName+"' AND [MeasureID] = '13' ");
					strH_xTop = drsSelected[0]["MaxMeasure"].ToString();
					strH_xBottom = drsSelected[0]["MinMeasure"].ToString();

  
					SqlCommand command = new SqlCommand("spSetRandomMeasurementsByBatchIDWithLoginInfo");
					command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
					command.CommandType= CommandType.StoredProcedure;
					command.CommandTimeout=0;

					/*
					 * 	@BatchID int,
						@PartID int,
						@StartValueMax decimal(6,4),
						@EndValueMax decimal(6,4),
						@StartValueMin decimal(6,4),
						@EndValueMin decimal(6,4),
						@StartValueH_x decimal(6,4),
						@EndValueH_x decimal(6,4)
					*/

					command.Parameters.Add(new SqlParameter("@AuthorId", Session["ID"].ToString()));
					command.Parameters.Add(new SqlParameter("@AuthorOfficeId", Session["AuthorOfficeID"].ToString()));
					command.Parameters.Add(new SqlParameter("@CurrentOfficeID", Session["AuthorOfficeID"].ToString()));

					command.Parameters.Add(new SqlParameter("@BatchID", strBatchID));	
					command.Parameters.Add(new SqlParameter("@PartID", strPartName));
					command.Parameters.Add(new SqlParameter("@StartValueMax", strMaxBottom));
					command.Parameters.Add(new SqlParameter("@EndValueMax", strMaxTop));
					command.Parameters.Add(new SqlParameter("@StartValueMin", strMinBottom));
					command.Parameters.Add(new SqlParameter("@EndValueMin", strMinTop));
					command.Parameters.Add(new SqlParameter("@StartValueH_x", strH_xTop));
					command.Parameters.Add(new SqlParameter("@EndValueH_x", strH_xBottom));
					
					command.Connection.Open();
					command.CommandTimeout = 0;
					command.ExecuteNonQuery();
					command.Connection.Close();					
				}
			}
		}
	}
}
