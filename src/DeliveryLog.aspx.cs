using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for DeliveryLog.
	/// </summary>
	public partial class DeliveryLog : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox @BatchNumber;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

			
		protected void cmdClear_Click(object sender, System.EventArgs e)
		{
			ClearAll();
		}		

		private void ClearAll()
		{			
			lstItems.Items.Clear();
//			txtBatchNumber.Text="";
			lblBatchNumber.Text="";
			txtItemNumber.Text="";		
			Session["DeliveryLogBatchID"] = null;
			Session["DeliveryLogItemList"]=null;
			lblInfo.Text="";
		}
		
		private void GetBatchFromItemNumber(string strItemNumber)
		{
			
			strItemNumber = strItemNumber.Trim();			
			if(Regex.IsMatch(strItemNumber,@"^\d{10}$|^\d{11}$"))
			{
				//Check If the item Belongs to the Batch
				SqlCommand command = new SqlCommand("spGetItemByCode");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

				string strGroupCode =	Utils.FillToFiveChars(Utils.ParseOrderCode(strItemNumber).ToString());
				string strBatchCode =	Utils.FillToThreeChars(Utils.ParseBatchCode(strItemNumber).ToString(), Utils.ParseOrderCode(strItemNumber).ToString());
				string strItemCode =	Utils.FillToTwoChars(Utils.ParseItemCode(strItemNumber).ToString());

				command.Parameters.Add(new SqlParameter("@CustomerCode",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BDate",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EDate",DBNull.Value));

				command.Parameters.Add(new SqlParameter("@GroupCode",strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode",strBatchCode));
				command.Parameters.Add(new SqlParameter("@ItemCode",strItemCode));

				command.Parameters.Add("@AuthorId",Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());

				command.Parameters.Add(new SqlParameter("@IsNew", 1));
                
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtItemCheck = new DataTable();

				da.Fill(dtItemCheck);
                //debug
//////				dgDebug1.DataSource = dtItemCheck;
//////				dgDebug1.DataBind();
				////////
				if(dtItemCheck.Rows.Count>0)
				{
					Session["DeliveryLogBatchID"] = dtItemCheck.Rows[0]["NewBatchID"].ToString();

					//Get Proper Batch Number
					string strNewOrderCode = dtItemCheck.Rows[0]["NewOrderCode"].ToString();
					string strNewBatchCode = dtItemCheck.Rows[0]["NewBatchCode"].ToString();

					string strNewBatchNumber = Utils.FullBatchNumber(strNewOrderCode, strNewBatchCode);

					string strBatchNumber = strNewBatchNumber;
//					txtBatchNumber.Text = strBatchNumber;
					lblBatchNumber.Text=strBatchNumber;
//					dgDebug2.DataSource=dtItemCheck;
//					dgDebug2.DataBind();
				}
				else
				{
					Exception up = new Exception("Can't get BatchID from item number.");
					throw up;
				}
			}

		}

		protected void txtItemNumber_TextChanged(object sender, System.EventArgs e)
		{			
			if(Utils.NullValue(Session["DeliveryLogBatchID"])=="")
			{
				/*
				lblInfo.Text="Enter the batch number first";
				txtItemNumber.Text="";
				return;
				*/
				GetBatchFromItemNumber(txtItemNumber.Text.Trim());
			}

			lblInfo.Text="";
			string strItemNumber = txtItemNumber.Text.Trim();
			if(Regex.IsMatch(strItemNumber,@"^\d{10}$|^\d{11}$"))
			{
				//Check If the item Belongs to the Batch
				SqlCommand command = new SqlCommand("spGetItemByCode");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

/*
 *  @CustomerCode dnCode,
 -@BGroupState dnTinyCode,
 -@EGroupState dnTinyCode,
 -@BState dnTinyCode,
 -@EState dnTinyCode,
 -@BDate ddStartDate,
 -@EDate ddStartDate,
 -@GroupCode dnCode,
 -@BatchCode dnBatchCode,
 @ItemCode dnItemCode,
 @AuthorID dnSmallID,
 @AuthorOfficeID dnTinyID,
 @IsNew int = 0
*/
				string strGroupCode =	Utils.FillToFiveChars(Utils.ParseOrderCode(strItemNumber).ToString());
				string strBatchCode =	Utils.FillToThreeChars(Utils.ParseBatchCode(strItemNumber).ToString(), strGroupCode);
				string strItemCode =	Utils.FillToTwoChars(Utils.ParseItemCode(strItemNumber).ToString());

				command.Parameters.Add(new SqlParameter("@CustomerCode",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EGroupState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EState",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@BDate",DBNull.Value));
				command.Parameters.Add(new SqlParameter("@EDate",DBNull.Value));

				command.Parameters.Add(new SqlParameter("@GroupCode",strGroupCode));
				command.Parameters.Add(new SqlParameter("@BatchCode",strBatchCode));
				command.Parameters.Add(new SqlParameter("@ItemCode",strItemCode));

				command.Parameters.Add("@AuthorId",Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());

				command.Parameters.Add(new SqlParameter("@IsNew", 1));
                
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtItemCheck = new DataTable();

				da.Fill(dtItemCheck);

				//debug
//////				dgDebug2.DataSource = dtItemCheck;
//////				dgDebug2.DataBind();
				//////////////////////

				if(dtItemCheck.Rows.Count>0&&int.Parse(Session["DeliveryLogBatchID"].ToString())==int.Parse(dtItemCheck.Rows[0]["NewBatchID"].ToString()))
				{
                    
					string strOldItemNumber = "";
					string strPrevGroupCode = dtItemCheck.Rows[0]["PrevGroupCode"].ToString();
					string strPrevBatchCode = dtItemCheck.Rows[0]["PrevBatchCode"].ToString();
					string strPrevItemCode = dtItemCheck.Rows[0]["PrevItemCode"].ToString();

					strOldItemNumber = Utils.FullItemNumber(strPrevGroupCode, strPrevBatchCode, strPrevItemCode);

					if(Utils.NullValue(Session["DeliveryLogItemList"])!="")
					{
						SortedList slItems = Session["DeliveryLogItemList"] as SortedList;						
						try
						{
							slItems.Add(strOldItemNumber,dtItemCheck.Rows[0]["ItemCode"].ToString());
							Session["DeliveryLogItemList"] =slItems;
						}
						catch(Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
					}
					else
					{
						SortedList slItems = new SortedList();
						slItems.Add(strOldItemNumber,dtItemCheck.Rows[0]["NewItemCode"].ToString()); //changed from ItemCode to NewItemCode.
						Session["DeliveryLogItemList"]=slItems;
					}
				}
				else
				{
					lblInfo.Text="Item Does Not belong to the current batch.";
					txtItemNumber.Text="";
					return;
				}

//				dgDebug.DataSource=dtItemCheck;
//				dgDebug.DataBind();

				//replace with adding from the sl
				//lstItems.Items.Add(strItemNumber);
				PopulateItemsList();
			}
			txtItemNumber.Text="";
		}

		private void PopulateItemsList()
		{
			if(Utils.NullValue(Session["DeliveryLogItemList"])!="")
			{
				lstItems.Items.Clear();
				SortedList slItems = Session["DeliveryLogItemList"] as SortedList;
				foreach(string strItemNumber in slItems.Keys)
				{
					lstItems.Items.Add(strItemNumber);
				}
			}			
		}

		protected void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if(lstItems.SelectedIndex>-1)
			{
				lstItems.Items.RemoveAt(lstItems.SelectedIndex);
			}
		}

		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			if(lstItems.Items.Count>0)
			{
				//Prepare DataTable
				DataTable dtItems = new DataTable("Items");
				dtItems.Columns.Add(new DataColumn("ItemNumber"));
				dtItems.Columns.Add(new DataColumn("ItemCode"));
                
				SortedList sl = Session["DeliveryLogItemList"] as SortedList;
				foreach(ListItem lii in lstItems.Items)
				{
					DataRow dr = dtItems.NewRow();
					dr["ItemNumber"]=lii.Value;
					dr["ItemCode"]=sl[lii.Value].ToString();
					dtItems.Rows.Add(dr);
				}

				dtItems.AcceptChanges();
 
				//Prepare DataSet 
				DataSet dsItems = new DataSet("Items");
				dsItems.Tables.Add(dtItems.Copy());
				dsItems.Tables[0].Columns[0].ColumnMapping = System.Data.MappingType.Attribute;
				dsItems.Tables[0].Columns[1].ColumnMapping = System.Data.MappingType.Attribute;

				SqlCommand command = new SqlCommand("trkSpSetDeliveryLog");
				command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter("@ItemList", dsItems.GetXml()));
				command.Parameters.Add(new SqlParameter("@BatchNumber", lblBatchNumber.Text.Trim()));
				command.Parameters.Add("@AuthorId",Session["ID"].ToString());
				command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());
				command.Parameters.Add(new SqlParameter("@trkEventID", lstOperation.SelectedValue));
				int intDeliveryLogBatchID = int.Parse(Session["DeliveryLogBatchID"].ToString());
				command.Parameters.Add(new SqlParameter("@BatchID", intDeliveryLogBatchID));

				SqlDataAdapter da = new SqlDataAdapter(command);
				DataTable dtResult = new DataTable();
				da.Fill(dtResult);

                /*dgDebug.DataSource = dtResult;
				dgDebug.DataBind();
				*/
				ClearAll();
				lblInfo.Text ="Saved";
				DislpayCurrentBatchDeliveryHistory(intDeliveryLogBatchID);
			}
		}

////		private void txtBatchNumber_TextChanged(object sender, System.EventArgs e)
////		{
////			string strBatchNumber;
////			strBatchNumber = txtBatchNumber.Text.Trim();
////
////			if(Regex.IsMatch(strBatchNumber,@"\d{8,8}$"))
////			{
////				//find the batch number;
////				SqlCommand command = new SqlCommand("spGetBatchByCode");
////				command.CommandType = CommandType.StoredProcedure;
////				command.Connection = new SqlConnection(Session["ConnectionString"].ToString());
////
////				/*
////				 @CustomerCode dnCode,
////				 @BGroupState dnTinyCode,
////				 @EGroupState dnTinyCode,
////				 @BState dnTinyCode,
////				 @EState dnTinyCode,
////				 @BDate ddStartDate,
////				 @EDate ddStartDate,
////				 @GroupCode dnCode,
////				 @BatchCode dnBatchCode,
////				 @AuthorID dnSmallID,
////				 @AuthorOfficeID dnTinyID
////				*/
////
////				command.Parameters.Add("@AuthorId",Session["ID"].ToString());
////				command.Parameters.Add("@AuthorOfficeId", Session["AuthorOfficeID"].ToString());
////				command.Parameters.Add("@CustomerCode", DBNull.Value);
////				command.Parameters.Add("@BGroupState", DBNull.Value);
////				command.Parameters.Add("@EGroupState", DBNull.Value);
////				command.Parameters.Add("@BState", DBNull.Value);
////				command.Parameters.Add("@EState", DBNull.Value);
////				command.Parameters.Add("@BDate", DBNull.Value);
////				command.Parameters.Add("@EDate", DBNull.Value);
////
////				string strGroupCode = strBatchNumber.Substring(0,5);
////				string strBatchCode = strBatchNumber.Substring(5,3);
////
////				command.Parameters.Add("@GroupCode", strGroupCode);
////				command.Parameters.Add("@BatchCode", strBatchCode);
////
////				//		command.Parameters.Add("@ItemCode", DBNull.Value);
////
////				SqlDataAdapter da = new SqlDataAdapter(command);
////				DataTable dtBatch = new DataTable();
////				da.Fill(dtBatch);
////
////				if(dtBatch.Rows.Count>0)
////				{
////					Session["DeliveryLogBatchID"] = dtBatch.Rows[0]["BatchID"].ToString();
////					lblInfo.Text="";
////					DislpayCurrentBatchDeliveryHistory(int.Parse(dtBatch.Rows[0]["BatchID"].ToString()));
////				}
////				else
////				{
////					lblInfo.Text="Batch Number is incorrect.";
////				}			
////
////			}
////			else
////			{
////				lblInfo.Text="Batch Number is incorrect.";
////			}
////		}


		private void DislpayCurrentBatchDeliveryHistory(int intBatchID)
		{
			SqlCommand command = new SqlCommand("trkSpGetDeliveryLog");
			command.Connection = new SqlConnection(Session["MyIP_ConnectionString"].ToString());
			command.CommandType=CommandType.StoredProcedure;

			command.Parameters.Add(new SqlParameter("@BatchID",intBatchID));

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dtRaw = new DataTable();
			da.Fill(dtRaw);

			if(dtRaw.Rows.Count>0)
			{
				DataTable dtDisplay = new DataTable();
				dtDisplay.Columns.Add("Description");
				dtDisplay.Columns.Add("Date/Time");
				dtDisplay.Columns.Add("Item Number");
				dtDisplay.Columns.Add("Batch");
				dtDisplay.AcceptChanges();

				foreach(DataRow dr in dtRaw.Rows)
				{
					DataRow drDisplay = dtDisplay.NewRow();
					drDisplay["Description"]=dr["EventName"].ToString();
					drDisplay["Date/Time"]=dr["RecordTimeStamp"].ToString();
					drDisplay["Item Number"]=dr["FullItemNumber"].ToString();

					string strOutBatchNumber = "";
					string strGroupCode = dr["GroupCode"].ToString();
					string strBatchCode = dr["BatchCode"].ToString();
					strOutBatchNumber = Utils.FullBatchNumber(strGroupCode, strBatchCode);
					drDisplay["Batch"]=strOutBatchNumber;
					dtDisplay.Rows.Add(drDisplay);
				}

				dtDisplay.AcceptChanges();
////				dgCurrentBatchDisplay.DataSource=dtDisplay;
////				dgCurrentBatchDisplay.DataBind();
			}
			else
			{
////				dgCurrentBatchDisplay.DataSource=null;
////				dgCurrentBatchDisplay.DataBind();
			}
		}

		protected void lstOperation_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}		
	}
}
