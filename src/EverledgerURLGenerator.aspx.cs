﻿
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.TreeModel;
using Corpt.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using TreeNode = System.Web.UI.WebControls.TreeNode;
using NPOI.SS.Formula.Functions;

namespace Corpt
{
    public partial class EverledgerURLGenerator : CommonPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Everledger URL Generator";
            if (!Page.IsPostBack)
            {
                ResultGrid.Visible = false;
            }
        }

        #region Search by OrderCode
        protected void OnSearchClick(object sender, ImageClickEventArgs e)
        {
            LoadOrderData();

            ResultGrid.DataSource = null;
            ResultGrid.DataBind();
        }
        private void LoadOrderData()
        {
            //ItemsGrid.DataSource = null;
            //ItemsGrid.DataBind();
            TreeHistory.Nodes.Clear();
            var items = new List<CustomerHistoryModel>();
            if (!string.IsNullOrEmpty(OrderField.Text.Trim()))
            {
                items = QueryItemiznUtils.GetOrderHistory(OrderField.Text, true, this);                
            }

            foreach (CustomerHistoryModel item in items)
            {
                item.Title = item.Title;
            }

            if (items.Count == 0)
            {
                SetSessionState(null, SessionConstants.OrderHistoryList);
                PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", OrderField.Text), true);

            }

            SetSessionState(items, SessionConstants.OrderHistoryList);
            SetSessionState(new UpdateOrderModel { OrderCode = OrderField.Text.Trim() }, SessionConstants.UpdateOrder);

            ShowTreeHistory(items);
        }
        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items)
        {
            TreeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            TreeHistory.Visible = customerHistoryModels.Length > 0;
            btnGenerateURL.Visible = TreeHistory.Visible;
            //UpdateButton.Visible = TreeHistory.Visible;
            ClearButton.Visible = TreeHistory.Visible;
            if (customerHistoryModels.Length == 0) return;
            var data = new List<TreeViewModel>();
            foreach (var item in customerHistoryModels)
            {
                if (item.Title.Contains("Old"))
                {
                    var itemtitle = item.Title.Split('(');
                    item.Title = itemtitle[0] + "(" + itemtitle[2];
                }

                data.Add(
                    new TreeViewModel
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        DisplayName = item.Title
                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = TreeNodeSelectAction.None };

            //PrintTreeUpToLevel(rootNode, 0, 3);

            FillNode(rootNode, root, true, TreeNodeSelectAction.None, 0, 2);

            rootNode.Expand();

            TreeHistory.Nodes.Add(rootNode);

        }

        public static void FillNode(TreeNode node, NodeTreeViewModel root, int currentDepth, int maxDepth)
        {
            FillNode(node, root, true, TreeNodeSelectAction.Select, currentDepth, maxDepth);
        }
        public static void FillNode(TreeNode node, NodeTreeViewModel root, bool expand, TreeNodeSelectAction action, int currentDepth, int maxDepth)
        {
            if (root.Childs.Count <= 0) return;
            if (node == null || currentDepth > maxDepth) return;

            foreach (var child in root.Childs)
            {
                var addNode = new TreeNode(child.DisplayName, child.Id);
                addNode.SelectAction = action;
                FillNode(addNode, child, expand, action, currentDepth + 1, maxDepth);
                node.ChildNodes.Add(addNode);
                node.Expanded = expand;
            }
        }

        #endregion

        #region View State
        private List<CustomerHistoryModel> GetOrderHistoryFromView()
        {
            return GetSessionState(SessionConstants.OrderHistoryList) as List<CustomerHistoryModel> ??
                   new List<CustomerHistoryModel>();
        }
        
        #endregion

        #region Add Items

        protected async void OnGenerateURL(object sender, EventArgs e)
        {
            
            ResultGrid.DataSource = null;
            ResultGrid.DataBind();

            List<EverledgerAPIStatusModel> apiData = new List<EverledgerAPIStatusModel>();
            var batchIds = GetCheckedBatch();
            if (batchIds.Count == 0)
            {
                PopupInfoDialog("No items chosen.", true);
                return;
            }
            //if (batchIds.Count > 1)
            //{
            //    PopupInfoDialog("Please select one batch.", true);
            //    return;
            //}

            var checkedItems = GetCheckedItems();

            ResultGrid.Visible = true;
            foreach (string item in checkedItems)
            {
                string gsiItemNumber = item.Replace(".", "");
                
                EverledgerURLUtils objEverledgerURLUtils = new EverledgerURLUtils();
                EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
                EverledgerAPIStatusModel objAPIStatus = new EverledgerAPIStatusModel();
                EverledgerAPIInputModel objInputAPI = EverledgerURLUtils.GetEverledgerDataByItemNumber(gsiItemNumber, this);

                objInputAPI.ItemNumber = gsiItemNumber;
                //objInputAPI.ItemNumber = gsiItemNumber + "A";
                objAPIStatus.ItemNumber = objInputAPI.ItemNumber;

                if (objInputAPI.Brand == "" || objInputAPI.Brand == "Tiffany")
                {
                    objAPIStatus.ItemNumber = objInputAPI.ItemNumber;
                    objAPIStatus.APIStatus = "Brand missing/invalid ";
                    apiData.Add(objAPIStatus);
                    continue;
                }

                //if (objInputAPI.ManufacturerID == "")
                //{
                //    objAPIStatus.ItemNumber = objInputAPI.ItemNumber;
                //    objAPIStatus.APIStatus = "Manufacturer ID missing";
                //    apiData.Add(objAPIStatus);
                //    continue;
                //}

                if (objInputAPI.ManufacturerDescription == "")
                {
                    objAPIStatus.ItemNumber = objInputAPI.ItemNumber;
                    objAPIStatus.APIStatus = "Manufacturer Description missing";
                    apiData.Add(objAPIStatus);
                    continue;
                }

                if (objInputAPI.EverledgerURL != "")
                {
                    objAPIStatus.ItemNumber = objInputAPI.ItemNumber;
                    objAPIStatus.URL = objInputAPI.EverledgerURL;
                    objAPIStatus.ShortCode = objInputAPI.ShortCode;
                    objAPIStatus.APIStatus = "URL already exists";
                    apiData.Add(objAPIStatus);
                    continue;
                }

                int count = objInputAPI.ManufacturerID.Count(c => c == '/');
                if (count == 0)
                {
                    bool isValidLightPerformanceData = false;
                    int result = 0;

                    if (objInputAPI.LightPerformanceData.ScintillationValue == "" && objInputAPI.LightPerformanceData.FireValue == "" && objInputAPI.LightPerformanceData.BrillianceValue == "")
                    {
                        isValidLightPerformanceData = true;
                    }
                    else
                    {
                        if (int.TryParse(objInputAPI.LightPerformanceData.ScintillationValue, out result) && int.TryParse(objInputAPI.LightPerformanceData.FireValue, out result) && int.TryParse(objInputAPI.LightPerformanceData.BrillianceValue, out result))
                            isValidLightPerformanceData = true;
                        else
                            isValidLightPerformanceData = false;
                    }
                                        
                    if (isValidLightPerformanceData == true)
                    {
                        //Create Item By Item Number
                        objResultAPI = await objEverledgerURLUtils.CreateItemByItemNumber(objInputAPI, this);
                        objAPIStatus.CreateItemStatus = objResultAPI.APIStatus;
                        objAPIStatus.APIStatus = objResultAPI.APIErrorMessage;

                        if (objAPIStatus.CreateItemStatus != "created" && objAPIStatus.CreateItemStatus != "exists")
                        {
                            apiData.Add(objAPIStatus);
                            continue;
                        }

                    }
                    else
                    {
                        objAPIStatus.CreateItemStatus = "Error";
                        objAPIStatus.APIStatus = "In-Valid Light Performance Data";
                        apiData.Add(objAPIStatus);
                        continue;
                    }

                }
                else
                {
                    string[] uidsArray = objInputAPI.ManufacturerID.Split('/');
                    List<string> associatedUIDs = new List<string>(uidsArray);
                    foreach (string manufacturerID in associatedUIDs)
                    {
                        //Create Item By Manufacturer ID
                        objInputAPI.ManufacturerID = manufacturerID;
                        objResultAPI = await objEverledgerURLUtils.CreateItemByManufacturerID(objInputAPI, this);  // repeat per ManufacturerID    
                        objAPIStatus.CreateItemStatus = objResultAPI.APIStatus;
                        objAPIStatus.APIStatus = objResultAPI.APIErrorMessage;

                        if (objAPIStatus.CreateItemStatus != "created" && objAPIStatus.CreateItemStatus != "exists")
                        {
                            break;
                        }
                    }

                    if (objAPIStatus.CreateItemStatus == "created" || objAPIStatus.CreateItemStatus == "exists")
                    {
                        //Associate Diamond To Jewelry
                        objResultAPI = await objEverledgerURLUtils.AssociateDiamondToJewelry(objInputAPI, this);
                        objAPIStatus.AssociateDiamondStatus = objResultAPI.APIStatus;
                        objAPIStatus.APIStatus = objResultAPI.APIErrorMessage;

                        if (objAPIStatus.AssociateDiamondStatus != "success")
                        {
                            apiData.Add(objAPIStatus);
                            continue;
                        }
                    }
                    else
                    {
                        apiData.Add(objAPIStatus);
                        continue;
                    }

                }

                //Get Short Code And URL By Item Number
                objResultAPI = await objEverledgerURLUtils.ShortCodeAndURLByItemNumber(objInputAPI, this);
                objAPIStatus.URLGenStatus = objResultAPI.APIStatus;
                objAPIStatus.APIStatus = objResultAPI.APIErrorMessage;
                                                  
                if (objAPIStatus.URLGenStatus == "created" || objAPIStatus.URLGenStatus == "exists")
                {
                    MeasureValueModel measureModel = new MeasureValueModel();
                    DataTable dt = objEverledgerURLUtils.GetItemPartByFullItem(objInputAPI.ItemNumber, 15, this);
                    DataRow row = dt.Rows[0];
                    measureModel.ItemCode = row["NewItemCode"].ToString();
                    measureModel.BatchId = int.Parse(row["NewBatchID"].ToString());
                    measureModel.PartId = int.Parse(row["PartID"].ToString());
                    measureModel.MeasureValue = null;
                    measureModel.MeasureValueId = null;

                    //For short code for Suffix
                    measureModel.MeasureCode = "122";
                    measureModel.StringValue = objResultAPI.ShortCode;
                    string msgSuffix = objEverledgerURLUtils.SaveMeasureValues(measureModel, this);

                    //For Everledger URL
                    measureModel.MeasureCode = "313";
                    measureModel.StringValue = objResultAPI.URL;
                    string msgURL = objEverledgerURLUtils.SaveMeasureValues(measureModel, this);

                    objAPIStatus.URL = objResultAPI.URL;
                    objAPIStatus.ShortCode = objResultAPI.ShortCode;

                    if (msgSuffix != "" && msgURL != "")
                        objAPIStatus.DBSaveURL = "Saved";
                    else
                    {
                        objAPIStatus.DBSaveURL = "Not Saved";
                    }
                }
                else
                {
                    apiData.Add(objAPIStatus);
                    continue;
                }

                if (objInputAPI.ManufacturerID.Split('/').Length == 1 && objInputAPI.Color != "")
                {
                    //Add Grading Report Information
                    objResultAPI = await objEverledgerURLUtils.AddGradingReportInformation(objInputAPI, this);
                    objAPIStatus.AddGradingStatus = objResultAPI.APIStatus;
                    objAPIStatus.APIStatus = objResultAPI.APIErrorMessage;

                    if (objAPIStatus.AddGradingStatus != "updated")
                    {
                        apiData.Add(objAPIStatus);
                        continue;
                    }
                }

                apiData.Add(objAPIStatus);
            }

            ResultGrid.DataSource = apiData;
            ResultGrid.DataBind();

        }

        private List<string> GetCheckedBatch()
        {
            var items = GetOrderHistoryFromView();
            var batchIds = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                var item = items.Find(m => m.Id == node.Value);
                if (item == null) continue;
                if (batchIds.Contains(item.BatchId)) continue;
                batchIds.Add(item.BatchId);
            }
            return batchIds;
        }
        private IEnumerable<string> GetCheckedItems()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                if (node.Text.Contains("Old #"))
                {
                    checkItems.Add(node.Text.Split('#')[1].Replace(")", "").Replace(" ", ""));
                }
                else
                {
                    checkItems.Add(node.Value.Split('_')[1]); // 1283_61887.001.01 -> 61887.001.01
                }
            }
            return checkItems;
        }

        #endregion


        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion


        #region Clear ItemsGrid
        protected void OnClearClick(object sender, EventArgs e)
        {
            TreeHistory.Nodes.Clear();
            OrderField.Text = string.Empty;

            ResultGrid.DataSource = null;
            ResultGrid.DataBind();

            ResultGrid.Visible = false;
        }

        #endregion
    }
}