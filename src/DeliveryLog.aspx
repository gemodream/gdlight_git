<%@ Page language="c#" Codebehind="DeliveryLog.aspx.cs" AutoEventWireup="True" Inherits="Corpt.DeliveryLog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeliveryLog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body onload="document.Form1.txtItemNumber.focus();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:button style="Z-INDEX: 119; POSITION: absolute; TOP: 176px; LEFT: 408px" id="cmdAdd" runat="server"
				CssClass="buttonStyle" Width="80px" Text="Add"></asp:button>
			<asp:HyperLink style="Z-INDEX: 116; POSITION: absolute; TOP: 88px; LEFT: 376px" id="HyperLink3"
				runat="server" NavigateUrl="DeliveryLogLookup.aspx">Items Outside</asp:HyperLink><asp:label style="Z-INDEX: 113; POSITION: absolute; TOP: 176px; LEFT: 96px" id="lblBatchNumber"
				runat="server" CssClass="text" Font-Size="Larger">Batch Number</asp:label><asp:hyperlink style="Z-INDEX: 111; POSITION: absolute; TOP: 560px; LEFT: 432px" id="HyperLink1"
				runat="server" CssClass="text" NavigateUrl="Middle.aspx">Back</asp:hyperlink>
			<asp:button style="Z-INDEX: 108; POSITION: absolute; TOP: 208px; LEFT: 408px" id="cmdDelete"
				runat="server" CssClass="buttonStyle" Text="Delete" Width="80px" onclick="cmdDelete_Click"></asp:button>
			<asp:button style="Z-INDEX: 107; POSITION: absolute; TOP: 528px; LEFT: 432px" id="cmdClear"
				runat="server" CssClass="buttonStyle" Text="Clear" Width="80px" onclick="cmdClear_Click"></asp:button><asp:listbox style="Z-INDEX: 106; POSITION: absolute; TOP: 208px; LEFT: 248px" id="lstItems"
				runat="server" CssClass="text" Width="152px" Height="320px"></asp:listbox><asp:dropdownlist style="Z-INDEX: 105; POSITION: absolute; TOP: 56px; LEFT: 88px" id="lstOperation"
				runat="server" CssClass="inputStyleCC5" AutoPostBack="True" onselectedindexchanged="lstOperation_SelectedIndexChanged">
				<asp:ListItem Value="14">Sent to Avi Polish</asp:ListItem>
				<asp:ListItem Value="15">Received from Avi Polish</asp:ListItem>
				<asp:ListItem Value="16">Sent to Photoscribe Engraving</asp:ListItem>
				<asp:ListItem Value="17">Received from Photoscribe Engraving</asp:ListItem>
			</asp:dropdownlist>
			<asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 600px; LEFT: 104px" id="lblLog" runat="server"
				CssClass="text"></asp:label><asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 152px; LEFT: 248px" id="Label2" runat="server"
				CssClass="text">Items for work
				</asp:label>
			<asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 152px; LEFT: 88px" id="lblDebugMessage"
				runat="server" CssClass="text">Batch Number</asp:label>
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 496px; LEFT: 432px" id="cmdSave" runat="server"
				CssClass="buttonStyle" Text="Save" Width="80px" onclick="cmdSave_Click"></asp:button>
			<asp:textbox style="Z-INDEX: 100; POSITION: absolute; TOP: 176px; LEFT: 248px" id="txtItemNumber"
				runat="server" CssClass="inputStyleCC" AutoPostBack="True" name="txtItemNumber" ontextchanged="txtItemNumber_TextChanged"></asp:textbox>
			<asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 112px; LEFT: 88px" id="lblInfo" runat="server"
				CssClass="text"></asp:label>
			<asp:HyperLink style="Z-INDEX: 114; POSITION: absolute; TOP: 56px; LEFT: 376px" id="HyperLink2"
				runat="server" NavigateUrl="DeliveryLogLookup.aspx">Lookup</asp:HyperLink>
		</form>
	</body>
</HTML>
