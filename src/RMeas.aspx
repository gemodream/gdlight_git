﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="RMeas.aspx.cs" Inherits="Corpt.RMeas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        body
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 75%;
        }
        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }
    </style>
    
    <script type="text/javascript">
    $(window).resize(function () {
        showHeight();
    });
    $(document).ready(function () {
        showHeight();
    });
    function showHeight() {

        var h = parseInt(window.innerHeight) - 230;
        var div1 = document.getElementById('<%= MeasureDiv.ClientID %>');
        div1.style.height = h + 'px';

    };
    </script>
    <div class="demoarea">
        <div class="demoheading">
            AutoMeasure</div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel">
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="LoadOrderBtn" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <div style="font-size: small; color: black">
                    <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadOrderBtn">
                        <asp:Label ID="Label2" runat="server" Text="Batch number:" />
                        <asp:TextBox runat="server" ID="BatchNumberFld" MaxLength="9" />
                        <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                            ValidChars="0123456789-" TargetControlID="BatchNumberFld" />
                        <asp:ImageButton runat="server" ID="LoadOrderBtn" OnClick="OnLoadOrderClick" ImageUrl="~/Images/ajaxImages/search.png"
                            ToolTip="Load Order data" />
                        <asp:Button ID="ClearButton" runat="server" Text="Clear" CssClass="btn btn-info"
                            Style="margin-left: 30px" OnClick="OnClearClick" />
                        <asp:CheckBox ID="AddBatchBox" runat="server" Text="Add Another Batch" CssClass="btn btn-info"
                            Style="margin-left: 30px" AutoPostBack="true"/>
                        <asp:Label ID="SaveMsgLabel" runat="server" Style="font-size: small; color: Red"></asp:Label>
                        <asp:CheckBox ID="MultiParts" runat="server" Text="MultiParts" CssClass="btn btn-info"
                            Style="margin-left: 30px" OnCheckedChanged="MultiParts_CheckedChanged" AutoPostBack="true"/>
                        <asp:HiddenField ID="HdnItemTypeID" runat="server" />
                    </asp:Panel>
                </div>
                <asp:Panel runat="server" ID="InfoPanel" Visible="false">
                    <table style="color: Black">
                        <tr>
                            <td style="vertical-align: top; font-size: smaller">
                                <label>
                                    Batches with the same structure as the batch above</label>
                            </td>
                            <td style="text-align: center">
                                <label>
                                    Measures</label>
                            </td>
                            <td>
                            </td>
                            <td style="text-align: center">
                                <label>
                                    Сalculation rules</label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; height: 100%; width: 250px; border: silver solid thin">
                                <asp:TreeView ID="tvwItems" runat="server" ShowCheckBoxes="All" ExpandDepth="1" AfterClientCheck="CheckChildNodes();"
                                    PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeNodeCheckChanged"
                                    onclick="OnTreeClick(event)">
                                    <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                    <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                    <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                </asp:TreeView>
                            </td>
                            <td style="vertical-align: top; padding: 5px">
                                <asp:Button runat="server" ID="ImageButton2" OnClick="DeleteCheckedItemsClick" CssClass="btn btn-info"
                                    ToolTip="Delete checked items" Text="DELETE" />
                            </td>
                            <td style="padding-left: 20px; margin-left: 10px; vertical-align: top; color: black;
                                border: silver solid thin">
                                <asp:Panel ID="SelectMeasurePanel" runat="server">
                                    <label>
                                        Part:</label>
                                    <asp:DropDownList runat="server" ID="PartsListField" OnSelectedIndexChanged="OnPartChanged"
                                        AutoPostBack="True" DataTextField="PartName" DataValueField="PartId" Font-Size="Small"
                                        ForeColor="Black" />
                                    <div style="overflow-y: auto; overflow-x: hidden; height: 500px; width: 300px; color: black" id="MeasureDiv" runat="server" >
                                        <asp:DataGrid ID="grdMeasure" runat="server" AllowSorting="False" CssClass="table table-condensed"
                                            DataKeyField="MeasureId" AutoGenerateColumns="False" ShowHeader="False">
                                            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White"
                                                Font-Size="small" />
                                            <ItemStyle ForeColor="#0D0D0D" BackColor="White" BorderColor="silver" Font-Size="Small">
                                            </ItemStyle>
                                            <Columns>
                                                <asp:TemplateColumn>
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Text='' ID="Checkbox1"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="MeasureName" HeaderText="Measure" />
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td style="vertical-align: top; padding: 5px">
                                <asp:ImageButton runat="server" ID="ImageButton1" OnClick="OnAddMeasureImgClick"
                                    ImageUrl="~/Images/ajaxImages/rightArrow36.png" ToolTip="Add selected measures to Rule table" />
                            </td>
                            <td style="vertical-align: top; width: 550px">
                                <asp:DataGrid runat="server" ID="RulesGrid" AutoGenerateColumns="False" CssClass="table table-condensed"
                                    DataKeyField="UniqueKey" OnItemDataBound="OnRuleItemDataBound">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="RuleResetBtn" runat="server" Text="Del" OnClick="OnRuleResetBtnClick"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="PartName" HeaderText="Part"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MeasureName" HeaderText="Measure"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                From</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Panel runat="server" ID="RuleMinValuePanel" CssClass="form-inline">
                                                    <asp:TextBox runat="server" ID="RuleMinNumericFld" Width="90%"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMinExtender" runat="server" FilterType="Custom"
                                                        ValidChars="0123456789." TargetControlID="RuleMinNumericFld" />
                                                    <asp:DropDownList runat="server" ID="RuleMinEnumFld" DataTextField="MeasureValueName"
                                                        DataValueField="MeasureValueId" Width="99%" />
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                To</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Panel runat="server" ID="RuleMaxValuePanel" CssClass="form-inline">
                                                    <asp:TextBox runat="server" ID="RuleMaxNumericFld" Width="90%"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="RuleMaxExtender" runat="server" FilterType="Custom"
                                                        ValidChars="0123456789." TargetControlID="RuleMaxNumericFld" />
                                                    <asp:DropDownList runat="server" ID="RuleMaxEnumFld" DataTextField="MeasureValueName"
                                                        DataValueField="MeasureValueId" Width="99%" />
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White"
                                        Font-Size="Small" />
                                    <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                </asp:DataGrid>
                                <asp:ListBox ID="MultiPartsList" runat="server" AutoPostBack="True" Rows="10" Visible="false" Enabled="false"></asp:ListBox>
                            </td>
                            <td style="vertical-align: top; padding: 5px">
                                <asp:Button runat="server" ID="SaveEnumButton" Text="Save" OnClick="OnSaveMeasureClick"
                                    Style="margin-left: 10px" CssClass="btn btn-info" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!-- Information Dialog  -->
                <asp:Panel runat="server" ID="InfoPanelDlg" CssClass="modalPopup" Style="display: none;
                    width: 310px; border: solid 2px #5390D5">
                    <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #E9EDF0;
                        border: solid 1px Silver; color: black; text-align: left">
                        <div>
                            <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                            <asp:Label runat="server" ID="InfoTitle"></asp:Label>
                        </div>
                    </asp:Panel>
                    <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px;
                        color: black; text-align: center" id="MessageDiv" runat="server">
                        Test
                    </div>
                    <div style="padding-top: 10px">
                        <p style="text-align: center; font-family: sans-serif">
                            <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                    PopupControlID="InfoPanelDlg" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                    OkControlID="InfoCloseButton">
                </ajaxToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
