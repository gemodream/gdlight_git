﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningUtilities.aspx.cs" Inherits="Corpt.ScreeningUtilities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%-- IvanB start --%>
<asp:Content ContentPlaceHolderID="PageHead" ID="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet" />
</asp:Content>
<%-- IvanB end --%>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

            input[type="checkbox"], label {
                margin-top: 0;
                margin-bottom: 0;
                line-height: normal;
            }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }

        /*#master_content {
            padding-top: 20px;
        }*/

        /* MultiView Tab Using Menu Control */

        .tabs {
            position: relative;
            top: 1px;
            z-index: 2;
        }

        .tab {
            border: 1px solid black;
            padding: 2px 10px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }

        .selectedtab {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            text-decoration: none;
        }

        .tabcontents {
            border: 1px solid black;
            padding: 10px;
            width: 760px;
            height: auto;
            background-color: white;
        }

        .filedSetBorder {
            border: 1px groove black !important;
            padding: 10px 10px 10px 10px;
            border-radius: 8px;
            margin: 0px 5px 5px 5px;
            float: left;
            vertical-align: top;
        }

            .filedSetBorder legend {
                padding-left: 7px;
                padding-right: 7px;
                margin-bottom: 0px;
                border-bottom: 0px !important;
                font-family: Tahoma,Arial,sans-serif;
                width: max-content;
            }

        /*.ajax__tab_xp .ajax__tab_body {
            font-size: font-size: 75%;
            font-family: Tahoma,Arial,sans-serif;
        }*/

        /*IvanB start*/
        .ErrorText {
            color: red;
        }

        .SuccessText {
            color: green;
        }
        /*IvanB end*/
        .auto-style1 {
            height: 23px;
        }

        .select2-dropdown.increasedzindexclass {
          z-index: 999999;
        }
        .ajax__tab_default .ajax__tab_header{
            white-space:normal !important;
        }
        .ajax__tab_default .ajax__tab_outer{
            display:-moz-inline-box !important;
            display:inline-block !important;
        }

        .ajax__tab_default .ajax__tab_inner{
            display:-moz-inline-box !important;
            display:inline-block !important;
        }
        .ajax__tab_default .ajax__tab_tab{
            overflow:hidden !important;
            text-align:center !important;
            cursor:pointer !important;
            display:-moz-inline-box !important;
            display:inline-block !important;            
        }
        .ajax__tab_xp .ajax__tab_header .ajax__tab_tab 
        {
            width: 120px !important;
            height:25px !important;            
        }

        .tab_fieldset {
            border: 1px groove #ddd !important; 
            padding: 0 10px 10px 10px; 
            border-radius: 8px; 
            text-align: left; 
            margin-top:20px;
        }
    </style>



    <script type="text/javascript">
               
        function pageLoad() {
            $( "#<%= SelectorListBox.ClientID %>").select2({});
            /*IvanB 12/09 start*/
            $("#<%= ddlMeasures.ClientID %>").select2({});
            /*IvanB 12/09  end*/
            $("#<%= lstSkus.ClientID %>").select2({});
            $("#<%= ddlCutomer.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });
            $("#<%= ddlBNCustomer.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });
            $("#<%= ddlCustomerforContractor.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });
            $("#<%= ddlContractorCustomer.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $("#<%= ddlRetailerVendors.ClientID %>").select2({});
            $("#<%= ddlProgram.ClientID %>").select2({});
            $("#<%= ddlRetailerList.ClientID %>").select2({});
            $("#<%= ddlBrand.ClientID %>").select2({});
            $("#<%= ddlNature.ClientID %>").select2({});   
            
            $("#<%= ddlAPVendor.ClientID %>").select2({
                dropdownCssClass: "increasedzindexclass",
            });

            $("#<%= ddlAPProgram.ClientID %>").select2({
                dropdownCssClass: "increasedzindexclass",
            });
            $("#<%= ddlABBrand.ClientID %>").select2({
                dropdownCssClass: "increasedzindexclass",
            });
            $("#<%= ddlABRetailer.ClientID %>").select2({
                dropdownCssClass: "increasedzindexclass",
            });
            
            

            $('#<%=filterTextbox.ClientID %>').on('input', function () {
              var filterText = $(this).val().toLowerCase();
              $('#<%=lstCustomerList.ClientID %> option').each(function() {
                if ($(this).text().toLowerCase().indexOf(filterText) === -1) {
                  $(this).hide();
                }else
                {
                $(this).show();
                }
              });
            });
       
            
        }

        function actualNamesListClick() {
            var selVal = $("#<%= ActualNamesList.ClientID %>").val();
            var isLabel = selVal.split('_')[1];
            $("#<%= RollBackBtn.ClientID %>").prop("disabled", isLabel === "0");
        }

        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=chklstContractorServiceType.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }

       


    </script>

    <div class="demoarea">
        <div style="font-size: smaller; height: 25px;">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="demoheading">Utilities</div>

        <div style="margin-left: 10px;" class="container-fluid">
            <div class="row">

                <asp:UpdatePanel runat="server" ID="EnteredValuesPanel">
                    <ContentTemplate>
                        <ajaxToolkit:TabContainer ID="TabContainer2" runat="server" Width="1100px" CombineScripts="false" AutoPostBack="True" OnActiveTabChanged="OnActiveTab_Changed">
                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Vendor's Retailer">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Vendor's Retailer</legend>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtCustomerName" style="float: left; margin-top: 2px; margin-bottom: 0px;">
                                                        Vendor Name:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCutomer" runat="server" DataTextField="CustomerName"
                                                        AutoPostBack="True" DataValueField="CustomerCode" CssClass="text-style" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                                        ToolTip="Vendors List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Text="*" ErrorMessage="Please Select Customer" ValidationGroup="vgRetailerToCustomer" ControlToValidate="ddlCutomer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtCustomerCode1" class="form-control form-control-height text-style" Style="width: 90px; margin-top: 4px; margin-left: 0px;" runat="server" OnClick="OnCustCodeClick" OnTextChanged="OnCustCodeClick"
                                                        placeholder="CustomerCode" autocomplete="off" AutoPostBack="True" Visible="False"></asp:TextBox>

                                                    <asp:Button ID="CustomerLikeButton" runat="server" Text="Load" OnClick="OnCustCodeClick" CssClass="btn btn-info btn-large" />
                                                    <asp:Button ID="AddRetailerToCustomer" runat="server" class="btn btn-info btn-large" ValidationGroup="vgRetailerToCustomer" Style="width: auto; vertical-align: middle; margin-left: 0px; margin-top: 0px;"
                                                        Text="Save" OnClick="AddRetailerToCustomer_Click" />
                                                    <asp:Button ID="btnClearAll" runat="server" class="btn btn-info btn-large" Text="Clear" OnClick="btnRetailerClear_Click" Style="margin-left: 0px; margin-top: 0px;"
                                                        CausesValidation="False" />
                                                </td>
                                                <td rowspan="4" style="vertical-align: top; padding-left: 10px; width: 250px;">
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vgRetailerToCustomer" HeaderText="You must enter a value in the following fields:" />


                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtRetailer" style="float: left; margin-top: 4px;">
                                                        Select Retailer:</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRetailer" runat="server" Enabled="False"></asp:TextBox>
                                                    <asp:Button ID="btnRetailer" runat="server" Text="Retailer" CssClass="btn btn-info "
                                                        Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" />
                                                    <asp:HiddenField ID="hdnRetailerCode" runat="server" Value="0" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="vgRetailerToCustomer" runat="server" Text="*" ErrorMessage="Please Select Retailer" ControlToValidate="txtRetailer" Display="Dynamic"></asp:RequiredFieldValidator>


                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Blue" Style="margin-left: 10px;"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td style="padding-left: 0px; vertical-align: top; font-size: 14px;" colspan="3">
                                                    <br />
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="EnteredLabel" Text="Vendors Retailer:" Visible="False"></asp:Label>


                                                                <asp:GridView runat="server" ID="gvCustomersRetailer" AutoGenerateColumns="False" DataKeyNames="RetailerID" CellPadding="4"
                                                                    ShowHeaderWhenEmpty="True" EmptyDataText="No records found." ForeColor="#333333" OnDataBound="gvCustomersRetailer_OnRowDataBound">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate></HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" CssClass="btn btn-info" OnClick="DeleteRetailer"
                                                                                    OnClientClick="return confirm('Are you sure you want to delete this Retailer?');"
                                                                                    Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;">Delete </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Retailer Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkRetailerName" runat="server" Text='<%#Eval("RetailerName")%>' CausesValidation="false"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hdnRetailerID" runat="server" Value='<%#Eval("RetailerID")%>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ServiceType Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkServiceTypeName" runat="server" Text='<%#Eval("ServiceTypeName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Category Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkCategoryName" runat="server" Text='<%#Eval("CategoryName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ServiceTime Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkServiceTimeName" runat="server" Text='<%#Eval("ServiceTimeName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <EmptyDataRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="False" ForeColor="White" Font-Size="12px" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />

                                                                </asp:GridView>
                                                            </td>


                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 7px;" colspan="2">
                                                    <div class="form-group" style="margin-bottom: 2px;">
                                                    </div>
                                                </td>

                                                <td style="padding-right: 7px;">
                                                    <div class="form-group" style="margin-bottom: 2px;">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px; display: none; border: solid 2px lightgray;">
                                            <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                                <div>
                                                    <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                                    <b>Information</b>

                                                </div>
                                            </asp:Panel>
                                            <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="MessageDiv" runat="server">
                                            </div>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center; font-family: sans-serif">
                                                    <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                                        <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" PopupControlID="InfoPanel" ID="InfoPopupExtender"
                                            PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" DynamicServicePath="" Enabled="True">
                                        </ajaxToolkit:ModalPopupExtender>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel9" runat="server" HeaderText="Retailer">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Retailer Detail</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;" colspan="1">
                                                    <div>
                                                        <asp:Label ID="Label1" runat="server" Text="Retailer Name"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtRetailerName" runat="server" Width="300px" placeholder="Enter Retailer Name"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnRetailerId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtRetailerName" ValidationGroup="vgRetailer"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text="Service Type" Width="100px"></asp:Label>
                                                    <asp:Panel ID="Panel1" runat="server" Width="300px" Height="110px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="chklstServiceType" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>

                                                </td>
                                                <td style="vertical-align: bottom; text-align: right; width: 170px;">
                                                    <asp:Button ID="btnAdd" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAdd_Click" ValidationGroup="vgRetailer" />
                                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClear_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblRetailerMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>

                                        </table>
                                        <table>
                                            <tr>

                                                <td>
                                                    <asp:GridView ID="gvRetailer" runat="server" DataKeyNames="RetailerID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingRetailer" Width="500px" CellPadding="4" ForeColor="#333333"
                                                        OnRowDataBound="gvRetailer_RowDataBound">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditRetailer" CssClass="btn btn-info"
                                                                        Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>

                                                                <ItemStyle Width="50px" />

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Retailer Name" SortExpression="RetailerName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkRetailerName" runat="server" Text='<%#Eval("RetailerName")%>' CausesValidation="false" OnClick="EditRetailer"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnRetailerID" runat="server" Value='<%#Eval("RetailerID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Service Type" SortExpression="ServiceTypeNames">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkServiceType" runat="server" Text='<%#Eval("ServiceTypeNames")%>' CausesValidation="false" OnClick="EditRetailer"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnServiceTypeCodeSet" runat="server" Value='<%#Eval("ServiceTypeCodeSet")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Contractor">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Contractor Detail</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;" colspan="1">
                                                    <div>
                                                        <asp:Label ID="Label23" runat="server" Text="Contractor Name"></asp:Label>
                                                    </div>
                                                    <asp:DropDownList ID="ddlContractorCustomer" runat="server" DataTextField="CustomerName"
                                                        AutoPostBack="false" DataValueField="CustomerCode" CssClass="text-style" OnSelectedIndexChanged="OnCustomersContractorSelectedChanged"
                                                        ToolTip="Vendors List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                    <%--<asp:TextBox ID="txtContractorName" runat="server" Width="300px" placeholder="Enter Contractor Name"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnContractorId" runat="server" Value="0"></asp:HiddenField>--%>
                                                    <asp:HiddenField ID="hdnContractorId" runat="server" Value="0" />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Required" InitialValue="" ControlToValidate="ddlContractorCustomer" ValidationGroup="vgContractor"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    <asp:Label ID="Label24" runat="server" Text="Service Type" Width="100px"></asp:Label>
                                                    <asp:Panel ID="Panel6" runat="server" Width="300px" Height="110px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="chklstContractorServiceType" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>

                                                </td>
                                                <td style="vertical-align: bottom; text-align: right; width: 170px;">
                                                    <div style="vertical-align: top;">
                                                        <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Required"
                                                            ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" ValidationGroup="vgContractor" Style="float: left;" />
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="btnAddContractor" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnContractorAdd_Click" ValidationGroup="vgContractor" />
                                                    <asp:Button ID="btnClearContractor" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnContractorsClear_Click" CausesValidation="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblContractorsMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>

                                        </table>
                                        <table>
                                            <tr>

                                                <td>
                                                    <asp:GridView ID="gvContractor" runat="server" DataKeyNames="ContractorID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingContractor" Width="500px" CellPadding="4" ForeColor="#333333"
                                                        OnRowDataBound="gvContractor_RowDataBound">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditContractor" CssClass="btn btn-info"
                                                                        Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>

                                                                <ItemStyle Width="50px" />

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Contractor Name" SortExpression="ContractorName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkContractorName" runat="server" Text='<%#Eval("ContractorName")%>' CausesValidation="false" OnClick="EditContractor"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnContractorID" runat="server" Value='<%#Eval("ContractorID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Service Type" SortExpression="ServiceTypeNames">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkServiceType" runat="server" Text='<%#Eval("ServiceTypeNames")%>' CausesValidation="false" OnClick="EditContractor"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnServiceTypeCodeSet" runat="server" Value='<%#Eval("ServiceTypeCodeSet")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="Service Type">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Service Type</legend>
                                        <br />
                                        <table style="width: 500px;">
                                            <tr>
                                                <td colspan="1">
                                                    <asp:Label ID="Label3" runat="server" Text="Service Type"></asp:Label><br />
                                                    <asp:TextBox ID="txtServiceType" runat="server" placeholder="Enter Service Type Name"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnServiceTypeId" runat="server" Value="0"></asp:HiddenField>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtServiceType" ValidationGroup="vgServiceType"></asp:RequiredFieldValidator>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="width: 325px;">
                                                    <asp:Label ID="Label5" runat="server" Text="Category"></asp:Label>
                                                    <asp:Panel ID="Panel2" runat="server" Width="300px" Height="110px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="chklstCategory" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                                <td style="vertical-align: bottom; text-align: right; width: 170px;">
                                                    <asp:Button ID="btnAddServiceType" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddServiceType_Click" ValidationGroup="vgServiceType" />
                                                    <asp:Button ID="btnClearServiceType" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearServiceType_Click" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblServiceTypeMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvServiceType" runat="server" DataKeyNames="ServiceTypeID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingServiceType" Width="500px" CellPadding="4" ForeColor="#333333" OnRowDataBound="gvServiceType_RowDataBound">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditServiceType" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ServiceType Name" SortExpression="ServiceTypeName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkServiceTypeName" runat="server" Text='<%#Eval("ServiceTypeName")%>' CausesValidation="false" OnClick="EditServiceType"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnServiceTypeID" runat="server" Value='<%#Eval("ServiceTypeID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category Names " SortExpression="CategoryNames">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCategoryNames" runat="server" Text='<%#Eval("CategoryNames")%>' CausesValidation="false" OnClick="EditServiceType"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnCategoryCodeSet" runat="server" Value='<%#Eval("CategoryCodeSet")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel11" runat="server" HeaderText="Category">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Category</legend>
                                        <table border="0" style="width: 500px;">

                                            <tr>
                                                <td colspan="1">
                                                    <asp:Label ID="Label4" runat="server" Text="Category Name"></asp:Label>
                                                    <asp:TextBox ID="txtCategoryName" runat="server" Width="250px" placeholder="Enter Category Name"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnCategoryId" runat="server" Value="0"></asp:HiddenField>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtCategoryName" ValidationGroup="vgCategory"></asp:RequiredFieldValidator>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td style="width: 325px;">
                                                    <asp:Label ID="Label13" runat="server" Text="Service Time"></asp:Label>
                                                    <asp:Panel ID="Panel3" runat="server" Width="300px" Height="110px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="chklstServiceTime" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                                <td style="vertical-align: bottom; text-align: right; width: 170px;">
                                                    <asp:Button ID="btnAddCategory" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddCategory_Click" ValidationGroup="vgCategory" />
                                                    <asp:Button ID="btnClearCategory" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearCategory_Click" />

                                                </td>
                                            </tr>


                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblCategoryMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvCategory" runat="server" DataKeyNames="CategoryId" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingCategory" Width="500px" CellPadding="4" ForeColor="#333333" OnRowDataBound="gvCategory_RowDataBound">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditCategory" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category Name" SortExpression="CategoryName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCategoryName" runat="server" Text='<%#Eval("CategoryName")%>' CausesValidation="false" OnClick="EditCategory"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ServiceTime Names" SortExpression="ServiceTimeNames">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkServiceTimeNames" runat="server" Text='<%#Eval("ServiceTimeNames")%>' CausesValidation="false" OnClick="EditCategory"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnServiceTimeCodeSet" runat="server" Value='<%#Eval("ServiceTimeCodeSet")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel12" runat="server" HeaderText="ServiceTime">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Service Time</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label6" runat="server" Text="Service Time"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtServiceTimeName" runat="server" Width="250px" placeholder="Enter Service Time"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnServiceTimeId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtServiceTimeName" ValidationGroup="vgServiceTime"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddServiceTime" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddServiceTime_Click" ValidationGroup="vgServiceTime" />
                                                    <asp:Button ID="btnClearServiceTime" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearServiceTime_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblServiceTimeMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvServiceTime" runat="server" DataKeyNames="ServiceTimeId" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingServiceTime" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditServiceTime" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ServiceTime Name" SortExpression="ServiceTimeName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkServiceTimeName" runat="server" Text='<%#Eval("ServiceTimeName")%>' CausesValidation="false" OnClick="EditServiceTime"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnServiceTimeID" runat="server" Value='<%#Eval("ServiceTimeId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Jewelry Type">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Jewelry Type</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label11" runat="server" Text="Jewelry Type"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtJewelryTypeName" runat="server" Width="250px" placeholder="Enter Jewelry Type"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnJewelryTypeId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtJewelryTypeName" ValidationGroup="vgJewelryType"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddJewelryType" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddJewelryType_Click" ValidationGroup="vgJewelryType" />
                                                    <asp:Button ID="btnClearJewelryType" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearJewelryType_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblJewelryTypeMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvJewelryType" runat="server" DataKeyNames="JewelryTypeID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingJewelryType" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditJewelryType" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="JewelryType Name" SortExpression="JewelryTypeName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkJewelryTypeName" runat="server" Text='<%#Eval("JewelryTypeName")%>' CausesValidation="false" OnClick="EditJewelryType"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnJewelryTypeID" runat="server" Value='<%#Eval("JewelryTypeID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel15" runat="server" HeaderText="Screening Device">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Screening Instrument</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label9" runat="server" Text="Screening Instrument"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtScreeningInstrumentName" runat="server" Width="250px" placeholder="Enter Screening Instrument"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnScreeningInstrumentId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtScreeningInstrumentName" ValidationGroup="vgScreeningInstrument"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddScreeningInstrument" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddScreeningInstrument_Click" ValidationGroup="vgScreeningInstrument" />
                                                    <asp:Button ID="btnClearScreeningInstrument" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearScreeningInstrument_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblScreeningInstrumentMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvScreeningInstrument" runat="server" DataKeyNames="InstrumentID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingInstrumentName" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false" OnClick="EditScreeningInstrument" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ScreeningInstrument Name" SortExpression="InstrumentName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkInstrumentName" runat="server" Text='<%#Eval("InstrumentName")%>' CausesValidation="false" OnClick="EditScreeningInstrument"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnInstrumentID" runat="server" Value='<%#Eval("InstrumentID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel16" runat="server" HeaderText="CertifiedBy">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Certified By</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label10" runat="server" Text="Certified By"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtCertifiedByName" runat="server" Width="250px" placeholder="Enter CertifiedBy"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnCertifiedById" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtCertifiedByName" ValidationGroup="vgCertifiedBy"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddCertifiedBy" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddCertifiedBy_Click" ValidationGroup="vgCertifiedBy" />
                                                    <asp:Button ID="btnClearCertifiedBy" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearCertifiedBy_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblCertifiedByMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvCertifiedBy" runat="server" DataKeyNames="CertifiedID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingCertifiedBy" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditCertifiedBy" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CertifiedBy" SortExpression="CertifiedBy">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCertifiedBy" runat="server" Text='<%#Eval("CertifiedBy")%>' CausesValidation="false" OnClick="EditCertifiedBy"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnCertifiedID" runat="server" Value='<%#Eval("CertifiedID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel20" runat="server" HeaderText="Block Lists Editor">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Block Lists Editor</legend>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td colspan="3">
                                                    <asp:Label ID="Label20" runat="server" Text="Block List For:"></asp:Label><br>
                                                    <asp:Panel runat="server" ID="SelectorDropDawnPanel">
                                                        <asp:ListBox runat="server" ID="SelectorListBox" AutoPostBack="True"
                                                            OnSelectedIndexChanged="OnSelectorSelectedChanged" Rows="5" Width="220px"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel runat="server" ID="ActualListForEdit">
                                                        <asp:Label ID="Actual" runat="server" Text="Actual List" CssClass="label"></asp:Label><br>
                                                        <asp:ListBox runat="server" ID="ActualList" Rows="20" Width="385px" SelectionMode="Multiple"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <div style="padding-top: 30px;">
                                                        <asp:ImageButton ID="MoveToBlockBtn" runat="server" ToolTip="Move to Block List"
                                                            ImageUrl="~/Images/ajaxImages/rightArrowBl.png" Style="width: 30px; margin-right: 50px" OnClick="OnMoveToBlockClick" />
                                                    </div>
                                                    <div style="padding-top: 120px;">
                                                        <asp:ImageButton ID="MoveToActualBtn" runat="server" ToolTip="Move to Actual List"
                                                            ImageUrl="~/Images/ajaxImages/leftArrowBl.png" Style="width: 30px; margin-right: 50px" OnClick="OnMoveToActualClick" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="BlockListForEdit">
                                                        <asp:Label ID="Block" runat="server" Text="Block List" CssClass="label"></asp:Label><br>
                                                        <asp:ListBox runat="server" ID="BlockList" Rows="20" Width="385px" SelectionMode="Multiple"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="DocumentMergePanel" runat="server" HeaderText="Document Merge">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Document Merge
                                        </legend>
                                        <div style="min-height: 50px; max-width: 920px; margin: 10px;">
                                            <asp:Label ID="ErrorSuccess" runat="server" Visibility="false"></asp:Label>
                                        </div>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Panel runat="server" ID="DropDownNamesPanel">
                                                        <asp:Label ID="ListForEditing" runat="server" Text="DropDown Names List" CssClass="label"></asp:Label><br>
                                                        <asp:ListBox runat="server" ID="DropDownNamesList" Rows="20" Width="385px" SelectionMode="Multiple"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <div style="padding-top: 120px;">
                                                        <asp:ImageButton ID="ReplaceBtn" runat="server" ToolTip="Replace name"
                                                            ImageUrl="~/Images/ajaxImages/leftArrowBl.png" Style="width: 30px; margin-right: 50px" OnClick="OnReplaceClick" />
                                                    </div>
                                                    <div style="margin-top: 20px; margin-left: -35px;">
                                                        <asp:Button ID="RollBackBtn" runat="server" Text="Rollback" disabled="true" CssClass="btn btn-info btn-large" OnClick="onRollBackClick" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="ActualNamesPanel">
                                                        <asp:Label ID="ListForReplace" runat="server" Text="Actual Names List" CssClass="label"></asp:Label><br>
                                                        <asp:ListBox runat="server" ID="ActualNamesList" Rows="20" Width="385px" onClick="actualNamesListClick();"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="BNEntriesPanel" runat="server" HeaderText="Blue Nile Entries">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Blue Nile Entries</legend>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtCustomerName" style="float: right; margin-top: 2px; margin-bottom: 0px; width: 150px;">
                                                        Select Customer Name</label>
                                                </td>
                                                <td>
                                                    <div>
                                                        <label class="control-label" for="txtCustomerName" style="float: left; margin-top: 2px; margin-bottom: 0px; width: 150px;">
                                                            Customer Name:</label>
                                                    </div>
                                                    <div>
                                                        <asp:DropDownList ID="ddlBNCustomer" runat="server" DataTextField="CustomerName"
                                                            AutoPostBack="True" DataValueField="CustomerCode" CssClass="text-style" OnSelectedIndexChanged="OnBNCustomerSelectedChanged"
                                                            ToolTip="Customers List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Text="*" ErrorMessage="Please Select Customer" ValidationGroup="vgCustomerToBN" ControlToValidate="ddlBNCustomer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtBNCustomerCode" class="form-control form-control-height text-style" Style="width: 90px; margin-top: 4px; margin-left: 0px;" runat="server" OnClick="OnBNCustCodeClick" OnTextChanged="OnBNCustCodeClick"
                                                            placeholder="CustomerCode" autocomplete="off" AutoPostBack="True" Visible="False"></asp:TextBox>
                                                    </div>
                                                    <div>
                                                        <asp:Button ID="bnLoadBtn" runat="server" Text="Load" OnClick="OnBNCustCodeClick" CssClass="btn btn-info btn-large" />
                                                        <asp:Button ID="bnSaveBtn" runat="server" class="btn btn-info btn-large" Style="width: auto; vertical-align: middle; margin-left: 0px; margin-top: 0px;"
                                                            Text="Save" OnClick="SaveBNEntries_Click" />
                                                        <asp:Button ID="Button3" runat="server" class="btn btn-info btn-large" Text="Clear" OnClick="btnBNClear_Click" Style="margin-left: 0px; margin-top: 0px;"
                                                            CausesValidation="False" />
                                                    </div>
                                                </td>
                                                <td rowspan="4" style="vertical-align: top; padding-left: 10px; width: 250px;">
                                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="vgCustomerToBN" HeaderText="You must enter a value in the following fields:" />
                                                    <asp:Label ID="lblBNError" runat="server" Style="color: red; padding-right: 20px;" Visible="False"></asp:Label>
                                                    <td></td>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="text-align: left; padding-top: 15px;" colspan="1">
                                                    <div>
                                                        <asp:Label ID="Label14" runat="server" Text="BN Category"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtBNCategory" runat="server" Width="150px" placeholder="Enter Category"></asp:TextBox>
                                                    <asp:Button ID="addBNCategoryBtn" runat="server" Text="ADD" OnClick="OnBNAddCategoryClick" Width="72px" Height="35px" CssClass="btn btn-info btn-large" />
                                                    <asp:HiddenField ID="hdnBNCustomerName" runat="server" Value="0"></asp:HiddenField>
                                                </td>
                                                <td style="text-align: left; padding-top: 15px;" colspan="1">
                                                    <div>
                                                        <asp:Label ID="Label15" runat="server" Text="BN Destination"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtBNDestination" runat="server" Width="150px" placeholder="Enter Destination"></asp:TextBox>
                                                    <asp:Button ID="addBNDestinationBtn" runat="server" Text="ADD" OnClick="OnBNAddDestinationClick" Width="72px" Height="35px" CssClass="btn btn-info btn-large" />
                                                    <asp:HiddenField ID="hdnBNDestination" runat="server" Value="0"></asp:HiddenField>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label17" runat="server" Text="BN Categories" Width="100px"></asp:Label>
                                                    <asp:Panel ID="Panel4" runat="server" Width="250px" Height="200px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="bnCategoryCheckList" runat="server" Width="300px"></asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label18" runat="server" Text="BN Destinations" Width="200px"></asp:Label>
                                                    <asp:Panel ID="Panel5" runat="server" Width="250px" Height="200px" ScrollBars="Auto" BorderColor="Black" BorderWidth="1px" HorizontalAlign="Left" Style="padding-left: 5px;">
                                                        <asp:CheckBoxList ID="bnDestinationCheckList" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="BulkCutGradePanel" runat="server" HeaderText="Bulk Cut Grade">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Cut Grade Info
                                        </legend>
                                        <div style="min-height: 50px; max-width: 920px; margin: 10px;">
                                            <asp:Label ID="errorCutGradeLbl" runat="server" Visibility="false"></asp:Label>
                                        </div>
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top; padding-left: 20px; width: 100%">
                                                    <asp:Label ID="orderLbl" runat="server" Text="GSI NUMBER" Width="100px" Style="text-align: center;" Font-Bold="true" Visible="true"></asp:Label>
                                                    <asp:Label ID="batchLbl" runat="server" Text="BATCH" Width="70px" Visible="false"></asp:Label>
                                                    <asp:Label ID="Label16" runat="server" Text="ITEM" Width="70px" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; padding-left: 20px; width: 100%">
                                                    <asp:TextBox runat="server" ID="orderText" Width="100px" Style="text-align: center;"></asp:TextBox>
                                                    <asp:TextBox runat="server" ID="batchText" Width="70px" Style="text-align: center;" Visible="false"></asp:TextBox>
                                                    <asp:TextBox runat="server" ID="itemText" Width="70px" Style="text-align: center;" Visible="false"></asp:TextBox>
                                                    <asp:CheckBox ID="notPassedBox" runat="server" CssClass="checkbox" Text="Passed/Failed" Width="80px" OnCheckedChanged="notPassedBox_CheckedChanged" Visible="false" />
                                                    <asp:CheckBox ID="passedBox" runat="server" CssClass="checkbox" Text="Details" Width="80px" OnCheckedChanged="passedBox_CheckedChanged" />
                                                    <asp:CheckBox ID="fullBox" runat="server" CssClass="checkbox" Text="Full List" Width="80px" OnCheckedChanged="fullBox_CheckedChanged" Visible="false" />
                                                    <asp:Button ID="runBulkCutGradeBtn" runat="server" Text="LOAD" CssClass="btn btn-info btn-large" OnClick="runBulkCutGrade_Click" />
                                                    <asp:Button ID="clearBulkCutGradeBtn" runat="server" Text="CLEAR" CssClass="btn btn-info btn-large" OnClick="clearBulkCutGradeBtn_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DataGrid runat="server" ID="CutGradesGrid" AutoGenerateColumns="False" CellPadding="5">
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ItemNumber" HeaderText="ItemNumber"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Part" HeaderText="Part"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Cut Grade Type" HeaderText="Cut Grade Type"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="MeasureName" HeaderText="MeasureName"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Pass" HeaderText="Pass"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Rule From" HeaderText="Rule From"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Rule To" HeaderText="Rule To"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Result Value" HeaderText="Result Value"></asp:BoundColumn>
                                                        </Columns>
                                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                                    </asp:DataGrid>
                                                </td>
                                                <%--<td style="vertical-align: top;padding-left: 20px; width: 100%">
                                                                    <asp:ListBox runat="server" ID="BulkCutGradeBox" Rows="10" DataValueField="ItemNumber" Width="500px" AutoPostBack="True"
                                                                        DataTextField="MeasureName" Visible="false" />
                                                </td>--%>
                                            </tr>

                                        </table>

                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="VRSkuPanel" runat="server" HeaderText="Sku Only Reports List">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Sku Only Reports
                                        </legend>
                                       <br />
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top; padding-left: 20px; width: 100%">
                                                    <asp:Panel runat="server" ID="MeasuresListDropDawnPanel">
                                                        <asp:ListBox runat="server" ID="ddlMeasures" AutoPostBack="True"
                                                            OnSelectedIndexChanged="OnMeasuresSelectedChanged" Rows="3" Width="200px"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; padding-left: 20px; width: 100%">
                                                    <asp:TextBox runat="server" ID="SkuNameBox" Width="185px"></asp:TextBox>
                                                    <asp:Button ID="AddSkuBtn" runat="server" Text="Add" CssClass="btn btn-info btn-large" OnClick="AddSkuOnly_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; padding-left: 20px; width: 100%">
                                                    <asp:ListBox runat="server" ID="lstSkus" Rows="10" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="SkuSelected_Click" />
                                                    <asp:Button ID="DeleteSkuBtn" runat="server" Text="Delete" OnClick="OnDeleteSkuClick" CssClass="btn btn-info btn-large" Visible="False" />
                                                </td>
                                            </tr>

                                        </table>
                                         <div style="min-height: 50px; max-width: 920px; margin: 10px;">
                                            <asp:Label ID="ErrorSuccess1" runat="server" Visibility="false"></asp:Label>
                                        </div>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                                                        
                            <ajaxToolkit:TabPanel ID="CompanyGroupPanel" runat="server" HeaderText="Company Group">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Company Group</legend>
                                        <table border="0" >
                                            <tr>
                                                <td style="text-align: left; padding-top: 15px;" colspan="1">
                                                    <div>
                                                        <asp:Label ID="Label19" runat="server" Text="Company Group Name"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtCompanyGroupName" runat="server" Width="300px" placeholder="Enter Company Group Name"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnCompanyGroupID" runat="server" Value="0"></asp:HiddenField>

                                                </td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    <asp:Panel runat="server" ID="Panel9">
                                                        <asp:Label ID="Label25" runat="server" Text="List of Customers in Company Group" CssClass="label"></asp:Label><br>
                                                        <asp:ListBox runat="server" ID="lstGroupList" Rows="10" Width="385px" SelectionMode="Multiple"></asp:ListBox>
                                                    </asp:Panel>
                                                </td>
                                                <td>

                                                    <div style="padding-top: 35px; padding-left: 35px;">
                                                        <asp:ImageButton ID="imgMoveToGroupList" runat="server" ToolTip="Add to List of Customers in Company Group"
                                                            ImageUrl="~/Images/ajaxImages/leftArrowBl.png" Style="width: 30px; margin-right: 30px" OnClick="imgMoveToGroupList_Click" />
                                                    </div>

                                                    <div style="padding-top: 30px; padding-left: 35px;">
                                                        <asp:ImageButton ID="imgMoveToCustomerList" runat="server" ToolTip="Remove from the List of Customers in Company Group"
                                                            ImageUrl="~/Images/ajaxImages/rightArrowBl.png" Style="width: 30px; margin-right: 30px" OnClick="imgMoveToCustomerList_Click" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="Panel8">
                                                        <asp:Label ID="Label21" runat="server" Text="Customer List" CssClass="label"></asp:Label><br>
                                                        <asp:TextBox id="filterTextbox" runat="server" placeholder="Filter Customer List" Style="width: 370px; margin-bottom:0px!important;"></asp:TextBox><br>
                                                        <asp:ListBox runat="server" ID="lstCustomerList" Rows="10" Width="385px" Height="170px" SelectionMode="Multiple"></asp:ListBox>                                                       
                                                    </asp:Panel>
                                                </td>


                                                <td style="vertical-align: bottom; width: 400px;">
                                                    <asp:Button ID="btnSaveCompanyGroup" runat="server" Text="Save" CssClass="btn btn-info btn-large" Style="margin-left: 2px;" OnClick="btnSaveCompanyGroup_Click" />
                                                    <asp:Button ID="btnClearCompanyGroup" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearCompanyGroup_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblMsgCompanyGroup" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>

                                        </table>


                                        <table>
                                            <tr>

                                                <td>
                                                    <asp:GridView ID="gvCompanyGroup" runat="server" DataKeyNames="CompanyGroupID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="gvCompanyGroup_Sorting" Width="800px" CellPadding="4" ForeColor="#333333" OnRowDataBound="gvCompanyGroup_RowDataBound">

                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEditCompanyGroup" runat="server" Text="Edit" CausesValidation="false" OnClick="EditCompanyGroupName" CssClass="btn btn-info"
                                                                        Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>

                                                                <ItemStyle Width="50px" />

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company Group Name" SortExpression="CompanyGroupName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCompanyGroupName" runat="server" Text='<%#Eval("CompanyGroupName")%>' CausesValidation="false" OnClick="EditCompanyGroupName"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnCompanyGroupID" runat="server" Value='<%#Eval("CompanyGroupID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Vendors Name" SortExpression="CustomerNameSet">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCustomerNameSet" runat="server" Text='<%#Eval("CustomerNameSet")%>' CausesValidation="false" OnClick="EditCompanyGroupName"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnCustomerIDSet" runat="server" Value='<%#Eval("CustomerIDSet")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TPRetailersVendor" runat="server" HeaderText="Retailer's Vendor">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Retailer's Vendor</legend>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td width="150px">
                                                    <label class="control-label" for="ddlRetailerList" style="float: left; margin-top: 2px; margin-bottom: 0px;">
                                                        Retailer Name:</label>
                                                </td>
                                                <td width="280px">
                                                    <asp:DropDownList ID="ddlRetailerList" runat="server" DataTextField="RetailerName"
                                                        AutoPostBack="True" DataValueField="RetailerID" CssClass="text-style" OnSelectedIndexChanged="OnRetailerSelectedChanged"
                                                        ToolTip="Retailer List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="120px">
                                                    <asp:Button ID="ClearCustomerList" runat="server" class="btn btn-info" Text="Clear" OnClick="btnCustomerClear_Click" Style="margin-left: 0px; margin-top: 0px;"
                                                        CausesValidation="False" />
                                                </td>
                                                <td></td>
                                                <td rowspan="4" style="vertical-align: top; padding-left: 10px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="ddlNature" style="float: left; margin-top: 4px;">
                                                        Nature:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlNature" runat="server" 
                                                        AutoPostBack="True" CssClass="text-style" OnSelectedIndexChanged="OnNatureSelectedChanged"
                                                        ToolTip="Nature List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="ddlBrand" style="float: left; margin-top: 4px;">
                                                        Brand:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlBrand" runat="server" 
                                                        AutoPostBack="True" CssClass="text-style" OnSelectedIndexChanged="OnBrandSelectedChanged"
                                                        ToolTip="Brand List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddBrand" runat="server" class="btn btn-info" Text="Add New" OnClick="btnAddNewBrand_OnClick" Style="margin-left: 0px; margin-top: 0px;"
                                                        CausesValidation="False" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnBrandToRetailer" runat="server" class="btn btn-info" Text="Brand To Retailer" OnClick="btnBrandToRetailer_OnClick" Style="margin-left: 0px; margin-top: 0px; width:145px;"
                                                        CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="ddlProgram" style="float: left; margin-top: 4px;">
                                                        Program:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlProgram" runat="server" CssClass="text-style" OnSelectedIndexChanged="OnProgramSelectedChanged" AutoPostBack="true"
                                                        ToolTip="Program List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddProgram" runat="server" class="btn btn-info" Text="Add New" OnClick="btnAddNewProgram_OnClick" Style="margin-left: 0px; margin-top: 0px;"
                                                        CausesValidation="False" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnProgramToVendor" runat="server" class="btn btn-info" Text="Program To Vendor" OnClick="btnProgramToVendor_OnClick" Style="margin-left: 0px; margin-top: 0px; width:145px;"
                                                        CausesValidation="False" />
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtVendor" style="float: left; margin-top: 4px;">
                                                        Vendor:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRetailerVendors" runat="server" DataTextField="CustomerName"
                                                        AutoPostBack="True" DataValueField="CustomerID" CssClass="text-style" OnSelectedIndexChanged="OnRetailerVendorsSelectedChanged"
                                                        ToolTip="Vendor List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="lblActionError" runat="server" style="color: red; font-weight: bold;" Visible="false" value=""></asp:Label>
                                                    <asp:Label ID="lblActionSuccess" runat="server" style="color: green; font-weight: bold;" Visible="false" value=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 0px; vertical-align: top; font-size: 14px;" colspan="5">
                                                    <br />
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblRetailersVen" Text="Retailer's Vendors:" Visible="False"></asp:Label>
                                                                <asp:GridView runat="server" ID="gvRetailersCustomers" AutoGenerateColumns="False" DataKeyNames="CustomerCode" CellPadding="4"
                                                                    ShowHeaderWhenEmpty="True" EmptyDataText="No records found." ForeColor="#333333" AllowSorting="true" OnSorting="sort_table"
                                                                        OnRowDataBound="gvRetailersCustomer_OnRowDataBound" OnDataBound="gvRetailersCustomer_OnDataBound">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate></HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkDeleteVendor" runat="server" CausesValidation="false" CssClass="btn btn-info" OnClick="DeleteVendor"
                                                                                    OnClientClick="return confirm('Are you sure you want to delete this Vendor?');"
                                                                                    Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;">Delete </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="CustomerName" HeaderText="Vendor's Name">
                                                                            <ItemTemplate>
                                                                           <%-- <asp:LinkButton ID="lnkVendorName" runat="server" Text='<%#Eval("CustomerName")%>' CausesValidation="false"></asp:LinkButton>--%>
                                                                                <asp:Label ID="lblVendorName" runat="server" Value='<%#Eval("CustomerName")%>'><%#Eval("CustomerName")%></asp:Label>
                                                                                <asp:HiddenField ID="hdnVendorCode" runat="server" Value='<%#Eval("CustomerCode")%>' />
                                                                                <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%#Eval("CustomerID")%>' />
                                                                                <asp:HiddenField ID="hdnVendorOfficeId" runat="server" Value='<%#Eval("CustomerOfficeID")%>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="VendorGroup" HeaderText="Vendor's Group">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblVendorGroup" runat="server" value=""><%#Eval("VendorGroup")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField  SortExpression="VendorProgram" HeaderText="Vendor Program/Brand">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblVendorPrograms" runat="server" value=""></asp:Label>
                                                                            </ItemTemplate>
                                                                            
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate></HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkViewVendor" runat="server" CausesValidation="false" CssClass="btn btn-info" OnClick="ViewVendor"
                                                                                    Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;">View Details </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>

                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <EmptyDataRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="False" ForeColor="White" Font-Size="12px" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />

                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                         <%-- New Brand dialog --%>
                                        <asp:Panel runat="server" ID="AddNewBrandPanel" CssClass="modalPopup" Width="400px" Style="display: none;border: solid 2px #5377A9;" >
                                            <asp:Panel ID="AddNewBrandPanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 25px">
                                                <div style="text-align: left; color:#5377A9; font-weight: bold; margin-left:10px; margin-top:2px;">
                                                    <b>Add new Brand</b>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel CssClass="form-inline" DefaultButton="NewBrandNameBtn" runat="server" Style="padding-top: 5px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlNewBrandNature" style="float: left; margin-top: 4px;">
                                                                Nature:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewBrandNature" runat="server" AutoPostBack="False" CssClass="text-style" 
                                                                ToolTip="Nature List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="NewBrandNameFld" style="float: left; margin-top: 4px;">
                                                                Brand Name*:</label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="NewBrandNameFld" style="width:268px; height:20px; margin-top:8px;"></asp:TextBox>
                                                            <asp:Button runat="server" ID="NewBrandNameBtn" Style="display: none"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="ErrNewBrandNameLabel" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center;">
                                                    <asp:Button ID="AddNewBrandOkButton" runat="server" OnClick="OnNewBrandDialogOkClick" Text="Ok" />
                                                    <asp:Button ID="AddNewBrandCancelButton" runat="server" Text="Cancel" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="NewBrandPopupExtender" runat="server" TargetControlID="NewBrandNameBtn"
                                            PopupControlID="AddNewBrandPanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="AddNewBrandPanelDragHandle"
                                            OkControlID="AddNewBrandCancelButton" DropShadow="true">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <%-- New Program dialog --%>
                                        <asp:Panel runat="server" ID="AddNewProgramPanel" CssClass="modalPopup" Width="400px" Style="display: none;border: solid 2px #5377A9;" >
                                            <asp:Panel ID="AddNewProgramPanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 25px">
                                                <div style="text-align: left; color:#5377A9; font-weight: bold; margin-left:10px; margin-top:2px;">
                                                    <b>Add new Program</b>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel CssClass="form-inline" DefaultButton="NewProgramNameBtn" runat="server" Style="padding-top: 5px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlNewProgramBrand" style="float: left; margin-top: 4px;">
                                                                Brand:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewProgramBrand" runat="server" AutoPostBack="True" CssClass="text-style" OnSelectedIndexChanged="OnNewProgramBrandSelectedChanged"
                                                                ToolTip="Brand List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlNewProgramNature" style="float: left; margin-top: 4px;">
                                                                Nature:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewProgramNature" runat="server" AutoPostBack="False" CssClass="text-style" 
                                                                ToolTip="Nature List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="NewProgramNameFld" style="float: left; margin-top: 4px;">
                                                                Program Name*:</label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="NewProgramNameFld" style="width:268px; height:20px; margin-top:8px;"></asp:TextBox>
                                                            <asp:Button runat="server" ID="NewProgramNameBtn" Style="display: none"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="ErrNewProgramNameLabel" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>                                              
                                            </asp:Panel>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center;">
                                                    <asp:Button ID="AddNewProgramOkButton" runat="server" OnClick="OnNewProgramDialogOkClick" Text="Ok" />
                                                    <asp:Button ID="AddNewProgramCancelButton" runat="server" Text="Cancel" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="NewProgramPopupExtender" runat="server" TargetControlID="NewProgramNameBtn"
                                            PopupControlID="AddNewProgramPanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="AddNewProgramPanelDragHandle"
                                            OkControlID="AddNewProgramCancelButton" DropShadow="true">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <%-- Assign Brand To Retailer dialog --%>
                                        <asp:Panel runat="server" ID="AssignBrandToRetailerPanel" CssClass="modalPopup" Width="660px" Style="display: none;border: solid 2px #5377A9;" >
                                            <asp:Panel ID="AssignBrandToRetailerPanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 25px">
                                                <div style="text-align: left; color:#5377A9; font-weight: bold; margin-left:10px; margin-top:2px;">
                                                    <b>Assign Brand to Retailer</b>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel CssClass="form-inline" DefaultButton="AssignBrandToRetailerBtn" runat="server" Style="padding-top: 5px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlABRetailer" style="float: left; margin-top: 4px;">
                                                                Retailer:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlABRetailer" runat="server" AutoPostBack="True" CssClass="text-style"  OnSelectedIndexChanged="OnAssignBrandRetailerSelectedChanged"
                                                                ToolTip="Retailer List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td rowspan="3" id="retailersBrands" runat="server">
                                                            <label class="control-label" for="retailersBrandList" style="float: left; margin-top: 4px; margin-left:10px;">
                                                                Assigned Brands:</label>
                                                            <asp:ListBox runat="server" ID="retailersBrandList" AutoPostBack="False" Rows="5" Width="300px" style="margin-left:10px;"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlABNature" style="float: left; margin-top: 4px;">
                                                                Nature:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlABNature" runat="server" AutoPostBack="True" CssClass="text-style" OnSelectedIndexChanged="OnAssignBrandNatureSelectedChanged"
                                                                ToolTip="Nature List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlABBrand" style="float: left; margin-top: 4px;">
                                                                Brand:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlABBrand" runat="server" AutoPostBack="False" CssClass="text-style" 
                                                                ToolTip="Brand List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Button runat="server" ID="AssignBrandToRetailerBtn" Style="display: none"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="lblAssignBrandToRetailerError" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>                                              
                                            </asp:Panel>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center;">
                                                    <asp:Button ID="AssignBrandOkButton" runat="server" OnClick="OnAssignBrandDialogOkClick" Text="Add" />
                                                    <asp:Button ID="DeleteBrandButton" runat="server" OnClick="OnDeleteBrandDialogOkClick" Text="Delete" />
                                                    <asp:Button ID="AssignBrandCancelButton" runat="server" Text="Cancel" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="AssignBrandToRetailerPopupExtender" runat="server" TargetControlID="AssignBrandToRetailerBtn"
                                            PopupControlID="AssignBrandToRetailerPanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="AssignBrandToRetailerPanelDragHandle"
                                            OkControlID="AssignBrandCancelButton" DropShadow="true">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <%-- Assign Program To Vendor dialog --%>
                                        <asp:Panel runat="server" ID="AssignProgramToVendorPanel" CssClass="modalPopup" Width="660px" Style="display: none;border: solid 2px #5377A9;" >
                                            <asp:Panel ID="AssignProgramToVendorPanelDragHandle" runat="server" Style="cursor: move; background-color: #DDDDDD;height: 25px">
                                                <div style="text-align: left; color:#5377A9; font-weight: bold; margin-left:10px; margin-top:2px;">
                                                    <b>Assign Program to Vendor</b>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel CssClass="form-inline" DefaultButton="AssignProgramToVendorBtn" runat="server" Style="padding-top: 5px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlAPVendor" style="float: left; margin-top: 4px;">
                                                                Vendor:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAPVendor" runat="server" AutoPostBack="True" CssClass="text-style"  OnSelectedIndexChanged="OnAssignProgramVendorSelectedChanged"
                                                                ToolTip="Vendor List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td rowspan="3" id="assignedPrograms" style="vertical-align:top;" runat="server">
                                                            <label class="control-label" for="vendorProgramsList" style="float: left; margin-left:10px;">
                                                                Assigned Programs:</label>
                                                            <asp:ListBox runat="server" ID="vendorProgramsList" AutoPostBack="False" Rows="15" Width="300px" style="margin-left:10px;"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="vendorGroupTr" runat="server">
                                                        <td colspan="2">
                                                            Assign Program to Group - <span id="lblCompanyGroup" runat="server"></span>
                                                            <div style="height:190px;overflow-y:scroll;border:solid 1px grey;">
                                                                <asp:CheckBoxList ID="cblAPVendorGroupList" runat="server" Width="320px" CssClass="table table-condensed chkGroupCompanyList"
                                                                     RepeatLayout="Table" style="overflow-y: scroll;padding:5px;">
                                                                </asp:CheckBoxList>
                                                           </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="ddlAPProgram" style="float: left; margin-top: 4px;">
                                                                Program:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAPProgram" runat="server" AutoPostBack="False" CssClass="text-style" 
                                                                ToolTip="Program List" Style="height:30px; width:280px; margin-top: 8px; margin-left: 0px;">
                                                            </asp:DropDownList>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Button runat="server" ID="AssignProgramToVendorBtn" Style="display: none"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="lblAssignProgramToVendorError" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>                                              
                                            </asp:Panel>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center;">
                                                    <asp:Button ID="AssignProgramOkButton" runat="server" OnClick="OnAssignProgramDialogOkClick" Text="Add" />
                                                    <asp:Button ID="AssignProgramDeleteButton" runat="server" OnClick="OnDeleteProgramDialogOkClick" Text="Delete" />
                                                    <asp:Button ID="AssignProgramCancelButton" runat="server" Text="Cancel" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="AssignProgramToVendorPopupExtender" runat="server" TargetControlID="AssignProgramToVendorBtn"
                                            PopupControlID="AssignProgramToVendorPanel" BackgroundCssClass="popUpStyle" PopupDragHandleControlID="AssignProgramToVendorPanelDragHandle"
                                            OkControlID="AssignProgramCancelButton" DropShadow="true">
                                        </ajaxToolkit:ModalPopupExtender>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Vendor's Contractor" Visible="false">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Vendor's Contractor</legend>
                                        <table class="form-group" style="margin-bottom: 7px; width: 1000px;" border="0">
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtCustomerName" style="float: left; margin-top: 2px; margin-bottom: 0px;">
                                                        Vendor Name:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCustomerforContractor" runat="server" DataTextField="CustomerName"
                                                        AutoPostBack="True" DataValueField="CustomerCode" CssClass="text-style" OnSelectedIndexChanged="OnCustomersContractorSelectedChanged"
                                                        ToolTip="Customers List" Width="250px" Height="30px" Style="margin-top: 8px; margin-left: 0px;">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Text="*" ErrorMessage="Please Select Customer" ValidationGroup="vgContractorToCustomer" ControlToValidate="ddlCustomerforContractor" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtCustomerforContractor" class="form-control form-control-height text-style" Style="width: 90px; margin-top: 4px; margin-left: 0px;" runat="server" OnClick="OnCustomersContractorCodeClick" OnTextChanged="OnCustomersContractorCodeClick"
                                                        placeholder="CustomerCode" autocomplete="off" AutoPostBack="True" Visible="False"></asp:TextBox>

                                                    <asp:Button ID="btnCustomersContractorLoad" runat="server" Text="Load" CssClass="btn btn-info btn-large" OnClick="OnCustomersContractorCodeClick" />
                                                    <asp:Button ID="btnbtnCustomersContractorSave" runat="server" class="btn btn-info btn-large" ValidationGroup="vgContractorToCustomer" Style="width: auto; vertical-align: middle; margin-left: 0px; margin-top: 0px;"
                                                        Text="Save" OnClick="AddContractorToCustomer_Click" />
                                                    <asp:Button ID="btnbtnCustomersContractorClear" runat="server" class="btn btn-info btn-large" Text="Clear" OnClick="btnContractorClear_Click" Style="margin-left: 0px; margin-top: 0px;"
                                                        CausesValidation="False" />
                                                </td>
                                                <td rowspan="4" style="vertical-align: top; padding-left: 10px; width: 250px;">
                                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="vgContractorToCustomer" HeaderText="You must enter a value in the following fields:" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="control-label" for="txtStyle" style="float: left; margin-top: 4px;">
                                                        Select Contractor:</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtContractor" runat="server" Enabled="False" Style="width: 235px;"></asp:TextBox>
                                                    <asp:Button ID="btnContractor" runat="server" Text="Contractor" CssClass="btn btn-info "
                                                        Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" />

                                                    <asp:HiddenField ID="hdnContractorCode" runat="server" Value="0" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="vgContractorToCustomer" runat="server" Text="*" ErrorMessage="Please Select Contractor" ControlToValidate="txtContractor" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Label ID="lblContractorMsg" runat="server" ForeColor="Blue" Style="margin-left: 10px;"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td style="padding-left: 0px; vertical-align: top; font-size: 14px;" colspan="3">
                                                    <br />
                                                    <table>
                                                        <tr>
                                                            <td>

                                                                <asp:Label runat="server" ID="lblCustomersContractor" Text="Vendors Contractor:" Visible="False"></asp:Label>


                                                                <asp:GridView runat="server" ID="gvCustomersContractor" AutoGenerateColumns="False" DataKeyNames="ContractorID" CellPadding="4"
                                                                    ShowHeaderWhenEmpty="True" EmptyDataText="No records found." ForeColor="#333333" OnDataBound="gvCustomersContractor_OnRowDataBound">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate></HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" CssClass="btn btn-info" OnClick="DeleteContractor"
                                                                                    OnClientClick="return confirm('Are you sure you want to delete this Contractor?');"
                                                                                    Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;">Delete </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Contractor Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkContractorName" runat="server" Text='<%#Eval("ContractorName")%>' CausesValidation="false"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hdnContractorID" runat="server" Value='<%#Eval("ContractorID")%>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ServiceType Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkServiceTypeName" runat="server" Text='<%#Eval("ServiceTypeName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Category Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkCategoryName" runat="server" Text='<%#Eval("CategoryName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ServiceTime Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkServiceTimeName" runat="server" Text='<%#Eval("ServiceTimeName")%>' CausesValidation="false"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <EmptyDataRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="False" ForeColor="White" Font-Size="12px" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />

                                                                </asp:GridView>
                                                            </td>


                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 7px;" colspan="2">
                                                    <div class="form-group" style="margin-bottom: 2px;">
                                                    </div>
                                                </td>

                                                <td style="padding-right: 7px;">
                                                    <div class="form-group" style="margin-bottom: 2px;">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel runat="server" ID="InfoPanelcontractor" CssClass="modalPopup" Style="width: 410px; display: none; border: solid 2px lightgray;">
                                            <asp:Panel runat="server" ID="Panel7" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                                <div>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                                    <b>Information</b>

                                                </div>
                                            </asp:Panel>
                                            <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px" id="Div1" runat="server">
                                            </div>
                                            <div style="padding-top: 10px">
                                                <p style="text-align: center; font-family: sans-serif">
                                                    <asp:Button ID="Button6" runat="server" Text="Ok" />
                                                </p>
                                            </div>
                                        </asp:Panel>
                                        <asp:Button runat="server" ID="PopupInfoButtoncontractor" Style="display: none" />
                                        <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButtoncontractor" PopupControlID="InfoPanelcontractor" ID="ModalPopupExtender22"
                                            PopupDragHandleControlID="InfoPanelDragHandle" OkControlID="InfoCloseButton" DynamicServicePath="" Enabled="True">
                                        </ajaxToolkit:ModalPopupExtender>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel13" runat="server" HeaderText="Delivery Method" Visible="false">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Delivery Method</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label7" runat="server" Text="Delivery Method"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtDeliveryMethodName" runat="server" Width="250px" placeholder="Enter Delivery Method"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnDeliveryMethodId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtDeliveryMethodName" ValidationGroup="vgDeliveryMethod"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddDeliveryMethod" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddDeliveryMethod_Click" ValidationGroup="vgDeliveryMethod" />
                                                    <asp:Button ID="btnClearDeliveryMethod" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearDeliveryMethod_Click" />

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblDeliveryMethodMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvDeliveryMethod" runat="server" DataKeyNames="DeliveryMethodId" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingDeliveryMethod" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditDeliveryMethod" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeliveryMethod Name" SortExpression="DeliveryMethodName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDeliveryMethodName" runat="server" Text='<%#Eval("DeliveryMethodName")%>' CausesValidation="false" OnClick="EditDeliveryMethod"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnDeliveryMethodID" runat="server" Value='<%#Eval("DeliveryMethodId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel14" runat="server" HeaderText="Shipping Courier" Visible="false">
                                <ContentTemplate>
                                    <fieldset class="tab_fieldset">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Shipping Courier</legend>
                                        <table border="0" style="width: 500px;">
                                            <tr>

                                                <td style="text-align: left; padding-top: 15px;">
                                                    <div>
                                                        <asp:Label ID="Label8" runat="server" Text="Shipping Courier"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtShippingCourierName" runat="server" Width="250px" placeholder="Enter Shipping Courier"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnShippingCourierId" runat="server" Value="0"></asp:HiddenField>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required"
                                                        ControlToValidate="txtShippingCourierName" ValidationGroup="vgShippingCourier"></asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnAddShippingCourier" runat="server" Text="Save" CssClass="btn btn-info btn-large" OnClick="btnAddShippingCourier_Click" ValidationGroup="vgShippingCourier" />
                                                    <asp:Button ID="btnClearShippingCourier" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClearShippingCourier_Click" />


                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: left; height: 15px;" colspan="1">
                                                    <asp:Label ID="lblShippingCourierMsg" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvShippingCourier" runat="server" DataKeyNames="CarrierID" AutoGenerateColumns="False"
                                                        AllowSorting="True" OnSorting="OnSortingCarrierName" Width="500px" CellPadding="4" ForeColor="#333333">
                                                        <AlternatingRowStyle BackColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="EditShippingCourier" CssClass="btn btn-info" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ShippingCourier Name" SortExpression="CarrierName">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkShippingCourierName" runat="server" Text='<%#Eval("CarrierName")%>' CausesValidation="false" OnClick="EditShippingCourier"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnShippingCourierID" runat="server" Value='<%#Eval("CarrierID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>                           
                        </ajaxToolkit:TabContainer>

                        <%--Retailer--%>
                        <asp:Panel runat="server" ID="pnlRetailer" CssClass="modal-dialog" Width="800px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label12" Text="Retailer"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRetailer" runat="server" DataTextField="RetailerName" DataValueField="RetailerId"
                                            Width="180px" Height="30px" CssClass="text-style" AutoPostBack="true" OnSelectedIndexChanged="ddlRetailer_SelectedIndexChanged">
                                        </asp:DropDownList>

                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Repeater ID="repRetailer" runat="server" OnItemCommand="repRetailer_ItemCommand">
                                            <ItemTemplate>
                                                <asp:Button ID="btnRetailer" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                                    CommandName="RetailerId"
                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RetailerId")%>'
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "RetailerName")%>'
                                                    CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlRetailer"
                            TargetControlID="TabContainer2$TabPanel1$btnRetailer"
                            CancelControlID="ctl00_SampleContent_ModalPopupExtender3_backgroundElement"
                            Enabled="True" DynamicServicePath="">
                        </ajaxToolkit:ModalPopupExtender>

                        <%--Contracor--%>
                        <asp:Panel runat="server" ID="pnlContracor" CssClass="modal-dialog" Width="800px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label22" Text="Contracor"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlContractor" runat="server" DataTextField="ContractorName" DataValueField="ContractorId"
                                            Width="180px" Height="30px" CssClass="text-style" AutoPostBack="true" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Repeater ID="repContractor" runat="server" OnItemCommand="repContractor_ItemCommand">
                                            <ItemTemplate>
                                                <asp:Button ID="btnContractor" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                                    CommandName="ContractorId"
                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ContractorId")%>'
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "ContractorName")%>'
                                                    CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="pnlContracor"
                            TargetControlID="TabContainer2$TabPanel3$btnContractor"
                            CancelControlID="ctl00_SampleContent_ModalPopupExtender2_backgroundElement"
                            Enabled="True" DynamicServicePath="">
                        </ajaxToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
