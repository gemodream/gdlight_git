﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="BillingInvoice.aspx.cs" Inherits="Corpt.BillingInvoice" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
    </style>
    <div class="demoarea">
        <div class="demoheading">Regular Billing</div>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
             <ContentTemplate>
                 <div class="navbar nav-tabs">
                     <asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
                         <asp:Label ID="Label1" runat="server" Text="Order: " CssClass="label" Style=""></asp:Label>
                         <asp:TextBox runat="server" ID="OrderField" MaxLength="7" Style="margin-top: 10px;margin-left: 5px"></asp:TextBox>
                         <ajaxToolkit:FilteredTextBoxExtender ID="BatchExtender" runat="server" FilterType="Custom"
                             ValidChars="0123456789" TargetControlID="OrderField" />
                         <asp:ImageButton ID="SearchButton" runat="server" ToolTip="Load Order" ImageUrl="~/Images/ajaxImages/search.png"
                             OnClick="OnSearchClick" Style="margin-right: 15px" />
                         <asp:Button runat="server" ID="BillingButton" Text="Billing" CssClass="btn btn-info" 
                             Visible="False" OnClick="OnBillingClick" OnClientClick = 'return confirm("Are you sure you want to billing?");'/>
                         
                     </asp:Panel>
                 </div>
                 <table>
                     <tr>
                         <td><asp:Label runat="server" Text="Order Items" CssClass="label"></asp:Label></td>
                         <td style="padding-left: 30px"><asp:Label ID="Label2" runat="server" Text="Billing Info" CssClass="label"></asp:Label></td>
                         <td style="padding-left: 30px"><asp:Label runat="server" Text="Migrated Items" CssClass="label"></asp:Label></td>
                     </tr>
                     <tr>
                         <td style="vertical-align: top">
                             <asp:TreeView ID="TreeHistory" runat="server" ShowCheckBoxes="All" AfterClientCheck="CheckChildNodes();"
                                 onclick="OnTreeClick(event)" Font-Names="Tahoma" Font-Size="small" ForeColor="Black"
                                 Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true" EnableClientScript="True"
                                 ShowLines="False" ShowExpandCollapse="true">
                             </asp:TreeView>
                         </td>
                         <td style="vertical-align: top;padding-left: 30px">
                             <asp:DataGrid runat="server" ID="InfoGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                 <Columns>
                                     <asp:BoundColumn DataField="NewItemNumber" HeaderText="Current #"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="DisplayStatus" HeaderText="Status"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="BillingDate" HeaderText="Billing Date"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="Description" HeaderText="Description"></asp:BoundColumn>
                                 </Columns>
                                 <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                 <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                             </asp:DataGrid>
                         </td>
                         <td style="vertical-align: top;padding-left: 30px">
                             <asp:DataGrid runat="server" ID="MigratedItemsGrid" AutoGenerateColumns="False" CssClass="table table-condensed">
                                 <Columns>
                                     <asp:BoundColumn DataField="PrevItemNumber" HeaderText="From Old #" />
                                     <asp:BoundColumn DataField="CurrentItemNumber" HeaderText="Current #" />
                                     <asp:BoundColumn DataField="NewItemNumber" HeaderText="To New #" />
                                 </Columns>
                                 <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                 <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                             </asp:DataGrid>
                         </td>
                     </tr>
                 </table>
                 <div>
                 </div>
                 
                 <%-- Information Dialog --%>
                 <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 410px;
                     display: none">
                     <asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD;
                         border: solid 1px Silver; color: black; text-align: left">
                         <div>
                             <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                             <b>Information</b>
                         </div>
                     </asp:Panel>
                     <div style="overflow: auto; max-width: 400px; max-height: 300px; margin-top: 10px"
                         id="MessageDiv" runat="server">
                     </div>
                     <div style="padding-top: 10px">
                         <p style="text-align: center; font-family: sans-serif">
                             <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                         </p>
                     </div>
                 </asp:Panel>
                 <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                 <ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton"
                     PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle"
                     OkControlID="InfoCloseButton">
                 </ajaxToolkit:ModalPopupExtender>
             </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
