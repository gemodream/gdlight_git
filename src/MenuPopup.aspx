﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuPopup.aspx.cs" Inherits="Corpt.MenuPopup" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Style/bootstrap3.3.4/bootstrap.min.css" />
    <link rel="stylesheet" href="Style/bootstrap3.3.4/bootstrap-theme.min.css" />
    <script src="Style/bootstrap2.0/js/jquery.min.js" type="text/javascript"></script>
    <script src="Style/bootstrap3.3.4/bootstrap.min.js" type="text/javascript"></script>
    <style type="text/css">
        .bs-example
        {
            margin: 20px;
        }
        hr
        {
            margin: 60px 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="btn-group">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Main Menu <span class="caret">
            </span></a>
            <ul class="dropdown-menu" style="font-size: small">
                <li><a href="ReportLookup.aspx">Report Lookup </a></li>
                <li><a href="CPResult.aspx">CP Rules </a></li>
                <li><a href="ShortReport.aspx">Short Report</a></li>
                <li><a href="CPRulesDisplay2.aspx">CP Overview</a></li>
                <li><a href="BelongsTo.aspx">Customer-Report</a></li>
                <li><a href="CustomerVendorCp.aspx">Customer/Vendor/Cp</a></li>
                <li><a href="BillingInfo.aspx">Billing Info</a></li>
                <li><a href="ListOpenBatches.aspx">Open Batch List </a></li>
                <li><a href="PrintedNot.aspx">Reports in an order </a></li>
                <li><a href="OpenOrders.aspx">Open Orders </a></li>
                <li><a href="GetInvoiceNumberByOrderNumber.aspx">Get Invoice Number</a></li>
                <li><a href="MarkLaser2.aspx">Mark Laser</a></li>
                <li><a href="ShortReportII.aspx">Full Order </a></li>
                <li><a href="LeoReport2.aspx">Leo Report II </a></li>
                <li class="divider"></li>
                <li><a href="LotNumber.aspx">Find by Lot Number</a></li>
                <li><a href="MemoLookup.aspx">Find by Memo Number </a></li>
                <li><a href="FindItemByMeasureValue.aspx">Find by Measure Value</a></li>
                <li><a href="Tracking.aspx">Tracking</a></li>
                <li><a href="Tracking2.aspx">Tracking II</a></li>
                <li><a href="PSX.aspx">Photospex</a></li>
                <li><a href="SortingStat.aspx">Sorting Stat</a></li>
                <li><a href="PrvStat2.aspx">Color/Clarity Stat</a></li>
                <li><a href="DeliveryLog2.aspx">Delivery Log</a></li>
                <li class="divider"></li>
            </ul>
        </div>
        <div class="btn-group">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">GDLight<span class="caret"></span>
            </a>
            <ul class="dropdown-menu" style="font-size: small">
                <li><a href="NewOrder.aspx">New Order</a></li>
            </ul>
        </div>
    </div>
    </form>
</body>
</html>
