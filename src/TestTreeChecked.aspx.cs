﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class TestTreeChecked : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrepareData();
            }
        }

        private void PrepareData()
        {
            const string order = "18546";
            var batchList = QueryUtils.GetBatches(order, null, this);
            var itemList = QueryUtils.GetItemsByOrderCode(order, this);
            var data = new List<TreeViewModel>();
            data.Add(new TreeViewModel { ParentId = "", Id = order, DisplayName = order });
            data.AddRange(batchList.Select(batch => new TreeViewModel {ParentId = order, Id = batch.BatchId, DisplayName = batch.FullBatchNumberWithDot}));
            data.AddRange(itemList.Select(item => new TreeViewModel
            {
                ParentId = item.BatchId, 
                Id = Utils.FillToTwoChars(item.ItemCode), 
                DisplayName = item.FullItemNumberWithDotes
            }));

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tvwRegionCountry.Nodes.Add(rootNode);

        }

        protected void TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            if (tvwRegionCountry.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tvwRegionCountry.CheckedNodes)
                {

                    if (node.ChildNodes.Count > 0)
                    {
                        foreach (TreeNode childNode in node.ChildNodes)
                        {
                            childNode.Checked = true;
                        }
                    }

                }

            }
        }

        protected void OnButtonClick(object sender, EventArgs e)
        {
            foreach (var node in tvwRegionCountry.CheckedNodes)
            {
                var s = "";
            }
        }
    }
}