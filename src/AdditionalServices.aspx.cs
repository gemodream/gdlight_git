﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpt.Utilities;
using Corpt.Models;

namespace Corpt
{
	public partial class AdditionalServices : System.Web.UI.Page
	{
        
        protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				//divSetPrice.Style.Add("display", "none");
			}
		}

        protected void OnLoadClick(object sender, EventArgs e)
        {
            if ((txtBatchNumber.Text.Length == 0 || txtBatchNumber.Text == "0"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter value.');", true);
                return;
            }
            else
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            DataSet ds = AdditionalServicesUtils.GetBatchRecheckChangeSKU(txtBatchNumber.Text, this);
            if (ds.Tables[0].Rows.Count >= 1)
            {
                LoadEditData(ds);
                gvAdditionalServicesResult.DataSource = ds;
                gvAdditionalServicesResult.DataBind();
                btnSetService.Visible = true;
                divSetPrice.Visible = true;
                divDetail.Visible = true;
                //divSetPrice.Style.Add("display", "block");
                WrongInfoLabel.Text = "";
                divSetService.Visible = true;

            }
            else
            {
                gvAdditionalServicesResult.DataSource = null;
                gvAdditionalServicesResult.DataBind();
                btnSetService.Visible = false;
                divSetPrice.Visible = false;
                divSetService.Visible = false;
                divDetail.Visible = false;
                WrongInfoLabel.Text = "Batch not found";
            }
        }

        public void LoadEditData(DataSet ds)
        {
            //LinkButton btn = (LinkButton)sender;
            //GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            AdditionalServicesModel additionalServicesModel = new AdditionalServicesModel();

            //HiddenField hfCustomerID = (HiddenField)gvr.Cells[4].FindControl("hfCustomerID");
            //HiddenField hfCPID = (HiddenField)gvr.Cells[4].FindControl("hfCPID");

            additionalServicesModel.CustomerID = ds.Tables[0].Rows[0]["CustomerID"].ToString();
            additionalServicesModel.CPID = ds.Tables[0].Rows[0]["CPID"].ToString();

            HttpContext.Current.Session["AdditionalServicesModel"] = additionalServicesModel;

            //lblBatchNumber.Text = "Batch Number: " + gvr.Cells[1].Text;
            lblCustomer.Text = "Customer:&nbsp&nbsp" + ds.Tables[0].Rows[0]["CustomerName"].ToString();
            lblSKU.Text = "SKU:&nbsp&nbsp" + ds.Tables[0].Rows[0]["CustomerProgramName"].ToString();

            txtSKUChangePrice.Text = ds.Tables[0].Rows[0]["ChangeSKUPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["ChangeSKUPrice"].ToString();
            txtRecheckPrice.Text = ds.Tables[0].Rows[0]["RecheckPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["RecheckPrice"].ToString();
            txtUrgentServicePrice.Text = ds.Tables[0].Rows[0]["UrgentServicePrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["UrgentServicePrice"].ToString();

            txtColorStonePrice.Text = ds.Tables[0].Rows[0]["ColorStonePrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["ColorStonePrice"].ToString();
            txtRePrintPrice.Text = ds.Tables[0].Rows[0]["RePrintPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["RePrintPrice"].ToString();
            txtTNDPrice.Text = ds.Tables[0].Rows[0]["TNDPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["TNDPrice"].ToString();

            txtLaserInscriptionPrice.Text = ds.Tables[0].Rows[0]["LaserInscriptionPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["LaserInscriptionPrice"].ToString();
            txtLaserRemovalPrice.Text = ds.Tables[0].Rows[0]["LaserRemovalPrice"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["LaserRemovalPrice"].ToString();
            txtUrgentService48Price.Text = ds.Tables[0].Rows[0]["UrgentService48Price"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["UrgentService48Price"].ToString();

            // divSetPrice.Style.Add("display", "block");
        }

        protected void OnSaveClick(object sender, EventArgs e)
		{
			if ((txtRecheckPrice.Text.Length == 0 || txtRecheckPrice.Text == "0")
				&& (txtSKUChangePrice.Text.Length == 0 || txtSKUChangePrice.Text == "0")
				&& (txtUrgentServicePrice.Text.Length == 0 || txtUrgentServicePrice.Text == "0")
				&& (txtColorStonePrice.Text.Length == 0 || txtColorStonePrice.Text == "0")
				&& (txtRePrintPrice.Text.Length == 0 || txtRePrintPrice.Text == "0")
				&& (txtTNDPrice.Text.Length == 0 || txtTNDPrice.Text == "0")
				&& (txtLaserRemovalPrice.Text.Length == 0 || txtLaserRemovalPrice.Text == "0")
				&& (txtLaserInscriptionPrice.Text.Length == 0 || txtLaserInscriptionPrice.Text == "0")
                && (txtUrgentService48Price.Text.Length == 0 || txtUrgentService48Price.Text == "0"))
			{
				ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter value.');", true);
				return;
			}

			AdditionalServicesModel additionalServicesModel = new AdditionalServicesModel();
			additionalServicesModel = HttpContext.Current.Session["AdditionalServicesModel"] as AdditionalServicesModel;
			additionalServicesModel.RecheckPrice = txtRecheckPrice.Text;
			additionalServicesModel.ChangeSKUPrice = txtSKUChangePrice.Text;
			additionalServicesModel.UrgentServicePrice = txtUrgentServicePrice.Text;
			additionalServicesModel.ColorStonePrice = txtColorStonePrice.Text;

			additionalServicesModel.RePrintPrice = txtRePrintPrice.Text;
			additionalServicesModel.TNDPrice = txtTNDPrice.Text;
			additionalServicesModel.LaserInscriptionPrice = txtLaserInscriptionPrice.Text;
			additionalServicesModel.LaserRemovalPrice = txtLaserRemovalPrice.Text;
            additionalServicesModel.UrgentService48Price = txtUrgentService48Price.Text;
            bool setBatch = AdditionalServicesUtils.SetRecheckChangeSKUPrice(additionalServicesModel, this);
			BindGrid();
			//divSetPrice.Style.Add("display", "none");

		}

		protected void OnSaveService(object sender, EventArgs e)
		{
            string BatchID = string.Empty;


            foreach (GridViewRow row in gvAdditionalServicesResult.Rows)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					AdditionalServicesModel additionalServicesModel = new AdditionalServicesModel();
					Label lblReportNumbr = (Label)row.Cells[0].FindControl("lblReportNumber");

					HiddenField hfBatchID = (HiddenField)row.Cells[0].FindControl("hfBatchID");
					HiddenField hfGroupID = (HiddenField)row.Cells[0].FindControl("hfGroupID");
					HiddenField hfCustomerID = (HiddenField)row.Cells[0].FindControl("hfCustomerID");
					HiddenField hfCPID = (HiddenField)row.Cells[0].FindControl("hfCPID");
                    HiddenField hfCPName = (HiddenField)row.Cells[0].FindControl("hfCPName");
                    

                    CheckBox chkIsSKUChange = (CheckBox)row.Cells[1].FindControl("chkIsSKUChange");
					CheckBox chkIsRecheck = (CheckBox)row.Cells[2].FindControl("chkIsRecheck");
					CheckBox chkIsUrgentService = (CheckBox)row.Cells[3].FindControl("chkIsUrgentService");
                    CheckBox chkIsUrgent48Service = (CheckBox)row.Cells[4].FindControl("chkIsUrgent48Service");
                    CheckBox chkIsColorStone = (CheckBox)row.Cells[5].FindControl("chkIsColorStone");

					CheckBox chkIsRePrint = (CheckBox)row.Cells[6].FindControl("chkIsRePrint");
					CheckBox chkIsTND = (CheckBox)row.Cells[7].FindControl("chkIsTND");
					CheckBox chkIsLaserInscription = (CheckBox)row.Cells[8].FindControl("chkIsLaserInscription");
					CheckBox chkIsLaserRemoval = (CheckBox)row.Cells[9].FindControl("chkIsLaserRemoval");
                    

                    additionalServicesModel.ReportNumber = lblReportNumbr.Text;

					additionalServicesModel.BatchID = hfBatchID.Value;
					additionalServicesModel.GroupID = hfGroupID.Value;
					additionalServicesModel.CustomerID = hfCustomerID.Value;
					additionalServicesModel.CPID = hfCPID.Value;
                    additionalServicesModel.CustomerProgramName = hfCPName.Value;

                    additionalServicesModel.IsRecheck = chkIsRecheck.Checked;
					additionalServicesModel.IsSKUChange = chkIsSKUChange.Checked;
					additionalServicesModel.IsUrgent = chkIsUrgentService.Checked;
					additionalServicesModel.IsColorStone = chkIsColorStone.Checked;

					additionalServicesModel.IsRePrint = chkIsRePrint.Checked;
					additionalServicesModel.IsTND = chkIsTND.Checked;
					additionalServicesModel.IsLaserInscription = chkIsLaserInscription.Checked;
					additionalServicesModel.IsLaserRemoval = chkIsLaserRemoval.Checked;
                    additionalServicesModel.IsUrgent48 = chkIsUrgent48Service.Checked;

                    bool setBatch = false;
                    setBatch = AdditionalServicesUtils.SetBatchRecheckChangeSKU(additionalServicesModel, this);

                    BatchID = hfBatchID.Value;

                }
			}
			BindGrid();
        }
        private bool IsItemInvoiced(string certificate)
        {
            bool isInvoiced = false;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(this.Session["MyIP_ConnectionString"].ToString());
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "select certificate from sales with (nolock) where certificate = '" + certificate + "'",
                Connection = conn,
                CommandType = CommandType.Text
                //CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                isInvoiced = true;
            }
            else
            {
                isInvoiced = false;
            }
            conn.Close();
            return isInvoiced;
        }
        private void ExecuteNonQuery(string connectionString, string cmdText)
        {
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                using (command = new SqlCommand(cmdText, connection))
                {
                    command.CommandTimeout = 1000;
                    SqlDataAdapter daAdapter;
                    daAdapter = new SqlDataAdapter(command);
                    daAdapter.SelectCommand = command;
                    daAdapter.SelectCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                //writeFile.WriteLine(@"ERROR ---------------------------------------------");
                //writeFile.WriteLine("ExecuteNonQuery : Error in DB");
                //writeFile.WriteLine(ex.Message);
                //writeFile.WriteLine(cmdText);
                throw;
            }
            finally
            {
                //writeFile.Flush();
                connection.Close();
            }
        }

        protected void gvAdditionalServicesResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                if (lblStatus.Text == "TallyProc" || lblStatus.Text == "Completed")
                {

                    CheckBox chkIsSKUChange = (CheckBox)e.Row.Cells[1].FindControl("chkIsSKUChange");
                    CheckBox chkIsRecheck = (CheckBox)e.Row.Cells[2].FindControl("chkIsRecheck");
                    CheckBox chkIsUrgentService = (CheckBox)e.Row.Cells[3].FindControl("chkIsUrgentService");

                    CheckBox chkIsUrgent48Service = (CheckBox)e.Row.Cells[4].FindControl("chkIsUrgent48Service");
                    CheckBox chkIsColorStone = (CheckBox)e.Row.Cells[5].FindControl("chkIsColorStone");
                    CheckBox chkIsRePrint = (CheckBox)e.Row.Cells[6].FindControl("chkIsRePrint");

                    CheckBox chkIsTND = (CheckBox)e.Row.Cells[7].FindControl("chkIsTND");
                    CheckBox chkIsLaserInscription = (CheckBox)e.Row.Cells[8].FindControl("chkIsLaserInscription");
                    CheckBox chkIsLaserRemoval = (CheckBox)e.Row.Cells[9].FindControl("chkIsLaserRemoval");

                    chkIsSKUChange.Enabled = false;
                    chkIsRecheck.Enabled = false;
                    chkIsUrgentService.Enabled = false;

                    chkIsUrgent48Service.Enabled = false;
                    chkIsColorStone.Enabled = false;
                    chkIsRePrint.Enabled = false;

                    chkIsTND.Enabled = false;
                    chkIsLaserInscription.Enabled = false;
                    chkIsLaserRemoval.Enabled = false;
                }

            }
        }
    }
}