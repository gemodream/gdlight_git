﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;
using System.IO;

namespace Corpt
{
    public partial class ShortReportExt : CommonPage
    {
        private const string CmdDelOrder = "CmdDelOrder";
        private const string MsgOrderLoad = "Order {0} loaded";
        private const string MsgOrderDelete = "Order {0} deleted";
        private const string MsgItemsTreeLoad = "'Item Numbers' tree for selected documents loaded";
        private const string MsgTableBuild = "Table built";
        private const string MsgCheckAll = "All items marked";
        private const string MsgUnCheckAll = "All items not marked";
        private const string MsgClearAll = "All cleared";
        private const string MsgEditItem = "Edit dialog for item {0} opened";
        private const string MsgSaveItem = "Item {0} data saved";
        private const string MsgLabelPreview = "Labels preview opened";
        private const string MsgLabelToExcel = "Label {0} downloaded";
        private const string MsgPdfView = "PDF item {0} loaded";
        private const string MsgYellowHide = "Unhighlighted";
        private const string MsgYellowShow = "Highlighted";
        private const string MsgPrintShow = "Printed docs dialog for item {0} opened";
        private const string MsgShowHide = "Show/Hide dialog opened";
        private const string MsgLblDocument = "Lbl document dialog opened";
        private const string MsgEmailSent = "Email sent";
        private const string MsgBulkUpdateOpen = "'Bulk Update' dialog opened";
        private const string MsgBulkUpdateSave = "'Bulk Update' data saved";
        private const string MsgNoreport = "No reports for batch {0}";

        const string DlgDivFormat = "overflow-y: auto; height:{0}px; width: 500px; color: black; border: silver solid 1px; margin-top: 5px";
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Header.Title = "GSI: ShortReport (New)";
            if (IsPostBack)
            {
                PostBackHandler();
                return;
            }
            //-- Init events
            OrderList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(OrderList, CmdDelOrder));
            OrderFld.Focus();

            //-- Enum measures
            LoadEnumMeasures();
            LastActionLbl.ForeColor = System.Drawing.Color.DarkBlue;
        }
        
        private void PostBackHandler()
        {
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelOrder)
            {
                OnRemoveOrder();
            }
        }
        #endregion

        #region Enum Measures
        private void LoadEnumMeasures()
        {
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.CpCommonEnumMeasures);
        }
        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }
        #endregion
        private void SetActionText(string text)
        {
            LastActionLbl.Text = text;
        }
        #region Add & Remove Orders
        protected void OnRefreshClick(object sender, EventArgs e)
        {
            //-- Save orders list
            var orders = new List<string>();
            foreach (ListItem item in OrderList.Items)
            {
                orders.Add(item.Text);
            }
            //-- Clear All
            OnClearAllClick(null, null);
            foreach(var order in orders)
            {
                OrderFld.Text = order;
                OnFindOrderClick(null, null);
            }
            SetActionText("Refresh data");
        }

        protected void OnFindOrderClick(object sender, ImageClickEventArgs e)
        {
            var order = OrderFld.Text.Trim();
            if (order.Length < 5 || order.Length > 7)
            {
                PopupInfoDialog("Invalid Order code", true);
                return;
            }
            if (OrderList.Items.Cast<ListItem>().Any(item => item.Text == order))
            {
                PopupInfoDialog("The order has already been added", true);
                return;
            }
            hdnOrder.Value = order;
            SetActionText("Loading data for order " + order);
            FindOrderBtn.Enabled = false;
            string errMsg;
            List<MovedItemModel> orderMovedItems;
            var reports = QueryUtils.GetLeoFullOrder(order, out errMsg, this, out orderMovedItems);
            
            FindOrderBtn.Enabled = true;
            if (!string.IsNullOrEmpty(errMsg))
            {
                PopupInfoDialog(errMsg, true);
                return;
            }
            if (reports == null || reports.Count == 0 && orderMovedItems.Count == 0)
            {
                PopupInfoDialog("The order not found or SRT document is not attached to the order", true);
                return;
            }
            //-- Get Data For ItemNumners Tree
            var historyOrder = QueryItemiznUtils.GetOrderHistory(order, true, this);
            hdnCustomerCode.Value = historyOrder[0].CustomerCode;
            string batchError = null;
            var batchesHst = historyOrder.FindAll(m => m.TargetCode == StateTargetModel.TrgCodeBatch);
            foreach(var batchHst in batchesHst)
            {
                var batchModel = reports.Find(m => m.BatchModel.BatchId == batchHst.BatchId);
                if (batchModel == null)
                {
                    batchHst.DocumentName = "unknown";
                } else
                {
                    if (batchModel.DocStructure.Count == 0)
                    {
                        //LastActionLbl.Text = "No reports for " + batchModel.BatchModel.FullBatchNumber;
                        SetActionText(string.Format(MsgNoreport, batchModel.BatchModel.FullBatchNumber));
                        LastActionLbl.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    else
                        batchHst.DocumentName = batchModel.DocStructure.ElementAt(0).DocumentName;
                }
            }
            GetOrderHistoryFromView().Add(new OrderHistoryModel{OrderCode = order, HistoryItems = historyOrder});
            
            var orderItem = new ListItem { Value = order, Text = order };
            OrderList.Items.Insert(0, orderItem);
            GetReportFromView().AddRange(reports);
           
            if (orderMovedItems.Count > 0)
            {
                GetMovedItemsFromView().AddRange(orderMovedItems);
            }
            SetMovedItemsBtnState();
            LoadDocumentList();
            OrderFld.Text = "";

            SetActionText(string.Format(MsgOrderLoad, order));
            OrderFld.Focus();
            if (reports.Count == 0 && orderMovedItems.Count > 0)
            {
                var first = orderMovedItems[0];
                var format = "Items moved from {0} order to {1} order";
                PopupInfoDialog(string.Format(format, first.Order, first.NewOrder), true);
            }

        }

        private void SetMovedItemsBtnState()
        {
            var hasItems = GetMovedItemsFromView().Count > 0;
            MovedItemsBtn.Enabled = hasItems;
        }
        
        //-- On add/delete Order
        private void LoadDocumentList()
        {
            DocumentList.Items.Clear();

            var reports = GetReportFromView();
            var groups = reports.GroupBy(m => m.DocumentId + ";" + m.DocumentName).ToList();
            
            foreach (var group in groups)
            {
                var keys = group.Key.Split(';');
                if (!string.IsNullOrEmpty(keys[0]))
                {
                    var item = new ListItem { Value = keys[0], Text = string.IsNullOrEmpty(keys[1]) ? "ID " + keys[0] : keys[1] };
                    DocumentList.Items.Add(item);
                }
            }
            if (groups.Count > 0)
            {
                DocumentList.SelectedIndex = 0;
            }
            OnDocumentSelected(null, null);
        }
        private void LoadColumnsView()
        {
            var docs = GetCheckedDocuments();
            if (docs.Count == 0)
            {
                ColumnsGrid.DataSource = null;
                ColumnsGrid.DataBind();
                return;
            }
            var docsStruct = new List<DocStructModel>();
            foreach(var doc in docs)
            {
                var rep = GetReportFromView().Find(m => m.DocumentId == doc);
                if (rep == null) continue;
                foreach(var line in rep.DocStructure)
                {
                    var addedLine = docsStruct.Find(m => m.Title == line.Title);
                    if (addedLine == null)
                    {
                        docsStruct.Add(line);
                    } else
                    {
                        addedLine.DocumentName = addedLine.DocumentName + ", " + line.DocumentName;
                        var valEqual = addedLine.Value == line.Value;
                        if (!valEqual)
                        {
                            addedLine.Value = addedLine.Value + "<br/> ---- <br/> " + line.Value;
                        }
                    }
                }
                //docsStruct.AddRange(rep.DocStructure);
            }
            ColumnsGrid.DataSource = docsStruct;
            ColumnsGrid.DataBind();

        }
        private IEnumerable<string> GetMeasuresFromDocument(string docId)
        {
            var structure = GetReportFromView().Find(m => m.DocumentId == docId).DocStructure;
            char[] delimiterChars = { '[', ']'};
            var result = new List<string>();
            foreach(var line in structure)
            {
                var value = line.Value.Trim();
                var parts = value.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                foreach (var part in parts)
                {
                    if (part.Trim().Length > 0&& part.IndexOf('.') > 0)
                    {
                        result.Add(part.Trim());
                    }
                }
            }
            return result;
        }
        private void OnRemoveOrder()
        {
            var order = OrderList.SelectedValue;
            if (string.IsNullOrEmpty(order)) return;
            OrderList.Items.Remove(OrderList.SelectedItem);
            
            //-- Short report delete
            var reports = GetReportFromView();
            var delRpts = reports.FindAll(m => m.BatchModel.GroupCodeFiveChars == order);
            foreach(var delRpt in delRpts)
            {
                reports.Remove(delRpt);
            }

            //-- Moved Items delete
            var movedItems = GetMovedItemsFromView();
            var delItems = movedItems.FindAll(m => m.Order == order);
            foreach (var delItem in delItems)
            {
                movedItems.Remove(delItem);
            }
            SetMovedItemsBtnState();
            LoadDocumentList();
            SetActionText(string.Format(MsgOrderDelete, order));
        }
        private void OnRemoveAllOrders()
        {
            OrderList.Items.Clear();
            SetViewState(new List<OrderHistoryModel>(), SessionConstants.LeoHistoryOrders);
            SetViewState(new List<ShortReportModel>(), SessionConstants.LeoShortReports);
            SetViewState(new List<MovedItemModel>(), SessionConstants.LeoMovedItems);
            LoadDocumentList();
            SetMovedItemsBtnState();
        }
        #endregion

        #region View State

        #region Moved Items
        private List<MovedItemModel> GetMovedItemsFromView()
        {
            var movedItems = GetViewState(SessionConstants.LeoMovedItems) as List<MovedItemModel> ??
                                new List<MovedItemModel>();
            if (movedItems.Count == 0)
            {
                SetViewState(movedItems, SessionConstants.LeoMovedItems);
            }
            return movedItems;
        }
        #endregion

        private List<OrderHistoryModel> GetOrderHistoryFromView()
        {
            var historyOrders = GetViewState(SessionConstants.LeoHistoryOrders) as List<OrderHistoryModel> ??
                                new List<OrderHistoryModel>();
            if (historyOrders.Count == 0)
            {
                SetViewState(historyOrders, SessionConstants.LeoHistoryOrders);
            }
            return historyOrders;
        }
        private List<ShortReportModel> GetReportFromView()
        {
            var reports = GetViewState(SessionConstants.LeoShortReports) as List<ShortReportModel> ?? new List<ShortReportModel>();
            if (reports.Count == 0)
            {
                SetViewState(reports, SessionConstants.LeoShortReports);
            }
            return reports;
        }
        #endregion

        #region Information Dialog

        private void PopupInfoDialog(string msg, bool isErr, bool onCloseEditUp = false)
        {
            EditDialogUp.Value = onCloseEditUp ? "Y" : "N";
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        private void PopupSpecInstrDialog(string msg)
        {
            SpecInstrText.Text = msg;
            SpecInstrPopupExtender.Show();
        }
        protected void OnCloseInfoDialogClick(object sender, EventArgs e)
        {
            //-- If Save fails
            if (EditDialogUp.Value == "Y")
            {
                EditItemPopupExtender.Show();
            }
        }

        #endregion

        #region Document List 
        private List<ShortReportModel> GetReportsByCheckedDocuments()
        {
            var srList = new List<ShortReportModel>();
            var reports = GetReportFromView();
            foreach (ListItem item in DocumentList.Items)
            {
                if (!item.Selected) continue;
                srList.AddRange(reports.FindAll(model => model.DocumentId == item.Value));
            }
            return srList;
        }
        private List<string> GetCheckedDocuments()
        {
            return (from ListItem item in DocumentList.Items where item.Selected select item.Value).ToList();
        }

        protected void OnDocumentSelected(object sender, EventArgs e)
        {
            LoadColumnsView();
            LoadItemNumbersTree1();
            
            //-- Reset Short Report
            ButtonsPanel.Visible = false;
            grdShortReport.DataSource = null;
            grdShortReport.DataBind(); 

        }
        #endregion

        #region Customer Program Details
        private void ShowCpDetails(BatchModel batchModel)
        {
            if (batchModel == null)
            {
                ShowCustomerProgram("", "");
                ShowPicture("");
                return;
            }
            if (batchModel.FullBatchNumber != DetailsBatchNumberFld.Value)
            {
                ShowCustomerProgram(batchModel.CpId, batchModel.FullBatchNumber);
                ShowPicture(batchModel.PathToPicture);
            }
        }
        #endregion
        
        #region Item Numbers Tree
        private void LoadItemNumbersTree1()
        {
            ItemNumberTree1.Nodes.Clear();
            
            var chkDocs = GetCheckedDocuments();
            if (chkDocs.Count == 0)
            {
                ShowCpDetails(null);
                return;
            }
            
            SetActionText(string.Format(MsgItemsTreeLoad));
            
            //-- Show Picture
            var reports = GetReportsByCheckedDocuments();
           
            //var reports = GetReportFromView().FindAll(m => m.DocumentId == chkDocs[0]);
            if (reports.Count > 0)
            {
                var firstBatch = reports.ToArray()[0].BatchModel;
                ShowCpDetails(firstBatch);
            }

            var orders = reports.GroupBy(m => m.BatchModel.GroupCode).ToList();
            foreach(var orderGrp in orders)
            {
                var order = Utils.FillToFiveChars(orderGrp.Key);
                var history = GetOrderHistoryFromView().Find(m => m.OrderCode == order);
                if (history == null) continue;
                var orderModel =
                    history.HistoryItems.Find(m => m.StateTarget.TargetCode == StateTargetModel.TrgCodeOrder);
                if (orderModel == null) continue;
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = orderModel.Id, DisplayName = orderModel.Title } }; // Id = CustomerCode + "_" + id;
                
                var batches = reports.FindAll(m => m.BatchModel.GroupCodeFiveChars == order);
                foreach (var batch in batches)
                {
                    var batchModel =
                        history.HistoryItems.Find(
                            m =>
                            m.StateTarget.TargetCode == StateTargetModel.TrgCodeBatch &&
                            m.BatchId == batch.BatchModel.BatchId);
                    if (batchModel == null) continue;
                    data.Add(new TreeViewModel { ParentId = batchModel.ParentId, Id = batchModel.Id, DisplayName = batchModel.TitleBatch }); // Id = CustomerCode + "_" + OrderCode + "." + BatchCode;
                    var items =
                        history.HistoryItems.FindAll(
                            m =>
                            m.StateTarget.TargetCode == StateTargetModel.TrgCodeItem && m.BatchId == batchModel.BatchId);
                    foreach(var item in items)
                    {
                        data.Add(new TreeViewModel() { ParentId = item.ParentId, Id = item.Id, DisplayName = item.Title }); // Id = OrderCode + "." + BatchCode + "." + ItemCode;
                        var docs = history.HistoryItems.FindAll(m => m.ParentId == item.Id);
                        foreach(var doc in docs)
                        {
                            data.Add(new TreeViewModel{ParentId = doc.ParentId, Id = doc.Id, DisplayName = doc.Title});
                        }
                    }
                }
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                ItemNumberTree1.Nodes.Add(rootNode);
                rootNode.Expand();

            }
        }
        private BatchModel GetBatchModelByItemNode(TreeNode node)
        {
            var keys = node.Value.Split('_')[1];
            if (node.Depth == 0)
            {
                //-- CustomerCode + "_" + OrderCode
                var report = GetReportFromView().Find(m => m.BatchModel.GroupCodeFiveChars == keys);
                return report == null ? null : report.BatchModel;
            }
            if (node.Depth == 1 || node.Depth == 2) //-- Batch
            {
                //-- CustomerCode + "_" + OrderCode + "." + BatchCode
                //-- CustomerCode + "_" + OrderCode + "." + BatchCode + "." + ItemCode
                var batchKeys = keys.Split('.');
                var report = GetReportFromView().Find(m => m.BatchModel.GroupCodeFiveChars == batchKeys[0] && m.BatchModel.BatchCodeTreeChars == batchKeys[1]);
                return report == null ? null : report.BatchModel;
            }
            if (node.Depth == 3) //-- Documents
            {
                //-- CustomerCode + "_" + OperationChar + GroupCode + "." + BatchCode + "." + ItemCode
                var batchKeys = keys.Substring(1).Split('.');
                var report = GetReportFromView().Find(m => m.BatchModel.GroupCodeFiveChars == batchKeys[0] && m.BatchModel.BatchCodeTreeChars == batchKeys[1]);
                return report == null ? null : report.BatchModel;
            }
            return null;
        }
        protected void OnTreeChanged(object sender, EventArgs e)
        {
            var node = ItemNumberTree1.SelectedNode;
            var batchModel = GetBatchModelByItemNode(node);
            ShowCpDetails(batchModel);
        }
        #endregion

        #region Display Picture & Customer Program 
        private void ShowCustomerProgram(string cpId, string batchNumber)
        {
            if (string.IsNullOrEmpty(cpId))
            {
                CpCommDiv.InnerHtml = "";
                CpDescDiv.InnerHtml = "";
                CpRef.Visible = false;
                CpRules.Visible = false;
                DetailsBatchNumberFld.Value = "";
                return;
            }
            var cpModel = QueryCpUtils.GetCustomerProgramByCpId(cpId, this);
            CpDescDiv.InnerHtml = "<strong>Description: </strong>" + cpModel.CommentDisplay;
            CpCommDiv.InnerHtml = "<strong>Comment: </strong>" + cpModel.DescriptionDisplay;
            CpRef.InnerHtml = "<strong>Customer Program: </strong>" + cpModel.CustomerProgramName + string.Format(" (CPID = {0})", cpId);
            CpRef.HRef = "~/CustomerProgramNew.aspx?BatchNumber=" + batchNumber;
            CpRef.Visible = true;
            CpRules.InnerHtml = "<strong>CPRules: </strong>" + cpModel.CustomerProgramName + string.Format(" (CPID = {0})", cpId);
            CpRules.HRef = "~/CPRulesDisplay2.aspx?CustomerProgram=" + cpModel.CustomerProgramName;
            CpRules.Visible = true;
            DetailsBatchNumberFld.Value = batchNumber;
            hdnSKU.Value = cpModel.CustomerProgramName;
        }
        private void ShowPicture(string dbPicture)
        {
			try
			{
				string imgPath = "";
				string errMsg = "";
				var ms = new MemoryStream();
				var fileType = "";
				var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
				//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
				if (result)
				{
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					itemPicture.ImageUrl = ImageUrl;
					itemPicture.Visible = true;
				}
				else
				{
					itemPicture.Visible = false;
				}
				//-- Path to picture
				var shortImagePath = imgPath.Replace(imgPath.Substring(0, imgPath.IndexOf("pictures")), "");

				CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", shortImagePath,
														string.IsNullOrEmpty(dbPicture) ? "" : "Cp details for " + DetailsBatchNumberFld.Value);
				if (errMsg.Contains("does not exist"))
				{
					errMsg = "File does not exists";
				}
				if (string.IsNullOrEmpty(dbPicture) || dbPicture.ToLower().Contains("default"))
				{
					errMsg = "";
				}
				ErrPictureField.Text = errMsg;
			}
			catch(Exception ex)
			{ var s = ex.Message; }
        }

        #endregion

        #region Build Button
        protected void OnBuildBtnClick(object sender, EventArgs e)
        {
            var checkItems = GetCheckedItems();
            if (!checkItems.Any())
            {
                PopupInfoDialog("Checked 'Item Numbers' Not Found.", true);
                return;
            }
            DisplayData();
            LoadLblDocuments();
            LoadEmails();
            SetActionText(MsgTableBuild);
        }
        private void LoadEmails()
        {
            var checkedReports = GetCheckedShortReports();
            var emails = QueryUtils.GetPersonsByBatchIds(checkedReports, this);
            MailAddressList.DataSource = emails.FindAll(m => !string.IsNullOrEmpty(m.Email));
            MailAddressList.DataBind();
            OnChoiceMailFromContacts(null, null);
        }
        protected void OnChoiceMailFromContacts(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MailAddressList.SelectedValue)) return;
            AddressTo.Text = MailAddressList.SelectedValue;
        }

        private void LoadLblDocuments()
        {
            AttLabelsList.Items.Clear();
            var checkedReports = GetCheckedShortReports();
            foreach(var report in checkedReports)
            {
                foreach(var lblDoc in report.LabelDocuments)
                {
                    var lblItem = new ListItem {Value = lblDoc.DocumentId, Text = lblDoc.DocumentName};
                    if(!AttLabelsList.Items.Contains(lblItem))
                    {
                        AttLabelsList.Items.Add(lblItem);
                    }
                }
            }
            if (AttLabelsList.Items.Count > 0) AttLabelsList.SelectedIndex = 0;
        }
        private void DisplayData()
        {
            SetActionText(MsgTableBuild);
            var dt = new ShortReportUtils(false).GetLeoReport(GetCheckedShortReports(), PageConstants.DirHorizontal, false, false/*HighlightOffCheckBox.Checked*/);
            if (HighlightOffCheckBox.Checked)
            {
                foreach(DataRow row in dt.Rows)
                {
                    for (int i=0; i < dt.Columns.Count; i++)
                    {
                        var val = "" + row[i];
                        val = val.Replace("text_highlitedyellow", "text_nohighlitedyellow");
                        row[i] = val;
                    }
                }
                dt.AcceptChanges();
            }
            //-- Remove columns
            var hidden = GetHiddenColumns();
            foreach(var col in hidden)
            {
                dt.Columns.Remove(col);
            }
            var param = "" + Session["RemoveEmptyCols"];
            if (param == "1")
            {
                List<string> colList = Utlities.GetEmptyColumns(dt);
                if (colList.Count > 0)
                {
                    foreach (var col in colList)
                        dt.Columns.Remove(col);
                }
            }
            dt.AcceptChanges();
            SetViewState(dt, SessionConstants.LeoDataTable);
            UpdateDataView(dt);
            ButtonsPanel.Visible = dt.Rows.Count > 0;
            CheckAllCheckBox.Checked = false;
        }

        private IEnumerable<string> GetHiddenColumns()
        {
            var hidden = new List<string>();
            foreach(DataGridItem item in ColumnsGrid.Items)
            {
                var title = ColumnsGrid.DataKeys[item.ItemIndex].ToString();
                var chkIsHidden = item.FindControl("HideCheckBox") as CheckBox;
                if (chkIsHidden != null && chkIsHidden.Checked) hidden.Add(title);
            }
            return hidden;
        }
        private List<ShortReportModel> GetCheckedShortReports()
        {
            var result = new List<ShortReportModel>();
            var batchNumbers = new List<string>();
            var itemNumbers = GetCheckedItems();
            foreach(var itemNumber in itemNumbers)
            {
                var batchNumber = itemNumber.Substring(0, itemNumber.Length - 2);
                if (batchNumbers.Contains(batchNumber)) continue;
                
                batchNumbers.Add(batchNumber);
                var order = "" + Utils.ParseOrderCode(itemNumber);
                var batchCode = "" + Utils.ParseBatchCode(itemNumber);
                var srModel =
                    GetReportFromView().Find(m => m.BatchModel.GroupCode == order && m.BatchModel.BatchCode == batchCode);
                foreach(var itemModel in srModel.Items)
                {
                    itemModel.SkipOnGeneration = !itemNumbers.Contains(itemModel.FullItemNumber);
                }
                result.Add(srModel);
            }
            return result;
        }
        private IEnumerable<string> GetCheckedItems()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in ItemNumberTree1.CheckedNodes)
            {
                if (node.Depth != 2) continue;//-- 0 - order, 1 - batch, 2 - item, 3 - document
                checkItems.Add(node.Value.Split('_')[1].Replace(".", ""));
            }
            return checkItems;
        }

        #endregion

        #region Order Reports button
        protected void OnOrderReportsClick(object sender, EventArgs e)
        {
            var items = GetCheckedItemsFromGrid();
            if (items.Count == 0)
            {
                PopupInfoDialog("Nothing to order!", true);
                return;
            }
            PopupInfoDialog("In developing...", false);
            var msg = QueryUtils.SetLeoOrderReports(items, this);
            if (string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog("Reports ordered.", false);
            } else
            {
                PopupInfoDialog(msg, true);
            }
        }
        private List<string> GetCheckedItemsFromGrid()
        {
            var items = new List<string>();
            foreach (DataGridItem item in grdShortReport.Items)
            {
                var checkBox = item.FindControl("Checkbox1") as CheckBox; 
                if (checkBox == null || !checkBox.Checked) continue;
                var itemNumber = GetItemNumberFromGrid(item);
                items.Add(itemNumber);
            }
            return items;
        }
        #endregion

        #region Label export to Excel and Download
        protected void OnDownLoadLabelClick(object sender, EventArgs e)
        {
            var items = GetCheckedItemsFromGrid();
            if (items.Count == 0)
            {
                PopupInfoDialog("Checked Items Not Found!", true);
                return;
            }
            var itemNumber = items.ElementAt(0);
            var reports = GetReportFromView().Where(r => r.Items.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
            if (reports.Count == 0) return;
            //-- Find Lbl document for item number
            if (reports[0].LabelDocuments.Count == 0)
            {
                PopupInfoDialog("Label document not found (" + itemNumber + ")!", true);
                return;
            }
            SetActionText(string.Format(MsgLabelToExcel, itemNumber));
            var lblDoc = reports[0].LabelDocuments[0];
            var docStruct = QueryCpUtils.GetDocumentValues(lblDoc.DocumentId, this);
            var isTlkw = lblDoc.CorelFile.ToUpper().Contains("ACC_REP_TLKW_LABEL.XLS");
            string err;
            if (isTlkw)
            {
                var models = PrintUtils.CreateLabelTlkw(reports[0], itemNumber, docStruct, out err, this);
                err = ExcelUtils.DownloadLabelTlkw(models, this);
            }
            else
            {
                var printLabelModel = PrintUtils.CreateLabelOldModel(reports[0], itemNumber, docStruct, out err, this);
                err = ExcelUtils.DownloadLabelOld(printLabelModel, this);

            }
            if (!string.IsNullOrEmpty(err))
            {
                PopupInfoDialog(err, true);
            }
        }
        #endregion

        #region Preview Labels button
        protected void OnPreviewLabelsClick(object sender, EventArgs e)
        {
            var items = GetCheckedItemsFromGrid();
            if (items.Count == 0)
            {
                PopupInfoDialog("Checked Items Not Found!", true);
                return;
            }
            var readedDocs = new List<string>();
            var errors = new List<string>();
            var labelModels = new List<PrintLabelModel>();
            foreach(var itemNumber in items)
            {
                var reports = GetReportFromView().Where(r => r.Items.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
                if (reports.Count == 0) continue;
                
                //-- Find Lbl document for item number
                if (reports[0].LabelDocuments.Count == 0)
                {
                    errors.Add("Label document not found (" + itemNumber + ")!");
                    continue;
                }
                var lblDoc = reports[0].LabelDocuments[0];

                if (!readedDocs.Contains(lblDoc.DocumentId))
                {
                    var docStruct = QueryCpUtils.GetDocumentValues(lblDoc.DocumentId, this);
                    readedDocs.Add(lblDoc.DocumentId);
                    SetViewState(docStruct, "LblDoc" + lblDoc.DocumentId);
                }
                var docDefine = GetViewState("LblDoc" + lblDoc.DocumentId) as List<DocumentValueModel> ?? new List<DocumentValueModel>();

                string err;
                var isTlkw = lblDoc.CorelFile.ToUpper().Contains("ACC_REP_TLKW_LABEL.XLS");
                if (isTlkw)
                {
                    var models  = PrintUtils.CreateLabelTlkw(reports[0], itemNumber, docDefine, out err, this);
                    if (!string.IsNullOrEmpty(err)) errors.Add(err);
                    labelModels.AddRange(models);
                }
                else
                {
                    var printLabelModel = PrintUtils.CreateLabelOldModel(reports[0], itemNumber, docDefine, out err, this);
                    if (!string.IsNullOrEmpty(err))
                    {
                        errors.Add(err);
                    } else
                    {
                        labelModels.Add(printLabelModel);
                    }

                }
            }
            var cntOld = labelModels.FindAll(m => !m.IsTlkw).Count;
            var cntTlkw = labelModels.FindAll(m => m.IsTlkw).Count;
            if (cntOld > 0 && cntTlkw > 0)
            {
                PopupInfoDialog("Different lbl document styles", true);
                return;
            }
            SetActionText(MsgLabelPreview);
            if (LetterBox.Checked)
            {
                LoadLabels2Grid(labelModels);
            }
            else {
                LoadLabelsGrid(labelModels);
            }
            

        }
        #endregion

        #region Excel button
        private DataTable GenerateShortReportTable(bool forEmail, out string filename)
        {
            var srList = GetCheckedShortReports();
            filename = "ShortReport";
            var orders = srList.GroupBy(m => m.BatchModel.GroupCodeFiveChars);
            foreach (var order in orders)
            {
                filename += "_"+ order.Key;
            }
            if (srList == null || srList.Count == 0) return null;
            var dataTable = new ShortReportUtils(forEmail).GetLeoReport(srList, PageConstants.DirHorizontal, false, HighlightOffCheckBox.Checked);
            if (dataTable == null) return null;
            var param = "" + Session["RemoveEmptyCols"];
            if (param == "1")
            {
                List<string> colList = Utlities.GetEmptyColumns(dataTable);
                if (colList.Count > 0)
                {
                    foreach (var col in colList)
                        dataTable.Columns.Remove(col);
                    dataTable.AcceptChanges();
                }
            }
            var table = dataTable.Copy();
            if (table.Columns.Contains(PageConstants.ColNameLeoOrdered))
            {
                table.Columns.Remove(PageConstants.ColNameLeoOrdered);
            }
            if (table.Columns.Contains(PageConstants.ColNameLeoPrinted))
            {
                table.Columns.Remove(PageConstants.ColNameLeoPrinted);
            }
            if (table.Columns.Contains("Order"))
            {
                table.Columns.Remove("Order");
            }
            var dv = new DataView { Table = table };

            // Apply sort information to the view
            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null && dv.Table.Columns.Contains(sortModel.ColumnName))
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }

            var result = table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < dv.Count; i++)
            {
                result.Rows.Add(dv[i].Row.ItemArray);
            }
            result.AcceptChanges();
            return result;
        }
        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            var fname = ""; 
            var result = GenerateShortReportTable(false, out fname);
			
            ExcelUtils.ExportThrouPoi(result, fname, this);
        }       
        #endregion

        #region Send Email with attached Short report
        protected void OnSendEmailClick(object sender, ImageClickEventArgs e)
        {
            string custCode = hdnCustomerCode.Value;
            string order = hdnOrder.Value;
            string sku = hdnSKU.Value;
            string toNY = "";
            if (sender == null && e == null)
            {
                toNY = custCode + ";" + order + ";" + sku;
                if (SpecInstrText.Text != "")
                    toNY += ";" + SpecInstrText.Text;
            }
            string toAddress = "";
            if (sender == null && e == null)
                toAddress = NYAddress.Text.Trim();
            else
            {
                toAddress = AddressTo.Text.Trim();
            }
            if (string.IsNullOrEmpty(toAddress))
            {
                PopupInfoDialog("Please enter a 'Address to'!", true);
                return;
            }
            var fname = ""; 
            var result = GenerateShortReportTable(true, out fname);
            if (result == null || result.Rows.Count == 0)
            {
                PopupInfoDialog("A Short report is empty!", true);
                return;
            }
            var dss = new DataSet();
            dss.Tables.Add(result);

			//-- Generate excel file
			
            ExcelUtils.ExportThrouPoi(dss, fname, true, this);

            var msg = ExcelUtils.SendMailWithAttachExcel(toAddress, fname, this, toNY);
            SetActionText(MsgEmailSent);
            PopupInfoDialog(msg, msg != "OK");
        }
        #endregion

        #region Sort DataGrid Command
        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);

            UpdateDataView(GetViewState(SessionConstants.LeoDataTable) as DataTable);
        }
        void UpdateDataView(DataTable table)
        {
            if (table == null)
            {
                grdShortReport.DataSource = null;
                grdShortReport.DataBind();
                return;
            }
            var dv = new DataView { Table = table };
            if (grdShortReport.AllowSorting)
            {
                // Apply sort information to the view
                var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
                if (sortModel != null && table.Columns.Contains(sortModel.ColumnName))
                {
                    dv.Sort = sortModel.ColumnName;
                    if (!sortModel.Direction) dv.Sort += " DESC";
                }
                else
                {
                    dv.Sort = PageConstants.ColNameLinkOnItemView;
                    SetViewState(new SortingModel { ColumnName = PageConstants.ColNameLinkOnItemView, Direction = true }, SessionConstants.CurrentSortModel);
                }
            }

            // Rebind data 
            grdShortReport.DataSource = dv;
            grdShortReport.DataBind();
        }

        #endregion
        #region Moved Items Dialog
        #endregion
        #region Show/Hide columns
        protected void OnHideColumnsClick(object sender, EventArgs e)
        {
            SetActionText(MsgShowHide);
            var h = Convert.ToInt32(ForHeightFld.Value);
            ShowHideDiv.Attributes.CssStyle.Value = string.Format(DlgDivFormat, h);
            ShowHideTitle.Text = "Show/Hide columns";
            HideColumnsPopupExtender.Show();
        }

        protected void OnHideColumnsOkClick(object sender, EventArgs e)
        {
            DisplayData();
            HideColumnsPopupExtender.Show();
        }
        #endregion

        #region Expand/Collapse Filter Panel
        protected void OnCollapseBtnClick(object sender, ImageClickEventArgs e)
        {
            TopPanel.Visible = false;
            CollapseBtn.Visible = false;
            ExpandBtn.Visible = true;
            SetActionText("Top panel collapsed");
        }
        protected void OnExpandBtnClick(object sender, ImageClickEventArgs e)
        {
            TopPanel.Visible = true;
            CollapseBtn.Visible = true;
            ExpandBtn.Visible = false;
            SetActionText("Top panel expanded");
        }
        #endregion

        #region Edit Item dialog
        protected void OnEditItemOkClick(object sender, EventArgs e)
        {
            var itemValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ??
            new List<ItemValueEditModel>();
            if (itemValues.Count == 0) return;
            var editingItem = itemValues.ElementAt(0).EditingItem;
            SetActionText(string.Format(MsgSaveItem, editingItem.FullItemNumber));
            var batchItemModel = new BatchItemModel
                                     {BatchId = editingItem.BatchId, ItemCode = Convert.ToInt32(editingItem.ItemCode)};
            var changedValues = new List<ItemValueEditModel>();
            foreach(DataGridItem item in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[item.ItemIndex];
                var partId = key.Split(';')[0];
                var measureId = key.Split(';')[1];
                var itemValue = itemValues.Find(m => m.PartId == partId && ""+m.MeasureId == measureId);
                if (itemValue == null) continue;
                var newValue = "";
                if (itemValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enumFld = item.FindControl(ValueEnumFld) as DropDownList;
                    if (enumFld != null) newValue = enumFld.SelectedValue;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                    if (numericFld != null) newValue = numericFld.Text;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassText)
                {
                    var stringFld = item.FindControl(ValueStringFld) as TextBox;
                    if (stringFld != null) newValue = stringFld.Text;
                }
                if (!itemValue.HasChanged(newValue)) continue;
                var cloneModel = itemValue.Clone();
                cloneModel.Value = newValue;
                changedValues.Add(cloneModel);
            }
            if (changedValues.Count == 0)
            {
                PopupInfoDialog("No changes", true, true);
                return;
            }
            var updateModel = new BulkUpdateMeasureModel();
            foreach(var value in changedValues)
            {
                updateModel.MeasureValues.Add(new MeasureValueModel(value));
            }
            updateModel.ItemTypeId = editingItem.ItemTypeId;
            updateModel.Items.Add(batchItemModel);
            var msg = QueryUtils.SaveMeasureValues(updateModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true, true);
                return;
            }
            
            //-- Refresh item data and rebuild table
            var newValues =
                QueryUtils.GetItemValues(
                    new BatchModel {GroupCode = editingItem.OrderCode, BatchCode = editingItem.BatchCode}, this);
            var forRefresh = GetReportFromView().Find(m => m.BatchModel.BatchId == ""+editingItem.BatchId);
            if (forRefresh == null) return;
            forRefresh.ItemValues = newValues;
            OnBuildBtnClick(null, null);
            PopupInfoDialog("Changes were updated successfully", false);
        }
        protected void OnEditItemCommand(object source, DataGridCommandEventArgs e)
        {
            if ((e.CommandSource as LinkButton) == null) return;
            var key = GetItemNumberFromGrid(e.Item);
            SetActionText(string.Format(MsgEditItem, key));
            List<string> unknownMeasures;
            var itemValues = GetMeasuresForEdit(key, out unknownMeasures);
            SetViewState(itemValues, SessionConstants.ShortReportExtMeasuresForEdit);
            ItemValuesGrid.DataSource = itemValues;
            ItemValuesGrid.DataBind();
            EditPanelTitle.Text = "Data for Item " + key;
            UnknownMeasureList.DataSource = unknownMeasures;
            UnknownMeasureList.DataBind();
            
            //-- Open Edit Item dialog
            EditingItemKeyFld.Value = key;
            var h = Convert.ToInt32(ForHeightFld.Value);
            EditItemDiv.Attributes.CssStyle.Value = string.Format(DlgDivFormat, h);
            EditItemPopupExtender.Show();

        }

        private const string ValueStringFld = "ValueStringFld";
        private const string ValueNumericFld = "ValueNumericFld";
        private const string ValueEnumFld = "ValueEnumFld";

        protected void OnEditItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //-- Load dataon controls
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            return;
            var editModel = e.Item.DataItem as ItemValueEditModel;
            if (editModel == null) return;
            var stringFld = e.Item.FindControl(ValueStringFld) as TextBox;
            var numericFld = e.Item.FindControl(ValueNumericFld) as TextBox;
            var enumFld = e.Item.FindControl(ValueEnumFld) as DropDownList;

            if (stringFld == null || numericFld == null || enumFld == null) return;
            if (editModel.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                /*IvanB 15/03 start*/
                if (editModel.MeasureName.Equals("Shape (cut)"))
                {
                    enumFld.Attributes.Add("class", "filtered-select");
                }
                /*IvanB 15/03 end*/
                enumFld.DataSource = editModel.EnumSource;
                enumFld.DataBind();
                if (string.IsNullOrEmpty(editModel.Value))
                {
                    enumFld.SelectedIndex = -1;
                } else
                {
                    enumFld.SelectedValue = editModel.Value;
                }
                enumFld.Visible = true;
                stringFld.Visible = false;
                numericFld.Visible = false;
                return;
            } 
            if (editModel.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                numericFld.Text = editModel.Value;
                enumFld.Visible = false;
                stringFld.Visible = false;
                numericFld.Visible = true;
                return;
            }
            stringFld.Text = editModel.Value;
            enumFld.Visible = false;
            stringFld.Visible = true;
            numericFld.Visible = false;


        }
        private List<ItemValueEditModel> GetMeasuresForEdit(string itemNumber, out List<string> unknownMeasures)
        {
            //alex 10182023
            //var enumsMeasure = GetEnumMeasure();
            var AllEnumsMeasure = GetEnumMeasure();
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
            List<EnumMeasureModel> enumsMeasure = new List<EnumMeasureModel>();
            foreach (var eMeasure in AllEnumsMeasure)
            {
                var value = eMeasure.ValueTitle;
                var blockedValue = blockList.Find(x => x.BlockedDisplayName == value);
                if (blockedValue == null || eMeasure.MeasureValueMeasureId != 8)
                {
                    enumsMeasure.Add(eMeasure);
                }
            }
            //alex 10182023
            var order = "" + Utils.ParseOrderCode(itemNumber);
            var batchCode = "" + Utils.ParseBatchCode(itemNumber);
            var itemCode = "" + Utils.ParseItemCode(itemNumber);
            var shortReport = GetReportFromView().Find(m => m.BatchModel.GroupCode == order && m.BatchModel.BatchCode == batchCode);
            var itemModel = new SingleItemModel(shortReport.BatchModel, itemCode);
            
            var measures = QueryUtils.GetMeasureListByAcces(itemModel, "", true, this);
            
            var parts = QueryUtils.GetMeasureParts(Convert.ToInt32(shortReport.BatchModel.ItemTypeId), this);
            var measureValues = QueryUtils.GetMeasureValues(new SingleItemModel(shortReport.BatchModel, itemCode), "",
                                                            this);
            var pairs = GetMeasuresFromDocument(shortReport.DocumentId);
            var result = new List<ItemValueEditModel>();
            unknownMeasures = new List<string>();
            foreach(var pair in pairs)
            {
                var partName = pair.Split('.')[0].ToUpper();
                var measureName = pair.Split('.')[1].ToUpper();
                var partModel = parts.Find(m => m.PartName.ToUpper() == partName);
                if (partModel != null)
                {
                    var measureModel =
                        measures.Find(m => m.PartId == partModel.PartId && (m.MeasureTitle.ToUpper() == measureName) );
                    if (measureModel == null)
                    {
                        if (!unknownMeasures.Contains(pair)) unknownMeasures.Add(pair);
                        continue;
                    }
                    var valueModel =
                        measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == measureModel.MeasureId);
                    var enums = (measureModel.MeasureClass == MeasureModel.MeasureClassEnum
                        ? enumsMeasure.FindAll(m => ""+m.MeasureValueMeasureId == measureModel.MeasureId) : null);                
                    result.Add(new ItemValueEditModel(partModel, measureModel, valueModel, enums, itemModel, true));
                } else
                {
                    if (!unknownMeasures.Contains(pair)) unknownMeasures.Add(pair);
                }
            }
            //--- Add measures not including in document structure
            foreach(var measure in measures)
            {
                if (measure.MeasureClass > 3) continue;
                var partModel = parts.Find(m => m.PartId == measure.PartId);
                if (partModel == null) continue;
                var addedMeasure =
                    result.Find(m => "" + m.MeasureId == measure.MeasureId && m.PartId == "" + measure.PartId);
                if (addedMeasure == null)
                {
                    var valueModel =
                        measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == measure.MeasureId);
                    var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                        ? enumsMeasure.FindAll(m => ""+m.MeasureValueMeasureId == measure.MeasureId) : null);
                    result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
                }
            }
            unknownMeasures.Sort((m1,m2) => string.CompareOrdinal(m1, m2));
            result.Sort(new ItemValueEditComparer().Compare);
            //IvanB 11/03 start
            var blockShapesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
            var shapes = result.FindAll(m => m.MeasureName.Equals("Shape (cut)"));
            ItemErrorLbl.Text = "";
            if (shapes != null && shapes.Count >0)
            {
                foreach (var shapeEnum in shapes)
                {
                    try
                    {
                        
                        var selectedShape = shapeEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(shapeEnum.Value)).ValueTitle;
                        shapeEnum.EnumSource = shapeEnum.EnumSource.Where(x => !blockShapesList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedShape.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                        //Remove duplicates
                        
                    }
                    catch
                    {
                        //ErrorLabel.Text = "Wrong shape for " + shapeEnum.PartName;
                        ItemErrorLbl.Text = "Wrong shape for " + shapeEnum.PartName;
                        //alex 06/21/2024
                        continue;
                        //alex 06/21/2024
                    }
                    shapeEnum.EnumSource = shapeEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }                
            }
            //IvanB 11/03 end
            return result;
        }
        #endregion

        #region Save procedure for changed measures
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {

        }
        #endregion

        #region Utilities
        private string GetItemNumberFromGrid(DataGridItem dgItem)
        {
            var itemNumber = "" + grdShortReport.DataKeys[dgItem.ItemIndex];
            itemNumber = Regex.Replace(itemNumber, @"<[^>]+>", "");
            itemNumber = itemNumber.Replace(".", "");
            return itemNumber;
        }
        private string GetDotItemNumberFromGrid(DataGridItem dgItem)
        {
            var itemNumber = "" + grdShortReport.DataKeys[dgItem.ItemIndex];
            itemNumber = Regex.Replace(itemNumber, @"<[^>]+>", "");
            return itemNumber;
        }
        private List<PrintedModel> GetPrintedDocsByItemNumber(string itemNumber)
        {
            var reports = GetReportFromView().Where(r => r.ItemsPrinted.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
            return reports.Count == 0 ? new List<PrintedModel>() : reports[0].ItemsPrinted.FindAll(m => m.FullItemNumber == itemNumber);
        }
        private ItemModel GetItemModelFromView(DataGridItem dgItem)
        {
            var itemNumber = GetItemNumberFromGrid(dgItem);
            var reports = GetReportFromView().Where(r => r.Items.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
            if (reports.Count == 0) return null;
            var report = reports[0];
            return report.Items.Find(m => m.FullItemNumber == itemNumber);
        }
        private StateTargetModel GetItemStateModelByItemNumber(string dotItemNumber)
        {
            var history = GetOrderHistoryFromView().Where(h => h.HistoryItems.FindAll(m => m.TargetCode == StateTargetModel.TrgCodeItem && m.Id.Contains(dotItemNumber)).Count > 0).ToList();
            if (history.Count == 0) return null;
            var itemHistory = history[0].HistoryItems.Find(
                m => m.TargetCode == StateTargetModel.TrgCodeItem && m.Id.Contains(dotItemNumber));

            return itemHistory.StateTarget;
        }
        private IEnumerable<DocumentModel> GetLabelDocsByItemNumber(string itemNumber)
        {
            var reports =
                GetReportFromView().Where(r => r.Items.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
            if (reports.Count == 0)
            {
                return new List<DocumentModel>{new DocumentModel()};
            }
            return reports[0].LabelDocuments;
        }
        #endregion

        #region Short Report DataGrid
        protected void OnDataItemBinding(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            //-- Column "Printed"
            var itemNumber = GetItemNumberFromGrid(e.Item);
            var printedDocs = GetPrintedDocsByItemNumber(itemNumber);
            var infoBtn = e.Item.FindControl("ShowPrintInfoBtn") as ImageButton;
            if (infoBtn != null) infoBtn.Visible = printedDocs.Count > 0;

            var pdfBtn = e.Item.FindControl("ShowPdfBtn") as ImageButton;
            var itemModel = GetItemModelFromView(e.Item);
            if (pdfBtn != null)
            {
                pdfBtn.Visible = itemModel != null && itemModel.PdfFile != null;
            }

            //-- Column "State"
            var dotItemNumber = GetDotItemNumberFromGrid(e.Item);
            var stateModel = GetItemStateModelByItemNumber(dotItemNumber);
            if (stateModel != null)
            {
                var stateImg = e.Item.FindControl("ItemStateImage") as Image;
                if (stateImg != null)
                {
                    stateImg.ToolTip = stateModel.ImageTooltip;
                    stateImg.ImageUrl = stateModel.ImageUrl;
                    string failStatus = Session["SRnoPrintStatus"].ToString();
                    if (failStatus == "1")
                    {
                        if (printedDocs.Count == 0)
                        {
                            stateImg.ToolTip = "failed";
                            stateImg.ImageUrl = @"Images/stateImages/5.png";
                        }
                    }
                }
            }

            //-- Column "Lbl Doc"
            var downloadBtn = e.Item.FindControl("LblDocViewBtn") as ImageButton;
            if (downloadBtn == null) return;
            //MainPanel.Triggers.Add(new AsyncPostBackTrigger{ControlID = downloadBtn.ClientID, EventName = "Click"});
            
            downloadBtn.Visible = stateModel != null && stateModel.StateCode != 3; // 3 - Invalid state
            var lblDocs = GetLabelDocsByItemNumber(itemNumber);
            downloadBtn.ToolTip = "View Lbl document '" + lblDocs.ElementAt(0).DocumentName + "'";
//            var lblDocsFld = e.Item.FindControl("LabelDocsList") as DropDownList;
//            if (lblDocsFld == null) return;
//            lblDocsFld.DataSource = GetLabelDocsByItemNumber(itemNumber);
//            lblDocsFld.DataBind();

        }
        #endregion
   
        #region Show Printed Reports Info
        protected void OnShowPrintInfoClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;

            var itemNumber = GetItemNumberFromGrid(item);
            var printed = GetPrintedDocsByItemNumber(itemNumber);
            if (printed.Count == 0) return;
            SetActionText(string.Format(MsgPrintShow, itemNumber));
            PrintedDocsTitle.Text = string.Format("Printed documents for {0}", itemNumber);
            PrintedDocsGrid.DataSource = printed;
            PrintedDocsGrid.DataBind();
            PrintedDocsPopupExtender.Show();
        }
        #endregion

        #region Show PDF File
        protected void OnShowPdfClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            var itemModel = GetItemModelFromView(item);
            if (itemModel == null) return;
            SetActionText(string.Format(MsgPdfView, itemModel.FullItemNumber));
            PdfFrame.Attributes["src"] = itemModel.PdfFile.VirtualPath;
            PdfFrame.DataBind();
            PdfName.Text = itemModel.PdfFile.FileName;
            PdfPopupExtender.Show();

        }
        #endregion

        #region "Check All" checkbox
        protected void OnCheckAllClick(object sender, EventArgs e)
        {
            foreach (DataGridItem item in grdShortReport.Items)
            {
                var checkBox = item.FindControl("Checkbox1") as CheckBox;
                if (checkBox == null) continue;
                checkBox.Checked = CheckAllCheckBox.Checked;
            }
            SetActionText(CheckAllCheckBox.Checked ? MsgCheckAll : MsgUnCheckAll);
        }
        #endregion

        #region View Label Document structure
        protected void OnLblDocViewClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            var itemNumber = GetItemNumberFromGrid(item);
            var reports = GetReportFromView().Where(r => r.Items.FindAll(m => m.FullItemNumber == itemNumber).Count > 0).ToList();
            if (reports.Count == 0) return;
            
            //-- Find Lbl document for item number
            if (reports[0].LabelDocuments.Count == 0)
            {
                PopupInfoDialog("Label document not found!", true);
                return;
            }
            var lblDoc = reports[0].LabelDocuments[0];
            var docStruct = QueryCpUtils.GetDocumentValues(lblDoc.DocumentId, this);
            LblDocGrid.DataSource = docStruct;
            LblDocGrid.DataBind();
            LblDocTitle.Text = "Lbl document: " + lblDoc.DocumentName;
            LblDocPopupExtender.Show();
        }
        #endregion

        #region Preview and Print Labels for checked item numbers on Grid
        protected void OnLabelOldPrintClick(object sender, EventArgs e)
        {
            LabelOldPopupExtender.Show();
        }
        protected void OnLabelOld2PrintClick(object sender, EventArgs e)
        {
            LabelOld2PopupExtender.Show();
        }
        protected void OnLabelTlkwPrintClick(object sender, EventArgs e)
        {
            LabelTlkwPopupExtender.Show();
        }
        protected void OnLabelTlkw2PrintClick(object sender, EventArgs e)
        {
            LabelTlkw2PopupExtender.Show();
        }

        protected void OnPreviewLabelDataBound(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
            var labelModel = e.Item.DataItem as PrintLabelModel;
            if (labelModel == null) return;
            foreach(var val in labelModel.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName) as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
        }
        protected void OnPreviewLabelDataBound2(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
            var label2Model = e.Item.DataItem as PrintLabel2Model;
            if (label2Model == null) return;
            foreach (var val in label2Model.LabelOne.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName +"_l") as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
            foreach (var val in label2Model.LabelTwo.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName + "_r") as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
        }
        private void LoadLabels2Grid(List<PrintLabelModel> labelModels)
        {
            var labels2 = PrintUtils.Convert2ColumnsFormat(labelModels);
            if (labelModels.ToArray()[0].IsTlkw)
            {
                LabelTlkw2Grid.DataSource = labels2;
                LabelTlkw2Grid.DataBind();
                LabelTlkw2PopupExtender.Show();
            }
            else
            {
                LabelOld2Grid.DataSource = labels2;
                LabelOld2Grid.DataBind();
                LabelOld2PopupExtender.Show();
            }
        }
        private void LoadLabelsGrid(List<PrintLabelModel> labelModels)
        {
            if (labelModels.ToArray()[0].IsTlkw)
            {
                LabelTlkwGrid.DataSource = labelModels;
                LabelTlkwGrid.DataBind();
                LabelTlkwPopupExtender.Show();
            }
            else
            {
                LabelOldGrid.DataSource = labelModels;
                LabelOldGrid.DataBind();
                LabelOldPopupExtender.Show();
            }
        }

        #endregion
        
        #region Attached Label Documents
        protected void OnLookLblDocClick(object sender, ImageClickEventArgs e)
        {
            if (AttLabelsList.SelectedItem == null)
            {
                PopupInfoDialog("LBL document is not known", false);
                return;
            }
            SetActionText(MsgLblDocument);
            var docStruct = QueryCpUtils.GetDocumentValues(AttLabelsList.SelectedValue, this);
            LblDocGrid.DataSource = docStruct;
            LblDocGrid.DataBind();
            LblDocTitle.Text = "Lbl document: " + AttLabelsList.SelectedItem.Text;
            LblDocPopupExtender.Show();


        }
        #endregion

        protected void OnClearAllClick(object sender, EventArgs e)
        {
            OnRemoveAllOrders();
            SetActionText(MsgClearAll);
            LastActionLbl.ForeColor = System.Drawing.Color.DarkBlue;


            //New code to deal with memory leak issue test
            /*
			List<DataGrid> dataGrids = new List<DataGrid> {grdShortReport, PrintedDocsGrid, ColumnsGrid, ItemValuesGrid, LabelOldGrid, LabelOld2Grid,
				LabelTlkwGrid, LabelTlkw2Grid, LblDocGrid, EnteredGrid, MovedItemsGrid };

			foreach(var item in dataGrids)
			{
				item.DataSource = null;
				item.DataBind();
			}
			*/
        }

        protected void OnHighlightOffClick(object sender, EventArgs e)
        {
            //-- Rebuild table
            DisplayData();
            SetActionText(HighlightOffCheckBox.Checked ? MsgYellowHide : MsgYellowShow);
        }
       
        #region Bulk Update Panel
        
        #region Bulk Update open
        protected void OnBulkUpdateBtnClick(object sender, EventArgs e)
        {
            if (ItemNumberTree1.Nodes.Count == 0)
            {
                PopupInfoDialog("'Item Numbers' is empty", true);
                return;
            }
            //-- Get ItemTypes Collection
            //SetActionText(MsgBulkUpdateOpen);
            
            var ids = new List<string>();
            var itemTypeGroups = GetReportsByCheckedDocuments().GroupBy(m => m.BatchModel.ItemTypeId);
            foreach (var itemTypeGroup in itemTypeGroups)
            {
                ids.Add(""+ itemTypeGroup.Key);
            }
            var itemTypes = QueryCpUtils.GetItemTypesByIds(ids, this);
            BulkInitOnLoad(itemTypes);
        }
        #endregion

        #region BulkUpdate dialog preparation
        private void BulkUpdateOnLoad()
        {
            var orderCode = "";
            var batchCode = "";
            //-- Load Document Parts
            var checkedItems = BulkUpdateItems.CheckedNodes;    //-- 0 - order, 1 - batch, 2 - item
            foreach(TreeNode chkItem in checkedItems)
            {
                if (chkItem.Depth != 2) continue;
                if (chkItem.Parent == null) continue;
                var keys = chkItem.Parent.Value.Split(';');     //order + ";" + batch.BatchModel.BatchId
                orderCode = keys[0];
                batchCode = keys[1];
                var report =
                    GetReportFromView().Find(
                        m => m.BatchModel.GroupCode == orderCode && m.BatchModel.BatchCode == batchCode);
                if (report == null) return;
                SetViewState(report.BatchModel, SessionConstants.BulkFirstBatch);
                break;
            }
            
            var parts = QueryUtils.GetPartsMeasure(orderCode, batchCode, this);
            SetViewState(parts, SessionConstants.BulkParts);
            PartsListField.DataSource = parts;
            PartsListField.DataBind();

            //-- Clear preview measures
            SetViewState(null, SessionConstants.BulkMeasures);
            SetViewState(null, SessionConstants.BulkEnteredValues);
            BulkItemOkBtn.Enabled = false;
            if (!string.IsNullOrEmpty(PartsListField.SelectedValue))
            {
                OnBulkPartChanged(null, null);
            }
            else
            {
                ShowValuePanels(0);
            }
            ShowEnteredValues();
            BulkItemPopupExtender.Show();
        }
        #endregion

        #region Parts of BulkUpdate
        protected void OnBulkPartChanged(object sender, EventArgs e)
        {
            var batch = GetViewState(SessionConstants.BulkFirstBatch) as BatchModel;
            if (batch == null)
            {
                MeasureListField.Items.Clear();
                return;
            }
            //EnteredGrid.DataSource = new List<BulkUpdateViewModel>();
            //EnteredGrid.DataBind();
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel> ?? new List<MeasureModel>();
            if (measures.Count == 0)
            {
                measures = QueryUtils.GetMeasures(batch, false, this);
                SetViewState(measures, SessionConstants.BulkMeasures);
            }
            var measuresByType = measures.FindAll(m => m.PartId == GetPartIdSelected());

            measuresByType.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));

            MeasureListField.DataSource = measuresByType;
            MeasureListField.DataBind();
            if (!string.IsNullOrEmpty(MeasureListField.SelectedValue))
            {
                OnBulkMeasureChanged(null, null);
            }
            else
            {
                ShowValuePanels(0);
            }
            BulkItemPopupExtender.Show();
        }
        private int GetPartIdSelected()
        {
            var value = PartsListField.SelectedValue;
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
        }

        public PartModel GetPartModel(int partId)
        {
            var parts = GetViewState(SessionConstants.BulkParts) as List<PartModel> ?? new List<PartModel>();
            return parts.Find(m => m.PartId == partId);
        }


        #endregion

        #region Measures of BulkUpdate
        protected void OnBulkMeasureChanged(object sender, EventArgs e)
        {
            var measure = GetMeasureSelected();
            if (measure == null)
            {
                return;
            }
            if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                SetEnumData(measure.MeasureId);
            }
            ShowValuePanels(measure.MeasureClass);
            BulkItemPopupExtender.Show();
        }
        private MeasureModel GetMeasureSelected()
        {
            var value = MeasureListField.SelectedValue;
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel> ?? new List<MeasureModel>();
            return measures.Find(m => m.MeasureId == Convert.ToInt32(value));
        }
        #endregion

        #region Enum Measures of Bulk Update
        private void SetEnumData(int measureId)
        {
            var enumMeasures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
            var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
            if (enums.Find(m => m.MeasureValueId == 0) == null)
            {
                enums.Add(EnumMeasureModel.GetEmptyItem(measureId));
            }
            enums.Sort((m1, m2) => m1.NOrder.CompareTo(m2.NOrder));
            MeasureEnumField.DataSource = enums;
            MeasureEnumField.DataBind();
        }

        protected void OnBulkEnumMeasureChanged(object sender, EventArgs e)
        {
            if (MeasureEnumField.SelectedValue == "0")
            {
                //-- Value not set
                return;
            }
            AddEnteredValue(MeasureEnumField.SelectedValue, MeasureEnumField.SelectedItem.Text);
            BulkItemPopupExtender.Show();
        }
        #endregion

        #region Entered Values of BulkUpdate
        private List<BulkUpdateViewModel> GetEnteredValues()
        {
            if (GetViewState(SessionConstants.BulkEnteredValues) == null)
            {
                SetViewState(new List<BulkUpdateViewModel>(), SessionConstants.BulkEnteredValues);
            }
            return GetViewState(SessionConstants.BulkEnteredValues) as List<BulkUpdateViewModel> ??
                         new List<BulkUpdateViewModel>();
        }
        private void AddEnteredValue(object value, string valueTitle)
        {
            var values = GetEnteredValues();
            var partId = GetPartIdSelected();
            var partModel = GetPartModel(partId);
            if (partModel == null)
            {
                BulkItemPopupExtender.Show();
                return;
            }
            var measure = GetMeasureSelected();
            var enteredValue = values.Find(m => m.MeasureValue.PartId == partId && m.MeasureValue.MeasureId == "" + measure.MeasureId);
            if (enteredValue == null)
            {
                enteredValue = new BulkUpdateViewModel();
                enteredValue.MeasureValue.MeasureClass = measure.MeasureClass;
                enteredValue.MeasureValue.MeasureId = "" + measure.MeasureId;
                enteredValue.MeasureName = measure.MeasureName;

                enteredValue.MeasureValue.PartId = partModel.PartId;
                enteredValue.PartName = partModel.PartName;

                values.Add(enteredValue);
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassText)
            {
                enteredValue.MeasureValue.StringValue = "" + value;
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                enteredValue.MeasureValue.MeasureValue = "" + value;
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                enteredValue.MeasureValue.MeasureValueId = "" + value;
                enteredValue.MeasureValue.EnumValueTitle = valueTitle;
            }
            ShowEnteredValues();
        }
        private void ShowEnteredValues()
        {
            var values = GetEnteredValues();

            EnteredGrid.DataSource = values.Count > 0 ? values : new List<BulkUpdateViewModel>();
            EnteredGrid.DataBind();
            BulkItemOkBtn.Enabled = values.Count > 0;
            BulkItemPopupExtender.Show();
        }
        #endregion

        #region Numeric/Text Fields of BulkUpdate
        protected void OnBulkNumFieldChanged(object sender, EventArgs e)
        {
            AddEnteredValue(MeasureNumericField.Text, "");
            BulkItemPopupExtender.Show();
        }
        protected void OnBulkTxtFieldChanged(object sender, EventArgs e)
        {
            AddEnteredValue(MeasureTextField.Text, "");
            BulkItemPopupExtender.Show();
        }
        #endregion

        #region Save on BulkUpdate
        protected void OnBulkUpdateSaveClick(object sender, EventArgs e)
        {
            var batchModel = GetViewState(SessionConstants.BulkFirstBatch) as BatchModel;
            if (batchModel == null)
            {
                BulkItemPopupExtender.Show();
                return;
            }
            var checkedItems = GetBulkCheckedItems();
            var enteredValues = GetEnteredValues();
            var valuesList = enteredValues.Select(value => value.MeasureValue).ToList();
            var updateModel = new BulkUpdateMeasureModel
            {
                Items = checkedItems,
                MeasureValues = valuesList,
                ItemTypeId = batchModel.ItemTypeId,
            };
            var msg = QueryUtils.SaveMeasureValues(updateModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                System.Web.HttpContext.Current.Response.Write(
                        "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                BulkItemPopupExtender.Show();
                return;
            }
            SetActionText(MsgBulkUpdateSave);
            //-- Changes saved
            //-- Refresh item data and rebuild table
            var batches = checkedItems.GroupBy(m => m.OrderCode + ";" + m.BatchCode + ";" + m.BatchId).ToList();
            foreach(var batch in batches)
            {
                var updOrder = batch.Key.Split(';')[0];
                var updBatch = batch.Key.Split(';')[1];
                var updBatchId = batch.Key.Split(';')[2];
                var newValues =
                    QueryUtils.GetItemValues(
                        new BatchModel { GroupCode = updOrder, BatchCode = updBatch }, this);
                var forRefresh = GetReportFromView().Find(m => m.BatchModel.BatchId == "" + updBatchId);
                if (forRefresh != null)
                {
                    forRefresh.ItemValues = newValues;
                }
            }
            OnBuildBtnClick(null, null);
            PopupInfoDialog("Changes were updated successfully", false);
        }
        #endregion

        #region Delete Entered Value on BulkUpdate
        protected void OnBulkDelCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + EnteredGrid.DataKeys[e.Item.ItemIndex];
            var partId = Convert.ToInt32(uniqueKey.Split(';')[0]);
            var measureId = Convert.ToInt32(uniqueKey.Split(';')[1]);
            var values = GetEnteredValues();
            var value = values.Find(m => m.MeasureValue.PartId == partId && m.MeasureValue.MeasureId == "" + measureId);
            if (value == null) return;
            values.Remove(value);
            ShowEnteredValues();

        }
        #endregion

        #region Utilities for BulkUpdate
        public List<BatchItemModel> GetBulkCheckedItems()
        {
            var checkedItems = new List<BatchItemModel>();
            foreach(TreeNode node in BulkUpdateItems.CheckedNodes)
            {
                if (node.Depth != 2) continue;//-- 0 - order, 1 - batch, 2 - item
                var itemNumber = node.Value;
                var order = itemNumber.Substring(0, itemNumber.Length - 5);
                var batchCode = itemNumber.Substring(itemNumber.Length - 5, 3);
                var report =
                    GetReportFromView().Find(
                        m => m.BatchModel.GroupCode == order && m.BatchModel.BatchCodeTreeChars == batchCode);
                if (report == null) continue;
                var chkItem = new BatchItemModel
                {
                    BatchId = Convert.ToInt32(report.BatchModel.BatchId),
                    ItemCode = Convert.ToInt32(Utils.ParseItemCode(itemNumber)),
                    OrderCode = order,
                    BatchCode = batchCode
                };
                checkedItems.Add(chkItem);

            }
            return checkedItems;
        }

        private void ShowValuePanels(int measureClass)
        {
            if (measureClass == 0)
            {
                //-- Hide All
                MeasureEnumField.Visible = false;
                MeasureNumericField.Visible = false;
                MeasureTextField.Visible = false;
                return;
            }
            MeasureEnumField.Visible = measureClass == MeasureModel.MeasureClassEnum;
            if (MeasureEnumField.Visible)
            {
                MeasureEnumField.SelectedIndex = 0;
                MeasureEnumField.Focus();
            }

            MeasureNumericField.Visible = measureClass == MeasureModel.MeasureClassNumeric;
            if (MeasureNumericField.Visible)
            {
                MeasureNumericField.Text = "";
                MeasureNumericField.Focus();
            }

            MeasureTextField.Visible = measureClass == MeasureModel.MeasureClassText;
            if (MeasureTextField.Visible)
            {
                MeasureTextField.Text = "";
                MeasureTextField.Focus();
            }
        }
        protected void OnBulkEnter(object sender, EventArgs e)
        {
            BulkItemPopupExtender.Show();
        }
        #endregion

        #endregion

        #region Bulk Update Init Dialog (select item type, item numbers)
        protected void OnItemTypeChanged(object sender, EventArgs e)
        {
            LoadBulkUpdateItemNumbers();
            BulkInitPopupExtender.Show();
        }
        private void BulkInitOnLoad(List<ItemTypeModel> itemTypes)
        {
            ItemTypesList.DataSource = itemTypes;
            ItemTypesList.DataBind();
            ItemTypesList.SelectedValue = "" + itemTypes.ToArray()[0].ItemTypeId;
            SetActionText(MsgBulkUpdateOpen);
            OnItemTypeChanged(null, null);
        }
        private void LoadBulkUpdateItemNumbers()
        {
            BulkUpdateItems.Nodes.Clear();

            var batches = GetReportFromView().FindAll(m => m.BatchModel.ItemTypeId == Convert.ToInt32(ItemTypesList.SelectedValue));
            var orders = batches.GroupBy(m => m.BatchModel.GroupCode);
            foreach (var orderGrp in orders)
            {
                var order = Utils.FillToFiveChars(orderGrp.Key.Split(';')[0]);
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = order, DisplayName = order } };
                var batchForOrder = batches.FindAll(m => m.BatchModel.GroupCodeFiveChars == order);
                foreach(var batch in batchForOrder)
                {
                    data.Add(new TreeViewModel { ParentId = order, Id = order + ";" + batch.BatchModel.BatchCode, DisplayName = batch.BatchModel.FullBatchNumberWithDot });
                    foreach (var itemModel in batch.Items )
                    {
                        data.Add(new TreeViewModel { ParentId = order + ";" + batch.BatchModel.BatchCode , Id=itemModel.FullItemNumber, DisplayName = itemModel.FullItemNumberWithDotes});
                    }
                }
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                BulkUpdateItems.Nodes.Add(rootNode);
                rootNode.Expand();
            }

        }
        protected void OnBulkInitNextClick(object sender, EventArgs e)
        {
            if (BulkUpdateItems.CheckedNodes.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + 
                    "Checked 'Item Numbers' Not Found." + "\")</SCRIPT>");
                BulkItemPopupExtender.Show();
                return;
            }
            BulkUpdateOnLoad();
        }
        protected void OnBulkUpdateTreeSelectedChanged(object sender, EventArgs e)
        {
            BulkInitPopupExtender.Show();
        }
        #endregion

        protected void OnMovedItemsClick(object sender, EventArgs e)
        {
            MovedItemsGrid.DataSource = GetMovedItemsFromView();
            MovedItemsGrid.DataBind();
            MovedItemsBtnPopupExtender.Show();
        }

        protected void OnNYMailContacts(object sender, EventArgs e)
        {
            NYAddress.Text = NYMailList.SelectedItem.Text;
        }

        protected void OnSendNYEmailClick(object sender, EventArgs e)
        {
            OnSendEmailClick(null, null);
        }

        protected void OnSendNYEmailSpecInstrClick(object sender, EventArgs e)
        {
            string custCode = hdnCustomerCode.Value;
            string sku = hdnSKU.Value;
            //string msg = "Please note this order has been processed in India and certs are ready to be printed in NY. </br></br>" + Environment.NewLine + Environment.NewLine;
            //msg += " Customer Code: " + custCode + ". </br></br> " + Environment.NewLine + Environment.NewLine;
            //msg += " Sku: " + sku + ". </br></br>";
            string msg = "Special Instructions: ";
            PopupSpecInstrDialog(msg);
        }
        protected void OnSpecInstrOkClick(object sender, EventArgs e)
        {
            string msg = SpecInstrText.Text;
        }
    }
}