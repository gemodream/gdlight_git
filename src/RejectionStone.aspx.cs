﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using Corpt.Constants;
using Corpt.Models.Stats;
using Corpt.TreeModel;
namespace Corpt
{
    public partial class RejectionStone : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Rejected Stones";
            if (!IsPostBack)
            {
                //ScriptManager1.RegisterAsyncPostBackControl(GridReject);
                //ScriptManager.GetCurrent(this).RegisterPostBackControl(LookupButton);
                LoadMeasureTree();
                LoadCustomers();
                OnChangedPeriodType(null, null);
            }
        }
        

        #region Period Type
        private static string PeriodMonthly = "m";
        private static string PeriodWeekly = "w";
        private static string PeriodRandom = "r";
        protected void OnChangedPeriodType(object sender, EventArgs e)
        {
            var type = periodType.SelectedValue;
            var isRandom = (type == PeriodRandom);
            calFrom.Enabled = isRandom;
            calTo.Enabled = isRandom;
            if (isRandom) return;
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var isWeekly = (type == PeriodWeekly);
            var dateFrom = PaginatorUtils.ConvertDateToString(isWeekly ? today.AddDays(-7) : today.AddMonths(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;
        }
        #endregion

        #region Filter
        private StatsFilterModel GetFilterFromPage()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime)dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.PeriodType = periodType.SelectedValue;
            filterModel.CustomerId = lstCustomerList.SelectedValue;
            return filterModel;
        }
        #endregion

        #region Customer Search
        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this);
            customers.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
            lstCustomerList.SelectedValue = "";
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            filtered.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (filtered.Count == 1) lstCustomerList.SelectedValue = "";
            if (filtered.Count > 1) lstCustomerList.SelectedValue = filtered.ElementAt(1).CustomerId;
        }
        private string GetCustomerName(string customerId)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            var customer = customers.Find(m => m.CustomerId == customerId);
            return (customer == null ? "" : customer.CustomerName);
        }
        #endregion
        #region Measures Tree
        private void LoadMeasureTree()
        {
            var measures = QueryUtils.GetFailedMeasuresForVerify(this);
            MeasureTree.Nodes.Clear();
            
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Measures" } };
            var groups = measures.GroupBy(m => m.MeasureGroupId + ";" + m.MeasureGroupName).ToList();
            foreach (var group in groups)
            {
                var groupId = group.Key.Split(';')[0];
                var groupName = group.Key.Split(';')[1];
                data.Add(new TreeViewModel { ParentId = "0", Id = groupId, DisplayName = groupName });
                var measByGrp = measures.FindAll(m => m.MeasureGroupId == groupId);
                foreach (var meas in measByGrp)
                {
                    data.Add(new TreeViewModel { ParentId = groupId, Id = groupId + ";" + meas.MeasureId, DisplayName = meas.MeasureName });
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            MeasureTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }
        private IEnumerable<string> GetCheckedMeasures()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in MeasureTree.CheckedNodes)
            {
                if (node.Depth != 2) continue;//-- 0 - all, 1 - country, 2 - office
                var measId = node.Value.Split(';')[1];
                checkItems.Add(measId);
            }
            return checkItems;
        }
        #endregion
        protected void OnLookupByCustomerClick(object sender, ImageClickEventArgs e)
        {
            var filter = GetFilterFromPage();
            var measures = GetCheckedMeasures();
            if (!measures.Any())
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Measures Not Found!" + "\")</SCRIPT>");
                return;
            }
            filter.MeasureIds = measures.ToList();
            filter.RejectionType = TypeRejection.SelectedValue;
            var data = QueryUtils.GetRejectRatesByCustomer(filter, this);
            SetViewState(data, "details");
            GridReject.DataSource = data;//.FindAll(m=> m.PrintedQuantity > 0);
            GridReject.DataBind();
            var total = data.Sum(m => m.Quantity);
            var printed = data.Sum(m => m.InvalidQuantity);
            var format = "Total: {0} customers, {1} stones, {2} invalid stones";
            RejectRatesLabel.Text = string.Format(format, data.Count, total, printed);
            CustomerDetailsLabel.Text = "";
            GridRejectDetails.DataSource = null;
            GridRejectDetails.DataBind();
        }

        protected void GridReject_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "Details") return;
            var customerId = "" + e.CommandArgument;
            var filter = GetFilterFromPage();
            filter.CustomerId = customerId;
            var customers = GetViewState("details") as List<StoneRejectModel> ?? new List<StoneRejectModel>();
            var customer = customers.Find(m => m.CustomerId == customerId);
            var details = (customer != null ? customer.Details : new List<StoneRejectDetailsModel>());
            GridRejectDetails.DataSource = details;
            GridRejectDetails.DataBind();
            var customerName  = GetCustomerName(customerId);
            var format = "Details for {0}, {1} stones, {2} failed parameters";
            var stoneCounts = details.GroupBy(m => m.OrderCode + ";" + m.BatchCode + ";" + m.ItemCode).ToList().Count;
            CustomerDetailsLabel.Text = string.IsNullOrEmpty(customerName) ? "" : string.Format(format, customerName, stoneCounts, details.Count);
        }

        protected void GridReject_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            if ((e.Row.DataItem as StoneRejectModel).InvalidQuantity == 0)
            {
                (e.Row.FindControl("Button1") as Button).Visible = false;
            }
        }

    }
}