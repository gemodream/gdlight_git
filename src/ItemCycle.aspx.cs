﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models.Stats;
using Corpt.Models;
namespace Corpt
{
    public partial class ItemCycle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Old/New orders ##";
        }

        protected void OnFindItemBtnClick(object sender, EventArgs e)
        {
            var inputItemNumber = InputItemFld.Text.Trim();
            if (!(inputItemNumber.Length == 10 || inputItemNumber.Length == 11))
            {
                //PopupInfoDialog("Invalid Item Number", true);
                return;
            }
            var items = QueryUtils.GetItemLifeCicle(inputItemNumber, this);
            var p = items.Find(m => m.FullItemNumber == inputItemNumber);
            if (p != null)
            {
                p.IsParam = true;
            }
            GridItems.DataSource = items;
            GridItems.DataBind();
            if (items.Count == 0)
            {
                NewItemNumberFld.Text = "";
            }
            else
            {
                NewItemNumberFld.Text = items.ElementAt(items.Count - 1).FullItemNumberWithDotes;
            }

        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType != DataControlRowType.DataRow) return;
            if ((e.Row.DataItem as ItemCycleModel).IsParam)
            {
                e.Row.Font.Bold = true;//Color.FromArgb(252, 248, 227);
            }
        }
    }
}