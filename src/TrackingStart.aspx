<%@ Page language="c#" Codebehind="TrackingStart.aspx.cs" AutoEventWireup="True" Inherits="Corpt.TrackingStart" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TrackingStart</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/main.css">
	</HEAD>
	<body class="text">
		<form id="Form1" method="post" runat="server">
			<p><asp:button id="cmdMiddle" CssClass="ButtonStyle" Text="Home" Runat="server" onclick="cmdMiddle_Click"></asp:button><br>
			</p>
			<p><asp:dropdownlist id="lstCustomerList" CssClass="inputStyleCC5" Runat="server"></asp:dropdownlist><br>
				<br>
			</p>
			<p>Date Range:<br>
				from&nbsp;<asp:dropdownlist id="lstMonthFrom" CssClass="inputStyleCC5" Runat="server" Width="72px">
					<asp:ListItem Value="1">January</asp:ListItem>
					<asp:ListItem Value="2">February</asp:ListItem>
					<asp:ListItem Value="3">March</asp:ListItem>
					<asp:ListItem Value="4">April</asp:ListItem>
					<asp:ListItem Value="5">May</asp:ListItem>
					<asp:ListItem Value="6">June</asp:ListItem>
					<asp:ListItem Value="7">July</asp:ListItem>
					<asp:ListItem Value="8">August</asp:ListItem>
					<asp:ListItem Value="9">September</asp:ListItem>
					<asp:ListItem Value="10">October</asp:ListItem>
					<asp:ListItem Value="11">November</asp:ListItem>
					<asp:ListItem Value="12">December</asp:ListItem>
				</asp:dropdownlist>/<asp:dropdownlist id="lstDayFrom" CssClass="inputStyleCC5" Runat="server" Width="72px"></asp:dropdownlist>/<asp:dropdownlist id="lstYearFrom" CssClass="inputStyleCC5" Runat="server" Width="72px"></asp:dropdownlist>
				&nbsp;&nbsp; to<asp:dropdownlist id="lstMonthTo" CssClass="inputStyleCC5" Runat="server" Width="72px">
					<asp:ListItem Value="1">January</asp:ListItem>
					<asp:ListItem Value="2">February</asp:ListItem>
					<asp:ListItem Value="3">March</asp:ListItem>
					<asp:ListItem Value="4">April</asp:ListItem>
					<asp:ListItem Value="5">May</asp:ListItem>
					<asp:ListItem Value="6">June</asp:ListItem>
					<asp:ListItem Value="7">July</asp:ListItem>
					<asp:ListItem Value="8">August</asp:ListItem>
					<asp:ListItem Value="9">September</asp:ListItem>
					<asp:ListItem Value="10">October</asp:ListItem>
					<asp:ListItem Value="11">November</asp:ListItem>
					<asp:ListItem Value="12">December</asp:ListItem>
				</asp:dropdownlist>/<asp:dropdownlist id="lstDayTo" CssClass="inputStyleCC5" Runat="server" Width="72px"></asp:dropdownlist>/<asp:dropdownlist id="lstYearTo" CssClass="inputStyleCC5" Runat="server" Width="72px"></asp:dropdownlist>
			</p>
			<p><asp:button id="cmdLoadActivity" CssClass="ButtonStyle" Text="Load activity" Runat="server" onclick="cmdLoadActivity_Click"></asp:button></p>
			<p>
				<asp:Label Runat="server" ID="lblInfo"></asp:Label>
			</p>
			<P>Orders:</P>
			<P>
				<asp:Label Runat="server" ID="lblOrders"></asp:Label></P>
			<P>Customer Programs:</P>
			<P>
				<asp:Label Runat="server" ID="lblCustomerPrograms"></asp:Label></P>
			<P>Memo Numbers:</P>
			<P>
				<asp:Label Runat="server" ID="lblMemos"></asp:Label></P>
			<p>
				<asp:DataGrid id="dgDebug" runat="server" CssClass="text"></asp:DataGrid>
			</p>
		</form>
	</body>
</HTML>
