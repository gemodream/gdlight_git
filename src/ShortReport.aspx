<%@ Page language="c#" MasterPageFile="~/DefaultMaster.Master" Codebehind="ShortReport.aspx.cs" AutoEventWireup="True" Inherits="Corpt.ShortReport" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
<div class="demoarea">
    <div class="demoheading">Short report</div>
    
    <!-- 'Batch Number' field -->
    <asp:Panel runat="server" DefaultButton="Lookup" CssClass="form-inline" Style="padding: 10px">
        <strong>Batch Number: </strong>
        <asp:TextBox type="text" ID="BatchNumber" MaxLength="9" placeholder="Batch Number"
            runat="server" name="BatchNumber" Style="font-weight: bold; width: 155px;" />
        <!-- 'Lookup' button -->
        <asp:Button ID="Lookup" runat="server" CssClass="btn btn-primary" OnClick="LookupClick"
            Text="Lookup"></asp:Button>
    </asp:Panel>
    <div class="control-group error">
        <asp:Label class="control-label" id="WrongInfoLabel" runat="server" />
    </div>
</div>
</asp:Content>

