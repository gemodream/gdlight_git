﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class UpdateOrder : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Update Order";

            if (!IsPostBack)
            {
                var orderCode = Request.QueryString["oc"].ToString();
                OrderField.Text = orderCode;
                LoadOrderData();
            }
        }

        #region Search by OrderCode
        protected void OnSearchClick(object sender, ImageClickEventArgs e)
        {
            LoadOrderData();
        }
        private void LoadOrderData()
        {
            ItemsGrid.DataSource = null;
            ItemsGrid.DataBind();
            TreeHistory.Nodes.Clear();
            var items = new List<CustomerHistoryModel>();
            if (!string.IsNullOrEmpty(OrderField.Text.Trim()))
            {
                items = QueryItemiznUtils.GetOrderHistory(OrderField.Text, true, this); 
            }

            if (items.Count == 0)
            {
                SetViewState(null, SessionConstants.OrderHistoryList);
                PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", OrderField.Text), true);

            }

            SetViewState(items, SessionConstants.OrderHistoryList);
            SetViewState(new UpdateOrderModel { OrderCode = OrderField.Text.Trim() }, SessionConstants.UpdateOrder);

            ShowTreeHistory(items);
        }
        private void ShowTreeHistory3(IEnumerable<CustomerHistoryModel> items)
        {
            TreeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            TreeHistory.Visible = customerHistoryModels.Length > 0;
            AddButton.Visible = TreeHistory.Visible;
            UpdateButton.Visible = TreeHistory.Visible;
            ClearButton.Visible = TreeHistory.Visible;
            if (customerHistoryModels.Length == 0) return;
            var origTreeView = (TreeView) Session["TreeNode"];
            TreeNode newTn;
            foreach (TreeNode tn in origTreeView.Nodes)
            {
                bool collapsed = false;
                newTn = new TreeNode(tn.Text, tn.Value);
                CopyChilds(newTn, tn, collapsed);
                TreeHistory.Nodes.Add(newTn);
            }
            //var data = new List<TreeViewModel>();
            //foreach (var item in customerHistoryModels)
            //{
            //    data.Add(
            //        new TreeViewModel
            //        {
            //            Id = item.Id,
            //            ParentId = item.ParentId,
            //            DisplayName = item.Title
            //        });
            //}
            //var root = TreeUtils.GetRootTreeModel(data);
            //var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = TreeNodeSelectAction.Select };// TreeNodeSelectAction.None };

            //TreeUtils.FillNode(rootNode, root, true, TreeNodeSelectAction.None);
            //rootNode.Expand();

            //rootNode.CollapseAll();
            //TreeHistory.Nodes.Add(rootNode);
        }
        private void ShowTreeHistory2(IEnumerable<CustomerHistoryModel> items)
        {
            TreeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            TreeHistory.Visible = customerHistoryModels.Length > 0;
            AddButton.Visible = TreeHistory.Visible;
            UpdateButton.Visible = TreeHistory.Visible;
            ClearButton.Visible = TreeHistory.Visible;
            if (customerHistoryModels.Length == 0) return;
            var origTreeView = (TreeView)Session["TreeNode"];
            TreeNode newTn;
            foreach (TreeNode tn in origTreeView.Nodes)
            {
                newTn = new TreeNode(tn.Text, tn.Value);
                //CopyChilds(newTn, tn);
                TreeHistory.Nodes.Add(newTn);
            }
            var data = new List<TreeViewModel>();
            foreach (var item in customerHistoryModels)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        DisplayName = item.Title
                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = TreeNodeSelectAction.Select };// TreeNodeSelectAction.None };

            TreeUtils.FillNode(rootNode, root, true, TreeNodeSelectAction.None);
            rootNode.Expand();

            rootNode.CollapseAll();
            TreeHistory.Nodes.Add(rootNode);
        }
        public void CopyChilds(TreeNode parent, TreeNode willCopied, bool collapsed)
        {
            TreeNode newTn;
            foreach (TreeNode tn in willCopied.ChildNodes)
            {
                newTn = new TreeNode(tn.Text, tn.Value);
                if (tn.Checked)
                    newTn.Checked = true;
                if (collapsed)
                    newTn.CollapseAll();
                else
                    newTn.ExpandAll();
                parent.ChildNodes.Add(newTn);
                collapsed = false;
                CopyChilds(newTn, tn, collapsed);
            }
        }
        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items)
        {
            TreeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            TreeHistory.Visible = customerHistoryModels.Length > 0;
            AddButton.Visible = TreeHistory.Visible;
            UpdateButton.Visible = TreeHistory.Visible;
            ClearButton.Visible = TreeHistory.Visible;
            if (customerHistoryModels.Length == 0) return;
            var origTreeView = (TreeNodeCollection)Session["TreeNode"];
            List<string> checkedItemsList = new List<string>();
            for (int i=0; i<= origTreeView.Count-1; i++)
            {
                if (origTreeView[i].Depth == 1)//batch
                {
                    string item = origTreeView[i].Text.Substring(origTreeView[i].Text.IndexOf(".")+1);
                    if (item.Contains("("))
                        item = item.Substring(0, item.IndexOf("(")).Trim();
                    checkedItemsList.Add(item);
                }
                else if (origTreeView[i].Depth == 2)//item
                {
                    string item = origTreeView[i].Text.Substring(origTreeView[i].Text.IndexOf(".")+1);
                    if (item.Contains("("))
                        item = item.Substring(0, item.IndexOf("(")).Trim();
                    checkedItemsList.Add(item);
                }
            }
            for (int i = 0; i <= checkedItemsList.Count - 1; i++)
            {
                if (checkedItemsList[i].Contains(" "))
                    checkedItemsList[i] = checkedItemsList[i].Substring(0, checkedItemsList[i].IndexOf(" ")).Trim();
            }
            var data = new List<TreeViewModel>();
            foreach (var item in customerHistoryModels)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        DisplayName = item.Title
                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = TreeNodeSelectAction.Select };// TreeNodeSelectAction.None };

            TreeUtils.FillNode(rootNode, root, true, TreeNodeSelectAction.None);
            rootNode.Expand();

            //rootNode.CollapseAll();
            TreeHistory.Nodes.Add(rootNode);
            foreach(TreeNode node in TreeHistory.Nodes)
            {
                if (node.ChildNodes.Count > 0)
                {
                    MarkCheckedNodes(node, checkedItemsList);
                }
            }
        }
        private void MarkCheckedNodes(TreeNode node, List<string> checkedItemsList)
        {
            
            
            if (node.Depth != 0 && node.Depth != 4)
            {
                string value = node.Value.Substring(node.Value.IndexOf("_") + 1).Trim();
                if (value.Contains(" "))//
                    value = value.Substring(0, value.IndexOf(" ")).Trim();
                foreach (string item in checkedItemsList)
                {
                    if (value == item)
                    {
                        node.Checked = true;
                        break;
                    }
                }
            }
            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode childNode in node.ChildNodes)
                    MarkCheckedNodes(childNode, checkedItemsList);
            }
        }
        #endregion

        #region View State
        private List<CustomerHistoryModel> GetOrderHistoryFromView()
        {
            return GetViewState(SessionConstants.OrderHistoryList) as List<CustomerHistoryModel> ??
                   new List<CustomerHistoryModel>();
        }
        private UpdateOrderModel GetUpdateOrderFromView()
        {
            return GetViewState(SessionConstants.UpdateOrder) as UpdateOrderModel ?? new UpdateOrderModel{OrderCode = OrderField.Text.Trim()};
        }
        private UpdateOrderBatchModel GetUpdateBatchFromView(string batchId)
        {
            return GetUpdateOrderFromView().Batches.Find(m => m.BatchId == batchId);
        }
        #endregion

        #region Add Items
        protected void OnAddClick(object sender, EventArgs e)
        {
            var batchIds = GetCheckedBatch();
            if (batchIds.Count == 0 )
            {
                PopupInfoDialog("No items chosen.", true);
                return;
            }
            if (batchIds.Count > 1)
            {
                PopupInfoDialog("Please select one batch.", true);
                return;
            }
            
            //-- Get CP and Current Docs
            var updateOrder = GetUpdateOrderFromView();
            var batchId = batchIds.ToArray()[0];
            var updateBatch = updateOrder.Batches.Find(m => m.BatchId == batchId);
            if (updateBatch == null)
            {
                var addBatch = new UpdateOrderBatchModel {BatchId = batchId, CurrentDocs = new List<CpDocPrintModel>()};
                QueryCpUtils.GetCpAndCurrentDocsByBatch(addBatch, this);
                if (addBatch.Cp != null)
                {
                    updateOrder.Batches.Add(addBatch);
                }
            }
            
            //-- Show choice doc dialog
            updateBatch = updateOrder.Batches.Find(m => m.BatchId == batchId);
            if (updateBatch == null) return;
            ChoiceBatchIdFld.Value = updateBatch.BatchId;
            DocsFld.DataSource = updateBatch.CurrentDocs;
            DocsFld.DataBind();
            CpNameFld.Text = updateBatch.Cp.CustomerProgramName;
            ChoiceDocPopupExtender.Show();
        }
        private List<string> GetCheckedBatch()
        {
            var items = GetOrderHistoryFromView();
            var batchIds = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                var item = items.Find(m => m.Id == node.Value);
                if (item == null) continue;
                if (batchIds.Contains(item.BatchId)) continue;
                batchIds.Add(item.BatchId);
            }
            return batchIds;
        }
        private IEnumerable<string> GetCheckedItems()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth != 3) continue;//-- 0 - customer, 1 - order, 2 - batch, 3 - item, 4 - document
                checkItems.Add(node.Value.Split('_')[1]); // 1283_61887.001.01 -> 61887.001.01
            }
            return checkItems;
        }
        protected void OnChoiceAddClick(object sender, EventArgs e)
        {
            UpdateItemsFromGrid();
            var updateOrder = GetUpdateOrderFromView();
            if (updateOrder == null) return;
            var updateBatch = updateOrder.Batches.Find(m => m.BatchId == ChoiceBatchIdFld.Value);
            if (updateBatch == null) return;
            var checkItems = GetCheckedItems();
            foreach(var item in checkItems)
            {
                var itemCode = item.Split('.')[2]; //-- 61887.001.01
                var editModel = updateBatch.EditItems.Find(m => m.ItemCode == itemCode);
                if (editModel == null)
                {
                    editModel = new UpdateOrderEditModel{BatchId = updateBatch.BatchId, FullItemNumber = item, ItemCode = itemCode};
                    updateBatch.EditItems.Add(editModel);
                }
                editModel.DocKey = DocsFld.SelectedValue;
                editModel.CpName = CpNameFld.Text;
            }

            LoadEditGrid();

        }
        private void LoadEditGrid()
        {
            var updateOrder = GetUpdateOrderFromView();
            var items = new List<UpdateOrderEditModel>();
            foreach(var batch in updateOrder.Batches)
            {
                items.AddRange(batch.EditItems);
            }
            items.Sort((m1, m2) => string.CompareOrdinal(m1.FullItemNumber, m2.FullItemNumber));
            ItemsGrid.DataSource = items.Count == 0 ? null : items;
            ItemsGrid.DataBind();
        }
        #endregion

        #region Update command
        protected void OnUpdateClick(object sender, EventArgs e)
        {
            if (ItemsGrid.Items.Count == 0)
            {
                PopupInfoDialog("At least one item must be checked to end session", true);
                return;
            }

            UpdateItemsFromGrid();
            var updateOrder = GetUpdateOrderFromView();
            foreach(var batch in updateOrder.Batches)
            {
                batch.ItemsInBatch = GetItemsCountInBatch(batch.BatchId);
            }
            var msg = QueryItemiznUtils.UpdateOrder(updateOrder, this);
            if (string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog("Order details were updated successfully", false);
                LoadOrderData();
            } else
            {
                PopupInfoDialog(msg, true);
            }
        }
        
        private int GetItemsCountInBatch(string batchId)
        {
            var history = GetViewState(SessionConstants.OrderHistoryList) as List<CustomerHistoryModel> ??
            new List<CustomerHistoryModel>();
            var items = history.FindAll(m => m.TargetCode == StateTargetModel.TrgCodeItem && m.BatchId == batchId);
            return items.Count;
        }

        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Edit Grid
        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var editModel = e.Item.DataItem as UpdateOrderEditModel;
            if (editModel == null) return;

            var itemDocFld = e.Item.FindControl("DocItemFld") as DropDownList;
            if (itemDocFld == null) return;
            var docs = GetUpdateBatchFromView(editModel.BatchId).CurrentDocs;
            itemDocFld.DataSource = docs;
            itemDocFld.DataBind();
            itemDocFld.SelectedValue = editModel.DocKey;
        }
        protected void OnDelItemClick(object sender, ImageClickEventArgs e)
        {
            var key = GetCommandLineKey(sender);
            if (string.IsNullOrEmpty(key)) return;
            var keys = key.Split(',');  //BatchId, ItemCode, DocKey
            if (keys.Length < 3) return;
            var batch = GetUpdateBatchFromView(keys[0]);
            if (batch == null) return;
            var item = batch.EditItems.Find(m => m.ItemCode == keys[1]);
            if (item != null)
            {
                batch.EditItems.Remove(item);
                LoadEditGrid();
            }
        }
        private string GetCommandLineKey(object sender)
        {
            var btn = sender as ImageButton;
            if (btn == null) return "";
            var dgItem = btn.NamingContainer as DataGridItem;
            return dgItem == null ? "" : "" + ItemsGrid.DataKeys[dgItem.ItemIndex];
        }
        private void UpdateItemsFromGrid()
        {
            foreach(DataGridItem item in ItemsGrid.Items)
            {
                var ddl = item.FindControl("DocItemFld") as DropDownList;
                var key = ""+ItemsGrid.DataKeys[item.ItemIndex]; //BatchId_ItemCode_DocKey
                var batchId = key.Split(',')[0];
                var itemCode = key.Split(',')[1];
                
                var updateBatch = GetUpdateBatchFromView(batchId);
                if (updateBatch == null) continue;
                var editModel = updateBatch.EditItems.Find(m => m.ItemCode == itemCode);
                if (editModel == null) continue;
                editModel.DocKey = (ddl != null ? ddl.SelectedValue : "");
                if (!string.IsNullOrEmpty(editModel.DocKey))
                {
                    var doc = updateBatch.CurrentDocs.Find(m => m.Key == editModel.DocKey);
                    if (doc != null)
                    {
                        editModel.DocChar = doc.OperationChar;
                    }
                }
            }
        }
        #endregion

        #region Clear ItemsGrid
        protected void OnClearClick(object sender, EventArgs e)
        {

            var order = GetUpdateOrderFromView();
            order.Batches = new List<UpdateOrderBatchModel>();
            LoadEditGrid();
        }
        #endregion
    }
}