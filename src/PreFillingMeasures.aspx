﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="PreFillingMeasures.aspx.cs" Inherits="Corpt.PreFillingMeasures" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
 
    <div class="demoarea">
        <div class="demoheading">Pre-Filling Measures</div><br/>
        <asp:UpdatePanel runat="server" ID="MainPanel">
            <Triggers>
                <asp:PostBackTrigger ControlID="SaveButton" />
            </Triggers>
            <ContentTemplate>
                <div class="navbar nav-tabs">
                    <table>
                        <tr>
                            <td style="padding-left: 20px;vertical-align: top;padding-top: 5px;font-size: small">Batch Number</td>
                            <td>
                                <asp:Panel runat="server" DefaultButton="LoadButton">
                                    <asp:TextBox runat="server" ID="BatchNumberFld" MaxLength="9" Width="100px"></asp:TextBox>
                                    <asp:ImageButton ID="LoadButton" runat="server" ToolTip="Load Batch Numbers" ImageUrl="~/Images/ajaxImages/search.png"
                                        OnClick="OnLoadClick" />
                                    <asp:HiddenField runat="server" ID="BatchField" />
                                    <asp:HiddenField runat="server" ID="OrderField" />
                                    <asp:Button runat="server" ID="SaveButton" Text="Pre-Fill Checked Measures in Checked Batches" OnClick="OnSaveClick" CssClass="btn btn-default" Style="margin-top: -10px"/>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="LoadInfoLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="vertical-align: top; font-size: smaller; font-family: Calibri; width: 300px;">
                                Batches with the same structure as the batch above
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:UpdatePanel runat="server" ID="BatchesTree" >
                                <ContentTemplate>
                                        <asp:TreeView ID="treeBatches" runat="server" ShowCheckBoxes="All" Width="150px"
                                            ExpandDepth="0" AfterClientCheck="CheckChildNodes();"
                                            PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeBatchCheckChanged"
                                            onclick="OnTreeClick(event)" 
                                            OnSelectedNodeChanged="OnTreeBatchSelectedChanged" 
                                            ondatabinding="treeBatches_DataBinding" 
                                            ontreenodedatabound="OnTreeNodeDataBound">
                                            <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                            <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                            
                                        </asp:TreeView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td style="vertical-align: top;padding-left: 20px">
                                <asp:UpdatePanel runat="server" ID="MeasurePanel">
                                    <ContentTemplate>
                                        <asp:TreeView ID="treeMeasures" runat="server" ShowCheckBoxes="All" Width="200px"
                                            ExpandDepth="0" AfterClientCheck="CheckChildNodes();"
                                            PopulateNodesFromClient="true" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeMeasureCheckChanged"
                                            onclick="OnTreeClick(event)">
                                            <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                                            <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                                            <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                                        </asp:TreeView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            
                        </td>
                        <td style="vertical-align: top; padding-left: 40px">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    
                                    <asp:AsyncPostBackTrigger ControlID="treeBatches" 
                                        EventName="SelectedNodeChanged" />
                                    
                                </Triggers>
                                <ContentTemplate>
                                    <asp:DataGrid runat="server" ID="dgMeasures" AutoGenerateColumns="False" CellPadding="5"
                                        CellSpacing="5" OnItemDataBound="OnItemDataBound">
                                        <Columns>
                                            <asp:BoundColumn DataField="DisplayBatch" HeaderText="Batch number"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PartName" HeaderText="Part Name"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MeasureTitle" HeaderText="Measure"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DisplayValue" HeaderText="Value"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                                        <ItemStyle Font-Names="Cambria" Font-Size="Small" />
                                    </asp:DataGrid>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
