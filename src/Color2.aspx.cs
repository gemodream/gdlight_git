﻿using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Corpt
{
	public partial class Color2 : CommonPage
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Color";

			OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
			OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;

			if (!IsPostBack)
			{
                string measureIds = QueryUtils.GetGradeMeasureIds(this);
                Session["MeasureIds"] = measureIds;
                CleanUp();
				SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
                SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
            }
			txtBatchNumber.Focus();
		}
		#endregion

		#region CleanUp (on PageLoad, on Load button click)
		private void CleanUp()
		{
			CleanItemListArea();
			CleanPartTreeArea();
			ShowHideImages(false);
			CleanValueGridArea();
			CleanViewState();

			InvalidLabel.Text = "";
		}
		private void CleanItemListArea()
		{
			lstItemList.Items.Clear();
			ItemEditing.Value = "";
			lstItemList.Style["display"] = "none";
		}
		private void CleanPartTreeArea()
		{
			PartTree.Nodes.Clear();
			PartEditing.Value = "";
		}
		private void CleanValueGridArea()
		{
			//New code for unbinding data sources
			ElementPanel.DataSource = null;
			ElementPanel.DataBind();

			//Make Measurement Panel Invisible
			ShowHideItemDetails(false);
			/*
			ColorRadio.Visible = false;
			Comments.Visible = false;
			WeightPanel.Visible = false;
			LaserPanel.Visible = false;
			IntExtCommentPanel.Visible = false;
			SarinPanel.Visible = false;
			*/
		}
		private void CleanViewState()
		{
			//-- Item numbers
			SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);

			//-- Measure Parts
			SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);

			//-- Measure Descriptions
			SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

			//-- Item Measure Values
			SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);
        }
		#endregion

		#region Load
		int ListMaxRow = 40;
		int ListMinRow = 10;
		protected void OnLoadClick(object sender, EventArgs e)
		{
            Session["ItemList"] = null;
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnLoadClick);
				return;
			}
			LoadExecute();
		}
		/* alex
		private void LoadExecute()
		{
			//Save current lstItemList temporarily
			List<string> prevList = new List<string>();
			string prevItemNumber = "0";
			var prevModel = GetItemModelFromView(ItemEditing.Value);
			if (prevModel != null)
			{
				prevItemNumber = prevModel.FullBatchNumber;
				foreach (ListItem item in lstItemList.Items)
				{
					if (item.Text.StartsWith("*"))
					{
						prevList.Add(item.Text.Substring(1));
					}
				}
			}

			CleanUp();
			if (txtBatchNumber.Text.Trim().Length == 0)
			{
				InvalidLabel.Text = "A Batch Number is required!";
				return;
			}
			//Check 7digit prefix
			var myResult = QueryUtils.GetItemNumberBy7digit(txtBatchNumber.Text.Trim(), this.Page);
			if (myResult.Trim() != "")
			{
				txtBatchNumber.Text = myResult;
			}
			//-- Get ItemNumbers by BatchNumber
			var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
			if (itemList.Count == 0)
			{
				InvalidLabel.Text = "Items not found";
				return;
			}
			SetViewState(itemList, SessionConstants.GradeItemNumbers);

			//-- Refresh batchNumber
			var tempTxtBatchNumber = txtBatchNumber.Text;//Temporarily save item number for later selection
			var itemNumber = itemList[0];
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}

			//-- Loading ItemList control
			lstItemList.Items.Clear();
			foreach (var singleItemModel in itemList)
			{
				lstItemList.Items.Add(singleItemModel.FullItemNumber);
			}
			if (itemList.Count > ListMaxRow)
			{
				lstItemList.Rows = ListMaxRow;
			}
			else if (itemList.Count < ListMinRow)
			{
				lstItemList.Rows = ListMinRow;
			}
			else
			{
				lstItemList.Rows = itemList.Count + 1;
			}
			lstItemList.Style["display"] = "";

			//-- Loading Parts by first ItemNumber and show on PartTree Control
			var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
			SetViewState(parts, SessionConstants.GradeMeasureParts);
			LoadPartsTree();

			//-- Loading Measures by Cp or ItemTypeId
			if (IgnoreCpFlag.Checked)
			{
				var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}
			else
			{
				var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}

			//Sergey: Selects the exact item on load, rather than the first
			var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
			if (selectListItem == null)
			{
				lstItemList.SelectedIndex = 0;
			}
			else
			{
				lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
			}

			//If same batch as previous, mark saved entries
			if (prevItemNumber == txtBatchNumber.Text)
			{
				foreach (ListItem item in lstItemList.Items)
				{
					foreach (var prevItem in prevList)
					{
						if (prevItem == item.Text)
						{
							item.Text = "*" + item.Text;
						}
					}
				}
			}
			else//Otherwise reset the Radio Buttons
			{
				ColorRadio.SelectedIndex = 0;
                SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
            }

			/*
			ColorRadio.Visible = true;
			Comments.Visible = true;
			WeightPanel.Visible = true;
			SarinPanel.Visible = true;
			LaserPanel.Visible = true;
			IntExtCommentPanel.Visible = true;
			
			CreateMeasurePanel();
			OnItemListSelectedChanged(null, null);
		}
        */
		private void LoadExecute()
		{
            //Save current lstItemList temporarily
            Session["TMPNUMBER"] = null;
            Session["MatchItemNumber"] = null;
			hdnColorBatchEventID.Value = "";
            List<string> prevList = new List<string>();
            List<string> fullPrevList = new List<string>();
            string prevItemNumber = "";
            var prevModel = GetItemModelFromView(ItemEditing.Value);
            if (prevModel != null)
            {
                prevItemNumber = prevModel.FullBatchNumber;
                foreach (ListItem item in lstItemList.Items)
                {
                    if (item.Text.StartsWith("*"))
                    {
                        prevList.Add(item.Text.Substring(1));
                        fullPrevList.Add(item.Text.Substring(1));
                    }
                    else
                        fullPrevList.Add(item.Text);
                }
            }

            CleanUp();
            if (txtBatchNumber.Text.Trim().Length == 0)
            {
                InvalidLabel.Text = "A Batch Number is required!";
                return;
            }
            OldTextNumber.Value = txtBatchNumber.Text;
            //Check 7digit prefix
            var myResult = QueryUtils.GetItemNumberBy7digit(txtBatchNumber.Text.Trim(), this.Page);
            if (myResult.Trim() != "")
            {
                txtBatchNumber.Text = myResult;
            }
            //-- Get ItemNumbers by BatchNumber
            DataTable dt = new DataTable();
            dt = QueryUtils.GetItemsCpOldNew(txtBatchNumber.Text.Trim(), this.Page);
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
            //var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
            Session["ItemList"] = dt;
            if (itemList.Count == 0)
            {
                InvalidLabel.Text = "Items not found";
                return;
            }
            SetViewState(itemList, SessionConstants.GradeItemNumbers);

            //-- Refresh batchNumber
            var tempTxtBatchNumber = txtBatchNumber.Text;//Sergey
            var itemNumber = itemList[0];
            string matchedItemNumber = null;
            if (txtBatchNumber.Text != itemNumber.FullBatchNumber)//not batch
            {
                RemeasParamModel matchItemNumber = QueryUtils.GetNewItemNumber(txtBatchNumber.Text, this);
                Session["MatchItemNumber"] = matchItemNumber.NewItemNumber;
                matchedItemNumber = matchItemNumber.NewItemNumber;
            }
            /* alex
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}
            alex */
            List<string> newItemList = Utils.GetOriginalItemListValues(dt, itemList, txtBatchNumber.Text, matchedItemNumber);
            string prevItemCode = null;
            if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
            {
                int numberLength = txtBatchNumber.Text.Length;
                if (numberLength == 10 || numberLength == 11)//item
                {

                    if (itemNumber.FullBatchNumber != txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2))//search by batch
                    {

                        //prevItemCode = Utils.getPrevItemCode(dt, txtBatchNumber.Text);//search by item
                        prevItemCode = Utils.getPrevItemCodeNew(itemList, txtBatchNumber.Text, matchedItemNumber);//search by item
                        if (prevItemCode != null)
                            txtBatchNumber.Text = prevItemCode;
                    }
                }
                else //batch
                    txtBatchNumber.Text = itemNumber.FullBatchNumber;
            }
            //-- Loading ItemList control
            lstItemList.Items.Clear();
            if (newItemList == null)
            {
                foreach (var singleItemModel in itemList)
                {
                    lstItemList.Items.Add(singleItemModel.FullItemNumber);
                }
            }
            else
            {
                //txtBatchNumber.Text = Utils.getPrevItemCode(dt, tempTxtBatchNumber);
                txtBatchNumber.Text = Utils.getPrevItemCodeNew(itemList, tempTxtBatchNumber, matchedItemNumber);
                foreach (var item in newItemList)
                    lstItemList.Items.Add(item);
            }
            /*
            foreach (var singleItemModel in itemList)
            {
                lstItemList.Items.Add(singleItemModel.FullItemNumber);
            }
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                lstItemList.Rows = itemList.Count + 1;
            }
            */
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                if (newItemList == null)
                    lstItemList.Rows = itemList.Count + 1;
                else
                    lstItemList.Rows = newItemList.Count + 1;
            }
            lstItemList.Style["display"] = "";

            //-- Loading Parts by first ItemNumber and show on PartTree Control
            var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
            SetViewState(parts, SessionConstants.GradeMeasureParts);
            LoadPartsTree();

            //-- Loading Measures by Cp or ItemTypeId
            if (IgnoreCpFlag.Checked)
            {
                var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }
            else
            {
                var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
                SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
            }

            //Sergey: Selects the exact item on load, rather than the first
            /*
            var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
            if (selectListItem == null)
                selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
            if (selectListItem == null)
            {
                lstItemList.SelectedIndex = 0;
            }
            else
            {
                lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
            }
            */
            if (newItemList == null)
            {
                //var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
                var selectListItem = itemList.Find(m => m.FullItemNumber == txtBatchNumber.Text);
                if (selectListItem == null)
                    selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
                if (selectListItem == null)
                {
                    lstItemList.SelectedIndex = 0;
                }
                else
                {
                    lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
                }
            }
            else
            {
                string selectListItem = null;
                //selectListItem = newItemList.Find(m => m == tempTxtBatchNumber);
                selectListItem = newItemList.Find(m => m == txtBatchNumber.Text);
                if (selectListItem == null)
                {
                    lstItemList.SelectedIndex = 0;
                }
                else
                    //lstItemList.SelectedValue = tempTxtBatchNumber;
                    lstItemList.SelectedValue = txtBatchNumber.Text;
            }
            //If same batch as previous, mark saved entries
            //if (prevItemNumber == txtBatchNumber.Text)
            bool inPrevList = false;
            string matchItem = null;
            if (fullPrevList.Count > 0 && ((matchItem = fullPrevList.Find(p => p == txtBatchNumber.Text)) != null))
                inPrevList = true;
            //if ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)))
            if ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)) || inPrevList)
            {
                foreach (ListItem item in lstItemList.Items)
                {
                    string pItem = null;
                    if ((pItem = prevList.Find(p => p == item.Text)) != null)
                        item.Text = "*" + item.Text;
                    //foreach (var prevItem in prevList)
                    //{
                    //    if (prevItem == item.Text)
                    //    {
                    //        item.Text = "*" + item.Text;
                    //    }
                    //}
                }
            }
            else//Otherwise reset the Radio Buttons
            {
                ColorRadio.SelectedIndex = 0;
                SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
            }

            ShowHideItemDetails(true);
            /*
			ClarityRadio.Visible = true;
			Comments.Visible = true;
			WeightPanel.Visible = true;
			SarinPanel.Visible = true;
			IntExtCommentPanel.Visible = true;
			LaserPanel.Visible = true;
			*/
            CreateMeasurePanel();
            //if (txtBatchNumber.Text != tempTxtBatchNumber && txtBatchNumber.Text != tempTxtBatchNumber.Substring(0, tempTxtBatchNumber.Length - 2))
            if (tempTxtBatchNumber.Length >= 10 && (txtBatchNumber.Text != tempTxtBatchNumber && txtBatchNumber.Text != tempTxtBatchNumber.Substring(0, tempTxtBatchNumber.Length - 2)))
                Session["TMPNUMBER"] = tempTxtBatchNumber;
            OnItemListSelectedChanged(null, null);
			int noInBatch = lstItemList.Items.Count;
			int eventID = 3;
			int formCode = 5;
			int noAffected = 0;
			long batchId = itemList[0].BatchId;
			if (tempTxtBatchNumber.Length < 10)//batch
			{
				
				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);
                if (batchEventId != "")
                {
                    string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
                }
            }
			else
			{
				//int noInBatch = lstItemList.Items.Count;
				//int eventID = 3;
				//int formCode = 5;
				//int noAffected = 0;
				//long batchId = itemList[0].BatchId;
				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);

				if (batchEventId != "")
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
		}//LoadExecute
        
        protected void OnSubmitClick(object sender, EventArgs e)
        {
            var result = new List<MeasureValueModel>();
            result = ViewState["Results"] as List<MeasureValueModel>;
            if (result.Count > 0)
            {
                var errMsg = QueryUtils.SaveNewMeasures(result, this);
                if (string.IsNullOrEmpty(errMsg))
                {
					
					var estResults = new List<SingleItemModel>();
                    if (ViewState["EstResults"] != null)
                        estResults = ViewState["EstResults"] as List<SingleItemModel>;
                    if (estResults.Count > 0)
                    {
                        foreach (var estResult in estResults)
                        {
                            errMsg = QueryUtils.SetEstimatedValues(estResult, this);
                            if (!string.IsNullOrEmpty(errMsg))
                                PopupInfoDialog(errMsg, true);
                        }
                    }
					//-- Reload Item Values
					//OnItemListSelected();
					InvalidLabel.Text = "Changes were updated successfully for " + result.Count.ToString() + " parameters.";
					OnInfoCloseButtonClick(null, null);
					InvalidLabel.Visible = true;
					InvalidLabel.Text = "Changes were updated successfully for " + result.Count.ToString() + " parameters.";
					//PopupInfoDialog("Changes were updated successfully for " + result.Count.ToString() + " parameters.", false);
                    //if(lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                    //{
                    //    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                    //    OnItemListSelectedChanged(null, null);
                    //}
                }
                else
                {
                    PopupInfoDialog(errMsg, true);
                }
            }
        }
#endregion

        #region Save
        protected void OnSaveClick(object sender, EventArgs e)
		{
			//-- no changes
			if (!HasUnSavedChanges())
			{
				//PopupInfoDialog("No changes!", false);
				InvalidLabel.Text = "No changes!";
				return;
			}
			var errMsg = SaveExecute();
			if (string.IsNullOrEmpty(errMsg))
			{
				//-- Reload Item Values
				OnItemListSelected();
				
				OnInfoCloseButtonClick(null, null);
				InvalidLabel.Visible = true; 
				InvalidLabel.Text = "Changes were updated successfully.";
				//PopupInfoDialog("Changes were updated successfully.", false);
                //if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                //{
                //    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                //    OnItemListSelectedChanged(null, null);
                //}
			}
			else
			{
				PopupInfoDialog(errMsg, true);
			}
			return;
		}

/* alex
		private string SaveExecute()
		{
			var itemModel = GetItemModelFromView(ItemEditing.Value);
			var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			var result = new List<MeasureValueModel>();
            bool valueIn = false;
			foreach (var itemValue in itemValues)
			{
				result.Add(new MeasureValueModel(itemValue, itemModel));
                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }
            }
			var errMsg = QueryUtils.SaveNewMeasures(result, this);
			if (!string.IsNullOrEmpty(errMsg)) return errMsg;

			if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
			{
				errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
				if (!string.IsNullOrEmpty(errMsg)) return errMsg;
			}
				

			//Highlight after saved
			var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
			if(unsavedNum != null)
			{
				lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
			}

			return "";
		}
		alex */
		private string SaveExecute()
        {
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var batchId = itemModel.BatchId.ToString();
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            if (itemValues.Count == 0)
                return "";
            var result = new List<MeasureValueModel>();
            bool valueIn = false;
            List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel>;
            List<MeasureValueModel> toAdd = new List<MeasureValueModel>();
            List<MeasureValueModel> toRemove = new List<MeasureValueModel>();
            foreach (var itemValue in itemValues)
            {
                result.Add(new MeasureValueModel(itemValue, itemModel));
                toAdd.Add(new MeasureValueModel(itemValue, itemModel));
                foreach (var measure in measureValuesBatch)
                {
                    if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString()
                        && measure.PartId.ToString() == itemValue.PartId)
                    {
                        toRemove.Add(measure);
                        break;
                    }
                }

                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }


                /*
                foreach (var measure in measureValuesBatch)
                {
                    if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString() 
                        && measure.PartId.ToString() == itemValue.PartId)
                    {
                        measureValuesBatch.Remove(measure);
                        break;
                    }
                    measureValuesBatch.Add(new MeasureValueModel(itemValue, itemModel));
                }
                */
            }
            var UseAzureQueue = Page.Session["UseAzureQueue"].ToString();
            if (UseAzureQueue == "1")
            {
                string estimate = null;
                if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
                    estimate = "yes";
                else
                    estimate = "no";
                var errMsg1 = QueryUtils.SaveNewMeasuresToStorage(result, batchId, estimate, this);
                if (!string.IsNullOrEmpty(errMsg1)) return errMsg1;
            }
            else
            {
                var errMsg = QueryUtils.SaveNewMeasures(result, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
				//add to tracking
				int noInBatch = lstItemList.Items.Count;
				int eventID = 2;
				int formCode = 5;
				int noAffected = 1;
				long batchIdInt = itemModel.BatchId;
				string batchEventId = "";
				if (hdnColorBatchEventID.Value == "") //fist item in batch
				{

					batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchIdInt, noAffected, noInBatch, 0, this);
					if (batchEventId != "")
					{
						string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(batchEventId), 0, this);
						hdnColorBatchEventID.Value = batchEventId;
						hdnColorItemEventID.Value = itemEventId;
					}

				}
				else //next saved items
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(hdnColorBatchEventID.Value), 1, this);
					hdnColorItemEventID.Value = itemEventId;
				}

				if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
                {
                    errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
                    if (!string.IsNullOrEmpty(errMsg)) return errMsg;
                }

            }
            //Highlight after saved
            //ListItem unsavedNum = new ListItem();
            var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
            if (unsavedNum != null)
            {
                lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
            }
            else
            {
                unsavedNum = lstItemList.Items.FindByText(itemModel.FullOldItemNumber);
                if (unsavedNum != null)
                {
                    lstItemList.Items.FindByText(itemModel.FullOldItemNumber).Text = "*" + unsavedNum.Text;
                }
            }
            bool shapeChanged = false;
            foreach (var measure in toAdd)
            {
                if (measure.MeasureId == "8")
                {
                    measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
                    shapeChanged = true;
                    break;
                }
            }
			foreach (var measure in toAdd)
			{
				if (measure.MeasureId == "211")
				{
					measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
					shapeChanged = true;
					break;
				}
			}
			if (!shapeChanged)
            {
                measureValuesBatch.RemoveAll(x => toRemove.Contains(x));
                foreach (var measure in toAdd)
                    measureValuesBatch.Add(measure);
            }
            SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
            return "";
        }
		#endregion

		#region ShortReportLink
		protected void OnShortReportClick(object sender, EventArgs e)
		{
			//Check if changes have been saved
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnShortReportClick);
				return;
			}
			//Redirect to short report
			var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
			Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
		}
		#endregion

		#region ItemNumber List
		protected void OnItemListSelectedChanged(object sender, EventArgs e)
		{
			if (HasUnSavedChanges())
			{
				//PopupSaveQDialog(ModeOnItemChanges);
				SaveDlgMode.Value = ModeOnItemChanges;
				YesBtn.Style["display"] = "";
				NoBtn.Style["display"] = "";
				CancelBtn.Style["display"] = "";
				SaveDialogLbl.Style["display"] = "";
				lstItemList.Enabled = false;
				txtBatchNumber.Enabled = false;
				return;
			}
			else
            {
				YesBtn.Style["display"] = "none";
				NoBtn.Style["display"] = "none";
				CancelBtn.Style["display"] = "none";
				SaveDialogLbl.Style["display"] = "none";
				lstItemList.Enabled = true;
				txtBatchNumber.Enabled = true;
			}
			OnItemListSelected();
		}

		private void OnItemListSelected()
		{
            string tmpBatchNumber = null, itemNumber = null, matchItemNumber = null;
            if (Session["MatchItemNumber"] != null)
                matchItemNumber = (string)Session["MatchItemNumber"];
            if (Session["TMPNUMBER"] != null)
            {
                tmpBatchNumber = (string)Session["TMPNUMBER"];
                Session["TMPNUMBER"] = null;
            }
            if (matchItemNumber != null)
                itemNumber = matchItemNumber;
            else if (tmpBatchNumber != null)
                itemNumber = tmpBatchNumber;
            else
            {
                DataTable dt = (DataTable)Session["ItemList"];
                var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
                foreach (SingleItemModel item in itemList)
                {

                    string tmpSelectedValue = null;
                    if (lstItemList.SelectedValue[0] == '*')
                        tmpSelectedValue = lstItemList.SelectedValue.Substring(1);
                    else
                        tmpSelectedValue = lstItemList.SelectedValue;
                    //if (item.FullOldItemNumber == lstItemList.SelectedValue)
                    if (item.FullOldItemNumber == tmpSelectedValue)
                    {
                        itemNumber = item.FullItemNumber;
                        break;
                    }
                }

            }
            //var itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			var singleItemModel = GetItemModelFromView(itemNumber);
			if (singleItemModel == null) return;
			if (singleItemModel.StateId == DbConstants.ItemInvalidStateId)
			{
				InvalidLabel.Text = string.Format("Item {0} is invalid", singleItemModel.FullItemNumber);
			}
			else
			{
				InvalidLabel.Text = "";
			}
            ShowImagesNew(singleItemModel.Path2Picture, "Picture");//TODO implement this - done
			LoadMeasuresForEdit(itemNumber);
			PartEditing.Value = "";
			ItemEditing.Value = itemNumber;
			
			/*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Selected = true;
                */
			PartTree.Nodes[0].Selected = true;
            OnPartsTreeChanged(null, null);
			ItemEditing.Value = itemNumber;
			if (hdnColorBatchEventID != null && hdnColorBatchEventID.Value != "")//next item for existing batch event - touched
			{
				string batchEventId = hdnColorBatchEventID.Value;
				int noInBatch = lstItemList.Items.Count;
				int eventID = 3;
				int formCode = 5;
				long batchId = singleItemModel.BatchId;
                if (hdnColorItemEventID.Value != "")
                    hdnColorItemEventID.Value = "";
                else
                {
                    string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
                }
			}
		}

		private bool HasUnSavedChanges()
		{
			GetNewValuesFromGrid();
			var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			return itemValues.Count > 0;
		}
		#endregion

		#region Parts Tree
		protected void OnPartsTreeChanged(object sender, EventArgs e)
		{
			if (GetSelectedItemModel() == null) return;
			var currPart = PartTree.SelectedValue;
			var prevPart = PartEditing.Value;
            //if (PartTree.SelectedNode.Text.Contains("Stone Set"))
            //    ColorRadio.SelectedIndex = 1;
            //else
                ColorRadio.SelectedIndex = 0;
			//-- Get Changes
			if (!string.IsNullOrEmpty(prevPart) && currPart != prevPart)
			{
				GetNewValuesFromGrid();
			}

			//Set Small Buttons to altered state on Item Container
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				var elemBtn = elem.FindControl("SmBtn") as Button;
				SetButtonState(elemBtn, PartTree.SelectedNode.Text);
			}

			//In Item Container, use SS Color instead of Color
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				if ((elem.FindControl("ElementName") as HiddenField).Value == "Color")
				{
					if (PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
				}
				else if ((elem.FindControl("ElementName") as HiddenField).Value == "SS Color")
				{
					if (PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
				}
			}

			LoadDataForEditing();
            var ordernumber = lstItemList.SelectedValue;
            //alex if (sender != null)
                LoadShapeForEdit(ordernumber);
            PartEditing.Value = currPart;
			ClearButtonCategory();
		}

		private void LoadPartsTree()
		{
			PartTree.Nodes.Clear();
			var parts = GetPartsFromView();
			if (parts.Count == 0)
			{
				return;
			}
			var data = new List<TreeViewModel>();
			foreach (var part in parts)
			{
				data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
			}
			var root = TreeUtils.GetRootTreeModel(data);
			var rootNode = new TreeNode(root.DisplayName, root.Id);

			TreeUtils.FillNode(rootNode, root);
			rootNode.Expand();

			PartTree.Nodes.Add(rootNode);
            /*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Select();
                */
            PartTree.Nodes[0].Selected = true;

            OnPartsTreeChanged(null, null);
		}
		#endregion

		#region Elements
		private List<String> commentNums = new List<string> { "31", "47" };
		private void CreateMeasurePanel()
		{
			List<ExpandableControlModel> Elements = new List<ExpandableControlModel>();

			XmlDocument doc = new XmlDocument();
			doc.Load(Server.MapPath("ColorData.xml"));

			foreach (XmlNode node in doc.SelectNodes("//Element"))
			{
				ExpandableControlModel elem;
				elem = ExpandableControlModel.CreateNew(node);

				if (elem.DropDownPanelVis)
				{
					elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == elem.ElemMeasure);
				}

				if (elem.ElemType == "Standard")
				{
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNewStandard(elem.DropDownEnums);
				}
				else if (elem.ElemType == "Filter" || elem.ElemType == "Custom")
				{
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNew(node);
				}
				else if (elem.ElemType == "Comments")
				{
					/*
					foreach (XmlNode item in node.SelectNodes("CommentSource/Comments"))
					{
						commentNums.Add(item.InnerText);
					}
					*/
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNewComments(GetMeasureEnumsFromView(), node);
				}
				if (elem.ElemName.Equals("CD Color"))
				{
					//Create and fill block list
					var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ColorDiamondColors, Page);
					//Remove items with empty ValueTitle
					elem.DropDownEnums = elem.DropDownEnums.Where(x => !string.IsNullOrEmpty(x.ValueTitle.Trim())).Distinct().ToList();
					//Remove items from a list of shapes with a ValueTitle equals value from block list
					//elem.DropDownEnums = elem.DropDownEnums.Where(x => !blockList.Exists(y => x.ValueTitle.Equals(y.BlockedDisplayName))).ToList();
					elem.DropDownEnums = elem.DropDownEnums.Where(x => !blockList.Exists(y => x.MeasureValueName.Equals(y.BlockedDisplayName))).ToList();
					//remove doubles
					elem.DropDownEnums = elem.DropDownEnums.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).OrderBy(x => x.ValueTitle).ToList();
				}
				Elements.Add(elem);
			}

			ElementPanel.DataSource = Elements;
			ElementPanel.DataBind();

			//Make filtering dropdown work
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				if ((element.FindControl("ElementType") as HiddenField).Value == "Filter")
				{
					(element.FindControl("BigTxt") as TextBox).Attributes.Add("onKeyUp", "FilterShapes(this)");
					(element.FindControl("BigTxt") as TextBox).Attributes.Add("onFocus", "FilterShapes(this)");
				}
			}

			//Apply styles to particular elements
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementType") as HiddenField).Value.StartsWith("Pic"))
				{
					(item.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					(item.FindControl("Pic") as Panel).Style["display"] = "inline-block";
				}
				else
				{
					(item.FindControl("Pic") as Panel).Style["display"] = "none";
				}
			}

		}
		//On selecting item from dropdown list
		protected void OnDropDownSelectedIndexChanged(object sender, EventArgs e)
		{
			var listBox = sender as ListBox;
			var textBox = listBox.Parent.Parent.FindControl("BigTxt") as TextBox;
			var smTextBox = listBox.Parent.Parent.FindControl("SmTxt") as TextBox;
			textBox.Text = listBox.SelectedItem.Text;
			smTextBox.Text = listBox.SelectedItem.Text;
		}

		protected void OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
			var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
			bool changed = false;

			foreach (ListItem item in listBox.Items)
			{
				if (textBox.Text == item.Text)
				{
					listBox.SelectedValue = item.Value;
					smTextBox.Text = smTextBox.Text;
					changed = true;
					break;
				}
			}

			if (!changed)
			{
				if (listBox.SelectedItem != null)
				{
					textBox.Text = listBox.SelectedItem.Text;
				}
				else
				{
					textBox.Text = "";
				}
			}
		}

		protected void OnButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("RepeatButton") as Button;
			var val = e.Item.FindControl("BtnValue") as HiddenField;
			var redir = e.Item.FindControl("BtnLink") as HiddenField;
			var cat = e.Item.FindControl("BtnCat") as HiddenField;

			if ((e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value == "Comments")
			{
				var mea = e.Item.FindControl("BtnMeasure") as HiddenField;
				var buttonStyle = btn.Style["border-bottom-color"];//Comment button styles

				if (mea.Value != "0")
				{
					var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
					if (itemValues != null)
					{
						var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
						if (itemValue != null)
						{
							var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
							EnumMeasureModel enumValue;
							if (buttonStyle == "#821717" || buttonStyle == "0")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
							{
								enumValue = enumValues.Find(m => m.ValueTitle == "");
							}
							else
							{
								enumValue = enumValues.Find(m => m.MeasureValueName == val.Value);
							}

							if (enumValue != null)
							{
								var newValue = enumValue.MeasureValueId.ToString();
								itemValue.Value = newValue;
							}
							else
							{
								itemValue.Value = null;
							}
							PopulateComments();
						}
					}
				}
			}
			else if (val.Value != "0")
			{
				var ddl = e.Item.Parent.Parent.FindControl("DropDownListBox") as ListBox;
				var elemMeasure = e.Item.Parent.Parent.Parent.FindControl("ElementMeasure") as HiddenField;
				ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == val.Value).MeasureValueId.ToString();
				OnDropDownSelectedIndexChanged(ddl, null);

				//90% of all FL is blue, set FL Color to Blue if FL is Medium, Strong, or Very Strong
				if((e.Item.Parent.Parent.Parent.FindControl("ElementName") as HiddenField).Value == "FL")
				{
					if(ddl.SelectedItem.Text == "Medium" || ddl.SelectedItem.Text == "Strong" || ddl.SelectedItem.Text == "Very Strong")
					{
						foreach(RepeaterItem elem in ElementPanel.Items)
						{
							if((elem.FindControl("ElementName") as HiddenField).Value == "FL Color")
							{
								ddl = elem.FindControl("DropDownListBox") as ListBox;
								elemMeasure = elem.FindControl("ElementMeasure") as HiddenField;
								ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == "Blue").MeasureValueId.ToString();
								OnDropDownSelectedIndexChanged(ddl, null);
								break;
							}
						}
					}
                    else
                    {
                        foreach (RepeaterItem elem in ElementPanel.Items)
                        {
                            if ((elem.FindControl("ElementName") as HiddenField).Value == "FL Color")
                            {
                                ddl = elem.FindControl("DropDownListBox") as ListBox;
                                elemMeasure = elem.FindControl("ElementMeasure") as HiddenField;
                                ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == "None").MeasureValueId.ToString();
                                OnDropDownSelectedIndexChanged(ddl, null);
                                break;
                            }
                        }
                    }
				}
			}

			if (redir.Value != "0")
			{
				if (redir.Value == "Small")
				{
					ElementEditing.Value = "None";
				}
				CategoryEditing.Value = redir.Value;
				SetButtonCategory();
			}
		}

		protected void OnSmBtnClick(object sender, EventArgs e)
		{
			var btn = sender as Button;
			var smallPanel = btn.Parent as Panel;
			var bigPanel = smallPanel.Parent.FindControl("BigPanel") as Panel;
			var eleName = bigPanel.Parent.FindControl("ElementName") as HiddenField;

			ElementEditing.Value = eleName.Value;

			if (ElementEditing.Value == "Color" || ElementEditing.Value == "SS Color" || ElementEditing.Value == "FL")
			{
				CategoryEditing.Value = ColorRadio.SelectedValue;
			}
			else
			{
				CategoryEditing.Value = "Root";
			}

			SetButtonCategory();
		}

		private void SetButtonCategory()
		{
			Panel activeBigPanel = null;

			//Display big panel for the current element
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					activeBigPanel = item.FindControl("BigPanel") as Panel;
					activeBigPanel.Style["display"] = "";
				}
				else
				{
					(item.FindControl("BigPanel") as Panel).Style["display"] = "none";
				}
			}

			//Display buttons in the big panel for the current category
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					foreach (RepeaterItem btnItem in (item.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if ((btnItem.FindControl("BtnCat") as HiddenField).Value == CategoryEditing.Value)
						{
							(btnItem.FindControl("RepeatButton") as Button).Style["display"] = "";
						}
						else
						{
							(btnItem.FindControl("RepeatButton") as Button).Style["display"] = "none";
						}
					}
					break;
				}
			}
		}
		private void ClearButtonCategory()
		{
			ElementEditing.Value = "";
			CategoryEditing.Value = "";
			SetButtonCategory();
		}

		protected void ColorRadioChanged(object sender, EventArgs e)
		{
			if (ElementEditing.Value == "Color" || ElementEditing.Value == "SS Color" || ElementEditing.Value == "FL")
			{
				CategoryEditing.Value = ColorRadio.SelectedValue;
				SetButtonCategory();
			}
		}

		private void ShowImages(string path, string option)
		{
            //<asp:Image runat="server" ID="PicImg" Width="180px"></asp:Image>
            string imgPath;
			string errMsg;
			Panel imgPanel = null;
			if (option == "Shape")
			{
				//string pngPath;
				if (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\")
				{
					path = path.Substring(1);
				}
				var pngStream = new MemoryStream();
				//alex
				var fileType = "";
				var ms = new MemoryStream();
				path = path.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				//var result = Utlities.GetShapeImageUrlAzure(path, out imgPath, out errMsg, this, /*out pngPath,*/ out pngStream);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PictureShape")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;//alex "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length);	
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "1";
				}
				else
				{
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "none";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "0";
				}
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Shape");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}
			else if (option == "Picture")
			{
				var ms = new MemoryStream();
				var fileType = "";
				//var result = Utlities.GetPictureImageUrl(path, out imgPath, out errMsg, this);
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PicturePicture")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "1";
				}
				else
				{
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "none";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "0";
				}
				//-- Path to picture
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Picture");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}
			ShowHideImages(true);
		}

        private void ShowImagesNew(string path, string option)
        {
            //<asp:Image runat="server" ID="PicImg" Width="180px"></asp:Image>
            Panel imgPanel = null;
            var imgPanelName = "Picture" + option;
            var imgUrl = Utlities.GetPictureImageUrl(path, this);

            foreach (RepeaterItem elem in ElementPanel.Items)
            {
                if ((elem.FindControl("ElementType") as HiddenField).Value == imgPanelName)
                {
                    imgPanel = elem.FindControl("Pic") as Panel;
                    break;
                }
            }
            if (imgPanel != null)
            {
                var imgControl = imgPanel.FindControl("Image") as Image;
                var imgLabel = imgPanel.FindControl("PicLbl") as Label;
                if (option == "Shape")
                {
                    imgUrl = imgUrl.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
                    path = (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\") ? path.Substring(1) : path;
                    path = path.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
                }

                imgControl.ImageUrl = imgUrl;
                imgControl.Style["opacity"] = "1";
                imgControl.AlternateText = "NoImage";
                imgLabel.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, option);
            }
            ShowHideImages(true);
        }
        private void LoadShapeForEdit(string itemNumber)
        {
			itemNumber = itemNumber.TrimStart('*');

			var itemModel = GetItemModelFromView(itemNumber);
            var enumsMeasure = GetMeasureEnumsFromView();
            var parts = GetPartsFromView();
            string itemCode = itemNumber.Substring(itemNumber.Length - 2);
            //List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
            List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
            if (measureValuesBatch.Count == 0)
            {
                measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
                SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
            }
            List<MeasureValueModel> measureValues = measureValuesBatch.FindAll(m => m.ItemCode.TrimStart('0') == itemCode.TrimStart('0') && m.BatchId == itemModel.BatchId).ToList();

            //foreach (var measure in measureValuesBatch)
            //    if (Utils.FillToTwoChars(measure.ItemCode) == itemCode)
            //        measureValues.Add(measure);
            //var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            /* TODO replace this code with something that works with our Element objects - done */
            var currPart = PartTree.SelectedValue;
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
            if (withShapes.Count > 0)
            {
                var path = withShapes[0].ShapePath2Drawing;
                if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
                else ShowHideImages(false);
            }
            else
            {
                ShowHideImages(false);
            }
        }
        private void ShowHideImages(bool show)
		{
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				if (show)
				{
                    //if ((elem.FindControl("PicImg") as Image).ImageUrl != "")
                    if ((elem.FindControl("Image") as Image).ImageUrl != "")
                    {
						//(elem.FindControl("Pic") as Panel).Style["display"] = "";
						(elem.FindControl("Pic") as Panel).Style["opacity"] = "1";
					}	
				}
				else
				{
					//(elem.FindControl("Pic") as Panel).Style["display"] = "none";
					(elem.FindControl("Pic") as Panel).Style["opacity"] = "0";
				}
			}
		}
		#endregion

		#region Laser Inscription
		protected void OnLaserInscriptionChanged(object sender, EventArgs e)
		{
			var liscBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 67);
			if (itemValue != null)
			{
				itemValue.Value = liscBox.Text;
			}
		}
		#endregion
		
		#region Prefix
		protected void OnPrefixChanged(object sender, EventArgs e)
		{
			var prefBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 112);
			if (itemValue != null)
			{
				itemValue.Value = prefBox.Text;
			}
		}
		#endregion

		#region IntExtComments
		protected void OnIntExtCommentChanged(object sender, EventArgs e)
		{
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var commentBox = sender as TextBox;
			int measureId = 0;

			if (commentBox.ID == "InternalComments")
			{
				measureId = 26;
			}
			else if (commentBox.ID == "ExternalComments")
			{
				measureId = 9;
			}

			if (measureId != 0)
			{
				var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == measureId);
				if (itemValue != null)
				{
					itemValue.Value = commentBox.Text;
				}
			}
		}
		#endregion

		#region Data
		private void LoadMeasuresForEdit(string itemNumber)
		{
			SingleItemModel itemModel = GetItemModelFromView(itemNumber);
			var enumsMeasure = GetMeasureEnumsFromView();
			var parts = GetPartsFromView();

            Utils.DissectItemNumber(itemNumber, out string _, out _, out var itemCode);
            //List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
            List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
            if (measureValuesBatch.Count == 0)
            {
                measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
                SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
            }
            //foreach (var measure in measureValuesBatch)
            //    if (Utils.FillToTwoChars(measure.ItemCode) == itemCode && itemModel.BatchId == measure.BatchId)
            //        measureValues.Add(measure);
            List<MeasureValueModel> measureValues = measureValuesBatch.FindAll(m => m.ItemCode.TrimStart('0') == itemCode.TrimStart('0') && m.BatchId == itemModel.BatchId).ToList();
            //var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            var currPart = PartTree.SelectedValue;
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
			if (withShapes.Count > 0)
			{
				var path = withShapes[0].ShapePath2Drawing;
				if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
				else ShowHideImages(false);
			}
			else
			{
				ShowHideImages(false);
			}
			var measures = GetMeasuresCpFromView();
			var result = new List<ItemValueEditModel>();
			foreach (var measure in measures)
			{
				if (measure.MeasureClass > 3) continue;
				var partModel = parts.Find(m => m.PartId == measure.PartId);
				if (partModel == null) continue;
				var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
				var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
						? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
				result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
			}
			result.Sort(new ItemValueEditComparer().Compare);
			SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);
		}

		private void LoadDataForEditing()
		{
			var itemModel = GetSelectedItemModel();
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			var enumValues = GetMeasureEnumsFromView();
            string matchedItemNumber = null, tmpBatchNumber = null;
            if (Session["MatchItemNumber"] != null)
            {
                matchedItemNumber = (string)Session["MatchItemNumber"];
                Session["MatchItemNumber"] = null;
            }
                if (Session["TMPNUMBER"] != null)
            {
                tmpBatchNumber = (string)Session["TMPNUMBER"];
                Session["TMPNUMBER"] = null;
            }
            //New code
            foreach (RepeaterItem element in ElementPanel.Items)
			{
				var smallTxt = element.FindControl("SmTxt") as TextBox;
				var bigTxt = element.FindControl("BigTxt") as TextBox;
				smallTxt.Text = "";
				bigTxt.Text = "";

				var dropDownList = element.FindControl("DropDownListBox") as ListBox;
				dropDownList.SelectedIndex = -1;

				var dropDownMeasure = element.FindControl("ElementMeasure") as HiddenField;
				var dropDownValue = itemValues.Find(m => m.MeasureId.ToString() == dropDownMeasure.Value);
				if (dropDownValue != null)
				{
					var dropDownEnumValue = enumValues.Find(m => m.MeasureValueMeasureId.ToString() == dropDownMeasure.Value
						&& m.MeasureValueId.ToString() == dropDownValue.Value);
					if (dropDownEnumValue != null)
					{
						smallTxt.Text = dropDownEnumValue.ValueTitle.ToString();
						bigTxt.Text = smallTxt.Text;
						try
						{
							dropDownList.SelectedValue = dropDownValue.Value;
						}
						catch (Exception ex)
                        {
							string msg = ex.Message;
                        }
					}
				}
				else
				{
					var smallButton = element.FindControl("SmBtn") as Button;
					if (smallButton.Text == "Comments")//Disable buttons in comments
					{
						bool allDisabled = true;
						foreach (RepeaterItem bigBtn in (element.FindControl("ButtonRepeater") as Repeater).Items)
						{
							var commentMeasure = bigBtn.FindControl("BtnMeasure") as HiddenField;
							var commentValue = itemValues.Find(m => m.MeasureId.ToString() == commentMeasure.Value);
							var bigBtnBtn = bigBtn.FindControl("RepeatButton") as Button;
							if (commentValue == null && commentMeasure.Value != "0" && commentMeasure.Value != "")
							{
								SetButtonState(bigBtnBtn, "Disable");
							}
							else
							{
								SetButtonState(bigBtnBtn, "");

								if (commentMeasure.Value != "0" && commentMeasure.Value != "")
								{
									allDisabled = false;
								}
							}
						}
						if (allDisabled)
						{
							SetButtonState(smallButton, "Disabled");
						}
					}
					else
					{
						SetButtonState(smallButton, "Disable");
					}
				}
			}

			PopulateComments();

			//Populate Prefix
			PrefixText.Text = "";
			var prefixValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 112);
			if (prefixValue != null && prefixValue.Value != null)
			{
				PrefixText.Text = prefixValue.Value;
			}

			//Populate Weight
			WeightTxt.Text = "Weight:";
			WeightValue.Text = "";
			if (PartTree.SelectedNode.Text.ToLower().Contains("stone set"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 2);
				if (weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Total Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
			}
			else if (PartTree.SelectedNode.Text.ToLower().Contains("diamond"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 4);
				if (weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Measured Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
				else
				{
					weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 5);
					if (weightValue != null && weightValue.Value != "")
					{
						WeightTxt.Text = "Calculated Weight (ct):";
						WeightValue.Text = weightValue.Value;
					}
				}
			}

			//Populate Sarin Panel
			SarinRatio.Text = "";
			SarinMeasure.Text = "";
			var sarinMaxValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 11);
			var sarinMinValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 12);
			var sarinDepthValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 13);//Depth is wrong, need height! H_x is 13
			if (sarinMaxValue != null && sarinMinValue != null && sarinDepthValue != null)
			{
				//if (sarinMaxValue.Value != null && sarinMinValue.Value != null && sarinDepthValue.Value != null)
                if (!String.IsNullOrEmpty(sarinMaxValue.Value) && !String.IsNullOrEmpty(sarinMinValue.Value) && !String.IsNullOrEmpty(sarinDepthValue.Value))
				{
					double sarinRatio = double.Parse(sarinMaxValue.Value) / double.Parse(sarinMinValue.Value);
					SarinRatio.Text = sarinRatio.ToString("0.0000");
					SarinMeasure.Text = sarinMaxValue.Value + " x " + sarinMinValue.Value + " x " + sarinDepthValue.Value;
				}
			}

			//Populate Int/Ext Comments
			InternalComments.Text = "";
			ExternalComments.Text = "";
			var intComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 26);
			if (intComments != null && intComments.Value != null)
			{
				InternalComments.Text = intComments.Value;
			}
			var extComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 9);
			if (extComments != null && extComments.Value != null)
			{
				ExternalComments.Text = extComments.Value;
			}

			//Populate Laser Inscribed
			LaserInscription.Text = "";
			var linsc = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 67);
			if (linsc != null && linsc.Value != null)
			{
				LaserInscription.Text = linsc.Value;
			}

            //DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            DataTable dt = (DataTable)Session["ItemList"];
            if (dt != null)
            {
                var prevNumber = Utils.getPrevItemCode(dt, itemModel.FullItemNumber);
                if (prevNumber != null)
                    OldNumText.Text = prevNumber;
                else
                    OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            }
            if (matchedItemNumber != null)
                matchedItemNumber = matchedItemNumber.Trim();
            if (tmpBatchNumber != null)
                tmpBatchNumber = tmpBatchNumber.Trim();
            if ((itemModel.FullItemNumber == OldNumText.Text) ||
                (itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldNumText.Text))
                DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            else
            {
                if (matchedItemNumber == null || tmpBatchNumber == null || (String.Equals(matchedItemNumber, tmpBatchNumber) == true))
                    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldNumText.Text + @")";
                else
                    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + tmpBatchNumber + @")";
            }
            //OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            //if ((itemModel.FullItemNumber == OldNumText.Text) ||
            //    (itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldNumText.Text))
            //    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            //else
            //    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldNumText.Text + @")";
            ShowHideItemDetails(true);
		}
		private void PopulateComments()
		{
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			CommentsList.Text = "";
			if (itemValues.Count == 0)
			{
				return;
			}

			//Find comments element
			RepeaterItem commentElement = null;
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				if ((element.FindControl("ElementType") as HiddenField).Value == "Comments")
				{
					commentElement = element;
					break;
				}
			}

			foreach (var measureId in commentNums)
			{
				var commentValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId.ToString() == measureId);
				if (commentValue != null)
				{
					if (commentValue.Value == "0" || commentValue.Value == null)
					{
						foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
						{
							if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
							{
								SetButtonState(btn.FindControl("RepeatButton") as Button, "");
								(btn.FindControl("BtnActive") as HiddenField).Value = "0";
							}
						}
					}
					var enumValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == measureId && m.MeasureValueId.ToString() == commentValue.Value);
					if (enumValue != null)
					{
						if (enumValue.ValueTitle != "")
						{
							CommentsList.Text += enumValue.MeasureValueName;
							CommentsList.Text += "\n";
						}

						//Update style of buttons
						if (commentElement != null)
						{
							foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
							{
								if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
								{
									var rBtn = btn.FindControl("RepeatButton") as Button;
									if (rBtn.Enabled)
									{
										if (enumValue.MeasureValueName == (btn.FindControl("BtnValue") as HiddenField).Value)
										{
											SetButtonState(rBtn, "Active");
											(btn.FindControl("BtnActive") as HiddenField).Value = "1";
										}
										else
										{
											SetButtonState(rBtn, "");
											(btn.FindControl("BtnActive") as HiddenField).Value = "0";
										}
									}
								}
							}
						}
					}
				}
				else//If commentvalue is null, clear all styles
				{
					foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
						{
							var rBtn = btn.FindControl("RepeatButton") as Button;
							if (rBtn.Enabled)
							{
								SetButtonState(rBtn, "");
								(btn.FindControl("BtnActive") as HiddenField).Value = "0";
							}
						}
					}
				}
			}
		}
		private void SetButtonState(Button btn, string state)
		{
			btn.Enabled = true;
			if (state.ToUpper().Contains("ITEM CONTAINER"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else if (state.Contains("Active"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else if (state.Contains("Disable"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #b3b3b3, #4d4d4d)";
				btn.Style["border-top-color"] = "#4d4d4d";
				btn.Style["border-left-color"] = "#4d4d4d";
				btn.Style["border-right-color"] = "#4d4d4d";
				btn.Style["border-bottom-color"] = "#333333";
				btn.Enabled = false;
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}
		private void SetCommentButtonState(Button btn, bool active)
		{
			if (active)
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}
		private void SetSmallButtonState(Button btn, string part)
		{
			if (part.ToUpper().Contains("ITEM CONTAINER"))
			{
				/*
				btn.Style["background"] = "linear-gradient(to bottom, #cc0099, #990073)";
				btn.Style["border-top-color"] = "#990073";
				btn.Style["border-left-color"] = "#990073";
				btn.Style["border-right-color"] = "#990073";
				btn.Style["border-bottom-color"] = "#66004d";
				*/
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}

		void ShowHideItemDetails(bool show)
		{
			if (show)
			{
				DataForLabel.Style["display"] = "";
				PrefixPanel.Style["display"] = "";
				OldNumPanel.Style["display"] = "";
				MeasurementPanel.Style["display"] = "";
				ColorRadio.Style["display"] = "";
				Comments.Style["display"] = "";
				WeightPanel.Style["display"] = "";
				LaserPanel.Style["display"] = "";
				IntExtCommentPanel.Style["display"] = "";
				SarinPanel.Style["display"] = "";
				cmdSave.Style["display"] = "";
				shortrpt.Style["display"] = "";
			}
			else
			{
				DataForLabel.Style["display"] = "none";
				PrefixPanel.Style["display"] = "none";
				OldNumPanel.Style["display"] = "none";
				MeasurementPanel.Style["display"] = "none";
				ColorRadio.Style["display"] = "none";
				YesBtn.Style["display"] = "none";
				NoBtn.Style["display"] = "none";
				CancelBtn.Style["display"] = "none";
				SaveDialogLbl.Style["display"] = "none";
				Comments.Style["display"] = "none";
				WeightPanel.Style["display"] = "none";
				LaserPanel.Style["display"] = "none";
				IntExtCommentPanel.Style["display"] = "none";
				SarinPanel.Style["display"] = "none";
				cmdSave.Style["display"] = "none";
				shortrpt.Style["display"] = "none";
			}
			
		}

		private void GetNewValuesFromGrid()
		{
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			ItemValueEditModel itemValue;

			foreach (RepeaterItem element in ElementPanel.Items)
			{
				var eleValue = element.FindControl("ElementMeasure") as HiddenField;
				itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
				if (itemValue != null)
				{
					var eleList = element.FindControl("DropDownListBox") as ListBox;
					var newValue = eleList.SelectedValue;
					itemValue.Value = newValue;
				}
			}

			//Text boxes
			List<KeyValuePair<TextBox, int>> textBoxes = new List<KeyValuePair<TextBox, int>>
			{
				new KeyValuePair<TextBox, int>(PrefixText, 112),
				new KeyValuePair<TextBox, int>(LaserInscription, 67),
				new KeyValuePair<TextBox, int>(InternalComments, 26),
				new KeyValuePair<TextBox, int>(ExternalComments, 9)
			};

			foreach (var box in textBoxes)
			{
				itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == box.Value);
				if (itemValue != null)
				{
					itemValue.Value = box.Key.Text;
				}
			}

			//Comments
			foreach (RepeaterItem ele in ElementPanel.Items)
			{
				if ((ele.FindControl("ElementType") as HiddenField).Value == "Comments")
				{
					var btnList = ele.FindControl("ButtonRepeater") as Repeater;
					//First, set all of the values to be the null version
					foreach (RepeaterItem item in btnList.Items)
					{
						var mea = item.FindControl("BtnMeasure") as HiddenField;
						if (mea.Value != "0")
						{
							if (itemValues != null)
							{
								itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
								if (itemValue != null)
								{
									var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
									EnumMeasureModel enumValue;
									enumValue = enumValues.Find(m => m.ValueTitle == "");

									if (itemValue != null && enumValue != null && itemValue.PrevValue != "0")
									{
										var newValue = enumValue.MeasureValueId.ToString();
										itemValue.Value = newValue;
									}
								}
							}
						}
					}

					//Then, set them active upon finding an active button
					foreach (RepeaterItem item in btnList.Items)
					{
						var mea = item.FindControl("BtnMeasure") as HiddenField;
						var btnStyle = (item.FindControl("RepeatButton") as Button).Style["border-bottom-color"];
						var btnActive = (item.FindControl("BtnActive") as HiddenField).Value;
						if (mea.Value != "0")
						{
							if (itemValues != null)
							{
								itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
								if (itemValue != null)
								{
									var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
									EnumMeasureModel enumValue = null;
									if (btnActive == "1")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
									{
										enumValue = enumValues.Find(m => m.MeasureValueName == (item.FindControl("BtnValue") as HiddenField).Value);
									}

									if (itemValue != null && enumValue != null)
									{
										var newValue = enumValue.MeasureValueId.ToString();
										itemValue.Value = newValue;
									}
								}
							}
						}
					}
					break;
				}
			}
		}
		#endregion

		#region Get Data From View

		//-- Measure Values
		private List<MeasureValueModel> GetMeasureValuesFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
		}

		//-- Measure Enums
		private List<EnumMeasureModel> GetMeasureEnumsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
		}

		//-- Loading Measures Description
		private List<MeasureValueCpModel> GetMeasuresCpFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
		}

		//-- Loading Item Numbers
		private List<SingleItemModel> GetItemNumbersFromView()
		{

			return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
		}
		private SingleItemModel GetItemModelFromView(string itemNumber)
		{
            //return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber);
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber || m.FullOldItemNumber == itemNumber || m.FullBatchNumber == itemNumber);
        }
        private SingleItemModel GetSelectedItemModel()
		{
			var itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			if (string.IsNullOrEmpty(itemNumber)) return null;
			return GetItemModelFromView(itemNumber);
		}
		private List<MeasurePartModel> GetPartsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
		}
		private List<ItemValueEditModel> GetMeasuresForEditFromView(string partId)
		{
			var data = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ?? new List<ItemValueEditModel>();
			if (string.IsNullOrEmpty(partId)) return data;
			else return data.FindAll(m => m.PartId == partId);
		}
		#endregion

		#region Information Dialog
		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}

        protected void OnInfoCloseButtonClick(object sender, EventArgs e)
        {
            if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
            {
                lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                OnItemListSelectedChanged(null, null);
            }
        }

		#endregion

		#region Question Dialog
		static string ModeOnItemChanges = "item";
		static string ModeOnLoadClick = "load";
		static string ModeOnShortReportClick = "report";
		private void PopupSaveQDialog(string src)
		{
			var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
			SaveDlgMode.Value = src;
			QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
			SaveQPopupExtender.Show();
		}
		protected void OnQuesSaveYesClick(object sender, EventArgs e)
		{
			var errMsg = SaveExecute();
			if (!string.IsNullOrEmpty(errMsg))
			{
				// Return on prev item
				txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
				lstItemList.SelectedValue = ItemEditing.Value;
				PopupInfoDialog(errMsg, true);
				return;
			}

			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				LoadExecute();
			}
			if (SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
		protected void OnQuesSaveNoClick(object sender, EventArgs e)
		{
			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				LoadExecute();
			}
			if (SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
		protected void OnQuesSaveCancelClick(object sender, EventArgs e)
		{
			lstItemList.SelectedValue = ItemEditing.Value;
			txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
		#endregion
	}
}