﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Corpt
{
    public partial class TestDataList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MyGridView.DataSource = CreateDataSource1();
                MyGridView.DataBind();
                //ItemsList.DataSource = CreateDataSource();
                // ItemsList.DataBind();
            }
        }
        class TestModel
        {
            public TestModel(){}
            public int Item { get; set; }
            public int Qty { get; set; }
            public double Price { get; set; }
        }
        List<TestModel> CreateDataSource1()
        {
            var result = new List<TestModel>();
            for (int i = 0; i < 9; i++)
            {
                result.Add(new TestModel { Item = i, Qty = i * 2, Price = 1.23 * (i + 1) });
            }

            return result;
        }

        ICollection CreateDataSource()
        {

            // Create sample data for the DataList control.
            DataTable dt = new DataTable();
            DataRow dr;

            // Define the columns of the table.
            dt.Columns.Add(new DataColumn("Item", typeof(Int32)));
            dt.Columns.Add(new DataColumn("Qty", typeof(Int32)));
            dt.Columns.Add(new DataColumn("Price", typeof(double)));

            // Populate the table with sample values.
            for (int i = 0; i < 9; i++)
            {
                dr = dt.NewRow();

                dr[0] = i;
                dr[1] = i * 2;
                dr[2] = 1.23 * (i + 1);

                dt.Rows.Add(dr);
            }

            DataView dv = new DataView(dt);
            return dv;

        }
        protected void Item_Command(Object sender, DataListCommandEventArgs e)
        {

            // Set the SelectedIndex property to select an item in the DataList.
            ItemsList.SelectedIndex = e.Item.ItemIndex;

            // Rebind the data source to the DataList to refresh the control.
            ItemsList.DataSource = CreateDataSource();
            ItemsList.DataBind();

        }

        protected void OnReloadClick(object sender, EventArgs e)
        {
            var selind = ItemsList.SelectedIndex;
            ItemsList.DataSource = CreateDataSource();
            ItemsList.DataBind();
            ItemsList.SelectedIndex = selind;

//            var idx = MyGridView.SelectedIndex;
//            MyGridView.DataSource = CreateDataSource1();
//            MyGridView.DataBind();
//            MyGridView.SelectedIndex = idx;

            MyDataGrid.DataSource = CreateDataSource();
            MyDataGrid.DataBind();
        }

        protected void OnGridViewSelectedChanged(object sender, EventArgs e)
        {
            var idx = MyGridView.SelectedIndex;
        }


        protected void OnDataGridItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(MyDataGrid,
                                                                                           "SelectedIndexChanged$" + e.Item.ItemIndex));
            }
        }

        protected void OnDataGridSelectedChanged(object sender, EventArgs e)
        {
            var idx = MyDataGrid.SelectedIndex;
        }

        protected void MyDataGrid_ItemCommand(object source, DataGridCommandEventArgs e)
        {

        }
    }
}