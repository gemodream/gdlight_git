﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;
using Corpt.Utilities;

namespace Corpt
{
    public partial class ReItemizn : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!IsPostBack)
            {
                ModeFld.Value = "" + Request.Params["Mode"];
            }
            Page.Title = "GSI: " + (IsEndSessionMode() ? "End Session" : "ReItemizn");
            TitleDiv.InnerText = (IsEndSessionMode() ? "End Session" : "ReItemizn");
        }
        bool IsEndSessionMode()
        {
            return ModeFld.Value == "End";
        }

        protected void OnSearchClick(object sender, ImageClickEventArgs e)
        {
            var items = QueryItemiznUtils.GetOrderHistory(OrderField.Text, false, this);
            if (items.Count == 0)
            {
                PopupInfoDialog(string.Format("The entered Order code {0} does not exist.", OrderField.Text), true);
                
            }
            SetViewState(items);
            ShowTreeHistory(items);

        }
        private void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items)
        {
            TreeUtils.ShowTreeHistory(items, TreeHistory);
            ValidButton.Visible = TreeHistory.Visible && !IsEndSessionMode();
            InvalidButton.Visible = TreeHistory.Visible && !IsEndSessionMode(); ;
            EndSessionButton.Visible = TreeHistory.Visible && IsEndSessionMode(); ;
        }

        #region View State

        private const string OrderHistoryItems = "orderHistoryItems";

        void SetViewState(List<CustomerHistoryModel> items)
        {
            SetViewState(items, OrderHistoryItems);
        }
        SingleItemModel GetItemFromView(string id)
        {
            var orderItems = GetViewState(OrderHistoryItems) as List<CustomerHistoryModel> ?? new List<CustomerHistoryModel>();
            var itemModel = orderItems.Find(m => m.Id == id);
            if (itemModel == null) return null;
            var codes = itemModel.Id.Split('_')[1].Split('.');
            var itemCode = codes[codes.Length - 1];
            return new SingleItemModel {NewBatchId = Convert.ToInt32(itemModel.BatchId), NewItemCode = itemCode};
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Valid/Invalid Buttons
        protected void OnValidClick(object sender, EventArgs e)
        {
            UpdateItemState(DbConstants.ItemValidStateCode);

        }

        protected void OnInvalidClick(object sender, EventArgs e)
        {
            UpdateItemState(DbConstants.ItemInvalidStateCode);

        }
        private void UpdateItemState(int newState)
        {
            if (TreeHistory.CheckedNodes.Count == 0)
            {
                PopupInfoDialog("Checked Items Not Found!", true);
                return;
            }
			/*
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth < 3)     continue;
                var msg = QueryItemiznUtils.UpdateItemState(node.Value.Split('_')[1], newState, this);
                if (string.IsNullOrEmpty(msg)) continue;
                PopupInfoDialog(msg, true);
                break;
            }*/
			//12/31/2018 Update to make a single call to the DB instead of one for every item in the list	   
			List<String> data = new List<String>();
			foreach (TreeNode node in TreeHistory.CheckedNodes)
			{
				if(node.Depth < 3)
				{
					continue;
				}
				data.Add(node.Value.Split('_')[1]);
			}
			var msg = QueryItemiznUtils.UpdateItemStateBulk(data, newState, this);
			if (!string.IsNullOrEmpty(msg))
			{
				PopupInfoDialog(msg, true);
			}
			
            OnSearchClick(null, null);
        }
        #endregion

        #region End Session Button
        protected void OnEndSessionClick(object sender, EventArgs e)
        {
            if (TreeHistory.CheckedNodes.Count == 0)
            {
                PopupInfoDialog("Checked Items Not Found!", true);
                return;
            } 
            foreach (TreeNode node in TreeHistory.CheckedNodes)
            {
                if (node.Depth < 3) continue;
                var singleItemModel = GetItemFromView(node.Value);
                var msg = QueryUtils.SetEndSession(singleItemModel, this);
                if (msg.Contains("Error"))
                {
                    PopupInfoDialog(msg, true);
                    return;
                }
            }
			/*
			List<SingleItemModel> data = new List<SingleItemModel>();
			foreach (TreeNode node in TreeHistory.CheckedNodes)
			{
				if (node.Depth < 3) continue;
				var singleItemModel = GetItemFromView(node.Value);
				data.Add(singleItemModel);
			}
			var msg = QueryUtils.SetEndSessionBulk(data, this);
			if (msg.Contains("Error"))
			{
				PopupInfoDialog(msg, true);
				return;
			}
			*/
			PopupInfoDialog("End session is finished", false);
        }
        #endregion

    }
}