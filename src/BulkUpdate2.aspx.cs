﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class BulkUpdate2 : CommonPage
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (IsPostBack) return;
            SetViewState(new List<BulkUpdateViewModel>(), SessionConstants.BulkEnteredValues);
            
            //-- Load Enum Measures
            SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.BulkEnumMeasures);
            UpdateButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");

            var orderCode = Request.Params["OrderCode"] ?? "";
            var batchCode = Request.Params["BatchCode"] ?? "";
            if (!string.IsNullOrEmpty(orderCode))
            {
                OrderField.Text = Utils.FillToFiveChars(orderCode);
            }
            if (!string.IsNullOrEmpty(batchCode))
            {
                BatchField.Text = Utils.FillToThreeChars(batchCode, orderCode);
            }
            if (!string.IsNullOrEmpty(orderCode) && !string.IsNullOrEmpty(batchCode))
            {
                OnLoadClick(null, null);
            } else
            {
                OrderField.Focus();
            }
        }
        #endregion

        #region Tree Batches
        protected void TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            if (tvwItems.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in tvwItems.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        protected void OnTreeSelectedChanged(object sender, EventArgs e)
        {
            if (tvwItems.SelectedNode == null || tvwItems.SelectedNode.ChildNodes.Count > 0 ||
                tvwItems.SelectedNode.Parent == null)
            {
                ResetInfoPanel();
                return;
            }
            //-- Get Measure Values
            var batchId = Convert.ToInt32(tvwItems.SelectedNode.Parent.Value);
            var itemCode = Convert.ToInt32(tvwItems.SelectedNode.Value);
            var itemCopyModel = new ItemCopyModel { BatchId = batchId, ItemCode = itemCode };
            LoadInfoPanel(tvwItems.SelectedNode.Text, itemCopyModel);
        }
        private List<BatchItemModel> GetCheckedBatchItems()
        {
            var checkedItems = new List<BatchItemModel>();
            foreach (TreeNode node in tvwItems.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0 || node.Parent == null) continue;
                var chkItem = new BatchItemModel
                {
                    BatchId = Convert.ToInt32(node.Parent.Value),
                    ItemCode = Convert.ToInt32(node.Value)
                };
                checkedItems.Add(chkItem);
            }
            return checkedItems;
        }

        #endregion

        #region Data Panel for selected batch number
        private void LoadInfoPanel(string itemFullNumber, ItemCopyModel itemModel)
        {
            SelectedItemNumber.Text = "Data For # " + itemFullNumber;
            var measures = QueryUtils.GetMeasures(itemModel, this);
            dgMeasures.DataSource = measures;
            dgMeasures.DataBind();
        }
        private void ResetInfoPanel()
        {
            SelectedItemNumber.Text = "";
            dgMeasures.DataSource = null;
            dgMeasures.DataBind();
        }
        #endregion

        #region Load button
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            LoadMsgLabel.Text = "";
            //-- Load Similar Batches
            var order = OrderField.Text.Trim();
            var batchNumber = BatchField.Text.Trim();
            var batchList = QueryUtils.GetSimilarBatches(order, batchNumber, this);
            SetViewState(batchList, SessionConstants.BulkBatches);
            if (batchList.Count == 0)
            {
                LoadMsgLabel.Text = "Batch number " + Utils.FillToFiveChars(order) + Utils.FillToThreeChars(batchNumber, order) + " not found";
                return;
            }

            //-- Load Tree Batches
            var itemList = QueryUtils.GetItemsByOrderCode(order, this);
            var data = new List<TreeViewModel> {new TreeViewModel {ParentId = "", Id = order, DisplayName = order}};
            foreach(var batch in batchList)
            {
                var items = itemList.FindAll(m => m.BatchId == batch.BatchId);
                if (items.Count == 0) continue;
                var batchTvModel = new TreeViewModel
                {
                    ParentId = order,
                    Id = batch.BatchId,
                    DisplayName = batch.FullBatchNumberWithDot
                };
                data.Add(batchTvModel);
                foreach(var item in items)
                {
                    data.Add(new TreeViewModel { ParentId = item.BatchId, Id = Utils.FillToTwoChars(item.ItemCode), DisplayName = item.FullItemNumberWithDotes });
                }
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            tvwItems.Nodes.Add(rootNode);
            
            //-- Load Document Parts
            var parts = QueryUtils.GetPartsMeasure(OrderField.Text.Trim(), BatchField.Text, this);
            SetViewState(parts, SessionConstants.BulkParts);
            PartsListField.DataSource = parts;
            PartsListField.DataBind();

            //-- Clear preview measures
            SetViewState(null, SessionConstants.BulkMeasures);
            if (!string.IsNullOrEmpty(PartsListField.SelectedValue))
            {
                OnPartChanged(null, null);
            }
            SetInitControlsState(false);
        }
        #endregion

        #region Parts Combo
        protected void OnPartChanged(object sender, EventArgs e)
        {
            var batch = GetFirstBatch();
            if (batch == null)
            {
                MeasureListField.Items.Clear();
                return;
            }
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel> ?? new List<MeasureModel>();
            if (measures.Count == 0)
            {
                measures = QueryUtils.GetMeasures(batch, IsByCpMode(), this);
                SetViewState(measures, SessionConstants.BulkMeasures);
            }
            var measuresByType = measures.FindAll(m => m.PartId == GetPartIdSelected());

            measuresByType.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));

            MeasureListField.DataSource = measuresByType;
            MeasureListField.DataBind();
            if (!string.IsNullOrEmpty(MeasureListField.SelectedValue))
            {
                OnMeasureChanged(null, null);
            }
            else
            {
                ShowValuePanels(0);
            }
        }
        private int GetPartIdSelected()
        {
            var value = PartsListField.SelectedValue;
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
        }
        public PartModel GetPartModel(int partId)
        {
            var parts = GetViewState(SessionConstants.BulkParts) as List<PartModel> ?? new List<PartModel>();
            return parts.Find(m => m.PartId == partId);
        }
        #endregion

        #region Measure List
        protected void OnMeasureChanged(object sender, EventArgs e)
        {
            var measure = GetMeasureSelected();
            if (measure == null) return;
            if (measure.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                SetEnumData(measure.MeasureId);
            }
            ShowValuePanels(measure.MeasureClass);
        }
        private MeasureModel GetMeasureSelected()
        {
            var value = MeasureListField.SelectedValue;
            if (string.IsNullOrEmpty(value)) return null;
            var measures = GetViewState(SessionConstants.BulkMeasures) as List<MeasureModel> ?? new List<MeasureModel>();
            return measures.Find(m => m.MeasureId == Convert.ToInt32(value));
        }

        #endregion

        #region Measure Enum Field
        private void SetEnumData(int measureId)
        {
            var enumMeasures = GetViewState(SessionConstants.BulkEnumMeasures) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
            var enums = enumMeasures.FindAll(m => m.MeasureValueMeasureId == measureId);
            if (enums.Find(m => m.MeasureValueId == 0) == null)
            {
                enums.Add(EnumMeasureModel.GetEmptyItem(measureId));
            }
            enums.Sort((m1, m2) => m1.NOrder.CompareTo(m2.NOrder));
            MeasureEnumField.DataSource = enums;
            MeasureEnumField.DataBind();
        }
        protected void OnEnumMeasureChanged(object sender, EventArgs e)
        {
            if (MeasureEnumField.SelectedValue == "0")
            {
                //-- Value not set
                return;
            }
            AddEnteredValue(MeasureEnumField.SelectedValue, MeasureEnumField.SelectedItem.Text);
        }
        #endregion
        
        #region Utils
        private bool IsByCpMode()
        {
            return !IgnoreCpBox.Checked;
        }
        private BatchModel GetFirstBatch()
        {
            var batches = GetViewState(SessionConstants.BulkBatches) as List<BatchModel>;
            return batches == null ? null : batches[0];
        }
        private void ShowValuePanels(int measureClass)
        {
            if (measureClass == 0)
            {
                //-- Hide All
                MeasureEnumField.Visible = false;
                MeasureNumericField.Visible = false;
                MeasureTextField.Visible = false;
                return;
            }
            MeasureEnumField.Visible = measureClass == MeasureModel.MeasureClassEnum;
            if (MeasureEnumField.Visible)
            {
                MeasureEnumField.SelectedIndex = 0;
                MeasureEnumField.Focus();
            }

            MeasureNumericField.Visible = measureClass == MeasureModel.MeasureClassNumeric;
            if (MeasureNumericField.Visible)
            {
                MeasureNumericField.Text = "";
                MeasureNumericField.Focus();
            }
            
            MeasureTextField.Visible = measureClass == MeasureModel.MeasureClassText;
            if (MeasureTextField.Visible)
            {
                MeasureTextField.Text = "";
                MeasureTextField.Focus();
            }
        }
        #endregion

        #region Update button
        protected void OnUpdateButtonClick(object sender, EventArgs e)
        {
            var checkedItems = GetCheckedBatchItems();
            if (checkedItems.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Items Not Found!" + "\")</SCRIPT>");
                return;
            }

            var enteredValues = GetEnteredValues();
            var valuesList = enteredValues.Select(value => value.MeasureValue).ToList();
            var updateModel = new BulkUpdateMeasureModel
            {
                Items = checkedItems,
                MeasureValues = valuesList,
                ItemTypeId = GetFirstBatch().ItemTypeId,
            };
            var msg = QueryUtils.SaveMeasureValues(updateModel, this);
            if (!string.IsNullOrEmpty(msg))
            {
                System.Web.HttpContext.Current.Response.Write(
                        "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            
            //-- Refresh Data Panel
            OnTreeSelectedChanged(null, null);
            SetViewState(new List<BulkUpdateViewModel>(), SessionConstants.BulkEnteredValues);
            ShowEnteredValues();
            //changes saved
            msg = "Changes saved";
            System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
        }

        #endregion

        #region Next Order button
        protected void OnNextOrderClick(object sender, EventArgs e)
        {
            //-- Clear All
            SetInitControlsState(true);

        }
        //-- On Load button - false
        //-- On Next Order button - true
        private void SetInitControlsState(bool enable)
        {
            LoadButton.Enabled = enable;
            OrderField.Enabled = enable;
            BatchField.Enabled = enable;
            NextOrderButton.Enabled = !enable;
            IgnoreCpBox.Enabled = enable;
            EditPanel.Visible = !enable;

            if (enable)
            {
                OrderField.Text = "";
                BatchField.Text = "";
                ShowValuePanels(0);
                PartsListField.Items.Clear();
                MeasureListField.Items.Clear();
                MeasureEnumField.Items.Clear();
                tvwItems.Nodes.Clear();
                ResetInfoPanel();

            }
        }
        #endregion

        #region NumericField - On Changed Value
        protected void OnNumericFieldChanged(object sender, EventArgs e)
        {
            AddEnteredValue(MeasureNumericField.Text, "");
        }
        #endregion

        #region StringField - On Changed Value
        protected void OnTextFieldChanged(object sender, EventArgs e)
        {
            AddEnteredValue(MeasureTextField.Text, "");
        }
        #endregion

        #region Entered Measure Values
        private List<BulkUpdateViewModel> GetEnteredValues()
        {
            return GetViewState(SessionConstants.BulkEnteredValues) as List<BulkUpdateViewModel> ??
                         new List<BulkUpdateViewModel>();
        }
        private void AddEnteredValue(object value, string valueTitle)
        {
            var values = GetEnteredValues();
            var partId = GetPartIdSelected();
            var partModel = GetPartModel(partId);
            if (partModel == null) return;
            var measure = GetMeasureSelected();
            var enteredValue = values.Find(m => m.MeasureValue.PartId == partId && m.MeasureValue.MeasureId == "" + measure.MeasureId);
            if (enteredValue == null)
            {
                enteredValue = new BulkUpdateViewModel();
                enteredValue.MeasureValue.MeasureClass = measure.MeasureClass;
                enteredValue.MeasureValue.MeasureId = "" + measure.MeasureId;
                enteredValue.MeasureName = measure.MeasureName;
                
                enteredValue.MeasureValue.PartId = partModel.PartId;
                enteredValue.PartName = partModel.PartName;

                values.Add(enteredValue);
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassText)
            {
                enteredValue.MeasureValue.StringValue = "" + value;
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                enteredValue.MeasureValue.MeasureValue = "" + value;
            }
            if (enteredValue.MeasureValue.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                enteredValue.MeasureValue.MeasureValueId = "" + value;
                enteredValue.MeasureValue.EnumValueTitle = valueTitle;
            }
            ShowEnteredValues();
        }
        private void ShowEnteredValues()
        {
            var values = GetEnteredValues();
            EnteredLabel.Visible = values.Count > 0;
            EnteredGrid.DataSource = values.Count > 0 ? values : null;
            EnteredGrid.DataBind();
            SetButtonsState();
        }
        protected void OnDelCommand(object source, DataGridCommandEventArgs e)
        {
            var uniqueKey = "" + EnteredGrid.DataKeys[e.Item.ItemIndex];
            var partId = Convert.ToInt32(uniqueKey.Split(';')[0]);
            var measureId = Convert.ToInt32(uniqueKey.Split(';')[1]);
            var values = GetEnteredValues();
            var value = values.Find(m => m.MeasureValue.PartId == partId && m.MeasureValue.MeasureId == "" + measureId);
            if (value == null) return;
            values.Remove(value);
            ShowEnteredValues();
        }
        #endregion
        
        #region State Buttons
        private void SetButtonsState()
        {
            var hasChanges = GetEnteredValues().Count > 0;
            UpdateButton.Enabled = hasChanges;
            UndoAllButton.Enabled = hasChanges;
            NextOrderButton.Enabled = !hasChanges;
        }
        #endregion

        #region Undo All Button
        protected void OnUndoAllButtonClick(object sender, EventArgs e)
        {
            SetViewState(new List<BulkUpdateViewModel>(), SessionConstants.BulkEnteredValues);
            ShowEnteredValues();
        }
        #endregion
    }
}