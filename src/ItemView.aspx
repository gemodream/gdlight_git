﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ItemView.aspx.cs" Inherits="Corpt.ItemView" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
    

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server" >
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <style>
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
    </style>
    <script type="text/javascript">
//        $(window).resize(function () {
//            gridviewScrollPercentFull();
//        });
//        $(document).ready(function () {
//            gridviewScrollPercentFull();
//
//        });
//        function gridviewScrollPercentFull() {
//            var widthGrid = $('#gridContainerFull').width();
//            var heightGrid = $('#gridContainerFull').height();
//            $('#<%=tblFullReport.ClientID%>').gridviewScroll({
//                width: widthGrid,
//                height: heightGrid,
//                freezesize: 1
//            });
//        }
    </script>
    <div class="demoarea">
        <div class="demoheading">Items View</div>
        <!-- Error message-->
        <div class="control-group error">
            <asp:Label class="control-label" id="WrongInfoLabel" runat="server" />
        </div>
        <div class="control-group info">
            <asp:Label ID="lblInfo" runat="server" />
        </div>
        <asp:HyperLink ID="lnkMovedItems" runat="server" CssClass="btn-link" NavigateUrl="ShowMovedItems.aspx"> Items moved from this batch to another batches </asp:HyperLink>
        
        <asp:Panel ID="ShortReportPanel" runat="server" Visible="False">
            <div class="gridheading"><asp:Label ID="headerShortReport" runat="server" Text="Short Data"></asp:Label></div>
            <div style="margin-bottom: 10px">
                <div style="margin-bottom: 10px">
                    <!-- 'Download Excel' button -->
                    <span>
                        <asp:ImageButton ID="cmdDownloadExcel" ToolTip="Download to Excel" runat="server"
                            ImageUrl="~/Images/ajaxImages/excel.jpg" OnClick="CmdDownloadExcelClick" />
                    </span>
                    <!-- 'Email Short Report' button -->
                    <span style="margin-top: 15px; padding-top: 10px;padding-left: 10px">E-mail:
                        <asp:DropDownList runat="server" ID="MailAddressList" DataValueField="Email" DataTextField="DisplayName" AutoPostBack="True" OnSelectedIndexChanged="OnChoiceMailFromContacts"/>
                        <asp:Label ID="Label1" runat="server" Text="Address To: " Style="padding-left: 10px"></asp:Label>
                        <asp:TextBox runat="server" ID="AddressTo" Width="200px" placeholder="Email"></asp:TextBox>
                        <asp:ImageButton ID="GoEmailButton" ToolTip="Email Short Report" runat="server" ImageUrl="~/Images/ajaxImages/email_go.png"
                            OnClick="OnSendEmailClick" />
                    </span>
                    <span>
                        <asp:LinkButton ID="btnMovedItems" runat="server" OnClientClick="return false;" Visible="False" Style="margin-left: 5px">Items moved from this batch to another batches</asp:LinkButton>
<%--                        <asp:Button ID="btnMovedItems" runat="server" OnClientClick="return false;" Text="Moved Items"--%>
<%--                            Style="margin-left: 5px" CssClass="btn-default btn-small" Visible="false" />--%>
                    </span>
                </div>
                <!-- Item Numbers Short Report-->
                <div id="gridContainerFull" style="font-size: small">
                    <asp:DataGrid ID="tblFullReport" runat="server" AutoGenerateColumns="true" OnItemDataBound="OnItemDataBound"
                        CellPadding="5">
                        <ItemStyle ForeColor="#0D0D0D" BackColor="White" BorderColor="silver"></ItemStyle>
<%--                        <HeaderStyle CssClass="GridviewScrollHeader" />--%>
<%--                        <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>--%>
<%--                        <PagerStyle CssClass="GridviewScrollPager" />--%>
                    <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" Font-Size="small"/>
                    </asp:DataGrid>
                </div>
            </div>
        </asp:Panel>

        <!--- Doc Structure -->
        <asp:Panel runat="server" ID="PropertiesPanel" Visible="False">
            <div class="gridheading"><asp:Label ID="headerDocStruct" runat="server" Text="Item Properties"></asp:Label></div>
                <div id="gridContainerDet" style="border: dotted thin silver;font-size: small;">
                    <asp:DataGrid ID="tblDocStructure" runat="server" AutoGenerateColumns="true" CellPadding="5" CellSpacing="5">
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <ItemStyle ForeColor="#0D0D0D"></ItemStyle>
                    </asp:DataGrid>
                </div>
        </asp:Panel>
    <!-- Moved Items region -->
    <div style="margin: 0px; margin: 5px;">
        
        <!-- "Wire frame" div used to transition from the button to the info panel -->
        <div id="flyout" style="display: none; overflow: hidden; z-index: 2; background-color: #FFFFFF; border: solid 1px #D0D0D0;"></div>
        
        <!-- Info panel to be displayed as a flyout when the button is clicked -->
        <div id="info" style="display: none; width:400px; z-index: 2; opacity: 0; 
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); 
            font-size: 12px; font-family: Cambria; border: solid 1px #CCCCCC; background-color: #FFFFFF; padding: 5px;">
            <!-- Close Button -->
            <div id="btnCloseParent" style="float: right; opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);">
                <asp:LinkButton id="btnClose" runat="server" OnClientClick="return false;" Text="X" ToolTip="Close"
                    Style="background-color: #666666; color: #FFFFFF; text-align: center; font-weight: bold; text-decoration: none; border: outset thin #FFFFFF; padding: 2px;" />
            </div>
			<!-- Data Grid with Removed Items -->
            <div style="overflow-x: auto; overflow-y: auto; padding: 2px;height: 400px; width: 400px;">
                <asp:DataGrid runat="server" ID="dgPopup" CssClass="table table-bordered" >
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:DataGrid>
            </div>
        </div>
        
        <script type="text/javascript" language="javascript">
            // Move an element directly on top of another element (and optionally
            // make it the same size)
            function Cover(bottom, top, ignoreSize) {
                var location = Sys.UI.DomElement.getLocation(bottom);
                top.style.position = 'absolute';
                top.style.top = location.y + 'px';
                top.style.left = location.x + 'px';
                if (!ignoreSize) {
                    top.style.height = bottom.offsetHeight + 'px';
                    top.style.width = bottom.offsetWidth + 'px';
                }
            }
        </script>
        <ajaxToolkit:AnimationExtender id="OpenAnimation" runat="server" TargetControlID="btnMovedItems">
            <Animations>
                <OnClick>
                    <Sequence>
                        <%-- Disable the button so it can't be clicked again --%>
                        <EnableAction Enabled="false" />
                        
                        <%-- Position the wire frame on top of the button and show it --%>
                        <ScriptAction Script="Cover($get('ctl00_SampleContent_btnMovedItems'), $get('flyout'));" />
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="block"/>
                        
                        <%-- Move the wire frame from the button's bounds to the info panel's bounds --%>
                        <Parallel AnimationTarget="flyout" Duration=".3" Fps="25">
                            <Move Horizontal="150" Vertical="-50" />
                            <Resize Width="400" Height="400" />
                            <Color PropertyKey="backgroundColor" StartValue="#AAAAAA" EndValue="#FFFFFF" />
                        </Parallel>
                        
                        <%-- Move the info panel on top of the wire frame, fade it in, and hide the frame --%>
                        <ScriptAction Script="Cover($get('flyout'), $get('info'), true);" />
                        <StyleAction AnimationTarget="info" Attribute="display" Value="block"/>
                        <FadeIn AnimationTarget="info" Duration=".2"/>
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="none"/>
                        
                        <%-- Flash the text/border red and fade in the "close" button --%>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#666666" EndValue="#FF0000" />
                            <Color PropertyKey="borderColor" StartValue="#666666" EndValue="#FF0000" />
                        </Parallel>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#FF0000" EndValue="#666666" />
                            <Color PropertyKey="borderColor" StartValue="#FF0000" EndValue="#666666" />
                            <FadeIn AnimationTarget="btnCloseParent" MaximumOpacity=".9" />
                        </Parallel>
                    </Sequence>
                </OnClick>
            </Animations>
        </ajaxToolkit:AnimationExtender>
        <ajaxToolkit:AnimationExtender id="CloseAnimation" runat="server" TargetControlID="btnClose">
            <Animations>
                <OnClick>
                    <Sequence AnimationTarget="info">
                        <%--  Shrink the info panel out of view --%>
                        <StyleAction Attribute="overflow" Value="hidden"/>
                        <Parallel Duration=".3" Fps="15">
                            <Scale ScaleFactor="0.05" Center="true" ScaleFont="true" FontUnit="px" />
                            <FadeOut />
                        </Parallel>
                        
                        <%--  Reset the sample so it can be played again --%>
                        <StyleAction Attribute="display" Value="none"/>
                        <StyleAction Attribute="width" Value="400px"/>
                        <StyleAction Attribute="height" Value=""/>
                        <StyleAction Attribute="fontSize" Value="12px"/>
                        <OpacityAction AnimationTarget="btnCloseParent" Opacity="0" />
                        
                        <%--  Enable the button so it can be played again --%>
                        <EnableAction AnimationTarget="btnMovedItems" Enabled="true" />
                    </Sequence>
                </OnClick>
                <OnMouseOver>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FFFFFF" EndValue="#FF0000" />
                </OnMouseOver>
                <OnMouseOut>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FF0000" EndValue="#FFFFFF" />
                </OnMouseOut>
             </Animations>
        </ajaxToolkit:AnimationExtender>
    </div>

    </div>
</asp:Content>
