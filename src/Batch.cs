using System;
using System.Data;

namespace Corpt
{
	/// <summary>
	/// Summary description for Batch.
	/// </summary>
	public class Batch
	{
		private DataRow dtBatch;

		private Batch()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Batch(DataRow drBatchInfo)
		{
			dtBatch = drBatchInfo;
		}

		public string FullBatchNumber
		{
			get
			{
				return Utils.FullBatchNumber(dtBatch["GroupCode"].ToString(),dtBatch["BatchCode"].ToString());
			}
		}

		public string OrderCode
		{
			get
			{
				return Utils.FullGroupNumber(dtBatch["GroupCode"].ToString());
			}
		}

		public int BatchID
		{
			get
			{
				return int.Parse(dtBatch["BatchID"].ToString());
			}
		}		

		public override string ToString()
		{
			return Utils.FullBatchNumber(dtBatch["GroupCode"].ToString(),dtBatch["BatchCode"].ToString());
		}

	}
}
