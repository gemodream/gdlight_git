﻿using Corpt.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Utilities;
using Corpt.TreeModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Corpt
{
	public partial class Clarity2 : CommonPage
	{
		#region Page Load
		protected void Page_Load(object sender, EventArgs e)
		{
			//IsShapes = false;
			if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Clarity";

			OrderRegExpr.ValidationExpression = PageConstants.BatchNumberPattern;
			OrderReq.ValidationGroup = PageConstants.BatchNumberPattern;
			ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
			scriptManager.RegisterPostBackControl(this.ShapeEditButton);
			if (!IsPostBack)
            {
                string measureIds = QueryUtils.GetGradeMeasureIds(this);
                Session["MeasureIds"] = measureIds;
				CleanUp();
                SetViewState(QueryUtils.GetEnumMeasure(this), SessionConstants.GradeMeasureEnums);
                SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
                ShapeEditButton.Visible = false;
            }
            else
                ShapeEditButton.Visible = true;
            txtBatchNumber.Focus();
		}
		#endregion

		#region CleanUp (on PageLoad, on Load button click)
		private void CleanUp()
		{
			CleanItemListArea();
			CleanPartTreeArea();
			//HidePicturePanel();//TODO modify this - done, see line below
			//HideShapePanel();
			ShowHideImages(false);
			CleanValueGridArea();
			CleanViewState();

			InvalidLabel.Text = "";
		}
		private void CleanItemListArea()
		{
			lstItemList.Items.Clear();
			ItemEditing.Value = "";
			lstItemList.Style["display"] = "none";
			InvalidLabel.Text = "";
		}
		private void CleanPartTreeArea()
		{
			PartTree.Nodes.Clear();
			PartEditing.Value = "";
			OldTextNumber.Value = "";
			LIPresentDiv.Visible = false;
		}
		private void CleanValueGridArea()
		{
			//ItemValuesGrid.DataSource = null;
			//ItemValuesGrid.DataBind();

			//New code for unbinding data sources
			ElementPanel.DataSource = null;
			ElementPanel.DataBind();

			//Make Measurement Panel Invisible
			ShowHideItemDetails(false);
			/*
			MeasurementPanel.Visible = false;
			DataForLabel.Visible = false;
			ClarityRadio.Visible = false;
			Comments.Visible = false;
			WeightPanel.Visible = false;
			IntExtCommentPanel.Visible = false;
			LaserPanel.Visible = false;
			SarinPanel.Visible = false;
			*/
		}
		private void CleanViewState()
		{
			//-- Item numbers
			SetViewState(new List<SingleItemModel>(), SessionConstants.GradeItemNumbers);

			//-- Measure Parts
			SetViewState(new List<MeasurePartModel>(), SessionConstants.GradeMeasureParts);

			//-- Measure Descriptions
			SetViewState(new List<MeasureValueCpModel>(), SessionConstants.GradeMeasureValuesCp);

			//-- Item Measure Values
			SetViewState(new List<ItemValueEditModel>(), SessionConstants.ShortReportExtMeasuresForEdit);
            
        }
		#endregion

		#region Load
		int ListMaxRow = 40;
		int ListMinRow = 10;
		protected void OnLoadClick(object sender, EventArgs e)
		{
            Session["ItemList"] = null;
            if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnLoadClick);
				return;
			}
			DSSCheckBox.Checked = false;
			LoadExecute();
		}

		private void LoadExecute()
		{
            //Save current lstItemList temporarily
            Session["TMPNUMBER"] = null;
            Session["MatchItemNumber"] = null;
			hdnBatchEventID.Value = "";
			hdnItemEventID.Value = "";
            List<string> prevList = new List<string>();
            List<string> fullPrevList = new List<string>();
            string prevItemNumber = "0";
			var prevModel = GetItemModelFromView(ItemEditing.Value);
			if (prevModel != null)
			{
                prevItemNumber = prevModel.FullBatchNumber;
                //prevItemNumber = prevModel.FullOldItemNumber;
                foreach (ListItem item in lstItemList.Items)
				{
					if (item.Text.StartsWith("*"))
					{
						prevList.Add(item.Text.Substring(1));
                        fullPrevList.Add(item.Text.Substring(1));
                    }
                    else
                        fullPrevList.Add(item.Text);
                }
			}

			CleanUp();
			if (txtBatchNumber.Text.Trim().Length == 0)
			{
				InvalidLabel.Text = "A Batch Number is required!";
				return;
			}
            OldTextNumber.Value = txtBatchNumber.Text;
            //Check 7digit prefix
            var myResult = QueryUtils.GetItemNumberBy7digit(txtBatchNumber.Text.Trim(), this.Page);
			if (myResult.Trim() != "")
			{
				txtBatchNumber.Text = myResult;
			}
			DataTable customerCodeDt = QueryUtils.GetCustomerCodeByItem(txtBatchNumber.Text, this.Page);
			if (customerCodeDt.Rows.Count > 0)
				CustomerCodeValue.Value = customerCodeDt.Rows[0].ItemArray[0].ToString();
			//-- Get ItemNumbers by BatchNumber
			DataTable dt = new DataTable();
            dt = QueryUtils.GetItemsCpOldNew(txtBatchNumber.Text.Trim(), this.Page);
            //var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row)).ToList();
            var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
            Session["ItemList"] = dt;
            //var itemList = QueryUtils.GetItemsCp(txtBatchNumber.Text.Trim(), this.Page);
            if (itemList.Count == 0)
			{
				InvalidLabel.Text = "Items not found";
				return;
			}
			SetViewState(itemList, SessionConstants.GradeItemNumbers);

			//-- Refresh batchNumber
			var tempTxtBatchNumber = txtBatchNumber.Text;//Sergey
			var itemNumber = itemList[0];
            string matchedItemNumber = null;
            if (txtBatchNumber.Text != itemNumber.FullBatchNumber)//not batch
            {
                RemeasParamModel matchItemNumber = QueryUtils.GetNewItemNumber(txtBatchNumber.Text, this);
                Session["MatchItemNumber"] = matchItemNumber.NewItemNumber;
                matchedItemNumber = matchItemNumber.NewItemNumber;
            }
            List<string> newItemList = Utils.GetOriginalItemListValues(dt, itemList, txtBatchNumber.Text, matchedItemNumber);

            /* alex
			if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
			{
				txtBatchNumber.Text = itemNumber.FullBatchNumber;
			}
            alex */
            string prevItemCode = null;
            if (itemNumber.FullBatchNumber != txtBatchNumber.Text)
            {
                int numberLength = txtBatchNumber.Text.Length;
                if (numberLength == 10 || numberLength == 11)//item
                {

                    if (itemNumber.FullBatchNumber != txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2))//search by batch
                    {
                        
                        //prevItemCode = Utils.getPrevItemCode(dt, txtBatchNumber.Text);//search by item
                        prevItemCode = Utils.getPrevItemCodeNew(itemList, txtBatchNumber.Text, matchedItemNumber);//search by item
                        if (prevItemCode != null)
                        {
                            txtBatchNumber.Text = prevItemCode;
                            //tempTxtBatchNumber = prevItemCode;
                        }
                    }
                }
                else //batch
                    txtBatchNumber.Text = itemNumber.FullBatchNumber;
            }
                //-- Loading ItemList control
                lstItemList.Items.Clear();
            if (newItemList == null)
            {
                foreach (var singleItemModel in itemList)
                {
                    lstItemList.Items.Add(singleItemModel.FullItemNumber);
                }
            }
            else
            {
                txtBatchNumber.Text = Utils.getPrevItemCodeNew(itemList, tempTxtBatchNumber, matchedItemNumber);
                foreach (var item in newItemList)
                    lstItemList.Items.Add(item);
            }
            /*
            foreach (var singleItemModel in itemList)
			{
				lstItemList.Items.Add(singleItemModel.FullItemNumber);
			}
			if (itemList.Count > ListMaxRow)
			{
				lstItemList.Rows = ListMaxRow;
			}
			else if (itemList.Count < ListMinRow)
			{
				lstItemList.Rows = ListMinRow;
			}
			else
			{
				lstItemList.Rows = itemList.Count + 1;
			}
            */
            if (itemList.Count > ListMaxRow)
            {
                lstItemList.Rows = ListMaxRow;
            }
            else if (itemList.Count < ListMinRow)
            {
                lstItemList.Rows = ListMinRow;
            }
            else
            {
                //alex oldnew
                if (newItemList == null)
                    lstItemList.Rows = itemList.Count + 1;
                else
                    lstItemList.Rows = itemList.Count + 1;
            }
            lstItemList.Style["display"] = "";

			//-- Loading Parts by first ItemNumber and show on PartTree Control
			var parts = QueryUtils.GetMeasureParts(itemNumber.ItemTypeId, this);
			SetViewState(parts, SessionConstants.GradeMeasureParts);
			LoadPartsTree();

			//-- Loading Measures by Cp or ItemTypeId
			if (IgnoreCpFlag.Checked)
			{
				var measures = QueryUtils.GetMeasureListByAcces(itemNumber, "", true, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}
			else
			{
				var measures = QueryUtils.GetMeasureValuesCp(itemNumber, this);
				SetViewState(measures, SessionConstants.GradeMeasureValuesCp);
			}

            //Sergey: Selects the exact item on load, rather than the first
            /*
			var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
			if (selectListItem == null)
                selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
            if (selectListItem == null)
            {
				lstItemList.SelectedIndex = 0;
			}
			else
			{
				lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
			}
            */
            if (newItemList == null)
            {
                var selectListItem = itemList.Find(m => m.FullItemNumber == tempTxtBatchNumber);
                if (selectListItem == null)
                    selectListItem = itemList.Find(m => m.FullItemNumber == prevItemCode);
                if (selectListItem == null)
                {
                    lstItemList.SelectedIndex = 0;
                }
                else
                {
                    lstItemList.SelectedValue = selectListItem.FullItemNumber.ToString();
                }
            }
            else
            {
                string selectListItem = null;
                //selectListItem = newItemList.Find(m => m == tempTxtBatchNumber);
                selectListItem = newItemList.Find(m => m == txtBatchNumber.Text);
                if (selectListItem == null)
                {
                    lstItemList.SelectedIndex = 0;
                }
                else
                    //lstItemList.SelectedValue = tempTxtBatchNumber;
                    lstItemList.SelectedValue = txtBatchNumber.Text;
                    //lstItemList.SelectedValue = matchedItemNumber;
            }
            //If same batch as previous, mark saved entries
            //if (prevItemNumber == txtBatchNumber.Text)
            /*alex if (prevItemNumber != "0" && ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length-2)) ||
                prevItemNumber.Substring(0, prevItemNumber.Length-2) == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)))
                */
            bool inPrevList = false;
            string matchItem = null;
            if (fullPrevList.Count > 0 && ((matchItem = fullPrevList.Find(p => p == txtBatchNumber.Text)) != null))
                    inPrevList = true;
			else
				AddToPrefixBox.Text = "";
			//foreach (var item in fullPrevList)
			//{
			//    if (item.ToString() == txtBatchNumber.Text)
			//    {
			//        inPrevList = true;
			//        break;
			//    }
			//}
			if ((prevItemNumber == txtBatchNumber.Text) || (prevItemNumber == txtBatchNumber.Text.Substring(0, txtBatchNumber.Text.Length - 2)) || inPrevList)
            {
				foreach (ListItem item in lstItemList.Items)
				{
                    string pItem = null;
                    if ((pItem = prevList.Find(p => p == item.Text)) != null)
                        item.Text = "*" + item.Text;
                    //foreach (var prevItem in prevList)
                    //{
                    //	if (prevItem == item.Text)
                    //	{
                    //		item.Text = "*" + item.Text;
                    //	}
                    //}
                }
			}
			else//Otherwise reset the Radio Buttons
			{
                //if (ClarityRadio.SelectedIndex < 1)
				    ClarityRadio.SelectedIndex = 0;
				SetViewState(new List<MeasureValueModel>(), SessionConstants.MeasureValueBatch);
			}

			ShowHideItemDetails(true);
            /*
			ClarityRadio.Visible = true;
			Comments.Visible = true;
			WeightPanel.Visible = true;
			SarinPanel.Visible = true;
			IntExtCommentPanel.Visible = true;
			LaserPanel.Visible = true;
			*/
            if (tempTxtBatchNumber.Length >= 10 && (txtBatchNumber.Text != tempTxtBatchNumber && txtBatchNumber.Text != tempTxtBatchNumber.Substring(0, tempTxtBatchNumber.Length - 2)))
                Session["TMPNUMBER"] = tempTxtBatchNumber;
            CreateMeasurePanel();
			OnItemListSelectedChanged(null, null);
			int noInBatch = lstItemList.Items.Count;
			int eventID = 3;
			int formCode = 4;
			int noAffected = 0;
			long batchId = itemList[0].BatchId;
			if (tempTxtBatchNumber.Length < 10)//batch
			{
				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);
				if (batchEventId != "")
                {
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
			else
            {
				//int noInBatch = lstItemList.Items.Count;
				//int eventID = 3;
				//int formCode = 4;
				//int noAffected = 0;
				//long batchId = itemList[0].BatchId;
				string batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchId, noAffected, noInBatch, 0, this);

				if (batchEventId != "")
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemList[0].FullItemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
		}//LoadExecute
        #endregion
        protected void OnSubmitClick(object sender, EventArgs e)
        {
            var result = new List<MeasureValueModel>();
            result = ViewState["Results"] as List<MeasureValueModel>;
            if (result.Count > 0)
            {
                var errMsg = QueryUtils.SaveNewMeasures(result, this);
                if (string.IsNullOrEmpty(errMsg))
                {
                    var estResults = new List<SingleItemModel>();
                    if (ViewState["EstResults"] != null)
                        estResults = ViewState["EstResults"] as List<SingleItemModel>;
                    if (estResults.Count > 0)
                    {
                        foreach (var estResult in estResults)
                        {
                            errMsg = QueryUtils.SetEstimatedValues(estResult, this);
                            if (!string.IsNullOrEmpty(errMsg))
                                PopupInfoDialog(errMsg, true);
                        }
                    }
					//-- Reload Item Values
					//OnItemListSelected();
					//PopupInfoDialog("Changes were updated successfully for " + result.Count.ToString() + " parameters.", false);
					OnInfoCloseButtonClick(null, null);
					InvalidLabel.Visible = true;
					InvalidLabel.Text = "Changes were updated successfully for " + result.Count.ToString() + " parameters.";
					//if(lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
					//{
					//    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
					//    OnItemListSelectedChanged(null, null);
					//}
				}
                else
                {
                    PopupInfoDialog(errMsg, true);
                }
            }
        }
        #region Save
        protected void OnSaveClick(object sender, EventArgs e)
		{
			//-- no changes
			if (!HasUnSavedChanges())
			{
				//PopupInfoDialog("No changes!", false);
				InvalidLabel.Text = "No Changes!";
				return;
			}
			var errMsg = SaveExecute();
			if (string.IsNullOrEmpty(errMsg))
			{
				//-- Reload Item Values
				OnItemListSelected();
				InvalidLabel.Text = "Changes were updated successfully.";
				OnInfoCloseButtonClick(null, null);
				InvalidLabel.Text = "Changes were updated successfully.";
				InvalidLabel.Visible = true;
				//PopupInfoDialog("Changes were updated successfully.", false);
                //if(lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
                //{
                //    lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                //    OnItemListSelectedChanged(null, null);
                //}
			}
			else
			{
				PopupInfoDialog(errMsg, true);
			}
			return;
		}
        /* alex submit
        private string SaveExecute()
        {
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            var result = new List<MeasureValueModel>();
            if (ViewState["Results"] != null)
                result = ViewState["Results"] as List<MeasureValueModel>;
            bool valueIn = false;
            foreach (var itemValue in itemValues)
            {
                result.Add(new MeasureValueModel(itemValue, itemModel));
                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }
            }
            ViewState["Results"] = result;
            string errMsg = null;
            //var errMsg = QueryUtils.SaveNewMeasures(result, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;

            //Only run SetEstimatedValues if CutGrade or ColorGrade exists and isn't blank
            //if (GetMeasuresForEditFromView("").Exists(m => (m.MeasureId == 25 || m.MeasureId == 27) && !String.Equals(m.Value, "0")))
            var estResult = new List<SingleItemModel>();
            if (ViewState["EstResults"] != null)
                estResult = ViewState["EstResults"] as List<SingleItemModel>;
            //alex
            var abc = QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView(""));
            //if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
            if (abc && valueIn)
            {
                estResult.Add(itemModel);
                //11,12,14,15,16,18,19,20,22,24,27,77,80,81,92,93,100,117,179
                //errMsg = QueryUtils.SetEstimatedValues(itemModel, this); alex removed
                ViewState["EstResults"] = estResult;
                //if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            }
            //Highlight after saved
            //lstItemList.Items.FindByText(itemModel.FullItemNumber).Attributes.Add("style", "color: green");//For some reason, this doesn't last!
            var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
            if (unsavedNum != null)
            {
                lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
            }

            return "";
        }
        

        private string SaveExecute()
        {
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            var result = new List<MeasureValueModel>();
            bool valueIn = false;
            foreach (var itemValue in itemValues)
            {
                result.Add(new MeasureValueModel(itemValue, itemModel));
                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }
            }
            var errMsg = QueryUtils.SaveNewMeasures(result, this);
            if (!string.IsNullOrEmpty(errMsg)) return errMsg;

            if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
            {
                errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            }


            //Highlight after saved
            var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
            if (unsavedNum != null)
            {
                lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
            }

            return "";
        }
        */
        
        private string SaveExecute()
        {
            var itemModel = GetItemModelFromView(ItemEditing.Value);
            var batchId = itemModel.BatchId.ToString();
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);

            //if (itemValues.Count == 0)
            //             return "";

            bool hasChanged = false;
            hasChanged = itemValues.Count > 0;
            /*if (.
             * != null)
            {
                if (Session["OriginalExtDescription"].ToString() != txtExtDescription.Text.Trim())
                {
                    hasChanged = true;
                }
            }*/
            if (hasChanged == false)
                return "";

            var result = new List<MeasureValueModel>();
            bool valueIn = false;
            List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel>;
            List<MeasureValueModel> toAdd = new List<MeasureValueModel>();
            List<MeasureValueModel> toRemove = new List<MeasureValueModel>();
            foreach (var itemValue in itemValues)
            {
				if (itemValue.MeasureId == 26)
                {
					itemValue.Value = itemValue.Value.Replace("\n", ",");
                }
                result.Add(new MeasureValueModel(itemValue, itemModel));
                toAdd.Add(new MeasureValueModel(itemValue, itemModel));
                foreach (var measure in measureValuesBatch)
                {
                    if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString()
                        && measure.PartId.ToString() == itemValue.PartId)
                    {
                        toRemove.Add(measure);
                        break;
                    }
                }

                var measureId = itemValue.MeasureId.ToString();
                if (!valueIn)
                {
                    string ids = (string)Session["MeasureIds"];
                    valueIn = QueryUtils.IsValueInNew(ids, measureId);
                }


                /*
                foreach (var measure in measureValuesBatch)
                {
                    if (measure.BatchId == itemModel.BatchId && measure.ItemCode == itemModel.ItemCode && measure.MeasureId == itemValue.MeasureId.ToString() 
                        && measure.PartId.ToString() == itemValue.PartId)
                    {
                        measureValuesBatch.Remove(measure);
                        break;
                    }
                    measureValuesBatch.Add(new MeasureValueModel(itemValue, itemModel));
                }
                */
            }
            var UseAzureQueue = Page.Session["UseAzureQueue"].ToString();
            if (UseAzureQueue == "1")
            {
                string estimate = null;
                if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
                    estimate = "yes";
                else
                    estimate = "no";
                var errMsg1 = QueryUtils.SaveNewMeasuresToStorage(result, batchId, estimate, this);
                if (!string.IsNullOrEmpty(errMsg1)) return errMsg1;
            }
            else
            {
                var errMsg = QueryUtils.SaveNewMeasures(result, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
				//add tracking
				int noInBatch = lstItemList.Items.Count;
				int eventID = 2;
				int formCode = 4;
				int noAffected = 1;
				long batchIdInt = itemModel.BatchId;
				string batchEventId = "";
				if (hdnBatchEventID.Value == "") //first item in batch
                {
					
					batchEventId = QueryCpUtilsNew.SetBatchEvent(formCode, eventID, batchIdInt, noAffected, noInBatch, 0, this);
					if (batchEventId != "")
					{
						string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(batchEventId), 0, this);
						hdnBatchEventID.Value = batchEventId;
						hdnItemEventID.Value = itemEventId;
					}

				}
				else //next saved items
                {
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemModel.FullItemNumber, batchIdInt, Convert.ToInt32(hdnBatchEventID.Value), 1, this);
					hdnItemEventID.Value = itemEventId;
				}
                if (QueryUtils.CheckSetEstimatedValues(GetMeasuresForEditFromView("")) && valueIn)
                {
                    errMsg = QueryUtils.SetEstimatedValues(itemModel, this);
                    if (!string.IsNullOrEmpty(errMsg)) return errMsg;
                }
            }

            //Highlight after saved
            var unsavedNum = lstItemList.Items.FindByText(itemModel.FullItemNumber);
            if (unsavedNum != null)
            {
                lstItemList.Items.FindByText(itemModel.FullItemNumber).Text = "*" + unsavedNum.Text;
            }
            else
            {
                unsavedNum = lstItemList.Items.FindByText(itemModel.FullOldItemNumber);
                if (unsavedNum != null)
                {
                    lstItemList.Items.FindByText(itemModel.FullOldItemNumber).Text = "*" + unsavedNum.Text;
                }
            }
            bool shapeChanged = false;
            foreach(var measure in toAdd)
            {
                if (measure.MeasureId == "8")
                {
                    measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
                    shapeChanged = true;
                    break;
                }
            }
			foreach (var measure in toAdd)
			{
				if (measure.MeasureId == "211")
				{
					measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
					shapeChanged = true;
					break;
				}
			}
			if (!shapeChanged)
            { 
                measureValuesBatch.RemoveAll(x => toRemove.Contains(x));
                foreach (var measure in toAdd)
                    measureValuesBatch.Add(measure);
			}
			bool ssNotexamined = false, extComChanged = false;
			foreach (var measure in toAdd)
            {
				if (measure.MeasureId == "9")
                {
					extComChanged = true;
					if (measure.StringValue.Contains("Side Stones not examined"))
					{
						ssNotexamined = true;
						break;
					}
                }
            }
			if (extComChanged)
			{
				var parts = GetPartsFromView();
				List<MeasureValueModel> toAddExtCom = new List<MeasureValueModel>();
				var extComList = measureValuesBatch.FindAll(m => m.MeasureId == "9");
				foreach (var extCom in extComList)
                {
					if (QueryUtils.IsStone(extCom, parts))
					{
						if (ssNotexamined)
                        {
							if (!extCom.StringValue.ToLower().Contains("side stones not examined"))
								extCom.StringValue += "Side Stones not examined";
						}
						else
                        {
							if (extCom.StringValue.ToLower().Contains("side stones not examined"))
							{
								extCom.StringValue = extCom.StringValue.ToLower();
								extCom.StringValue = extCom.StringValue.Replace("side stones not examined", "");
							}

						}
					}

                }
				//if (toAddExtComments.Count > 0)
				//{
				//	foreach (var measure in toAddExtComments)
				//	{
				//		var oldMeasure = measureValuesBatch.Find(m => m.ItemCode == measure.ItemCode && m.MeasureId == measure.MeasureId && m.PartId == measure.PartId);
				//		oldMeasure.StringValue = measure.StringValue;
				//		var test = measureValuesBatch.Find(m => m.ItemCode == measure.ItemCode && m.MeasureId == measure.MeasureId && m.PartId == measure.PartId);
				//	}
				//}
			}

            /*if (Session["OriginalExtDescription"].ToString() != txtExtDescription.Text.Trim())
            {
                var errMsg = QueryUtils.SetExtendedDescription(txtExtDescription.Text.Trim(), int.Parse(PartTree.SelectedValue), itemModel.BatchId, int.Parse(itemModel.ItemCode),38, this);
                if (!string.IsNullOrEmpty(errMsg)) return errMsg;
            }*/

            SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
            return "";
        }

        private string SaveNewMeasuresToStorage(List<MeasureValueModel> measureValueList, string batchId)
        {
            DataTable dataInputs = new DataTable("SaveMeasures");
            dataInputs.Columns.Add("BatchID", typeof(int));
            dataInputs.Columns.Add("ItemCode", typeof(int));
            dataInputs.Columns.Add("PartID", typeof(int));
            dataInputs.Columns.Add("MeasureID", typeof(int));
            dataInputs.Columns.Add("MeasureValueID", typeof(int));
            dataInputs.Columns.Add("MeasureValue", typeof(double));
            dataInputs.Columns.Add("StringValue", typeof(String));
            dataInputs.Columns.Add("AuthorOfficeID", typeof(String));
            dataInputs.Columns.Add("CurrentOfficeID", typeof(String));
            dataInputs.Columns.Add("ID", typeof(String));
            string authorOfficeID = Session["AuthorOfficeID"].ToString();
            string currentOfficeID = Session["AuthorOfficeID"].ToString();
            string authorID = Session["ID"].ToString();
            DataRow row;

            foreach (MeasureValueModel model in measureValueList)
            {
                row = dataInputs.NewRow();
                row[0] = model.BatchId;
                row[1] = model.ItemCode;
                row[2] = model.PartId;
                row[3] = model.MeasureId;
                row[4] = model.MeasureValueIdSql;
                row[5] = model.MeasureValueSql;
                row[6] = model.StringValueSql;
                row[7] = authorOfficeID;
                row[8] = currentOfficeID;
                row[9] = authorID;


                dataInputs.Rows.Add(row);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool msgSent = SendMessageToStorage(ms);
                if (msgSent)
                    return null;
                else
                    return "problem sending message";
                /*
                string appendix = Path.GetRandomFileName();
                bool xmlSent = SendXMLToStorage(ms, batchId, appendix);
                if (xmlSent)
                {
                    bool msgSent = true;
                    msgSent = SendMessageToStorage(ms, batchId, appendix);
                    if (msgSent)
                        return "success";
                    else
                        return "problem sending message";
                }
                else
                    return "problem sending XML";
                    */
            }
        }

        private bool SendMessageToStorage(MemoryStream ms)
        {
            string myAccountName = "gdlightstorage";
            string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
            CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            string queueName = "testqueue";
            CloudQueue queue = queueClient.GetQueueReference(queueName);
            queue.CreateIfNotExists();
            //queue.AddMessage(new CloudQueueMessage(batchId + @"_" + appendix + @".xml"));
            var msg = ms.ToArray();
            queue.AddMessage(new CloudQueueMessage(msg));

            return true;
        }//SendMessageToStorage
        #endregion

        #region ShortReportLink
        protected void OnShortReportClick(object sender, EventArgs e)
		{
			//Check if changes have been saved
			if (HasUnSavedChanges())
			{
				PopupSaveQDialog(ModeOnShortReportClick);
				return;
			}
			//Redirect to short report
			var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
			Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
		}
        #endregion
        protected void CopyToLI_click(object sender, EventArgs e)
        {
            LaserInscription.Text = PrefixText.Text;
            var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
            var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 67);
            if (itemValue != null)
            {
                itemValue.Value = LaserInscription.Text;
            }
        }
        #region ItemNumber List
        protected void OnItemListSelectedChanged(object sender, EventArgs e)
		{
			if (HasUnSavedChanges())
			{
				//PopupSaveQDialog(ModeOnItemChanges);
				SaveDlgMode.Value = ModeOnItemChanges;
				var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
				QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
				YesBtn.Style["display"] = "";
				NoBtn.Style["display"] = "";
				CancelBtn.Style["display"] = "";
				SaveDialogLbl.Style["display"] = "";
				lstItemList.Enabled = false;
				txtBatchNumber.Enabled = false;
				return;
			}
			else
            {
				YesBtn.Style["display"] = "none";
				NoBtn.Style["display"] = "none";
				CancelBtn.Style["display"] = "none";
				SaveDialogLbl.Style["display"] = "none";
				lstItemList.Enabled = true;
				txtBatchNumber.Enabled = true;
			}
			OnItemListSelected();
		}

		private void OnItemListSelected()
		{
            string tmpBatchNumber = null, itemNumber = null, matchItemNumber = null;
            if (Session["MatchItemNumber"] != null)
                matchItemNumber = (string)Session["MatchItemNumber"];
            if (Session["TMPNUMBER"] != null)
            {
                tmpBatchNumber = (string)Session["TMPNUMBER"];
                //Session["TMPNUMBER"] = null;
            }
            if (matchItemNumber != null)
            {
                itemNumber = matchItemNumber;
                //Session["MatchItemNumber"] = null;
            }
            else if (tmpBatchNumber != null)
                itemNumber = tmpBatchNumber;
            else
            {
                DataTable dt = (DataTable)Session["ItemList"];
                var itemList = (from DataRow row in dt.Rows select new SingleItemModel(row, null)).ToList();
                foreach (SingleItemModel item in itemList)
                {

                    string tmpSelectedValue = null;
                    if (lstItemList.SelectedValue[0] == '*')
                        tmpSelectedValue = lstItemList.SelectedValue.Substring(1);
                    else
                        tmpSelectedValue = lstItemList.SelectedValue;
                    //if (item.FullOldItemNumber == lstItemList.SelectedValue)
                    if (item.FullOldItemNumber == tmpSelectedValue)
                    {
                        itemNumber = item.FullItemNumber;
                        break;
                    }
                }

            }
            //itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			var singleItemModel = GetItemModelFromView(itemNumber);//alex 3 items
			if (singleItemModel == null) return;
			if (singleItemModel.StateId == DbConstants.ItemInvalidStateId)
			{
				InvalidLabel.Text = string.Format("Item {0} is invalid", singleItemModel.FullItemNumber);
			}
			else
			{
				InvalidLabel.Text = "";
			}
            ShowImagesNew(singleItemModel.Path2Picture, "Picture");//TODO implement this - done
			LoadMeasuresForEdit(itemNumber);
			PartEditing.Value = "";
            PartTree.Nodes[0].Selected = true;
            /*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Selected = true;
            */
            OnPartsTreeChanged(null, null);
			ItemEditing.Value = itemNumber;
			if (hdnBatchEventID != null && hdnBatchEventID.Value != "")//next item for existing batch event - touched
			{
				string batchEventId = hdnBatchEventID.Value;
				int noInBatch = lstItemList.Items.Count;
				int eventID = 3;
				int formCode = 4;
				long batchId = singleItemModel.BatchId;
				if (hdnItemEventID.Value != "")
					hdnItemEventID.Value = "";
				else
				{
					string itemEventId = QueryCpUtilsNew.SetItemEvent(formCode, eventID, itemNumber, batchId, Convert.ToInt32(batchEventId), 0, this);
				}
			}
		}//OnItemListSelected

		private bool HasUnSavedChanges()
		{
			//GetNewValuesFromGrid();
			//var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			//return itemValues.Count > 0;

            bool hasChanged = false;
            GetNewValuesFromGrid();
            var itemValues = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
            hasChanged = itemValues.Count > 0;
            /*if (Session["OriginalExtDescription"] != null)
            {
                if (Session["OriginalExtDescription"].ToString() != txtExtDescription.Text.Trim())
                {
                    hasChanged = true;
                }
            }*/
            return hasChanged;

        }
		#endregion

		#region Parts Tree
		protected void OnPartsTreeChanged(object sender, EventArgs e)
		{
            if (PartTree.SelectedNode.Text.Contains("Stone Set"))
                ClarityRadio.SelectedIndex = 1;
            else
                ClarityRadio.SelectedIndex = 0;
            if (GetSelectedItemModel() == null) return;
			var currPart = PartTree.SelectedValue;
			var prevPart = PartEditing.Value;
            if (PartTree.SelectedNode.Text.ToUpper().Contains("DIAMOND"))
                ShapeEditButton.Enabled = true;
            else
                ShapeEditButton.Enabled = false;

            //-- Get Changes
            if (!string.IsNullOrEmpty(prevPart) && currPart != prevPart)
			{
				GetNewValuesFromGrid();
			}

			//Set Small Buttons to altered state on Item Container
			foreach (RepeaterItem elem in ElementPanel.Items)
			{
				var elemBtn = elem.FindControl("SmBtn") as Button;
				SetButtonState(elemBtn, PartTree.SelectedNode.Text);
				if(elemBtn.Text=="DiamondQuality")
					elemBtn.Style["font-size"] = "16px";

			}

			//In Item Container, use SS Clarity instead of Clarity
			foreach(RepeaterItem elem in ElementPanel.Items)
			{
				if((elem.FindControl("ElementName") as HiddenField).Value == "Clarity")
				{
					//if(PartTree.SelectedNode.Text.Contains("Item Container"))
					if(PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER") || PartTree.SelectedNode.Text.ToUpper().Contains("COLOR STONE"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
				}
				else if((elem.FindControl("ElementName") as HiddenField).Value == "SS Clarity")
				{
					if (PartTree.SelectedNode.Text.ToUpper().Contains("ITEM CONTAINER"))
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
					}
					else
					{
						(elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					}
				}
				//alex
                else if ((elem.FindControl("ElementName") as HiddenField).Value == "Color Stone Clarity")
                {
                    if (PartTree.SelectedNode.Text.ToUpper().Contains("COLOR STONE"))
                    {
                        (elem.FindControl("SmallPanel") as Panel).Style["display"] = "";
                    }
                    else
                    {
                        (elem.FindControl("SmallPanel") as Panel).Style["display"] = "none";
                    }
                }
				//alex
				/*IvanB start*/
				//for Shapes we remove extender after select of tree part
				else if ((elem.FindControl("ElementName") as HiddenField).Value == "Shapes")
				{
					elem.FindControl("DropDownExtender").Dispose();
				}
				/*IvanB end*/
			}

			LoadDataForEditing();
            var ordernumber = lstItemList.SelectedValue;
            //alex if (sender != null)
                LoadShapeForEdit(ordernumber);
			PartEditing.Value = currPart;
			ClearButtonCategory();
			if (CommentsList.Text.Length > 0)
				CommentsList.BackColor = System.Drawing.Color.Coral;
			else
				CommentsList.BackColor = System.Drawing.Color.White;
			if (DiaQualityList.Text.Length > 0)
				DiaQualityList.BackColor = System.Drawing.Color.Coral;
			else
				DiaQualityList.BackColor = System.Drawing.Color.White;
		}

		private void LoadPartsTree()
		{
			PartTree.Nodes.Clear();
			var parts = GetPartsFromView();
			if (parts.Count == 0)
			{
				return;
			}
			var data = new List<TreeViewModel>();
			foreach (var part in parts)
			{
				data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
			}
			var root = TreeUtils.GetRootTreeModel(data);
			var rootNode = new TreeNode(root.DisplayName, root.Id);

			TreeUtils.FillNode(rootNode, root);
			rootNode.Expand();

			PartTree.Nodes.Add(rootNode);
            PartTree.Nodes[0].Selected = true;
            /*
            bool found = false;
            for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
            {
                if (PartTree.Nodes[0].ChildNodes[i].Text.ToUpper().Contains("DIAMOND"))
                {
                    PartTree.Nodes[0].ChildNodes[i].Select();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.ToUpper().Contains("DIAMOND"))
                            {
                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].Select();
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Text.ToUpper().Contains("DIAMOND"))
                                    {
                                        PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].Select();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
            {
                for (int i = 0; i <= PartTree.Nodes[0].ChildNodes.Count - 1; i++)
                {
                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count > 0)
                    {
                        for (int j = 0; j <= PartTree.Nodes[0].ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count > 0)
                            {
                                for (int x = 0; x <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1; x++)
                                {
                                    if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count > 0)
                                    {
                                        for (int y = 0; y <= PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes.Count - 1; y++)
                                            if (PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Text.ToUpper().Contains("DIAMOND"))
                                            {
                                                PartTree.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes[x].ChildNodes[y].Select();
                                                found = true;
                                                break;
                                            }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                    if (found)
                        break;
                }
            }
            if (!found)
                PartTree.Nodes[0].Selected = true;
            */
            OnPartsTreeChanged(null, null);
		}
		#endregion

		#region Elements
		private List<String> commentNums = new List<String>{"48", "49", "51", "86", "97", "241" };
		//private List<String> diaQualityNums = new List<String> { "274", "278", "282", "103", "285", "277", "105", "293", "291", "292", "104", "288", "289", "290", "287" };
		private List<String> diaQualityNums = new List<String> { "280", "281", "282", "103", "285", "277", "105", "286", "284", "292", "104", "288", "289", "290", "287", "102" };

		private void CreateMeasurePanel()
		{
			List<ExpandableControlModel> Elements = new List<ExpandableControlModel>();

			XmlDocument doc = new XmlDocument();
			doc.Load(Server.MapPath("ClarityData.xml"));

			foreach (XmlNode node in doc.SelectNodes("//Element"))
			{
				ExpandableControlModel elem;
				elem = ExpandableControlModel.CreateNew(node);

				if (elem.DropDownPanelVis)
				{
					elem.DropDownEnums = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == elem.ElemMeasure);
				}

				if (elem.ElemType == "Standard")
				{
					var buttonList = ButtonDetailModel.CreateNewStandard(elem.DropDownEnums);
					if (elem.ElemMeasure == "18" || elem.ElemMeasure == "19")
					{
						var noneEntry = buttonList[0];
						buttonList.Remove(buttonList[0]);
						buttonList.Insert(buttonList.Count() - 1, noneEntry);
					}
					elem.BtnRepeaterSource = buttonList;
				}
				else if (elem.ElemType == "Filter" || elem.ElemType == "Custom")
				{
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNew(node);
				}
				else if (elem.ElemType == "Comments")
				{
					/*
					foreach (XmlNode item in node.SelectNodes("CommentSource/Comments"))
					{
						commentNums.Add(item.InnerText);
					}
					*/
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNewComments(GetMeasureEnumsFromView(), node);
				}
				else if (elem.ElemType == "DiamondQuality")
				{
					elem.BtnRepeaterSource = ButtonDetailModel.CreateNewDiamondQuality(node); 
				}
				/*IvanB start*/
				if (elem.ElemName.Equals("Shapes"))
				{
					//Create and fill block list
					var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
					//Remove items with empty ValueTitle
					elem.DropDownEnums = elem.DropDownEnums.Where(x => !string.IsNullOrEmpty(x.ValueTitle.Trim())).Distinct().ToList();
					//Remove items from a list of shapes with a ValueTitle equals value from block list
					elem.DropDownEnums = elem.DropDownEnums.Where(x => !blockList.Exists(y => x.ValueTitle.Equals(y.BlockedDisplayName))).ToList();
					//remove doubles
					elem.DropDownEnums = elem.DropDownEnums.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).OrderBy(x => x.ValueTitle).ToList();
				}
				/*IvanB end*/

				Elements.Add(elem);
			}

			ElementPanel.DataSource = Elements;
			ElementPanel.DataBind();

			////Make filtering dropdown work
			//foreach(RepeaterItem element in ElementPanel.Items)
			//{
			//	if((element.FindControl("ElementType") as HiddenField).Value == "Filter")
			//	{
			//		(element.FindControl("BigTxt") as TextBox).Attributes.Add("onKeyUp", "FilterShapes(this)");
			//		(element.FindControl("BigTxt") as TextBox).Attributes.Add("onFocus", "FilterShapes(this)");
			//	}
			//}

			//Apply styles to particular elements
			foreach(RepeaterItem item in ElementPanel.Items)
			{
				if((item.FindControl("ElementType") as HiddenField).Value.StartsWith("Pic"))
				{
					(item.FindControl("SmallPanel") as Panel).Style["display"] = "none";
					(item.FindControl("Pic") as Panel).Style["display"] = "inline-block";
					//(item.FindControl("Pic") as Panel).Style["clear"] = "right";
				}
				else
				{
					(item.FindControl("Pic") as Panel).Style["display"] = "none";
				}
				/*IvanB start*/
				if ((item.FindControl("BigLbl") as Label).Text.Contains("Shapes"))
				{
					//For Shapes dropbox to add select2 we hide textbox and show dropDown panel, plus remove extender
					(item.FindControl("DropDownListBox") as ListBox).Attributes.Add("class", "filtered-select");
					(item.FindControl("BigTxt") as TextBox).Style["display"] = "none";
					(item.FindControl("DropDownListPanel") as Panel).Style["display"] = "inline-block";
					(item.FindControl("DropDownExtender")).Dispose();					
				}
				/*IvanB end*/
			}
		}
		//On selecting item from dropdown list
		protected void OnDropDownSelectedIndexChanged(object sender, EventArgs e)
		{
			var listBox = sender as ListBox;
			var textBox = listBox.Parent.Parent.FindControl("BigTxt") as TextBox;
			var smTextBox = listBox.Parent.Parent.FindControl("SmTxt") as TextBox;
			textBox.Text = listBox.SelectedItem.Text;
			smTextBox.Text = listBox.SelectedItem.Text;
			/*IvanB start*/
			//for Shapes we remove extender after select again
			var label = listBox.Parent.Parent.FindControl("BigLbl") as Label;
			if (label.Text == "Shapes")
			{
				listBox.Parent.Parent.FindControl("DropDownExtender").Dispose();
			}
			/*IvanB end*/
		}

		protected void OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			var smTextBox = textBox.Parent.FindControl("SmTxt") as TextBox;
			var listBox = textBox.Parent.FindControl("DropDownListBox") as ListBox;
			bool changed = false;

			foreach(ListItem item in listBox.Items)
			{
				if(textBox.Text == item.Text)
				{
					listBox.SelectedValue = item.Value;
					smTextBox.Text = smTextBox.Text;
					changed = true;
					break;
				}
			}

			if (!changed)
			{
				if(listBox.SelectedItem != null)
				{
					textBox.Text = listBox.SelectedItem.Text;
				}
				else
				{
					textBox.Text = "";
				}
			}
		}

		protected void OnButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("RepeatButton") as Button;
			var val = e.Item.FindControl("BtnValue") as HiddenField;
			var redir = e.Item.FindControl("BtnLink") as HiddenField;
			var cat = e.Item.FindControl("BtnCat") as HiddenField;

			if((e.Item.Parent.Parent.Parent.FindControl("ElementType") as HiddenField).Value == "Comments")
			{
				var mea = e.Item.FindControl("BtnMeasure") as HiddenField;
				var buttonStyle = btn.Style["border-bottom-color"];//Comment button styles

				if (mea.Value != "0")
				{
					var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
					if(itemValues != null)
					{
						var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
						if(itemValue != null)
						{
							var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
							EnumMeasureModel enumValue;
							if (buttonStyle == "#821717" || buttonStyle == "0")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
							{
								enumValue = enumValues.Find(m => m.ValueTitle == "");
								//btn.Style["background-color"] = "rgb(73,175,205)";
							}
							else
							{
								enumValue = enumValues.Find(m => m.MeasureValueName == val.Value);
								//btn.Style["background-color"] = "red";

								//Find control that is red, if any, for that measure value, and change it to default
								/*
								var buttonList = e.Item.Parent as Repeater;
								foreach (RepeaterItem button in buttonList.Items)
								{
									if ((button.FindControl("BtnMeasure") as HiddenField).Value == mea.Value)
									{
										if (btn != button.FindControl("RepeatButton") as Button && (button.FindControl("RepeatButton") as Button).Style["background-color"] == "red")
										{
											SetCommentButtonState(button.FindControl("RepeatButton") as Button, false);
											//(button.FindControl("RepeatButton") as Button).Style["background-color"] = "rgb(73,175,205)";
											break;
										}
									}
								}
								*/
							}

							if (itemValue != null && enumValue != null)
							{
								var newValue = enumValue.MeasureValueId.ToString();
								itemValue.Value = newValue;
							}
							PopulateComments();
						}
					}
				}
			}
			else if(val.Value != "0")
			{
				var ddl = e.Item.Parent.Parent.FindControl("DropDownListBox") as ListBox;
				var elemMeasure = e.Item.Parent.Parent.Parent.FindControl("ElementMeasure") as HiddenField;
				ddl.SelectedValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == elemMeasure.Value && m.ValueTitle.ToString() == val.Value).MeasureValueId.ToString();
				OnDropDownSelectedIndexChanged(ddl, null);
			}

			if(redir.Value != "0")
			{
				if(redir.Value == "Small")
				{
					ElementEditing.Value = "None";
				}
				CategoryEditing.Value = redir.Value;
				SetButtonCategory();
			}
		}

		protected void OnSmBtnClick(object sender, EventArgs e)
		{
			var btn = sender as Button;
			var smallPanel = btn.Parent as Panel;
			var bigPanel = smallPanel.Parent.FindControl("BigPanel") as Panel;
			var eleName = bigPanel.Parent.FindControl("ElementName") as HiddenField;

			ElementEditing.Value = eleName.Value;

			if(ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
			{
				CategoryEditing.Value = ClarityRadio.SelectedValue;
			}
			else
			{
				CategoryEditing.Value = "Root";
			}

			SetButtonCategory();
		}

		private void SetButtonCategory()
		{
			Panel activeBigPanel = null;

			//Display big panel for the current element
			foreach (RepeaterItem item in ElementPanel.Items)
			{							  
				if ((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					activeBigPanel = item.FindControl("BigPanel") as Panel;

					//(item.FindControl("SmallPanel") as Panel).Visible = false;
					activeBigPanel.Style["display"] = "";
				}
				else
				{
					//(item.FindControl("SmallPanel") as Panel).Visible = true;
					(item.FindControl("BigPanel") as Panel).Style["display"] = "none";
				}
			}

			//Display buttons in the big panel for the current category
			foreach (RepeaterItem item in ElementPanel.Items)
			{
				if((item.FindControl("ElementName") as HiddenField).Value == ElementEditing.Value)
				{
					foreach(RepeaterItem btnItem in (item.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if((btnItem.FindControl("BtnCat") as HiddenField).Value == CategoryEditing.Value)
						{
							(btnItem.FindControl("RepeatButton") as Button).Style["display"] = "";
						}
						else
						{
							(btnItem.FindControl("RepeatButton") as Button).Style["display"] = "none";
						}
					}
					break;
				}
			}
		}
		private void ClearButtonCategory()
		{
			ElementEditing.Value = "";
			CategoryEditing.Value = "";
			SetButtonCategory();
		}
		
		protected void ClarityRadioChanged(object sender, EventArgs e)
		{
			if(ElementEditing.Value == "Clarity" || ElementEditing.Value == "SS Clarity")
			{
				CategoryEditing.Value = ClarityRadio.SelectedValue;
				SetButtonCategory();
			}
		}

		private void ShowImages(string path, string option)
		{
			string imgPath;
			string errMsg;
			Panel imgPanel = null;
			if (option == "Shape")
			{
				//string pngPath;				
				if (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\")
				{
					path = path.Substring(1);
				}
				var pngStream = new MemoryStream();
				//alex
				var fileType = "";
				var ms = new MemoryStream();
				path = path.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				//var result = Utlities.GetShapeImageUrlAzure(path, out imgPath, out errMsg, this, /*out pngPath,*/ out pngStream);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PictureShape")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;//alex "data:image/png;base64," + Convert.ToBase64String(pngStream.ToArray(), 0, pngStream.ToArray().Length);	
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "1";
				}
				else
				{
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "none";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "0";
				}
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Shape");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}
			else if (option == "Picture")
			{
				var ms = new MemoryStream();
				var fileType = "";
				//var result = Utlities.GetPictureImageUrl(path, out imgPath, out errMsg, this);
				//GetPictureImageUrl(string dbPicture, out MemoryStream ms, out string errMsg, out string fileType, Page p)
				var result = Utlities.GetPictureImageUrl(path, out ms, out errMsg, out fileType, this);
				foreach (RepeaterItem elem in ElementPanel.Items)
				{
					if ((elem.FindControl("ElementType") as HiddenField).Value == "PicturePicture")
					{
						imgPanel = elem.FindControl("Pic") as Panel;
						break;
					}
				}
				if (result)
				{
					var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
					(imgPanel.FindControl("PicImg") as Image).ImageUrl = ImageUrl;
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "1";
				}
				else
				{
					//(imgPanel.FindControl("PicImg") as Image).Style["display"] = "none";
					(imgPanel.FindControl("PicImg") as Image).Style["opacity"] = "0";
				}
				//-- Path to picture
				(imgPanel.FindControl("PicLbl") as Label).Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, "Picture");
				(imgPanel.FindControl("PicErrLbl") as Label).Text = errMsg;
			}

			ShowHideImages(true);
		}
        private void ShowImagesNew(string path, string option)
        {
            //
            //<asp:Image runat="server" ID="PicImg" Width="180px"></asp:Image>
            //<asp:Image runat="server" class="image_gdlight" ID="Image" Width="180px"></asp:Image>
            Panel imgPanel = null;
            var imgPanelName = "Picture" + option;
            var imgUrl = Utlities.GetPictureImageUrl(path, this);

            foreach (RepeaterItem elem in ElementPanel.Items)
            {
                if ((elem.FindControl("ElementType") as HiddenField).Value == imgPanelName)
                {
                    imgPanel = elem.FindControl("Pic") as Panel;
                    break;
                }
            }
			if (imgPanel != null)
			{
				imgPanel.Visible = true;
				var imgControl = imgPanel.FindControl("Image") as Image;
				var imgLabel = imgPanel.FindControl("PicLbl") as Label;
				if (option == "Shape")
				{
					imgUrl = imgUrl.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
					path = (path.Substring(0, 1) == "/" || path.Substring(0, 1) == "\\") ? path.Substring(1) : path;
					path = path.Replace(@".wmf", @".png").Replace("shapes", "shapespng");
				}
				else if (option == "Picture")
                {
					imgPanel.Visible = false;
					PictureImage.ImageUrl = imgUrl;
					PictureImage.AlternateText = "NoImage";
					PictureImage.Style["opacity"] = "1";
				}
				imgControl.ImageUrl = imgUrl;
				imgControl.Style["opacity"] = "1";
				imgControl.AlternateText = "NoImage";
				imgLabel.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", path, option);
			}
			else
				imgPanel.Visible = false;
            ShowHideImages(true);
        }
        private void ShowHideImages(bool show)
		{
			foreach(RepeaterItem elem in ElementPanel.Items)
			{
				if (show)
				{
                    //if((elem.FindControl("PicImg") as Image).ImageUrl != "")
                    if ((elem.FindControl("Image") as Image).ImageUrl != "")
                    {
						//(elem.FindControl("Pic") as Panel).Style["display"] = "";
						(elem.FindControl("Pic") as Panel).Style["opacity"] = "1";
					}
					
				}
				else
				{
					//(elem.FindControl("Pic") as Panel).Style["display"] = "none";
					(elem.FindControl("Pic") as Panel).Style["opacity"] = "0";
				}
			}
		}
		#endregion

		#region IntExtComments
		protected void OnIntExtCommentChanged(object sender, EventArgs e)
		{
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var commentBox = sender as TextBox;
			int measureId = 0;

			if(commentBox.ID == "InternalComments")
			{
				measureId = 26;
			}
			else if(commentBox.ID == "ExternalComments")
			{
				measureId = 9;
			}

			if(measureId != 0)
			{
				var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == measureId);
				if(itemValue != null)
				{
					itemValue.Value = commentBox.Text;
				}
			}
		}
		#endregion

		#region Laser Inscription
		protected void OnLaserInscriptionChanged(object sender, EventArgs e)
		{
			var liscBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 67);
			if(itemValue != null)
			{
				itemValue.Value = liscBox.Text;
			}
		}
		#endregion

		#region Prefix
		protected void OnPrefixChanged(object sender, EventArgs e)
		{
			var prefBox = sender as TextBox;
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 112);
			if(itemValue != null)
			{
				itemValue.Value = prefBox.Text;
			}
		}
        #endregion
        public void DownloadExcelFile(String filename, Page p)
        {

            //p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
            //string redirect = "<script>location.assign(window.location);</script>";
            //p.Response.Write(redirect);
            p.Response.Clear();
            //p.Response.Buffer = true; //alex
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.OutputStream.Flush();//alex
            //p.Response.OutputStream.Close();//alex
            //p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.Flush(); //alex
            //p.Response.Redirect("ScreeningFrontEntries.aspx");
            try
            {
                //SyntheticScreeningModel data = new SyntheticScreeningModel();
                //data.ScreeningID = 123;
                //data.SKUName = txtSku.Text;
                //data.CustomerName = txtCustomerName.Text;
                //Session["ScreeningData"] = data;
                // Context.Items.Add("abc", "xyz");
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("ScreeningFrontEntries.aspx", true);
                //Response.Redirect(Request.RawUrl);
                //Context.Items.Add(txtGSIOrder, "abc");
                //Server.Transfer("ScreeningFrontEntries.aspx", false);
                //string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
                //Response.Redirect(navigate);
                //p.Response.End();
            }
            catch (Exception ex)
            {
                // string abc = ex.Message;
                // txtGSIOrder.Text = "abc";

            }
            finally
            {
                p.Response.End();
            }
        }
        protected void OnShapeEditClick(object sender, EventArgs e)
        {
            /*
            
            byte[] Content = File.ReadAllBytes(@"c:\temp\LoadOrder_71.txt"); //missing ;
            Response.ContentType = "text /csv";
            Response.AddHeader("content-disposition", "attachment; filename=abc.csv");
            Response.BufferOutput = true;
            Response.OutputStream.Write(Content, 0, Content.Length);
            Response.Redirect("Clarity2.aspx");
            try
            {
                Response.End();
            }
            catch (Exception ex)
            {
                string msg1 = ex.Message;

            }
            */
            /*
            string filename = @"label_Front_Take_In_abc.xml";
            var stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", "abc");
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            DownloadExcelFile(filename, this);
           


            MemoryStream mStream = new MemoryStream();
            TextWriter textWriter = new StreamWriter(mStream);
            textWriter.WriteLine("Something");
            byte[] bytesInStream = new byte[mStream.Length];
            mStream.Write(bytesInStream, 0, bytesInStream.Length);
            mStream.Close();
            Response.Clear();
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition",
                               "attachment; filename=name_you_file.xls");
            Response.BinaryWrite(bytesInStream);
            Response.End();
             */
            PlottingLabel.Text = "";
            var part = GetPartSelected();
            if (part == null) return;
            var currPart = PartTree.SelectedValue;
            //var part = currPart;
            var selItem = GetItemFromFullList(lstItemList.SelectedValue);
            if (selItem == null) return;
            var shapePath = GetShapePath();
            if (string.IsNullOrEmpty(shapePath))
            {
                PlottingLabel.Text = "ShapePath2Drawing is empty";
                return;
            }
            //var msg = PlottingUtils.CallPlotting(this, GetShapePath(), selItem.FullItemNumber, part.PartName);
            var msg = PlottingUtils.CallPlotting(this, GetShapePath(), selItem.FullOldItemNumber, part.PartName);
            //var msg = PlottingUtils.CallPlotting(this, GetShapePath(), selItem.FullItemNumber, part.PartName);
            //string msg = null;
            using (var memoryStream = new MemoryStream())
            using (var writer = new StreamWriter(memoryStream))
            {
                var sShapePath = GetShapePath();
                var curPartName = part.PartName;
                var sItemNumber = selItem.FullItemNumber;
                var sPlotFileName = sItemNumber + "." + curPartName;
                var tmpDir = this.Session[SessionConstants.GlobalShapeRoot].ToString();
                //var sShapeDir = p.MapPath(p.Session[SessionConstants.GlobalShapeRoot].ToString());
                var sShapeDir = this.MapPath(tmpDir);
                //var sShapeDir = p.Session[SessionConstants.GlobalShapeRoot].ToString();

                //-- Path to plotting dir    
                var sPathToPlotting = ("" + this.Session[SessionConstants.GlobalPlotDir]);
                var arguments = "\"" + sShapePath + "\", " + "\"" + sPlotFileName + "\"" + ", " + sPathToPlotting + ", " + sShapeDir;
                writer.Write(arguments);
                 
                this.Response.Clear();
                this.Response.ContentType = "text/plain";
                //this.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
                //this.Response.AppendHeader("Content-Disposition", "attachment; filename=" + sPlotFileName);
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + sPlotFileName);
                this.Response.BinaryWrite(memoryStream.ToArray());
                this.Response.End();
            }
            
            if (!string.IsNullOrEmpty(msg))
                {
                    PlottingLabel.Text = msg;
                }
        }
        
        private MeasurePartModel GetPartSelected()
        {
            var partId = PartTree.SelectedValue;
            if (string.IsNullOrEmpty(partId)) return null;
            var parts = GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ??
                        new List<MeasurePartModel>();
            return parts.Find(m => m.PartId == Convert.ToInt32(partId));
        }
        
        private SingleItemModel GetItemFromFullList(string itemNumber)
        {
            var list = GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel>;
            return list == null ? null : list.Find(m => m.FullOldItemNumber == itemNumber);
        }

        private string GetShapePath()
        {
            var values = GetMeasureValuesByItem();
            if (values == null) return "";
            var withShapes = values.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0);
            if (withShapes.Count > 0) return withShapes[0].ShapePath2Drawing;
            return "";
        }

        private List<MeasureValueModel> GetMeasureValuesByItem()
        {
            var itemModel = GetItemFromFullList(lstItemList.SelectedValue);
            var values = GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ??
                         new List<MeasureValueModel>();
            if (values.FindAll(m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode).Count == 0)
            {
                values.AddRange(QueryUtils.GetMeasureValues(itemModel, "4", this));
                SetViewState(values, SessionConstants.GradeMeasureValues);
            }
            return
                values.FindAll(
                    m => m.ForItem.NewBatchId == itemModel.NewBatchId && m.ForItem.NewItemCode == itemModel.NewItemCode);
        }
        /*
		#region Shapes
		protected void OnShapesChanged(object sender, EventArgs e)
		{
			ShapesTextBox.Text = ShapesList.SelectedItem.Text;
		}

		protected void OnShapesButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ShapesButton") as Button;
			var val = e.Item.FindControl("ShapesButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ShapesButtonRedirect") as HiddenField;

			if(val.Value != "0")
			{
				//Input selected value into textbox
				ShapesList.SelectedValue = val.Value;
				OnShapesChanged(null, null);
			}

			if(redir.Value != "0")
			{
				//Redirect to new button menu
				ShapesCategoryEditing.Value = redir.Value;
				InitShapesButtons();
			}
		}

		private void InitShapesButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ShapesList.Items;

			if (ShapesCategoryEditing.Value == "Root")
			{
				//List of Categories
				btnCategories.Add(new ButtonDetailUtils("Generic Type 1", "Generic Type 1"));
				btnCategories.Add(new ButtonDetailUtils("Generic Type 2", "Generic Type 2"));
				btnCategories.Add(new ButtonDetailUtils("Signet Branded", "Signet Branded"));
				btnCategories.Add(new ButtonDetailUtils("Non Signet", "Non Signet"));
				btnCategories.Add(new ButtonDetailUtils("Shapes For Plot", "Shapes For Plot"));
				btnCategories.Add(new ButtonDetailUtils("Color Stones", "Color Stones"));
				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if(ShapesCategoryEditing.Value == "Generic Type 1")
			{
				//List of Shape Names
				names.Add("Emerald");
				names.Add("Heart Brill");
				names.Add("Marquise Brill");
				names.Add("Oval Brill");
				names.Add("Pear Brill");
				names.Add("Princess Regular");
				names.Add("Radiant");
				names.Add("Rectangular Brilliant");
				names.Add("Round Brill Default");
				names.Add("Sq. Emerald");
				names.Add("Triangular Brilliant");
				names.Add("Triangular Step Cut");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if(ShapesCategoryEditing.Value == "Generic Type 2")
			{
				//List of Shape Names
				names.Add("Half Moon");
				names.Add("Kite");
				names.Add("Mod. Heart Brilliant");
				names.Add("Mod. Rectangular Brilliant");
				names.Add("Modified Oval Brill");
				names.Add("Octagonal Mod. Brill");
				names.Add("Old European");
				names.Add("Old Mine");
				names.Add("Pear Mod. Brilliant");
				names.Add("Radiant Square");
				names.Add("Rectangular Modified Brilliant");
				names.Add("Round 108 Facets");
				names.Add("Round Mod. Brilliant");
				names.Add("Shield");
				names.Add("Single");
				names.Add("Sq. Cushion Mod.Brill");
				names.Add("Square Step");
				names.Add("Transitional");
				names.Add("Trilliant");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Signet Branded")
			{
				//List of Shape Names
				names.Add("Celebration Cushion");
				names.Add("Celebration Princess");
				names.Add("Celebration Radiant");
				names.Add("Celebration Round");
				names.Add("Leo Cushion");
				names.Add("Leo Emerald");
				names.Add("Leo Marq_6_old");
				names.Add("Leo Marquise");
				names.Add("Leo PR");
				names.Add("Leo PR Rect");
				names.Add("Leo PR/Leo RBC");
				names.Add("Leo Princess");
				names.Add("Leo Radiant");
				names.Add("Leo RBC");
				names.Add("Leo Round");
				names.Add("preTolk_PR");
				names.Add("preTolk_RBC");
				names.Add("Princess Sterling");
				names.Add("TOLKOWSKY IDEAL CUT-PR");
				names.Add("TOLKOWSKY IDEAL CUT-RBC");
				names.Add("Tolkowsky Legacy Princess");
				names.Add("Tolkowsky Legacy Round");
				names.Add("Zales Princess");
				names.Add("Zales Princess Rect");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Non Signet")
			{
				//List of Shape Names
				names.Add("Preferred105");
				names.Add("Radiant Star Cush");
				names.Add("Radiant Star Oval");
				names.Add("Radiant Star PR");
				names.Add("Radiant Star RBC");
				names.Add("Shane Cushion");
				names.Add("Shane PR");
				names.Add("Shane Princess");
				names.Add("Sitara Cushion");
				names.Add("Sitara Princess");
				names.Add("Sitara Round");
				names.Add("Sitara Round(RMB)");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Shapes For Plot")
			{
				//List of Shape Names
				
				//This is likely a mistake...
				//names.Add("Preferred105");
				//names.Add("Radiant Star Cush");
				//names.Add("Radiant Star Oval");
				//names.Add("Radiant Star PR");
				//names.Add("Radiant Star RBC");
				//names.Add("Shane Cushion");
				//names.Add("Shane PR");
				//names.Add("Shane Princess");
				//names.Add("Sitara Cushion");
				//names.Add("Sitara Princess");
				//names.Add("Sitara Round");
				//names.Add("Sitara Round(RMB)");

				//foreach (var name in names)
				//{
				//	btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				//}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Color Stones")
			{
				//List of Shape Names
				names.Add("Heart Mixed");
				names.Add("Marquise Mixed");
				names.Add("Mod.Trapezoid/Triangle Mix");
				names.Add("Octagon Mix");
				names.Add("Oval Cab");
				names.Add("Oval Cabochon");
				names.Add("Oval Double Cabochon");
				names.Add("Oval Mixed");
				names.Add("Pear Mixed");
				names.Add("Rectangular Mix");
				names.Add("Round Cabochon");
				names.Add("Round Mixed");
				names.Add("Square Mixed");
				names.Add("Square Mixed Cut");
				names.Add("Trapezoid Mixed");
				names.Add("Triangular Cabochon");

				foreach (var name in names)
				{
					btnCategories.Add(new ButtonDetailUtils(name, "Collapsed", int.Parse(enums.FindByText(name).Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Back to Categories", "Root"));
			}
			else if (ShapesCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Shape", "Root"));
			}

			ShapesButtonList.DataSource = btnCategories;
			ShapesButtonList.DataBind();
		}
		#endregion

		#region Polish
		protected void OnPolishChanged(object sender, EventArgs e)
		{
			PolishTextBox.Text = PolishList.SelectedItem.Text;
		}

		protected void OnPolishButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("PolishButton") as Button;
			var val = e.Item.FindControl("PolishButtonValue") as HiddenField;
			var redir = e.Item.FindControl("PolishButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				PolishList.SelectedValue = val.Value;
				OnPolishChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				PolishCategoryEditing.Value = redir.Value;
				InitPolishButtons();
			}
		}

		private void InitPolishButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = PolishList.Items;

			if (PolishCategoryEditing.Value == "Root")
			{
				for(int c = 1; c < PolishList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(PolishList.Items[c].Text, "Collapsed", int.Parse(PolishList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (PolishCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Polish", "Root"));
			}

			PolishButtonList.DataSource = btnCategories;
			PolishButtonList.DataBind();
		}
		#endregion

		#region Symmetry
		protected void OnSymmetryChanged(object sender, EventArgs e)
		{
			SymmetryTextBox.Text = SymmetryList.SelectedItem.Text;
		}

		protected void OnSymmetryButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("SymmetryButton") as Button;
			var val = e.Item.FindControl("SymmetryButtonValue") as HiddenField;
			var redir = e.Item.FindControl("SymmetryButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				SymmetryList.SelectedValue = val.Value;
				OnSymmetryChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				SymmetryCategoryEditing.Value = redir.Value;
				InitSymmetryButtons();
			}
		}

		private void InitSymmetryButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = SymmetryList.Items;

			if (SymmetryCategoryEditing.Value == "Root")
			{
				for (int c = 1; c < SymmetryList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(SymmetryList.Items[c].Text, "Collapsed", int.Parse(SymmetryList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (SymmetryCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Symmetry", "Root"));
			}

			SymmetryButtonList.DataSource = btnCategories;
			SymmetryButtonList.DataBind();
		}
		#endregion

		#region Reflection
		protected void OnReflectionChanged(object sender, EventArgs e)
		{
			ReflectionTextBox.Text = ReflectionList.SelectedItem.Text;
		}

		protected void OnReflectionButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ReflectionButton") as Button;
			var val = e.Item.FindControl("ReflectionButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ReflectionButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				ReflectionList.SelectedValue = val.Value;
				OnReflectionChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				ReflectionCategoryEditing.Value = redir.Value;
				InitReflectionButtons();
			}
		}

		private void InitReflectionButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ReflectionList.Items;

			if (ReflectionCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(ReflectionList.Items[0].Value)));
				btnCategories.Add(new ButtonDetailUtils(ReflectionList.Items[1].Text, "Collapsed", int.Parse(ReflectionList.Items[1].Value)));
				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (ReflectionCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Reflection", "Root"));
			}

			ReflectionButtonList.DataSource = btnCategories;
			ReflectionButtonList.DataBind();
		}
		#endregion

		#region Clarity
		protected void OnClarityChanged(object sender, EventArgs e)
		{
			ClarityTextBox.Text = ClarityList.SelectedItem.Text;
		}

		protected void OnClarityButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("ClarityButton") as Button;
			var val = e.Item.FindControl("ClarityButtonValue") as HiddenField;
			var redir = e.Item.FindControl("ClarityButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				ClarityList.SelectedValue = val.Value;
				OnClarityChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				ClarityCategoryEditing.Value = redir.Value;
				InitClarityButtons();
			}
		}

		private void InitClarityButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = ClarityList.Items;

			if (ClarityCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(ClarityList.Items[0].Value)));

				for (int c = 1; c < ClarityList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(ClarityList.Items[c].Text, "Collapsed", int.Parse(ClarityList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (ClarityCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Clarity", "Root"));
			}

			ClarityButtonList.DataSource = btnCategories;
			ClarityButtonList.DataBind();
		}
		#endregion

		#region Inclusion
		protected void OnInclusionChanged(object sender, EventArgs e)
		{
			InclusionTextBox.Text = InclusionList.SelectedItem.Text;
		}

		protected void OnInclusionButtonListClick(object sender, RepeaterCommandEventArgs e)
		{
			var btn = e.Item.FindControl("InclusionButton") as Button;
			var val = e.Item.FindControl("InclusionButtonValue") as HiddenField;
			var redir = e.Item.FindControl("InclusionButtonRedirect") as HiddenField;

			if (val.Value != "0")
			{
				//Input selected value into textbox
				InclusionList.SelectedValue = val.Value;
				OnInclusionChanged(null, null);
			}

			if (redir.Value != "0")
			{
				//Redirect to new button menu
				InclusionCategoryEditing.Value = redir.Value;
				InitInclusionButtons();
			}
		}

		private void InitInclusionButtons()
		{
			List<String> names = new List<String>();
			List<ButtonDetailUtils> btnCategories = new List<ButtonDetailUtils>();
			var enums = InclusionList.Items;

			if (InclusionCategoryEditing.Value == "Root")
			{
				btnCategories.Add(new ButtonDetailUtils("None", "Collapsed", int.Parse(InclusionList.Items[0].Value)));

				for (int c = 1; c < InclusionList.Items.Count; c++)
				{
					btnCategories.Add(new ButtonDetailUtils(InclusionList.Items[c].Text, "Collapsed", int.Parse(InclusionList.Items[c].Value)));
				}

				btnCategories.Add(new ButtonDetailUtils("Collapse", "Collapsed"));
			}
			else if (InclusionCategoryEditing.Value == "Collapsed")
			{
				btnCategories.Add(new ButtonDetailUtils("Select Inclusion", "Root"));
			}

			InclusionButtonList.DataSource = btnCategories;
			InclusionButtonList.DataBind();
		}
		#endregion
		*/

        /*
		#region Shape Panel
		private void ShowShape(string dbShape)
		{
			string imgPath;
			string errMsg;
			string pngPath;
			ShapePanel.Visible = true;
			if (dbShape.Substring(0, 1) == "/" || dbShape.Substring(0, 1) == "\\")
			{
				dbShape = dbShape.Substring(1);
			}
			var result = Utlities.GetShapeImageUrl(dbShape, out imgPath, out errMsg, this, out pngPath);
			if (result)
			{
				itemShape.ImageUrl = pngPath;// imgPath;
				itemShape.Visible = true;
			}
			else
			{
				itemShape.Visible = false;
			}
			//-- Path to picture
			ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", imgPath, "Shape");
			ErrShapeField.Text = errMsg;

		}
		private void HideShapePanel()
		{
			ShapePanel.Visible = false;
		}
		#endregion

		#region Picture Panel
		private void ShowPicture(string dbPicture)
		{
			string imgPath;
			string errMsg;
			var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
			if (result)
			{
				itemPicture.ImageUrl = imgPath;
				itemPicture.Visible = true;
			}
			else
			{
				itemPicture.Visible = false;
			}
			//-- Path to picture
			PicturePathAbbr.Text = ShapePathAbbr.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", imgPath, "Picture");
			ErrPictureField.Text = errMsg;

		}
		private void HidePicturePanel()
		{
			itemPicture.Visible = false;
			PicturePathAbbr.Text = "";
			ErrPictureField.Text = "";
		}
		#endregion
		*/

        #region Data
            /* alex
        private void LoadMeasuresForEdit(string itemNumber)
		{
			var itemModel = GetItemModelFromView(itemNumber);
			var enumsMeasure = GetMeasureEnumsFromView();
			var parts = GetPartsFromView();
			var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
           
            var currPart = PartTree.SelectedValue;
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId == "8");
			if (withShapes.Count > 0)
			{
				var path = withShapes[0].ShapePath2Drawing;
				if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImages(path, "Shape");
				else ShowHideImages(false);
			}
			else
			{
				ShowHideImages(false);
			}
			
			var measures = GetMeasuresCpFromView();
			var result = new List<ItemValueEditModel>();
			foreach (var measure in measures)
			{
				if (measure.MeasureClass > 3) continue;
				var partModel = parts.Find(m => m.PartId == measure.PartId);
				if (partModel == null) continue;
				var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
				var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
						? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
				result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
			}
			result.Sort(new ItemValueEditComparer().Compare);
			SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);

		}//LoadMeasuresForEdit
        */
    private void LoadMeasuresForEdit(string itemNumber)
	{
		var itemModel = GetItemModelFromView(itemNumber);
		var enumsMeasure = GetMeasureEnumsFromView();
		var parts = GetPartsFromView();
		string itemCode = itemNumber.Substring(itemNumber.Length - 2);
		//alex var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
		//List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
		List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
		List<ExpressGradingModel> defaultMeasures = GetViewState(SessionConstants.ExpressGradingMeasures) as List<ExpressGradingModel> ?? new List<ExpressGradingModel>();
		if (defaultMeasures.Count == 0)
		{
			defaultMeasures = QueryUtils.GetSkuRules(Convert.ToInt64(itemNumber), this);
			SetViewState(defaultMeasures, SessionConstants.ExpressGradingMeasures);
		}
		if (measureValuesBatch.Count == 0)
		{
			measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
			SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
		}
		List<MeasureValueModel> measureValues = measureValuesBatch.FindAll(m => m.ItemCode.TrimStart('0') == itemCode.TrimStart('0') && m.BatchId == itemModel.BatchId).ToList();

        //    foreach (var measure in measureValuesBatch)
        //if (Utils.FillToTwoChars(measure.ItemCode) == itemCode && itemModel.BatchId == measure.BatchId)
        //    measureValues.Add(measure);

    //measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
    /* TODO replace this code with something that works with our Element objects - done */
    var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0);
    if (withShapes.Count > 0)
    {
        var path = withShapes[0].ShapePath2Drawing;
        if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
        else ShowHideImages(false);
    }
    else
    {
        ShowHideImages(false);
    }
    // */
    var measures = GetMeasuresCpFromView();
    var result = new List<ItemValueEditModel>();
    foreach (var measure in measures)
    {
        if (measure.MeasureClass > 3) continue;
        var partModel = parts.Find(m => m.PartId == measure.PartId);
        if (partModel == null) continue;
        var valueModel = measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
        var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                ? enumsMeasure.FindAll(m => "" + m.MeasureValueMeasureId == measure.MeasureId) : null);
        result.Add(new ItemValueEditModel(partModel, measure, valueModel, enums, itemModel, false));
    }
    result.Sort(new ItemValueEditComparer().Compare);
    SetViewState(result, SessionConstants.ShortReportExtMeasuresForEdit);

}
private void LoadShapeForEdit(string itemNumber)
        {
			itemNumber = itemNumber.TrimStart('*');

            var itemModel = GetItemModelFromView(itemNumber);
            var enumsMeasure = GetMeasureEnumsFromView();
            var parts = GetPartsFromView();
            
            string itemCode = itemNumber.Substring(itemNumber.Length - 2);
            //List<MeasureValueModel> measureValues = new List<MeasureValueModel>();
            List<MeasureValueModel> measureValuesBatch = GetViewState(SessionConstants.MeasureValueBatch) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
            if (measureValuesBatch.Count == 0)
            {
                measureValuesBatch = QueryUtils.GetMeasureValuesBatch(itemModel, "", this);
                SetViewState(measureValuesBatch, SessionConstants.MeasureValueBatch);
            }
            List<MeasureValueModel> measureValues = measureValuesBatch.FindAll(m => m.ItemCode.TrimStart('0') == itemCode.TrimStart('0') && m.BatchId == itemModel.BatchId).ToList();

            //foreach (var measure in measureValuesBatch)
            //    if (Utils.FillToTwoChars(measure.ItemCode) == itemCode)
            //        measureValues.Add(measure);
                    
            //var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            /* TODO replace this code with something that works with our Element objects - done */
            var currPart = PartTree.SelectedValue;
            var withShapes = measureValues.FindAll(m => m.ShapePath2Drawing != null && m.ShapePath2Drawing.Length > 0 && m.PartId == Convert.ToInt32(currPart) && m.MeasureId=="8");
            if (withShapes.Count > 0)
            {
                var path = withShapes[0].ShapePath2Drawing;
                if (path.IndexOf(".wmf", StringComparison.Ordinal) != -1) ShowImagesNew(path, "Shape");
                else ShowHideImages(false);
            }
            else
            {
                ShowHideImages(false);
            }
        }

        private void LoadDataForEditing()
		{
			var itemModel = GetSelectedItemModel();
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			var enumValues = GetMeasureEnumsFromView();
            string matchedItemNumber = null, tmpBatchNumber = null;
            if (Session["MatchItemNumber"] != null)
            {
                matchedItemNumber = (string)Session["MatchItemNumber"];
                Session["MatchItemNumber"] = null;
            }
            if (Session["TMPNUMBER"] != null)
            {
                tmpBatchNumber = (string)Session["TMPNUMBER"];
                Session["TMPNUMBER"] = null;
            }
            //Test
            //var test = GetMeasuresForEditFromView("").FindAll(m => m.MeasureId == 25);//EXISTS!

            //New c
            foreach (RepeaterItem element in ElementPanel.Items)
			{
				var smallTxt = element.FindControl("SmTxt") as TextBox;
				var bigTxt = element.FindControl("BigTxt") as TextBox;
				smallTxt.Text = "";
				bigTxt.Text = "";

				var dropDownList = element.FindControl("DropDownListBox") as ListBox;
				dropDownList.SelectedIndex = -1;

				//Populate dropdownlist and text boxes if appropriate
				var dropDownMeasure = element.FindControl("ElementMeasure") as HiddenField;
				var dropDownValue = itemValues.Find(m => m.MeasureId.ToString() == dropDownMeasure.Value);
				if(dropDownValue != null)
				{
					var dropDownEnumValue = enumValues.Find(m => m.MeasureValueMeasureId.ToString() == dropDownMeasure.Value 
						&& m.MeasureValueId.ToString() == dropDownValue.Value);
					if(dropDownEnumValue != null)
					{
						/*IvanB start*/
						//after select of Parts Tree
						//if we loading shapes data
						if (dropDownEnumValue.MeasureValueMeasureId == 8)
						{
							//get selected item from dropDownList.Items
							var selItem = dropDownList.Items.FindByValue(dropDownValue.Value);
							//if its not there
							if (selItem == null)
							{
								//we insert selected item to dropDownList
								dropDownList.Items.Insert(0, new ListItem(dropDownEnumValue.ValueTitle, dropDownEnumValue.MeasureValueId.ToString()));	
							}
						}
						/*IvanB end*/
						smallTxt.Text = dropDownEnumValue.ValueTitle.ToString();
						bigTxt.Text = smallTxt.Text;
						if (smallTxt.Text.ToLower() == "natural")
							smallTxt.BackColor = System.Drawing.Color.Yellow;
						else if (smallTxt.Text.ToLower() == "synthetic")
							smallTxt.BackColor = System.Drawing.Color.Red;
						else if (smallTxt.Text.ToLower() == "li" || smallTxt.Text.ToLower() == "li missing" || smallTxt.Text.ToLower() == "li pre-existing")
							smallTxt.BackColor = System.Drawing.Color.Pink;
						dropDownList.SelectedValue = dropDownValue.Value;
					}
				}
				else//Disable button if ElementMeasure is not empty
				{
					var smallButton = element.FindControl("SmBtn") as Button;
					if (smallButton.Text == "DiamondQuality")
					{
						foreach (RepeaterItem bigBtn in (element.FindControl("ButtonRepeater") as Repeater).Items)
						{
							var commentMeasure = bigBtn.FindControl("BtnMeasure") as HiddenField;
							var commentValue = itemValues.Find(m => m.MeasureId.ToString() == commentMeasure.Value);
							var bigBtnBtn = bigBtn.FindControl("RepeatButton") as Button;
							SetButtonState(bigBtnBtn, "");
							(bigBtnBtn.FindControl("BtnActive") as HiddenField).Value = "1";
						}
					}
					else if (smallButton.Text == "Comments")//Disable buttons in comments
					{
						bool allDisabled = true;
						foreach (RepeaterItem bigBtn in (element.FindControl("ButtonRepeater") as Repeater).Items)
						{
							var commentMeasure = bigBtn.FindControl("BtnMeasure") as HiddenField;
							var commentValue = itemValues.Find(m => m.MeasureId.ToString() == commentMeasure.Value);
							var bigBtnBtn = bigBtn.FindControl("RepeatButton") as Button;
							if(commentValue == null && commentMeasure.Value != "0" && commentMeasure.Value != "")
							{
								SetButtonState(bigBtnBtn, "Disable");
							}
							else
							{
								SetButtonState(bigBtnBtn, "");

								if(commentMeasure.Value != "0" && commentMeasure.Value != "")
								{
									allDisabled = false;
								}
							}
						}
						if (allDisabled)
						{
							SetButtonState(smallButton, "Disabled");
						}
					}
					else
					{
						SetButtonState(smallButton, "Disable");
					}
				}
			}

			PopulateComments();

			//Populate Prefix
			PrefixText.Text = "";
			var prefixValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 112);
			if(prefixValue != null && prefixValue.Value != null)
			{
				PrefixText.Text = prefixValue.Value;
			}

			//Populate Weight
			WeightValue.Text = "";
			if(PartTree.SelectedNode.Text.ToLower().Contains("stone set"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 2);
				if(weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Total Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
			}
			else if (PartTree.SelectedNode.Text.ToLower().Contains("diamond"))
			{
				var weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 4);
				if (weightValue != null && weightValue.Value != "")
				{
					WeightTxt.Text = "Measured Weight (ct):";
					WeightValue.Text = weightValue.Value;
				}
				else
				{
					weightValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 5);
					if (weightValue != null && weightValue.Value != "")
					{
						WeightTxt.Text = "Calculated Weight (ct):";
						WeightValue.Text = weightValue.Value;
					}
				}
			}

			//Populate broken stones
			BSValue.Text = "";
			BSLabel.Font.Size = 10;
			var bsValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 208);
			if (bsValue != null && bsValue.Value != null)
			{
                if (bsValue.Value.Contains("."))
                    bsValue.Value = bsValue.Value.Substring(0, bsValue.Value.IndexOf("."));
                BSValue.Text = bsValue.Value;
			}
			DSFailValue.Text = "";
			DSFailLbl.Font.Size = 10;
			var dsFailValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 206);
			if (dsFailValue != null && dsFailValue.Value != null)
			{
                if (dsFailValue.Value.Contains("."))
                    dsFailValue.Value = TruncateToDot(dsFailValue.Value);
                DSFailValue.Text = dsFailValue.Value;
			}
			DSPassValue.Text = "";
			DSPassLbl.Font.Size = 10;
			var dsPassValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 205);
			if (dsPassValue != null && dsPassValue.Value != null)
			{
                if (dsPassValue.Value.Contains("."))
                    dsPassValue.Value = TruncateToDot(dsPassValue.Value);
                DSPassValue.Text = dsPassValue.Value;
			}
			FFStonesValue.Text = "";
			FFStonesLbl.Font.Size = 10;
			var ffStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 270);
			if (ffStonesValue != null && ffStonesValue.Value != null)
			{
                if (ffStonesValue.Value.Contains("."))
                    ffStonesValue.Value = TruncateToDot(ffStonesValue.Value);
                FFStonesValue.Text = ffStonesValue.Value;
			}
			LabGrownValue.Text = "";
			LabGrownLbl.Font.Size = 10;
			var labGrownValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 212);
			if (labGrownValue != null && labGrownValue.Value != null)
			{
                if (labGrownValue.Value.Contains("."))
                    labGrownValue.Value = TruncateToDot(labGrownValue.Value);
                LabGrownValue.Text = labGrownValue.Value;
			}
			LDStonesValue.Text = "";
			LDStonesLbl.Font.Size = 10;
			var ldStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 269);
			if (ldStonesValue != null && ldStonesValue.Value != null)
			{
                if (ldStonesValue.Value.Contains("."))
                    ldStonesValue.Value = TruncateToDot(ldStonesValue.Value);
                LDStonesValue.Text = ldStonesValue.Value;
			}
			ILDStonesValue.Text = "";
			ILDStonesLbl.Font.Size = 10;
			var ildStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 271);
			if (ildStonesValue != null && ildStonesValue.Value != null)
			{
                if (ildStonesValue.Value.Contains("."))
                    ildStonesValue.Value = TruncateToDot(ildStonesValue.Value);
                ILDStonesValue.Text = ildStonesValue.Value;
			}
			MissingStonesValue.Text = "";
			MissingStonesLbl.Font.Size = 10;
			var missingStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 209);
			if (missingStonesValue != null && missingStonesValue.Value != null)
			{
                if (missingStonesValue.Value.Contains("."))
                    missingStonesValue.Value = TruncateToDot(missingStonesValue.Value);
                MissingStonesValue.Text = missingStonesValue.Value;
			}
			SimStonesValue.Text = "";
			SimStonesLbl.Font.Size = 10;
			var simStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 213);
			if (simStonesValue != null && simStonesValue.Value != null)
			{
                if (simStonesValue.Value.Contains("."))
                    simStonesValue.Value = TruncateToDot(simStonesValue.Value);
                SimStonesValue.Text = simStonesValue.Value;
			}
			SynthStonesValue.Text = "";
			SynthStonesLbl.Font.Size = 10;
			var synthStonesValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 211);
			if (synthStonesValue != null && synthStonesValue.Value != null)
			{
                if (synthStonesValue.Value.Contains("."))
                    synthStonesValue.Value = TruncateToDot(synthStonesValue.Value);
                SynthStonesValue.Text = synthStonesValue.Value;
			}
			UnitsValue.Text = "";
			UnitsLbl.Font.Size = 10;
			var unitsValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 65);
			if (unitsValue != null && unitsValue.Value != null)
			{
                if (unitsValue.Value.Contains("."))
                    unitsValue.Value = TruncateToDot(unitsValue.Value);
                UnitsValue.Text = unitsValue.Value;
			}
			//alex 04162024
			TextLIPresentNoneBox.Text = "";
			TextLIPresentNoneBox.BackColor = System.Drawing.Color.White;
			TextLIPresentPreExistingBox.Text = "";
			TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.White;
			TextLIPresentLIBox.Text = "";
			TextLIPresentLIBox.BackColor = System.Drawing.Color.White;
			TextLIPresentMissingBox.Text = "";
			TextLIPresentMissingBox.BackColor = System.Drawing.Color.White;
			var liPresentValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 113);
			if (liPresentValue != null && liPresentValue.Value != "")
			{
				if (liPresentValue.Value == "7192")
				{
					TextLIPresentPreExistingBox.Text = "Pre-Existing";
					TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.Pink;
				}
				else if (liPresentValue.Value == "5644")
				{
					TextLIPresentLIBox.Text = "LI";
					TextLIPresentLIBox.BackColor = System.Drawing.Color.Pink;
				}
				else if (liPresentValue.Value == "9803")
				{
					TextLIPresentMissingBox.Text = "Missing";
					TextLIPresentMissingBox.BackColor = System.Drawing.Color.Pink;
				}
				else if (liPresentValue.Value == "5645")
				{
					TextLIPresentNoneBox.Text = "None";
					TextLIPresentNoneBox.BackColor = System.Drawing.Color.Pink;
				}
			}
			//alex 04162024
			//Populate Sarin Panel
			SarinRatio.Text = "";
			SarinMeasure.Text = "";
			var sarinMaxValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 11);
			var sarinMinValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 12);
			var sarinDepthValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 13);//Depth is wrong, need height! H_x is 13
			if(sarinMaxValue != null && sarinMinValue != null && sarinDepthValue != null)
			{
				//if(sarinMaxValue.Value != null && sarinMinValue.Value != null && sarinDepthValue.Value != null)
                if (!String.IsNullOrEmpty(sarinMaxValue.Value) && !String.IsNullOrEmpty(sarinMinValue.Value) && !String.IsNullOrEmpty(sarinDepthValue.Value))
				{
					double sarinRatio = double.Parse(sarinMaxValue.Value) / double.Parse(sarinMinValue.Value);
					SarinRatio.Text = sarinRatio.ToString("0.0000");
					SarinMeasure.Text = sarinMaxValue.Value + " x " + sarinMinValue.Value + " x " + sarinDepthValue.Value;
				}
			}

			//Populate Int/Ext Comments
			InternalComments.Text = "";
			ExternalComments.Text = "";
			var intComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 26);
			if(intComments != null && intComments.Value != null)
			{
				InternalComments.Text = intComments.Value;
			}
			var extComments = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 9);
			if(extComments != null && extComments.Value != null && !PartTree.SelectedNode.Text.ToLower().Contains("item container"))
			{
				ExternalComments.Text = extComments.Value;
				if (ExternalComments.Text.ToLower().Contains("side stones not examined"))
					DSSCheckBox.Checked = true;
			}

			//Populate Laser Inscribed
			LaserInscription.Text = "";
			var linsc = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 67);
			if(linsc != null && linsc.Value != null)
			{
				LaserInscription.Text = linsc.Value;
			}
			//else
			//{
			//	var liType = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId == 147);
			//	if (liType != null && liType.Value != null && liType.Value != "0")
			//	{
			//		DataTable liDt = QueryUtils.GetLaserInscrFromLiType(itemModel.BatchId, Convert.ToInt16(itemModel.ItemCode), Convert.ToInt16(liType.Value), this);
			//		if (liDt != null && liDt.Rows.Count > 0)
			//		{
			//			LaserInscription.Text = liDt.Rows[0].ItemArray[0].ToString();
			//			InvalidLabel.Text = "Laser Inscription was pre-calculated";
			//		}
			//	}
			//}
			DataTable dt = (DataTable)Session["ItemList"];
            if (dt != null)
            {
                var prevNumber = Utils.getPrevItemCode(dt, itemModel.FullItemNumber);
                if (prevNumber != null)
                    OldNumText.Text = prevNumber;
                else
                    OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            }
            if (matchedItemNumber != null)
                matchedItemNumber = matchedItemNumber.Trim();
            if (tmpBatchNumber != null)
                tmpBatchNumber = tmpBatchNumber.Trim();
            //DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            //OldNumText.Text = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
            if ((itemModel.FullItemNumber == OldNumText.Text) ||
                (itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldNumText.Text))
                DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
            else
            {
                if (matchedItemNumber == null || tmpBatchNumber == null || (String.Equals(matchedItemNumber,tmpBatchNumber)== true))
                    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldNumText.Text + @")";
                else
                    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + tmpBatchNumber + @")";
            }
			if (PartTree.SelectedNode.Text.ToLower().Contains("diamond") || PartTree.SelectedNode.Text.ToLower().Contains("stone"))
			{
				DSSCheckBox.Visible = true;
				DSSCheckBox_CheckedChanged(null, null);
			}
			else
            {
				DSSCheckBox.Visible = false;
            }
			//alex populate default shape from sku
			List<ExpressGradingModel> defaultMeasures = GetViewState(SessionConstants.ExpressGradingMeasures) as  List<ExpressGradingModel>;
			if (defaultMeasures.Count > 0)
			{
				try
				{
					var partShape = defaultMeasures.Find(m => m.MeasureID == 8 && m.PartId.ToString() == PartTree.SelectedValue);
					if (partShape != null)
					{
						string shapeName = defaultMeasures.Find(m => m.MeasureID == 8 && m.PartId.ToString() == PartTree.SelectedValue).MeasureValueSku;
						if (shapeName != null)
						{
							DefaultSkuBox.Text = shapeName;
							DefaultSkuBox.BackColor = System.Drawing.Color.Pink;
							DefaultSkuBox.Font.Bold = true;
							DefaultSkuLbl.Font.Bold = true;
						}
						else
                        {
							DefaultSkuBox.Text = "";
							DefaultSkuBox.BackColor = System.Drawing.Color.Green;
							DefaultSkuLbl.Font.Bold = false;
						}
					}
					else
                    {
						DefaultSkuBox.Text = "";
						DefaultSkuBox.BackColor = System.Drawing.Color.Green;
						DefaultSkuLbl.Font.Bold = false;
					}
				}
				catch
                {
					DefaultSkuBox.Text = "";
					DefaultSkuBox.BackColor = System.Drawing.Color.Green;
					DefaultSkuLbl.Font.Bold = false;

                }
			}
			//if ((itemModel.FullItemNumber == OldTextNumber.Value) ||
			//    (itemModel.FullItemNumber.Substring(0, itemModel.FullItemNumber.Length - 2) == OldTextNumber.Value))
			//    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			//else
			//    DataForLabel.Text = "Data For # " + itemModel.FullItemNumber + @"(" + OldTextNumber.Value + @")";

			//Get Old Number
			//var test = QueryUtils.GetOldItemNumber(itemModel.BatchId.ToString(), itemModel.ItemCode, this);
			ShowHideItemDetails(true);

            //txtExtDescription.Text = QueryUtils.GetExtendedDescription(int.Parse(PartTree.SelectedValue), itemModel.BatchId, int.Parse(itemModel.ItemCode),38, this);
            //Session["OriginalExtDescription"] = txtExtDescription.Text.Trim();

            /*
			cmdSave.Visible = true;
			shortrpt.Visible = true;
			*/

            /* Old code for LoadDataForEditing
			//Load Shapes in ListBox
			var shapeEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 8);
			ShapesList.DataSource = shapeEnums;
			ShapesList.DataBind();

			//Load text of Shapes ListBox and Selected Index of Dropdown
			ShapesTextBox.Text = "";
			ShapesList.SelectedIndex = -1;
			ShapesPanel.Visible = false;
			var shapeValue = itemValues.Find(m => m.MeasureId == 8);
			if (shapeValue != null)
			{
				ShapesPanel.Visible = true;
				var shapeEnum = shapeEnums.Find(m => m.MeasureValueId.ToString() == shapeValue.Value);
				if (shapeEnum != null)
				{
					ShapesTextBox.Text = shapeEnum.ValueTitle.ToString();
					ShapesList.SelectedValue = shapeValue.Value;
				}
			}

			//Load Polish in ListBox
			var polishEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 18);
			PolishList.DataSource = polishEnums;
			PolishList.DataBind();

			//Load text of Polish ListBox and Selected Index of Dropdown
			PolishTextBox.Text = "";
			PolishList.SelectedIndex = -1;
			PolishPanel.Visible = false;
			var polishValue = itemValues.Find(m => m.MeasureId == 18);
			if(polishValue != null)
			{
				PolishPanel.Visible = true;
				var polishEnum = polishEnums.Find(m => m.MeasureValueId.ToString() == polishValue.Value);
				if(polishEnum != null)
				{
					PolishTextBox.Text = polishEnum.ValueTitle.ToString();
					PolishList.SelectedValue = polishValue.Value;
				}
			}

			//Load Symmetry in ListBox
			var symmetryEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 19);
			SymmetryList.DataSource = symmetryEnums;
			SymmetryList.DataBind();

			//Load text of Symmetry ListBox and Selected Index of Dropdown
			SymmetryTextBox.Text = "";
			SymmetryList.SelectedIndex = -1;
			SymmetryPanel.Visible = false;
			var symmetryValue = itemValues.Find(m => m.MeasureId == 19);
			if (symmetryValue != null)
			{
				SymmetryPanel.Visible = true;
				var symmetryEnum = symmetryEnums.Find(m => m.MeasureValueId.ToString() == symmetryValue.Value);
				if (symmetryEnum != null)
				{
					SymmetryTextBox.Text = symmetryEnum.ValueTitle.ToString();
					SymmetryList.SelectedValue = symmetryValue.Value;
				}
			}

			//Load Reflection in ListBox
			var reflectionEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 177);
			ReflectionList.DataSource = reflectionEnums;
			ReflectionList.DataBind();

			//Load text of Reflection ListBox and Selected Index of Dropdown
			ReflectionTextBox.Text = "";
			ReflectionList.SelectedIndex = -1;
			ReflectionPanel.Visible = false;
			var reflectionValue = itemValues.Find(m => m.MeasureId == 177);
			if (reflectionValue != null)
			{
				ReflectionPanel.Visible = true;
				var reflectionEnum = reflectionEnums.Find(m => m.MeasureValueId.ToString() == reflectionValue.Value);
				if (reflectionEnum != null)
				{
					ReflectionTextBox.Text = reflectionEnum.ValueTitle.ToString();
					ReflectionList.SelectedValue = reflectionValue.Value;
				}
			}

			//Load Weight textbox(es) and populate
			WeightPanel.Visible = false;
			var weightValues = itemValues.FindAll(m => m.MeasureTitle.ToLower().Contains("weight") && !m.MeasureTitle.ToLower().Contains("sarin") && 
															m.Value != null && m.Value != "" && m.Value != "0");
			if(weightValues != null)
			{
				WeightPanel.Visible = true;
				WeightPanel.DataSource = weightValues;
				WeightPanel.DataBind();
			}

			//Load Clarity in ListBox
			var clarityEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 29);
			ClarityList.DataSource = clarityEnums;
			ClarityList.DataBind();

			//Load text of Clarity ListBox and Selected Index of Dropdown
			ClarityTextBox.Text = "";
			ClarityList.SelectedIndex = -1;
			ClarityPanel.Visible = false;
			var clarityValue = itemValues.Find(m => m.MeasureId == 29);
			if (clarityValue != null)
			{
				ClarityPanel.Visible = true;
				var clarityEnum = clarityEnums.Find(m => m.MeasureValueId.ToString() == clarityValue.Value);
				if (clarityEnum != null)
				{
					ClarityTextBox.Text = clarityEnum.ValueTitle.ToString();
					ClarityList.SelectedValue = clarityValue.Value;
				}
			}

			//Load Inclusion in ListBox
			var inclusionEnums = enumValues.FindAll(m => m.MeasureValueMeasureId == 198);
			InclusionList.DataSource = inclusionEnums;
			InclusionList.DataBind();

			//Load text of Inclusion ListBox and Selected Index of Dropdown
			InclusionTextBox.Text = "";
			InclusionList.SelectedIndex = -1;
			InclusionPanel.Visible = false;
			var inclusionValue = itemValues.Find(m => m.MeasureId == 198);
			if (inclusionValue != null)
			{
				InclusionPanel.Visible = true;
				var inclusionEnum = inclusionEnums.Find(m => m.MeasureValueId.ToString() == inclusionValue.Value);
				if (inclusionEnum != null)
				{
					InclusionTextBox.Text = inclusionEnum.ValueTitle.ToString();
					InclusionList.SelectedValue = inclusionValue.Value;
				}
			}

			//Unimplemented Panels
			SarinPanel.Visible = false;
			CommentsPanel.Visible = false;
			PlotPanel.Visible = false;

			//Enable and Populate Data Label and Measure Panel
			DataForLabel.Text = "Data For # " + itemModel.FullItemNumber;
			ShowHideItemDetails(true);
			cmdSave.Visible = true;
			ShapesCategoryEditing.Value = "Collapsed";
			PolishCategoryEditing.Value = "Collapsed";
			SymmetryCategoryEditing.Value = "Collapsed";
			ReflectionCategoryEditing.Value = "Collapsed";
			ClarityCategoryEditing.Value = "Collapsed";
			InclusionCategoryEditing.Value = "Collapsed";
			InitShapesButtons();
			InitPolishButtons();
			InitSymmetryButtons();
			InitReflectionButtons();
			InitClarityButtons();
			InitInclusionButtons();
			*/
        }
		private void PopulateComments()
		{
			var itemValues = GetMeasuresForEditFromView(PartTree.SelectedValue);
			CommentsList.Text = "";
			DiaQualityList.Text = "";
			if (itemValues.Count == 0)
			{
				return;
			}

			//Find comments element
			RepeaterItem commentElement = null, diaQualityElement = null;
			foreach (RepeaterItem element in ElementPanel.Items)
			{
				if ((element.FindControl("ElementType") as HiddenField).Value == "Comments")
				{
					commentElement = element;
				}
				else if ((element.FindControl("ElementName") as HiddenField).Value == "DiamondQuality")
				{
					diaQualityElement = element;
				}
			}

			foreach (var measureId in commentNums)
			{
				var commentValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId.ToString() == measureId);
				if (commentValue != null)
				{
					//Collapse button
					if(commentValue.Value == "0")
					{
						foreach(RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
						{
							if((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
							{
								SetButtonState(btn.FindControl("RepeatButton") as Button, "");
								(btn.FindControl("BtnActive") as HiddenField).Value = "0";
							}
						}
					}
					var enumValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == measureId && m.MeasureValueId.ToString() == commentValue.Value);
					if(enumValue != null)
					{
						if (enumValue.ValueTitle != "")
						{
							CommentsList.Text += enumValue.MeasureValueName;
							CommentsList.Text += "\n";
						}

						//Update style of buttons
						if(commentElement != null)
						{
							foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
							{
								if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString() /*enumValue.MeasureValueMeasureId.ToString()*/)
								{
									var rBtn = btn.FindControl("RepeatButton") as Button;
									if (rBtn.Enabled)
									{
										if (enumValue.MeasureValueName == (btn.FindControl("BtnValue") as HiddenField).Value)
										{
											SetButtonState(rBtn, "Active");
											(btn.FindControl("BtnActive") as HiddenField).Value = "1";
										}
										else
										{
											SetButtonState(rBtn, "");
											(btn.FindControl("BtnActive") as HiddenField).Value = "0";
										}
									}
								}
								
							}
						}
					}
				}
				else//If commentvalue is null, clear all styles
				{
					foreach (RepeaterItem btn in (commentElement.FindControl("ButtonRepeater") as Repeater).Items)
					{
						if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
						{
							var rBtn = btn.FindControl("RepeatButton") as Button;
							if (rBtn.Enabled)
							{
								SetButtonState(rBtn, "");
								(btn.FindControl("BtnActive") as HiddenField).Value = "0";
							}
						}

					}
				}
			}
            foreach (var measureId in diaQualityNums)
            {
                FillDCListAndSetButtonActive(itemValues, diaQualityElement, measureId);
            }
        }

        private void FillDCListAndSetButtonActive(List<ItemValueEditModel> itemValues, RepeaterItem element, string measureId)
        {
            var diaQualityValue = itemValues.Find(m => m.PartId == PartTree.SelectedValue && m.MeasureId.ToString() == measureId);
            if (diaQualityValue != null)
            {
                //Collapse button and all Back to root buttons
                if (diaQualityValue.Value == "0")
                {
                    foreach (RepeaterItem btn in (element.FindControl("ButtonRepeater") as Repeater).Items)
                    {
                        if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString() || (btn.FindControl("BtnMeasure") as HiddenField).Value=="0")
						{
                            SetButtonState(btn.FindControl("RepeatButton") as Button, "");
                            (btn.FindControl("BtnActive") as HiddenField).Value = "0";
                        }
                    }
                }
                var enumValue = GetMeasureEnumsFromView().Find(m => m.MeasureValueMeasureId.ToString() == measureId && m.MeasureValueId.ToString() == diaQualityValue.Value);
                if (enumValue != null)
                {
                    if (enumValue.ValueTitle != "")
                    {
                        DiaQualityList.Text += enumValue.MeasureValueName;
                        DiaQualityList.Text += "\n";
                    }

                    //Update style of buttons
                    if (element != null)
                    {
                        foreach (RepeaterItem btn in (element.FindControl("ButtonRepeater") as Repeater).Items)
                        {
                            if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString())
                            {
                                var rBtn = btn.FindControl("RepeatButton") as Button;
                                if (rBtn.Enabled)
                                {
                                    if (enumValue.MeasureValueName == (btn.FindControl("BtnValue") as HiddenField).Value)
                                    {
                                        SetButtonState(rBtn, "Active");
                                        (btn.FindControl("BtnActive") as HiddenField).Value = "1";
                                    }
                                    else
                                    {
                                        SetButtonState(rBtn, "");
                                        (btn.FindControl("BtnActive") as HiddenField).Value = "0";
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else//If commentvalue is null, clear all styles
            {
                foreach (RepeaterItem btn in (element.FindControl("ButtonRepeater") as Repeater).Items)
                {
                    if ((btn.FindControl("BtnMeasure") as HiddenField).Value == measureId.ToString() || (btn.FindControl("BtnMeasure") as HiddenField).Value == "0")
                    {
                        var rBtn = btn.FindControl("RepeatButton") as Button;
                        if (rBtn.Enabled)
                        {
                            SetButtonState(rBtn, "");
                            (btn.FindControl("BtnActive") as HiddenField).Value = "0";
                        }
                    }

                }
            }
        }

        private void SetButtonState(Button btn, string state)
		{
			btn.Enabled = true;
            
			if(state.ToUpper().Contains("ITEM CONTAINER"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else if (state.Contains("Active"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else if (state.Contains("Disable"))
			{
				btn.Style["background"] = "linear-gradient(to bottom, #b3b3b3, #4d4d4d)";
				btn.Style["border-top-color"] = "#4d4d4d";
				btn.Style["border-left-color"] = "#4d4d4d";
				btn.Style["border-right-color"] = "#4d4d4d";
				btn.Style["border-bottom-color"] = "#333333";
				btn.Enabled = false;
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}

		private void SetCommentButtonState(Button btn, bool active)
		{
			if (active)
			{
				btn.Style["background"] = "linear-gradient(to bottom, #e61919, #c32222)";
				btn.Style["border-top-color"] = "#c32222";
				btn.Style["border-left-color"] = "#c32222";
				btn.Style["border-right-color"] = "#c32222";
				btn.Style["border-bottom-color"] = "#821717";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}
		private void SetSmallButtonState(Button btn, string part)
		{
			if(part.ToUpper().Contains("ITEM CONTAINER"))
			{
				/* Too bright!
				btn.Style["background"] = "linear-gradient(to bottom, #cc0099, #990073)";
				btn.Style["border-top-color"] = "#990073";
				btn.Style["border-left-color"] = "#990073";
				btn.Style["border-right-color"] = "#990073";
				btn.Style["border-bottom-color"] = "#66004d";
				*/
				btn.Style["background"] = "linear-gradient(to bottom, #9966cc, #6620aa)";
				btn.Style["border-top-color"] = "#6620aa";
				btn.Style["border-left-color"] = "#6620aa";
				btn.Style["border-right-color"] = "#6620aa";
				btn.Style["border-bottom-color"] = "#441a88";
			}
			else
			{
				btn.Style["background"] = "linear-gradient(to bottom, #5bc0de, #2f96b4)";
				btn.Style["border-top-color"] = "#2f96b4";
				btn.Style["border-left-color"] = "#2f96b4";
				btn.Style["border-right-color"] = "#2f96b4";
				btn.Style["border-bottom-color"] = "#1f6377";
			}
		}

		void ShowHideItemDetails(bool show)
		{
			if (show)
			{
				DataForLabel.Style["display"] = "";
				PrefixPanel.Style["display"] = "";
				OldNumPanel.Style["display"] = "";
				MeasurementPanel.Style["display"] = "";
				ClarityRadio.Style["display"] = "";
				Comments.Style["display"] = "";
				WeightPanel.Style["display"] = "";
				AddToPrefixPanel.Style["display"] = "";
				IntExtCommentPanel.Style["display"] = "";
				LaserPanel.Style["display"] = "";
				SarinPanel.Style["display"] = "";
				cmdSave.Style["display"] = "";
				shortrpt.Style["display"] = "";
				BrokenStonesPanel.Style["display"] = "";
				DiaQuality.Style["display"] = "";
				LIPresentDiv.Visible = true;
				DefaultSkuBox.Style["display"] = "";
				DefaultSkuLbl.Style["display"] = "";
				DefaultSkuBox.BackColor = System.Drawing.Color.White;
			}
			else
			{
				DataForLabel.Style["display"] = "none";
				PrefixPanel.Style["display"] = "none";
				OldNumPanel.Style["display"] = "none";
				MeasurementPanel.Style["display"] = "none";
				ClarityRadio.Style["display"] = "none";
				YesBtn.Style["display"] = "none";
				NoBtn.Style["display"] = "none";
				CancelBtn.Style["display"] = "none";
				SaveDialogLbl.Style["display"] = "none";
				Comments.Style["display"] = "none";
				WeightPanel.Style["display"] = "none";
				AddToPrefixPanel.Style["display"] = "none";
				IntExtCommentPanel.Style["display"] = "none";
				LaserPanel.Style["display"] = "none";
				SarinPanel.Style["display"] = "none";
				cmdSave.Style["display"] = "none";
				shortrpt.Style["display"] = "none";
				BrokenStonesPanel.Style["display"] = "none";
				DiaQuality.Style["display"] = "none";
				LIPresentDiv.Visible = false;
				DefaultSkuBox.Style["display"] = "none";
				DefaultSkuLbl.Style["display"] = "none";
				DefaultSkuBox.BackColor = System.Drawing.Color.White;
			}  
		}

		private void GetNewValuesFromGrid()
		{
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			ItemValueEditModel itemValue;

			foreach(RepeaterItem element in ElementPanel.Items)
			{
				var eleValue = element.FindControl("ElementMeasure") as HiddenField;
				itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == eleValue.Value);
				if(itemValue != null)
				{
					var eleList = element.FindControl("DropDownListBox") as ListBox;
					var newValue = eleList.SelectedValue;
					itemValue.Value = newValue;
				}
			}
			//LI present
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == 113);
			if (itemValue != null)
			{
                if (TextLIPresentNoneBox.Text != "")
                    itemValue.Value = "5645"; //TextLIPresentNoneBox.Text;
                else
                if (TextLIPresentPreExistingBox.Text != "")
					itemValue.Value = "7192"; //TextLIPresentPreExistingBox.Text;
				else if (TextLIPresentLIBox.Text != "")
					itemValue.Value = "5644"; // TextLIPresentLIBox.Text;
				else if (TextLIPresentMissingBox.Text != "")
					itemValue.Value = "9803"; // TextLIPresentMissingBox.Text;
				//else
				//	itemValue.Value = "5645";//None
			}
			//Text boxes
			List<KeyValuePair<TextBox, int>> textBoxes = new List<KeyValuePair<TextBox, int>>
			{
				new KeyValuePair<TextBox, int>(PrefixText, 112),
				new KeyValuePair<TextBox, int>(LaserInscription, 67),
				new KeyValuePair<TextBox, int>(InternalComments, 26),
				new KeyValuePair<TextBox, int>(ExternalComments, 9),
				new KeyValuePair<TextBox, int>(BSValue, 208),
				new KeyValuePair<TextBox, int>(DSFailValue,206),
				new KeyValuePair<TextBox, int>(DSPassValue,205),
				new KeyValuePair<TextBox, int>(FFStonesValue,270),
				new KeyValuePair<TextBox, int>(LabGrownValue,212),
				new KeyValuePair<TextBox, int>(LDStonesValue,269),
				new KeyValuePair<TextBox, int>(ILDStonesValue,271),
				new KeyValuePair<TextBox, int>(MissingStonesValue,209),
				new KeyValuePair<TextBox, int>(SimStonesValue,213),
				new KeyValuePair<TextBox, int>(SynthStonesValue,211),
				new KeyValuePair<TextBox, int>(UnitsValue,65)
			};

			foreach(var box in textBoxes)
			{
				itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId == box.Value);
				if(itemValue != null)
				{
					itemValue.Value = box.Key.Text;
				}
			}

			//Comments
			foreach(RepeaterItem ele in ElementPanel.Items)
			{
				//string zzz = (ele.FindControl("ElementType") as HiddenField).Value;
				if ((ele.FindControl("ElementType") as HiddenField).Value == "Comments" || (ele.FindControl("ElementType") as HiddenField).Value == "DiamondQuality")
				{
					var btnList = ele.FindControl("ButtonRepeater") as Repeater;
					//First, set all of the values to be the null version
					foreach (RepeaterItem item in btnList.Items)
					{
						var mea = item.FindControl("BtnMeasure") as HiddenField;
						if (mea.Value != "0")
						{
							if (itemValues != null)
							{
								itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
								if (itemValue != null)
								{
									var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
									EnumMeasureModel enumValue;
									enumValue = enumValues.Find(m => m.ValueTitle == "");

									if (itemValue != null && enumValue != null && itemValue.PrevValue != "0")
									{
										var newValue = enumValue.MeasureValueId.ToString();
										itemValue.Value = newValue;
									}
								}
							}
						}
					}
					
					//Then, set them active upon finding an active button
					foreach(RepeaterItem item in btnList.Items)
					{
						var mea = item.FindControl("BtnMeasure") as HiddenField;
						var btnStyle = (item.FindControl("RepeatButton") as Button).Style["border-bottom-color"];
						var btnActive = (item.FindControl("BtnActive") as HiddenField).Value;
						if (mea.Value != "0")
						{
							if (itemValues != null)
							{
								itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && m.MeasureId.ToString() == mea.Value);
								if (itemValue != null)
								{
									var enumValues = GetMeasureEnumsFromView().FindAll(m => m.MeasureValueMeasureId.ToString() == mea.Value);
									EnumMeasureModel enumValue = null;
									if (btnActive == "1")//#1f6377 for inactive, #821717 for active, 0 for uninitialized
									{
										enumValue = enumValues.Find(m => m.MeasureValueName == (item.FindControl("BtnValue") as HiddenField).Value);
									}

									if (itemValue != null && enumValue != null)
									{
										var newValue = enumValue.MeasureValueId.ToString();
										itemValue.Value = newValue;
									}
								}
							}
						}
					}
					//break;
				}
			}

			/* Old code for GetNewValuesFromGrid
			//Shapes
			var itemValues = GetMeasuresForEditFromView(PartEditing.Value);
			var itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "8");
			if (itemValue != null)
			{
				var newValue = ShapesList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Polish
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "18");
			if(itemValue != null)
			{
				var newValue = PolishList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Symmetry
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "19");
			if (itemValue != null)
			{
				var newValue = SymmetryList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Clarity
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "29");
			if(itemValue != null)
			{
				var newValue = ClarityList.SelectedValue;
				itemValue.Value = newValue;
			}
			//Inclusion
			itemValue = itemValues.Find(m => m.PartId == PartEditing.Value && "" + m.MeasureId == "198");
			if(itemValue != null)
			{
				var newValue = InclusionList.SelectedValue;
				itemValue.Value = newValue;
			}
			*/
		}
		#endregion

		#region Get Data From View

		//-- Measure Values
		private List<MeasureValueModel> GetMeasureValuesFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValues) as List<MeasureValueModel> ?? new List<MeasureValueModel>();
		}

		//-- Measure Enums
		private List<EnumMeasureModel> GetMeasureEnumsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureEnums) as List<EnumMeasureModel> ?? new List<EnumMeasureModel>();
		}

		//-- Loading Measures Description
		private List<MeasureValueCpModel> GetMeasuresCpFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureValuesCp) as List<MeasureValueCpModel>;
		}

		//-- Loading Item Numbers
		private List<SingleItemModel> GetItemNumbersFromView()
		{

			return GetViewState(SessionConstants.GradeItemNumbers) as List<SingleItemModel> ?? new List<SingleItemModel>();
		}
		private SingleItemModel GetItemModelFromView(string itemNumber)
		{
            //return GetItemNumbersFromView().Find(m => m.FullOldItemNumber == itemNumber);
            return GetItemNumbersFromView().Find(m => m.FullItemNumber == itemNumber || m.FullOldItemNumber == itemNumber || m.FullBatchNumber == itemNumber);
        }
		private SingleItemModel GetSelectedItemModel()
		{
			var itemNumber = lstItemList.SelectedValue;
			if (itemNumber.StartsWith("*"))
			{
				itemNumber = itemNumber.Substring(1);
			}
			if (string.IsNullOrEmpty(itemNumber)) return null;
			return GetItemModelFromView(itemNumber);
		}
		private List<MeasurePartModel> GetPartsFromView()
		{
			return GetViewState(SessionConstants.GradeMeasureParts) as List<MeasurePartModel> ?? new List<MeasurePartModel>();
		}
		private List<ItemValueEditModel> GetMeasuresForEditFromView(string partId)
		{
			var data = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ?? new List<ItemValueEditModel>();
			if (string.IsNullOrEmpty(partId)) return data;
			else return data.FindAll(m => m.PartId == partId);
		}
		#endregion

		#region Information Dialog
		private void PopupInfoDialog(string msg, bool isErr)
		{
			MessageDiv.InnerText = msg;
			InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
			InfoPopupExtender.Show();
		}

        protected void OnInfoCloseButtonClick(object sender, EventArgs e)
        {            
            if (lstItemList.SelectedIndex != lstItemList.Items.Count - 1)
            {
                lstItemList.SelectedIndex = lstItemList.SelectedIndex + 1;
                OnItemListSelectedChanged(null, null);
            }

        }

		#endregion

		#region Question Dialog
		static string ModeOnItemChanges = "item";
		static string ModeOnLoadClick = "load";
		static string ModeOnShortReportClick = "report";
		private void PopupSaveQDialog(string src)
		{
			//var test = GetMeasuresForEditFromView("").FindAll(m => m.HasChange);
			var cnt = GetMeasuresForEditFromView("").FindAll(m => m.HasChange).Count;
			SaveDlgMode.Value = src;
			QTitle.Text = string.Format("Save #{0} Item Values ({1} measures)", ItemEditing.Value, cnt);
			SaveQPopupExtender.Show();
		}
		protected void OnQuesSaveYesClick(object sender, EventArgs e)
		{
			var errMsg = SaveExecute();
			if (!string.IsNullOrEmpty(errMsg))
			{
				// Return on prev item
				txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
				lstItemList.SelectedValue = ItemEditing.Value;
				PopupInfoDialog(errMsg, true);
				return;
			}

			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				DSSCheckBox.Checked = false;
				LoadExecute();
			}
			if(SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
		protected void OnQuesSaveNoClick(object sender, EventArgs e)
		{
			if (SaveDlgMode.Value == ModeOnItemChanges)
			{
				OnItemListSelected();
			}
			if (SaveDlgMode.Value == ModeOnLoadClick)
			{
				DSSCheckBox.Checked = false;
				LoadExecute();
			}
			if (SaveDlgMode.Value == ModeOnShortReportClick)
			{
				//Redirect to Short Report
				var batchID = GetItemModelFromView(ItemEditing.Value).BatchId;
				Response.Redirect("ItemView.aspx?BatchId=" + batchID + "&All=1");
			}
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
		protected void OnQuesSaveCancelClick(object sender, EventArgs e)
		{
			lstItemList.SelectedValue = ItemEditing.Value;
			txtBatchNumber.Text = ItemEditing.Value.Substring(0, ItemEditing.Value.Length - 2);
			YesBtn.Style["display"] = "none";
			NoBtn.Style["display"] = "none";
			CancelBtn.Style["display"] = "none";
			SaveDialogLbl.Style["display"] = "none";
			lstItemList.Enabled = true;
			txtBatchNumber.Enabled = true;
		}
        #endregion
        protected string TruncateToDot(string value)
        {
			return value.Substring(0, value.IndexOf("."));
        }

        protected void DSSCheckBox_CheckedChanged(object sender, EventArgs e)
        {
			if (DSSCheckBox.Checked)
			{
				string extComm = ExternalComments.Text.ToLower();
				if (!extComm.ToLower().Contains("side stones not examined"))
				{
					if (ExternalComments.Text == "")
						ExternalComments.Text = "Side Stones not examined";
					else
						ExternalComments.Text += Environment.NewLine + "Side Stones not examined";
				}
			}
			else
			{
				ExternalComments.Text = ExternalComments.Text.ToLower();
				ExternalComments.Text = ExternalComments.Text.Replace("side stones not examined", "");
			}

		}
		protected void OnLiPresentClick(object sender, EventArgs e)
		{
			string name = sender.ToString();
			Button btn = (Button)sender;
			string btnTxt = btn.Text;
            if (btnTxt == "LI Present None")
            {
                TextLIPresentNoneBox.Text = "None";
                TextLIPresentNoneBox.BackColor = System.Drawing.Color.Pink;
                TextLIPresentPreExistingBox.Text = "";
                TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.White;
                TextLIPresentLIBox.Text = "";
                TextLIPresentLIBox.BackColor = System.Drawing.Color.White;
                TextLIPresentMissingBox.Text = "";
                TextLIPresentMissingBox.BackColor = System.Drawing.Color.White;
            }
            else
            if (btnTxt == "LI Pre-Existing")
			{
                TextLIPresentNoneBox.Text = "";
                TextLIPresentNoneBox.BackColor = System.Drawing.Color.White;
                if (TextLIPresentPreExistingBox.Text == "")
				{
					TextLIPresentPreExistingBox.Text = "Pre-Existing";
					TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.Pink;
				}
				else
				{
					TextLIPresentPreExistingBox.Text = "";
					TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.White;
					TextLIPresentNoneBox.Text = "None";
					TextLIPresentNoneBox.BackColor = System.Drawing.Color.Pink;
				}
				TextLIPresentLIBox.Text = "";
				TextLIPresentLIBox.BackColor = System.Drawing.Color.White;
				TextLIPresentMissingBox.Text = "";
				TextLIPresentMissingBox.BackColor = System.Drawing.Color.White;
			}
			if (btnTxt == "LI Present")
			{
                TextLIPresentNoneBox.Text = "";
                TextLIPresentNoneBox.BackColor = System.Drawing.Color.White;
                if (TextLIPresentLIBox.Text == "")
				{
					TextLIPresentLIBox.Text = "LI";
					TextLIPresentLIBox.BackColor = System.Drawing.Color.Pink;
				}
				else
				{
					TextLIPresentLIBox.Text = "";
					TextLIPresentLIBox.BackColor = System.Drawing.Color.White;
					TextLIPresentNoneBox.Text = "None";
					TextLIPresentNoneBox.BackColor = System.Drawing.Color.Pink;
				}
				TextLIPresentPreExistingBox.Text = "";
				TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.White;
				TextLIPresentMissingBox.Text = "";
				TextLIPresentMissingBox.BackColor = System.Drawing.Color.White;
			}
			if (btnTxt == "LI Missing")
			{
                TextLIPresentNoneBox.Text = "";
                TextLIPresentNoneBox.BackColor = System.Drawing.Color.White;
                if (TextLIPresentMissingBox.Text == "")
				{
					TextLIPresentMissingBox.Text = "Missing";
					TextLIPresentMissingBox.BackColor = System.Drawing.Color.Pink;
				}
				else
				{
					TextLIPresentMissingBox.Text = "";
					TextLIPresentMissingBox.BackColor = System.Drawing.Color.White;
					TextLIPresentNoneBox.Text = "None";
					TextLIPresentNoneBox.BackColor = System.Drawing.Color.Pink;
				}
				TextLIPresentLIBox.Text = "";
				TextLIPresentPreExistingBox.BackColor = System.Drawing.Color.White;
				TextLIPresentPreExistingBox.Text = "";
				TextLIPresentLIBox.BackColor = System.Drawing.Color.White;
				//TextLIPresentMissingBox.Text = "Missing";
				//TextLIPresentMissingBox.BackColor = System.Drawing.Color.Pink;

			}

		}
	}
}