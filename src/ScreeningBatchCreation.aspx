﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningBatchCreation.aspx.cs" Inherits="Corpt.ScreeningBatchCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        .lbl {
            width: 400px;
            display: inline-block;
            font-size: 12px;
        }

        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        input, button, select, textarea {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

            input[type="checkbox"], label {
                margin-top: 0;
                margin-bottom: 0;
                line-height: normal;
            }

        body {
            font-family: Tahoma,Arial,sans-serif;
            /*font-size: 75%;*/
        }

        .headingPanel {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow {
            background-color: #FFFF00;
        }

        .text_nohighlitedyellow {
            background-color: white;
        }
    </style>

    <script type="text/javascript">

        <%-- $().ready(function () {

            $('#<%=txtOrder.ClientID%>, #<%=txtAllocateItems.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

        });

        function isOnlyNumber(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57)
                return false;
            return true;
        }--%>
		
    </script>
    <asp:UpdatePanel ID="MainPanel" runat="server">
        <ContentTemplate>
            <div class="demoarea">
                <div style="font-size: smaller; height: 25px;">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                        <ProgressTemplate>
                            <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                            <b>Please, wait....</b>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>

                <div class="demoheading">
                    Create Batch
                    <asp:Label runat="server" ID="InvalidLabel" ForeColor="Red" Style="padding-left: 10px"></asp:Label>
                </div>
                <table style="width: 100%;">

                    <tr>
                        <td style="vertical-align: top; white-space: nowrap;">

                            <asp:TextBox ID="txtOrder" runat="server" name="txtOrder" MaxLength="7"
                                placeholder="Order code" Style="width: 115px;" OnFocus="this.select();" autocomplete="off"
                                onkeypress="return event.charCode >= 48 && event.charCode <= 57" ValidationGroup="BatchGroup"  />
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtOrder"></ajaxToolkit:FilteredTextBoxExtender>
                            <!-- 'Load' button -->



                            <asp:RequiredFieldValidator runat="server" ID="OrderReq" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Order code is required."
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqE" TargetControlID="OrderReq"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:RegularExpressionValidator runat="server" ID="OrderRegExpr" ControlToValidate="txtOrder"
                                Display="None" ValidationExpression="(.{1,100})" ErrorMessage="<b>Invalid Field</b><br />Please enter a order code in the format:<br /><strong>six or seven numeric characters</strong>"
                                ValidationGroup="BatchGroup" />
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OrderReqExpr" TargetControlID="OrderRegExpr"
                                HighlightCssClass="validatorCalloutHighlight" />
                            <asp:Button ID="btnOrderSearch" runat="server" class="btn btn-info btn-large" Text="Load" ValidationGroup="BatchGroup"
                                Style="margin-left: 10px" OnClick="btnOrderSearch_Click" />

                        </td>

                    </tr>
                </table>

                <div id="divResult" runat="server">

                    <table style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; white-space: nowrap;">

                                <table>
                                    <tr>
                                        <td style="height: 35px;">Total Quantity : 
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotQty" runat="server" Style="height: 35px;"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Certified By
                                        </td>
                                        <td>

                                            <asp:TextBox ID="txtCertifiedBy" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:Button ID="btnCertifiedBy" runat="server" Text="CertifiedBy" CssClass="btn btn-info btn-large" Style="padding-left: 10px; background: linear-gradient(to bottom, #9966cc, #6620aa); border-top-color: #6620aa; border-left-color: #6620aa; border-right-color: #6620aa; border-bottom-color: #441a88; margin-left: 3px;" />
                                            <asp:HiddenField ID="hdnCertifiedByCode" runat="server" Value="0" />
                                            <%-- Screening Instrument Dialog --%>
                                            <asp:Panel runat="server" ID="pnlCertifiedBy" CssClass="modal-dialog" Width="800px" Style="background-color: white; border: 2px solid gray; display: none; background-color: #EFEFEF; box-shadow: 10px 10px  5px rgba(0,0,0,0.6); padding: 10px 10px 10px 10px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label runat="server" ID="Label1" Text="Screening Instrument"></asp:Label>
                                                        </td>
                                                        <td><%-- --%>
                                                            <asp:DropDownList ID="ddlCertifiedBy" runat="server" DataTextField="CertifiedBy" OnSelectedIndexChanged="ddlCertifiedBy_SelectedIndexChanged" AutoPostBack="true"
                                                                DataValueField="CertifiedByCode" Width="150px" Height="26px">
                                                            </asp:DropDownList>

                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><%--width:100px;--%>
                                                            <asp:Repeater ID="repCertifiedBy" runat="server" OnItemCommand="repCertifiedBy_ItemCommand">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnCertifiedBy" runat="server" CssClass="btn btn-info btn-large" Style="margin: 5px 5px 5px 5px;"
                                                                        CommandName="CertifiedByCode"
                                                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CertifiedCode")%>'
                                                                        Text='<%# DataBinder.Eval(Container.DataItem, "CertifiedBy")%>'
                                                                        CausesValidation="false" />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="pnlCertifiedBy" TargetControlID="btnCertifiedBy" CancelControlID="ctl00_SampleContent_ModalPopupExtender3_backgroundElement"
                                                Enabled="True" DynamicServicePath="">
                                            </ajaxToolkit:ModalPopupExtender>


                                        </td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td style="width: 120px;">Screening Instrument</td>
                                        <td>

                                            <asp:CheckBoxList ID="chkLstScreeningInstrument" runat="server" AutoPostBack="false" RepeatDirection="Horizontal" CssClass="checkboxSingle">
                                            </asp:CheckBoxList>

                                        </td>
                                    </tr>
                                </table>

                            </td>

                        </tr>

                        <tr>

                            <td style="vertical-align: -webkit-baseline-middle;">
                                <div style="padding-left: 5px; padding-top: 25px; padding-bottom: 25px;">
                                    <h5>Item allocation for creating batches</h5>

                                    <div>


                                        <table>
                                            <tr>
                                                <td style="vertical-align: bottom;">No. of Items Left</td>
                                                <td style="vertical-align: bottom;">Allocate Items in Batch</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtItemsLeft" runat="server" MaxLength="6" Width="100" Text="" ReadOnly="true"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtAllocateItems" runat="server" MaxLength="6" Width="100" autocomplete="off"></asp:TextBox></td>
                                                <td>
                                                    <asp:Button ID="btnAddBatch" runat="server" Text="Add Batch" CssClass="btn btn-info btn-large" OnClick="btnAddBatch_Click" /></td>
                                            </tr>

                                        </table>

                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" Style="margin-top: 20px;" OnRowDeleting="gvItems_RowDeleting" CellPadding="10">

                                            <HeaderStyle HorizontalAlign="Left" />
                                            <RowStyle HorizontalAlign="Left" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Batch Code" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBatchCode" runat="server" Text='<%# Eval("BatchCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Item Qty" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAllocateItemQty" runat="server" Text='<%# Eval("AllocateItemQty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Fail Item Qty" HeaderStyle-Width="90">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemQtyFail" runat="server" Text='<%# Eval("ItemQtyFail") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="120">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:CommandField ShowDeleteButton="true" DeleteImageUrl="Images/delete.png" />
                                            </Columns>
                                        </asp:GridView>


                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div>
                                    <asp:Button ID="btnCreateBatches" runat="server" Text="Create Batches" CssClass="btn btn-info btn-large" OnClick="btnCreateBatches_Click" />
                                    <asp:Button ID="btnItemize" runat="server" Text="Itemize Fail Items" CssClass="btn btn-sm btn-info" Visible="false" />
                                    <asp:Button ID="btnPrint" runat="server" Text="Print Batch Label" CssClass="btn btn-sm btn-info" Visible="false" />
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClear_Click" />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div style="visibility: hidden;">
                                    <asp:Label ID="lblMemo" runat="server" Style="height: 35px;"></asp:Label>
                                    <asp:Label ID="lblServiceType" runat="server" Style="height: 35px;"></asp:Label>
                                    <asp:Label ID="lblRetailer" runat="server" Style="height: 35px;"></asp:Label>
                                </div>
                            </td>
                        </tr>

                    </table>

                </div>



            </div>
        </ContentTemplate>

        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddBatch" EventName="Click" />
        </Triggers>

    </asp:UpdatePanel>
</asp:Content>
