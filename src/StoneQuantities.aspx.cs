﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models.Stats;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;

namespace Corpt
{
    public partial class StoneQuantities : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Stone Quantities";
            if (!IsPostBack)
            {
                LoadOrderStates();
                LoadCustomers();
                LoadOfficesTree();
                OnChangedPeriodType(null, null);
            }
        }
        private void LoadOrderStates()
        {
            OrderStateList.DataSource = QueryUtils.GetOrderStateCodes(this);
            OrderStateList.DataBind();
            OrderStateList.SelectedIndex = 0;
        }
        #region Period Type
        private static string PeriodMonthly = "m";
        private static string PeriodWeekly = "w";
        private static string PeriodRandom = "r";
        protected void OnChangedPeriodType(object sender, EventArgs e)
        {
            var type = periodType.SelectedValue;
            var isRandom = (type == PeriodRandom);
            calFrom.Enabled = isRandom;
            calTo.Enabled = isRandom;
            if (isRandom) return;
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var isWeekly = (type == PeriodWeekly);
            var dateFrom = PaginatorUtils.ConvertDateToString(isWeekly ? today.AddDays(-7) : today.AddMonths(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;
        }
        #endregion
        
        #region Filter
        private StatsFilterModel GetFilterFromPage()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime)dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.PeriodType = periodType.SelectedValue;
            filterModel.CustomerId = lstCustomerList.SelectedValue;
            filterModel.OrderStateCode = OrderStateList.SelectedValue;
            return filterModel;
        }
        #endregion

        #region Offices Tree
        private void LoadOfficesTree()
        {
            var countries = QueryUtils.GetCountryOffices(this);
            OfficeTree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Offices" } };
            foreach (var country in countries) 
            {
                data.Add(new TreeViewModel { ParentId = "0", Id = country.Country, DisplayName = country.Country });
                foreach (var office in country.Offices) 
                {
                    data.Add(new TreeViewModel { ParentId = country.Country, Id = office.OfficeId, DisplayName = office.DisplayOffice });
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            OfficeTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }
        private IEnumerable<string> GetCheckedOffices()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in OfficeTree.CheckedNodes)
            {
                if (node.Depth != 2) continue;//-- 0 - all, 1 - country, 2 - office
                checkItems.Add(node.Value);
            }
            return checkItems;
        }
        #endregion
       
        #region Stats buttons click
        static int ColumnOfficeCodeIdx = 1;
        protected void OnDetailsBtnClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            var officeId = "" + GridCountries.DataKeys[item.ItemIndex];
            var officeName = item.Cells[ColumnOfficeCodeIdx].Text;

            var items = QueryUtils.GetStoneQuantityByLocationPerCustomer(GetFilterFromPage(), officeId, this);
            GridOfficeCustomers.DataSource = items;
            GridOfficeCustomers.DataBind();
            var format = "{0} office, {1} customers, {2} stones";
            var qnty = items.Sum(m => m.Quantity);
            OfficeLabel.InnerText = string.Format(format, officeName, items.Count, qnty);
        }
        protected void OnLookupByLocationClick(object sender, ImageClickEventArgs e)
        {
            var filterModel = GetFilterFromPage();
            var offices = GetCheckedOffices();
            if (!offices.Any())
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Offices Not Found!" + "\")</SCRIPT>");
                return;
            }
            ClearGridOfficeCustomer();
            filterModel.OfficesIds = offices.ToList();
            var data = QueryUtils.GetStoneQuantityByLocation(filterModel, this);
            
            var total = data.Sum(m => m.Quentity);
            GridCountries.DataSource = data;
            GridCountries.DataBind();
            var format = "Total: {0} offices, {1} stones";
            ByCountryTotal.InnerText = string.Format(format, data.Count, total);
        }
        private void ClearGridOfficeCustomer()
        {
            OfficeLabel.InnerText = "";
            GridOfficeCustomers.DataSource = null;
            GridOfficeCustomers.DataBind();
        }
        protected void OnLookupByCustomerClick(object sender, ImageClickEventArgs e)
        {
            var filterModel = GetFilterFromPage();
            var data = QueryUtils.GetStoneQuantityByCustomer(filterModel, this);
            var total = data.Sum(m => m.Quantity);
            GridCustomers.DataSource = data;
            GridCustomers.DataBind();
            var format = "Total: {0} customers, {1} stones";
            ByCustomerTotal.InnerText = string.Format(format, data.Count, total);
        }
        #endregion

        #region Customer Search
        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this);
            customers.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
            lstCustomerList.SelectedValue = "";
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            OnCustomerLikeClick(null, null);
        }
        protected void OnCustomerLikeClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            filtered.Insert(0, new CustomerModel { CustomerId = "", CustomerName = "All" });
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
            if (filtered.Count == 1) lstCustomerList.SelectedValue = "";
            if (filtered.Count > 1) lstCustomerList.SelectedValue = filtered.ElementAt(1).CustomerId;
        }
        #endregion
    }
}