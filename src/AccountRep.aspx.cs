﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Corpt.AccountRepModels;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.Utilities;
using Corpt.Utilities.AccountRepresentative;
using Corpt.Utilities.BulkBilling;
using Corpt.Utilities.BulkBilling.CostManager;
using Corpt.Utilities.BulkBilling.CostManager.Models;
using GemoDream.Database;
using GemoDream.PdfGenerator.Invoice;
using GemoDream.QueueMessages.AccountRepDto;
using Newtonsoft.Json;
using GemoDream.QueueMessages.QueueMessagesModels;
using GemoDream.QueueMessages.QueueMessagesModels.QBInvoice;
using Button = System.Web.UI.WebControls.Button;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Control = System.Web.UI.Control;
using Formatting = Newtonsoft.Json.Formatting;
using Image = System.Web.UI.WebControls.Image;
using Label = System.Web.UI.WebControls.Label;
using ListItem = System.Web.UI.WebControls.ListItem;
using QueryUtils = Corpt.Utilities.QueryUtils;
using TextBox = System.Web.UI.WebControls.TextBox;
using TreeNode = System.Web.UI.WebControls.TreeNode;
using TreeNodeCollection = System.Web.UI.WebControls.TreeNodeCollection;
using MessagingToolkit.QRCode.Codec;
using System.Text.RegularExpressions;
using System.Net;

namespace Corpt
{
    public partial class AccountRep : CommonPage
    {
        #region Constants and Variables

        const string OrderSearchByOrderFieldName = "Order[.Batch[.Item]]";
        const string OrderSearchByOrderListFieldName = "Order";
        public string OrderSearchFieldName => OrderSearchByOrderFieldName;

        private const string StateCodeColumnName = "StateCode";
        private const string LoadOrderFunctionTitle = "Load Order";
        private const string EndSessionFunctionTitle = "End Session";
        private const string EndSessionIsFinishedFunctionTitle = "End Session is finished.";
        private const string BatchLabelMultipleBatchConfirmationTitle = "Edit Batch Label Properties.Warning!";
        private const int OrderDepth = 0;
        private const int BatchDepth = 1;
        private const int ItemDepth = 2;

        // ReSharper disable once UnusedMember.Local
        private const int ItemDocDepth = 3;
        private const string CreateDateFormat = "M/d/yyyy";
        private const int GroupState = 2;

        // Controls Ids
        private const string ControlIdPricesLabel = "PricesLabel";
        private const string ControlIdDefaultPricesLabel = "DefaultPricesLabel";
        private const string ControlIdSpecialPricesLabel = "SpecialPricesLabel";
        private const string ControlIdQuantitiesLabel = "QuantitiesLabel";
        private const string ControlIdCostsLabel = "CostsLabel";
        private const string ControlIdCostsInvoiceLabel = "CostsInvoiceLabel";
        private const string ControlIdAnswerCheckBox = "AnswerCheckBox";
        private const string ControlIdOrderStatusImage = "OrderStatusImage";

        private const string OrderStructurePattern = "{0} batch - {1} items";
        private const int TreeViewTotalNodesLimitDefault = 4096;
        private const string ExtendedOrderSearchButtonText = @"Extended Order Search";
        private const string SignetRetailerRule = "Signet";
        private const int MaxDisplayBatches = 8;

        public string BulkBillingHideStyle { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.TryParse(this.TimeZoneOffsetHidden.Value, out var timeZoneOffset))
            {
                SetViewState(timeZoneOffset, SessionConstants.TimeZoneOffsetInMinutes);
            }

            var parameter = Request["__EVENTARGUMENT"];

            if (parameter == "PopupHide")
            {
                return;
            }

            if (Session["ID"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }

            if (IsPostBack)
            {
                ResetCalendarExtenders();
                if (parameter == "TreeNodeChecked")
                {
                    AdjustOrdersTreeViewCheckedText();
                    AdjustAdditionalServicesDataGrid();
                    InitOrdersTreeCommands();

                    var areSelectedItemsScreening =
                        SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Screening;
                    this.BulkBillingCheckBox.Checked = areSelectedItemsScreening;
                    BulkBillingCheckBox_OnCheckedChanged(null, null);
                    this.BulkBillingCheckBox.Visible = areSelectedItemsScreening;
                }

                if (parameter != "BulkBillingChecked")
                {
                }

                return;
            }

            if (this.Session["MachineName"] is string machineName)
            {
                this.MachineName.Text = machineName;
                this.MachineName.ForeColor = Color.Black;
            }
            else
            {
                this.MachineName.Text = @"Missing !";
                this.MachineName.ForeColor = Color.Red;
            }

            InitServiceTypeCategory(Session["ID"]);

            IsSearchByOrderList = false;
            SetPageState();

            LoadCustomers();
            LoadObjectStates();
            InitCreateDateCriteria();
            InitOrdersTreeCommands();
            InitAdditionalServicesDataGrid();
            InitMigratedItemsDataGrid();
            InitCurrentBatchItems();
            InitMeasuresDataGrid();
            ClearInvoiceDropDownList();
            InitInvoiceItemsGridView();
            InitTimeZoneTypeDropDownList();
            HideImages();

            this.CustomerCodeTextBox.Focus();
        }

        private void InitServiceTypeCategory(object userId)
        {
            this.ServiceTypesCategoryDropDown.DataTextField = "Text";
            this.ServiceTypesCategoryDropDown.DataValueField = "Value";

            List<ListItem> dataSource = Enum.GetValues(typeof(ServiceTypesCategory))
                .Cast<ServiceTypesCategory>()
                .Select(filter => new ListItem(GetEnumDescription(filter), filter.ToString()))
                .ToList();
            dataSource.Last().Attributes["style"] = "color: lightgray !important;";

            this.ServiceTypesCategoryDropDown.DataSource = dataSource;

            this.ServiceTypesCategoryDropDown.DataBind();

            // Set Selection
            var isScreeningServices = AccessUtils.IsBulkBillingUser(userId.ToString(), this, out var err);
            LogUtils.UpdateSpLog($"AccessUtils.IsBulkBillingUser returns \"{isScreeningServices}\", err=\"{err}\"",
                this.Session);

            var serviceTypesCategory = isScreeningServices
                ? ServiceTypesCategory.ScreeningServices
                : ServiceTypesCategory.LabServices;

            var selectedIndex = dataSource.FindIndex(item => item.Value.ToString() == serviceTypesCategory.ToString());
            if (selectedIndex >= 0)
            {
                this.ServiceTypesCategoryDropDown.SelectedIndex = selectedIndex;
            }

            ServiceTypeCategoryDropDown_OnSelectedIndexChanged(null, null);
        }

        public string GetEnumDescription<TEnum>(TEnum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var attributes = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0
                ? ((DescriptionAttribute)attributes[0]).Description
                : value.ToString();
        }

        public ServiceTypesBillingSet SelectedItemsServiceTypesBillingSet
        {
            get =>
                GetViewState(SessionConstants.SelectedItemsServiceTypesBillingSet) == null
                    ? ServiceTypesBillingSet.Unset
                    : (ServiceTypesBillingSet)GetViewState(SessionConstants.SelectedItemsServiceTypesBillingSet);
            set
            {
                SetViewState(value, SessionConstants.SelectedItemsServiceTypesBillingSet);
                this.BillingOptionsLabel.Text = $@"Billing Options ({value})";
            }
        }

        public bool IsSearchByOrderList
        {
            get
            {
                var o = GetViewState(SessionConstants.IsSearchByOrderList);
                return o != null && (bool)o;
            }
            set => SetViewState(value, SessionConstants.IsSearchByOrderList);
        }

        private void InitOrdersTreeCommands()
        {
            if (OrdersTreeView.Nodes.Count == 0)
            {
                EndSessionButton.Enabled = false;
                PrintLabelButton.Enabled = false;
                FinalLabelBtn.Enabled = false;
                FinalLabelLEBtn.Enabled = false;
                LELabelsCount.Enabled = false;
                BillingButton.Enabled = false;
                BillingButton.Text = @"Billing";
                UpdateOrderButton.Enabled = false;
                //BatchLabelPropertiesButton.Enabled = false;
            }
            else
            {
                var checkedItems = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                    .Where(treeNode => treeNode.Depth == ItemDepth).ToList();

                if (checkedItems.Count == 0)
                {
                    EndSessionButton.Enabled = false;
                    PrintLabelButton.Enabled = false;
                    FinalLabelBtn.Enabled = false;
                    FinalLabelLEBtn.Enabled = false;
                    LELabelsCount.Enabled = false;
                    BillingButton.Enabled = false;
                    UpdateOrderButton.Enabled = false;
                    BillingButton.Text = @"Billing";
                }
                else
                {
                    EndSessionButton.Enabled = true;
                    PrintLabelButton.Enabled = true;
                    FinalLabelBtn.Enabled = true;
                    FinalLabelLEBtn.Enabled = true;
                    LELabelsCount.Enabled = true;
                    BillingButton.Enabled = true;
                    UpdateOrderButton.Enabled = true;
                    var category = GetServiceTypesCategory(checkedItems);
                    SelectedItemsServiceTypesBillingSet = category;
                    SetBillingButtonTextAndTooltip(category);
                    SetPageState();
                }

                var checkedBatches = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                    .Where(treeNode => treeNode.Depth == BatchDepth).ToList();
                BatchLabelPropertiesButton.Enabled = checkedBatches.Count > 0;

            }
        }

        private void SetBillingButtonTextAndTooltip(ServiceTypesBillingSet category)
        {
            if (category == ServiceTypesBillingSet.Screening)
            {
                BillingButton.Text = @"Billing (Screening)";
                BillingButton.ToolTip = @"Billing for order(s) of Screening services will be started";
            }
            else if (category == ServiceTypesBillingSet.LabServices)
            {
                BillingButton.Text = @"Billing (Lab Services)";
                BillingButton.ToolTip = @"Lab Services Billing will be started";
            }
            else
            {
                BillingButton.Text = @"Billing (Mixed!)";
                BillingButton.ToolTip = @"All checked items should have either Screening or Others service types";
            }
        }

        private ServiceTypesBillingSet GetServiceTypesCategory(IEnumerable<TreeNode> checkedItems)
        {
            var serviceTypeIds = checkedItems.Select(DefineGroupModel).ToList()
                .Select(groupModel => groupModel.ServiceTypeId).ToList();
            return GetServiceTypesCategory(serviceTypeIds);
        }

        private ServiceTypesBillingSet GetServiceTypesCategory(List<int?> serviceTypeIds)
        {
            ServiceTypesBillingSet billingSet = ServiceTypesBillingSet.Unset;
            serviceTypeIds.ForEach(serviceTypeId =>
            {
                if (IsScreening(serviceTypeId))
                {
                    if (billingSet == ServiceTypesBillingSet.Unset)
                    {
                        billingSet = ServiceTypesBillingSet.Screening;
                    }
                    else if (billingSet == ServiceTypesBillingSet.LabServices)
                    {
                        billingSet = ServiceTypesBillingSet.Mixed;
                    }
                }
                else
                {
                    if (billingSet == ServiceTypesBillingSet.Unset)
                    {
                        billingSet = ServiceTypesBillingSet.LabServices;
                    }
                    else if (billingSet == ServiceTypesBillingSet.Screening)
                    {
                        billingSet = ServiceTypesBillingSet.Mixed;
                    }
                }
            });
            return billingSet;
        }

        protected void AdditionalServicesDataGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] =
                    Page.ClientScript.GetPostBackClientHyperlink(AdditionalServicesDataGrid,
                        "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = @"Click to select this row.";
            }
        }

        protected void AdditionalServicesDataGrid_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            AdditionalServicesDataGrid_OnSelectedIndexChanged();
        }

        private void AdditionalServicesDataGrid_OnSelectedIndexChanged()
        {
        }

        protected void MigratedItemsDataGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] =
                    Page.ClientScript.GetPostBackClientHyperlink(MigratedItemsDataGrid, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = @"Click to select this row. ";
            }
        }

        protected void MigratedItemsDataGrid_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            MigratedItemsDataGrid_OnSelectedIndexChanged();
        }

        protected void CurrentBatchItemsDataGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] =
                    Page.ClientScript.GetPostBackClientHyperlink(CurrentBatchItemsDataGrid, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = @"Click to select this row.";
            }
        }

        protected void CurrentBatchItemsDataGrid_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CurrentBatchItemsDataGrid_OnSelectedIndexChanged();
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Current Batch Items(data grid) selected row changed failed!" +
                              $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
        }

        private void CurrentBatchItemsDataGrid_OnSelectedIndexChanged()
        {
            var itemIndex = CurrentBatchItemsDataGrid.SelectedIndex;
            var fullItemCode = CurrentBatchItemsDataGrid.Rows[itemIndex].Cells[0].Text;

            var orderTreeViewNode = FindNodeByPrefix(fullItemCode);
            if (orderTreeViewNode == null)
                return;
            orderTreeViewNode.Select();
            OrdersTreeView_OnSelectedNodeChanged();

            this.OrdersTreeView.SelectedNode.Expand();
            ExpandAllAncestry(this.OrdersTreeView.SelectedNode);
            EnsureOrderTreeSelectionVisible();
        }

        private void MigratedItemsDataGrid_OnSelectedIndexChanged()
        {
            this.RedirectToNewItemButton.ToolTip = @"Open page for 'To New #' item of selected row";
            if (this.MigratedItemsDataGrid.SelectedIndex >= 0 &&
                this.MigratedItemsDataGrid.SelectedIndex < this.MigratedItemsDataGrid.Rows.Count)
            {
                this.RedirectToNewItemButton.Enabled = true;
                if (GetViewState(SessionConstants.MigratedItems) is List<MigratedItemModel> migratedItemModels)
                {
                    var selectedModel = migratedItemModels[this.MigratedItemsDataGrid.SelectedIndex];
                    this.RedirectToNewItemButton.ToolTip = $@"Redirect page to {selectedModel.NewItemNumber}";
                }
            }
            else
            {
                this.RedirectToNewItemButton.Enabled = false;
            }
        }


        protected void ItemMeasuresDataGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] =
                    Page.ClientScript.GetPostBackClientHyperlink(ItemMeasuresDataGrid, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = @"Click to select this row.";
            }
        }

        protected void ItemMeasuresDataGrid_OnSelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void ResetCalendarExtenders()
        {

            if (!DateTime.TryParseExact(BeginCreateDate.Text, CreateDateFormat,
                    CultureInfo.CreateSpecificCulture("en-US"),
                    DateTimeStyles.AdjustToUniversal,
                    out var beginCreateDate))
            {
                BeginCreateDate.Focus();
                return;
            }

            BeginCalendarExtender.SelectedDate = beginCreateDate;

            if (!DateTime.TryParseExact(EndCreateDate.Text, CreateDateFormat,
                    CultureInfo.CreateSpecificCulture("en-US"),
                    DateTimeStyles.AdjustToUniversal,
                    out var endCreateDate))
            {
                EndCreateDate.Focus();
                return;
            }

            EndCalendarExtender.SelectedDate = endCreateDate;

            EndCalendarExtender.StartDate = beginCreateDate;
            if (beginCreateDate > endCreateDate)
            {
                EndCalendarExtender.SelectedDate = beginCreateDate;
                EndCreateDate.Text = BeginCreateDate.Text;
            }

            var prevBeginDate = (DateTime?)GetViewState(SessionConstants.BeginDate);
            var prevEndDate = (DateTime?)GetViewState(SessionConstants.EndDate);
            if (prevBeginDate != BeginCalendarExtender.SelectedDate ||
                prevEndDate != EndCalendarExtender.SelectedDate)
            {
                LoadMemoTextBoxAndDropDownList();
            }

            SetViewState(BeginCalendarExtender.SelectedDate, SessionConstants.BeginDate);
            SetViewState(EndCalendarExtender.SelectedDate, SessionConstants.EndDate);

        }

        private void InitCreateDateCriteria()
        {
            BeginCalendarExtender.SelectedDate = DateTime.Now;
            EndCalendarExtender.SelectedDate = DateTime.Now;

            SetViewState(BeginCalendarExtender.SelectedDate, SessionConstants.BeginDate);
            SetViewState(EndCalendarExtender.SelectedDate, SessionConstants.EndDate);
        }

        private void LoadObjectStates()
        {
            var stateTargetModels = QueryCustomerUtils.GetStateTargets(this);
            SetViewState(stateTargetModels, SessionConstants.StateTargetModels);
        }

        private void LoadCustomers()
        {
            List<CustomerModel> customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            SetViewState(customers, SessionConstants.CustomersList);
            this.CustomerDropDownList.DataSource = customers;
            this.CustomerDropDownList.DataBind();
            this.CustomerDropDownList.SelectedIndex = 0;

        }

        private List<PriceSetModel> BulkBillingPriceSets
        {
            get
            {
                var bulkBillingPriceSets = (List<PriceSetModel>)GetViewState(SessionConstants.BulkBillingPrices);
                if (bulkBillingPriceSets == null)
                {
                    bulkBillingPriceSets = QueryAccountRep.GetBulkBillingPrices(this);
                    SetViewState(bulkBillingPriceSets, SessionConstants.BulkBillingPrices);
                }

                return bulkBillingPriceSets;
            }
        }

        #region Controls Event Hanlers


        protected void CustomerDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CustomerDropDownList.SelectedValue))
            {
                var message = @"You should select specific customer";
                PopupInfoDialog(message, isErr: true);
                return;
            }

            var customerModel = GetCustomerModel(this.CustomerDropDownList.SelectedIndex);
            if (customerModel == null)
            {
                throw new Exception("Internal error");
            }

            var customerCode = customerModel.CustomerCode;

            this.CustomerCodeTextBox.Text = customerCode;
            this.OrderCodeTextBox.Text = null;
            LoadMemoTextBoxAndDropDownList();
        }

        private void ShowCustomerNotFoundDialog(string customerCode)
        {
            PopupInfoDialog($"Customer {customerCode} not found", isErr: true);
        }

        protected void OnSearchByCustomerButtonClick(object sender, ImageClickEventArgs e)
        {

            if (!int.TryParse(this.CustomerCodeTextBox.Text, out var customerCode))
            {
                var message = $@"Input string({this.CustomerCodeTextBox.Text}) is not in correct format";
                if (string.IsNullOrEmpty(this.CustomerCodeTextBox.Text))
                {
                    message = @"Select customer from list or specify Customer Code";
                }

                PopupInfoDialog(message, isErr: true);
                return;
            }

            var memo = this.MemoDropDownList.SelectedIndex == 0 ? null : this.MemoDropDownList.SelectedValue;
            var po = this.PoDropDownList.SelectedIndex == 0 ? null : this.PoDropDownList.SelectedValue;

            var started = DateTime.Now;
            try
            {
                var resultDataSet = SearchByCustomer(
                    customerCode,
                    GetServiceTypeIds(),
                    BeginCalendarExtender.SelectedDate,
                    EndCalendarExtender.SelectedDate,
                    memo,
                    po);
                OrderTreeLoaded();

                AdjustCustomerDropDownList(this.CustomerCodeTextBox.Text);
                this.OrderCodeTextBox.Text = null;
                InitAdditionalServicesDataGrid();
                InitOrdersTreeCommands();
                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    InitMigratedItemsDataGrid(OrdersTreeView.Nodes.Cast<TreeNode>()
                        .Select(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).GroupCode).ToArray());
                    InitCurrentBatchItems();
                    InitMeasuresDataGrid();
                }
                else
                {
                    InitMigratedItemsDataGrid();
                    InitCurrentBatchItems();
                    InitMeasuresDataGrid();

                    var message = $@"Request result is empty! Criteria:";
                    var criteria = new List<string>
                    {
                        $"Customer Code = {this.CustomerCodeTextBox.Text}",
                        $"Customer Name = {this.CustomerDropDownList.SelectedItem.Text}",
                        $"GroupState   >= {AccountRep.GroupState}",
                        $"ServiceTypes  = {this.ServiceTypesServiceTypeCheckBoxList.SelectedSummary}",
                        $"Create Date   = {this.BeginCalendarExtender.SelectedDate:d}-{this.EndCalendarExtender.SelectedDate:d}",
                        $"Memo          = {memo ?? "<N/A>"}",
                        $"PO            = {po ?? "<N/A>"}"
                    };
                    LogUtils.UpdateSpLog(message, this.Session);
                    LogUtils.UpdateSpLog("\n" + string.Join(",\n", criteria), this.Session);

                    PopupInfoDialog(message, isErr: false, messageList: criteria);
                }
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Search by customer({this.CustomerCodeTextBox.Text}) is failed!" +
                              $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
            finally
            {
                AdjustOrderTreeStatusBar(started);
                AdjustOrdersTreeViewCheckedText();
            }

            LogUtils.UpdateSpLog("OnSearchByCustomerButtonClick(...) ended", this.Session);
        }

        private List<int> GetOrderIds()
        {
            return this.OrdersTreeView.Nodes.Cast<TreeNode>()
                .Select(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).GroupId).ToList();
        }

        private List<int> GetServiceTypeIds()
        {
            var serviceTypeIds = this.ServiceTypesServiceTypeCheckBoxList.SelectedItems
                .Select(listItem => int.Parse(listItem.Value)).ToList();
            if (serviceTypeIds.Count == 0)
            {
                serviceTypeIds =
                    this.ServiceTypesServiceTypeCheckBoxList.DefaultSelection
                        .Select(int.Parse).ToList();
            }

            return serviceTypeIds;
        }

        protected void OnSearchByOrderButtonClick(object sender, ImageClickEventArgs e)
        {
            ErrPrintedOrders.Text = "";
            OnSearchByOrderButtonClick();
        }

        private void OnSearchByOrderButtonClick()
        {
            var itemNumber = this.OrderCodeTextBox.Text.Replace(".", "");
            var oldItemNumber = this.OrderCodeTextBox.Text.Replace(".", "");
            if (!long.TryParse(itemNumber, out var _))
            {
                var message = string.IsNullOrEmpty(itemNumber)
                    ? $@"Field {OrderSearchFieldName} is empty"
                    : $@"Input string({this.OrderCodeTextBox.Text}) is not in correct format";
                PopupInfoDialog(message, isErr: true);
                return;
            }

            if (!Utils.DissectItemNumber(itemNumber, out int groupCode, out var batchCode, out var itemCode))
            {
                var message = $@"Cannot parse field '{OrderSearchFieldName}' = {this.OrderCodeTextBox.Text}";
                PopupInfoDialog(message, isErr: true);
                return;
            }
            if (batchCode != 0 && itemCode != 0)
            {
                RemeasParamModel matchItemNumber = QueryUtils.GetNewItemNumber(itemNumber, this);
                if (matchItemNumber == null)
                {
                    var message = $@"Cannot find valid item " + itemNumber;
                    PopupInfoDialog(message, isErr: true);
                    return;
                }
                Utils.DissectItemNumber(matchItemNumber.NewItemNumber, out int newGroupCode, out var newBatchCode, out var newItemCode);
                if (groupCode != newGroupCode || batchCode != newBatchCode || itemCode != newItemCode)
                {
                    itemNumber = matchItemNumber.NewItemNumber;
                    groupCode = newGroupCode;
                    batchCode = newBatchCode;
                    itemCode = newItemCode;
                }
            }
            var groupTable =
                QueryAccountRep.GetGroupsByCriteria(this.Session, null, null, null, new List<int> { groupCode }, null);
            if (groupTable.Rows.Count == 0)
            {
                //alex 12/10/2024
                var message = $@"Cannot find valid order " + itemNumber;
                //var message = $@"Request result is empty! Criteria:";
                //var criteria = new List<string>
                //{
                //    $"Customer Code = Any",
                //    $"Customer Name = Any",
                //    $"GroupState    = Any",
                //    $"Group Code    = {groupCode}",
                //    $"ServiceTypes  = Any",
                //    $"Create Date   = Any",
                //    $"Memo          = Any",
                //    $"PO            = Any"
                //};
                
                LogUtils.UpdateSpLog(message, this.Session);
                //LogUtils.UpdateSpLog("\n" + string.Join(",\n", criteria), this.Session);

                //PopupInfoDialog(message, isErr: true, messageList: criteria);
                PopupInfoDialog(message, isErr: true);
                //alex end
                return;
            }

            var customerCode = groupTable.Rows[0].ConvertToString("CustomerCode");
            var customerModel = GetCustomerModel(customerCode);
            if (customerModel == null)
            {
                LogUtils.UpdateSpLog(
                    $"Customer({customerCode}) for specified order({this.OrderCodeTextBox.Text}) cannot be found in customer list available for " +
                    $"the user{this.Session["ID"]} and office{this.Session["AuthorOfficeID"]}",
                    this.Session);
                var message = $@"Cannot found order by '{OrderSearchFieldName}' = '{this.OrderCodeTextBox.Text}'. " +
                              $@"Parsed='{Utils.FullItemNumberWithDots(groupCode, batchCode, itemCode)}'";
                PopupInfoDialog(message, isErr: true);
                return;
            }

            if (this.OrdersTreeView.Nodes.Count > 0)
            {
                var currentTreeViewGroups = this.OrdersTreeView.Nodes.Cast<TreeNode>()
                    .Select(GetTreeNodeModel<TreeNodeGroupModel>)
                    .Select(m => m.GroupCode)
                    .ToList();
                currentTreeViewGroups.Add(groupCode);
                this.OrderListDialogListTextBox.Text = string.Join("\n", currentTreeViewGroups);
                this.ExtendedOrderSearchButton.Text =
                    ExtendedOrderSearchButtonText + $@"({currentTreeViewGroups.Count})";
                SearchByGroupList(this.OrderListDialogListTextBox.Text);
                return;
            }

            SetViewState(groupCode, SessionConstants.GroupCode);
            SetViewState(batchCode, SessionConstants.BatchCode);
            SetViewState(itemCode, SessionConstants.ItemCode);

            if (Convert.ToInt32(groupTable.Rows[0][StateCodeColumnName]) == 1)
            {
                if (itemNumber == oldItemNumber)
                    PopupQuestionDialog(
                        $@"Requested Order({groupCode}) is closed. Would you like to open it temporary?",
                        LoadOrderFunctionTitle);
                else
                {
                    
                    long oldItem = Convert.ToInt64(itemNumber);
                    long newNumber = Convert.ToInt64(itemNumber);
                    PopupQuestionDialog(
                        $@"Requested Item(" + oldItemNumber + @") was moved to new item (" + itemNumber + @") and is closed. Would you like to open it temporary?",
                        LoadOrderFunctionTitle);
                }
                //$@"Requested Item({Convert.ToInt32(itemNumber)}) was moved to new item ({Convert.ToInt32(itemNumber)}) and is closed. Would you like to open it temporary?",
                //LoadOrderFunctionTitle);
                //when "Yes" - SearchByGroup() will be called
            }
            else if (itemNumber != oldItemNumber)
                PopupQuestionDialog(
                        $@"Requested Item(" + oldItemNumber + @") was moved to new item (" + itemNumber + @"). Would you like to open it temporary?",
                        LoadOrderFunctionTitle);
            //$@"Requested Item({Convert.ToInt32(itemNumber)}) was moved to new item ({Convert.ToInt32(itemNumber)}). Would you like to open it temporary?",
            //LoadOrderFunctionTitle);
            else
            {
                SearchByGroup();
            }

            LogUtils.UpdateSpLog("OnSearchByOrderButtonClick() ended", this.Session);
        }

        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            switch (this.QuestionTitle.InnerText)
            {
                case LoadOrderFunctionTitle:
                    SearchByGroup();
                    break;
                case EndSessionFunctionTitle:
                    EndSession();
                    break;
                case EndSessionIsFinishedFunctionTitle:
                    PrintLabel();
                    break;
            }
        }

        protected void EndSessionButton_OnCommand(object sender, CommandEventArgs e)
        {
            var commandText = ((Button)sender).Text;
            var checkedItems = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == ItemDepth).ToList();

            if (checkedItems.Count == 0)
            {
                PopupInfoDialog(
                    $"At least one item must be checked to try '{commandText}' command.", true);
            }
            else
            {
                PopupQuestionDialog(
                    $@"Are you going to try '{commandText}' command for the {checkedItems.Count} checked items?",
                    EndSessionFunctionTitle);
                //when "Yes" - EndSession() will be called
            }
        }

        protected void PrintLabelButton_OnCommand(object sender, CommandEventArgs e)
        {
            try
            {
                PrintLabel();
            }
            catch (Exception ex)
            {
                PopupInfoDialog($"\"{this.PrintLabelButton.Text}\" command failed. Exception: \"{ex.Message}\"", true);
                LogUtils.UpdateSpLogWithException(ex, this);
            }
        }

        protected void OrdersTreeView_OnSelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                OrdersTreeView_OnSelectedNodeChanged();
                this.CurrentBatchItemsDataGrid.SelectedIndex = -1;
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Order Tree selected node changed failed!" +
                              $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: true);
            }
        }

        private void OrdersTreeView_OnSelectedNodeChanged()
        {
            if (OrdersTreeView.SelectedNode == null)
            {
                return;
            }

            switch (OrdersTreeView.SelectedNode.Depth)
            {
                case OrderDepth:
                    var groupModel = GetTreeNodeModel<TreeNodeGroupModel>(OrdersTreeView.SelectedNode);
                    InitMeasuresDataGrid();
                    HideImages();
                    LoadCurrentBatchItems(groupModel.GroupCode);
                    break;
                case BatchDepth:
                    var batchModel = GetTreeNodeModel<TreeNodeBatchModel>(OrdersTreeView.SelectedNode);
                    InitMeasuresDataGrid();
                    HideImages();
                    LoadCurrentBatchItems(batchModel.GroupCode, batchModel.BatchCode);
                    break;
                case ItemDepth:
                    var itemModel = GetTreeNodeModel<TreeNodeItemModel>(OrdersTreeView.SelectedNode);
                    LoadItemMeasures();
                    var batchModel1 = GetTreeNodeModel<TreeNodeBatchModel>(OrdersTreeView.SelectedNode.Parent);
                    ShowImages(batchModel1.Path2Picture, itemModel.Shape, itemModel.Path2Drawing);
                    break;
                case ItemDocDepth:
                    HideImages();
                    InitCurrentBatchItems();
                    InitMeasuresDataGrid();
                    break;
                default:
                    LogUtils.UpdateSpLogWithException(
                        $"Undefined OrdersTreeView.SelectedNode.Depth={OrdersTreeView.SelectedNode.Depth}", this);
                    throw new Exception("Internal Error");
            }
        }

        private void ShowImages(string path2Picture, int pathCode, string path2Drawing)
        {
            ShowPictureImage(path2Picture);
            ShowShapeImage(pathCode, path2Drawing);
        }

        private void ShowPictureImage(string path2Picture)
        {
            var imageUrl = path2Picture == null ? null : Utlities.GetImgPath(path2Picture, this);

            this.PicturePanel.Visible = true;
            this.PictureImage.ImageUrl = imageUrl;
            this.PictureImage.Style["opacity"] = "1";
            this.PictureImage.ToolTip = $@"Path2Drawing: {path2Picture}, Url: {imageUrl}";

            this.PictureLink.HRef = imageUrl;
            this.PictureLink.Visible = true;
        }

        private void ShowShapeImage(int shapeCode, string path2Drawing)
        {

            var imageUrl = path2Drawing == null ? null : Utlities.GetImgPath(path2Drawing, this);
            imageUrl = imageUrl?.Replace(@".wmf", @".png").Replace("shapes", @"shapespng");

            this.ShapePanel.Visible = true;
            this.ShapeImage.ImageUrl = imageUrl;
            this.ShapeImage.Style["opacity"] = "1";
            this.ShapeImage.ToolTip = $@"ShapeCode: {shapeCode}, Path2Drawing: {path2Drawing}, Url: {imageUrl}";

            this.ShapeLink.HRef = imageUrl;
            this.ShapeLink.Visible = true;

        }

        private void HideImages()
        {
            ChangeImagesVisibility(true);
        }

        private void ShowImages()
        {
            ChangeImagesVisibility(false);
        }

        private void ChangeImagesVisibility(bool visible)
        {
            this.PictureLink.Visible = visible;
            this.PicturePanel.Visible = visible;

            this.ShapeLink.Visible = visible;
            this.ShapePanel.Visible = visible;
        }

        private void LoadCurrentBatchItems(int groupCode, int? batchCode = null)
        {
            TreeNode groupNode = OrdersTreeView.Nodes.Cast<TreeNode>()
                .FirstOrDefault(treeNode =>
                    GetTreeNodeModel<TreeNodeGroupModel>(treeNode).GroupCode == groupCode);
            if (groupNode == null)
            {
                return;
            }

            List<CurrentBatchItemModel> itemModels = groupNode.ChildNodes.Cast<TreeNode>()
                .Where(treeNode => batchCode == null ||
                                   GetTreeNodeModel<TreeNodeBatchModel>(treeNode).BatchCode == batchCode)
                .SelectMany(batchNode => batchNode.ChildNodes.Cast<TreeNode>()).Select(itemNode =>
                    new CurrentBatchItemModel
                    {
                        FullItemNumber = ExtractFullItemCode(itemNode),
                        ToolTip = itemNode.Text
                    })
                .ToList();

            InitCurrentBatchItems(itemModels);
            this.CurrentBatchItemsDataGrid.SelectedIndex = -1;
        }

        private TreeNode FindNodeByPrefix(string fullItemCode)
        {
            TreeNode treeNode = null;
            for (var i = 0; i < this.OrdersTreeView.Nodes.Count && treeNode == null; i++)
            {
                var orderNode = this.OrdersTreeView.Nodes[i];
                for (var j = 0; j < orderNode.ChildNodes.Count && treeNode == null; j++)
                {
                    var batchNode = orderNode.ChildNodes[j];
                    for (var k = 0; k < batchNode.ChildNodes.Count && treeNode == null; k++)
                    {
                        var itemNode = batchNode.ChildNodes[k];
                        if (itemNode.Text.StartsWith(fullItemCode))
                        {
                            treeNode = itemNode;
                        }
                    }
                }
            }

            return treeNode;
        }

        private static string ExtractFullItemCode(TreeNode itemNode)
        {
            var firstBlankIndex = itemNode.Text.IndexOf(" ", StringComparison.Ordinal);
            if (firstBlankIndex > 0)
                return itemNode.Text.Substring(0, firstBlankIndex);
            else
                return itemNode.Text;
        }

        private void LoadItemMeasures()
        {
            var itemModel = GetTreeNodeModel<TreeNodeItemModel>(OrdersTreeView.SelectedNode);
            LoadItemMeasures(OrdersTreeView.SelectedNode.Text, itemModel, itemModel);
        }

        private T GetTreeNodeModel<T>(TreeNode treeNode)
        {
            return JsonConvert.DeserializeObject<T>(treeNode.Value);
        }

        private void LoadItemMeasures(string itemFullNumber, TreeNodeItemModel itemModel,
            TreeNodeItemModel treeNodeItemModel)
        {
            var started = DateTime.Now;
            var measures = QueryAccountRep.GetMeasureWithPrevious(
                    itemModel.GroupCode,
                    itemModel.BatchCode,
                    itemModel.ItemCode,
                    itemModel.PrevGroupCode,
                    itemModel.PrevBatchCode,
                    itemModel.PrevItemCode,
                    this)
                .OrderBy(measureModel => measureModel.PartId)
                .ThenBy(measureModel => measureModel.MeasureGroupId)
                .ThenBy(measureModel => measureModel.MeasureName)
                .ToList();

            AdjustMeasuresDataGridStatusBar(started, measures);

            InitMeasuresDataGrid(GetItemMeasureCaption(treeNodeItemModel), itemFullNumber, measures,
                emptyDataText: "No data found for the item");
        }

        private static string GetItemMeasureCaption(TreeNodeItemModel itemModel)
        {
            var itemNumber =
                Utils.FullItemNumberWithDots(
                    Convert.ToInt32(itemModel.GroupCode),
                    Convert.ToInt32(itemModel.BatchCode),
                    Convert.ToInt32(itemModel.ItemCode));

            var prevItemNumber = "";
            if (itemModel.PrevGroupCode != 0 && itemModel.PrevBatchCode != 0 &&
                itemModel.PrevItemCode != 0)
            {
                prevItemNumber =
                    Utils.FullItemNumberWithDots(
                        Convert.ToInt32(itemModel.PrevGroupCode),
                        Convert.ToInt32(itemModel.PrevBatchCode),
                        Convert.ToInt32(itemModel.PrevItemCode));
            }

            var captionIdent = itemNumber == prevItemNumber || prevItemNumber == ""
                ? itemNumber
                : itemNumber + " / " + prevItemNumber;
            var caption = "Item # " + captionIdent + ", " +
                          "Lot # " + (string.IsNullOrEmpty(itemModel.LotNumber?.Trim())
                              ? "N/A"
                              : itemModel.LotNumber?.Trim());
            return caption;
        }

        private void InitAdditionalServicesDataGrid(
            List<AdditionalServiceRow> additionalServicesRows = null,
            string emptyDataText = "No data requested yet")
        {
            this.AdditionalServicesDataGrid.EmptyDataText = emptyDataText;

            if (additionalServicesRows != null && additionalServicesRows.Count > 0)
            {
                this.AdditionalServicesDataGrid.SelectedIndex = 0;
                this.AdditionalServiceDataGridStatusBar.Text = $@"Rows: {additionalServicesRows.Count}";
                this.AdditionalServiceClearListButton.Enabled = true;
                this.AdditionalServiceRefreshListButton.Enabled = true;
            }
            else
            {
                this.AdditionalServiceDataGridStatusBar.Text = "";
                this.AdditionalServiceClearListButton.Enabled = false;
                this.AdditionalServiceRefreshListButton.Enabled = OrdersTreeView.CheckedNodes.Count > 0;
            }

            var dataSource = additionalServicesRows ?? new List<AdditionalServiceRow>();
            SetViewState(dataSource, SessionConstants.AdditionalServicesRows);
            AdditionalServicesDataGrid.DataSource = dataSource;
            AdditionalServicesDataGrid.DataBind();

            AdditionalServicesDataGrid_OnSelectedIndexChanged();
        }

        private void InitMigratedItemsDataGrid(params int[] groupCodes)
        {
            if (SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Screening ||
                SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Unset)
            {
                return;
            }

            var migratedItemModels = QueryAccountRep.GetMigratedItems(groupCodes, this);

            this.SetViewState(migratedItemModels, SessionConstants.MigratedItems);

            var ordersText = (groupCodes.Length == 1
                ? groupCodes[0] + " order"
                : groupCodes.Length + " orders");
            InitMigratedItemsDataGrid(
                migratedItemModels,
                "No migrated items found for " +
                ordersText,
                $@"Migrated Items ({ordersText})");
        }

        private void InitMigratedItemsDataGrid(
            List<MigratedItemModel> migratedItemModels = null,
            string emptyDataText = "No data requested yet",
            string caption = "Migrated Items")
        {

            this.MigratedItemsDataGrid.EmptyDataText = emptyDataText;
            this.MigratedItemDataGridCaption.Text = caption;

            if (migratedItemModels != null && migratedItemModels.Count > 0)
            {
                this.MigratedItemsDataGrid.SelectedIndex = 0;
                this.MigratedItemDataGridStatusBar.Text = $@"Rows: {migratedItemModels.Count}";
            }
            else
            {
                this.MigratedItemDataGridStatusBar.Text = "";
            }

            this.MigratedItemsDataGrid.DataSource = migratedItemModels ?? new List<MigratedItemModel>();
            this.MigratedItemsDataGrid.DataBind();

            MigratedItemsDataGrid_OnSelectedIndexChanged();
        }

        protected void RedirectToNewItemButton_OnCommand(object sender, CommandEventArgs e)
        {
            var migratedItemModels = this.GetViewState(SessionConstants.MigratedItems) as List<MigratedItemModel>;
            if (migratedItemModels == null)
            {
                return;
            }

            var selectedIndex = this.MigratedItemsDataGrid.SelectedIndex;
            if (selectedIndex < 0)
            {
                return;
            }

            var migratedItemModel = migratedItemModels[selectedIndex];
            this.OrderCodeTextBox.Text = migratedItemModel.NewItemNumber;
            OnSearchByOrderButtonClick();
        }

        private void InitCurrentBatchItems(
            List<CurrentBatchItemModel> itemModels = null,
            string emptyDataText = "No data requested yet")
        {
            this.CurrentBatchItemsDataGrid.DataSource = itemModels ?? new List<CurrentBatchItemModel>();
            this.CurrentBatchItemsDataGrid.EmptyDataText = emptyDataText;
            this.CurrentBatchItemsDataGrid.DataBind();
        }

        private void InitMeasuresDataGrid(
            string text = @"Item Details",
            string toolTip = @"Empty",
            List<MeasureWithPreviousModel> measures = null,
            string emptyDataText = "No data requested yet (select 'Item' on Order Tree)")
        {
            ItemMeasuresDataGrid.DataSource = measures ?? new List<MeasureWithPreviousModel>();
            MeasuresDataGridCaption.Text = text;
            MeasuresDataGridCaption.ToolTip = toolTip;
            ItemMeasuresDataGrid.EmptyDataText = emptyDataText;
            if (measures == null || measures.Count == 0)
            {
                MeasureDataGridStatusBar.Text = "";
            }

            ItemMeasuresDataGrid.DataBind();
        }

        protected void ExpandButtonClick(object sender, EventArgs e)
        {
            this.OrdersTreeView.ExpandAll();
        }

        protected void CollapseButtonClick(object sender, EventArgs e)
        {
            this.OrdersTreeView.CollapseAll();
        }

        #endregion

        private void AdjustCustomerDropDownList(string customerCode)
        {
            if (customerCode == null)
            {
                this.CustomerDropDownList.SelectedIndex = 0;
                return;
            }

            var customerModels = (List<CustomerModel>)GetViewState(SessionConstants.CustomersList);
            var customerModel = customerModels.Find(model => model.CustomerCode == customerCode);
            this.CustomerDropDownList.SelectedIndex =
                customerModel == null ? 0 : customerModels.IndexOf(customerModel);
        }

        private DataSet SearchByCustomer(int customerCode, List<int> serviceTypes, DateTime? beginDate,
            DateTime? endDate, string memo, string po)
        {
            bool IsSelected(TreeViewLevels levelName, int orderCode, int batchCode, int itemCode, TreeNode treeNode)
            {
                return false;
            }

            ResetUIControls(ResetMode.ForCustomerSearch);
            var dataSet = QueryAccountRep.GetOrdersTreeByCustomerCode(customerCode,
                GroupState,
                this.Session,
                serviceTypes,
                beginDate,
                endDate,
                memo,
                po);

            RenameTables(dataSet);
            SetViewState(dataSet, SessionConstants.OrdersTreeViewDataSet);

            OrderTreeViewClear();
            FillTreeView(dataSet, OrderLevel, "1=1", OrdersTreeView.Nodes, IsSelected);
            return dataSet;
        }

        private static void RenameTables(DataSet dataSet)
        {
            if (dataSet.Tables.Count > 0)
            {
                dataSet.RenameTable(0, TreeViewLevels.Group.ToString());
            }

            if (dataSet.Tables.Count > 1)
            {
                dataSet.RenameTable(1, TreeViewLevels.Batch.ToString());
            }

            if (dataSet.Tables.Count > 2)
            {
                dataSet.RenameTable(2, TreeViewLevels.Item.ToString());
            }

            if (dataSet.Tables.Count > 3)
            {
                dataSet.RenameTable(3, TreeViewLevels.ItemDoc.ToString());
            }

            if (dataSet.Tables.Count > 4)
            {
                dataSet.RenameTable(4, MemoTableName);
            }

            if (dataSet.Tables.Count > 5)
            {
                dataSet.RenameTable(5, PoTableName);
            }
        }

        private void SearchByGroup()
        {
            var groupCode = (int)GetViewState(SessionConstants.GroupCode);
            var batchCode = (int)GetViewState(SessionConstants.BatchCode);
            var itemCode = (int)GetViewState(SessionConstants.ItemCode);

            this.OrderCodeTextBox.Text = Utils.FullItemNumberWithDots(groupCode, batchCode, itemCode);
            var started = DateTime.Now;
            try
            {
                LoadOrdersTreeViewByGroupCode(groupCode, batchCode, itemCode);
                OrderTreeLoaded();

                InitAdditionalServicesDataGrid();

                if (OrdersTreeView.Nodes.Count > 0)
                {
                    var groupModel = JsonConvert.DeserializeObject<TreeNodeGroupModel>(OrdersTreeView.Nodes[0].Value);
                    if (groupModel == null)
                    {
                        throw new Exception("Internal Error");
                    }

                    var customerModel = GetCustomerModel(groupModel.CustomerCode.ToString());
                    if (customerModel == null)
                    {
                        throw new Exception("Internal Error");
                    }

                    this.CustomerCodeTextBox.Text = customerModel.CustomerCode;
                    if (this.OrdersTreeView.SelectedNode != null)
                    {
                        OrdersTreeView_OnSelectedNodeChanged();
                        AdjustCustomerDropDownList(customerModel.CustomerCode);
                        this.OrdersTreeView.SelectedNode.Expand();
                        ExpandAllAncestry(this.OrdersTreeView.SelectedNode);
                        EnsureOrderTreeSelectionVisible();
                    }

                    AdjustCreateDates();
                    AdjustServiceType();

                    InitMigratedItemsDataGrid(groupCode);
                    LoadMemoTextBoxAndDropDownList(groupState: null);
                }
                else
                {
                    InitMigratedItemsDataGrid();
                    InitCurrentBatchItems();
                    InitMeasuresDataGrid();

                    var message = $@"Request result is empty! '{OrderSearchFieldName}' = {this.OrderCodeTextBox.Text}";
                    PopupInfoDialog(message, isErr: false);
                }
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Search by order({this.OrderCodeTextBox.Text}) is failed!" +
                              $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
            finally
            {
                AdjustOrderTreeStatusBar(started);
                AdjustOrdersTreeViewCheckedText();
                InitOrdersTreeCommands();
            }

            LogUtils.UpdateSpLog("SearchByGroup() ended", this.Session);

        }

        private void OrderTreeLoaded()
        {
            SetViewState(true, SessionConstants.NewOrderTreeLoaded);
        }

        private void AdjustServiceType()
        {
            var selection = this.OrdersTreeView.Nodes.Cast<TreeNode>()
                .Select(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).ServiceTypeId)
                .Distinct()
                .OrderBy(serviceTypeId => serviceTypeId ?? 0)
                .Select(serviceTypeId => serviceTypeId.ToString())
                .ToArray();

            this.ServiceTypesServiceTypeCheckBoxList.SetSelection(selection);
            this.ServiceTypesCategoryDropDown.SelectedIndex =
                GetServiceTypesCategoryIndexBySelectedServiceTypes(
                    selection.Select(s => (int?)int.Parse(s)).ToList(),
                    this.ServiceTypesServiceTypeCheckBoxList.Items.Count);
        }

        private void AdjustCreateDates()
        {
            var minCreatedDate = this.OrdersTreeView.Nodes.Cast<TreeNode>()
                .Min(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).CreateDate);
            var maxCreatedDate = this.OrdersTreeView.Nodes.Cast<TreeNode>()
                .Max(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).CreateDate);

            BeginCalendarExtender.SelectedDate = minCreatedDate;
            EndCalendarExtender.SelectedDate = maxCreatedDate;
        }

        private void EnsureOrderTreeSelectionVisible()
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(),
                "ShowOrderTreeView", "Tree_scrollIntoView_WithTimeout();", true);
        }

        private void LoadOrdersTreeViewByGroupCode(int requestedGroupCode, int requestedBatchCode,
            int requestedItemCode)
        {
            bool IsSelected(TreeViewLevels levelName, int orderCode, int batchCode, int itemCode, TreeNode treeNode)
            {
                var isOnlyGroupRequested = requestedBatchCode == 0;
                var isBatchCodeNotMatchRequested = batchCode != requestedBatchCode;

                if (isOnlyGroupRequested || isBatchCodeNotMatchRequested)
                {
                    return false;
                }

                var isBatchCodeMatchAndBatchLevel =
                    requestedItemCode == 0 && itemCode == 0 && levelName == TreeViewLevels.Batch;
                var isItemCodeMatchAndItemLevel = itemCode == requestedItemCode && levelName == TreeViewLevels.Item;

                return isBatchCodeMatchAndBatchLevel || isItemCodeMatchAndItemLevel;
            }

            ResetUIControls(ResetMode.ForFirstOrderSearch);
            LoadOrdersTreeViewByGroupCodes(new List<int> { requestedGroupCode }, IsSelected);
            this.OrderCodeTextBox.Focus();
        }

        private void LoadOrdersTreeViewByGroupCodes(List<int> groupCodes)
        {
            bool IsSelected(TreeViewLevels levelName, int orderCode, int batchCode, int itemCode, TreeNode treeNode)
            {
                return false;
            }

            ResetUIControls(ResetMode.ForOrderListSearch);
            LoadOrdersTreeViewByGroupCodes(groupCodes, IsSelected);
            this.OrderCodeTextBox.Focus();
        }

        private void LoadOrdersTreeViewByGroupCodes(List<int> groupCodes,
            Func<TreeViewLevels, int, int, int, TreeNode, bool> isSelected)
        {
            var started = DateTime.Now;

            var dataSet = QueryAccountRep.GetOrdersTreeByOrderCode(groupCodes, this.Session);

            RenameTables(dataSet);
            SetViewState(dataSet, SessionConstants.OrdersTreeViewDataSet);

            OrderTreeViewClear();
            FillTreeView(dataSet, OrderLevel, "1=1", OrdersTreeView.Nodes, isSelected);

            if (this.OrdersTreeView.SelectedNode == null)
            {
                if (OrdersTreeView.Nodes.Count > 0)
                {
                    OrdersTreeView.Nodes[0].Select();
                }
            }

            if (OrdersTreeView.SelectedNode != null)
            {
                OrdersTreeView.SelectedNode.Expand();
                ExpandAllAncestry(OrdersTreeView.SelectedNode);
            }

            AdjustOrderTreeStatusBar(started);
            AdjustOrdersTreeViewCheckedText();
            InitOrdersTreeCommands();
        }

        private void OrderTreeViewClear()
        {
            OrdersTreeView.Nodes.Clear();
            if (OrdersTreeView.SelectedNode != null)
            {
                OrdersTreeView.SelectedNode.Selected = false;
            }
        }

        private static void ExpandAllAncestry(TreeNode treeNode)
        {
            var parent = treeNode.Parent;
            if (parent == null)
            {
                return;
            }

            parent.Expand();
            ExpandAllAncestry(parent);
        }

        private void EndSession()
        {
            try
            {
                EndSessionInternal();
            }
            catch (Exception e)
            {
                LogUtils.UpdateSpLogWithException(e, this);
                PopupInfoDialog($"{this.EndSessionButton.Text} command failed. Exception = {e.Message}", true);
            }
        }

        private void EndSessionInternal()
        {
            var itemModels = GetCheckedItemModels().ToList();
            var itemAddedCount = 0;
            var itemDocTypes = new List<DocTypeNameByItemModel>();

            foreach (var itemModel in itemModels)
            {
                itemAddedCount += GetItemAddedCount(
                    QueryUtils.SetEndSession(
                        itemModel.NewBatchId, itemModel.NewItemCode, this));
                itemDocTypes.AddRange(
                    QueryAccountRep.GetDocTypeNameByItemCode(
                        itemModel.GroupCode, itemModel.BatchCode, itemModel.ItemCode, this).ToList());
            }

            SetBatchEvents(itemModels);
            if (itemDocTypes.Count == 0)//no reports attached
                PopupQuestionDialog(
                questionTitle: EndSessionIsFinishedFunctionTitle,
                firstParagraph:
                $"End session is finished for {itemModels.Count} items. {itemAddedCount} documents are ordered. Item rejected.",
                secondListParagraph: itemDocTypes.Select(model => $"{model.ItemNumber} : {model.Reports}"),
                thirdParagraph: "Do you want to print labels?");
            else
                PopupQuestionDialog(
                questionTitle: EndSessionIsFinishedFunctionTitle,
                firstParagraph:
                $"End session is finished for {itemModels.Count} items. {itemAddedCount} documents are ordered.",
                secondListParagraph: itemDocTypes.Select(model => $"{model.ItemNumber} : {model.Reports}"),
                thirdParagraph: "Do you want to print labels?");
        }

        private void SetBatchEvents(IEnumerable<TreeNodeItemModel> itemModels)
        {
            var batches = OrdersTreeView.GetNodes(BatchDepth)
                .Select(node => JsonConvert.DeserializeObject<TreeNodeBatchModel>(node.Value));

            var groupItemsByBatches = itemModels
                .GroupBy(model => model.NewBatchId)
                .Select(g => new GroupItemsByBatch(newBatchId: g.Key, count: g.Count()));

            batches.Join(groupItemsByBatches, b => b?.BatchId, g => g?.NewBatchId,
                    (b, g) => new BatchEventModel
                    {
                        BatchId = g.NewBatchId,
                        ItemsAffected = g.Count,
                        ItemsInBatch = b.ItemsQuantity,
                        GroupCode = b.GroupCode,
                        BatchCode = b.BatchCode
                    })
                .ToList()
                .ForEach(m =>
                    FrontUtils.SetBatchEvent(m.EventId, m.BatchId, m.FormId, m.ItemsAffected, m.ItemsInBatch,
                        this.Session));
        }

        private IEnumerable<TreeNodeItemModel> GetCheckedItemModels()
        {
            return GetItemCheckedTreeNodes()
                .Select(itemTreeNode => JsonConvert.DeserializeObject<TreeNodeItemModel>(itemTreeNode.Value));
        }

        private IEnumerable<TreeNode> GetItemCheckedTreeNodes()
        {
            return OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == ItemDepth);
        }

        private IEnumerable<TreeNode> GetOrderCheckedTreeNodes()
        {
            return OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == OrderDepth);
        }

        private void PrintLabel()
        {
            if (string.IsNullOrEmpty(this.Session["MachineName"] as string))
            {
                var errorMessage = "Machine Name is empty (should be set with client start)";
                PopupInfoDialog(errorMessage, true);
                return;
            }

            var checkedItemModels = GetCheckedItemModels().ToList();
            if (checkedItemModels.Count == 0)
            {
                PopupInfoDialog("No Checked Orders/Items in List", true);
                return;
            }

            var queryUtils = new GemoDream.Database.QueryUtils(
                (string)Session["MyIP_ConnectionString"],
                (int)Session["ID"],
                (int)Session["AuthorOfficeID"],
                new CorptLogger(Session));
            var xmlFormats = XmlUtils.LoadXmlFormats(this);
            var xmlLabels = XmlUtils.LoadXmlLabels(this);
            var accountRepData = new AccountRepData(queryUtils, xmlFormats, xmlLabels);

            var dataErrors = new StringBuilder();
            int sentCount = 0;
            checkedItemModels.ForEach(itemModel =>
            {
                var message = new PrintLabelMessage
                {
                    Title = Utils.FullItemNumberWithDots(
                        itemModel.GroupCode,
                        itemModel.BatchCode,
                        itemModel.ItemCode),
                    Application = "GDLight",
                    MachineName = Convert.ToString(Session["MachineName"]),
                    GroupCode = itemModel.GroupCode,
                    BatchCode = itemModel.BatchCode,
                    ItemCode = itemModel.ItemCode,
                    BatchId = itemModel.BatchId,
                    NewBatchId = itemModel.NewBatchId,
                    NewItemCode = itemModel.NewItemCode
                };
                ItemData itemData;
                try
                {
                    itemData = accountRepData.GetData(message);
                }
                catch (DataErrorException e)
                {
                    dataErrors.AppendLine("\n" + e.Message);
                    return;
                }

                message.ItemData = itemData;
                var serializedObject = JsonConvert.SerializeObject(message);
                var deserializeObject = JsonConvert.DeserializeObject<PrintLabelMessage>(serializedObject);
                Console.WriteLine(deserializeObject);
                var isOk = QueryUtils.SendMessageToStorage(serializedObject, this,
                    dir: QueryUtils.AccountRepQueueNamePrefix);
                if (!isOk)
                {
                    dataErrors.AppendLine($"# {message.Title.Replace(".", "")} cannot be sent. Internal Error.");
                }
                else
                {
                    sentCount++;
                }
            });

            var errorList = dataErrors.ToString().Length == 0
                ? new List<string>()
                : dataErrors.ToString().Split('\n')
                    .Where(s => !string.IsNullOrEmpty(s) && s != "\r").ToList();

            PopupInfoDialog($"{checkedItemModels.Count} items have been checked. " +
                            $"\nProcessed: {sentCount}. Errors: {errorList.Count}." +
                            $"{(errorList.Count > 0 ? ". \nMissing data:" : "")}",
                isErr: errorList.Count > 0,
                messageList: errorList);
        }

        #region Information Dialog

        private void PopupInfoDialog(string msg, bool isErr, IEnumerable<string> messageList = null, int width = 560)
        {
            MessageDiv.InnerText = msg;
            var informationType = isErr ? "Error" : "Information";
            InfoImage.ImageUrl = $"Images/ajaxImages/{informationType.ToLowerInvariant()}24.png";
            InfoPanelTitle.InnerText = informationType;
            messageList?.ToList().ForEach(item =>
            {
                var li = new HtmlGenericControl("li");
                li.InnerText = item;
                this.MessageList.Controls.Add(li);
            });
            this.InfoPanel.Width = Unit.Pixel(width);
            InfoPopupExtender.Show();
        }

        #endregion

        #region Question dialog On Save

        private void PopupQuestionDialog(
            string question, string questionTitle)
        {
            PopupQuestionDialog(questionTitle, question, null, null);
        }

        private void PopupQuestionDialog(
            string questionTitle, string firstParagraph, IEnumerable<string> secondListParagraph, string thirdParagraph)
        {
            this.QuestionTitle.InnerText = questionTitle;
            this.FirstParagraph.InnerText = firstParagraph;
            secondListParagraph?.ToList().ForEach(item =>
            {
                var li = new HtmlGenericControl("li");
                li.InnerText = item;
                this.SecondListParagraph.Controls.Add(li);
            });
            this.ThirdParagraph.InnerText = thirdParagraph;
            SaveQPopupExtender.Show();
        }

        private int GetItemAddedCount(string endSessionResult)
        {
            var strings = endSessionResult.Split(' ');
            int itemAddedCount = strings.Length > 0 ? Convert.ToInt32(strings[0]) : 0;
            return itemAddedCount;
        }

        #endregion


        private void AdjustMeasuresDataGridStatusBar(DateTime start, List<MeasureWithPreviousModel> itemValueModels)
        {
            var gradesCount = itemValueModels.Count;
            var duration = DateTime.Now.Subtract(start);
            MeasureDataGridStatusBar.Text =
                $@"Rows: {gradesCount} ({duration.Seconds}:{duration.Milliseconds})";
        }

        protected void AdditionalServiceClearListButton_OnCommand(object sender, CommandEventArgs e)
        {
            InitAdditionalServicesDataGrid();
        }

        protected void OrdersTreeView_OnTreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            OnTreeNodeCheckChanged(e.Node);
        }

        private void OnTreeNodeCheckChanged(TreeNode treeNode)
        {
            AdjustOrdersTreeViewCheckedText();
            AdjustAdditionalServicesDataGrid();
            InitOrdersTreeCommands();
        }

        private void AdjustOrdersTreeViewCheckedText()
        {
            OrdersTreeCounts(out var ordersCount, out var batchCount, out var itemCount, out var itemDocCount,
                treeNode => treeNode.Checked);
            OrdersTreeViewChecked.Text = $@"Checked: {ordersCount}/{batchCount}/{itemCount}/{itemDocCount}";
        }

        private TreeNodeBatchModel DefineBatchModel(TreeNode treeNode)
        {
            switch (treeNode.Depth)
            {
                case OrderDepth:
                    return null;
                case BatchDepth:
                    return GetTreeNodeModel<TreeNodeBatchModel>(treeNode);
                case ItemDepth:
                    return GetTreeNodeModel<TreeNodeBatchModel>(treeNode.Parent);
                case ItemDocDepth:
                    return GetTreeNodeModel<TreeNodeBatchModel>(treeNode.Parent?.Parent);
                default:
                    LogUtils.UpdateSpLogWithException(
                        $"Undefined TreeNode.Depth={treeNode.Depth}", this);
                    throw new Exception("Internal Error");
            }
        }

        private TreeNodeGroupModel DefineGroupModel(TreeNode treeNode)
        {
            switch (treeNode.Depth)
            {
                case OrderDepth:
                    return GetTreeNodeModel<TreeNodeGroupModel>(treeNode);
                case BatchDepth:
                    return GetTreeNodeModel<TreeNodeGroupModel>(treeNode.Parent);
                case ItemDepth:
                    return GetTreeNodeModel<TreeNodeGroupModel>(treeNode.Parent?.Parent);
                case ItemDocDepth:
                    return GetTreeNodeModel<TreeNodeGroupModel>(treeNode.Parent?.Parent?.Parent);
                default:
                    LogUtils.UpdateSpLogWithException(
                        $"Undefined TreeNode.Depth={treeNode.Depth}", this);
                    throw new Exception("Internal Error");
            }
        }

        private void AdjustAdditionalServicesDataGrid()
        {
            var checkedBatches = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Select(DefineBatchModel)
                .Where(batchModel => batchModel != null)
                .Distinct(new TreeNodeBatchModelComparer())
                .ToList();

            var groupCodes = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Select(DefineGroupModel)
                .Where(groupModel => groupModel != null)
                .Select(groupModel => groupModel.GroupCode)
                .Distinct()
                .ToList();

            var additionalServices = (List<AdditionalServiceModel>)
                GetViewState(SessionConstants.AdditionalServicesModels);
            var additionalServicesOrders = (List<int>)
                GetViewState(SessionConstants.AdditionalServicesOrders);

            var areAllOrdersCached = false;
            if (additionalServicesOrders != null)
            {
                areAllOrdersCached = groupCodes.All(groupCode =>
                    additionalServicesOrders.Exists(cachedGroupCode => cachedGroupCode == groupCode));
            }

            if (!areAllOrdersCached)
            {
                additionalServices = QueryAccountRep.GetAdditionalServicesByOrderList(groupCodes, this);
                SetViewState(additionalServices, SessionConstants.AdditionalServicesModels);
                SetViewState(groupCodes, SessionConstants.AdditionalServicesOrders);
            }

            var additionalServicesRows = BillingUtils.GetAdditionalServicesRows(additionalServices, checkedBatches);

            InitAdditionalServicesDataGrid(
                additionalServicesRows,
                checkedBatches.Count > 0
                    ? "No data found for checked items"
                    : "No checked items in Orders Tree");
        }

        protected void AdditionalServiceRefreshListButton_OnCommand(object sender, CommandEventArgs e)
        {
            AdjustAdditionalServicesDataGrid();
        }

        protected void AdditionalServicesCheckedCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            var gridViewRow = (GridViewRow)checkBox.Parent.Parent;
            var rowIndex = gridViewRow.RowIndex;
            var additionalServicesRows = (List<AdditionalServiceRow>)
                GetViewState(SessionConstants.AdditionalServicesRows);
            additionalServicesRows[rowIndex].Checked = checkBox.Checked;
            SetViewState(additionalServicesRows, SessionConstants.AdditionalServicesRows);
        }

        protected void BillingButton_OnCommand(object sender, CommandEventArgs e)
        {
            if (SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Mixed)
            {
                PopupInfoDialog(
                    $@"Selected Items have both 'Screening' and 'Lab Services' service types. To do 'Billing' you should select items only one service types category.",
                    isErr: true);
                return;
            }

            List<TreeNodeGroupModel> groupModels = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Select(DefineGroupModel)
                .Where(model => model != null)
                .GroupBy(model => model.GroupCode)
                .Select(grouping => grouping.ToList()[0])
                .ToList();

            if (groupModels.Count == 0)
            {
                PopupInfoDialog($@"Please check some item nodes.", isErr: true);
                return;
            }

            SetViewState(groupModels, SessionConstants.BillingCurrentGroupModels);

            try
            {
                if (SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Screening && IsBulkBilling)
                {
                    PopupBulkBillingPriceSettingsDialog();
                }
                else
                {
                    var additionalServiceRows = (List<AdditionalServiceRow>)
                        GetViewState(SessionConstants.AdditionalServicesRows);
                    BillingUtils.SaveAdditionalServices(additionalServiceRows, this.Session);

                    var billingCurrentGroupModels = (List<TreeNodeGroupModel>)
                        GetViewState(SessionConstants.BillingCurrentGroupModels);
                    InvoiceGo(billingCurrentGroupModels, this.Session);
                }
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Billing is failed!\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
        }
        protected void UpdateOrderButton_OnCommand(object sender, CommandEventArgs e)
        {
            List<TreeNodeGroupModel> groupModels = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Select(DefineGroupModel)
                .Where(model => model != null)
                .GroupBy(model => model.GroupCode)
                .Select(grouping => grouping.ToList()[0])
                .ToList();

            if (groupModels.Count == 0)
            {
                PopupInfoDialog($@"Please check some item nodes.", isErr: true);
                return;
            }
            Session["TreeNode"] = OrdersTreeView.CheckedNodes;
            var orderCode = groupModels[0].GroupCode;
            
            if (orderCode != 0)
            {
                string url = "UpdateOrder.aspx?oc=" + orderCode;
                string fullURL = "window.open('" + url + "', '_blank' );";
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
            }
            
        }


        public void InvoiceGo(List<TreeNodeGroupModel> groupModels, HttpSessionState session)
        {
            if (groupModels.Count == 0)
            {
                return;
            }

            var groupModel = groupModels[0];
            var accountType =
                QueryAccountRep.GetAccountType(groupModel.GroupCode, groupModel.CustomerId, groupModel.CustomerOfficeId,
                    session);

            PopupBillingOptionsDialog(accountType, groupModel.ServiceTypeId);
            // if OK -> BillingOptionsOkButton_OnClick -> InvoiceGo(.., .., ...) 
        }

        private void PopupBillingOptionsDialog(int accountType, int? serviceTypeId)
        {
            this.RegularCheckBox.Checked = true;
            this.AddSKUCheckBox.Checked = false;
            this.AddLotNoCheckBox.Checked = false;

            SetInvoiceTargetButtons(accountType);

            this.BillingTypeDiv.Visible = true;
            this.BulkBillingPricesPanel.Visible = false;
            this.BillingOptionsDialog.Width = Unit.Pixel(750);
            this.AllOrdersToOneInvoiceRadioButton.Checked = false;
            this.EachOrderToSeparateInvoiceRadioButton.Checked = true;
            BillingOptionsModalPopupExtender.Show();
            // OK -> BillingOptionsOkButton_OnClick
        }

        private void SetInvoiceTargetButtons(int accountType)
        {
            this.QBPrimaryRadioButton.Text = @"QB Primary";
            this.QBCorptRadioButton.Text = @"QB corpt";
            this.TallyRadioButton.Text = @"Tally";

            this.QBPrimaryRadioButton.Checked = false;
            this.QBCorptRadioButton.Checked = false;
            this.TallyRadioButton.Checked = false;
            switch (accountType)
            {
                case 1:
                    this.QBPrimaryRadioButton.Checked = true;
                    this.QBPrimaryRadioButton.Text += @"(Default)";
                    break;
                case 2:
                    this.QBCorptRadioButton.Checked = false;
                    this.QBCorptRadioButton.Text += @"(Default)";
                    break;
                case 3:
                    this.TallyRadioButton.Checked = false;
                    this.TallyRadioButton.Text += @"(Default)";
                    break;
                default:
                    LogUtils.UpdateSpLog($@"Billing Options. Unexpected Account Type={accountType}.", this.Session);
                    break;
            }
        }

        protected void BillingOptionsOkButton_OnClick(object sender, EventArgs e)
        {
            try
            {

                if (SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.Screening)
                {
                    if (IsBulkBilling)
                    {
                        DoBulkBilling();
                    }
                    else
                    {
                        Billing();
                    }
                }
                else if (SelectedItemsServiceTypesBillingSet == ServiceTypesBillingSet.LabServices)
                {
                    Billing();
                }
                else
                {
                    throw new Exception(
                        $"Cannot do billing when Selected Items Service Types Category is \"{SelectedItemsServiceTypesBillingSet.ToString()}\"");
                }
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Billing is failed! Exception={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
        }

        private void Billing()
        {
            var groupModels = (List<TreeNodeGroupModel>)GetViewState(SessionConstants.BillingCurrentGroupModels);

            if (this.QBPrimaryRadioButton.Checked)
            {
                InvoiceGo(1, "QB primary", groupModels);
            }
            else if (this.QBCorptRadioButton.Checked)
            {
                InvoiceGo(2, "QB corpt", groupModels);
            }
            else if (this.TallyRadioButton.Checked)
            {
                InvoiceGo(3, "Tally", groupModels);
            }
            else
            {
                PopupInfoDialog("It is need to select one of the 3 choices: " +
                                $@"{QBPrimaryRadioButton.Text}, {QBCorptRadioButton.Text} or {TallyRadioButton.Text}.",
                    true);
            }
        }

        public void InvoiceGo(int billToAccount, string addressToBill, List<TreeNodeGroupModel> groupModels)
        {
            var items4Invoice = new List<Item4Invoice>();
            if (this.AllOrdersToOneInvoiceRadioButton.Checked)
            {
                var customerModel = GetCustomerModel(this.CustomerCodeTextBox.Text);
                var invoiceId =
                    QueryAccountRep.AddInvoice2(int.Parse(customerModel.CustomerId), int.Parse(customerModel.OfficeId),
                        this.Session);
                foreach (var groupModel in groupModels)
                {
                    items4Invoice.AddRange(GetItems4Invoice(billToAccount, invoiceId, groupModel));
                }
            }
            else if (this.EachOrderToSeparateInvoiceRadioButton.Checked)
            {
                foreach (var groupModel in groupModels)
                {
                    var invoiceId =
                        QueryAccountRep.AddInvoice2(groupModel.CustomerId, groupModel.CustomerOfficeId, this.Session);
                    items4Invoice.AddRange(GetItems4Invoice(billToAccount, invoiceId, groupModel));
                }
            }
            else
            {
                throw new Exception("Internal logical error");
            }

            var groupCodes = groupModels
                .Select(model => model.GroupCode.ToString())
                .Aggregate((summary, groupCode) => summary + ", " + groupCode);
            var message =
                $"Order(s): {groupCodes}, billed to {addressToBill}";
            var invoicesList = items4Invoice
                .Select(item4Invoice => $"{item4Invoice.FullBatch}: {item4Invoice.Result}").ToList();

            LogUtils.UpdateSpLog(message, this.Session);
            invoicesList.ForEach(line =>
                LogUtils.UpdateSpLog("\t" + line, this.Session));

            PopupInfoDialog(message, isErr: false, messageList: invoicesList);

        }

        private List<Item4Invoice> GetItems4Invoice(int billToAccount, int invoiceId, TreeNodeGroupModel groupModel)
        {
            var batchShortModels = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Select(DefineBatchModel)
                .Where(batchModel => batchModel != null && groupModel.GroupCode == batchModel.GroupCode)
                .GroupBy(batchModel => batchModel.BatchId)
                .Select(grouping => grouping.ToList()[0])
                .Cast<BatchShortModel>()
                .ToList();
            var items4Invoice = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == ItemDepth)
                .Select(GetTreeNodeModel<TreeNodeItemModel>)
                .Where(itemModel => groupModel.GroupCode == itemModel.GroupCode &&
                                    (groupModel.QCLab != "Y" || itemModel.BatchCode > 1))
                .Select(itemModel => new Item4Invoice(itemModel))
                .ToList();
            return BillingUtils.InvoiceGo(
                invoiceId, groupModel.CustomerId, groupModel.CustomerOfficeId, billToAccount,
                this.AddSKUCheckBox.Checked, this.AddLotNoCheckBox.Checked, batchShortModels, items4Invoice,
                this.Session,
                groupModel.QCLab == "Y" ? 1 : (int?)null);
        }

        private void AdjustOrderTreeStatusBar(DateTime start)
        {
            OrdersTreeCounts(out var ordersCount, out var batchCount, out var itemCount, out var itemDocCount);
            var duration = DateTime.Now.Subtract(start);
            OrdersTreeViewStatusBar.Text =
                $@"Nodes: {ordersCount}/{batchCount}/{itemCount}/{itemDocCount} ({duration.Seconds}:{duration.Milliseconds})";
            var treeViewTotalNodesLimit = GetTreeViewTotalNodesLimit();
            var realTotalNodesCount = ordersCount + batchCount + itemCount + itemDocCount;
            if (realTotalNodesCount >= treeViewTotalNodesLimit)
            {
                OrdersTreeViewStatusBar.ForeColor = Color.Red;
                var queryTotalNodesCount = GetQueryTotalNodesCount();
                OrdersTreeViewStatusBar.ToolTip =
                    $@"Maximum total nodes count exceeded. Only first {realTotalNodesCount} (from {queryTotalNodesCount}) is shown. Change query criteria to show rest";
            }
            else
            {
                OrdersTreeViewStatusBar.ForeColor = Color.Black;
                OrdersTreeViewStatusBar.ToolTip = @"{orders-Count}/{batch-Count}/{item-Count}/{itemDoc-Count}";
            }
        }

        private int GetQueryTotalNodesCount()
        {
            var dataSet = (DataSet)GetViewState(SessionConstants.OrdersTreeViewDataSet);
            var queryTotalNodesCount = 0;
            foreach (DataTable dataSetTable in dataSet.Tables)
            {
                queryTotalNodesCount += dataSetTable.Rows.Count;
            }

            return queryTotalNodesCount;
        }

        private int GetTreeViewTotalNodesLimit()
        {
            return this.Session[SessionConstants.TreeViewTotalNodesLimit] == null
                ? TreeViewTotalNodesLimitDefault
                : (int)this.Session[SessionConstants.TreeViewTotalNodesLimit];
        }

        private void OrdersTreeCounts(out int ordersCount, out int batchCount, out int itemCount, out int itemDocCount,
            Func<TreeNode, bool> condition = null)
        {
            if (condition == null)
            {
                condition = treeNode => true;
            }

            ordersCount = 0;
            batchCount = 0;
            itemCount = 0;
            itemDocCount = 0;

            foreach (TreeNode orderNode in this.OrdersTreeView.Nodes)
            {
                if (condition(orderNode))
                    ordersCount++;
                foreach (TreeNode batchNode in orderNode.ChildNodes)
                {
                    if (condition(batchNode))
                        batchCount++;
                    foreach (TreeNode itemNode in batchNode.ChildNodes)
                    {
                        if (condition(itemNode))
                            itemCount++;
                        foreach (TreeNode itemDocNode in itemNode.ChildNodes)
                        {
                            if (condition(itemDocNode))
                                itemDocCount++;
                        }
                    }
                }
            }
        }

        private void SetPageState()
        {
            this.ServiceTypesServiceTypeCheckBoxList.SetSelectionByServiceTypeCategory(
                (ServiceTypesCategory)Enum.Parse(typeof(ServiceTypesCategory),
                    this.ServiceTypesCategoryDropDown.SelectedItem.Value));
            SetOrderSearchMode();
        }

        [SuppressMessage("ReSharper", "LocalizableElement")]
        public List<ListItem> GetServiceTypes()
        {
            return QueryAccountRep.GetServiceTypes(this.Session);
            //return new List<ListItem>
            //{
            //    //new ListItem { Value = "0" , Text = "Any" },
            //    new ListItem { Value = "1" , Text = "24 hours" },           //style = "color: lightgray !important;" />
            //    new ListItem { Value = "2" , Text = "48 hours" },           //style = "color: lightgray !important;" />
            //    new ListItem { Value = "3" , Text = "72 hours" },           //style = "color: lightgray !important;" />
            //    new ListItem { Value = "4" , Text = "5 business days" },    //style = "color: lightgray !important;" />
            //    new ListItem { Value = "5" , Text = "7 business days" },    //style = "color: lightgray !important;" />
            //    new ListItem { Value = "6" , Text = "10 business days" },   //style = "color: lightgray !important;" />
            //    new ListItem { Value = "7" , Text = "Screening" },          //style = "color: black !important;" />
            //    new ListItem { Value = "8" , Text = "Sorting" },            //style = "color: black !important;" />
            //    new ListItem { Value = "9" , Text = "Video 360" },          //style = "color: black !important;" />
            //    new ListItem { Value = "10", Text = "Reprint" },            //style = "color: lightgray !important;" />
            //    new ListItem { Value = "11", Text = "Appraisal" },          //style = "color: black !important;" />
            //    new ListItem { Value = "12", Text = "QC" },                 //style = "color: black !important;" />
            //    new ListItem { Value = "13", Text = "QC/Screening" }        //style = "color: black !important;" />

            //};
        }

        protected void MemoTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        protected void POTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        protected void MemoDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.MemoDropDownList.SelectedValue == "Any")
            {
                this.MemoTextBox.Text = null;
                return;
            }

            this.MemoTextBox.Text = this.MemoDropDownList.SelectedValue;
        }

        protected void PoDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PoDropDownList.SelectedValue == "Any")
            {
                this.POTextBox.Text = null;
                return;
            }

            this.POTextBox.Text = this.PoDropDownList.SelectedValue;
        }

        private void FillMemoDropDownList(DataSet dataSet)
        {
            FillMemoPoDropDownList(dataSet, MemoTableName, this.MemoDropDownList);
            FillMemoPoDropDownList(dataSet, PoTableName, this.PoDropDownList);
        }

        private static void FillMemoPoDropDownList(DataSet dataSet, string tableName, DropDownList dropDownList)
        {
            if (!dataSet.Tables.Contains(tableName))
            {
                throw new Exception($"DataSet does not contain table={tableName}");
            }

            string prevValue = null;
            if (dropDownList.Items.Count > 0 && dropDownList.SelectedIndex > 1)
            {
                prevValue = dropDownList.SelectedItem.Value;
            }

            dropDownList.DataSource = dataSet.Tables[tableName];
            dropDownList.DataBind();

            if (prevValue != null)
            {
                var item = dropDownList.Items.FindByValue(prevValue);
                if (item != null)
                {
                    item.Selected = true;
                }
            }

            dropDownList.Enabled = true;
        }

        private void LoadMemoTextBoxAndDropDownList(int? groupState = GroupState)
        {
            if (!int.TryParse(this.CustomerCodeTextBox.Text, out var customerCode))
            {
                return;
            }

            try
            {
                var dataSet = QueryAccountRep.GetMemoAndPoByCustomer(
                    this.Session,
                    customerCode,
                    groupState,
                    null,
                    GetServiceTypeIds(),
                    BeginCalendarExtender.SelectedDate,
                    EndCalendarExtender.SelectedDate);
                dataSet.RenameTable(0, MemoTableName);
                dataSet.RenameTable(1, PoTableName);
                FillMemoDropDownList(dataSet);
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message =
                    $@"QueryCustomerUtils.GetMemoAndPoByCustomer({customerCode}, {GroupState}, ...) is failed!" +
                    $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
        }

        protected void CheckAllTreeNodesButton_OnClick(object sender, ImageClickEventArgs e)
        {
            CheckUncheckAllNodes(true);
        }

        protected void UnCheckAllTreeNodesButton_OnClick(object sender, ImageClickEventArgs e)
        {
            CheckUncheckAllNodes(false);
        }

        private void CheckUncheckAllNodes(bool check)
        {
            SetChecked(this.OrdersTreeView.Nodes, check);
            OrdersTreeView.Nodes.Cast<TreeNode>()
                .SelectMany(treeNode => treeNode.ChildNodes.Cast<TreeNode>())
                .ToList().ForEach(OnTreeNodeCheckChanged);
        }

        private static void SetChecked(TreeNodeCollection nodes, bool check)
        {
            nodes.Cast<TreeNode>().ToList()
                .ForEach(treeNode =>
                {
                    treeNode.Checked = check;
                    if (treeNode.ChildNodes.Count > 0)
                    {
                        SetChecked(treeNode.ChildNodes, check);
                    }
                });
        }

        protected void CustomerCodeTextBox_OnTextChanged(object sender, EventArgs e)
        {
            var customerModel = GetCustomerModel(this.CustomerCodeTextBox.Text.Trim());
            if (customerModel == null)
            {
                ShowCustomerNotFoundDialog(this.CustomerCodeTextBox.Text.Trim());
                return;
            }

            AdjustCustomerDropDownList(customerModel.CustomerCode);
            LoadMemoTextBoxAndDropDownList();
        }

        protected void ServiceTypesCheckBoxList_OnSelectedIndexChanged(object sender,
            CheckBoxesDropDownEventHandlerArgs e)
        {
            this.ServiceTypesCategoryDropDown.SelectedIndex =
                GetServiceTypesCategoryIndexBySelectedServiceTypes(e.SelectedServiceTypeIds, e.ServiceTypesNumber);
            LoadMemoTextBoxAndDropDownList();
        }

        private int GetServiceTypesCategoryIndexBySelectedServiceTypes(
            List<int?> selectedServiceTypeIds,
            int serviceTypesNumber)
        {
            if (selectedServiceTypeIds.Count == serviceTypesNumber)
            {
                return (int)ServiceTypesCategory.All; //All
            }

            var serviceTypesCategory = GetServiceTypesCategory(selectedServiceTypeIds);
            if (serviceTypesCategory == ServiceTypesBillingSet.Screening)
            {
                return (int)ServiceTypesCategory.ScreeningServices;
            }

            if (serviceTypesCategory == ServiceTypesBillingSet.LabServices)
            {
                return (int)ServiceTypesCategory.LabServices;
            }

            return (int)ServiceTypesCategory.Custom; //Custom
        }

        protected void ResetButton_OnClick(object sender, EventArgs e)
        {
            ErrPrintedOrders.Text = "";
            ResetUIControls(ResetMode.All);
        }

        enum ResetMode
        {
            All,
            ForCustomerSearch,
            ForFirstOrderSearch,
            ForOrderListSearch
        }

        private void ResetUIControls(ResetMode resetMode)
        {
            if (resetMode != ResetMode.ForCustomerSearch)
            {
                this.CustomerDropDownList.ClearSelection();
                this.CustomerCodeTextBox.Text = null;
            }

            if (resetMode != ResetMode.ForCustomerSearch)
            {
                this.ServiceTypesServiceTypeCheckBoxList.SetSelection(this.ServiceTypesServiceTypeCheckBoxList
                    .DefaultSelection);
                InitCreateDateCriteria();
                this.MemoDropDownList.Items.Clear();
                this.MemoDropDownList.Enabled = false;
                this.PoDropDownList.Items.Clear();
                this.PoDropDownList.Enabled = false;
                ErrPrintedOrders.Text = "";
                InitServiceTypeCategory(Session["ID"]);
            }

            if (resetMode == ResetMode.All || resetMode == ResetMode.ForCustomerSearch)
            {
                this.OrderCodeTextBox.Text = null;
            }

            this.OrdersTreeView.Nodes.Clear();
            OrdersTreeViewStatusBar.Text = null;
            OrdersTreeViewChecked.Text = null;

            //MeasureGridDiv.Visible = true;

            InitAdditionalServicesDataGrid();
            InitMigratedItemsDataGrid();
            InitCurrentBatchItems();
            InitMeasuresDataGrid();
            HideImages();

            ClearInvoiceDropDownList();
            InitInvoiceItemsGridView();

            if (resetMode != ResetMode.ForOrderListSearch)
            {
                this.OrderListDialogListTextBox.Text = string.Empty;
                this.ExtendedOrderSearchButton.Text = ExtendedOrderSearchButtonText;
            }
            FinalLabelLEBtn.Enabled = false;
            FinalLabelBtn.Enabled = false;
            LELabelsCount.Text = "";

        }

        #region Bulk Billing Prices

        private void PopupBulkBillingPriceSettingsDialog()
        {
            this.BillingTypeDiv.Visible = false;
            this.BulkBillingPricesPanel.Visible = true;

            var priceSets = BulkBillingPriceSets;
            var priceManager = new BulkBillingPriceManager(priceSets);
            var orderCheckedTreeNodes = GetOrderCheckedTreeNodes().ToList();

            var groupCodes = orderCheckedTreeNodes
                .Select(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).GroupCode).ToList();
            var alreadyBilledOrders = GetAlreadyBilledOrders(groupCodes);
            var quantitiesData = GetQuantitiesData(groupCodes);

            var checkedItems = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == ItemDepth)
                .Select(GetTreeNodeModel<TreeNodeItemModel>)
                .ToList();

            var qbCustomerNames = new Dictionary<int, string>();

            TreeNodeGroupModel groupModel;
            var bulkBillingPricesModels = orderCheckedTreeNodes
                .Select(treeNode =>
                {
                    groupModel = GetTreeNodeModel<TreeNodeGroupModel>(treeNode);
                    var quantities = quantitiesData.FirstOrDefault(data => data.GroupCode == groupModel.GroupCode);
                    var priceSet = priceManager.GetPrices(groupModel);
                    var alreadyBilledData =
                        alreadyBilledOrders.FirstOrDefault(data => data.GroupCode == groupModel.GroupCode);
                    var orderItems = checkedItems.Where(model => model.GroupCode == groupModel.GroupCode).ToList();
                    var costManager =
                        BulkBillingCostManager.GetInstance(groupModel, quantities, priceSet, alreadyBilledData);

                    if (!qbCustomerNames.ContainsKey(groupModel.RetailerId))
                    {
                        qbCustomerNames[groupModel.RetailerId] =
                            QueryAccountRep.GetQbCustomerName(groupModel.CustomerCode, groupModel.RetailerId, this);
                    }

                    return new PricesGridViewItemModel
                    {
                        OrderText = treeNode.Text,
                        OrderCode = groupModel.GroupCode,
                        CreateDate = ApplyTimeZoneOffset(groupModel.CreateDate, groupModel.AuthorOfficeId),
                        SynthServiceTypeCode = groupModel.SynthServiceTypeCode,
                        SynthServiceTypeName = groupModel.SynthServiceTypeName,
                        ServiceTypeId = groupModel.ServiceTypeId,
                        ServiceTypeName = GetServiceTypes()
                            .FirstOrDefault(item => int.Parse(item.Value) == groupModel.ServiceTypeId)?.Text,
                        RetailerId = groupModel.RetailerId,
                        RetailerName = groupModel.RetailerName,
                        QuestionText = priceSet.QuestionText +
                                       (!string.IsNullOrEmpty(priceSet.QuestionText) ? " ?" : ""),
                        IsSatisfyACondition = false,
                        IsAlreadyBilled = costManager.IsAlreadyBilled,
                        AlreadyBilledData = alreadyBilledData,
                        IsRepack = costManager.IsRepack,
                        Pricing = priceSet.Pricing,
                        DefaultPricing = priceSet.DefaultPricing,
                        SpecialPricing = priceSet.SpecialPricing,
                        ShortDescription = priceSet.ShortDescription,
                        Quantities = costManager.GetQuantities(),
                        Costs = costManager.GetCosts(),
                        InvoiceCosts = costManager.GetInvoiceCosts(),
                        OrderStructurePattern = GetOrderStructurePattern(groupModel.GroupCode),
                        YesAnswerText = priceSet.YesAnswerText,
                        NoAnswerText = priceSet.NoAnswerText,
                        OrderItems = orderItems,
                        HasCustomerRequest = groupModel.HasCustomerRequest,
                        QbCustomerName = qbCustomerNames[groupModel.RetailerId]
                    };
                }).ToList();
            InitBulkBillingPricesGridView(bulkBillingPricesModels);
            BulkBillingPricesPanel.Visible = true;

            if (orderCheckedTreeNodes.Count > 0)
            {
                groupModel = GetTreeNodeModel<TreeNodeGroupModel>(orderCheckedTreeNodes[0]);
                var accountType =
                    QueryAccountRep.GetAccountType(groupModel.GroupCode, groupModel.CustomerId,
                        groupModel.CustomerOfficeId, this.Session);
                SetInvoiceTargetButtons(accountType);
            }

            this.BillingOptionsDialog.Width = Unit.Pixel(1100);
            this.EachOrderToSeparateInvoiceRadioButton.Checked = false;
            this.AllOrdersToOneInvoiceRadioButton.Checked = true;
            BillingOptionsModalPopupExtender.Show();
            // OK -> BillingOptionsOkButton_OnClick(...) -> BulkBilling()
        }

        private bool IsScreening(int? serviceTypeId)
        {
            return serviceTypeId != null &&
                   IsScreening(new List<int> { serviceTypeId.Value });
        }

        public bool IsScreening(IEnumerable<int> serviceTypeIds)
        {
            return serviceTypeIds.All(id => ServiceTypesServiceTypeCheckBoxList.IsScreening(id));
        }

        private void DoBulkBilling()
        {
            var customerModel = GetCustomerModel(this.CustomerCodeTextBox.Text.Trim());
            if (customerModel == null)
            {
                throw new Exception("Internal Error");
            }

            List<PricesGridViewItemModel> pricesModels =
                (List<PricesGridViewItemModel>)GetViewState(SessionConstants.BulkBillingPricesModels);
            var readyToBilling = pricesModels.Where(pricesModel => pricesModel.IsReadyToBilling).ToList();
            if (this.AllOrdersToOneInvoiceRadioButton.Checked)
            {
                var qbNames = readyToBilling.GroupBy(model => model.QbCustomerName)
                    .ToDictionary(grouping => grouping.Key);
                if (qbNames.Count > 1)
                {
                    var msg = "Cannot make an invoice for orders with different QuickBooks Customer Names";
                    var list = qbNames.Select(pair => $"QB Customer: {pair.Key} - {pair.Value.ToList().Count} order(s)")
                        .ToList();
                    PopupInfoDialog(msg, true, list);
                    return;
                }
            }

            var checkedItems = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == ItemDepth)
                .Select(GetTreeNodeModel<TreeNodeItemModel>)
                .ToList();

            var orderStructWarnings = new List<OrderStructWarningData>();
            var toBeInvoicedItems =
                GetToBeInvoicedItems(readyToBilling, checkedItems, customerModel, orderStructWarnings);

            string invoiceStatus;
            if (this.QBPrimaryRadioButton.Checked)
                invoiceStatus = "Invoiced";
            else if (this.QBCorptRadioButton.Checked)
                invoiceStatus = "Inv.Corpt";
            else if (this.TallyRadioButton.Checked)
                invoiceStatus = "TallyInv";
            else
            {
                PopupInfoDialog("It is need to select one of the 3 choices: " +
                                $@"{QBPrimaryRadioButton.Text}, {QBCorptRadioButton.Text} or {TallyRadioButton.Text}.",
                    true);
                return;
            }

            if (this.AllOrdersToOneInvoiceRadioButton.Checked)
            {
                DoAllOrdersInOneInvoice(customerModel, toBeInvoicedItems, invoiceStatus, orderStructWarnings);
            }
            else if (this.EachOrderToSeparateInvoiceRadioButton.Checked)
            {
                DoEachOrdersInSeparateInvoice(customerModel, toBeInvoicedItems, invoiceStatus, orderStructWarnings);
            }
        }

        private void DoAllOrdersInOneInvoice(
            CustomerModel customerModel,
            List<ItemToBeInvoiced> toBeInvoicedItems,
            string invoiceStatus,
            List<OrderStructWarningData> orderStructWarnings)
        {
            int invoiceId;
            var items4Invoices = new List<Item4Invoice>();
            try
            {
                invoiceId = QueryAccountRep.AddBulkBillingInvoice(
                    int.Parse(customerModel.CustomerId),
                    int.Parse(customerModel.OfficeId),
                    toBeInvoicedItems.Where(itemToBeInvoiced => itemToBeInvoiced.BatchCode <= 2).ToList(),
                    this,
                    invoiceStatus);

                var qcLabGroupModels = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                    .Where(treeNode => treeNode.Depth == OrderDepth)
                    .Select(GetTreeNodeModel<TreeNodeGroupModel>)
                    .Where(treeNodeGroupModel => treeNodeGroupModel.QCLab == "Y")
                    .ToList();
                qcLabGroupModels.ForEach(groupModel =>
                {
                    var items4Invoice = GetItems4Invoice(1, invoiceId, groupModel);
                    items4Invoices.AddRange(items4Invoice);
                });

                FillInvoiceDropDownList(customerModel);
                ListItem lastCreated = this.InvoicesDropDownList.Items.FindByValue(invoiceId.ToString());
                if (lastCreated != null)
                {
                    this.InvoicesDropDownList.SelectedIndex = this.InvoicesDropDownList.Items.IndexOf(lastCreated);
                }
            }
            catch (Exception e)
            {
                var errorMessage = $"Customer cannot be billed for selected orders, internal error occurs: {e.Message}";
                LogUtils.UpdateSpLog(errorMessage, this.Session);
                LogUtils.UpdateSpLogWithException(e, this);
                PopupInfoDialog(errorMessage, true);
                return;
            }

            var invoiceDetails = QueryAccountRep.GetInvoiceDetails(invoiceId, this);
            if (invoiceDetails.Count <= 0) return;

            var okMessage =
                $"Invoice {invoiceId}(\"{invoiceDetails[0].Status}\") has been created for customer \"{customerModel.CustomerName}\" with the following  item(s):";
            LogUtils.UpdateSpLog(okMessage, this.Session);

            var messageList = new List<string> { "" };
            messageList.AddRange(invoiceDetails.Select(data => data.ToString()).ToList());
            messageList.AddRange(items4Invoices.Select(item4Invoice =>
                $"{item4Invoice.FullBatch}.{item4Invoice.ItemCode}: {item4Invoice.Result}").ToList());

            if (orderStructWarnings.Count > 0)
            {
                messageList.Add("");
                messageList.Add("Warnings:");
                messageList.AddRange(orderStructWarnings.Select(data => data.ToString()));
            }

            messageList.ForEach(s => LogUtils.UpdateSpLog(s, this.Session));
            PopupInfoDialog(okMessage, false, messageList, 1400);
        }

        private void DoEachOrdersInSeparateInvoice(
            CustomerModel customerModel,
            List<ItemToBeInvoiced> toBeInvoicedItems,
            string invoiceStatus,
            List<OrderStructWarningData> orderStructWarnings)
        {
            List<int> invoiceIds;
            int invoiceId;
            try
            {
                invoiceIds = QueryAccountRep.AddBulkBillingInSeparateInvoices(
                    int.Parse(customerModel.CustomerId),
                    int.Parse(customerModel.OfficeId),
                    toBeInvoicedItems,
                    this,
                    invoiceStatus);

                FillInvoiceDropDownList(customerModel);
                invoiceId = invoiceIds.Last();
                var lastCreated = this.InvoicesDropDownList.Items.FindByValue(invoiceId.ToString());
                if (lastCreated != null)
                {
                    this.InvoicesDropDownList.SelectedIndex = this.InvoicesDropDownList.Items.IndexOf(lastCreated);
                }
            }
            catch (Exception e)
            {
                var errorMessage = $"Customer cannot be billed for selected orders, internal error occurs: {e.Message}";
                LogUtils.UpdateSpLog(errorMessage, this.Session);
                LogUtils.UpdateSpLogWithException(e, this);
                PopupInfoDialog(errorMessage, true);
                return;
            }

            var invoiceDetails = QueryAccountRep.GetInvoiceDetails(invoiceId, this);
            if (invoiceDetails.Count <= 0) return;

            var okMessage =
                $"{invoiceIds.Count} invoice(s) has been created for customer \"{customerModel.CustomerName}\":";
            LogUtils.UpdateSpLog(okMessage, this.Session);

            var messageList = new List<string> { "" };
            foreach (var id in invoiceIds)
            {
                var invoiceListItem = this.InvoicesDropDownList.Items.FindByValue(id.ToString());
                messageList.Add(invoiceListItem.Text);
            }

            if (orderStructWarnings.Count > 0)
            {
                messageList.Add("");
                messageList.Add("Warnings:");
                messageList.AddRange(orderStructWarnings.Select(data => data.ToString()));
            }

            messageList.ForEach(s => LogUtils.UpdateSpLog(s, this.Session));
            PopupInfoDialog(okMessage, false, messageList, 1400);
        }

        private List<ItemToBeInvoiced> GetToBeInvoicedItems(List<PricesGridViewItemModel> pricesGridViewItemModels,
            List<TreeNodeItemModel> checkedItems, CustomerModel customerModel,
            ICollection<OrderStructWarningData> warnings)
        {
            var invoicingData = new List<ItemToBeInvoiced>();
            pricesGridViewItemModels.ForEach(pricesGridViewItemModel =>
            {
                var orderItems = checkedItems.Where(model => model.GroupCode == pricesGridViewItemModel.OrderCode)
                    .ToList();

                int itemQuantity;
                double cost;
                string costName;
                TreeNodeItemModel itemModel;

                if (!pricesGridViewItemModel.HasCustomerRequest)
                {
                    costName = "HasNotRequest";
                    itemQuantity = pricesGridViewItemModel.Quantities.Total;
                    cost = ((SinglePricingModel)pricesGridViewItemModel.Costs).Total;
                    itemModel = VirtualOrderStructure.GetItemForInvoiceItem1(orderItems);
                    if (itemModel != null)
                    {
                        invoicingData.Add(GetItemToBeInvoiced(itemModel, pricesGridViewItemModel, customerModel,
                            costName, cost, pricesGridViewItemModel.Quantities));
                    }
                    else
                    {
                        warnings.Add(new OrderStructWarningData(pricesGridViewItemModel.OrderCode,
                            VirtualOrderStructure.GetFullItemCode1(pricesGridViewItemModel.OrderCode), costName,
                            itemQuantity, cost));
                    }
                }
                else if (pricesGridViewItemModel.IsRepack)
                {
                    costName = "Repack";
                    itemQuantity = pricesGridViewItemModel.Quantities.Total;
                    cost = ((SinglePricingModel)pricesGridViewItemModel.Costs).Total;
                    itemModel = VirtualOrderStructure.GetItemForInvoiceItem1(orderItems);
                    if (itemModel != null)
                    {
                        invoicingData.Add(GetItemToBeInvoiced(itemModel, pricesGridViewItemModel, customerModel,
                            costName, cost, pricesGridViewItemModel.Quantities));
                    }
                    else
                    {
                        warnings.Add(new OrderStructWarningData(pricesGridViewItemModel.OrderCode,
                            VirtualOrderStructure.GetFullItemCode1(pricesGridViewItemModel.OrderCode), costName,
                            itemQuantity, cost));
                    }
                }
                else
                {
                    costName = "Pass";
                    itemQuantity = pricesGridViewItemModel.Quantities.Pass;
                    cost = ((RegularPricingModel)pricesGridViewItemModel.Costs).Pass.ToDouble();
                    itemModel = VirtualOrderStructure.GetItemForInvoiceItem1(orderItems);
                    if (itemModel != null)
                    {
                        invoicingData.Add(GetItemToBeInvoiced(itemModel, pricesGridViewItemModel, customerModel,
                            costName, cost, pricesGridViewItemModel.Quantities));
                    }
                    else
                    {
                        warnings.Add(new OrderStructWarningData(pricesGridViewItemModel.OrderCode,
                            VirtualOrderStructure.GetFullItemCode1(pricesGridViewItemModel.OrderCode), costName,
                            itemQuantity, cost));
                    }

                    if (IsRejectNeeded(pricesGridViewItemModel))
                    {
                        costName = "Reject";
                        itemQuantity = pricesGridViewItemModel.Quantities.Reject;
                        cost = ((RegularPricingModel)pricesGridViewItemModel.Costs).Reject.ToDouble();
                        itemModel = VirtualOrderStructure.GetItemForInvoiceItem2(orderItems);
                        if (itemModel != null)
                        {
                            invoicingData.Add(GetItemToBeInvoiced(itemModel, pricesGridViewItemModel, customerModel,
                                costName, cost, pricesGridViewItemModel.Quantities));
                        }
                        else
                        {
                            warnings.Add(new OrderStructWarningData(pricesGridViewItemModel.OrderCode,
                                VirtualOrderStructure.GetFullItemCode2(pricesGridViewItemModel.OrderCode), costName,
                                itemQuantity, cost));
                        }
                    }
                }

            });
            return invoicingData;
        }

        private static bool IsRejectNeeded(PricesGridViewItemModel pricesGridViewItemModel)
        {
            return !pricesGridViewItemModel.IsRepack &&
                   pricesGridViewItemModel.HasCustomerRequest &&
                   pricesGridViewItemModel.Quantities != null &&
                   pricesGridViewItemModel.Quantities.Reject > 0 &&
                   pricesGridViewItemModel.ShortDescription != SignetRetailerRule;
        }

        private ItemToBeInvoiced GetItemToBeInvoiced(TreeNodeItemModel itemModel,
            PricesGridViewItemModel pricesGridViewItemModel,
            CustomerModel customerModel,
            string costName,
            double cost,
            QuantitiesModel quantitiesModel)
        {
            var serviceTypeName = GetServiceTypes()
                .First(listItem => listItem.Value == pricesGridViewItemModel.ServiceTypeId.ToString()).Text;

            QuantitiesModel quantitiesModel2 = quantitiesModel;
            double cost2 = cost;

            if (pricesGridViewItemModel.ShortDescription == SignetRetailerRule &&
                quantitiesModel is RegularQuantitiesModel regularQuantitiesModel)
            {
                quantitiesModel2 = new RegularQuantitiesModel(
                    regularQuantitiesModel.Pass + regularQuantitiesModel.Reject, 0,
                    regularQuantitiesModel.PassPieces + regularQuantitiesModel.RejectPieces, 0,
                    regularQuantitiesModel.RejectStones,
                    regularQuantitiesModel.Total,
                    regularQuantitiesModel.QuantitySource);
                if (pricesGridViewItemModel.Pricing is RegularPricingModel pricingModel &&
                    pricingModel.Pass != null)
                {
                    //cost2 = quantitiesModel.Pass * pricingModel.Pass.Value;
                    cost2 = quantitiesModel2.Pass * pricingModel.Pass.Value;
                }
            }

            var description = new InvoiceItemDescription(
                    costName, pricesGridViewItemModel, pricesGridViewItemModel.ServiceTypeId,
                    serviceTypeName, itemModel.GroupCode, customerModel.CustomerName, quantitiesModel2)
                .GetDescription();

            return new ItemToBeInvoiced
            {
                OrderCode = itemModel.GroupCode,
                BatchCode = itemModel.BatchCode,
                BatchId = itemModel.BatchId,
                ItemCode = itemModel.ItemCode,
                Cost = cost2,
                Description = description
            };
        }

        private List<AlreadyBilledOrderData> GetAlreadyBilledOrders(IEnumerable<int> groupCodes)
        {
            var alreadyBilledOrders = QueryAccountRep.GetAlreadyBilledOrders(groupCodes, this);
            SetViewState(alreadyBilledOrders, SessionConstants.AlreadyBilledOrders);
            return alreadyBilledOrders;
        }

        private List<QuantitiesData> GetQuantitiesData(IEnumerable<int> groupCodes)
        {
            var quantitiesData = QueryAccountRep.GetBulkBillingQuantitiesData(groupCodes, this);
            SetViewState(quantitiesData, SessionConstants.BulkBillingQuantitiesData);
            return quantitiesData;
        }

        private static CheckBox GetBulkBillingPricesAnswerCheckBox(GridViewRow gridViewRow)
        {
            return GetGridViewRowControl<CheckBox>(gridViewRow, ControlIdAnswerCheckBox);
        }

        private static T GetGridViewRowControl<T>(GridViewRow gridViewRow, string controlId) where T : Control
        {
            return (T)gridViewRow.FindControl(controlId);
        }

        private CustomerModel GetCustomerModel(string customerCode)
        {
            var customerModels = (List<CustomerModel>)GetViewState(SessionConstants.CustomersList);
            var customerModel = customerModels.FirstOrDefault(m => m.CustomerCode?.Trim() == customerCode);
            return customerModel;
        }

        private CustomerModel GetCustomerModel(int customerIndex)
        {
            var customerModels = (List<CustomerModel>)GetViewState(SessionConstants.CustomersList);
            var customerModel = customerModels[customerIndex];
            return customerModel;
        }

        private void InitBulkBillingPricesGridView(
            List<PricesGridViewItemModel> bulkBillingPricesModels = null,
            string emptyDataText = "No data requested yet")
        {
            var dataSource = bulkBillingPricesModels ?? new List<PricesGridViewItemModel>();

            this.BulkBillingPricesGridView.EmptyDataText = emptyDataText;


            if (dataSource.Count > 0)
            {
                var readyForBillCount =
                    dataSource.Count(billingPricesModel => billingPricesModel.IsReadyToBilling);
                var alreadyBilledCount =
                    dataSource.Count(billingPricesModel => billingPricesModel.IsAlreadyBilled);
                var noQtyDataCount =
                    dataSource.Count(billingPricesModel => billingPricesModel.Quantities == null);
                this.BulkBillingPricesGridViewStatusBar.Text = $@"Total: {dataSource.Count}" +
                                                               (alreadyBilledCount > 0
                                                                   ? $@", Already Billed = {alreadyBilledCount}"
                                                                   : "") +
                                                               (noQtyDataCount > 0
                                                                   ? $@", No quantity data = {noQtyDataCount}"
                                                                   : "") +
                                                               $@", Ready for Billing = {readyForBillCount}";
            }
            else
            {
                this.BulkBillingPricesGridViewStatusBar.Text = "";
            }

            var enabledQuestionsCount = dataSource.Count(billingPricesModel =>
                billingPricesModel.IsReadyToBilling && !string.IsNullOrEmpty(billingPricesModel.QuestionText));

            this.BulkBillingPricesCheckAllRowsButton.Visible = enabledQuestionsCount > 0;
            this.BulkBillingPricesUnCheckAllRows.Visible = enabledQuestionsCount > 0;

            SetViewState(dataSource, SessionConstants.BulkBillingPricesModels);
            BulkBillingPricesGridView.DataSource = dataSource;
            BulkBillingPricesGridView.DataBind();
        }

        protected void BulkBillingPricesAnswerCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            var checks = this.BulkBillingPricesGridView.Rows.Cast<GridViewRow>()
                .Select(gridViewRow =>
                {
                    var checkBox = GetBulkBillingPricesAnswerCheckBox(gridViewRow);
                    return checkBox.Checked;
                })
                .ToArray();

            var groupModels = (List<TreeNodeGroupModel>)
                GetViewState(SessionConstants.BillingCurrentGroupModels);
            var dataSource = (List<PricesGridViewItemModel>)
                GetViewState(SessionConstants.BulkBillingPricesModels);
            var quantitiesModels = (List<QuantitiesData>)
                GetViewState(SessionConstants.BulkBillingQuantitiesData);
            var priceSets = BulkBillingPriceSets;

            for (var i = 0; i < checks.Length; i++)
            {
                dataSource[i].IsSatisfyACondition = checks[i];
                dataSource[i].Pricing = dataSource[i].IsSatisfyACondition
                    ? dataSource[i].SpecialPricing
                    : dataSource[i].DefaultPricing;

                var quantitiesData = quantitiesModels.FirstOrDefault(q => q.GroupCode == dataSource[i].OrderCode);
                if (quantitiesData == null)
                {
                    continue;
                }

                var priceManager = new BulkBillingPriceManager(priceSets);
                var groupModel = groupModels.First(g => g.GroupCode == dataSource[i].OrderCode);
                var priceSet = priceManager.GetPrices(groupModel);
                var alreadyBilledOrders =
                    (List<AlreadyBilledOrderData>)GetViewState(SessionConstants.AlreadyBilledOrders);
                var alreadyBilledOrderData =
                    alreadyBilledOrders.FirstOrDefault(data => data.GroupCode == groupModel.GroupCode);

                var costManager =
                    BulkBillingCostManager.GetInstance(groupModel, quantitiesData, priceSet, alreadyBilledOrderData);

                dataSource[i].Costs = checks[i]
                    ? costManager.GetSpecialCosts()
                    : costManager.GetDefaultCosts();
            }

            SetViewState(dataSource, SessionConstants.BulkBillingPricesModels);
        }

        private string GetOrderStructurePattern(int groupCode)
        {
            var groupNode = OrdersTreeView.Nodes.Cast<TreeNode>()
                .First(treeNode => GetTreeNodeModel<TreeNodeGroupModel>(treeNode).GroupCode == groupCode);
            var batchesNumber = groupNode.ChildNodes.Count;
            var itemsNumber = groupNode.ChildNodes.Cast<TreeNode>()
                .Sum(batchNode => batchNode.ChildNodes.Count);
            return string.Format(OrderStructurePattern, batchesNumber, itemsNumber);
        }

        protected void BulkBillingPricesGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            var gridViewRow = e.Row;
            if (gridViewRow.RowIndex >= 0)
            {
                PricesGridViewItemModel pricesGridViewItemModel = (PricesGridViewItemModel)gridViewRow.DataItem;
                BulkBillingPricesGridView_AdjustRow(gridViewRow, pricesGridViewItemModel);
            }
        }

        /// <summary>
        /// Bulks the billing prices grid view adjust row.
        /// </summary>
        /// <param name="gridViewRow">The grid view row.</param>
        /// <param name="pricesGridViewItemModel">The prices grid view item model.</param>
        /// <returns></returns>
        private static void BulkBillingPricesGridView_AdjustRow(GridViewRow gridViewRow,
            PricesGridViewItemModel pricesGridViewItemModel)
        {
            var isThereAdditionalQuestion = !string.IsNullOrEmpty(pricesGridViewItemModel.QuestionText);
            var isThereQuantityData = pricesGridViewItemModel.Quantities != null;
            var answerCheckBox = GetGridViewRowControl<CheckBox>(gridViewRow, ControlIdAnswerCheckBox);

            answerCheckBox.Visible = isThereAdditionalQuestion;

            var errors = "";
            var errorsCount = 0;

            if (!isThereQuantityData)
            {
                errors += $" {++errorsCount}. There is no quantity data;";
            }

            var pricesLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdPricesLabel);
            var defaultPricesLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdDefaultPricesLabel);
            var specialPricesLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdSpecialPricesLabel);
            var orderStatusImage = GetGridViewRowControl<Image>(gridViewRow, ControlIdOrderStatusImage);

            var quantitiesLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdQuantitiesLabel);
            var costsLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdCostsLabel);
            var costsInvoiceLabel = GetGridViewRowControl<Label>(gridViewRow, ControlIdCostsInvoiceLabel);

            pricesLabel.Text = pricesGridViewItemModel.Pricing?.ToString();
            pricesLabel.ToolTip = @"Pricing Rule: """ + pricesGridViewItemModel.ShortDescription + @"""";

            defaultPricesLabel.Text = pricesGridViewItemModel.DefaultPricing?.ToString();
            specialPricesLabel.Text = pricesGridViewItemModel.SpecialPricing?.ToString();

            quantitiesLabel.Text = pricesGridViewItemModel.Quantities != null
                ? pricesGridViewItemModel.Quantities.ToString()
                : "No Data";
            quantitiesLabel.ToolTip = pricesGridViewItemModel.Quantities != null
                ? $"Source: {pricesGridViewItemModel.Quantities.QuantitySource.ToString()}"
                : "Quantity data cannot be found in the source expected for current price rule";

            // Costs
            costsLabel.Text = pricesGridViewItemModel.Costs?.ToString();
            costsLabel.ToolTip = !string.IsNullOrEmpty(costsLabel.Text) ? @"Preliminary calculation" : @"N/A";

            costsInvoiceLabel.Text = pricesGridViewItemModel.InvoiceCosts?.ToString();
            costsInvoiceLabel.ToolTip = @"Costs from invoice of already billed order";

            // Image
            if (errorsCount == 0 && !pricesGridViewItemModel.IsAlreadyBilled)
            {
                orderStatusImage.ToolTip = @"Ready for Bulk Billing.";

                var item1 = VirtualOrderStructure.GetItemForInvoiceItem1(pricesGridViewItemModel.OrderItems);
                var item2 = VirtualOrderStructure.GetItemForInvoiceItem2(pricesGridViewItemModel.OrderItems);
                var isRejectNeeded = pricesGridViewItemModel.Quantities != null &&
                                     pricesGridViewItemModel.Quantities.Reject > 0;

                if ((item1 == null) || (isRejectNeeded && item2 == null))
                {
                    orderStatusImage.ImageUrl = "~\\Images\\yellow_circle.jpg";

                    string costName;
                    string orderItem;
                    var warning = OrderStructWarningData.WarningText;

                    if (item1 == null && pricesGridViewItemModel.IsRepack)
                    {
                        costName = "Repack";
                        orderItem = VirtualOrderStructure.GetFullItemCode1(pricesGridViewItemModel.OrderCode);
                    }
                    else if (item1 == null && !pricesGridViewItemModel.IsRepack)
                    {
                        costName = "Pass";
                        orderItem = VirtualOrderStructure.GetFullItemCode1(pricesGridViewItemModel.OrderCode);
                    }
                    else
                    {
                        costName = "Reject";
                        orderItem = VirtualOrderStructure.GetFullItemCode2(pricesGridViewItemModel.OrderCode);
                    }

                    orderStatusImage.ToolTip += $@" {warning} - for {costName} - {orderItem} missing";
                }
                else
                {
                    orderStatusImage.ImageUrl = "~\\Images\\green_circle.jpg";
                }
            }
            else
            {
                orderStatusImage.ImageUrl = "~\\Images\\red_circle.jpg";
                if (pricesGridViewItemModel.IsAlreadyBilled)
                {
                    var alreadyBilledMessage =
                        $@"Order already billed. Invoice Status={pricesGridViewItemModel.AlreadyBilledData.InvoiceStatus}.";
                    if (errorsCount > 0)
                    {
                        orderStatusImage.ToolTip = alreadyBilledMessage = " Also: " + errors;
                    }

                    orderStatusImage.ToolTip = alreadyBilledMessage;
                }
                else
                {
                    orderStatusImage.ToolTip = @"Order cannot be billed. Cause:" + errors;
                }
            }

            if (!pricesGridViewItemModel.IsReadyToBilling)
            {
                gridViewRow.Enabled = false;
                gridViewRow.ForeColor = Color.Gray;
            }
        }

        protected void InvoicesGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[GetColumnIndex(this.InvoiceItemsGridView, "Create Date")].ToolTip = @"Item Created Date";
                e.Row.Cells[GetColumnIndex(this.InvoiceItemsGridView, "Description")].ToolTip =
                    @"Description for QuickBooks";
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] =
                    Page.ClientScript.GetPostBackClientHyperlink(this.InvoiceItemsGridView, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = @"Click to select this row.";
            }
        }

        private static int GetColumnIndex(GridView gridView, string headerText)
        {
            for (var i = 0; i < gridView.Columns.Count; i++)
            {
                if (gridView.Columns[i].HeaderText == headerText)
                {
                    return i;
                }
            }

            return -1;
        }

        protected void InvoiceRefreshButton_OnCommand(object sender, CommandEventArgs e)
        {
            var customerModel = GetCustomerModel(this.CustomerCodeTextBox.Text);
            if (customerModel == null)
            {
                return;
            }

            FillInvoiceDropDownList(customerModel);
        }

        protected void OpenQbInvoicesButton_OnCommand(object sender, CommandEventArgs e)
        {
            string message;

            var listItem = this.InvoicesDropDownList.Items[this.InvoicesDropDownList.SelectedIndex];
            var invoiceId = int.Parse(listItem.Value);
            var invoices = (IEnumerable<CustomerInvoiceModel>)GetViewState(SessionConstants.CustomerInvoices);
            var invoiceModel = invoices.First(invoice => invoice.InvoiceId == invoiceId);
            if (invoiceModel.Status != "Completed")
            {
                message =
                    $"Command \"Open QB Invoice\" is allowed only for invoice with status \"Completed\", status of current invoice is \"{invoiceModel.Status}\"";
                PopupInfoDialog(message, false);
                return;
            }

            var requestId = QueryAccountRep.AddQbInvoiceRequest(
                invoiceModel.InvoiceId, QbInvoiceRequestModel.RequestTypeGet, this);

            int iterationTimeout = 30000;
            var iterationTime = 2000;
            QbInvoiceRequestModel qbInvoiceRequestModel;
            do
            {
                Thread.Sleep(iterationTime);
                iterationTimeout -= iterationTime;
                qbInvoiceRequestModel = QueryAccountRep.GetQbInvoiceRequest(requestId, this);
            } while (qbInvoiceRequestModel != null &&
                     qbInvoiceRequestModel.Status == QbInvoiceRequestModel.RequestStatusNew &&
                     iterationTimeout > 0);

            if (qbInvoiceRequestModel?.Status == QbInvoiceRequestModel.RequestStatusCompleted)
            {
                try
                {
                    JsonConvert.DeserializeObject<InvoiceData>(qbInvoiceRequestModel.InvoiceData);
                }
                catch (Exception e1)
                {
                    message = $"Cannot parse invoice data received from QuickBooks, error: {e1.Message}";
                    PopupInfoDialog(message, true);
                    return;
                }

                //PrintPdfInvoice(requestId, invoiceId, $@"c:\temp\AccountRep\QBInvoices\QBInvoice{invoiceId}.pdf");

                var url = $"/AccountRepInvoicePdf.aspx?requestId={requestId}&invoiceId={invoiceId}";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(),
                    "OpenInvoicePdfInNewTab", $"window.open('{url}', '_blank' );", true);

                return;
            }

            if (qbInvoiceRequestModel?.Status == QbInvoiceRequestModel.RequestStatusError)
            {
                if (!DeserializeInvoiceData(qbInvoiceRequestModel, out var invoiceData, out var exception))
                {
                    message = $"Failed invoice data received from QuickBooks. {exception?.Message}";
                    PopupInfoDialog(message, true);
                    return;
                }

                message = invoiceData.InvoiceQueryRs.statusCode == "500"
                    ? "Invoice not found"
                    : $"{invoiceData.InvoiceQueryRs.statusMessage}({invoiceData.InvoiceQueryRs.statusCode})";

                PopupInfoDialog(message, true);
                return;
            }

            message = $"Command \"Print Invoice\" is failed. Probably, QB WebConnector Service does not work.";
            PopupInfoDialog(message, true);
        }

        private void PrintPdfInvoice(int requestId, int invoiceId, string fileName)
        {
            QbInvoiceRequestModel qbInvoiceRequestModel = QueryAccountRep.GetQbInvoiceRequest(requestId, this);
            var generator = InvoicePdfDocumentGenerator.CreateInstance(
                qbInvoiceRequestModel.InvoiceData, $"QB-Invoice-{invoiceId}");
            var templateFileName = (string)Session["PDFInvoiceTemplateFileName"];
            var templatePath = this.Server.MapPath(templateFileName);
            generator.Init(templatePath);
            generator.Fill();
            generator.Print("Microsoft Print to PDF",
                $"Invoice {invoiceId}",
                fileName,
                null);
        }

        protected void PrintAllCompletedInvoicesButton_OnCommand(object sender, CommandEventArgs e)
        {
            var invoices =
                ((List<CustomerInvoiceModel>)GetViewState(SessionConstants.CustomerInvoices))
                .Where(m => m.Status == "Completed").ToList();
            if (invoices.Count == 0)
            {
                PopupInfoDialog("There are no completed invoices", true);
                return;
            }

            PrintInvoices(invoices);
        }

        protected void PrintQbInvoicesButton_OnCommand(object sender, CommandEventArgs e)
        {
            var listItem = this.InvoicesDropDownList.Items[this.InvoicesDropDownList.SelectedIndex];
            var invoiceId = int.Parse(listItem.Value);
            var invoices = (IEnumerable<CustomerInvoiceModel>)GetViewState(SessionConstants.CustomerInvoices);
            var invoiceModel = invoices.First(invoice => invoice.InvoiceId == invoiceId);
            if (invoiceModel.Status != "Completed")
            {
                var message =
                    $"Command \"Print QB Invoice\" is allowed only for invoice with status \"Completed\", status of current invoice is \"{invoiceModel.Status}\"";
                PopupInfoDialog(message, false);
                return;
            }

            PrintInvoices(new List<CustomerInvoiceModel> { invoiceModel });
        }

        private void PrintInvoices(List<CustomerInvoiceModel> invoices)
        {
            string message;
            if (string.IsNullOrEmpty(this.Session["MachineName"] as string))
            {
                message = "Machine Name is empty (should be set with client start)";
                PopupInfoDialog(message, true);
                return;
            }

            var errorSentInvoiceIds = new List<string>();
            var requestedIds = invoices
                .Select(invoiceModel =>
                    QueryAccountRep.AddQbInvoiceRequest(invoiceModel.InvoiceId, QbInvoiceRequestModel.RequestTypeGet,
                        this))
                .ToList();

            int iterationTimeout = 120000;
            var iterationTime = 2000;
            var completedRequests = new List<QbInvoiceRequestModel>();
            var errorRequests = new List<QbInvoiceRequestModel>();
            do
            {
                Thread.Sleep(iterationTime);
                iterationTimeout -= iterationTime;
                var iterationCompletedRequests = new List<QbInvoiceRequestModel>();
                var iterationErrorRequests = new List<QbInvoiceRequestModel>();
                requestedIds.ForEach(requestId =>
                {
                    var qbInvoiceRequestModel =
                        QueryAccountRep.GetQbInvoiceRequest(requestId, this);
                    if (qbInvoiceRequestModel.Status == QbInvoiceRequestModel.RequestStatusCompleted)
                    {
                        iterationCompletedRequests.Add(qbInvoiceRequestModel);
                    }

                    if (qbInvoiceRequestModel.Status == QbInvoiceRequestModel.RequestStatusError)
                    {
                        iterationErrorRequests.Add(qbInvoiceRequestModel);
                    }
                });

                if (iterationCompletedRequests.Count > 0)
                {
                    completedRequests.AddRange(iterationCompletedRequests);
                    iterationCompletedRequests.ForEach(m => requestedIds.Remove(m.RequestId));
                }

                if (iterationErrorRequests.Count > 0)
                {
                    errorRequests.AddRange(iterationErrorRequests);
                    errorRequests.ForEach(m => requestedIds.Remove(m.RequestId));
                }
            } while (completedRequests.Count + errorRequests.Count < requestedIds.Count &&
                     iterationTimeout > 0);

            completedRequests.ForEach(m =>
            {
                if (!DeserializeInvoiceData(m, out var invoiceData, out _))
                {
                    errorSentInvoiceIds.Add(m.QbInvoiceNumber);
                    return;
                }

                var invoiceMessage = new InvoiceMessage
                {
                    Title = GetMessageTitle(m.QbInvoiceNumber),
                    Application = "GDLight",
                    MachineName = Convert.ToString(Session["MachineName"]),
                    InvoiceData = invoiceData
                };
                var serializedObject = JsonConvert.SerializeObject(invoiceMessage);
                var isOk = QueryUtils.SendMessageToStorage(serializedObject, this,
                    QueryUtils.AccountRepPrintInvoiceQueueNamePrefix);
                if (!isOk)
                {
                    errorSentInvoiceIds.Add(m.QbInvoiceNumber);
                }
            });

            var errMessages = new List<string>();
            if (errorRequests.Count > 0)
            {
                errMessages.Add($"QuickBooks Errors({errorRequests.Count}): ");
                errMessages.AddRange(errorRequests.Select(m =>
                {
                    string errorDesc;
                    if (DeserializeInvoiceData(m, out var invoiceData, out var exception))
                    {
                        errorDesc = invoiceData.InvoiceQueryRs.statusCode == "500"
                            ? "Invoice not found"
                            : $"{invoiceData.InvoiceQueryRs.statusMessage}({invoiceData.InvoiceQueryRs.statusCode})";
                    }
                    else
                    {
                        errorSentInvoiceIds.Add(m.QbInvoiceNumber);
                        return $"Failed invoice data received from QuickBooks. {exception?.Message}";
                    }

                    return $"   QB Invoice {m.QbInvoiceNumber} - {errorDesc}";
                }));
            }

            if (requestedIds.Count > 0)
            {
                errMessages.Add(
                    $"Undefined Errors ({requestedIds.Count}). Probably, QB WebConnector Service does not work.");
            }

            if (errorSentInvoiceIds.Count > 0)
            {
                errMessages.Add("The following invoices cannot be sent to print. Try again later.");
                errMessages.AddRange(errorSentInvoiceIds.Select(qbId => $"   QB Invoice {qbId}"));
            }

            if (errMessages.Count > 0)
            {
                message =
                    $"{completedRequests.Count - errorSentInvoiceIds.Count} (of {invoices.Count}) invoices sent to printer.";
                PopupInfoDialog(message, true, errMessages);
            }
            else
            {
                PopupInfoDialog($"All invoices({completedRequests.Count}) sent to your printer", false);
            }
        }

        private static bool DeserializeInvoiceData(QbInvoiceRequestModel qbInvoiceRequestModel,
            out InvoiceData invoiceData, out Exception exception)
        {
            invoiceData = null;
            exception = null;
            try
            {
                invoiceData = JsonConvert.DeserializeObject<InvoiceData>(qbInvoiceRequestModel.InvoiceData);
                if (invoiceData == null)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }

            return true;
        }

        private static string GetMessageTitle(string qbRefNumber)
        {
            return $"QB-Invoice-{qbRefNumber}";
        }

        public static string JsonPrettify(string json)
        {
            using (var stringReader = new StringReader(json))
            using (var stringWriter = new StringWriter())
            {
                var jsonReader = new JsonTextReader(stringReader);
                var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
                jsonWriter.WriteToken(jsonReader);
                return stringWriter.ToString();
            }
        }

        private void ClearInvoiceDropDownList()
        {
            this.InvoicesDropDownList.DataSource = new List<object>();
            this.InvoicesDropDownList.ToolTip = @"Empty";
            this.InvoicesDropDownList.DataBind();
            this.InvoicesDropDownList.ClearSelection();

            this.OrdersInvoiceDropDownList.DataSource = new List<object>();
            this.OrdersInvoiceDropDownList.DataBind();
            this.OrdersInvoiceDropDownList.ClearSelection();

            this.InvoiceAuthorValue.Text = null;
            this.InvoiceRefreshButton.Text = @"Load Invoices";

        }

        private void FillInvoiceDropDownList(CustomerModel customerModel)
        {
            if (!int.TryParse(this.CustomerCodeTextBox.Text, out _))
            {
                return;
            }

            var customerId = int.Parse(customerModel.CustomerId);
            var customerInvoicesResult = QueryAccountRep.GetCustomerInvoices(customerId, GetOrderIds(), this);
            var invoices = customerInvoicesResult.CustomerInvoiceModels;

            var ordersInvoiceModels = customerInvoicesResult.OrdersInvoice;

            SetViewState(invoices, SessionConstants.CustomerInvoices);
            SetViewState(ordersInvoiceModels, SessionConstants.OrdersInvoice);

            this.InvoicesDropDownList.DataTextField = "InvoiceName";
            this.InvoicesDropDownList.DataValueField = "InvoiceId";

            this.InvoicesDropDownList.DataSource = GetInvoicesDropDownListDataSource(invoices);
            this.InvoicesDropDownList.ToolTip =
                @"Pattern: <InvoiceId> <Status> [<QBInvoiceNumber>] <BeginDate> <Author Login>";

            this.InvoicesDropDownList.DataBind();
            this.InvoicesDropDownList.ClearSelection();
            if (invoices.Count <= 0)
            {
                InitInvoiceItemsGridView(
                    emptyDataText: "No invoice items data for no invoices for last orders request");
                return;
            }

            this.InvoicesDropDownList.SelectedIndex = 0;

            this.OrdersInvoiceDropDownList.DataSource = ordersInvoiceModels;
            this.OrdersInvoiceDropDownList.DataBind();
            this.OrdersInvoiceDropDownList.ClearSelection();

            var invoiceDetails = QueryAccountRep.GetInvoiceDetails(invoices[0].InvoiceId, this);
            InitInvoiceItemsGridView(toolTip: $"Customer {customerModel.CustomerCode} invoice {invoices[0].InvoiceId}",
                invoiceItems: invoiceDetails,
                emptyDataText: $"No data for Customer={customerModel.CustomerCode}, Invoice={invoices[0].InvoiceId}, " +
                               $"ServiceTypes={string.Join(",", GetServiceTypeIds())}");

            if (invoiceDetails.Count > 0)
                OrdersInvoiceDropDownListSelect(invoiceDetails[0].GroupCode);

            InvoiceAuthorSelect();
            this.InvoiceRefreshButton.Text = @"Refresh Invoices";

        }

        private IEnumerable<object> GetInvoicesDropDownListDataSource(IEnumerable<CustomerInvoiceModel> invoices)
        {
            return invoices.Select(m =>
            {
                var beginDate = ApplyTimeZoneOffset(m.BeginDate, m.AuthorOfficeId);
                var invoiceName = $"{m.InvoiceId} \"{m.Status}\" {m.QBInvoiceNumber} {beginDate} {m.AuthorLogin}";
                return new { InvoiceName = invoiceName, m.InvoiceId };
            });
        }

        private void InitInvoiceItemsGridView(string toolTip = @"Empty",
            List<InvoiceDetailsData> invoiceItems = null,
            string emptyDataText = "No data requested yet (select 'Customer'/'Customer Code')")
        {
            invoiceItems = invoiceItems ?? new List<InvoiceDetailsData>();
            this.InvoiceItemsGridView.DataSource = invoiceItems;
            this.InvoicesDataGridCaption.ToolTip = toolTip;
            this.InvoiceItemsGridView.EmptyDataText = emptyDataText;
            this.InvoiceItemsGridView.DataBind();
            this.InvoiceItemsGridView.SelectedIndex = -1;

            SetViewState(invoiceItems, SessionConstants.InvoiceItems);
            if (invoiceItems.Count == 0)
            {
                this.InvoicesDataGridStatusBar.Text = "";
            }
        }

        protected void InvoicesDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!int.TryParse(this.CustomerCodeTextBox.Text, out var customerCode))
            {
                return;
            }

            if (this.InvoicesDropDownList.SelectedIndex < 0)
            {
                InitInvoiceItemsGridView();
                return;
            }

            var listItem = this.InvoicesDropDownList.Items[this.InvoicesDropDownList.SelectedIndex];
            var invoiceId = int.Parse(listItem.Value);

            var invoiceDetails = QueryAccountRep.GetInvoiceDetails(invoiceId, this);
            InitInvoiceItemsGridView(toolTip: $"Customer {customerCode} invoice {invoiceId}",
                invoiceItems: invoiceDetails,
                emptyDataText: $"No data for Customer={customerCode}, Invoice={invoiceId}, " +
                               $"ServiceTypes={string.Join(",", GetServiceTypeIds())}");
            if (invoiceDetails.Count > 0)
                OrdersInvoiceDropDownListSelect(invoiceDetails[0].GroupCode);

            InvoiceAuthorSelect();
        }

        protected void OrdersInvoiceDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var customerModel = GetCustomerModel(this.CustomerCodeTextBox.Text);
            var ordersInvoiceListItem =
                this.OrdersInvoiceDropDownList.Items[this.OrdersInvoiceDropDownList.SelectedIndex];
            var invoiceId = int.Parse(ExtractInvoiceId(ordersInvoiceListItem));
            InvoicesDropDownListSelect(invoiceId);

            var invoiceDetails = QueryAccountRep.GetInvoiceDetails(invoiceId, this);
            InitInvoiceItemsGridView(toolTip: $"Customer {customerModel.CustomerCode} invoice {invoiceId}",
                invoiceItems: invoiceDetails,
                emptyDataText: $"No data for Customer={customerModel.CustomerCode}, Invoice={invoiceId}, " +
                               $"ServiceTypes={string.Join(",", GetServiceTypeIds())}");

            InvoiceItemsGridViewSelect(int.Parse(ordersInvoiceListItem.Text));
            InvoiceAuthorSelect();
        }

        private static string ExtractInvoiceId(ListItem ordersInvoiceListItem)
        {
            return ordersInvoiceListItem.Value.Split(' ')[0];
        }

        protected void InvoiceItemsGridView_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var invoiceItems = (List<InvoiceDetailsData>)GetViewState(SessionConstants.InvoiceItems);
            var groupCode = invoiceItems[this.InvoiceItemsGridView.SelectedIndex].GroupCode;
            OrdersInvoiceDropDownListSelect(groupCode);
        }

        private void InvoicesDropDownListSelect(int invoiceId)
        {
            var item = this.InvoicesDropDownList.Items.FindByValue(invoiceId.ToString());
            this.InvoicesDropDownList.ClearSelection();
            item.Selected = true;
        }

        private void OrdersInvoiceDropDownListSelect(int groupCode)
        {
            var item = this.OrdersInvoiceDropDownList.Items.FindByText(groupCode.ToString());
            this.OrdersInvoiceDropDownList.ClearSelection();
            item.Selected = true;
        }

        private void InvoiceItemsGridViewSelect(int orderCode)
        {
            var invoiceItems = (List<InvoiceDetailsData>)GetViewState(SessionConstants.InvoiceItems);
            for (var index = 0; index < invoiceItems.Count; index++)
            {
                var invoiceItem = invoiceItems[index];
                if (invoiceItem.GroupCode == orderCode)
                {
                    var invoiceItemsGridView = this.InvoiceItemsGridView;
                    invoiceItemsGridView.SelectedIndex = index;
                    var script = $"scrollTop_InvoiceItemsGridDiv = getScrollValueUpTo({index});\n";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "ScrollGrid", script, true);
                    break;
                }
            }
        }

        private void InvoiceAuthorSelect()
        {
            if (this.InvoicesDropDownList.SelectedIndex < 0)
            {
                return;
            }

            List<CustomerInvoiceModel> invoices =
                (List<CustomerInvoiceModel>)GetViewState(SessionConstants.CustomerInvoices);
            this.InvoiceAuthorValue.Text = invoices[this.InvoicesDropDownList.SelectedIndex].AuthorName;
        }

        #endregion

        private TimeZoneManager _timeZoneManager;

        private TimeZoneManager TimeZoneManager
        {
            get
            {
                if (_timeZoneManager != null) return _timeZoneManager;

                _timeZoneManager = new TimeZoneManager(
                    GetViewState(SessionConstants.TimeZoneOffsetInMinutes));

                return _timeZoneManager;
            }
        }

        public string ApplyTimeZoneOffset(DateTime dateTime, int authorOfficeId)
        {
            var displayTimeZoneMode = (DisplayTimeZoneModes)GetViewState(SessionConstants.DisplayTimeZoneMode);
            return TimeZoneManager.ApplyTimeZoneOffset(dateTime, authorOfficeId, displayTimeZoneMode);
        }

        private void InitTimeZoneTypeDropDownList()
        {
            this.TimeZoneModeDropDownList.DataTextField = "Text";
            this.TimeZoneModeDropDownList.DataValueField = "Value";
            this.TimeZoneModeDropDownList.DataSource = Enum.GetValues(typeof(DisplayTimeZoneModes))
                .Cast<DisplayTimeZoneModes>()
                .Select(mode => new ListItem(GetEnumDescription(mode), mode.ToString()));
            this.TimeZoneModeDropDownList.DataBind();
            TimeZoneModeDropDownList.SelectedIndex = 0;
            TimeZoneDropDownList_OnSelectedIndexChanged();
        }

        protected void TimeZoneDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            TimeZoneDropDownList_OnSelectedIndexChanged();

            // TO DO: restore selection and checked nodes
            //var selectedNode = this.OrdersTreeView.SelectedNode;
            //var checkedNodes = this.OrdersTreeView.CheckedNodes;
            bool IsSelected(TreeViewLevels levelName, int orderCode, int batchCode, int itemCode, TreeNode treeNode)
            {
                return false;
            }

            if (this.OrdersTreeView.Nodes.Count == 0)
            {
                return;
            }

            var dataSet = (DataSet)GetViewState(SessionConstants.OrdersTreeViewDataSet);
            OrderTreeViewClear();
            FillTreeView(dataSet, OrderLevel, "1=1", OrdersTreeView.Nodes, IsSelected);

            var invoiceItems = GetViewState(SessionConstants.InvoiceItems);
            this.InvoiceItemsGridView.DataSource = invoiceItems ?? new List<object>();
            this.InvoiceItemsGridView.DataBind();

            var invoices = (IEnumerable<CustomerInvoiceModel>)GetViewState(SessionConstants.CustomerInvoices);
            this.InvoicesDropDownList.DataSource = invoices == null
                ? new List<object>()
                : GetInvoicesDropDownListDataSource(invoices);
            this.InvoicesDropDownList.DataBind();
        }

        private void TimeZoneDropDownList_OnSelectedIndexChanged()
        {
            var displayTimeZoneMode =
                (DisplayTimeZoneModes)Enum.Parse(typeof(DisplayTimeZoneModes), TimeZoneModeDropDownList.SelectedValue);
            SetViewState(displayTimeZoneMode, SessionConstants.DisplayTimeZoneMode);
            _timeZoneManager = null;
        }

        protected string ApplyTimeZoneOffset(object containerDataItem)
        {
            if (containerDataItem is InvoiceDetailsData invoiceDetailsData)
            {
                return ApplyTimeZoneOffset(invoiceDetailsData.CreateDate, invoiceDetailsData.AuthorOfficeId);
            }

            return null;
        }

        private void SetOrderSearchMode()
        {
            if (IsSearchByOrderList)
            {
                this.OrderSearchFieldNameLabel.Text = OrderSearchByOrderListFieldName;
            }
            else
            {
                this.OrderSearchFieldNameLabel.Text = OrderSearchByOrderFieldName;
            }
        }

        #region Search By Order List

        protected void ExtendedOrderSearchButton_OnCommand(object sender, CommandEventArgs e)
        {
            this.SearchByOrderListDialogModalPopupExtender.Show();
        }

        protected void SearchByOrderListDialogOKButton_OnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CustomerCodeTextBox.Text))
            {
                this.CustomerCodeTextBox.Focus();
                PopupInfoDialog("Select customer first", false);
                return;
            }

            SearchByGroupList(this.OrderListDialogListTextBox.Text);
        }

        protected void SearchByOrderListDialogVerifyButton_OnClick(object sender, EventArgs e)
        {
            var checkOrderCodeModels = CheckOrderCodeModels(this.OrderListDialogListTextBox.Text, out var toDisplay);
            var isThereAnyOrdersToLoad = checkOrderCodeModels.Count > 0;
            var msg = isThereAnyOrdersToLoad
                ? $"Order codes verification for customer({this.CustomerCodeTextBox.Text}):"
                : $"There is no any order for selected customer({this.CustomerCodeTextBox.Text}):";
            PopupInfoDialog(msg, !isThereAnyOrdersToLoad, toDisplay);
        }

        private void SearchByGroupList(string rawText)
        {
            var checkOrderCodeModels = CheckOrderCodeModels(rawText, out _);
            this.ExtendedOrderSearchButton.Text = ExtendedOrderSearchButtonText + $@"({checkOrderCodeModels.Count})";
            if (string.IsNullOrEmpty(this.CustomerCodeTextBox.Text))
            {
                PopupInfoDialog("Set Customer, please", isErr: true);
                return;
            }

            var currentCustomerOrderCodes = checkOrderCodeModels
                .Where(m => m.GroupId != null && m.CustomerCode == int.Parse(this.CustomerCodeTextBox.Text))
                .Select(m => m.GroupCode)
                .ToList();
            if (currentCustomerOrderCodes.Count == 0)
            {
                var message = @"Request result is empty! Criteria:";
                var criteria = new List<string>
                {
                    $"Customer Code = {this.CustomerCodeTextBox.Text}",
                    $"Customer Name = {this.CustomerDropDownList.SelectedItem.Text}",
                    $"GroupState    = Any",
                    $"Group Codes   = {rawText.Replace(Environment.NewLine, ",")}",
                    $"ServiceTypes  = Any",
                    $"Create Date   = Any",
                    $"Memo          = Any",
                    $"PO            = Any"
                };
                PopupInfoDialog(message, isErr: true, messageList: criteria);
                return;
            }

            SearchByGroupList(currentCustomerOrderCodes);
        }

        private List<CheckOrderCodeModel> CheckOrderCodeModels(string rawText, out List<string> toDisplay)
        {
            var rawOrderCodes = ParseOrderList(rawText, out List<string> parseErrors);
            var checkOrderCodeModels =
                QueryAccountRep.CheckOrderCodes(rawOrderCodes, null, this);

            toDisplay = new List<string>();
            if (parseErrors.Count > 0)
            {
                toDisplay.Add("");
                toDisplay.Add("ERRORS:");
                toDisplay.Add("");
                toDisplay.AddRange(parseErrors);
            }

            if (rawOrderCodes.Count > 0)
            {
                toDisplay.Add("");
                toDisplay.Add("PARSE RESULT:");
                toDisplay.Add("-------------");
                toDisplay.AddRange(rawOrderCodes.Select(i => i.ToString()));
            }

            if (checkOrderCodeModels.Count > 0)
            {
                toDisplay.Add("");
                toDisplay.Add($"VALIDATE ORDER CODES:");
                toDisplay.Add("----------------------");
                toDisplay.AddRange(checkOrderCodeModels
                    .Select(m => $"{m.GroupCode} - " +
                                 $"{(m.Found ? "Found" : "NotFound")}" +
                                 $"{(m.Found ? " - Customer:" + m.CustomerCode : "")}"));
            }

            return checkOrderCodeModels;
        }

        private void SearchByGroupList(List<int> orderCodesForSearch)
        {
            var started = DateTime.Now;
            try
            {
                LoadOrdersTreeViewByGroupCodes(orderCodesForSearch);
                OrderTreeLoaded();

                InitAdditionalServicesDataGrid();

                if (OrdersTreeView.Nodes.Count > 0)
                {
                    var groupModel = JsonConvert.DeserializeObject<TreeNodeGroupModel>(OrdersTreeView.Nodes[0].Value);
                    if (groupModel == null)
                    {
                        throw new Exception("Internal Error");
                    }

                    var customerModel = GetCustomerModel(groupModel.CustomerCode.ToString());
                    if (customerModel == null)
                    {
                        throw new Exception("Internal Error");
                    }

                    this.CustomerCodeTextBox.Text = customerModel.CustomerCode;
                    if (this.OrdersTreeView.SelectedNode != null)
                    {
                        OrdersTreeView_OnSelectedNodeChanged();
                        AdjustCustomerDropDownList(customerModel.CustomerCode);
                        this.OrdersTreeView.SelectedNode.Expand();
                        ExpandAllAncestry(this.OrdersTreeView.SelectedNode);
                        EnsureOrderTreeSelectionVisible();
                    }

                    AdjustCreateDates();
                    AdjustServiceType();

                    InitMigratedItemsDataGrid(orderCodesForSearch.ToArray());
                    LoadMemoTextBoxAndDropDownList(groupState: null);
                }
                else
                {
                    InitMigratedItemsDataGrid();
                    InitCurrentBatchItems();
                    InitMeasuresDataGrid();

                    var message = $@"Request result is empty! '{OrderSearchFieldName}' = {this.OrderCodeTextBox.Text}";
                    PopupInfoDialog(message, isErr: false);
                }
            }
            catch (Exception exception)
            {
                LogUtils.UpdateSpLogWithException(exception, this);
                var message = $@"Search by order({this.OrderCodeTextBox.Text}) is failed!" +
                              $@"\nException={exception.Message}";
                PopupInfoDialog(message, isErr: false);
            }
            finally
            {
                AdjustOrderTreeStatusBar(started);
                AdjustOrdersTreeViewCheckedText();
                InitOrdersTreeCommands();
            }

            LogUtils.UpdateSpLog("SearchByGroupList() ended", this.Session);
        }

        private static List<int> ParseOrderList(string text, out List<string> errors)
        {
            var result = new List<int>();
            errors = new List<string>();
            var current = string.Empty;
            foreach (var c in text.ToCharArray())
            {
                if (char.IsDigit(c))
                {
                    current += c.ToString();
                }
                else
                {
                    AddNumber(errors, current, result);
                    current = string.Empty;
                }
            }

            AddNumber(errors, current, result);
            return result;
        }

        private static void AddNumber(List<string> errors, string current, List<int> result)
        {
            if (!string.IsNullOrEmpty(current))
            {
                if (int.TryParse(current, out var currentInt))
                {
                    result.Add(currentInt);
                }
                else
                {
                    errors.Add(current);
                }
            }
        }

        protected void SearchByOrderListDialogCancelButton_OnClick(object sender, EventArgs e)
        {
            this.SearchByOrderListDialogModalPopupExtender.Hide();
            var checkOrderCodeModels = CheckOrderCodeModels(this.OrderListDialogListTextBox.Text, out _);
            this.ExtendedOrderSearchButton.Text = ExtendedOrderSearchButtonText + $@"({checkOrderCodeModels.Count})";
        }

        protected void SearchByOrderListDialogUploadButton_OnClick(object sender, EventArgs e)
        {
            var fileName = Request?.Files["OrderListDialogAsyncFileUpload"]?.FileName;

            //if (OrderListDialogAsyncFileUpload.PostedFile == null ||
            //    OrderListDialogAsyncFileUpload.PostedFile.ContentLength <= 0)
            //{
            //    return;
            //}

            //using (var streamReader = new StreamReader(OrderListDialogAsyncFileUpload.PostedFile.InputStream))
            //{
            //    this.OrderListDialogListTextBox.Text = streamReader.ReadToEnd();
            //}
        }

        #endregion

        protected void ServiceTypeCategoryDropDown_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var category = (ServiceTypesCategory)Enum.Parse(typeof(ServiceTypesCategory),
                this.ServiceTypesCategoryDropDown.SelectedItem.Value);
            var isScreeningServices = category == ServiceTypesCategory.ScreeningServices;

            this.BulkBillingCheckBox.Checked = isScreeningServices;
            BulkBillingCheckBox_OnCheckedChanged(null, null);
            this.BulkBillingCheckBox.Visible = isScreeningServices;

            SetPageState();
        }

        public bool IsBulkBilling
        {
            get
            {
                var viewState = GetViewState(SessionConstants.IsBulkBilling);
                return viewState != null && (bool)viewState;
            }
            set => SetViewState(value, SessionConstants.IsBulkBilling);
        }

        protected void BulkBillingCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            IsBulkBilling = this.BulkBillingCheckBox.Checked;
        }

        protected void BatchLabelProperties_OnCommand(object sender, CommandEventArgs e)
        {
            PopupBatchLabelPropertiesDialog();
        }

        private void PopupBatchLabelPropertiesDialog()
        {
            var checkedBatches = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == BatchDepth)
                .Select(GetTreeNodeModel<TreeNodeBatchModel>)
                .ToList();

            if (checkedBatches.Count == 0)
            {
                PopupInfoDialog("Select any batch first.", false);
                return;
            }

            var checkedBatch = checkedBatches[0];
            var batchNumberFormatted = Utils.FullBatchNumber(checkedBatch.GroupCode, checkedBatch.BatchCode, true);

            var isIsSkuOnly = QueryAccountRep.IsIsSkuOnly(checkedBatch.GroupCode, checkedBatch.BatchCode, this);
            if (!isIsSkuOnly)
            {
                PopupInfoDialog($"No additional instructions for batch \"{batchNumberFormatted}\". (Not \"LE\" order).", true);
                return;
            }

            var title = "Batch Label Properties";
            
            BatchLabelEditLabel.Text = title;
            BatchLabelEditBatchNumberLabel.Text = batchNumberFormatted;

            var skuRulesDataPerBatchModels = QueryAccountRep.GetSkuRulesDataPerBatch(checkedBatch.GroupCode, checkedBatch.BatchCode, this);

            int total = QueryAccountRep.GetBatchPassedFailedTotal(checkedBatch.GroupCode, checkedBatch.BatchCode, this);
            int passed = QueryAccountRep.GetBatchPassed(checkedBatch.GroupCode, checkedBatch.BatchCode, 0, this);
            var failPassValue = (total - passed) + "/" + passed;

            var failPassItem = new SkuRulesDataPerBatchModel(
                skuRulesDataPerBatchModels[0].ResultValue, failPassValue);
            if (skuRulesDataPerBatchModels.Count > 0 && skuRulesDataPerBatchModels[0].ResultValue == "Fail/Pass")
            {
                skuRulesDataPerBatchModels[0] = failPassItem;
            }
            else
            {
                if (skuRulesDataPerBatchModels.Count > 0)
                {
                    skuRulesDataPerBatchModels.Insert(0, failPassItem);
                }
                else
                {
                    skuRulesDataPerBatchModels.Add(failPassItem);
                }
            }

            var dialogModel = new BatchLabelDialogModel
            {
                Batch = checkedBatch,
                Title = title,
                Properties = skuRulesDataPerBatchModels
            };

            SetViewState(dialogModel, SessionConstants.BatchLabelDialogModel);
            var dataSource = dialogModel.Properties;
            BatchLabelEditGridView.DataSource = dataSource;
            BatchLabelEditGridView.DataBind();

            BatchLabelEditModalPopupExtender.Show();
            // Close -> BatchLabelEditOkButton_OnClick(...)
        }

        protected void BatchLabelEditCloseButton_OnClick(object sender, EventArgs e)
        {
            this.BatchLabelEditModalPopupExtender.Hide();
        }

        protected void SetOrdersPrinted_Click(object sender, CommandEventArgs e)
        {
            bool validated = ValidateOrdersPrinted(OrderCodeTextBox.Text.ToString().Trim());
            if (validated)
            {
                int orderCode = int.Parse(OrderCodeTextBox.Text.ToString().Trim());
            }
            else
            {
                ErrPrintedOrders.Text = "Error validating reports for order " + OrderCodeTextBox.Text.Trim();
            }

        }
        protected bool ValidateOrdersPrinted(string order)
        {
            string msg = "";
            var checkedBatches = OrdersTreeView.CheckedNodes.Cast<TreeNode>()
                .Where(treeNode => treeNode.Depth == BatchDepth)
                .Select(GetTreeNodeModel<TreeNodeBatchModel>)
                .ToList();

            if (checkedBatches.Count == 0)
            {
                PopupInfoDialog("Select any batch first.", false);
                return false;
            }

            foreach (var checkedBatch in checkedBatches)
            {

                var batchNumberFormatted = Utils.FullBatchNumber(checkedBatch.GroupCode, checkedBatch.BatchCode, true);

                var isIsSkuOnly = QueryAccountRep.IsIsSkuOnly(checkedBatch.GroupCode, checkedBatch.BatchCode, this);
                if (!isIsSkuOnly)
                {
                    msg += batchNumberFormatted + ",";
                    continue;
                }
                string sku = QueryUtils.GetSkuByBatch(checkedBatch.GroupCode, checkedBatch.BatchCode, this);
                if (sku != null && sku != "failed")
                {
                    bool reportFound = FindReportBySku(sku);
                    if (reportFound)
                        QueryUtils.SetDocumentPrintedByBatch(checkedBatch.GroupCode, checkedBatch.BatchCode, this);
                    else
                        msg += batchNumberFormatted + ",";
                }
                else
                {
                    msg += batchNumberFormatted + ",";
                    continue;
                }
            }
            if (msg != "")
            {
                msg = "The following batches don't have valid sku: " + msg;
                ErrPrintedOrders.Text = msg;
            }
            return true;
        }
        protected bool FindReportBySku(string sku)
        {
            //int lmKey = 0;
            string url = @"https://wg.gemscience.net/PDF/" + sku + @".pdf";
            WebRequest request = WebRequest.Create(new Uri(url));
            request.Method = "HEAD";
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    return true;//found
                }
            }
            catch
            {
                return false;
            }
        }


        protected void PrintLabels_OnCommand(object sender, CommandEventArgs e)
        {
            ErrPrintedOrders.Text = "";
            string machineName = null;
            if (Session["MachineName"] != null)
                machineName = Session["MachineName"].ToString();
            else
            {
                ErrPrintedOrders.Text = "No machine name to print";
                return;
            }
            var checkedNodes = OrdersTreeView.CheckedNodes;
            foreach (TreeNode node in checkedNodes)
            {
                if (node.Depth != 2)//item only
                    continue;
                var itemModel = GetTreeNodeModel<TreeNodeItemModel>(node);
                //List<DocumentModel> itemData = QueryCpUtilsNew.GetDefaultDocumentsByBatchId(itemModel.BatchId.ToString(), "8", this.Page);
                DataSet rejectDocData = QueryCpUtilsNew.GetRejectDocumentId(itemModel.BatchId.ToString(), itemModel.ItemCode.ToString(), "8", this.Page);
                bool rejected = true;
                string documentId = null, partName = null;
                if (rejectDocData == null)
                {
                    try
                    {
                        rejected = false;
                        List<DocumentModel> itemData = QueryCpUtilsNew.GetDefaultDocumentsByBatchId(itemModel.BatchId.ToString(), "8", this.Page);
                        if (itemData.Count > 0)
                            documentId = itemData[0].DocumentId;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        documentId = rejectDocData.Tables[1].Rows[0]["DocumentID"].ToString();
                        partName = rejectDocData.Tables[0].Rows[0]["PartName"].ToString();
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                }
                if (documentId == null)
                {
                    ErrPrintedOrders.Text = "No LBL document found";
                    return;
                }
                List<ItemValueModel> itemValues = QueryUtils.GetItemValues(itemModel.GroupCode, itemModel.BatchCode, itemModel.ItemCode, this.Page);
                List<DocStructModel> docStructure = QueryUtils.GetDocStructure(documentId, this.Page);
                string brackets = "[";
                DataTable dataInputs = new DataTable("FinalLabel");
                dataInputs.Columns.Add("Measure0", typeof(String));
                dataInputs.Columns.Add("Measure1", typeof(String));
                dataInputs.Columns.Add("Measure2", typeof(String));
                dataInputs.Columns.Add("Measure3", typeof(String));
                dataInputs.Columns.Add("Measure4", typeof(String));
                dataInputs.Columns.Add("Measure5", typeof(String));
                dataInputs.Columns.Add("Measure6", typeof(String));
                dataInputs.Columns.Add("Measure7", typeof(String));
                dataInputs.Columns.Add("Measure8", typeof(String));
                dataInputs.Columns.Add("Measure9", typeof(String));
                dataInputs.Columns.Add("Measure10", typeof(String));
                dataInputs.Columns.Add("Measure11", typeof(String));
                dataInputs.Columns.Add("Measure12", typeof(String));
                dataInputs.Columns.Add("Measure13", typeof(String));
                dataInputs.Columns.Add("MachineName", typeof(String));
                dataInputs.Columns.Add("NewItemNumber", typeof(String));
                dataInputs.Columns.Add("CreateDate", typeof(string));
                dataInputs.Columns.Add("QRCODE", typeof(string));
                dataInputs.Columns.Add("OldItemNumber", typeof(string));
                dataInputs.Columns.Add("BARCODE", typeof(string));


                DataRow row;
                row = dataInputs.NewRow();
                row["MachineName"] = machineName;
                string newItemNumber =  Utils.FillToFiveChars(itemModel.GroupCode.ToString()) + Utils.FillToThreeChars(itemModel.BatchCode.ToString(), itemModel.GroupCode.ToString()) + 
                    Utils.FillToTwoChars(itemModel.ItemCode.ToString());
                row["NewItemNumber"] = newItemNumber;
                string oldItemNumber = itemValues[0].OldItemNumber;
                row["OldItemNumber"] = oldItemNumber;
                int i = 0;
                bool qrCodeExists = false;
                string colName = "";
                foreach (var doc in docStructure)
                {
                    if (doc.Title.Contains("Empty"))
                        doc.Title = "";
                    
                    if (doc.Title != "" && ((doc.Title.Trim().ToLower()).Contains("$itemid") || (doc.Title.Trim().ToLower()).Contains("$skip")))
                        continue;
                    if (doc.Value.Trim() != "")
                    {
                        if (rejected && doc.Value.Contains("partname."))
                            doc.Value = doc.Value.Replace("partname.", partName + ".");
                        string value = ReplaceBracketsWithValues(itemValues, doc.Value, itemModel.BatchId.ToString(), itemModel.ItemCode.ToString(), itemModel.GroupCode.ToString(), itemModel.BatchCode.ToString());
                        if (value == "error")
                            return;
                        if (doc.Title.Trim() != "")
                            value = doc.Title + ":" + value;
                        if (doc.Title.ToLower().Contains("qrcode"))
                        {
                            colName = "QRCODE";
                            if (value.Contains(@"$QRCODE:"))
                                value = value.Replace(@"$QRCODE:", "").Trim();
                            if (value.Contains(@"HTTPS://wg.gemscience.net/vr/veri.aspx?"))
                                value = value.Replace(@"HTTPS://wg.gemscience.net/vr/veri.aspx?", "").Trim();
                            row["QRCODE"] = value;
                            qrCodeExists = true;
                            continue;
                        }
                        if (doc.Title.ToLower().Contains("barcode"))
                        {
                            colName = "BARCODE";
                            if (value.Contains(@"$BARCODE:"))
                                value = value.Replace(@"$BARCODE:", "").Trim();
                            row["BARCODE"] = value;
                            continue;
                        }
                        value = value.Replace("VERY GOOD", "VG").Replace("IDEAL", "ID").Replace("GOOD", "G").Replace("EXCELLENT", "EX").Replace("FAIR", "F").Replace("N/A", "NA");
                        value = value.Replace("Very Good", "VG").Replace("Ideal", "ID").Replace("Good", "G").Replace("Excellent", "EX").Replace("Fair", "F");
                        value = value.Replace(" %", "%").Replace(" " + Convert.ToChar(176).ToString(), Convert.ToChar(176).ToString());
                        value = value.Replace("SQUARE", "SQ.").Replace("MODIFIED", "MOD.");
                        value = value.Replace("Square", "Sq.").Replace("Modified", "Mod.").Replace("Slightly", "SL.");
                        value = value.Replace("DATA", "").Trim();
                        colName = "Measure" + i.ToString();
                        if (value == "")
                            continue;
                        if (value.Contains(""))
                        if (value != "Empty:")
                            row[colName] = value;
                        i++;
                        if (i > 13)
                            break;
                    }
                }
                if (!qrCodeExists)
                {
                    colName = "QRCODE";
                    string qrCodeValue =  @"[Item Container.Item #]";
                    string value = ReplaceBracketsWithValues(itemValues, qrCodeValue, itemModel.BatchId.ToString(), itemModel.ItemCode.ToString(), itemModel.GroupCode.ToString(), itemModel.BatchCode.ToString());
                    if (value == "error")
                        return;
                    //value = @"HTTPS://wg.gemscience.net/vr/veri.aspx?" + value;
                    row[colName] = value;
                } 
                 dataInputs.Rows.Add(row);
                using (MemoryStream ms = new MemoryStream())
                {
                    dataInputs.WriteXml(ms);
                    bool sent = QueryUtils.SendMessageToStorage(ms, this, "FinalLabel");

                }
            }    
        }
        protected string ParseDocValue(string docValue)
        {
           
            return null;
        }
        protected String ReplaceBracketsWithValues(List<ItemValueModel> itemValues, String sBrackets, String sBatchID, String sItemCode, String sGroupCode, String sBatchCode)
        {
            ErrPrintedOrders.Text = "";
            String rexBracket = @"\[[^]^[]{1,}\.[^]^[]{1,}\]";
            MatchCollection matches = Regex.Matches(sBrackets, rexBracket);
            StringBuilder exceptionMessage = new StringBuilder();
            bool first = true;
            foreach (Match match in matches)
            {
                String sMatch = match.ToString();
                sMatch = sMatch.Remove(0, 1);
                sMatch = sMatch.Remove(sMatch.Length - 1, 1);

                string[] asParts = sMatch.Split(new char[] { '.' });
                ItemValueModel valueRow = itemValues.Find(m => m.PartName.ToLower() == asParts[0].ToLower() && m.MeasureName.ToLower() == asParts[1].ToLower());
                if (valueRow == null)//exception
                {
                    ErrPrintedOrders.Visible = true;
                    ErrPrintedOrders.Text = "[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]" + " doesn't exist for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.";
                    return "error";
                    throw new Exception("[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]" + " doesn't exist for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.");
                }
                if (valueRow.ResultValue == null)
                { 
                    if (!first)
                    {
                        exceptionMessage.Append(", ");
                    }
                    else
                    {
                        first = false;
                    }
                    exceptionMessage.Append("[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]");
                }
                sBrackets = sBrackets.Replace(match.ToString(), valueRow.ResultValue);

            }
            if (exceptionMessage.Length != 0)
            {
                ErrPrintedOrders.Visible = true;
                ErrPrintedOrders.Text = exceptionMessage.ToString() + " is empty for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.";
                return "error";
                throw new Exception(exceptionMessage.ToString() + " is empty for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.");
            }
            return sBrackets;
        }

        protected void PrintLELabels_OnCommand(object sender, CommandEventArgs e)
        {
            ErrPrintedOrders.Text = "";
            
            string machineName = null;
            if (Session["MachineName"] != null)
                machineName = Session["MachineName"].ToString();
            else
            {
                ErrPrintedOrders.Text = "No machine name to print";
                return;
            }
            var checkedNodes = OrdersTreeView.CheckedNodes;
            foreach (TreeNode node in checkedNodes)
            {
                if (node.Depth != 1)//batch only
                    continue;
                var itemModel = GetTreeNodeModel<TreeNodeItemModel>(node);
                var isIsSkuOnly = QueryAccountRep.IsIsSkuOnly(itemModel.GroupCode, itemModel.BatchCode, this);
                if (!isIsSkuOnly)
                {
                    PopupInfoDialog($"No LE labels for order " + itemModel.GroupCode + ", batch " + itemModel.BatchCode + ". Not LE batch.", true);
                    return;
                }
                string sku = QueryUtils.GetSkuByBatch(itemModel.GroupCode, itemModel.BatchCode, this);
                //List<DocumentModel> itemData = QueryCpUtilsNew.GetDefaultDocumentsByBatchId(itemModel.BatchId.ToString(), "8", this.Page);
                //string documentId = "";
                //if (itemData.Count > 0)
                //{
                //    documentId = itemData[0].DocumentId;
                //}
                //List<ItemValueModel> itemValues = QueryUtils.GetItemValues(itemModel.GroupCode, itemModel.BatchCode, itemModel.ItemCode, this.Page);
                //List<DocStructModel> docStructure = QueryUtils.GetDocStructure(documentId, this.Page);
                //string brackets = "[";
                DataTable dataInputs = new DataTable("FinalLabelLE");
                dataInputs.Columns.Add("SKU", typeof(String));
                dataInputs.Columns.Add("Message", typeof(String));
                dataInputs.Columns.Add("LabelCount", typeof(String));
                DataRow row;
                row = dataInputs.NewRow();
                row["SKU"] = sku;
                row["Message"] = "GSI Graded";
                if (LELabelsCount.Text != "")
                    row["LabelCount"] = LELabelsCount.Text;
                else
                    row["LabelCount"] = "1";
                dataInputs.Rows.Add(row);
                using (MemoryStream ms = new MemoryStream())
                {
                    dataInputs.WriteXml(ms);
                    bool sent = QueryUtils.SendMessageToStorage(ms, this, "FinalLabel");

                }
            }
        }

        protected void UpdateOrderButton_Click(object sender, EventArgs e)
        {

        }
        //protected String ReplaceBracketsWithValues(List<ItemValueModel> itemValues,String sBrackets, String sBatchID, String sItemCode, String sGroupCode, String sBatchCode)
        //{
        //    DataSet dsResults;
        //    DataSet dsMeasureValue = new DataSet();

        //    dsMeasureValue.Tables.Add("MeasureValueByPart");
        //    dsMeasureValue.Tables[0].Columns.Add("BatchID");
        //    dsMeasureValue.Tables[0].Columns.Add("ItemCode");
        //    dsMeasureValue.Tables[0].Columns.Add("PartName");
        //    dsMeasureValue.Tables[0].Columns.Add("MeasureTitle");
        //    dsMeasureValue.Tables[0].Columns.Add("CutGrade");
        //    dsMeasureValue.Tables[0].Rows.Add(dsMeasureValue.Tables[0].NewRow());

        //    dsMeasureValue.Tables[0].Rows[0]["BatchID"] = sBatchID;
        //    dsMeasureValue.Tables[0].Rows[0]["ItemCode"] = sItemCode;
        //    dsMeasureValue.Tables[0].Rows[0]["CutGrade"] = 0;

        //    //String rexBracket = @"(\[\w{0,}\W{0,}\w{0,}\.\w{0,}\W{0,}\w{0,}\({0,1}\w{0,}\){0,1}\])";
        //    //String rexBracket = @"\[(\w{0,}\s?(\(\w{0,}\))?){0,}\.(\w{0,}\s?(\(\w{0,}\))?){0,}\]";
        //    String rexBracket = @"\[[^]^[]{1,}\.[^]^[]{1,}\]";

        //    MatchCollection matches = Regex.Matches(sBrackets, rexBracket);

        //    Object[,] aoReplacements = new Object[2, matches.Count];

        //    int i = 0;
        //    StringBuilder exceptionMessage = new StringBuilder();
        //    bool first = true;
        //    foreach (Match match in matches)
        //    {
        //        String sMatch = match.ToString();
        //        sMatch = sMatch.Remove(0, 1);
        //        sMatch = sMatch.Remove(sMatch.Length - 1, 1);

        //        string[] asParts = sMatch.Split(new char[] { '.' });
        //        dsMeasureValue.Tables[0].Rows[0]["PartName"] = asParts[0];
        //        dsMeasureValue.Tables[0].Rows[0]["MeasureTitle"] = asParts[1];
        //        ItemValueModel valueRow = itemValues.Find(m => m.PartName == asParts[0] && m.MeasureName == asParts[1]);
        //        dsResults = QueryUtils.GetMeasureValueByPart(dsMeasureValue, this.Page);//Procedure dbo.spGetMeasureValueByPart
        //        if (dsResults.Tables[0].Rows.Count == 0)
        //        {
        //            throw new Exception("[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]" + " doesn't exist for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.");
        //        }
        //        aoReplacements[0, i] = match;
        //        aoReplacements[1, i] = dsResults.Tables[0].Rows[0][0].ToString();

        //        if (dsResults.Tables[0].Rows[0][0].ToString().Equals("exception"))
        //        {
        //            //throw new Exception( "[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]" + " is empty for item {"+sGroupCode+"."+sGroupCode+"."+sBatchCode+"."+sItemCode+"}. Label will not be printed.");
        //            if (!first)
        //            {
        //                exceptionMessage.Append(", ");
        //            }
        //            else
        //            {
        //                first = false;
        //            }
        //            exceptionMessage.Append("[" + asParts[0].ToString() + "." + asParts[1].ToString() + "]");
        //        }
        //        i++;
        //         //sBrackets = sBrackets.Replace(match.ToString(), dsResults.Tables[0].Rows[0][0].ToString());
        //        sBrackets = sBrackets.Replace(match.ToString(), valueRow.ResultValue);

        //    }
        //    if (exceptionMessage.Length != 0)
        //    {
        //        throw new Exception(exceptionMessage.ToString() + " is empty for item {" + sGroupCode + "." + sGroupCode + "." + sBatchCode + "." + sItemCode + "}. Label will not be printed.");
        //    }
        //    return sBrackets;
        //}

    }

    [Serializable]
    public class BatchLabelDialogModel
    {
        public string Title { get; set; }
        public TreeNodeBatchModel Batch { get; set; }
        public List<SkuRulesDataPerBatchModel> Properties { get; set; }
    }
}
