﻿using System;
using Corpt.Utilities;

namespace Corpt
{
    public partial class GetInvoiceNumberByOrderNumber : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack) txtOrderNumber.Focus();
        }

        protected void OnLookupClick(object sender, EventArgs e)
        {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
            var result = QueryUtils.GetInvoiceNumbers(txtOrderNumber.Text.Trim(), this);
            dgResult.DataSource = result;
            dgResult.DataBind();

        }
    }
}