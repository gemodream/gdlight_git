﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class PrintLabels : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: " + (IsCertifyMode() ? "Print Certified" : "Print Itemized");
            if (!IsPostBack)
            {
                TitleLabel.Text = IsCertifyMode() ? "Print Certified" : "Print Itemized";
                dgrCertLabels.Visible = true;// !IsCertifyMode();
            }

        }
        private bool IsCertifyMode()
        {
            return Request.Params["Mode"] == "Certified";
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            Page.Validate("BatchGroup");
            if (!Page.IsValid) return;
            InfoLabel.Text = "";
            var batchId = QueryUtils.GetBatchId(txtBatchNumber.Text.Trim(), this);
            if (String.IsNullOrEmpty(batchId))
            {
                InfoLabel.Text = "This batch number is invalid. Please check the batch number and try again.";
                return;
            }
            var docId = QueryUtils.GetDocumentId(new BatchModel {BatchId = batchId}, "8", this);
            if (string.IsNullOrEmpty(docId))
            {
                InfoLabel.Text = "Unable to print label for this batch. Please attach label template into Customer Program";
                return;
            }
            SetViewState(new ShortReportModel { BatchModel = new BatchModel { BatchId = batchId }, DocumentId = docId}, SessionConstants.PrintBatchModel);

            if (IsCertifyMode())
            {
                SetCertifiedLabels();
            } else
            {
                SetItemizedLabels();
            }
        }

        private void SetItemizedLabels()
        {

            var sFullBatchNumber = txtBatchNumber.Text.Trim() + "00";
            var order = "" + Utils.ParseOrderCode(sFullBatchNumber);
            var batch = "" + Utils.ParseBatchCode(sFullBatchNumber);
            var labels = QueryUtils.GetItemizedLabels(order, batch, this);
            dgrCertLabels.DataSource = labels;
            dgrCertLabels.DataBind();
            SetViewState(labels, SessionConstants.PrintLabelsList);
        }

        private void SetCertifiedLabels()
        {
            //-- Reset prev state
            dgrCertLabels.DataSource = null;
            dgrCertLabels.DataBind();
            SetViewState(null, SessionConstants.PrintLabelsList);
            InfoLabel.Text = "";

            //-- Get Batch Numbers
            var srModel = GetViewState(SessionConstants.PrintBatchModel) as ShortReportModel;
            if (srModel == null) return;
            var batchId = srModel.BatchModel.BatchId;
            var items = QueryUtils.GetItemsByBatchRealNumbers(batchId, this);
            if (items.Count == 0)
            {
                InfoLabel.Text = "This batch number is invalid. Please check the batch number and try again.";
                return;
            }
            //-- Document structure
            var docStruct = QueryUtils.GetDocStructure(srModel.DocumentId, this);
            if (docStruct.Count > 4)
            {
                InfoLabel.Text = "Incorrect Label Structure. Please correct report in the Customer Program.";
                return;
            }
            var labels = new List<LabelModel>();

            Session["LabelNoPrintList"] = new ArrayList();
            
            foreach(var item in items)
            {
                var label = new LabelModel
                {
                    BarCode = "*" + item.NewItemNumber + "*",
                    CombinedNumber = item.NewItemNumber + (item.NewItemNumber == item.OldItemNumber ? "" : "(" + item.OldItemNumber + ")"),

                };
                var col = 1;
                foreach (var dc in docStruct)
                {
                    var colValue = Utils.ParametersToValueNOHighlights(
                            dc.Value, batchId, item.ItemCode, this) + Utils.NullValue(dc.Unit);
                    if (col == 1)
                    {
                        label.Column1 = colValue;
                    }
                    if (col == 2)
                    {
                        label.Column2 = colValue;
                    }
                    if (col == 3)
                    {
                        label.Column3 = colValue;
                    }
                    if (col == 4)
                    {
                        label.Column4 = colValue;
                    }
                    col++;
                }
                labels.Add(label);
            }
            dgrCertLabels.DataSource = labels;
            dgrCertLabels.DataBind();
            var errList = Session["LabelNoPrintList"] as ArrayList;
            if (errList.Count > 0)
            {
                InfoLabel.Text = "Can not print labels - missing data.";
            }
            SetViewState(labels, SessionConstants.PrintLabelsList);
        }

        protected void OnDownloadClick(object sender, EventArgs e)
        {
            InfoLabel.Text = "";
            var labels = GetViewState(SessionConstants.PrintLabelsList) as List<LabelModel>;
            if (labels == null || labels.Count == 0) return;
            InfoLabel.Text = ExcelUtils.LabelsFileDownLoad(labels,
                    IsCertifyMode() ? SessionConstants.GlobalCertLabel : SessionConstants.GlobalItemizedLabel, this);

        }
    }
}