﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ItemCycle.aspx.cs" Inherits="Corpt.ItemCycle" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div style="height: 25px">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel runat="server" ID="MainPanel" >
            <ContentTemplate>
                <div class="demoheading">Old/New orders ##</div>
                <div>
                    <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="FindInputItemBtn">
                        <label>Old Number: </label>
                        <asp:TextBox runat="server" ID="InputItemFld" MaxLength="11"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                            ValidChars="0123456789" TargetControlID="InputItemFld" Enabled="True" />
                        <label>New number: </label>
                        <asp:TextBox runat="server" ID="NewItemNumberFld" ReadOnly="true"></asp:TextBox>
                        <asp:Button runat="server" ID="FindInputItemBtn" OnClick="OnFindItemBtnClick" CssClass="btn btn-info btn-small"
                            Text="Lookup" />
                    </asp:Panel>
                </div>
                <br />
                <div>
                    <asp:GridView ID="GridItems" runat="server" AutoGenerateColumns="false" class="table-bordered"
                        CellPadding="5" OnRowDataBound="OnRowDataBound">
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <Columns>
                            <asp:BoundField HeaderText="BatchId" DataField="BatchId" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="#" DataField="FullItemNumberWithDotes" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="Create Date" DataField="CreateDate" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
