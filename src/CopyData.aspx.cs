using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;

namespace Corpt
{
	/// <summary>
	/// Summary description for CopySht.
	/// </summary>
	public partial class CopyData : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ID"] == null)
				Response.Redirect("Login.aspx");
			if (!IsPostBack)
			{
				lblMessage.Text = "";
//				cmdCopy.Attributes.Add("onclick", "return confirm('Are you sure you want to copy?');");

//				txtItemNumber.Attributes.Add("onKeyPress", "return doClick('" + cmdLoad.ClientID + "', event);");
//				txtDest.Attributes.Add("onKeyPress", "return doClick('" + cmdLoadDest.ClientID + "', event);");

				//txtItemNumber.Attributes.Add("onkeypress", "return doClick(event,'" + cmdLoad.ClientID + "')");
			
			}
//			ClrScreen(true);
			//txtItemNumber.
//			Item srcItem;
//			int srcItemTypeID = 0;
//			int srcItemPartID = 0;
//			int srcPartTypeID = 0;
//			DataTable srcItemStructure = new DataTable();
//			Item dstItem;
//			int dstItemTypeID = 0;
//			int dstItemPartID = 0;
//			int dstPartTypeID = 0;
//
//			DataTable dstItemStructure = new DataTable();
			try
			{
//				if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{10}$|^\d{11}$")) return; 
//				else
//				{
//					cmdLoad_Click(sender, System.EventArgs.Empty);
//					
//					object mySession = Page.Session;
//					return;
//				}
				//				else
				//				{
				//					try
				//					{
				//						srcItem = Session["SrcItem"] as Item;
				//
				//						srcItemTypeID = int.Parse(Session["SourceItemTypeID"].ToString());
				//						srcItemPartID = int.Parse(sourceTree.GetNodeFromIndex(sourceTree.SelectedNodeIndex).NodeData.ToString());
				//						srcPartTypeID = int.Parse(srcItemStructure.Select("PartID = " + srcItemPartID.ToString())[0].ItemArray[srcItemStructure.Columns.IndexOf("PartTypeID")].ToString());
				//						srcItemStructure = Session["ItemStructure_"+srcItemTypeID.ToString()] as DataTable;
				//					}
				//					catch{}
				//					try
				//					{
				//						dstItem = Session["DstItem"] as Item;
				//						dstItemTypeID = int.Parse(Session["DestItemTypeID"].ToString());
				//						dstItemPartID = int.Parse(destTree.GetNodeFromIndex(destTree.SelectedNodeIndex).NodeData.ToString());
				//					   	dstItemStructure = Session["ItemStructure_"+dstItemTypeID.ToString()] as DataTable;
				//					}
				//					catch{}
				//
				//					dstPartTypeID = int.Parse(dstItemStructure.Select("PartID = " + dstItemPartID.ToString())[0].ItemArray[dstItemStructure.Columns.IndexOf("PartTypeID")].ToString());
				//					if(srcPartTypeID==dstPartTypeID)
				//					{
				//						Image1.ImageUrl="Images/left2right_g.jpg";
				//						Image1.Visible=true;
				//					}
				//					else
				//					{
				//						Image1.ImageUrl="Images/left2right.jpg";
				//						Image1.Visible=true;
				//					}
				//				}
			}
			catch(Exception ex)
			{
				
				Image1.Visible=false;
			}
		}		
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.sourceTree.SelectedIndexChange += new Microsoft.Web.UI.WebControls.SelectEventHandler(this.sourceTree_SelectedIndexChange);
			this.destTree.SelectedIndexChange += new Microsoft.Web.UI.WebControls.SelectEventHandler(this.destTree_SelectedIndexChange);

		}
		#endregion

		private void ClrScreen(bool bFullClean)
		{
			ClearTree(sourceTree);
			lblSourceCustomer.Text = "";
			lblSource.Text = "";
			sourceDataGrid.DataSource = null;
			sourceDataGrid.DataBind();
			lblMessage.Text = "";
			ClearTree(destTree);
			lblDestCustomer.Text = "";
			DestDataGrid.DataSource = null;
			DestDataGrid.DataBind();
			lblDest.Text = "";
			if (bFullClean)
			{
				txtItemNumber.Text = "";
				txtDest.Text = "";
			}
		}

		protected void cmdLoad_Click(object sender, System.EventArgs e)
		{
			LoadSourceItem();
			return;
			ClearTree(sourceTree);
			lblSourceCustomer.Text = "";
			lblSource.Text = "";
			sourceDataGrid.DataSource = null;
			sourceDataGrid.DataBind();
			lblMessage.Text = "";

			if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{10}$|^\d{11}$"))
//				if(txtItemNumber.Text.Trim().Length!=10)
			{
				lblMessage.Text="Wrong source item number format: " + txtItemNumber.Text.Trim(); 
				Panel1.Visible=false;
				return;
			}

			lblSource.Text=txtItemNumber.Text.Trim();
			
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value= int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value= int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState",DBNull.Value);
			command.Parameters.Add("@EGroupState",DBNull.Value);
			command.Parameters.Add("@BState",DBNull.Value);
			command.Parameters.Add("@EState",DBNull.Value);
			command.Parameters.Add("@BDate",DBNull.Value);
			command.Parameters.Add("@EDate",DBNull.Value);
			command.Parameters.Add("@IsNew", SqlDbType.Int).Value=1;  
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value = Utils.ParseOrderCode(txtItemNumber.Text.Trim());
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value = Utils.ParseBatchCode(txtItemNumber.Text.Trim());
			command.Parameters.Add("@ItemCode",SqlDbType.Int).Value = Utils.ParseItemCode(txtItemNumber.Text.Trim());

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			int itemTypeID;
			string sOldNumber;
			if(dt.Rows.Count>0)
			{
				Item srcItem = new Item();
				srcItem.ItemCode = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewItemCode")].ToString());
				srcItem.BatchID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewBatchID")].ToString());
				srcItem.OfficeID= int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString());
				Session["SrcItem"] = srcItem;
				itemTypeID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("ItemTypeID")].ToString());
				//lblSourceCustomer.Text="CC: " + dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerCode")].ToString();
				sOldNumber =Utils.FillToFiveChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToThreeChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevBatchCode")].ToString(), dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToTwoChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevItemCode")].ToString());
				if(sOldNumber != lblSource.Text) lblSource.Text = lblSource.Text + "/" + sOldNumber;			
				lblSourceCustomer.Text=GetCustomer(int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerID")].ToString()),int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString()));
				Session["SourceItemTypeID"]=itemTypeID;
				//				lblItemType.Text=itemTypeID.ToString();
				FillTree(sourceTree,BuildItemStructure(itemTypeID));
			}
			else
			{
				lblSource.Text = "Item # " + txtItemNumber.Text.Trim() + " does not exist";
				lblSourceCustomer.Text = "";
				ClearTree(sourceTree);
			}

//			DataGrid1.DataSource=dt;
//			DataGrid1.DataBind();
		}

		public ItemPart BuildItemStructure(int itemTypeID)
		{
			//exec spGetPartsByItemType 
			//			@AuthorId=19, 
			//			@AuthorOfficeId=1, 
			//			@ItemTypeID=13

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetPartsByItemType");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@ItemTypeID", SqlDbType.Int).Value = itemTypeID;

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);

			Session["ItemStructure_"+itemTypeID.ToString()]=dt.Copy();
			
//			DataGrid2.DataSource=dt;
//			DataGrid2.DataBind();

			ItemPart rootItem = new ItemPart();
			DataRow[] dr = dt.Select("ParentPartID is NULL");
			if(dr.Length>0)
			{
				rootItem.PartName=dr[0].ItemArray[dt.Columns.IndexOf("PartName")].ToString();
				rootItem.PartTypeName=dr[0].ItemArray[dt.Columns.IndexOf("PartTypeName")].ToString();
				rootItem.PartId=int.Parse(dr[0].ItemArray[dt.Columns.IndexOf("PartID")].ToString());
				rootItem.PartTypeId=int.Parse(dr[0].ItemArray[dt.Columns.IndexOf("PartTypeID")].ToString());
				rootItem.ParentPart = null;
				itemPart(rootItem,dt);
			}
			return rootItem;
		}
		private void itemPart(ItemPart rootItem, DataTable dt)
		{			
			DataRow[] drs = dt.Select("ParentPartID = " + rootItem.PartId.ToString());
			foreach(DataRow dr in drs)
			{
				ItemPart subItem = new ItemPart();
				subItem.PartName=dr.ItemArray[dt.Columns.IndexOf("PartName")].ToString();
				subItem.PartTypeName=dr.ItemArray[dt.Columns.IndexOf("PartTypeName")].ToString();
				subItem.PartId=int.Parse(dr.ItemArray[dt.Columns.IndexOf("PartID")].ToString());
				subItem.PartTypeId=int.Parse(dr.ItemArray[dt.Columns.IndexOf("PartTypeID")].ToString());
				subItem.ParentPart=rootItem;
				itemPart(subItem,dt);
				rootItem.AddChild(subItem);
			}
		}
		private void ClearTree(Microsoft.Web.UI.WebControls.TreeView tView)
		{
			tView.Nodes.Clear();	
		}

		private void LoadSourceItem()
		{
			ClearTree(sourceTree);
			lblSourcePartName.Text = "";
			lblSourceCustomer.Text = "";
			lblSource.Text = "";
			sourceDataGrid.DataSource = null;
			sourceDataGrid.DataBind();
			lblMessage.Text = "";

			if(!Regex.IsMatch(txtItemNumber.Text.Trim(), @"^\d{10}$|^\d{11}$"))
				//				if(txtItemNumber.Text.Trim().Length!=10)
			{
				lblMessage.Text="Wrong source item number format: " + txtItemNumber.Text.Trim(); 
				Panel1.Visible=false;
				return;
			}

			lblSource.Text=txtItemNumber.Text.Trim();
			
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value= int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value= int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState",DBNull.Value);
			command.Parameters.Add("@EGroupState",DBNull.Value);
			command.Parameters.Add("@BState",DBNull.Value);
			command.Parameters.Add("@EState",DBNull.Value);
			command.Parameters.Add("@BDate",DBNull.Value);
			command.Parameters.Add("@EDate",DBNull.Value);
			command.Parameters.Add("@IsNew", SqlDbType.Int).Value=1;  
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value = Utils.ParseOrderCode(txtItemNumber.Text.Trim());
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value = Utils.ParseBatchCode(txtItemNumber.Text.Trim());
			command.Parameters.Add("@ItemCode",SqlDbType.Int).Value = Utils.ParseItemCode(txtItemNumber.Text.Trim());

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			int itemTypeID;
			string sOldNumber;
			if(dt.Rows.Count>0)
			{
				Item srcItem = new Item();
				srcItem.ItemCode = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewItemCode")].ToString());
				srcItem.BatchID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewBatchID")].ToString());
				srcItem.OfficeID= int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString());
				Session["SrcItem"] = srcItem;
				itemTypeID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("ItemTypeID")].ToString());
				//lblSourceCustomer.Text="CC: " + dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerCode")].ToString();
				sOldNumber =Utils.FillToFiveChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToThreeChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevBatchCode")].ToString(), dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToTwoChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevItemCode")].ToString());
				if(sOldNumber != lblSource.Text) lblSource.Text = lblSource.Text + "/" + sOldNumber;			
				lblSourceCustomer.Text=GetCustomer(int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerID")].ToString()),int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString()));
				Session["SourceItemTypeID"]=itemTypeID;
				//				lblItemType.Text=itemTypeID.ToString();
				FillTree(sourceTree,BuildItemStructure(itemTypeID));

				GetMeasures(sourceTree, Session["SrcItem"] as Item, sourceDataGrid, ref lblSourcePartName);	
			}
			else
			{
				lblSource.Text = "Item # " + txtItemNumber.Text.Trim() + " does not exist";
				lblSourceCustomer.Text = "";
				ClearTree(sourceTree);
			}

			//			DataGrid1.DataSource=dt;
			//			DataGrid1.DataBind();
		}
	
		private void FillTree(Microsoft.Web.UI.WebControls.TreeView tView, ItemPart rootItem)
		{
			tView.Nodes.Clear();
			Microsoft.Web.UI.WebControls.TreeNode root = new Microsoft.Web.UI.WebControls.TreeNode();
			root.Text=rootItem.PartName;
			root.NodeData=rootItem.PartId.ToString();
			FillNode(root,rootItem);
			tView.Nodes.Add(root);
			if (tView.Nodes.Count > 0)
			{
				foreach(Microsoft.Web.UI.WebControls.TreeNode node in tView.Nodes)
					node.Expanded = true;
				tView.Nodes[0].Checked = true;
			}
		
		}
		private void FillNode(Microsoft.Web.UI.WebControls.TreeNode rootNode, ItemPart rootItem)
		{
			if(rootItem.Children.Count>0)
			{
				foreach(ItemPart iPart in rootItem.Children)
				{
					Microsoft.Web.UI.WebControls.TreeNode tn = new Microsoft.Web.UI.WebControls.TreeNode();
					tn.NodeData=iPart.PartId.ToString();
					tn.Text=iPart.PartName;
					FillNode(tn,iPart);
					rootNode.Nodes.Add(tn);
					tn.Expanded = true;
				}
			}
		}

		protected void cmdLoadDest_Click(object sender, System.EventArgs e)
		{
			LoadDestItem();
			return;
			ClearTree(destTree);
			lblDestCustomer.Text = "";
			DestDataGrid.DataSource = null;
			DestDataGrid.DataBind();
			lblDest.Text = "";
			lblMessage.Text = "";

			if(!Regex.IsMatch(txtDest.Text.Trim(), @"^\d{10}$|^\d{11}$"))
//			if(txtDest.Text.Trim().Length!=10)
			{
				lblMessage.Text= "Wrong destination item number format: " + txtDest.Text.Trim();
				Panel1.Visible=false;
				return;
			}

			lblDest.Text=txtDest.Text.Trim();

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState",DBNull.Value);
			command.Parameters.Add("@EGroupState",DBNull.Value);
			command.Parameters.Add("@BState",DBNull.Value);
			command.Parameters.Add("@EState",DBNull.Value);
			command.Parameters.Add("@BDate",DBNull.Value);
			command.Parameters.Add("@EDate",DBNull.Value);
			command.Parameters.Add("@IsNew", SqlDbType.Int).Value=1;  
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value = Utils.ParseOrderCode(txtDest.Text.Trim());
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value = Utils.ParseBatchCode(txtDest.Text.Trim());
			command.Parameters.Add("@ItemCode",SqlDbType.Int).Value = Utils.ParseItemCode(txtDest.Text.Trim());

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			int itemTypeID;
			string sOldNumber;
			if(dt.Rows.Count>0)
			{
				Item dstItem = new Item();
				dstItem.ItemCode = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewItemCode")].ToString());
				dstItem.BatchID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewBatchID")].ToString());
				dstItem.OfficeID= int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString());
				Session["DstItem"] = dstItem;
				itemTypeID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("ItemTypeID")].ToString());
				sOldNumber =	Utils.FillToFiveChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
								Utils.FillToThreeChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevBatchCode")].ToString(), dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
								Utils.FillToTwoChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevItemCode")].ToString());
				if(sOldNumber != lblDest.Text) lblDest.Text = lblDest.Text + "/" + sOldNumber;	
				
				lblDestCustomer.Text=GetCustomer(int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerID")].ToString()),int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString()));
				
				Session["DestItemTypeID"]=itemTypeID;
				FillTree(destTree,BuildItemStructure(itemTypeID));

			}
			else
			{
				lblDest.Text = "Item # " + txtDest.Text.Trim() + " does not exist";
				lblDestCustomer.Text = "";
				ClearTree(destTree);
			}
		}
		private void LoadDestItem()
		{
			ClearTree(destTree);
			lblDestPartName.Text = "";
			lblDestCustomer.Text = "";
			DestDataGrid.DataSource = null;
			DestDataGrid.DataBind();
			lblDest.Text = "";
			lblMessage.Text = "";

			if(!Regex.IsMatch(txtDest.Text.Trim(), @"^\d{10}$|^\d{11}$"))
				//			if(txtDest.Text.Trim().Length!=10)
			{
				lblMessage.Text= "Wrong destination item number format: " + txtDest.Text.Trim();
				Panel1.Visible=false;
				return;
			}

			lblDest.Text=txtDest.Text.Trim();

			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetItemByCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value=int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value=int.Parse(Session["AuthorOfficeId"].ToString());
			command.Parameters.Add("@CustomerCode", DBNull.Value);
			command.Parameters.Add("@BGroupState",DBNull.Value);
			command.Parameters.Add("@EGroupState",DBNull.Value);
			command.Parameters.Add("@BState",DBNull.Value);
			command.Parameters.Add("@EState",DBNull.Value);
			command.Parameters.Add("@BDate",DBNull.Value);
			command.Parameters.Add("@EDate",DBNull.Value);
			command.Parameters.Add("@IsNew", SqlDbType.Int).Value=1;  
			command.Parameters.Add("@GroupCode",SqlDbType.Int).Value = Utils.ParseOrderCode(txtDest.Text.Trim());
			command.Parameters.Add("@BatchCode",SqlDbType.Int).Value = Utils.ParseBatchCode(txtDest.Text.Trim());
			command.Parameters.Add("@ItemCode",SqlDbType.Int).Value = Utils.ParseItemCode(txtDest.Text.Trim());

			SqlDataAdapter da = new SqlDataAdapter(command);
			DataTable dt = new DataTable();
			da.Fill(dt);
			int itemTypeID;
			string sOldNumber;
			if(dt.Rows.Count>0)
			{
				Item dstItem = new Item();
				dstItem.ItemCode = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewItemCode")].ToString());
				dstItem.BatchID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("NewBatchID")].ToString());
				dstItem.OfficeID= int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString());
				Session["DstItem"] = dstItem;
				itemTypeID = int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("ItemTypeID")].ToString());
				sOldNumber =	Utils.FillToFiveChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToThreeChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevBatchCode")].ToString(), dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevGroupCode")].ToString()) +
					Utils.FillToTwoChars(dt.Rows[0].ItemArray[dt.Columns.IndexOf("PrevItemCode")].ToString());
				if(sOldNumber != lblDest.Text) lblDest.Text = lblDest.Text + "/" + sOldNumber;	
				
				lblDestCustomer.Text=GetCustomer(int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerID")].ToString()),int.Parse(dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerOfficeID")].ToString()));
				
				Session["DestItemTypeID"]=itemTypeID;
				FillTree(destTree,BuildItemStructure(itemTypeID));
				
				GetMeasures(destTree, Session["dstItem"] as Item, DestDataGrid, ref lblDestPartName);	
			}
			else
			{
				lblDest.Text = "Item # " + txtDest.Text.Trim() + " does not exist";
				lblDestCustomer.Text = "";
				ClearTree(destTree);
			}
		}

		private ItemPart GetItemPartByIndex(String index)
		{
			String[] parts = index.Split('.');
			return null;
		}

		protected void cmdCopy_Click(object sender, System.EventArgs e)
		{
			Item srcItem = Session["SrcItem"] as Item;
			Item dstItem = Session["DstItem"] as Item;

			int srcItemTypeID = int.Parse(Session["SourceItemTypeID"].ToString());
			int dstItemTypeID = int.Parse(Session["DestItemTypeID"].ToString());

			int srcItemPartID = int.Parse(sourceTree.GetNodeFromIndex(sourceTree.SelectedNodeIndex).NodeData.ToString());
			int dstItemPartID = int.Parse(destTree.GetNodeFromIndex(destTree.SelectedNodeIndex).NodeData.ToString());

			DataTable srcItemStructure = Session["ItemStructure_"+srcItemTypeID.ToString()] as DataTable;
			DataTable dstItemStructure = Session["ItemStructure_"+dstItemTypeID.ToString()] as DataTable;

			int srcPartTypeID = int.Parse(srcItemStructure.Select("PartID = " + srcItemPartID.ToString())[0].ItemArray[srcItemStructure.Columns.IndexOf("PartTypeID")].ToString());
			int dstPartTypeID = int.Parse(dstItemStructure.Select("PartID = " + dstItemPartID.ToString())[0].ItemArray[dstItemStructure.Columns.IndexOf("PartTypeID")].ToString());
			if(srcPartTypeID!=dstPartTypeID)
			{
				lblMessage.Text="Different part types. Copying can't be done";
				Panel1.Visible=false;
			}
			else
			{
				try
				{
					SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

					SqlCommand command = new SqlCommand("sp_CopyItemPart2ItemPartByBatchIDItemCode");
					command.Connection = conn;
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@BatchID_From",SqlDbType.Int).Value = srcItem.BatchID;
					command.Parameters.Add("@ItemCodeFrom", SqlDbType.Int).Value = srcItem.ItemCode;
					command.Parameters.Add("@PartID_From", SqlDbType.Int).Value = srcItemPartID;

					command.Parameters.Add("@BatchID_To", SqlDbType.Int).Value = dstItem.BatchID;
					command.Parameters.Add("@ItemCodeTo", SqlDbType.Int).Value = dstItem.ItemCode;
					command.Parameters.Add("@PartID_To", SqlDbType.Int).Value = dstItemPartID;

					command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse(Session["ID"].ToString());
					command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse(Session["AuthorOfficeID"].ToString());
					command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = srcItem.OfficeID;
	
					command.CommandTimeout=0;
					conn.Open();
					command.ExecuteNonQuery();
					conn.Close();

//					lblMessage.Text="Copy done.";
//					Panel1.Visible=true;
				}

				catch(Exception ex)
				{
					lblMessage.Text="Can't copy. Some data are wrong or missing.";
					Panel1.Visible=false;
				}
			}
			sourceTree_SelectedIndexChange(sourceTree, null);
			destTree_SelectedIndexChange(destTree, null);
			lblMessage.Text = "Copy Successful";
			//	sourceTree_SelectedIndexChange
		}

		protected void cmdOK_Click(object sender, System.EventArgs e)
		{
			//Panel1.Visible=false;
			Response.Redirect("GDLight.aspx");
		}

		protected void cmdHome_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect("GDLight.aspx");		
		}
		
		private void sourceTree_SelectedIndexChange(object sender, Microsoft.Web.UI.WebControls.TreeViewSelectEventArgs e)
		{
            GetMeasures(sender as Microsoft.Web.UI.WebControls.TreeView, Session["SrcItem"] as Item, sourceDataGrid, ref lblSourcePartName);	
		}
		
		private void destTree_SelectedIndexChange(object sender, Microsoft.Web.UI.WebControls.TreeViewSelectEventArgs e)
		{
			GetMeasures(sender as Microsoft.Web.UI.WebControls.TreeView, Session["dstItem"] as Item, DestDataGrid, ref lblDestPartName);	
		}

		private void GetMeasures(Microsoft.Web.UI.WebControls.TreeView tree, Item item, DataGrid dg, ref System.Web.UI.WebControls.Label lblPartName)
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);
			
			SqlCommand command = new SqlCommand("spGetPartValueByBatchIDItemCode");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@BatchID", SqlDbType.Int).Value = item.BatchID;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = item.ItemCode;
			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse(Session["AuthorOfficeID"].ToString());

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(dt);
			dg.DataSource=dt;
			dg.DataBind();

			DataTable ddt = new DataTable("Values");
			ddt.Columns.Add(new DataColumn("Measure"));
			ddt.Columns.Add(new DataColumn("Value"));

			bool show = false;

			foreach(DataRow row in dt.Select("PartID = " + tree.GetNodeFromIndex(tree.SelectedNodeIndex).NodeData.Trim()))
			{
				DataRow displayRow = ddt.NewRow();
				displayRow["Measure"] = row["MeasureName"].ToString();
				displayRow["Value"] = row["ResultValue"].ToString();
				ddt.Rows.Add(displayRow);
				show = true;
			}
			//ddt.Compute("","order by Measure desc");
			dg.DataSource = ddt;
			dg.DataBind();
			dg.Visible = show;
			lblPartName.Text = "Part: " + tree.GetNodeFromIndex(tree.SelectedNodeIndex).Text;
		}

		private String GetCustomer(int CustomerID, int CustomerOfficeID)
		{
			SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

			SqlCommand command = new SqlCommand("spGetCustomer");
			command.Connection = conn;
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = CustomerOfficeID;
			command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CustomerID;
			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse(Session["ID"].ToString());
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse(Session["AuthorOfficeID"].ToString());
			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(dt);
			if(dt.Rows.Count>0)
			{
				return dt.Rows[0].ItemArray[dt.Columns.IndexOf("CustomerName")].ToString();
			}
			else
			{
				return "";
			}
		}

		protected void txtDest_TextChanged(object sender, System.EventArgs e)
		{
			LoadDestItem();
			//cmdLoadDest_Click(this, System.EventArgs.Empty);
		}

		protected void txtItemNumber_TextChanged(object sender, System.EventArgs e)
		{
			LoadSourceItem();
			//cmdLoad_Click(this, System.EventArgs.Empty);
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			LoadSourceItem();
		}
	}
}
