﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using System.Net;
using System.Data;

namespace Corpt
{
    public partial class DeletedItemsLookup : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Deleted Order/Batch Lookup";
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        //New report lookup routine - replace the slow one.

        #region Search By Item Number
        protected void OnSearchByItemNumberClick(object sender, ImageClickEventArgs e)
        {
            //HasDirectoryRoot();
            ErrLabel1.Text = "";
            if (!Regex.IsMatch(ItemNumberField.Text, @"^\d{5}$|^\d{6}$|^\d{7}$|^\d{8}$|^\d{9}$|^\d{10}$|^\d{11}$"))
            {
                ErrLabel1.Text = "Item number is incorrect.";
                ItemNumberField.Focus();
                return;

            }
            DataTable dt = QueryUtils.GetDeletedItems(ItemNumberField.Text, this);
            if (dt == null || dt.Rows.Count == 0)
                ErrLabel1.Text = "No Record Found";
            FileGrid.DataSource = dt;
            FileGrid.DataBind();
            //FindReport(findModel);
            //FindReport(findModel);
        }
        #endregion
        protected void ClearBtn_Click(object sender, EventArgs e)
        {
            ItemNumberField.Text = "";
            FileGrid.DataSource = null;
            FileGrid.DataBind();
        }
    }
}