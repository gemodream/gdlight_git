﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class TrackingDetails : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var filterModel = new TrackDetailsFilterModel
            {
                Mode = "" + Request.Params["p1"],
                Value = "" + Request.Params["p2"],
                CustomerId = Request.Params["p3"].Split('_')[0],
                OfficeId = Request.Params["p3"].Split('_')[1],
                DateFrom = Request.Params["p4"],
                DateTo = Request.Params["p5"],
            };
            SetViewState(filterModel, SessionConstants.TrackDetailsFilter);
            DisplayData();
        }
        private void DisplayData()
        {
            var filter = GetViewState(SessionConstants.TrackDetailsFilter) as TrackDetailsFilterModel;
            if (filter == null) return;
            var items = QueryUtils.GetTrackingDetails(filter, this);
            SetViewState(items, SessionConstants.TrackDetailsList);
            ApplyCurrentSort();
            if (filter.IsOrderMode)
            {
                ShowCarrierInfo(Int32.Parse(filter.Value));
            } else
            {
                CarrierInfoLabel.Text = "";
            }
            filterLabel.Text = "Tracking, " + filter.Description;
        }
        private void ApplyCurrentSort()
        {
            var trackings = GetViewState(SessionConstants.TrackDetailsList) as List<TrackingDetModel> ??
              new List<TrackingDetModel>();
            var table = new DataTable("TrackingDetails");

            table.Columns.Add(new DataColumn { ColumnName = "Order", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Memo", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Batch", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "Date", DataType = typeof(DateTime) });
            table.Columns.Add(new DataColumn { ColumnName = "CP", DataType = typeof(string) });
            foreach (var item in trackings)
            {
                table.Rows.Add(new object[] {item.OrderDisplay, item.MemoDisplay, item.BatchDisplay, item.LastModifiedDate, item.CpNameDisplay});
            }
            table.AcceptChanges();
            
            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "Order";
                SetViewState(new SortingModel { ColumnName = "Order", Direction = true }, SessionConstants.CurrentSortModel);
            }

            // Rebind data 
            grdBatches.DataSource = dv;
            grdBatches.DataBind();

        }
        private void ShowCarrierInfo(int orderCode)
        {
            var carrierModel = QueryUtils.GetCarrierInfo(orderCode, this);
            if (carrierModel == null)
            {
                CarrierInfoLabel.Text = "";
            } else
            {
                CarrierInfoLabel.Text = "<b>Delivery: </b>Received from " + carrierModel.CarrierDisplay;
            }
        }
        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            ApplyCurrentSort();
        }
    }
}