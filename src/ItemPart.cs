using System;
using System.Collections;

namespace Corpt
{
	/// <summary>
	/// 
	/// </summary>
	public class ItemPart
	{
		public ItemPart()
		{
			// 
			// TODO: Add constructor logic here
			//
			_Children = new ArrayList();
		}

		String _PartName;
		String _PartTypeName;
		int _PartId;
		int _PartTypeId;

		ItemPart _ParentPart;
		ArrayList _Children;


		public void AddChild(ItemPart child)
		{
			this._Children.Add(child);
		}
        
		public ArrayList Children
		{
			get
			{
				return _Children;
			}
			set
			{
				_Children = value;
			}
		}
		public String PartName
		{
			get
			{
				return _PartName;
			}
			set
			{
				_PartName = value;
			}
		}

		public String PartTypeName
		{
			get
			{
				return _PartTypeName;
			}
			set
			{
				_PartTypeName = value;
			}
		}

		public int PartId
		{
			get
			{
				return _PartId;
			}
			set
			{
				_PartId = value;
			}
		}

		public int PartTypeId
		{
			get
			{
				return _PartTypeId;
			}
			set
			{
				_PartTypeId = value;
			}
		}

		public ItemPart ParentPart
		{
			get
			{
				return _ParentPart;
			}
			set
			{
				_ParentPart = value;
			}
		}

//		public ArrayList Children
//		{
//			get
//			{
//				return _Children;
//			}
//			set
//			{
//				_Children = value;
//			}
//		}

	}
}
