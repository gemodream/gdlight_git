﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class GetFullOrder : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");

        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            var newShortReports = QueryUtils.GetFullOrder(OrderField.Text, this);
            newShortReports = newShortReports.FindAll(m => m.DocStructure.Count > 0);
            newShortReports.Sort((m1, m2) => Int32.Parse(m1.BatchModel.BatchCode).CompareTo(Int32.Parse(m2.BatchModel.BatchCode)));
            var oldShortReports = GetFullOrderOld();
            SetViewState(newShortReports, SessionConstants.TestFullOrderNew);
            SetViewState(oldShortReports, SessionConstants.TestFullOrderOld);
            ShowBatchList();
            ShowBatchCombo();
        }
        private void ShowBatchCombo()
        {
            var newList = GetViewState(SessionConstants.TestFullOrderNew) as List<ShortReportModel>;
            var oldList = GetViewState(SessionConstants.TestFullOrderOld) as List<ShortReportModel>;
            if (newList == null || oldList == null) return;
            var batchList = new List<BatchModel>();
            foreach(var newItem in newList)
            {
                var oldItem = oldList.Find(m => m.BatchModel.BatchId == newItem.BatchModel.BatchId);
                if (oldItem != null)
                {
                    var msg = "";
                    if (newItem.Items.Count != oldItem.Items.Count)
                    {
                        msg += (msg.Length > 0 ? ", " : "") + "Items";
                    }
                    if (newItem.MovedItems.Count != oldItem.MovedItems.Count)
                    {
                        msg += (msg.Length > 0 ? ", " : "") + "Moved Items";
                    }
                    if (newItem.ItemsFailed.Count != oldItem.ItemsFailed.Count)
                    {
                        msg += (msg.Length > 0 ? ", " : "") + "Failed Items";
                    }
                    if (newItem.ItemValues.Count != oldItem.ItemValues.Count)
                    {
                        msg += (msg.Length > 0 ? ", " : "") + "Items Values";
                    }
                    newItem.BatchModel.ErrMsg = msg;
                    batchList.Add(newItem.BatchModel);
                }
            }

            BatchCombo.DataTextField = "TestDisplay";
            BatchCombo.DataValueField = "BatchId";
            BatchCombo.DataSource = batchList;
            BatchCombo.DataBind();
            ShowBatchId();

        }
        private void ShowBatchList()
        {
            var newList = GetViewState(SessionConstants.TestFullOrderNew) as List<ShortReportModel>;
            var oldList = GetViewState(SessionConstants.TestFullOrderOld) as List<ShortReportModel>;
            if (newList == null || oldList == null) return;
            
            var table = new DataTable("Batches");
            table.Columns.Add("BatchId");
            table.Columns.Add("BatchCode");
            table.Columns.Add("DocId");
            
            foreach(var newItem in newList)
            {
                //var oldItem = oldList.Find(m => m.BatchModel.BatchId == newItem.BatchModel.BatchId);
                table.Rows.Add(new object[]
                                       {
                                           newItem.BatchModel.BatchId,
                                           newItem.BatchModel.BatchCode,
                                           newItem.DocumentId
                                       });
            }
            table.AcceptChanges();
            gridBatch.DataSource = table;
            gridBatch.DataBind();
        }
        private void ShowMoved()
        {
            var batchId = BatchCombo.SelectedValue;
            if (string.IsNullOrEmpty(batchId)) return;
            var newList = GetViewState(SessionConstants.TestFullOrderNew) as List<ShortReportModel>;
            var oldList = GetViewState(SessionConstants.TestFullOrderOld) as List<ShortReportModel>;
            if (newList == null || oldList == null) return;
            
            var newMoved = newList.Find(m => m.BatchModel.BatchId == batchId);
            var oldMoved = oldList.Find(m => m.BatchModel.BatchId == batchId);

            movedNewLbl.Text = "New: " + newMoved.MovedItems.Count + " rows";
            gridMovedNew.DataSource = newMoved.MovedItems;
            gridMovedNew.DataBind();

            movedOldLbl.Text = "Old: " + oldMoved.MovedItems.Count + " rows";
            gridMovedOld.DataSource = oldMoved.MovedItems;
            gridMovedOld.DataBind();

            spMovedOldLabel.Text = "wspvvGetItemsByBatchExistingNumbersOnly " + BatchCombo.SelectedValue + "; from Tables[1]";
            spMovedNewLabel.Text = "spGetFullOrder " + OrderField.Text + "; from Tables[5]";
        }
        private void ShowFailed()
        {
            var batchId = BatchCombo.SelectedValue;
            if (string.IsNullOrEmpty(batchId)) return;
            var newList = GetViewState(SessionConstants.TestFullOrderNew) as List<ShortReportModel>;
            var oldList = GetViewState(SessionConstants.TestFullOrderOld) as List<ShortReportModel>;
            if (newList == null || oldList == null) return;
            var newFaild = newList.Find(m => m.BatchModel.BatchId == batchId);
            var oldFaild = oldList.Find(m => m.BatchModel.BatchId == batchId);

            failedNewLbl.Text = "New: " + newFaild.ItemsFailed.Count + " rows";
            gridFaildNew.DataSource = newFaild.ItemsFailed;
            gridFaildNew.DataBind();

            failedOldLbl.Text = "Old: " + oldFaild.ItemsFailed.Count + " rows";
            gridFaildOld.DataSource = oldFaild.ItemsFailed;
            gridFaildOld.DataBind();

            spOldLabel.Text = "sp_RulesTracking24 " + BatchCombo.SelectedItem.Text.Split(' ')[0] + "00, 0; from Tables[1]";
            spNewLabel.Text = "spGetFullOrder " + OrderField.Text + "; from Tables[3]";
        }
        private void ShowItemValues()
        {
            var batchId = BatchCombo.SelectedValue;
            if (string.IsNullOrEmpty(batchId)) return;
            var newList = GetViewState(SessionConstants.TestFullOrderNew) as List<ShortReportModel>;
            var oldList = GetViewState(SessionConstants.TestFullOrderOld) as List<ShortReportModel>;
            if (newList == null || oldList == null) return;
            
            var newValues = newList.Find(m => m.BatchModel.BatchId == batchId);
            var oldValues = oldList.Find(m => m.BatchModel.BatchId == batchId);

            valuesNewLbl.Text = "New: " + newValues.ItemValues.Count + " rows";
            var dsNew = newValues.ItemValues.OrderBy(m=>m.PartId).ThenBy(m => m.PartName).ThenBy(m => m.MeasureName).ToList();
            
            gridValuesNew.DataSource = dsNew;
            gridValuesNew.DataBind();

            valuesOldLbl.Text = "Old: " + oldValues.ItemValues.Count + " rows";
            var dsOld = oldValues.ItemValues.OrderBy(m => m.PartId).ThenBy(m => m.PartName).ThenBy(m => m.MeasureName).ToList();
            
            gridValuesOld.DataSource = dsOld;
            gridValuesOld.DataBind();
            
            var batchNumber = BatchCombo.SelectedItem.Text.Split(' ')[0];
            var batchCode = Utils.ParseBatchCode(batchNumber);
            spValuesOldLabel.Text = "spGetItemDataFromOrderbatchItem " + 
                                    OrderField.Text + ", " + batchCode + ", 0";
            spValuesNewLabel.Text = "spGetFullOrder " + OrderField.Text + "; from Tables[6]";
        }
        private void ShowBatchId()
        {
            if (string.IsNullOrEmpty(BatchCombo.SelectedValue))
            {
                BatchIdLabel.Text = "";
            } else
            {
                BatchIdLabel.Text = "BatchId = " + BatchCombo.SelectedValue;
            }
        }
        protected void OnActiveTabChanged(object sender, EventArgs e)
        {
            if (TabContainer.ActiveTabIndex == 0)
            {
                ShowBatchList();
            }
            if (TabContainer.ActiveTabIndex == 1)
            {
                ShowFailed();
            }
            if (TabContainer.ActiveTabIndex == 2)
            {
                ShowMoved();
            }
            if (TabContainer.ActiveTabIndex == 3)
            {
                ShowItemValues();
            }

        }
        private List<ShortReportModel> GetFullOrderOld()
        {
            var batchModelList = QueryUtils.GetBatchesByOrderCode(OrderField.Text, this);
            var shortReportList = new List<ShortReportModel>();
            
            foreach (BatchModel batchModel in batchModelList)
            {
                //-- Document Id
                var docId = QueryUtils.GetDocumentId(batchModel, this);
                if (docId == "0") continue;
                
                //-- Document structure
                var structure = QueryUtils.GetDocStructure(docId, this);

                //-- Failed Items
                var failedItems = QueryUtils.GetItemsFailed(batchModel, this);

                var shortReport = new ShortReportModel
                {
                    BatchModel = batchModel,
                    DocumentId = docId,
                    DocStructure = structure,
                    ItemsFailed = failedItems
                };


                //-- Items and MovedItems
                QueryUtils.GetItemsAndMovedItems(shortReport, this);

                //-- Values Items
                shortReport.ItemValues = QueryUtils.GetItemValues(batchModel, this);

                shortReportList.Add(shortReport);
            }
            return shortReportList;
        }

        protected void OnBatchChanged(object sender, EventArgs e)
        {
            if (TabContainer.ActiveTabIndex == 1)
            {
                ShowFailed();
            }
            if (TabContainer.ActiveTabIndex == 2)
            {
                ShowMoved();
            }
            if (TabContainer.ActiveTabIndex == 3)
            {
                ShowItemValues();
            }
            ShowBatchId();
            
        }
    }
}