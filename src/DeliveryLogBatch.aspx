﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true"
    CodeBehind="DeliveryLogBatch.aspx.cs" Inherits="Corpt.DeliveryLogBatch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
        <div class="demoheading">
            Batch Delivery Log</div>
        <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="LoadButton">
            Batch Number:
            <asp:TextBox runat="server" ID="BatchNumber" placeholder="Batch Number"></asp:TextBox>
            <asp:ImageButton ID="LoadButton" runat="server" ImageUrl="~/Images/ajaxImages/search16.png"
                OnClick="OnLoadClick" />
            <asp:Label runat="server" Text="Event: " CssClass="control-label" Style="margin-left: 15px"></asp:Label>
            <asp:DropDownList runat="server" ID="EventList">
                <asp:ListItem Value="14">Sent to Avi Polish</asp:ListItem>
                <asp:ListItem Value="15">Received from Avi Polish</asp:ListItem>
                <asp:ListItem Value="16">Sent to Photoscribe Engraving</asp:ListItem>
                <asp:ListItem Value="17">Received from Photoscribe Engraving</asp:ListItem>
            </asp:DropDownList>
            <asp:Button runat="server" ID="SaveButton" Text="Create" OnClick="OnSaveClick" CssClass="btn btn-info" />
            <asp:Button runat="server" ID="ClearButon" Text="Clear" OnClick="OnClearClick" CssClass="btn btn-info" />
            <br />
            <asp:Label runat="server" ID="InfoLabel" ForeColor="Red"></asp:Label>
        </asp:Panel>
        <br/>
        <table>
            <tr>
                <td style="vertical-align: top">
                    <asp:TreeView ID="tvwItems" runat="server" ShowCheckBoxes="All" ExpandDepth="1" AfterClientCheck="CheckChildNodes();"
                        PopulateNodesFromClient="true" ShowLines="false" ShowExpandCollapse="true" OnTreeNodeCheckChanged="TreeNodeCheckChanged"
                        onclick="OnTreeClick(event)">
                        <LeafNodeStyle Font-Names="Cambria" Font-Size="Small" />
                        <ParentNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                        <RootNodeStyle Font-Bold="True" Font-Names="Cambria" Font-Size="Small" />
                        <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="False" />
                    </asp:TreeView>
                </td>
                <td style="vertical-align: top; padding-left: 100px;font-size: small">
                    <asp:DataGrid runat="server" ID="dgrLog" AutoGenerateColumns="False" CellPadding="5">
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                        <Columns>
                            <asp:BoundColumn DataField="FullBatchNumber" HeaderText="Batch"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FullItemNumber" HeaderText="Item Number"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RecordTimeStamp" HeaderText="Date/Time"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventName" HeaderText="Description"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator runat="server" ID="BatchReq" ControlToValidate="BatchNumber"
            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A Batch Number is required." />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqE" TargetControlID="BatchReq"
            HighlightCssClass="validatorCalloutHighlight" />
        <asp:RegularExpressionValidator runat="server" ID="BatchRegExpr" ControlToValidate="BatchNumber"
            Display="None" ValidationExpression="^\d{8}$|^\d{9}$" ErrorMessage="<b>Invalid Field</b><br />Please enter a batch number in the format:<br /><strong>Eight or nine numeric characters</strong>" />
        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="BatchReqExpr" TargetControlID="BatchRegExpr"
            HighlightCssClass="validatorCalloutHighlight" />
    </div>
</asp:Content>
