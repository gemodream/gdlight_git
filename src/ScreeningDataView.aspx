﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="ScreeningDataView.aspx.cs" Inherits="Corpt.ScreeningDataView" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>

    <style>
        
        select, textarea, label, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input
        {
            display: inline-block;
            padding: 7px 8px;
            margin-bottom: 3px;
            font-size: 12px;
            line-height: 13px;
            color: black; /* #555555;*/
            vertical-align: middle;
            font-family: Tahoma,Arial,sans-serif;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        input, button, select, textarea 
        {
            font-family: Tahoma,Arial,sans-serif;
            font-size: 14px;
        }

        body
        {
            font-family: Tahoma,Arial,sans-serif;
           /*font-size: 75%;*/
        }

        .headingPanel
        {
            padding-bottom: 2px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.0em;
        }

        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
        
        .text_nohighlitedyellow
        {
            background-color: white;
        }

		.GridPager a, .GridPager span
        {
            display: block;
            height: 20px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;
            text-decoration: none;
        }
        
        .GridPager a
        {
            /*background-color: #f5f5f5;*/
            color: #969696;
            border: 1px solid #969696;
        }
        
        .GridPager span
        {
            background-color: #5377A9;
            color: #000;
            border: 1px solid #5377A9;
        }
		.DdlCss{
			width:155px;
		}

    </style>

    <div class="demoarea">
        

        <asp:UpdatePanel runat="server" ID="MainPanel">
            <ContentTemplate>
				
			 <div style="font-size: smaller;height:25px;">
								<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
									<ProgressTemplate>
										<img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
										<b>Please, wait....</b>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
				<table>
					<tr>
						<td colspan="2">
							<table>
								<tr>
						<td style="width:120px;">
							<div class="demoheading">Data View</div>
					</td><td>
							
						</td>
									</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top;">
							  <fieldset style="border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; text-align: left;">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Filter Criteria</legend>
							<table >
								<tr>
									<td>
										<asp:Label ID="Label1" runat="server" Text="Customer Code"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label2" runat="server" Text="Retailer"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:TextBox ID="txtCustomerCode" runat="server"></asp:TextBox>
									</td>
									<td>
										<asp:DropDownList ID="ddlRetailer" runat="server" CssClass="DdlCss" ></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label3" runat="server" Text="PO#"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label4" runat="server" Text="Memo#"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:TextBox ID="txtPONumber" runat="server"></asp:TextBox>
									</td>
									<td>
											<asp:TextBox ID="txtMemo" runat="server"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label5" runat="server" Text="SKU"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label6" runat="server" Text="Style"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:TextBox ID="txtSKUName" runat="server"></asp:TextBox>
									</td>
									<td>
											<asp:TextBox ID="txtStyleName" runat="server"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label7" runat="server" Text="Position"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label8" runat="server" Text="CertifiedBy"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:DropDownList ID="ddlPosition" runat="server"  CssClass="DdlCss">
												
												
											</asp:DropDownList>
									</td>
									<td>
											<asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="DdlCss"></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label9" runat="server" Text="Service Type"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label10" runat="server" Text="Category"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:DropDownList ID="ddlServiceType" runat="server" CssClass="DdlCss" ></asp:DropDownList>
									</td>
									<td>
											<asp:DropDownList ID="ddlCategory" runat="server" CssClass="DdlCss"></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label11" runat="server" Text="Service Time"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label12" runat="server" Text="Delivery Method"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:DropDownList ID="ddlServiceTime" runat="server" CssClass="DdlCss" ></asp:DropDownList>
									</td>
									<td>
											<asp:DropDownList ID="ddlDeliveryMethod" runat="server" CssClass="DdlCss"></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label13" runat="server" Text="Carrier"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label14" runat="server" Text="Instrument"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:DropDownList ID="ddlShippingCurrier" runat="server" CssClass="DdlCss" ></asp:DropDownList>
									</td>
									<td>
											<asp:DropDownList ID="ddlScreeningInstrument" runat="server" CssClass="DdlCss"></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="Label15" runat="server" Text="Jewellery Type"></asp:Label>
									</td>
										<td>
										<asp:Label ID="Label16" runat="server" Text="Date"></asp:Label>
									</td>
								</tr>
								<tr>
									
									<td>
										<asp:DropDownList ID="ddlJewelryType" runat="server" CssClass="DdlCss"></asp:DropDownList>
									</td>
									<td>
										<asp:TextBox ID="txtDate" runat="server" TextMode="Date" Width="140px"></asp:TextBox>
											
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<br />
									</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;    padding-right: 0px;">
										<asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-info btn-large" Style="padding-right:10px;" OnClick="btnSubmit_Click" />
										<asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-info btn-large" OnClick="btnClear_Click" />
									</td>
								</tr>
							</table>
								  </fieldset>
						</td>
						<td style="vertical-align:top;">
							<fieldset style="margin-left:10px;border: 1px groove #ddd !important; padding: 0 10px 10px 10px; border-radius: 8px; text-align: left;">
                                        <legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
                                            class="label">Result As Per Criteria</legend>
								<table>
									<tr id="tdExportButton" runat="server" visible="false">
										<td >
									<br />
								<asp:LinkButton ID="btnimgExcelExport" runat="server"  ToolTip="English"  CssClass="btn btn-info"
									style="padding:6px;" OnClick="btnimgExcelExport_Click" Visible="false" >
									<img src="Images/ajaxImages/excel.jpg" alt="English"  border="0" style="width:20px;height:20px;padding-right:5px;"/>
									Download Result to Excel</asp:LinkButton>
								</td>
										</tr><tr>
									<td>	
										<br />
								<asp:GridView ID="gvDataView" Width="2000px" runat="server" CellPadding="4" ForeColor="#333333"  AllowSorting="True" OnSorting="OnSortingDataView"
									AllowPaging="True" OnPageIndexChanging="gvDataView_PageIndexChanging" AutoGenerateColumns="false" Font-Size="12px"
									EmptyDataText="No Record As Per Filter Criteria" ShowHeaderWhenEmpty="True">
									<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
									<AlternatingRowStyle BackColor="White"   />
									<Columns>
										<asp:TemplateField HeaderText="Created Date" SortExpression="CreatedDate">
											<ItemTemplate>
												<asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Customer Name" SortExpression="CustomerName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblCustomerName" runat="server" Text='<%#Eval("CustomerName")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Order No" SortExpression="OrderCode">
											<ItemTemplate>
												<asp:Label ID="lblOrderCode" runat="server" Text='<%#Eval("OrderCode")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Memo Number" SortExpression="MemoNum">
											<ItemTemplate>
												<asp:Label ID="lblMemoNum" runat="server" Text='<%#Eval("MemoNum")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="PO" SortExpression="PONum">
											<ItemTemplate>
												<asp:Label ID="lblPONum" runat="server" Text='<%#Eval("PONum")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="SKU/Style" SortExpression="SKUStyle" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblSKUStyle" runat="server" Text='<%#Eval("SKUStyle")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
											<%--<asp:TemplateField HeaderText="Style Name" SortExpression="StyleName">
											<ItemTemplate>
												<asp:Label ID="lblStyleName" runat="server" Text='<%#Eval("StyleName")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>--%>
											<asp:TemplateField HeaderText="Retailer" SortExpression="RetailerName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblRetailerName" runat="server" Text='<%#Eval("RetailerName")%>' ></asp:Label>
											</ItemTemplate>
												<HeaderStyle Width="100px" />
										</asp:TemplateField>							
									   <asp:TemplateField HeaderText="Total Qty" SortExpression="TotalQty" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTotalQty" runat="server" Text='<%#Eval("TotalQty")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										 <asp:TemplateField HeaderText="Total Passed Qty" SortExpression="TotalPassedQty" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTotalPassedQty" runat="server" Text='<%#Eval("TotalPassedQty")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										 <asp:TemplateField HeaderText="Total Reject Qty" SortExpression="TotalRejectQty" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTotalRejectQty" runat="server" Text='<%#Eval("TotalRejectQty")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Screener Pass Qty" SortExpression="ScreenerPassItems" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblScreenerPassItems" runat="server" Text='<%#Eval("ScreenerPassItems")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Screener Fail Qty" SortExpression="ScreenerFailItems" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblScreenerFailItems" runat="server" Text='<%#Eval("ScreenerFailItems")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Tester Pass Qty" SortExpression="TesterPassItems" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTesterPassItems" runat="server" Text='<%#Eval("TesterPassItems")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Tester Synthetic Qty" SortExpression="TesterSyntheticItems" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTesterSyntheticItems" runat="server" Text='<%#Eval("TesterSyntheticStones")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Tester Suspect Qty" SortExpression="TesterSuspectItems" HeaderStyle-Width="40px">
											<ItemTemplate>
												<asp:Label ID="lblTesterSuspectItems" runat="server" Text='<%#Eval("TesterSuspectStones")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Certified By" SortExpression="CertifiedBy" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblCertifiedBy" runat="server" Text='<%#Eval("CertifiedBy")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Category" SortExpression="CategoryName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblCategoryName" runat="server" Text='<%#Eval("CategoryName")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Jewelry Type" SortExpression="JewelryTypeName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblJewelryTypeName" runat="server" Text='<%#Eval("JewelryTypeName")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
											<asp:TemplateField HeaderText="Service Type" SortExpression="ServiceTypeName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblServiceTypeName" runat="server" Text='<%#Eval("ServiceTypeName")%>' ></asp:Label>
											</ItemTemplate>
												<HeaderStyle Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Service Time" SortExpression="ServiceTimeName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblServiceTimeName" runat="server" Text='<%#Eval("ServiceTimeName")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
										
											<asp:TemplateField HeaderText="Delivery Method" SortExpression="DeliveryMethodName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblDeliveryMethodName" runat="server" Text='<%#Eval("DeliveryMethodName")%>' ></asp:Label>
											</ItemTemplate>
												<HeaderStyle Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Carrier" SortExpression="CarrierName">
											<ItemTemplate>
												<asp:Label ID="lblCarrierName" runat="server" Text='<%#Eval("CarrierName")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										
										<asp:TemplateField HeaderText="Comment" SortExpression="Comment" HeaderStyle-Width="150px">
											<ItemTemplate>
												<asp:Label ID="lblComment" runat="server" Text='<%#Eval("Comment")%>' ></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Stone Count " SortExpression="StoneCount">
											<ItemTemplate>
											<%--	<asp:Label ID="lblStoneCount" runat="server" Text='<%#Eval("StoneCount")%>' ></asp:Label>--%>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Status" SortExpression="StatusName" HeaderStyle-Width="100px">
											<ItemTemplate>
												<asp:Label ID="lblStatusName" runat="server" Text='<%#Eval("StatusName")%>' ></asp:Label>
											</ItemTemplate>
											<HeaderStyle Width="100px" />
										</asp:TemplateField>
											<asp:TemplateField HeaderText="Date of Completion" SortExpression="DeliveryDate">
											<ItemTemplate>
												<%--<asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>' ></asp:Label>--%>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Additional Comments" SortExpression="Additional Comments">
											<ItemTemplate>
												<%--<asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>' ></asp:Label>--%>
											</ItemTemplate>
										</asp:TemplateField>
										
									</Columns>
									<EditRowStyle BackColor="#2461BF" />
									<EmptyDataRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
									<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									 <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
									<RowStyle BackColor="#EFF3FB" />
									<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
									<SortedAscendingCellStyle BackColor="#F5F7FB" />
									<SortedAscendingHeaderStyle BackColor="#6D95E1" />
									<SortedDescendingCellStyle BackColor="#E9EBEF" />
									<SortedDescendingHeaderStyle BackColor="#4870BE" />
										</asp:GridView>
								<div id="EmptyDataGrid" runat="server" style="border:1px solid black;width:800px;height:30px;text-align:center;padding:5px;">
									No Selection Criteria.
								</div></td>
 </tr>
								</table>
								</fieldset>
						</td>
					</tr>
				</table>


            </ContentTemplate>
        	<Triggers>
				<asp:PostBackTrigger ControlID="btnimgExcelExport" />
			</Triggers>
        </asp:UpdatePanel>

    </div>

</asp:Content>