﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="SortingStat.aspx.cs" Inherits="Corpt.SortingStat" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
<%--        <script type="text/javascript">--%>
<%--            $(window).resize(function () {--%>
<%--                gridviewScrollPercent();--%>
<%--            });--%>
<%--            $(document).ready(function () {--%>
<%--                gridviewScrollPercent();--%>
<%----%>
<%--            });--%>
<%--            function gridviewScrollPercent() {--%>
<%--                var widthGrid = $('#gridContainer').width();--%>
<%--                var heightGrid = $('#gridContainer').height();--%>
<%--                $('#<%=grdOrders.ClientID%>').gridviewScroll({--%>
<%--                    width: widthGrid,--%>
<%--                    height: heightGrid,--%>
<%--                    freezesize: 1--%>
<%--                });--%>
<%--            }--%>
<%--    </script>--%>

    <div class="demoheading">Sorting Stat</div>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdLoad" CssClass="form-inline">
        <!-- Date From -->
        <label >From</label>
        <asp:TextBox runat="server" ID="calFrom" Width="100px" OnTextChanged="OnChangedDateFrom"
            AutoPostBack="True" />
        <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
            AlternateText="Click to show calendar" />
        <!-- Date To -->
        <label style="padding-left: 20px;">To</label>
        <asp:TextBox runat="server" ID="calTo" OnTextChanged="OnChangedDateTo"
            AutoPostBack="True" />
        <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
            AlternateText="Click to show calendar" />
        <asp:Button ID="cmdLoad" class="btn btn-primary" runat="server" Text="Lookup" OnClick="OnLoadClick"
            Style="margin-left: 15px"></asp:Button>
    </asp:Panel><br/>
    <asp:label id="RowsCount" runat="server"  ForeColor="#5377A9"  Font-Bold="True"></asp:label>
    <!-- DataGrid -->
    <div id="gridContainer" style="font-size: small">
        <asp:DataGrid ID="grdOrders" runat="server" Enabled="True" AllowSorting="true" OnSortCommand="OnSortCommand" CellPadding="5">
            <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
<%--            <HeaderStyle CssClass="GridviewScrollHeader" />--%>
<%--            <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>--%>
<%--            <PagerStyle CssClass="GridviewScrollPager"  />--%>
        </asp:DataGrid>
    </div>
    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
        PopupButtonID="Image1" />
    <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
        PopupButtonID="Image2" />
    </div>
</asp:Content>
