﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="CustomerHistory.aspx.cs" Inherits="Corpt.CustomerHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True">
    </ajaxToolkit:ToolkitScriptManager>
     <div class="demoarea">
         <div class="demoheading">
            Customer History
         </div>
         <div style="height: 20px">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                 <ProgressTemplate>
                     <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                     <b>Please, wait....</b>
                 </ProgressTemplate>
             </asp:UpdateProgress>
         </div>
          <asp:UpdatePanel runat="server" ID="MainPanel">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" CssClass="form-inline" DefaultButton="CustomerLikeButton" ID="CustomerSearchPanel">
                    <table style="color: Black">
                        <tr>
                            <td style="padding-right: 5px">
                                <asp:DropDownList ID="lstCustomerList" runat="server" DataTextField="CustomerName"
                                    AutoPostBack="True" DataValueField="CustomerId" CssClass="" OnSelectedIndexChanged="OnCustomerSelectedChanged"
                                    ToolTip="Customers List" Width="300px" Height="26px"/>
                            </td>
                            <td style="padding-top: 0px">
                                <asp:TextBox runat="server" ID="CustomerLike" Width="100px"></asp:TextBox>
                            </td>
                            <td style="padding-top: 10px">
                                <asp:ImageButton ID="CustomerLikeButton" runat="server" ToolTip="Filtering the list of customers"
                                    ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnCustomerSearchClick" />
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" CssClass="form-inline">
                                    <label>
                                        From</label>
                                    <asp:TextBox runat="server" ID="calFrom" Width="100px" />
                                    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                        AlternateText="Click to show calendar" />
                                    <!-- Date To -->
                                    <label style="padding-left: 20px;">
                                        To</label>
                                    <asp:TextBox runat="server" ID="calTo" Width="100px" />
                                    <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Images/ajaxImages/Calendar_scheduleHS.png"
                                        AlternateText="Click to show calendar" />
                                    <label style="padding-left:10px">Order State</label>
                                    <asp:DropDownList ID="OrderStateList" runat="server" DataTextField="OrderStateName"
                                        DataValueField="OrderStateCode" Width="150px" />
                                    <asp:Button runat="server" ID="LookupBtn" Text="Lookup" OnClick="OnLookupClick" CssClass="btn btn-small btn-info" />
                                </asp:Panel>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFrom" runat="server" TargetControlID="calFrom"
                                    PopupButtonID="Image1" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderTo" runat="server" TargetControlID="calTo"
                                    PopupButtonID="Image2" />

                            </td>
                            <td><asp:Label runat="server" ID="MessageLabel" ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server">
                    <table>
                        <tr>
                            <td></td>
                            <td style="padding-left: 20px">
                                <asp:Label runat="server" ID="ItemsTotals"  ForeColor="Black" Font-Size="Small"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <asp:TreeView ID="treeHistory" runat="server" ShowCheckBoxes="None" Font-Names="Tahoma"
                                    Font-Size="small" ForeColor="Black" Font-Bold="False" ExpandDepth="2" PopulateNodesFromClient="true"
                                    EnableClientScript="True" ShowLines="False" ShowExpandCollapse="true">
                                </asp:TreeView>
                            </td>
                            <td style="font-size: small;vertical-align: top;padding-left:20px">
                            <asp:GridView ID="GridItems" runat="server" AutoGenerateColumns="false" 
                                class="table-bordered" CellPadding="3" >
                                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White"  />
                                <RowStyle Font-Size="Small"  ForeColor="Black" />
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" >
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="GSI_ID" DataField="FullItemNumber" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="State" DataField="StateName" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="Weight" DataField="Weight" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField HeaderText="Color" DataField="Color" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Clarity" DataField="Clarity" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Shape" DataField="Shape" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField HeaderText="Create Date" DataField="CreateDateDisplay" ItemStyle-HorizontalAlign="Center" />
                                     <asp:BoundField HeaderText="Order Date" DataField="OrderDateDisplay" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
          </asp:UpdatePanel>
     </div>
</asp:Content>
