﻿using System;
using System.Web.UI;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using Corpt.Utilities;
using System.Linq;
using System.Net;

namespace Corpt
{
    public partial class NotifyBilling : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            if (!IsPostBack) txtOrder.Focus();
        }       

        protected void btnNotification_Click(object sender, EventArgs e)
        {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;

            string indiaCustomerName = "";
            string nyCustomerName = "";

            var orderDetails = QueryUtils.GetOrderInfo(txtOrder.Text,this);

            if (orderDetails != null)
                indiaCustomerName = GetCustomer(Convert.ToInt32(orderDetails.Customer.CustomerId), 2);
            else
            {
                //error message 'Order not found'
                ClientScript.RegisterStartupScript(this.GetType(), "Info", "alert('Order number not found.');", true);
                return;
            }
           
            DataSet dsPartMeasures = new DataSet();
            dsPartMeasures.ReadXml(Server.MapPath("BillingByNY.xml"));

            var rowSources = dsPartMeasures.Tables[0].AsEnumerable()
                                .Where(r => r.Field<string>("IndiaCustomer") == Convert.ToInt32(orderDetails.Customer.CustomerId).ToString());
            if (rowSources.Any())
            {
                DataTable tblFiltered = dsPartMeasures.Tables[0].AsEnumerable()
                                .Where(r => r.Field<string>("IndiaCustomer") == Convert.ToInt32(orderDetails.Customer.CustomerId).ToString())
                                .CopyToDataTable();
                nyCustomerName = GetCustomer(Convert.ToInt32(tblFiltered.Rows[0][1]), 1);
            }
            else
            {
                //error message 'Customer not found'
                ClientScript.RegisterStartupScript(this.GetType(), "Info", "alert('Not a valid customer for billing in NY.');", true);
                return;
            }

            //if (tblFiltered.Rows.Count > 0)
            //    nyCustomerName = GetCustomer(Convert.ToInt32(tblFiltered.Rows[0][1]), 1);
            //else
            //{
            //    //error message 'Customer not found'
            //    ClientScript.RegisterStartupScript(this.GetType(), "Info", "alert('Not a valid customer for billing in NY.');", true);
            //    return;
            //}

            string subject = "Order# " + txtOrder.Text.Trim() + " / " + indiaCustomerName + "(" + nyCustomerName + ") / " + txtProgramName.Text + " / Certificates/Tag : " + txtCertificates.Text;
            string body = "";
            body = "Hello,</br></br>Please find order details-";
            body = body + "</br></br>Order No : " + txtOrder.Text;
            body = body + "</br>Customer (India) : " + indiaCustomerName;
            body = body + "</br>Customer (USA Billing) : " + nyCustomerName;
            body = body + "</br>Programme : " + txtProgramName.Text;        
            body = body + "</br>Certificates/Tag : " + txtCertificates.Text;
            body = body + "</br>Laser Inscription : " + txtLaserInscription.Text;
            body = body + "</br>Comments/Remark : " + txtDescription.Text;
            body = body + "</br></br>Best Wishes</br>Customer Service - India Operations</br>Gemological Science International";

            SendMail(txtOrder.Text, subject, body);
            ClientScript.RegisterStartupScript(this.GetType(), "Info", "alert('Your message has been sent successfully.');", true);
            txtOrder.Text = "";
            txtProgramName.Text = "";
            txtCertificates.Text = "";
            txtLaserInscription.Text = "";
            txtDescription.Text = "";
            txtOrder.Focus();
        }

        private String GetCustomer(int CustomerID, int CustomerOfficeID)
        {
            SqlConnection conn = new SqlConnection(Session["MyIP_ConnectionString"] as String);

            SqlCommand command = new SqlCommand("spGetCustomer");
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = CustomerOfficeID;
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CustomerID;
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse(Session["ID"].ToString());
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse(Session["AuthorOfficeID"].ToString());
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0].ItemArray[dt.Columns.IndexOf("CompanyName")].ToString();
            }
            else
            {
                return "";
            }
        }

        public void SendMail(string orderCode, string emailSubject, string emailBody)
        {
            MailMessage mail = new MailMessage();
            
            string emailFrom = Session["EmailFromAddr"].ToString();
            string emailTo = Session["EmailToAddr"].ToString();
            string emailCC = Session["EmailCCAddr"].ToString();
            string userName = Session["EmailFromAddr"].ToString();
            string password = Session["EmailFromPassw"].ToString();
                        
            mail.From = new MailAddress(emailFrom);
            mail.To.Add(emailTo);
            mail.CC.Add(emailCC);
            mail.Subject = emailSubject;

            mail.Body = emailBody;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = System.Text.Encoding.UTF8;

            SmtpClient smtp = new SmtpClient();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12;
            smtp.TargetName = "STARTTLS/" + Session["EMailHost"].ToString();

            smtp.Host = Session["EMailHost"].ToString(); 
            smtp.Port = Convert.ToUInt16(Session["EMailPort"].ToString());
            smtp.Credentials = new System.Net.NetworkCredential(userName, password);
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}