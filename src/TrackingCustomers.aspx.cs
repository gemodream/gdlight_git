﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using Corpt.TreeModel;

namespace Corpt
{
    public partial class TrackingCustomers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //BindGrid(0);
                InsertBlankRecord();
                //gvTitle.InnerText = "Screening Result - Top 50";
                //txtNotes.Value = "";
            }
        }
        // By using this method we can convert datatable to xml
        
        public System.Web.UI.WebControls.SortDirection SortDirection { get; set; }
        protected void sort_table(object sender, GridViewSortEventArgs e)
        {
           List<TrackingCustomersModel> lstScreening = new List<TrackingCustomersModel>();
            var sortField = e.SortExpression;
            

            DataSet dsScr = (DataSet)Session["CustTrackData"];
            string direction = null;
            if (System.Web.HttpContext.Current.Session["sortExpression"]!= null)
            {
                string[] sortData = Session["sortExpression"].ToString().Trim().Split(' ');
                direction = sortData[1];
                if (e.SortExpression == sortData[0])
                {
                    if (sortData[1] == "ASC")
                    {
                        dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "DESC";
                        Session["sortExpression"] = e.SortExpression + " " + "DESC";
                    }
                    else
                    {
                        dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                        Session["sortExpression"] = e.SortExpression + " " + "ASC";
                    }
                }
                else
                {
                    dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                    Session["sortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                dsScr.Tables[0].DefaultView.Sort = e.SortExpression + " " + "ASC";
                Session["sortExpression"] = e.SortExpression + " " + "ASC";
            }
            
            //lstScreening = (from DataRow row in dsScr.Tables[0].Rows select new SyntheticScreeningModel(row)).ToList();
            //SyntheticScreeningModel abc = new SyntheticScreeningModel();
            //lstScreening.Sort((x, y) => x.sortField.CompareTo(y.sortField));
            //lstScreening = lstScreening.Sort(x => x.sortField);
            lstScreening = (from System.Data.DataRow row in dsScr.Tables[0].Rows select new TrackingCustomersModel(row)).ToList();

            //grdScreening.DataSource = lstScreening;
            Session["CustTrackData"] = dsScr;
            if (e.SortExpression == "TotalQty" || e.SortExpression == "BatchID" || e.SortExpression == "GroupCode")
            {
                if (dsScr.Tables[0].Columns.Contains("ForSort"))
                {
                    dsScr.Tables[0].Columns.Remove("ForSort");
                }
                string convertStr = @"Convert(" + e.SortExpression + @", 'System.Int32')";
                dsScr.Tables[0].Columns.Add("ForSort", typeof(int), convertStr);
                if (direction == "ASC")
                    dsScr.Tables[0].DefaultView.Sort = "ForSort DESC";
                else
                    dsScr.Tables[0].DefaultView.Sort = "ForSort ASC";
            }
            else if (e.SortExpression == "OrderDate" || e.SortExpression == "ShippedDate" || e.SortExpression == "BilledDate")
            {
                if (dsScr.Tables[0].Columns.Contains("ForDateSort"))
                {
                    dsScr.Tables[0].Columns.Remove("ForDateSort");
                }
                dsScr.Tables[0].Columns.Add("ForDateSort", typeof(DateTime), e.SortExpression);
                if (direction == "ASC")
                    dsScr.Tables[0].DefaultView.Sort = "ForDateSort DESC";
                else
                    dsScr.Tables[0].DefaultView.Sort = "ForDateSort ASC";
            }
            grdTrackingCustomers.DataSource = dsScr.Tables[0].DefaultView;
            grdTrackingCustomers.DataBind();
            

        }//sort_table
        public string ConvertDatatableToXML(DataSet ds)
        {
            MemoryStream str = new MemoryStream();  
            ds.Tables[0].WriteXml(str, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }

        protected void grdScreening_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //grdScreening.PageIndex = e.NewPageIndex;
            //if ((txtCreateDateSearch.Value.Trim() != string.Empty && txtToCreateDateSearch.Value.Trim() != string.Empty) ||
            //    gsiOrderFilter.Value.Trim() != string.Empty)
            //    BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), gsiOrderFilter.Value.Trim());
        }

        private void BindGrid(string strCreateFromDate, string strCreateToDate, string customerID)
        {
            List<TrackingCustomersModel> lstTrackingCustomers = new List<TrackingCustomersModel>();
            DataSet dsScr = new DataSet();
            
            if (strCreateToDate == "")
                strCreateToDate = null;
            lstTrackingCustomers = GSIAppQueryUtils.GetTrackingCustomersData(strCreateFromDate, strCreateToDate, customerID, this);
            
            totalRecNumber.Value = lstTrackingCustomers.Count.ToString() + @" Items Found";
            dsScr.Tables.Add(ToDataTable<TrackingCustomersModel>(lstTrackingCustomers));
            Session["CustTrackData"] = dsScr;
            grdTrackingCustomers.DataSource = lstTrackingCustomers;
            grdTrackingCustomers.DataBind();
        }

        private bool IsOrderExist(int orderCode)
        {
            bool isOrderExist = false;
            if (GSIAppQueryUtils.GetScreeningByOrder(orderCode, this).Count > 0)
                isOrderExist = true;
            return isOrderExist;
        }
        
        private void ClearText()
        {
            /*
            txtGsiOrder.Value = string.Empty;
            txtVendorNum.Value = string.Empty;
            txtMemoNum.Value = string.Empty;
            txtTotalQTY.Value = string.Empty;
            txtQtySynth.Value = string.Empty;
            txtQtySusp.Value = string.Empty;
            txtQtyPass.Value = string.Empty;
            txtNotes.Value = string.Empty;
            btnSave.Text = "Add New";
            grdScreening.SelectedIndex = -1;
            */
            InsertBlankRecord();
        }

        private void ClearFilterText()
        {
            //gsiOrderFilter.Value = string.Empty;
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            customerCodeFilter.Value = string.Empty;
            /*
            txtGsiOrder.Value = string.Empty;
            txtMemoNum.Value = string.Empty;
            txtTotalQTY.Value = string.Empty;
            txtQtyPass.Value = string.Empty;
            txtQtySynth.Value = string.Empty;
            txtQtySusp.Value = string.Empty;
            txtVendorNum.Value = string.Empty;
            txtNotes.Value = string.Empty;
            txtRetailer.Value = string.Empty;
            BindGrid(0);
            */
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //dataTable.Columns.Remove("Notes");
            return dataTable;
        }

        public static void Export(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0);
            var cellStyle = CreateStyle(workbook, false, 0);

            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }


            //-- Save File
            var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";
            var sw = File.Create(dir + filename);
            workbook.Write(sw);
            sw.Close();
            if (!skipDownload) DownloadExcelFile(filename, p);
        }
        
        public static void DownloadExcelFile(String filename, Page p)
        {
            var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(dir + filename);
            p.Response.End();
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            //-- Border
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Center;
            return style;
        }

        protected void grdTrackingCustomers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdTrackingCustomers.PageIndex = e.NewPageIndex;
            if ((txtCreateDateSearch.Value.Trim() != string.Empty && txtToCreateDateSearch.Value.Trim() != string.Empty) ||
                customerCodeFilter.Value.Trim() != string.Empty)
                BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), customerCodeFilter.Value.Trim());
        }

        protected void grdTrackingCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            string order = grdTrackingCustomers.SelectedRow.Cells[3].Text.Trim();
            ds = GSIAppQueryUtils.GetShortStatsDetails(null, null, order, this);
            /*
            txtGsiOrder.Value = ds.Tables[0].Rows[0]["GSIOrder"].ToString();
            txtVendorNum.Value = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
            txtMemoNum.Value = ds.Tables[0].Rows[0]["Memo"].ToString();
            txtRetailer.Value = ds.Tables[0].Rows[0]["Destination"].ToString();
            txtRetailer.Value = txtRetailer.Value.ToUpper();
            txtTotalQTY.Value = ds.Tables[0].Rows[0]["TotalQty"].ToString();
            txtQtySusp.Value = ds.Tables[0].Rows[0]["QtySusp"].ToString();
            txtQtySynth.Value = ds.Tables[0].Rows[0]["QtySynth"].ToString();
            txtQtyPass.Value = ds.Tables[0].Rows[0]["QtyPass"].ToString();
            txtNotes.Value = ds.Tables[0].Rows[0]["Notes"].ToString();
            btnSave.Text = "Update";
            */
        }

        protected void grdTrackingCustomers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Deleting feature is temporary disabled.');", true);
        }

        protected void btnClear_ServerClick(object sender, EventArgs e)
        {
            grdTrackingCustomers.PageIndex = 0;
            grdTrackingCustomers.SelectedIndex = -1;
            //ClearText();
            ClearFilterText();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //ClearText();
            txtCreateDateSearch.Value = string.Empty;
            txtToCreateDateSearch.Value = string.Empty;
            grdTrackingCustomers.PageIndex = 0;
            grdTrackingCustomers.SelectedIndex = -1;
        }

        protected void btnCreateDateSearch_Click(object sender, EventArgs e)
        {
            if (customerCodeFilter.Value.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter Customer Code');", true);
                return;
            }
            /*
            if (txtCreateDateSearch.Value.Trim() != "" && txtToCreateDateSearch.Value.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter To Date');", true);
                return;
            }
            */
            if (txtToCreateDateSearch.Value.Trim() == "" && txtCreateDateSearch.Value.Trim() == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please enter From Date');", true);
                return;
            }
            if (txtToCreateDateSearch.Value.Trim() == "" && txtCreateDateSearch.Value.Trim() != "")
            {
                txtToCreateDateSearch.Value = DateTime.Now.ToString();
            }
            grdTrackingCustomers.PageIndex = 0;
            grdTrackingCustomers.SelectedIndex = -1;
            BindGrid(txtCreateDateSearch.Value.Trim(), txtToCreateDateSearch.Value.Trim(), customerCodeFilter.Value.Trim());
            if (customerCodeFilter.Value.Trim() == "")
                gvTitle.InnerText = "Result - For " + txtCreateDateSearch.Value + " and " + txtToCreateDateSearch.Value;
            else
                gvTitle.InnerText = "Result - For order " + customerCodeFilter.Value;
        }

        

        protected void btnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            DataSet dsScr = new DataSet();
            dsScr = (DataSet)Session["CustTrackData"];
            Export(dsScr, "CustomerTracking_" + DateTime.Now.Month.ToString() + @"_" + DateTime.Now.Day.ToString() + @"_" + DateTime.Now.Year.ToString(), false, this);
        }

        private void InsertBlankRecord()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add("GSIItemNumber");
            dt.Columns.Add("ItemQTYFail");
            dr = dt.NewRow();
            dr["GSIItemNumber"] = string.Empty;
            dr["ItemQTYFail"] = string.Empty;
            dt.Rows.Add(dr);
            //Store the DataTable in Session
            Session["FailItems"] = dt;
        }
    }
}