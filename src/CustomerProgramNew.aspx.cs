﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Corpt.TreeModel;
using Corpt.Utilities;
using NPOI.XWPF.UserModel;

namespace Corpt
{
    public partial class CustomerProgramNew : CommonPage
    {
        private const string CmdAddMeasure = "InsertMeasure";
        private const string CmdFocusComment = "CmdFocusComment";
        private const string CmdFocusDescrip = "CmdFocusDescrip";
        private const string CmdAddPriceMeasure = "CmdAddPriceMeasure";
        private const string CmdDelPriceMeasure = "CmdDelPriceMeasure";
        private const string CmdDelComments = "CmdDelComments";
        private const string CmdDelDescrip = "CmdDelDescrip";
        //IvanB 20.02.2024
        const string Dlg1DivFormat = "overflow-y: auto; height:{0}px; color: black; border: silver solid 1px; margin-top: 5px; width: 200px";
        const string Dlg2DivFormat = "overflow-y: auto; height:{0}px; color: black; border: silver solid 1px; margin-top: 5px; width: 400px";
        
        const string Dlg3DivFormat = "overflow-y: auto; height:{0}px; color: black; border: silver solid 1px; margin-top: 5px; width: 300px";
        //IvanB 20.02.2024
        public bool access = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Enctype = "multipart/form-data";
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Session["CustomerCode"] = null;
            Session["ProgramName"] = null;
            Session["DocId"] = null;
            //Session["AttachedReport"] = null;
            Page.Title = "GSI: Customer Program";
            Session["NewCustomerLoad"] = null;
            RefreshCpDocumentsTree.Visible = false;
            ViewLink.Visible = false;
            iframeMemoSkuPDFViewer.Src = "";
            string err = "";
            access = AccessUtils.HasAccessToPage(Session["ID"].ToString(), AccessUtils.CustomerProgramAccess, this, out err);
            if (!access || err != "")
            {
                PricingPanel.Enabled = false;
                OperationsPanel.Enabled = false;
                DescriptionPanel.Enabled = false;
                SaveAsButton.Enabled = false;
                SaveButton.Enabled = false;
                btnSaveInCompanyGroup.Enabled = false;
            }
                /*
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.MDXReportButton);
                scriptManager.RegisterPostBackControl(this.FDXReportButton);
                scriptManager.RegisterPostBackControl(this.SDXReportButton);
                scriptManager.RegisterPostBackControl(this.IDXReportButton);
                scriptManager.RegisterPostBackControl(this.WTReportButton);
                scriptManager.RegisterPostBackControl(this.PCReportButton);
                scriptManager.RegisterPostBackControl(this.CNSLTReportButton);
                scriptManager.RegisterPostBackControl(this.HDSReportButton);
                scriptManager.RegisterPostBackControl(this.GEMTAGReportButton);
                scriptManager.RegisterPostBackControl(this.GOLDTAGReportButton);
                scriptManager.RegisterPostBackControl(this.GMXReportButton);
                scriptManager.RegisterPostBackControl(this.CDXReportButton);
                scriptManager.RegisterPostBackControl(this.LBLReportButton);
                scriptManager.RegisterPostBackControl(this.TXTReportButton);
                scriptManager.RegisterPostBackControl(this.SRTReportButton);
                */
                /*
                 <%--<HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />--%>
                 <td style ="width:50px; border:hidden;"></td>
                    <td style ="width:100px;text-align: center; border:hidden;">Measure</td>
                    <td style ="width:230px;text-align: center; border:hidden;">Min Value</td>
                    <td style ="width:220px;text-align: center; border:hidden;">Max Value</td>
                    <td style ="width:30px;text-align: center; border:hidden;">Def</td>
                    <td style ="width:40px;text-align: center; border:hidden;">NV</td>
                    //<asp:Button runat="server" ID="SavePriceRangeBtn" OnClick="OnSavePriceRangeClick" CssClass="btn btn-small btn-info" Text="Save Price Range" Width="120px" Height="60px" />
                 */
                if (IsPostBack)
            {
                PostBackHandler();
                return;
            }
            
            //-- Init events
            PartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PartMeasureList, CmdAddPriceMeasure));
            PricePartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PricePartMeasureList, CmdDelPriceMeasure));
            CpComments.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PricePartMeasureList, CmdDelComments));
            CpDescrip.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PricePartMeasureList, CmdDelDescrip));
            if (Session["ReplaceRpt"] != null)
            {
                ReplaceRptBox.Checked = false;
            }
            //-- ItemTypeGroups
            LoadItemTypeGroups();

            //-- Short Measures
            LoadMeasuresShort();

            //-- Additional Services
            LoadAdditionalServices();

            //-- EnumMeasures
            LoadEnumMeasures();

            //-- Customers List
            LoadCustomers();

            if (!string.IsNullOrEmpty("" + Request.Params["BatchNumber"]))
            {
                BatchNumberFld.Text = Request.Params["BatchNumber"];
                OnSearchCpByBatchClick(null, null);
                return;
            }
            if (Request.QueryString["BatchCode"] != null)
            {
                BatchNumberFld.Text = (string)Request.QueryString["BatchCode"];
                OnSearchCpByBatchClick(null, null);
                return;
            }
            CustomerLike.Focus();

            LoadCurrency();
            LoadSamplePriceList();
            //alex 6/22
            if (Session["BatchReturnData"] == null && Session["CpReturnData"] == null && Session["BackToSku"] == null)
            {
                HdrLbl.Visible = false;
                Rules.Visible = false;
            }
            //alex 6/22
            if (Session["CpReturnData"] != null)
            {
                if (Session["EditData"] != null)
                //if (Session["EditData"] != null)
                {
                    var editData = (CpEditModel)Session["EditData"];
                    SetViewState(editData, SessionConstants.CpEditModel);
                    GetCheckedParts(editData);
                    //Session["EditData"] = null;
                }
                string cpReturnData = (string)Session["CpReturnData"];
                string[] dataList = cpReturnData.Split(';');
                string cCode = dataList[0].Substring(dataList[0].IndexOf("=") + 1);
                string programName = dataList[1].Substring(dataList[1].IndexOf("=") + 1);
                string docId = dataList[2].Substring(dataList[2].IndexOf("=") + 1);
                Session["CustomerCode"] = dataList[0].Substring(dataList[0].IndexOf("=") + 1);
                Session["ProgramName"] = dataList[1].Substring(dataList[1].IndexOf("=") + 1);
                Session["DocId"] = dataList[2].Substring(dataList[2].IndexOf("=") + 1);
                string attachedReport = null;
                if (Session["AttachedReport"] != null)
                    attachedReport = (string)Session["AttachedReport"];
                OnCustomerSearchClick(null, null);
                OnCpSelectedChanged(null, null);
                ShowReportButtons(true);
                //Session["CpReturnData"] = null;
                //Session["CustomerCode"] = null;
                //Session["ProgramName"] = null;
                //Session["DocId"] = null;
                OnCorelFileChange(attachedReport);
            }
            else if (Session["BatchReturnData"] != null)
            {
                if (Session["EditData"] != null)
                //if (Session["EditData"] != null)
                {
                    var editData = (CpEditModel)Session["EditData"];
                    SetViewState(editData, SessionConstants.CpEditModel);
                    //Session["EditData"] = null;
                    GetCheckedParts(editData);
                }
                string cpReturnData = (string)Session["BatchReturnData"];
                string[] dataList = cpReturnData.Split(';');
                string cCode = dataList[0].Substring(dataList[0].IndexOf("=") + 1);
                string programName = dataList[1].Substring(dataList[1].IndexOf("=") + 1);
                string docId = dataList[2].Substring(dataList[2].IndexOf("=") + 1);
                Session["CustomerCode"] = dataList[0].Substring(dataList[0].IndexOf("=") + 1);
                Session["ProgramName"] = dataList[1].Substring(dataList[1].IndexOf("=") + 1);
                Session["DocId"] = dataList[2].Substring(dataList[2].IndexOf("=") + 1);
                Session["BatchNumber"] = dataList[3].Substring(dataList[3].IndexOf("=") + 1);
                string attachedReport = null;
                if (Session["AttachedReport"] != null)
                    attachedReport = (string)Session["AttachedReport"];
                OnSearchCpByBatchClick(null, null);
                OnCorelFileChange(attachedReport);
            }
            else
            {
                Session["RangeList"] = null;
                Session["RangeGroups"] = null;
            }
            //alex 6/22
            if (Session["BackToSku"] != null)
            {
                //if (Session["EditModel"] != null)
                ////if (Session["EditData"] != null)
                //{
                //    var editData = (CpEditModel)Session["EditData"];
                //    SetViewState(editData, SessionConstants.CpEditModel);
                //    //Session["EditData"] = null;
                //}
                Session["BackToSku"] = null;
                //alex 6/23
                Session["CpReturnData"] = null;
                Session["BatchReturnData"] = null;
                //alex 6/23
            }
            TabContainer.ActiveTabIndex = 2;
            if (!string.IsNullOrEmpty("" + Request.Params["CPName"]))
            {
                OnCustomerSearchClick(null, null);
                CPNameClick(null, null);
            }
            
            //alex 6/22

        }

        #region PostBack handlers
        private void PostBackHandler()
        {
            if (Request.Params["__EVENTTARGET"] == "ctl00$SampleContent$ItemTypesTreeView")
            {
                NewSkuPopupExtender.Show();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddPriceMeasure)
            {
                OnAddCpPartMeasure();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelPriceMeasure)
            {
                OnRemoveCpPartMeasure();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelComments)
            {
                OnRemoveComments();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdDelDescrip)
            {
                OnRemoveDescrip();
            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdAddMeasure)
            {
                if (DescriptionPartsTreeView.SelectedNode != null && DescripMeasuresField.SelectedValue != null)
                {
                    var text = string.Format("[{0}].[{1}]", DescriptionPartsTreeView.SelectedNode.Text.Trim(),
                                             DescripMeasuresField.SelectedItem.Text.Trim());
                    var lastFocusIsComment = GetViewState(SessionConstants.CpLastFocusIsComment) as string ?? "Yes";
                    if (lastFocusIsComment == "Yes")
                    {
                        CpComments.Text = CpComments.Text + " " + text;
                    }
                    else
                    {
                        CpDescrip.Text = CpDescrip.Text + " " + text;
                    }
                }

            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdFocusComment)
            {
                SetViewState("Yes", SessionConstants.CpLastFocusIsComment);

            }
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == CmdFocusDescrip)
            {
                SetViewState("No", SessionConstants.CpLastFocusIsComment);

            }

        }
        #endregion

        #region Short Measures on Descrption
        private void LoadMeasuresShort()
        {
            SetViewState(QueryCpUtilsNew.GetMeasuresShort(this), SessionConstants.CpCommonShortMeasures);
        }
        private List<MeasureShortModel> GetMeasuresShortByPartType(string partTypeId)
        {
            var measures = GetViewState(SessionConstants.CpCommonShortMeasures) as List<MeasureShortModel> ??
                           new List<MeasureShortModel>();
            var byType = measures.FindAll(m => m.PartTypeId == partTypeId);
            byType.Sort((m1, m2) => String.CompareOrdinal(m1.MeasureTitle.ToUpper(), m2.MeasureTitle.ToUpper()));
            return byType;
        }
        #endregion

        #region Enum Measures
        private void LoadEnumMeasures()
        {
            SetViewState(QueryUtils.GetEnumMeasureUnion(this), SessionConstants.CpCommonEnumMeasures);
        }
        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }
        #endregion

        #region Additional Services
        private void LoadAdditionalServices()
        {
            SetViewState(QueryCpUtilsNew.GetAdditionalServices(this), SessionConstants.CpCommonAdditionalServices);
        }
        #endregion

        #region ItemTypeGroups & ItemTypes
        private void LoadItemTypeGroups()
        {
            var itemGroups = QueryCpUtilsNew.GetItemTypeGroups(this);
            itemGroups.Sort((m1, m2) => string.CompareOrdinal(m1.ItemTypeGroupName.ToUpper(), m2.ItemTypeGroupName.ToUpper()));
            itemGroups.Insert(0, new ItemTypeGroupModel { ItemTypeGroupId = -1, ItemTypeGroupName = "All Types", Path2Icon = "" });
            itemGroups.Insert(1, new ItemTypeGroupModel { ItemTypeGroupId = 0, ItemTypeGroupName = "Most Recently Used", Path2Icon = "D23.ico" });
            SetViewState(itemGroups, SessionConstants.CpCommonItemTypeGroups);
            ItemGroupsList.DataSource = itemGroups;
            ItemGroupsList.DataBind();
            ItemGroupsList.SelectedValue = "0";
        }
        private List<ItemTypeModel> GetItemTypes(string itemTypeGroup)
        {
            var itemTypes = GetViewState(SessionConstants.CpCommonItemTypes) as List<ItemTypeModel> ?? new List<ItemTypeModel>();
            if (itemTypeGroup == "0")
            {
                var types = itemTypes.FindAll(m => m.IsRecently);
                foreach (var type in types)
                {
                    itemTypes.Remove(type);
                }
                var recently = QueryCpUtilsNew.GetItemTypesRecently(
                    GetCustomerModelFromView(CustomerList.SelectedValue),
                    GetCustomerModelFromView(VendorList.SelectedValue), this);
                itemTypes.AddRange(recently);
                SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
                return recently;
            }
            if (itemTypeGroup == "-1")
            {
                var itemTypesAll = QueryCpUtilsNew.GetItemTypesByGroup(0, this);
                itemTypes.AddRange(itemTypesAll);
                SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
                return itemTypesAll;
            }

            var itemTypesByGroup = itemTypes.FindAll(m => "" + m.ItemTypeGroupId == itemTypeGroup && !m.IsRecently);
            if (itemTypesByGroup.Count > 0) return itemTypesByGroup;

            itemTypesByGroup = QueryCpUtilsNew.GetItemTypesByGroup(Convert.ToInt32(itemTypeGroup), this);
            itemTypes.AddRange(itemTypesByGroup);
            SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
            return itemTypesByGroup;
        }
        private ItemTypeGroupModel GetItemTypeGroup(CustomerProgramModel cpModel)
        {
            var groups = GetViewState(SessionConstants.CpCommonItemTypeGroups) as List<ItemTypeGroupModel> ??
                         new List<ItemTypeGroupModel>();
            return groups.Find(m => m.ItemTypeGroupId == cpModel.ItemTypeGroupId);
        }
        private ItemTypeModel GetItemType(string group, string type)
        {
            var types = GetItemTypes(group);
            return types.Find(m => "" + m.ItemTypeId == type);
        }
        private ItemTypeModel GetItemType(CustomerProgramModel cpModel)
        {
            var itemTypes = GetViewState(SessionConstants.CpCommonItemTypes) as List<ItemTypeModel> ?? new List<ItemTypeModel>();
            var itemTypesByGroup = itemTypes.FindAll(m => m.ItemTypeGroupId == cpModel.ItemTypeGroupId);
            if (itemTypesByGroup.Count == 0)
            {
                itemTypesByGroup = QueryCpUtilsNew.GetItemTypesByGroup(cpModel.ItemTypeGroupId, this);
                itemTypes.AddRange(itemTypesByGroup);
                SetViewState(itemTypes, SessionConstants.CpCommonItemTypes);
            }
            return itemTypesByGroup.Find(m => m.ItemTypeId == cpModel.ItemTypeId);
        }
        #endregion

        #region Customer/Vendor Search Panel
        private CustomerModel GetCustomerModelFromView(string customerId)
        {
            if (string.IsNullOrEmpty(customerId)) return null;
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            return customers.Find(m => m.CustomerId == customerId);

        }
        private List<CustomerModel> GetCustomersFromView()
        {
            return GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
        }
        private List<CustomerModel> GetCustomersFromView(string filterText)
        {
            var customers = GetCustomersFromView();
            return string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);

        }
        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                Session["NewCustomerLoad"] = "true";
                ProgramNameFld.Text = "";
                CustId.Text = "";
                Srp.Text = "";
                Path2PictureFld.Text = "";
                CpStyle.Text = "";
            }
            if (VendorAsCustomer.Checked)
            {
                var customer = VendorList.Items.FindByValue(CustomerList.SelectedValue);
                if (customer != null)
                {
                    VendorList.SelectedValue = CustomerList.SelectedValue;
                }
            }

            LoadCpList();
            GetCurrencyByProgram();
            Session["NewCustomerLoad"] = null;
            if (Session["CpReturnData"] == null)
            {
                ShowReportButtons(false);
                TabContainer.Visible = false;
            }
        }
        protected void OnCustomerSelectedChanged1(object sender, EventArgs e)
        {
            //var filterText = CustomerLike.Text.Trim().ToLower();
            var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            string filterText = "";
            if (customersList.Count != 0)
            {
                filterText = customersList.Find(m => m.CustomerId == CustomerList.SelectedValue.Trim().ToLower()).CustomerCode;
                
            }
            //filterText = CustomerList.SelectedValue.Trim().ToLower();
            string custCode = null;
            if (Session["CustomerCode"] != null)
            {
                custCode = (string)Session["CustomerCode"];
                if (filterText == null || filterText == "")
                    CustomerLike.Text = custCode;
                filterText = custCode;
                Session["CustomerCode"] = null;
            }
            //alex 6/25
            else
            {
                ProgramNameFld.Text = "";
                CustId.Text = "";
                Srp.Text = "";
                Path2PictureFld.Text = "";
                CpStyle.Text = "";
            }
            //alex 6/25
            var filtered = GetCustomersFromView(filterText);
            //alex 6/24
            //var customers = GetCustomersFromView();
            //alex 6/29
            if (CustomerList.Items.Count > 0 && filtered.Count > 0)
            {
                //alex 6/29
                for (int i = 0; i <= CustomerList.Items.Count - 1; i++)
                {
                    if (CustomerList.Items[i].Text == filtered[0].CustomerName)
                    {
                        CustomerList.SelectedIndex = i;
                        CustomerLike.Text = filtered[0].CustomerCode;
                        //CustId.Text = filtered[0].CustomerId;
                        break;
                    }
                }
            }
            else
            {
                PopupInfoDialog(string.Format("The entered customer code {0} does not exist.", filterText), true);
                return;
            }

            //CustomerList.DataSource = filtered;
            //CustomerList.DataBind();
            //alex 6/24
            Session["NewCustomerLoad"] = "true";
            if (filtered.Count == 1)
            {
                CustomerList.SelectedValue = filtered[0].CustomerId;
            }
            else
            {
                ResetCpList();
            }
            //alex 6/24
            //CustomerList.SelectedIndex = -1;
            //alex 6/24
            if (VendorAsCustomer.Checked)
            {
                VendorLike.Text = filterText;
                OnVendorSearchClick(null, null);
            }
            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
            //IvanB 15.02.2024
            GetSkuList(false);
            //IvanB 15.02.2024
            SKUTypesTreeView.Nodes.Clear();
            //alex 6/23
            NewSkuBtn.Enabled = !string.IsNullOrEmpty(CustomerList.SelectedValue);
            //NewSkuBtn.Enabled = false;
            //alex 6/23
            Session["NewCustomerLoad"] = null;
            if (Session["CpReturnData"] == null)
            {
                ShowReportButtons(false);
                TabContainer.Visible = false;
            }
            if (Session["CpReturnData"] == null && Session["BatchReturnData"] == null)
            {
                var typeList = GetItemTypes("0");
                //CPTypesList.DataSource = typeList;
                //CPTypesList.DataBind();
            }
        }
        protected void OnVendorSelectedChanged(object sender, EventArgs e)
        {
            LoadCpList();
        }
        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            string filterText = "";
            if (!string.IsNullOrEmpty(Request.Params["CustomerId"]))
            {
                string customerCode = Request.Params["CustomerId"].ToLower();
                DataTable dtCustomerCode = QueryCpUtils.GetCustomerCodeByID(customerCode, this);
                if (dtCustomerCode == null || dtCustomerCode.Rows.Count == 0)
                    return;
                filterText = dtCustomerCode.Rows[0].ItemArray[0].ToString();

            }
            else
                filterText = CustomerLike.Text.Trim().ToLower();
            string custCode = null;
            if (Session["CustomerCode"] != null)
            {
                custCode = (string)Session["CustomerCode"];
                if (filterText == null || filterText == "")
                    CustomerLike.Text = custCode;
                filterText = custCode;
                Session["CustomerCode"] = null;
            }
            //alex 6/25
            else
            {
                ProgramNameFld.Text = "";
                CustId.Text = "";
                Srp.Text = "";
                Path2PictureFld.Text = "";
                CpStyle.Text = "";
            }
            //alex 6/25
            var filtered = GetCustomersFromView(filterText);
            //alex 6/24
            //var customers = GetCustomersFromView();
            //alex 6/29
            if (CustomerList.Items.Count > 0 && filtered.Count > 0)
            {
                //alex 6/29
                for (int i = 0; i <= CustomerList.Items.Count - 1; i++)
                {
                    if (CustomerList.Items[i].Text == filtered[0].CustomerName)
                    {
                        CustomerList.SelectedIndex = i;
                        CustomerLike.Text = filtered[0].CustomerCode;
                        hdnCustId.Value = filtered[0].CustomerId;
                        break;
                    }
                }
            }
            else
            {
                PopupInfoDialog(string.Format("The entered customer code {0} does not exist.", filterText), true);
                return;
            }

            //CustomerList.DataSource = filtered;
            //CustomerList.DataBind();
            //alex 6/24
            Session["NewCustomerLoad"] = "true";
            if (filtered.Count == 1)
            {
                CustomerList.SelectedValue = filtered[0].CustomerId;
            }
            else
            {
                ResetCpList();
            }
            //alex 6/24
            //CustomerList.SelectedIndex = -1;
            //alex 6/24
            if (VendorAsCustomer.Checked)
            {
                VendorLike.Text = filterText;
                OnVendorSearchClick(null, null);
            }
            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnCustomerSelectedChanged(null, null);
            }
            //IvanB 15.02.2024
            GetSkuList(false);
            //IvanB 15.02.2024
            SKUTypesTreeView.Nodes.Clear();
            //alex 6/23
            NewSkuBtn.Enabled = !string.IsNullOrEmpty(CustomerList.SelectedValue);
            //NewSkuBtn.Enabled = false;
            //alex 6/23
            Session["NewCustomerLoad"] = null;
            if (Session["CpReturnData"] == null)
            {
                ShowReportButtons(false);
                TabContainer.Visible = false;
            }
            if (Session["CpReturnData"] == null && Session["BatchReturnData"] == null)
            {
                var typeList = GetItemTypes("0");
                //CPTypesList.DataSource = typeList;
                //CPTypesList.DataBind();
            }
        }
        protected void OnVendorSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = VendorLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            VendorList.DataSource = filtered;
            VendorList.DataBind();
            if (filtered.Count == 1) VendorList.SelectedValue = filtered[0].CustomerId;
            if (!string.IsNullOrEmpty(VendorList.SelectedValue))
            {
                OnVendorSelectedChanged(null, null);
                VendorList.DataSource = filtered;
                VendorList.DataBind();
                VendorList.SelectedIndex = -1;
            }

        }
        protected void OnVendorAsCustomerChanging(object sender, EventArgs e)
        {
            var asCustomer = VendorAsCustomer.Checked;
            VendorList.Enabled = !asCustomer;
            VendorLike.Enabled = !asCustomer;
            var vendors = GetCustomersFromView();
            if (!VendorAsCustomer.Checked)
            {
                VendorList.DataSource = vendors;
                VendorList.DataBind();
                VendorList.SelectedIndex = -1;
                VendorLike.Text = "";
            }
        }
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            customers.Add(new CustomerModel { CustomerId = "", CustomerName = "" });
            customers.Sort((m1, m2) => String.CompareOrdinal(m1.CustomerName, m2.CustomerName));
            

            /*IvanB start*/
            //create and fill blocklist
            var excluded = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Customers, Page);

            //Remove items from a list of customers with a CustomerName equals value from block list
            customers = customers.Where(x => !excluded.Exists(y => x.CustomerName.Equals(y.BlockedDisplayName))).ToList();
            SetViewState(customers, SessionConstants.CustomersList);


            CustomerList.DataSource = customers;
            /*IvanB end*/

            CustomerList.DataBind();
            CustomerList.SelectedIndex = 0;

            VendorList.DataSource = customers;
            VendorList.DataBind();
            VendorList.SelectedIndex = 0;

            //-- Save as dialog
            SaCustomerList.DataSource = customers;
            SaCustomerList.DataBind();

            SaVendorList.DataSource = customers;
            SaVendorList.DataBind();
        }

        #endregion

        #region Customer Program Section
        protected void OnNewSkuClick(object sender, EventArgs e)
        {
            NewProgramNameFld.Text = "";
            ItemGroupsList.SelectedValue = "0";
            OnItemGroupsListChanged(null, null);
            var h = Convert.ToInt32(ForHeightFld.Value);
            ItemTypeGrpDiv.Attributes.CssStyle.Value = string.Format(Dlg1DivFormat, h);
            ItemTypeDiv.Attributes.CssStyle.Value = string.Format(Dlg2DivFormat, h);
            ItemTreeDiv.Attributes.CssStyle.Value = string.Format(Dlg3DivFormat, h);
            NewSkuPopupExtender.Show();
            NewProgramNameFld.Focus();
        }
        protected void CPNameClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.Params["CPName"]))
                ProgramNameFld.Text = Request.Params["CPName"];
            bool cpFound = false;
            for (int i = 0; i <= CpList.Items.Count - 1; i++)
            {
                if (CpList.Items[i].Text == ProgramNameFld.Text)
                {
                    CpList.SelectedIndex = i;
                    CpList.SelectedItem.Text = ProgramNameFld.Text;
                    CpList.SelectedValue = CpList.Items[i].Value;
                    cpFound = true;
                    break;
                }
            }
            if (cpFound)
                OnCpSelectedChanged(null, null);
        }
        protected void OnClearAllClick(object sender, EventArgs e)
        {
            PartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PartMeasureList, CmdAddPriceMeasure));
            PricePartMeasureList.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(PricePartMeasureList, CmdDelPriceMeasure));
            Session["CustomerCode"] = null;
            Session["ProgramName"] = null;
            Session["DocId"] = null;
            //Session["AttachedReport"] = null;
            Page.Title = "GSI: Customer Program";
            Session["NewCustomerLoad"] = null;
            RefreshCpDocumentsTree.Visible = false;
            Session["BatchReturnData"] = null;
            Session["CpReturnData"] = null;
            Session["BackToSku"] = null;
            Session["EditData"] = null;
            Session["SelectedDoc"] = null;
            CustomerList.Items.Clear();
            VendorList.Items.Clear();
            CustomerLike.Text = "";
            VendorLike.Text = "";
            CpList.Items.Clear();
            ProgramNameFld.Text = "";
            CpDocIdHidden.Value = "";
            ProgramNameFld.Enabled = true;
            CpStyle.Text = "";
            CustId.Text = "";
            Srp.Text = "";
            Path2PictureFld.Text = "";
            BatchNumberFld.Text = "";
            //CpReportsTree.Visible = false;
            CpPanel.Visible = false;
            TabContainer.Visible = false;
            CustomerLike.Enabled = true;
            CpList.Enabled = true;
            CustomerList.Enabled = true;
            BatchNumberFld.Enabled = true;
            itemPicture.Visible = false;
            ErrPictureField.Text = "";
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureBatchButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
            NewReportPictureBatchButton.Visible = false;
			FractionRangeFld.Visible = false;
            FractionRangeFld.Text = "";
            FractionRangesList.Visible = false;
            FractionRangesList.SelectedIndex = -1;
            CpDocsList.DataSource = null;
            CpDocsList.DataBind();
            if (SamplePriceList.Items.Count > 0)
                SamplePriceList.SelectedIndex = 0;
            UnitList.Visible = false;
            SamplePriceGrid.DataSource = null;
            SamplePriceGrid.DataBind();
            PriceSamplePartMeasureList.Items.Clear();
            SampleServicePriceGrid.DataSource = null;
            SampleServicePriceGrid.DataBind();
            LoadSampleError.Visible = false;
            SKUTypesList.DataSource = null;
            SKUTypesList.DataBind();
            SKUTypesTreeView.Nodes.Clear();
            UpperCaseBox.Checked = false;
            ErrCP.Text = "";
            ErrCP.Visible = false;
            iframeMemoSkuPDFViewer.Src = "";
            /*IvanB start*/
            ReportTypeAssignmentPanel.Visible = false;
            hdnCustId.Value = "0";
            SavedDescList.Items.Clear();
            SavedCommentsList.Items.Clear();
            ReplaceRptBox.Checked = true;
            /*IvanB end*/
            //-- ItemTypeGroups
            LoadItemTypeGroups();

            //-- Short Measures
            LoadMeasuresShort();

            //-- Additional Services
            LoadAdditionalServices();

            //-- EnumMeasures
            LoadEnumMeasures();

            //-- Customers List
            LoadCustomers();
            CustomerLike.Focus();

            LoadCurrency();
        }
        private void ShowReportButtons(bool show)
        {
            MDXReportButton.Visible = show;
            FDXReportButton.Visible = show;
            SDXReportButton.Visible = show;
            IDXReportButton.Visible = show;
            WTReportButton.Visible = show;
            PCReportButton.Visible = show;
            CNSLTReportButton.Visible = show;
            HDSReportButton.Visible = show;
            GEMTAGReportButton.Visible = show;
            GOLDTAGReportButton.Visible = show;
            GMXReportButton.Visible = show;
            CDXReportButton.Visible = show;
            LBLReportButton.Visible = show;
            TXTReportButton.Visible = show;
            SRTReportButton.Visible = show;
            GradingReportButton.Visible = show;
            //alex 6/22
            AddDocumentBtn.Visible = show;
            DelDocumentBtn.Visible = show;
            Label16.Visible = show;
            AddPrintDocToDocBtn.Visible = show;
            DelDocPrintDocBtn.Visible = show;
            //ViewLink.Visible = show;
            DocAttachedReportsList.Visible = show;
            //alex 6/22
        }
        private void HideCpDetailsSections()
        {
            CpPanel.Visible = false;
            TabContainer.Visible = false;
            /*IvanB start*/
            ReportTypeAssignmentPanel.Visible = false;
            /*IvanB end*/
        }
        private void ShowCpDetailsSections()
        {
            CpPanel.Visible = true;
            TabContainer.Visible = true;            
        }
        private void ResetCpList()
        {
            SetViewState(null, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.DataSource = null;
            CpList.DataBind();
            HideCpDetailsSections();
        }
        private void LoadCpList()
        {
            var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
            if (customersList.Count == 0)
            {
                return;
            }
            var customerModel = customersList.Find(m => m.CustomerId == CustomerList.SelectedValue);
            if (customerModel == null) return;
            CustomerLike.Text = customerModel.CustomerCode;
            var vendorModel = VendorAsCustomer.Checked ? null : customersList.Find(m => m.CustomerId == VendorList.SelectedValue);
            if (vendorModel != null && (vendorModel.CustomerId == null || vendorModel.CustomerId == ""))
                vendorModel = null;
            var cpList = QueryUtils.GetCustomerPrograms(customerModel, vendorModel, this);

            /*IvanB start*/
            //Create and fill block list
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CustomerPrograms, Page);
            //Remove items from a list of cp with a CustomerProgramName equals value from block list
            cpList = cpList.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName))).ToList();
            
            SetViewState(cpList, SessionConstants.CustomerProgramList);

            CpList.DataSource = cpList;
            /*IvanB end*/
          
            CpList.DataBind();
            if (cpList.Count == 0)
            {
                HideCpDetailsSections();
                return;
            }
            else
            {
                ShowCpDetailsSections();
            }
            OnCpSelectedChanged(null, null);
        }
        protected void OnCpSelectedChanged(object sender, EventArgs e)
        {
            /*IvanB start*/
            ReportTypeAssignmentPanel.Visible = false;
            /*IvanB end*/
            ClearPriceSession();
            DescNameBox.Text = "";
            CommentsNameBox.Text = "";
            SavedDescList.Items.Clear();
            SavedCommentsList.Items.Clear();
            if (Session["CpReturnData"] == null)
            {
                ShowReportButtons(true);
                TabContainer.Visible = true;
            }
            try
            {
                var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                new List<CustomerProgramModel>();
                var cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
                if (cp != null)
                {
                    LoadSavedDescCommentsList(cp.ItemTypeId);
                    hdnItemTypeID.Value = cp.ItemTypeId.ToString();
                }
                if (Session["ProgramName"] != null)
                {
                    string pName = (string)Session["ProgramName"];
                    for (int i = 0; i <= CpList.Items.Count - 1; i++)
                    {
                        if (CpList.Items[i].Text == pName)
                        {
                            CpList.SelectedValue = CpList.Items[i].Value;
                            CpList.SelectedItem.Text = CpList.Items[i].Text;
                            cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
                            break;
                        }
                    }
                    //alex 6/23
                    var editData = (CpEditModel)Session["EditData"];
                    cp = editData.Cp;
                    //alex 6/23
                }
                //ErrPictureField.Text = cp.Path2Picture;
                string newCustomerLoad = (string)Session["NewCustomerLoad"];
                //alex 6/26
                //if (newCustomerLoad != null)
                //{
                //    CpList.SelectedItem.Text = "";
                //    //Session["NewCustomerLoad"] = null;
                //}
                //else
                //{
                //    ErrPictureField.Text = cp.Path2Picture;
                //    SetCpDetails(cp);
                //}
                if (newCustomerLoad != null && Session["CpReturnData"] == null && Session["CpReturnData"] == null && Session["BackToSku"] == null)
                {
                    CpList.SelectedIndex = -1;
                    CpList.Items.Insert(0, new ListItem("Please select Customer Program", ""));
                    VendorList.SelectedIndex = -1;
                    //Session["NewCustomerLoad"] = null;
                }
                else if (newCustomerLoad != null)
                {

                }
                else
                {
                    ErrPictureField.Text = cp.Path2Picture;
                    SetCpDetails(cp);
                    if (cp.VendorId != cp.CustomerId)
                    {
                        for (int i = 0; i <= VendorList.Items.Count-1; i++)
                        {
                            if (VendorList.Items[i].Value == cp.VendorId)
                            {
                                VendorList.SelectedIndex = i;
                                break;
                            }
                        }
                    }
                }
                //alex 6/26
                //CpList.SelectedIndex = -1;
                //CpList.SelectedValue = "";
                //CpList.SelectedItem.Text = "";

                //SetCpDetails(cp);

                if (!string.IsNullOrEmpty(CpList.SelectedValue) && !TabContainer.Visible)
                {
                    ShowCpDetailsSections();
                }
                SetEnabledSaveButtons();
                GetCurrencyByProgram();
                string cpName = CpList.SelectedItem.Text.Replace(" ", "");
                SavePriceRangeFld.Text = CustomerLike.Text + "-" + cpName;
                if (SKUTypesList.Items.Count > 0)
                {
                    SKUTypesList.SelectedValue = cp.ItemTypeId.ToString();
                    OnSKUTypesListSelectedChanged(null, null);
                }
                if (sender != null && e != null)
                    LoadSamplePriceList();
                /*IvanB start*/
                if (int.TryParse(CpList.SelectedValue, out int result))
                {
                    ReportTypeAssignmentPanel.Visible = true;
                    ShowHideIframeButton.Attributes.Add("onclick", "ShowHideIframe(); return false;");
                    ApplySkuReportButtonsStyles(CpList.SelectedItem.Text);
                }
				/*IvanB end*/

				//--Load CustomerCompanyGroup
				if (!string.IsNullOrEmpty(CpList.SelectedValue))
					LoadCustomerCompanyGroup(CustomerList.SelectedValue.ToString());
			}
            catch (Exception ex)
            {
                ErrPictureField.Text = ex.Message;
            }
        }
        //IvanB 15.02.2024 start
        private void GetSkuList(bool newSku)
        {
            DataTable dt = newSku ? QueryCpUtilsNew.GetSKUAllTypesList(this) : QueryCpUtilsNew.GetSKUTypesList(CustomerLike.Text, this);
            if (dt.Rows.Count > 0)
            {
                List<SKUTypesLIstModel> skuTypesList = (from DataRow row in dt.Rows select new SKUTypesLIstModel(row)).ToList();
                SKUTypesList.DataSource = skuTypesList;
                SKUTypesList.DataBind();
                SKUTypesList.SelectedIndex = -1;
                SKUTypesList.Items.Insert(0, new ListItem("All Types", ""));
            }
        }
        private void GetSkuListForNewSku(string value)
        {
            DataTable dtAll = QueryCpUtilsNew.GetSKUAllTypesList(this);
            DataTable dt = QueryCpUtilsNew.GetSKUTypesList(CustomerLike.Text, this);
            if (dt.Rows.Count > 0)
            {
                List<SKUTypesLIstModel> skuTypesList = (from DataRow row in dt.Rows select new SKUTypesLIstModel(row)).ToList();
                SKUTypesList.DataSource = skuTypesList;
                SKUTypesList.DataBind();
                SKUTypesList.SelectedIndex = -1;
                SKUTypesList.Items.Insert(0, new ListItem("All Types", ""));
            }
            var selected = SKUTypesList.Items.FindByValue(value);
            if (selected != null)
            {
                SKUTypesList.SelectedValue = selected.Value;
                OnSKUTypesListSelectedChanged(null, null);
            }
            else 
            {
                if (dtAll.Rows.Count > 0)
                {
                    List<SKUTypesLIstModel> skuAllTypesList = (from DataRow row in dtAll.Rows select new SKUTypesLIstModel(row)).ToList();
                    SKUTypesLIstModel selectedType = skuAllTypesList.Where(x => x.ItemTypeID.ToString() == value).FirstOrDefault();
                    var index = SKUTypesList.Items.Count - 1;
                    if (index < 0)
                        index = 0;
                    SKUTypesList.Items.Insert(index, new ListItem(selectedType.ItemTypeName, selectedType.ItemTypeID.ToString()));
                    SKUTypesList.SelectedIndex = index;
                } 
            }
        }
        //IvanB 15.02.2024 end
        protected void OnSKUTypesListSelectedChanged(object sender, EventArgs e)
        {
            if (sender != null && e != null)
            {
                var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel>;
            if (SKUTypesList.SelectedItem.Text == "All Types" || (sender == null && e == null))
                CpList.DataSource = cpList;
            else
                CpList.DataSource = cpList.FindAll(m => m.ItemTypeId == Convert.ToInt32(SKUTypesList.SelectedValue));
            CpList.DataBind();
            
                CpList.SelectedIndex = -1;
                CpList.Items.Insert(0, new ListItem("Please select Customer Program", ""));
            }
            
            var cpModel = GetViewState(SessionConstants.CpEditModel) as CpEditModel;
            //var parts = cpModel.MeasureParts;
            if (SKUTypesList.SelectedItem.Text != "All Types")
            {
                var measureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(SKUTypesList.SelectedValue), this);
                //IvanB 20.02.2024 start
                LoadItemTypesPartsTree(measureParts, false);
                //IvanB 20.02.2024 end
                //OnSKUTypesListSelectedChanged(null, null);
            }
        }
        //IvanB 20.02.2024 start
        private void LoadItemTypesPartsTree(List<MeasurePartModel> parts, bool isNewSku)
        {
            //var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();
            if (isNewSku)
            {
                NewSKUTypesTreeView.Nodes.Clear();
                NewSKUTypesTreeView.Nodes.Add(rootNode);
                NewSKUTypesTreeView.Nodes[0].Select();
            }
            else
            {
                SKUTypesTreeView.Nodes.Clear();
                SKUTypesTreeView.Nodes.Add(rootNode);
                SKUTypesTreeView.Nodes[0].Select();
            }
            
            //OnDescripPartsTreeChanged(null, null);
        }
        //IvanB 20.02.2024 end
        private void SetEnabledSaveButtons()
        {
            var cpEdit = GetCpEditFromView();
            SaveAsButton.Enabled = (cpEdit != null && cpEdit.Cp.CpId != "0" && !cpEdit.Cp.IsCopy && access);
            SaveButton.Enabled = (cpEdit != null && access);
            MDXReportButton.Enabled = (cpEdit != null);
            FDXReportButton.Enabled = (cpEdit != null);
            SDXReportButton.Enabled = (cpEdit != null);
            IDXReportButton.Enabled = (cpEdit != null);
            WTReportButton.Enabled = (cpEdit != null);
            PCReportButton.Enabled = (cpEdit != null);
            CNSLTReportButton.Enabled = (cpEdit != null);
            HDSReportButton.Enabled = (cpEdit != null);
            GEMTAGReportButton.Enabled = (cpEdit != null);
            GOLDTAGReportButton.Enabled = (cpEdit != null);
            GMXReportButton.Enabled = (cpEdit != null);
            CDXReportButton.Enabled = (cpEdit != null);
            LBLReportButton.Enabled = (cpEdit != null);
            TXTReportButton.Enabled = (cpEdit != null);
            SRTReportButton.Enabled = (cpEdit != null);
            GradingReportButton.Enabled = (cpEdit != null);
            //alex 6/22
            AddDocumentBtn.Enabled = (cpEdit != null);
            DelDocumentBtn.Enabled = (cpEdit != null);
            Label16.Enabled = (cpEdit != null);
            AddPrintDocToDocBtn.Enabled = (cpEdit != null);
            DelDocPrintDocBtn.Enabled = (cpEdit != null);
            ViewLink.Disabled = (cpEdit == null);
            DocAttachedReportsList.Enabled = (cpEdit != null);
            //alex 6/22
        }
        protected void OnApplyPath2PictureClick(object sender, ImageClickEventArgs e)
        {
            ShowPicture(Path2PictureFld.Text);
        }
		protected void OnNewPictureClick(object sender, ImageClickEventArgs e)
        {
            var item = sender as ImageButton;
            if (item.ImageUrl.Contains("no_image") || item.ImageUrl == "")
                return;
            NewReportPicture.ImageUrl = item.ImageUrl;

            NewPicturePopupExtender.Show();
        }

        private CpEditModel GetCpEditFromView()
        {
            return GetViewState(SessionConstants.CpEditModel) as CpEditModel;
        }
        private CpEditModel GetCpDetails()
        {
            var cpEdit = GetCpEditFromView();

            //-- Cp Details Panel
            cpEdit.Cp.CustomerProgramName = ProgramNameFld.Text;
            cpEdit.Cp.Path2Picture = Path2PictureFld.Text;
            cpEdit.Cp.CustomerStyle = CpStyle.Text;
            cpEdit.Cp.Srp = "" + ConvertToDouble(Srp.Text);
            cpEdit.Cp.CpPropertyCustId = CustId.Text;
            cpEdit.Cp.Description = CpDescrip.Text;
            cpEdit.Cp.Comment = CpComments.Text;
            if (!VendorAsCustomer.Checked)
            {
                    cpEdit.Vendor = GetCustomerModelFromView(VendorList.SelectedValue);
            }
            //-- Operations Tab
            var opChecked = new List<string>();
            foreach (TreeNode node in TreeOpers.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0) continue;
                var operKey = node.Value.Split('_'); //-- OperationTypeOfficeId_OperationTypeId
                if (operKey.Length > 1)
                {
                    opChecked.Add(operKey[1]);
                }
            }
            cpEdit.OperationTypes = opChecked;

            //-- Requirements Tab
            SaveCpDocChanges();

            //== Pricing Tab ========

            //-- Pricing, Fail Tab
            cpEdit.Cp.FailDiscount = ConvertToDouble(FailDiscountFld.Text);
            cpEdit.Cp.FailFixed = ConvertToDouble(FailFixedFld.Text);

            //-- Pricing, Pass Tab
            cpEdit.Cp.IsFixed = IsFixedFld.SelectedIndex == 0;
            if (cpEdit.Cp.IsFixed)
            {
                cpEdit.CpPrice.PricePartMeasures = new List<PricePartMeasureModel>();
                cpEdit.CpPrice.PriceRanges = new List<PriceRangeModel>();
                cpEdit.Cp.FixedPrice = ConvertToDouble(PassFixedPrice.Text);
                cpEdit.Cp.Discount = ConvertToDouble(PassDiscount.Text);
                cpEdit.Cp.DeltaFix = 0;
            }
            else
            {
                cpEdit.CpPrice.PriceRanges = GetPriceRangeValues();
                cpEdit.Cp.FixedPrice = 0;
                cpEdit.Cp.Discount = 0;
                cpEdit.Cp.DeltaFix = ConvertToDouble(DeltaFix.Text);
            }

            //-- Additinal Services
            var addServices = GetAddServicePriceValues();
            cpEdit.CpPrice.AddServicePrices =
                addServices.FindAll(m => !string.IsNullOrEmpty(m.ServiceId) && m.Price > 0);

            return cpEdit;
        }

        /// <summary>
        /// On CpList changing and New SKU action
        /// </summary>
        /// <param name="cpModel"></param>
        private void SetCpDetails(CustomerProgramModel cpModel)
        {
            if (cpModel != null)
            {
                var editCpModel = cpModel.CpId == "0" ?
                    QueryCpUtilsNew.InitCustomerProgram(cpModel, GetEnumMeasure(), this) :
                    QueryCpUtilsNew.GetCustomerProgramFull(cpModel, GetEnumMeasure(), this);
                editCpModel.Customer = GetCustomerModelFromView(editCpModel.Cp.CustomerId);
                if (!VendorAsCustomer.Checked)
                {
                    editCpModel.Vendor = GetCustomerModelFromView(VendorList.SelectedValue);
                    if (VendorList.SelectedValue != "")
                        editCpModel.Cp.VendorId = VendorList.SelectedValue;
                    else
                        editCpModel.Vendor = GetCustomerModelFromView(editCpModel.Cp.VendorId);
                }
                else
                    editCpModel.Vendor = GetCustomerModelFromView(editCpModel.Cp.VendorId);
                SetViewState(editCpModel, SessionConstants.CpEditModel);

                ProgramNameFld.Text = cpModel.CustomerProgramName;
                Path2PictureFld.Text = cpModel.Path2Picture;
                CpStyle.Text = cpModel.CustomerStyle;
                Srp.Text = cpModel.Srp;
                CustId.Text = cpModel.CpPropertyCustId;
                hdnCustId.Value = cpModel.CustomerId;

                CpDescrip.Text = cpModel.Description;
                CpComments.Text = cpModel.Comment;
                //-- Load ItemTypeGroup Icon
                var group = GetItemTypeGroup(cpModel);
                if (group != null)
                {
                    itGroupName.Text = group.ItemTypeGroupName;
                    itGroupIcon.ImageUrl = "~/Images/ItemTypeImages/" + group.Path2Icon;
                }
                //add comment to commentlist
                foreach (var doc in editCpModel.CpDocs)//display CP internal comments
                {
                    if (doc.IsDefault)
                    {
                        CommentsList.Text = doc.Description;
                        break;
                    }
                }
                //-- Load ItemType Icon
                var itemType = GetItemType(cpModel);
                if (itemType != null)
                {
                    itName.Text = itemType.ItemTypeName;
                    itIcon.ImageUrl = "~/Images/ItemTypeImages/" + itemType.Path2Icon;
                    itId.Text = "" + cpModel.ItemTypeId;
                }
                //-- Load Picture
                ShowPicture(cpModel.Path2Picture);

                //-- Load Print Docs Attached to Cp
                LoadCpReportsTree();

                //-- Load Operations Tree
                LoadTreeOperations(editCpModel);

                //-- Load Parts Tree
                LoadDescripPartsTree(editCpModel);

                //-- Load Docs
                LoadCpDocsList(editCpModel, "");

                //-- Load Doc PartsTree
                LoadDocPartsTree(editCpModel);

                //-- Load Pricing
                LoadPricePartsTree(editCpModel);
                LoadPricingPanel(editCpModel);
                PricePartTree.Nodes[0].Select();
                OnPricePartsTreeChanged(null, null);
                GetCheckedParts(editCpModel);
            }
        }
        private void ShowPicture(string dbPicture)
        {
            itemPicture.ImageUrl = "";
            //IvanB 15.02.2024 start
            if (dbPicture == null || dbPicture == "")
            {
                ErrPictureField.Text = "no pictures";
                return;
            }
            else 
            {
                int picLength = dbPicture.Length;
                string tmpPic = dbPicture.ToLower();
                dbPicture = dbPicture.Replace(@"\", @"/");
                if (dbPicture.Substring(picLength - 1, 1) == @"/" || tmpPic.Contains("default"))
                {
                    ErrPictureField.Text = "no pictures";
                    return;
                }
            }
            //IvanB 15.02.2024 end

            //string imgPath;
            string errMsg;
            var ms = new MemoryStream();
            var fileType = "";
            var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
            //var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
                var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exist.";
            }
            ErrPictureField.Text = errMsg;

        }

        private void OnCorelFileChange(string aRep = null)
        {
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
            NewReportPictureBatchButton.ImageUrl = "";
            NewReportPictureBatchButton.Visible = false;
            try
            {
                string docName = null, typeId = null, typeCode = null;

                var cpEditModel = GetCpEditFromView();
                var cpDocModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == CpDocIdHidden.Value);
                if (DocAttachedReportsList.SelectedValue != "")
                {
                    var attachedReport = cpDocModel.AttachedReports.Find(m => "" + m.Key == DocAttachedReportsList.SelectedValue);
                    docName = attachedReport.DocumentName;
                    typeId = attachedReport.OperationTypeId;
                    typeCode = attachedReport.DocumentTypeCode;
                }
                else if (aRep != null)
                {
                    docName = aRep;
                    var attachedReport = cpDocModel.AttachedReports.Find(m => "" + m.DocumentName == docName);
                    if (attachedReport != null)
                        typeId = attachedReport.OperationTypeId;
                    else
                        return;
                }
                else
                    return;
                string corelFile = QueryCpUtilsNew.GetCorelFileName(docName, typeId, this);
                corelFile = corelFile.ToLower();
                string errMsg = "";
                //var corelFile = DocAttachedReportsList.SelectedItem == null ? "" : DocAttachedReportsList.SelectedItem.Text.Trim();
                bool rightTypeCode = (typeCode == "1" || typeCode == "2" || typeCode == "3" || typeCode == "4" || typeCode == "7" || typeCode == "15");
                string imageName = null;
                bool skuFound = false;
                if (rightTypeCode)
                {
                    string filter = ProgramNameFld.Text + @", " + CustomerLike.Text;
                    var existsFiles = Utlities.GetSkuDrawFilesAzure(filter, this, out errMsg);
                    foreach (var file in existsFiles)
                    {
                        if (file.Name.Contains(ProgramNameFld.Text) && file.Name.Contains(CustomerLike.Text))
                        {
                            string[] parts = file.Name.Split(',');
                            //string custCode = parts[3].Substring(1, parts[3].IndexOf(".")-1);
                            //string sku = parts[2].Substring(1);
                            string custCode = parts[3].Substring(1, parts[3].IndexOf(".") - 1);
                            string sku = parts[2];
                            if (custCode.Trim() == CustomerLike.Text.Trim() && sku.Trim() == ProgramNameFld.Text.Trim())
                                imageName = file.Name;//parts[3].Substring(1);
                            //imageName = file.Name;
                            break;
                        }
                    }
                    if (imageName != null)
                    {
                        MemoryStream skuMs = new MemoryStream();
                        skuFound = Utlities.GetSkuJpgUrlAzure(imageName, this, out errMsg, out skuMs);
                    }
                }
                //if (rightTypeCode)
                //{
                //    var existsFiles = Utlities.GetSkuDrawFilesAzure(this, out errMsg);
                //    foreach (var file in existsFiles)
                //    {
                //        if (file.Name.Contains(ProgramNameFld.Text.ToLower()))
                //        {
                //            imageName = file.Name;
                //            break;
                //        }
                //    }
                //    if (imageName != null)
                //    {
                //        MemoryStream skuMs = new MemoryStream();
                //        skuFound = Utlities.GetSkuJpgUrlAzure(imageName, this, out errMsg, out skuMs);
                //    }
                //}

                if (skuFound)
                {
                    //var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"sku_images/" + imageName.ToLower();
                    var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"sku_jpg/" + imageName;
                    //ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/cdr_to_jpg/fdx.jpg";
                    NewReportPictureButton.ImageUrl = ImageUrl;
                    NewReportPictureButton.Width = 250;
                    NewReportPictureButton.Visible = true;
                }
                else if (corelFile.Contains(".xls"))
                {
                    var ImageUrl = "~/Images/ajaxImages/no_image1.png";
                    NewReportPictureButton.ImageUrl = ImageUrl;
                    NewReportPictureButton.Width = 100;
                    NewReportPictureButton.Visible = true;
                }
                else
                {
                    corelFile = corelFile.Replace(".cdr", ".jpg");
                    //corelFile = "2361400.jpg";
                    
                    MemoryStream ms = new MemoryStream();
                    //Stream stream = new Stream();
                    var fileType = "jpg";
                    bool found = Utlities.GetJpgUrlAzure(corelFile.ToLower(), this, out errMsg, out ms);
                    //Stream stream = Utlities.GetPdfUrlAzure(corelFile, this, out errMsg);
                    if (found)
                    {
                        //var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                        var ImageUrl = "" + Session[SessionConstants.WebPictureShapeRoot] + @"cdr_to_jpg/" + corelFile.ToLower();
                        //ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/cdr_to_jpg/fdx.jpg";
                        NewReportPictureButton.ImageUrl = ImageUrl;
                        NewReportPictureButton.Width = 250;
                        NewReportPictureButton.Visible = true;
                    }
                    else
                    {
                        var ImageUrl = "~/Images/ajaxImages/no_image1.png";
                        NewReportPictureButton.ImageUrl = ImageUrl;
                        NewReportPictureButton.Width = 100;
                        NewReportPictureButton.Visible = true;
                    }
                }
                if (BatchNumberFld.Text != null && BatchNumberFld.Text != "")
                {
                    Utils.DissectItemNumber(BatchNumberFld.Text, out string groupCode, out var batchCode, out _);
                    string customerProgramName = cpEditModel.Cp.CustomerProgramName;
                    DataTable dtOrigCorelFileName =  QueryCpUtilsNew.GetCorelFileForCp(groupCode, batchCode, customerProgramName, typeCode, this);
                    string origCorelFileName = null, origDocName = null;
                    if (dtOrigCorelFileName != null && dtOrigCorelFileName.Rows.Count > 0)
                    {
                        origCorelFileName = dtOrigCorelFileName.Rows[0].ItemArray[1].ToString();
                        origDocName = dtOrigCorelFileName.Rows[0].ItemArray[0].ToString();

                        if (origCorelFileName.Contains(".xls"))
                        {
                            var ImageUrlBatch = "~/Images/ajaxImages/no_image2.png";
                            NewReportPictureBatchButton.ImageUrl = ImageUrlBatch;
                            NewReportPictureBatchButton.Width = 100;
                            NewReportPictureBatchButton.Visible = true;
                        }
                        else
                        {
                            //if (origDocName == docName)
                            //    return;
                            origCorelFileName = origCorelFileName.Replace(".cdr", ".jpg");
                            MemoryStream msBatch = new MemoryStream();
                            //Stream stream = new Stream();
                            var fileTypeBatch = "jpg";
                            bool foundBatch = Utlities.GetJpgUrlAzure(origCorelFileName.ToLower(), this, out errMsg, out msBatch);
                            if (foundBatch)
                            {

                                //var ImageUrlBatch = "data:image/" + fileTypeBatch + ";base64," + Convert.ToBase64String(msBatch.ToArray(), 0, msBatch.ToArray().Length);
                                var ImageUrlBatch = "" + Session[SessionConstants.WebPictureShapeRoot] + @"cdr_to_jpg/" + corelFile.ToLower();
                                NewReportPictureBatchButton.ImageUrl = ImageUrlBatch;
                                NewReportPictureBatchButton.Width = 250;
                                NewReportPictureBatchButton.Visible = true;
                            }
                            else
                            {
                                var ImageUrlBatch = "~/Images/ajaxImages/no_image2.png";
                                NewReportPictureBatchButton.ImageUrl = ImageUrlBatch;
                                NewReportPictureBatchButton.Width = 100;
                                NewReportPictureBatchButton.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        var ImageUrlBatch = "~/Images/ajaxImages/no_image2.png";
                        NewReportPictureBatchButton.ImageUrl = ImageUrlBatch;
                        NewReportPictureBatchButton.Width = 100;
                        NewReportPictureBatchButton.Visible = true;
                    }

                }
                //using (var fileStream = new FileStream(dest, FileMode.Create, FileAccess.Write))
                //{
                //    stream.CopyTo(fileStream);
                //}
                //PdfFrame.Src = "PdfMapHandler.ashx?pdf=" + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                //PdfFrame.Attributes["src"] = "PdfMapHandler.ashx?pdf=" + dest;// + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
            }
            
        }
        #endregion

        #region Description tab
        private void LoadDescripPartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            DescriptionPartsTreeView.Nodes.Clear();
            DescriptionPartsTreeView.Nodes.Add(rootNode);
            DescriptionPartsTreeView.Nodes[0].Select();
            OnDescripPartsTreeChanged(null, null);
        }
        protected void OnDescripPartsTreeChanged(object sender, EventArgs e)
        {
            var selNode = DescriptionPartsTreeView.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var parts = GetCpEditFromView().MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            if (part != null)
            {
                var measures = GetMeasuresShortByPartType(part.PartTypeId);
                DescripMeasuresField.DataSource = measures;
                DescripMeasuresField.DataBind();
                if (KeepRangeBox.Checked)
                {
                    List<FractionRangesModel> rangeList = Session["RangeList"] as List<FractionRangesModel>;
                    List<FractionRangesGroupModel> rangeGroups = Session["RangeGroups"] as List<FractionRangesGroupModel>;
                    if (rangeList != null && rangeList.Count > 0 && rangeGroups != null && rangeGroups.Count > 0)
                    {
                        string groupName = rangeList[0].GroupName;
                        FractionRangesList.DataSource = rangeGroups;
                        FractionRangesList.DataBind();
                        //FractionRangesList.Items.Insert(0, new ListItem("Please Select Range Group", ""));
                        for (int i = 0; i <= rangeGroups.Count - 1; i++)
                        {
                            if (rangeGroups[i].GroupName == groupName)
                            {
                                FractionRangesList.SelectedIndex = i;
                                var fractionName = GetFraction();
                                var range = rangeList.Find(m => m.Fraction == fractionName);
                                if (range != null)
                                {
                                    FullRangeFld.Value = range.RangeName;
                                    FractionRangeFld.Text = fractionName;
                                    FractionRangeFld.Visible = true;
                                }
                                else
                                {
                                    FullRangeFld.Value = "";
                                    FractionRangeFld.Text = "";
                                }
                                break;
                            }
                        }
                    }
                }
                else
                {
                    FullRangeFld.Value = "";
                    FractionRangeFld.Text = "";
                    //FractionRangesList.SelectedIndex = 0;
                }
            }
        }
        private string GetDescripChars()
        {
            if (DescriptionPartsTreeView.SelectedNode == null || DescripMeasuresField.SelectedItem == null) return "";
            return string.Format("[{0}.{1}]", DescriptionPartsTreeView.SelectedNode.Text.Trim(),
                                             DescripMeasuresField.SelectedItem.Text.Trim());
        }
        protected void OnCopyCharToCommentClick(object sender, ImageClickEventArgs e)
        {
            CpComments.Text = CpComments.Text + " " + GetDescripChars();
        }
        protected void OnCopyCharToDescripClick(object sender, ImageClickEventArgs e)
        {
            CpDescrip.Text = CpDescrip.Text + " " + GetDescripChars();
        }
		protected void OnCopyRangeToCommentsClick(object sender, EventArgs e)
        {
            string unit = UnitList.SelectedValue;
            //rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" CT.T.W.";
            string rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " " + unit + "\" the actual weight may range between " + FullRangeFld.Value + @" " + unit;
            if (CpComments.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
                CpComments.Text = CpComments.Text.Substring(0, CpComments.Text.ToUpper().LastIndexOf("WHEN WEIGHT IS EXPRESSED AS FRACTION"));
            if (UpperCaseBox.Checked)
                CpComments.Text = (CpComments.Text + " " + rangeEntry).ToUpper();
            else
                CpComments.Text = CpComments.Text + " " + rangeEntry;
        }
        protected void OnCopyRangeToDescriptionClick(object sender, EventArgs e)
        {
            string unit = UnitList.SelectedValue;
            string rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " " + unit + "\" the actual weight may range between " + FullRangeFld.Value + " " + unit;
            if (CpDescrip.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
                CpDescrip.Text = CpDescrip.Text.Substring(0, CpDescrip.Text.LastIndexOf("When weight is expressed as fraction"));
            if (UpperCaseBox.Checked)
                CpDescrip.Text = CpDescrip.Text + " " + rangeEntry.ToUpper();
            else
                CpDescrip.Text = (CpDescrip.Text + " " + rangeEntry);
        }
        //protected void OnCopyRangeToCommentsClick(object sender, EventArgs e)
        //{
        //    string unit = UnitList.SelectedValue;
        //    string rangeEntry = null;
        //    //rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" CT.T.W.";
        //    if (UpperCaseBox.Checked)
        //    {
        //        rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" ct(s)";
        //        //rangeEntry = rangeEntry.ToUpper();
        //    }
        //    else
        //        rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" ct(s).";
        //    if (CpComments.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
        //        CpComments.Text = CpComments.Text.Substring(0, CpComments.Text.ToUpper().LastIndexOf("WHEN WEIGHT IS EXPRESSED AS FRACTION"));
        //    //if (CpComments.Text.Contains("When weight is expressed as fraction"))
        //    //    CpComments.Text = CpComments.Text.Substring(0, CpComments.Text.LastIndexOf("When weight is expressed as fraction"));
        //    if (UpperCaseBox.Checked)
        //        CpComments.Text = (CpComments.Text + " " + rangeEntry).ToUpper();
        //    else
        //        CpComments.Text = CpComments.Text + " " + rangeEntry;
        //}
        //protected void OnCopyRangeToDescriptionClick(object sender, EventArgs e)
        //{
        //    string unit = UnitList.SelectedValue;
        //    string rangeEntry = null;
        //    //rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" CT.T.W.";
        //    if (UpperCaseBox.Checked)
        //        // rangeEntry = rangeEntry.ToUpper();
        //        rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" ct(s).";
        //    else
        //        rangeEntry = "When weight is expressed as fraction \"" + FractionRangeFld.Text + " ct(s)\" the actual weight may range between " + FullRangeFld.Value + @" ct(s).";
        //    if (CpDescrip.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
        //        CpDescrip.Text = CpDescrip.Text.Substring(0, CpDescrip.Text.LastIndexOf("When weight is expressed as fraction"));
        //    if (UpperCaseBox.Checked)
        //        CpDescrip.Text = CpDescrip.Text + " " + rangeEntry.ToUpper();
        //    else
        //        CpDescrip.Text = (CpDescrip.Text + " " + rangeEntry);
        //}
        protected void OnFractionGroupClick(object sender, EventArgs e)
        {
            FractionRangesList.Visible = true;
            FractionRangeFld.Visible = true;
            FractionRangeFld.Enabled = false;
            FractionRangeFld.Text = "";
            FullRangeFld.Value = "";
            var dt = new DataTable();
            dt = QueryCpUtilsNew.GetRangeGroupList(this);
            if (dt != null && dt.Rows.Count != 0)
            {
                List<FractionRangesGroupModel> rangeGroups = (from DataRow row in dt.Rows select new FractionRangesGroupModel(row)).ToList();
                Session["RangeGroups"] = rangeGroups;
                FractionRangesList.DataSource = rangeGroups;
                FractionRangesList.DataBind();
                FractionRangesList.SelectedIndex = -1;
                FractionRangesList.Items.Insert(0, new ListItem("Please Select Range Group", ""));
                
            }
            UnitList.Visible = true;
        }
        protected void OnRangeGroupSelectedChanged(object sender, EventArgs e)
        {
            var rangeGroup = FractionRangesList.SelectedItem.Text;
            var fractionName = GetFraction();
            if (fractionName != null)
            {
                FractionRangeFld.Text = fractionName;
                FractionRangeFld.Visible = true;
            }
            var dt = QueryCpUtilsNew.GetFractionRangeList(rangeGroup, this);
            if (dt != null && dt.Rows.Count != 0)
            {
                List<FractionRangesModel> rangeList = (from DataRow row in dt.Rows select new FractionRangesModel(row)).ToList();
                Session["RangeList"] = rangeList;
                var range = rangeList.Find(m => m.Fraction == fractionName);
                if (range != null)
                    FullRangeFld.Value = range.RangeName;
            }

        }
        private string GetFraction()
        {
            var cpEditModel = GetCpEditFromView();
            var partId = DescriptionPartsTreeView.SelectedValue;
            var editRules = cpEditModel.CpDocs.Find(m => m.IsDefault == false).EditDocRules;
            var editRulesNow = editRules.FindAll(m => m.PartId == partId);
            string fraction = editRulesNow.Find(m => m.MeasureId == 98).DisplayMinValue;
            return fraction;
        }
        #endregion

        #region Available Operations
        protected void TreeOperationCheckChanged(object sender, TreeNodeEventArgs e)
        {


        }
        protected void OnTreeOperationSelectedChanged(object sender, EventArgs e)
        {

        }
        private void LoadTreeOperations(CpEditModel cpModel)
        {
            var allOPers = QueryCpUtilsNew.GetAllOperations(this);
            cpModel.OperationTypeOfficeId =
                allOPers.FindAll(m => !string.IsNullOrEmpty(m.OperationTypeOfficeId))[0].OperationTypeOfficeId;
            foreach (var oper in allOPers.Where(oper => string.IsNullOrEmpty(oper.TreeParentId)))
            {
                oper.TreeParentId = "0";
            }
            var cpOpers = cpModel.OperationTypes;

            var cpTreeIds = cpOpers.Select(o => allOPers.Find(m => m.OperationTypeId == o).TreeId).ToList();

            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Operations" } };
            foreach (var item in allOPers)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.TreeId,
                        ParentId = item.TreeParentId,
                        DisplayName = item.TreeItemName

                    });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            TreeUtils.CheckNodes(rootNode, cpTreeIds);
            rootNode.Expand();
            TreeOpers.Nodes.Clear();
            TreeOpers.Nodes.Add(rootNode);
        }
        #endregion

        #region Requirements TAB
        private void LoadDocPartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = new List<TreeViewModel>();
            foreach (var part in parts)
            {
                data.Add(new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName });
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            DocPartTree.Nodes.Clear();
            DocPartTree.Nodes.Add(rootNode);
            if (DocPartTree.Visible)
            {
                DocPartTree.Nodes[0].Select();
            }
            OnDocPartsTreeChanged(null, null);
        }
        protected void OnDocPartsTreeChanged(object sender, EventArgs e)
        {
            ApplyRulesAll();
            LoadCpDocRules(CpDocsList.SelectedValue, DocPartTree.SelectedValue);
        }
        private void LoadCpDocsList(CpEditModel cpModel, string selectDocId)
        {
            var docs = cpModel.CpDocs;
            //for (var i = 0; i < docs.Count; i++)
            //{
            //    docs[i].Title = docs[i].IsDefault ? "Default" : "Document " + (i);
            //}
            List<CpDocModel> newdocs = new List<CpDocModel>();
            for(var i=0; i<docs.Count; i++)
            {
                if (docs[i].IsDefault)
                {
                    docs[i].Title = "Default";
                    newdocs.Add(docs[i]);
                    break;
                }
            }
            int j = 1;
            for (var i=0; i<docs.Count; i++)
            {
                
                if (docs[i].IsDefault)
                    continue;
                docs[i].Title = "Document " + (j);
                newdocs.Add(docs[i]);
                j++;
            }
            //CpDocsList.DataSource = docs;
            CpDocsList.DataSource = newdocs;
            CpDocsList.DataBind();
            if (docs.Count <= 0) return;
            string docId = null;
            if (Session["DocId"] != null)
            {
                docId = (string)Session["DocId"];
                selectDocId = docId;
            }
            //CpDocsList.SelectedValue = !string.IsNullOrEmpty(selectDocId) ? selectDocId : "" + docs[0].CpDocId;
            CpDocsList.SelectedValue = !string.IsNullOrEmpty(selectDocId) ? selectDocId : "" + newdocs[0].CpDocId;
            OnCpDocsListChanged(null, null);
        }
        protected void OnCpDocsListChanged(object sender, EventArgs e)
        {
            NewReportPictureButton.ImageUrl = "";
            NewReportPictureButton.Visible = false;
            NewReportPictureBatchButton.ImageUrl = "";
            NewReportPictureBatchButton.Visible = false;
            PartName.Text = "";
            PartName.Visible = false;
            BulkCopyButton.Visible = false;
            if (Session["AttachedReport"] != null || Session["BackToSku"] != null)
                OnRefreshCpDocumentsClick(null, null);
            SaveCpDocChanges();
            CpDocIdHidden.Value = CpDocsList.SelectedValue;
            if (CpDocsList.SelectedItem.Text == "Default")
            {
                DocPartTree.Visible = false;
            }
            else
                DocPartTree.Visible = true;
            LoadCpDocRules(CpDocsList.SelectedValue, DocPartTree.SelectedValue);
            LoadDocMeasureGroups(CpDocsList.SelectedValue);
            //if (Session["CpReturnData"] != null) ??????????????????????????
            LoadDocPrintDocs(CpDocsList.SelectedValue);
            var doc = GetCpEditFromView().CpDocs.Find(m => "" + m.CpDocId == CpDocsList.SelectedValue);
            if (doc.IsDefault)
            {
                PartName.Visible = false;
                BulkCopyButton.Visible = false;
            }
            else
            {
                if (DocPartTree != null && DocPartTree.SelectedNode != null)
                {
                    PartName.Visible = true;
                    BulkCopyButton.Visible = true;
                }

            }
            DocDescripField.Text = doc == null ? "" : doc.Description;
            IsReturnDoc.Checked = doc != null && doc.IsReturn;
            DelDocumentBtn.Enabled = doc != null && !doc.IsDefault;
        }
        protected void OnAddDocumentClick(object sender, EventArgs e)
        {
            SaveCpDocChanges();
            var cpEditModel = GetCpEditFromView();
            var docId = QueryCpUtilsNew.AddCpDocModel(cpEditModel, GetEnumMeasure(), false, this);
            LoadCpDocsList(cpEditModel, docId);
        }
        protected void OnDelDocumentClick(object sender, EventArgs e)
        {
            var cpDocId = CpDocsList.SelectedValue;
            if (string.IsNullOrEmpty(cpDocId) || CpDocsList.SelectedIndex == 0) return;
            CpDocIdHidden.Value = "";
            var cpEditModel = GetCpEditFromView();

            var docModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == cpDocId);
            if (docModel == null) return;
            cpEditModel.CpDocs.Remove(docModel);
            var lastDoc = cpEditModel.CpDocs[cpEditModel.CpDocs.Count - 1];
            LoadCpDocsList(cpEditModel, "" + lastDoc.CpDocId);
        }
        private void SaveCpDocChanges()
        {
            var docId = CpDocIdHidden.Value;
            if (string.IsNullOrEmpty(docId)) return;
            var cpEditModel = GetCpEditFromView();
            var cpDocModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
            if (cpDocModel == null) return;

            cpDocModel.Description = DocDescripField.Text;
            cpDocModel.IsReturn = IsReturnDoc.Checked;

            //-- Apply Recheck Measure Groups
            ApplyRechecksAll();

            //-- Apply Rules
            ApplyRulesAll();

            //-- Attached Reports - DONE

        }
        #endregion
        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var serviceFld = (DropDownList)sender;
            var item = serviceFld.NamingContainer as DataGridItem;

            if (item == null) return;
            var rownum = "" + RulesGrid.DataKeys[item.ItemIndex];
            DataGridItem dgItem = RulesGrid.Items[item.ItemIndex];
            var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
            var keys = uniqueKey.Split(';');
            var docId = keys[0];
            var partId = keys[1];
            var measureId = keys[2];
            int idx = item.ItemIndex;
            int idx1 = 5;
            List<EnumMeasureModel> measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel>;
            var rule = GetRuleByKey(uniqueKey);
            if (rule == null) return;
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                var minFld = dgItem.FindControl(RuleNumericFldMin) as TextBox;
                var maxFld = dgItem.FindControl(RuleNumericFldMax) as TextBox;
                if (minFld != null && maxFld != null)
                {
                    var minV = Convert.ToDouble(minFld.ToString());
                    var maxV = Convert.ToDouble(maxFld.ToString());
                    if (maxV < minV)
                    {
                        var cpEditModel = GetCpEditFromView();
                        bool found = false;
                        for (int i = 0; i <= cpEditModel.CpDocs.Count - 1; i++)
                        {
                            if (cpEditModel.CpDocs[i].CpDocId != Convert.ToInt32(docId))
                                continue;
                            for (int j = 0; j <= cpEditModel.CpDocs[i].EditDocRules.Count - 1; j++)
                            {
                                if (cpEditModel.CpDocs[i].EditDocRules[j].UniqueKey == uniqueKey)
                                {
                                    cpEditModel.CpDocs[i].EditDocRules[j].MaxValue = minFld.ToString();
                                    cpEditModel.CpDocs[i].EditDocRules[j].MinValue = minFld.ToString();
                                    SetViewState(cpEditModel, SessionConstants.CpEditModel);
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                            {
                                LoadCpDocRules(docId, partId, idx, idx1);
                                break;
                            }
                        }
                    }
                    if (RulesGrid.Items.Count > 0)
                        RulesGrid.Items[idx].Cells[idx1].Focus();
                }
            }
            else
            {
                var minFld = dgItem.FindControl(RuleEnumFldMin) as DropDownList;
                var maxFld = dgItem.FindControl(RuleEnumFldMax) as DropDownList;
                if (minFld != null && maxFld != null)
                {
                    int minValue = Convert.ToInt32(minFld.SelectedValue);
                    int maxValue = Convert.ToInt32(maxFld.SelectedValue);
                    if (minValue == 0 || maxValue == 0)
                        return;
                    int minNorder = measures.Find(m => m.MeasureValueId == minValue).NOrder;
                    int maxNorder = measures.Find(m => m.MeasureValueId == maxValue).NOrder;
                    if (maxNorder < minNorder)
                    {
                        var cpEditModel = GetCpEditFromView();
                        bool found = false;
                        for (int i = 0; i <= cpEditModel.CpDocs.Count - 1; i++)
                        {
                            if (cpEditModel.CpDocs[i].CpDocId != Convert.ToInt32(docId))
                                continue;
                            for (int j = 0; j <= cpEditModel.CpDocs[i].EditDocRules.Count - 1; j++)
                            {
                                if (cpEditModel.CpDocs[i].EditDocRules[j].UniqueKey == uniqueKey)
                                {
                                    cpEditModel.CpDocs[i].EditDocRules[j].MaxValue = minFld.SelectedValue;
                                    cpEditModel.CpDocs[i].EditDocRules[j].MinValue = minFld.SelectedValue;
                                    SetViewState(cpEditModel, SessionConstants.CpEditModel);
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                            {
                                LoadCpDocRules(docId, partId, idx, idx1);
                                break;
                            }
                        }
                    }
                    else if (RulesGrid.Items.Count > 0)
                        RulesGrid.Items[idx].Cells[idx1].Focus();
                }
            }
        }
        protected void SelectedIndexChangedMin(object sender, EventArgs e)
        {
            var serviceFld = (DropDownList)sender;
            var item = serviceFld.NamingContainer as DataGridItem;

            if (item == null) return;
            var rownum = "" + RulesGrid.DataKeys[item.ItemIndex];
            DataGridItem dgItem = RulesGrid.Items[item.ItemIndex];
            var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
            var keys = uniqueKey.Split(';');
            var docId = keys[0];
            var partId = keys[1];
            var measureId = keys[2];
            int idx = item.ItemIndex;
            List<EnumMeasureModel> measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel>;
            var rule = GetRuleByKey(uniqueKey);
            if (rule == null)
            {
                if (RulesGrid.Items.Count > 0)
                    RulesGrid.Items[idx].Cells[4].Focus();
                return;
            }
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                var minFld = dgItem.FindControl(RuleNumericFldMin) as TextBox;
                var maxFld = dgItem.FindControl(RuleNumericFldMax) as TextBox;
                if (minFld != null && maxFld != null)
                {
                    var minV = Convert.ToDouble(minFld.ToString());
                    var maxV = Convert.ToDouble(maxFld.ToString());
                    maxV = minV;
                    if (maxV <= minV)
                    {	
                        ApplyRulesAll();
                        var cpEditModel = GetCpEditFromView();
                        bool found = false;
                        for (int i = 0; i <= cpEditModel.CpDocs.Count - 1; i++)
                        {
                            if (cpEditModel.CpDocs[i].CpDocId != Convert.ToInt32(docId))
                                continue;
                            for (int j = 0; j <= cpEditModel.CpDocs[i].EditDocRules.Count - 1; j++)
                            {
                                if (cpEditModel.CpDocs[i].EditDocRules[j].UniqueKey == uniqueKey)
                                {
                                    cpEditModel.CpDocs[i].EditDocRules[j].MaxValue = minFld.ToString();
                                    cpEditModel.CpDocs[i].EditDocRules[j].MinValue = minFld.ToString();
                                    SetViewState(cpEditModel, SessionConstants.CpEditModel);
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                            {
                                LoadCpDocRules(docId, partId, idx, 4);
                                break;
                            }
                        }
                    }
                    if (RulesGrid.Items.Count > 0)
                        RulesGrid.Items[idx].Cells[4].Focus();
                    rule.MaxValue = rule.MinValue;
                }
            }
            else
            {
                var minFld = dgItem.FindControl(RuleEnumFldMin) as DropDownList;
                var maxFld = dgItem.FindControl(RuleEnumFldMax) as DropDownList;
                if (minFld != null && maxFld != null)
                {
                    int minValue = Convert.ToInt32(minFld.SelectedValue);
                    int maxValue = Convert.ToInt32(maxFld.SelectedValue);
                    if (minValue != 0 && maxValue == 0)
                        maxValue = minValue;
                    if (minValue == 0 || maxValue == 0)
                        return;
                    int minNorder = measures.Find(m => m.MeasureValueId == minValue).NOrder;
                    int maxNorder = measures.Find(m => m.MeasureValueId == maxValue).NOrder;
                    maxNorder = minNorder;

                    if (maxNorder <= minNorder)
                    {
                        ApplyRulesAll();
                        var cpEditModel = GetCpEditFromView();
                        bool found = false;
                        for (int i = 0; i <= cpEditModel.CpDocs.Count - 1; i++)
                        {
                            if (cpEditModel.CpDocs[i].CpDocId != Convert.ToInt32(docId))
                                continue;
                            for (int j = 0; j <= cpEditModel.CpDocs[i].EditDocRules.Count - 1; j++)
                            {
                                if (cpEditModel.CpDocs[i].EditDocRules[j].UniqueKey == uniqueKey)
                                {
                                    cpEditModel.CpDocs[i].EditDocRules[j].MaxValue = minFld.SelectedValue;
                                    cpEditModel.CpDocs[i].EditDocRules[j].MinValue = minFld.SelectedValue;
                                    if (measureId == "53")//species - reset list of varieties
                                    {
                                        string measureName = minFld.SelectedItem.ToString();
                                        //var newMeasures = ResetVarietiesList(measures, measureName);
                                        List<EnumMeasureModel> newMeasures = QueryUtils.GetVarietyBySpecies(measureName, this);
                                        List<CpDocRuleEditModel> editRulesNow = new List<CpDocRuleEditModel>();
                                        List<CpDocRuleEditModel> editRules = new List<CpDocRuleEditModel>();
                                        editRules = cpEditModel.CpDocs[i].EditDocRules;
                                        editRulesNow = cpEditModel.CpDocs[i].EditDocRules;
                                        editRulesNow = editRules.FindAll(m => m.PartId == partId);
                                        var varietyRule = editRulesNow.Find(m => m.MeasureName.Equals("Variety"));
                                        if (varietyRule != null)
                                        {
                                            varietyRule.EnumSource.Clear();
                                            foreach(EnumMeasureModel variety in newMeasures)
                                            {
                                                string valueTitle = variety.ValueTitle;
                                                string valueName = variety.MeasureValueName;
                                                int measureValueId = variety.MeasureValueId;
                                                var newEntry = new EnumMeasureModel
                                                {
                                                    ValueTitle = valueTitle,
                                                    MeasureValueName = valueName,
                                                    MeasureValueId = measureValueId,
                                                    MeasureClass = MeasureModel.MeasureClassEnum,
                                                    MeasureValueMeasureId = 54
                                                };
                                                varietyRule.EnumSource.Add(newEntry);
                                            }
                                            //Remove items from a list of shapes with a MeasureValueName equals value from block list except selected 
                                            //varietyRule.EnumSource = varietyRule.EnumSource.Where(x => newMeasures.Exists(y => (x.MeasureValueName.Equals(y.MeasureValueName)))).OrderBy(x => x.MeasureValueName).ToList();
                                            //remove items with empty MeasureValueName
                                            //varietyRule.EnumSource = varietyRule.EnumSource.Where(x => !string.IsNullOrEmpty(x.MeasureValueName.Trim())).Distinct().ToList();
                                            //Remove duplicates
                                            //varietyRule.EnumSource = varietyRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
                                        }
                                        //SetViewState(newMeasures, SessionConstants.CpCommonEnumMeasures);

                                    }
                                    SetViewState(cpEditModel, SessionConstants.CpEditModel);
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                            {
                                LoadCpDocRules(docId, partId, idx, 4);
                                break;
                            }
                        }
                    }
                    else if (RulesGrid.Items.Count > 0)
                        RulesGrid.Items[idx].Cells[4].Focus();
                }
                if (minFld != null)
                    rule.MaxValue = rule.MinValue;
            }

        }

        protected List<EnumMeasureModel> ResetVarietiesList(List<EnumMeasureModel> measures, string speciesName)
        {
            List<EnumMeasureModel> varieties = QueryUtils.GetVarietyBySpecies(speciesName, this);
            if (varieties == null || varieties.Count == 0)
                return measures;
            List<EnumMeasureModel> newMeasures = measures.ToList();
            for (int i=0; i<=measures.Count-1; i++)
            {
                if (measures[i].MeasureValueMeasureId == 54)
                    measures.RemoveAt(i);
            }
            foreach (EnumMeasureModel variety in varieties)
            {
                string valueTitle = variety.ValueTitle;
                string valueName = variety.MeasureValueName;
                int measureValueId = Convert.ToInt32(variety.MeasureValueId);
                var newEntry = new EnumMeasureModel
                {
                    ValueTitle = valueTitle,
                    MeasureValueName = valueName,
                    MeasureValueId = measureValueId,
                    MeasureClass = MeasureModel.MeasureClassEnum,
                    MeasureValueMeasureId = 54
                };
                newMeasures.Add(newEntry);
            }
            return newMeasures;
        }
        //protected List<EnumMeasureModel> ResetVarietiesList(List<EnumMeasureModel> measures, string speciesName)
        //{
        //    DataTable dt = QueryUtils.getSpeciesVariety(speciesName, this);
        //    if (dt == null || dt.Rows.Count == 0)
        //        return measures;
        //    List<EnumMeasureModel> newMeasures = measures.ToList();
        //    for (int i = 0; i <= measures.Count - 1; i++)
        //    {
        //        if (measures[i].MeasureValueMeasureId == 54)
        //            measures.RemoveAt(i);
        //    }
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        string valueTitle = dr["VarietyTitle"].ToString();
        //        string valueName = dr["VarietyName"].ToString();
        //        int measureValueId = Convert.ToInt32(dr["VarietyID"].ToString());
        //        var newEntry = new EnumMeasureModel
        //        {
        //            ValueTitle = valueTitle,
        //            MeasureValueName = valueName,
        //            MeasureValueId = measureValueId,
        //            MeasureClass = MeasureModel.MeasureClassEnum,
        //            MeasureValueMeasureId = 54
        //        };
        //        newMeasures.Add(newEntry);
        //    }
        //    return newMeasures;
        //}
        protected void OnCheckedBoxInfoChanged(object sender, EventArgs e)
        {
            //var checkedBoxChecked = (CheckBox)sender;
            var serviceFld = (CheckBox)sender;
            var item = serviceFld.NamingContainer as DataGridItem;
            if (serviceFld.Checked == true)
            {
                //CategoryItem item = args.Item.DataItem as CategoryItem;
                
                //Literal litItemName = Item.FindControl("litItemName") as Literal;
                CheckBox chkLinked = item.FindControl("CheckBoxIsDefault") as CheckBox;

                //litItemName.Text = item.Text;

                chkLinked.Checked = false;
                chkLinked.AutoPostBack = true;
                //chkLinked.InputAttributes.Add("Value", item.ID.ToString());
                //chkLinked.InputAttributes.Add("Value", item.Id.ToString());
            }
            int idx = item.ItemIndex;
            RulesGrid.Items[idx].Cells[6].Focus();
        }

        protected void OnCheckedBoxIsDefaultChanged(object sender, EventArgs e)
        {
            //var checkedBoxChecked = (CheckBox)sender;
            var serviceFld = (CheckBox)sender;
            var item = serviceFld.NamingContainer as DataGridItem;
            if (serviceFld.Checked == true)
            {
                //CategoryItem item = args.Item.DataItem as CategoryItem;
                
                //Literal litItemName = Item.FindControl("litItemName") as Literal;
                CheckBox chkLinked = item.FindControl("CheckBoxInfoOnly") as CheckBox;

                //litItemName.Text = item.Text;

                chkLinked.Checked = false;
                chkLinked.AutoPostBack = true;
                //chkLinked.InputAttributes.Add("Value", item.ID.ToString());
                //chkLinked.InputAttributes.Add("Value", item.Id.ToString());
            }
            int idx = item.ItemIndex;
            RulesGrid.Items[idx].Cells[6].Focus();
        }
        protected void OnBulkCopyBtnClick(object sender, EventArgs e)
        {
            List<CpDocRuleEditModel> editRulesNew = new List<CpDocRuleEditModel>();
            List<CpDocRuleEditModel> editRulesNow = new List<CpDocRuleEditModel>();
            
            var partId = DocPartTree.SelectedNode.Value;
            var cpEditModel = GetCpEditFromView();
            var docId = Convert.ToInt32(CpDocsList.SelectedValue);
            List<CpDocRuleEditModel> editRules = new List<CpDocRuleEditModel>();
            editRules = cpEditModel.CpDocs.Find(m => m.CpDocId == docId).EditDocRules;
            editRulesNow = editRules.FindAll(m => m.PartId == partId);
            var parts = cpEditModel.MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            var partTypeId = parts.Find(m => m.PartId == Convert.ToInt32(partId)).PartTypeId;
            foreach(var prt in parts)
            {
                if (prt.PartId == Convert.ToInt32(partId))
                {
                    //foreach (var item in editRulesNow)
                    //    editRulesNew.Add(item);
                    continue;
                }
                if (prt.PartTypeId == partTypeId)
                {
                    //var rulesToUpdate = editRules.FindAll(m => m.PartId == prt.PartId);
                    var editRulesSamePartType = editRules.FindAll(m => m.PartId == prt.PartId.ToString());
                    foreach (var item in editRulesNow)
                    {
                        var measureId = item.MeasureId;
                        var newItem = editRulesSamePartType.Find(m => m.MeasureId == measureId);
                        newItem.MaxValue = item.MaxValue;
                        newItem.MinValue = item.MinValue;

                        
                        //CpDocRuleEditModel ruleOrigin = editRulesNow.Find(m => m.MeasureId == measureId);
                        //CpDocRuleEditModel abc = (CpDocRuleEditModel)this.MemberwiseClone();
                        ////CpDocRuleEditModel newItem = new CpDocRuleEditModel();

                        ////newItem.UniqueKey = item.UniqueKey;
                        ////newItem.UniqueKey = "";
                        //var newItem = item;
                        //newItem.PartId = prt.PartId.ToString();
                        
                        //editRulesNew.Add(newItem);
                    }
                }
                else
                {
                    //var rulesNoUpdate = editRules.FindAll(m => m.PartId == prt.PartId.ToString());
                    //foreach (var item in rulesNoUpdate)
                    //    editRulesNew.Add(item);
                }
            }
            //foreach (var doc in cpEditModel.CpDocs)
            //{
            //    if (doc.CpDocId == docId)
            //    {
            //        doc.EditDocRules = editRulesNew;
            //        break;
            //    }
            //}
            SetViewState(cpEditModel, SessionConstants.CpEditModel);
        }
        protected void OnItemCommandClick(object sender, EventArgs e)
        {
            string abc = "aaa";
            string ddd = "bbb";
        }
        protected void SelectedIndexChanged2(object sender, EventArgs e)
        {

            string abc = "aaa";
            string ddd = "bbb";
        }
        #region Doc rules DataGrid
        private void LoadCpDocRules(string docId, string partId, int idx = 0, int idx1 = 0)
        {
            if (DocPartTree.SelectedNode != null)
            {
                PartName.Text = DocPartTree.SelectedNode.Text;
                PartName.Visible = true;
                BulkCopyButton.Visible = true;
            }
            else if (Session["SelectedDoc"] != null)
            {
                PartName.Text = (string)Session["SelectedDoc"];
                PartName.Visible = true;
                BulkCopyButton.Visible = true;
                Session["SelectedDoc"] = null;
            }
            else
            {
                PartName.Visible = false;
                BulkCopyButton.Visible = false;
            }
            var cpEditModel = GetCpEditFromView();
            var doc = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
            if (string.IsNullOrEmpty(docId) || string.IsNullOrEmpty(partId) || doc == null || doc.IsDefault)
            {
                RulesGrid.DataSource = null;
                RulesGrid.DataBind();
                RuleCountLabel.Text = "";
                //alex 6/22
                HdrLbl.Visible = false;
                Rules.Visible = false;
                //alex 6/22
                return;
            }

            if (doc.IsDefault)
            {
                RulesGrid.DataSource = null;
                RulesGrid.DataBind();
                //alex 6/22
                HdrLbl.Visible = false;
                Rules.Visible = false;
                //alex 6/22
                RuleCountLabel.Text = "";
                return;
            }
            var editRules = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId).EditDocRules;
            var editRulesNow = editRules.FindAll(m => m.PartId == partId);
            //Create and fill block list
            //IvanB 07/03 start
            var blockShapesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
            //IvanB 07/03 end
            var shapeRule = editRulesNow.Find(m => m.MeasureName.Equals("Shape (cut)"));
            if(shapeRule != null)
            {
                //remove items with empty MeasureValueName
                //alex commented line to release blank value of shape
                //shapeRule.EnumSource = shapeRule.EnumSource.Where(x => !string.IsNullOrEmpty(x.MeasureValueName.Trim())).Distinct().ToList();
                //alex
                //Remove items from a list of shapes with a MeasureValueName equals value from block list except selected 
                //IvanB 07/03 start
                shapeRule.EnumSource = shapeRule.EnumSource.Where(x => !blockShapesList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) && !shapeRule.DisplayMinValue.Equals(y.BlockedDisplayName))).OrderBy(x => x.MeasureValueName).ToList();
                //IvanB 07/03 end
                //Remove duplicates
                shapeRule.EnumSource = shapeRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            //alex 1/27/2023
            var blockKaratageList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Karatage, Page);
            var karatageRule = editRulesNow.Find(m => m.MeasureName.Equals("Karatage"));
            if (karatageRule != null)
            {
                karatageRule.EnumSource = karatageRule.EnumSource.Where(x => !blockKaratageList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) && !karatageRule.DisplayMinValue.Equals(y.BlockedDisplayName))).OrderBy(x => x.MeasureValueName).ToList();
                karatageRule.EnumSource = karatageRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            //alex 1/27/2023
            //alex 4/12/2024
            var blockItemNameList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ItemName, Page);
            var itemNameRule = editRulesNow.Find(m => m.MeasureName.Equals("Item Name"));
            if (itemNameRule != null)
            {
                itemNameRule.EnumSource = itemNameRule.EnumSource.Where(x => !blockItemNameList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) && !itemNameRule.DisplayMinValue.Equals(y.BlockedDisplayName))).OrderBy(x => x.MeasureValueName).ToList();
                itemNameRule.EnumSource = itemNameRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            var blockColorDiamondColorsList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ColorDiamondColors, Page);
            var colorDiamondColorsRule = editRulesNow.Find(m => m.MeasureName.ToLower().Equals("color diamond color"));
            if (colorDiamondColorsRule != null)
            {
                colorDiamondColorsRule.EnumSource = colorDiamondColorsRule.EnumSource.Where(x => !blockColorDiamondColorsList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) 
                && !colorDiamondColorsRule.DisplayMinValue.Equals(y.BlockedDisplayName))).OrderBy(x => x.MeasureValueName).ToList();
                colorDiamondColorsRule.EnumSource = colorDiamondColorsRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            //alex 4/12/2024
            //IvanB 07/03 start
            //Create and fill block list
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Variety, Page);
            var varietyRule = editRulesNow.Find(m => m.MeasureName.Equals("Variety"));
            if (varietyRule != null)
            {
                //Remove items from a list of shapes with a MeasureValueName equals value from block list except selected 
                varietyRule.EnumSource = varietyRule.EnumSource.Where(x => !blockList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) && !varietyRule.DisplayMinValue.Equals(y.BlockedDisplayName))).OrderBy(x => x.MeasureValueName).ToList();
                //remove items with empty MeasureValueName
                //varietyRule.EnumSource = varietyRule.EnumSource.Where(x => !string.IsNullOrEmpty(x.MeasureValueName.Trim())).Distinct().ToList();
                //Remove duplicates
                varietyRule.EnumSource = varietyRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            //alex 01/10
            var blockSpeciesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Species, Page);
            var speciesRule = editRulesNow.Find(m => m.MeasureName.Equals("Species"));
            if (speciesRule != null)
            {
                //Remove items from a list of shapes with a MeasureValueName equals value from block list except selected 
                speciesRule.EnumSource = speciesRule.EnumSource.Where(x => !blockSpeciesList.Exists(y => (x.MeasureValueName.Equals(y.BlockedDisplayName)) )).OrderBy(x => x.MeasureValueName).ToList();
                List<EnumMeasureModel> newMeasures = QueryUtils.GetVarietyBySpecies(speciesRule.DisplayMinValue, this);
                varietyRule.EnumSource.Clear();
                foreach (EnumMeasureModel variety in newMeasures)
                {
                    string valueTitle = variety.ValueTitle;
                    string valueName = variety.MeasureValueName;
                    int measureValueId = variety.MeasureValueId;
                    var newEntry = new EnumMeasureModel
                    {
                        ValueTitle = valueTitle,
                        MeasureValueName = valueName,
                        MeasureValueId = measureValueId,
                        MeasureClass = MeasureModel.MeasureClassEnum,
                        MeasureValueMeasureId = 54
                    };
                    varietyRule.EnumSource.Add(newEntry);
                }
                //remove items with empty MeasureValueName
                //varietyRule.EnumSource = varietyRule.EnumSource.Where(x => !string.IsNullOrEmpty(x.MeasureValueName.Trim())).Distinct().ToList();
                //Remove duplicates
                speciesRule.EnumSource = speciesRule.EnumSource.GroupBy(x => x.MeasureValueName).Select(x => x.FirstOrDefault()).ToList();
            }
            //alex 01/10
            //IvanB 07/03 end
            editRulesNow.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureName.ToUpper(), m2.MeasureName.ToUpper()));
            RulesGrid.DataSource = editRulesNow;
            //alex 6/24
            //HdrLbl.Style.Add("width", "700px");
            //HdrLbl.Style["width"] = "700px";
            //var lblWidth = RulesGrid.Width;
            //HdrLbl.Style["width"] = RulesGrid.Width.ToString();
            //alex 6/24
            //alex 6/23
            RulesGrid.Columns[0].ItemStyle.Width = 44;
            RulesGrid.Columns[2].ItemStyle.Width = 100;
            RulesGrid.Columns[4].ItemStyle.Width = 230;//min value
            RulesGrid.Columns[5].ItemStyle.Width = 230;//max value
            RulesGrid.Columns[6].ItemStyle.Width = 40;
            RulesGrid.Columns[7].ItemStyle.Width = 40;
            //alex 6/23
            RulesGrid.DataBind();
            if (RulesGrid.Items.Count > 0)
                RulesGrid.Items[idx].Cells[idx1].Focus();
            //alex 6/22
            HdrLbl.Visible = true;
            Rules.Visible = true;
            //alex 6/22
            //RuleCountLabel.Text = editRulesNow.Count + " rules";
        }

        private const string RuleCheckBoxIsDefault = "CheckBoxIsDefault";
        private const string RuleCheckBoxNotVisible = "CheckBoxNotVisibleInCcm";
        private const string RuleNumericFldMin = "RuleMinNumericFld";
        private const string RuleNumericFldMax = "RuleMaxNumericFld";
        private const string RuleEnumFldMin = "RuleMinEnumFld";
        private const string RuleEnumFldMax = "RuleMaxEnumFld";
        private const string RuleCheckBoxInfoOnly = "CheckBoxInfoOnly";
        protected void OnGridItemChanged(object sender, EventArgs e)
        {
            string abcd = "aaa";
            string ddd = "ddd";
        }
        protected void OnRuleItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rule = e.Item.DataItem as CpDocRuleEditModel;
                if (rule == null) return;
                if (rule.IsDefault)
                {
                    var chkBox = e.Item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
                    if (chkBox != null) chkBox.Checked = true;
                }
                if (rule.IsInfoOnly)
                {
                    var chkBox = e.Item.FindControl(RuleCheckBoxInfoOnly) as CheckBox;
                    if (chkBox != null) chkBox.Checked = true;
                }
                if (rule.NotVisibleInCcm)
                {
                    var chkBox = e.Item.FindControl(RuleCheckBoxNotVisible) as CheckBox;
                    if (chkBox != null) chkBox.Checked = true;
                }
                var numFldMin = e.Item.FindControl(RuleNumericFldMin) as TextBox;
                var numFldMax = e.Item.FindControl(RuleNumericFldMax) as TextBox;
                var enumFldMin = e.Item.FindControl(RuleEnumFldMin) as DropDownList;
                var enumFldMax = e.Item.FindControl(RuleEnumFldMax) as DropDownList;
                if (numFldMin == null || numFldMax == null || enumFldMin == null || enumFldMax == null) return;
                var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
                numFldMin.Visible = isNumeric;
                numFldMax.Visible = isNumeric;
                enumFldMin.Visible = !isNumeric;
                enumFldMax.Visible = !isNumeric;
                /*IvanB 15/03 start*/
                if (rule.MeasureName.Equals("Shape (cut)") || rule.MeasureName.Equals("Variety") || rule.MeasureName.ToLower().Equals("color diamond color"))
                {
                    enumFldMin.Attributes.Add("class", "filtered-select");
                    enumFldMax.Attributes.Add("class", "filtered-select");
                }
                /*IvanB 15/03 end*/
                //var ruleMax = Convert.ToDouble(rule.MaxValue.ToString());
                //var ruleMin = Convert.ToDouble(rule.MinValue.ToString());
                if (!isNumeric)
                {
                    enumFldMax.DataSource = rule.EnumSource;
                    enumFldMax.DataBind();
                    enumFldMin.DataSource = rule.EnumSource;
                    enumFldMin.DataBind();
                    enumFldMax.SelectedValue = rule.MaxValue;
                    enumFldMin.SelectedValue = rule.MinValue;
                    //if (ruleMax < ruleMin)
                    //    enumFldMax.SelectedValue = rule.MaxValue;
                    //else
                    //    enumFldMax.SelectedValue = rule.MaxValue;
                    //enumFldMin.SelectedValue = rule.MinValue;

                }
                else
                {
                    //if (ruleMax < ruleMin)
                    //    numFldMax.Text = rule.MinValue;
                    //else
                    //    numFldMax.Text = rule.MaxValue;
                    numFldMax.Text = rule.MaxValue;
                    numFldMin.Text = rule.MinValue;
                }
            }
        }
        private void ApplyRulesAll()
        {
            foreach (DataGridItem item in RulesGrid.Items)
            {
                ApplyRule(item);
            }
        }
        private void ApplyRule(DataGridItem item)
        {
            if (item == null) return;
            var uniqueKey = RulesGrid.DataKeys[item.ItemIndex].ToString();
            var rule = GetRuleByKey(uniqueKey);
            if (rule == null) return;

            //-- Is Default Measure
            var chkIsDefault = item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
            var chkIsInfoOnly = item.FindControl(RuleCheckBoxInfoOnly) as CheckBox;
            if (chkIsDefault != null)
                rule.IsDefault = chkIsDefault.Checked;
            if (chkIsInfoOnly != null)
                rule.IsInfoOnly = chkIsInfoOnly.Checked;
            //-- Is Not Visible in CCM
            var chkNotVisible = item.FindControl(RuleCheckBoxNotVisible) as CheckBox;
            if (chkNotVisible != null) rule.NotVisibleInCcm = chkNotVisible.Checked;

            //-- Max Value, Min Value
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                var minFld = item.FindControl(RuleNumericFldMin) as TextBox;
                var maxFld = item.FindControl(RuleNumericFldMax) as TextBox;
                if (minFld != null) rule.MinValue = minFld.Text;

                if (maxFld != null && maxFld.Text != "")
                {
                    var min = decimal.Parse(minFld.Text);
                    var max = decimal.Parse(maxFld.Text);
                    if (min > max)
                    {
                        if (rule.MeasureName.ToUpper().Contains("WEIGHT"))
                        {
                            rule.MaxValue = (min + 1).ToString();
                        }
                        else
                            rule.MaxValue = minFld.Text;
                    }
                    //alex 6/30
                    else if (maxFld != null) rule.MaxValue = maxFld.Text;
                    //alex 6/30
                }
                else if (maxFld != null) rule.MaxValue = maxFld.Text;
            }
            else
            {
                var minFld = item.FindControl(RuleEnumFldMin) as DropDownList;
                var maxFld = item.FindControl(RuleEnumFldMax) as DropDownList;
                if (minFld != null) rule.MinValue = minFld.SelectedValue;
                if (maxFld != null) rule.MaxValue = maxFld.SelectedValue;
            }
        }

        private CpDocRuleEditModel GetRuleByKey(string key)
        {
            var keys = key.Split(';');
            if (keys.Length != 3) return null;
            var docId = keys[0];
            var partId = keys[1];
            var measureId = keys[2];
            var cpEditModel = GetCpEditFromView();
            var rules = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId).EditDocRules;
            return
                rules.Find(m => "" + m.PartId == partId && "" + m.MeasureId == measureId);
        }
        protected void OnRuleResetBtnClick(object sender, EventArgs e)
        {
            var item = ((LinkButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            //-- Reset Min Value, Max Value, Is Default

            var key = RulesGrid.DataKeys[item.ItemIndex];

            //-- Load Data on EditRulePanel
            var rule = GetRuleByKey(key.ToString());
            if (rule == null) return;
            var numFldMin = item.FindControl(RuleNumericFldMin) as TextBox;
            var numFldMax = item.FindControl(RuleNumericFldMax) as TextBox;
            var enumFldMin = item.FindControl(RuleEnumFldMin) as DropDownList;
            var enumFldMax = item.FindControl(RuleEnumFldMax) as DropDownList;
            var chbIsDefault = item.FindControl(RuleCheckBoxIsDefault) as CheckBox;
            var chbIsInfoOnly = item.FindControl(RuleCheckBoxInfoOnly) as CheckBox;
            if (numFldMin == null || numFldMax == null || enumFldMin == null || enumFldMax == null || chbIsDefault == null && chbIsInfoOnly == null) return;
            var isNumeric = rule.MeasureClass == MeasureModel.MeasureClassNumeric;
            if (isNumeric)
            {
                numFldMin.Text = "";
                numFldMax.Text = "";
            }
            else
            {
                enumFldMax.SelectedValue = "0";
                enumFldMin.SelectedValue = "0";
            }
            numFldMin.Focus();
            chbIsDefault.Checked = false;
            chbIsInfoOnly.Checked = false;
        }

        #endregion

        #region Doc Measure groups
        private const string RechecksField = "RechecksField";

        private void LoadDocMeasureGroups(string docId)
        {
            if (string.IsNullOrEmpty(docId))
            {
                MeasureGroupGrid.DataSource = null;
                MeasureGroupGrid.DataBind();
                return;
            }
            var cpEditModel = GetCpEditFromView();
            var docModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
            MeasureGroupGrid.DataSource = docModel.MeasureGroups;
            MeasureGroupGrid.DataBind();
        }
        protected void OnMeasureGroupDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var group = e.Item.DataItem as MeasureGroupModel;
                var textBox = e.Item.FindControl(RechecksField) as TextBox;
                if (group != null && textBox != null)
                {
                    textBox.Text = "" + group.NoRecheck;
                }
            }

        }
        protected void OnDocRechecksEnter(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            if (item.ItemIndex < (MeasureGroupGrid.Items.Count - 1))
            {
                var itemNext = MeasureGroupGrid.Items[item.ItemIndex + 1];
                var nextTextBox = (TextBox)itemNext.FindControl(RechecksField);
                if (nextTextBox == null) return;
                nextTextBox.Focus();
            }
        }
        //-- On Changing CpDoc, Before Save
        private void ApplyRechecksAll()
        {
            foreach (DataGridItem item in MeasureGroupGrid.Items)
            {
                ApplyRecheck(item);
            }
        }
        private void ApplyRecheck(DataGridItem item)
        {
            if (item == null) return;
            var uniqueKey = MeasureGroupGrid.DataKeys[item.ItemIndex].ToString();
            var keys = uniqueKey.Split(';'); //-- CpDocId + ";" + MeasureGroupId
            var cpDocId = keys[0];
            var measureGroupId = keys[1];
            var textBox = item.FindControl(RechecksField) as TextBox;
            if (textBox == null) return;
            var newVal = textBox.Text;

            var cpEditModel = GetCpEditFromView();
            var docModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == cpDocId);
            if (docModel == null) return;
            var group = docModel.MeasureGroups.Find(m => "" + m.MeasureGroupId == "" + measureGroupId);
            if (group != null)
            {
                group.NoRecheck = string.IsNullOrEmpty(newVal) ? 0 : Convert.ToInt32(newVal);
            }
        }

        #endregion

        #region New Sku parameters dialog
        protected void OnNewSkuDialogOkClick(object sender, EventArgs e)
        {
            ErrNewCpNameLabel.Text = "";
            if (string.IsNullOrEmpty(NewProgramNameFld.Text))
            {
                NewProgramNameFld.Focus();
                OnNewSkuChangeParameters();
                return;
            }
            //-- Check unique new program name
            var exists = QueryCpUtilsNew.ExistsCp(NewProgramNameFld.Text,
                                    GetCustomerModelFromView(CustomerList.SelectedValue),
                                    GetCustomerModelFromView(VendorList.SelectedValue), "", this);
            if (exists)
            {
                ErrNewCpNameLabel.Text = "Customer program with this name already exists";
                OnNewSkuChangeParameters();
                return;
            }

            //-- Create CpModel
            var itemType = GetItemType(ItemGroupsList.SelectedValue, ItemTypesList.SelectedValue);
            var cpModel = new CustomerProgramModel
            {
                CustomerId = CustomerList.SelectedValue,
                VendorId = VendorList.SelectedValue,
                CustomerProgramName = NewProgramNameFld.Text,
                ItemTypeGroupId = itemType.ItemTypeGroupId,
                ItemTypeId = itemType.ItemTypeId,
                CpId = "0"
            };
            var editCp = QueryCpUtilsNew.InitCustomerProgram(cpModel, GetEnumMeasure(), this);
            editCp.Customer = GetCustomerModelFromView(CustomerList.SelectedValue);
            editCp.Vendor = GetCustomerModelFromView(VendorList.SelectedValue);
            SetViewState(editCp, SessionConstants.CpEditModel);
            CpPanel.Visible = true;
            TabContainer.Visible = true;
            SetCpDetails(cpModel);
            SetEnabledSaveButtons();
            //IvanB 15.02.2024
            //GetSkuList(true);
            //var selectedNew = SKUTypesList.Items.FindByValue(itemType.ItemTypeId.ToString());
            //if (selectedNew != null)
            //{
            //    SKUTypesList.SelectedValue = selectedNew.Value;
            //    OnSKUTypesListSelectedChanged(null, null);
            //}

            GetSkuListForNewSku(itemType.ItemTypeId.ToString());
            OnSKUTypesListSelectedChanged(null, null);
            //IvanB 15.02.2024
        }
        private void OnNewSkuChangeParameters()
        {
            var h = Convert.ToInt32(ForHeightFld.Value);
            ItemTypeGrpDiv.Attributes.CssStyle.Value = string.Format(Dlg1DivFormat, h);
            ItemTypeDiv.Attributes.CssStyle.Value = string.Format(Dlg2DivFormat, h);
            //IvanB 20.02.2024
            if (ItemGroupsList.SelectedItem.Value == "-1")
            {
                var selectedItemType = ItemTypesList.SelectedItem.Value;
                var measureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(selectedItemType), this);
                LoadItemTypesPartsTree(measureParts, true);
            }
            else 
            {
                NewSKUTypesTreeView.Nodes.Clear(); 
            }
            //IvanB 20.02.2024

            NewSkuPopupExtender.Show();
        }

        protected void OnItemGroupsListChanged(object sender, EventArgs e)
        {
            var itemTypes = GetItemTypes(ItemGroupsList.SelectedValue);
            itemTypes.Sort((m1, m2) => string.CompareOrdinal(m1.ItemTypeName.ToUpper(), m2.ItemTypeName.ToUpper()));

            ItemTypesList.Items.Clear();
            ItemTypesList.DataSource = itemTypes;
            ItemTypesList.DataBind();
            if (itemTypes.Count > 0)
            {
                ItemTypesList.SelectedValue = "" + itemTypes[0].ItemTypeId;
            }
            //IvanB 20.02.2024
            if (ItemGroupsList.SelectedItem.Value != "-1")
            {
               NewSKUTypesTreeView.Nodes.Clear();
            }
            //IvanB 20.02.2024 

            OnNewSkuChangeParameters();
        }
        protected void OnItemTypesListChanged(object sender, EventArgs e)
        {
            OnNewSkuChangeParameters();
        }

        protected void OnNewProgramNameClick(object sender, EventArgs e)
        {
            OnNewSkuChangeParameters();
        }

        #endregion

        #region Save and SaveAs buttons
        protected void OnSaveClick(object sender, EventArgs e)
        {
            string name = sender.ToString();
            Button btn = (Button)sender;
            string abc = btn.Text;
            var saveData = GetCpDetails();
            var mode = ModeSaveExists;
            if (saveData.Cp.IsCopy)
            {
                mode = ModeSaveBranch;
            }
            else if (saveData.Cp.CpId == "0")
            {
                mode = ModeSaveNew;
            }
            Session["CustomerCode"] = null;
            Session["ProgramName"] = null;
            Session["DocId"] = null;
            Session["CpReturnData"] = null;
            Session["AttachedReport"] = null;
            Session["EditData"] = null;
            PopupSaveQDialog(mode);
        }
        private void SaveRun(bool copyAll)
        {
            if (!KeepRangeBox.Checked)
            {
                Session["RangeList"] = null;
                Session["RangeGroups"] = null;
            }
            var saveData = GetCpDetails();
            // TODO - verify data
            if (string.IsNullOrEmpty(BatchNumberFld.Text))
            {
                var exists = QueryCpUtilsNew.ExistsCp(saveData.Cp.CustomerProgramName, saveData.Customer, saveData.Vendor, saveData.Cp.CpId, this);
                if (exists)
                {
                    var err = string.Format("Customer program with '{0}' name already exists for this customer and vendor.", saveData.Cp.CustomerProgramName);
                    PopupInfoDialog(err, true);
                    return;
                }
            }
            if (CommentsList.Text != "") //save cp comments 
            {
                foreach (var cpdoc in saveData.CpDocs)
                {
                    if (cpdoc.IsDefault == true)
                    {
                        cpdoc.Description = CommentsList.Text;
                        break;
                    }
                }
            }
            var msg = QueryCpUtilsNew.SaveCustomerProgram(saveData, copyAll, null, this);
            if (msg.Contains("Error"))
            {
                PopupInfoDialog(msg, true);
                return;
            }
            SaveCheckedParts(saveData);
            RefreshCpAfterSave(msg, saveData.Customer, saveData.Vendor);
            LoadSamplePriceList();
            PopupInfoDialog("Customer program was saved successfully.", false);
        }
        protected void OnSaveAsClick(object sender, EventArgs e)
        {
            Session["CustomerCode"] = null;
            Session["ProgramName"] = null;
            Session["DocId"] = null;
            Session["CpReturnData"] = null;
            Session["AttachedReport"] = null;
            Session["EditData"] = null;
            SaveAsNameFld.Text = ProgramNameFld.Text;
            SaCustomerLike.Text = "";
            SaCustomerList.DataSource = GetCustomersFromView();
            SaCustomerList.DataBind();

            SaVendorLike.Text = "";
            SaVendorList.DataSource = GetCustomersFromView();
            SaVendorList.DataBind();

            SaveAsOkBtn.Enabled = false;
            SaveAsPopupExtender.Show();
            SaveAsNameFld.Focus();
        }
        private void SaveAsRun()
        {
            string origCustID = hdnCustId.Value.Trim();
            var saveData = GetCpDetails();
            var cpFrom = new CustomerProgramModel { CpId = saveData.Cp.CpId, CpOfficeId = saveData.Cp.CpOfficeId };
            saveData.Customer = GetCustomerModelFromView(SaCustomerList.SelectedValue);
            saveData.Vendor = GetCustomerModelFromView(SaVendorList.SelectedValue);
            saveData.Cp.CpId = "";
            saveData.Cp.CpOfficeId = "";
            saveData.Cp.CustomerProgramName = SaveAsNameFld.Text.Trim();
            // TODO - verify data
            var msg = QueryCpUtilsNew.SaveCustomerProgram(saveData, false, cpFrom, this);
            if (msg.Contains("Error"))
            {
                PopupInfoDialog(msg, true);
                return;
            }
            //-- Reload Saved Cp
            CustomerLike.Text = saveData.Customer.CustomerCode;
            CustomerList.DataSource = new List<CustomerModel> { saveData.Customer };
            CustomerList.DataBind();

            VendorLike.Text = saveData.Vendor.CustomerCode;
            VendorList.DataSource = new List<CustomerModel> { saveData.Vendor };
            VendorList.DataBind();

            RefreshCpAfterSave(msg, saveData.Customer, saveData.Vendor);
            string fileType = QueryCpUtils.FileUploaded(ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(" ", "").Replace(@"/", "") + @"_" + origCustID.Trim().Replace("Customer ID: ", ""), this);
            if (fileType != null)
            {
                bool uploaded = QueryCpUtils.UploadSkuFileOnSaveAs(ProgramNameFld.Text, saveData.Customer.CustomerId, origCustID, fileType, this);
            }
            LoadSamplePriceList();
            PopupInfoDialog("Customer program was saved successfully.", false);
        }

        private void RefreshCpAfterSave(string cpKey, CustomerModel customer, CustomerModel vendor)
        {
            var saveCpId = cpKey.Split('_').Length > 1 ? cpKey.Split('_')[1] : "";
            if (string.IsNullOrEmpty(saveCpId)) return;

            //-- Refresh CpList
            var cpList = QueryUtils.GetCustomerPrograms(customer, vendor, this);

            /*IvanB start*/
            //Create and fill block list
            var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CustomerPrograms, Page);
            //remove items with empty CustomerProgramName
            cpList = cpList.Where(x => !string.IsNullOrEmpty(x.CustomerProgramName.Trim())).Distinct().ToList();
            //Remove items from a list of cp with a CustomerProgramName equals value from block list
            cpList = cpList.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName))).ToList();
            /*IvanB end*/

            SetViewState(cpList, SessionConstants.CustomerProgramList);

            CpList.DataSource = cpList;
            CpList.DataBind();
            if (cpList.Count == 0)
            {
                HideCpDetailsSections();
            }
            else
            {
                ShowCpDetailsSections();
            }
            if (cpList.Find(m => m.CpId == saveCpId) == null) return;
            CpList.SelectedValue = saveCpId;
            OnCpSelectedChanged(null, null);
        }
        protected void SaveCheckedParts(CpEditModel cpEdit)
        {
            List<int> partIdList = new List<int>();
            int[] partIds = new int[20];
            if (DocPartTree.CheckedNodes.Count == 0)
                partIds = null;
            else
            {
                foreach (TreeNode node in DocPartTree.CheckedNodes)
                {
                    string partId = node.Value;
                    partIdList.Add(Convert.ToInt32(partId));
                    
                }
                partIds = partIdList.ToArray();

            }
            DataTable dt = QueryCpUtilsNew.SetSaveBlockedParts(cpEdit, partIds, this);
                if (dt == null || dt.Rows.Count == 0)
                {
                    ErrCP.Visible = false;
                    ErrCP.Text = "error saving checked parts.";
                }
                else
                {
                    ErrCP.Visible = false;
                    ErrCP.Text = "";
                }
        }
        
        protected void GetCheckedParts(CpEditModel cpEdit)
        {
            DataTable dt = QueryCpUtilsNew.GetSavedBlockedParts(cpEdit, this);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string partId = dr["PartID"].ToString();
                    foreach (TreeNode node in DocPartTree.Nodes)
                    {
                        checkNodes(partId, node);
                        
                    }
                }
            }
        }
        protected bool checkNodes (string partId, TreeNode node)
        {
            if (node.Value == partId)
                node.Checked = true;
            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode childNode in node.ChildNodes)
                    checkNodes(partId, childNode);
            }
            return true;
        }
        #endregion

        #region Pricing Panel
        private void LoadPricingPanel(CpEditModel cpEdit)
        {
            FailFixedFld.Text = (cpEdit.Cp.FailFixed > 0 ? "" + cpEdit.Cp.FailFixed : "");
            FailDiscountFld.Text = (cpEdit.Cp.FailDiscount > 0 ? "" + cpEdit.Cp.FailDiscount : "");
            IsFixedFld.SelectedValue = (cpEdit.Cp.IsFixed ? "1" : "0");
            PassFixedPrice.Text = (cpEdit.Cp.FixedPrice > 0 ? "" + cpEdit.Cp.FixedPrice : "");
            PassDiscount.Text = (cpEdit.Cp.Discount > 0 ? "" + cpEdit.Cp.Discount : "");
            DeltaFix.Text = (cpEdit.Cp.DeltaFix > 0 ? "" + cpEdit.Cp.DeltaFix : "");

            LoadCpPartMeasures(cpEdit);
            LoadPriceRangeGrid(cpEdit);
            LoadAddServicePrices(cpEdit);
            OnPricingModeChanges(null, null);
        }
        protected void OnPricingModeChanges(object sender, EventArgs e)
        {
            var isFixed = IsFixedFld.SelectedValue == "1";
            PassFixedPrice.Enabled = isFixed;
            PassDiscount.Enabled = isFixed;
            DeltaFix.Enabled = !isFixed;
            PricePartMeasureList.Enabled = !isFixed;
            PriceRangeGrid.Enabled = !isFixed;
            AddPriceRangeBtn.Enabled = !isFixed;
            PartMeasureList.Enabled = !isFixed;
        }

        private void LoadCpPartMeasures(CpEditModel cpEdit)
        {
            PricePartMeasureList.DataSource = cpEdit.CpPrice.PricePartMeasures;
            PricePartMeasureList.DataBind();
        }
        private void OnAddCpPartMeasure()
        {
            if (PartMeasureList.SelectedItem == null || PricePartTree.SelectedNode == null) return;
            var name = string.Format("[{0}.{1}]", PricePartTree.SelectedNode.Text.Trim(), PartMeasureList.SelectedItem.Text.Trim());
            var cpEdit = GetCpEditFromView();
            var measures = cpEdit.CpPrice.PricePartMeasures;
            if (measures.Find(m => m.PartNameMeasureName == name) != null) return;
            //-- TODO Check HomogeneousClassId
            measures.Add(new PricePartMeasureModel { PartNameMeasureName = name, PartId = PricePartTree.SelectedValue, MeasureCode = PartMeasureList.SelectedValue });
            LoadCpPartMeasures(cpEdit);
        }
        private void OnRemoveCpPartMeasure()
        {
            if (PricePartMeasureList.SelectedItem == null) return;
            var forDel = PricePartMeasureList.SelectedItem.Text;
            var cpEdit = GetCpEditFromView();
            var measures = cpEdit.CpPrice.PricePartMeasures;
            var measure = measures.Find(m => m.PartNameMeasureName == forDel);
            if (measure == null) return;
            measures.Remove(measure);
            LoadCpPartMeasures(cpEdit);
        }
        private void OnRemoveComments()
        {
            if (CpComments.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
                CpComments.Text = CpComments.Text.Substring(0, CpComments.Text.ToUpper().LastIndexOf("WHEN WEIGHT IS EXPRESSED AS FRACTION"));
            else if (CpComments.Text[CpComments.Text.Length - 1] == ']')
                CpComments.Text = CpComments.Text.Substring(0, CpComments.Text.LastIndexOf(@"[") - 1);
        }
        private void OnRemoveDescrip()
        {
            if (CpDescrip.Text.ToUpper().Contains("WHEN WEIGHT IS EXPRESSED AS FRACTION"))
                CpDescrip.Text = CpDescrip.Text.ToUpper().Substring(0, CpDescrip.Text.ToUpper().LastIndexOf("WHEN WEIGHT IS EXPRESSED AS FRACTION"));
            else if (CpDescrip.Text[CpDescrip.Text.Length - 1] == ']')
                CpDescrip.Text = CpDescrip.Text.Substring(0, CpDescrip.Text.LastIndexOf(@"[") - 1);
        }
        private void LoadPricePartsTree(CpEditModel cpModel)
        {
            var parts = cpModel.MeasureParts;
            var data = parts.Select(part => new TreeViewModel { Id = "" + part.PartId, ParentId = part.ParentPartId == 0 ? "" : "" + part.ParentPartId, DisplayName = part.PartName }).ToList();
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root);
            rootNode.Expand();

            PricePartTree.Nodes.Clear();
            PricePartTree.Nodes.Add(rootNode);
        }
        protected void OnPricePartsTreeChanged(object sender, EventArgs e)
        {
            var cpEditModel = GetCpEditFromView();

            var selNode = PricePartTree.SelectedNode;
            if (selNode == null) return;
            var partId = selNode.Value;
            var parts = cpEditModel.MeasureParts;
            var part = parts.Find(m => m.PartId == Convert.ToInt32(partId));
            if (part == null) return;
            var measures = cpEditModel.MeasuresByItemType.FindAll(m => m.Billable && m.PartTypeId == "" + part.PartTypeId);
            PartMeasureList.DataSource = measures;
            PartMeasureList.DataBind();
        }
        private static double ConvertToDouble(string src)
        {
            if (string.IsNullOrEmpty(src)) return 0;
            try
            {
                return Convert.ToDouble(src);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        #endregion

        #region Price Range Grid

        private const string FromField = "FromField";
        private const string ToField = "ToField";
        private const string PriceField = "PriceField";
        private void LoadPriceRangeGrid(CpEditModel cpEdit)
        {
            PriceRangeGrid.DataSource = cpEdit.CpPrice.PriceRanges.Count == 0 ? null : cpEdit.CpPrice.PriceRanges;
            PriceRangeGrid.DataBind();
        }
        protected void OnSamplePriceSelectedChanged(object sender, EventArgs e)
        {
            LoadSampleError.Visible = false;
            //List<SamplePriceListRangesModel> priceRanges = QueryCpUtilsNew.GetSamplePriceRanges(SamplePriceList.Text, this);
            PriceSamplePartMeasureList.Visible = true;
            //var priceRanges = GetViewState(SessionConstants.SamplePriceListranges) as List<SamplePriceListRangesModel>;
            var samplePriceList = GetViewState(SessionConstants.SamplePriceList) as List<SamplePriceListModel>;
            var samplePrice = samplePriceList.Find(m => m.PriceRangeID == SamplePriceList.Text);
            string cpId = samplePrice.CPID;
            string samplePriceName = samplePrice.SamplePriceName;
            DataSet ds = QueryCpUtilsNew.GetSamplePriceRanges(samplePriceName, this);
            List<SamplePriceListRangesModel> priceRanges = (from DataRow row in ds.Tables[0].Rows select new SamplePriceListRangesModel(row)).ToList();
            //SetViewState(priceRanges, SessionConstants.SamplePriceListRanges);
            List<SampleServicePriceModel> sampleServicePrice = (from DataRow row in ds.Tables[1].Rows select new SampleServicePriceModel(row)).ToList();
            var samplePricePartMeasures = QueryCpUtilsNew.GetSamplePricePartMeasures(cpId, this);
            SamplePriceGrid.DataSource = priceRanges.Count == 0 ? null : priceRanges;
            SamplePriceGrid.DataBind();
            SampleServicePriceGrid.DataSource = sampleServicePrice.Count == 0 ? null : sampleServicePrice;
            SampleServicePriceGrid.DataBind();
            PriceSamplePartMeasureList.DataSource = samplePricePartMeasures;
            PriceSamplePartMeasureList.DataBind();
            SetViewState(samplePricePartMeasures, SessionConstants.SamplePricePartMeasures);
        }
        
        protected void OnPriceRangeDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as PriceRangeModel;
            if (priceModel == null) return;

            var fromFld = e.Item.FindControl(FromField) as TextBox;
            if (fromFld != null) fromFld.Text = "" + priceModel.ValueFrom;

            var toFld = e.Item.FindControl(ToField) as TextBox;
            if (toFld != null) toFld.Text = "" + priceModel.ValueTo;
            var priceFld = e.Item.FindControl(PriceField) as TextBox;
            if (priceFld != null) priceFld.Text = "" + priceModel.Price;
        }
        protected void OnRemovePriceRangeCommand(object source, DataGridCommandEventArgs e)
        {
            var rownum = "" + PriceRangeGrid.DataKeys[e.Item.ItemIndex];
            var result = GetPriceRangeValues();
            var forDel = result.Find(m => "" + m.Rownum == rownum);
            if (forDel != null)
            {
                result.Remove(forDel);
                GetCpEditFromView().CpPrice.PriceRanges = result;
                LoadPriceRangeGrid(GetCpEditFromView());
            }
        }
        private List<PriceRangeModel> GetPriceRangeValues()
        {
            var result = new List<PriceRangeModel>();
            foreach (DataGridItem item in PriceRangeGrid.Items)
            {
                var rownum = "" + PriceRangeGrid.DataKeys[item.ItemIndex];

                var fromFld = item.FindControl(FromField) as TextBox;
                var toFld = item.FindControl(ToField) as TextBox;
                var priceFld = item.FindControl(PriceField) as TextBox;
                if (fromFld == null || toFld == null || priceFld == null) continue;
                var priceRangeModel = new PriceRangeModel
                {
                    Rownum = Convert.ToInt32(rownum),
                    ValueFrom = ConvertToDouble(fromFld.Text),
                    ValueTo = ConvertToDouble(toFld.Text),
                    Price = ConvertToDouble(priceFld.Text),
                };
                result.Add(priceRangeModel);
            }
            return result;
        }
        protected void OnAddPriceRangeBtnClick(object sender, EventArgs e)
        {
            var result = GetPriceRangeValues();
            var rownum = (result.Count == 0 ? 0 : result.Max(m => m.Rownum));
            var lastRange = result.Find(m => m.Rownum == rownum);

            var newRow = new PriceRangeModel { Rownum = rownum + 1, Price = 0, ValueFrom = lastRange == null ? 0 : lastRange.ValueTo + 0.01, ValueTo = 0 };
            result.Insert(result.Count, newRow);
            GetCpEditFromView().CpPrice.PriceRanges = result;
            LoadPriceRangeGrid(GetCpEditFromView());
        }
        #endregion

        #region Additional Services

        private const string ServicePriceField = "ServicePriceField";
        private const string AddServiceField = "AddServiceField";
        private void LoadAddServicePrices(CpEditModel cpEditModel)
        {
            AddServicePriceGrid.DataSource = cpEditModel.CpPrice.AddServicePrices.Count == 0 ? null : cpEditModel.CpPrice.AddServicePrices;
            AddServicePriceGrid.DataBind();
        }
        private List<AddServicePriceModel> GetAddServicePriceValues()
        {
            var result = new List<AddServicePriceModel>();
            foreach (DataGridItem item in AddServicePriceGrid.Items)
            {
                var rownum = "" + AddServicePriceGrid.DataKeys[item.ItemIndex];
                var addPriceModel = new AddServicePriceModel { Rownum = Convert.ToInt32(rownum) };

                var priceFld = item.FindControl(ServicePriceField) as TextBox;
                if (priceFld != null && !string.IsNullOrEmpty(priceFld.Text))
                {
                    try
                    {
                        addPriceModel.Price = Convert.ToDouble(priceFld.Text);
                    }
                    catch (Exception x)
                    {
                        var msg = x.Message;
                        addPriceModel.Price = 0;
                    }
                }
                var serviceFld = item.FindControl(AddServiceField) as DropDownList;
                if (serviceFld != null)
                {
                    if (serviceFld.SelectedItem != null)
                    {
                        addPriceModel.ServiceId = serviceFld.SelectedItem.Value;
                        addPriceModel.ServiceName = serviceFld.SelectedItem.Text;
                    }
                }
                result.Add(addPriceModel);
            }
            return result;
        }
        protected void OnAddServiceChanged(object sender, EventArgs e)
        {
            var serviceFld = (DropDownList)sender;

            var item = serviceFld.NamingContainer as DataGridItem;

            if (item == null) return;
            var rownum = "" + AddServicePriceGrid.DataKeys[item.ItemIndex];

            var cpEdit = GetCpEditFromView();
            var serviceModel = cpEdit.CpPrice.AddServicePrices.Find(m => "" + m.Rownum == rownum);
            serviceModel.ServiceId = serviceFld.SelectedItem.Value;
            serviceModel.ServiceName = serviceFld.SelectedItem.Text;
        }
        protected void OnAddServicePriceDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var priceModel = e.Item.DataItem as AddServicePriceModel;
            if (priceModel == null) return;

            var addServiceFld = e.Item.FindControl(AddServiceField) as DropDownList;
            if (addServiceFld != null)
            {
                addServiceFld.DataSource =
                    GetViewState(SessionConstants.CpCommonAdditionalServices) as List<AddServiceModel>;
                addServiceFld.DataBind();
                if (!string.IsNullOrEmpty(priceModel.ServiceId))
                {
                    addServiceFld.SelectedValue = priceModel.ServiceId;
                }
            }
            var priceFld = e.Item.FindControl(ServicePriceField) as TextBox;
            if (priceFld != null) priceFld.Text = "" + priceModel.Price;
        }
        protected void OnDelAddServiceCommand(object source, DataGridCommandEventArgs e)
        {
            //-- Remove line
            var rownum = "" + AddServicePriceGrid.DataKeys[e.Item.ItemIndex];
            var result = GetAddServicePriceValues();
            var forDel = result.Find(m => "" + m.Rownum == rownum);
            if (forDel != null)
            {
                result.Remove(forDel);
                GetCpEditFromView().CpPrice.AddServicePrices = result;
                LoadAddServicePrices(GetCpEditFromView());
            }
        }
        protected void OnAddServiceBtnClick(object sender, EventArgs e)
        {
            //-- Add new line
            var result = GetAddServicePriceValues();
            var rownum = (result.Count == 0 ? 0 : result.Max(m => m.Rownum)) + 1;
            var newRow = new AddServicePriceModel { Rownum = rownum, Price = 0 };
            result.Insert(result.Count, newRow);
            GetCpEditFromView().CpPrice.AddServicePrices = result;
            LoadAddServicePrices(GetCpEditFromView());
        }

        protected void OnSavePriceRangeClick(object sender, EventArgs e)
        {
            SavePriceRangeFld.Visible = true;
            string cpList = CpList.SelectedItem.Text.Replace(" ", "");
            SavePriceRangeFld.Text = CustomerLike.Text + "-" + cpList;
        }

        protected void SavePriceRangeNameChanged(object sender, EventArgs e)
        {
            string priceRangeName = SavePriceRangeFld.Text;
            if (SavePriceRangeFld.Text == "")
                return;
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                new List<CustomerProgramModel>();
            //var cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
            var cp = cpList.Find(m => m.CustomerProgramName == ProgramNameFld.Text);
            int priceid = 0;
            if (cp != null)
                priceid = cp.PriceId;
            else
            {
                var cpEdit = GetCpEditFromView();
                priceid = Convert.ToInt32(cpEdit.Cp.PriceId);
            }
            DataTable dt = QueryCpUtilsNew.SaveSamplePriceRange(priceRangeName, priceid, Convert.ToInt32(cp.CpId), this);
            var result = dt.Rows[0].ItemArray[0].ToString();
            if (result == "not found")
            {
                
                SavePriceRangeError.Visible = true;
                SavePriceRangeError.Enabled = false;
                SavePriceRangeError.Text = "Record not found in Customer Program";
            }
            else
            {
                LoadSamplePriceList();
                SavePriceRangeError.Visible = true;
                SavePriceRangeError.Enabled = false;
                SavePriceRangeError.Text = "Price " + SavePriceRangeFld.Text + " saved";
            }
        }
        #endregion
        protected void OnLoadSamplePriceRangeClick(object sender, EventArgs e)
        {
            if (SamplePriceList.SelectedIndex == 0)
            {
                LoadSampleError.Visible = true;
                LoadSampleError.Text = "Please select the sample";
                return;
            }
            LoadSampleError.Visible = false;
            SamplePriceGrid.Enabled = true;
            PriceSamplePartMeasureList.Enabled = true;
            PricePartMeasureList.Enabled = true;
            var priceListName = SamplePriceList.Items.FindByValue(SamplePriceList.Text);
            string priceName = priceListName.Text;
            DataSet ds = QueryCpUtilsNew.GetSamplePriceRanges(priceName, this);
            var priceRanges = (from DataRow row in ds.Tables[0].Rows select new PriceRangeModel(row)).ToList();
            PriceRangeGrid.DataSource = null;
            PriceRangeGrid.DataBind();
            PriceRangeGrid.DataSource = priceRanges;
            PriceRangeGrid.DataBind();
            var sampleServicePrices = (from DataRow row in ds.Tables[1].Rows select new AddServicePriceModel(row)).ToList();
            AddServicePriceGrid.DataSource = null;
            AddServicePriceGrid.DataBind();
            IsFixedFld.SelectedValue = "0";
            PassFixedPrice.Text = "";
            PassFixedPrice.Enabled = false;
            AddServicePriceGrid.DataSource = sampleServicePrices.Count == 0 ? null : sampleServicePrices;
            AddServicePriceGrid.DataBind();
            //AddServicePriceGrid.SelectedIndex = 0;
            var cpEdit = GetCpEditFromView();
            var addServices = GetAddServicePriceValues();
            cpEdit.CpPrice.AddServicePrices =
                addServices.FindAll(m => !string.IsNullOrEmpty(m.ServiceId) && m.Price > 0);
            cpEdit.CpPrice.PriceRanges = GetPriceRangeValues();
            
        }
        protected void OnDelSamplePriceRangeClick(object sender, EventArgs e)
        {
            if (SamplePriceList.SelectedValue == "")
                return;
            DataTable dt = QueryCpUtilsNew.DeleteSamplePriceRange(SamplePriceList.SelectedItem.Text, this);
            if (dt.Rows.Count > 0)
            {
                string result = dt.Rows[0].ItemArray[0].ToString();
                if (result == "deleted")
                {
                    bool deleted = true;
                    LoadSamplePriceList(deleted);
                    PriceSamplePartMeasureList.DataSource = null;
                    PriceSamplePartMeasureList.DataBind();
                    PriceSamplePartMeasureList.SelectedIndex = -1;
                    if (PriceSamplePartMeasureList.Items.Count > 0)
                        PriceSamplePartMeasureList.Items.Clear();
                    SamplePriceGrid.DataSource = null;
                    SamplePriceGrid.DataBind();
                    SampleServicePriceGrid.DataSource = null;
                    SampleServicePriceGrid.DataBind();
                }
            }

        }
        #region Attached Print Docs on Documents (Tab Requirements)
        private void LoadDocPrintDocs(string docId)
        {
            DocAttachedReportsList.Items.Clear();
            int dId = 0;
            string docName = null;
            string dName = null;
            string typeName = null;
            if (!string.IsNullOrEmpty(docId))
            {
                var cpEditModel = GetCpEditFromView();
                var cpDocModel = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
                var attachedReports = cpDocModel.AttachedReports;
                attachedReports.Sort((m1, m2) => string.CompareOrdinal(m1.OperationTypeName.ToUpper(), m2.OperationTypeName.ToUpper()));
                string v1 = (string)Session["DocId"];
                string v2 = (string)Session["BackToSku"];
                if (Session["DocId"] != null && Session["BackToSku"] == null)
                {
                    var editData = GetCpEditFromView();
                    editData.Reports = QueryCpUtilsNew.GetReportsByCp(editData.Cp, this);
                    dId = Convert.ToInt32((string)Session["DocId"]);
                    dName = (string)Session["AttachedReport"];
                    CpDocPrintModel extraModel = new CpDocPrintModel(); // 
                    extraModel = editData.Reports.Find(m => m.DocumentName == dName && m.OperationTypeOfficeId != null);
                    docName = extraModel.DocumentName;
                    typeName = extraModel.OperationTypeName.Substring(extraModel.OperationTypeName.LastIndexOf(',') + 1);
                    //var returnReports = cpEditModel.Reports.FindAll(m => m.IsAttachedToCp);AddPrintDocToDocClick

                    //attachedReports.Add(extraModel);
                    //foreach (CpDocModel doc in cpEditModel.CpDocs)
                    for (int i = 0; i <= cpEditModel.CpDocs.Count - 1; i++)
                    {
                        if (cpEditModel.CpDocs[i].CpDocId == Convert.ToInt32(docId))
                        {
                            if (cpEditModel.CpDocs[i].AttachedReports.Count > 0)
                            {
                                //alex 6/29
                                bool attached = false;
                                foreach (var attachedreport in cpEditModel.CpDocs[i].AttachedReports)
                                {
                                    if (attachedreport.DocumentName == extraModel.DocumentName)
                                    {
                                        attached = true;
                                        break;
                                    }
                                }
                                if (!attached)
                                    cpEditModel.CpDocs[i].AttachedReports.Add(extraModel);
                                
                            }
                            else
                                cpEditModel.CpDocs[i].AttachedReports.Add(extraModel);
                            //alex 6/29
                            SetViewState(editData, SessionConstants.CpEditModel);
                            Session["EditData"] = editData;
                            break;
                        }
                    }
                    //cpDocModel.AttachedReports.Add(extraModel);

                    for (int i = 0; i <= DocAttachedReportsList.Items.Count - 1; i++)
                    {
                        string reportType = DocAttachedReportsList.Items[i].Text.Substring(DocAttachedReportsList.Items[i].Text.LastIndexOf(','));
                        string reportListName = DocAttachedReportsList.Items[i].Text.Substring(0, DocAttachedReportsList.Items[i].Text.LastIndexOf(','));
                        if (reportType == extraModel.OperationTypeName.Substring(extraModel.OperationTypeName.LastIndexOf(',')) &&
                            reportListName != docName)
                            DocAttachedReportsList.Items.Remove(DocAttachedReportsList.Items[i]);
                    }
                }
                if (ReplaceRptBox.Checked && docName != null && typeName != null)
                {
                    for (int i = 0; i <= attachedReports.Count - 1; i++)
                    {
                        if (attachedReports[i].OperationTypeName.Contains(typeName) && attachedReports[i].DocumentName != docName)
                            attachedReports.Remove(attachedReports[i]);
                    }
                }
                DocAttachedReportsList.DataSource = attachedReports;
                DocAttachedReportsList.DataBind();
                if (docName != null)
                {
                    for (int i = 0; i <= DocAttachedReportsList.Items.Count - 1; i++)
                    {
                        //string reportListName = DocAttachedReportsList.Items[i].Text.Substring(0, DocAttachedReportsList.Items[i].Text.LastIndexOf(','));
                        string reportListName = DocAttachedReportsList.Items[i].Text.Contains(",")? DocAttachedReportsList.Items[i].Text.Substring(0, DocAttachedReportsList.Items[i].Text.LastIndexOf(',')) : DocAttachedReportsList.Items[i].Text;
                        if (reportListName == docName)
                        {
                            DocAttachedReportsList.Items[i].Attributes.Add("style", "background-color: AQUAMARINE");
                        }
                    }
                }
            }
            SetEnabledReportDelBtn();
            SetEnabledReportAddBtn();
        }
        //protected void AddPrintDocToDocClick(object sender, ImageClickEventArgs e)
        //{
        //    //-- Load Reports Attached to Cp
        //    //if (Session["CpReturnData"]!= null)
        //    //    OnRefreshCpDocumentsClick(null, null);
        //    AttachToDocPopupExtender.Show();
        //}

        protected void AddPrintDocToDocClick(object sender, ImageClickEventArgs e)
        {
            var attCpDocs = GetAttachedToCpDocs();
            var canAttach = new List<CpDocPrintModel>();
            var cpdoc = CpDocsList.SelectedItem.Text;
            foreach (var cpDoc in attCpDocs)
            {
                if (cpdoc == "Default")
                {
                    if (cpDoc.DocumentTypeCode == "8" || cpDoc.DocumentTypeCode == "10")
                        canAttach.Add(cpDoc);
                }
                else if (cpdoc != "Default")
                {
                    if (cpDoc.DocumentTypeCode != "8" && cpDoc.DocumentTypeCode != "10")
                        canAttach.Add(cpDoc);
                }
            }
            AvailPrintDocsForDocument.DataSource = null;
            AvailPrintDocsForDocument.DataBind();
            AvailPrintDocsForDocument.DataSource = canAttach;
            AvailPrintDocsForDocument.DataBind();
            AttachToDocPopupExtender.Show();
        }

        protected void LoadReportByTypeClick(object sender, EventArgs e)
        {
            if (DocPartTree.SelectedNode != null)
                Session["SelectedDoc"] = DocPartTree.SelectedNode.Text;
            ApplyRulesAll();
            LoadCpDocRules(CpDocsList.SelectedValue, DocPartTree.SelectedValue);
            List<TreeViewModel> reportsList = (List<TreeViewModel>)Session["ReportsList"];
            string urlName = null;
            Button btn = (Button)sender;
            string btnText = btn.Text;
            foreach (var report in reportsList)
            {
                if (report.DisplayName.Contains(btnText))
                {
                    urlName = report.DisplayName;
                    int first = urlName.IndexOf("'");
                    urlName = urlName.Substring(first + 1);
                    int second = urlName.IndexOf("'");
                    urlName = urlName.Substring(0, second);
                    break;
                }
            }
            string cpReturnData = null, batchReturnData = null;
            if (BatchNumberFld.Text == null || BatchNumberFld.Text == "")
            { 
                cpReturnData += "CustomerCode=" + CustomerLike.Text;
                cpReturnData += ";ProgramName=" + ProgramNameFld.Text;
                cpReturnData += ";DocId=" + CpDocsList.SelectedValue;
                Session["CpReturnData"] = cpReturnData;
            }
            else
            {
                batchReturnData += "CustomerCode=" + CustomerLike.Text;
                batchReturnData += ";ProgramName=" + ProgramNameFld.Text;
                batchReturnData += ";DocId=" + CpDocsList.SelectedValue;
                batchReturnData += ";BatchNumber=" + BatchNumberFld.Text;
                Session["BatchReturnData"] = batchReturnData;
            }
            //alex 6/22
            CpEditModel editData = new CpEditModel();
            editData = (CpEditModel)GetViewState(SessionConstants.CpEditModel);
            //editData.Cp.Comment = CpComments.Text;
            //editData.Cp.Description = CpDescrip.Text;

            Session["EditData"] = editData;
            //alex 6/22
            //alex 6/23
            var editData1 = GetCpDetails();
            Session["EditData"] = editData1;
            //alex 6/23
            //alex 6/26
            urlName = urlName.Replace("DefineDocument", "DefineDocumentNew");
            //alex 6/26
            //alex 6/29
            Session["CP_View"] = null;
            foreach(var cpDocs in editData1.CpDocs)
            {
                if (cpDocs.AttachedReports.Count > 0)
                {
                    string[] parm = (urlName.Substring(urlName.IndexOf("?")+1)).Split('&');
                    string[] cpKeys = parm[0].Split('=');
                    string cpKey = cpKeys[1].Substring(cpKeys[1].IndexOf("_") + 1);
                    if (cpKey != cpDocs.CpId)
                        continue;
                    foreach(var report in cpDocs.AttachedReports)
                    {
                        if (!report.OperationTypeName.Contains(btnText))
                            continue;
                        string key = report.Key;
                        if (DocAttachedReportsList.Items.Count > 0)
                        {
                            for (int i = 0; i<= DocAttachedReportsList.Items.Count - 1; i++)
                            {
                                if (DocAttachedReportsList.Items[i].Text.Contains(btnText)  && DocAttachedReportsList.Items[i].Value == report.Key)
                                {
                                    urlName += "&Report_Key=" + report.Key;
                                }
                            }
                        }
                    }
                }
            }
            //alex 6/29
            if (!ReplaceRptBox.Checked)
                Session["ReplaceRpt"] = "No";
            else
                Session["ReplaceRpt"] = null;
            Response.Redirect(urlName);
        }
        private void SetEnabledReportDelBtn()
        {
            var printDocId = DocAttachedReportsList.SelectedValue;
            var hasValue = !string.IsNullOrEmpty(printDocId);
            DelDocPrintDocBtn.Enabled = hasValue;

            ViewLink.HRef = "";
            if (!hasValue) return;
            var cpKey = GetCpEditFromView().Cp.CpOfficeIdAndCpId;
            var docKey = DocAttachedReportsList.SelectedValue;
            var cpDocId = CpDocsList.SelectedValue;
            var operationTypeId = "";
            if (docKey.Split('_').Length == 2)
            {
                operationTypeId = docKey.Split('_')[1];
            }
            if (string.IsNullOrEmpty(cpKey) || string.IsNullOrEmpty(docKey)) return;
            var docType = GetDocumentTypeCode(docKey);
            var format = "DefineDocumentNew.aspx?" +
                         DefineDocumentNew.ParamCpKey + "={0}&" +
                         DefineDocumentNew.ParamDocTypeCode + "={1}&" +
                         DefineDocumentNew.ParamCpDocId + "={2}&" +
                         DefineDocumentNew.ParamOperationTypeId + "={3}";
            ViewLink.HRef = string.Format(format, cpKey, docType, cpDocId, operationTypeId);
            //alex 6/29
            Session["CP_View"] = true;
            //alex 6/29
            DocAttachedReportsList.Focus();
        }
        /// <summary>
        /// After: Load DocAttachPrintDocs, Add Print Doc, Del Print Doc
        /// </summary>%
        private void SetEnabledReportAddBtn()
        {
            var cpEditModel = GetCpEditFromView();
            var attCpDocs = GetAttachedToCpDocs();
            if (Session["DocId"] != null)
            {
                string dId = (string)Session["DocId"];
                CpDocsList.SelectedValue = dId;
            }
            var cpDocument = cpEditModel.CpDocs.Find(m => "" + m.CpDocId == CpDocsList.SelectedValue);
            var attDocDocs = cpDocument.AttachedReports;
            var canAttach = new List<CpDocPrintModel>();
            string newRep = null;
            if (Session["AttachedReport"] != null)
                newRep = (string)Session["AttachedReport"];

            foreach (var cpDoc in attCpDocs)
            {
                var unAttach = attDocDocs.Find(m => m.Key == cpDoc.Key);
                //if (unAttach == null || (unAttach != null && unAttach.DocumentName == newRep)) 
                if (unAttach == null)
                    canAttach.Add(cpDoc);
            }
            AddPrintDocToDocBtn.Enabled = canAttach.Count > 0;
            AvailPrintDocsForDocument.DataSource = canAttach;
            AvailPrintDocsForDocument.DataBind();
            if (canAttach.Count > 0)
            {
                if (Session["AttachedReport"] == null)
                    AvailPrintDocsForDocument.SelectedIndex = 0;
                else
                {
                    for (int i = 0; i <= AvailPrintDocsForDocument.Items.Count - 1; i++)
                    {
                        string itemName = AvailPrintDocsForDocument.Items[i].Text.Substring(0, AvailPrintDocsForDocument.Items[i].Text.LastIndexOf(','));
                        //if (AvailPrintDocsForDocument.Items[i].Text == newRep)
                        if (itemName == newRep)
                        {
                            AvailPrintDocsForDocument.SelectedIndex = i;
                            break;
                        }
                    }
                }
            }
            string refresh = (string)Session["Refresh"];
            if (refresh == null)
            {
                Session["ProgramName"] = null;
                Session["AttachedReport"] = null;
                Session["CustomerCode"] = null;
                Session["DocId"] = null;
                Session["CpReturnData"] = null;
                Session["BatchReturnData"] = null;
                Session["BatchNumber"] = null;
            }
            Session["Refresh"] = null;

        }
        private List<CpDocPrintModel> GetAttachedToCpDocs()
        {
            var cpEditModel = GetCpEditFromView();
            //if (Session["ProgramName"] != null)
            //{
            //    string pName = (string)Session["ProgramName"];
            //    var extraModel = cpEditModel.Reports.Find(m => m.DocumentName == pName);
            //    var returnReports = cpEditModel.Reports.FindAll(m => m.IsAttachedToCp);
            //    returnReports.Add(extraModel);
            //    return returnReports;
            //}
            //else
            return cpEditModel.Reports.FindAll(m => m.IsAttachedToCp);
        }
        protected void OnAttachToDocOkClick(object sender, EventArgs e)
        {
            //if (Session["CpReturnData"]!= null)
            //OnRefreshCpDocumentsClick(null, null);
            var docId = CpDocsList.SelectedValue;
            var document = GetCpDocSelected();
            if (document == null) return;
            var keyAdded = AvailPrintDocsForDocument.SelectedValue;
            bool addKey = true;
            foreach (var report in document.AttachedReports)
            {
                if (report.Key == keyAdded)
                {
                    addKey = false;
                    break;
                }
            }
            if (addKey)
            {
                var docAdded = GetAttachedToCpDocs().Find(m => m.Key == keyAdded);
                string typeName = docAdded.OperationTypeName.Substring(docAdded.OperationTypeName.LastIndexOf(','));
                List<CpDocPrintModel> attached = document.AttachedReports;
                for (int i = 0; i <= document.AttachedReports.Count - 1; i++)
                {
                    if (document.AttachedReports[i].OperationTypeName.Contains(typeName))
                        document.AttachedReports.Remove(document.AttachedReports[i]);
                }
                document.AttachedReports.Add(new CpDocPrintModel(docAdded));
            }
            LoadDocPrintDocs(docId);
        }

        protected void OnDocAttachPrintDocSelection(object sender, EventArgs e)
        {
            SetEnabledReportDelBtn();
            OnCorelFileChange();
            
        }

        //protected void OnCPTypesSelection(object sender, EventArgs e)
        //{
        //    var itemTypes = GetViewState(SessionConstants.CpCommonItemTypes) as List<ItemTypeModel> ?? new List<ItemTypeModel>();
        //}
        private CpDocModel GetCpDocSelected()
        {
            var cpEditModel = GetCpEditFromView();
            var docId = CpDocsList.SelectedValue;
            return cpEditModel.CpDocs.Find(m => "" + m.CpDocId == docId);
        }
        protected void OnDelDocPrintDocClick(object sender, ImageClickEventArgs e)
        {
            var docId = CpDocsList.SelectedValue;
            var document = GetCpDocSelected();
            if (document == null) return;
            var printDocKey = DocAttachedReportsList.SelectedValue;
            var printDoc = document.AttachedReports.Find(m => m.Key == printDocKey);
            if (printDoc == null) return;
            document.AttachedReports.Remove(printDoc);
            LoadDocPrintDocs(docId);
        }
        private string GetDocumentTypeCode(string id)
        {
            var cpEdit = GetCpEditFromView();
            var doc = cpEdit.Reports.Find(m => m.Key == id);
            return doc == null ? "" : doc.DocumentTypeCode;
        }
        #endregion

        #region Cp Reports Tree
        private void LoadCpReportsTree()
        {
            //CpReportsTree.Nodes.Clear();
            var documents = GetCpEditFromView().Reports;
            var docTypes = documents.FindAll(m => !m.IsAttachedToCp);
            docTypes.Sort((m1, m2) => string.CompareOrdinal(m1.OperationTypeName.ToUpper(), m2.OperationTypeName.ToUpper()));
            List<TreeViewModel> reportsList = new List<TreeViewModel>();
            foreach (var docType in docTypes)
            {
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = docType.DocumentTypeCode, DisplayName = docType.DisplayUrl } };
                var docsByType = documents.FindAll(m => m.IsAttachedToCp && m.DocumentTypeCode == docType.DocumentTypeCode);
                docsByType.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
                foreach (var docByType in docsByType)
                {
                    data.Add(new TreeViewModel { ParentId = docByType.DocumentTypeCode, Id = docByType.Key, DisplayName = docByType.DisplayUrl });
                }
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);
                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                //CpReportsTree.Nodes.Add(rootNode);
                reportsList.AddRange(data);
            }
            Session["ReportsList"] = reportsList;
        }
        protected void OnRefreshCpDocumentsClick(object sender, ImageClickEventArgs e)
        {
            Session["Refresh"] = "true";
            var editData = GetCpEditFromView();
            //alex 6/23
            //if (Session["EditData"] != null)
            //{
            //    editData = (CpEditModel)Session["EditData"];
            //    //Session["EditData"] = null;
            //}
            //alex 6/23
            //Session["Refresh"] = null;
            editData.Reports = QueryCpUtilsNew.GetReportsByCp(editData.Cp, this);
            SetViewState(editData, SessionConstants.CpEditModel);
            LoadCpReportsTree();
            SetEnabledReportAddBtn();
        }
        #endregion

        #region Cp By Batch Number
        protected void OnSearchCpByBatchClick(object sender, EventArgs e)
        {
            ClearCpSessions();
            DescNameBox.Text = "";
            CommentsNameBox.Text = "";
            SavedDescList.Items.Clear();
            SavedCommentsList.Items.Clear();
            if (Session["BatchNumber"] != null)
                BatchNumberFld.Text = (string)Session["BatchNumber"];
            var myResult = Utilities.QueryUtils.GetBatchNumberBy7digit(BatchNumberFld.Text.Trim(), this.Page);
            if (myResult.Trim() != "") BatchNumberFld.Text = myResult;
            var batchNumber = BatchNumberFld.Text.Trim();
            if (string.IsNullOrEmpty(batchNumber))
            {
                CpByBatchModeOff();
                return;
            }
            if (batchNumber.Length < 8)
            {
                PopupInfoDialog(string.Format("The entered batch code {0} does not exist.", batchNumber), true);
                return;
            }
            var cp = QueryCpUtilsNew.GetCustomerProgramByBatchNumber(batchNumber, this);
            if (cp == null)
            {
                PopupInfoDialog(string.Format("The entered batch code {0} does not exist.", batchNumber), true);
                return;
            }

            //-- Customer and Vendor
            var customer = GetCustomerModelFromView(cp.CustomerId);
            if (customer == null) return;
            var vendor = string.IsNullOrEmpty(cp.VendorId) ? customer : GetCustomerModelFromView(cp.VendorId);
            CustomerLike.Text = customer.CustomerCode;
            CustomerList.Items.Clear();
            CustomerList.Items.Add(new ListItem(customer.CustomerName, customer.CustomerId));
            CustomerList.SelectedValue = customer.CustomerId;
            VendorLike.Text = vendor.CustomerCode;
            VendorList.Items.Clear();
            VendorList.Items.Add(new ListItem(vendor.CustomerName, vendor.CustomerId));
            VendorList.SelectedValue = vendor.CustomerId;
            VendorAsCustomer.Checked = customer.CustomerId == vendor.CustomerId;
            OnVendorAsCustomerChanging(null, null);

            var cpList = new List<CustomerProgramModel> { cp };
            SetViewState(cpList, SessionConstants.CustomerProgramList);
            CpList.Items.Clear();
            CpList.Items.Add(new ListItem(cp.CustomerProgramName, cp.CpId));
            CpList.SelectedValue = cp.CpId;
            CpByBatchModeOn();
            OnCpSelectedChanged(null, null);
            CpPanel.Visible = true;
            int itemTypeId = cp.ItemTypeId;
            //IvanB 15.02.2024
            GetSkuList(false);
            //IvanB 15.02.2024
            try
            {
                SKUTypesList.SelectedIndex = -1;
                for (int i = 0; i <= SKUTypesList.Items.Count; i++)
                {
                    if (SKUTypesList.Items[i].Value == cp.ItemTypeId.ToString())
                    {
                        SKUTypesList.SelectedIndex = i;
                        break;
                    }
                    //SKUTypesList.SelectedValue = cp.ItemTypeId.ToString();
                }
            }
            catch
            {
                ;
            }
            OnSKUTypesListSelectedChanged(null, null);
        }
        private void ClearCpSessions()
        {
            if (Session["BatchReturnData"] == null)
            {
                Session["CustomerCode"] = null;
                Session["ProgramName"] = null;
                Session["DocId"] = null;
                Session["AttachedReport"] = null;
                Session["EditData"] = null;
                Session["BatchNumber"] = null;
            }
            Session["CpReturnData"] = null;
            ClearPriceSession();
        }
        private void ClearPriceSession()
        {
            //SavePriceRangeFld.Visible = false;
            SavePriceRangeError.Visible = false;
            PriceSamplePartMeasureList.Visible = false;
            SamplePriceGrid.DataSource = null;
            SamplePriceGrid.DataBind();
            SampleServicePriceGrid.DataSource = null;
            SampleServicePriceGrid.DataBind();
        }
        private void CpByBatchModeOff()
        {
            BatchNumberFld.Enabled = false;
            CustomerList.Enabled = true;
            CustomerLike.Enabled = true;
            VendorAsCustomer.Enabled = true;
            //alex 6/23
            //NewSkuBtn.Enabled = CustomerList.SelectedItem != null;
            NewSkuBtn.Enabled = false;
            //alex 6/23
            ProgramNameFld.Enabled = true;
            if (CustomerList.SelectedItem != null)
            {
                OnCustomerSelectedChanged(null, null);
            }
        }
        private void CpByBatchModeOn()
        {
            //alex 6/24
            CustomerList.Enabled = false;
            CustomerLike.Enabled = false;
            //alex 6/24
            VendorAsCustomer.Enabled = false;
            NewSkuBtn.Enabled = false;
            ProgramNameFld.Enabled = false;
        }
        #endregion

        #region SaveAs dialog
        private bool IsEnabledSaveAsBtn()
        {
            return SaveAsNameFld.Text.Trim().Length > 0 && SaCustomerList.SelectedItem != null;
        }
        protected void OnSaCustomerSelectedChanged(object sender, EventArgs e)
        {
            if (SaVendorAsCustomer.Checked)
            {
                var customer = SaVendorList.Items.FindByValue(SaCustomerList.SelectedValue);
                if (customer != null)
                {
                    SaVendorList.SelectedValue = SaCustomerList.SelectedValue;
                }
            }
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();

        }

        protected void OnSaCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = SaCustomerLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            SaCustomerList.DataSource = filtered;
            SaCustomerList.DataBind();
            if (filtered.Count == 1)
            {
                SaCustomerList.SelectedValue = filtered[0].CustomerId;
            }
            if (SaVendorAsCustomer.Checked)
            {
                SaVendorLike.Text = filterText;
                OnSaVendorSearchClick(null, null);
            }
            if (!string.IsNullOrEmpty(CustomerList.SelectedValue))
            {
                OnSaCustomerSelectedChanged(null, null);
            }
        }

        protected void OnSaVendorSelectedChanged(object sender, EventArgs e)
        {
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }

        protected void OnSaVendorSearchClick(object sender, ImageClickEventArgs e)
        {
            var filterText = SaVendorLike.Text.Trim().ToLower();
            var filtered = GetCustomersFromView(filterText);
            SaVendorList.DataSource = filtered;
            SaVendorList.DataBind();
            if (filtered.Count > 0) SaVendorList.SelectedValue = filtered[0].CustomerId;
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();

        }

        protected void OnSaVendorAsCustomerChanging(object sender, EventArgs e)
        {
            var asCustomer = SaVendorAsCustomer.Checked;
            SaVendorList.Enabled = !asCustomer;
            SaVendorLike.Enabled = !asCustomer;
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }

        protected void OnSaveAsOkClick(object sender, EventArgs e)
        {
            SaveAsErrLabel.Text = "";
            var exists = QueryCpUtilsNew.ExistsCp(SaveAsNameFld.Text,
                                    GetCustomerModelFromView(SaCustomerList.SelectedValue),
                                    GetCustomerModelFromView(SaVendorList.SelectedValue), "", this);
            if (exists)
            {
                SaveAsErrLabel.Text = "Customer program with this name already exists for this customer and vendor.";
                SaveAsPopupExtender.Show();
                return;
            }
            SaveAsRun();
        }

        protected void OnSaveAsNameEnter(object sender, EventArgs e)
        {
            SaveAsOkBtn.Enabled = IsEnabledSaveAsBtn();
            SaveAsPopupExtender.Show();
        }
        #endregion

        #region Question dialog On Save

        private const string ModeSaveExists = "ModeSaveExists";
        private const string ModeSaveNew = "ModeSaveNew";
        private const string ModeSaveBranch = "ModeSaveBranch";
        private void PopupSaveQDialog(string mode)
        {
            SaveMode.Value = mode;
            ApplyAllCp.Visible = mode == ModeSaveBranch || mode == ModeSaveExists;
            ApplyAllCp.Checked = false;
            SaveQPopupExtender.Show();
        }
        protected void OnQuesSaveYesClick(object sender, EventArgs e)
        {
            SaveRun(ApplyAllCp.Checked);
        }
        #endregion

        #region Information Dialog
        private void PopupInfoDialog(string msg, bool isErr)
        {
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

        #region Currency

        private void LoadCurrency()
        {
            List<CurrencyModel> currency = new List<CurrencyModel>();
            currency = GSIAppQueryUtils.GetCurrency(this);
            currency.Add(new CurrencyModel { CurrencyID = -1, Currency = "" });
            currency.Sort((m1, m2) => String.CompareOrdinal(m1.CurrencyID.ToString(), m2.CurrencyID.ToString()));
            currency.Add(new CurrencyModel { CurrencyID = 0, Currency = "---Remove Currency---" });
            SetViewState(currency, SessionConstants.CurrencyList);

            ddlCurrency.DataSource = currency;
            ddlCurrency.DataBind();
            ddlCurrency.SelectedIndex = 0;
        }

        private void GetCurrencyByProgram()
        {
            Int32 customerID = Int32.Parse(CustomerList.SelectedValue);
            string customerProgramName = CpList.SelectedItem.Text;
            var currency = GSIAppQueryUtils.GetCurrency(customerID, customerProgramName, this);
            if (currency != null)
                //ddlCurrency.SelectedItem.Text = currency.Currency;
                ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByText(currency.Currency));
            else
                ddlCurrency.SelectedIndex = -1;

        }

        protected void btnChangeCurrency_Click(object sender, EventArgs e)
        {
            Int32 customerID = Int32.Parse(CustomerList.SelectedValue);
            string customerProgramName = CpList.SelectedItem.Text;
            //Int32 cpid = ddlCpList.SelectedValue == "" ? 0 : Int32.Parse(ddlCpList.SelectedValue);
            Int32 currencyID = ddlCurrency.SelectedValue == "" ? 0 : Int32.Parse(ddlCurrency.SelectedValue);
            string result = GSIAppQueryUtils.SaveCurrency(customerID, customerProgramName, currencyID, this);
            GetCurrencyByProgram();
        }

        #endregion

        private void LoadSamplePriceList(bool deleted = false)
        {
            SavePriceRangeError.Visible = false;
            SavePriceRangeError.Text = "";
            
            DataTable dt = QueryCpUtilsNew.GetSamplePriceList(this);
            if (dt != null)
            {
                var result = (from DataRow row in dt.Rows select new SamplePriceListModel(row)).ToList();
                SetViewState(result, SessionConstants.SamplePriceList);
                List<string> samplePriceList = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    samplePriceList.Add(dr["SamplePriceName"].ToString());
                }
                SamplePriceList.DataSource = result;//samplePriceList;
                SamplePriceList.DataBind();
                SamplePriceList.SelectedIndex = -1;
                SamplePriceList.Items.Insert(0, new ListItem("Please select Sample Price List", ""));
                SamplePriceGrid.DataSource = null;
                SamplePriceGrid.DataBind();
                if (!deleted)
                {
                    var cpId = result[0].CPID;
                    var samplePricePartMeasures = QueryCpUtilsNew.GetSamplePricePartMeasures(cpId, this);
                    PriceSamplePartMeasureList.DataSource = samplePricePartMeasures;
                    PriceSamplePartMeasureList.DataBind();
                    PriceSamplePartMeasureList.SelectedIndex = -1;
                    SetViewState(samplePricePartMeasures, SessionConstants.SamplePricePartMeasures);
                }

            }
        }
        private void LoadSavedDescCommentsList(int itemTypeId)
        {
            SavePriceRangeError.Visible = false;
            SavePriceRangeError.Text = "";

            DataTable dt = QueryCpUtilsNew.GetSavedDescCommentsList(this);
            if (dt != null)
            {
                var result = (from DataRow row in dt.Rows select new SavedDescCommentsListModel(row)).ToList();
                Session["SavedDescComments"] = result;
                //SetViewState(result, SessionConstants.SavedDescComments);
                List<SavedDescCommentsListModel> savedDescList = new List<SavedDescCommentsListModel>();
                foreach (SavedDescCommentsListModel item in result.Where(m => m.Type == "Description" && m.ItemTypeID == itemTypeId))
                {
                    savedDescList.Add(item);
                }

                if (savedDescList.Count > 0)
                {
                    SavedCommentsList.DataSource = savedDescList;//samplePriceList;
                    SavedCommentsList.DataBind();
                    SavedCommentsList.SelectedIndex = -1;
                    SavedCommentsList.Items.Insert(0, new ListItem("Please select Comments Name from List", ""));
                }
                savedDescList = new List<SavedDescCommentsListModel>();
                foreach (SavedDescCommentsListModel item in result.Where(m => m.Type == "Comments" && m.ItemTypeID == itemTypeId))
                {
                    savedDescList.Add(item);
                }
                if (savedDescList.Count > 0)
                {
                    SavedDescList.DataSource = savedDescList;//samplePriceList;
                    SavedDescList.DataBind();
                    SavedDescList.SelectedIndex = -1;
                    SavedDescList.Items.Insert(0, new ListItem("Please select Comments Name from List", ""));
                }
            }
        }
        protected void BatchNumberFld_TextChanged(object sender, EventArgs e)
        {

        }

        protected void RulesGrid_EditCommand(object source, DataGridCommandEventArgs e)
        {
            //var abc = "abc";
        }
        /*IvanB start*/
        protected void ReportTypeAssignClick(object sender, EventArgs e)
        {
            if(int.TryParse(CpList.SelectedValue, out int result))
            {
                var sku = CpList.SelectedItem.Text;
                var repType = Convert.ToInt32(((Button)sender).Text);

                QueryUtils.AssignReportType(repType, sku, Page);
                ApplySkuReportButtonsStyles(sku);
            }
        }

        protected void DeleteReportTypeClick(object sender, EventArgs e)
        {
            if (int.TryParse(CpList.SelectedValue, out int result))
            {
                var sku = CpList.SelectedItem.Text;

                QueryUtils.DeleteReportTypeAssignment(sku, Page);
                ApplySkuReportButtonsStyles(sku);
            }
        }
        protected void ApplySkuReportButtonsStyles(string sku) 
        {
            DeleteReportTypeButton.Enabled = true;
            var reportType = QueryUtils.GetReportAssigned(sku, Page);
            for (var i = 1; i <= 6; i++) 
            { 
                var btnId = "ReportType" + i + "Button";
                var btn = ReportTypeAssignmentPanel.FindControl(btnId) as Button;
                btn.CssClass = "buttonStyleReport" + i + (i == reportType ? " buttonHighlighted" : "");
            }
            if (reportType == 0) 
            {
                DeleteReportTypeButton.Enabled = false;
            }
        }

		/*IvanB end*/

		#region Group Company Save
		private void LoadCustomerCompanyGroup(string CustomerId)
		{
			CustomerModel objCustomerModel = new CustomerModel();
			objCustomerModel.CustomerId = CustomerId;
			var customers = QueryUtils.GetCustomerCompanyGroup(objCustomerModel, this);
			chkGroupCompanyList.DataValueField = "CustomerID";
			chkGroupCompanyList.DataTextField = "CustomerName";
			chkGroupCompanyList.DataSource = customers;
			chkGroupCompanyList.DataBind();
            string companyGroupName = "";
            if (customers.Rows.Count > 0)
                companyGroupName= customers.Rows[0]["CompanyGroupName"].ToString();
			if (customers.Rows.Count >= 1)
			{
				chkGroupCompanyList.Visible = true;
				dvchkSisterCompanyList.Visible = true;
                if (access)
				    btnSaveInCompanyGroup.Enabled = true;
                lblCompanyGroup.InnerText = companyGroupName;
			}
			else
			{
				chkGroupCompanyList.Visible = false;
				dvchkSisterCompanyList.Visible = false;
				btnSaveInCompanyGroup.Enabled = false;
			}
		}
		protected void btnSaveInCompanyGroup_OnClick(object sender, EventArgs e)
		{
			string name = sender.ToString();
			Button btn = (Button)sender;
			string abc = btn.Text;
			var saveData = GetCpDetails();
			var mode = ModeSaveExists;
			if (saveData.Cp.IsCopy)
			{
				mode = ModeSaveBranch;
			}
			else if (saveData.Cp.CpId == "0")
			{
				mode = ModeSaveNew;
			}
			Session["CustomerCode"] = null;
			Session["ProgramName"] = null;
			Session["DocId"] = null;
			Session["CpReturnData"] = null;
			Session["AttachedReport"] = null;
			Session["EditData"] = null;
			PopupSaveCompanyQDialog(mode);
		}
		private void PopupSaveCompanyQDialog(string mode)
		{
			SaveMode.Value = mode;
			chkGroupApplyAllcp.Visible = mode == ModeSaveBranch || mode == ModeSaveExists;
			chkGroupApplyAllcp.Checked = false;
			SaveGroupQPopupExtender.Show();
		}
		protected void BtnQuesSaveYes_OnClick(object sender, EventArgs e)
		{
			SetGroupCompany(chkGroupApplyAllcp.Checked);
		}
		private void SetGroupCompany(bool copyAll)
		{
			if (!KeepRangeBox.Checked)
			{
				Session["RangeList"] = null;
				Session["RangeGroups"] = null;
			}
			var msgsis = string.Empty;
			//Insert or Update CP in selected group compnies
			var cpModel = GetCpDetails();
			var cpFrom = new CustomerProgramModel { CpId = cpModel.Cp.CpId, CpOfficeId = cpModel.Cp.CpOfficeId };
            string origCustID = hdnCustId.Value.Trim();
            foreach (ListItem chkGroupCompany in chkGroupCompanyList.Items)
			{
				if (chkGroupCompany.Selected == true)
				{
					string customerID = chkGroupCompany.Value.ToString();
					msgsis += SetCompanyGroupByCustID(customerID, copyAll, cpFrom);
                    if (iframeMemoSkuPDFViewer.Src != "")//sku document open - upload it for group
                    {
                        string fileType = QueryCpUtils.FileUploaded(ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(" ", "").Replace(@"/", "") + @"_" + origCustID.Trim().Replace("Customer ID: ", ""), this);
                        if (fileType != null)
                        {
                            bool uploaded = QueryCpUtils.UploadSkuFileOnSaveAs(ProgramNameFld.Text, customerID, origCustID, fileType, this);
                        }
                    }
                }
			}
			if (msgsis.Contains("Error"))
			{
				PopupInfoDialog(msgsis, true);
				return;
			}
			//var saveData = GetCpDetails();
			var customer = GetCustomerModelFromView(CustomerList.SelectedValue);

			var cpList = QueryUtils.GetCustomerPrograms(customer, customer, this);
			//Create and fill block list
			var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CustomerPrograms, Page);
			//Remove items from a list of cp with a CustomerProgramName equals value from block list
			cpList = cpList.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName))).ToList();
			List<CustomerProgramModel> objcpModel = cpList.Where(x => x.CustomerProgramName.Equals(ProgramNameFld.Text.Trim())).ToList();
			var CpOfficeIdAndCpId = objcpModel[0].CpOfficeIdAndCpId;
			RefreshCpAfterSave(CpOfficeIdAndCpId, customer, customer);
			//LoadSamplePriceList();
			PopupInfoDialog("Customer program was saved successfully.", false);
		}
		private string SetCompanyGroupByCustID(string customerID, bool copyAll, CustomerProgramModel cpFrom)
		{
			var msg = "";
            try
            {
                var cpModel = GetCpDetails();

                cpModel.Customer = GetCustomerModelFromView(customerID);
                cpModel.Vendor = GetCustomerModelFromView(customerID);

                var customersList = GetViewState(SessionConstants.CustomersList) as List<CustomerModel> ?? new List<CustomerModel>();
                var customerModel = customersList.Find(m => m.CustomerId == customerID);
                var vendorModel = VendorAsCustomer.Checked ? null : customersList.Find(m => m.CustomerId == customerID);

                var cpList = QueryUtils.GetCustomerPrograms(customerModel, vendorModel, this);
                //Create and fill block list
                var blockList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.CustomerPrograms, Page);
                //Remove items from a list of cp with a CustomerProgramName equals value from block list
                cpList = cpList.Where(x => !blockList.Exists(y => x.CustomerProgramName.Equals(y.BlockedDisplayName))).ToList();

                List<CustomerProgramModel> objcpModel = cpList.Where(x => x.CustomerProgramName.Equals(ProgramNameFld.Text.Trim())).ToList();

                if (objcpModel.Count == 0)
                {
                    //SKU insert in Group Company
                    cpModel.Cp.CpId = "";
                    cpModel.Cp.CpOfficeId = "";
                    msg = QueryCpUtilsNew.SaveCustomerProgram(cpModel, copyAll, cpFrom, this);
                }
                else
                {
                    //SKU Update in Group Company
                    cpModel.Cp.CpId = objcpModel[0].CpId;
                    cpModel.Cp.CpOfficeId = objcpModel[0].CpOfficeId;
                    msg = QueryCpUtilsNew.SaveCustomerProgram(cpModel, copyAll, null, this);
                }
            }
            catch 
            {
                ErrCP.Text = "Error processing customer ID " + customerID;
                ErrCP.Visible = true;
            }
			return msg;
		}
        #endregion

        protected void ViewSkuPdf(object sender, EventArgs e)
        {
            string skuPdf = null;
            iframeMemoSkuPDFViewer.Src = null;
            string fileType = QueryCpUtils.FileUploaded(ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(" ", "").Replace(@"/", "") + @"_" + hdnCustId.Value.Trim().Replace("Customer ID: ", ""), this);
            if (fileType != null)
            {
                if (fileType.ToLower() != "jpg" && fileType.ToLower() != "png")
                {
                    SkuImage.Visible = false;
                    iframeMemoSkuPDFViewer.Visible = true;
                    string urlPrefix = "";
                    if (fileType == "xls" || fileType == "xlsx" || fileType == "xlsm")
                        urlPrefix = @"https://view.officeapps.live.com/op/embed.aspx?src=";
                    iframeMemoSkuPDFViewer.Src = urlPrefix + @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + hdnCustId.Value.Trim().Replace("Customer ID: ", "") + "." + fileType;
                }
                else
                {
                    SkuImage.Visible = true;
                    iframeMemoSkuPDFViewer.Visible = false;
                    SkuImage.ImageUrl = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + hdnCustId.Value.Trim().Replace("Customer ID: ", "") + "." + fileType;


                }
            }
            else
            {
                DataTable dt = QueryCpUtils.GetSkuMemoUrl(ProgramNameFld.Text.Trim().Replace("Program Name: ", ""), hdnCustId.Value, this);
                if (dt != null && dt.Rows.Count > 0)
                {
                    skuPdf = dt.Rows[0]["SkuReceiptPDF"].ToString();
                    iframeMemoSkuPDFViewer.Visible = true;
                    if (skuPdf != null && skuPdf != "")
                        iframeMemoSkuPDFViewer.Src = skuPdf;
                    //iframeMemoSkuPDFViewer.Src = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/C242A150SZOVWX.jpg";
                    //iframeMemoSkuPDFViewer.Src = @"https://view.officeapps.live.com/op/embed.aspx?src=https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/testupload.xlsx";
                    //viewSku.Visible = false;
                }
                else
                    ErrPictureField.Text = "No sku file found";
            }
        }

        protected void UploadSkuBtnClick(object sender, EventArgs e)
        {
            string skuFileLink = "";
            if (SkuFileUploader1.HasFile)
            {
                UploadSKUFileToAzure();
                skuFileLink = GetSKUPath();
                ViewSkuPdf(null, null);
            }
        }
        protected void UploadSKUFileToAzure()
        {
            int fileLen = SkuFileUploader1.PostedFile.ContentLength;
            byte[] input = new byte[fileLen - 1];
            input = SkuFileUploader1.FileBytes;
            string fileName = SkuFileUploader1.FileName;
            //fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.:/]+", "", RegexOptions.Compiled);
            using (MemoryStream ms = new MemoryStream())
            {
                var msInput = new MemoryStream(input);
                ms.Write(input, 0, SkuFileUploader1.FileBytes.Length);
                fileName = ProgramNameFld.Text.Trim().Trim().Replace("Program Name: ", "").Replace(" ", "").Replace(@"/", "").Replace(@".", "") + @"_" + hdnCustId.Value.Trim().Replace("Customer ID: ", "") + fileName.Substring(fileName.LastIndexOf(".")).ToLower();
                bool uploaded = QueryCpUtils.UploadImages(fileName, "gdlight/SkuReceipt", ms, this);
            }
        }

        public string GetSKUPath()
        {
            string oldFileName = SkuFileUploader1.FileName;
            string fileName = ProgramNameFld.Text.Trim().Replace("Program Name: ", "").Replace(@".", "").Replace(@"/", "").Replace(" ", "") + SkuFileUploader1.FileName.Substring(SkuFileUploader1.FileName.LastIndexOf("."));
            string container = Session["AzureContainerName"].ToString();
            string directory = Session["SkuReceiptDirectory"].ToString();
            string url = @"https://gdlightstorage.blob.core.windows.net/" + container + @"/" + directory + @"/" + fileName;
            return url;
        }

        protected void AddSkuOnlyBtnClick(object sender, EventArgs e)
        {
            var editData = GetCpEditFromView();
            List<int> docIds = new List<int>();
            bool found = false;
            foreach (CpDocModel cpDoc in editData.CpDocs)
            {
                foreach (CpDocPrintModel attachedReport in cpDoc.AttachedReports)
                {
                    if (attachedReport.DocumentTypeCode != "8" && attachedReport.DocumentTypeCode != "10")
                        docIds.Add(attachedReport.DocId);
                }
                if (docIds.Count > 0)
                {
                    foreach (int docId in docIds)
                    {
                        found = QueryCpUtilsNew.GetMinSkuDoc(docId, this);
                        if (found)
                            break;
                    }
                }
            }
            if (found)
            {
                if (ErrPictureField.Text == null)
                    ErrPictureField.Text = "";
                if (ProgramNameFld.Text == "")
                    return;
                var skuOnlyList = SyntheticScreeningUtils.AddNewSku(ProgramNameFld.Text, this);
                if (skuOnlyList != null)
                {
                    if (ErrPictureField.Text == "")
                        ErrPictureField.Text = "Loaded Sku" + ProgramNameFld.Text;
                    else
                        ErrPictureField.Text += ", Loaded Sku" + ProgramNameFld.Text;
                }
                else
                    if (ErrPictureField.Text == "")
                    ErrPictureField.Text = "Error adding Sku " + ProgramNameFld.Text;
                else
                    ErrPictureField.Text += ", Error adding Sku" + ProgramNameFld.Text;
            }
        }
        protected void OnReqFractionGroupClick(object sender, EventArgs e)
        {
            ReqFRangesGroupList.Visible = true;
            FullRangeFld.Value = "";
            var dt = new DataTable();
            dt = QueryCpUtilsNew.GetRangeGroupList(this);
            if (dt != null && dt.Rows.Count != 0)
            {
                List<FractionRangesGroupModel> rangeGroups = (from DataRow row in dt.Rows select new FractionRangesGroupModel(row)).ToList();
                //foreach (FractionRangesGroupModel group in rangeGroups)
                //{
                //    ReqFRangesGroupList.Items.Add(group.GroupName + "(" + group.GroupId + ")");
                //}
                ReqFRangesGroupList.DataSource = rangeGroups;
                ReqFRangesGroupList.DataBind();
                ReqFRangesGroupList.SelectedIndex = 0;
                ReqFRangesGroupList.SelectedIndex = -1;
                ReqFRangesGroupList.Items.Insert(0, new ListItem("Please Select Range Group", ""));

            }
        }
        protected void OnReqRangeGroupClick(object sender, EventArgs e)
        {
            ReqFRangeList.Visible = true;
            ReqFDisplayBtn.Visible = true;
            var rangeGroup = ReqFRangesGroupList.SelectedItem.Text;

            DataTable dt = QueryCpUtilsNew.GetFractionRangeList(rangeGroup, this);
            List<string> dataList = new List<string>();
            if (dt != null && dt.Rows.Count > 0)
            {
                ReqFRangeList.Items.Add("Fraction  Range Name");
                foreach (DataRow dr in dt.Rows)
                {
                    ReqFRangeList.Items.Add(dr["Fraction"].ToString() + "   " + dr["RangeName"].ToString());
                }
            }
        }
        protected void OnCloseReqRangeGroupClick(object sender, EventArgs e)
        {
            ReqFRangeList.Items.Clear();
            ReqFRangeList.Visible = false;
            ReqFRangesBtn.Visible = false;
        }

        protected void SaveCommentsClick(object sender, EventArgs e)
        {
            ErrSaveDescComments.Text = "";
            Button btn = (Button)sender;
            string btnName = btn.Text;
            if (btnName.ToLower().Contains("comments") && (CpDescrip.Text == ""))
            {
                ErrSaveDescComments.Text = "Comments are blank";
                return;
            }
            if (btnName.ToLower().Contains("description") && (CpComments.Text == ""))
            {
                ErrSaveDescComments.Text = "Description is blank";
                return;
            }
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                new List<CustomerProgramModel>();
            if (cpList == null)
            {
                ErrSaveDescComments.Text = "Error processing";
                return;
            }
            var cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
            SavedDescCommentsListModel model = new SavedDescCommentsListModel();
            model.ItemTypeID = cp.ItemTypeId;
            if (btnName.ToLower().Contains("comments"))
                model.Text = CpDescrip.Text;
            else
                model.Text = CpComments.Text;


            if (btnName == "Save Comments")
            {
                model.Type = "Description";
                if (CommentsNameBox.Text == "")
                    model.Name = Regex.Replace(ProgramNameFld.Text, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + @"_" + CustomerLike.Text + @"_" + model.ItemTypeID;
                else
                {
                    if (!CommentsNameBox.Text.Contains(CustomerLike.Text) && !CommentsNameBox.Text.Contains(model.ItemTypeID.ToString()))
                        model.Name = CommentsNameBox.Text + @"_" + CustomerLike.Text + @"_" + model.ItemTypeID.ToString();
                    else
                        model.Name = CommentsNameBox.Text;
                }
                CommentsNameBox.Text = model.Name;
                //model.Name = CommentsNameBox.Text;
            }
            else
            {
                model.Type = "Comments";
                //model.Name = DescNameBox.Text;
                //string skuName = Regex.Replace(CustomerLike.Text, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled)
                if (DescNameBox.Text == "")
                    model.Name = Regex.Replace(ProgramNameFld.Text, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + @"_" + CustomerLike.Text + @"_" + model.ItemTypeID;
                else
                {
                    if (!DescNameBox.Text.Contains(CustomerLike.Text) && !DescNameBox.Text.Contains(model.ItemTypeID.ToString()))
                        model.Name = DescNameBox.Text + @"_" + CustomerLike.Text + @"_" + model.ItemTypeID;
                    else
                        model.Name = DescNameBox.Text;
                }
                DescNameBox.Text = model.Name;
            }
            var measureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(model.ItemTypeID), this);
            List<string> parts = new List<string>();
            foreach (var measure in measureParts)
            {
                parts.Add(measure.PartName);
            }
            bool filtered = FilterDescCommentsText(CpDescrip.Text, parts);
            if (filtered)
            {
                DataTable dt = QueryCpUtilsNew.AddCPComments(model, this);
                if (dt != null && dt.Rows.Count != 0 && dt.Rows[0].ItemArray[0].ToString() == "success")
                    LoadSavedDescCommentsList(model.ItemTypeID);
                else if (dt != null && dt.Rows.Count != 0 && dt.Rows[0].ItemArray[0].ToString() == "exists")
                    ErrSaveDescComments.Text = model.Name + " already exists.";
            }
            else
                ErrSaveDescComments.Text = "Error. Text does not match with parts.";
        }
        protected bool FilterDescCommentsText(string text, List<string> parts)
        {
            bool found = false;
            text = text.Replace("^", "");
            if (!text.Contains("["))
                return true;
            int Done = 0;
            while (text.Contains("["))
            {
                text = text.Substring(text.IndexOf("[") + 1);
                if (text.Length == 0)
                    Done = 1;
                else
                {
                    string partName = text.Substring(0, text.IndexOf("."));
                    string result = parts.Find(m => m.ToLower() == partName.ToLower());
                    if (result != null)
                        found = true;
                    else
                    {
                        found = false;
                    }
                }

            }
            return found;
        }
        protected void DeleteCommentsClick(object sender, EventArgs e)
        {
            ErrSaveDescComments.Text = "";
            if (CommentsNameBox.Text == "")
            {
                ErrSaveDescComments.Text = "Select comment name to delete";
                return;
            }
            DataTable dt = QueryCpUtilsNew.DeleteSavedCommentsDesc(CommentsNameBox.Text, "description", this);
            if (dt == null || dt.Rows.Count == 0 || dt.Rows[0].ItemArray[0].ToString() == "failed")
            {
                ErrSaveDescComments.Text = "Error deleting comments for " + SavedCommentsList.SelectedItem;
                if (hdnItemTypeID.Value != "")
                    LoadSavedDescCommentsList(Convert.ToInt32(hdnItemTypeID.Value));
            }
            else
            {
                ErrSaveDescComments.Text = SavedCommentsList.SelectedValue + " was deleted";
            }
        }

        protected void OnCommentsNameSelectedChanged(object sender, EventArgs e)
        {
            //var savedCommentsNameList = GetViewState(SessionConstants.SavedDescComments) as List<SavedDescCommentsListModel>;
            //string text = savedCommentsNameList.Find(m => m.Name == SavedCommentsList.SelectedValue).Text;
            ErrSaveDescComments.Text = "";
            DropDownList btn = (DropDownList)sender;
            string name = "Description";
            if (btn.ID.Contains("Desc"))
                name = "Comments";

            List<SavedDescCommentsListModel> savedList = new List<SavedDescCommentsListModel>();
            string text = "", secondText = "";
            if (Session["SavedDescComments"] == null)
            {
                DataTable dt = QueryCpUtilsNew.GetSavedDescCommentsList(this);
                if (dt != null)
                {
                    var result = (from DataRow row in dt.Rows select new SavedDescCommentsListModel(row)).ToList();
                    Session["SavedDescComments"] = result;

                }
            }
            savedList = Session["SavedDescComments"] as List<SavedDescCommentsListModel>;
            if (savedList.Count == 0)
                return;
            try
            {
                if (name == "Description")
                    text = savedList.Find(m => m.Name == SavedCommentsList.SelectedItem.Text && m.Type.ToLower() == name.ToLower()).Text;
                else
                    text = savedList.Find(m => m.Name == SavedDescList.SelectedItem.Text && m.Type.ToLower() == name.ToLower()).Text;
            }
            catch (Exception ex)
            {
                ErrSaveDescComments.Text = "Error retrieving saved text for " + name;
                return;
            }
            try
            {
                string secondName = "comments";
                if (name == "Comments")
                {
                    secondName = "description";
                    var descItem = savedList.Find(m => m.Name == SavedDescList.SelectedItem.Text && m.Type.ToLower() == secondName);
                    if (descItem != null)
                    {
                        secondText = descItem.Text;
                        for (int i = 0; i <= SavedCommentsList.Items.Count - 1; i++)
                        {
                            if (SavedCommentsList.Items[i].Text == SavedDescList.SelectedItem.Text)
                            {
                                SavedCommentsList.SelectedIndex = i;
                                break;
                            }
                        }
                    }
                    //secondText = savedList.Find(m => m.Name == SavedDescList.SelectedItem.Text && m.Type.ToLower() == secondName).Text;
                }
                else
                {
                    var descItem = savedList.Find(m => m.Name == SavedCommentsList.SelectedItem.Text && m.Type.ToLower() == secondName);
                    if (descItem != null)
                    {
                        secondText = descItem.Text;
                        for (int i = 0; i <= SavedDescList.Items.Count - 1; i++)
                        {
                            if (SavedDescList.Items[i].Text == SavedCommentsList.SelectedItem.Text)
                            {
                                SavedDescList.SelectedIndex = i;
                                break;
                            }
                        }
                    }
                    //secondText = savedList.Find(m => m.Name == SavedDescList.SelectedItem.Text && m.Type.ToLower() == secondName).Text;
                }
            }
            catch
            {
                if (name == "Description")
                    ErrSaveDescComments.Text = "No text found for Comments";
                else
                    ErrSaveDescComments.Text = "No text found for Description";
            }
            var cpList = GetViewState(SessionConstants.CustomerProgramList) as List<CustomerProgramModel> ??
                new List<CustomerProgramModel>();
            if (cpList == null)
            {
                ErrSaveDescComments.Text = "Error processing";
                return;
            }
            var cp = cpList.Find(m => m.CpId == CpList.SelectedValue);
            var measureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(cp.ItemTypeId), this);
            List<string> parts = new List<string>();
            foreach (var measure in measureParts)
            {
                parts.Add(measure.PartName);
            }
            bool filtered = FilterDescCommentsText(text, parts);
            if (filtered)
            {

                if (name == "Description")
                {
                    CpDescrip.Text = text;
                    CommentsNameBox.Text = SavedCommentsList.SelectedItem.Text;
                    if (secondText != "")
                    {
                        CpComments.Text = secondText;
                        DescNameBox.Text = SavedCommentsList.SelectedItem.Text;
                    }
                }
                else
                {
                    CpComments.Text = text;
                    DescNameBox.Text = SavedDescList.SelectedItem.Text;
                    if (secondText != "")
                    {
                        CpDescrip.Text = secondText;
                        CommentsNameBox.Text = SavedCommentsList.SelectedItem.Text;
                    }
                }
            }
            else
                ErrSaveDescComments.Text = "Text does not match with parts.";
        }

        protected void DeleteDescClick(object sender, EventArgs e)
        {
            ErrSaveDescComments.Text = "";
            if (DescNameBox.Text == "")
            {
                ErrSaveDescComments.Text = "Select comments to delete";
                return;
            }
            DataTable dt = QueryCpUtilsNew.DeleteSavedCommentsDesc(DescNameBox.Text, "comments", this);
            if (dt == null || dt.Rows.Count == 0 || dt.Rows[0].ItemArray[0].ToString() == "failed")
            {
                ErrSaveDescComments.Text = "Error deleting Description for " + SavedCommentsList.SelectedItem;
                if (hdnItemTypeID.Value != "")
                    LoadSavedDescCommentsList(Convert.ToInt32(hdnItemTypeID.Value));
            }
            else
            {
                ErrSaveDescComments.Text = SavedCommentsList.SelectedValue + " was deleted";
            }
        }
    }
}