﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="LeoReport2.aspx.cs" Inherits="Corpt.LeoReport2" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ></ajaxToolkit:ToolkitScriptManager>
    <div class="demoarea">
<%--    <script type="text/javascript">--%>
<%----%>
<%--        $(window).resize(function () {--%>
<%--            gridviewScrollPercent();--%>
<%--        });--%>
<%--        $(document).ready(function () {--%>
<%--            gridviewScrollPercent();--%>
<%----%>
<%--        });--%>
<%--        function gridviewScrollPercent() {--%>
<%--            var widthGrid = $('#gridContainer').width();--%>
<%--            var heightGrid = $('#gridContainer').height();--%>
<%--            $('#<%=grdShortReport.ClientID%>').gridviewScroll({--%>
<%--                width: widthGrid,--%>
<%--                height: heightGrid,--%>
<%--                freezesize: 3--%>
<%--            });--%>
<%--        }--%>
<%--    </script>--%>
    <style>
        .text_highlitedyellow
        {
            background-color: #FFFF00;
        }
    </style>
    <div class="demoheading">Leo Report II</div>
    <div class="navbar nav-tabs">
        <table>
            <tr>
                <td><asp:Button runat="server" ID="ShowReportButton" Text="Show Report" OnClick="OnShowReportClick" CssClass="btn btn-info" /></td>        
                <td><asp:Button ID="ClearButton" runat="server" Text="Clear" CssClass="btn btn-info" OnClick="OnClearClick" /> </td>
                <td>
                    <asp:ImageButton ID="ExcelButton" ToolTip="Export to Excel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                        OnClick="OnExcelClick" Enabled="False" />
                </td>
                <td>
                    <asp:Button ID="btnMovedItems" runat="server" OnClientClick="return false;" Text="Moved Items"
                        Style="margin-left: 5px" CssClass="btn btn-info " Visible="false" />
                </td>
                <td><asp:Button ID="OrderButton" runat="server" Text="Order Reports" CssClass="btn btn-info" OnClick="OnOrderReportsClick" /></td>
                <td><asp:Label runat="server" ID="SaveErrLabel" ForeColor="#990000"></asp:Label></td>
            </tr>
            <tr>
                <td>Send Email</td>
                <td colspan="2">Address To</td>
                <td colspan="2">
                    <asp:Panel ID="Panel2" runat="server" CssClass="form-inline" DefaultButton="GoEmailButton"
                        Style="padding-top: 10px">
                        <asp:TextBox runat="server" ID="AddressTo" Width="200px" placeholder="Email"></asp:TextBox>
                        <asp:ImageButton ID="GoEmailButton" ToolTip="Email Short Report" runat="server" ImageUrl="~/Images/ajaxImages/email_go.png"
                            OnClick="OnSendEmailClick" />
                        
                    </asp:Panel>
                </td>
                <td><asp:Label runat="server" ID="SendEmailMessage" CssClass="control-label" ForeColor="#990000"></asp:Label></td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td style="vertical-align: top">
                <asp:Label ID="Label1" runat="server" Text="Order Code"></asp:Label>
            </td>
            <td style="vertical-align: top">
                <asp:Panel ID="Panel1" runat="server" CssClass="form-inline" DefaultButton="AddOrderButton">
                <asp:TextBox runat="server" ID="OrderField" ></asp:TextBox>
                    <asp:ImageButton ID="AddOrderButton" runat="server" ToolTip="Add Order" ImageUrl="~/Images/ajaxImages/add.png"
                        OnClick="OnAddOrderClick" />
                </asp:Panel>
            </td>
            <td style="vertical-align: top;width: 200px" >
                <asp:Panel ID="OrdersPanel" runat="server" Style="vertical-align: top" CssClass="form-inline" >
                    <asp:ListBox runat="server" ID="OrderList" Rows="8" CssClass="control-list" Style="width: 88%;height: 100%" />
                    <asp:ImageButton ID="DelOrderButton" runat="server" ToolTip="Remove selected Order"
                        ImageUrl="~/Images/ajaxImages/del.png" OnClick="OnDelOrderClick" Style="vertical-align: top" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel CssClass="form-inline" runat="server" ID="GridButtonsPanel"  Visible="False">
        <asp:CheckBox CssClass="checkbox" runat="server" Text="Check All (Order)" ID="OrderCheckBox" AutoPostBack="True"  Width="200px"
            OnCheckedChanged="OnCheckAllClick">
        </asp:CheckBox>
        <asp:ImageButton ID="cmdReorient" ToolTip="Change direction table" runat="server"
            ImageUrl="~/Images/ajaxImages/reorient1.png" OnClick="OnReorientClick" 
            Style="margin-left: 5px;padding-right: 10px" />
        <asp:CheckBox CssClass="checkbox" runat="server" Text="Hide Empty Columns" ID="HideEmptyCheckBox" AutoPostBack="True"  Width="200px"
            OnCheckedChanged="OnHideEmptyClick">
        </asp:CheckBox>
        <asp:Label ID="LblRowCounts" runat="server" Style="padding-left: 40px;text-align: right; font-weight: bold" />
        <asp:TextBox ID="reorientNowFld" runat="server" AutoPostBack="true" Visible="False"
            Text="H" Width="20"></asp:TextBox>
        
    </asp:Panel>
    
    <div id="gridContainer" style="font-size: small">
        <asp:DataGrid ID="grdShortReport" runat="server" CellPadding="5" AllowSorting="True"
            OnSortCommand="OnSortCommand">
            <HeaderStyle CssClass="GridviewScrollHeader" />
            <ItemStyle CssClass="GridviewScrollItem"></ItemStyle>
            <PagerStyle CssClass="GridviewScrollPager" />
            <Columns>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                        Order
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox runat="server" Text='' ID="Checkbox1"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <!-- Moved Items region -->
    <div style="margin: 0px; margin: 5px;">
        
        <!-- "Wire frame" div used to transition from the button to the info panel -->
        <div id="flyout" style="display: none; overflow: hidden; z-index: 2; background-color: #FFFFFF; border: solid 1px #D0D0D0;"></div>
        
        <!-- Info panel to be displayed as a flyout when the button is clicked -->
        <div id="info" style="display: none; width:400px; z-index: 2; opacity: 0; 
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); 
            font-size: 12px; font-family: Cambria; border: solid 1px #CCCCCC; background-color: #FFFFFF; padding: 5px;">
            <!-- Close Button -->
            <div id="btnCloseParent" style="float: right; opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);">
                <asp:LinkButton id="btnClose" runat="server" OnClientClick="return false;" Text="X" ToolTip="Close"
                    Style="background-color: #666666; color: #FFFFFF; text-align: center; font-weight: bold; text-decoration: none; border: outset thin #FFFFFF; padding: 2px;" />
            </div>
			<!-- Data Grid with Removed Items -->
            <div style="overflow-x: auto; overflow-y: auto; padding: 2px;height: 400px; width: 400px;">
                <asp:DataGrid runat="server" ID="dgPopup" CssClass="table table-bordered" >
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:DataGrid>
            </div>
        </div>
        
        <script type="text/javascript" language="javascript">
            // Move an element directly on top of another element (and optionally
            // make it the same size)
            function Cover(bottom, top, ignoreSize) {
                var location = Sys.UI.DomElement.getLocation(bottom);
                top.style.position = 'absolute';
                top.style.top = location.y + 'px';
                top.style.left = location.x + 'px';
                if (!ignoreSize) {
                    top.style.height = bottom.offsetHeight + 'px';
                    top.style.width = bottom.offsetWidth + 'px';
                }
            }
        </script>
        <ajaxToolkit:AnimationExtender id="OpenAnimation" runat="server" TargetControlID="btnMovedItems">
            <Animations>
                <OnClick>
                    <Sequence>
                        <%-- Disable the button so it can't be clicked again --%>
                        <EnableAction Enabled="false" />
                        
                        <%-- Position the wire frame on top of the button and show it --%>
                        <ScriptAction Script="Cover($get('ctl00_SampleContent_btnMovedItems'), $get('flyout'));" />
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="block"/>
                        
                        <%-- Move the wire frame from the button's bounds to the info panel's bounds --%>
                        <Parallel AnimationTarget="flyout" Duration=".3" Fps="25">
                            <Move Horizontal="150" Vertical="-50" />
                            <Resize Width="400" Height="400" />
                            <Color PropertyKey="backgroundColor" StartValue="#AAAAAA" EndValue="#FFFFFF" />
                        </Parallel>
                        
                        <%-- Move the info panel on top of the wire frame, fade it in, and hide the frame --%>
                        <ScriptAction Script="Cover($get('flyout'), $get('info'), true);" />
                        <StyleAction AnimationTarget="info" Attribute="display" Value="block"/>
                        <FadeIn AnimationTarget="info" Duration=".2"/>
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="none"/>
                        
                        <%-- Flash the text/border red and fade in the "close" button --%>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#666666" EndValue="#FF0000" />
                            <Color PropertyKey="borderColor" StartValue="#666666" EndValue="#FF0000" />
                        </Parallel>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#FF0000" EndValue="#666666" />
                            <Color PropertyKey="borderColor" StartValue="#FF0000" EndValue="#666666" />
                            <FadeIn AnimationTarget="btnCloseParent" MaximumOpacity=".9" />
                        </Parallel>
                    </Sequence>
                </OnClick>
            </Animations>
        </ajaxToolkit:AnimationExtender>
        <ajaxToolkit:AnimationExtender id="CloseAnimation" runat="server" TargetControlID="btnClose">
            <Animations>
                <OnClick>
                    <Sequence AnimationTarget="info">
                        <%--  Shrink the info panel out of view --%>
                        <StyleAction Attribute="overflow" Value="hidden"/>
                        <Parallel Duration=".3" Fps="15">
                            <Scale ScaleFactor="0.05" Center="true" ScaleFont="true" FontUnit="px" />
                            <FadeOut />
                        </Parallel>
                        
                        <%--  Reset the sample so it can be played again --%>
                        <StyleAction Attribute="display" Value="none"/>
                        <StyleAction Attribute="width" Value="400px"/>
                        <StyleAction Attribute="height" Value=""/>
                        <StyleAction Attribute="fontSize" Value="12px"/>
                        <OpacityAction AnimationTarget="btnCloseParent" Opacity="0" />
                        
                        <%--  Enable the button so it can be played again --%>
                        <EnableAction AnimationTarget="btnMovedItems" Enabled="true" />
                    </Sequence>
                </OnClick>
                <OnMouseOver>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FFFFFF" EndValue="#FF0000" />
                </OnMouseOver>
                <OnMouseOut>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FF0000" EndValue="#FFFFFF" />
                </OnMouseOut>
             </Animations>
        </ajaxToolkit:AnimationExtender>


    </div>
    <ajaxToolkit:FilteredTextBoxExtender ID="CountFilteredTextBoxExtender" runat="server"
        TargetControlID="OrderField" FilterType="Numbers" />
    </div>
</asp:Content>
