﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Web.SessionState;
using GemoDream.QueueMessages;

namespace Corpt.Utilities
{
    public class CorptLogger : ILogger
    {
        private readonly HttpSessionState _session;

        public CorptLogger(HttpSessionState session)
        {
            _session = session;
        }

        public void Error(string message, bool newLine = false, [CallerMemberName] string member = "", [CallerFilePath] string callerFile = "")
        {
            WriteEntry(message, "error", member, callerFile, newLine);
        }

        public void Error(Exception ex, bool newLine = true, [CallerMemberName] string member = null, [CallerFilePath] string callerFile = "")
        {
            WriteEntry(string.Empty, "error", member, callerFile, newLine);
            WriteEntry(ex.Message, "error", member, callerFile);
            WriteEntry(ex.StackTrace, "error", member, callerFile);
            if (ex.InnerException != null)
            {
                Error(ex.InnerException);
            }
        }

        public void Warning(string message, bool newLine = false, [CallerMemberName] string member = null, [CallerFilePath] string callerFile = "")
        {
            WriteEntry(message, "warning", member, callerFile, newLine);
        }

        public void Info(string message, bool newLine = false, [CallerMemberName] string member = null, [CallerFilePath] string callerFile = "")
        {
            WriteEntry(message, "info", member, callerFile, newLine);
        }

        public void Info(string queryText, TimeSpan duration, bool newLine = false, [CallerMemberName] string member = null, [CallerFilePath] string callerFile = "")
        {
            var durationStr = Convert.ToInt32(duration.TotalMilliseconds)
                .ToString(CultureInfo.InvariantCulture).PadLeft(5);
            WriteEntry(queryText, "info", member, callerFile, newLine, durationStr);
        }

        private void WriteEntry(string message, string type, string member, string callerFile, bool newLine = false, string duration = null)
        {
            var fileName = callerFile.Substring(callerFile.LastIndexOf("\\", StringComparison.InvariantCulture) + 1);
            if (fileName.Contains("."))
            {
                fileName = fileName.Substring(0, fileName.LastIndexOf(".", StringComparison.InvariantCulture));
            }

            var message2 = (newLine ? "\n" : "") +
                           $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}\t{type}\t{duration}\t{message}\t({fileName}.{member})";
            LogUtils.UpdateSpLog(message2, _session);
        }
    }
}