﻿using System.Web;

namespace Corpt.Utilities
{
    public class AppUtils
    {
        public static string MappedApplicationPath
        {
            get
            {
                var appPath = HttpContext.Current.Request.ApplicationPath.ToLower();
                if (appPath == "/")      //a site
                    appPath = "/";
                else if (!appPath.EndsWith(@"/")) //a virtual
                    appPath += @"/";

                string it = HttpContext.Current.Server.MapPath(appPath);
                if (!it.EndsWith(@"\"))
                    it += @"\";
                return it;
            }
        }
    }
}