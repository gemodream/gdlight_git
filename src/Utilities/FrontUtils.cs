﻿using Corpt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;

namespace Corpt.Utilities
{
    public class FrontUtils
	{
		#region Constants
		public struct BatchEvents
		{
			public const int Opened = 1;
			public const int Updated = 2;
			public const int Touched = 3;
			public const int Closed = 4;
			public const int Created = 5;
			public const int EndSession = 6;
			public const int Billed = 7;
			public const int TakeOut = 8;
			public const int ShipOut = 9;
			public const int AddReport = 10;
			public const int Printed = 11;           
            public const int ReadyForDelivery = 19;
			public const int CreatedFromOldNumbers = 20;
		}
		public struct Codes
		{
			public const int Color = 5;
			public const int Clarity = 4;
			public const int Measure = 6;
			public const int Itemizing = 2;
			public const int AccRep = 8;
			public const int LaserInscription = 67;
			public const int Shape = 8;
		}

		#endregion

		#region TakeIn
		public static DataTable GetMeasureUnits(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasureUnits",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;
        }

        public static DataTable GetServiceTypes(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetServiceTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;
        }
       
        public static string CreateOrder(NewOrderModelExt objNewOrderModelExt, string orderEvent, Page p, out string msg)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spsetEntryBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;

            command.Parameters["@rID"].Size = 1024;

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID; 
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@IsIQInspected", SqlDbType.Int).Value = objNewOrderModelExt.IsIQInspected;
            command.Parameters.Add("@IsTWInspected", SqlDbType.Int).Value = objNewOrderModelExt.IsTWInspected;
            if (objNewOrderModelExt.NumberOfItems == 0)
                command.Parameters.Add("@ItemsQuantity", SqlDbType.Int).Value = DBNull.Value;
            else
                command.Parameters.Add("@ItemsQuantity", SqlDbType.Int).Value = objNewOrderModelExt.NumberOfItems;

			if (objNewOrderModelExt.TotalWeight ==0)
				command.Parameters.Add("@TotalWeight", SqlDbType.Float).Value = DBNull.Value;
			else
				command.Parameters.Add("@TotalWeight", SqlDbType.Float).Value = objNewOrderModelExt.TotalWeight;

			command.Parameters.Add("@MeasureUnitID", SqlDbType.Int).Value = objNewOrderModelExt.MeasureUnitID;
			//IvanB 13/02 start
            command.Parameters.Add("@ServiceTypeID", SqlDbType.Int).Value = objNewOrderModelExt.ServiceTypeID;  
			//IvanB 13/02 end
			if (string.IsNullOrEmpty(objNewOrderModelExt.Memo))
				command.Parameters.Add("@Memo", SqlDbType.VarChar).Value = DBNull.Value;
			else
				command.Parameters.Add("@Memo", SqlDbType.VarChar).Value = objNewOrderModelExt.Memo;


			if ((objNewOrderModelExt.MemoNumbers != null) && (objNewOrderModelExt.MemoNumbers.Count > 0))
                command.Parameters.Add("@IsMemo", SqlDbType.Int).Value = objNewOrderModelExt.MemoNumbers.Count;
            else
                command.Parameters.Add("@IsMemo", SqlDbType.Int).Value = 0;

            if (string.IsNullOrEmpty(objNewOrderModelExt.SpecialInstructions))
                command.Parameters.Add("@SpecialInstruction", SqlDbType.VarChar).Value = DBNull.Value;
            else
                command.Parameters.Add("@SpecialInstruction", SqlDbType.VarChar).Value = objNewOrderModelExt.SpecialInstructions;

			if (string.IsNullOrEmpty(objNewOrderModelExt.CarrierTrackingNumber))
				command.Parameters.Add("@CarrierTrackingNumber", SqlDbType.VarChar).Value = DBNull.Value;
			else
				command.Parameters.Add("@CarrierTrackingNumber", SqlDbType.VarChar).Value = objNewOrderModelExt.CarrierTrackingNumber;
			if (objNewOrderModelExt.CarrierID == 0)
				command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = DBNull.Value;
			else
				command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = objNewOrderModelExt.CarrierID;
			if (objNewOrderModelExt.PersonID!=0)
			{
				command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.PersonCustomerOfficeID;
				command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = objNewOrderModelExt.PersonCustomerID;
				command.Parameters.Add("@PersonID", SqlDbType.Int).Value = objNewOrderModelExt.PersonID;
			}
			else
			{
				command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = DBNull.Value;
				command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = DBNull.Value;
				command.Parameters.Add("@PersonID", SqlDbType.Int).Value = DBNull.Value;
			}
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.Customer.OfficeId;
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = objNewOrderModelExt.Customer.CustomerId;
            command.Parameters.Add("@VendorOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.VendorOfficeID==0? (object)DBNull.Value: objNewOrderModelExt.VendorOfficeID;
			command.Parameters.Add("@VendorID", SqlDbType.Int).Value = objNewOrderModelExt.VendorID == 0 ? (object)DBNull.Value : objNewOrderModelExt.VendorID;
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return null;
            }
                      
            try
            {
                var res = command.Parameters["@rId"].Value.ToString();
                objNewOrderModelExt.GroupOfficeID = Int32.Parse(res.Split('_')[0]);
                objNewOrderModelExt.GroupID = Int32.Parse(res.Split('_')[1]);

                command = new SqlCommand
                {
                    CommandText = "spGetGroup",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
                command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value =
                    Int32.Parse("" + p.Session["AuthorOfficeID"]);
                command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;
                command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    objNewOrderModelExt.OrderNumber = Utils.FillToFiveChars("" + dt.Rows[0]["OrderCode"]);
                    objNewOrderModelExt.Customer.CustomerName = "" + dt.Rows[0]["CustomerName"];
                }
                if (orderEvent == "TakeIn")
                    SetOrderEvent(Convert.ToInt32(objNewOrderModelExt.OrderNumber.ToString()), 1, p);
                else if(orderEvent == "ShipIn")
                    SetOrderEvent(Convert.ToInt32(objNewOrderModelExt.OrderNumber.ToString()), 16, p);

                if ((dt.Rows.Count > 0) && (objNewOrderModelExt.MemoNumbers != null) && (objNewOrderModelExt.MemoNumbers.Count > 0))
                {
                    foreach (var memo in objNewOrderModelExt.MemoNumbers)
                    {
                        command = new SqlCommand
                        {
                            CommandText = "spSetGroupMemoNumber",
                            Connection = conn,
                            CommandType = CommandType.StoredProcedure,
                            CommandTimeout = p.Session.Timeout
                        };

                        command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
                        command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value =
                            Int32.Parse("" + p.Session["AuthorOfficeID"]);
                        command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;
                        command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
                        command.Parameters["@rId"].Size = 1024;
                        command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
                        command.Parameters.Add("@Name", SqlDbType.VarChar);
                        command.Parameters["@Name"].Size = 2048;
                        command.Parameters["@Name"].Value = memo;
                        LogUtils.UpdateSpLogWithParameters(command, p);
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            msg = ex.Message;
                            LogUtils.UpdateSpLogWithException(msg, p);
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return null;
            }
            conn.Close();
            msg = "";
            return objNewOrderModelExt.OrderNumber;

        }

        public static bool SetPickedUpByOurMessenger(NewOrderModelExt objNewOrderModelExt, Page p, out string msg)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spsetPickedUpByOurMessenger",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //var res = command.Parameters["@rId"].Value.ToString();
            //groupModel.GroupOfficeID = Int32.Parse(res.Split('_')[0]);
            //groupModel.GroupID = Int32.Parse(res.Split('_')[1]);

            command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
            command.Parameters["@rID"].Size = 1024;

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value =Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@PickedUpByOurMessenger", SqlDbType.Bit).Value = objNewOrderModelExt.PickedUpByOurMessenger;
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            conn.Close();
            msg = "";
            return true;
        }

        public static bool SetNoGoods(NewOrderModelExt objNewOrderModelExt, Page p, out string msg)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetNoGoods",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@IsNoGoods", SqlDbType.Bit).Value = objNewOrderModelExt.IsNoGoods;
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            conn.Close();
            msg = "";
            return true;
        }

        public static bool AddInvoice(int iViewAccessCode, int iGroupOfficeId, int iGroupId,NewOrderModelExt objNewOrderModelExt, Page p, out string msg)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spAddInvoice",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var res = command.Parameters["@rId"].Value.ToString();
            objNewOrderModelExt.GroupOfficeID = Int32.Parse(res.Split('_')[0]);
            objNewOrderModelExt.GroupID = Int32.Parse(res.Split('_')[1]);

            command.Parameters.Add("@Price", SqlDbType.Money).Value = -2;// same effect as NULL
            command.Parameters.Add("@ViewAccessCode", SqlDbType.Bit).Value = iViewAccessCode;
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = iGroupOfficeId;
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = iGroupId;

            command.Parameters.Add("@BatchID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            conn.Close();
            msg = "";
            return true;
        }

		public static List<BatchModel> GetBatches(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "SpGetBatchByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
            if (String.IsNullOrEmpty(batchCode))
            {
                command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
                command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = Int32.Parse(batchCode);
            }
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = Int32.Parse(orderCode.Trim());
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtBatchIds = new DataTable();
            da.Fill(dtBatchIds);
            var result = (from DataRow row in dtBatchIds.Rows select new BatchModel(row)).ToList();
            conn.Close();
            return result;

        }

		public static DataTable GetOrderFullInfo(string order, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetGroupByCode2",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);

			command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.AddWithValue("@BDate", DBNull.Value);
			command.Parameters.AddWithValue("@EDate", DBNull.Value);

			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = int.Parse(order);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);

			if (dt.Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = dt;
			conn.Close();
			return result;
		}
		
		public static DataTable GetOrderSummery(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGroupSummeryByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = int.Parse(order);
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count == 0) return null;
            //	var result = new OrderModel(dt.Rows[0], order);

            var result = dt;
            conn.Close();
            return result;
        }
		public static DataTable GetItemsByCriteria(string criteria, string value, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetItemsByCriteria",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@Criteria", SqlDbType.VarChar).Value = criteria;
			command.Parameters.Add("@Value", SqlDbType.VarChar).Value = value;

			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);

			if (dt.Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = dt;
			conn.Close();
			return result;
		}
		public static bool SetOrderOffice(int GroupID, int RealOfficeID, int RealUserID,string IPAddress, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			string msg = string.Empty;
			var command = new SqlCommand
			{
				CommandText = "spSetOrderOffice",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@GroupID", SqlDbType.Int).Value = GroupID;
			command.Parameters.Add("@RealOfficeID", SqlDbType.Int).Value = RealOfficeID;
			command.Parameters.Add("@RealUserID", SqlDbType.Int).Value = RealUserID;
			command.Parameters.Add("@IPAddress", SqlDbType.VarChar).Value = IPAddress;
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}
		public static DataSet GetJewelryItemDetail(int requestID, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetJewelryItemDetail",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@RequestID", SqlDbType.Int).Value = requestID;

			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var ds = new DataSet();
			da.Fill(ds);

			if (ds.Tables[0].Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = ds;
			conn.Close();
			return result;
		}
		public static string DelJewelryItemDetail(int requestID, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spDelJewelryItemDetail",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@RequestID", SqlDbType.Int).Value = requestID;
			LogUtils.UpdateSpLogWithParameters(command, p);
			command.ExecuteNonQuery();
			conn.Close();
			return null;
		}
		#endregion

		#region Update/Get Group
		public static DataSet GetGroupDetail(string groupCode, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			string sql = @"select GroupID,GroupOfficeID,CustomerCode from v0group where groupcode = " + groupCode;
			SqlDataAdapter adapter;
			DataSet ds = new DataSet();
			try
			{
				adapter = new SqlDataAdapter(sql, conn);
				adapter.Fill(ds);
				conn.Close();
				return ds;
			}
			catch (Exception ex)
			{
				string msg = ex.Message;
				return null;
			}
		}
		public static string UpdateGroupDetail(NewOrderModel itemModel, Page p)
		{
			var msg = "";
			//run recalc here
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spUpdateGroup",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@OrderCode", itemModel.OrderNumber);
			command.Parameters.AddWithValue("@SpecialInstruction", itemModel.SpecialInstructions);
            command.Parameters.AddWithValue("@NotInspectedQuantity", (object) itemModel.NumberOfItems ?? DBNull.Value);
			command.Parameters.AddWithValue("@NotInspectedTotalWeight", (object) itemModel.TotalWeight ?? DBNull.Value);
            command.Parameters.AddWithValue("@Memo", itemModel.OrderMemo);
            command.Parameters.AddWithValue("@IsNoGoods", itemModel.IsNoGoods);
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);

			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				msg = e.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}
		#endregion

		#region GiveOut
		public static DataTable GetVendorStruct(Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetGroupVendorTypeOf",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);

			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);

			//if (dt.Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = dt;
			conn.Close();
			return result;
		}

		public static bool SetGroupVendor(NewOrderModelExt objNewOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetGroupVendor",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.GroupOfficeID;
			command.Parameters.Add("@GroupID", SqlDbType.Int).Value = objNewOrderModelExt.GroupID;
			command.Parameters.Add("@VendorOfficeID", SqlDbType.Int).Value = objNewOrderModelExt.VendorOfficeID;
			command.Parameters.Add("@VendorID", SqlDbType.Int).Value = objNewOrderModelExt.VendorID==0 ? (object)DBNull.Value : objNewOrderModelExt.VendorID;
			command.Parameters.Add("@ShipmentCharge", SqlDbType.Int).Value = objNewOrderModelExt.ShipmentCharge == 0 ? (object)DBNull.Value : objNewOrderModelExt.ShipmentCharge;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static DataTable GetItemOutStruct(Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetItemOutTypeOf",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);

			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);

			//if (dt.Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = dt;
			conn.Close();
			return result;
		}

		public static bool SetItemOut(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetItemOut",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@BatchID", SqlDbType.Int).Value = newOrderModelExt.BatchID;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = newOrderModelExt.ItemCode;
			command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = newOrderModelExt.PersonCustomerOfficeID == 0 ? (object)DBNull.Value : newOrderModelExt.PersonCustomerOfficeID;
			command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = newOrderModelExt.PersonCustomerID == 0 ? (object)DBNull.Value : newOrderModelExt.PersonCustomerID;
			command.Parameters.Add("@PersonID", SqlDbType.Int).Value = newOrderModelExt.PersonID==0? (object)DBNull.Value: newOrderModelExt.PersonID;
			command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = newOrderModelExt.CarrierID==0? (object)DBNull.Value : newOrderModelExt.CarrierID;
			command.Parameters.Add("@CarrierTrackingNumber", SqlDbType.VarChar).Value = newOrderModelExt.CarrierTrackingNumber == "" ? (object)DBNull.Value : newOrderModelExt.CarrierTrackingNumber; 

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static bool SetOrderOut(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetOrderOut",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = newOrderModelExt.GroupCode;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@PersonID", SqlDbType.Int).Value = newOrderModelExt.PersonID == 0 ? (object)DBNull.Value : newOrderModelExt.PersonID;
			if (newOrderModelExt.PersonID != 0)
			{
				command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = newOrderModelExt.PersonCustomerOfficeID == 0 ? (object)DBNull.Value : newOrderModelExt.PersonCustomerOfficeID;
				command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = newOrderModelExt.PersonCustomerID == 0 ? (object)DBNull.Value : newOrderModelExt.PersonCustomerID;
			}
			else
			{
				command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = (object)DBNull.Value;
				command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = (object)DBNull.Value;
			}
			command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = newOrderModelExt.CarrierID == 0 ? (object)DBNull.Value : newOrderModelExt.CarrierID;
			command.Parameters.Add("@CarrierTrackingNumber", SqlDbType.VarChar).Value = newOrderModelExt.CarrierTrackingNumber == "" ? (object)DBNull.Value : newOrderModelExt.CarrierTrackingNumber;
			command.Parameters.Add("@TakenOutByOurMessenger", SqlDbType.Int).Value = newOrderModelExt.TakenOutByOurMessenger;

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static DataTable SetAcceptRequest(int requestId, Page p)
		{
			DataTable result = new DataTable();
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetScreeningAcceptRequest",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@RequestID", SqlDbType.Int).Value = requestId;
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var cda = new SqlDataAdapter(command);
				var cdt = new DataSet();
				cda.Fill(cdt);
				result = cdt.Tables[0];
			}
			catch (Exception ex)
            {
				result = null;
            }
			finally
            {
				conn.Close();
			}
			return result;
		}

		public static string MarkGiveOutBulkEmail (string requestId, Page p)
        {
			string result = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spMarkGiveOutEmailBulk",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@RequestID", SqlDbType.Int).Value = requestId;
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var cda = new SqlDataAdapter(command);
				var cdt = new DataSet();
				cda.Fill(cdt);
				result = cdt.Tables[0].Rows[0].ItemArray[0].ToString();
			}
			catch (Exception ex)
			{
				result = null;
			}
			finally
			{
				conn.Close();
			}
			return result;
		}


		public static string MarkPickupBulkEmail(string bulkReqId, Page p, bool single = false)
		{
			string result = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spMarkPickupEmailBulk",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			if (single)
			{
				command.Parameters.Add("@BulkPickupID", SqlDbType.VarChar).Value = DBNull.Value;
				command.Parameters.AddWithValue("@RequestID", Convert.ToInt32(bulkReqId));
			}
			else
			{
				command.Parameters.Add("@BulkPickupID", SqlDbType.VarChar).Value = bulkReqId;
				command.Parameters.AddWithValue("RequestID", DBNull.Value);
			}
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var cda = new SqlDataAdapter(command);
				var cdt = new DataSet();
				cda.Fill(cdt);
				result = cdt.Tables[0].Rows[0].ItemArray[0].ToString();
			}
			catch (Exception ex)
			{
				result = null;
			}
			finally
			{
				conn.Close();
			}
			return result;
		}
		public static DataTable GetGroupByCode(NewOrderModelExt objNewOrderModelExt, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetGroupByCode",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupCode;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var cda = new SqlDataAdapter(command);
			var cdt = new DataSet();
			cda.Fill(cdt);
			return cdt.Tables[0];
		}

		public static DataTable GetBatchbyCode(NewOrderModelExt objNewOrderModelExt, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetBatchbyCode",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = objNewOrderModelExt.BatchCode == 0 ? (object)DBNull.Value : objNewOrderModelExt.BatchCode;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var cda = new SqlDataAdapter(command);
			var cdt = new DataSet();
			cda.Fill(cdt);
			return cdt.Tables[0];
		}

		public static DataTable GetItembyCode(NewOrderModelExt objNewOrderModelExt, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetItembyCode",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = objNewOrderModelExt.BatchCode == 0 ? (object)DBNull.Value : objNewOrderModelExt.BatchCode;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = objNewOrderModelExt.ItemCode == 0 ? (object)DBNull.Value : objNewOrderModelExt.ItemCode;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var cda = new SqlDataAdapter(command);
			var cdt = new DataSet();
			cda.Fill(cdt);
			return cdt.Tables[0];
		}

		public static DataTable GetItemDocByCode(NewOrderModelExt objNewOrderModelExt, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetItemDocByCode",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = objNewOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = objNewOrderModelExt.BatchCode == 0 ? (object)DBNull.Value : objNewOrderModelExt.BatchCode;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = objNewOrderModelExt.ItemCode == 0 ? (object)DBNull.Value : objNewOrderModelExt.ItemCode;
			command.Parameters.Add("@OperationChar", SqlDbType.Int).Value = objNewOrderModelExt.OperationChar == "" ? (object)DBNull.Value : objNewOrderModelExt.OperationChar;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var cda = new SqlDataAdapter(command);
			var cdt = new DataSet();
			cda.Fill(cdt);
			return cdt.Tables[0];
		}

		public static bool SetCloseGroupStateByCode(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCloseGroupStateByCode",

				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = newOrderModelExt.GroupCode;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static bool SetCloseBatchStateByCode(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCloseBatchStateByCode",

				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = newOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = newOrderModelExt.BatchCode;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static bool SetCloseItemStateByCode(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCloseItemStateByCode",

				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = newOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = newOrderModelExt.BatchCode;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = newOrderModelExt.ItemCode
;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static bool SetCloseOperationStateByCode(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCloseOperationStateByCode",

				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = newOrderModelExt.GroupCode;
			command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = newOrderModelExt.BatchCode;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = newOrderModelExt.ItemCode;
			command.Parameters.Add("@OperationChar", SqlDbType.Int).Value = newOrderModelExt.OperationChar;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

        public static bool SetBatchEvent(int eventId, int batchId, int formId, int itemsAffected, int itemsInBatch, Page p)
        {
            return SetBatchEvent(eventId, batchId, formId, itemsAffected, itemsInBatch, p.Session);
        }

        public static bool SetBatchEvent(int eventId, int batchId, int formId, int itemsAffected, int itemsInBatch, HttpSessionState session)
		{
            var conn = new SqlConnection("" + session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetBatchEvents",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@FormCode", SqlDbType.Int).Value = formId;
			command.Parameters.Add("@EventID", SqlDbType.Int).Value = eventId;
			command.Parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
			command.Parameters.Add("@NumberOfItemsAffected", SqlDbType.Int).Value = itemsAffected;
			command.Parameters.Add("@NumberOfItemsInBatch", SqlDbType.Int).Value = itemsInBatch;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + session["AuthorOfficeID"]);
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + session["AuthorOfficeID"]);


			string msg;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, session);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

        public static bool SetOrderEvent(int orderCode, int orderEventID, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkSpSetOrderEvent",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };           
            command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;
            command.Parameters.Add("@OrderEventID", SqlDbType.Int).Value = orderEventID;            
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            string msg;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            conn.Close();
            msg = "";
            return true;
        }

        public static bool SetItemOutByOurMessenger(NewOrderModelExt newOrderModelExt, Page p, out string msg)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetItemOutByOurMessenger",

				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
			command.Parameters["@rID"].Size = 1024;
			command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = newOrderModelExt.ItemCode;
			command.Parameters.Add("@BatchID", SqlDbType.Int).Value = newOrderModelExt.BatchID;
			command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = DBNull.Value;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@TakenOutByOurMessenger", SqlDbType.Int).Value = newOrderModelExt.TakenOutByOurMessenger;
			
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

        public static DataTable GetScreeningOrderState(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select statename from v0group where groupcode = " + order,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;
        }

		public static string InsertBulkGiveOutInfo(int customerCode, Page p)
		{
			var result = "";
			using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
			{
				conn.Open();

				var command = new SqlCommand
				{
					CommandText = "spInsertBulkGiveOutInfo",
					Connection = conn,
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = p.Session.Timeout
				};

				command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = customerCode;

				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt.Rows.Count > 0)
				{
					result = dt.Rows[0]["BulkGiveOutId"].ToString();
				}
			}
			return result;
		}

		public static DataTable UpdateBulkGiveOutData(int requestId, string bulkGiveOutRequestId, Page p)
		{
			DataTable result;
			using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
			{
				conn.Open();

				var command = new SqlCommand
				{
					CommandText = "spUpdateBulkGiveOutEntry",
					Connection = conn,
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = p.Session.Timeout
				};

				command.Parameters.Add("@RequestID", SqlDbType.Int).Value = requestId;
				command.Parameters.Add("@BulkGiveOutId", SqlDbType.VarChar).Value = bulkGiveOutRequestId;
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				try
				{
					da.Fill(dt);
					if (dt.Rows.Count > 0)
					{
						result = dt;
					}
					else
						result = null;
				}
				catch (Exception ex)
                {
					result = null;
                }
			}
			return result;
		}

		public static DataTable GetBulkGiveOutList(string bulkGiveOutRequestId, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "Select * from tblsyntheticcustomerentries where BulkGiveOutID= '" + bulkGiveOutRequestId + "'",
				Connection = conn,
				CommandType = CommandType.Text,
				CommandTimeout = p.Session.Timeout
			};
			var cdt = new DataTable();
			try
			{
				var cda = new SqlDataAdapter(command);

				cda.Fill(cdt);
			}
			catch
            {
				conn.Close();
				return null;
            }
			return cdt;
		}

		public static DataTable GetTrackingNumberGiveOutList(string bulkGiveOutRequestId, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "Select * from tblsyntheticcustomerentries where BulkGiveOutID= '" + bulkGiveOutRequestId + "'",
				Connection = conn,
				CommandType = CommandType.Text,
				CommandTimeout = p.Session.Timeout
			};
			var cdt = new DataTable();
			try
			{
				var cda = new SqlDataAdapter(command);

				cda.Fill(cdt);
			}
			catch
			{
				conn.Close();
				return null;
			}
			return cdt;
		}
		public static DataTable GetBulkGiveOutInfo(string bulkRequestId, Page p)
        {
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			DataTable result;
			var command = new SqlCommand
			{
				CommandText = "spGetBulkGiveOutInfo",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@BulkRequestID", SqlDbType.Int).Value = bulkRequestId;
			try
			{
				var cda = new SqlDataAdapter(command);
				var cdt = new DataTable();
				cda.Fill(cdt);
				result = cdt;
			}
			catch (Exception ex)
            {
				result = null;
            }
			finally
            {
				conn.Close();
            }
			return result;
		}
		#endregion

		#region For Customer front In/Out

		public static DataTable GetOrderInOutDetails(string order, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetOrderInOutMessengerSignature",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = int.Parse(order);
			//command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
			//command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);

			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);

			if (dt.Rows.Count == 0) return null;
			//	var result = new OrderModel(dt.Rows[0], order);

			var result = dt;
			conn.Close();
			return result;
		}
		public static string SetOrderInOutDetails(int orderCode, int takeInPersonId, string takeInCaptureSignature, string TakeInRecieptImage, int GiveOutPersonId, string GiveOutCaptureSignature, string GiveOutRecieptImage, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetOrderInOutMessengerSignature",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;

			if (takeInPersonId == 0)
				command.Parameters.Add("@TakeInPersonId", SqlDbType.Int).Value = DBNull.Value;
			else
				command.Parameters.Add("@TakeInPersonId", SqlDbType.Int).Value = takeInPersonId;

			command.Parameters.Add("@TakeInCaptureSignature", SqlDbType.VarChar).Value = takeInCaptureSignature;
			command.Parameters.Add("@TakeInRecieptImage", SqlDbType.VarChar).Value = TakeInRecieptImage;
			if (GiveOutPersonId == 0)
				command.Parameters.Add("@GiveOutPersonId", SqlDbType.Int).Value = DBNull.Value;
			else
				command.Parameters.Add("@GiveOutPersonId", SqlDbType.Int).Value = GiveOutPersonId;

			command.Parameters.Add("@GiveOutCaptureSignature", SqlDbType.VarChar).Value = GiveOutCaptureSignature;
			command.Parameters.Add("@GiveOutRecieptImage", SqlDbType.VarChar).Value = GiveOutRecieptImage;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			string msg;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return msg;
			}
			conn.Close();
			msg = "";
			return msg;
		}

		/*public static string SetOrderInOutDetails(int orderCode,int? takeInPersonId, string takeInCaptureSignature, string TakeInRecieptImage,int? GiveOutPersonId,string GiveOutCaptureSignature,string GiveOutRecieptImage, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetOrderInOutDetails",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;
			command.Parameters.Add("@TakeInPersonId", SqlDbType.Int).Value = takeInPersonId;
			command.Parameters.Add("@TakeInCaptureSignature", SqlDbType.Int).Value = takeInCaptureSignature;
			command.Parameters.Add("@TakeInRecieptImage", SqlDbType.Int).Value = TakeInRecieptImage;
			command.Parameters.Add("@GiveOutPersonId", SqlDbType.Int).Value = GiveOutPersonId;
			command.Parameters.Add("@GiveOutCaptureSignature", SqlDbType.Int).Value = GiveOutCaptureSignature;
			command.Parameters.Add("@GiveOutRecieptImage", SqlDbType.Int).Value = GiveOutRecieptImage;
			command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			string msg;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return msg;
			}
			conn.Close();
			msg = "";
			return msg;
		}*/

		public static bool SetOrderTakeInDetails(int orderCode, string takeInCaptureSignature, int takeInPersonId, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetOrderInOutMessengerSignature",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;
			command.Parameters.Add("@TakeInCaptureSignature", SqlDbType.VarChar).Value = takeInCaptureSignature;
			command.Parameters.Add("@TakeInPersonId", SqlDbType.Int).Value = takeInPersonId;

			string msg;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		public static bool SetOrderGiveOutDetails(int orderCode, string giveOutCaptureSignature, int giveOutPersonId, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetOrderInOutMessengerSignature",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;
			command.Parameters.Add("@GiveOutCaptureSignature", SqlDbType.VarChar).Value = giveOutCaptureSignature;
			command.Parameters.Add("@GiveOutPersonId", SqlDbType.Int).Value = giveOutPersonId;

			string msg;
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				conn.Close();
				msg = ex.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				return false;
			}
			conn.Close();
			msg = "";
			return true;
		}

		#endregion
		public static bool PrintAcceptanceLabel(DataTable dataInput, Page p)
        {
			DataRow dr = dataInput.Rows[0];
			string customerName = null;
			string requestsList = null;
			customerName = dr["CustomerName"].ToString();
			requestsList = dr["Requests"].ToString();
			string machineName = dr["MachineName"].ToString();
			string sumQty = dr["SumOfQty"].ToString();
			string totalItems = dr["TotalItems"].ToString();
			string messenger = dr["Messenger"].ToString();
			string carrier = dr["Carrier"].ToString();
			string carrierTrackingNumber = dr["CarrierTrackingNumber"].ToString();
			string authorName = dr["AuthorName"].ToString();
			string serviceTypeID = "7";
			string pName = null;
			if (p.Session["PName"] != null)
				pName = p.Session["PName"].ToString();
			try
			{
				serviceTypeID = dr["ServiceType"].ToString();
			}
			catch
			{ }

			string serviceType = (serviceTypeID == "1") ? "Lab Services" : "Screening/QC";
			string s = "";

			s += customerName + " at " + DateTime.Now.ToString() + @": Submitted to GSI, ";
			if (carrier == "N/A" || carrier == "")
				s += "Messenger: " + messenger + Environment.NewLine;
			else if (carrier != "")
				s += "Carrier: " + carrier + @", Tracking Number: " + carrierTrackingNumber + Environment.NewLine;
			s += "REQUEST MEMO         QTY  RETAILER" + Environment.NewLine;
			s += @"----------------------------------------------------------------" + Environment.NewLine;
			//if (messenger is null)
			//    s += "Carrier: " + carrier + @", CarrierTrackingNumber: " + carrierTrackingNumber + Environment.NewLine;
			//else
			//    s += "Messenger: " + messenger + Environment.NewLine;
			requestsList = requestsList.Replace("|", "               ");
			s += requestsList + Environment.NewLine;
			s += @"----------------------------------------------------------------" + Environment.NewLine;
			s += @"Total Memos: " + totalItems + @", Total Quantity: " + sumQty + Environment.NewLine + Environment.NewLine;
			s += "Accepted by: " + authorName + Environment.NewLine;
			s += "Service Type: " + serviceType;
			//string s = "string to print";

			PrintDocument prt = new PrintDocument();
			prt.PrintPage += delegate (object sender1, PrintPageEventArgs e1)
			{
				e1.Graphics.DrawString(s, new System.Drawing.Font("Times New Roman", 12), new SolidBrush(Color.Black), new RectangleF(0, 0, prt.DefaultPageSettings.PrintableArea.Width, prt.DefaultPageSettings.PrintableArea.Height));
			};
			try
			{
				if (pName != null)
					prt.PrinterSettings.PrinterName = pName;
				prt.PrinterSettings.Copies = 2;
				try
				{
					prt.Print();
					//doc.PrintDocument.Print();
					//doc.SaveToFile(@"c:\temp\abc.pdf");
					//Spire.Pdf.PdfDocument printPdf = new Spire.Pdf.PdfDocument(@"c:\temp\abc1.pdf");
					//printPdf.PrintDocument.PrinterSettings.PrinterName = pName;//"Microsoft Print to PDF";
					//printPdf.PrintDocument.Print();
					//p.Print();
				}
				catch (Exception ex)
				{
					prt.Print();
				}
				//p.Print();

			}
			catch (Exception ex)
			{
				throw new Exception("Exception Occured While Printing", ex);
			}
			return true;
        }
	}
} 