﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.SyntheticScreening;

//using System.Text.RegularExpressions;
//using Excel = Microsoft.Office.Interop.Excel;
//using System.Globalization;
//using Corpt.Models.Stats;
//using Corpt.Models.SyntheticScreening;
//using System.IO;
//using System.Xml;

namespace Corpt.Utilities
{
    #region Synhtetic Order Status

    public enum SynhteticOrderStatus
    {
        OrderOpen = 1,
        BatchOpen = 2,
        ScreeningStart = 6,
        ScreeningComplete = 8,
        TestingComplete = 11,
        PackingFinished = 14,
        Itemized = 15,
        OrderComplete = 16
    }

    #endregion

    public class SyntheticScreeningUtils : BaseQueryUtils
    {

        #region Synthetic Front Entry

        //public static DataTable GetSyntheticCustomerEntries(string requestId, Page p)
        //{
        //    DataTable dt = new DataTable();
        //    var conn = new SqlConnection("" + p.Session["ConnectionString"]);
        //    conn.Open();

        //    try
        //    {
        //        var command = new SqlCommand
        //        {
        //            CommandText = "spGetSyntheticCustomerEntriesByRequestID",
        //            Connection = conn,
        //            CommandType = CommandType.StoredProcedure,
        //            CommandTimeout = p.Session.Timeout
        //        };
        //        command.Parameters.AddWithValue("@RequestID", requestId);
        //        var da = new SqlDataAdapter(command);
        //        da.Fill(dt);
        //        LogUtils.UpdateSpLogWithParameters(command, p);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtils.UpdateSpLogWithException(ex.Message, p);
        //        dt = null;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return dt;
        //}

        public static DataSet IsSyntheticOrderFinishedPacking(int orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();

            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spIsSyntheticOrderFinishedPacking",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@OrderCode", orderCode);
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        public static DataSet SetSyntheticCustomerOrder(int requestID, string orderCode, int serviceTimeCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticCustomerOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@RequestID", requestID);
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            command.Parameters.AddWithValue("@ServiceTimeCode", serviceTimeCode);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
            conn.Close();
            return ds;
        }

        public static bool SetMemoNumber(string groupId, string memo, Page p)
        {
            SqlCommand command = new SqlCommand("spSetGroupMemoNumber");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@GroupID", groupId));
            command.Parameters.Add(new SqlParameter("@Name", memo));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", Int32.Parse("" + p.Session["AuthorOfficeID"])));
            command.Parameters.Add(new SqlParameter("@AuthorID", Int32.Parse("" + p.Session["ID"])));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", Int32.Parse("" + p.Session["AuthorOfficeID"])));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }//SetMemoNumber

        public static bool SaveTakeOutPhotoPath(string order, string photoPath, Page p)
        {
            SqlCommand command = new SqlCommand("spSetTakeOutPhotoPath");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(order)));
            command.Parameters.Add(new SqlParameter("@PhotoPath", photoPath));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }//SetMemoNumber

        public static bool UpdateTakeOutMessengerID(string order, string id, Page p)
        {
            SqlCommand command = new SqlCommand("spUpdateTakeOutID");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(order)));
            command.Parameters.Add(new SqlParameter("@ID", id));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }//SetMemoNumber
        public static string GetTakeOutPhotoPath(string id, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from tblOrderOut where personID = " + id,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            string result = null;
            if (cdt.Rows.Count > 0)
            {
                DataRow dr = cdt.Rows[0];
                result = dr["TakeOutMessengerPhoto"].ToString();
            }
            conn.Close();
            return result;
        }//SetMemoNumber

        #endregion

        #region Synthetic Retailers
        public static List<CustomerModel> GetCustomers(Page p)
        {
            return GetCustomers(p, false);
        }

        public static List<CustomerModel> GetCustomers(Page p, bool addAll)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetCustomers,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new CustomerModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            if (addAll)
            {
                result.Insert(0, new CustomerModel { CustomerId = null, CustomerName = "(All)" });
            }
            conn.Close();
            return result;
        }

        public static DataTable GetRetailers(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetRetailers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();


            return dt;
        }
        /*IvanB start 12/09*/
        public static string GetMeasureIDByName(Page p, string MeasureName)
        {
            var result = "";
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spGetMeasureIDByName",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@MeasureName", SqlDbType.VarChar).Value = MeasureName;

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? "" : dt.Rows[0].ItemArray[dt.Columns.IndexOf("MeasureID")].ToString());
            }
            return result;
        }

        public static List<MeasureValueModel> GetMeasureValueListByID(Page p, int measureID)
        {
            var result = new List<MeasureValueModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spGetMeasureValueListByID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@MeasureID", SqlDbType.Int).Value = measureID;

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? new List<MeasureValueModel>() : (from DataRow row in dt.Rows select new MeasureValueModel(row)).ToList());
            }
            return result;
        }
        /*IvanB end 12/09*/

        public static DataTable GetSkuOnly(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetSkuOnly",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();


            return dt;
        }

        public static DataTable DeleteSelectedSku(string skuName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spDeleteSelectedSku",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SkuName", skuName);
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();


            return dt;
        }
        /*IvanB start 12/09*/
        public static List<MeasureValueModel> DeleteSelectedMeasureValue(int valueID, int measureID, Page p)
        {
            var result = new List<MeasureValueModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spDeleteSelectedMeasureValue",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@MeasureValueID", SqlDbType.Int).Value = valueID;
                command.Parameters.Add("@MeasureID", SqlDbType.Int).Value = measureID;

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                try
                {
                    da.Fill(dt);
                }
                catch (Exception ex)
                { 
                
                }                

                result = (dt.Rows.Count == 0 ? new List<MeasureValueModel>() : (from DataRow row in dt.Rows select new MeasureValueModel(row)).ToList());
            }
            return result;
        }
        /*IvanB end 12/09*/
        public static DataTable AddNewSku(string newSku, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spAddNewSku",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SkuName", newSku);
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();


            return dt;
        }
        /*IvanB start 12/09*/
        public static List<MeasureValueModel> AddNewMeasureValue(Page p, int measureID, string measureValueName)
        {
            var result = new List<MeasureValueModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spAddNewMeasureValue",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@MeasureValueName", SqlDbType.VarChar).Value = measureValueName;
                command.Parameters.Add("@MeasureID", SqlDbType.Int).Value = measureID;

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? new List<MeasureValueModel>() : (from DataRow row in dt.Rows select new MeasureValueModel(row)).ToList());
            }
            return result;
        }
        /*IvanB end 12/09*/
        public static DataTable GetBulkCutGrades(long order, int batch, int item, int checkedBox, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetBulkCutGrade",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", order);
            command.Parameters.AddWithValue("@BatchCode", batch);
            command.Parameters.AddWithValue("@ItemCode", item);
            command.Parameters.AddWithValue("@Ideal", checkedBox);
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }

            return dt;
        }
        public static string GetContactEmail(string contactId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from v0person where personid = " + contactId,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            string result = null;
            if (cdt.Rows.Count > 0)
            {
                DataRow dr = cdt.Rows[0];
                result = dr["Email"].ToString();
            }
            conn.Close();
            return result;
        }

        public static List<SyntheticRetailerModel> GetSyntheticCustomerRetailer(SyntheticRetailerModel retailer, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticCustomerRetailer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(retailer.customerCode));
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                var msg = ex.Message;
            }
            conn.Close();

            var result = (from DataRow row in dt.Rows select new SyntheticRetailerModel(row)).ToList();

            return result;
        }

        //public static bool SetRetailer(SyntheticRetailerModel retailer, Page p)
        //{
        //    bool result = true;
        //    var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
        //    conn.Open();
        //    var command = new SqlCommand
        //    {
        //        CommandText = "spSetRetailers",
        //        Connection = conn,
        //        CommandType = CommandType.StoredProcedure,
        //        CommandTimeout = p.Session.Timeout
        //    };
        //    command.Parameters.AddWithValue("@RetailerName", retailer.retailerName);
        //    LogUtils.UpdateSpLogWithParameters(command, p);
        //    try
        //    {
        //        command.ExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtils.UpdateSpLogWithException(ex.Message, p);
        //        result = false;
        //    }
        //    conn.Close();
        //    return result;
        //}

        public static bool SetSyntheticCustomerRetailer(SyntheticRetailerModel retailer, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticCustomerRetailer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(retailer.customerCode));
            command.Parameters.AddWithValue("@RetailerId", retailer.retailerId);
            command.Parameters.AddWithValue("@CustomerName", retailer.customerName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        public static bool DelSyntheticCustomerRetailer(SyntheticRetailerModel retailer, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spDelSyntheticCustomerRetailer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(retailer.customerCode));
            command.Parameters.AddWithValue("@RetailerId", Convert.ToInt16(retailer.retailerId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        #endregion

        #region Order History & Status

        public static string SetSyntheticStatus(SyntheticOrderHistoryModel objSyntheticOrderHistoryModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticStatus",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", objSyntheticOrderHistoryModel.OrderCode);
            command.Parameters.AddWithValue("@StatusId", objSyntheticOrderHistoryModel.StatusId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                msg = ex.Message;
            }
            conn.Close();
            return msg;
        }

        public static string SetSyntheticShippingAccountNumber(int orderCode, string accountNumber, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticShippingAccountNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            command.Parameters.AddWithValue("@AccountNumber", accountNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                msg = ex.Message;
            }
            conn.Close();
            return msg;
        }

        public static string SetSyntheticOrderHistory(SyntheticOrderHistoryModel objSyntheticOrderHistoryModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticOrderHistory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", objSyntheticOrderHistoryModel.OrderCode);

            if (objSyntheticOrderHistoryModel.BatchCode == 0)
                command.Parameters.AddWithValue("@BatchCode", DBNull.Value);
            else
                command.Parameters.AddWithValue("@BatchCode", objSyntheticOrderHistoryModel.BatchCode);

            if (objSyntheticOrderHistoryModel.ItemQty == 0)
                command.Parameters.AddWithValue("@ItemQty", DBNull.Value);
            else
                command.Parameters.AddWithValue("@ItemQty", objSyntheticOrderHistoryModel.ItemQty);

            if (objSyntheticOrderHistoryModel.AssignedTo == 0)
                command.Parameters.AddWithValue("@AssignedTo", DBNull.Value);
            else
                command.Parameters.AddWithValue("@AssignedTo", objSyntheticOrderHistoryModel.AssignedTo);

            command.Parameters.AddWithValue("@StatusId", objSyntheticOrderHistoryModel.StatusId);
			command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                msg = ex.Message;
            }
            conn.Close();
            return msg;
        }

        public static DataSet GetSyntheticOrderHistoryByOrderCode(int OrderCode, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderHistoryByOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", OrderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }

        #endregion

        #region Synthetic screening order details

        public static DataSet GetSyntheticCustomerEntries(string requestId, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();

            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCustomerEntriesByRequestIDNew",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                if (requestId != "")
                    command.Parameters.Add(new SqlParameter("@RequestID", requestId));
                else
                    command.Parameters.Add(new SqlParameter("@RequestID", DBNull.Value));

                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        public static DataTable GetRequestsList(string requestGroup, Page p)
        {
            var cdt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = @"Select ID, RequestID, GSIOrder from tblSyntheticCustomerEntries where RequestGroup = '" + requestGroup + @"'",
                    Connection = conn,
                    CommandType = CommandType.Text,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);
                var cda = new SqlDataAdapter(command);

                cda.Fill(cdt);
            }
            catch (Exception ex)
            {
                cdt = null;
            }
            finally
            {
                conn.Close();
            }
            return cdt;
        }
        public static DataSet GetSyntheticCustomerEntriesByOrder(string order, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();

            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCustomerEntriesByOrder",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add(new SqlParameter("@Order", order));
                
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        public static DataSet GetSyntheticCustomerEntries(string requestId, string memoNum, string orderNumber, Page p)
		{
			DataSet ds = new DataSet();
			var conn = new SqlConnection("" + p.Session["ConnectionString"]);
			conn.Open();

			try
			{
				var command = new SqlCommand
				{
					CommandText = "spGetSyntheticEntries",
					Connection = conn,
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = p.Session.Timeout
				};

				if (requestId != "")
					command.Parameters.Add(new SqlParameter("@RequestID", requestId));
				else
					command.Parameters.Add(new SqlParameter("@RequestID", DBNull.Value));

				if (memoNum != "")
					command.Parameters.Add(new SqlParameter("@MemoNum", memoNum));
				else
					command.Parameters.Add(new SqlParameter("@MemoNum", DBNull.Value));

				if (orderNumber != "")
					command.Parameters.Add(new SqlParameter("@OrderCode", orderNumber));
				else
					command.Parameters.Add(new SqlParameter("@OrderCode", DBNull.Value));

				var da = new SqlDataAdapter(command);
				da.Fill(ds);
				LogUtils.UpdateSpLogWithParameters(command, p);
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				ds = null;
			}
			finally
			{
				conn.Close();
			}
			return ds;
		}

        public static DataSet GetSyntheticEntriesWithOutRequest(string orderNumber, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();

            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticEntriesWithOutRequest",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                if (orderNumber != "")
                    command.Parameters.Add(new SqlParameter("@OrderCode", orderNumber));
                else
                    command.Parameters.Add(new SqlParameter("@OrderCode", DBNull.Value));

                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        public static DataSet GetSyntheticUser(Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticUser",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
			try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }

        public static DataSet GetSyntheticOrder(Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
			command.Parameters.AddWithValue("@StatusID", SynhteticOrderStatus.BatchOpen);
			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
			LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }
		public static DataSet GetSyntheticOrderViewBy(int OrderCode, string CreateDate, string MemoNum, int CustomerCode, int PositionCode, Page p)
		{
			DataSet ds = new DataSet();
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();

			var command = new SqlCommand
			{
				CommandText = "spGetSyntheticOrderViewBy",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@OrderCode", OrderCode);
			command.Parameters.AddWithValue("@CreateDate", CreateDate);
			command.Parameters.AddWithValue("@MemoNum", MemoNum);
			command.Parameters.AddWithValue("@CustomerCode", CustomerCode);
			command.Parameters.AddWithValue("@PositionCode", PositionCode);

			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var da = new SqlDataAdapter(command);
				da.Fill(ds);
				conn.Close();
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				ds = null;
			}
			return ds;
		}

		public static DataSet GetSyntheticOrderByOrderCode(int orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderByOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }

        public static DataSet GetSyntheticOrderBatchesByOrderCode(int orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderBatchesByOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }

        public static DataSet GetSyntheticOrderBatchStatusByOrderCode(int orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderBatchStatusByOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }

        public static DataSet GetSyntheticOrderByUserID(int UserID, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderByUserID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@UserID", UserID);

            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            conn.Close();
            return ds;
        }

        public static DataSet GetSyntheticOrderStatus(int Code, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();

            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticOrderStatus",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@Code", Code);
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        public static DataSet GetSyntheticOrderStatusByOrderCode(int OrderCode, string Memo, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticOrderStatusByOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GSIOrder", OrderCode);
            command.Parameters.AddWithValue("@Memo", Memo);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }

        public static bool SetSyntheticOrderComment(int OrderCode, string Comment, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticOrderComment",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", OrderCode);
            command.Parameters.AddWithValue("@Comment", Comment);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        public static DataSet GetSyntheticUserStatus(Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticUserStatus",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
			command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
			command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
			LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            return ds;
        }

       

        public static DataSet GetSyntheticBatch(int OrderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", OrderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                ds = null;
            }
            conn.Close();
            return ds;
        }

        #endregion

        #region Fill droup down fields

        public static DataTable GetSyntheticScreeningInstrument(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticScreeningInstrument",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetSyntheticCertifiedBy(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCertifiedBy",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        //public static DataTable GetSyntheticServiceTime(Page p)
        //{
        //    DataTable dt = new DataTable();
        //    var conn = new SqlConnection("" + p.Session["ConnectionString"]);
        //    conn.Open();
        //    try
        //    {
        //        var command = new SqlCommand
        //        {
        //            CommandText = "spGetSyntheticServiceTime",
        //            Connection = conn,
        //            CommandType = CommandType.StoredProcedure,
        //            CommandTimeout = p.Session.Timeout
        //        };
        //        var da = new SqlDataAdapter(command);
        //        da.Fill(dt);
        //        LogUtils.UpdateSpLogWithParameters(command, p);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtils.UpdateSpLogWithException(ex.Message, p);
        //        dt = null;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return dt;
        //}

        //public static DataTable GetCarriers(Page p)
        //{
        //    DataTable dt = new DataTable();
        //    var conn = new SqlConnection("" + p.Session["ConnectionString"]);
        //    conn.Open();
        //    try
        //    {
        //        var command = new SqlCommand
        //        {
        //            CommandText = "spGetCarriers",
        //            Connection = conn,
        //            CommandType = CommandType.StoredProcedure,
        //            CommandTimeout = p.Session.Timeout
        //        };
        //        command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
        //        command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);

        //        var da = new SqlDataAdapter(command);
        //        da.Fill(dt);
        //        LogUtils.UpdateSpLogWithParameters(command, p);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtils.UpdateSpLogWithException(ex.Message, p);
        //        dt = null;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return dt;
        //}

        #endregion

        #region Itemizing

        public static bool AddSyntheticBatch(int orderCode, string customerProgramName, int batchCode,  int quantity, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spAddSyntheticBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            command.Parameters.AddWithValue("@CustomerProgramName", customerProgramName);
            command.Parameters.AddWithValue("@BatchCode", batchCode);          
            command.Parameters.AddWithValue("@ItemsQuantity", quantity);
            command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        public static bool SetSyntheticScreeningInstrument(int orderCode, int certifiedBy, string screeningInstrument, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticScreeningInstrument",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
           
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            //command.Parameters.AddWithValue("@ProductType", productType);
            command.Parameters.AddWithValue("@CertifiedBy", certifiedBy);
            command.Parameters.AddWithValue("@ScreeningInstrument", screeningInstrument);

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        public static bool AddSyntheticItem(SyntheticItemModel item, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spAddItemSynth",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
     
            command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
            command.Parameters["@rId"].Size = 1024;

            command.Parameters.Add("@BatchID", SqlDbType.Int).Value = item.BatchID;
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = item.ItemCode;
            command.Parameters.Add("@LotNumber", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@StateID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@StateTargetID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@PrevItemCode", SqlDbType.Int).Value = item.ItemCode;
            command.Parameters.Add("@PrevBatchCode", SqlDbType.Int).Value = item.BatchCode;
            command.Parameters.Add("@PrevGroupCode", SqlDbType.Int).Value = item.GroupCode;
            command.Parameters.Add("@PrevOrderCode", SqlDbType.Int).Value = item.GroupCode;
            command.Parameters.Add("@ItemComment", SqlDbType.VarChar).Value = DBNull.Value;

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);           
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }

        #endregion

        #region Synthetic Data Entry
        // Retailer
        public static DataTable GetSyntheticServiceType(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticServiceType",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetSyntheticServiceTypeByRetailer(int retailerID, Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticServiceTypeByRetailer",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@RetailerID", retailerID);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        
        public static DataTable GetAllRetailers(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetAllRetailers",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetAllSyntheticCertifiedBy(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCertifiedBy",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetRetailer(SyntheticRetailerModel retailer, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetRetailers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@RetailerId", retailer.retailerId);
            command.Parameters.AddWithValue("@RetailerName", retailer.retailerName);
            command.Parameters.AddWithValue("@ServiceTypeCodeSet", retailer.ServiceTypeCodeSet);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }
        // Setvice Type
        public static DataTable GetSyntheticCategoryByServiceType(int serviceTypeCode, Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCategoryByServiceType",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@ServiceTypeCode", serviceTypeCode);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetSyntheticCategory(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCategory",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetRetailerListByCustomerCode(string custCode, Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticCustomerRetailer",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@CustomerCode", custCode);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetAllServiceType(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetAllServiceType",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetAllCategory(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetAllCategory",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetSyntheticServiceTimeByCategory(string categoryCode, Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticServiceTimeByCategory",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@CategoryCode", categoryCode);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetSyntheticServiceType(SyntheticServiceTypeModel serviceType, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticServiceType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ServiceTypeID", serviceType.serviceTypeID);
            command.Parameters.AddWithValue("@ServiceTypeName", serviceType.serviceTypeName);
            command.Parameters.AddWithValue("@CategoryCodeSet", serviceType.categoryCodeSet);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        // Category
        public static DataTable GetScreeningCategory(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetScreeningCategory",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetSyntheticCategory(SyntheticCategoryModel category, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticCategory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CategoryId", category.categoryId);
            command.Parameters.AddWithValue("@CategoryName", category.categoryName);
            command.Parameters.AddWithValue("@ServiceTimeCodeSet", category.ServiceTimeCodeSet);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        //Service Time

        public static DataTable GetSyntheticServiceTime(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticServiceTime",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetSyntheticServiceTime(SyntheticServiceTimeModel serviceTime, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticServiceTime",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ServiceTimeID", serviceTime.ServiceTimeId);
            command.Parameters.AddWithValue("@ServiceTimeName", serviceTime.ServiceTimeName);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        //Delivery Method

        public static DataTable GetSyntheticDeliveryMethod(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetSyntheticDeliveryMethod",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetSyntheticDeliveryMethod(SyntheticDeliveryMethodModel deliveryMethod, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticDeliveryMethod",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DeliveryMethodID", deliveryMethod.DeliveryMethodId);
            command.Parameters.AddWithValue("@DeliveryMethodName", deliveryMethod.DeliveryMethodName);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        //Shipping Method / Carriers
        public static DataTable GetCarriers(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetCarriers",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetCarriers(SyntheticShippingCourierModel shippingCourier, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetCarriers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CarrierID", shippingCourier.CarrierID);
            command.Parameters.AddWithValue("@CarrierName", shippingCourier.CarrierName);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        // Screening Instrument

        public static DataTable SetSyntheticInstruments(SyntheticScreeningInstrumentModel screeningInstrument, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticInstruments",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@InstrumentID", screeningInstrument.InstrumentID);
            command.Parameters.AddWithValue("@InstrumentName", screeningInstrument.InstrumentName);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

        // CertifiedBy

        public static DataTable SetSyntheticCertifiedBy(SyntheticCertifiedByModel certifiedBy, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticCertifiedBy",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CertifiedID", certifiedBy.CertifiedID);
            command.Parameters.AddWithValue("@CertifiedBy", certifiedBy.CertifiedBy);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }

		// JewelryType
		public static DataTable GetSyntheticJewelryType(Page p)
		{
			DataTable dt = new DataTable();
			var conn = new SqlConnection("" + p.Session["ConnectionString"]);
			conn.Open();
			try
			{
				var command = new SqlCommand
				{
					CommandText = "spGetSyntheticJewelryType",
					Connection = conn,
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = p.Session.Timeout
				};
				var da = new SqlDataAdapter(command);
				da.Fill(dt);
				LogUtils.UpdateSpLogWithParameters(command, p);
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				dt = null;
			}
			finally
			{
				conn.Close();
			}
			return dt;
		}
		public static DataTable SetSyntheticJewelryType(SyntheticJewelryTypeModel jewelryType, Page p)
		{
			var dt = new DataTable();
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetSyntheticJewelryType",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@JewelryTypeID", jewelryType.JewelryTypeID);
			command.Parameters.AddWithValue("@JewelryTypeName", jewelryType.JewelryTypeName);
			command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
			command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var da = new SqlDataAdapter(command);
				da.Fill(dt);
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				dt = null;
			}
			conn.Close();
			return dt;
		}


		#endregion

		#region DataView
		public static DataTable GetSyntheticDataView(SyntheticDataViewModel objDataView, Page p)
		{
			DataTable dt = new DataTable();
			var conn = new SqlConnection("" + p.Session["ConnectionString"]);
			conn.Open();
			try
			{
				var command = new SqlCommand
				{
					CommandText = "spGetSyntheticDataView",
					Connection = conn,
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = p.Session.Timeout
				};
				command.Parameters.AddWithValue("@CustomerCode", objDataView.CustomerCode);
				command.Parameters.AddWithValue("@RetailerID", objDataView.RetailerID);
				command.Parameters.AddWithValue("@PONum", objDataView.PONum);
				command.Parameters.AddWithValue("@MemoNum", objDataView.MemoNum);
				command.Parameters.AddWithValue("@StyleName", objDataView.StyleName);
				command.Parameters.AddWithValue("@SKUName", objDataView.SKUName);
				command.Parameters.AddWithValue("@PositionID", objDataView.Status);
				command.Parameters.AddWithValue("@CertifiedCode", objDataView.CertifiedBy);
				command.Parameters.AddWithValue("@ServiceTypeCode", objDataView.ServiceTypeCode);
				command.Parameters.AddWithValue("@CategoryCode", objDataView.CategoryCode);
				command.Parameters.AddWithValue("@ServiceTimeCode", objDataView.ServiceTimeCode);
				command.Parameters.AddWithValue("@DeliveryMethodCode", objDataView.DeliveryMethodCode);
				command.Parameters.AddWithValue("@CarrierCode", objDataView.CarrierCode);
				command.Parameters.AddWithValue("@ScreeningInstrument", objDataView.InstrumentCode);
				command.Parameters.AddWithValue("@JewelryTypeCode", objDataView.JewelryTypeCode);
				command.Parameters.AddWithValue("@CreateDate", objDataView.CreateDate);

				var da = new SqlDataAdapter(command);
				da.Fill(dt);
				LogUtils.UpdateSpLogWithParameters(command, p);
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				dt = null;
			}
			finally
			{
				conn.Close();
			}
			return dt;
		}

        public static DataSet GetSyntheticCustomerMemo(string Memo, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticCustomerMemo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@MemoNum", Memo);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }

        #endregion

        #region Screening / Testing / Packing
        public static bool SetSyntheticBatch(SyntheticBatchModel objSyntheticBatchModel, Page p)
		{
			bool result = true;
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetSyntheticBatch",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@BatchCode", objSyntheticBatchModel.BatchCode);
			command.Parameters.AddWithValue("@OrderCode", objSyntheticBatchModel.OrderCode);
			command.Parameters.AddWithValue("@StatusID", objSyntheticBatchModel.StatusID);
			command.Parameters.AddWithValue("@UpdateID", objSyntheticBatchModel.UpdateID);
			
			//Screener
			command.Parameters.AddWithValue("@ScreenerID", objSyntheticBatchModel.ScreenerID);

			if (objSyntheticBatchModel.ScreeningInstrumentID == 0)
				command.Parameters.AddWithValue("@ScreeningInstrumentID", DBNull.Value);
			else
				command.Parameters.AddWithValue("@ScreeningInstrumentID", objSyntheticBatchModel.ScreeningInstrumentID);

			if (objSyntheticBatchModel.ScreenerPassItems == 0)
				command.Parameters.AddWithValue("@ScreenerPassItems", 0);
			else
				command.Parameters.AddWithValue("@ScreenerPassItems", objSyntheticBatchModel.ScreenerPassItems);

			if (objSyntheticBatchModel.ScreenerFailItems == 0)
				command.Parameters.AddWithValue("@ScreenerFailItems", 0);
			else
				command.Parameters.AddWithValue("@ScreenerFailItems", objSyntheticBatchModel.ScreenerFailItems);

			if (objSyntheticBatchModel.ScreenerFailedStones == 0)
				command.Parameters.AddWithValue("@ScreenerFailedStones", 0);
			else
				command.Parameters.AddWithValue("@ScreenerFailedStones", objSyntheticBatchModel.ScreenerFailedStones);

			if (objSyntheticBatchModel.ScreenerFailedStonesPerItem == null)
				command.Parameters.AddWithValue("@ScreenerFailedStonesPerItem", DBNull.Value);
			else
				command.Parameters.AddWithValue("@ScreenerFailedStonesPerItem", objSyntheticBatchModel.ScreenerFailedStonesPerItem);

			if (objSyntheticBatchModel.ScreenerComments == null)
				command.Parameters.AddWithValue("@ScreenerComments", DBNull.Value);
			else
				command.Parameters.AddWithValue("@ScreenerComments", objSyntheticBatchModel.ScreenerComments);


			//Tester
			command.Parameters.AddWithValue("@TesterID", objSyntheticBatchModel.TesterID);

			if (objSyntheticBatchModel.TestingInstrumentID == 0)
				command.Parameters.AddWithValue("@TestingInstrumentID", 0);
			else
				command.Parameters.AddWithValue("@TestingInstrumentID", objSyntheticBatchModel.TestingInstrumentID);

			if (objSyntheticBatchModel.TesterPassItems == 0)
				command.Parameters.AddWithValue("@TesterPassItems", 0);
			else
				command.Parameters.AddWithValue("@TesterPassItems", objSyntheticBatchModel.TesterPassItems);

			if (objSyntheticBatchModel.TesterFailItems == 0)
				command.Parameters.AddWithValue("@TesterFailItems", 0);
			else
				command.Parameters.AddWithValue("@TesterFailItems", objSyntheticBatchModel.TesterFailItems);

			if (objSyntheticBatchModel.TesterSyntheticStones == 0)
				command.Parameters.AddWithValue("@TesterSyntheticStones", 0);
			else
				command.Parameters.AddWithValue("@TesterSyntheticStones", objSyntheticBatchModel.TesterSyntheticStones);

			if (objSyntheticBatchModel.TesterSuspectStones == 0)
				command.Parameters.AddWithValue("@TesterSuspectStones", 0);
			else
				command.Parameters.AddWithValue("@TesterSuspectStones", objSyntheticBatchModel.TesterSuspectStones);

			if (objSyntheticBatchModel.TesterFailedStones == 0)
				command.Parameters.AddWithValue("@TesterFailedStones", 0);
			else
				command.Parameters.AddWithValue("@TesterFailedStones", objSyntheticBatchModel.TesterFailedStones);

			if (objSyntheticBatchModel.TesterFailedStonesPerItem == null)
				command.Parameters.AddWithValue("@TesterFailedStonesPerItem", DBNull.Value);
			else
				command.Parameters.AddWithValue("@TesterFailedStonesPerItem", objSyntheticBatchModel.TesterFailedStonesPerItem);

			if (objSyntheticBatchModel.TesterComments == null)
				command.Parameters.AddWithValue("@TesterComments", DBNull.Value);
			else
				command.Parameters.AddWithValue("@TesterComments", objSyntheticBatchModel.TesterComments);

			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				result = false;
			}
			conn.Close();
			return result;
		}

		public static bool SetSyntheticOrderPacking(SyntheticBatchModel objSyntheticBatchModel, Page p)
		{
			bool result = true;
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetSyntheticOrderPacking",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@OrderCode", objSyntheticBatchModel.OrderCode);
			if (objSyntheticBatchModel.PackingTotalBlueBagsPerOrder == 0)
				command.Parameters.AddWithValue("@PackingTotalBlueBagsPerOrder", 0);
			else
				command.Parameters.AddWithValue("@PackingTotalBlueBagsPerOrder", objSyntheticBatchModel.PackingTotalBlueBagsPerOrder);

			if (objSyntheticBatchModel.PackingTotalItemsFailPerOrder == 0)
				command.Parameters.AddWithValue("@PackingTotalItemsFailPerOrder", 0);
			else
				command.Parameters.AddWithValue("@PackingTotalItemsFailPerOrder", objSyntheticBatchModel.PackingTotalItemsFailPerOrder);

			if (objSyntheticBatchModel.PackingTotalStonesFailPerOrder == 0)
				command.Parameters.AddWithValue("@PackingTotalStonesFailPerOrder", 0);
			else
				command.Parameters.AddWithValue("@PackingTotalStonesFailPerOrder", objSyntheticBatchModel.PackingTotalStonesFailPerOrder);

			if (objSyntheticBatchModel.PackingComment == null)
				command.Parameters.AddWithValue("@PackingComment", DBNull.Value);
			else
				command.Parameters.AddWithValue("@PackingComment", objSyntheticBatchModel.PackingComment);

			if (objSyntheticBatchModel.PackingBillingInfo == null)
				command.Parameters.AddWithValue("@PackingBillingInfo", DBNull.Value);
			else
				command.Parameters.AddWithValue("@PackingBillingInfo", objSyntheticBatchModel.PackingBillingInfo);

			command.Parameters.AddWithValue("@PackerID", objSyntheticBatchModel.PackerID);
			command.Parameters.AddWithValue("@StatusID", objSyntheticBatchModel.StatusID);

			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				LogUtils.UpdateSpLogWithException(ex.Message, p);
				result = false;
			}
			conn.Close();
			return result;
		}

        public static DataSet SetSyntheticCustomerEntries(SyntheticCustomerEntriesModel objCustomerEntrie, Page p)
        {
            DataSet result = new DataSet();
            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand("spSetSyntheticCustomerEntries");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@ID", objCustomerEntrie.ID));

            if (objCustomerEntrie.MemoNum != null && objCustomerEntrie.MemoNum != "")
                command.Parameters.Add(new SqlParameter("@MemoNum", objCustomerEntrie.MemoNum));
            else
                command.Parameters.Add(new SqlParameter("@MemoNum", DBNull.Value));

            if (objCustomerEntrie.PONum != null || objCustomerEntrie.PONum != "")
                command.Parameters.Add(new SqlParameter("@PONum", objCustomerEntrie.PONum));
            else
                command.Parameters.Add(new SqlParameter("@PONum", DBNull.Value));

            if (objCustomerEntrie.SKUName != null || objCustomerEntrie.SKUName != "")
                command.Parameters.Add(new SqlParameter("@SKUName", objCustomerEntrie.SKUName));
            else
                command.Parameters.Add(new SqlParameter("@SKUName", DBNull.Value));

            if (objCustomerEntrie.StyleName != null || objCustomerEntrie.StyleName != "")
                command.Parameters.Add(new SqlParameter("@StyleName", objCustomerEntrie.StyleName));
            else
                command.Parameters.Add(new SqlParameter("@StyleName", DBNull.Value));

            command.Parameters.Add(new SqlParameter("@TotalQty", Convert.ToInt32(objCustomerEntrie.TotalQty)));

            if (objCustomerEntrie.ServiceTypeCode != 0)
                command.Parameters.Add(new SqlParameter("@ServiceTypeCode", objCustomerEntrie.ServiceTypeCode));
            else
                command.Parameters.Add(new SqlParameter("@ServiceTypeCode", DBNull.Value));

            if (objCustomerEntrie.DeliveryMethodCode != -1)
                command.Parameters.Add(new SqlParameter("@DeliveryMethodCode", objCustomerEntrie.DeliveryMethodCode));
            else
                command.Parameters.Add(new SqlParameter("@DeliveryMethodCode", DBNull.Value));

            if (objCustomerEntrie.CarrierCode != 0)
                command.Parameters.Add(new SqlParameter("@CarrierCode", objCustomerEntrie.CarrierCode));
            else
                command.Parameters.Add(new SqlParameter("@CarrierCode", DBNull.Value));

            if (objCustomerEntrie.RetailerID != 0)
                command.Parameters.Add(new SqlParameter("@RetailerID", objCustomerEntrie.RetailerID));
            else
                command.Parameters.Add(new SqlParameter("@RetailerID", DBNull.Value));

            if (objCustomerEntrie.CategoryCode != 0)
                command.Parameters.Add(new SqlParameter("@CategoryCode", objCustomerEntrie.CategoryCode));
            else
                command.Parameters.Add(new SqlParameter("@CategoryCode", DBNull.Value));

            if (objCustomerEntrie.JewelryTypeCode != 0)
                command.Parameters.Add(new SqlParameter("@JewelryTypeCode", objCustomerEntrie.JewelryTypeCode));
            else
                command.Parameters.Add(new SqlParameter("@JewelryTypeCode", DBNull.Value));

            if (objCustomerEntrie.PersonID != 0)
                command.Parameters.Add(new SqlParameter("@PersonID", objCustomerEntrie.PersonID));
            else
                command.Parameters.Add(new SqlParameter("@PersonID", DBNull.Value));

            command.Parameters.Add(new SqlParameter("@SpecialInstructions", objCustomerEntrie.SpecialInstructions));

            command.Parameters.Add(new SqlParameter("@Messenger", objCustomerEntrie.Messenger));

            if (objCustomerEntrie.VendorID != 0)
            {
                command.Parameters.Add(new SqlParameter("@VendorID", objCustomerEntrie.VendorID));
                command.Parameters.Add(new SqlParameter("@VendorName", objCustomerEntrie.VendorName));
            }
            else
            {
                command.Parameters.Add(new SqlParameter("@VendorID", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@VendorName", DBNull.Value));
            }

            command.Parameters.Add(new SqlParameter("@CustomerCode", objCustomerEntrie.CustomerCode));
            command.Parameters.Add(new SqlParameter("@Account", objCustomerEntrie.Account));
            command.Parameters.Add(new SqlParameter("@IsSave", objCustomerEntrie.IsSave));

            try
            {
                command.Connection.Open();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                result = ds;
                command.Connection.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                command.Connection.Close();
                result = null;
            }
            return result;
        }
		#endregion

		#region Retailer Attachments
		public static DataSet GetRetailerAttachments(SyntheticRetailerModel retailer, Page p)
		{
			var msg = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetRetailerAttachments",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(retailer.customerCode));
			LogUtils.UpdateSpLogWithParameters(command, p);
			DataSet ds = new DataSet();
			try
			{
				SqlDataAdapter adapter;

				adapter = new SqlDataAdapter(command);
				adapter.Fill(ds);
				conn.Close();
			}
			catch (Exception x)
			{
				msg = x.Message;
			}
			conn.Close();
			return ds;
		}
        public static DataSet GetRetailerAttachmentsByCustomer(string customerCode, string retailerId, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetRetailerAttachmentsByCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(customerCode));
            command.Parameters.AddWithValue("@RetailerID", Convert.ToInt16(retailerId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }
       public static CustomerProgramModel GetCPInfo(string cpId, Page p)
        {
            var msg = "";
            CustomerProgramModel cp = new CustomerProgramModel();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select * from v1CustomerProgram1 where CPID=" + cpId  + "and iscopy is null",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            
            try
            {
                SqlDataAdapter adapter;
                var cdt = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(cdt);
                conn.Close();
                if (cdt.Rows.Count > 0)
                {
                    DataRow dr = cdt.Rows[0];
                    CustomerProgramModel.SetExtData(dr, cp);
                    cp.CpId = Convert.ToString(dr["CPID"]);
                    cp.CpOfficeId = Convert.ToString(dr["CPOfficeID"]);
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
            }            
            conn.Close();            
            return cp;
        }

        public static DataSet GetBNAttachments(string customerCode, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetBNAttachments",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(customerCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }
        public static bool SetNewBNEntries(string value, string table, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetNewBNEntries",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Value", value);
            command.Parameters.AddWithValue("@Table", table);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataTable dt = new DataTable(); 
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            if (dt == null || dt.Rows.Count == 0)
                return false;
            else if (dt.Rows[0].ItemArray[0].ToString() == "failed")
                return false;
            else
                return true;
        }
        public static bool UpdateBNEntries(DataTable dtCategory, DataTable dtDestination, int customerCode, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spUpdateBNEntries",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            var paramCategory = new SqlParameter
            {
                ParameterName = "@CategoryList",
                SqlDbType = SqlDbType.Structured,
                Value = dtCategory,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramCategory);
            var paramDestination = new SqlParameter
            {
                ParameterName = "@DestinationList",
                SqlDbType = SqlDbType.Structured,
                Value = dtDestination,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramDestination);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataTable dt = new DataTable();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            if (dt == null || dt.Rows.Count == 0)
                return false;
            else
                return true;
        }
        #endregion
        public static int GetTimeDiff(Page p)
        {
            int officeId = Convert.ToInt16(p.Session["AuthorOfficeID"]);
            int timeDiff = 5;
            if (officeId == 2)//India
                return 5;
            string diff = "";
            if (p.Session["TimeDiffNY"] != null)
                diff = p.Session["TimeDiffNY"].ToString();
            if (diff != null && diff != "")
                timeDiff = Convert.ToInt16(diff);
            return -timeDiff;
        }

        #region Submission and Front
        public static DataSet SetSyntheticCustomerEntriesNew(SyntheticCustomerEntriesModel objCustomerEntrie, Page p)
        {
            DataSet result = new DataSet();
            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand("spSetSyntheticCustomerEntriesNew");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@ID", objCustomerEntrie.ID));

            if (objCustomerEntrie.MemoNum != null && objCustomerEntrie.MemoNum != "")
                command.Parameters.Add(new SqlParameter("@MemoNum", objCustomerEntrie.MemoNum));
            else
                command.Parameters.Add(new SqlParameter("@MemoNum", DBNull.Value));

            if (objCustomerEntrie.PONum != null || objCustomerEntrie.PONum != "")
                command.Parameters.Add(new SqlParameter("@PONum", objCustomerEntrie.PONum));
            else
                command.Parameters.Add(new SqlParameter("@PONum", DBNull.Value));

            if (objCustomerEntrie.SKUName != null || objCustomerEntrie.SKUName != "")
                command.Parameters.Add(new SqlParameter("@SKUName", objCustomerEntrie.SKUName));
            else
                command.Parameters.Add(new SqlParameter("@SKUName", DBNull.Value));

            if (objCustomerEntrie.StyleName != null || objCustomerEntrie.StyleName != "")
                command.Parameters.Add(new SqlParameter("@StyleName", objCustomerEntrie.StyleName));
            else
                command.Parameters.Add(new SqlParameter("@StyleName", DBNull.Value));

            command.Parameters.Add(new SqlParameter("@TotalQty", Convert.ToInt32(objCustomerEntrie.TotalQty)));

            if (objCustomerEntrie.ServiceTypeCode != 0)
                command.Parameters.Add(new SqlParameter("@ServiceTypeCode", objCustomerEntrie.ServiceTypeCode));
            else
                command.Parameters.Add(new SqlParameter("@ServiceTypeCode", DBNull.Value));

            if (objCustomerEntrie.DeliveryMethodCode != -1)
                command.Parameters.Add(new SqlParameter("@DeliveryMethodCode", objCustomerEntrie.DeliveryMethodCode));
            else
                command.Parameters.Add(new SqlParameter("@DeliveryMethodCode", "0")); // default set GSI Pickup

            if (objCustomerEntrie.CarrierCode != 0)
                command.Parameters.Add(new SqlParameter("@CarrierCode", objCustomerEntrie.CarrierCode));
            else
                command.Parameters.Add(new SqlParameter("@CarrierCode", DBNull.Value));

            if (objCustomerEntrie.RetailerID != 0)
                command.Parameters.Add(new SqlParameter("@RetailerID", objCustomerEntrie.RetailerID));
            else
                command.Parameters.Add(new SqlParameter("@RetailerID", DBNull.Value));

            if (objCustomerEntrie.CategoryCode != 0)
                command.Parameters.Add(new SqlParameter("@CategoryCode", objCustomerEntrie.CategoryCode));
            else
                command.Parameters.Add(new SqlParameter("@CategoryCode", DBNull.Value));

            if (objCustomerEntrie.JewelryTypeCode != 0)
                command.Parameters.Add(new SqlParameter("@JewelryTypeCode", objCustomerEntrie.JewelryTypeCode));
            else
                command.Parameters.Add(new SqlParameter("@JewelryTypeCode", DBNull.Value));
            if (objCustomerEntrie.PersonID != 0)
                command.Parameters.Add(new SqlParameter("@PersonID", objCustomerEntrie.PersonID));
            else
                command.Parameters.Add(new SqlParameter("@PersonID", DBNull.Value));
            if (objCustomerEntrie.VendorID != 0)
                command.Parameters.Add(new SqlParameter("@VendorID", objCustomerEntrie.VendorID));
            else
                command.Parameters.Add(new SqlParameter("@VendorID", DBNull.Value));
            if (objCustomerEntrie.TotalWeight == 0)
                command.Parameters.Add(new SqlParameter("@TotalWeight", DBNull.Value));
            else
                command.Parameters.Add(new SqlParameter("@TotalWeight", objCustomerEntrie.TotalWeight));
            command.Parameters.Add(new SqlParameter("@SpecialInstructions", objCustomerEntrie.SpecialInstructions));

            command.Parameters.Add(new SqlParameter("@Messenger", objCustomerEntrie.Messenger));
            command.Parameters.Add(new SqlParameter("@VendorName", objCustomerEntrie.VendorName));
            command.Parameters.Add(new SqlParameter("@CustomerCode", objCustomerEntrie.CustomerCode));
            command.Parameters.Add(new SqlParameter("@Account", objCustomerEntrie.Account));
            command.Parameters.Add(new SqlParameter("@IsSave", objCustomerEntrie.IsSave));

            command.Parameters.Add(new SqlParameter("@MemoReceiptPDF", objCustomerEntrie.MemoReceiptPDF));
            command.Parameters.Add(new SqlParameter("@GDServiceTypeId", objCustomerEntrie.GDServiceTypeId));
            if (objCustomerEntrie.BatchMemo != null && objCustomerEntrie.BatchMemo != "")
                command.Parameters.Add(new SqlParameter("@BatchMemo", objCustomerEntrie.BatchMemo));
            else
                command.Parameters.Add(new SqlParameter("@BatchMemo", DBNull.Value));

            if (objCustomerEntrie.ASN != null || objCustomerEntrie.ASN != "")
                command.Parameters.Add(new SqlParameter("@ASN", objCustomerEntrie.ASN));
            else
                command.Parameters.Add(new SqlParameter("@ASN", DBNull.Value));
            //IvanB 28/03 start
            if (objCustomerEntrie.BNCategoryId != 0)
                command.Parameters.Add(new SqlParameter("@BNCategoryId", objCustomerEntrie.BNCategoryId));
            else
                command.Parameters.Add(new SqlParameter("@BNCategoryId", DBNull.Value));

            if (objCustomerEntrie.BNDestinationId != 0)
                command.Parameters.Add(new SqlParameter("@BNDestinationId", objCustomerEntrie.BNDestinationId));
            else
                command.Parameters.Add(new SqlParameter("@BNDestinationId", DBNull.Value));
            //IvanB 28/03 end

            try
            {
                command.Connection.Open();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                result = ds;
                command.Connection.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                command.Connection.Close();
                result = null;
            }
            return result;
        }

        public static List<CustomerProgramModel> GetCustomerPrograms(CustomerModel customerModel, CustomerModel vendorModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramsPerCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerOfficeID", customerModel.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customerModel.CustomerId);
            command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
            command.Parameters.AddWithValue("@VendorID", DBNull.Value);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerProgramModel(row)).ToList();
            conn.Close();
            return result;
        }

        public static DataTable GetBNScreeningCategoryList(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetBNScreeningCategoryList",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetBNScreeningDestinationList(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetBNScreeningDestinationList",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataSet SetBlueNileValues(string requestId, string onlineOrder, string bnCategory, string bnDestination, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetBlueNileCustomerEntries",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (onlineOrder == "")
                command.Parameters.AddWithValue("@OnlineOrder", DBNull.Value);
            else
                command.Parameters.AddWithValue("@OnlineOrder", onlineOrder);
            command.Parameters.AddWithValue("@RequestID", Convert.ToInt32(requestId));
            command.Parameters.AddWithValue("@BNCategory", bnCategory);
            command.Parameters.AddWithValue("@BNDestination", bnDestination);

            LogUtils.UpdateSpLogWithParameters(command, p);
            ;
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }
        #endregion

        #region Customers Contractor

        public static DataTable GetAllContractor(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetAllContractors",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public static DataTable GetContractors(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetContractors",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();


            return dt;
        }
        public static DataTable SetContractor(SyntheticContractorModel contractor, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetContractors",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //command.Parameters.AddWithValue("@ContractorId", contractor.contractorId);
            command.Parameters.AddWithValue("@CustomerCode", contractor.customerCode);
            command.Parameters.AddWithValue("@ServiceTypeCodeSet", contractor.ServiceTypeCodeSet);
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt16(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt16(p.Session["AuthorOfficeId"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }
        public static List<SyntheticContractorModel> GetSyntheticCustomerContractor(SyntheticContractorModel contractor, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var dt = new DataTable();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticCustomerContractor",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(contractor.customerCode));
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                var msg = ex.Message;
            }
            conn.Close();

            var result = (from DataRow row in dt.Rows select new SyntheticContractorModel(row)).ToList();

            return result;
        }
        public static bool SetSyntheticCustomerContractor(SyntheticContractorModel contractor, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticCustomerContractor",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(contractor.customerCode));
            command.Parameters.AddWithValue("@ContractorId", contractor.contractorId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }
        public static bool DelSyntheticCustomerContractor(SyntheticContractorModel contractor, Page p)
        {
            bool result = true;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spDelSyntheticCustomerContractor",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(contractor.customerCode));
            command.Parameters.AddWithValue("@ContractorId", Convert.ToInt16(contractor.contractorId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                result = false;
            }
            conn.Close();
            return result;
        }
        public static DataSet GetContractorsAttachments(SyntheticContractorModel contractor, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetContractorAttachments",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt16(contractor.customerCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }
        #endregion

        #region Company Group

        public static DataTable GetAllCompanyGroup(Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetAllCompanyGroup",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable GetCompanyGroupByCompanyGroupID(int CompanyGroupID, Page p)
        {
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetCompanyGroupByCompanyGroupID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@CompanyGroupID", CompanyGroupID);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static DataTable SetCompanyGroup(string companyGroupName, string customerIDList, Page p)
        {
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetCompanyGroup",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CompanyGroupName", companyGroupName);
            command.Parameters.AddWithValue("@CustomerIDList", customerIDList);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }


        #endregion
        #region Retailer's Vendor
        public static List<CustomerProgramModel> GetCustomerPrograms(SyntheticCustomerModel customerModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramsPerCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerOfficeID", customerModel.customerOfficeId);
            command.Parameters.AddWithValue("@CustomerID", customerModel.customerId);

            command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
            command.Parameters.AddWithValue("@VendorID", DBNull.Value);

            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerProgramModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<CustomerProgramModel> GetCustomerProgramsByRetailer(SyntheticCustomerModel customerModel, int retailerId, Page p)
        {
            List<CustomerProgramModel> result = new List<CustomerProgramModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPByRetailer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerModel.customerCode);
            command.Parameters.AddWithValue("@RetailerID", retailerId);

            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                result = (from DataRow row in dt.Rows select CustomerProgramModel.Create3(row)).ToList();
            }
            conn.Close();
            return result;
        }

        public static List<CustomerProgramModel> GetCustomerProgramsByNature(SyntheticCustomerModel customerModel, int nature, Page p)
        {
            List<CustomerProgramModel> result = new List<CustomerProgramModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSkuListByNaturalOrigin",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerModel.customerCode);
            command.Parameters.AddWithValue("@MinMeasure", nature);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                result = (from DataRow row in dt.Rows select CustomerProgramModel.Create4(row)).ToList();
            }
            conn.Close();
            return result;
        }

        public static List<CustomerProgramModel> GetCustomerProgramsByProgram(SyntheticCustomerModel customerModel, int programId, Page p)
        {
            List<CustomerProgramModel> result = new List<CustomerProgramModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSkuListByProgram",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerModel.customerCode);
            command.Parameters.AddWithValue("@ProgramId", programId);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                result = (from DataRow row in dt.Rows select CustomerProgramModel.Create4(row)).ToList();
            }
            conn.Close();
            return result;
        }

        public static int GetSkuProgramId(string customerCode, string cpId, Page p)
        {
            int result = 0;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSkuProgramByCpId",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt32(customerCode));
            command.Parameters.AddWithValue("@CPId", Convert.ToDecimal(cpId));

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToInt32(dt.Rows[0]["ProgramId"]);
            }
            conn.Close();
            return result;
        }

        public static string SaveSkuProgram(string customerCode, string cpId, string programId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spSaveSkuProgram");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@CustomerCode", Convert.ToInt32(customerCode)));
            command.Parameters.Add(new SqlParameter("@CPId", Convert.ToDecimal(cpId)));
            command.Parameters.Add(new SqlParameter("@ProgramId", Convert.ToDecimal(programId)));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Program added";
            }
            catch (Exception ex)
            {
                result = "Error";
            }
            return result;
        }
        public static string DeleteSkuProgram(string customerCode, string cpId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spDeleteSkuProgram");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@CustomerCode", Convert.ToInt32(customerCode)));
            command.Parameters.Add(new SqlParameter("@CPId", Convert.ToDecimal(cpId)));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Program deleted";
            }
            catch (Exception ex)
            {
                result = "Delete error";
            }
            return result;
        }
        public static DataSet GetRetailerCustomers(string retailerId, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomersByRetailer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@RetailerID", Convert.ToInt16(retailerId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }

        public static List<BrandProgramModel> GetRetailerPrograms(string retailerId, Page p)
        {
            var msg = "";
            List<BrandProgramModel> result = new List<BrandProgramModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetFullBrandProgramList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                SqlDataAdapter adapter;
                var dt = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        BrandProgramModel prg = new BrandProgramModel(dr);
                        result.Add(prg);
                    }
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return result;
        }
        public static List<BrandModel> GetAllBrands(Page p)
        {
            var msg = "";
            List<BrandModel> result = new List<BrandModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetBrandList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                SqlDataAdapter adapter;
                var dt = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        BrandModel brand = new BrandModel(dr);
                        result.Add(brand);
                    }
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return result;
        }
        public static DataSet GetTop10ItemsByCpName(string cpName, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemsByCpName",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CpName", cpName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return ds;
        }

        //public static List<BrandProgramModel> GetVendorBrandProgramList(string customerCode, Page p)
        //{
        //    var msg = "";
        //    List<BrandProgramModel> result = new List<BrandProgramModel>();
        //    var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
        //    conn.Open();
        //    var command = new SqlCommand
        //    {
        //        CommandText = "spGetVendorBrandProgramList",
        //        Connection = conn,
        //        CommandType = CommandType.StoredProcedure,
        //        CommandTimeout = p.Session.Timeout
        //    };
        //    command.Parameters.AddWithValue("@CustomerCode", Convert.ToInt32(customerCode));
        //    LogUtils.UpdateSpLogWithParameters(command, p);

        //    try
        //    {
        //        SqlDataAdapter adapter;
        //        var dt = new DataTable();
        //        adapter = new SqlDataAdapter(command);
        //        adapter.Fill(dt);
        //        conn.Close();
        //        if (dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                BrandProgramModel prg = new BrandProgramModel(dr);
        //                result.Add(prg);
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        msg = x.Message;
        //    }
        //    conn.Close();
        //    return result;
        //}

        public static List<string> GetCustomersByProgram(int programId, Page p)
        {
            var msg = "";
            var result = new List<string>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomersByProgram",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ProgramId", Convert.ToInt16(programId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        result.Add(row["CustomerCode"].ToString());
                    }
                }
                conn.Close();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return result;
        }
        public static string AddNewBrand(string brandName, string nature, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spAddNewBrand");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@BrandName", brandName));
            command.Parameters.AddWithValue("@Nature", nature);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Brand "+ brandName + " (" + nature + ") added successfully.";
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                result = "Brand " + brandName + " (" + nature + ") adding failed.";
            }
            return result;
        }

        public static bool IsProgramExist(int brandId, string programName, string nature, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spProgramExist",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@BrandId", brandId);
            command.Parameters.AddWithValue("@ProgramName", programName);
            command.Parameters.AddWithValue("@Nature", nature);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                conn.Close();
                return false;
            }
            conn.Close();
            return true;
        }

        public static string AddNewProgram(int brandId, string brandName, string programName, string nature, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spAddNewProgram");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@BrandId", brandId);
            command.Parameters.AddWithValue("@ProgramName", programName);
            command.Parameters.AddWithValue("@Nature", nature);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Program" + programName +  "(" + nature + ") successfully added to brand " + brandName + ".";
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                result = "Program" + programName + "(" + nature + ") adding to brand " + brandName + " failed.";
            }
            return result;
        }

        public static List<BrandModel> GetRetailerBrands(int retailerId, Page p)
        {
            var msg = "";
            List<BrandModel> result = new List<BrandModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetRetailersBrandList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@RetailerId", retailerId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                SqlDataAdapter adapter;
                var dt = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        BrandModel prg = new BrandModel(dr);
                        result.Add(prg);
                    }
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return result;
        }

        public static string AddBrandToRetailer(int retailerId, int brandId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spAssignBrandToRetailer");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@RetailerId", retailerId);
            command.Parameters.AddWithValue("@BrandId", brandId);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Success";
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                result = "failed";
            }
            return result;
        }

        public static string DeleteRetailerBrand(int retailerId, int brandId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spDeleteRetailerBrand");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@RetailerId", retailerId));
            command.Parameters.Add(new SqlParameter("@BrandId", brandId));

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Brand deleted";
            }
            catch (Exception ex)
            {
                result = "Delete brand failed";
            }
            return result;
        }

        public static string AddProgramToVendor(int customerCode, int programId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spAssignProgramToVendor");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            command.Parameters.AddWithValue("@ProgramId", programId);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Success";
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                result = "failed";
            }
            return result;
        }

        public static string DeleteVendorProgram(int customerCode, int programId, Page p)
        {
            var result = "";
            SqlCommand command = new SqlCommand("spDeleteVendorProgram");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            command.Parameters.AddWithValue("@ProgramId", programId);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                result = "Program deleted";
            }
            catch (Exception ex)
            {
                result = "Delete program failed";
            }
            return result;
        }

        public static List<BrandProgramModel> GetVendorPrograms(int customerCode, Page p)
        {
            var msg = "";
            List<BrandProgramModel> result = new List<BrandProgramModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetVendorsProgramList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                SqlDataAdapter adapter;
                var dt = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        BrandProgramModel prg = new BrandProgramModel(dr);
                        result.Add(prg);
                    }
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return result;
        }
        public static DataTable GetCustomerIdByCode(string customerCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = @"select CustomerID from v0Customer where CustomerCode=" + customerCode,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt == null || dt.Rows.Count == 0)
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return dt;
        }

        public static bool IsProgramAssingedToVendor(int programId, int customerCode,Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spProgramAssingedToVendor",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ProgramId", programId);
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                conn.Close();
                return false;
            }
            conn.Close();
            return true;
        }
        public static List<CustomerModel> GetCustomersBySku(string skuName, Page p)
        {
            List<CustomerModel> result = new List<CustomerModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = @"select CustomerCode from v1CustomerProgram1 where CustomerProgramName='" + skuName + "' and IsCopy is null",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    CustomerModel cust = new CustomerModel();
                    cust.CustomerCode = "" + dr["CustomerCode"];
                    result.Add(cust);
                }
            }
            return result;
        }
        #endregion

    }

}