﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.UI;
using Corpt.Constants;

namespace Corpt.Utilities
{
    public static class PlottingUtils
    {
        public static string GetProgramFileDir()
        {
            const string sProgramFileFolder = @"C:\PROGRAM FILES";
            string[] dirs = Directory.GetDirectories(@"c:\", "program files*");
            foreach (var dir in dirs.Where(dir => dir.Trim().ToUpper() != sProgramFileFolder))
            {
                return dir;
            }
            return sProgramFileFolder;
        }
        public static string CallPlotting(Page p, string sShapePath, string sItemNumber, string curPartName) 
        {
            const int errorFileNotFound = 2;
            const int errorAccessDenied = 5;
            if (string.IsNullOrEmpty(sShapePath) || string.IsNullOrEmpty(sItemNumber) || string.IsNullOrEmpty(curPartName))
                return "";
            var progFileName = "";
            try
            {
                //-- Path to shape directory
                if (string.IsNullOrEmpty("" + p.Session[SessionConstants.GlobalShapeRoot]))
                {
                    return "Parameter " + SessionConstants.GlobalShapeRoot + " not found. (Global.asax)";
                }
                var sShapeDir =  p.MapPath("" + p.Session[SessionConstants.GlobalShapeRoot]);
                if (!Directory.Exists(sShapeDir))
                {
                    return "Directory " + sShapeDir + " does not exists.";
                }
                
                //-- Path to plotting.exe
                progFileName = "" + p.Session[SessionConstants.GlobalPathToPlotting];
                if (string.IsNullOrEmpty(progFileName))
                {
                    return "Parameter " + SessionConstants.GlobalPathToPlotting + " not found. (Global.asax)";
                }

                progFileName = GetProgramFileDir() + Path.DirectorySeparatorChar + progFileName;
                if (!File.Exists(progFileName))
                {
                    return "File " + progFileName + " does not exists.";
                }

                //-- Path to plotting dir    
                var sPathToPlotting = ("" + p.Session[SessionConstants.GlobalPlotDir]);
                if (string.IsNullOrEmpty(sPathToPlotting))
                {
                    return "Parameter " + SessionConstants.GlobalPlotDir + " not found. (Global.asax)";
                }

                if (!string.IsNullOrEmpty(sItemNumber))
                {
                    var pInfo = new ProcessStartInfo {FileName = progFileName};
                    var sPlotFileName = sItemNumber + "." + curPartName;
                    pInfo.Arguments = "\"" + sShapePath + "\" " + "\"" + sPlotFileName + "\"" + " " + sPathToPlotting + " " + sShapeDir;

                    pInfo.UseShellExecute = true;
                    Process.Start(pInfo);
                }
            }
            catch (Win32Exception ex)
            {
                if (ex.NativeErrorCode == errorFileNotFound)
                {
                    return "The system cannot find the file " + progFileName;
                }
                if (ex.NativeErrorCode == errorAccessDenied)
                {
                   return "The file " + progFileName + " access denied";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }


    }
}