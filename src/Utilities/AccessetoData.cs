﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Configuration;
using System.IO;


namespace DAL
{
    public class AccesstoData
    {
        public string myConnectionString;
        private SqlConnection cn;

        public AccesstoData()


        {
            myConnectionString = "";//Convert.ToString(ConfigurationManager.AppSettings["ConnectionString"]);
            cn = new SqlConnection(myConnectionString);


        }

        public AccesstoData(SqlConnection con)
        {
            cn = con;

        }

        public AccesstoData(string connectionString)
        {
         cn = new SqlConnection(connectionString);
        
        }



        public void getBatchAppraisal(DataSet ds, string GetProc, int BatchID)
        { // getTable according to new StoredProc 	

            try
            {
                // string collection mapping tables 
                SqlDataAdapter daDictionary = new SqlDataAdapter();
                daDictionary.TableMappings.Add("Table", "BatchT1");
               
                SqlCommand sqlGetDataset = new System.Data.SqlClient.SqlCommand();
                sqlGetDataset.CommandText = GetProc;
                sqlGetDataset.CommandTimeout = 3000;
                sqlGetDataset.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter pBatchID = new SqlParameter();
                pBatchID.SqlDbType = SqlDbType.Int;
                pBatchID.ParameterName = "@BatchID";
                pBatchID.Value = BatchID;
                sqlGetDataset.Parameters.Add(pBatchID);

                SqlParameter pItemCode = new SqlParameter();
                pItemCode.SqlDbType = SqlDbType.Int;
                pItemCode.ParameterName = "@ItemCode";
                pItemCode.Value = DBNull.Value;
                sqlGetDataset.Parameters.Add(pItemCode);



                sqlGetDataset.Connection = cn;
                daDictionary.SelectCommand = sqlGetDataset;
                //daDictionary.ContinueUpdateOnError = true;
                //ds.Clear();
                daDictionary.Fill(ds);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }



        public string PostAnyTable(DataTable dt, string insertsqlCommand, string updatesqlCommand, string deletesqlCommand, string pkey, ArrayList namesParm)
        {

            string ret;
            string outputPar = "@" + pkey.Trim();
            cn.Open();
            // insertion in Tables must be in one transactions 
            SqlTransaction TR = cn.BeginTransaction();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand SqlInsert = new System.Data.SqlClient.SqlCommand();
            SettingforSQLCommand(cn, insertsqlCommand.Trim(), System.Data.CommandType.StoredProcedure,
                TR, SqlInsert, dt, namesParm);

            SqlInsert.Parameters[outputPar].Direction = ParameterDirection.Output;
            SqlInsert.UpdatedRowSource = UpdateRowSource.OutputParameters;
            da.InsertCommand = SqlInsert;


            SqlCommand SqlUpdate = new System.Data.SqlClient.SqlCommand();
            SettingforSQLCommand(cn, updatesqlCommand.Trim(), System.Data.CommandType.StoredProcedure,
                TR, SqlUpdate, dt, namesParm);
            da.UpdateCommand = SqlUpdate;

            SqlCommand SqlDelete = new System.Data.SqlClient.SqlCommand();
            SettingforSQLCommandforDelete(cn, deletesqlCommand.Trim(), System.Data.CommandType.StoredProcedure,
                TR, SqlDelete, dt, pkey);
            da.DeleteCommand = SqlDelete;

            try
            {
                da.Update(dt);
                TR.Commit();
                ret = "Changes are saved successfully";
                //cn.Close();
                dt.AcceptChanges();
                return ret;
            }
            catch (Exception ex)
            {
                TR.Rollback();
                throw (ex);

            }

            finally
            {
                //???   dt.AcceptChanges();
                cn.Close();

            }

        }


   


  

   

        public DataSet GetDctionary(string dictionaryName)
        {
            DataSet dsDictionary = new DataSet();

            try
            {
                SqlCommand sqlGetDictionary = new System.Data.SqlClient.SqlCommand();
                sqlGetDictionary.CommandText = "select * from  " + dictionaryName;
                sqlGetDictionary.CommandType = System.Data.CommandType.Text;
                sqlGetDictionary.Connection = cn;
                cn.Open();
                SqlDataAdapter daDictionary = new SqlDataAdapter();
                daDictionary.SelectCommand = sqlGetDictionary;
                daDictionary.Fill(dsDictionary, dictionaryName);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dsDictionary;
        }


      

   




        public void GetODataTable(string GetStoredProcedure, DataTable dictionary, string WhereClause)
        { // getTable according to new StoredProc 							
            try
            {
                SqlCommand sqlGetDictionary = new System.Data.SqlClient.SqlCommand();
                if (WhereClause != null)
                {
                    sqlGetDictionary.Parameters.AddWithValue("@WhereClause", WhereClause);

                }
                sqlGetDictionary.CommandText = GetStoredProcedure;
                sqlGetDictionary.CommandType = System.Data.CommandType.StoredProcedure;
                sqlGetDictionary.Connection = cn;
                SqlDataAdapter daDictionary = new SqlDataAdapter();
                daDictionary.SelectCommand = sqlGetDictionary;
                daDictionary.Fill(dictionary);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public ArrayList GetParameters(string StoredProcName)
        {
            //parameters we get for insert ,delete ,update proc . For Select proc we have just one param
            // this  is used for sql proc with parameters - not with  Open XML 
            // provide caching for parameters  in future 

            ArrayList SqlParameterList = new ArrayList();
            string statement = "select parameter_name, data_type,parameter_mode  from Information_schema.Parameters" + " Where specific_name = '" + StoredProcName.Trim() + "'" +
                "  order by Ordinal_position ";
            ;
            DataTable dt = this.GetJustTable(statement, "Parameters");

            string columname;
            string paramName;
            string paramType;
            foreach (DataRow row in dt.Rows)
            {
                columname = ((string)row["parameter_name"]).Substring(1).Trim();
                paramName = ((string)row["parameter_name"]).Trim();
                paramType = ((string)row["data_type"]).Trim();
                SqlParameter par = new SqlParameter();
                par.ParameterName = paramName;
                par.SqlDbType = MappingSqlType(paramType);
                par.SourceColumn = columname;
                par.SourceVersion = DataRowVersion.Current;
                SqlParameterList.Add(par);
            }
            return SqlParameterList;
        }



        // fill out (teoretically existing dataTable
        public void GetJustTable(string statement, DataTable dt)
        {

            SqlConnection con = cn;

            try
            {
                SqlCommand sqlSourceCommand = new System.Data.SqlClient.SqlCommand();
                sqlSourceCommand.CommandText = statement;
                sqlSourceCommand.CommandType = System.Data.CommandType.Text;
                sqlSourceCommand.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlSourceCommand;
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                throw (ex);
            }

            finally
            { cn.Close(); }
        }

        public DataTable GetJustTable(string statement, string TableName)
        {

            SqlConnection con = cn;
            DataTable dt = new DataTable();
            try
            {
                SqlCommand sqlSourceCommand = new System.Data.SqlClient.SqlCommand();
                sqlSourceCommand.CommandText = statement;
                sqlSourceCommand.CommandType = System.Data.CommandType.Text;
                sqlSourceCommand.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlSourceCommand;
                da.Fill(dt);
                dt.TableName = TableName;
                return dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            finally
            { cn.Close(); }
        }

        /**
         * Valeriya Yampolskaya
        <summary>
         * this proc fillout datatable dictionary
         * parameters values - it's parameters values for stored proc
         * GetStoredProcedure stored proc name
         * so right now every DataTable can be fillout by this proc
         * by the way I do not think , it's a good idea to fill out huge dataset in one time
        </summary>
        */
        public void GetODataTable(string GetStoredProcedure, DataTable dictionary, ArrayList ParametersValues)
        { // getTable according to new StoredProc 	

            ArrayList GetProcPrameters = new ArrayList();
            GetProcPrameters = GetParameters(GetStoredProcedure);
            try
            {
                SqlCommand sqlGetDictionary = new System.Data.SqlClient.SqlCommand();
                if (ParametersValues.Count == GetProcPrameters.Count)
                {
                    for (int i = 0; i < GetProcPrameters.Count; ++i)
                    {
                        ((SqlParameter)GetProcPrameters[i]).Value = ParametersValues[i];
                        sqlGetDictionary.Parameters.Add(GetProcPrameters[i]);

                    }

                    sqlGetDictionary.CommandText = GetStoredProcedure;
                    sqlGetDictionary.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlGetDictionary.Connection = cn;
                    SqlDataAdapter daDictionary = new SqlDataAdapter();
                    daDictionary.SelectCommand = sqlGetDictionary;
                    daDictionary.Fill(dictionary);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }



        // excute stored proc whithout filling out the table

        public string ExecStorProc(string StorProcName, ArrayList ParametersValues)
        { // getTable according to new StoredProc 	

            ArrayList GetProcPrameters = new ArrayList();
            GetProcPrameters = GetParameters(StorProcName);
            string result = "You changes are saved successfully";


            SqlCommand sqlExcProc = new System.Data.SqlClient.SqlCommand();
            if (ParametersValues.Count == GetProcPrameters.Count)
            {
                for (int i = 0; i < GetProcPrameters.Count; ++i)
                {
                    ((SqlParameter)GetProcPrameters[i]).Value = ParametersValues[i];
                    sqlExcProc.Parameters.Add(GetProcPrameters[i]);
                }
                cn.Open();

                sqlExcProc.CommandText = StorProcName;
                sqlExcProc.CommandType = System.Data.CommandType.StoredProcedure;
                sqlExcProc.Connection = cn;

                try
                {

                    sqlExcProc.ExecuteNonQuery();

                    cn.Close();
                }

                catch (Exception ex)
                {
                    result = ex.Message;

                }
                finally
                { cn.Close(); }
            }
            return result;
        }


        private void parameterForSQLCommand(SqlCommand someCommand, DataColumn dc)
        {
            string pname = "@" + dc.ColumnName;
            SqlParameter p1 = new SqlParameter();
            p1.ParameterName = pname;
            p1.SqlDbType = SqlDbType.Int;
            someCommand.Parameters.Add(p1);
            someCommand.Parameters[pname].SourceColumn = dc.ColumnName;
            someCommand.Parameters[pname].SourceVersion = DataRowVersion.Current;
        }


        private void 
            
            
            parameterscomand(SqlCommand someCommand, DataTable someTable, ArrayList ParamforCommand)
        {
            someCommand.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, null));
            foreach (object o in ParamforCommand)
            {
                SqlParameter par = new SqlParameter();
                par.ParameterName = ((SqlParameter)o).ParameterName;
                par.SqlDbType = ((SqlParameter)o).SqlDbType;
                par.SourceColumn = ((SqlParameter)o).SourceColumn;
                par.SourceVersion = ((SqlParameter)o).SourceVersion;
                someCommand.Parameters.Add(par);
            }
        }

      

        public void ExecuteTheCommand(string queryString)
        {
            using (SqlConnection connection = cn)
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }




        private SqlDbType MappingSqlType(string paramType)
        {
            //http://64.233.169.104/search?q=cache:gKiFtqV07hoJ:www.carlprothman.net/Technology/DataTypeMapping/tabid/97/Default.aspx+Mapping+SqlDbType&hl=en&ct=clnk&cd=5&gl=us
            // mapping type see at the link 

            switch (paramType.Trim())
            {
                case "bigint":
                    return SqlDbType.BigInt;

                case "binary":
                    return SqlDbType.Binary;


                case "bit":
                    return SqlDbType.Bit;


                case "money":
                case "smallmoney":
                    return SqlDbType.Money;


                case "datetime":
                case "smalldatetime":
                    return SqlDbType.DateTime;


                case "int":
                    return SqlDbType.Int;

                case "uniqueidentifier":
                    return SqlDbType.UniqueIdentifier;

                case "image":

                    return SqlDbType.Binary;


                case "text":
                    return SqlDbType.VarChar;

                case "ntext":

                    return SqlDbType.NText;


                case "decimal":
                case "numeric":
                    return SqlDbType.Decimal;

                case "real":
                    return SqlDbType.Real;

                case "smallInt":
                    return SqlDbType.SmallInt;

                case "tinyInt":
                    return SqlDbType.TinyInt;

                case "varbinary":
                    return SqlDbType.VarBinary;

                case "varchar":
                    return SqlDbType.VarChar;


                case "sql_variant":
                    return SqlDbType.Variant;


                case "nvarchar":
                    return SqlDbType.NVarChar;

                case "nchar":
                    return SqlDbType.NChar;


                case "char":
                    return SqlDbType.Char;


                case "float":
                    return SqlDbType.Float;

                default:
                    throw new Exception("Unknown data type '" + paramType.Trim() + "' encountered.");

            }

        }


        private void SettingforSQLCommand(SqlConnection cn, string updateName, CommandType typeSqlCommand, SqlTransaction TR, SqlCommand someSqlCoomand, DataTable Posting, ArrayList namesParam)
        {
            someSqlCoomand.Connection = cn;
            someSqlCoomand.CommandText = updateName;
            someSqlCoomand.CommandType = typeSqlCommand;
            someSqlCoomand.Transaction = TR;
            parameterscomand(someSqlCoomand, Posting, namesParam);
        }




        private void SettingforSQLCommandforDelete
            (SqlConnection cn, string updateName, CommandType typeSqlCommand,
            SqlTransaction TR, SqlCommand someSqlCoomand, DataTable Posting, string key)
        {
            someSqlCoomand.Connection = cn;
            someSqlCoomand.CommandText = updateName;
            someSqlCoomand.CommandType = typeSqlCommand;
            someSqlCoomand.Transaction = TR;
            DataColumn dc = Posting.Columns[key];
            parameterForSQLCommand(someSqlCoomand, dc);
        }





        // test decision

        //  add  decision  
        // input data - sertain combination of nodes on client
        // output  registred decision according to this logic  
        // nice to have only one row , really ?
        // one combination - one decision

    

        //
     










     




































        public void FSM_GetSectionDecision(int IDSection, int IDDocument, DataSet ds)
        {
            try
            {

                SqlDataAdapter daDictionary = new SqlDataAdapter();
                SqlCommand sqlGetDataset = new System.Data.SqlClient.SqlCommand();
                sqlGetDataset.CommandText = "dbo.FSM_GetSectionDecision";
                SqlParameter pIDSection = new SqlParameter();
                pIDSection.SqlDbType = SqlDbType.Int;
                pIDSection.ParameterName = "@IDSection";
                pIDSection.Value = IDSection;

                sqlGetDataset.Parameters.Add(pIDSection);

                SqlParameter pIDDocument = new SqlParameter();
                pIDDocument.SqlDbType = SqlDbType.Int;
                pIDDocument.ParameterName = "@IDDocument";
                pIDDocument.Value = IDDocument;

                sqlGetDataset.Parameters.Add(pIDDocument);
                // first parametr
                daDictionary.TableMappings.Add("Table", "FSM_Decision");
                daDictionary.TableMappings.Add("Table1", "FSM_SummarySectionNode");
                daDictionary.TableMappings.Add("Table2", "FSM_SummarySectionNodeDetail");

                sqlGetDataset.CommandType = System.Data.CommandType.StoredProcedure;
                sqlGetDataset.Connection = cn;
                daDictionary.SelectCommand = sqlGetDataset;
                ds.Clear();
                daDictionary.Fill(ds);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


    }
}
