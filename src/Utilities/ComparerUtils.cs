﻿using System;

namespace Corpt.Utilities
{
    public static class ComparerUtils
    {
        public static int NumberCompare(int n1, int n2)
        {
            int result;
            if (n1 > n2)
            {
                result = 1;
            }
            else if (n1 < n2)
            {
                result = -1;
            }
            else
            {
                result = 0;
            }
            return result;
        }
        public static int StringCompare(string s1, string s2)
        {
            int result;
            if (s1 == null)
            {
                result = s2 == null ? 0 : 1;
            }
            else
            {
                result = String.Compare(s1, s2, StringComparison.Ordinal);
            }
            return result;
        }
    }

}