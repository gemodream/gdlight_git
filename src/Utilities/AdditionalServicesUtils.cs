﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Corpt.Models;

namespace Corpt.Utilities
{
    public class AdditionalServicesUtils : BaseQueryUtils
    {
        #region Additional Services

        // ReSharper disable once InconsistentNaming
        public static DataSet GetBatchRecheckChangeSKU(string fullBatchCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetAdditionalServiceBatchWise",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            var iGroupCode = Utils.ParseOrderCode(fullBatchCode);
            var iBatchCode = Utils.ParseBatchCode(fullBatchCode);

            command.Parameters.AddWithValue("@OrderCode", iGroupCode);
            command.Parameters.AddWithValue("@BatchCode", iBatchCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }

        // ReSharper disable once InconsistentNaming
        public static bool SetBatchRecheckChangeSKU(AdditionalServicesModel additionalServicesModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spSetAdditionalServiceBatchWise",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@ReportNumber", additionalServicesModel.ReportNumber));

            command.Parameters.Add(new SqlParameter("@BatchID", additionalServicesModel.BatchID));
            command.Parameters.Add(new SqlParameter("@GroupID", additionalServicesModel.GroupID));
            command.Parameters.Add(new SqlParameter("@CustomerID", additionalServicesModel.CustomerID));
            command.Parameters.Add(new SqlParameter("@CPID", additionalServicesModel.CPID));
            command.Parameters.Add(new SqlParameter("@CPName", additionalServicesModel.CustomerProgramName));

            command.Parameters.Add(new SqlParameter("@IsRecheck", additionalServicesModel.IsRecheck));
            command.Parameters.Add(new SqlParameter("@IsChangeSKU", additionalServicesModel.IsSKUChange));
            command.Parameters.Add(new SqlParameter("@IsUrgent", additionalServicesModel.IsUrgent));
            command.Parameters.Add(new SqlParameter("@IsColorStone", additionalServicesModel.IsColorStone));

            command.Parameters.Add(new SqlParameter("@IsRePrint", additionalServicesModel.IsRePrint));
            command.Parameters.Add(new SqlParameter("@IsTND", additionalServicesModel.IsTND));
            command.Parameters.Add(new SqlParameter("@IsLaserInscription", additionalServicesModel.IsLaserInscription));
            command.Parameters.Add(new SqlParameter("@IsLaserRemoval", additionalServicesModel.IsLaserRemoval));
            command.Parameters.Add(new SqlParameter("@IsUrgent48", additionalServicesModel.IsUrgent48));

            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return false;
            }
            conn.Close();
            return true;
        }

        // ReSharper disable once InconsistentNaming
        public static bool SetRecheckChangeSKUPrice(AdditionalServicesModel additionalServicesModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spSetAdditionalServiceSKUPrice",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@CustomerID", additionalServicesModel.CustomerID));
            command.Parameters.Add(new SqlParameter("@CPID", additionalServicesModel.CPID));
            command.Parameters.Add(new SqlParameter("@RecheckPrice", additionalServicesModel.RecheckPrice));
            command.Parameters.Add(new SqlParameter("@ChangeSKUPrice", additionalServicesModel.ChangeSKUPrice));
            command.Parameters.Add(new SqlParameter("@UrgentServicePrice", additionalServicesModel.UrgentServicePrice));
            command.Parameters.Add(new SqlParameter("@ColorStonePrice", additionalServicesModel.ColorStonePrice));
            command.Parameters.Add(new SqlParameter("@RePrintPrice", additionalServicesModel.RePrintPrice));
            command.Parameters.Add(new SqlParameter("@TNDPrice", additionalServicesModel.TNDPrice));
            command.Parameters.Add(new SqlParameter("@LaserInscriptionPrice", additionalServicesModel.LaserInscriptionPrice));
            command.Parameters.Add(new SqlParameter("@LaserRemovalPrice", additionalServicesModel.LaserRemovalPrice));
            command.Parameters.Add(new SqlParameter("@UrgentService48Price", additionalServicesModel.UrgentService48Price));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return false;
            }
            conn.Close();
            return true;
        }

        #endregion
    }
}