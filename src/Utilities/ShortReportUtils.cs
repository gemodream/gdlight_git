﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Corpt.Constants;
using Corpt.Models;
using Mathos.Parser;

namespace Corpt.Utilities
{
    public class ShortReportUtils
    {
        public ShortReportUtils(bool hideInternalComments)
        {
            HideInternalComments = hideInternalComments;
        }

        public bool HideInternalComments { get; set; }

        #region Short Report II
        public DataTable GetReportViewByGroup(int grp, string dir, List<ShortReportModel> shortReportList)
        {
            if (shortReportList == null || shortReportList.Count == 0) return null;
            var grpShortReportList = shortReportList.FindAll(model => model.DocStructGrp == grp);

            //-- Data Collection
            foreach (var shortReportModel in grpShortReportList)
            {
                //--  Report View
                CreateMergedItemStructValueTable(shortReportModel);
            }
            var table = grpShortReportList[0].ReportView.Copy();
            table.TableName = "ShortReport";
            for (var i = 1; i < grpShortReportList.Count; i++)
            {
                var addTbl = grpShortReportList[i].ReportView.Copy();
                foreach (DataRow row in addTbl.Rows)
                {
                    table.Rows.Add(row.ItemArray);

                }
            }
            table.AcceptChanges();
            return dir == PageConstants.DirVertical ? Utlities.CreateTableVertic(table) : table;
        }
        #endregion
        
        #region Leo Short Report
        public DataTable GetLeoReport(List<ShortReportModel> shortReportList, string dir, bool hideEmpty, bool hideErr)
        {
            if (shortReportList == null || shortReportList.Count == 0) return null;
            var table = new DataTable("LeoReport");
            //-- Create columns
            var columns = new List<string>();
            foreach (var sr in shortReportList)
            {
                foreach (var ds in sr.DocStructure)
                {
                    if (!columns.Contains(ds.Title))
                    {
                        columns.Add(ds.Title);
                    }
                }
            }

            //-- Create HLink column on ViewItem Details page
            table.Columns.Add(PageConstants.ColNameLinkOnItemView);

            //-- Create column Ordered
            table.Columns.Add(PageConstants.ColNameLeoOrdered);

            //-- Create column Printed
            //table.Columns.Add(PageConstants.ColNameLeoPrinted);

            //-- Create Columns by Document Structure
            table.Columns.Add(new DataColumn("Result"));

            foreach (var col in columns)
            {
                table.Columns.Add(col);
            }
            foreach (var shortReportModel in shortReportList)
            {
                var itemsFailed = hideErr ? new List<ItemFailedModel>() : shortReportModel.ItemsFailed;
                table.AcceptChanges();

                //-- Set data in columns
                foreach (ItemModel itemModel in shortReportModel.Items)
                {
                    if (itemModel.SkipOnGeneration) continue;
                    var strNewItemNumber = itemModel.FullItemNumber;
                    var row = table.NewRow();
                    //-- Add Hlink on ItemView
                    var sLink = string.Format(PageConstants.LinkOnPageItemViewDetail,
                        shortReportModel.BatchModel.BatchId, itemModel.ItemCode, itemModel.FullItemNumberWithDotes);
                    row[PageConstants.ColNameLinkOnItemView] = "<a href=" + sLink + ">" + itemModel.FullItemNumberWithDotes + "</a>";
                    row[PageConstants.ColNameLeoOrdered] = itemModel.LeoOrdered;
                    

                    foreach (DocStructModel docStructModel in shortReportModel.DocStructure)
                    {
                        if (docStructModel.IsCutGrade)
                        {
                            var cutGradeValue = Utlities.ParseCutGrade(itemModel, docStructModel.Value, strNewItemNumber, shortReportModel.ItemValues, itemsFailed);
                            
                            row[docStructModel.Title] = cutGradeValue;
                        }
                        else
                        {
                            var cellValue = ParseValue(
                                docStructModel.Value, strNewItemNumber,
                                shortReportModel.ItemValues, itemsFailed);

                            row[docStructModel.Title] = cellValue;

                        }
                    }
                    if (itemsFailed.Count > 0)
                    {
                        //var val = row.ToString().ToLower();
                        //row["Result"] = @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                        //-- Get DottedItemNumber
                        var itemNumber = Regex.Replace(row[PageConstants.ColNameLinkOnItemView].ToString(), @"<[^>]+>", "");
                        foreach (var itemFailed in itemsFailed)
                        {
                            if (itemFailed.ItemNumberWithDotes == itemNumber)
                            {
                                var val = row.ToString().ToLower();
                                row["Result"] = @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                                row["Result"] = row["Result"].ToString().Replace("Fail",
                            "<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber=" + itemNumber + "\" target=\"_blank\">Fail</a>");
                                break;
                            }
                        }
                        //row["Result"] = row["Result"].ToString().Replace("Fail",
                        //    "<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber=" + itemNumber + "\" target=\"_blank\">Fail</a>");
                    }
                    table.Rows.Add(row);
                }
                //-- Result column value
                //SetFailColumnRedirect(table);

            }
            table.AcceptChanges();
            if (hideEmpty)
            {
                for (var c = table.Columns.Count - 1; c > 0; c--)
                {
                    var dataColumn = table.Columns[c];
                    var empty = true;
                    for (var r = 0; r < table.Rows.Count; r++)
                    {
                        var val = ("" + table.Rows[r][c]).Trim();
                        val = Regex.Replace(val, @"<[^>]+>", "");
                        if (string.IsNullOrEmpty(val)) continue;
                        empty = false;
                        break;
                    }
                    if (empty)
                    {
                        table.Columns.Remove(dataColumn);
                    }
                }
                table.AcceptChanges();
            }
            return dir == PageConstants.DirVertical ? Utlities.CreateTableVertic(table) : table;
        }

        #endregion

        #region Short Report Utils
        public void CreateMergedItemStructValueTable(ShortReportModel shortReportModel)
        {
            var table = new DataTable();
            //-- Create HLink column on ViewItem Details page
            table.Columns.Add(PageConstants.ColNameLinkOnItemView);

            //-- Create Columns by Document Structure
            table.Columns.Add(new DataColumn("Result"));
            foreach (DocStructModel docModel in shortReportModel.DocStructure)
            {
                if (!table.Columns.Contains(docModel.Title))
                {
                    table.Columns.Add(new DataColumn(docModel.Title));
                }
            }
            table.AcceptChanges();

            //-- Set data in columns
            foreach (var itemModel in shortReportModel.Items)
            {
                var strNewItemNumber = itemModel.FullItemNumber;
                var row = table.NewRow();
                //-- Add Hlink on ItemView
                var sLink = string.Format(PageConstants.LinkOnPageItemViewDetail,
                    shortReportModel.BatchModel.BatchId, itemModel.ItemCode, itemModel.FullItemNumberWithDotes);
                row[PageConstants.ColNameLinkOnItemView] = "<a href=" + sLink + ">" + itemModel.FullItemNumberWithDotes + "</a>";

                foreach (DocStructModel docStructModel in shortReportModel.DocStructure)
                {
                    if (docStructModel.IsCutGrade)
                    {
                        var sPartName = Utlities.GetPartName(docStructModel.Value);
                        var sPartId = Utlities.ParsePartIdValue(
                            docStructModel.Value, strNewItemNumber,
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);
                        var myRequest =
                            "<a href=\"CutGradeDetail.aspx?par1=" + itemModel.OrderCode +
                            "&par2=" + itemModel.BatchCode +
                            "&par3=" + itemModel.ItemCode +
                            "&par4=" + sPartId +
                            "&par5=" + sPartName + "\">" +
                            ParseValue(docStructModel.Value, strNewItemNumber, shortReportModel.ItemValues, shortReportModel.ItemsFailed) +
                            "</a>";
                        row[docStructModel.Title] = myRequest;
                    }
                    else
                    {
                        row[docStructModel.Title] = ParseValue(
                            docStructModel.Value, strNewItemNumber,
                            shortReportModel.ItemValues, shortReportModel.ItemsFailed);

                    }
                }
                //alex 04062021 add link to failed item similar to ShortReportExt
                if (shortReportModel.ItemsFailed.Count > 0)
                {
                    //var val = row.ToString().ToLower();
                    //row["Result"] = @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                    //-- Get DottedItemNumber
                    var itemNumber = Regex.Replace(row[PageConstants.ColNameLinkOnItemView].ToString(), @"<[^>]+>", "");
                    foreach (var itemFailed in shortReportModel.ItemsFailed)
                    {
                        if (itemFailed.ItemNumberWithDotes == itemNumber)
                        {
                            var val = row.ToString().ToLower();
                            row["Result"] = @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                            row["Result"] = row["Result"].ToString().Replace("Fail",
                        "<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber=" + itemNumber + "\" target=\"_blank\">Fail</a>");
                            break;
                        }
                    }
                    //row["Result"] = row["Result"].ToString().Replace("Fail",
                    //    "<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber=" + itemNumber + "\" target=\"_blank\">Fail</a>");
                }
                table.Rows.Add(row);
            }
            table.AcceptChanges();
            //-- Result column value
            //Utlities.SetFailColumn(table);
            shortReportModel.ReportView = table.Copy();
        }

        public string ParseValue(string strValue, string strItemNumber, List<ItemValueModel> dtItemValueList, List<ItemFailedModel> dtCpResult)
        {
            var strResult = "";
            var strPartName = "";
            var strPropertyName = "";
            var insidePartName = false;
            var insidePropertyName = false;
            bool isLink = false;

            foreach (char ch in strValue)
            {
                if ((ch != '[') && (!insidePartName) && (!insidePropertyName))
                {
                    strResult += ch;
                    continue;
                }

                if ((ch == '['))
                {
                    insidePartName = true;
                    continue;
                }

                if ((ch != '.') && insidePartName)
                {
                    strPartName += ch;
                    continue;
                }

                if ((ch == '.') && insidePartName)
                {
                    insidePartName = false;
                    insidePropertyName = true;
                    continue;
                }

                if ((ch != ']') && insidePropertyName)
                {
                    strPropertyName += ch;
                    continue;
                }
                if ((ch == ']') && insidePropertyName)
                {
                    insidePropertyName = false;
                    strResult += ParseSingleValue(strPartName, strPropertyName, strItemNumber, dtItemValueList, dtCpResult);
                    strResult = strResult.TrimStart('-');
                    if (strPropertyName == "WebPath2Picture")
                        isLink = true;
                    strPartName = "";
                    strPropertyName = "";
                }
            }
            if (VerifyDelimOnly(strResult)) return "";
            //if (strResult.Contains("+"))
            if (strResult.Contains("+") && !(strValue.ToUpper()).Contains("LOT NUMBER"))
            {
                var str = Regex.Replace(strResult, @"<[^>]+>", "");
                //str = "{0.25+0.28} Ct";
                var p1 = str.IndexOf('{');
                var p2 = str.IndexOf('}');
                if (p1 != -1 && p2 != -1)
                {
                    var forReplace = str.Substring(p1, p2 - p1 + 1);
                    var onReplace = CalcExpr(str.Substring(p1 + 1, p2 - p1 - 1));
                    str = str.Replace(forReplace, onReplace);
                } else
                {
                    str = CalcExpr(str);
                }
                int strLn = str.Length;
                if (strLn > 0 && str[strLn - 1] == '+')
                {
                    if (str.Length == 1)
                        str = "";
                    else
                        str = str.Substring(0, strLn - 2);
                }
                //else if (strLn > 0 && str[strLn - 2] == '+' && str[strLn - 1] == ' ')
                else if (str.Contains("+") && (strLn > 0 && str[strLn - 2] == '+' && str[strLn - 1] == ' '))
                    str = str.Substring(0, strLn - 3);
                if (strResult.Contains("yellow") && !str.Contains("yellow"))
                    str = "<SPAN class=\"text_highlitedyellow\">" + str.Trim() + "</SPAN>";
                return str.Trim();
            }
            while (strResult.Contains(@"/   /"))
                strResult = strResult.Replace(@"/   /", @"/");
            while (strResult.Contains(@"/  /"))
                strResult = strResult.Replace(@"/  /", @"/");
            while (strResult.Contains(@"//") && !isLink)//don't replace for // in URL
                strResult = strResult.Replace(@"//", @"/");
            while (strResult.StartsWith(" "))
                strResult = strResult.TrimStart(' ');
            while (strResult.StartsWith(@"/"))
                strResult = strResult.TrimStart('/');
            while (strResult.EndsWith(" "))
                strResult = strResult.TrimEnd(' ');
            if (strResult.EndsWith(@"/"))
                strResult = strResult.TrimEnd('/');
            return strResult.Trim();
        }
        private static string CalcExpr(string expr)
        {
            var result = expr;
            try
            {
                return Convert.ToString(new MathParser().Parse(expr));
            } catch(Exception x)
            {
                return result;
            }
        }
        private static bool VerifyDelimOnly(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;
            if (value.IndexOf("highlitedyellow", StringComparison.Ordinal) != -1) return false;
            var val = Regex.Replace(value, @"<[^>]+>", "");
            return IsDelimOnly(val, '/') || IsDelimOnly(val, ',');
        }
        private static bool IsDelimOnly(string val, char delim)
        {
            var items = val.Split(delim);
            return !items.Any(item => item.Trim().Length > 0);
        }
        public string ParseSingleValue(string strPartName, string strPropertyName, string strItemNumber, List<ItemValueModel> itemValueList, List<ItemFailedModel> itemFailedList)
        {
            if (strPropertyName.ToLower() == "item #")
            {
                //get old item number
                var itemNames = itemValueList.FindAll(model => model.NewItemNumber == strItemNumber);
                if (itemNames.Count == 0)
                    return null;
                return itemNames[0].OldItemNumber;
            }
            var drValues = itemValueList.FindAll(model =>
                model.NewItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drValues.Length == 0 || (drValues[0].MeasureCode == DbConstants.MeasureCodeInternalComment && HideInternalComments))
            {
                return "";
            }
            //check here for cprules and mark accordingly
            if (strPropertyName.ToUpper().Trim() == "FULLSHAPENAME") strPropertyName = "Shape (cut)";
            var strResultValue = drValues[0].ResultValue;
            var drCp = itemFailedList.FindAll(model =>
                model.ItemNumber == strItemNumber &&
                model.PartName.ToUpper() == strPartName.ToUpper() &&
                model.MeasureName.ToUpper() == strPropertyName.ToUpper()).ToArray();
            if (drCp.Length > 0)
            {
                if (strResultValue.Trim() == "") strResultValue = "_None";
                strResultValue = "<SPAN class=\"text_highlitedyellow\">" + strResultValue + "</SPAN>";
            }

            return strResultValue;
        }
        public static void SetFailColumnRedirect(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                for (var i = 0; i < table.Columns.Count; i++)
                {
                    var val = row[i].ToString().ToLower();
                    if (val.IndexOf("highlitedyellow", StringComparison.Ordinal) == -1) continue;
                    row["Result"] = @"<SPAN class=text_highlitedyellow>Fail</SPAN>";
                    //-- Get DottedItemNumber
                    var itemNumber = Regex.Replace(row[PageConstants.ColNameLinkOnItemView].ToString(), @"<[^>]+>", "");
                    row["Result"] = row["Result"].ToString().Replace("Fail",
                        "<a href=\"LeoReportToCpRulesRedirect.aspx?ItemNumber=" + itemNumber + "\" target=\"_blank\">Fail</a>");
                    row.AcceptChanges();
                    break;
                }
            }
        }

        #endregion

    }

}