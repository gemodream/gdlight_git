﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Models.Customer;
using Corpt.TreeModel;

namespace Corpt.Utilities
{
    public class TreeUtils
    {
        #region Main Menu items

        public const string Format = "<a href='{0}'>{1}</a>";
        public const string CenterId = "2";
        public const string ShortReportId = "3";
        public const string LookupsId = "4";
        public const string HistoryId = "5";
        public const string CustomerId = "6";
        public const string CpId = "7";
        public const string StatsId = "8";
        public const string GdlId = "9";
        public const string VvId = "10";
        public const string Screening = "13";
        public const string OptionsId = "11";
        public const string TestId = "12";
        public const string ScreeningNewId = "14";

        public static List<TreeViewModel> GetTreeMenuData(bool accessBulkUpdate, bool accessReassembly, bool accessItemHistory, bool accessRemoteSession, bool accessAutoMeasure)
        {
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "1", DisplayName = "Menu" } };

            //-- Middle
            data.Add(new TreeViewModel { ParentId = "1", Id = CenterId, DisplayName = String.Format(Format, "Middle.aspx", "G D L i g h t") });

            //-- SubMenu Short Report
            data.Add(new TreeViewModel { ParentId = "1", Id = ShortReportId, DisplayName = "Short Reports" });
            data.Add(new TreeViewModel { ParentId = ShortReportId, Id = "3_1", DisplayName = String.Format(Format, "ShortReport.aspx", "Short Report") });
            data.Add(new TreeViewModel { ParentId = ShortReportId, Id = "3_2", DisplayName = String.Format(Format, "ShortReportII.aspx", "Full Order") });
            data.Add(new TreeViewModel { ParentId = ShortReportId, Id = "3_3", DisplayName = String.Format(Format, "LeoReport2.aspx", "Leo Short Report") });
            data.Add(new TreeViewModel { ParentId = ShortReportId, Id = "3_4", DisplayName = String.Format(Format, "ShortReportExt.aspx", "Short Report New *") });

            //-- SubMenu Lookups
            data.Add(new TreeViewModel { ParentId = "1", Id = LookupsId, DisplayName = "Lookups" });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_1", DisplayName = String.Format(Format, "ReportLookup.aspx", "Report Lookup") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_2", DisplayName = String.Format(Format, "LotNumber.aspx", "Shipping Lookup") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_3", DisplayName = String.Format(Format, "MemoLookup.aspx", "By Memo Number") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_4", DisplayName = String.Format(Format, "FindItemByMeasureValue.aspx", "By Measure Value") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_5", DisplayName = String.Format(Format, "GetInvoiceNumberByOrderNumber.aspx", "Invoice by Order") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_6", DisplayName = String.Format(Format, "BelongsTo.aspx", "Customer by Report") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_7", DisplayName = String.Format(Format, "PSX.aspx", "Photospex") });
            data.Add(new TreeViewModel { ParentId = LookupsId, Id = "4_8", DisplayName = String.Format(Format, "DeletedItemsLookup.aspx", "Deleted Items") });

            //-- SubMenu History
            data.Add(new TreeViewModel { ParentId = "1", Id = HistoryId, DisplayName = "History" });
            if (accessItemHistory)
            {
                data.Add(new TreeViewModel { ParentId = HistoryId, Id = "5_2", DisplayName = String.Format(Format, "ItemHistory.aspx", "Item History") });
            }
            data.Add(new TreeViewModel { ParentId = HistoryId, Id = "5_3", DisplayName = String.Format(Format, "Tracking.aspx", "Tracking") });
            data.Add(new TreeViewModel { ParentId = HistoryId, Id = "5_4", DisplayName = String.Format(Format, "Tracking2.aspx", "Tracking II") });
            data.Add(new TreeViewModel { ParentId = HistoryId, Id = "5_5", DisplayName = String.Format(Format, "DeliveryLog2.aspx", "Delivery Log") });

            //-- SubMenu Customer
            data.Add(new TreeViewModel { ParentId = "1", Id = CustomerId, DisplayName = "Customer" });
            data.Add(new TreeViewModel { ParentId = CustomerId, Id = "6_1", DisplayName = String.Format(Format, "CustomerEdit1.aspx", "Maintenance") });
            data.Add(new TreeViewModel { ParentId = CustomerId, Id = "6_2", DisplayName = String.Format(Format, "CustomerHistory.aspx", "History") });

            //-- Customer Program
            data.Add(new TreeViewModel { ParentId = "1", Id = CpId, DisplayName = "Customer Program" });
            data.Add(new TreeViewModel { ParentId = CpId, Id = "7_1", DisplayName = String.Format(Format, "CPResult.aspx", "CP Rules") });
            data.Add(new TreeViewModel { ParentId = CpId, Id = "7_2", DisplayName = String.Format(Format, "CPRulesDisplay2.aspx", "CP Overview") });
            data.Add(new TreeViewModel { ParentId = CpId, Id = "7_3", DisplayName = String.Format(Format, "CustomerProgramNew.aspx", "Customer Program *") });

            //-- Reports
            data.Add(new TreeViewModel { ParentId = "1", Id = StatsId, DisplayName = "Reports & Stats" });
            if (accessRemoteSession)
            {
                data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_1", DisplayName = String.Format(Format, "RemoteSession.aspx", "Remote Session") });
            }
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_2", DisplayName = String.Format(Format, "StoneQuantities.aspx", "Stone Quantities") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_3", DisplayName = String.Format(Format, "ReportQuantities.aspx", "Reports Quantity") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_13", DisplayName = String.Format(Format, "StatsCalcPage.aspx", "Rejected Stones") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_5", DisplayName = String.Format(Format, "ItemCycle.aspx", "Old/New orders ##") });

            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_6", DisplayName = String.Format(Format, "ListOpenBatches.aspx", "Open Batch List") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_7", DisplayName = String.Format(Format, "PrintedNot.aspx", "Reports in an Order") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_8", DisplayName = String.Format(Format, "OpenOrders.aspx", "Open Orders") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_9", DisplayName = String.Format(Format, "BillingInfo.aspx", "Billing Info") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_10", DisplayName = String.Format(Format, "SortingStat.aspx", "Sorting Stat") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_11", DisplayName = String.Format(Format, "PrvStat2.aspx", "Color/Clarity Stat") });
            data.Add(new TreeViewModel { ParentId = StatsId, Id = "8_12", DisplayName = String.Format(Format, "CustomerVendorCp.aspx", "Customer/Vendor") });

            //-- GDLight
            data.Add(new TreeViewModel { ParentId = "1", Id = GdlId, DisplayName = "GDLight" });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_24", DisplayName = String.Format(Format, "ScreeningFront.aspx?Mode=End", "Front") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_1", DisplayName = String.Format(Format, "MarkLaser2.aspx", "Mark Laser") });

            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_2", DisplayName = String.Format(Format, "NewOrder.aspx", "New Order") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_16", DisplayName = String.Format(Format, "UpdateOrder.aspx", "Order Update") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_17", DisplayName = String.Format(Format, "BillingInvoice.aspx", "Billing") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_3", DisplayName = String.Format(Format, "Itemize1.aspx", "Itemize") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_16", DisplayName = String.Format(Format, "ReItemizn.aspx", "ReItemize") });

			if (accessAutoMeasure)
			{
				data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_4", DisplayName = String.Format(Format, "RMeas.aspx", "AutoMeasure") });
			}

            //data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_5", DisplayName = String.Format(Format, "Grade1.aspx?Mode=Color", "Color") });
			data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_21", DisplayName = String.Format(Format, "Color2.aspx", "Color *") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_23", DisplayName = String.Format(Format, "ColorStones.aspx", "Color Stones") });
            //data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_6", DisplayName = String.Format(Format, "Grade1.aspx?Mode=Clarity", "Clarity") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_20", DisplayName = String.Format(Format, "Clarity2.aspx", "Clarity *")});
            //data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_7", DisplayName = String.Format(Format, "Grade1.aspx?Mode=Measure", "Measure") });
			data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_22", DisplayName = String.Format(Format, "Measure2.aspx", "Measure *") });
            //data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_8", DisplayName = String.Format(Format, "GradeII.aspx", "Grade II") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_18", DisplayName = String.Format(Format, "Grade2.aspx", "Grade II *") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_19", DisplayName = String.Format(Format, "Appraisal.aspx", "Appraisal") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_20", DisplayName = String.Format(Format, "AccountRep.aspx", "Account Representative") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_21", DisplayName = String.Format(Format, "ExpressGrading1.aspx", "Bulk Grading") });
            if (accessReassembly)
            {
                data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_9", DisplayName = String.Format(Format, "Reassembly.aspx", "Reassembly") });
            }
            if (accessBulkUpdate)
            {
                data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_10", DisplayName = String.Format(Format, "BulkUpdateAsyncNew.aspx", "Bulk Update *") });
            }
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_11", DisplayName = String.Format(Format, "PreFillingMeasures.aspx", "Pre-Filling") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_12", DisplayName = String.Format(Format, "Remeasure.aspx", "Remeasure") });

            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_12", DisplayName = String.Format(Format, "PrintLabels.aspx?Mode=Certified", "Print Cetrified") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_13", DisplayName = String.Format(Format, "PrintLabels.aspx?Mode=Itemized", "Print Itemized") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_14", DisplayName = String.Format(Format, "NumberGenerator.aspx", "Number Generator") });
            data.Add(new TreeViewModel { ParentId = GdlId, Id = "9_15", DisplayName = String.Format(Format, "ReItemizn.aspx?Mode=End", "End Session") });
            
            //Screening
            //data.Add(new TreeViewModel { ParentId = "1", Id = Screening, DisplayName = "Screening" });
            //data.Add(new TreeViewModel { ParentId = Screening, Id = Screening + "_1", DisplayName = String.Format(Format, "SyntheticScreening.aspx", "Synthetic Screening") });
            //data.Add(new TreeViewModel { ParentId = Screening, Id = Screening + "_2", DisplayName = String.Format(Format, "ScreeningFrontEntriesNew.aspx", "Synthetic Front *") });
            //data.Add(new TreeViewModel { ParentId = Screening, Id = Screening + "_3", DisplayName = String.Format(Format, "SyntheticNewEntries.aspx", "Synthetic New") });
            //data.Add(new TreeViewModel { ParentId = Screening, Id = Screening + "_4", DisplayName = String.Format(Format, "SyntheticShortStats.aspx", "Synthetic Statistics *") });

            //Screening New
            data.Add(new TreeViewModel { ParentId = "1", Id = ScreeningNewId, DisplayName = "Screening" });
            data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_1", DisplayName = String.Format(Format, "ScreeningFront.aspx", "Front") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_2", DisplayName = String.Format(Format, "ScreeningBatchCreation.aspx", "Batch Creation") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_3", DisplayName = String.Format(Format, "ScreeningDistribution.aspx", "Distribution") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_4", DisplayName = String.Format(Format, "ScreeningScreener.aspx", "Screener") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_5", DisplayName = String.Format(Format, "ScreeningTester.aspx", "Tester") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_6", DisplayName = String.Format(Format, "ScreeningPacking.aspx", "Packing") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_7", DisplayName = String.Format(Format, "ScreeningItemizing.aspx", "Itemizing") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_8", DisplayName = String.Format(Format, "ScreeningManager.aspx", "Manager") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_9", DisplayName = String.Format(Format, "ScreeningStatisticReport.aspx", "Statistics Report") });
            //data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_10", DisplayName = String.Format(Format, "ScreeningDataView.aspx", "Data View") });
            data.Add(new TreeViewModel { ParentId = ScreeningNewId, Id = ScreeningNewId + "_11", DisplayName = String.Format(Format, "ScreeningUtilities.aspx", "Utilities *") });

            //-- VVAdmin
            data.Add(new TreeViewModel { ParentId = "1", Id = VvId, DisplayName = "VV Admin" });
            data.Add(new TreeViewModel { ParentId = VvId, Id = VvId + "_1", DisplayName = String.Format(Format, "VVAdmin1.aspx", "Verify VV Registration") });
            data.Add(new TreeViewModel { ParentId = VvId, Id = VvId + "_2", DisplayName = String.Format(Format, "NewVVActivation.aspx", "New User") });

            //-- Settings
            data.Add(new TreeViewModel { ParentId = "1", Id = OptionsId, DisplayName = "Settings" });
            data.Add(new TreeViewModel { ParentId = OptionsId, Id = OptionsId + "_1", DisplayName = String.Format(Format, "Login.aspx", "Login") });
            data.Add(new TreeViewModel { ParentId = OptionsId, Id = OptionsId + "_2", DisplayName = String.Format(Format, "ChangeUserPassword.aspx", "Change Password") });

            //-- Test Pages
            data.Add(new TreeViewModel { ParentId = "1", Id = TestId, DisplayName = "Test Pages" });
            data.Add(new TreeViewModel { ParentId = TestId, Id = TestId + "_1", DisplayName = String.Format(Format, "EmailTest.aspx", "Send Email") });
            data.Add(new TreeViewModel { ParentId = TestId, Id = TestId + "_2", DisplayName = String.Format(Format, "SessionParams.aspx", "Session params") });
            data.Add(new TreeViewModel { ParentId = TestId, Id = TestId + "_3", DisplayName = String.Format(Format, "GetFullOrder.aspx", "Test Full Order") });
            return data;
        }

        #endregion
        public static void CheckNodes(TreeNode node, List<string> ids)
        {
            if (ids.Contains(node.Value)) node.Checked = true;
            if (node.ChildNodes.Count <= 0) return;
            foreach(TreeNode child in node.ChildNodes)
            {
                CheckNodes(child, ids);
            }
        }
        public static void FillNode(TreeNode node, NodeTreeViewModel root)
        {
            FillNode(node, root, true, TreeNodeSelectAction.Select);
        }
        public static void FillNode(TreeNode node, NodeTreeViewModel root, bool expand, TreeNodeSelectAction action)
        {
            if (root.Childs.Count <= 0) return;
            foreach (var child in root.Childs)
            {
                var addNode = new TreeNode(child.DisplayName, child.Id);
                addNode.SelectAction = action;
                FillNode(addNode, child, expand, action);
                node.ChildNodes.Add(addNode);
                node.Expanded = expand;
            }
        }
        public static NodeTreeViewModel GetRootTreeModel(string id, List<TreeViewModel> parts)
        {
            var root = parts.Find(m => m.Id == id);
            var rootNode = root == null ? null : new NodeTreeViewModel(root);
            if (rootNode != null)
            {
                GetTreeChild(parts, rootNode);
            }
            return rootNode;
        }
        public static NodeTreeViewModel GetRootTreeModel(List<TreeViewModel> parts)
        {
            var root = parts.Find(m => !m.HasParent);
            var rootNode = root == null ? null : new NodeTreeViewModel(root);
            if (rootNode != null)
            {
                GetTreeChild(parts, rootNode);
            }
            return rootNode;
        }
        public static void GetTreeChild(List<TreeViewModel> parts, NodeTreeViewModel root)
        {
            var childs = parts.FindAll(m => m.ParentId == root.Id);
            foreach (var subItem in childs.Select(child => new NodeTreeViewModel(child)))
            {
                subItem.Parent = root;
                root.Childs.Add(subItem);
                GetTreeChild(parts, subItem);
            }
        }
        public static void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items, TreeView treeHistory)
        {
            ShowTreeHistory(items, treeHistory, TreeNodeSelectAction.None);
        }
        public static void ShowTreeHistory(IEnumerable<CustomerHistoryModel> items, TreeView treeHistory, TreeNodeSelectAction selectAction)
        {
            treeHistory.Nodes.Clear();
            var customerHistoryModels = items as CustomerHistoryModel[] ?? items.ToArray();
            treeHistory.Visible = customerHistoryModels.Length > 0;
            
            if (customerHistoryModels.Length == 0) return;
            var data = new List<TreeViewModel>();
            foreach (var item in customerHistoryModels)
            {
                data.Add(
                    new TreeViewModel
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        DisplayName = item.Title
                    });
            }
            var root = GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id) { SelectAction = selectAction };

            FillNode(rootNode, root, true, selectAction);
            rootNode.Expand();

            treeHistory.Nodes.Add(rootNode);

        }

    }
}