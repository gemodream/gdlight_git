﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;

namespace Corpt.Utilities
{
    public class QueryCpUtilsNew
    {
        #region Define Document
        public static string AttachDocumentToCp(string documentId, string cpId, string cpOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetDocument_CP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 255));
            command.Parameters["@rId"].Direction = ParameterDirection.Output;

            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.AddWithValue("@CPOfficeID", cpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]); //TODO ???
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();

            return msg;

        }
        public static bool IsAttachedDocument(string documentId, string cpId, string cpOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetDocument_CP1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.AddWithValue("@CPOfficeID", cpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var attached = dt.Rows.Count > 0;
            conn.Close();
            return attached;
        }
        public static string InsertDefineDocument(DocPredefinedModel saveDocument, string itemTypeId, string cpOfficeId, out string docId , Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spInsertDocumentEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@DocumentID", SqlDbType.Int));
            command.Parameters["@DocumentID"].Direction = ParameterDirection.Output;
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            command.Parameters.AddWithValue("@DocumentTypeCode", saveDocument.DocumentTypeCode);
            command.Parameters.AddWithValue("@DocumentName", saveDocument.DocumentName);
            command.Parameters.AddWithValue("@BarCodeFixedText", saveDocument.BarCodeFixedText);
            command.Parameters.AddWithValue("@UseDate", saveDocument.UseDate);
            command.Parameters.AddWithValue("@UseVirtualVaultNumber", saveDocument.UseVvNumber);
            command.Parameters.AddWithValue("@CorelFile", saveDocument.CorelFile);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CurrentOfficeID", cpOfficeId);

            command.Parameters.AddWithValue("@ExportTypeID", saveDocument.ExportTypeId);
            command.Parameters.AddWithValue("@ImportTypeID", saveDocument.ImportTypeId);
            command.Parameters.AddWithValue("@FormatTypeID", saveDocument.FormatTypeId);

            var tableValues = CreateDocumentValuesTable(saveDocument.DocumentId, saveDocument.DocumentValues);
            var paramValues = new SqlParameter
            {
                ParameterName = "@DocumentValues",
                SqlDbType = SqlDbType.Structured,
                Value = tableValues,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);
            LogUtils.UpdateSpLogWithParameters(command, p);
            docId = "";
            try
            {
                command.ExecuteNonQuery();
                docId = command.Parameters["@DocumentID"].Value.ToString();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static string UpdateDefineDocument(DocPredefinedModel saveDocument, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spUpdateDocumentEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", saveDocument.DocumentId);
            command.Parameters.AddWithValue("@DocumentName", saveDocument.DocumentName);
            command.Parameters.AddWithValue("@BarCodeFixedText", saveDocument.BarCodeFixedText);
            command.Parameters.AddWithValue("@UseDate", saveDocument.UseDate);
            command.Parameters.AddWithValue("@UseVirtualVaultNumber", saveDocument.UseVvNumber);
            command.Parameters.AddWithValue("@CorelFile", saveDocument.CorelFile);
            command.Parameters.AddWithValue("@ExportTypeID", saveDocument.ExportTypeId);
            command.Parameters.AddWithValue("@ImportTypeID", saveDocument.ImportTypeId);
            command.Parameters.AddWithValue("@FormatTypeID", saveDocument.FormatTypeId);

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var tableValues = CreateDocumentValuesTable(saveDocument.DocumentId, saveDocument.DocumentValues);
            var paramValues = new SqlParameter
            {
                ParameterName = "@DocumentValues",
                SqlDbType = SqlDbType.Structured,
                Value = tableValues,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();

            return msg;
        }
        private static DataTable CreateDocumentValuesTable(string documentId, IEnumerable<DocumentValueModel> values)
        {
            var table = new DataTable("DocumentValues");
            table.Columns.Add("DocumentId");
            table.Columns.Add("Title");
            table.Columns.Add("Value");
            table.Columns.Add("Unit");
            foreach (var value in values)
            {
                table.Rows.Add(new object[]
                { 
                    documentId, value.Title, value.Value, value.Unit
                });
            }
            table.AcceptChanges();
            return table;
        }
        public static Corpt.Models.DocumentTypeModel GetDocumentType(string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetDocumentTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var types = (from DataRow row in dt.Rows select new DocumentTypeModel(row)).ToList();
            var result = types.Find(m => m.DocumentTypeCode == documentTypeCode);
            conn.Close();
            return result;
        }
        public static DefineDocumentInitModel GetDefineDocumentInit(string cpId, string cpOfficeId, string docTypeCode, string cpDocId, Page p, string itemTypeId = null)
        {
            var initData = new DefineDocumentInitModel
            {
                CpId = cpId,
                CpOfficeId = cpOfficeId,
                Cp = GetCustomerProgramByCpId(cpId, p),
                CpDocId = cpDocId,
                DocumentType = GetDocumentType(docTypeCode, p)
            };
            int initItemTypeId = 0;
            if (itemTypeId == null)
                initItemTypeId = Convert.ToInt32(initData.Cp.ItemTypeId);
            else
                initItemTypeId = Convert.ToInt32(itemTypeId);
                //initData.MeasureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(initData.Cp.ItemTypeId), p);
            initData.MeasureParts = QueryUtils.GetMeasureParts(initItemTypeId, p);
            var result = new List<MeasureModel>();
            var measuresAll = GetMeasuresByItemTypeAll("" + initItemTypeId.ToString(), p);
            //var measuresAll = GetMeasuresByItemTypeAll(""+initData.Cp.ItemTypeId, p);
            var rules = GetCpDocRules(cpDocId, p);
            foreach (var part in initData.MeasureParts)
            {
                var measures = measuresAll.FindAll(m => m.PartTypeId == part.PartTypeId);
                foreach(var measure in measures)
                {
                    if ("" + measure.MeasureId == DbConstants.MeasureIdShapeCut) continue;
                    var measureNotVisible = rules.Find(m => m.MeasureId == measure.MeasureId && m.NotVisibleInCcm && m.PartId == part.PartId);
                    if (measureNotVisible != null)
                    {
                        continue;
                    }
                    result.Add(new MeasureModel{PartId = part.PartId, PartTypeId = part.PartTypeId, MeasureName = measure.MeasureName, MeasureId = measure.MeasureId});
                }
            }
            initData.MeasuresByParts = result;

            //--
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetImpExInfo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //command.Parameters.AddWithValue("@OfficeID", initData.Cp.CpOfficeId);
            command.Parameters.AddWithValue("@OfficeID", cpOfficeId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            
            var da = new SqlDataAdapter(command);
            var ds = new DataSet();
            da.Fill(ds);
            initData.ImportInfo = (from DataRow row in ds.Tables[0].Rows select new ImpExpInfoModel(row)).ToList();
            initData.ImportInfo.Sort((m1, m2) =>  string.CompareOrdinal(m1.Order, m2.Order) );

            initData.ExportInfo = (from DataRow row in ds.Tables[1].Rows select new ImpExpInfoModel(row)).ToList();
            initData.ExportInfo.Sort((m1, m2) => string.CompareOrdinal(m1.Order, m2.Order));

            initData.FormatInfo = (from DataRow row in ds.Tables[2].Rows select new DocFormatModel(row)).ToList();
            initData.FormatInfo.Sort((m1, m2) => string.CompareOrdinal(m1.Order, m2.Order));
            conn.Close();

            //initData.Documents = GetDocumentsByItemTypeAndDocumentTypeCode(""+initData.Cp.ItemTypeId, docTypeCode, p);
            initData.Documents = GetDocumentsByItemTypeAndDocumentTypeCode("" + initItemTypeId.ToString(), docTypeCode, p);
            return initData;
        }
        public static List<DocPredefinedModel> GetDocumentsByItemTypeAndDocumentTypeCode(string itemTypeId, string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentsPredefined",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            command.Parameters.AddWithValue("@DocumentTypeCode", documentTypeCode);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result =  (from DataRow row in dt.Rows select new DocPredefinedModel(row)).ToList();
            result.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
            result.Add(DocPredefinedModel.GetNewDocument(documentTypeCode));
            return result;
        }

        public static string SetBatchEvent(int formCode, int eventID, long batchId, int noAffected, int noInBatch, int batchEventId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetBatchEventsGDLight",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@FormCode", formCode);
            command.Parameters.AddWithValue("@EventID", eventID);
            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@NumberOfItemsAffected", noAffected);
            command.Parameters.AddWithValue("@NumberOfItemsInBatch", noInBatch);
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            string result = "";
            if (dt == null || dt.Rows.Count == 0)
                result = "failed";
            else
                result = dt.Rows[0].ItemArray[0].ToString();
            return result;
        }
        public static string SetItemEvent(int formCode, int eventID, string fullItemNumber, long batchId,  int batchEventId, int increment, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetItemEventsGDLight",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            int itemCode = Convert.ToInt16(fullItemNumber.Substring(fullItemNumber.Length - 2));
            command.Parameters.AddWithValue("@FormCode", formCode);
            command.Parameters.AddWithValue("@EventID", eventID);
            command.Parameters.AddWithValue("@FullItemNumber", fullItemNumber);
            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@BatchTrackingHistoryID", batchEventId);
            command.Parameters.AddWithValue("@ItemCode", itemCode);
            command.Parameters.AddWithValue("@Increment", increment);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            string result = "";
            if (dt == null || dt.Rows.Count == 0)
                result = "failed";
            else
                result = dt.Rows[0].ItemArray[0].ToString();
            return result;
        }
        public static List<DocPredefinedModel> GetAllDocuments(Page p)
        {
            var result = new List<DocPredefinedModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllDocuments",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new DocPredefinedModel(row)).ToList();
                result.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
            }

            return result;
        }
        public static List<CpRuleShortModel> GetAllShapeNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllShapes",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }               
            return result;
        }
        //IvanB 07/03 start
        public static List<CpRuleShortModel> GetAllVarietyNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllVariety",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        public static List<CpRuleShortModel> GetAllSpeciesNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllSpecies",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        //IvanB 07/03 end
        public static List<CpRuleShortModel> GetAllColorGroupNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllColorGroup",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        public static List<CpRuleShortModel> GetAllKaratageNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllKaratage",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        //alex 04152027
        public static List<CpRuleShortModel> GetAllItemNameNames(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllItemName",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        public static List<CpRuleShortModel> GetAllColorDiamondColors(Page p)
        {
            var result = new List<CpRuleShortModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllColorDiamondColors",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                LogUtils.UpdateSpLogWithParameters(command, p);

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CpRuleShortModel(row)).ToList();
            }
            return result;
        }
        //alex 04152027
        public static List<DocumentValueModel> GetDocumentValues(string documentId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentValue",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentValueModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        public static List<DocumentTitleModel> GetDocumentTitles(string languageId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDefaultDocumentTitle",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentLanguageID", languageId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentTitleModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static string GetCorelFileName(string documentName, string typeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select CorelFile from tblDocument where documentName = '" + documentName + @"' and OperationTypeId = " + typeId;
            //var criteria = ids.Aggregate("", (current, id) => current + ((current == "" ? "" : ", ") + id));
            var command = new SqlCommand
            {
                CommandText = sql,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = dt.Rows[0].ItemArray[0].ToString();
            conn.Close();
            return result;
        }
        public static List<LanguageModel> GetLanguages(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentLanguage",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new LanguageModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static CustomerProgramModel GetCustomerProgramByBatchNumber(string batchNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramInstanceByBatchCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", Utils.ParseOrderCode(batchNumber));
            command.Parameters.AddWithValue("@BatchCode", Utils.ParseBatchCode(batchNumber));
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var cpId = "";
            if (dt.Rows.Count >0)
            {
                cpId = "" + dt.Rows[0]["CPID"];
            }
            conn.Close();
            return string.IsNullOrEmpty(cpId) ? null : CustomerProgramModel.CreateCombineCp(dt.Rows[0], GetCustomerProgramByCpId(cpId, p));
        }
        public static CustomerProgramModel GetCustomerProgramByCpId(string cpId, Page p)
        {
            if (cpId == null || cpId == "")
                return null;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramInstanceByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerProgramModel(row, true)).ToList();
            conn.Close();
            return result.Count == 1 ? result.ElementAt(0) : null;
        }
        #endregion

        #region Find Cp by Program name, Customer, Vendor
        public static bool ExistsCp(string cpName, CustomerModel customer, CustomerModel vendor, string verifyCpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByNameAndCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerProgramName", cpName);
            command.Parameters.AddWithValue("@CustomerOfficeID", customer.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
            if (vendor != null)
            {
                command.Parameters.AddWithValue("@VendorOfficeID", customer.OfficeId);
                command.Parameters.AddWithValue("@VendorID", customer.CustomerId);
            }
            else
            {
                command.Parameters.Add("@VendorOfficeID", SqlDbType.Int).Value = DBNull.Value;
                command.Parameters.Add("@VendorID", SqlDbType.Int).Value = DBNull.Value;
            }
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var exists = false;
            if (dt.Rows.Count > 0)
            {
                if (string.IsNullOrEmpty(verifyCpId))
                {
                    exists = true;
                } else
                {
                    exists = ("" + dt.Rows[0]["CPID"]) != verifyCpId;
                }
            }
            conn.Close();
            return exists;
        }
        #endregion
		public static DataTable GetRangeGroupList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select distinct GroupName, GroupId from tblFractionRanges",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            return dt;
        }
        public static DataTable GetFractionRangeList(string groupName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select * from tblFractionRanges where groupname = '" + groupName + @"'",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            return dt;
        }
        public static DataTable GetCdrXlsFiles(string docTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "select * from tblDocNameToType where DocumentTypeCode = " + docTypeCode,
                    Connection = conn,
                    CommandType = CommandType.Text,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                conn.Close();
                return null;
            }
        }
        #region Operations Tree
        public static List<OperationModel> GetAllOperations(CommonPage p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetOperationTree",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new OperationModel(row)).ToList();
            conn.Close();
            return result;

        }
        public static List<string> GetCpOperations(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPOperations",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = Int32.Parse(cpModel.CpOfficeId);
            command.Parameters.Add("@CPID", SqlDbType.Int).Value = Int32.Parse(cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ("" + row["OperationTypeID"])).ToList();
            conn.Close();
            return result;
           
        }
        #endregion

        #region Short Measures (Characteristics)
        public static List<MeasureShortModel> GetMeasuresShort(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresWithAdditional",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureShortModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        #endregion

        #region Measures by ItemType
        public static List<MeasureModel> GetMeasuresByItemType(string itemTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresByItemType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasureModel.Create2(row)).ToList();
            conn.Close();
            return
                result.FindAll(
                    m =>
                    m.MeasureClass == MeasureModel.MeasureClassEnum ||
                    m.MeasureClass == MeasureModel.MeasureClassNumeric);
        }
        public static List<MeasureModel> GetMeasuresByItemTypeAll(string itemTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresByItemType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasureModel.Create2(row)).ToList();
            conn.Close();
            return result;
        }

        #endregion

        #region Most Recently (ItemTypeGroups and ItemTypes)
        public static List<ItemTypeModel> GetItemTypesRecently(CustomerModel customer, CustomerModel vendor, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMRUItems",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CustomerOfficeID", customer.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
            if (vendor != null)
            {
                command.Parameters.AddWithValue("@VendorOfficeID", vendor.OfficeId);
                command.Parameters.AddWithValue("@VendorID", vendor.CustomerId);
            } else
            {
                command.Parameters.AddWithValue("@VendorOfficeID", customer.OfficeId);
                command.Parameters.AddWithValue("@VendorID", customer.CustomerId);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeModel(row, true)).ToList();
            conn.Close();
            return result;

        }
        #endregion

        #region ItemTypeGroups
        public static List<ItemTypeGroupModel> GetItemTypeGroups(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemTypeGroups",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeGroupModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataTable GetSKUTypesList(string customerCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSKUItemTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            int custCode = Convert.ToInt32(customerCode);
            command.Parameters.AddWithValue("@CustomerCode", custCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            conn.Close();
            return dt;
        }
        public static DataTable GetSKUAllTypesList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSKUAllItemTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            conn.Close();
            return dt;
        }
        #endregion

        #region ItemTypes by Group
        public static List<ItemTypeModel> GetItemTypesByGroup(int itemTypeGroupId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemTypesByGroup",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeGroupID", itemTypeGroupId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeModel(row, false)).ToList();
            conn.Close();
            return result;
        }

        public static List<ItemTypeModel> GetItemTypesByIds(List<string> ids, Page p )
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            const string sql = "select * from v0ItemType where ItemTypeId in ({0})";
            var criteria = ids.Aggregate("", (current, id) => current + ((current == "" ? "" : ", ") + id));
            var command = new SqlCommand
            {
                CommandText = string.Format(sql, criteria),
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemTypeModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Additional Services
        public static List<AddServiceModel> GetAdditionalServices(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetAdditionalService",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new AddServiceModel(row)).ToList();
            conn.Close();
            result.Insert(0, new AddServiceModel{ServiceId = "", ServiceName = ""});
            result.Sort((m1, m2) => string.CompareOrdinal(m1.ServiceName, m2.ServiceName));
            return result;

        }
        public static List<AddServicePriceModel> GetAdditionalServicePrices(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetAdditionalServicePriceByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new AddServicePriceModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Pricing
        public static List<PricePartMeasureModel> GetPricePartMeasures(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPricePartsMeasuresByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PricePartMeasureModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<PriceRangeModel> GetPriceRanges(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPriceRangeByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PriceRangeModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataSet GetSamplePriceRanges(string samplePriceName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPriceRangeBySamplePriceName",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SamplePriceName", samplePriceName);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataSet();
            da.Fill(dt);
            //var result = (from DataRow row in dt.Rows select new SamplePriceListRangesModel(row)).ToList();
            conn.Close();
            //return result;
            return dt;
        }
        public static List<PricePartMeasureModel> GetSamplePricePartMeasures(string cpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPricePartsMeasuresByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", SqlDbType.Int).Value = Int32.Parse(cpId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PricePartMeasureModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataTable SaveSamplePriceRange(string samplePriceName, int priceId,  int cpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveSamplePriceRangeNew",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SamplePricerangeName", samplePriceName);
            command.Parameters.AddWithValue("@PriceID", priceId);
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            return dt;
        }
        public static DataTable DeleteSamplePriceRange(string samplePriceName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spDeleteSamplePriceRange",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SamplePricerangeName", samplePriceName);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            return dt;
        }

        public static DataTable GetSamplePriceList(Page p)
        {
                var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "Select * from tblSamplePriceRanges order by SamplePriceName",
                    Connection = conn,
                    CommandType = CommandType.Text,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);
                var cda = new SqlDataAdapter(command);
                var cdt = new DataTable();
                cda.Fill(cdt);
                conn.Close();
            if (cdt.Rows.Count > 0)
                return cdt;
            else
                return null;
        }
        public static DataTable GetSavedDescCommentsList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from tblSavedDescComments where expdate is null",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            conn.Close();
            if (cdt.Rows.Count > 0)
                return cdt;
            else
                return null;
        }
        public static DataTable AddCPComments(SavedDescCommentsListModel model, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            var dt = new DataTable();
            try
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spAddCPDescComments",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@ItemTypeID", model.ItemTypeID);
                command.Parameters.AddWithValue("@Type", model.Type);
                command.Parameters.AddWithValue("@Name", model.Name);
                command.Parameters.AddWithValue("@Text", model.Text);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public static DataTable DeleteSavedCommentsDesc(string name, string type, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            var dt = new DataTable();
            try
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spDeleteCPDescComments",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@Name", name);
                command.Parameters.AddWithValue("@Type", type);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        #endregion

        #region Save Customer Program Full
        public static string SaveCustomerProgram(CpEditModel cpEdit, bool copyAll, CustomerProgramModel cpFrom, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSaveCustomerProgram",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 255));
            command.Parameters["@rId"].Direction = ParameterDirection.Output;

            command.Parameters.AddWithValue("@CustomerOfficeID", Convert.ToInt32(cpEdit.Customer.OfficeId));
            command.Parameters.AddWithValue("@CustomerID", Convert.ToInt32(cpEdit.Customer.CustomerId));
            if (cpEdit.Vendor != null && !string.IsNullOrEmpty(cpEdit.Vendor.CustomerId))
            {
                command.Parameters.AddWithValue("@VendorOfficeID", Convert.ToInt32(cpEdit.Vendor.OfficeId));
                command.Parameters.AddWithValue("@VendorID", Convert.ToInt32(cpEdit.Vendor.CustomerId));
            } else
            {
                command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
                command.Parameters.AddWithValue("@VendorID", DBNull.Value);
            }
            if (cpEdit.Cp.CpId == "" || cpEdit.Cp.CpId == "0")
            {
                command.Parameters.AddWithValue("@CPOfficeID", DBNull.Value);
                command.Parameters.AddWithValue("@CPID", DBNull.Value);
            } else
            {
                command.Parameters.AddWithValue("@CPOfficeID", Convert.ToInt32(cpEdit.Cp.CpOfficeId));
                command.Parameters.AddWithValue("@CPID", Convert.ToInt32(cpEdit.Cp.CpId));
            }
            command.Parameters.AddWithValue("@CustomerProgramName", cpEdit.Cp.CustomerProgramName);
            command.Parameters.AddWithValue("@Comment", cpEdit.Cp.Comment);
            command.Parameters.AddWithValue("@Path2Picture", cpEdit.Cp.Path2Picture);
            command.Parameters.AddWithValue("@ItemTypeID", cpEdit.Cp.ItemTypeId);
            command.Parameters.AddWithValue("@ItemTypeGroupID", cpEdit.Cp.ItemTypeGroupId);
            command.Parameters.AddWithValue("@CustomerStyle", cpEdit.Cp.CustomerStyle);
            command.Parameters.AddWithValue("@CPPropertyCustomerID", cpEdit.Cp.CpPropertyCustId);
            if (cpEdit.Cp.Srp == "0")
            {
                command.Parameters.AddWithValue("@SRP", DBNull.Value);
            } else
            {
                command.Parameters.AddWithValue("@SRP", cpEdit.Cp.Srp);
            }
            command.Parameters.AddWithValue("@Description", cpEdit.Cp.Description);
            
            command.Parameters.AddWithValue("@IsFixed", cpEdit.Cp.IsFixed ? 1 : 0);
            if (cpEdit.Cp.IsFixed && cpEdit.Cp.FixedPrice > 0)
            {
                command.Parameters.AddWithValue("@FixedPrice", cpEdit.Cp.FixedPrice);
            } else
            {
                command.Parameters.AddWithValue("@FixedPrice", DBNull.Value);
            }
            if (cpEdit.Cp.IsFixed && cpEdit.Cp.Discount > 0)
            {
                command.Parameters.AddWithValue("@Discount", cpEdit.Cp.Discount);
            } else
            {
                command.Parameters.AddWithValue("@Discount", DBNull.Value);
            }

            if (!cpEdit.Cp.IsFixed && cpEdit.Cp.DeltaFix > 0)
            {
                command.Parameters.AddWithValue("@DeltaFix", cpEdit.Cp.DeltaFix); 
            } else
            {
                command.Parameters.AddWithValue("@DeltaFix", DBNull.Value);
            }
            if (cpEdit.Cp.FailFixed > 0)
            {
                command.Parameters.AddWithValue("@FailFixed", cpEdit.Cp.FailFixed);
            } else
            {
                command.Parameters.AddWithValue("@FailFixed", DBNull.Value);
            }
            if (cpEdit.Cp.FailDiscount > 0)
            {
                command.Parameters.AddWithValue("@FailDiscount", cpEdit.Cp.FailDiscount);
            } else
            {
                command.Parameters.AddWithValue("@FailDiscount", DBNull.Value);
            }
            command.Parameters.AddWithValue("@CopyAllCp", copyAll ? 1 : 0); 
            
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            if (cpFrom != null)
            {
                command.Parameters.AddWithValue("@CPIDFrom", Convert.ToInt32(cpFrom.CpId));
                command.Parameters.AddWithValue("@CPOfficeIDFrom", Convert.ToInt32(cpFrom.CpOfficeId));
            } else
            {
                command.Parameters.AddWithValue("@CPIDFrom", DBNull.Value);
                command.Parameters.AddWithValue("@CPOfficeIDFrom", DBNull.Value);
            }
            command.Parameters.Add(GetParamPriceParts(cpEdit));
            command.Parameters.Add(GetParamPriceRange(cpEdit));
            command.Parameters.Add(GetParamPriceAddServs(cpEdit));
            command.Parameters.Add(GetParamCpOperations(cpEdit));
            command.Parameters.Add(GetParamCpDocs(cpEdit));
            command.Parameters.Add(GetParamCpDocRules(cpEdit));
            command.Parameters.Add(GetParamCpDocGroups(cpEdit));
            command.Parameters.Add(GetParamCpDocOpers(cpEdit));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
                msg = "" + command.Parameters["@rId"].Value;

            }

            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static DataTable SetSaveBlockedParts(CpEditModel cpEdit, int[] partIds, Page p)
        {
            string sqlparam = "";
            if (partIds != null)
                sqlparam = string.Join(";", partIds);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetGDLightSaveBlockedPartsByBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPID", Convert.ToInt32(cpEdit.Cp.CpId));
            if (sqlparam == "")
                command.Parameters.AddWithValue("@PartIDStr", DBNull.Value);
            else
                command.Parameters.AddWithValue("@PartIDStr", sqlparam);
            command.Parameters.AddWithValue("@CustomerOfficeID", Convert.ToInt32(cpEdit.Customer.OfficeId));
            command.Parameters.AddWithValue("@CustomerID", Convert.ToInt32(cpEdit.Customer.CustomerId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        
        public static DataTable GetSavedBlockedParts(CpEditModel cpEdit, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select * from tblBlockedParts where ExpireDate is null and cpid = " + cpEdit.Cp.CpId,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            return dt;
        }
        
        private static SqlParameter GetParamCpDocOpers(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocOpers");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("OperationTypeOfficeId");
            tbl.Columns.Add("OperationTypeID");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var oper in doc.AttachedReports)
                {
                    tbl.Rows.Add(new object[] { doc.CpDocId, oper.OperationTypeOfficeId, oper.OperationTypeId });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocOpers",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocGroups(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocGroups");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("MeasureGroupID");
            tbl.Columns.Add("NoRecheck");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var group in doc.MeasureGroups)
                {
                    if (group.NoRecheck > 0)
                    {
                        tbl.Rows.Add(new object[] { doc.CpDocId, group.MeasureGroupId, Convert.ToInt32(group.NoRecheck) });
                    }
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocGroups",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocRules(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocRules");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("MeasureId");
            tbl.Columns.Add("MinMeasure");
            tbl.Columns.Add("MaxMeasure");
            tbl.Columns.Add("PartID");
            tbl.Columns.Add("NotVisibleInCCM");
            tbl.Columns.Add("IsDefaultMeasureValue");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var rule in doc.EditDocRules)
                {
                    if (!rule.HasValues) continue;
                    object minValue;
                    object maxValue;
                    if (string.IsNullOrEmpty(rule.MinValue))
                    {
                        minValue = DBNull.Value;
                    }
                    else
                    {
                        minValue = float.Parse(rule.MinValue);
                    }
                    if (string.IsNullOrEmpty(rule.MaxValue))
                    {
                        maxValue = DBNull.Value;
                    }
                    else
                    {
                        maxValue = float.Parse(rule.MaxValue);
                    }
                    if (rule.IsDefault)
                    {
                        tbl.Rows.Add(new[]
                        {
                        doc.CpDocId, rule.MeasureId, minValue, maxValue, rule.PartId, rule.NotVisibleInCcm ? 1 : 0, rule.IsDefault ? 1 : 0
                        });
                    }
                    else if (rule.IsInfoOnly)
                    {
                        tbl.Rows.Add(new[]
                        {
                        doc.CpDocId, rule.MeasureId, minValue, maxValue, rule.PartId, rule.NotVisibleInCcm ? 1 : 0, 2
                        });
                    }
                    else if (!rule.IsDefault)
                    {
                        tbl.Rows.Add(new[]
                        {
                        doc.CpDocId, rule.MeasureId, minValue, maxValue, rule.PartId, rule.NotVisibleInCcm ? 1 : 0, 0
                        });
                    }
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocRules",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocs(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocs");
            tbl.Columns.Add("CpDocId", typeof(string));
            tbl.Columns.Add("Description", typeof(string));
            tbl.Columns.Add("IsReturn", typeof(string));
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                tbl.Rows.Add(new object[] { doc.CpDocId, doc.Description, doc.IsReturn ? 1 : 0 });
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocs",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpOperations(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpOperations");
            tbl.Columns.Add("OperationTypeOfficeId", typeof(int));
            tbl.Columns.Add("OperationTypeID", typeof(int));
            tbl.Columns.Add("Checked", typeof(int));
            foreach (var operation in cpEdit.OperationTypes)
            {
                tbl.Rows.Add(new object[] { cpEdit.OperationTypeOfficeId, operation, 1 });
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpOperations",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceAddServs(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceAddServs");
            tbl.Columns.Add("AdditionalServiceID", typeof(int));
            tbl.Columns.Add("Price", typeof(float));
            //if (!cpEdit.Cp.IsFixed)//Sandip changes to fix fixed cp saving, 8/21/2018
            //{
                foreach(var serv in cpEdit.CpPrice.AddServicePrices)
                {
                    tbl.Rows.Add(new object[] {serv.ServiceId, serv.Price});
                }
            //}
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceAddServs",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceRange(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceRange");
            tbl.Columns.Add("HomogeneousClassID", typeof(int));
            tbl.Columns.Add("FromValue", typeof(float));
            tbl.Columns.Add("ToValue", typeof(float));
            tbl.Columns.Add("Price", typeof(float));
            if (!cpEdit.Cp.IsFixed)
            {
                foreach (var range in cpEdit.CpPrice.PriceRanges)
                {
                    tbl.Rows.Add(new object[] { 1/* TODO range.HomogeneousClassId*/, range.ValueFrom, range.ValueTo, range.Price });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceRange",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceParts(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceParts");
            tbl.Columns.Add("PartId", typeof(int));
            tbl.Columns.Add("MeasureCode", typeof(int));
            tbl.Columns.Add("HomogeneousClassID", typeof(int));
            tbl.Columns.Add("PartNameMeasureName", typeof(string));
            if (!cpEdit.Cp.IsFixed)
            {
                foreach (var part in cpEdit.CpPrice.PricePartMeasures)
                {
                    tbl.Rows.Add(new object[]
                    {
                        part.PartId, 
                        part.MeasureCode, 
                        1, //TODO part.HomogeneousClassId, 
                        part.PartNameMeasureName
                    });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceParts",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };

            return param;
        }
        #endregion
        
        #region Get Customer Program Full
        public static CpEditModel InitCustomerProgram(CustomerProgramModel cp, List<EnumMeasureModel> enumsMeasure, Page p)
        {
            var cpEdit = new CpEditModel
            {
                Cp = cp,
                MeasureParts = QueryUtils.GetMeasureParts(cp.ItemTypeId, p),
                MeasuresByItemType = GetMeasuresByItemType("" + cp.ItemTypeId, p),
            };
            if (p.Session["SavedCpEditModel"] != null)
                cpEdit = (CpEditModel)p.Session["SavedCpEditModel"];
            AddCpDocModel(cpEdit, enumsMeasure, true, p);
            return cpEdit;
        }
        public static CpEditModel GetCustomerProgramFull(CustomerProgramModel cp, List<EnumMeasureModel> enumsMeasure, Page p)
        {
            var cpEdit = new CpEditModel
            {
                Cp = cp,
                MeasureParts = QueryUtils.GetMeasureParts(cp.ItemTypeId, p),
                MeasuresByItemType = GetMeasuresByItemType("" + cp.ItemTypeId, p),
                OperationTypes = GetCpOperations(cp, p),
                Reports = GetReportsByCp(cp, p),
                CpDocs = GetCpDocs(cp, p),
                CpPrice =
                {
                    PricePartMeasures = GetPricePartMeasures(cp, p),
                    PriceRanges = GetPriceRanges(cp, p),
                    AddServicePrices = GetAdditionalServicePrices(cp, p)
                },
            };
            //alex 6/23
            CpEditModel editData = new CpEditModel();
            if (p.Session["EditData"] != null)
            {
                editData = (CpEditModel)p.Session["EditData"];
                cpEdit = editData;
                //p.Session["EditData1"] = editData;
            }
            CpEditModel editData2 = new CpEditModel();
            //if (p.Session["EditData1"] != null)
            //    editData2 = (CpEditModel)p.Session["EditData1"];
            //alex 6/23
            //var cpDocs = cpEdit.CpDocs;
            //foreach (var doc in cpEdit.CpDocs)
            foreach(var doc in cpEdit.CpDocs)
            {
                // Rules
                if (p.Session["EditData"] == null)
                {
                    var savingDocRules = GetCpDocRules("" + doc.CpDocId, p);
                    var editRules = new List<CpDocRuleEditModel>();
                    foreach (var part in cpEdit.MeasureParts)
                    {
                        var measures = cpEdit.MeasuresByItemType.FindAll(m => m.PartTypeId == part.PartTypeId);
                        foreach (var measure in measures)
                        {
                            var rule = savingDocRules.Find(m => m.MeasureId == measure.MeasureId && m.PartId == part.PartId);
                            var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                                         ? enumsMeasure.FindAll(m => m.MeasureValueMeasureId == measure.MeasureId)
                                         : null);
                            editRules.Add(new CpDocRuleEditModel(Convert.ToInt32(doc.CpDocId), measure, enums, rule, "" + part.PartId));
                        }
                    }
                    var docId = doc.CpDocId;
                    doc.EditDocRules = editRules;
                    doc.AttachedReports = GetAttachedReportsByDoc(doc, cpEdit.Reports, p);
                }
                else
                {
                    //doc.AttachedReports = GetAttachedReportsByDoc(doc, cpEdit.Reports, p);
                    var rules = cpEdit.CpDocs.Find(m => m.CpDocId == doc.CpDocId);
                    doc.EditDocRules = rules.EditDocRules;
                }
                // Measure Recheks
                doc.MeasureGroups = GetMeasureGroupsByDoc(doc, p);
                // Attached Reports
                 
                //alex 6/23
                //if (p.Session["EditData"] != null)
                ////alex 6/23
                //{

                //    var editData1 = (CpEditModel)p.Session["EditData"];
                //    doc.AttachedReports = GetAttachedReportsByDoc(doc, cpEdit.Reports, p);
                //    var rules = editData1.CpDocs.Find(m => m.CpDocId == doc.CpDocId);
                //    doc.EditDocRules = rules.EditDocRules;
                //    p.Session["EditData"] = editData1;
                //    editData1 = null;
                //}

            }
            if (cpEdit.CpDocs.Find(m => m.IsDefault) == null)
            {
                //-- Add Default Document
                AddCpDocModel(cpEdit, enumsMeasure, true, p);
            }
            return cpEdit;
        }
        public static string AddCpDocModel(CpEditModel cpEdit, List<EnumMeasureModel> enumsMeasure, bool isDefault, Page p)
        {
            var maxDocId = (cpEdit.CpDocs.Count == 0 ? 0 : cpEdit.CpDocs.Max(m => m.CpDocId));
            var doc = new CpDocModel{CpDocId = maxDocId + 1, IsDefault = isDefault};
            if (p.Session["NewSku"] != null && isDefault && maxDocId > 0)
            {
                foreach (CpDocModel model in cpEdit.CpDocs)
                {
                    if (model.IsDefault && model.AttachedReports.Count == 0)//first default is initiated
                        return null;
                }
            }

            //-- Rules
            if (!doc.IsDefault)
            {
                var editRules = new List<CpDocRuleEditModel>();
                foreach (var part in cpEdit.MeasureParts)
                {
                    var measures = cpEdit.MeasuresByItemType.FindAll(m => m.PartTypeId == part.PartTypeId);
                    foreach (var measure in measures)
                    {
                        var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                                     ? enumsMeasure.FindAll(m => m.MeasureValueMeasureId == measure.MeasureId)
                                     : null);
                        editRules.Add(new CpDocRuleEditModel(Convert.ToInt32(doc.CpDocId), measure, enums, null, "" + part.PartId));
                    }
                }
                doc.EditDocRules = editRules;
            }

            //-- Measure Groups
            if (MeasureGroups.Count == 0)
            {
                GetMeasureGroups(p);
            }
            var grpsByDoc = MeasureGroups.Select(@group => MeasureGroupModel.Copy(@group, doc.CpDocId)).ToList();
            grpsByDoc.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureGroupName, m2.MeasureGroupName));
            doc.MeasureGroups = grpsByDoc;
            if (isDefault)
            {
                cpEdit.CpDocs.Insert(0, doc);
            } else
            {
                cpEdit.CpDocs.Add(doc);
            }
            return "" + doc.CpDocId;
        }
        #endregion

        #region Reports by Cp, Doc
        public static List<CpDocPrintModel> GetReportsByCp(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocsByCP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID", cpModel.CpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocPrintModel(row, cpModel.CpOfficeIdAndCpId)).ToList();
            result.Sort(new DocComparer().Compare);
            conn.Close();
            return result;
        }
        public static List<CpDocPrintModel> GetAttachedReportsByDoc(CpDocModel docModel, List<CpDocPrintModel> cpPrintDocs, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDoc_Operation",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPDocID", docModel.CpDocId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select CpDocPrintModel.CreateAttachedReport(row, cpPrintDocs)).ToList();
            //result.Sort(new DocComparer().Compare);
            conn.Close();
            return result;
        }
        public static bool GetMinSkuDoc(int id, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMinSkuDoc",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocID", id);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            bool result = false;
            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Result"].ToString() == "true")
                    result = true;
            }
            //result.Sort(new DocComparer().Compare);
            conn.Close();
            return result;
        }
        #endregion

        #region Documents by Cp
        public static List<CpDocModel> GetCpDocs(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDocs",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID", cpModel.CpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Measure Groups (What to do)
        public static List<MeasureGroupModel>  MeasureGroups = new List<MeasureGroupModel>();
        public static List<MeasureGroupModel>  GetMeasureGroups(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasureGroups",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureGroupModel(row, false)).ToList();
            conn.Close();
            MeasureGroups = result;
            return result;

        }
        public static List<MeasureGroupModel> GetMeasureGroupsByDoc(CpDocModel cpDoc, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDoc_MeasureGroup",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPDocID", cpDoc.CpDocId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureGroupModel(row, true)).ToList();
            conn.Close();
            //-- Union with common
            if (MeasureGroups.Count == 0)
            {
                GetMeasureGroups(p);
            }
            var grpsByDoc = new List<MeasureGroupModel>();
            foreach(var group in MeasureGroups)
            {
                var groupByDoc = result.Find(m => m.MeasureGroupId == group.MeasureGroupId);
                if (groupByDoc == null)
                {
                    grpsByDoc.Add(MeasureGroupModel.Copy(group, cpDoc.CpDocId));
                } else
                {
                    groupByDoc.MeasureGroupName = group.MeasureGroupName;
                    groupByDoc.CpDocId = ""+cpDoc.CpDocId;
                    grpsByDoc.Add(groupByDoc);
                }
            }
            grpsByDoc.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureGroupName, m2.MeasureGroupName));
            return grpsByDoc;
        }
        public static void SetMeasureGroup(List<MeasureGroupModel> groups, Page p)
        {
            //spSetCPDoc_MeasureGroup: @AuthorId=19; @AuthorOfficeId=1; @CurrentOfficeId=1; @rId=; @CPDocID=2190198; @MeasureGroupID=2; @NoRecheck=3; 
        }
        #endregion

        #region Document Rules
        public static List<CpDocRuleModel>  GetCpDocRules(string cpDocId, Page p)
        {
            if (string.IsNullOrEmpty(cpDocId)) return new List<CpDocRuleModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDocRule",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@CPDocID", cpDocId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocRuleModel(row)).ToList();

            conn.Close();
            return result;
        }

        public static DataTable GetCorelFileForCp(string groupCode, string batchCode, string customerProgramName, string typeCode, Page p)
        {
            var dt = new DataTable();
            string corelFile = null;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            try
            {
                
                var command = new SqlCommand
                {
                    CommandText = "spGetCorelFileForCP",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(groupCode)));
                command.Parameters.Add(new SqlParameter("@BatchCode", Convert.ToInt16(batchCode)));
                command.Parameters.Add(new SqlParameter("@CustomerProgramName", customerProgramName));
                command.Parameters.Add(new SqlParameter("@DocumentTypeCode", Convert.ToInt32(typeCode)));
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                
                if (dt.Rows.Count > 0)
                    corelFile = dt.Rows[0].ItemArray[0].ToString();
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                dt = null;
            }
            conn.Close();
            return dt;
        }
        #endregion

        #region Document Properties
        // Attach Document to CP: spSetDocument_CP(documrntId, CpOfficeId, CpId), tblDocument_CP
        // Get All PrintDocs + attached to Cp: spGetDocsByCP (@CPOfficeID = 2, @CPID = 260133, @AuthorID = 19, @AuthorOfficeID = 1)
        // Attach PrintDoc to CpDoc: spSetCPDoc_Operation: @AuthorId=19; @AuthorOfficeId=1; @CurrentOfficeId=2; @rId=; @CPDocID=2190217; @OperationTypeOfficeID=1; @OperationTypeID=622;
        // Get PrintDocs, attached to CpDoc: spGetCPDoc_Operation (@CPDocID = 2190217, @AuthorID = 19, @AuthorOfficeID = 1)
        #endregion

        #region spGetCustomerProgramByBatchID
        public static string GetPath2Picture(string batchId, Page p)
        {
            /*
			SqlCommand command = new SqlCommand("SELECT v0Part.PartID, v0Part.PartName FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID WHERE (v0Batch.GroupCode = '"+ order.Text.Trim()+"') AND (v0Batch.BatchCode = '"+ batch.Text.Trim()+ "')");
			command.Connection = conn;
			command.CommandType = CommandType.Text;
             */
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = "";
            if (dt.Rows.Count > 0)
            {
                result = "" + dt.Rows[0]["Path2Picture"];
            }
            conn.Close();
            return result;
        }
        public static void GetCpAndCurrentDocsByBatch(UpdateOrderBatchModel batchModel, Page p)
        {
            //-- Find Customer Program
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchModel.BatchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                conn.Close();
                return;
            }

            var cpModel = CustomerProgramModel.Create2(dt.Rows[0]);
            batchModel.Cp = cpModel;
            
            //-- Find Current Docs
            command = new SqlCommand
            {
                CommandText = "spGetCurrentDocsByCP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@CPID", cpModel.CpId));
            command.Parameters.Add(new SqlParameter("@CPOfficeID", cpModel.CpOfficeId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            da = new SqlDataAdapter(command);
            dt = new DataTable();
            da.Fill(dt);
            var docs = (from DataRow row in dt.Rows select new CpDocPrintModel(row)).ToList();
            cpModel.CurrentDocs = docs;
            conn.Close();
            batchModel.CurrentDocs = docs;
        }
        #endregion

        #region Get Default Document Type Code by BatchID
        public static List<DocumentModel> GetDefaultDocumentsByBatchId(string batchId, string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDefaultDocumentTypeCodeByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentModel(row)).ToList();

            conn.Close();
            return result.FindAll(m => m.DocumentTypeCode == documentTypeCode);
            
        }
        public static DataSet GetRejectDocumentId(string batchId, string itemCode, string documentTypeCode, Page p)
        {
            var ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetRejectDocumentID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemCode));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                
                da.Fill(ds);
                
            }
            catch (Exception ex)
            {
                ds = null;
            }
            finally
            {
                conn.Close();
            }
            if (ds.Tables.Count == 1)
                return null;
            return ds;

        }
        #endregion
    }
}