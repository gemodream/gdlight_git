﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;

namespace Corpt.Utilities
{
    public class PrintUtils
    {
        public static string PrintLabelDefaut(int itemTypeId, ShortReportModel report, string itemNumber, CommonPage p, out PrintLabelModel printLabelModel)
        {
            printLabelModel = new PrintLabelModel();
            var itemModel = report.Items.Find(m => m.FullItemNumber == itemNumber);
            if (itemModel == null)
            {
                return "Not found item " + itemNumber;
            }
            printLabelModel.DotNewItemNumber = itemModel.FullItemNumberWithDotes;
            printLabelModel.DotOldItemNumber = itemModel.OldFullItemNumberWithDotes;
            printLabelModel.DotNewBatchNumber =
                itemModel.FullItemNumberWithDotes.Substring(0, itemModel.FullItemNumberWithDotes.Length - 3);

            var itemParts = QueryUtils.GetMeasurePartsShort(itemTypeId, p);
            var isColorDiamond = itemParts.Find(m => m.PartTypeCode == DbConstants.PartTypeCodeColorDiamond) != null;

            //-- Total Weight
            var weights =
                report.ItemValues.FindAll(
                    m =>
                    m.NewItemNumber == itemNumber &&
                    (m.MeasureCode == DbConstants.MeasureCodeTotalWeight1 ||
                     m.MeasureCode == DbConstants.MeasureCodeTotalWeight2));
            if (weights.Count == 0)
            {
                return string.Format("Weight is empty for item {0}. Label will not be printed.", itemNumber);
            }
            var weight = weights[0].ResultValue.Trim();
            
            //-- Color && Clarity
            var colorModels =
                report.ItemValues.FindAll(
                    m => m.NewItemNumber == itemNumber && m.MeasureCode == DbConstants.MeasureCodeColorGrade).ToArray();
            var clarityModels =
                report.ItemValues.FindAll(
                    m => m.NewItemNumber == itemNumber && m.MeasureCode == DbConstants.MeasureCodeClarityGrade).ToArray();
            var colorDiamondModels =
                report.ItemValues.FindAll(
                    m => m.NewItemNumber == itemNumber && m.MeasureCode == DbConstants.MeasureCodeColorDiamond).ToArray();
            var color =  "";
            if (!isColorDiamond && colorModels.Length > 0)
            {
                color = colorModels[0].ResultValue;
            }
            if (isColorDiamond && colorDiamondModels.Length > 0)
            {
                color = colorDiamondModels[0].ResultValue;
            }
            var clarity = clarityModels.Length == 0 ? "" : clarityModels[0].ResultValue;
            
            //-- ShortShapeName (text1)
            var shapeNames =
                report.ItemValues.FindAll(
                    m => m.NewItemNumber == itemNumber && m.MeasureName.Trim().ToUpper() == "SHORTSHAPENAME").ToArray();

            printLabelModel.Barcode = "*" + itemModel.OldFullIemNumber + "*";
            printLabelModel.BarcodeNum = 
                itemModel.FullItemNumber != itemModel.OldFullIemNumber ?
                    string.Format("{0}({1})", itemModel.OldFullItemNumberWithDotes, itemModel.FullItemNumberWithDotes) :
                    itemModel.FullItemNumber;
            printLabelModel.CaratWeight = FormatWeight(weight, "ct.");
            printLabelModel.Color = color;
            printLabelModel.Clarity = clarity;
            printLabelModel.ShortShapeName = shapeNames.Length == 0 ? "" : shapeNames[0].ResultValue;

            //-- IsPrinted
            printLabelModel.IsPrinted = report.ItemsPrinted.FindAll(m => m.FullItemNumber == itemNumber).Count == 0 ? "Z" : "";
            return "";
        }
        private static string FormatWeight(string w, string unit)
        {
            if (string.IsNullOrEmpty(w)) return "";
            try
            {
                return Convert.ToDouble(w).ToString("0.00") + " " + unit;
            } catch (Exception x)
            {
                return "";
            }
        }
        public static void FillCommonLabelProps(ShortReportModel report, ItemModel itemModel, PrintLabelModel printLabelModel)
        {
            //-- OldFullItemNumber
            var valueModel =
                report.ItemValues.Find(
                    m =>
                    m.NewItemNumber == itemModel.FullItemNumber && m.PartName == "Item Container" &&
                    m.MeasureName == "OldItemNumber");
            if (valueModel != null)
            {
                printLabelModel.OldItemNumber = valueModel.ResultValue;
            }

            //-- DotOldFullNumber
            valueModel =
                report.ItemValues.Find(
                    m =>
                    m.NewItemNumber == itemModel.FullItemNumber && m.PartName == "Item Container" &&
                    m.MeasureName == "DotOldItemNumber");
            if (valueModel != null)
            {
                printLabelModel.DotOldItemNumber = valueModel.ResultValue;
            }

            //-- DotNewItemNumber, DotNewBatchNumber
            printLabelModel.DotNewItemNumber = itemModel.FullItemNumberWithDotes;
            printLabelModel.DotNewBatchNumber =
                itemModel.FullItemNumberWithDotes.Substring(0, itemModel.FullItemNumberWithDotes.Length - 3);

            //-- Barcode, Barcodenum
            //var barcode = @ConvertTo128(printLabelModel.OldItemNumber);
            printLabelModel.Barcode = ConvertTo128(printLabelModel.OldItemNumber);
            printLabelModel.BarcodeNum = "GSI# " + printLabelModel.DotOldItemNumber;

        }
        public static PrintLabelModel CreateLabelOldModel(ShortReportModel report, string itemNumber, List<DocumentValueModel> docStruct, out string errMsg, CommonPage p)
        {
            errMsg = "";
            var printLabelModel = new PrintLabelModel { IsTlkw = false };
            //-- ItemModel
            var itemModel = report.Items.Find(m => m.FullItemNumber == itemNumber);
            if (itemModel == null)
            {
                errMsg = "Not found item " + itemNumber;
                return printLabelModel;
            }
            FillCommonLabelProps(report, itemModel, printLabelModel);

            printLabelModel.DotNewItemNumber = itemModel.FullItemNumberWithDotes;
            printLabelModel.DotNewBatchNumber =
                itemModel.FullItemNumberWithDotes.Substring(0, itemModel.FullItemNumberWithDotes.Length - 3);

            //-- IsPrinted
            printLabelModel.IsPrinted = report.ItemsPrinted.FindAll(m => m.FullItemNumber == itemNumber).Count == 0 ? "Z" : "";

            var lblDoc = report.LabelDocuments.ToArray()[0];
            printLabelModel.LblDocumentName = lblDoc.DocumentName;
            
            var srUtils = new ShortReportUtils(false);
            var hasGnum = docStruct.ToArray()[0].Value.ToUpper().Contains("GNUMBER");
            if (hasGnum)
            {
                var gnumValue = srUtils.ParseValue(
                        docStruct[0].Value, itemModel.FullItemNumber,
                        report.ItemValues, new List<ItemFailedModel>());
                printLabelModel.Barcode = string.Format("*{0}*", gnumValue.Trim().ToUpper());
                printLabelModel.BarcodeNum = string.Format("GSI# {0}", gnumValue.Trim().ToUpper());
            }
            var first = hasGnum ? 1 : 0;
            var curr = 1;
            for (var i = first; i < docStruct.ToArray().Length; i++)
            {
                var row = docStruct[i];
                var value = srUtils.ParseValue(
                        row.Value, itemModel.FullItemNumber,
                        report.ItemValues, new List<ItemFailedModel>());
                value = value.Replace("VERY GOOD", "VG").Replace("IDEAL", "ID").Replace("GOOD", "G").Replace("EXCELLENT", "EX").Replace("FAIR", "F").Replace("N/A", "NA");
                value = value.Replace("Very Good", "VG").Replace("Ideal", "ID").Replace("Good", "G").Replace("Excellent", "EX").Replace("Fair", "F");
                value = value + " " + row.Unit;
                value = value.Replace(" %", "%").Replace(" " + Convert.ToChar(176), "" + Convert.ToChar(176));
                value = value.Replace("SQUARE", "SQ.").Replace("MODIFIED", "MOD.");
                value = value.Replace("Square", "Sq.").Replace("Modified", "Mod.");
                if (curr == 1)
                {
                    printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "Value1", Row = 2, Col = 1, CellValue = value});
                }
                if (curr == 2)
                {
                    printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "Value2_3", Row = 3, Col = 1, CellValue = value });
                }
                if (curr == 3)
                {
                    var val23Model = printLabelModel.PaintValues.Find(m => m.CellName == "Value2_3");
                    if (val23Model != null)
                    {
                        val23Model.CellValue += " " + value;
                    }
                }
                if (curr == 4)
                {
                    printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "Value4", Row = 4, Col = 1, CellValue = value });
                }
                if (curr == 4) break;
                curr++;
            }
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "Barcode", Row = 0, Col = 1, CellValue = printLabelModel.Barcode });
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "BarcodeNum", Row = 1, Col = 1, CellValue = printLabelModel.BarcodeNum });
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "IsPrinted", Row = 2, Col = 0, CellValue = printLabelModel.IsPrinted });
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "NewBatchNumber", Row = 5, Col = 1, CellValue = printLabelModel.DotNewBatchNumber });
            
            return printLabelModel;
        }
        public static List<PrintLabelModel> CreateLabelItemize(string order, string batch, int count)
        {
            var result = new List<PrintLabelModel>();
            for (int i = 1; i <= count; i++)
            {
                var fullItemNumber = Utils.FullItemNumber(order, batch, "" + i);
                var dotFullItemNumber = Utils.GetItemNumberWithDots(fullItemNumber);
                var printLabelModel = new PrintLabelModel 
                { 
                    OldItemNumber = fullItemNumber, 
                    NewItemNumber = fullItemNumber ,
                    DotOldItemNumber = dotFullItemNumber,
                    DotNewItemNumber = dotFullItemNumber,
                    Barcode = ConvertTo128(fullItemNumber),
                    BarcodeNum = "GSI# " + dotFullItemNumber
                };
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "Barcode", Row = 0, Col = 1, CellValue = printLabelModel.Barcode });
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "BarcodeNum", Row = 1, Col = 1, CellValue = printLabelModel.BarcodeNum });
                result.Add(printLabelModel);
            }
            return result;
        }
        public static List<PrintLabelModel> CreateLabelTlkw(ShortReportModel report, string itemNumber, List<DocumentValueModel> docStruct, out string errMsg, CommonPage p)
        {
            errMsg = "";
            var result = new List<PrintLabelModel>();
            
            //-- ItemModel
            var itemModel = report.Items.Find(m => m.FullItemNumber == itemNumber);
            if (itemModel == null)
            {
                errMsg = string.Format("Not found item {0}", itemNumber);
                return result;
            }
            var printLabelModel = new PrintLabelModel { IsTlkw = true };

            FillCommonLabelProps(report, itemModel, printLabelModel);
            printLabelModel.PaintValues.Add(new PrintLblPaintModel{CellName = "TlkwBarcode", Row = 0, Col = 0, CellValue = printLabelModel.Barcode});
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwDate", Row = 0, Col = 3, CellValue = DateTime.Now.Date.ToShortDateString() });
            printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwBarcodeNum", Row = 1, Col = 3, CellValue = printLabelModel.BarcodeNum });

            var lblDoc = report.LabelDocuments.ToArray()[0];
            printLabelModel.LblDocumentName = lblDoc.DocumentName;

            var srUtils = new ShortReportUtils(false);
            var isLeft = true;
            const int baseRow = 2;
            const int baseRowCount = 6;
            var currRow = 0;
            foreach (var row in docStruct)
            {
                if (row.Title == "$CUTGRADEFAILCODE")
                {
                    var failCode = srUtils.ParseValue(
                        row.Value, itemModel.FullItemNumber,
                        report.ItemValues, new List<ItemFailedModel>());
                    printLabelModel.GradeFailCodes = GetCutGradeFailGroupCodes(failCode);
                    continue;
                }
                var value = srUtils.ParseValue(
                    row.Value, itemModel.FullItemNumber,
                    report.ItemValues, new List<ItemFailedModel>());
                value = value.Replace("VERY GOOD", "VG").Replace("IDEAL", "ID").Replace("GOOD", "G").Replace("EXCELLENT", "EX").Replace("FAIR", "F").Replace("N/A", "NA");
                value = value.Replace("Very Good", "VG").Replace("Ideal", "ID").Replace("Good", "G").Replace("Excellent", "EX").Replace("Fair", "F");
                value = value + " " + row.Unit;
                value = value.Replace(" %", "%").Replace(" " + Convert.ToChar(176), "" + Convert.ToChar(176));
                value = value.Replace("SQUARE", "SQ.").Replace("MODIFIED", "MOD.");
                value = value.Replace("Square", "Sq.").Replace("Modified", "Mod.");
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = string.Format("Tlkw{0}Title{1}", isLeft ? "L" : "R", currRow), Row = baseRow + currRow, Col = (isLeft ? 0 : 2), CellValue = row.Title });
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = string.Format("Tlkw{0}Value{1}", isLeft ? "L" : "R", currRow), Row = baseRow + currRow, Col = (isLeft ? 0 : 2) + 1, CellValue = value });
                
                currRow++;
                if (currRow <= baseRowCount) continue;
                isLeft = false;
                currRow = 0;
            }
            //-- Generate comments value
            var comments = "";
            if (docStruct.Find(m => m.Value.ToUpper().Contains("CUTGRADE]")) != null) comments += "Cut";
            if (docStruct.Find(m => m.Value.ToUpper().Contains("POLISH]")) != null) comments += (comments.Length > 0 ? @"/" : "") + "Polish";
            if (docStruct.Find(m => m.Value.ToUpper().Contains("SYMMETRY]")) != null) comments += (comments.Length > 0 ? @"/" : "") + "Symmentry";
            if (comments.Length > 0)
            {
                comments = "*" + comments;
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwComments1", Row = 9, Col = 0, CellValue = comments });
            }
            result.Add(printLabelModel);
            //-- Generate comments2 value
            if (printLabelModel.GradeFailCodes.Count > 0)
            {
                printLabelModel.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwComments2", Row = 9, Col = 2, CellValue = "See Comments on 2nd Label" });

                //-- Generate 2nd Label
                
                var printLabelModel2 = new PrintLabelModel { IsTlkw = true, IsSecond = true, NewItemNumber = printLabelModel.NewItemNumber, DotOldItemNumber = printLabelModel.DotOldItemNumber };
                printLabelModel2.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwBarcode", Row = 0, Col = 0, CellValue = printLabelModel.Barcode });
                printLabelModel2.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwDate", Row = 0, Col = 3, CellValue = DateTime.Now.Date.ToShortDateString() });
                printLabelModel2.PaintValues.Add(new PrintLblPaintModel { CellName = "TlkwBarcodeNum", Row = 1, Col = 3, CellValue = printLabelModel.BarcodeNum });
                
                currRow = 0;
                printLabelModel2.PaintValues.Add(new PrintLblPaintModel { CellName = string.Format("Tlkw{0}Title{1}", "L", currRow), Row = currRow + 2, Col = 1, CellValue = "Comments: " });
                foreach (var fail in printLabelModel.GradeFailCodes)
                {
                    currRow++;
                    printLabelModel2.PaintValues.Add(new PrintLblPaintModel { CellName = string.Format("Tlkw{0}Title{1}", "L", currRow), Row = currRow + 2, Col = 1, CellValue = fail });
                    if (currRow == 7) break;
                }
                result.Add(printLabelModel2);
            }
            return result;
        }
        public static string ConvertTo128(string chaine)
        {
            var checksum = 0;
            int dummy;
            var code128 = "";

            
            if (string.IsNullOrEmpty(chaine)) return "";
            var longueur = chaine.Length;
            for (int i = 0; i < longueur; i++)
            {
                if ((chaine[i] < 32) || (chaine[i] > 126)) return "";
            }

            var tableB = true;
            var ind = 0;

            while (ind < longueur)
            {
                int mini;
                if (tableB)
                {
                    mini = ((ind == 0) || (ind + 3 == longueur - 1) ? 4 : 6) - 1;

                    if ((ind + mini) <= longueur - 1)
                    {
                        while (mini >= 0)
                        {
                            if ((chaine[ind + mini] < 48) || (chaine[ind + mini] > 57))
                            {
                                code128 = "";
                                break;
                            }
                            mini = mini - 1;
                        }
                    }

                    if (mini < 0)
                    {
                        if (ind == 0)
                            code128 = Char.ToString((char)205);
                        else
                            code128 = code128 + Char.ToString((char)199);
                        tableB = false;
                    }
                    else
                    {
                        if (ind == 0)
                            code128 = Char.ToString((char)204);
                    }
                }

                if (tableB == false)
                {
                    mini = 2;
                    mini = mini - 1;
                    if (ind + mini < longueur)
                    {
                        while (mini >= 0)
                        {
                            if (((chaine[ind + mini]) < 48) || ((chaine[ind]) > 57))
                            {
                                break;
                            }
                            mini = mini - 1;
                        }
                    }

                    if (mini < 0)
                    {
                        dummy = Int32.Parse(chaine.Substring(ind, 2));

                        if (dummy < 95)
                            dummy = dummy + 32;
                        else
                            dummy = dummy + 100;

                        code128 = code128 + (char)(dummy);
                        ind = ind + 2;
                    }
                    else
                    {
                        code128 = code128 + Char.ToString((char)200);
                        tableB = true;
                    }
                }
                if (!tableB) continue;
                code128 = code128 + chaine[ind];
                ind = ind + 1;
            }

            for (ind = 0; ind <= code128.Length - 1; ind++)
            {
                dummy = code128[ind];
                if (dummy < 127)
                    dummy = dummy - 32;
                else
                    dummy = dummy - 100;

                if (ind == 0)
                {
                    checksum = dummy;
                }
                checksum = (checksum + (ind) * dummy) % 103;
            }

            if (checksum < 95)
            {
                checksum = checksum + 32;
            }
            else
            {
                checksum = checksum + 100;
            }
            code128 = code128 + Char.ToString((char)checksum) + Char.ToString((char)206);
            return code128;
        }


        private static List<string> GetCutGradeFailGroupCodes(string sCutGradeFailCode)
        {
            var alCodeDescription = new List<string>();
            if (string.IsNullOrEmpty(sCutGradeFailCode)) return alCodeDescription;
            try
            {
                var y = 10;
                var i = 0;
                var iMyCutGradeCode = Convert.ToInt32(Convert.ToDecimal(sCutGradeFailCode));
                if (iMyCutGradeCode > 0)
                {
                    for (; iMyCutGradeCode > 0; )
                    {
                        i++;
                        var iRemainder = iMyCutGradeCode % y;
                        iMyCutGradeCode = iMyCutGradeCode / y;
                        if (iRemainder != 0) alCodeDescription.Add("NOT IDEAL " + i);
                    }
                }
            }
            catch { }

            return alCodeDescription;
        }
        public static FileInfoModel FindPdfDocument(string itemNumber, string virtualNumber, Page p)
        {
            //-- From Global.asax
            var dirName = "" + p.Session[SessionConstants.GlobalDocumentRoot];
            if (!(dirName.EndsWith("\\") || dirName.EndsWith("/")))
                dirName = dirName + "/";

            //-- Exists dir
            try
            {
                var dir = new DirectoryInfo(p.Server.MapPath(dirName));
                if (!dir.Exists)
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            var findModel = new ReportFindModel { ItemNumber = itemNumber, VirtualVaultNumber = virtualNumber };
            string[] sDocumentType = { "H", "D", "S", "T", "C", "Q", "Z" };
            var al0 = new List<string>();

            //al0.Add("*.pdf");
            al0.Add("367_" + findModel.ItemNumber + " T.pdf");
            al0.Add("133_" + findModel.ItemNumber + " T.pdf");
            al0.Add(findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
            al0.Add(findModel.ItemNumber + ".pdf");

            foreach (var type in sDocumentType)
            {
                al0.Add(type + findModel.ItemNumber + "." + findModel.VirtualVaultNumber + ".pdf");
            }

            foreach (var obj in al0)
            {
                var strTempFileName = "" + p.Server.MapPath(dirName) + obj;
                var fi = new FileInfo(strTempFileName);
                if (fi.Exists)
                {
                    return new FileInfoModel { FileName = fi.Name, LastWriteTimeUtc = fi.LastWriteTimeUtc, Exists = true, VirtualPath = dirName + fi.Name};
                }
            }
            return null;

        }
        public static List<PrintLabel2Model> Convert2ColumnsFormat(List<PrintLabelModel> labels)
        {
            var labels2 = new List<PrintLabel2Model>();
            for (int i = 0; i < labels.Count; i += 2)
            {
                 var label2 = new PrintLabel2Model();
                 label2.LabelOne = labels[i];
                 if (i == (labels.Count - 1))
                 {
                     label2.LabelTwo = new PrintLabelModel();
                 }
                 else
                 {
                     label2.LabelTwo = labels[i + 1];
                 }
                 labels2.Add(label2);
            }
            return labels2;
        }

        public static DataTable GetReprintBatchID(string order, string batch, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select BatchID from v0batch where GroupCode = " + order + " and BatchCode = " + batch,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;

        }

    }
}