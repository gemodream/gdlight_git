﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net;

namespace Corpt.Utilities
{
    public class AccessUtils
    {
        #region Access To Pages
        public static string PrgBulkUpdate = "BulkUpdate";
        public static string PrgReassembly = "Reassembly";
        public static string PrgRemoteSession = "RemoteSession";
        public static string PrgItemHistory = "ItemHistory";
		public static string PrgAutoMeasure = "AutoMeasure";
        /*IvanB start*/
        public static string PrgUtilities = "ScreeningUtilities";
        public static string CustomerProgramAccess = "CustomerProgramAccess";
        public static string Management = "Management";
        /*IvanB end*/
        public static string PrgAppraisal = "OpenAppraisal";
        public static string PrgBulkBilling = "BulkBilling";

        public static bool HasAccessToPage(string userId, string prg, Page p, out string errMsg)
        {
            errMsg = "";
            try
            {
                var ds = new DataSet();
                ds.ReadXml(p.Server.MapPath("Users.xml"));
                const string format = "{0}_Id='0' and Id='{1}'";
                return ds.Tables[1].Select(string.Format(format, prg, userId)).Length > 0;

            }
            catch (Exception ex)
            {
               errMsg = ex.Message;
            }
            return false;
        }

        public static bool IsBulkBillingUser(string userId, Page p, out string errMsg)
        {
            errMsg = "";
            try
            {
                var ds = new DataSet();
                ds.ReadXml(p.Server.MapPath("Users.xml"));
                const string format = "{0}_Id='0' and Id='{1}'";
                return ds.Tables[1].Select(string.Format(format, PrgBulkBilling, userId)).Length > 0;

            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            return false;
        }

        public static IEnumerable<string> GetOfficeIps(Page p, out string errMsg)
        {
            errMsg = "";
            var ds = new DataSet();
            try
            {
                ds.ReadXml(p.Server.MapPath("OfficeIps.xml"));
                var ips = (from DataRow row in ds.Tables[0].Select("ID <>''") select row["ID"].ToString()).ToArray();
                return ips;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            return new List<string>();
        }
        public static IEnumerable<string> GetOfficeIpsByHost(Page p, out string errMsg)
        {
            errMsg = "";
            var result = new List<string>();
            var ds = new DataSet();
            try
            {
                ds.ReadXml(p.Server.MapPath("OfficeIps.xml"));
                var hosts = (from DataRow row in ds.Tables[0].Select("Host <>''") select row["Host"].ToString()).ToArray();
                //var myHost1 = LocationUtils.GetDnsIps("gsibetnt.is-found.org");
                //var myHost2 = LocationUtils.GetDnsIps("ilgsi.dnsalias.com");
                foreach (var host in hosts)
                {
                    var ips = LocationUtils.GetDnsIps(host);
                    foreach (IPAddress ipAddr in ips)
                    {
                        result.Add(ipAddr.ToString());
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            return new List<string>();
        }

        #endregion
    }
}