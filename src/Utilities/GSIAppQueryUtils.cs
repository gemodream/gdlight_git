﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using Corpt.Models.Stats;

namespace Corpt.Utilities
{
    public class GSIAppQueryUtils : BaseQueryUtils
    {
        #region Synthetic Screening

        public static List<SyntheticScreeningModel> GetScreeningByOrder(Int32 orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@GSIOrder", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
            conn.Close();
            return result;
        }
		
		

        public static DataSet GetScreeningDetailsByOrder(Int32 orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@GSIOrder", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            ;
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }

		

        public static List<SyntheticScreeningModel> GetScreeningDetailsByDestination(string destination, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetScreeningByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Destination", destination);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
            conn.Close();
            return result;
        }

		

        public static List<SyntheticScreeningModel> GetScreeningByCreateDate(DateTime dtCreateFromDate, DateTime dtCreateToDate, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@CreateDate", dtCreateFromDate);
            command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
            conn.Close();
            return result;
        }
		
		

        public static string SaveSyntheticCustomerData(string memo, string poNum, string sku, string qty, string style, string retailer,
            Page p, int update, string custCode, string gsiOrder)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                string sql = null;
                if (update == 1)
                    sql = @"update tblSyntheticCustomerEntries set poNum = '" + poNum + @"', totalQty = " + qty + @", stylename = '" + style +
                        @"', skuName = '" + sku + @"', retailer = '" + retailer + @"' where memoNum = '" + memo + @"' and customercode = " + custCode;
                else
                    sql = @"insert into  tblSyntheticCustomerEntries(memoNum, poNum, totalQty, stylename, skuName, retailer, GSIOrder, CreatedDate, CustomerCode ) values('" + memo + @"', '" +
                        poNum + @"', " + qty + @", '" + style + @"', '" + sku + @"', '" + retailer + @"', null, getdate(), " + custCode + @")";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
                return "success";
            }
            catch (Exception e)
            {

                //LogUtils.UpdateSpLogWithParameters(command, p);
                conn.Close();
                return "failed";
            }
        }//SaveSyntheticCustomerData
		
		public static bool AddOrderToSynthScreening(string groupCode, string retailer, string poNum, string qty, string sku, string style, string memo, string custCode, Page p)
        {
            SqlCommand command = new SqlCommand("spAddOrderToSyntheticScreening");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@GSIOrder", groupCode));
            command.Parameters.Add(new SqlParameter("@Memo", memo));
            command.Parameters.Add(new SqlParameter("@Sku", sku));
            command.Parameters.Add(new SqlParameter("@Style", style));
            command.Parameters.Add(new SqlParameter("@Retailer", retailer));
            command.Parameters.Add(new SqlParameter("@POnum", poNum));
            command.Parameters.Add(new SqlParameter("@VendorNum", custCode));
            command.Parameters.Add(new SqlParameter("@Qty", Convert.ToInt32(qty)));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }//AddOrderToSynthScreening
        
		public static bool AddBatchToSynthScreening(string groupCode, string retailer, string poNum, string qty, string sku, string style, string memo, string batchNumber, string custCode, Page p)
        {
            SqlCommand command = new SqlCommand("spAddBatchToSyntheticScreening");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@GSIOrder", Convert.ToInt32(groupCode)));
            command.Parameters.Add(new SqlParameter("@Memo", memo));
            command.Parameters.Add(new SqlParameter("@Sku", sku));
            command.Parameters.Add(new SqlParameter("@Style", style));
            command.Parameters.Add(new SqlParameter("@Retailer", retailer));
            command.Parameters.Add(new SqlParameter("@POnum", poNum));
            command.Parameters.Add(new SqlParameter("@VendorNum", custCode));
            command.Parameters.Add(new SqlParameter("@Qty", Convert.ToInt32(qty)));
            command.Parameters.Add(new SqlParameter("@BatchNumber", Convert.ToInt32(batchNumber)));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }//AddBatchToSynthScreening

        /* alex
        public static string SaveSyntheticCustomerData(string memo, string poNum, string sku, string qty, string bagQty, Page p, int update, string custCode, string gsiOrder)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                string sql = null;
                if (update == 1)
                    sql = @"update tblSyntheticCustomerEntry set poNum = '" + poNum + @"', totalQty = " + qty + @", bagQty = " + bagQty +
                        @", skuName = '" + sku + @"' where memoNum = '" + memo + @"'";
                else
                    sql = @"insert into  tblSyntheticCustomerEntry(memoNum, poNum, totalQty, bagQty, skuName, GSIOrder, CreatedDate, CustomerCode ) values('" + memo + @"', '" +
                        poNum + @"', " + qty + @", " + bagQty + @", '" + sku + @"', " + gsiOrder + @", getdate(), " + custCode + @")";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
                return "success";
            }
            catch (Exception e)
            {

                //LogUtils.UpdateSpLogWithParameters(command, p);
                conn.Close();
                return "failed";
            }
        }
        alex */
        public static DataTable GetPartIdList(int groupcode, int batchcode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select PartId, PartName from v0part where ItemTypeID=(select distinct itemtypeid from v0batch where GroupCode=" +  groupcode + " and batchcode=" + batchcode + ")";
            SqlDataAdapter adapter;
            DataTable dt = new DataTable();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(dt);
                conn.Close();
                if (dt != null && dt.Rows.Count != 0)
                    return dt;
                else
                    return null;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }//GetRequestId
        public static bool UpdateItemFailedInfo(string gsiNumber, int part, string synth, Page p)
        {
            long groupCode = Convert.ToInt64(gsiNumber.Substring(0, gsiNumber.Length - 5));
            string strbatchCode = gsiNumber.Substring(gsiNumber.Length - 5,3);
            string stritemCode = gsiNumber.Substring(gsiNumber.Length - 2);
            int batchCode = Convert.ToInt32(strbatchCode);
            int itemCode = Convert.ToInt16(stritemCode);
            int partId = Convert.ToInt16(part);
            SqlCommand command = new SqlCommand("spSetFailedItemInfo");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@GroupCode", groupCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchCode));
            command.Parameters.Add(new SqlParameter("@OrderItemCode", itemCode));
            command.Parameters.Add(new SqlParameter("@PartID", part));
            command.Parameters.Add(new SqlParameter("@Synth", synth));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SetInvalidGroupState(string gsiNumber, Page p)
        {
            long groupCode = Convert.ToInt64(gsiNumber.Substring(0, gsiNumber.Length - 5));
            SqlCommand command = new SqlCommand("spSetGroupStateByCode");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@GroupCode", groupCode));
            command.Parameters.Add(new SqlParameter("@StateCode", 3));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", 1));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SetInvalidBatchState(string gsiNumber, Page p)
        {
            long groupCode = Convert.ToInt64(gsiNumber.Substring(0, gsiNumber.Length - 5));
            string strbatchCode = gsiNumber.Substring(gsiNumber.Length - 5, 3);
            int batchCode = Convert.ToInt32(strbatchCode);
            SqlCommand command = new SqlCommand("spSetBatchStateByCode");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@GroupCode", groupCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchCode));
            command.Parameters.Add(new SqlParameter("@StateCode", 3));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", 1));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SetInvalidItemState(string gsiNumber, Page p)
        {
            long groupCode = Convert.ToInt64(gsiNumber.Substring(0, gsiNumber.Length - 5));
            string strbatchCode = gsiNumber.Substring(gsiNumber.Length - 5, 3);
            string stritemCode = gsiNumber.Substring(gsiNumber.Length - 2);
            int batchCode = Convert.ToInt32(strbatchCode);
            int itemCode = Convert.ToInt16(stritemCode);
            SqlCommand command = new SqlCommand("spSetItemStateByCode");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@GroupCode", groupCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchCode));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemCode));
            command.Parameters.Add(new SqlParameter("@StateCode", 3));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorID", 1));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", 1));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static int GetRequestId(string memo, int custCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select requestId from tblSyntheticCustomerEntries where memoNum = '" + memo + @"' and customercode = " + custCode.ToString();
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                if (ds != null && ds.Tables[0].Rows.Count != 0)
                    return Convert.ToInt16(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                else
                    return 0;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }//GetRequestId
        public static DataSet GetMessengersList(string custCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select * from v0Person where CustomerID=(select CustomerID from v0Customer where CustomerCode=" + custCode + @")";
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public static DataSet GetCustomerName(string custCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select * from v0customer where customercode = " + custCode;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                /* alex
                if (ds == null || ds.Tables[0].Rows.Count == 0)
                    return null;
                else
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    */
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public static int? GetPersonID(string custCode, string messengerFullName, Page p)
        {
            string firstName = null, lastName = null;
            string[] fullName = messengerFullName.Split(' ');
            firstName = fullName[0];
            lastName = fullName[1];
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select personId from v0Person where firstname = '" + firstName + @"' and lastname = '" + lastName + @"' and customerId = " + custCode;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                if (ds == null || ds.Tables[0].Rows.Count == 0)
                    return null;
                else
                    return Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public static DataSet GetGroupId(string groupCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select groupid from v0group where groupcode = " + groupCode;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public static bool SetMemoNumber(string groupId, string memo, int authorId, int authorOfficeId, int currentOfficeId, Page p)
        {
            SqlCommand command = new SqlCommand("spSetGroupMemoNumber");
            command.Connection = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@GroupID", groupId));
            command.Parameters.Add(new SqlParameter("@Name", memo));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", currentOfficeId));
            command.Parameters.Add(new SqlParameter("@AuthorID", authorId));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", authorOfficeId));
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }//SetMemoNumber

        public static DataSet GetCPInfo(string customerCodeName, string txtCustomerCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            //string sql = @"select requestId from v1customerprogram1 where customercode = " + customerCodeName + @" and customerprogramname = '" + customerCodeName + @"'";
            string sql = @"select top 1 * from v1customerprogram1 where customerprogramname = '" + customerCodeName + @"'";
            //string sql = @"select top 1 * from v1customerprogram1 where customercode = " + Convert.ToInt16(txtCustomerCode) + @" and customerprogramname = '" + customerCodeName + @"'";
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }//GetCPInfo

        public static string AddSyntheticBatch(int authorId, int authorOfficeId, int currentOfficeId, string groupId,
                            string itemTypeId, string batchItemsNumber, string cpid, string batchMemo, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSyntheticAddBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@GroupOfficeID", currentOfficeId);
                command.Parameters.AddWithValue("@GroupID", Convert.ToInt32(groupId));
                command.Parameters.AddWithValue("@ItemTypeID", Convert.ToInt32(itemTypeId));
                command.Parameters.AddWithValue("@ItemsQuantity", Convert.ToInt32(batchItemsNumber));
                command.Parameters.AddWithValue("@CPOfficeID", currentOfficeId);
                command.Parameters.AddWithValue("@CPID", Convert.ToInt32(cpid));
                command.Parameters.AddWithValue("@AuthorID", authorId);
                command.Parameters.AddWithValue("@AuthorOfficeID", authorOfficeId);
                command.Parameters.AddWithValue("@StoredItemsQuantity", Convert.ToInt32(batchItemsNumber));
                command.Parameters.AddWithValue("@CurrentOfficeID", currentOfficeId);
                //command.Parameters.AddWithValue("@MemoNumber", batchMemo);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                if (ds != null && ds.Tables[0].Rows.Count != 0)
                {
                    string batchId = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    return batchId;

                }

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                //return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }//AddSyntheticBatch
        public static DataSet GetGroupWithCustomer(string groupId, int authorId, int authorOfficeId, int groupOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetGroupWithCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@GroupID", groupId);
                command.Parameters.AddWithValue("@GroupOfficeID", groupOfficeId);
                command.Parameters.AddWithValue("@AuthorID", authorId);
                command.Parameters.AddWithValue("@AuthorOfficeID", authorOfficeId);

                LogUtils.UpdateSpLogWithParameters(command, p);
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//GetGroupWithCustomer

        public static DataSet GetAuthorData(string groupId, int authorId, int authorOfficeId, int groupOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetGroupAuthor",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@GroupID", groupId);
                command.Parameters.AddWithValue("@GroupOfficeID", groupOfficeId);
                command.Parameters.AddWithValue("@AuthorID", authorId);
                command.Parameters.AddWithValue("@AuthorOfficeID", authorOfficeId);

                LogUtils.UpdateSpLogWithParameters(command, p);
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//GetAuthorData

        public static DataSet GetSyntheticEntriesByOrder(string groupCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticEntriesByOrder1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@GSIOrder", Convert.ToInt32(groupCode));

                LogUtils.UpdateSpLogWithParameters(command, p);
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//GetSyntheticEntriesByOrder

        public static DataTable GetSyntheticCustomerEntriesByOrder(string groupCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticCustomerEntriesByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@Order", groupCode);

                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//GetSyntheticEntriesByOrder
        public static DataSet GetSyntheticCustomerEntries(string requestId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select * from tblSyntheticCustomerEntries where requestId = " + requestId;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }//GetSyntheticCustomerEntries

        public static string UpdateCustomerData(int requestId, string memo, int groupCode, int customerCode, string poNum,
            string sku, int totalQty, string style, string retailer, string messenger, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSynthUpdateCustomerData",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@RequestID", requestId);
                command.Parameters.AddWithValue("@Memo", memo);
                command.Parameters.AddWithValue("@GroupCode", groupCode);
                command.Parameters.AddWithValue("@CustomerCode", customerCode);
                command.Parameters.AddWithValue("@PONum", poNum);
                command.Parameters.AddWithValue("@SKU", sku);
                command.Parameters.AddWithValue("@TotalQty", totalQty);
                command.Parameters.AddWithValue("@Style", style);
                command.Parameters.AddWithValue("@Retailer", retailer);
                command.Parameters.AddWithValue("@Messenger", messenger);


                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return "success";
            }
            catch (Exception ex)
            {
                return "error";
            }
        }//UpdateCustomerData

        public static DataSet GetShortStatsDetails(string strCreateFromDate, string strCreateToDate, string GSIOrder, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetShortStatsAll",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            DateTime dtCreateFromDate = new DateTime();
            DateTime dtCreateToDate = new DateTime();
            if (strCreateFromDate != null)
            {
                dtCreateFromDate = DateTime.Parse(strCreateFromDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDate", dtCreateFromDate);
            }
            if (strCreateToDate != null)
            {
                dtCreateToDate = DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            }
            if (GSIOrder != null)
                command.Parameters.AddWithValue("@GSIOrder", Convert.ToInt32(GSIOrder));
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }//GetShortStatsDetails

        public static string SaveShortScreeningData(SyntheticShortStatsModel synScrData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveShortScreening",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //command.Parameters.AddWithValue("@ScreeningID", synScrData.ScreeningID);
            command.Parameters.AddWithValue("@GSIOrder", synScrData.GSIOrder);
            command.Parameters.AddWithValue("@User", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@VendorNum", synScrData.CustomerCode.ToString());
            command.Parameters.AddWithValue("@MemoNum", synScrData.Memo.Trim());
            command.Parameters.AddWithValue("@TotalQty", synScrData.TotalQty);
            command.Parameters.AddWithValue("@QtyPass", synScrData.QtyPass);
            //command.Parameters.AddWithValue("@QtyFail", synScrData.QtyFail);
            //command.Parameters.AddWithValue("@FTQuantity", synScrData.FTQuantity);
            //command.Parameters.AddWithValue("@SKUName", synScrData.SKUName.Trim());
            //command.Parameters.AddWithValue("@Test", synScrData.Test);
            // command.Parameters.AddWithValue("@CertifiedBy", synScrData.CertifiedBy);
            command.Parameters.AddWithValue("@Destination", synScrData.Destination);

            command.Parameters.AddWithValue("@QtySynth", synScrData.QtySynth);
            command.Parameters.AddWithValue("@QtySusp", synScrData.QtySusp);
            command.Parameters.AddWithValue("@Notes", synScrData.Notes.Trim());
            //command.Parameters.AddWithValue("@XmlItemsData", xmlItemsData);

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }//SaveShortScreeningData

        public static List<SyntheticShortStatsModel> GetSyntheticShortInit(Int32 orderCode, string office, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticShortByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@GSIOrder", orderCode);
            if (office != null && office != "" && office != "all")
                command.Parameters.AddWithValue("@OfficeID", Convert.ToInt32(office));


            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
            conn.Close();
            return result;
        }//GetSyntheticShortInit
        
        public static DataSet GetShortStatsAll(string strCreateFromDate, string strCreateToDate, string GSIOrder, string office, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetShortStatsAllAlex1Test",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            DateTime dtCreateFromDate = new DateTime();
            DateTime dtCreateToDate = new DateTime();
            if (strCreateFromDate != null)
            {
                dtCreateFromDate = DateTime.Parse(strCreateFromDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDate", dtCreateFromDate);
            }
            if (strCreateToDate != null)
            {
                dtCreateToDate = DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            }
            if (strCreateFromDate != null && strCreateToDate == null)
            {
                dtCreateToDate = DateTime.Now; //DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            }
            if (GSIOrder != null)
                command.Parameters.AddWithValue("@GSIOrder", Convert.ToInt32(GSIOrder));
            if (office != null && office != "" && office != "all")
                command.Parameters.AddWithValue("@OfficeID", Convert.ToInt32(office));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            //var result = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
            conn.Close();
            return ds;
            //return result;
        }//GetShortStatsAll

        public static string GetCustomerEntriesDestination(string gsiOrder, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCustomerEntriesDestination",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@GSIOrder", gsiOrder);
                
                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                if (dt != null && dt.Rows.Count > 0)
                {
                    string dest = dt.Rows[0]["Destination"].ToString();
                    return dest;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return "";
        }

        public static DataTable GetShortStatsClosed(string strCreateFromDate, string strCreateToDate, string GSIOrder, string office, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetShortStatsClosed",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            DateTime dtCreateFromDate = new DateTime();
            DateTime dtCreateToDate = new DateTime();
            if (strCreateFromDate != null)
            {
                dtCreateFromDate = DateTime.Parse(strCreateFromDate, CultureInfo.CreateSpecificCulture("en-US"));
                string tmpFrom = dtCreateFromDate.ToString();
                command.Parameters.AddWithValue("@CreateDate", dtCreateFromDate.ToString());
            }
            if (strCreateToDate != null)
            {
                dtCreateToDate = DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
                string tmpTo = dtCreateToDate.ToString();
                command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate.ToString());
            }
            if (strCreateFromDate != null && strCreateToDate == null)
            {
                dtCreateToDate = DateTime.Now; //DateTime.Parse(strCreateToDate, CultureInfo.CreateSpecificCulture("en-US"));
                command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            }
            if (GSIOrder != null)
                command.Parameters.AddWithValue("@GSIOrder", Convert.ToInt32(GSIOrder));
            if (office != null && office != "" && office != "all")
                command.Parameters.AddWithValue("@OfficeID", Convert.ToInt32(office));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            //var result = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
            conn.Close();
            return dt;
            //return result;
        }//GetShortStatsClosed

        public static List<SyntheticShortStatsModel> GetScreeningShortDetailsByDestination(string destination, string office, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetScreeningShortByDestination",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Destination", destination);
            if (office != null && office != "")
                command.Parameters.AddWithValue("@OfficeID", Convert.ToInt32(office));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticShortStatsModel(row)).ToList();
            conn.Close();
            return result;
        }//GetScreeningShortDetailsByDestination

        public static DataSet CreateScreeningNewOrder(int authorId, int authorOfficeId, int currentOfficeId, int groupOfficeId,
                                        int itemsQuantity, int serviceTypeId, string memo, string specialInstruction,
                                        int personCustomerOfficeId, int personCustomerId, int? personId, int customerOfficeId,
                                        int customerId, Page p)
        {
            if (personId == null)
                personId = 0;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetSyntheticEntryBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            try
            {
                command.Parameters.AddWithValue("@AuthorID", authorId);
                command.Parameters.AddWithValue("@AuthorOfficeID", authorOfficeId);
                command.Parameters.AddWithValue("@CurrentOfficeID", currentOfficeId);
                command.Parameters.AddWithValue("@GroupOfficeID", groupOfficeId);
                command.Parameters.AddWithValue("@ItemsQuantity", itemsQuantity);
                command.Parameters.AddWithValue("@ServiceTypeID", serviceTypeId);
                command.Parameters.AddWithValue("@Memo", memo);
                command.Parameters.AddWithValue("@SpecialInstruction", specialInstruction);
                command.Parameters.AddWithValue("@PersonCustomerOfficeID", personCustomerOfficeId);
                command.Parameters.AddWithValue("@PersonCustomerID", personCustomerId);
                command.Parameters.AddWithValue("@PersonID", personId);
                command.Parameters.AddWithValue("@CustomerOfficeID", customerOfficeId);
                command.Parameters.AddWithValue("@CustomerID", customerId);

                LogUtils.UpdateSpLogWithParameters(command, p);
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);

                //var result = (from DataRow row in dt.Rows select new SyntheticScreeningModel(row)).ToList();
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//CreateScreeningNewOrder

        public static string SaveSuspectListData(string xmlItems, string order, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveSuspectListData",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@XmlItemsData", xmlItems);
            command.Parameters.AddWithValue("@GSINumber", order);
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
        
        public static string SaveSyntheticListData(string xmlItems, string order, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveSyntheticListData",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@XmlItemsData", xmlItems);
            command.Parameters.AddWithValue("@GSINumber", order);
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }

        public static string SaveSyntheticUserData(SyntheticScreeningModel synScrData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveSyntheticUserData",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ItemNumber", synScrData.GSIOrder);
            command.Parameters.AddWithValue("@UserName", synScrData.CreatedBy);
            command.Parameters.AddWithValue("@ScreeningType", synScrData.ScreeningType);
            command.Parameters.AddWithValue("@TestType", synScrData.Test);
            command.Parameters.AddWithValue("@FurtherTest", synScrData.FTQuantity);
            command.Parameters.AddWithValue("@Suspect", synScrData.QtySuspect);
            command.Parameters.AddWithValue("@Synthetic", synScrData.QtySynthetic);
            command.Parameters.AddWithValue("@TotalQty", synScrData.TotalQty);

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
                msg = "success";
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
        public static string GetSyntheticCustomer(string loginName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"SELECT customerCode FROM tblSyntheticCustomers WHERE Login = '" + loginName + @"'";
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            string custCode = null;
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    custCode = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    conn.Close();
                    return custCode;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }//GetSyntheticCustomer

        public static string SaveScreeningData(SyntheticScreeningModel synScrData, string xmlItemsData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSaveScreening",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            synScrData.SyntheticSuspect = 0;
            command.Parameters.AddWithValue("@ScreeningID", synScrData.ScreeningID);
            command.Parameters.AddWithValue("@GSIOrder", synScrData.GSIOrder);
            command.Parameters.AddWithValue("@User", synScrData.CreatedBy);
            
            command.Parameters.AddWithValue("@VendorNum", synScrData.VendorNum.Trim());
            command.Parameters.AddWithValue("@PONum", synScrData.PONum.Trim());
            command.Parameters.AddWithValue("@TotalQty", synScrData.TotalQty);
            command.Parameters.AddWithValue("@QtyPass", synScrData.QtyPass);
            command.Parameters.AddWithValue("@QtyFail", synScrData.QtyFail);
            command.Parameters.AddWithValue("@FTQuantity", synScrData.FTQuantity);
            command.Parameters.AddWithValue("@QtySynthetic", synScrData.QtySynthetic);
            command.Parameters.AddWithValue("@QtySuspect", synScrData.QtySuspect);
            command.Parameters.AddWithValue("@SyntheticSuspect", synScrData.SyntheticSuspect);
            command.Parameters.AddWithValue("@SKUName", synScrData.SKUName.Trim());
            command.Parameters.AddWithValue("@Test", synScrData.Test);
            command.Parameters.AddWithValue("@CertifiedBy", synScrData.ScreeningType);
            command.Parameters.AddWithValue("@Destination", synScrData.Destination);
            command.Parameters.AddWithValue("@Notes", synScrData.Notes.Trim());
            command.Parameters.AddWithValue("@XmlItemsData", xmlItemsData);

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }

		

        public static string GetUserName(int id, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select firstname from v0user where userId = " + id;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
            return null;
        }

        public static string SaveNewScreeningData(SyntheticScreeningModel synScrData, string xmlItemsData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSaveScreening",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            synScrData.SyntheticSuspect = 0;
            command.Parameters.AddWithValue("@ScreeningID", synScrData.ScreeningID);
            command.Parameters.AddWithValue("@GSIOrder", synScrData.GSIOrder);
            command.Parameters.AddWithValue("@User", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@VendorNum", synScrData.VendorNum.Trim());
            command.Parameters.AddWithValue("@PONum", synScrData.PONum.Trim());
            command.Parameters.AddWithValue("@TotalQty", synScrData.TotalQty);
            command.Parameters.AddWithValue("@QtyPass", synScrData.QtyPass);
            command.Parameters.AddWithValue("@QtyFail", synScrData.QtyFail);
            command.Parameters.AddWithValue("@FTQuantity", synScrData.FTQuantity);
            command.Parameters.AddWithValue("@QtySynthetic", synScrData.QtySynthetic);
            command.Parameters.AddWithValue("@QtySuspect", synScrData.QtySuspect);
            //command.Parameters.AddWithValue("@SyntheticSuspect", synScrData.SyntheticSuspect);
            //command.Parameters.AddWithValue("@SKUName", synScrData.SKUName.Trim());
            command.Parameters.AddWithValue("@Test", synScrData.Test);
            command.Parameters.AddWithValue("@CertifiedBy", synScrData.ScreeningType);
            command.Parameters.AddWithValue("@Destination", synScrData.Destination);
            command.Parameters.AddWithValue("@Notes", synScrData.Notes.Trim());
            //command.Parameters.AddWithValue("@XmlItemsData", xmlItemsData);

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
        public static string DeleteScreeningData(Int64 screeningID, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spDeleteScreening",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ScreeningID", screeningID);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }

		
        public static DataSet GetSyntheticRecordByMemo(string memoNum, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select * from tblSyntheticCustomerEntry where memoNum = '" + memoNum + @"'";
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch
            {
                return null;
            }
        }
        public static DataTable GetDestinationsList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select distinct trim(destination) as destination from  tblSyntheticScreening order by trim(destination)";
            SqlDataAdapter adapter;
            DataTable dt = new DataTable();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(dt);
                conn.Close();
                return dt;
            }
            catch
            {
                return null;
            }
        }
        public static int GetScreeningByMemo(string memoNum, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            string sql = @"select * from tblSyntheticCustomerEntries where memoNum = '" + memoNum + @"'";
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(sql, conn);
                adapter.Fill(ds);
                conn.Close();
                int count = ds.Tables[0].Rows.Count;
                if (count == 0)
                    return count;
                else
                {
                    string order = ds.Tables[0].Rows[0].ItemArray[5].ToString();
                    if (order != "" && order != null)
                        return 1;
                    else
                        return 2;
                }
            }
            catch
            {
                return -1;
            }
        }

        public static DataSet GetSyntheticItemData(string gsiNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetSyntheticStatsAll1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@GSIOrder", gsiNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//GetSyntheticItemData


        /* alex
        public static bool PrintCustomerLabel(string memoNum, string poNum, string skuName, string totalQty, string bagQty, string custCode, Page p)
        {
            string sReportName = @"c:\PrintLabel\Label_Synthetic.xlsx";

            Microsoft.Office.Interop.Excel._Workbook BookTemp = Open_Excel(sReportName);
            Excel.Worksheet SheetData = (Excel.Worksheet)BookTemp.Sheets[1];
            //Excel.Worksheet SheetData1 = (Excel.Worksheet)BookTemp.Sheets[2];

            Excel.Range crCell = null;
            //string sTitleColumn = "b";
            //string sDataColumn = "c";
            //crCell = SheetData.get_Range("B2:C9", Type.Missing);
            //crCell.Cells.Value2 = "";
            //int i1 = 0;
            //int iBaseCellRow = 2;
            try
            {
                //int count = 0;//ds.Tables[0].Columns.Count;
                crCell = SheetData.get_Range("d20", Type.Missing);
                crCell.Cells.Value2 = custCode;
                crCell = SheetData.get_Range("d22", Type.Missing);
                crCell.Cells.Value2 = memoNum;
                crCell = SheetData.get_Range("d24", Type.Missing);
                crCell.Cells.Value2 = poNum;
                crCell = SheetData.get_Range("d26", Type.Missing);
                crCell.Cells.Value2 = skuName;
                crCell = SheetData.get_Range("h22", Type.Missing);
                crCell.Cells.Value2 = totalQty;
                crCell = SheetData.get_Range("h25", Type.Missing);
                crCell.Cells.Value2 = bagQty;
                crCell = SheetData.get_Range("h21", Type.Missing);
                crCell.Cells.Value2 = DateTime.Now.ToString();
                crCell = SheetData.get_Range("d16", Type.Missing);
                crCell.Cells.Value2 = @"*" + memoNum.ToUpper() + @"*";

                //string sPrinterName = @"\\GSI-NY-18\Elizabeth HP Printer";//ConfigurationManager.AppSettings["PrinterName"].ToString();
                //#if DEBUG
                string myFile = @"C:\Users\aremennik\Documents\Visual Studio 2010\Projects\NewCorpt_merge0803\" + memoNum.ToString() + ".pdf";
                //string myFile = @"Images/" + memoNum.ToString() + ".pdf";
                if (File.Exists(myFile)) File.Delete(myFile);
                //Worksheet sheet = workbook.Worksheets[1];
                //sheet.SaveToImage(@"c:\temp.jpg");
                if (File.Exists(@"c:\temp\tst.xlsx")) File.Delete(@"c:\temp\tst.xlsx");
                SheetData.SaveAs(@"c:\temp\tst.xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                BookTemp.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, myFile);
                //SheetData.PrintOut(1, 1, 1, false, sPrinterName, Type.Missing, true, Type.Missing);
                //#else
                //SheetData.PrintOut(1, 1, 1, false, sPrinterName, Type.Missing, true, Type.Missing);               
                //#endif

                BookTemp.Close(false, sReportName, null);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }//PrintCustomerLabel
        alex */

        #endregion

        #region Synthetic Screening India


        public static List<SyntheticScreeningINDModel> GetScreeningByOrderIND(Int32 orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningINDByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@GSIOrder", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningINDModel(row)).ToList();
            conn.Close();
            return result;
        }

		public static DataSet GetScreeningDetailsByOrderIND(Int32 orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningINDByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@GSIOrder", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            ;
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }

		public static List<SyntheticScreeningINDModel> GetScreeningDetailsByDestinationIND(string destination, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetScreeningINDByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Destination", destination);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningINDModel(row)).ToList();
            conn.Close();
            return result;
        }

		public static List<SyntheticScreeningINDModel> GetScreeningByCreateDateIND(DateTime dtCreateFromDate, DateTime dtCreateToDate, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetScreeningINDByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@CreateDate", dtCreateFromDate);
            command.Parameters.AddWithValue("@CreateDateTo", dtCreateToDate);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SyntheticScreeningINDModel(row)).ToList();
            conn.Close();
            return result;
        }

		public static string SaveScreeningDataIND(SyntheticScreeningINDModel synScrData, string xmlItemsData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveScreeningIND",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ScreeningID", synScrData.ScreeningID);
            command.Parameters.AddWithValue("@GSIOrder", synScrData.GSIOrder);
            command.Parameters.AddWithValue("@User", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@VendorNum", synScrData.CustomerCode.Trim());
            command.Parameters.AddWithValue("@PONum", synScrData.PONum.Trim());
            command.Parameters.AddWithValue("@TotalQty", synScrData.TotalQty);
            command.Parameters.AddWithValue("@QtyPass", synScrData.QtyPass);
            command.Parameters.AddWithValue("@QtyFail", synScrData.QtyFail);
            command.Parameters.AddWithValue("@FTQuantity", synScrData.FTQuantity);
            command.Parameters.AddWithValue("@SKUName", synScrData.SKUName.Trim());
            command.Parameters.AddWithValue("@Test", synScrData.Test);
            command.Parameters.AddWithValue("@CertifiedBy", synScrData.CertifiedBy);
            command.Parameters.AddWithValue("@Destination", synScrData.Destination);
            command.Parameters.AddWithValue("@Notes", synScrData.Notes.Trim());
            command.Parameters.AddWithValue("@XmlItemsData", xmlItemsData);

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
		
		public static string DeleteScreeningDataIND(Int64 screeningID, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spDeleteScreeningIND",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ScreeningID", screeningID);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
		
        #endregion

        #region Currency in Pricing
        public static List<CurrencyModel> GetCurrency(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCurrency",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CurrencyModel(row)).ToList();
            conn.Close();
            return result;
        }

        public static CurrencyModel GetCurrency(Int32 customerID, string customerProgramName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCurrency",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerID", customerID);
            command.Parameters.AddWithValue("@CustomerProgramName", customerProgramName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CurrencyModel(row)).ToList();
            conn.Close();
            if (result.Count != 0)
                return result[0];
            else
                return null;
        }

        public static string SaveCurrency(Int32 customerID, string customerProgramName, Int32 currency, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetCurrency",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerID", customerID);
            command.Parameters.AddWithValue("@CustomerProgramName", customerProgramName);
            command.Parameters.AddWithValue("@CurrencyID", currency);
            command.Parameters.AddWithValue("@User", p.Session["ID"].ToString());

            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
        #endregion

        #region Quality Control

        public static List<QualityControlModel> GetQualityControlByOrder(Int32 orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetQualityControlByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@OrderCode", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new QualityControlModel(row)).ToList();
            conn.Close();
            return result;
        }

        public static DataSet GetQualityControlDetailsByOrder(Int32 orderCode, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetQualityControlByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (orderCode != 0)
                command.Parameters.AddWithValue("@OrderCode", orderCode);

            LogUtils.UpdateSpLogWithParameters(command, p);
            ;
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }

        public static List<QualityControlModel> GetQualityControlByCreateDate(DateTime dtCreateDate, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetQualityControlByOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@CreateDate", dtCreateDate);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new QualityControlModel(row)).ToList();
            conn.Close();
            return result;
        }

        public static DataSet GetQualityControlByQCID(Int32 qcID, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetQualityControlByQCId",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@QCID", qcID);

            LogUtils.UpdateSpLogWithParameters(command, p);
            ;
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }
       
        public static string SaveQualityControl(QualityControlModel qcData, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSaveQualityControl",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
                       
            command.Parameters.AddWithValue("@QCID", qcData.QCID);
            command.Parameters.AddWithValue("@OrderCode", qcData.OrderCode);
            command.Parameters.AddWithValue("@CustomerID", qcData.CustomerID);
            command.Parameters.AddWithValue("@CustomerProgram", qcData.CustomerProgram);
            command.Parameters.AddWithValue("@ProgramID", qcData.ProgramID);
            command.Parameters.AddWithValue("@Quantity", qcData.Quantity);
            command.Parameters.AddWithValue("@Comments", qcData.Comments);
            command.Parameters.AddWithValue("@IsQCDone", qcData.IsQCDone);
            command.Parameters.AddWithValue("@IsDisable", qcData.IsDisable);
            command.Parameters.AddWithValue("@User", "" + p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@Office", "" + p.Session["AuthorOfficeID"].ToString());
            
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }

        public static DataSet GetPrograms(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetQualityControlProgram",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var ds = new DataSet();
            cda.Fill(ds);
            conn.Close();
            return ds;
        }

        #endregion

        #region Bulk Item Data Entry
        public static List<MeasureValueCpModel> GetMeasureValuesCpID(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemMeasuresByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.AddWithValue("@ItemTypeID", cpModel.ItemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var pvdsds = new DataSet("ItemStuff2");
            var pvda = new SqlDataAdapter(command);
            pvda.Fill(pvdsds);
            var dt = pvdsds.Tables[0].Copy();
            conn.Close();
            return (from DataRow row in dt.Rows select MeasureValueCpModel.Create(row)).ToList();
        }

        public static SingleItemModel GetItemCp(string itemNumber, Page p)
        {
            //batchNumber = GetBatchNumberBy7digit(batchNumber, p);//sasha

            //var strItemNumber = batchNumber + "00";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "spGetItemCPPictureByCode",
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@AuthorId", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);

            var strGroupCode = Utils.FillToFiveChars("" + Utils.ParseOrderCode(itemNumber));
            var strBatchCode = Utils.FillToThreeChars("" + Utils.ParseBatchCode(itemNumber), strGroupCode);
            var strItemCode = Utils.FillToTwoChars("" + Utils.ParseItemCode(itemNumber));

            command.Parameters.AddWithValue("@GroupCode", strGroupCode);
            command.Parameters.AddWithValue("@BatchCode", strBatchCode);
            command.Parameters.AddWithValue("@ItemCode", strItemCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SingleItemModel(row)).FirstOrDefault();
            conn.Close();
            return result;
        }
        #endregion

        #region Open Batch List Filter
        public static DataTable GetInvoiceStatusList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spInvoiceStatus",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            return dt;
        }

        public static List<OfficeModel> GetCountryOffices(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spCountryOffices",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var countries = (from DataRow row in ds.Tables[2].Rows select new OfficeModel(row)).ToList();
            conn.Close();
            return countries;
        }
        #endregion
    }
}