﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.Util;
using NPOI.XSSF.UserModel;
using QRCoder;

namespace Corpt.Utilities
{
    public class ExcelUtils
    {
        public static void ExportThrouPoi(DataTable dataTable, string fname, Page p)
        {
            var ds = new DataSet();
            ds.Tables.Add(dataTable.Copy());
            ExportThrouPoi(ds, fname, p);
            
        }
        public static void ExportThrouPoi(DataSet dataSet, string fname, Page p)
        {
            ExportThrouPoi(dataSet, fname, false, p);
        }
        public static string[] GetHiddenColumns()
        {
            return new[] { PageConstants.ColNameLinkOnItemView /*, DbConstants.MeasureNameInternalComment*/};
        }

        #region Rejected Stones to Excel
        public static void ExportThrouPoiStats(DataSet dataSet, string fname, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Report");
            sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers
            var headerStyle = CreateStyle(workbook, true, 0, false);
            var cellStyle = CreateStyle(workbook, false, 0, false);
            var rejCellStyle = CreateRejectStyle(workbook);
            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
            var cellStyleURL = CreateStyle(workbook, false, 0, true);

            var currRow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {

                //-- Create Columns
                var headerRow = sheet.CreateRow(currRow);
                headerRow.RowStyle = headerStyle;

                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currRow++;

                //-- Rows
                int rejCol = dataTable.Columns.Count - 1;
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currRow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (i == rejCol)
                        {
                            cell.CellStyle = rejCellStyle;
                        }
                        else
                        {
                            if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                            {
                                data = Regex.Replace(data, @"<[^>]+>", "");
                                cell.CellStyle = cellStyleYellow;
                            }
                            else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                            {
                                data = Regex.Replace(data, @"<[^>]+>", "");
                                cell.CellStyle = cellStyle;
                            }
                            else if (data.ToLower().Contains("http"))
                            {
                                cell.SetCellValue("URL Link");
                                IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                                link.Address = data;
                                cell.Hyperlink = (link);
                                cell.CellStyle = cellStyleURL;
                            }
                            else
                            {
                                cell.CellStyle = cellStyle;
                            }
                        }
                        cell.SetCellValue(data);
                    }
                    currRow++;
                }
                var rangeFilter = new CellRangeAddress(0, dataTable.Rows.Count, 0, dataTable.Columns.Count);
                IAutoFilter autoFilter = sheet.SetAutoFilter(rangeFilter);
                currRow++;
            }

            //-- Save to memory
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";

            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            p.Session["Memory"] = Memory;
            DownloadExcelFile(filename, p);//, Memory);
            Memory.Flush();

        }
        private static ICellStyle CreateRejectStyle(IWorkbook workbook)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            font.FontHeight = 10;
            font.FontName = "Calibri";

            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            style.WrapText = true;

            //-- Border
            style.BorderBottom = BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;

            style.Alignment = HorizontalAlignment.Left;
            return style;

        }

		#endregion

		#region Export to Excel
		public static void ExportThrouPoi(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers, cells and URL
            var headerStyle = CreateStyle(workbook, true, 0, false);
            var cellStyle = CreateStyle(workbook, false, 0, false);
            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
            var cellStyleURL = CreateStyle(workbook, false, 0, true);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Remove work column
                foreach (var hiddenColumn in GetHiddenColumns())
                {
                    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
                }

                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else if (data.ToLower().Contains("http"))
                        {
                            cell.SetCellValue("URL Link");
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            //string linkToCheck = data.Replace(@"https:", @"https:/");
                            //if (GetUrlLink(linkToCheck))
                            if (GetUrlLink(data))//check if picture exists
                                link.Address = data;
                            else
                            {
                                cell.CellStyle = cellStyle;
                                cell.SetCellValue("");
                                continue;
                            }
                            cell.Hyperlink = (link);
                            cell.CellStyle = cellStyleURL;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }

            //-- Save to memory
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";

            //FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
            //workbook.Write(sw);
            //sw.Close();
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            //var fileBytes = Memory.ToArray();
            //return Convert.ToBase64String(fileBytes);
            p.Session["Memory"] = Memory;
            if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

            //Memory.Flush();

        }
        public static string ExportThrouPoiNew(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);

            //-- Create Style for column headers, cells and URL
            var headerStyle = CreateStyle(workbook, true, 0, false);
            var cellStyle = CreateStyle(workbook, false, 0, false);
            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
            var cellStyleURL = CreateStyle(workbook, false, 0, true);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Remove work column
                foreach (var hiddenColumn in GetHiddenColumns())
                {
                    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
                }

                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);

                    sheet.SetColumnWidth(i, 20 * 256);
                }
                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
                    sheet.RepeatingRows = range;
                }

                currrow++;
                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {
                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else if (data.ToLower().Contains("http"))
                        {
                            cell.SetCellValue("URL Link");
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            //string linkToCheck = data.Replace(@"https:", @"https:/");
                            //if (GetUrlLink(linkToCheck))
                            if (GetUrlLink(data))//check if picture exists
                                link.Address = data;
                            else
                            {
                                cell.CellStyle = cellStyle;
                                cell.SetCellValue("");
                                continue;
                            }
                            cell.Hyperlink = (link);
                            cell.CellStyle = cellStyleURL;
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }
                    currrow++;
                }
                currrow++;
            }

            //-- Save to memory
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";

            //FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
            //workbook.Write(sw);
            //sw.Close();
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
			var fileBytes = Memory.ToArray();
			return Convert.ToBase64String(fileBytes);
			//p.Session["Memory"] = Memory;
			//if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);
   //         return null;
			//Memory.Flush();

		}
		public static string ExportWithImage(DataSet dataSet, string fname, bool skipDownload, Page p)
		{
			IWorkbook workbook = new XSSFWorkbook();
			var sheet = workbook.CreateSheet("Report");

			IDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
			//XSSFClientAnchor anchor = new XSSFClientAnchor();


			//sheet.CreateFreezePane(0, 1, 0, 1);

			//-- Create Style for column headers, cells and URL
			var headerStyle = CreateStyle(workbook, true, 0, false);
			var cellStyle = CreateStyle(workbook, false, 0, false);
			var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
			var cellStyleURL = CreateStyle(workbook, false, 0, true);
			var cellStyleImage = CreateStyle(workbook, false, 0, false);

			var currrow = 0;
			string BatchNo = "";
			foreach (DataTable dataTable in dataSet.Tables)
			{

				//-- Remove work column
				foreach (var hiddenColumn in GetHiddenColumns())
				{
					if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
				}

				//-- Create Columns
				var headerRow = sheet.CreateRow(currrow);
				headerRow.RowStyle = headerStyle;
				for (var i = 0; i < dataTable.Columns.Count; i++)
				{
					var cell = headerRow.CreateCell(i);
					cell.CellStyle = headerStyle;
					cell.SetCellValue(dataTable.Columns[i].ColumnName);

					sheet.SetColumnWidth(i, 20 * 256);

				}
				if (dataSet.Tables.Count == 1)
				{
					var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
					sheet.RepeatingRows = range;
				}

				//Add Header for Image
				int columnname = dataTable.Columns.Count;
				var cellLink = headerRow.CreateCell(columnname);
				cellLink.CellStyle = headerStyle;
				cellLink.SetCellValue("Image");
				sheet.SetColumnWidth(columnname, 20 * 256);

				currrow++;
				//-- Rows
				byte[] imageBytes = null;
				for (var j = 0; j < dataTable.Rows.Count; j++)
				{
					var row = sheet.CreateRow(currrow);
					row.RowStyle = cellStyle;
					for (var i = 0; i < dataTable.Columns.Count; i++)
					{
						var cell = row.CreateCell(i);
						var data = dataTable.Rows[j][i].ToString();
						if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyleYellow;
						}
						else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyle;
						}
						else if (data.ToLower().Contains("http"))
						{
							cell.SetCellValue("URL Link");
							IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
							//string linkToCheck = data.Replace(@"https:", @"https:/");
							//if (GetUrlLink(linkToCheck))
							if (GetUrlLink(data))//check if picture exists
								link.Address = data;
							else
							{
								cell.CellStyle = cellStyle;
								cell.SetCellValue("");
								continue;
							}
							cell.Hyperlink = (link);
							cell.CellStyle = cellStyleURL;
						}
						else if (data.Length == 11 && Regex.IsMatch(data, "^[0-9]+$") == true) //Check if Item number found 
						{

							string itemcode = dataTable.Rows[j][i].ToString();
							if (BatchNo != itemcode.Substring(0, 9))
							{
								BatchNo = itemcode.Substring(0, 9);
								imageBytes = GetImageToBytebyBatchCode(itemcode, p);
							}
							if (imageBytes != null)
							{
								var imgCell = row.CreateCell(dataTable.Columns.Count);
								int pictureIndex = workbook.AddPicture(imageBytes, PictureType.JPEG);
								XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, imgCell.ColumnIndex, imgCell.RowIndex, imgCell.ColumnIndex + 1, imgCell.RowIndex + 1);
								XSSFPicture picture = (XSSFPicture)drawing.CreatePicture(anchor, pictureIndex);
								//row.Height = 768;
								row.Height = 1400;
								double scale = 0.90;
								picture.Resize(0.30);

								// Assuming the original image is 100 pixels high and scaled to 50%
								int originalHeightPixels = 100;
								//double scale = 0.5;
								double pictureHeightPixels = originalHeightPixels * scale;

								// Convert pixel height to EMUs
								double pictureHeightEMU = pictureHeightPixels * Units.EMU_PER_PIXEL;

								// Calculate the vertical offset to center the image within the cell
								double cellHeightEMU = row.HeightInPoints * Units.EMU_PER_POINT;
								anchor.Dy1 = (int)((cellHeightEMU - pictureHeightEMU) / 2);
							}
						}
						else
						{
							cell.CellStyle = cellStyle;
						}
						cell.SetCellValue(data);
					}
					currrow++;
				}
				currrow++;
			}
			//-- Save to memory
			//var dir = GetExportDirectory(p);

			var filename = fname + ".xlsx";

			//FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
			//workbook.Write(sw);
			//sw.Close();
			var Memory = new MemoryStream();
			Memory.Position = 0;
			workbook.Write(Memory);
			var fileBytes = Memory.ToArray();
			return Convert.ToBase64String(fileBytes);
			//p.Session["Memory"] = Memory;
			//if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

			//Memory.Flush();
		}
		public static string ExportWithItemImage(DataSet dataSet, string fname, bool skipDownload, Page p)
		{
			IWorkbook workbook = new XSSFWorkbook();
			var sheet = workbook.CreateSheet("Report");

			IDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
			//XSSFClientAnchor anchor = new XSSFClientAnchor();


			//sheet.CreateFreezePane(0, 1, 0, 1);

			//-- Create Style for column headers, cells and URL
			var headerStyle = CreateStyle(workbook, true, 0, false);
			var cellStyle = CreateStyle(workbook, false, 0, false);
			var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
			var cellStyleURL = CreateStyle(workbook, false, 0, true);
			var cellStyleImage = CreateStyle(workbook, false, 0, false);

			var currrow = 0;
			string BatchNo = "";

			string imagePath = "" + HttpContext.Current.Session[SessionConstants.WebPictureShapeRoot] + "pictures/mb/";
			List<string> ReportImg = dataSet.Tables[0].Rows.Cast<DataRow>().Select(r => Convert.ToString(imagePath + r["Report #"]) + ".png").ToList();
			Dictionary<string, byte[]> dict = DownloadImagesAsyncData(ReportImg);

			foreach (DataTable dataTable in dataSet.Tables)
			{

				//-- Remove work column
				foreach (var hiddenColumn in GetHiddenColumns())
				{
					if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
				}

				//-- Create Columns
				var headerRow = sheet.CreateRow(currrow);
				headerRow.RowStyle = headerStyle;
				for (var i = 0; i < dataTable.Columns.Count; i++)
				{
					var cell = headerRow.CreateCell(i);
					cell.CellStyle = headerStyle;
					cell.SetCellValue(dataTable.Columns[i].ColumnName);

					sheet.SetColumnWidth(i, 20 * 256);

				}

				//Add Header for QR
				int columnnameQR = dataTable.Columns.Count + 1;
				var cellQR = headerRow.CreateCell(columnnameQR);
				cellQR.CellStyle = headerStyle;
				cellQR.SetCellValue("QR");
				sheet.SetColumnWidth(columnnameQR, 20 * 256);

				//Add Header for Image
				int columnname = dataTable.Columns.Count;
				var cellLink = headerRow.CreateCell(columnname);
				cellLink.CellStyle = headerStyle;
				cellLink.SetCellValue("Image");
				sheet.SetColumnWidth(columnname, 20 * 256);

				if (dataSet.Tables.Count == 1)
				{
					var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
					sheet.RepeatingRows = range;
				}

				currrow++;
				//-- Rows
				byte[] imageBytes = null;
				for (var j = 0; j < dataTable.Rows.Count; j++)
				{
					var row = sheet.CreateRow(currrow);
					row.RowStyle = cellStyle;
					for (var i = 0; i < dataTable.Columns.Count; i++)
					{
						var cell = row.CreateCell(i);
						var data = dataTable.Rows[j][i].ToString();
						if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyleYellow;
						}
						else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyle;
						}
						else if (data.ToLower().Contains("http"))
						{
							cell.SetCellValue("URL Link");
							IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
							//string linkToCheck = data.Replace(@"https:", @"https:/");
							//if (GetUrlLink(linkToCheck))
							if (GetUrlLink(data))//check if picture exists
								link.Address = data;
							else
							{
								cell.CellStyle = cellStyle;
								cell.SetCellValue("");
								continue;
							}
							cell.Hyperlink = (link);
							cell.CellStyle = cellStyleURL;
						}
						else if (data.Length == 11 && Regex.IsMatch(data, "^[0-9]+$") == true) //Check if Item number found 
						{

							string itemcode = dataTable.Rows[j][i].ToString();
							//if (BatchNo != itemcode.Substring(0, 9))
							//{
							BatchNo = itemcode.Substring(0, 9);

							string urlQR = "https://wg.gemscience.net/vr/veri.aspx?" + data;
							QRCodeGenerator qrGenerator = new QRCodeGenerator();
							QRCodeData qrCodeData = qrGenerator.CreateQrCode(urlQR, QRCodeGenerator.ECCLevel.Q);
							QRCode qrCode = new QRCode(qrCodeData);
							Bitmap qrCodeImage = qrCode.GetGraphic(20);

							byte[] imageBytesQR = BitmapToBytes(qrCodeImage);
							var imgCellQR = row.CreateCell(dataTable.Columns.Count + 1);

							int pictureIndexQR = workbook.AddPicture(imageBytesQR, PictureType.PNG);
							XSSFPicture pictureQR = (XSSFPicture)drawing.CreatePicture(new XSSFClientAnchor(0, 0, 0, 0, imgCellQR.ColumnIndex, imgCellQR.RowIndex, imgCellQR.ColumnIndex + 1, imgCellQR.RowIndex + 1), pictureIndexQR);

							//for medium QR picture
							row.Height = 1700;
							double scaleQR = 0.10;
							pictureQR.Resize(scaleQR);

							dict.TryGetValue(itemcode, out imageBytes);

							if (imageBytes != null)
							{
								var imgCell = row.CreateCell(dataTable.Columns.Count);
								int pictureIndex = workbook.AddPicture(imageBytes, PictureType.JPEG);
								XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, imgCell.ColumnIndex, imgCell.RowIndex, imgCell.ColumnIndex + 1, imgCell.RowIndex + 1);
								XSSFPicture picture = (XSSFPicture)drawing.CreatePicture(anchor, pictureIndex);
								//row.Height = 368;
								row.Height = 1400;
								double scale = 0.90;
								picture.Resize(0.30);

								// Assuming the original image is 100 pixels high and scaled to 50%
								int originalHeightPixels = 100;
								//double scale = 0.5;
								double pictureHeightPixels = originalHeightPixels * scale;

								// Convert pixel height to EMUs
								double pictureHeightEMU = pictureHeightPixels * Units.EMU_PER_PIXEL;

								// Calculate the vertical offset to center the image within the cell
								double cellHeightEMU = row.HeightInPoints * Units.EMU_PER_POINT;
								anchor.Dy1 = (int)((cellHeightEMU - pictureHeightEMU) / 2);
							}
						}
						else
						{
							cell.CellStyle = cellStyle;
						}
						cell.SetCellValue(data);
					}
					currrow++;
				}
				currrow++;
			}
			//-- Save to memory
			//var dir = GetExportDirectory(p);

			var filename = fname + ".xlsx";

			//FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
			//workbook.Write(sw);
			//sw.Close();
			var Memory = new MemoryStream();
			Memory.Position = 0;
			workbook.Write(Memory);
			var fileBytes = Memory.ToArray();
			return Convert.ToBase64String(fileBytes);
			//p.Session["Memory"] = Memory;
			//if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

			// Memory.Flush();
		}
		public static string ExportWithItemImageWithLink(DataSet dataSet, string fname, bool skipDownload, Page p)
		{
			IWorkbook workbook = new XSSFWorkbook();
			var sheet = workbook.CreateSheet("Report");

			IDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
			//XSSFClientAnchor anchor = new XSSFClientAnchor();


			//sheet.CreateFreezePane(0, 1, 0, 1);

			//-- Create Style for column headers, cells and URL
			var headerStyle = CreateStyle(workbook, true, 0, false);
			var cellStyle = CreateStyle(workbook, false, 0, false);
			var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
			var cellStyleURL = CreateStyle(workbook, false, 0, true);
			var cellStyleImage = CreateStyle(workbook, false, 0, false);

			var currrow = 0;
			string BatchNo = "";

			string imagePath = "" + HttpContext.Current.Session[SessionConstants.WebPictureShapeRoot] + "pictures/mb/";
			List<string> ReportImg = dataSet.Tables[0].Rows.Cast<DataRow>().Select(r => Convert.ToString(imagePath + r["Report #"]) + ".png").ToList();
			Dictionary<string, byte[]> dict = DownloadImagesAsyncData(ReportImg);

			foreach (DataTable dataTable in dataSet.Tables)
			{

				//-- Remove work column
				foreach (var hiddenColumn in GetHiddenColumns())
				{
					if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
				}

				//-- Create Columns
				var headerRow = sheet.CreateRow(currrow);
				headerRow.RowStyle = headerStyle;
				for (var i = 0; i < dataTable.Columns.Count; i++)
				{
					var cell = headerRow.CreateCell(i);
					cell.CellStyle = headerStyle;
					cell.SetCellValue(dataTable.Columns[i].ColumnName);

					sheet.SetColumnWidth(i, 20 * 256);

				}
				//Add Header for Image
				int columnname = dataTable.Columns.Count;
				var cellLink = headerRow.CreateCell(columnname);
				cellLink.CellStyle = headerStyle;
				cellLink.SetCellValue("Image");
				sheet.SetColumnWidth(columnname, 20 * 256);

				//Add Header for QR
				int columnnameQR = dataTable.Columns.Count + 1;
				var cellQR = headerRow.CreateCell(columnnameQR);
				cellQR.CellStyle = headerStyle;
				cellQR.SetCellValue("Report Link");
				sheet.SetColumnWidth(columnnameQR, 20 * 256);



				if (dataSet.Tables.Count == 1)
				{
					var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count);
					sheet.RepeatingRows = range;
				}

				currrow++;
				//-- Rows
				byte[] imageBytes = null;
				for (var j = 0; j < dataTable.Rows.Count; j++)
				{
					var row = sheet.CreateRow(currrow);
					row.RowStyle = cellStyle;
					for (var i = 0; i < dataTable.Columns.Count; i++)
					{
						var cell = row.CreateCell(i);
						var data = dataTable.Rows[j][i].ToString();
						if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyleYellow;
						}
						else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
						{
							data = Regex.Replace(data, @"<[^>]+>", "");
							cell.CellStyle = cellStyle;
						}
						else if (data.Length == 11 && Regex.IsMatch(data, "^[0-9]+$") == true) //Check if Item number found 
						{
							string itemcode = dataTable.Rows[j][i].ToString();

							dict.TryGetValue(itemcode, out imageBytes);
							if (imageBytes != null)
							{
								var imgCell = row.CreateCell(dataTable.Columns.Count);
								int pictureIndex = workbook.AddPicture(imageBytes, PictureType.JPEG);
								XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, imgCell.ColumnIndex, imgCell.RowIndex, imgCell.ColumnIndex + 1, imgCell.RowIndex + 1);
								XSSFPicture picture = (XSSFPicture)drawing.CreatePicture(anchor, pictureIndex);
								//row.Height = 368;
								row.Height = 1400;
								double scale = 0.90;
								picture.Resize(0.30);

								// Assuming the original image is 100 pixels high and scaled to 50%
								int originalHeightPixels = 100;
								//double scale = 0.5;
								double pictureHeightPixels = originalHeightPixels * scale;

								// Convert pixel height to EMUs
								double pictureHeightEMU = pictureHeightPixels * Units.EMU_PER_PIXEL;

								// Calculate the vertical offset to center the image within the cell
								double cellHeightEMU = row.HeightInPoints * Units.EMU_PER_POINT;
								anchor.Dy1 = (int)((cellHeightEMU - pictureHeightEMU) / 2);
							}

							var urlCell = row.CreateCell(dataTable.Columns.Count + 1);
							string urlLink = "https://wg.gemscience.net/vr/veri.aspx?" + itemcode;
							IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
							urlCell.SetCellValue(urlLink);
							link.Address = urlLink;
							urlCell.Hyperlink = (link);
							urlCell.CellStyle = cellStyleURL;
						}
						else
						{
							cell.CellStyle = cellStyle;
						}
						cell.SetCellValue(data);
					}
					currrow++;
				}
				currrow++;
			}
			//-- Save to memory
			//var dir = GetExportDirectory(p);

			var filename = fname + ".xlsx";

			//FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
			//workbook.Write(sw);
			//sw.Close();
			var Memory = new MemoryStream();
			Memory.Position = 0;
			sheet.ForceFormulaRecalculation = true;
			workbook.Write(Memory);
			var fileBytes = Memory.ToArray();
			return Convert.ToBase64String(fileBytes);
			//p.Session["Memory"] = Memory;
			//if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

			// Memory.Flush();
		}
        public static string ExportThrouPoiQR(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);
            IDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;

            //-- Create Style for column headers, cells and URL
            var headerStyle = CreateStyle(workbook, true, 0, false);
            var cellStyle = CreateStyle(workbook, false, 0, false);
            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
            var cellStyleURL = CreateStyle(workbook, false, 0, true);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Remove work column
                foreach (var hiddenColumn in GetHiddenColumns())
                {
                    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
                }

                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);
                    sheet.SetColumnWidth(i, 20 * 256);
                }

                //Add Header for Link
                int columnname = dataTable.Columns.Count;
                var cellLink = headerRow.CreateCell(columnname);
                cellLink.CellStyle = headerStyle;
                cellLink.SetCellValue("Report Link");
                sheet.SetColumnWidth(columnname, 50 * 256);

                //Add Header for QR
                int columnnameQR = dataTable.Columns.Count + 1;
                var cellQR = headerRow.CreateCell(columnnameQR);
                cellQR.CellStyle = headerStyle;
                cellQR.SetCellValue("QR");
                sheet.SetColumnWidth(columnnameQR, 20 * 256);


                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count + 1);
                    sheet.RepeatingRows = range;
                }
                currrow++;

                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {

                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {

                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else if (data.ToLower().Contains("http"))
                        {
                            cell.SetCellValue("URL Link");
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            //string linkToCheck = data.Replace(@"https:", @"https:/");
                            //if (GetUrlLink(linkToCheck))
                            if (GetUrlLink(data))//check if picture exists
                                link.Address = data;
                            else
                            {
                                cell.CellStyle = cellStyle;
                                cell.SetCellValue("");
                                continue;
                            }
                            cell.Hyperlink = (link);
                            cell.CellStyle = cellStyleURL;
                        }
                        else if (data.Length == 11 && Regex.IsMatch(data, "^[0-9]+$") == true)
                        {
                            var urlCell = row.CreateCell(dataTable.Columns.Count);
                            string urlLink = "https://wg.gemscience.net/vr/veri.aspx?" + data;
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            urlCell.SetCellValue(urlLink);
                            link.Address = urlLink;
                            urlCell.Hyperlink = (link);
                            urlCell.CellStyle = cellStyleURL;

                            string urlQR = "https://wg.gemscience.net/vr/veri.aspx?" + data;
                            QRCodeGenerator qrGenerator = new QRCodeGenerator();
                            QRCodeData qrCodeData = qrGenerator.CreateQrCode(urlQR, QRCodeGenerator.ECCLevel.Q);
                            QRCode qrCode = new QRCode(qrCodeData);
                            Bitmap qrCodeImage = qrCode.GetGraphic(20);

                            byte[] imageBytes = BitmapToBytes(qrCodeImage);
                            var imgCell = row.CreateCell(dataTable.Columns.Count + 1);

                            int pictureIndex = workbook.AddPicture(imageBytes, PictureType.PNG);
                            XSSFPicture picture = (XSSFPicture)drawing.CreatePicture(new XSSFClientAnchor(0, 0, 0, 0, imgCell.ColumnIndex, imgCell.RowIndex, imgCell.ColumnIndex + 1, imgCell.RowIndex + 1), pictureIndex);

                            //for medium QR picture
                            row.Height = 1700;
                            double scale = 0.10;

                            picture.Resize(scale);
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }

                    currrow++;
                }
                currrow++;
            }

            //-- Save to memory
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";

            //FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
            //workbook.Write(sw);
            //sw.Close();
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            var fileBytes = Memory.ToArray();
            return Convert.ToBase64String(fileBytes);
            //p.Session["Memory"] = Memory;
            //if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

            //Memory.Flush();

        }
        public static string ExportThrouPoiLink(DataSet dataSet, string fname, bool skipDownload, Page p)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet("Report");
            //sheet.CreateFreezePane(0, 1, 0, 1);
            IDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;

            //-- Create Style for column headers, cells and URL
            var headerStyle = CreateStyle(workbook, true, 0, false);
            var cellStyle = CreateStyle(workbook, false, 0, false);
            var cellStyleYellow = CreateStyle(workbook, false, IndexedColors.Yellow.Index, false);
            var cellStyleURL = CreateStyle(workbook, false, 0, true);

            var currrow = 0;
            foreach (DataTable dataTable in dataSet.Tables)
            {
                //-- Remove work column
                foreach (var hiddenColumn in GetHiddenColumns())
                {
                    if (dataTable.Columns.Contains(hiddenColumn)) dataTable.Columns.Remove(hiddenColumn);
                }

                //-- Create Columns
                var headerRow = sheet.CreateRow(currrow);
                headerRow.RowStyle = headerStyle;
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.CellStyle = headerStyle;
                    cell.SetCellValue(dataTable.Columns[i].ColumnName);
                    sheet.SetColumnWidth(i, 20 * 256);
                }

                //Add Header for Link
                int columnname = dataTable.Columns.Count;
                var cellLink = headerRow.CreateCell(columnname);
                cellLink.CellStyle = headerStyle;
                cellLink.SetCellValue("Report Link");
                sheet.SetColumnWidth(columnname, 50 * 256);

                //Add Header for QR
                //int columnnameQR = dataTable.Columns.Count + 1;
                //var cellQR = headerRow.CreateCell(columnnameQR);
                //cellQR.CellStyle = headerStyle;
                //cellQR.SetCellValue("QR");
                //sheet.SetColumnWidth(columnnameQR, 20 * 256);


                if (dataSet.Tables.Count == 1)
                {
                    var range = new CellRangeAddress(0, 0, 0, dataTable.Columns.Count + 1);
                    sheet.RepeatingRows = range;
                }
                currrow++;

                //-- Rows
                for (var j = 0; j < dataTable.Rows.Count; j++)
                {

                    var row = sheet.CreateRow(currrow);
                    row.RowStyle = cellStyle;
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {

                        var cell = row.CreateCell(i);
                        var data = dataTable.Rows[j][i].ToString();
                        if (data.IndexOf("text_highlited", StringComparison.OrdinalIgnoreCase) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyleYellow;
                        }
                        else if (data.IndexOf(" href", StringComparison.Ordinal) != -1)
                        {
                            data = Regex.Replace(data, @"<[^>]+>", "");
                            cell.CellStyle = cellStyle;
                        }
                        else if (data.ToLower().Contains("http"))
                        {
                            cell.SetCellValue("URL Link");
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            //string linkToCheck = data.Replace(@"https:", @"https:/");
                            //if (GetUrlLink(linkToCheck))
                            if (GetUrlLink(data))//check if picture exists
                                link.Address = data;
                            else
                            {
                                cell.CellStyle = cellStyle;
                                cell.SetCellValue("");
                                continue;
                            }
                            cell.Hyperlink = (link);
                            cell.CellStyle = cellStyleURL;
                        }
                        else if (data.Length == 11 && Regex.IsMatch(data, "^[0-9]+$") == true)
                        {
                            var urlCell = row.CreateCell(dataTable.Columns.Count);
                            string urlLink = "https://wg.gemscience.net/vr/veri.aspx?" + data;
                            IHyperlink link = new XSSFHyperlink(HyperlinkType.Url);
                            urlCell.SetCellValue(urlLink);
                            link.Address = urlLink;
                            urlCell.Hyperlink = (link);
                            urlCell.CellStyle = cellStyleURL;

                            /*string urlQR = "https://wg.gemscience.net/vr/veri.aspx?" + data;
                            QRCodeGenerator qrGenerator = new QRCodeGenerator();
                            QRCodeData qrCodeData = qrGenerator.CreateQrCode(urlQR, QRCodeGenerator.ECCLevel.Q);
                            QRCode qrCode = new QRCode(qrCodeData);
                            Bitmap qrCodeImage = qrCode.GetGraphic(20);

                            byte[] imageBytes = BitmapToBytes(qrCodeImage);
                            var imgCell = row.CreateCell(dataTable.Columns.Count + 1);

                            int pictureIndex = workbook.AddPicture(imageBytes, PictureType.PNG);
                            XSSFPicture picture = (XSSFPicture)drawing.CreatePicture(new XSSFClientAnchor(0, 0, 0, 0, imgCell.ColumnIndex, imgCell.RowIndex, imgCell.ColumnIndex + 1, imgCell.RowIndex + 1), pictureIndex);

                            //for medium QR picture
                            row.Height = 1700;
                            double scale = 0.10;

                            picture.Resize(scale);*/
                        }
                        else
                        {
                            cell.CellStyle = cellStyle;
                        }
                        cell.SetCellValue(data);
                    }

                    currrow++;
                }
                currrow++;
            }

            //-- Save to memory
            //var dir = GetExportDirectory(p);

            var filename = fname + ".xlsx";

            //FileStream sw = new FileStream(Path.GetTempPath() + filename, FileMode.Create, FileAccess.Write);
            //workbook.Write(sw);
            //sw.Close();
            var Memory = new MemoryStream();
            Memory.Position = 0;
            workbook.Write(Memory);
            var fileBytes = Memory.ToArray();
            return Convert.ToBase64String(fileBytes);
            //p.Session["Memory"] = Memory;
            //if (!skipDownload) DownloadExcelFile(filename, p); //, Memory);

            //Memory.Flush();

        }

        public static byte[] GetImageToBytebyBatchCode(string itemcode, Page p)
		{
			byte[] imageBytes = null;
			var itemList = QueryUtils.GetItemsCp(itemcode, p);
			var folderPath = "" + HttpContext.Current.Session[SessionConstants.WebPictureShapeRoot];
			if (itemList.Where(x => x.FullItemNumber == itemcode).Count() > 0)
			{
				string filePath = itemList.Find(x => x.FullItemNumber == itemcode).Path2Picture.ToLower();
				if (filePath != "")
				{
					if (filePath.ToCharArray()[0].ToString() == "/")
					{
						filePath = filePath.Substring(1, filePath.Length - 1);
					}
					//string imagePath = folderPath + filePath;
					string imagePath = filePath;
					string base64String = AzureStorageBlob.ShowFileFromBlob(imagePath, p);
					if (base64String.Contains(","))
						imageBytes = Convert.FromBase64String(base64String.Split(',')[1]);
					else
						imageBytes = Convert.FromBase64String(base64String);
					//WebClient webClient = new WebClient();
					try
					{
						//imageBytes = webClient.DownloadData(imagePath);
						Image img = byteArrayToImage(imageBytes);
						Image newimage = ResizeImage(img, 300);
						imageBytes = ImageToByteArray(newimage);
					}
					catch (WebException e) { }
				}
			}
			return imageBytes;
		}
		private static Dictionary<string, byte[]> DownloadImagesAsyncData(List<string> urls)
		{
			Task<Dictionary<string, byte[]>> downloadTask = DownloadImagesAsync(urls);
			downloadTask.Wait(); // Block synchronously until the task completes
			Dictionary<string, byte[]> imageDictionary = downloadTask.Result;
			return imageDictionary;
		}
		static async Task<Dictionary<string, byte[]>> DownloadImagesAsync(List<string> imageUrls)
		{
			// Create a dictionary to store the downloaded image bytes
			Dictionary<string, byte[]> imageDictionary = new Dictionary<string, byte[]>();

			// Create a list to store the download tasks
			List<Task> downloadTasks = new List<Task>();

			// Loop through the list of image URLs and start a download task for each URL
			foreach (string imageUrl in imageUrls)
			{
				downloadTasks.Add(Task.Run(async () =>
				{
					using (WebClient webClient = new WebClient())
					{
						try
						{
							// Download the image bytes asynchronously
							byte[] imageData = await webClient.DownloadDataTaskAsync(imageUrl);
							string fileName = imageUrl.Substring(imageUrl.LastIndexOf("/") + 1).Replace(".png", "");
							Image img = byteArrayToImage(imageData);
							Image newimage = ResizeImage(img, 300);
							imageData = ImageToByteArray(newimage);
							imageDictionary.Add(fileName, imageData);
							Console.WriteLine($"Downloaded image from {imageUrl}: {imageData.Length} bytes");
						}
						catch (Exception ex)
						{
							Console.WriteLine($"Error downloading image from {imageUrl}: {ex.Message}");
						}
					}
				}));
			}

			// Wait for all download tasks to complete
			await Task.WhenAll(downloadTasks);

			return imageDictionary;
		}
		public static Image byteArrayToImage(byte[] bytesArr)
		{
			using (MemoryStream memstr = new MemoryStream(bytesArr))
			{
				Image img = Image.FromStream(memstr);
				return img;
			}
		}
		public static byte[] ImageToByteArray(System.Drawing.Image images)
		{
			using (var _memorystream = new MemoryStream())
			{
				images.Save(_memorystream, ImageFormat.Png);
				return _memorystream.ToArray();
			}
		}
		private static Image ResizeImage(Image imgToResize, int newSize)
		{
			int sourceWidth = imgToResize.Width;
			int sourceHeight = imgToResize.Height;
			Color backgroundColor = Color.White;

			// Determine the ratio to scale both width and height
			float scaleFactor = (float)newSize / Math.Max(sourceWidth, sourceHeight);

			// Calculate the new dimensions
			int destWidth = (int)(sourceWidth * scaleFactor);
			int destHeight = (int)(sourceHeight * scaleFactor);

			// Resize the image
			Bitmap resizedImage = new Bitmap(destWidth, destHeight);
			using (Graphics g = Graphics.FromImage(resizedImage))
			{
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
			}

			// Create a new square canvas
			Bitmap paddedImage = new Bitmap(newSize, newSize);
			using (Graphics g = Graphics.FromImage(paddedImage))
			{
				// Fill the background with the specified color
				g.Clear(backgroundColor);

				// Calculate the position to draw the resized image so it's centered
				int offsetX = (newSize - destWidth) / 2;
				int offsetY = (newSize - destHeight) / 2;

				// Draw the resized image onto the canvas
				g.DrawImage(resizedImage, offsetX, offsetY, destWidth, destHeight);
			}

			return paddedImage;
		}
		public static byte[] GetImageToBytebyItemCode(string itemcode, Page p)
        {
            byte[] imageBytes = null;
            var itemList = QueryUtils.GetItemsCp(itemcode, p);
            var folderPath = "" + HttpContext.Current.Session[SessionConstants.WebPictureShapeRoot];
            if (itemList.Where(x => x.FullItemNumber == itemcode).Count() > 0)
            {
                string filePath = itemList.Find(x => x.FullItemNumber == itemcode).Path2Picture.ToLower();
                if (filePath != "")
                {
                    if (filePath.ToCharArray()[0].ToString() == "/")
                    {
                        filePath = filePath.Substring(1, filePath.Length - 1);
                    }
                    string imagePath = folderPath + filePath;
                    WebClient webClient = new WebClient();
                    try
                    {
                        imageBytes = webClient.DownloadData(imagePath);
                    }
                    catch (WebException e) { }
                }
            }
            return imageBytes;
        }

        public static bool GetUrlLink(string dbPicture)
        {
            var webClient = new WebClient();
            try
            {
                var stream = webClient.OpenRead(dbPicture);
            }
            catch (Exception ex)
            {
                var eMessage = ex.Message;
                return false;
            }
            return true;
        }

        public static void DownloadExcelFile(string filename, Page p)//, MemoryStream msMemory)
        {
            p.Response.Clear();
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];

            if (filename.ToLower().Contains(".xlsx"))
            {
                p.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                p.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=" + filename));
                p.Response.BinaryWrite(msMemory.ToArray());
            }
            else if (filename.ToLower().Contains(".xls"))
            {
                p.Response.ContentType = "application/vnd.ms-excel";
                p.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=" + filename));
                p.Response.BinaryWrite(msMemory.ToArray());
            }
            //p.Session["Memory"] = msMemory;
            p.Response.End();
        }

        public static string GetExportDirectory(Page p)
        {

            //var dir1 = p.Session["TempDir"] + p.Session.SessionID + @"\";
            var sTempDir = System.Environment.GetEnvironmentVariable("TEMP") + @"\" + p.Session.SessionID + @"\";


            var sShapeDir = p.Request.PhysicalApplicationPath;
            var dir = sTempDir;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }
        public static string SendMailWithAttachExcel(string toAddress, string fileName, Page p, string toNY = "")
        {
            MailMessage myEmail;
            try
            {
                myEmail = new MailMessage("program.update@gemscience.net", "alexander.remennik@gemscience.net");
                myEmail.From = new MailAddress("app@gemscience.net");
                
                //alex
                if (toNY != "")//send to print in NY
                {
                    string[] printData = toNY.Split(';');
                    myEmail.Subject = "NY Printing Order " + printData[1];
                    myEmail.Body = "Hi all, <br/>";
                    myEmail.Body += "Please note this order has been processed in India and certs are ready to be printed in NY.<br/>";// + Environment.NewLine;
                    myEmail.Body += "Customer Code: " + printData[0] + ".<br/>";// + Environment.NewLine;
                    myEmail.Body += " Sku: " + printData[2] + ".<br/>" + Environment.NewLine;
                    if (printData.Length == 4)
                        myEmail.Body += printData[3];
                }
                //alex
                else
                {
                    myEmail.Subject = "Report from GSI";
                    myEmail.Body = "See attached files.<br/><i>Gemological Science International.</i>";
                }
                myEmail.To.Add(toAddress);
                myEmail.CC.Add(@"production.team@gemscience.net");
                myEmail.IsBodyHtml = true;
                MemoryStream ms = (MemoryStream)p.Session["Memory"];
                ms = new MemoryStream(ms.ToArray());
                ms.Seek(0, SeekOrigin.Begin);
                //ms.Position = 0;
                var contentType = new ContentType();
                contentType.MediaType = MediaTypeNames.Text.Plain;
                contentType.Name = fileName + ".xlsx";
                myEmail.Attachments.Add(new Attachment(ms, contentType));
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                client.EnableSsl = true;
                client.Send(myEmail);
                ms.Flush();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        /* alex
        public static string SendMailWithAttachExcel(string toAddress, string fileName, Page p)
        {
            // var fullName = GetExportDirectory(p) + fileName + ".xlsx";
            try
            {
                var fromAddr = "alexander.remennik @gemscience.net"; // "" + p.Session[SessionConstants.GlobalEmailFromAddr];
                var toAddr = toAddress;
                //var password = "" + p.Session[SessionConstants.GlobalEmailFromPassw];

                var msg = new MailMessage
                {
                    Subject = "Report from GSI",
                    From = new MailAddress(fromAddr, null),
                    Body = "See attached files.<br/><i>Gemological Science International.</i>"
                };
                msg.To.Add(new MailAddress(toAddr));
                msg.IsBodyHtml = true;
                MemoryStream ms = (MemoryStream)p.Session["Memory"];
                ms = new MemoryStream(ms.ToArray());
                ms.Seek(0, SeekOrigin.Begin);
                //ms.Position = 0;
                var contentType = new ContentType();
                contentType.MediaType = MediaTypeNames.Text.Plain;
                contentType.Name = fileName + ".xlsx";
                msg.Attachments.Add(new Attachment(ms, contentType));
                var smtp = new SmtpClient("gemscience-net.mail.protection.outlook.com")
                {
                    //Host = "" + p.Session[SessionConstants.GlobalEmailHost],
                    Port = 25, //587, //Convert.ToInt32(p.Session[SessionConstants.GlobalEmailPort]),
                    UseDefaultCredentials = true,
                    EnableSsl = true // ("" + p.Session[SessionConstants.GlobalEmailSsl]) == "Y"
                };
                //var nc = new NetworkCredential(fromAddr, password);

                //smtp.Credentials = nc;

                //smtp.Credentials = new NetworkCredential("program.update@gemscience.net", "Pcsdoh1$"); // System.Net.CredentialCache.DefaultNetworkCredentials;
                smtp.Send(msg);
                ms.Flush();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }*/
        public static string EMailVvBody = "VV # {0}<br/>" +
            "Dear {1},<br/>" +
            "Thank you for joining VIRTUAL VAULT™, an exclusive service for GSI customers that gives your peace of mind about the security of your GSI Grading Report and your jewelry purchase.<br/>" +
            "Your one-time membership fee provides a wide variety of benefits:<br/>" +
            "<b>Free Appraisal Updates:</b> Every three years you may request an updated appraisal on your diamonds and/or jewelry that GSI has appraised.<br/>" +
            "<b>GSI Report Replacement:</b> Every six years, when you return your old GSI report to us, we will provide a fresh copy.<br/>" +
            "<ul><b>Information/report replacement on lost or stolen jewelry:</b> As a registered Virtual Vault member, in the event you lose your gemstone or your GSI grading report, you simply contact GSI with your member “password,” and we will provide a replacement report within a 24 hour business day.</ul>" +
            "<ul><b>Assistance in recovery of stolen jewelry.</b> Virtual Vault also helps facilitate the recovery of your stolen gemstone or jewelry. At your request, we will send information about your loss to all major U.S. labs and appropriate law enforcement agencies.</ul>" +
            "<ul><b>Discounts for family members:</b> Your immediate family will enjoy a discount on their future Virtual Vault memberships. Tell your family about this unique service!</ul>" +
            "<br/>" +
            "<b><u>Now you can login into your VirtualVault account with</u></b><br/>" +
            "<b>User Name:</b> {2}<br/>" +
            "<b>Password:</b> {3}<br/><br/>" +
            "Mark Gershburg CEO<br/>Gemological Science International<br/>551 5th Avenue, 6th Floor<br/>New York, NY 10176";

        public static string SendMailVirtulaVault(CustomerInfoModel customer, Page p)
        {
            //-- 0 - VV #
            //-- 1 - Customer_First_Name
            //-- 2 - CustomerEmail
            //-- 3 - CustomerPassword
            var body = string.Format(EMailVvBody, customer.VirtualVaultNumber, customer.FirstName, customer.Email, customer.Password);

            try
            {
                var fromAddr = "" + p.Session[SessionConstants.GlobalEmailFromAddr]; //p.Session[SessionConstants.GlobalVvEmailFromAddr];
                var toAddr = customer.Email;
                var password = "" + p.Session[SessionConstants.GlobalEmailFromPassw]; //p.Session[SessionConstants.GlobalVvEmailFromPassw];

                var msg = new MailMessage
                {
                    Subject = string.Format("GSI# {0}", customer.ReportNumber),
                    From = new MailAddress(fromAddr),
                    Body = body
                };
                msg.To.Add(new MailAddress(toAddr));
                msg.CC.Add(new MailAddress("virtualvault@gemscience.net"));
                msg.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = "" + p.Session[SessionConstants.GlobalEmailHost], //p.Session[SessionConstants.GlobalVvEmailHost],
                    Port = Convert.ToInt32(p.Session[SessionConstants.GlobalEmailPort]), //Convert.ToInt32(p.Session[SessionConstants.GlobalVvEmailPort]),
                    UseDefaultCredentials = false,
                    EnableSsl = ("" + p.Session[SessionConstants.GlobalEmailSsl]) == "Y" //("" + p.Session[SessionConstants.GlobalVvEmailSsl]) == "Y"
                };
                var nc = new NetworkCredential(fromAddr, password);
                smtp.Credentials = nc;
                smtp.Send(msg);
                return "OK;E-mail to customer was sent";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private static ICellStyle CreateStyle(IWorkbook workbook, bool forHeader, short fillColor, bool URL)
        {
            //-- Font
            IFont font = workbook.CreateFont();
            font.Color = IndexedColors.Black.Index;
            if (forHeader)
            {
                font.Boldweight = (short)FontBoldWeight.Bold;
            }

            font.FontHeight = 10;
            font.FontName = "Calibri";


            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            if (fillColor != 0)
            {
                style.FillForegroundColor = fillColor;//IndexedColors.Yellow.Index;
                style.FillPattern = FillPattern.Diamonds;//.SolidForeground;

            }
            if (!forHeader)
            {
                //style.WrapText = true;
            }
            if (URL)
            {
                font.Color = IndexedColors.Blue.Index;
                font.Underline = FontUnderlineType.Single;
            }
            //-- Border
            style.BorderBottom = BorderStyle.Thin;
            style.BottomBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderLeft = BorderStyle.Thin;
            style.LeftBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderRight = BorderStyle.Thin;
            style.RightBorderColor = IndexedColors.Grey50Percent.Index;
            style.BorderTop = BorderStyle.Thin;
            style.TopBorderColor = IndexedColors.Grey50Percent.Index;
            if (URL)
            {
                style.Alignment = HorizontalAlignment.Left;
            }
            else style.Alignment = HorizontalAlignment.Center;
            return style;
        }
        public static void LabelsFileDownLoad1(List<LabelModel> labels, Page p)
        {
            var dt = new DataTable("Labels");
            dt.Columns.Add("BarCode");
            foreach (var number in labels)
            {
                dt.Rows.Add(new object[] { number.BarCode });
            }
            dt.AcceptChanges();

            ExportThrouPoi(dt, "labels", p);
        }
        public static string LabelsFileDownLoad(List<LabelModel> labels, string patternLabel, Page p)
        {
            //-- Make table
            var dt = new DataTable();
            dt.Columns.Add("BarCode");
            dt.Columns.Add("CombinedNumber");
            dt.Columns.Add("Column1");
            dt.Columns.Add("Column2");
            dt.Columns.Add("Column3");
            dt.Columns.Add("Column4");
            foreach (var number in labels)
            {
                dt.Rows.Add(new object[] { number.BarCode, number.CombinedNumber, number.Column1, number.Column2, number.Column3, number.Column4 });
            }
            dt.AcceptChanges();

            //-- Create pjd file
            var ds = new DataSet("Labels");
            ds.Tables.Add(dt);
            var phisicalDoc = p.Session["TempDir"] + p.Session.SessionID + ".pjd";
            var textWriter = new XmlTextWriter(phisicalDoc, null) { Formatting = Formatting.Indented, Indentation = 6, Namespaces = false };

            textWriter.WriteStartDocument();
            textWriter.WriteStartElement("", "PrintJob", "");

            textWriter.WriteStartElement("", "Type", "");
            textWriter.WriteString(patternLabel);
            textWriter.WriteEndElement();
            // change to include schema - makes incompatible with prev. printutil version
            textWriter.WriteStartElement("", "DataSchema", "");
            textWriter.WriteRaw(ds.GetXmlSchema().Substring("<?xml version=\"1.0\" encoding=\"utf-16\"?>".Length));
            textWriter.WriteEndElement();
            // end change

            textWriter.WriteStartElement("", "Data", "");
            textWriter.WriteRaw(ds.GetXml());
            textWriter.WriteEndElement();

            //textWriter.WriteStartElement("", "Template", "");
            FileStream fs;
            try
            {
                fs = new FileStream(p.Session[patternLabel].ToString(), FileMode.Open, FileAccess.Read);
            }
            catch (Exception x)
            {
                return x.Message;
            }
            var br = new BinaryReader(fs);

            var al = new ArrayList();

            try
            {
                while (true)
                {
                    var mbw = new MyByteWrapper(br.ReadByte());
                    al.Add(mbw);
                }
            }
            catch (Exception ex)
            {//EOF
                Console.WriteLine(ex.Message);
            }

            var alb = new byte[al.Count];
            for (var i = 0; i < al.Count; i++)
            {
                if ((al[i] as MyByteWrapper) == null) continue;
                alb[i] = (al[i] as MyByteWrapper).myVal;
            }

            var enqdString = Convert.ToBase64String(alb);
            textWriter.WriteString(enqdString);
            textWriter.WriteEndDocument();
            textWriter.Flush();
            textWriter.Close();

            //-- Download
            var dir = p.Session["TempDir"].ToString();
            var fileName = p.Session.SessionID + ".pjd";
            p.Response.ContentType = "Application/pjd";
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            p.Response.TransmitFile(dir + fileName);
            p.Response.End();

            return "";
        }

		#endregion

		#region Export Labels to Excel and download (From Short Report New)
		public static string DownloadLabelOld(PrintLabelModel labelModel, Page p)
        {
            var fileName = AppUtils.MappedApplicationPath + @"/LabelTempl/account_rep_label_corpt.xls";// p.MapPath(@"~") repDir
            FileStream fileTemp;
            try
            {
                fileTemp = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            var hssfworkbook = new HSSFWorkbook(fileTemp);
            //-- Sheet 1
            var sheet1 = hssfworkbook.GetSheetAt(0);
            foreach (var valModel in labelModel.PaintValues)
            {
                sheet1.GetRow(valModel.Row).GetCell(valModel.Col).SetCellValue(valModel.CellValue);
            }
            var dir = GetExportDirectory(p);

            var filename = labelModel.DotNewItemNumber + ".xls";
            var fileOut = new FileStream(dir + filename, FileMode.Create);
            hssfworkbook.Write(fileOut);
            fileOut.Close();
            //DownloadExcelFile(filename, p);
            return "";
        }
        public static string DownloadLabelTlkw(List<PrintLabelModel> labelModels, Page p)
        {
            var fileName = AppUtils.MappedApplicationPath + @"/LabelTempl/Account_Rep_TLKW_Label_Corpt.xls";
            FileStream fileTemp;
            try
            {
                fileTemp = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            var hssfworkbook = new HSSFWorkbook(fileTemp);
            for (int i = 0; i < labelModels.Count; i++)
            {
                var sheet = hssfworkbook.GetSheetAt(i);
                var labelModel = labelModels.ToArray()[i];
                foreach (var valModel in labelModel.PaintValues)
                {
                    sheet.GetRow(valModel.Row).GetCell(valModel.Col).SetCellValue(valModel.CellValue);
                }
            }
            var dir = GetExportDirectory(p);

            var filename = labelModels.ToArray()[0].DotNewItemNumber + ".xls";
            var fileOut = new FileStream(dir + filename, FileMode.Create);
            hssfworkbook.Write(fileOut);
            fileOut.Close();
            //DownloadExcelFile(filename, p);
            return "";
        }
        private static void SetShapesValues(PrintLabelModel labelModel, ISheet sheet, int iDate, int iItemNumber, IFont fontDate, IFont fontGsi)
        {
            var controls = ((HSSFPatriarch)sheet.DrawingPatriarch).Children;

            var shapeDate = controls[iDate];
            var date = DateTime.Now.Date.ToShortDateString();
            var richTextDate = new HSSFRichTextString(date);
            richTextDate.ApplyFont(fontDate);
            ((HSSFTextbox)shapeDate).String = richTextDate;

            var shapeItemNumb = controls[iItemNumber];
            var gsi = labelModel.BarcodeNum;
            var richTextGsi = new HSSFRichTextString(gsi);
            richTextGsi.ApplyFont(fontGsi);
            ((HSSFTextbox)shapeItemNumb).String = richTextGsi;
        }
        #endregion

        #region Excel Export with QRCode
        public static string ExportThrouPoiQR(DataTable dataTable, string fname, Page p)
        {
            var ds = new DataSet();
            ds.Tables.Add(dataTable.Copy());
			//ExportThrouPoiQR(ds, fname, p);
			string strExcel = ExportThrouPoiQR(ds, fname, p);
			return strExcel;
		}
        public static string ExportThrouPoiQR(DataSet dataSet, string fname, Page p)
        {
			//ExportThrouPoiQR(dataSet, fname, false, p);
			string strExcel = ExportThrouPoiQR(dataSet, fname, false, p);
			return strExcel;
		}
        static byte[] BitmapToBytes(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        
        #endregion

    }
}