﻿using Corpt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Corpt.Utilities
{
    public class QueryDropDownBlock
    {
        public const string DropDownBlockTblName = "dbo.tblDropDownBlock";

        public static List<ListItem> GetDropDownEditList()
        {
            var result = new List<ListItem>();

            foreach (var enumItem in Enum.GetValues(typeof(EnumDropDownBlock)))
            {
                result.Add(new ListItem(((EnumDropDownBlock)enumItem).ToString(), ((int)enumItem).ToString()));
            }
            return result;
        }
        public static List<BlockModel> GetDropDownBlockList(int dropDownId, Page p)
        {
            var result = new List<BlockModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                
                var command = new SqlCommand
                {
                    CommandText = "spGetDropDownBlockList",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@DropDownId", SqlDbType.Int).Value = dropDownId;

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? new List<BlockModel>() : (from DataRow row in dt.Rows select new BlockModel(row)).ToList());
            }

            return result;
        }

        public static void DeleteDropDownBlock(List<int> ids, Page p)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spDeleteDropDownBlock",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                try
                {
                    command.Parameters.Add("@Ids", SqlDbType.VarChar).Value = string.Join(",", ids);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                }
            }
        }

        public static void AddDropDownBlock(int dropDownId, List<string> blockedNames, Page p)
        {
            DataTable dtToAdd = new DataTable();
            dtToAdd.Columns.Add(nameof(BlockModel.Id), typeof(int));
            dtToAdd.Columns.Add(nameof(BlockModel.BlockedDisplayName), typeof(string));
            dtToAdd.Columns.Add(nameof(BlockModel.DropDownId), typeof(int));
            

            foreach(var blocked in blockedNames)
            {
                dtToAdd.Rows.Add( new object[] {0, blocked, dropDownId});
            }

            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                {
                    bulkCopy.DestinationTableName = DropDownBlockTblName;
                    try
                    {
                        bulkCopy.WriteToServer(dtToAdd);
                    }
                    catch(Exception ex)
                    {
                        LogUtils.UpdateSpLogWithException(ex.Message, p);
                    }
                }
            }
        }
    }

    public enum EnumDropDownBlock
    {
        Customers = 1,
        CustomerPrograms = 2,
        Documents = 3,
        CorelFiles = 4,
        Shapes = 5,
        //IvanB 07/03
        Variety = 6,
        Species = 7,
        ColorGroup = 8,
        Karatage = 9,
        ItemName = 10,
        ColorDiamondColors = 11


        //IvanB 07/03 
    }
}