﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Customer;

namespace Corpt.Utilities
{
    public class QueryItemiznUtils
    {
        #region Order History (ReItemizn)
        public static List<CustomerHistoryModel> GetOrderHistory(string orderCode, bool withDoc, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetOrderHistory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var ds = new DataSet("OrderHistory");
            command.Parameters.AddWithValue("@GroupCode", orderCode);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                conn.Close();
                return new List<CustomerHistoryModel>();
            }
            var states = QueryCustomerUtils.GetStateTargets(p);
            //-- Root tree - Customer
            var customer = new CustomerModel(ds.Tables[0].Rows[0]);
            var items = new List<CustomerHistoryModel>
                            {
                                //-- Customer
                                new CustomerHistoryModel(customer, states),
                                //-- Order
                                new CustomerHistoryModel(ds.Tables[1].Rows[0], StateTargetModel.TrgCodeOrder, states,
                                                         customer.CustomerCode)
                            };

            //-- Batches
            items.AddRange(from DataRow row in ds.Tables[2].Rows
                           select
                               new CustomerHistoryModel(row, StateTargetModel.TrgCodeBatch, states, customer.CustomerCode));

            //-- Items
            items.AddRange(from DataRow row in ds.Tables[3].Rows
                           select
                               new CustomerHistoryModel(row, StateTargetModel.TrgCodeItem, states, customer.CustomerCode));

            //-- ItemDocs
            if (withDoc)
            {
                items.AddRange(from DataRow row in ds.Tables[4].Rows
                               select
                                   new CustomerHistoryModel(row, StateTargetModel.TrgCodeDocument, states, customer.CustomerCode));
            }
            conn.Close();
            return items;
        }
        #endregion

        #region Update Item State
		public static string UpdateItemStateBulk(List<String> data, int newState, Page p)
		{
			var msg = "";
			DataTable dataArg = UpdateItemStateBulkCreateDataTable(data, newState);
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetItemStateByCodeBulk",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar);
			command.Parameters["@rId"].Size = 150;
			command.Parameters["@rId"].Direction = ParameterDirection.Output;
			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
			command.Parameters.Add("@InputTable", SqlDbType.Structured).Value = dataArg;
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception x)
			{
				msg = x.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}

		private static DataTable UpdateItemStateBulkCreateDataTable(List<String> data, int State)
		{
			DataTable result = new DataTable();
			DataRow row;
			result.Columns.Add("StateCode", typeof(int));
			result.Columns.Add("GroupCode", typeof(int));
			result.Columns.Add("BatchCode", typeof(int));
			result.Columns.Add("ItemCode", typeof(int));
			String[] keys;
			int[] values = new int[4];
			values[0] = State;
			foreach(var item in data)
			{
				keys = item.Split('.');
				values[1] = Int32.Parse(keys[0]);
				values[2] = Int32.Parse(keys[1]);
				values[3] = Int32.Parse(keys[2]);
				row = result.NewRow();
				row[0] = values[0];
				row[1] = values[1];
				row[2] = values[2];
				row[3] = values[3];
				result.Rows.Add(row);
			}
			return result;
		}

        public static string UpdateItemState(string itemNumber, int newState, Page p)
        {
            var msg = "";
            var keys = itemNumber.Split('.');
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetItemStateByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@StateCode", newState));
            command.Parameters.Add(new SqlParameter("@GroupCode", keys[0]));
            command.Parameters.Add(new SqlParameter("@BatchCode", keys[1]));
            command.Parameters.Add(new SqlParameter("@ItemCode", keys[2]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        #endregion

        #region Order Update
        public static string UpdateOrder(UpdateOrderModel updateOrder, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spUpdateOrderEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(GetParamUpdateItems(updateOrder));
            command.Parameters.Add(GetParamBatchEvents(updateOrder));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        private static SqlParameter GetParamBatchEvents(UpdateOrderModel updateOrder)
        {
            var tbl = new DataTable("BatchEvents");
            tbl.Columns.Add("BatchId");
            tbl.Columns.Add("NumberOfItemsAffected");
            tbl.Columns.Add("NumberOfItemsInBatch");
            tbl.Columns.Add("FormCode");
            tbl.Columns.Add("EventID");
            foreach(var batch in updateOrder.Batches)
            {
                if (batch.EditItems.Count == 0) continue;
                tbl.Rows.Add(new object[]
                {
                    batch.BatchId, 
                    batch.EditItems.Count, 
                    batch.ItemsInBatch, 
                    DbConstants.ViewAccessAccountRep, 
                    DbConstants.TrkEventAddReport
                });//@FormCode=8; @EventID=10
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@BatchEvents",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;

        }
        private static SqlParameter GetParamUpdateItems(UpdateOrderModel updateOrder)
        {
            var tbl = new DataTable("UpdateItems");
            tbl.Columns.Add("BatchId");
            tbl.Columns.Add("ItemCode");
            tbl.Columns.Add("OperationTypeOfficeID");
            tbl.Columns.Add("OperationTypeID");
            foreach (var batch in updateOrder.Batches)
            {
                foreach(var item in batch.EditItems)
                {
                    tbl.Rows.Add(new object[]
                    {
                        batch.BatchId, item.ItemCode, item.DocKey.Split('_')[0], item.DocKey.Split('_')[1]
                    });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@UpdateItems",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }

        #endregion
    }
}