﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Windows.Forms.VisualStyles;
using Corpt.Models;
using Corpt.Models.Customer;

namespace Corpt.Utilities
{
    public class QueryCustomerUtils
    {

        #region Business Types
        public static List<BusinessTypeModel>  GetBusinessTypes(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetBusinessTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            
            var result = (from DataRow row in dt.Rows select new BusinessTypeModel(row)).ToList();
            result.Add(new BusinessTypeModel());
            result.Sort((m1, m2) => string.CompareOrdinal(m1.BusinessTypeName, m2.BusinessTypeName));
            conn.Close();
            return result;

        }
        #endregion

        #region Industry Memberships
        public static List<IndustryMembershipModel> GetIndustryMemberships(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetIndustryMemberships",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new IndustryMembershipModel(row)).ToList();
            conn.Close();
            return result;

        }
        #endregion

        #region Carriers
        public static List<CarriersModel>  GetCarriers(Page p)
        {
            const string spName = "spGetCarriers";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = spName,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CarriersModel(row)).ToList();
            result.Add(new CarriersModel{Id="", Name=""});
            result.Sort((m1,m2) => string.CompareOrdinal(m1.Name, m2.Name));
            conn.Close();
            return result;
        }
        #endregion

        #region USA States
        public static List<UsStateModel> GetUsStates(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            const string spName = "spGetUSStates";
            var command = new SqlCommand
            {
                CommandText = spName,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new UsStateModel(row)).ToList();
            result.Add(new UsStateModel());
            result.Sort((m1, m2) => string.CompareOrdinal(m1.State, m2.State));
            conn.Close();
            return result;
        }
        #endregion

        #region Positions
        public static List<PositionModel> GetPositions(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetPositions",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PositionModel(row)).ToList();
            result.Add(new PositionModel());
            result.Sort((m1, m2) => string.CompareOrdinal(m1.Name, m2.Name));
            conn.Close();
            return result;
            
        }
        #endregion

        #region Full Customer
        public static CustomerEditModel GetCustomer(CustomerModel customerModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.OfficeId);
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            
            var result = (dt.Rows.Count == 0 ? null : new CustomerEditModel(dt.Rows[0]));
            conn.Close();
            if (result != null)
            {
                result.Persons = GetPersonsByCustomer(customerModel, p);
            }
            return result;

        }
		#endregion

		#region Full Customer With New Carriers
		public static CustomerEditModelExt GetCustomerExt(CustomerModel customerModel, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();

			var command = new SqlCommand
			{
				CommandText = "spGetCustomer",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.OfficeId);
			command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
			var da = new SqlDataAdapter(command);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var dt = new DataTable();
			da.Fill(dt);

			var result = (dt.Rows.Count == 0 ? null : new CustomerEditModelExt(dt.Rows[0]));
			conn.Close();
			if (result != null)
			{
				result.Persons = GetPersonsByCustomer(customerModel, p);
				result.Company.Carriers = GetCarriersByCustomer(customerModel, p);
			}
			return result;
		}
        #endregion

        #region Person
        //public static List<PersonExModel> GetPersonsByCustomer(CustomerModel customerModel, Page p)
        //      {
        //          var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
        //          conn.Open();

        //          var command = new SqlCommand
        //          {
        //              CommandText = "spGetPersonsByCustomer",
        //              Connection = conn,
        //              CommandType = CommandType.StoredProcedure,
        //              CommandTimeout = p.Session.Timeout
        //          };

        //          command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
        //          command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
        //          command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
        //          command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.OfficeId);

        //          var da = new SqlDataAdapter(command);
        //          LogUtils.UpdateSpLogWithParameters(command, p);
        //          var dt = new DataTable();
        //          da.Fill(dt);
        //          var result = (from DataRow row in dt.Rows select new PersonExModel(row)).ToList();
        //          conn.Close();
        //          return result;
        //      }
        public static List<PersonExModel> GetPersonsByCustomer(CustomerModel customerModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetPersonsByCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.OfficeId);
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PersonExModel(row)).ToList();
            conn.Close();
            return result;
        }
        
        #endregion

        #region Carrier
        //For use with new multi-carrier functionality
        public static List<LimitCarrierModel> GetCarriersByCustomer(CustomerModel customerModel, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();

			var command = new SqlCommand
			{
				CommandText = "spGetCarriersByCustomer",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};

			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.OfficeId);
			var da = new SqlDataAdapter(command);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var dt = new DataTable();
			da.Fill(dt);
			var result = (from DataRow row in dt.Rows select new LimitCarrierModel(row)).ToList();
			conn.Close();
			return result;
		}
		#endregion

		#region Web Login
		public static bool IsWebLoginExists(WebLoginModel loginModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetPersonWebLogin",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@WebLogin", SqlDbType.Text).Value = loginModel.LoginName;
            command.Parameters.Add("@WebPwd", SqlDbType.Text).Value = loginModel.Password;
            command.Parameters.Add("@P", SqlDbType.Text).Value = loginModel.UniqueKey;
            var da = new SqlDataAdapter(command);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            da.Fill(dt);
            var exists = dt.Rows.Count > 0;
            conn.Close();
            return exists;
        }
        #endregion

        #region Delete Person
        public static string DeletePerson(PersonExModel personModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spExpirePerson",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@PersonCustomerOfficeID", personModel.Customer.OfficeId));
            command.Parameters.Add(new SqlParameter("@PersonCustomerID", personModel.Customer.CustomerId));
            command.Parameters.Add(new SqlParameter("@PersonID", personModel.PersonId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
		#endregion

		#region Delete Carrier from Customer
		public static string DeleteCarrierFromCustomer(CustomerEditModelExt customerModel, LimitCarrierModel carrierModel, Page p)
		{
			var msg = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spExpCustomerCarrier",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar);
			command.Parameters["@rId"].Size = 150;
			command.Parameters["@rID"].Direction = ParameterDirection.Output;
			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerOfficeId);
            command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = Int32.Parse(carrierModel.CarrierID);
			command.Parameters.Add("@Account", SqlDbType.VarChar).Value = carrierModel.Account;
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception x)
			{
				msg = x.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}
		#endregion

		#region Update Company
		public static string UpdateCompany(CustomerEditModel updateModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@CustomerName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@CreateDate", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@LastModifiedDate", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeClass", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateTargetCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@IconIndex", DBNull.Value));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerHistoryId) ?
                new SqlParameter("@CustomerHistoryID", DBNull.Value) :
                new SqlParameter("@CustomerHistoryID", updateModel.CustomerHistoryId));
            command.Parameters.Add(new SqlParameter("@CustomerCode", DBNull.Value));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerOfficeId) ?
                new SqlParameter("@CustomerOfficeID", DBNull.Value) :
                new SqlParameter("@CustomerOfficeID", updateModel.CustomerOfficeId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerId) ?
                new SqlParameter("@CustomerID", DBNull.Value) :
                new SqlParameter("@CustomerID", updateModel.CustomerId));
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add(new SqlParameter("@CompanyName", updateModel.Company.CompanyName));
            command.Parameters.Add(new SqlParameter("@ShortName", updateModel.Company.ShortName));
            command.Parameters.Add(new SqlParameter("@WeCarry", updateModel.Company.GoodsMovement.WeCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@TheyCarry", updateModel.Company.GoodsMovement.TheyCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@WeShipCarry", updateModel.Company.GoodsMovement.WeShipCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@UseTheirAccount", updateModel.Company.UseTheirAccount ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@Account", updateModel.Company.Account));
            command.Parameters.Add(new SqlParameter("@BusinessTypeID", updateModel.Company.BusinessTypeId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.CarrierId) ?
                new SqlParameter("@CarrierID", DBNull.Value) :
                new SqlParameter("@CarrierID", updateModel.Company.CarrierId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.AddressId) ?
                new SqlParameter("@AddressID", DBNull.Value) :
                new SqlParameter("@AddressID", updateModel.Company.Address.AddressId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.StateId) ?
                new SqlParameter("@StateID", DBNull.Value) :
                new SqlParameter("@StateID", updateModel.StateId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.StateTargetId) ?
                new SqlParameter("@StateTargetID", DBNull.Value) :
                new SqlParameter("@StateTargetID", updateModel.StateTargetId));

            command.Parameters.Add(new SqlParameter("@CountryPhoneCode", updateModel.Company.Address.CountryPhoneCode));
            command.Parameters.Add(new SqlParameter("@Phone", updateModel.Company.Address.Phone));
            command.Parameters.Add(new SqlParameter("@ExtPhone", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@CountryFaxCode", updateModel.Company.Address.CountryFaxCode));
            command.Parameters.Add(new SqlParameter("@Fax", updateModel.Company.Address.Fax));
            command.Parameters.Add(new SqlParameter("@CountryCellCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Cell", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Email", updateModel.Company.Address.Email));
            command.Parameters.Add(new SqlParameter("@Country", updateModel.Company.Address.Country));
            command.Parameters.Add(new SqlParameter("@City", updateModel.Company.Address.City));
            command.Parameters.Add(new SqlParameter("@Address1", updateModel.Company.Address.Address1));
            command.Parameters.Add(new SqlParameter("@Address2", updateModel.Company.Address.Address2));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.Zip1) ?
                new SqlParameter("@Zip1", DBNull.Value) :
                new SqlParameter("@Zip1", updateModel.Company.Address.Zip1));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.Zip2) ?
                new SqlParameter("@Zip2", DBNull.Value) :
                new SqlParameter("@Zip2", updateModel.Company.Address.Zip2));
            command.Parameters.Add(!string.IsNullOrEmpty(updateModel.Company.Address.UsStateId) ?
                new SqlParameter("@USStateID", updateModel.Company.Address.UsStateId) :
                new SqlParameter("@USStateID", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Communication", updateModel.Company.Communication));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@IndustryMembership", updateModel.Company.IndustryMembership));
            command.Parameters.Add(new SqlParameter("@Permission", updateModel.Company.Permissions));
            command.Parameters.Add(new SqlParameter("@QuickBookEditSequence", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@QuickBookListID", DBNull.Value));
            LogUtils.UpdateSpLogWithParameters(command, p);
            
            try
            {
                command.ExecuteNonQuery();
                var res = command.Parameters["@rId"].Value.ToString();
                if (res.Split('_').Length > 1 && res.Split('_')[1].Length > 0)
                {
                    updateModel.CustomerId = res.Split('_')[1];
                }
            }
            catch (Exception x)
            {
                msg = x.Message;LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }

        public static string UpdateCompanyExt(CustomerEditModelExt updateModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                //CommandText = "spSetCustomer",
                CommandText = "spSetCustomer",                
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@CustomerName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@CreateDate", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@LastModifiedDate", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@BusinessTypeClass", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateTargetCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@StateName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@IconIndex", DBNull.Value));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerHistoryId) ?
                new SqlParameter("@CustomerHistoryID", DBNull.Value) :
                new SqlParameter("@CustomerHistoryID", updateModel.CustomerHistoryId));
            command.Parameters.Add(new SqlParameter("@CustomerCode", DBNull.Value));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerOfficeId) ?
                new SqlParameter("@CustomerOfficeID", DBNull.Value) :
                new SqlParameter("@CustomerOfficeID", updateModel.CustomerOfficeId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.CustomerId) ?
                new SqlParameter("@CustomerID", DBNull.Value) :
                new SqlParameter("@CustomerID", updateModel.CustomerId));
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add(new SqlParameter("@CompanyName", updateModel.Company.CompanyName));
            command.Parameters.Add(new SqlParameter("@ShortName", updateModel.Company.ShortName));
            command.Parameters.Add(new SqlParameter("@WeCarry", updateModel.Company.GoodsMovement.WeCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@TheyCarry", updateModel.Company.GoodsMovement.TheyCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@WeShipCarry", updateModel.Company.GoodsMovement.WeShipCarry ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@UseTheirAccount", updateModel.Company.UseTheirAccount ? 1 : 0));
            command.Parameters.Add(new SqlParameter("@Account", updateModel.Company.Account));
            command.Parameters.Add(new SqlParameter("@BusinessTypeID", updateModel.Company.BusinessTypeId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.CarrierId) ?
                new SqlParameter("@CarrierID", DBNull.Value) :
                new SqlParameter("@CarrierID", updateModel.Company.CarrierId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.AddressId) ?
                new SqlParameter("@AddressID", DBNull.Value) :
                new SqlParameter("@AddressID", updateModel.Company.Address.AddressId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.StateId) ?
                new SqlParameter("@StateID", DBNull.Value) :
                new SqlParameter("@StateID", updateModel.StateId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.StateTargetId) ?
                new SqlParameter("@StateTargetID", DBNull.Value) :
                new SqlParameter("@StateTargetID", updateModel.StateTargetId));

            command.Parameters.Add(new SqlParameter("@CountryPhoneCode", updateModel.Company.Address.CountryPhoneCode));
            command.Parameters.Add(new SqlParameter("@Phone", updateModel.Company.Address.Phone));
            command.Parameters.Add(new SqlParameter("@ExtPhone", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@CountryFaxCode", updateModel.Company.Address.CountryFaxCode));
            command.Parameters.Add(new SqlParameter("@Fax", updateModel.Company.Address.Fax));
            command.Parameters.Add(new SqlParameter("@CountryCellCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Cell", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Email", updateModel.Company.Address.Email));
            command.Parameters.Add(new SqlParameter("@Country", updateModel.Company.Address.Country));
            command.Parameters.Add(new SqlParameter("@City", updateModel.Company.Address.City));
            command.Parameters.Add(new SqlParameter("@Address1", updateModel.Company.Address.Address1));
            command.Parameters.Add(new SqlParameter("@Address2", updateModel.Company.Address.Address2));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.Zip1) ?
                new SqlParameter("@Zip1", DBNull.Value) :
                new SqlParameter("@Zip1", updateModel.Company.Address.Zip1));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Company.Address.Zip2) ?
                new SqlParameter("@Zip2", DBNull.Value) :
                new SqlParameter("@Zip2", updateModel.Company.Address.Zip2));
            command.Parameters.Add(!string.IsNullOrEmpty(updateModel.Company.Address.UsStateId) ?
                new SqlParameter("@USStateID", updateModel.Company.Address.UsStateId) :
                new SqlParameter("@USStateID", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Communication", updateModel.Company.Communication));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@IndustryMembership", updateModel.Company.IndustryMembership));
            command.Parameters.Add(new SqlParameter("@Permission", updateModel.Company.Permissions));
            command.Parameters.Add(new SqlParameter("@QuickBookEditSequence", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@QuickBookListID", DBNull.Value));
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
                var res = command.Parameters["@rId"].Value.ToString();
                if (res.Split('_').Length > 1 && res.Split('_')[1].Length > 0)
                {
                    updateModel.CustomerId = res.Split('_')[1];
                }
            }
            catch (Exception x)
            {
                msg = x.Message; LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        #endregion

        #region Update Person
        public static string UpdatePerson(PersonExModel updateModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetPerson",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;
            command.Parameters.Add(new SqlParameter("@CreateDate",  updateModel.CreateDate));
            command.Parameters.Add(new SqlParameter("@LastModifiedDate", DBNull.Value));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.PersonHistoryId) ?
                new SqlParameter("@PersonHistoryID", DBNull.Value) :
                new SqlParameter("@PersonHistoryID", updateModel.PersonHistoryId));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.PersonCode) ? 
                new SqlParameter("@PersonCode", DBNull.Value) : 
                new SqlParameter("@PersonCode", updateModel.PersonCode));
            command.Parameters.Add(new SqlParameter("@CustomerOfficeID", updateModel.Customer.OfficeId));
            command.Parameters.Add(new SqlParameter("@CustomerID", updateModel.Customer.CustomerId));
            command.Parameters.Add(new SqlParameter("@PersonCustomerOfficeID", updateModel.Customer.OfficeId));
            command.Parameters.Add(new SqlParameter("@PersonCustomerID", updateModel.Customer.CustomerId));
            command.Parameters.Add( updateModel.PersonId == 0 ?
                new SqlParameter("@PersonID", DBNull.Value) : 
                new SqlParameter("@PersonID", updateModel.PersonId));
            command.Parameters.Add(new SqlParameter("@PositionID", updateModel.Position.Id));

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(new SqlParameter("@FirstName", updateModel.FirstName));
            command.Parameters.Add(new SqlParameter("@LastName", updateModel.LastName));
            command.Parameters.Add(updateModel.BirthDate != null ? 
                new SqlParameter("@BirthDate", updateModel.BirthDate) : 
                new SqlParameter("@BirthDate", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@WebLogin", updateModel.WebLogin.LoginName));
            command.Parameters.Add(new SqlParameter("@WebPwd", updateModel.WebLogin.Password));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Address.AddressId) ?
                new SqlParameter("@AddressID", DBNull.Value) :
                new SqlParameter("@AddressID", updateModel.Address.AddressId));
            command.Parameters.Add(new SqlParameter("@CountryPhoneCode", updateModel.Address.CountryPhoneCode));
            command.Parameters.Add(new SqlParameter("@Phone", updateModel.Address.Phone));
            command.Parameters.Add(new SqlParameter("@ExtPhone", updateModel.Address.ExtPhone));
            command.Parameters.Add(new SqlParameter("@CountryFaxCode", updateModel.Address.CountryFaxCode));
            command.Parameters.Add(new SqlParameter("@Fax", updateModel.Address.Fax));
            command.Parameters.Add(new SqlParameter("@CountryCellCode", updateModel.Address.CountryCellCode));
            command.Parameters.Add(new SqlParameter("@Cell", updateModel.Address.Cell));
            command.Parameters.Add(new SqlParameter("@Email", updateModel.Address.Email));
            command.Parameters.Add(new SqlParameter("@Country", updateModel.Address.Country));
            command.Parameters.Add(new SqlParameter("@City", updateModel.Address.City));
            command.Parameters.Add(new SqlParameter("@Address1", updateModel.Address.Address1));
            command.Parameters.Add(new SqlParameter("@Address2", updateModel.Address.Address2));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Address.Zip1) ?
                new SqlParameter("@Zip1", DBNull.Value) : 
                new SqlParameter("@Zip1", updateModel.Address.Zip1));
            command.Parameters.Add(string.IsNullOrEmpty(updateModel.Address.Zip2) ?
                new SqlParameter("@Zip2", DBNull.Value) :
                new SqlParameter("@Zip2", updateModel.Address.Zip2));
            command.Parameters.Add(!string.IsNullOrEmpty(updateModel.Address.UsStateId) ? 
                new SqlParameter("@USStateID", updateModel.Address.UsStateId) : 
                new SqlParameter("@USStateID", DBNull.Value));

            command.Parameters.Add(new SqlParameter("@Communication", updateModel.Communication));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@Permission", updateModel.Permissions));

			command.Parameters.Add(new SqlParameter("@PositionName", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@PositionCode", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@Path2PhotoFile", updateModel.Path2Photo));
            command.Parameters.Add(new SqlParameter("@Path2StoredSignature", updateModel.Path2Signature));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
                var res = command.Parameters["@rId"].Value.ToString();
                if (res.Split('_')[2].Length > 0)
                {
                    updateModel.PersonId = Convert.ToInt32(res.Split('_')[2]);
                }
            }
            catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
		#endregion

		#region Update Carrier
		public static string UpdateCarrier(CustomerEditModel customerModel, LimitCarrierModel carrierModel, Page p)
		{
			var msg = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCustomerCarrier",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.Add("@rId", SqlDbType.VarChar);
			command.Parameters["@rId"].Size = 150;
			command.Parameters["@rId"].Direction = ParameterDirection.Output;
			command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
			command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerId);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(customerModel.CustomerOfficeId);
            command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = Int32.Parse(carrierModel.CarrierID);
            //command.Parameters.Add("@Account", SqlDbType.Int).Value = Int32.Parse(carrierModel.Account);
            command.Parameters.Add("@Account", SqlDbType.VarChar).Value = carrierModel.Account;
            command.Parameters.Add("@UpperLimit", SqlDbType.Decimal).Value = Decimal.Parse(carrierModel.UpperLimit);
			command.Parameters.Add("@LowerLimit", SqlDbType.Decimal).Value = Decimal.Parse(carrierModel.LowerLimit);
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception x)
			{
				msg = x.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}
		#endregion

		#region State Targets
		public static List<StateTargetModel> GetStateTargets(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetStates",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new StateTargetModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Customer History
        public static List<CustomerItemModel> GetCustomerItemsHistory(CustomerModel customer, DateTime? dateFrom, DateTime? dateTo, string orderState, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spCustomerItemsHistory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
            if (dateFrom != null)
            {
                command.Parameters.AddWithValue("@DateFrom", dateFrom);
            }
            if (dateTo != null)
            {
                command.Parameters.AddWithValue("@DateTo", dateTo);
            }
            if (!string.IsNullOrEmpty(orderState))
            {
                command.Parameters.AddWithValue("@OrderState", orderState);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerItemModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<CustomerHistoryModel> GetCustomerHistory(CustomerModel customer, Page p, DateTime? dateFrom, DateTime? dateTo, out List<CustomerItemModel> customerItems)
        {
            var states = GetStateTargets(p);

            //-- Root tree - Customer
            var items = new List<CustomerHistoryModel> { new CustomerHistoryModel(customer, states) };
            List<CustomerItemModel> itemModels = new List<CustomerItemModel>();
            //-- Orders
            items.AddRange(GetHistoryTarget(customer.CustomerCode, StateTargetModel.TrgCodeOrder, states, p, dateFrom, dateTo, out itemModels));

            //-- Batches
            items.AddRange(GetHistoryTarget(customer.CustomerCode, StateTargetModel.TrgCodeBatch, states, p, dateFrom, dateTo, out itemModels));

            //-- Items
            items.AddRange(GetHistoryTarget(customer.CustomerCode, StateTargetModel.TrgCodeItem, states, p, dateFrom, dateTo, out customerItems));

            //-- Documents
            items.AddRange(GetHistoryTarget(customer.CustomerCode, StateTargetModel.TrgCodeDocument, states, p, dateFrom, dateTo, out itemModels));

            return items;
        }
        private static IEnumerable<CustomerHistoryModel> GetHistoryTarget(string customerCode, int targetCode, List<StateTargetModel> states, Page p, DateTime? dateFrom, DateTime? dateTo, out List<CustomerItemModel> customerItems)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var sp = "";
            if (targetCode == StateTargetModel.TrgCodeOrder)
            {
                sp = "spGetGroupByCode";
            }
            if (targetCode == StateTargetModel.TrgCodeBatch)
            {
                sp = "spGetBatchByCode";
            }
            if (targetCode == StateTargetModel.TrgCodeItem)
            {
                sp = "spGetItemByCode";
            }
            if (targetCode == StateTargetModel.TrgCodeDocument)
            {
                sp = "spGetItemDocByCode";
            }
            var command = new SqlCommand
            {
                CommandText = sp,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerCode", customerCode);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            if (dateFrom == null)
            {
                command.Parameters.AddWithValue("@BDate", DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue("@BDate", dateFrom);
            }
            if (dateTo == null)
            {
                command.Parameters.AddWithValue("@EDate", DBNull.Value);
            }
            else 
            {
                command.Parameters.AddWithValue("@EDate", dateTo);
            }
            
            command.Parameters.AddWithValue("@GroupCode", DBNull.Value);
            if (targetCode == StateTargetModel.TrgCodeBatch)
            {
                command.Parameters.AddWithValue("@BatchCode", DBNull.Value); 
            }
            if (targetCode == StateTargetModel.TrgCodeItem)
            {
                command.Parameters.AddWithValue("@BatchCode", DBNull.Value);
                command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
                command.Parameters.AddWithValue("@IsNew", DBNull.Value); 
            }
            if (targetCode == StateTargetModel.TrgCodeDocument)
            {
                command.Parameters.AddWithValue("@BatchCode", DBNull.Value);
                command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
                command.Parameters.AddWithValue("@OperationChar", DBNull.Value);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerHistoryModel(row, targetCode, states, customerCode)).ToList();
            if (targetCode == StateTargetModel.TrgCodeItem)
            {
                customerItems = (from DataRow row in dt.Rows select new CustomerItemModel(row)).ToList();
            }
            else
            {
                customerItems = new List<CustomerItemModel>();
            }
            conn.Close();
            return result;
        }
        #endregion

        #region Authors
        public static List<AuthorModel> GetAuthors(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetAuthors",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@Login", DBNull.Value);
            command.Parameters.AddWithValue("@DepartmentID", DBNull.Value);
            command.Parameters.AddWithValue("@DepartmentOfficeID", DBNull.Value);
            command.Parameters.AddWithValue("@Password", DBNull.Value);
            command.Parameters.AddWithValue("@UserID", DBNull.Value);
            command.Parameters.AddWithValue("@UserOfficeID", DBNull.Value);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new AuthorModel(row)).ToList();
            result.Add(new AuthorModel());
            result.Sort((m1,m2) => string.CompareOrdinal(m1.Name, m2.Name));
            conn.Close();
            return result;
        }
        #endregion

        #region CountryState

        public static DataSet GetCountry(Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCountry",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }
        public static DataSet GetStateByCountry(string CountryID, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetCountryState",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CountryID", CountryID.ToString());
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }
		#endregion

		#region Person Services
		public static List<PersonServiceModel> GetPersonsServicesList(Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			DataTable dt = new DataTable();
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetService",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			da.Fill(dt);
			conn.Close();
			var result = (from DataRow row in dt.Rows select new PersonServiceModel(row)).ToList();
			return result;
		}

		public static DataTable GetPersonServices(int PersonID, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			DataTable dt = new DataTable();
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetPersonServices",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@PersonID", PersonID);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			da.Fill(dt);
			conn.Close();
			return dt;
		}

		public static string SetPersonsServices(PersonServiceModel objPersonServiceModel, Page p)
		{
			var msg = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetPersonServices",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@PersonID", objPersonServiceModel.PersonID);
			command.Parameters.AddWithValue("@ServiceID", objPersonServiceModel.ServiceID);
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception x)
			{
				msg = x.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}

		public static string DeletePersonServices(PersonServiceModel objPersonServiceModel, Page p)
		{
			var msg = "";
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spDeletePersonServices",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@PersonID", objPersonServiceModel.PersonID);
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception x)
			{
				msg = x.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}
        #endregion

        #region Submission and Front
        public static List<PersonServiceModel> GetFrontServiceList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            DataTable dt = new DataTable();
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetFrontService",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            var result = (from DataRow row in dt.Rows select new PersonServiceModel(row)).ToList();
            return result;
        }
        #endregion
    }
}