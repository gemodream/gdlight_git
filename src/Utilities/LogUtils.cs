﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.SessionState;
using System.Web.UI;
using Corpt.Constants;

namespace Corpt.Utilities
{
    public static class LogUtils
    {
        public static string GetLogPath(Page p)
        {
            return GetLogPath(p.Session);
        }

        public static string GetLogPath(HttpSessionState session)
        {
            var param = "" + session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0") return "";
            //var dir = session[SessionConstants.GlobalTempDir] +session.SessionID + @"\";
            var dir = @"c:\temp\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir + "corptSp.log";
        }

        public static void UpdateSpLogWithInParameters(DataTable dt, Page p)
        {
            var param = "" + p.Session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;

            var fileName = GetFileName(p);
            string url = p.Session["WebPictureShapeRoot"] + @"gdlight_logs/" + fileName;
            string mLog = QueryUtils.GetLogFromAzure(url);

            
            try
            {
                if (dt.Rows.Count <= 0) return;
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        mLog += dc.ColumnName + "=" + dt.Rows[i][dc] + ";"; 
                    }
                }
                mLog += Environment.NewLine;
                QueryUtils.AddMessageToLog(mLog, fileName, p);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }

        private static string GetFileName(Page p)
        {
            return GetFileName(p.Session);
        }

        private static string GetFileName(HttpSessionState session)
        {
            string machineName = null, fileName;
            if (session["MachineName"] != null)
                machineName = session["MachineName"].ToString();
            if (machineName != null)
                fileName = "sp_" + machineName + "_" + DateTime.Now.DayOfYear.ToString() + @".txt";
            else
            {
                string authorId = "1";
                if (session["ID"] != null)
                    authorId = session["ID"].ToString();
                fileName = "sp_" + authorId + "_" + DateTime.Now.DayOfYear.ToString() + @".txt";
            }

            return fileName;
        }

        //public static void UpdateSpLogWithInParameters(DataTable dt, HttpSessionState session)
        //{
        //    var sLogPath = GetLogPath(session);
        //    if (string.IsNullOrEmpty(sLogPath)) return;

        //    if (!File.Exists(sLogPath))
        //        using (var fsCreate = new FileStream(sLogPath, FileMode.Create))
        //        {
        //            try
        //            {
        //                fsCreate.Position = 0;
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.Out.WriteLine(ex.Message);
        //            }
        //            finally
        //            {
        //                fsCreate.Close();
        //            }
        //        }

        //    try
        //    {
        //        using (var fsWrite = new FileStream(sLogPath, FileMode.Open, FileAccess.Write))
        //        {
        //            try
        //            {
        //                fsWrite.Position = fsWrite.Length;
        //                var bInfo = new UTF8Encoding(true).GetBytes("\n[" + DateTime.Now + "]" + dt.TableName + ": ");
        //                fsWrite.Write(bInfo, 0, bInfo.Length);

        //                if (dt.Rows.Count <= 0) return;
        //                for (var i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    foreach (DataColumn dc in dt.Columns)
        //                    {
        //                        bInfo = new UTF8Encoding(true).GetBytes(dc.ColumnName + "=" + dt.Rows[i][dc] + "; ");
        //                        fsWrite.Write(bInfo, 0, bInfo.Length);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.Out.WriteLine(ex.Message);
        //            }
        //            finally
        //            {
        //                fsWrite.Close();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Out.WriteLine(ex.Message);
        //    }
        //}

        //public static void UpdateSpLogWithParameters(SqlCommand sqlCommand, Page p)
        //{
        //    UpdateSpLogWithParameters(sqlCommand, p);
        //}

        public static void UpdateSpLogWithException(string sMsg, Page p)
        {
            var param = "" + p.Session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;
            var fileName = GetFileName(p);
            string url = p.Session["WebPictureShapeRoot"] + @"gdlight_logs/" + fileName;
            string mLog = QueryUtils.GetLogFromAzure(url);

            try
            {
                mLog += Environment.NewLine;
                mLog += "EXCEPTION: " + sMsg;
                QueryUtils.AddMessageToLog(mLog, fileName, p);
            }
            catch (Exception x)
            {
                Console.Out.WriteLine(x.Message);
            }
        }
        public static void UpdateSpLogWithException(string sMsg, HttpSessionState session)
        {
            var param = "" + session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;

            var fileName = GetFileName(session);
            string url = session["WebPictureShapeRoot"] + @"gdlight_logs/" + fileName;
            //string mLog = QueryUtils.GetLogFromAzure(url);
            
            try
            {
                string mLog = "";
                mLog += Environment.NewLine;
                mLog += "EXCEPTION: " + sMsg;
                QueryUtils.AddLogToDB(mLog, session);
                //QueryUtils.AddMessageToLog(mLog, fileName, session);
            }
            catch (Exception x)
            {
                Console.Out.WriteLine(x.Message);
            }
        }

        public static void UpdateSpLogWithExceptionLocal(string sMsg, HttpSessionState session)
        {
            string sLogPath = GetLogPath(session);
            if (string.IsNullOrEmpty(sLogPath)) return;
            if (sLogPath == "") return;

            try
            {
                using (var fsWrite = File.OpenWrite(sLogPath))
                {
                    try
                    {
                        fsWrite.Position = fsWrite.Length;
                        var bInfo = new UTF8Encoding(true).GetBytes("EXCEPTION: " + sMsg + Environment.NewLine);
                        fsWrite.Write(bInfo, 0, bInfo.Length);
                        fsWrite.Flush();
                    }
                    catch (Exception x)
                    {
                        Console.Out.WriteLine(x.Message);
                    }
                    finally
                    {
                        fsWrite.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Console.Out.WriteLine(x.Message);
            }
        }

        public static void UpdateSpLogWithException(Exception e, Page p)
        {
            UpdateSpLogWithException(e, p.Session);
        }

        public static void UpdateSpLog(string s, HttpSessionState session)
        {
            var param = "" + session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;

            var fileName = GetFileName(session);
            //QueryUtils.AddMessageToLog("[" + GetFormatted(DateTime.Now) + "] " + s, fileName, session);
            QueryUtils.AddLogToDB(s, session);
        }

        public static void UpdateSpLogWithException(Exception e, HttpSessionState session)
        {
            var param = "" + session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;

            var fileName = GetFileName(session);
            QueryUtils.AddLogToDB(e.Message, session);
            //QueryUtils.AddMessageToLog(e.Message, fileName, session);
            //QueryUtils.AddMessageToLog(e.StackTrace, fileName, session);
        }

        public static void UpdateSpLogWithParameters(SqlCommand sqlCommand, Page p)
        {
            var url = p.AppRelativeVirtualPath.Replace(@"/", "").Replace(@"~", "");
            UpdateSpLogWithParameters(sqlCommand, p.Session, url);
        }

        public static void UpdateSpLogWithParameters(SqlCommand sqlCommand, HttpSessionState session, string url = "AccountReps")
        {
            var param = "" + session[SessionConstants.GlobalSpLog];
            if (string.IsNullOrEmpty(param) || param == "0")
                return;

            var fileName = GetFileName(session);
            var command = sqlCommand.CommandText;
            try
            {
                var stringBuilder = new StringBuilder();
                try
                {
                    stringBuilder.Append("[" + GetFormatted(DateTime.Now) + "]" + command + " ");
                    foreach (SqlParameter sPrm in sqlCommand.Parameters)
                    {
                        var isParameterLast =
                            sPrm.Equals(sqlCommand.Parameters[sqlCommand.Parameters.Count - 1]);

                        if (sPrm.SqlDbType == SqlDbType.Structured)
                        {
                            var outString = sPrm.ParameterName + "={";
                            var isEachRowOnNewLine = false;

                            if (!(sPrm.Value is DataTable paramTable))
                            {
                                if (sPrm.Value == null)
                                    outString += "NULL}";
                                else
                                    outString += $"<{sPrm.Value.GetType().Name}?>" + "}";
                            }
                            else
                            {
                                isEachRowOnNewLine =
                                    (paramTable.Columns.Count > 1) || (paramTable.Rows.Count > 5);

                                for (var i = 0; i < paramTable.Rows.Cast<DataRow>().ToList().Count; i++)
                                {
                                    var row = paramTable.Rows.Cast<DataRow>().ToList()[i];
                                    var rowString = (isEachRowOnNewLine ? "(" : "") +
                                                    string.Join(", ", paramTable.Columns.Cast<DataColumn>()
                                                        .Select((column, j) =>
                                                            (isEachRowOnNewLine ? column.ColumnName + "= " : "") +
                                                            GetSqlValue(row.ItemArray[j]))) +
                                                    (isEachRowOnNewLine ? ")" : "");
                                    var isRowLast = (i == (paramTable.Rows.Count - 1));
                                    rowString = (isEachRowOnNewLine ? Environment.NewLine + "\t\t" : "") + rowString +
                                                (isRowLast ? "" : ",");
                                    outString += rowString;
                                }
                                outString += "}";
                            }

                            outString += (isParameterLast ? ";" : ",") + (isEachRowOnNewLine ? Environment.NewLine : "");
                            stringBuilder.Append(outString);
                        }
                        else
                        {
                            object sPrmValue = GetSqlValue(sPrm.Value);
                            stringBuilder.Append(sPrm.ParameterName + "=" + sPrmValue +
                                           (sPrm.Equals(sqlCommand.Parameters[sqlCommand.Parameters.Count - 1])
                                               ? ";"
                                               : ", "));
                        }
                    }
                    QueryUtils.AddLogToDB(stringBuilder.ToString(), session, url);
                    //QueryUtils.AddMessageToLog(stringBuilder.ToString(), fileName, session);
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.Message);
                }
                finally
                {
                    stringBuilder.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }

        private static object GetSqlValue(object sqlValue)
        {
            return sqlValue == DBNull.Value
                ? "NULL"
                : sqlValue is string || sqlValue is DateTime
                    ? $@"'{sqlValue}'"
                    : sqlValue;
        }

        private static string GetFormatted(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd hh:mm:ss.fff");
        }


    }
}