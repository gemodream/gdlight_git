﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Corpt.Models;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data;
using Corpt.Constants;

namespace Corpt.Utilities
{
    public class QueryPrice
    {
		//     public static List<PriceModel> GetPriceValues(SingleItemModel itemModel, List<MeasurePartModel> parts, Page p)
		//     {
		//         var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
		//         //conn.Open();
		//         var command = new SqlCommand
		//         {
		//             CommandText = "spGetAppraisal",
		//             Connection = conn,
		//             CommandType = CommandType.StoredProcedure,
		//             CommandTimeout = p.Session.Timeout
		//         };
		//         //command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 150));
		//         //command.Parameters["@rId"].Direction = ParameterDirection.Output;
		//         command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
		//         command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
		//         command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);

		//         command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);
		//         command.Parameters.AddWithValue("@ItemCode", itemModel.NewItemCode);
		//         //LogUtils.UpdateSpLogWithParameters(command, p);
		//         var dt = new DataTable();
		//conn.Open();
		//var da = new SqlDataAdapter(command);
		//         da.Fill(dt);
		//         var result = (from DataRow row in dt.Rows select new PriceModel(row, itemModel.BatchId, parts)).ToList();
		//         //var sum = command.Parameters["@rId"].Value.ToString();
		//         //// Add Item Container
		//         //var partContainer = parts.Find(m => m.PartTypeId == DbConstants.PartTypeIdItemContainer);
		//         //if (partContainer != null)
		//         //{
		//         //    result.Insert(0, new PriceModel(partContainer, sum, result[0]));
		//         //}
		//         conn.Close();

		//         //-- Get Measure Values
		//         var measureValues = QueryUtils.GetMeasureValues(itemModel, "", p);
		//         var apprValues = measureValues.FindAll(m => m.MeasureId == DbConstants.MeasureIdAppraisal);
		//         foreach (var appr in apprValues)
		//         {
		//             var priceModel = result.Find(m => m.PartId == appr.PartId);
		//             if (priceModel != null)
		//             {
		//                 priceModel.PriceSaved = appr.MeasureValue;
		//             }
		//         }
		//         return result;
		//     }

		public static List<PriceModel> GetPriceValues(SingleItemModel itemModel, List<MeasurePartModel> parts, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			//conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetAppraisal",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			//command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 150));
			//command.Parameters["@rId"].Direction = ParameterDirection.Output;
			command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
			command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);

			command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);
			command.Parameters.AddWithValue("@ItemCode", itemModel.NewItemCode);
			//LogUtils.UpdateSpLogWithParameters(command, p);
			var dt = new DataTable();
			conn.Open();
			var da = new SqlDataAdapter(command);
			da.Fill(dt);
			var result = (from DataRow row in dt.Rows select new PriceModel(row, itemModel.BatchId, parts)).ToList();
			//var sum = command.Parameters["@rId"].Value.ToString();
			//// Add Item Container
			//var partContainer = parts.Find(m => m.PartTypeId == DbConstants.PartTypeIdItemContainer);
			//if (partContainer != null)
			//{
			//    result.Insert(0, new PriceModel(partContainer, sum, result[0]));
			//}
			conn.Close();

			//-- Get Measure Values
			var measureValues = QueryUtils.GetMeasureValues(itemModel, "", p);
			var apprValues = measureValues.FindAll(m => m.MeasureId == DbConstants.MeasureIdAppraisal);
			foreach (var appr in apprValues)
			{
				var priceModel = result.Find(m => m.PartId == appr.PartId);
				if (priceModel != null)
				{
					priceModel.PriceSaved = appr.MeasureValue;
				}
			}
			return result;
		}




        public static List<PriceModel> GetPriceValuesVY(SingleItemModel itemModel, List<MeasurePartModel> parts, Page p, DataSet btch, string selectedValue ,  int BatchID)
        {

            if (btch == null)
            {
                btch = new DataSet();
                QueryUtils.GetAppraisalBatchdataset(BatchID, btch, p);
                p.Session["btch"] = btch;
            }

            //VY3
            List<PriceModel> result;
            DataTable dt = btch.Tables[0];
            DataView view = new DataView(dt);
            if (selectedValue != "")
            {
                var Query = from DataRowView rowView in view
                            where rowView["ItemNumber"].ToString() == selectedValue

                            select new
                            PriceModel(rowView.Row, BatchID);
                result = Query.ToList();
            }
            else
            {
                result = (from DataRow row in dt.Rows select new PriceModel(row, BatchID)).ToList();
            }

            //var measureValues = QueryUtils.GetMeasureValues(itemModel, "", p);
            //var apprValues = measureValues.FindAll(m => m.MeasureId == DbConstants.MeasureIdAppraisal);
            //foreach (var appr in apprValues)
            //{
            //    var priceModel = result.Find(m => m.PartId == appr.PartId);
            //    if (priceModel != null)
            //    {
            //        priceModel.PriceSaved = appr.MeasureValue;
            //    }
            //}
            return result;

        }

        public static List<PriceModel> GetPriceValuesVY(SingleItemModel itemModel, List<MeasurePartModel> parts, Page p,  DataSet btch , Boolean BatchMode ,  int BatchID  )
        {

            if (btch == null)
            {
                btch = new DataSet();
                QueryUtils.GetAppraisalBatchdataset(BatchID, btch,  p);
                p.Session["btch"] = btch;
            }

            //VY3
            List<PriceModel> result;
            DataTable dt = btch.Tables[0];

            DataView view = new DataView(dt);
            if (BatchMode)
            {
                var Query = from DataRowView rowView in view
                            where rowView["PartName"].ToString() == "IC"

                            select new
                            PriceModel(rowView.Row, BatchID);
                result = Query.ToList();
            }
            else
            {
                result = (from DataRow row in dt.Rows select new PriceModel(row, BatchID)).ToList();
            }


           

           
            return result;

        }

        public static List<PriceModel> GetPriceValues(SingleItemModel itemModel, int itemCode, List<MeasurePartModel> parts, Page p)
		{
            try
            {
                var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
                //conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetBatchItemAppraisal",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                //command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 150));
                //command.Parameters["@rId"].Direction = ParameterDirection.Output;
                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);

                command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);
                command.Parameters.AddWithValue("@ItemCode", itemCode);
                //LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                conn.Open();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                var result = (from DataRow row in dt.Rows select new PriceModel(row, itemModel.BatchId, parts)).ToList();
                //var sum = command.Parameters["@rId"].Value.ToString();
                //// Add Item Container
                //var partContainer = parts.Find(m => m.PartTypeId == DbConstants.PartTypeIdItemContainer);
                //if (partContainer != null)
                //{
                //    result.Insert(0, new PriceModel(partContainer, sum, result[0]));
                //}
                conn.Close();

                //-- Get Measure Values
                var measureValues = QueryUtils.GetMeasureValues(itemModel, "", p);
                var apprValues = measureValues.FindAll(m => m.MeasureId == DbConstants.MeasureIdAppraisal);
                foreach (var appr in apprValues)
                {
                    var priceModel = result.Find(m => m.PartId == appr.PartId);
                    if (priceModel != null)
                    {
                        priceModel.PriceSaved = appr.MeasureValue;
                    }
                }
                return result;
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
		}

		public static string SaveAppraisal(List<PriceModel> priceList, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            foreach (PriceModel model in priceList)
            {

                var command = new SqlCommand
                {
                    CommandText = "sp_SetPartValueCCM_MeasureID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@rId", SqlDbType.Int);
                command.Parameters["@rId"].Direction = ParameterDirection.Output;
                command.Parameters["@rId"].Value = DBNull.Value;

                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@ItemCode", model.ItemCode);
                command.Parameters.AddWithValue("@BatchID", model.BatchId);
                command.Parameters.AddWithValue("@PartID", model.PartId);
                command.Parameters.AddWithValue("@MeasureID", DbConstants.MeasureIdAppraisal);
                command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValue", float.Parse(model.PriceNew));
                command.Parameters.AddWithValue("@StringValue", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValueID", DBNull.Value);
                LogUtils.UpdateSpLogWithParameters(command, p);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    return ex.Message;
                }
            }
            conn.Close();
            return errMsg;

        }

        public static string SaveColoredDiamondAppraisal(List<PriceModel> priceList, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            foreach (PriceModel model in priceList)
            {
                if (model.PriceNew == "")
                    continue;
                var command = new SqlCommand
                {
                    CommandText = "sp_SetPartValueCCM_MeasureID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@rId", SqlDbType.Int);
                command.Parameters["@rId"].Direction = ParameterDirection.Output;
                command.Parameters["@rId"].Value = DBNull.Value;

                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@ItemCode", model.ItemCode);
                command.Parameters.AddWithValue("@BatchID", model.BatchId);
                command.Parameters.AddWithValue("@PartID", model.PartId);
                command.Parameters.AddWithValue("@MeasureID", DbConstants.MeasureIdAppraisal);
                command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValue", float.Parse(model.PriceNew));
                command.Parameters.AddWithValue("@StringValue", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValueID", DBNull.Value);
                LogUtils.UpdateSpLogWithParameters(command, p);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    return ex.Message;
                }
            }
            conn.Close();
            return errMsg;

        }//SaveColoredDiamondAppraisal

        public static string SaveColoredDiamondBulkAppraisal(List<PriceModel> priceList, Page p, string number = "0")
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            foreach (PriceModel model in priceList)
            {
                if (model.PriceNew == "")
                    continue;
                var command = new SqlCommand
                {
                    CommandText = "sp_SetPartValueCCM_MeasureID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@rId", SqlDbType.Int);
                command.Parameters["@rId"].Direction = ParameterDirection.Output;
                command.Parameters["@rId"].Value = DBNull.Value;

                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
                if (number == "0")
                    command.Parameters.AddWithValue("@ItemCode", model.ItemCode);
                else
                    command.Parameters.AddWithValue("@ItemCode", number);
                command.Parameters.AddWithValue("@BatchID", model.BatchId);
                command.Parameters.AddWithValue("@PartID", model.PartId);
                command.Parameters.AddWithValue("@MeasureID", DbConstants.MeasureIdAppraisal);
                command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValue", float.Parse(model.PriceNew));
                command.Parameters.AddWithValue("@StringValue", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValueID", DBNull.Value);
                LogUtils.UpdateSpLogWithParameters(command, p);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    return ex.Message;
                }
            }
            conn.Close();
            return errMsg;

        }//SaveColoredDiamondAppraisal

        public static void SaveMSRP(DataTable dt, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";
            var param = new SqlParameter
            {
                ParameterName = "SaveMSRPList",
                SqlDbType = SqlDbType.Structured,
                Value = dt,
                Direction = ParameterDirection.Input
            };
            
                var command = new SqlCommand
                {
                    CommandText = "spSaveMSRP",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
            command.Parameters.Add(param);
            
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
            }
            
            conn.Close();

        }//SaveMSRP

        public static DataTable GetSavedMSRP(string GroupCode, string BatchCode, string ItemCode, Page p)
        {
            string sqlText = @"select * from tblSavedMSRP where GroupCode=" + GroupCode + @" and BatchCode=" + BatchCode + @" and ItemCode=" + ItemCode;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = sqlText,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            return dt;
        }
    }
}