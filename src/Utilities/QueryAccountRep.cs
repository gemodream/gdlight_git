﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Models;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.Utilities.BulkBilling;
using GemoDream.Database;

namespace Corpt.Utilities
{
    public class QueryAccountRep
    {

        public static int GetBatchPassedFailedTotal(int groupCode, int batchCode, Page p)
        {
            var session = p.Session;
            var sql = "SELECT ItemsQuantity FROM [dbo].[v0Batch] WHERE GroupCode = @GroupCode AND BatchCode = @BatchCode";
            var dataTable = ExecuteSelectStatement(session, sql, parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
            });
            return int.Parse(dataTable.Rows[0][0].ToString());
        }

        public static int GetBatchPassed(int groupCode, int batchCode, int itemCode, AccountRep p)
        {
            var session = p.Session;
            var dataSet = ExecuteStoreProcedureToDataSet(session, "spGetPrintingQueue", parameters =>
            {
                /*
                    @GroupCode int,
                    @BatchCode int,
                    @ItemCode int,
                    @ReportStatus varchar(1),
                    @AuthorID dnSmallID = 1,
                    @AuthorOfficeID dnTinyID = 1,
                    @ShowLabel bit = 0,
                    @ShowLaserGold bit = 0,
                    @ShowGMXorderFiles bit = 0,
                    @ShowOldNumbers bit = 0,
                    @Prefix NVARCHAR(MAX) = ''
                 */
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
                parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;
                parameters.Add("@ReportStatus", SqlDbType.VarChar, 1).Value = "A";
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            var batchCodeModels = dataSet.Tables[0].Rows.Cast<DataRow>()
                .Select(row => new GroupBatchItemCodeModel(row)).ToList();

            var passed = batchCodeModels
                .GroupBy(model => new { model.GroupCode, model.BatchCode, model.ItemCode })
                .Count();

            return passed;
        }

        public static bool IsIsSkuOnly(int groupCode, int batchCode, Page p)
        {
            var session = p.Session;
            var sql = "SELECT [dbo].[IsSkuOnly](@GroupCode, @BatchCode)";
            var dataTable = ExecuteSelectStatement(session, sql, parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
            });
            return dataTable.Rows[0][0] != null && (dataTable.Rows[0][0] as bool? ?? false);
        }

        public static List<SkuRulesDataPerBatchModel> GetSkuRulesDataPerBatch(int groupCode, int batchCode, Page p)
        {
            var session = p.Session;
            var dataSet = ExecuteStoreProcedureToDataSet(session, "spGetSkuRulesDataPerBatch", parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
            });
            var qbInvoiceRequestModels = dataSet.Tables[0].Rows.Cast<DataRow>()
                .Select(row => new SkuRulesDataPerBatchModel(row)).ToList();
            return qbInvoiceRequestModels;
        }

        public static int AddQbInvoiceRequest(int invoiceId, string requestType, Page p)
        {
            var session = p.Session;
            var dataSet = ExecuteStoreProcedureToDataSet(session, "spAddQBInvoiceRequest", parameters =>
            {
                parameters.Add("@RequestType", SqlDbType.VarChar).Value = QbInvoiceRequestModel.RequestTypeGet;
                parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoiceId;
            });
            return dataSet.Tables[0].Rows.Cast<DataRow>()
                .Select(row => row.ConvertToInt("ID")).FirstOrDefault();
        }

        public static QbInvoiceRequestModel GetQbInvoiceRequest(int requestId, Page p)
        {
            var session = p.Session;
            var dataSet = ExecuteStoreProcedureToDataSet(session, "spGetQBInvoiceRequest", parameters =>
            {
                parameters.Add("@ID", SqlDbType.Int).Value = requestId;
            });
            var qbInvoiceRequestModel = dataSet.Tables[0].Rows.Cast<DataRow>()
                .Select(row => new QbInvoiceRequestModel(row)).FirstOrDefault();
            return qbInvoiceRequestModel;
        }
        
        public static string GetQbCustomerName(int customerCode, int retailerId, Page p)
        {
            var session = p.Session;

            var sql = "SELECT [dbo].[GetQBCustomerNameByRetailer](@CustomerCode, @RetailerId) AS QBCustomerName";
            var dataTable = ExecuteSelectStatement(session, sql, parameters =>
            {
                parameters.Add("@CustomerCode", SqlDbType.Int).Value = customerCode;
                parameters.Add("@RetailerId", SqlDbType.Int).Value = retailerId;
            });

            return dataTable.Rows[0][0] as string;
        }

        public static CustomerModel GetCustomerByOrderCode(int orderCode, HttpSessionState session)
        {
            var sql = "SELECT * " +
                      "FROM v0Customer c " +
                      "INNER JOIN tblGroup g ON g.CustomerID = c.CustomerID " +
                      "WHERE g.GroupCode = @GroupCode";
            var dataTable = ExecuteSelectStatement(session, sql, parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = orderCode;
            });

            if (dataTable.Rows.Count > 0)
            {
                return new CustomerModel(dataTable.Rows[0]);
            }

            return null;
        }

        public static List<ListItem> GetServiceTypes(HttpSessionState session)
        {
            const string sql = "SELECT ServiceTypeID, ServiceTypeName, ServiceTypeCode " +
                               "FROM tblServiceType " +
                               "ORDER BY ServiceTypeCode";
            var dataTable = ExecuteSelectStatement(session, sql, parameters => {});
            return dataTable.Rows.Cast<DataRow>()
                .Select(row => new ListItem
                    {
                        Text = row.ConvertToString("ServiceTypeName"),
                        Value = row.ConvertToString("ServiceTypeCode")
                    })
                .ToList();
        }

        public static List<CheckOrderCodeModel> CheckOrderCodes(
            List<int> orderCodes, List<int> serviceTypeIds, Page p)
        {
            var session = p.Session;

            var orderIdsWrappers = (orderCodes == null)
                ? new List<IdWrapper>()
                : orderCodes.Select(i => new IdWrapper() { ID = i }).ToList();

            var orderIdsTable = CollectionHelper.ConvertTo(orderIdsWrappers);
            orderIdsTable = orderIdsTable.GetChanges(DataRowState.Added);

            var serviceTypeIdWrappers = (serviceTypeIds == null)
                ? new List<IdWrapper>()
                : serviceTypeIds.Select(i => new IdWrapper() { ID = i }).ToList();

            var serviceTypeIdsTable = CollectionHelper.ConvertTo(serviceTypeIdWrappers);
            serviceTypeIdsTable = serviceTypeIdsTable.GetChanges(DataRowState.Added);

            var dataSet = ExecuteStoreProcedureToDataSet(session, "spCheckOrderCodes", parameters =>
            {
                var orderIdsParam = parameters.AddWithValue("@GroupCodes", orderIdsTable);
                orderIdsParam.SqlDbType = SqlDbType.Structured;
                orderIdsParam.TypeName = "dbo.intTableType";

                var serviceTypesParam = parameters.AddWithValue("@ServiceTypeIds", serviceTypeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            var checkOrderCodeModels = Enumerable.Cast<DataRow>(dataSet.Tables[0].Rows)
                .Select(row => new CheckOrderCodeModel(row))
                .ToList();

            return checkOrderCodeModels;

        }

        public static CustomerInvoicesResult GetCustomerInvoices(int customerId, List<int> orderIds, Page p)
        {
            var session = p.Session;

            var orderIdsWrappers = (orderIds == null)
                ? new List<IdWrapper>()
                : orderIds.Select(i => new IdWrapper() { ID = i }).ToList();

            var orderIdsTable = CollectionHelper.ConvertTo(orderIdsWrappers);
            orderIdsTable = orderIdsTable.GetChanges(DataRowState.Added);

            var dataSet = ExecuteStoreProcedureToDataSet(session, "spGetCustomerInvoices", parameters =>
            {
                parameters.Add("@CustomerId", SqlDbType.Int).Value = customerId;

                var orderIdsParam = parameters.AddWithValue("@OrderIds", orderIdsTable);
                orderIdsParam.SqlDbType = SqlDbType.Structured;
                orderIdsParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            var customerInvoiceModels = Enumerable.Cast<DataRow>(dataSet.Tables[0].Rows)
                .Select(row => new CustomerInvoiceModel(row))
                .ToList();
            var orderInvoices = Enumerable.Cast<DataRow>(dataSet.Tables[1].Rows)
                .Select(row => new OrdersInvoiceModel(row))
                .ToList();

            return new CustomerInvoicesResult
            {
                CustomerInvoiceModels = customerInvoiceModels,
                OrdersInvoice = orderInvoices
            };

        }

        public static List<InvoiceDetailsData> GetInvoiceDetails(int invoiceId, Page p)
        {
            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetInvoiceDetails", parameters =>
            {
                parameters.Add("@InvoiceID", SqlDbType.Int).Value = invoiceId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows)
                .Select(row => new InvoiceDetailsData(row))
                .ToList();
        }

        public static int AddBulkBillingInvoice(int customerId, int customerOfficeId,
            List<ItemToBeInvoiced> invoicingList, Page p, string invoiceStatus)
        {
            var invoicingTableTable = CollectionHelper.ConvertTo(invoicingList);
            invoicingTableTable = invoicingTableTable.GetChanges(DataRowState.Added);

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spAddBulkBillingInvoice", parameters =>
            {
                parameters.Add("@CustomerID", SqlDbType.Int).Value = customerId;
                parameters.Add("@CustomerOfficeId", SqlDbType.Int).Value = customerOfficeId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@InvoiceStatus", SqlDbType.Text).Value = invoiceStatus;

                var codesParam = parameters.AddWithValue("@ItemList", invoicingTableTable);
                codesParam.SqlDbType = SqlDbType.Structured;
                codesParam.TypeName = "dbo.InvoicingBulkBillingTableType";
            });
            var dataRow = Enumerable.Cast<DataRow>(dataTable.Rows).ToArray()[0];
            return dataRow.ConvertToInt("InvoiceID");
        }

        public static List<int> AddBulkBillingInSeparateInvoices(int customerId, int customerOfficeId,
            List<ItemToBeInvoiced> invoicingList, Page p, string invoiceStatus)
        {
            var invoicingTableTable = CollectionHelper.ConvertTo(invoicingList);
            invoicingTableTable = invoicingTableTable.GetChanges(DataRowState.Added);

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spAddBulkBillingInSeparateInvoices", parameters =>
            {
                parameters.Add("@CustomerID", SqlDbType.Int).Value = customerId;
                parameters.Add("@CustomerOfficeId", SqlDbType.Int).Value = customerOfficeId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@InvoiceStatus", SqlDbType.Text).Value = invoiceStatus;

                var codesParam = parameters.AddWithValue("@ItemList", invoicingTableTable);
                codesParam.SqlDbType = SqlDbType.Structured;
                codesParam.TypeName = "dbo.InvoicingBulkBillingTableType";
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows)
                .Select(row => row.ConvertToInt("InvoiceID"))
                .ToList();
        }

        public static List<AlreadyBilledOrderData> GetAlreadyBilledOrders(IEnumerable<int> orderCodes, Page p)
        {
            var orderCodeIdWrappers = (orderCodes == null)
                ? new List<IdWrapper>()
                : orderCodes.Select(i => new IdWrapper() { ID = i }).ToList();
            var orderCodeIdsTable = CollectionHelper.ConvertTo(orderCodeIdWrappers);
            orderCodeIdsTable = orderCodeIdsTable.GetChanges(DataRowState.Added);

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetAlreadyBilledOrders", parameters =>
            {
                var serviceTypesParam = parameters.AddWithValue("@OrderCodes", orderCodeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows)
                .Select(row => new AlreadyBilledOrderData(row))
                .ToList();
        }

        public static List<QuantitiesData> GetBulkBillingQuantitiesData(IEnumerable<int> orderCodes, Page p)
        {
            var orderCodeIdWrappers = (orderCodes == null)
                ? new List<IdWrapper>()
                : orderCodes.Select(i => new IdWrapper() { ID = i }).ToList();
            var orderCodeIdsTable = CollectionHelper.ConvertTo(orderCodeIdWrappers);
            orderCodeIdsTable = orderCodeIdsTable.GetChanges(DataRowState.Added);

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetBulkBillingQuantitiesData", parameters =>
            {
                var serviceTypesParam = parameters.AddWithValue("@OrderCodes", orderCodeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows)
                .Select(row => new QuantitiesData(row))
                .ToList();
        }

        public static List<PriceSetModel> GetBulkBillingPrices(Page p)
        {
            var dataTable = ExecuteStoreProcedure(p.Session, "spGetBulkBillingPrices", parameters => {});
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new PriceSetModel(row)).ToList();
        }

        public static List<MigratedItemModel> GetMigratedItems(IEnumerable<int> groupCodes, Page p)
        {
            var itemCodeList = groupCodes == null
                ? new List<IdWrapper>()
                : groupCodes.Select(groupCode => new IdWrapper { ID = groupCode }).ToList();
            var itemCodeTable = CollectionHelper.ConvertTo(itemCodeList);
            itemCodeTable = itemCodeTable.GetChanges(DataRowState.Added);

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetMigratedItemByCodeList", parameters =>
            {
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);

                var codesParam = parameters.AddWithValue("@GroupCodeList", itemCodeTable);
                codesParam.SqlDbType = SqlDbType.Structured;
                codesParam.TypeName = "dbo.intTableType";

            });
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new MigratedItemModel(row)).ToList();
        }

        public static List<MeasureWithPreviousModel> GetMeasureWithPrevious(
            int groupCode,
            int batchCode,
            int itemCode,
            int? prevGroupCode,
            int? prevBatchCode,
            int? prevItemCode,
            Page p)
        {

            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetMeasureWithPrevious", parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
                parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;

                parameters.Add("@PrevGroupCode", SqlDbType.Int).Value = prevGroupCode;
                parameters.Add("@PrevBatchCode", SqlDbType.Int).Value = prevBatchCode;
                parameters.Add("@PrevItemCode", SqlDbType.Int).Value = prevItemCode;

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new MeasureWithPreviousModel(row)).ToList();
        }

        public static string SetInvoicingGo(
            int invoiceId,
            int batchId,
            int itemCode,
            int pass,
            bool addSku,
            bool addLotNumber, 
            HttpSessionState session,
            int? serviceType)
        {
            var returnCode = ExecuteSetStoreProcedure(session, "spSetInvoicingGo", parameters =>
            {
                parameters.Add("@rId", SqlDbType.VarChar);
                parameters["@rId"].Size = 150;
                parameters["@rID"].Direction = ParameterDirection.Output;

                parameters.Add("@InvoiceID", SqlDbType.Int).Value = invoiceId;
                parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
                parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;
                parameters.Add("@Pass", SqlDbType.Int).Value = pass;

                parameters.Add("@AddSKU", SqlDbType.Bit).Value = addSku;
                parameters.Add("@AddLotNumber", SqlDbType.Bit).Value = addLotNumber;

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@ServiceType", SqlDbType.Int).Value = serviceType;
            });
            return returnCode;
        }

        public static CheckItemCpAndPriceModel GetCheckItemCpAndPrice(int batchId, int itemCode, HttpSessionState session)
        {
            var dataTable = ExecuteStoreProcedure(session, "spGetCheckItemCpAndPrice", parameters =>
            {
                parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
                parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new CheckItemCpAndPriceModel(row)).ToList()[0];
        }

        public static void UpdateInvoice2(
            int customerId, 
            int customerOfficeId, 
            int existsInvoiceId,
            int billTo, 
            HttpSessionState session)
        {
            ExecuteSetStoreProcedure(session, "spUpdateInvoice2", parameters =>
            {
                parameters.Add("@rId", SqlDbType.VarChar);
                parameters["@rId"].Size = 150;
                parameters["@rID"].Direction = ParameterDirection.Output;

                parameters.Add("@CustomerID", SqlDbType.Int).Value = customerId;
                parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = customerOfficeId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@ExistsInvoiceID", SqlDbType.BigInt).Value = existsInvoiceId;
                parameters.Add("@BillTo", SqlDbType.Int).Value = billTo;
            });
        }

        public static int AddInvoice2(int customerId, int customerOfficeId, HttpSessionState session)
        {
            var returnCode = ExecuteSetStoreProcedure(session, "spAddInvoice2", parameters =>
            {
                parameters.Add("@rId", SqlDbType.VarChar);
                parameters["@rId"].Size = 150;
                parameters["@rID"].Direction = ParameterDirection.Output;

                parameters.Add("@CustomerID", SqlDbType.Int).Value = customerId;
                parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = customerOfficeId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return int.Parse(returnCode);
        }

        public static List<BatchItemInvoiceModel> GetBatchInvoice(int batchId, HttpSessionState session)
        {
            var dataTable = ExecuteStoreProcedure(session, "spGetBatchInvoice", parameters =>
            {
                parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new BatchItemInvoiceModel(row)).ToList();
        }

        public static int GetAccountType(int groupCode, int customerId, int customerOfficeId, HttpSessionState session)
        {
            var dataTable = ExecuteStoreProcedure(session, "spGetAccountType", parameters =>
            {
                parameters.Add("@CustomerID", SqlDbType.Int).Value = customerId;
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = customerOfficeId;
            });
            var accountType = int.Parse(Enumerable.Cast<DataRow>(dataTable.Rows).ToList()[0][0].ToString());
            return accountType;
        }

        public static string SetSaveToTblTmpAdditionalServiceByBatch(
            int orderCode, string xmlServices, HttpSessionState session)
        {
            var returnCode = ExecuteSetStoreProcedure(session, "spSetSaveTotblTmpAdditionalServiceByBatch", parameters =>
            {
                parameters.Add("@rId", SqlDbType.VarChar);
                parameters["@rId"].Size = 150;
                parameters["@rID"].Direction = ParameterDirection.Output;

                parameters.Add("@XmlServices", SqlDbType.Text).Value = xmlServices;
                parameters.Add("@OrderCode", SqlDbType.Int).Value = orderCode;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return returnCode;

        }

        public static List<AdditionalServiceModel> GetAdditionalServicesByOrderList(List<int> orderCodes, Page p)
        {
            var orderCodeIdWrappers = (orderCodes == null)
                ? new List<IdWrapper>()
                : orderCodes.Select(i => new IdWrapper() { ID = i }).ToList();

            var orderCodeIdsTable = CollectionHelper.ConvertTo(orderCodeIdWrappers);
            orderCodeIdsTable = orderCodeIdsTable.GetChanges(DataRowState.Added);
            
            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetAdditionalServicesByOrderList", parameters =>
            {
                var serviceTypesParam = parameters.AddWithValue("@OrderCodes", orderCodeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows)
                .Select(row => new AdditionalServiceModel(row))
                .ToList();
        }

        public static IEnumerable<DocTypeNameByItemModel> GetDocTypeNameByItemCode(int groupCode, int batchCode, int itemCode, Page p)
        {
            var session = p.Session;
            var dataTable = ExecuteStoreProcedure(session, "spGetDocTypeNameByItemCode", parameters =>
            {
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode;
                parameters.Add("@BatchCode", SqlDbType.Int).Value = batchCode;
                parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;
                parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + session["AuthorOfficeID"]);
            });
            return Enumerable.Cast<DataRow>(dataTable.Rows).Select(row => new DocTypeNameByItemModel(row)).ToList();
        }

        public static DataSet GetMemoAndPoByCustomer(HttpSessionState session,
            int? customerCode = null,
            int? groupState = null,
            int? groupCode = null,
            List<int> serviceTypeIds = null,
            DateTime? beginDate = null,
            DateTime? endDate = null)
        {
            var serviceTypeIdWrappers = (serviceTypeIds == null)
                ? new List<IdWrapper>()
                : serviceTypeIds.Select(i => new IdWrapper() { ID = i }).ToList();

            var serviceTypeIdsTable = CollectionHelper.ConvertTo(serviceTypeIdWrappers);
            serviceTypeIdsTable = serviceTypeIdsTable.GetChanges(DataRowState.Added);

            return ExecuteStoreProcedureToDataSet(session, "spGetMemoAndPOByCustomer", parameters =>
            {
                parameters.Add("@CustomerCode", SqlDbType.Int).Value = customerCode ?? (object)DBNull.Value;
                parameters.Add("@BGroupState", SqlDbType.Int).Value = groupState ?? (object)DBNull.Value;
                parameters.Add("@BDate", SqlDbType.DateTime).Value = beginDate ?? (object)DBNull.Value;
                parameters.Add("@EDate", SqlDbType.DateTime).Value = endDate ?? (object)DBNull.Value;
                parameters.Add("@GroupCode", SqlDbType.Int).Value = groupCode ?? (object)DBNull.Value;
                parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + session["AuthorOfficeID"]);

                var serviceTypesParam = parameters.AddWithValue("@ServiceTypeIds", serviceTypeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";
            });
        }

        public static DataSet GetOrderTree(HttpSessionState session,
            int? customerCode = null,
            int? groupState = null,
            List<int> groupCodes = null,
            List<int> serviceTypeIds = null,
            DateTime? beginDate = null,
            DateTime? endDate = null, 
            string memo = null, 
            string po = null)
        {
            var serviceTypeIdWrappers = (serviceTypeIds == null)
                ? new List<IdWrapper>()
                : serviceTypeIds.Select(i => new IdWrapper() {ID = i}).ToList();

            var serviceTypeIdsTable = CollectionHelper.ConvertTo(serviceTypeIdWrappers);
            serviceTypeIdsTable = serviceTypeIdsTable.GetChanges(DataRowState.Added);

            var groupCodesWrappers = (groupCodes == null)
                ? new List<IdWrapper>()
                : groupCodes.Select(i => new IdWrapper() { ID = i }).ToList();

            var groupCodesTable = CollectionHelper.ConvertTo(groupCodesWrappers);
            groupCodesTable = groupCodesTable.GetChanges(DataRowState.Added);

            return ExecuteStoreProcedureToDataSet(session, "spGetOrderTree", parameters =>
            {
                parameters.Add("@CustomerCode", SqlDbType.Int).Value = customerCode ?? (object)DBNull.Value;
                parameters.Add("@BGroupState", SqlDbType.Int).Value = groupState ?? (object)DBNull.Value;
                parameters.Add("@BDate", SqlDbType.DateTime).Value = beginDate ?? (object)DBNull.Value;
                parameters.Add("@EDate", SqlDbType.DateTime).Value = endDate ?? (object)DBNull.Value;

                var groupCodesParam = parameters.AddWithValue("@GroupCodes", groupCodesTable);
                groupCodesParam.SqlDbType = SqlDbType.Structured;
                groupCodesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + session["AuthorOfficeID"]);
                parameters.Add("@Memo", SqlDbType.NVarChar).Value = memo ?? (object)DBNull.Value;
                parameters.Add("@Po", SqlDbType.NVarChar).Value = po ?? (object)DBNull.Value;

                var serviceTypesParam = parameters.AddWithValue("@ServiceTypeIds", serviceTypeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";
            });
        }

        public static DataTable GetGroupsByCriteria(HttpSessionState session, DataSet searchResultDataSet,
            int? customerCode = null,
            int? groupState = null,
            List<int> groupCodes = null,
            List<int> serviceTypeIds = null,
            DateTime? beginDate = null,
            DateTime? endDate = null)
        {
            var serviceTypeIdWrappers = (serviceTypeIds == null)
                ? new List<IdWrapper>()
                : serviceTypeIds.Select(i => new IdWrapper() { ID = i }).ToList();

            var serviceTypeIdsTable = CollectionHelper.ConvertTo(serviceTypeIdWrappers);
            serviceTypeIdsTable = serviceTypeIdsTable.GetChanges(DataRowState.Added);

            var groupCodesWrappers = (groupCodes == null)
                ? new List<IdWrapper>()
                : groupCodes.Select(i => new IdWrapper() { ID = i }).ToList();

            var groupCodesTable = CollectionHelper.ConvertTo(groupCodesWrappers);
            groupCodesTable = groupCodesTable.GetChanges(DataRowState.Added);

            return ExecuteStoreProcedure(session, "spGetGroupByCodeAccRep", parameters =>
            {
                parameters.Add("@CustomerCode", SqlDbType.Int).Value = customerCode ?? (object)DBNull.Value;
                parameters.Add("@BGroupState", SqlDbType.Int).Value = groupState ?? (object)DBNull.Value;
                parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
                parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
                parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
                parameters.Add("@BDate", SqlDbType.DateTime).Value = beginDate ?? (object)DBNull.Value;
                parameters.Add("@EDate", SqlDbType.DateTime).Value = endDate ?? (object)DBNull.Value;

                var groupCodesParam = parameters.AddWithValue("@GroupCodes", groupCodesTable);
                groupCodesParam.SqlDbType = SqlDbType.Structured;
                groupCodesParam.TypeName = "dbo.intTableType";

                var serviceTypesParam = parameters.AddWithValue("@ServiceTypeIds", serviceTypeIdsTable);
                serviceTypesParam.SqlDbType = SqlDbType.Structured;
                serviceTypesParam.TypeName = "dbo.intTableType";

                parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + session["ID"]);
                parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + session["AuthorOfficeID"]);

            }, searchResultDataSet);
        }

        private static DataTable ExecuteStoreProcedure(
            HttpSessionState session,
            string spName,
            Action<SqlParameterCollection> addParametersFunc,
            DataSet searchResultDataSet = null,
            int tableIndex = 0)
        {
            using (var sqlConnection = new SqlConnection("" + session["MyIP_ConnectionString"]))
            {
                sqlConnection.Open();
                using (var command = CreateSpSqlCommand(sqlConnection, session, spName))
                {
                    addParametersFunc(command.Parameters);
                    LogUtils.UpdateSpLogWithParameters(command, session);

                    var sqlDataAdapter = new SqlDataAdapter(command);
                    var dataSet = searchResultDataSet;
                    string tableName;

                    if (dataSet == null)
                    {
                        dataSet = new DataSet();
                        sqlDataAdapter.Fill(dataSet);
                        tableName = dataSet.Tables[tableIndex].TableName;
                    }
                    else
                    {
                        tableName = spName.Substring(5).Replace("ByCode", "").Replace("byCode", "");
                        try
                        {
                            sqlDataAdapter.Fill(dataSet, tableName);
                        }
                        catch (Exception e)
                        {
                            LogUtils.UpdateSpLogWithException(e.Message, session);
                            throw;
                        }
                    }

                    return dataSet.Tables[tableName];
                }
            }
        }

        private static string ExecuteSetStoreProcedure(
            HttpSessionState session,
            string spName,
            Action<SqlParameterCollection> addParametersFunc)
        {
            using (var sqlConnection = new SqlConnection("" + session["MyIP_ConnectionString"]))
            {
                sqlConnection.Open();
                using (var command = CreateSpSqlCommand(sqlConnection, session, spName))
                {
                    addParametersFunc(command.Parameters);
                    LogUtils.UpdateSpLogWithParameters(command, session);

                    command.ExecuteNonQuery();
                    var res = command.Parameters["@rId"].Value?.ToString();
                    return res;
                }
            }
        }

        private static DataSet ExecuteStoreProcedureToDataSet(
            HttpSessionState session,
            string spName,
            Action<SqlParameterCollection> addParametersFunc)
        {
            using (var sqlConnection = new SqlConnection("" + session["MyIP_ConnectionString"]))
            {
                sqlConnection.Open();
                using (var command = CreateSpSqlCommand(sqlConnection, session, spName))
                {
                    addParametersFunc(command.Parameters);
                    LogUtils.UpdateSpLogWithParameters(command, session);

                    var sqlDataAdapter = new SqlDataAdapter(command);
                    var dataSet = new DataSet();
                    try
                    {
                        sqlDataAdapter.Fill(dataSet);
                    }
                    catch (Exception e)
                    {
                        LogUtils.UpdateSpLogWithException(e.Message, session);
                        throw;
                    }
                    return dataSet;
                }
            }
        }

        private static DataTable ExecuteSelectStatement(
            HttpSessionState session,
            string selectStatement,
            Action<SqlParameterCollection> addParametersFunc)
        {
            using (var sqlConnection = new SqlConnection("" + session["MyIP_ConnectionString"]))
            {
                sqlConnection.Open();
                using (var command = new SqlCommand(selectStatement, sqlConnection))
                {
                    addParametersFunc(command.Parameters);
                    LogUtils.UpdateSpLogWithParameters(command, session);

                    var sqlDataAdapter = new SqlDataAdapter(command);
                    var dataSet = new DataSet();
                    try
                    {
                        sqlDataAdapter.Fill(dataSet);
                    }
                    catch (Exception e)
                    {
                        LogUtils.UpdateSpLogWithException(e.Message, session);
                        throw;
                    }

                    return dataSet.Tables[0];
                }
            }
        }

        private static SqlCommand CreateSpSqlCommand(SqlConnection sqlConnection, HttpSessionState session, string spName)
        {
            return new SqlCommand
            {
                CommandText = spName,
                Connection = sqlConnection,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = session.Timeout
            };
        }

        public static DataSet GetOrdersTreeByCustomerCode(int customerCode, int groupState, HttpSessionState session,
            List<int> serviceTypeIds = null, DateTime? beginDate = null, DateTime? endDate = null, string memo = null,
            string po = null)
        {
            return GetOrderTree(session, customerCode, groupState, null, serviceTypeIds, beginDate, endDate, memo, po);
        }

        public static DataSet GetOrdersTreeByOrderCode(List<int> orderCodes, HttpSessionState session)
        {
            return GetOrderTree(session, null, null, orderCodes.Distinct().ToList(), null, null, null);
        }
    }

    public class GroupBatchItemCodeModel
    {
        public GroupBatchItemCodeModel(DataRow dataRow)
        {
            GroupCode = dataRow["GroupCode"].ToString();
            BatchCode = dataRow["BatchCode"].ToString();
            ItemCode = dataRow["Item"].ToString();
        }
        public string GroupCode { get; set; }
        public string BatchCode { get; set; }
        public string ItemCode { get; set; }

    }
}