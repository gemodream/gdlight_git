﻿using Corpt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Corpt.Utilities
{
	public class QueryDropDownNamesMergeHistory
	{
        public static void ReplaceDocumentNameForLabel(string newName, string oldName, Page p, ref string errorMsg)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spReplaceDocumentNameForLabel",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add("@NewName", SqlDbType.VarChar).Value = newName;
                command.Parameters.Add("@OldName", SqlDbType.VarChar).Value = oldName;

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "Replace failed for following dropdown values: " + oldName : errorMsg + ", " + oldName;
                }
            }
        }

        public static void RollBackDocumentName(string labelName, Page p, ref string errorMsg)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spRollBackDocumentName",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add("@LabelName", SqlDbType.VarChar).Value = labelName;
                try
                {                    
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    errorMsg = "Rollback for label " + labelName + " - failed";
                }
            }
        }
        public static List<string> GetMergeHistoryNewNames(Page p)
        {
            var result = new List<string>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spGetMergeHistoryNewNames",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? new List<string>() : (from DataRow row in dt.Rows select row[0].ToString()).ToList());
            }
            return result;
        }
        public static List<DropDownNamesDocMergeModel> GetDocumentNamesBeforeMerge(Page p)
        {
            var result = new List<DropDownNamesDocMergeModel>();
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetDocumentNamesBeforeMerge",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);

                result = (dt.Rows.Count == 0 ? new List<DropDownNamesDocMergeModel>() : (from DataRow row in dt.Rows select new DropDownNamesDocMergeModel(row)).ToList());
            }
            return result;
        }
    }
}
