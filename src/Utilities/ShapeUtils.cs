﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;
using System.Web.UI;

namespace Corpt.Utilities
{
    public class ShapeUtils
    {
        public static DataSet spGetShapes(Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetShapesNew",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", Convert.ToInt32(p.Session["ID"].ToString()));
            command.Parameters.AddWithValue("@AuthorOfficeID", Convert.ToInt32(p.Session["AuthorOfficeId"].ToString()));
            //command.Parameters.AddWithValue("@ShapeGroup", 0);
            command.Parameters.AddWithValue("@FullList", 0);
            LogUtils.UpdateSpLogWithParameters(command, p);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adapter;

                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }


    public static int GetMeasure(Page p, string hshapeid)
    {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select MeasureValueID from dbo.tblmeasurevalue where MeasureValueMeasureID = 8 and ValueCode = (select ShapeCode from dbo.tblShape where ShapeHistoryID = " + hshapeid + ")",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };

            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            if (cdt.Rows.Count > 0)
            {
                string Measure = cdt.Rows[0].ItemArray[0].ToString();
                return Convert.ToInt32(Measure);
            }
             conn.Close();
            return 0;
          }

    }
}