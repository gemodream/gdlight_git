﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Corpt.Utilities
{
    public class EverledgerURLUtils
    {

        public static EverledgerAPIInputModel GetEverledgerDataByItemNumber(string itemNumber, Page p)
        {
            string strConn;
            if (p.Session["UseRoDb"].ToString() == "1")
                strConn = p.Session["RO_MyIP_ConnectionString"].ToString();
            else
                strConn = p.Session["MyIP_ConnectionString"].ToString();

            var conn = new SqlConnection("" + strConn);

            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetEverledgerDataByItemNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@ItemNumber", SqlDbType.BigInt);
            command.Parameters["@ItemNumber"].Value = Int64.Parse(itemNumber);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EverledgerAPIInputModel(row)).ToList();
            conn.Close();
            return result[0];

        }

        public async Task<EverledgerAPIOutputModel> CreateItemByItemNumber(EverledgerAPIInputModel input, Page p)
        {
            EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(5);
                    string requestURL = @"https://api.everledger.io/asset-passport/assets";
                    var request = new HttpRequestMessage(HttpMethod.Post, requestURL);
                    request.Headers.Add("x-evl-client-id", p.Session["EvlClientId"].ToString());
                    request.Headers.Add("x-evl-client-secret", p.Session["EvlClientSecret"].ToString());

                    object selectedData;

                    if (input.LightPerformanceData.BrillianceValue == "" || input.LightPerformanceData.FireValue == "" || input.LightPerformanceData.ScintillationValue == "")
                    {
                        selectedData = new
                        {
                            reportNo = input.ItemNumber,
                            SKUNO = input.SKUNo,
                            certifier = input.Certifier
                        };
                    }
                    else
                    {
                        object lightPerformanceData = new
                        {
                            brillianceValue = Convert.ToInt32(input.LightPerformanceData.BrillianceValue),
                            fireValue = Convert.ToInt32(input.LightPerformanceData.FireValue),
                            scintillationValue = Convert.ToInt32(input.LightPerformanceData.ScintillationValue)
                        };

                        selectedData = new
                        {
                            reportNo = input.ItemNumber,
                            SKUNO = input.SKUNo,
                            certifier = input.Certifier,
                            lightPerformanceData
                        };
                    }

                    string jsonInput = JsonConvert.SerializeObject(selectedData);
                    jsonInput = "[" + jsonInput + "]";

                    var content = new StringContent(jsonInput, null, "application/json");

                    request.Content = content;
                    var response = await client.SendAsync(request);

                    objResultAPI.ItemNumber = input.ItemNumber;

                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var apiResult = await response.Content.ReadAsStringAsync();
                        var jsonToken = JToken.Parse(apiResult);

                        if (jsonToken.Type == JTokenType.Array)
                        {
                            // Process as JArray
                            JArray jsonArray = (JArray)jsonToken;

                            foreach (var jsonItem in jsonArray)
                            {
                                JObject jsonObject = (JObject)jsonItem;

                                if (jsonObject["status"] != null)
                                {
                                    objResultAPI.APIStatus = jsonObject["status"].ToString();  // Status = exists/created/error: mfgr_uid or reportNo must be specified
                                    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;

                                    //if (objResultAPI.APIStatus != "created")
                                    //{
                                    //    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //}
                                }
                            }
                        }
                        else if (jsonToken.Type == JTokenType.Object)
                        {

                        }
                    }
                }
                catch (HttpRequestException e) 
                {
                    objResultAPI.APIStatus = "Error";
                    objResultAPI.APIErrorMessage = e.Message;
                }
                
            }
            return objResultAPI;
        }

        public async Task<EverledgerAPIOutputModel> CreateItemByManufacturerID(EverledgerAPIInputModel input, Page p)
        {
            EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(5);
                    string requestURL = @"https://api.everledger.io/asset-passport/assets";
                    var request = new HttpRequestMessage(HttpMethod.Post, requestURL);
                    request.Headers.Add("x-evl-client-id", p.Session["EvlClientId"].ToString());
                    request.Headers.Add("x-evl-client-secret", p.Session["EvlClientSecret"].ToString());

                    object selectedData;

                    object lightPerformanceData = new
                    {
                        brillianceValue = input.LightPerformanceData.BrillianceValue,
                        fireValue = input.LightPerformanceData.FireValue,
                        scintillationValue = input.LightPerformanceData.ScintillationValue
                    };

                    if (input.LightPerformanceData.BrillianceValue != "" && input.LightPerformanceData.FireValue != "" && input.LightPerformanceData.ScintillationValue != "")
                    {
                        selectedData = new
                        {
                            mfgr_uid = input.ManufacturerID,
                            SKUNO = input.SKUNo,
                            certifier = input.Certifier,
                            lightPerformanceData
                        };
                    }
                    else
                    {
                        selectedData = new
                        {
                            mfgr_uid = input.ManufacturerID,
                            SKUNO = input.SKUNo,
                            certifier = input.Certifier,
                        };
                    }

                    string jsonInput = JsonConvert.SerializeObject(selectedData);
                    jsonInput = "[" + jsonInput + "]";

                    var content = new StringContent(jsonInput, null, "application/json");

                    request.Content = content;
                    var response = await client.SendAsync(request);

                    objResultAPI.ItemNumber = input.ItemNumber;

                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var apiResult = await response.Content.ReadAsStringAsync();
                        var jsonToken = JToken.Parse(apiResult);
                        
                        if (jsonToken.Type == JTokenType.Array)
                        {
                            // Process as JArray
                            JArray jsonArray = (JArray)jsonToken;
                            foreach (var jsonItem in jsonArray)
                            {
                                JObject jsonObject = (JObject)jsonItem;

                                if (jsonObject["status"] != null)
                                {
                                    objResultAPI.APIStatus = jsonObject["status"].ToString();  // Status = exists/created/error: mfgr_uid or reportNo must be specified
                                    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //if (objResultAPI.APIStatus != "created")
                                    //{
                                    //    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //}
                                }
                            }
                        }
                        else if (jsonToken.Type == JTokenType.Object)
                        {

                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    objResultAPI.APIStatus = "Error";
                    objResultAPI.APIErrorMessage = e.Message;
                }
            }
            return objResultAPI;
        }

        public async Task<EverledgerAPIOutputModel> AssociateDiamondToJewelry(EverledgerAPIInputModel input, Page p)
        {
            EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(5);
                    string requestURL = @"https://api.everledger.io/asset-passport/association";
                    var request = new HttpRequestMessage(HttpMethod.Post, requestURL);
                    request.Headers.Add("x-evl-client-id", p.Session["EvlClientId"].ToString());
                    request.Headers.Add("x-evl-client-secret", p.Session["EvlClientSecret"].ToString());

                    string[] uidsArray = input.ManufacturerID.Split('/');
                    List<string> associatedUIDs = new List<string>(uidsArray);

                    object selectedData;

                    selectedData = new
                    {
                        reportNo = input.ItemNumber,
                        associatedUIDs
                    };

                    string jsonInput = JsonConvert.SerializeObject(selectedData);
                    jsonInput = "[" + jsonInput + "]";
                    var content = new StringContent(jsonInput, null, "application/json");
                    request.Content = content;
                    var response = await client.SendAsync(request);

                    objResultAPI.ItemNumber = input.ItemNumber;

                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var apiResult = await response.Content.ReadAsStringAsync();
                        var jsonToken = JToken.Parse(apiResult);
                        
                        if (jsonToken.Type == JTokenType.Array)
                        {
                            // Process as JArray
                            JArray jsonArray = (JArray)jsonToken;
                            foreach (var jsonItem in jsonArray)
                            {
                                JObject jsonObject = (JObject)jsonItem;
                                if (jsonObject["status"] != null)
                                {
                                    objResultAPI.APIStatus = jsonObject["status"].ToString();  // Status = exists/success/error: mfgr_uid or reportNo must be specified
                                    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;

                                    //if (objResultAPI.APIStatus != "success")
                                    //{
                                    //    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //}
                                }
                            }
                        }
                        else if (jsonToken.Type == JTokenType.Object)
                        {
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    objResultAPI.APIStatus = "Error";
                    objResultAPI.APIErrorMessage = e.Message;
                }
            }
            return objResultAPI;
        }

        public async Task<EverledgerAPIOutputModel> ShortCodeAndURLByItemNumber(EverledgerAPIInputModel input, Page p)
        {
            EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(5);
                    string requestURL = @"https://api.everledger.io/asset-passport/short-codes-by-certificate";
                    var request = new HttpRequestMessage(HttpMethod.Put, requestURL);
                    request.Headers.Add("x-evl-client-id", p.Session["EvlClientId"].ToString());
                    request.Headers.Add("x-evl-client-secret", p.Session["EvlClientSecret"].ToString());
                   
                    List<string> selectedData = new List<string> { input.ItemNumber };

                    string jsonInput = JsonConvert.SerializeObject(selectedData);

                    var content = new StringContent(jsonInput, null, "application/json");
                    request.Content = content;
                    var response = await client.SendAsync(request);

                    objResultAPI.ItemNumber = input.ItemNumber;

                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var apiResult = await response.Content.ReadAsStringAsync();
                        var jsonToken = JToken.Parse(apiResult);
                        
                        if (jsonToken.Type == JTokenType.Array)
                        {
                            // Process as JArray
                            JArray jsonArray = (JArray)jsonToken;
                            foreach (var jsonItem in jsonArray)
                            {
                                JObject jsonObject = (JObject)jsonItem;
                                if (jsonObject["status"] != null)
                                {
                                    objResultAPI.APIStatus = jsonObject["status"].ToString();  // Status = exists/created/
                                    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;

                                    //if (objResultAPI.APIStatus != "created" || objResultAPI.APIStatus != "exists")
                                    //{
                                    //    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //}
                                }
                                if (jsonObject["url"] != null)
                                {
                                    objResultAPI.URL = jsonObject["url"].ToString();  // Status = exists/created/
                                }
                                if (jsonObject["short_code"] != null)
                                {
                                    objResultAPI.ShortCode = jsonObject["short_code"].ToString();  // Status = exists/created/
                                }
                            }
                        }
                        else if (jsonToken.Type == JTokenType.Object)
                        {
                        }

                    }
                }
                catch (HttpRequestException e)
                {
                    objResultAPI.APIStatus = "Error";
                    objResultAPI.APIErrorMessage = e.Message;
                }
            }
            return objResultAPI;
        }

        public async Task<EverledgerAPIOutputModel> AddGradingReportInformation(EverledgerAPIInputModel input, Page p)
        {
            EverledgerAPIOutputModel objResultAPI = new EverledgerAPIOutputModel();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(5);
                    string requestURL = @"https://api.everledger.io/asset-passport/assets";                
                    var request = new HttpRequestMessage(HttpMethod.Put, requestURL);
                    request.Headers.Add("x-evl-client-id", p.Session["EvlClientId"].ToString());
                    request.Headers.Add("x-evl-client-secret", p.Session["EvlClientSecret"].ToString());

                    object selectedData;

                    selectedData = new
                    {
                        reportNo = input.ItemNumber,
                        color = input.Color,
                        clarity = input.Clarity,
                        weight = input.Weight,
                        cut = input.Cut
                    };

                    string jsonInput = JsonConvert.SerializeObject(selectedData);
                    jsonInput = "[" + jsonInput + "]";

                    var content = new StringContent(jsonInput, null, "application/json");

                    request.Content = content;
                    var response = await client.SendAsync(request);

                    objResultAPI.ItemNumber = input.ItemNumber;

                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var apiResult = await response.Content.ReadAsStringAsync();
                        var jsonToken = JToken.Parse(apiResult);
                        
                        if (jsonToken.Type == JTokenType.Array)
                        {
                            // Process as JArray
                            JArray jsonArray = (JArray)jsonToken;

                            foreach (var jsonItem in jsonArray)
                            {
                                JObject jsonObject = (JObject)jsonItem;

                                if (jsonObject["status"] != null)
                                {
                                    objResultAPI.APIStatus = jsonObject["status"].ToString();  // Status = updated/error
                                    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;

                                    //if (objResultAPI.APIStatus != "updated")
                                    //{
                                    //    objResultAPI.APIErrorMessage = objResultAPI.APIStatus;
                                    //}
                                }

                            }
                        }
                        else if (jsonToken.Type == JTokenType.Object)
                        {

                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    objResultAPI.APIStatus = "Error";
                    objResultAPI.APIErrorMessage = e.Message;
                }

            }
            return objResultAPI;
        }

        public DataTable GetItemPartByFullItem(string fullItemCode, int partTypeID, Page p)
        {
            string strConn;
            if (p.Session["UseRoDb"].ToString() == "1")
                strConn = p.Session["RO_MyIP_ConnectionString"].ToString();
            else
                strConn = p.Session["MyIP_ConnectionString"].ToString();

            var conn = new SqlConnection("" + strConn);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemPartByFullItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@FullItemCode", fullItemCode);
            command.Parameters.AddWithValue("@PartTypeID", partTypeID);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;
        }

        public string SaveMeasureValues(MeasureValueModel measureModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spSetPartValue",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                // Define the output parameter
                SqlParameter outputParam = new SqlParameter("@rId", SqlDbType.VarChar, 150)
                {
                    Direction = ParameterDirection.Output,
                    Value = ""
                };

                command.Parameters.Add(outputParam);
                command.Parameters.Add(new SqlParameter("@ItemCode", measureModel.ItemCode));
                command.Parameters.Add(new SqlParameter("@BatchID", (Int32) measureModel.BatchId));
                command.Parameters.Add(new SqlParameter("@PartID", measureModel.PartId));
                command.Parameters.Add(new SqlParameter("@MeasureCode", measureModel.MeasureCode));

                if(measureModel.MeasureValue == null)
                { 
                    command.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
                    command.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@MeasureValue", measureModel.MeasureValue));
                    command.Parameters.Add(new SqlParameter("@MeasureValueID", measureModel.MeasureValueId));
                }

                command.Parameters.Add(new SqlParameter("@StringValue", measureModel.StringValue));

                command.Parameters.Add(new SqlParameter("@UseClosedRecheckSession", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
                command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
                command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));

                LogUtils.UpdateSpLogWithParameters(command, p);
                command.ExecuteNonQuery();

                // Retrieve the value of the output parameter
                msg = outputParam.Value as string;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            finally
            {
                conn.Close();
            }
            return msg;
        }

    }
}