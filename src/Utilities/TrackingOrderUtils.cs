﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Corpt.Models;

namespace Corpt.Utilities
{
    public class TrackingOrderUtils
    {
        #region Additional Services

        private static DataTable GetFrontDeskEvents(Page p)
        {
            DataTable dt = new DataTable();            
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpGetEvents",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            conn.Close();
            return dt;
        }

        //public static DataSet GetOrderHistory(int orderCode, Page p)
        //{
        //    DataSet ds = new DataSet();
        //    var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
        //    conn.Open();

        //    var command = new SqlCommand
        //    {
        //        CommandText = "trkSpGetOrderHistory",
        //        Connection = conn,
        //        CommandType = CommandType.StoredProcedure,
        //        CommandTimeout = p.Session.Timeout
        //    };

        //    command.Parameters.AddWithValue("@OrderCode", orderCode);
        //    LogUtils.UpdateSpLogWithParameters(command, p);

        //    var da = new SqlDataAdapter(command);
        //    da.Fill(ds);
        //    conn.Close();
        //    return ds;
        //}

        public static DataSet GetAllFrontDeskEvents(Page p)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(GetFrontDeskEvents(p));
            return ds;
        }

        //public static DataSet GetFrontDeskEventsByFrontDeskID(int frontDeskID, Page p)
        //{
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(GetFrontDeskEvents(frontDeskID, p));
        //    return ds;
        //}

        public static bool SetOrderEvent(int orderCode, int eventID, Page p)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpSetOrderEvent",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@OrderCode", orderCode);
            command.Parameters.AddWithValue("@OrderEventID", eventID);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return false;
            }
            conn.Close();
            return true;
        }

        public List<OrderTracking> GetOrderTracking(OrderTracking objOrderTracking,string conn)
        {
            try
            {
                List<OrderTracking> objnewobjOrderTracking = new List<OrderTracking>();
                SqlConnection con = new SqlConnection(conn);
                
                    SqlCommand cmd = new SqlCommand("spGetOrderTracking", con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CustomerID", objOrderTracking.CustomerID);
                    cmd.Parameters.AddWithValue("@TakeinDateFrom", objOrderTracking.TakeinDateFrom);
                    cmd.Parameters.AddWithValue("@TakeinDateTo", objOrderTracking.TakeinDateTo);
                
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var rdrOrderTracking = new OrderTracking()
                    {
                        RequestID = reader["RequestID"].ToString(),
                        RequestDate = Convert.ToString(reader["RequestDate"].ToString()),
                        GSIOrder = reader["OrderCode"].ToString(),
                        TakeinDate = Convert.ToString(reader["TakeinDate"].ToString()),
                        Status = reader["Status"].ToString(),
                        Hours = Convert.ToString(reader["Hours"].ToString()),
                        ServiceTypeName = reader["ServiceTypeName"].ToString(),
                        GiveOutDate = reader["GiveOutDate"].ToString(),
                        PackingDate = reader["PackingDate"].ToString(),
                    };
                    objnewobjOrderTracking.Add(rdrOrderTracking);
                }
                cmd.Dispose();
                return objnewobjOrderTracking;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<OrderTracking> GetOrderTrackingByLocation(OrderTracking objOrderTracking, string conn)
        {
            try
            {
                List<OrderTracking> objnewobjOrderTracking = new List<OrderTracking>();
                using (SqlConnection con = new SqlConnection(conn))
                {   
                    SqlCommand cmd = new SqlCommand("spGetOrderTrackingByLocation", con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if(objOrderTracking.CustomerID == 0)
                        cmd.Parameters.AddWithValue("@CustomerID", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CustomerID", objOrderTracking.CustomerID);

                    cmd.Parameters.AddWithValue("@TakeinDateFrom", objOrderTracking.TakeinDateFrom);
                    cmd.Parameters.AddWithValue("@TakeinDateTo", objOrderTracking.TakeinDateTo);
                    cmd.Parameters.AddWithValue("@AuthorOfficeID", objOrderTracking.AuthorOfficeID);

                    if (objOrderTracking.RealOfficeID == 0)
                        cmd.Parameters.AddWithValue("@RealOfficeID", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@RealOfficeID", objOrderTracking.RealOfficeID);

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    { 
                        var rdrOrderTracking = new OrderTracking()
                        {
                            RequestID = reader["RequestID"].ToString(),
                            RequestDate = Convert.ToString(reader["RequestDate"].ToString()),
                            GSIOrder = reader["OrderCode"].ToString(),
                            TakeinDate = Convert.ToString(reader["TakeinDate"].ToString()),
                            Status = reader["Status"].ToString(),
                            //Hours = Convert.ToString(reader["Hours"].ToString()),
                            ServiceTypeName = reader["ServiceTypeName"].ToString(),
                            GiveOutDate = reader["GiveOutDate"].ToString(),
                            PackingDate = reader["PackingDate"].ToString(),
                            CustomerName = reader["CustomerName"].ToString(),
                            TotalQty = reader["TotalQty"].ToString(),
                            SKUName = reader["SKUName"].ToString(),
                            StyleName = reader["StyleName"].ToString(),
                            RetailerName = reader["RetailerName"].ToString(),
                            SpecialInstructions = reader["SpecialInstructions"].ToString(),
                            OfficeName = reader["OfficeName"].ToString(),
                            TotalWeight = reader["TotalWeight"].ToString(),
                            MeasureUnit = reader["MeasureUnit"].ToString()
                        };
                        objnewobjOrderTracking.Add(rdrOrderTracking);
                    }
                    cmd.Dispose();
                }
                return objnewobjOrderTracking;
            }
            catch (Exception ex)
            {
                //LogUtils.UpdateSpLogWithException(ex.Message,null);
                return null;
            }
        }
        public static DataSet GetOrderHistory(int orderCode,string Conn)
        {
            DataSet ds = new DataSet();
            var conn = new SqlConnection(Conn);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpGetOrderTrackingDetail",
                Connection = conn,
                CommandType = CommandType.StoredProcedure
                //CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@OrderCode", orderCode);
            //LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            conn.Close();
            return ds;
        }
        #endregion
    }
}