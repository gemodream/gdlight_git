﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Corpt.Constants;
using System.Web.UI;

namespace Corpt.Utilities
{
    public class BaseQueryUtils
    {
        public static SqlConnection OpenConnectionGemoDream(Page p)
        {
            var conn = new SqlConnection("" + p.Session[SessionConstants.GlobalConnectionString]);
            conn.Open();
            return conn;
        }
        public static SqlConnection OpenConnectionVv(Page p)
        {
            var conn = new SqlConnection("" + p.Session[SessionConstants.GlobalConnectionStringVv]);
            conn.Open();
            return conn;
        }
        public static SqlConnection OpenConnectionStats(Page p)
        {
            var conn = new SqlConnection("" + p.Session[SessionConstants.GlobalConnectionStringStats]);
            conn.Open();
            return conn;
        }
    }
}