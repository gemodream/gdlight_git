﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Corpt.Models.Stats.FailStats;
using System.Web.UI.WebControls;
using Corpt.TreeModel;
using Corpt.Models;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.ComponentModel;

namespace Corpt.Utilities
{
    public class StatsUtils
    {
        static string ColSku = "SKU";
        static string ColGsiId = "GSI_ID";
        static string ColCreateDate = "Date";
        static string ColStoneRejection = "Stone Rejections";
        #region Build DataTable 
        public static DataTable BuildReportTotalsTable(List<CalcStatsTotalModel> items)
        {
            var tbl = CreateReportTotalsTable();
            foreach (var item in items)
            {
                DataRow row = tbl.NewRow();
                row["CustomerId"] = item.CustomerId;
                row["CustomerName"] = item.CustomerName;
                row["StoneQty"] = item.StoneQty;
                row["PassQty"] = item.PassQty;
                row["PassPercent"] = item.PassPercent;

                row["FailQty"] = item.FailQty;
                row["FailPercent"] = item.FailPercent;
                row["FailDescr"] = item.FailDescr;

                row["MissQty"] = item.MissQty;
                row["MissPercent"] = item.MissPercent;
                row["MissDescr"] = item.MissDescr;

                tbl.Rows.Add(row);
            }
            return tbl;
        }
        private static DataTable CreateReportTotalsTable()
        {
            var tbl = new DataTable();
            tbl.Columns.Add("CustomerId");
            tbl.Columns.Add("CustomerName");
            tbl.Columns.Add("StoneQty");
            tbl.Columns.Add("PassQty");
            tbl.Columns.Add("PassPercent");
            tbl.Columns.Add("FailQty");
            tbl.Columns.Add("FailPercent");
            tbl.Columns.Add("FailDescr");
            tbl.Columns.Add("MissQty");
            tbl.Columns.Add("MissPercent");
            tbl.Columns.Add("MissDescr");
            return tbl;
        }
        public static DataTable BuildDataTable<T>(IList<T> lst)
        {
            //create DataTable Structure
            DataTable tbl = CreateTable<T>();
            Type entType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            //get the list item and add into the list
            foreach (T item in lst)
            {
                DataRow row = tbl.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }
                tbl.Rows.Add(row);
            }
            return tbl;
        }
        private static DataTable CreateTable<T>()
        {
            //T –> ClassName
            Type entType = typeof(T);
            //set the datatable name as class name
            DataTable tbl = new DataTable(entType.Name);
            //get the property list
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (PropertyDescriptor prop in properties)
            {
                //add property as column
                tbl.Columns.Add(prop.Name, prop.PropertyType);
            }
            return tbl;
        }
        #endregion

        public static void ExportToExcel(string reportId, string customerId, Page p)
        {
            //-- Data for Filter PAnel
            var commonData = QueryUtils.GetStatsReportFilterSource(reportId, customerId, p);
            
            //-- Create table
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn(ColSku));
            dt.Columns.Add(new DataColumn(ColGsiId));
            dt.Columns.Add(new DataColumn(ColCreateDate));
            foreach (var reason in commonData.Reasons)
            {
                dt.Columns.Add(new DataColumn(reason.ReasonName));
            }
            dt.Columns.Add(new DataColumn(ColStoneRejection));
            dt.AcceptChanges();
            int pageCount;
            var pageNo = 1;
            var firstItems = QueryUtils.GetStatsReportDetailsForExcel(reportId, customerId, "" + pageNo, out pageCount, p);
            AddItemsForExcel(dt, firstItems);
            for (int i = 2; i <= pageCount; i++)
            {
                var items = QueryUtils.GetStatsReportDetailsForExcel(reportId, customerId, "" + i, out pageCount, p);
                AddItemsForExcel(dt, items);
            }
            var ds = new DataSet();
            ds.Tables.Add(dt.Copy());
            ExcelUtils.ExportThrouPoiStats(ds, "StatsRejection", p);

        }
        private static void AddItemsForExcel(DataTable dt, List<CalcStatsItemModel> items) 
        {
            foreach (var item in items)
            {
                var row = dt.NewRow();
                row[ColSku] = item.CpName;
                row[ColGsiId] = item.FullItemNumber;
                row[ColCreateDate] = item.CreateDate;
                row[ColStoneRejection] = item.RejectionExcDescr;
                var reasons = item.ItemValues.GroupBy(m => m.ReasonName);
                foreach (var reason in reasons)
                {
                    var itemValues = item.ItemValues.FindAll(m => m.ReasonName == reason.Key && !string.IsNullOrEmpty(m.CurrValue));
                    if (itemValues.Count > 0)
                    {
                        var aggr = itemValues.Select(i => i.CurrValue).Aggregate((i, j) => i + ", " + j);
                        row[reason.Key] = aggr;
                    }
                }
                dt.Rows.Add(row); ;
            }
            dt.AcceptChanges();
        }
        public static DataTable CreateTableStruct(List<string> additional, List<CalcStatsItemModel> items)
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn(ColSku));
            dt.Columns.Add(new DataColumn(ColGsiId));
            dt.Columns.Add(new DataColumn(ColCreateDate));
            foreach (var col in additional)
            {
                dt.Columns.Add(new DataColumn(col));
            }
            dt.Columns.Add(new DataColumn(ColStoneRejection));
            dt.AcceptChanges();
            foreach (var item in items)
            {
                var row = dt.NewRow();
                row[ColSku] = item.CpName;
                row[ColGsiId] = item.FullItemNumber;
                row[ColCreateDate] = item.CreateDate;
                row[ColStoneRejection] = item.RejectionDescr;
                var reasons = item.ItemValues.GroupBy(m => m.ReasonName);
                foreach (var reason in reasons)
                {
                    var itemValues = item.ItemValues.FindAll(m => m.ReasonName == reason.Key && !string.IsNullOrEmpty(m.CurrValue));
                    if (itemValues.Count > 0)
                    {
                        var aggr = itemValues.Select(i => i.CurrValue).Aggregate((i, j) => i + ", " + j);
                        row[reason.Key] = aggr;
                    }
                }
                dt.Rows.Add(row);;
            }
            dt.AcceptChanges();
            //-- Remove empty columns
            for (var c = dt.Columns.Count - 1; c > 0; c--)
            {
                var dataColumn = dt.Columns[c];
                var empty = true;
                for (var r = 0; r < dt.Rows.Count; r++)
                {
                    var val = ("" + dt.Rows[r][c]).Trim();
                    val = Regex.Replace(val, @"<[^>]+>", "");
                    if (string.IsNullOrEmpty(val)) continue;
                    empty = false;
                    break;
                }
                if (empty)
                {
                    dt.Columns.Remove(dataColumn);
                }
            }
            dt.AcceptChanges();
            return dt;
        }
        public static List<string> GetReasonColNames(List<CalcStatsItemValueModel> itemValues)
        {
            var groups = itemValues.GroupBy(m => m.ItemId + ";" + m.ReasonName).Select(lst => new { UniqKey = lst.Key, Count = lst.Count() });
            foreach (var group in groups)
            {
                var itemId = group.UniqKey.Split(';')[0];
                var reason = group.UniqKey.Split(';')[1];
                var values = itemValues.FindAll(m => m.ItemId == itemId && m.ReasonName == reason).ToArray();
                
                
                for (int i = 0; i < values.Length; i++)
                {
                    values[i].ColumnTable = reason;// (i == 0 ? reason : reason + i);
                }
                
            }
            var columns = itemValues.GroupBy(m => m.ColumnTable).OrderBy(g => g.Key);
            var result = new List<string>();
            foreach (var column in columns)
            {
                result.Add(column.Key);
            }
            return result;
        }
        public static void StatsRuleTreeBuild(TreeView tree, bool expandAll, CommonPage p)
        {
            var reasons = QueryUtils.GetRejectionReasons(p);
            List<MeasureModel> measures;
            var rules = QueryUtils.GetReasonMeasureRules(out measures, p);
            tree.Nodes.Clear();

            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Report Struct" } };
            foreach (var reason in reasons)
            {
                var linkMeasures = rules.FindAll(m => m.ReasonId == reason.ReasonId && !string.IsNullOrEmpty(m.Id));
                if (linkMeasures.Count == 0) continue;

                data.Add(new TreeViewModel { ParentId = "0", Id = reason.ReasonId, DisplayName = reason.ReasonName });
                foreach (var linkMeasure in linkMeasures)
                {
                    data.Add(new TreeViewModel { ParentId = reason.ReasonId, Id = reason.ReasonId + "_" + linkMeasure.MeasureId, DisplayName = linkMeasure.MeasureName });
                }
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            tree.Nodes.Add(rootNode);
            if (expandAll) rootNode.ExpandAll();
            else rootNode.Expand();
        }
        public static string CalcPercent(int all, int part)
        {
            if (all == 0) return "";
            return ((float)(part) / all).ToString("0.00%");
        }
        public static float CalcPercentNum(int all, int part)
        {
            if (all == 0) return 100;
            return ((float)(part) / all);
        }
    }
}