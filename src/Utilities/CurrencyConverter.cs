﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Xml;

namespace Corpt.Utilities
{
    public class CurrencyConverter
    {
        //This function returns how many units of the given currency are obtainable today for 1 Euro 
        //For example, if 1 Euro = 1.17 US dollars, Rate.getRate(Rate.USD) returns 1.17
        //Private because being based on Euro instead of USD is not obvious
        private static decimal getRate(String curr, Page p)
        {
            if (curr.Equals("EUR"))
            {
                return 1;
            }
            string useXML = p.Session["CurrencyConverterXML"].ToString();
            if (useXML == "1")
            {
                var ds = new DataSet();

                ds.ReadXml(p.Server.MapPath("CurrencyConverter.xml"));
                string select = @"currency = '" + curr + @"'";
                DataRow[] drWCF = ds.Tables[0].Select(select);
                foreach (DataRow dr in drWCF)
                {
                    string drCurr = dr["Currency"].ToString();
                    if (drCurr == curr)
                    {
                        return Decimal.Parse(dr["Rate"].ToString());
                    }
                }
            }
            else
            {
                String urlString = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
                using (XmlTextReader reader = new XmlTextReader(urlString))
                {
                    while (reader.Read())
                    {
                        while (reader.MoveToNextAttribute())
                        {
                            if (reader.Value.Equals(curr))
                            {
                                reader.MoveToNextAttribute();
                                return Decimal.Parse(reader.Value);
                            }
                        }
                    }
                }
            }
            return -1;
        }

        //This function returns the number of units of currency 1 obtainable for 1 unit of currency 2
        //For example, if 1 USD = 1.3 AUD, Rate.getRate(Rate.AUD, Rate.USD) returns 1.3
        public static decimal getRate(String curr1, String curr2, Page p)
        {
            return getRate(curr1, p) / getRate(curr2, p);
        }
        //This function returns the converted price from currency 2 to currency 1
        public static decimal getRate(String curr1, String curr2, decimal price, Page p)
        {
            return price * getRate(curr1, curr2, p);
        }
    }
}