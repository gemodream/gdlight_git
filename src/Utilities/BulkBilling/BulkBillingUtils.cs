﻿using System.Collections.Generic;

namespace Corpt.Utilities.BulkBilling
{
    public class BulkBillingUtils
    {
        private static readonly HashSet<int> RepackServiceTypes = new HashSet<int>() { 3, 4 };

        public static bool IsRepack(int? synthServiceTypeCode)
        {
            if (synthServiceTypeCode == null)
            {
                return false;
            }
            return RepackServiceTypes.Contains((int)synthServiceTypeCode);
        }

        public static bool IsRepack(List<int> synthServiceTypeCode)
        {
            if (synthServiceTypeCode == null)
            {
                return false;
            }
            var serviceTypes = new HashSet<int>(synthServiceTypeCode);
            return RepackServiceTypes.SetEquals(serviceTypes);
        }
    }
}