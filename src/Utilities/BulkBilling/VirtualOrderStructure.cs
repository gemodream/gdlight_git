﻿using System.Collections.Generic;
using System.Linq;
using Corpt.AccountRepModels;

namespace Corpt.Utilities.BulkBilling
{
    /// <summary>
    /// Define criteria of virtual order structure which accepted for Bulk Billing processing.
    /// Today (Oct 28 2022) the following rule is accepted:
    /// For each record in tblSyntheticCustomerEntries - table of customer requests -
    /// the following virtual order is created:
    /// 
    /// Order 
    ///     Batch, BatchCode = 1
    ///         Item1, Item Code = 1 
    ///         Item2, Item Code = 2
    /// 
    /// Item1 is used for creation of "Pass" invoice item(for Screening)  or "Repack" invoice item(for Repack)
    /// Item2 is used for creation of "Reject" invoice item(for Screening)
    /// </summary>
    public class VirtualOrderStructure
    {

        /// <summary>
        /// Determines whether the specified "order item" is suitable for "Pass" or "Repack" "invoice item" creation.
        /// </summary>
        /// <param name="itemModel">The item model.</param>
        public static bool IsItem1(TreeNodeItemModel itemModel)
        {
            return itemModel.BatchCode == 1 && itemModel.ItemCode == 1;
        }

        /// <summary>
        /// Determines whether the specified "order item" is suitable for "Reject" "invoice item" creation.
        /// </summary>
        /// <param name="itemModel">The item model.</param>
        public static bool IsItem2(TreeNodeItemModel itemModel)
        {
            return itemModel.BatchCode == 1 && itemModel.ItemCode == 2;
        }

        /// <summary>
        /// Gets the "order item" for "Pass" or "Repack" "invoice item".
        /// </summary>
        /// <param name="orderItems">The order items.</param>
        /// <returns></returns>
        public static TreeNodeItemModel GetItemForInvoiceItem1(List<TreeNodeItemModel> orderItems)
        {
            return orderItems.FirstOrDefault(IsItem1);
        }

        /// <summary>
        /// Gets the "order item" for "Reject" "invoice item".
        /// </summary>
        /// <param name="orderItems">The order items.</param>
        /// <returns></returns>
        public static TreeNodeItemModel GetItemForInvoiceItem2(List<TreeNodeItemModel> orderItems)
        {
            return orderItems.FirstOrDefault(IsItem2);
        }

        public static string GetFullItemCode1(int orderCode)
        {
            return Utils.FullItemNumberWithDots(orderCode, 1, 1);
        }

        public static string GetFullItemCode2(int orderCode)
        {
            return Utils.FullItemNumberWithDots(orderCode, 1, 2);
        }
    }
}