﻿using System;
using System.Collections.Generic;
using System.Linq;
using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;

namespace Corpt.Utilities.BulkBilling
{
    [Serializable]
    public class BulkBillingPriceManager
    {
        private readonly List<PriceSetModel> _pricesSets;

        public BulkBillingPriceManager()
        {
        }

        public BulkBillingPriceManager(List<PriceSetModel> pricesSets)
        {
            _pricesSets = pricesSets;
        }

        public PriceSetModel GetPrices(TreeNodeGroupModel groupModel)
        {
            return _pricesSets.First(priceSetModel =>
                (priceSetModel.HasCustomerRequest && groupModel.HasCustomerRequest ||
                 !priceSetModel.HasCustomerRequest && !groupModel.HasCustomerRequest) &&
                (priceSetModel.SynthServiceTypeIds == null || groupModel.SynthServiceTypeCode != null &&
                    priceSetModel.SynthServiceTypeIds.Contains((int) groupModel.SynthServiceTypeCode)) &&
                (priceSetModel.ServiceTypeIds == null || groupModel.ServiceTypeId != null &&
                    priceSetModel.ServiceTypeIds.Contains((int)groupModel.ServiceTypeId)) &&
                (priceSetModel.CustomerCodes == null || priceSetModel.CustomerCodes.Contains(groupModel.CustomerCode)) &&
                (priceSetModel.RetailerIds == null || priceSetModel.RetailerIds.Contains(groupModel.RetailerId))
            );
        }
    }
}