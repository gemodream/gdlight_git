﻿using System;
using System.Collections.Generic;
using System.Data;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager;
using Corpt.Utilities.BulkBilling.CostManager.Models;
using GemoDream.Database;

namespace Corpt.Utilities.BulkBilling
{
    [Serializable]
    public class PriceSetModel
    {
        public PriceSetModel()
        {
        }

        public PriceSetModel(DataRow row)
        {
            Id = row.ConvertToInt("Id");
            ShortDescription = row.ConvertToString("ShortDescription");
            Priority = row.ConvertToInt("Priority");
            HasCustomerRequest = row.ConvertToBool("HasCustomerRequest");
            SynthServiceTypeIds = row.ConvertToListInt("SynthServiceTypeIds");
            ServiceTypeIds = row.ConvertToListInt("ServiceTypeIds");
            CustomerCodes = row.ConvertToListInt("CustomerCodes");
            RetailerIds = row.ConvertToListInt("RetailerIds");
            QuestionText = row.ConvertToString("QuestionText");

            IsRepack = BulkBillingUtils.IsRepack(SynthServiceTypeIds);

            if (!HasCustomerRequest)
            {
                Pricing = SinglePricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                DefaultPricing = SinglePricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                SpecialPricing = SinglePricingModel.GetInstance(row.ConvertToString("SpecialPrices"));
            }
            else if (IsRepack)
            {
                Pricing = SinglePricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                DefaultPricing = SinglePricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                SpecialPricing = SinglePricingModel.GetInstance(row.ConvertToString("SpecialPrices"));
            }
            else
            {
                Pricing = RegularPricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                DefaultPricing = RegularPricingModel.GetInstance(row.ConvertToString("DefaultPrices"));
                SpecialPricing = RegularPricingModel.GetInstance(row.ConvertToString("SpecialPrices"));
            }

            NoAnswerText = row.ConvertToString("NoAnswerText");
            YesAnswerText = row.ConvertToString("YesAnswerText");
        }

        public bool HasCustomerRequest { get; set; }

        public bool IsRepack { get; set; }

        public List<int> SynthServiceTypeIds { get; set; }

        public string YesAnswerText { get; set; }

        public string NoAnswerText { get; set; }

        public int Priority { get; set; }

        public string ShortDescription { get; set; }

        public int Id { get; set; }

        public List<int> ServiceTypeIds { get; set; }

        public List<int> CustomerCodes { get; set; }

        public List<int> RetailerIds { get; set; }

        public string QuestionText { get; set; }

        public PricingModel Pricing { get; set; }

        public PricingModel DefaultPricing { get; set; }

        public PricingModel SpecialPricing { get; set; }

        public override string ToString()
        {
            return ShortDescription + " - " + Pricing;
        }
    }
}