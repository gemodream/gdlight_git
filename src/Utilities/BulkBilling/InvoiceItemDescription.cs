﻿using System;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Utilities.BulkBilling
{
    public class InvoiceItemDescription
    {
        public const int BlueNileRetailerId = 1;
        public const int HelzbergRetailerId = 5;
        public const int MarksJewelersRetailerId = 34;

        private readonly PricesGridViewItemModel _pricesGridViewItemModel;
        private readonly string _costName;
        private readonly string _serviceTypeName;
        private readonly int _groupCode;
        private readonly string _customerName;
        private readonly QuantitiesModel _quantitiesModel;
        private readonly int? _serviceTypeId;

        public InvoiceItemDescription(string costName,
            PricesGridViewItemModel pricesGridViewItemModel,
            int? serviceTypeId,
            string serviceTypeName,
            int groupCode, string customerName,
            QuantitiesModel quantitiesModel)
        {
            this._pricesGridViewItemModel = pricesGridViewItemModel;
            this._costName = costName;
            this._serviceTypeName = serviceTypeName;
            this._groupCode = groupCode;
            this._customerName = customerName;
            this._quantitiesModel = quantitiesModel;
            _serviceTypeId = serviceTypeId;
        }

        public string GetDescription()
        {
            var description =
                $"{GetCostDetailName()}" +
                $"Order {_groupCode}, {_serviceTypeName}" +
                ", " + 
                GetQuantityDescription() +
                GetTail();
            return description;
        }

        public string GetQuantityDescription()
        {
            if (_costName == "Reject" && _quantitiesModel is RegularQuantitiesModel quantitiesModel)
            {
                var rejectPieces = quantitiesModel.RejectPieces;
                var rejectStones = quantitiesModel.RejectStones;

                if (_serviceTypeId == 7) //(Screening)
                {

                    var piecesDescription = $"{rejectPieces} piece" + (rejectPieces > 1 ? "s" : "");

                    if (rejectStones == 0)
                    {
                        return piecesDescription;
                    }

                    return piecesDescription + " - " +
                           $"{rejectStones} stone" + (rejectStones > 1 ? "s" : "");
                }

                if (_serviceTypeId == 12 || _serviceTypeId == 13) //(QC or QC/ Screening)
                {
                    return $"{rejectPieces} piece" + (rejectPieces > 1 ? "s" : "");
                }

                return "Unexpected service type";
            }

            int passPieces = _quantitiesModel.Total;
            if (_quantitiesModel is RegularQuantitiesModel regularQuantitiesModel)
            {
                passPieces = regularQuantitiesModel.PassPieces;
            }

            return $"{passPieces} piece" + (passPieces > 1 ? "s" : "");
        }

        private string GetTail()
        {
            if (_pricesGridViewItemModel.RetailerId == BlueNileRetailerId)
            {
                return $", {_pricesGridViewItemModel.RetailerName} {GetAnswerText()}".ToUpper();
            }

            if (_pricesGridViewItemModel.RetailerId == HelzbergRetailerId)
            {
                return ", HDS Gemstone and Diamond";
            }

            if (_pricesGridViewItemModel.RetailerId == MarksJewelersRetailerId)
            {
                return $", {GetCustomerName()}";
            }


            return string.Empty;
        }
        private string GetCustomerName()
        {
            var indexOf = _customerName.IndexOf(", #", StringComparison.Ordinal);
            if (indexOf >= 0)
                return _customerName.Substring(0, indexOf);

            return _customerName;
        }

        private string GetAnswerText() =>
            _pricesGridViewItemModel.IsSatisfyACondition
                ? _pricesGridViewItemModel.YesAnswerText ?? ""
                : _pricesGridViewItemModel.NoAnswerText ?? "";

        private string GetCostDetailName() =>
            _costName == "Reject"
                ? "Rejection, "
                : _costName == "Repack"
                    ? _pricesGridViewItemModel.SynthServiceTypeName + ", "
                    : string.Empty;
    }
}