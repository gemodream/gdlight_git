﻿using System.Linq;
using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Utilities.BulkBilling.CostManager
{
    internal class RegularBulkBillingCostManager : BulkBillingCostManager
    {
        private const int SdcDesignsCustomerCode = 1174;

        public RegularBulkBillingCostManager(TreeNodeGroupModel groupModel, QuantitiesData orderQuantities, PriceSetModel priceSet, AlreadyBilledOrderData alreadyBilledOrderData) 
            : base(groupModel, orderQuantities, priceSet, alreadyBilledOrderData)
        {
        }

        public override QuantitiesModel GetQuantities()
        {
            if (OrderQuantities != null)
            {
                var quantityPassed = GetQuantityPassed();
                var quantityRejected = GetQuantityRejected();
                if (quantityPassed != 0 || quantityRejected != 0)
                {
                    return new RegularQuantitiesModel(
                        quantityPassed, 
                        quantityRejected, 
                        GetPassPieces(), 
                        GetRejectPieces(), 
                        GetRejectStones(), 
                        OrderQuantities.TotalQuantity, 
                        QuantitySource.Gd2K);
                }
            }
            
            if (IsAlreadyBilled)
            {
                // if there is no quantity data in tblQCOrders then try to take it from tblSyntheticCustomerEntries
                return new SingleQuantitiesModel(GroupModel.SynthTotalQty, QuantitySource.Request);
            }

            return null;
        }

        protected int GetQuantityPassed()
        {
            if (GroupModel.ServiceTypeId == 7) //(Screening)
            {
                var quantityPass = OrderQuantities.QuantityPass;
                if (OrderQuantities.ItemsNumberWithBrokenFaults > 0)
                {
                    quantityPass += OrderQuantities.ItemsNumberWithBrokenFaults;
                }

                var itemsWithNotRejectingFaults = OrderQuantities.ItemsNumberWithAnyFaults - (OrderQuantities.ItemsNumberWithBrokenFaults + OrderQuantities.ItemsNumberWithSynthFaults);
                if (itemsWithNotRejectingFaults > 0)
                {
                    quantityPass += itemsWithNotRejectingFaults;
                }

                return quantityPass;
            }
            return OrderQuantities.QuantityPass;
        }
        protected int GetQuantityRejected()
        {
            if (GroupModel.ServiceTypeId == 7) //(Screening)
            {
                // Screening Reg - Synth per Stone
                return GetScreeningRejectedQuantity();
            }

            if (new int?[] { 12, 13 }.Contains(GroupModel.ServiceTypeId)) //(QC or QC/ Screening)
            {
                // QC - Synth per Item
                return OrderQuantities.QuantityFail;
            }

            return 0;
        }

        private int GetScreeningRejectedQuantity()
        {
            if (OrderQuantities.VendorId == SdcDesignsCustomerCode)
            {
                var rejectedQuantity = OrderQuantities.QuantityFail;

                var itemsWithNotRejectingFaults = OrderQuantities.ItemsNumberWithAnyFaults - (OrderQuantities.ItemsNumberWithBrokenFaults + OrderQuantities.ItemsNumberWithSynthFaults);
                if (itemsWithNotRejectingFaults > 0)
                {
                    if (rejectedQuantity >= itemsWithNotRejectingFaults)
                    {
                        rejectedQuantity -= itemsWithNotRejectingFaults;
                    } 
                }
                return rejectedQuantity;
            }

            return OrderQuantities.Synthetics + OrderQuantities.SyntheticsGems;
        }

        private int GetPassPieces()
        {
            return OrderQuantities.QuantityPass;
        }

        private int GetRejectPieces()
        {
            return OrderQuantities.QuantityFail;
        }

        private int GetRejectStones()
        {
            if (GroupModel.ServiceTypeId == 7) //(Screening)
            {
                // Screening Reg - Synth per Stone
                return GetScreeningRejectedQuantity();
            }

            if (new int?[] { 12, 13 }.Contains(GroupModel.ServiceTypeId)) //(QC or QC/ Screening)
            {
                // QC - Synth per Item
                return 0;
            }

            return 0;
        }

    }
}