﻿using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Utilities.BulkBilling.CostManager
{
    internal class NoRequestBulkBillingCostManager : BulkBillingCostManager
    {
        public NoRequestBulkBillingCostManager(TreeNodeGroupModel groupModel, QuantitiesData orderQuantities, PriceSetModel priceSet, AlreadyBilledOrderData alreadyBilledOrderData) 
            : base(groupModel, orderQuantities, priceSet, alreadyBilledOrderData)
        {
        }

        public override QuantitiesModel GetQuantities()
        {
            return GroupModel.NumberOfUnit != null 
                ? new SingleQuantitiesModel(GroupModel.NumberOfUnit.Value, QuantitySource.Measure)
                : null;
        }
    }
}