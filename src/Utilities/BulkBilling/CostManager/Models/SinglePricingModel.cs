﻿using System;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    [Serializable]
    public class SinglePricingModel : PricingModel
    {
        public static SinglePricingModel GetInstance(string price)
        {
            return new SinglePricingModel(price == null ? 0.00 : double.Parse(price));
        }

        public SinglePricingModel()
        {
        }

        public SinglePricingModel(double total)
        {
            Total = total;
        }

        public double Total { get; set; }

        public override string ToString()
        {
            return $"{Total:C}";
        }

    }
}