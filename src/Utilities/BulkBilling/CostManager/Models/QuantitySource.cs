﻿using System.ComponentModel;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    public enum QuantitySource
    {

        /// <summary>
        /// Source: Customer Request, Total quantity
        /// </summary>
        [Description("Customer Request, Total quantity")]
        Request,

        /// <summary>
        /// Source: Quantity entered by GD2K app
        /// </summary>
        [Description("Quantity entered by GD2K app")]
        Gd2K,

        /// <summary>
        /// Source: Quantity entered with Order creation in '# of Units' measure
        /// </summary>
        [Description("Quantity entered with Order creation in '# of Units' measure")]
        Measure


    }
}