﻿using System;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    [Serializable]
    public class RegularQuantitiesModel : QuantitiesModel
    {
        /// <summary>
        /// Gets the passed invoice item "pieces" quantity for description.
        /// </summary>
        public int PassPieces { get; }

        /// <summary>
        /// Gets the rejected invoice item "pieces" quantity for description.
        /// </summary>
        public int RejectPieces { get; }

        /// <summary>
        /// Gets the rejected invoice item "stones" quantity for description.
        /// </summary>
        public int RejectStones { get; }

        public RegularQuantitiesModel()
        {
        }

        public RegularQuantitiesModel(int pass, int reject,
            int passPieces, int rejectPieces, int rejectStones,
            int total,
            QuantitySource quantitySource)
        {
            PassPieces = passPieces;
            Pass = pass;
            Reject = reject;
            Total = total;
            QuantitySource = quantitySource;
            RejectPieces = rejectPieces;
            RejectStones = rejectStones;
        }

        public override string ToString()
        {
            return $"{Pass:D} / {Reject:D}";
        }
    }
}