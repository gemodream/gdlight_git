﻿using System;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    [Serializable]
    public class SingleQuantitiesModel : QuantitiesModel
    {
        public SingleQuantitiesModel()
        {
        }

        public SingleQuantitiesModel(int total, QuantitySource quantitySource)
        {
            Total = total;
            QuantitySource = quantitySource;
        }

        public override string ToString()
        {
            return $"{Total:D}";
        }

    }
}