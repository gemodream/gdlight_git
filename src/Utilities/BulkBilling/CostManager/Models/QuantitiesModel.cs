﻿using System;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    [Serializable]
    public class QuantitiesModel
    {
        // ReSharper disable once EmptyConstructor
        public QuantitiesModel()
        {
        }

        /// <summary>
        /// Gets or sets the total quantity for calculate cost for has not customer request invoices and for repack invoices.
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Gets or sets the passed quantities for calculate passed cost for Regular invoices.
        /// </summary>
        public int Pass { get; set; }

        /// <summary>
        /// Gets or sets the rejected quantities for calculate rejected cost for Regular invoices.
        /// </summary>
        public int Reject { get; set; }

        public QuantitySource QuantitySource { get; set; }

    }
}