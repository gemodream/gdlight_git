﻿using System;
using System.Linq;

namespace Corpt.Utilities.BulkBilling.CostManager.Models
{
    [Serializable]
    public class RegularPricingModel : PricingModel
    {
        public static RegularPricingModel GetInstance(string commaSeparatedList)
        {
            if (commaSeparatedList == null)
            {
                return null;
            }
            var list = commaSeparatedList.Split(',').Select(double.Parse).ToList();
            switch (list.Count)
            {
                case 1:
                    return new RegularPricingModel(list[0], null);
                case 2:
                    return new RegularPricingModel(list[0], list[1]);
            }

            throw new Exception(
                $"Unexpected number of elements(={list.Count}) in comma separated list(1or 2 supported)");
        }

        public RegularPricingModel()
        {
        }

        public RegularPricingModel(double? pass, double? reject)
        {
            Pass = pass;
            Reject = reject;
        }

        public double? Pass { get; set; }

        public double? Reject { get; set; }

        public override string ToString()
        {
            return Reject != null
                ? $"{Pass:C} / {Reject:C}"
                : Pass != null
                    ? $"{Pass:C}"
                    : @"Unsupported";
        }

    }
}