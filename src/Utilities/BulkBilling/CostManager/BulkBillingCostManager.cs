﻿using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Utilities.BulkBilling.CostManager
{
    internal abstract class BulkBillingCostManager
    {
        public static BulkBillingCostManager GetInstance(
            TreeNodeGroupModel groupModel,
            QuantitiesData orderQuantities,
            PriceSetModel priceSet,
            AlreadyBilledOrderData alreadyBilledOrderData)
        {
            if (!groupModel.HasCustomerRequest)
            {
                return new NoRequestBulkBillingCostManager(groupModel, orderQuantities, priceSet, alreadyBilledOrderData);
            }

            if (BulkBillingUtils.IsRepack(groupModel.SynthServiceTypeCode))
            {
                return new RepackBulkBillingCostManager(groupModel, orderQuantities, priceSet, alreadyBilledOrderData);
            }

            return new RegularBulkBillingCostManager(groupModel, orderQuantities, priceSet, alreadyBilledOrderData);
        }

        protected readonly QuantitiesData OrderQuantities;
        protected readonly TreeNodeGroupModel GroupModel;
        protected readonly PriceSetModel PriceSet;
        protected readonly AlreadyBilledOrderData AlreadyBilledOrderData;

        protected BulkBillingCostManager(TreeNodeGroupModel groupModel,
            QuantitiesData orderQuantities,
            PriceSetModel priceSet, AlreadyBilledOrderData alreadyBilledOrderData)
        {
            OrderQuantities = orderQuantities;
            GroupModel = groupModel;
            PriceSet = priceSet;
            AlreadyBilledOrderData = alreadyBilledOrderData;
        }

        public bool IsAlreadyBilled => AlreadyBilledOrderData != null && AlreadyBilledOrderData.IsAlreadyBilled;

        public bool IsRepack => BulkBillingUtils.IsRepack(GroupModel.SynthServiceTypeCode);

        public abstract QuantitiesModel GetQuantities();

        public PricingModel GetDefaultCosts()
        {
            return GetCosts(PriceSet.DefaultPricing, GetQuantities());
        }

        public PricingModel GetSpecialCosts()
        {
            return GetCosts(PriceSet.SpecialPricing, GetQuantities());
        }
        public PricingModel GetCosts()
        {
            return IsAlreadyBilled ? AlreadyBilledOrderData.Cost : GetDefaultCosts();
        }
        public PricingModel GetInvoiceCosts()
        {
            return AlreadyBilledOrderData?.Cost;
        }

        public PricingModel GetCosts(PricingModel pricing, QuantitiesModel quantities)
        {
            if (quantities == null || pricing == null)
            {
                return null;
            }

            if (!GroupModel.HasCustomerRequest)
            {
                return new SinglePricingModel(((SinglePricingModel)pricing).Total * quantities.Total);
            }

            if (IsRepack)
            {
                return new SinglePricingModel( ((SinglePricingModel)pricing).Total * quantities.Total );
            }

            var passPrice = ((RegularPricingModel)pricing).Pass;
            var rejectPrice = ((RegularPricingModel)pricing).Reject;

            var passQty = quantities.Pass;
            var rejectQty = quantities.Reject;

            if ( passPrice.HasValue && rejectPrice.HasValue)
            {
                return new RegularPricingModel(passPrice.Value * passQty, rejectPrice.Value * rejectQty);
            }

            return new PricingModel();
        }
    }
}