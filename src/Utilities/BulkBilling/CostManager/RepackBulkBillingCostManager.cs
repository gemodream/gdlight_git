﻿using Corpt.AccountRepModels;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Utilities.BulkBilling.CostManager.Models;

namespace Corpt.Utilities.BulkBilling.CostManager
{
    internal class RepackBulkBillingCostManager : BulkBillingCostManager
    {
        public RepackBulkBillingCostManager(TreeNodeGroupModel groupModel, QuantitiesData orderQuantities, PriceSetModel priceSet, AlreadyBilledOrderData alreadyBilledOrderData) 
            : base(groupModel, orderQuantities, priceSet, alreadyBilledOrderData)
        {
        }

        public override QuantitiesModel GetQuantities()
        {
            return new SingleQuantitiesModel(GroupModel.SynthTotalQty, QuantitySource.Request);
        }
    }
}