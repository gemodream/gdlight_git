﻿using System;
using System.Collections.Generic;
using System.Linq;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Models.AccountRepresentative.UI;

namespace Corpt.Utilities.AccountRepresentative
{
    public class TimeZoneManager
    {
        private readonly List<OfficeTimeZone> _officeTimeZones;
        private readonly object _timeZoneOffsetMinutesObject;

        public TimeZoneManager(object timeZoneOffsetMinutesObject) : 
            this(timeZoneOffsetMinutesObject, OfficeTimeZones)
        {
        }

        public TimeZoneManager(object timeZoneOffsetMinutesObject, List<OfficeTimeZone> officeTimeZones)
        {
            _officeTimeZones = officeTimeZones;
            _timeZoneOffsetMinutesObject = timeZoneOffsetMinutesObject;
        }

        public string ApplyTimeZoneOffset(DateTime dateTime, int authorOfficeId,
            DisplayTimeZoneModes displayTimeZoneMode)
        {
            if (displayTimeZoneMode.Equals(DisplayTimeZoneModes.Client))
            {
                if (_timeZoneOffsetMinutesObject is int timeZoneOffsetMinutes)
                {
                    var applied = dateTime.AddMinutes(-timeZoneOffsetMinutes);
                    return applied.ToString("MM-dd-yyyy hh:mm tt");
                }

                return dateTime.ToString("MM-dd-yyyy hh:mm tt");
            }

            if (displayTimeZoneMode.Equals(DisplayTimeZoneModes.UTC))
            {
                return dateTime.ToString("MM-dd-yyyy hh:mm tt UTC");
            }

            if (displayTimeZoneMode.Equals(DisplayTimeZoneModes.ByOffice))
            {
                var timeZone = _officeTimeZones.FirstOrDefault(zone => zone.OfficeId == authorOfficeId);
                if (timeZone != null)
                {
                    var applied = dateTime.AddMinutes(timeZone.OffsetMin);
                    return applied.ToString($"MM-dd-yyyy hh:mm tt ({timeZone.OffsetDisplay} {timeZone.TimeZoneAbbr})");
                }

                return dateTime.ToString($"MM-dd-yyyy hh:mm tt ({authorOfficeId}?)");
            }

            throw new Exception($"Unexpected displayTimeZoneMode={displayTimeZoneMode}");
        }

        private static List<OfficeTimeZone> OfficeTimeZones
        {
            get
            {
                var officeTimeZones = new List<OfficeTimeZone>
                {
                    new OfficeTimeZone(1, -5 * 60, "UTC-05", "EST", "Eastern Standard Time"),
                    new OfficeTimeZone(2, (int)5.5 * 60, "UTC+05:30", "IST", "India Standard Time"),
                    new OfficeTimeZone(4, 2 * 60, "UTC+02", "IST", "Israel Standard Time"),
                    new OfficeTimeZone(5, 2 * 60, "UTC+01", "CET", "Central European Time"),
                    new OfficeTimeZone(6, (int)5.5 * 60, "UTC+05:30", "IST", "India Standard Time"),
                    new OfficeTimeZone(7, 2 * 60, "UTC+02", "CAT", "Central Africa Time"),
                    new OfficeTimeZone(10, (int)5.5 * 60, "UTC+05:30", "IST", "India Standard Time"),
                    new OfficeTimeZone(11, 4 * 60, "UTC+04", "GST", "Gulf Standard Time"),
                    new OfficeTimeZone(12, 8 * 60, "UTC+08", "HKT", "Hong Kong Time"),
                    new OfficeTimeZone(13, (int)5.5 * 60, "UTC+05:30", "IST", "India Standard Time")
                };
                return officeTimeZones;
            }
        }
    }
}