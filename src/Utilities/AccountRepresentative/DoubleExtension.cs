﻿using System;

namespace Corpt.Utilities.AccountRepresentative
{
    public static class DoubleExtension
    {
        public static double ToDouble(this double? nullable)
        {
            if (nullable == null)
            {
                throw new Exception("Nullable value = null is net expected");
            }

            return (double) nullable;
        }
    }
}