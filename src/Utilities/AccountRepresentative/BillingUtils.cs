﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.SessionState;
using System.Xml;
using System.Xml.Serialization;
using Corpt.Models.AccountRepresentative.Dto;
using Corpt.Models.AccountRepresentative.UI;

namespace Corpt.Utilities.AccountRepresentative
{
    public class BillingUtils
    {
        private const string ItemIsAlreadyBilled = "Item is already billed";

        public static List<AdditionalServiceRow> GetAdditionalServicesRows(
            IEnumerable<AdditionalServiceModel> additionalServices,
            IEnumerable<BatchShortModel> batchModels) =>
            additionalServices.Join(batchModels, s => s.BatchId, b => b.BatchId,
                (s, b) => new AdditionalServiceRow
                {
                    Checked = true,
                    Name = s.Name,
                    OrderCode = b.GroupCode,
                    BatchCode = b.BatchCode,
                    AdditionalServiceId = s.AdditionalServiceId,
                    BatchId = s.BatchId,
                    Price = s.Price,
                    PriceId = s.PriceId,
                    UserId = s.UserId
                }).ToList();

        public static void SaveAdditionalServices(
            List<AdditionalServiceRow> additionalServicesRows, HttpSessionState session)
        {
            if (additionalServicesRows == null) return;

            var orderCodes = additionalServicesRows.GroupBy(row => row.OrderCode).Select(grouping => grouping.Key)
                .ToList();
            foreach (var orderCode in orderCodes)
            {
                var allServices = new AllServices
                {
                    Services = additionalServicesRows
                        .Where(row => row.Checked && row.OrderCode == orderCode)
                        .Select(row =>
                            new AdditionalService
                            {
                                Col0 = row.AdditionalServiceId,
                                Col1 = row.Name,
                                Col2 = row.BatchId,
                                Col3 = row.BatchCode,
                                Col4 = row.Price,
                                Col5 = row.PriceId,
                                Col6 = row.UserId
                            }).ToList()
                };

                var emptyNamespaces = new XmlSerializerNamespaces(new[]
                {
                    XmlQualifiedName.Empty
                });
                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };

                string xml;
                using (var stream = new StringWriter())
                {
                    using (var writer = XmlWriter.Create(stream, settings))
                    {
                        var serializer = new XmlSerializer(typeof(AllServices));
                        serializer.Serialize(writer, allServices, emptyNamespaces);
                    }

                    xml = stream.ToString();
                }

                try
                {
                    var returnCodeStr = QueryAccountRep.SetSaveToTblTmpAdditionalServiceByBatch(
                        orderCode, xml, session);

                    if (!int.TryParse(returnCodeStr, out var returnCode) || returnCode != orderCode)
                    {
                        throw new Exception($"Unexpected return code = \"{returnCode}\", \"{orderCode}\" is needed.");
                    }
                }
                catch (Exception exception)
                {
                    LogUtils.UpdateSpLog(
                        $"QueryCustomerUtils.SetSaveToTblTmpAdditionalServiceByBatch({orderCode}, {xml}, session) - ERROR!",
                        session);
                    LogUtils.UpdateSpLogWithException(exception, session);
                }
            }
        }

        public static List<Item4Invoice> InvoiceGo(int invoiceId,
            int customerId,
            int customerOfficeId,
            int billToAccount,
            bool addSku, 
            bool addLotNumber,
            List<BatchShortModel> batchModels,
            List<Item4Invoice> items4Invoice,
            HttpSessionState session,
            int? serviceType = null)
        {
            batchModels.ForEach(batchModel =>
            {
                
                InvoiceBatch(invoiceId, batchModel.BatchId, items4Invoice, addSku, addLotNumber, session, serviceType);

                QueryAccountRep.UpdateInvoice2(
                    customerId,
                    customerOfficeId,
                    invoiceId,
                    billToAccount, 
                    session);

                items4Invoice
                    .Where(item4Invoice => item4Invoice.BatchId == batchModel.BatchId)
                    .ToList()
                    .ForEach(item4Invoice =>
                    {
                        var sFullItemCode = Utils.FullItemNumber(
                            item4Invoice.GroupCode, item4Invoice.BatchCode,
                            item4Invoice.ItemCode, true);
                        var sDescriptionStatus = item4Invoice.Description.Split('_');
                        var result = "";
                        if (sDescriptionStatus[0] == "0")
                            result = "Item " + sFullItemCode + ": Invoicing Failed: " + sDescriptionStatus[1];

                        if (sDescriptionStatus[0] == "1")
                            result = "Item " + sFullItemCode + ": " + sDescriptionStatus[1];

                        item4Invoice.Result = result;
                        item4Invoice.FullBatch =
                            "Batch " + Utils.FullItemNumberWithDots(batchModel.GroupCode, batchModel.BatchCode);
                    });

                var itemsAffected = items4Invoice
                    .Count(item4Invoice => item4Invoice.BatchId == batchModel.BatchId &&
                                           !item4Invoice.Description.Contains(ItemIsAlreadyBilled));
                if (itemsAffected > 0)
                {
                    FrontUtils.SetBatchEvent(
                        FrontUtils.BatchEvents.Billed,
                        batchModel.BatchId,
                        FrontUtils.Codes.AccRep,
                        itemsAffected,
                        batchModel.ItemsQuantity,
                        session);
                }
            });
            return items4Invoice;
        }

        private static void InvoiceBatch(int invoiceId,
            int batchId,
            List<Item4Invoice> items4Invoice,
            bool addSku,
            bool addLotNumber,
            HttpSessionState session, int? serviceTypeId)
        {
            var alreadyInvoicedItems = QueryAccountRep.GetBatchInvoice(batchId, session);
            items4Invoice
                .Where(item4Invoice => item4Invoice.BatchId == batchId)
                .ToList()
                .ForEach(item4Invoice =>
                {
                    var isItemAlreadyInvoiced = alreadyInvoicedItems
                        .Exists(invoiceModel => invoiceModel.ItemCode == item4Invoice.ItemCode);
                    if (isItemAlreadyInvoiced)
                    {
                        item4Invoice.Passed = 1;
                        item4Invoice.Description = "0_" + ItemIsAlreadyBilled;
                        return;
                    }

                    var sDescription = InvoiceItem(
                        item4Invoice.ItemCode,
                        batchId,
                        invoiceId, 
                        addSku, 
                        addLotNumber, 
                        session, 
                        out var isPassed, 
                        serviceTypeId);
                    item4Invoice.Passed = isPassed;
                    item4Invoice.Description = sDescription;

                });
        }

        private static string InvoiceItem(int itemCode, int batchId, int invoiceId,
            bool addSku, bool addLotNumber,
            HttpSessionState session,
            out int isPassed, int? serviceTypeId)
        {
            var checkItemCpAndPriceModel = QueryAccountRep.GetCheckItemCpAndPrice(batchId, itemCode, session);
            if (!string.IsNullOrEmpty(checkItemCpAndPriceModel.Description))
            {
                isPassed = 1;
                return "0_" + checkItemCpAndPriceModel.Description;
            }

            string invoicingGoResult = QueryAccountRep.SetInvoicingGo(
                invoiceId,
                batchId,
                itemCode,
                checkItemCpAndPriceModel.Pass,
                addSku,
                addLotNumber, 
                session,
                serviceTypeId);

            isPassed = checkItemCpAndPriceModel.Pass;
            return invoicingGoResult;
        }
    }
}