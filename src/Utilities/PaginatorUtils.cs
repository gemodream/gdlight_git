﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Corpt.Models.Stats;
using System.Globalization;

namespace Corpt.Utilities
{
    public class PaginatorUtils
    {
        public static int SessionCountOnPage = 100;
        public static int StoneNumbersCountOnPage = 100;
        #region Paginator
        const int MaxPaginationLinks = 41;
        const int AdjacentPaginationLinks = 2;
        const int MaxNumInterjacent = MaxPaginationLinks - 2 * AdjacentPaginationLinks - 2 - 2;
        public static List<int> GetPagesList(int curPage, int numPages)
        {
            var pages = new List<int>();

            pages.Add(curPage);
            if ((curPage - 1) > 0) pages.Add(curPage - 1);
            if ((curPage - 2) > 0) pages.Add(curPage - 2);
            if ((curPage + 1) < numPages) pages.Add(curPage + 1);
            if ((curPage + 2) < numPages) pages.Add(curPage + 2);

            if (!pages.Contains(1)) pages.Add(1);
            if (!pages.Contains(numPages)) pages.Add(numPages);
            if (numPages > 40)
            {
                var step = (int)Math.Ceiling((decimal)(numPages / MaxNumInterjacent));
                if (step % 10 > 0)
                {
                    step += (10 - (step % 10));
                }
                step = Math.Max(10, step);
                var start = Math.Min(30, step);
                int i = 0;
                while ((start + i * step) < numPages)
                {
                    var p = (start + i * step);
                    if (!pages.Contains(p)) pages.Add(p);
                    i++;
                }
            }
            pages.Sort();
            return pages;
        }
        #endregion

        #region Stone Numbers Report
        public static string ConvertDateToString(DateTime date)
        {
            return date.ToString("d", CultureInfo.CreateSpecificCulture("en-US")).Trim();
        }
        public static DateTime? ConvertDateFromString(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return DateTime.Parse(value, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
            }
            catch (Exception x)
            {
                Console.Out.WriteLine(x.Message);
            }
            return null;
        }
        public static List<SessionPageModel> GenerateSessionPages(int pageCount, int pageNo, StatsFilterModel filterModel)
        {
            var pages = new List<SessionPageModel>();
            var numPages = GetPagesList(pageNo, pageCount);
            var dateFrom = filterModel.DateFrom == null ? "" : ConvertDateToString((DateTime)filterModel.DateFrom);
            var dateTo = filterModel.DateTo == null ? "" : ConvertDateToString((DateTime)filterModel.DateTo);
            foreach (int i in numPages)
            {
                pages.Add(new SessionPageModel
                {
                    IsActive = (i == pageNo),
                    PageNo = i,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    UserId = filterModel.UserId
                });
            }

            if (pages.Count == 0)
            {
                return pages;
            }
            if (pageNo > 1)
            {
                pages.Insert(0, new SessionPageModel
                {
                    PageNo = pageNo - 1,
                    IsPrevious = true,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    UserId = filterModel.UserId
                });
            }
            if (pageNo < pageCount)
            {
                pages.Add(new SessionPageModel
                {
                    PageNo = pageNo + 1,
                    IsNext = true,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    UserId = filterModel.UserId
                });
            }
            return pages;
        }
        public static List<PageModel> GenerateStoneNumbersPages(int rowCounts, int pageNo, StatsFilterModel filterModel)
        {
            var pages = new List<PageModel>();
            var pageCount = (int)Math.Ceiling((decimal)rowCounts / StoneNumbersCountOnPage);
            var numPages = GetPagesList(pageNo, pageCount);
            var dateFrom = filterModel.DateFrom == null ? "" : ConvertDateToString((DateTime)filterModel.DateFrom);
            var dateTo = filterModel.DateTo == null ? "" : ConvertDateToString((DateTime)filterModel.DateTo);
            var periodType = filterModel.PeriodType;
            foreach (int i in numPages)
            {
                pages.Add(new PageModel
                {
                    IsActive = (i == pageNo),
                    PageNo = i,
                    PeriodType = periodType,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    CustomerId = filterModel.CustomerId,
                    ParentOfficeId = filterModel.OfficeId
                });
            }

            if (pages.Count == 0)
            {
                return pages;
            }
            if (pageNo > 1)
            {
                pages.Insert(0, new PageModel
                {
                    PageNo = pageNo - 1,
                    IsPrevious = true,
                    PeriodType = periodType,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    CustomerId = filterModel.CustomerId,
                    ParentOfficeId = filterModel.OfficeId
                });
            }
            if (pageNo < pageCount)
            {
                pages.Add(new PageModel
                {
                    PageNo = pageNo + 1,
                    IsNext = true,
                    PeriodType = periodType,
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    CustomerId = filterModel.CustomerId,
                    ParentOfficeId = filterModel.OfficeId
                });
            }
            return pages;
        }
        #endregion
    }
}