﻿using System;
using System.IO;
using System.Web.UI;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Corpt
{
    public enum PersonFile
    {
        Photo = 1,
        Signature = 2,
        AuthorizationForm = 3
	}

    public enum FrontInOutFile
    {
        TakeInPersonSignature = 1,
        TakeInReceipt = 2,
        GiveOutPersonSignature = 3,
        GiveOutReceipt = 4
    }

    public class AzureStorageBlob
    {
		public static string UploadPDFFileToBlob(string storedPDFString, string fileName, Page p)
		{
			string myAccountName = p.Session["AzureAccountName"].ToString();
			string myAccountKey = p.Session["AzureAccountKey"].ToString();
			string containerName = p.Session["AzureContainerName"].ToString();
			string imageURL = "";

			try
			{
				CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
				CloudBlobClient client = storageAccount.CreateCloudBlobClient();
				CloudBlobContainer blobContainer = client.GetContainerReference(containerName);
				blobContainer.CreateIfNotExists();
				blobContainer.SetPermissions(
					new BlobContainerPermissions
					{
						PublicAccess = BlobContainerPublicAccessType.Blob
					});

				CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(fileName);
				blockBlob.Properties.ContentType = "application/pdf";

				byte[] imageBytes= Convert.FromBase64String(storedPDFString); 

				MemoryStream ms = new MemoryStream(imageBytes);
				blockBlob.UploadFromStream(ms);
				imageURL = blockBlob.Uri.AbsoluteUri.ToString();
			}
			catch (Exception ex)
			{
				imageURL = "";
			}
			return imageURL;
		}
		public static string UploadFileToBlob(string storedImageString, string fileName, Page p)
        {
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            string containerName = p.Session["AzureContainerName"].ToString();
            string imageURL = "";

            try
            {
                CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
                CloudBlobClient client = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer blobContainer = client.GetContainerReference(containerName);
                blobContainer.CreateIfNotExists();
                blobContainer.SetPermissions(
                    new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    });

                CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = "image/png";

                byte[] imageBytes;
                if(storedImageString.Contains(","))
                    imageBytes = Convert.FromBase64String(storedImageString.Split(',')[1]);
                else
                    imageBytes = Convert.FromBase64String(storedImageString);

                MemoryStream ms = new MemoryStream(imageBytes);
                blockBlob.UploadFromStream(ms);
                imageURL = blockBlob.Uri.AbsoluteUri.ToString();
            }
            catch (Exception ex)
            {
                imageURL = "";
            }
            return imageURL;
        }

        public static string ShowFileFromBlob(string fileName, Page p)
        {
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            string containerName = p.Session["AzureContainerName"].ToString();
            string imageBase64 = "";

            CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = client.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();
            blobContainer.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });

            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(fileName);

            MemoryStream memStream = new MemoryStream();
            if (blockBlob.Exists())
            {
                blockBlob.DownloadToStream(memStream);
                Byte[] bytes = memStream.ToArray();
                imageBase64 = Convert.ToBase64String(bytes, 0, bytes.Length);
            }

            if (memStream.Length == 0)
                imageBase64 = "data:image/png;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
            else
                imageBase64 = "data:image/png;base64," + imageBase64;

            return imageBase64;
        }

        public static string GetUrlOfFileFromBlob(string fileName, Page p)
        {
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            string containerName = p.Session["AzureContainerName"].ToString();
            string imageURL = "";

            CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = client.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();
            blobContainer.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });

            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(fileName);
            return blockBlob.Uri.AbsoluteUri.ToString();
        }

        public static bool BlobExists(string fileName, Page p)
        {
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            string containerName = "takeoutreceipts";//p.Session["AzureContainerName"].ToString();
            CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            bool exists = client.GetContainerReference(containerName).GetBlockBlobReference(fileName).Exists();
            return exists;
        }
		public static string GenerateCustomerKYCFileName(string fileName, Page p)
		{
			fileName = "CustomerKYC/" + fileName + "_KYC.pdf";
			return fileName;
		}

		public static string GeneratePersonFileName(string fileName, PersonFile person, Page p)
        {
            switch (person)
            {
                case PersonFile.Photo:
                    fileName = "Person/Photo/" + fileName + "_Photo.png";
                    break;
                case PersonFile.Signature:
                    fileName = "Person/Signature/" + fileName + "_Signature.png";
                    break;
                case PersonFile.AuthorizationForm:
                    fileName = "Person/AuthorizationForm/" + fileName + "_AuthorizationForm.pdf";
                    break;
				default:
                    break;
            }
            return fileName;
        }

        public static string GenerateFrontInOutFileName(string fileName, FrontInOutFile frontInOut, Page p)
        {
            switch (frontInOut)
            {
                case FrontInOutFile.TakeInPersonSignature:
                    fileName = "FrontInOut/TakeInPersonSignature/" + fileName + "_TakeInPersonSignature.png";
                    break;
                case FrontInOutFile.TakeInReceipt:
                    fileName = "FrontInOut/TakeInReceipt/" + fileName + "_TakeInReceipt.png";
                    break;
                case FrontInOutFile.GiveOutPersonSignature:
                    fileName = "FrontInOut/GiveOutPersonSignature/" + fileName + "_GiveOutPersonSignature.png";
                    break;
                case FrontInOutFile.GiveOutReceipt:
                    fileName = "FrontInOut/GiveOutReceipt/" + fileName + "_GiveOutReceipt.png";
                    break;
                default:
                    break;
            }
            return fileName;
        }



    }
}