﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Corpt.Models;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Xml;
using System.Data;

namespace  Corpt.Utilities
{
    public class LocationUtils
    {
        /*
        public const string IndiaCode = "IN";
        public const string IndiaUrl = "";

        public const string UsaCode = "US";
        public const string UsaUrl = "http://wg.gemscience.net/vr/veri.aspx";

        public const string BelgiumCode = "BE";
        public  const string BelgiumUrl = "";
        */
        //const string JsonFormat = "http://freegeoip.net/json/{0}";//Old
        //const string JsonFormat = "http://geoip.nekudo.com/api/{0}";//New as of 7/2/2018
		const string JsonFormat = "http://api.ipapi.com/api/check?access_key=fa5f36571138f53076960c09f181e7e0";//New as of 10/19/2018
        const string XMLFormat = "https://ipapi.co/xml";


        public static string GetIPAddress(HttpRequest request)
        {
            string ip = GetIPAddress1();
            string ipForwardedFor = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipForwardedFor != null)
                ip = ipForwardedFor;
            return ip;
        }

        private static string GetIPAddress1()
        {
            string IPAddress = null;
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            return IPAddress;
        }
        public static IpModel GetIpParameters(IpModel ipModel)
        {
            var ipUrl = XMLFormat;
            var ipRequest = System.Net.WebRequest.Create(ipUrl);
            using (WebResponse wrs = ipRequest.GetResponse())
            using (Stream stream = wrs.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string xml = reader.ReadToEnd();
                var xmlAPDP = new XmlDocument();
                xmlAPDP.LoadXml(xml);
                var xmlReader = new XmlNodeReader(xmlAPDP);
                DataSet ds = new DataSet();
                ds.ReadXml(xmlReader);
                ipModel.CountryCode = ds.Tables[0].Rows[0]["country_code"].ToString();
                ipModel.RegionCode = ds.Tables[0].Rows[0]["region_code"].ToString();
                ipModel.City = ds.Tables[0].Rows[0]["city"].ToString();
            }
            return ipModel;
        }
        public static IpModel GetIpParametersOld(IpModel ipModel)
        {
            //return new IpModel { Ip = "213.248.20.35" };
            var url = string.Format(JsonFormat, ipModel.Ip);
            var request = System.Net.WebRequest.Create(url);
            using (WebResponse wrs = request.GetResponse())
            using (Stream stream = wrs.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();
                var obj = Newtonsoft.Json.Linq.JObject.Parse(json);
                var countryCode = (string)obj["country_code"];
                ipModel.CountryCode = (string)obj["country_code"];
                ipModel.RegionCode = (string)obj["region_code"];
                ipModel.City = (string)obj["city"];
            }
            return ipModel;
        }
        public static IPAddress[] GetDnsIps(string hostName)
        {
            try
            {
               
                return Dns.GetHostAddresses(hostName);
                
            }
            catch (Exception x)
            {
                return new IPAddress[0];
            }
        }
    }
}