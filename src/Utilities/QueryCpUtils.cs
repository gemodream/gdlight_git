﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.CustomerProgram;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;

namespace Corpt.Utilities
{
    public class QueryCpUtils
    {
        #region Define Document
        public static string AttachDocumentToCp(string documentId, string cpId, string cpOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetDocument_CP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 255));
            command.Parameters["@rId"].Direction = ParameterDirection.Output;

            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.AddWithValue("@CPOfficeID", cpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]); //TODO ???
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();

            return msg;

        }
        public static bool IsAttachedDocument(string documentId, string cpId, string cpOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetDocument_CP1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.AddWithValue("@CPOfficeID", cpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var attached = dt.Rows.Count > 0;
            conn.Close();
            return attached;
        }
        public static string InsertDefineDocument(DocPredefinedModel saveDocument, string itemTypeId, string cpOfficeId, out string docId , Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spInsertDocumentEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@DocumentID", SqlDbType.Int));
            command.Parameters["@DocumentID"].Direction = ParameterDirection.Output;
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            command.Parameters.AddWithValue("@DocumentTypeCode", saveDocument.DocumentTypeCode);
            command.Parameters.AddWithValue("@DocumentName", saveDocument.DocumentName);
            command.Parameters.AddWithValue("@BarCodeFixedText", saveDocument.BarCodeFixedText);
            command.Parameters.AddWithValue("@UseDate", saveDocument.UseDate);
            command.Parameters.AddWithValue("@UseVirtualVaultNumber", saveDocument.UseVvNumber);
            command.Parameters.AddWithValue("@CorelFile", saveDocument.CorelFile);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CurrentOfficeID", cpOfficeId);

            command.Parameters.AddWithValue("@ExportTypeID", saveDocument.ExportTypeId);
            command.Parameters.AddWithValue("@ImportTypeID", saveDocument.ImportTypeId);
            command.Parameters.AddWithValue("@FormatTypeID", saveDocument.FormatTypeId);

            var tableValues = CreateDocumentValuesTable(saveDocument.DocumentId, saveDocument.DocumentValues);
            var paramValues = new SqlParameter
            {
                ParameterName = "@DocumentValues",
                SqlDbType = SqlDbType.Structured,
                Value = tableValues,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);
            LogUtils.UpdateSpLogWithParameters(command, p);
            docId = "";
            try
            {
                command.ExecuteNonQuery();
                docId = command.Parameters["@DocumentID"].Value.ToString();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static string UpdateDefineDocument(DocPredefinedModel saveDocument, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spUpdateDocumentEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", saveDocument.DocumentId);
            command.Parameters.AddWithValue("@DocumentName", saveDocument.DocumentName);
            command.Parameters.AddWithValue("@BarCodeFixedText", saveDocument.BarCodeFixedText);
            command.Parameters.AddWithValue("@UseDate", saveDocument.UseDate);
            command.Parameters.AddWithValue("@UseVirtualVaultNumber", saveDocument.UseVvNumber);
            command.Parameters.AddWithValue("@CorelFile", saveDocument.CorelFile);
            command.Parameters.AddWithValue("@ExportTypeID", saveDocument.ExportTypeId);
            command.Parameters.AddWithValue("@ImportTypeID", saveDocument.ImportTypeId);
            command.Parameters.AddWithValue("@FormatTypeID", saveDocument.FormatTypeId);

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            var tableValues = CreateDocumentValuesTable(saveDocument.DocumentId, saveDocument.DocumentValues);
            var paramValues = new SqlParameter
            {
                ParameterName = "@DocumentValues",
                SqlDbType = SqlDbType.Structured,
                Value = tableValues,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();

            return msg;
        }
        private static DataTable CreateDocumentValuesTable(string documentId, IEnumerable<DocumentValueModel> values)
        {
            var table = new DataTable("DocumentValues");
            table.Columns.Add("DocumentId");
            table.Columns.Add("Title");
            table.Columns.Add("Value");
            table.Columns.Add("Unit");
            foreach (var value in values)
            {
                table.Rows.Add(new object[]
                { 
                    documentId, value.Title, value.Value, value.Unit
                });
            }
            table.AcceptChanges();
            return table;
        }
        public static Corpt.Models.DocumentTypeModel GetDocumentType(string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetDocumentTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var types = (from DataRow row in dt.Rows select new DocumentTypeModel(row)).ToList();
            var result = types.Find(m => m.DocumentTypeCode == documentTypeCode);
            conn.Close();
            return result;
        }
        public static DefineDocumentInitModel GetDefineDocumentInit(string cpId, string cpOfficeId, string docTypeCode, string cpDocId, Page p)
        {
            var initData = new DefineDocumentInitModel
            {
                CpId = cpId,
                CpOfficeId = cpOfficeId,
                Cp = GetCustomerProgramByCpId(cpId, p),
                CpDocId = cpDocId,
                DocumentType = GetDocumentType(docTypeCode, p)
            };
            initData.MeasureParts = QueryUtils.GetMeasureParts(Convert.ToInt32(initData.Cp.ItemTypeId), p);
           
            var result = new List<MeasureModel>();
            var measuresAll = GetMeasuresByItemTypeAll(""+initData.Cp.ItemTypeId, p);
            var rules = GetCpDocRules(cpDocId, p);
            foreach (var part in initData.MeasureParts)
            {
                var measures = measuresAll.FindAll(m => m.PartTypeId == part.PartTypeId);
                foreach(var measure in measures)
                {
                    if ("" + measure.MeasureId == DbConstants.MeasureIdShapeCut) continue;
                    var measureNotVisible = rules.Find(m => m.MeasureId == measure.MeasureId && m.NotVisibleInCcm && m.PartId == part.PartId);
                    if (measureNotVisible != null)
                    {
                        continue;
                    }
                    result.Add(new MeasureModel{PartId = part.PartId, PartTypeId = part.PartTypeId, MeasureName = measure.MeasureName, MeasureId = measure.MeasureId});
                }
            }
            initData.MeasuresByParts = result;

            //--
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetImpExInfo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OfficeID", initData.Cp.CpOfficeId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            
            var da = new SqlDataAdapter(command);
            var ds = new DataSet();
            da.Fill(ds);
            initData.ImportInfo = (from DataRow row in ds.Tables[0].Rows select new ImpExpInfoModel(row)).ToList();
            initData.ImportInfo.Sort((m1, m2) =>  string.CompareOrdinal(m1.Order, m2.Order) );

            initData.ExportInfo = (from DataRow row in ds.Tables[1].Rows select new ImpExpInfoModel(row)).ToList();
            initData.ExportInfo.Sort((m1, m2) => string.CompareOrdinal(m1.Order, m2.Order));

            initData.FormatInfo = (from DataRow row in ds.Tables[2].Rows select new DocFormatModel(row)).ToList();
            initData.FormatInfo.Sort((m1, m2) => string.CompareOrdinal(m1.Order, m2.Order));
            conn.Close();

            initData.Documents = GetDocumentsByItemTypeAndDocumentTypeCode(""+initData.Cp.ItemTypeId, docTypeCode, p);
            return initData;
        }
        public static List<DocPredefinedModel> GetDocumentsByItemTypeAndDocumentTypeCode(string itemTypeId, string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentsPredefined",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            command.Parameters.AddWithValue("@DocumentTypeCode", documentTypeCode);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result =  (from DataRow row in dt.Rows select new DocPredefinedModel(row)).ToList();
            result.Sort((m1, m2) => string.CompareOrdinal(m1.DocumentName.ToUpper(), m2.DocumentName.ToUpper()));
            result.Add(DocPredefinedModel.GetNewDocument(documentTypeCode));
            return result;
        }
        
        public static List<DocumentValueModel> GetDocumentValues(string documentId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentValue",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", documentId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentValueModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        public static List<DocumentTitleModel> GetDocumentTitles(string languageId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDefaultDocumentTitle",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentLanguageID", languageId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentTitleModel(row)).ToList();
            conn.Close();
            return result;
        }

        public static List<LanguageModel> GetLanguages(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentLanguage",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new LanguageModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static CustomerProgramModel GetCustomerProgramByBatchNumber(string batchNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramInstanceByBatchCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", Utils.ParseOrderCode(batchNumber));
            command.Parameters.AddWithValue("@BatchCode", Utils.ParseBatchCode(batchNumber));
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var cpId = "";
            if (dt.Rows.Count >0)
            {
                cpId = "" + dt.Rows[0]["CPID"];
            }
            conn.Close();
            return string.IsNullOrEmpty(cpId) ? null : CustomerProgramModel.CreateCombineCp(dt.Rows[0], GetCustomerProgramByCpId(cpId, p));
        }
        public static CustomerProgramModel GetCustomerProgramByCpId(string cpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramInstanceByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPID", cpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerProgramModel(row, true)).ToList();
            conn.Close();
            return result.Count == 1 ? result.ElementAt(0) : null;
        }
        #endregion

        #region Find Cp by Program name, Customer, Vendor
        public static bool ExistsCp(string cpName, CustomerModel customer, CustomerModel vendor, string verifyCpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByNameAndCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerProgramName", cpName);
            command.Parameters.AddWithValue("@CustomerOfficeID", customer.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
            if (vendor != null)
            {
                command.Parameters.AddWithValue("@VendorOfficeID", customer.OfficeId);
                command.Parameters.AddWithValue("@VendorID", customer.CustomerId);
            }
            else
            {
                command.Parameters.Add("@VendorOfficeID", SqlDbType.Int).Value = DBNull.Value;
                command.Parameters.Add("@VendorID", SqlDbType.Int).Value = DBNull.Value;
            }
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var exists = false;
            if (dt.Rows.Count > 0)
            {
                if (string.IsNullOrEmpty(verifyCpId))
                {
                    exists = true;
                } else
                {
                    exists = ("" + dt.Rows[0]["CPID"]) != verifyCpId;
                }
            }
            conn.Close();
            return exists;
        }
        #endregion

        #region Operations Tree
        public static List<OperationModel> GetAllOperations(CommonPage p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetOperationTree",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new OperationModel(row)).ToList();
            conn.Close();
            return result;

        }
        public static List<string> GetCpOperations(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPOperations",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = Int32.Parse(cpModel.CpOfficeId);
            command.Parameters.Add("@CPID", SqlDbType.Int).Value = Int32.Parse(cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ("" + row["OperationTypeID"])).ToList();
            conn.Close();
            return result;
           
        }
        #endregion

        #region Short Measures (Characteristics)
        public static List<MeasureShortModel> GetMeasuresShort(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresWithAdditional",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureShortModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        #endregion

        #region Measures by ItemType
        public static List<MeasureModel> GetMeasuresByItemType(string itemTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresByItemType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasureModel.Create2(row)).ToList();
            conn.Close();
            return
                result.FindAll(
                    m =>
                    m.MeasureClass == MeasureModel.MeasureClassEnum ||
                    m.MeasureClass == MeasureModel.MeasureClassNumeric);
        }
        public static List<MeasureModel> GetMeasuresByItemTypeAll(string itemTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresByItemType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasureModel.Create2(row)).ToList();
            conn.Close();
            return result;
        }

        #endregion

        #region Most Recently (ItemTypeGroups and ItemTypes)
        public static List<ItemTypeModel> GetItemTypesRecently(CustomerModel customer, CustomerModel vendor, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMRUItems",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CustomerOfficeID", customer.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
            if (vendor != null)
            {
                command.Parameters.AddWithValue("@VendorOfficeID", vendor.OfficeId);
                command.Parameters.AddWithValue("@VendorID", vendor.CustomerId);
            } else
            {
                command.Parameters.AddWithValue("@VendorOfficeID", customer.OfficeId);
                command.Parameters.AddWithValue("@VendorID", customer.CustomerId);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeModel(row, true)).ToList();
            conn.Close();
            return result;

        }
        #endregion

        #region ItemTypeGroups
        public static List<ItemTypeGroupModel> GetItemTypeGroups(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemTypeGroups",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeGroupModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region ItemTypes by Group
        public static List<ItemTypeModel> GetItemTypesByGroup(int itemTypeGroupId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemTypesByGroup",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeGroupID", itemTypeGroupId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemTypeModel(row, false)).ToList();
            conn.Close();
            return result;
        }

        public static List<ItemTypeModel> GetItemTypesByIds(List<string> ids, Page p )
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            const string sql = "select * from v0ItemType where ItemTypeId in ({0})";
            var criteria = ids.Aggregate("", (current, id) => current + ((current == "" ? "" : ", ") + id));
            var command = new SqlCommand
            {
                CommandText = string.Format(sql, criteria),
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemTypeModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataTable GetCustomerCodeByID(string customerId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = @"select top 1 CustomerCode from v0Customer where customerId=" + customerId,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt == null || dt.Rows.Count == 0)
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return dt;
        }
        #endregion

        #region Additional Services
        public static List<AddServiceModel> GetAdditionalServices(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetAdditionalService",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new AddServiceModel(row)).ToList();
            conn.Close();
            result.Insert(0, new AddServiceModel{ServiceId = "", ServiceName = ""});
            result.Sort((m1, m2) => string.CompareOrdinal(m1.ServiceName, m2.ServiceName));
            return result;

        }
        public static List<AddServicePriceModel> GetAdditionalServicePrices(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetAdditionalServicePriceByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new AddServicePriceModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Pricing
        public static List<PricePartMeasureModel> GetPricePartMeasures(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPricePartsMeasuresByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PricePartMeasureModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<PriceRangeModel> GetPriceRanges(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPriceRangeByCPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);

            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PriceRangeModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Save Customer Program Full
        public static string SaveCustomerProgram(CpEditModel cpEdit, bool copyAll, CustomerProgramModel cpFrom, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSaveCustomerProgram",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 255));
            command.Parameters["@rId"].Direction = ParameterDirection.Output;

            command.Parameters.AddWithValue("@CustomerOfficeID", Convert.ToInt32(cpEdit.Customer.OfficeId));
            command.Parameters.AddWithValue("@CustomerID", Convert.ToInt32(cpEdit.Customer.CustomerId));
            if (cpEdit.Vendor != null && !string.IsNullOrEmpty(cpEdit.Vendor.CustomerId))
            {
                command.Parameters.AddWithValue("@VendorOfficeID", Convert.ToInt32(cpEdit.Vendor.OfficeId));
                command.Parameters.AddWithValue("@VendorID", Convert.ToInt32(cpEdit.Vendor.CustomerId));
            } else
            {
                command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
                command.Parameters.AddWithValue("@VendorID", DBNull.Value);
            }
            if (cpEdit.Cp.CpId == "" || cpEdit.Cp.CpId == "0")
            {
                command.Parameters.AddWithValue("@CPOfficeID", DBNull.Value);
                command.Parameters.AddWithValue("@CPID", DBNull.Value);
            } else
            {
                command.Parameters.AddWithValue("@CPOfficeID", Convert.ToInt32(cpEdit.Cp.CpOfficeId));
                command.Parameters.AddWithValue("@CPID", Convert.ToInt32(cpEdit.Cp.CpId));
            }
            command.Parameters.AddWithValue("@CustomerProgramName", cpEdit.Cp.CustomerProgramName);
            command.Parameters.AddWithValue("@Comment", cpEdit.Cp.Comment);
            command.Parameters.AddWithValue("@Path2Picture", cpEdit.Cp.Path2Picture);
            command.Parameters.AddWithValue("@ItemTypeID", cpEdit.Cp.ItemTypeId);
            command.Parameters.AddWithValue("@ItemTypeGroupID", cpEdit.Cp.ItemTypeGroupId);
            command.Parameters.AddWithValue("@CustomerStyle", cpEdit.Cp.CustomerStyle);
            command.Parameters.AddWithValue("@CPPropertyCustomerID", cpEdit.Cp.CpPropertyCustId);
            if (cpEdit.Cp.Srp == "0")
            {
                command.Parameters.AddWithValue("@SRP", DBNull.Value);
            } else
            {
                command.Parameters.AddWithValue("@SRP", cpEdit.Cp.Srp);
            }
            command.Parameters.AddWithValue("@Description", cpEdit.Cp.Description);
            
            command.Parameters.AddWithValue("@IsFixed", cpEdit.Cp.IsFixed ? 1 : 0);
            if (cpEdit.Cp.IsFixed && cpEdit.Cp.FixedPrice > 0)
            {
                command.Parameters.AddWithValue("@FixedPrice", cpEdit.Cp.FixedPrice);
            } else
            {
                command.Parameters.AddWithValue("@FixedPrice", DBNull.Value);
            }
            if (cpEdit.Cp.IsFixed && cpEdit.Cp.Discount > 0)
            {
                command.Parameters.AddWithValue("@Discount", cpEdit.Cp.Discount);
            } else
            {
                command.Parameters.AddWithValue("@Discount", DBNull.Value);
            }

            if (!cpEdit.Cp.IsFixed && cpEdit.Cp.DeltaFix > 0)
            {
                command.Parameters.AddWithValue("@DeltaFix", cpEdit.Cp.DeltaFix); 
            } else
            {
                command.Parameters.AddWithValue("@DeltaFix", DBNull.Value);
            }
            if (cpEdit.Cp.FailFixed > 0)
            {
                command.Parameters.AddWithValue("@FailFixed", cpEdit.Cp.FailFixed);
            } else
            {
                command.Parameters.AddWithValue("@FailFixed", DBNull.Value);
            }
            if (cpEdit.Cp.FailDiscount > 0)
            {
                command.Parameters.AddWithValue("@FailDiscount", cpEdit.Cp.FailDiscount);
            } else
            {
                command.Parameters.AddWithValue("@FailDiscount", DBNull.Value);
            }
            command.Parameters.AddWithValue("@CopyAllCp", copyAll ? 1 : 0); 
            
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            if (cpFrom != null)
            {
                command.Parameters.AddWithValue("@CPIDFrom", Convert.ToInt32(cpFrom.CpId));
                command.Parameters.AddWithValue("@CPOfficeIDFrom", Convert.ToInt32(cpFrom.CpOfficeId));
            } else
            {
                command.Parameters.AddWithValue("@CPIDFrom", DBNull.Value);
                command.Parameters.AddWithValue("@CPOfficeIDFrom", DBNull.Value);
            }
            command.Parameters.Add(GetParamPriceParts(cpEdit));
            command.Parameters.Add(GetParamPriceRange(cpEdit));
            command.Parameters.Add(GetParamPriceAddServs(cpEdit));
            command.Parameters.Add(GetParamCpOperations(cpEdit));
            command.Parameters.Add(GetParamCpDocs(cpEdit));
            command.Parameters.Add(GetParamCpDocRules(cpEdit));
            command.Parameters.Add(GetParamCpDocGroups(cpEdit));
            command.Parameters.Add(GetParamCpDocOpers(cpEdit));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
                msg = "" + command.Parameters["@rId"].Value;

            }

            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        private static SqlParameter GetParamCpDocOpers(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocOpers");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("OperationTypeOfficeId");
            tbl.Columns.Add("OperationTypeID");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var oper in doc.AttachedReports)
                {
                    tbl.Rows.Add(new object[] { doc.CpDocId, oper.OperationTypeOfficeId, oper.OperationTypeId });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocOpers",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocGroups(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocGroups");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("MeasureGroupID");
            tbl.Columns.Add("NoRecheck");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var group in doc.MeasureGroups)
                {
                    if (group.NoRecheck > 0)
                    {
                        tbl.Rows.Add(new object[] { doc.CpDocId, group.MeasureGroupId, Convert.ToInt32(group.NoRecheck) });
                    }
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocGroups",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocRules(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocRules");
            tbl.Columns.Add("CpDocId");
            tbl.Columns.Add("MeasureId");
            tbl.Columns.Add("MinMeasure");
            tbl.Columns.Add("MaxMeasure");
            tbl.Columns.Add("PartID");
            tbl.Columns.Add("NotVisibleInCCM");
            tbl.Columns.Add("IsDefaultMeasureValue");
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                foreach (var rule in doc.EditDocRules)
                {
                    if (!rule.HasValues) continue;
                    object minValue;
                    object maxValue;
                    if (string.IsNullOrEmpty(rule.MinValue))
                    {
                        minValue = DBNull.Value;
                    }
                    else
                    {
                        minValue = float.Parse(rule.MinValue);
                    }
                    if (string.IsNullOrEmpty(rule.MaxValue))
                    {
                        maxValue = DBNull.Value;
                    }
                    else
                    {
                        maxValue = float.Parse(rule.MaxValue);
                    }
                    tbl.Rows.Add(new[]
                    {
                        doc.CpDocId, rule.MeasureId, minValue, maxValue, rule.PartId, rule.NotVisibleInCcm ? 1 : 0, rule.IsDefault ? 1 : 0                     
                    });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocRules",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpDocs(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpDocs");
            tbl.Columns.Add("CpDocId", typeof(string));
            tbl.Columns.Add("Description", typeof(string));
            tbl.Columns.Add("IsReturn", typeof(string));
            foreach (var doc in cpEdit.CpDocs)
            {
                if (!doc.HasValue) continue;
                tbl.Rows.Add(new object[] { doc.CpDocId, doc.Description, doc.IsReturn ? 1 : 0 });
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpDocs",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamCpOperations(CpEditModel cpEdit)
        {
            var tbl = new DataTable("CpOperations");
            tbl.Columns.Add("OperationTypeOfficeId", typeof(int));
            tbl.Columns.Add("OperationTypeID", typeof(int));
            tbl.Columns.Add("Checked", typeof(int));
            foreach (var operation in cpEdit.OperationTypes)
            {
                tbl.Rows.Add(new object[] { cpEdit.OperationTypeOfficeId, operation, 1 });
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@CpOperations",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceAddServs(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceAddServs");
            tbl.Columns.Add("AdditionalServiceID", typeof(int));
            tbl.Columns.Add("Price", typeof(float));
            //if (!cpEdit.Cp.IsFixed)//Sandip changes to fix fixed cp saving, 8/21/2018
            //{
                foreach(var serv in cpEdit.CpPrice.AddServicePrices)
                {
                    tbl.Rows.Add(new object[] {serv.ServiceId, serv.Price});
                }
            //}
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceAddServs",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceRange(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceRange");
            tbl.Columns.Add("HomogeneousClassID", typeof(int));
            tbl.Columns.Add("FromValue", typeof(float));
            tbl.Columns.Add("ToValue", typeof(float));
            tbl.Columns.Add("Price", typeof(float));
            if (!cpEdit.Cp.IsFixed)
            {
                foreach (var range in cpEdit.CpPrice.PriceRanges)
                {
                    tbl.Rows.Add(new object[] { 1/* TODO range.HomogeneousClassId*/, range.ValueFrom, range.ValueTo, range.Price });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceRange",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };
            return param;
        }
        private static SqlParameter GetParamPriceParts(CpEditModel cpEdit)
        {
            var tbl = new DataTable("PriceParts");
            tbl.Columns.Add("PartId", typeof(int));
            tbl.Columns.Add("MeasureCode", typeof(int));
            tbl.Columns.Add("HomogeneousClassID", typeof(int));
            tbl.Columns.Add("PartNameMeasureName", typeof(string));
            if (!cpEdit.Cp.IsFixed)
            {
                foreach (var part in cpEdit.CpPrice.PricePartMeasures)
                {
                    tbl.Rows.Add(new object[]
                    {
                        part.PartId, 
                        part.MeasureCode, 
                        1, //TODO part.HomogeneousClassId, 
                        part.PartNameMeasureName
                    });
                }
            }
            tbl.AcceptChanges();
            var param = new SqlParameter
            {
                ParameterName = "@PriceParts",
                SqlDbType = SqlDbType.Structured,
                Value = tbl,
                Direction = ParameterDirection.Input
            };

            return param;
        }
        #endregion
        
        #region Get Customer Program Full
        public static CpEditModel InitCustomerProgram(CustomerProgramModel cp, List<EnumMeasureModel> enumsMeasure, Page p)
        {
            var cpEdit = new CpEditModel
            {
                Cp = cp,
                MeasureParts = QueryUtils.GetMeasureParts(cp.ItemTypeId, p),
                MeasuresByItemType = GetMeasuresByItemType("" + cp.ItemTypeId, p),
            };
            AddCpDocModel(cpEdit, enumsMeasure, true, p);
            return cpEdit;
        }
        public static CpEditModel GetCustomerProgramFull(CustomerProgramModel cp, List<EnumMeasureModel> enumsMeasure, Page p)
        {
            var cpEdit = new CpEditModel
            {
                Cp = cp,
                MeasureParts = QueryUtils.GetMeasureParts(cp.ItemTypeId, p),
                MeasuresByItemType = GetMeasuresByItemType("" + cp.ItemTypeId, p),
                OperationTypes = GetCpOperations(cp, p),
                Reports = GetReportsByCp(cp, p),
                CpDocs = GetCpDocs(cp, p),
                CpPrice =
                {
                    PricePartMeasures = GetPricePartMeasures(cp, p),
                    PriceRanges = GetPriceRanges(cp, p),
                    AddServicePrices = GetAdditionalServicePrices(cp, p)
                },
            };
            foreach(var doc in cpEdit.CpDocs)
            {
                // Rules
                var savingDocRules = GetCpDocRules(""+doc.CpDocId, p);
                var editRules = new List<CpDocRuleEditModel>();
                foreach(var part in cpEdit.MeasureParts)
                {
                    var measures = cpEdit.MeasuresByItemType.FindAll(m => m.PartTypeId == part.PartTypeId);
                    foreach (var measure in measures)
                    {
                        var rule = savingDocRules.Find(m => m.MeasureId == measure.MeasureId && m.PartId == part.PartId);
                        var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                                     ? enumsMeasure.FindAll(m => m.MeasureValueMeasureId == measure.MeasureId)
                                     : null);
                        editRules.Add(new CpDocRuleEditModel(Convert.ToInt32(doc.CpDocId), measure, enums, rule, ""+part.PartId));
                    }
                }
                doc.EditDocRules = editRules;
                // Measure Recheks
                doc.MeasureGroups = GetMeasureGroupsByDoc(doc, p);
                // Attached Reports
                doc.AttachedReports = GetAttachedReportsByDoc(doc, cpEdit.Reports, p);
            }
            if (cpEdit.CpDocs.Find(m => m.IsDefault) == null)
            {
                //-- Add Default Document
                AddCpDocModel(cpEdit, enumsMeasure, true, p);
            }
            return cpEdit;
        }
        public static string AddCpDocModel(CpEditModel cpEdit, List<EnumMeasureModel> enumsMeasure, bool isDefault, Page p)
        {
            var maxDocId = (cpEdit.CpDocs.Count == 0 ? 0 : cpEdit.CpDocs.Max(m => m.CpDocId));
            var doc = new CpDocModel{CpDocId = maxDocId + 1, IsDefault = isDefault};
            
            //-- Rules
            if (!doc.IsDefault)
            {
                var editRules = new List<CpDocRuleEditModel>();
                foreach (var part in cpEdit.MeasureParts)
                {
                    var measures = cpEdit.MeasuresByItemType.FindAll(m => m.PartTypeId == part.PartTypeId);
                    foreach (var measure in measures)
                    {
                        var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                                     ? enumsMeasure.FindAll(m => m.MeasureValueMeasureId == measure.MeasureId)
                                     : null);
                        editRules.Add(new CpDocRuleEditModel(Convert.ToInt32(doc.CpDocId), measure, enums, null, "" + part.PartId));
                    }
                }
                doc.EditDocRules = editRules;
            }

            //-- Measure Groups
            if (MeasureGroups.Count == 0)
            {
                GetMeasureGroups(p);
            }
            var grpsByDoc = MeasureGroups.Select(@group => MeasureGroupModel.Copy(@group, doc.CpDocId)).ToList();
            grpsByDoc.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureGroupName, m2.MeasureGroupName));
            doc.MeasureGroups = grpsByDoc;
            if (isDefault)
            {
                cpEdit.CpDocs.Insert(0, doc);
            } else
            {
                cpEdit.CpDocs.Add(doc);
            }
            return "" + doc.CpDocId;
        }
        #endregion

        #region Reports by Cp, Doc
        public static List<CpDocPrintModel> GetReportsByCp(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocsByCP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID", cpModel.CpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocPrintModel(row, cpModel.CpOfficeIdAndCpId)).ToList();
            result.Sort(new DocComparer().Compare);
            conn.Close();
            return result;
        }
        public static List<CpDocPrintModel> GetAttachedReportsByDoc(CpDocModel docModel, List<CpDocPrintModel> cpPrintDocs, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDoc_Operation",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPDocID", docModel.CpDocId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select CpDocPrintModel.CreateAttachedReport(row, cpPrintDocs)).ToList();
            //result.Sort(new DocComparer().Compare);
            conn.Close();
            return result;
        }
        #endregion

        #region Documents by Cp
        public static List<CpDocModel> GetCpDocs(CustomerProgramModel cpModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDocs",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID", cpModel.CpOfficeId);
            command.Parameters.AddWithValue("@CPID", cpModel.CpId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Measure Groups (What to do)
        public static List<MeasureGroupModel>  MeasureGroups = new List<MeasureGroupModel>();
        public static List<MeasureGroupModel>  GetMeasureGroups(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasureGroups",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureGroupModel(row, false)).ToList();
            conn.Close();
            MeasureGroups = result;
            return result;

        }
        public static List<MeasureGroupModel> GetMeasureGroupsByDoc(CpDocModel cpDoc, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDoc_MeasureGroup",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CPDocID", cpDoc.CpDocId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureGroupModel(row, true)).ToList();
            conn.Close();
            //-- Union with common
            if (MeasureGroups.Count == 0)
            {
                GetMeasureGroups(p);
            }
            var grpsByDoc = new List<MeasureGroupModel>();
            foreach(var group in MeasureGroups)
            {
                var groupByDoc = result.Find(m => m.MeasureGroupId == group.MeasureGroupId);
                if (groupByDoc == null)
                {
                    grpsByDoc.Add(MeasureGroupModel.Copy(group, cpDoc.CpDocId));
                } else
                {
                    groupByDoc.MeasureGroupName = group.MeasureGroupName;
                    groupByDoc.CpDocId = ""+cpDoc.CpDocId;
                    grpsByDoc.Add(groupByDoc);
                }
            }
            grpsByDoc.Sort((m1, m2) => string.CompareOrdinal(m1.MeasureGroupName, m2.MeasureGroupName));
            return grpsByDoc;
        }
        public static void SetMeasureGroup(List<MeasureGroupModel> groups, Page p)
        {
            //spSetCPDoc_MeasureGroup: @AuthorId=19; @AuthorOfficeId=1; @CurrentOfficeId=1; @rId=; @CPDocID=2190198; @MeasureGroupID=2; @NoRecheck=3; 
        }
        #endregion

        #region Document Rules
        public static List<CpDocRuleModel>  GetCpDocRules(string cpDocId, Page p)
        {
            if (string.IsNullOrEmpty(cpDocId)) return new List<CpDocRuleModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDocRule",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@CPDocID", cpDocId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpDocRuleModel(row)).ToList();

            conn.Close();
            return result;
        }
        #endregion

        #region Document Properties
        // Attach Document to CP: spSetDocument_CP(documrntId, CpOfficeId, CpId), tblDocument_CP
        // Get All PrintDocs + attached to Cp: spGetDocsByCP (@CPOfficeID = 2, @CPID = 260133, @AuthorID = 19, @AuthorOfficeID = 1)
        // Attach PrintDoc to CpDoc: spSetCPDoc_Operation: @AuthorId=19; @AuthorOfficeId=1; @CurrentOfficeId=2; @rId=; @CPDocID=2190217; @OperationTypeOfficeID=1; @OperationTypeID=622;
        // Get PrintDocs, attached to CpDoc: spGetCPDoc_Operation (@CPDocID = 2190217, @AuthorID = 19, @AuthorOfficeID = 1)
        #endregion

        #region spGetCustomerProgramByBatchID
        public static string GetPath2Picture(string batchId, Page p)
        {
            /*
			SqlCommand command = new SqlCommand("SELECT v0Part.PartID, v0Part.PartName FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID WHERE (v0Batch.GroupCode = '"+ order.Text.Trim()+"') AND (v0Batch.BatchCode = '"+ batch.Text.Trim()+ "')");
			command.Connection = conn;
			command.CommandType = CommandType.Text;
             */
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = "";
            if (dt.Rows.Count > 0)
            {
                result = "" + dt.Rows[0]["Path2Picture"];
            }
            conn.Close();
            return result;
        }
        public static void GetCpAndCurrentDocsByBatch(UpdateOrderBatchModel batchModel, Page p)
        {
            //-- Find Customer Program
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchModel.BatchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                conn.Close();
                return;
            }

            var cpModel = CustomerProgramModel.Create2(dt.Rows[0]);
            batchModel.Cp = cpModel;
            
            //-- Find Current Docs
            command = new SqlCommand
            {
                CommandText = "spGetCurrentDocsByCP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@CPID", cpModel.CpId));
            command.Parameters.Add(new SqlParameter("@CPOfficeID", cpModel.CpOfficeId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            da = new SqlDataAdapter(command);
            dt = new DataTable();
            da.Fill(dt);
            var docs = (from DataRow row in dt.Rows select new CpDocPrintModel(row)).ToList();
            cpModel.CurrentDocs = docs;
            conn.Close();
            batchModel.CurrentDocs = docs;
        }
        #endregion

        #region Get Default Document Type Code by BatchID
        public static List<DocumentModel> GetDefaultDocumentsByBatchId(string batchId, string documentTypeCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDefaultDocumentTypeCodeByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentModel(row)).ToList();

            conn.Close();
            return result.FindAll(m => m.DocumentTypeCode == documentTypeCode);
            
        }
        #endregion
        #region FileUploaded
        public static string FileUploaded(string sku, Page p)
        {
            if (p.Session["SkuFilesTypes"] == null)
                return null;
            //sku = sku.Replace(@"/", "").Replace(@".", "").Replace(" ", "");
            string[] types = p.Session["SkuFilesTypes"].ToString().Split(',');
            DateTime? maxDate = null;
            string maxType = null;
            int lmKey = 0;
            foreach (string type in types)
            {
                string url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @"." + type;
                WebRequest request = WebRequest.Create(new Uri(url));
                request.Method = "HEAD";
                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        if (lmKey == 0)
                        {
                            for (int i = 0; i <= response.Headers.Count - 1; i++)
                            {
                                var key = response.Headers.Keys[i].ToString();
                                if (key.ToLower().Contains("last-modified"))
                                {
                                    lmKey = i;
                                    break;
                                }
                            }
                        }
                        var lmResponse = response.Headers[lmKey].ToString();
                        DateTime date = Convert.ToDateTime(lmResponse);
                        if ((maxDate == null) || (maxDate < date))
                        {
                            maxDate = date;
                            maxType = type;
                        }
                    }

                }
                catch
                {
                    //url = @"https://gdlightstorage.blob.core.windows.net/gdlight/SkuReceipt/" + sku + @".xlsx";
                }
            }

            return maxType ;
        }
        public static DataTable GetSkuMemoUrl(string sku, string custId, Page p)
        {
            try
            {
                custId = custId.Replace("Customer ID: ", "");
                DataTable dt = QueryUtils.GetSkuMemoPerSku(sku, custId, p);
                if (dt != null)
                    return dt;
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }
        public static bool UploadImages(string fileName, string pathName, MemoryStream ms, Page p)
        {
            try
            {
                string myAccountName = p.Session["AzureAccountName"].ToString();
                //string myAccountName = "gdlightstorage"; //ConfigurationManager.AppSettings["myAccountName"].ToString();
                 //myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";//ConfigurationManager.AppSettings["myAccountKey"].ToString();
                string myAccountKey = p.Session["AzureAccountKey"].ToString();
                StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                CloudBlobContainer container = blobClient.GetContainerReference(pathName);
                var blockBlob = container.GetBlockBlobReference(fileName);
                if (!fileName.ToLower().Contains(".jpg") && !fileName.ToLower().Contains(".png"))
                    blockBlob.Properties.ContentType = "application/pdf";
                ms.Position = 0;
                blockBlob.UploadFromStream(ms);
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }
        public static bool UploadSkuFileOnSaveAs(string sku, string custId, string srcCustId, string type, Page p)
        {
            try
            {
                string myAccountName = p.Session["AzureAccountName"].ToString();
                string myAccountKey = p.Session["AzureAccountKey"].ToString();
                string containerName = p.Session["AzureContainerName"].ToString();
                string targetContainerName = p.Session["AzureContainerName"].ToString();
                string skuDir = p.Session["SkuReceiptDirectory"].ToString();
                CloudStorageAccount storageAccount = new CloudStorageAccount(new StorageCredentials(myAccountName, myAccountKey), true);
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer sourceContainer = cloudBlobClient.GetContainerReference(containerName);
                CloudBlobContainer targetContainer = cloudBlobClient.GetContainerReference(targetContainerName);
                string blobNameSrc = skuDir + @"/" + sku + @"_" + srcCustId + @"." + type;
                string blobNametgt = skuDir + @"/" + sku + @"_" + custId + @"." + type;
                CloudBlockBlob sourceBlob = sourceContainer.GetBlockBlobReference(blobNameSrc);
                CloudBlockBlob targetBlob = targetContainer.GetBlockBlobReference(blobNametgt);
                targetBlob.StartCopyFromBlob(sourceBlob);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}