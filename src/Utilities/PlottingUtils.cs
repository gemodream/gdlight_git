﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.UI;
using Corpt.Constants;
using System.Net;
using System.Web;
using System.Text;

namespace Corpt.Utilities
{
    public static class PlottingUtils
    {
        public static string GetProgramFileDir()
        {
            const string sProgramFileFolder = @"C:\PROGRAM FILES";
            string[] dirs = Directory.GetDirectories(@"c:\", "program files*");
            foreach (var dir in dirs.Where(dir => dir.Trim().ToUpper() != sProgramFileFolder))
            {
                return dir;
            }
            return sProgramFileFolder;
        }
        public static string CallPlotting(Page p, string sShapePath, string sItemNumber, string curPartName)
        {
            var sPlotFileName = sItemNumber + "." + curPartName;
            var tmpDir = p.Session[SessionConstants.GlobalShapeRoot].ToString();
            //var sShapeDir = p.MapPath(p.Session[SessionConstants.GlobalShapeRoot].ToString());
            var sShapeDir = p.Session[SessionConstants.GlobalShapeRoot].ToString();
            //var sShapeDir = p.Session[SessionConstants.GlobalShapeRoot].ToString();

            //-- Path to plotting dir    
            var sPathToPlotting = ("" + p.Session[SessionConstants.GlobalPlotDir]);
            if (string.IsNullOrEmpty(sPathToPlotting))
            {
                return "Parameter " + SessionConstants.GlobalPlotDir + " not found. (Global.asax)";
            }
            //var arguments = "\"" + sShapePath + "\" " + "\"" + sPlotFileName + "\"" + " " + sPathToPlotting + " " + sShapeDir;
            var arguments = "\"" + sShapePath + "\" " + "\"" + sPlotFileName + "\"" + " " + sPathToPlotting + " " + sShapeDir;
            StringWriter oStringWriter = new StringWriter();
            oStringWriter.WriteLine(arguments);
            p.Response.ContentType = "text/plain";

            p.Response.AddHeader("content-disposition", "attachment;filename=" + string.Format("{0}.Plotting.txt", sPlotFileName));
            p.Response.Clear();
            
            using (StreamWriter writer = new StreamWriter(p.Response.OutputStream, Encoding.UTF8))
            {
                writer.Write(oStringWriter.ToString());
            }
            
            p.Response.End();
            return "OK";
        }
        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }


    }
}