﻿using System.Web.UI;
using System.Xml;

namespace Corpt.Utilities
{
    public class XmlUtils
    {
        public static XmlDocument LoadXmlFormats(Page page)
        {
            return LoadXmlDocument("Formats.xml", page);
        }

        public static XmlDocument LoadXmlLabels(Page page)
        {
            return LoadXmlDocument("Labels.xml", page);
        }

        private static XmlDocument LoadXmlDocument(string fileName, Page page)
        {
            var xmlFormats = new XmlDocument();
            xmlFormats.Load(page.Server.MapPath(fileName));
            return xmlFormats;
            
        }

    }
}