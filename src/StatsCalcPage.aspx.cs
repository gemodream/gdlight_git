﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Models.Stats;
using Corpt.Models.Stats.FailStats;
using System.Data;

namespace Corpt
{
    public partial class StatsCalcPage : CommonPage
    {
        private static string ReportsList = "ReportsList";
        
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Rejected Stones";
            if (!IsPostBack)
            {
                TabContainer.ActiveTabIndex = 0;
                LoadOrderStates();
                LoadRulesTree();
                LoadCustomers();
                LoadCalcReports();
                OnChangedPeriodType(null, null);
                RemoveRptBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this Report?');");
                CalculateBtn.Attributes.Add("onclick", "return confirm('Are you sure you want to calculate new Report?');");
            }
        }
        #endregion
        
        #region Tab "New Report"

        #region Period Type
        private static string PeriodMonthly = "m";
        private static string PeriodWeekly = "w";
        private static string PeriodRandom = "r";
        protected void OnChangedPeriodType(object sender, EventArgs e)
        {
            var type = periodType.SelectedValue;
            var isRandom = (type == PeriodRandom);
            calFrom.Enabled = isRandom;
            calTo.Enabled = isRandom;
            if (isRandom) return;
            var today = DateTime.Today;
            var dateTo = PaginatorUtils.ConvertDateToString(today);
            var isWeekly = (type == PeriodWeekly);
            var dateFrom = PaginatorUtils.ConvertDateToString(isWeekly ? today.AddDays(-7) : today.AddMonths(-1));
            calFrom.Text = dateFrom;
            calTo.Text = dateTo;
        }
        #endregion

        #region Customers
        private void LoadCustomers()
        {
            var customers = QueryUtils.GetCustomers(this);
            SetViewState(customers, SessionConstants.CustomersList);
            SetViewState(new List<CustomerModel>(), SessionConstants.CustomersListSel);
        }
        private List<CustomerModel> GetSelCustomers()
        {
            return GetViewState(SessionConstants.CustomersListSel) as List<CustomerModel> ?? new List<CustomerModel>();
        }
        private List<CustomerModel> AddSelCustomers(List<CustomerModel> addCustomers)
        {
            var customers = GetSelCustomers();
            foreach (var customer in addCustomers)
            {
                if (customers.Find(m => m.CustomerId == customer.CustomerId) == null)
                {
                    customers.Add(customer);
                }
            }
            return customers;
        }
        private List<CustomerModel> DelSelCustomers(string customerId)
        {
            var customers = GetSelCustomers();
            var customer = customers.Find(m => m.CustomerId == customerId);
            if (customer != null)
            {
                customers.Remove(customer);
            }
            return customers;
        }
        protected void OnAddCustomerClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            if (string.IsNullOrEmpty(filterText)) return;
            var filtered = customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            if (filtered.Count == 0) return;
            var addCustomers = AddSelCustomers(filtered);
            CustomerGrid.DataSource = addCustomers;
            CustomerGrid.DataBind();
        }
        protected void OnDelCustomer(object source, DataGridCommandEventArgs e)
        {
            var customerId = "" + CustomerGrid.DataKeys[e.Item.ItemIndex];
            var customers = DelSelCustomers(customerId);
            CustomerGrid.DataSource = customers;
            CustomerGrid.DataBind();
        }
        protected void OnClearCustomerClick(object sender, EventArgs e)
        {
            var customers = GetSelCustomers();
            customers.Clear();
            CustomerGrid.DataSource = customers;
            CustomerGrid.DataBind();
        }
        private List<string> GetCheckedCustomers()
        {
            var ids = new List<string>();
            var customers = GetSelCustomers();
            foreach (var customer in customers)
            {
                ids.Add(customer.CustomerId);
            }
            return ids;
        }
        #endregion

        #region Rules Tree
        private void LoadRulesTree()
        {
            StatsUtils.StatsRuleTreeBuild(ReasonTree, false, this);
        }
        private List<string> GetCheckedMeasures()
        {
            var checkItems = new List<string>();
            foreach (TreeNode node in ReasonTree.CheckedNodes)
            {
                if (node.Depth != 2) continue;
                var reasonId = node.Value.Split('_')[1];
                checkItems.Add(reasonId);
            }
            return checkItems;
        }
        #endregion

        #region Order State Codes

        private void LoadOrderStates()
        {
            OrderStateList.DataSource = QueryUtils.GetOrderStateCodes(this);
            OrderStateList.DataBind();
            OrderStateList.SelectedIndex = 0;
        }
        public string GetOrderStateCode(string stateCode)
        {
            if (OrderStateList.Items == null) return "";
            foreach (ListItem item in OrderStateList.Items)
            {
                if (item.Value == stateCode) return item.Text;
            }
            return "";
        }
        #endregion


        #region New Report Parameters


        private StatsFilterModel GetNewReportParameters()
        {
            var dateFrom = PaginatorUtils.ConvertDateFromString(calFrom.Text);
            var dateTo = PaginatorUtils.ConvertDateFromString(calTo.Text);
            var filterModel = new StatsFilterModel();
            if (dateFrom != null) filterModel.DateFrom = (DateTime)dateFrom;
            if (dateTo != null) filterModel.DateTo = (DateTime)dateTo;
            filterModel.PeriodType = periodType.SelectedValue;
            filterModel.MeasureIds = GetCheckedMeasures();
            filterModel.CustomerIds = GetCheckedCustomers();
            filterModel.OrderStateCode = OrderStateList.SelectedValue;
            return filterModel;
        }
        #endregion

        #region Calculation Button Run
        protected void OnCalculateClick(object sender, EventArgs e)
        {
            var filter = GetNewReportParameters();
            var err = QueryUtils.StatsCalcReportRun(filter, this);
            if (!string.IsNullOrEmpty(err))
            {
                PopupInfoDialog(err, "New Report", true);
                return;
            }
            if (string.IsNullOrEmpty(err))
            {
                PopupInfoDialog("The calculation has been completed successfully", "New Report", false);
                LoadCalcReports();
                TabContainer.ActiveTabIndex = 1;
            }
        }

        #endregion

        #endregion

        #region Tab Reports

        #region Reports List 
        protected void OnReportListChanged(object sender, EventArgs e)
        {
            StatsReportModel report = null;
            var reportId = ReportsControl.SelectedValue;
            if (!string.IsNullOrEmpty(reportId))
            {
                var reports = GetViewState(ReportsList) as List<StatsReportModel> ?? new List<StatsReportModel>();
                report = reports.Find(m => m.ReportId == reportId);
                if (report == null) return;

                //-- Report Totals
                var rowTotals = QueryUtils.GetStatsReportTotals(reportId, this);
                GridTotals.DataSource = rowTotals;
                GridTotals.DataBind();

                //-- Report Totals Per Customer
                var rowsPerCustomer = QueryUtils.GetStatsReportTotalsPerCustomer(reportId, this);
                GridTotalsPerCustomer.DataSource = rowsPerCustomer;
                GridTotalsPerCustomer.DataBind();
                TotalsByCustomerLbl.Text = string.Format("Totals Per Customer ({0})", rowsPerCustomer.Count);
                
                //-- Parameters Panel
                LoadReportParameter(report);
                ReportsDiv.Visible = true;
            }
            else
            {
                GridTotalsPerCustomer.DataSource = null;
                GridTotalsPerCustomer.DataBind();

                GridTotals.DataSource = null;
                GridTotals.DataBind();
                HideReportParameterPanel();
                ReportsDiv.Visible = false;
            }

        }

        protected void OnRemoveRptClick(object sender, EventArgs e)
        {
            var reportId = ReportsControl.SelectedValue;
            if (string.IsNullOrEmpty(reportId)) return;
            var res = QueryUtils.StatsCalcReportRemove(reportId, this);
            if (!string.IsNullOrEmpty(res))
            {
            }
            if (string.IsNullOrEmpty(res))
            {
                PopupInfoDialog("Report deleted successfully", "Delete report", false);
                //Report deleted successfully
                LoadCalcReports();
                if (reportId == ReportIdField.Value)
                {
                    grdDetails.DataSource = null;
                    grdDetails.DataBind();
                    ReportDetailsPanel.Visible = false;
                }
            }

        }
        private void LoadCalcReports()
        {
            var reports = QueryUtils.GetStatsReports(this);
            SetViewState(reports, ReportsList);
            ReportsControl.DataSource = reports;
            ReportsControl.DataBind();
            if (reports.Count > 0) 
                ReportsControl.SelectedIndex = 0; 
            else 
                ReportsControl.SelectedValue = null;
            OnReportListChanged(null, null);
            RemoveRptBtn.Enabled = reports.Count() > 0;

        }
        #endregion

        #region Report Parameters Panel

        private void LoadReportParameter(StatsReportModel report)
        {
            ReportParamsPanel.Visible = true;
            LoadReportRuleTree(report.ReportDetails.Rules);
            LoadReportCustomerTree(report.ReportDetails.Customers);
            ExecuteTimeLbl.Text = string.Format("Execute Time: {0}", report.ExecutionTime);
            ReportOrderStateLbl.Text = string.Format("Order State: {0}", GetOrderStateCode(report.OrderStateCode));
            ReportPeriodLbl.Text = string.Format("{0} - {1}", report.ReportDetails.DisplayDateFrom, report.ReportDetails.DisplayDateTo);
        }

        private void LoadReportCustomerTree(List<string> customers)
        {
            ReportCustomerTree.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Report Customers" } };
            if (customers.Count == 0)
            {
                customers.Add("All");
            }
            foreach (var customer in customers)
            {
                data.Add(new TreeViewModel{ParentId="0", Id = customer, DisplayName = customer});
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            ReportCustomerTree.Nodes.Add(rootNode);
            //rootNode.ExpandAll();
        }

        private void LoadReportRuleTree(List<ReasonMeasureModel> rules)
        {
            var reasons = rules.GroupBy(m => m.ReasonName);
            ReportRuleTree.Nodes.Clear();

            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Report Struct" } };
            foreach (var reason in reasons)
            {
                data.Add(new TreeViewModel { ParentId = "0", Id = reason.Key, DisplayName = reason.Key });
                var linkMeasures = rules.FindAll(m => m.ReasonName == reason.Key);
                foreach (var linkMeasure in linkMeasures)
                {
                    data.Add(new TreeViewModel { ParentId = reason.Key, Id = reason.Key + "_" + linkMeasure.MeasureName, DisplayName = linkMeasure.MeasureName });
                }
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);

            TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
            ReportRuleTree.Nodes.Add(rootNode);
            rootNode.Expand();
        }
        
        private void HideReportParameterPanel()
        {
            ReportParamsPanel.Visible = false;
        }
        #endregion

        #region Totals Per Customer Grid (Sort, Buttons: Excel, Details)
        

        protected void OnExportBtnClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            var customerId = "" + GridTotalsPerCustomer.DataKeys[item.ItemIndex];
            StatsUtils.ExportToExcel(ReportsControl.SelectedValue, customerId, this);
        }
        
        private static int ColumnCustomerNameIdx = 3;
        
        protected void OnDetailsBtnClick(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).NamingContainer as DataGridItem;
            if (item == null) return;
            var customerId = "" + GridTotalsPerCustomer.DataKeys[item.ItemIndex];
            ReportCustomerLbl.Text = item.Cells[ColumnCustomerNameIdx].Text;
            CustomerIdFld.Value = customerId;
            var reportId = ReportsControl.SelectedValue;
            ReportTitleLbl.Text = ReportsControl.SelectedItem.Text;
            ReportIdField.Value = reportId;

            //-- Data for Filter PAnel
            var filterData = QueryUtils.GetStatsReportFilterSource(reportId, customerId, this);
            LoadFilterPanel(filterData);

            LoadReportDetailsGrid("1");
            ReportDetailsPanel.Visible = true;
            TabContainer.ActiveTabIndex = 2;
        }
        #endregion
        
        #endregion

        #region Tab 'Report Details'

        #region Filter Panel

        #region Filter Panel, Buttons
        protected void OnResetFilterClick(object sender, EventArgs e)
        {
            foreach (DataGridItem item in ReasonFilterGrid.Items)
            {
                var valurFld = item.FindControl(FilterValueControlName) as DropDownList;
                if (valurFld != null) valurFld.SelectedIndex = 0;
            }
            CpFilterList.SelectedIndex = 0;
            RejTypeFilterList.SelectedIndex = 0;
            ItemNumberFld.Text = "";
        }
        protected void OnApplyFilterClick(object sender, EventArgs e)
        {
            LoadReportDetailsGrid("1");
        }

        #endregion

        #region Filter Panel, Set data, Get Data
        private void LoadFilterPanel(StatsReportFilterSourceModel filterData)
        {
            //-- Customer Programs
            CpFilterList.DataSource = filterData.CustomerPrograms;
            CpFilterList.DataBind();
            CpFilterList.SelectedIndex = 0;

            //-- Reasons with values
            ReasonFilterGrid.DataSource = filterData.Reasons;
            ReasonFilterGrid.DataBind();

            //-- Rejection Types
            RejTypeFilterList.DataSource = filterData.RejectionTypes;
            RejTypeFilterList.DataBind();
            RejTypeFilterList.SelectedIndex = 0;
        }

        private StatsReportFilterSourceModel GetFilterFromPanel()
        {
            var filter = new StatsReportFilterSourceModel();

            //-- SKU
            var sp = CpFilterList.SelectedValue;
            if (sp != "Any" && sp != "")
            {
                filter.CpSelected = sp;
            }

            //-- Rejection Types
            var rejType = RejTypeFilterList.SelectedValue;
            if (!string.IsNullOrEmpty(rejType))
            {
                filter.RejectionTypeSelected = rejType;
            }

            //-- Order/Batch/Item number
            var itemNumber = ItemNumberFld.Text.Trim();
            if (!string.IsNullOrEmpty(itemNumber) && itemNumber.Length >= 5)
            {
                filter.ItemNumberSelected = itemNumber;
            }
            //-- Reasons and values
            filter.Reasons = GetReasonsFilterFromPanel();
            return filter;
        }
        #endregion

        #region Filter Reasons Grid
        private static string FilterValueControlName = "FilterValueFld";
        protected void OnFilterGridDataBound(object sender, DataGridItemEventArgs e)
        {
            //-- Load data on controls
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;
            var reasonModel = e.Item.DataItem as ReportFilterReasonModel;
            if (reasonModel == null) return;
            var valueFld = e.Item.FindControl(FilterValueControlName) as DropDownList;
            if (valueFld == null) return;
            valueFld.DataSource = reasonModel.Values;
            valueFld.DataBind();
            valueFld.SelectedIndex = 0;
        }
        private List<ReportFilterReasonModel> GetReasonsFilterFromPanel()
        {
            var reasons = new List<ReportFilterReasonModel>();
            foreach (DataGridItem item in ReasonFilterGrid.Items)
            {
                var valueFld = item.FindControl(FilterValueControlName) as DropDownList;
                if (valueFld == null) continue;
                if (valueFld.SelectedIndex == 0) continue;  // Any
                var reasonId = "" + ReasonFilterGrid.DataKeys[item.ItemIndex];
                reasons.Add(new ReportFilterReasonModel { ReasonId = reasonId, ValueSelected = valueFld.SelectedValue });
            }
            return reasons;
        }
        #endregion

        #endregion

        #region Report Details Grid
        private void LoadReportDetailsGrid(string pageNo)
        {
            var filter = GetFilterFromPanel();
            var customerId = CustomerIdFld.Value;
            var reportId = ReportIdField.Value;
            var pageSize = 500;
            try
            {
                pageSize = Convert.ToInt32(PageSizeFld.Text);
            }
            catch { }
            var reportPage = QueryUtils.GetStatsReportDetails(reportId, customerId, pageNo, pageSize, filter, this);

            //-- Pagination
            RowCountByCriteria.Text = reportPage.Pagination.RowCount;
            PageCountByCriteria.Text = reportPage.Pagination.PageCount;
            PageSizeFld.Text = reportPage.Pagination.PageSize;
            PageNoFld.Text = reportPage.Pagination.CurrPage;

            var firstRowOnPage = (Convert.ToInt32(reportPage.Pagination.CurrPage) - 1) * pageSize + 1;
            FirstRowNum.Value = "" + firstRowOnPage;
            
            var dt = reportPage.ReportPageTable;
            grdDetails.DataSource = dt;
            grdDetails.DataBind();

            DetailsQtyFld.Text = ""+ dt.Rows.Count;
        }

        protected void OnDetailsGridItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;
            var index = e.Item.ItemIndex;
            var firstNumber = Convert.ToInt32(FirstRowNum.Value);
            e.Item.Cells[0].Text = "" + (firstNumber + index);
        }

        protected void GrdDetailsSortCommand(object source, DataGridSortCommandEventArgs e)
        {
        }
        #endregion

        #region Paginator on Report Details

        protected void OnPageGoClick(object sender, EventArgs e)
        {
            var pageNo = Convert.ToInt32(PageNoFld.Text);
            var pageMax = Convert.ToInt32(PageCountByCriteria.Text);
            if (pageNo > pageMax || pageNo < 1) pageNo = 1;
            LoadReportDetailsGrid("" + pageNo);
        }

        protected void OnNextPageClick(object sender, EventArgs e)
        {
            var pageNo = Convert.ToInt32(PageNoFld.Text) + 1;
            var pageMax = Convert.ToInt32(PageCountByCriteria.Text);
            if (pageNo > pageMax) pageNo = 1;
            LoadReportDetailsGrid("" + pageNo);
        }

        protected void OnPrevPageClick(object sender, EventArgs e)
        {
            var pageNo = Convert.ToInt32(PageNoFld.Text) - 1;
            if (pageNo < 1) pageNo = 1;
            LoadReportDetailsGrid("" + pageNo);
        }
        #endregion


        #endregion


        #region Information Dialog
        private void PopupInfoDialog(string msg, string title, bool isErr)
        {
            InfoTitle.Text = title;
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }
        #endregion

    
    }
}