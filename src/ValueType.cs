using System;

namespace Corpt
{
	/// <summary>
	/// Summary description for ValueType.
	/// </summary>
	public abstract class ValueType
	{
		public static int ENUM = 10;
		public static int STRING = 20;
		public static int NUMBER = 30;
	}
}
