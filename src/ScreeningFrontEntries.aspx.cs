﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Utilities;
using Corpt.Models;
using System.Globalization;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using System.Collections;

namespace Corpt
{
    public partial class ScreeningFrontEntries : System.Web.UI.Page
    {
        public string memoNum = null, poNum = null, skuName = null, style = null;
        public string totalQty = null, bagQty = null, retailer = null;
        string loginName = null;
        public enum ResponseEndEnum //alex
        {
            EndResponse,
            ContinueResponse
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null)
                Response.Redirect("Login.aspx");
            if (false/*Session["ID"].ToString()!="10"*/)
                Response.Redirect("Login.aspx");
            btnPrint.Enabled = false;
            btnPrint1.Enabled = false;
            //this.Controls.Add(btnPrint1);
            // ClearText();
            //SyntheticScreeningModel data = new SyntheticScreeningModel();
            //data = (SyntheticScreeningModel)Session["ScreeningData"];
            //txtSku.Text = data.SKUName;
            //txtCustomerCode.Text = data.CustomerName;
        }//Page_Load

        protected void OnLoadClick(object sender, EventArgs e)
        {
            ClearText("requestId");
            LoadExecute();
        }

        protected void OnCustCodeClick(object sender, EventArgs e)
        {
            string tmpcode = txtCustomerCode.Text;
            ClearText("all");
            txtCustomerCode.Text = tmpcode;
            //LoadExecute();
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Text, this);
            if (ds != null && ds.Tables[0].Rows.Count != 0)
            {
                DataRow custDr = ds.Tables[0].Rows[0];
                txtCustomerName.Text = custDr["CompanyName"].ToString();
                txtCustomerID.Value = custDr["CustomerID"].ToString();
            }
            SetMessengersList();
            
        }

        private void LoadExecute()
        {
            string requestId = RequestIDBox.Text;
            if (requestId == null || requestId == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter request ID properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticCustomerEntries(requestId, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this request ID');", true);
            else
                PopulateFrontScreenRequestID(ds);

            //Response.Redirect("ScreeningFrontEntries.aspx");
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            //txtCustomerID.Value = @"c:\temp\abc.pdf";
            return;
        }
        protected void btnSavefront_Click(object sender, EventArgs e)
        {
            btnSave.UseSubmitBehavior = false; //alex
            btnSave.OnClientClick = "_spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true; ";
            bool multibatch = false;
            if (txtMemoNum.Text == "" || txtMemoNum.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Text == "" || txtQuantity.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode.Text == "" || txtCustomerCode.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (txtCustomerName.Text == "" || txtCustomerName.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Name');", true);
                return;
            }
            if (messengerListBox.SelectedItem.Text == "" || messengerListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Messenger');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string[] batchMemos = null;
            if (Textarea1.Value != null && Textarea1.Value != "")
            {
                string memoStr = Textarea1.Value.Replace("\r", "");
                batchMemos = memoStr.Split('\n');
                multibatch = true;
            }
            string rid = null;
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int currentOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int itemsQuantity = Convert.ToInt32(txtQuantity.Text);
            int serviceTypeId = 7;
            string memo = txtMemoNum.Text;
            string specialInstruction = specialInstructionsListBox.SelectedItem.Text;
            int personCustomerOfficeId = authorOfficeId;
            int personCustomerId = Convert.ToInt32(txtCustomerID.Value);
            int? personId = GSIAppQueryUtils.GetPersonID(txtCustomerCode.Text, messengerListBox.SelectedItem.Text, this);
            int customerOfficeId = authorOfficeId;
            int customerId = Convert.ToInt32(txtCustomerID.Value);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.CreateScreeningNewOrder(authorId, authorOfficeId, currentOfficeId, groupOfficeId, 
                itemsQuantity, serviceTypeId, memo, specialInstruction, 
                personCustomerOfficeId, personCustomerId, personId, customerOfficeId, 
                customerId, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order not created.');", true);
            else
            {
                string sGroupCode = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                txtGSIOrder.Text = sGroupCode;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Group Code " + sGroupCode + " created.');", true);
                string groupId = null;
                DataSet dsGroupId = new DataSet();
                dsGroupId = GSIAppQueryUtils.GetGroupId(sGroupCode, this);
                if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
                {
                    groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                bool added = GSIAppQueryUtils.SetMemoNumber(groupId, memo, authorId, authorOfficeId, currentOfficeId, this);
                if (!added)
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo was not added.');", true);
                else
                {
                    bool synthOrderAdded = true;
                    synthOrderAdded = GSIAppQueryUtils.AddOrderToSynthScreening(sGroupCode, specialInstruction, txtPONum.Text, txtQuantity.Text, txtSku.Text, 
                        txtStyle.Text, txtMemoNum.Text, txtCustomerCode.Text, this);

                }
                //if batch area is not empty add batches
                if (batchMemos != null)
                {
                    foreach (string batch in batchMemos)
                    {
                        string[] batchElements = batch.Split('/');
                        string batchNumber = batchElements[0];
                        string batchItemsNumber = batchElements[1];
                        string customerCodeName = "Screening";
                        DataSet cpDs = GSIAppQueryUtils.GetCPInfo(customerCodeName, txtCustomerCode.Text, this);
                        if (cpDs != null && cpDs.Tables[0].Rows.Count != 0)
                        {
                            DataRow dr = cpDs.Tables[0].Rows[0];
                            string itemTypeId = dr["ItemTypeID"].ToString();
                            string cpid = dr["CPID"].ToString();

                            string batchId = GSIAppQueryUtils.AddSyntheticBatch(authorId, authorOfficeId, currentOfficeId, groupId,
                                itemTypeId, batchItemsNumber, cpid, null, this);
                            //itemTypeId, batchItemsNumber, cpid, batchMemo, this);
                            if (batchId == null)
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Batch was not created');", true);
                            else
                            {
                                bool synthBatchAdded = GSIAppQueryUtils.AddBatchToSynthScreening(sGroupCode, specialInstruction, txtPONum.Text, batchItemsNumber, txtSku.Text, 
                                    txtStyle.Text, txtMemoNum.Text, batchNumber, txtCustomerCode.Text, this);
                            }
                        }
                    }
                }
                if (RequestIDBox.Text != "" && RequestIDBox.Text != null)
                {
                    string result = GSIAppQueryUtils.UpdateCustomerData(Convert.ToInt32(RequestIDBox.Text), memo, Convert.ToInt32(sGroupCode),
                        Convert.ToInt32(txtCustomerCode.Text), txtPONum.Text,
                        txtSku.Text, Convert.ToInt32(txtQuantity.Text), txtStyle.Text, 
                        specialInstructionsListBox.SelectedItem.Text, messengerListBox.SelectedItem.Text, this);
                    if (result == "success")
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer Data was updated.');", true);
                    else if (result == "failed")
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Failed to upodate Customer Data.');", true);
                }
                btnPrint.Enabled = true;
                btnPrint1.Enabled = true;
                onOrderUpdate();
                //PrintTwoLabels();
                // LoadExecute();
            }


        }//savefront


        
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearText("all");
        }//btnClear_Click

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(@"Middle.aspx");
        }
        protected void btnSearch1_Click(object sender, EventArgs e)
        {

        }
        /*
            protected void btnSave_Click(object sender, EventArgs e)
        {
            var personId = GSIAppQueryUtils.GetPersonID(txtCustomerCode.Text, messengerListBox.SelectedItem.Text, this);
            DataSet ds = new DataSet();
            //ds = GSIAppQueryUtils.CreateScreeningNewOrder();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int customerOfficeId = authorOfficeId;
            int personCustomerOfficeId = authorOfficeId;
            int personCustomerId = Convert.ToInt32(txtCustomerID.Value);



            
             [12/5/2018 5:24:06 PM]EntryBatch: PersonCustomerOfficeID_PersonCustomerID_PersonID=1_23_54; 
             CustomerOfficeID_CustomerID=1_23; VendorOfficeID_VendorID=; PersonID=; PersonCustomerOfficeID=; 
             PersonCustomerID=; GroupOfficeID=; GroupID=; GroupCode=; OrderCode=; IsIQInspected=0; IsTWInspected=2; 
             IsMemo=1; ItemsQuantity=1000; TotalWeight=; MeasureUnitID=; ServiceTypeID=7; Memo=ALEXTEST; 
             SpecialInstruction=FM; CarrierID=; CarrierTrackingNumber=; CustomerOfficeID=; CustomerID=; VendorOfficeID=; VendorID=; 
             
             [12/5/2018 5:24:06 PM]dbo.spsetEntryBatch: @AuthorId=261; @AuthorOfficeId=1; @CurrentOfficeId=1; @rId=; 
             @PersonID=54; @PersonCustomerOfficeID=1; @PersonCustomerID=23; @GroupOfficeID=; @GroupID=; @GroupCode=; 
             @OrderCode=; @IsIQInspected=0; @IsTWInspected=2; @IsMemo=1; @ItemsQuantity=1000; @TotalWeight=; @MeasureUnitID=;
             @ServiceTypeID=7; @Memo=ALEXTEST; @SpecialInstruction=FM; @CarrierID=; @CarrierTrackingNumber=; @CustomerOfficeID=1; 
             @CustomerID=23; @VendorOfficeID=; @VendorID=; 
             *
            /* alex
            loginName = (string)HttpContext.Current.Session["LoginName"];
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            bagQty = txtBagQty.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            int orderCount = IsOrderExist(txtMemoNum.Text);
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 2)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo already exists. Press UPDATE');", true);
            else if (orderCount == 0)
            {
                string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                if (custCode != null)
                {
                    string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, bagQty, retailer, this, 0, custCode, null);
                    if (status == "failed")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                        if (status == "failed")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                            bool printed = true;// GSIAppQueryUtils.PrintCustomerLabel(memoNum, poNum, skuName, totalQty, bagQty, retailer, custCode, this);
                            if (printed)
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                            else
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                            //ClearText();
                        }

                        //ClearText();
                    }
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Customer does not exist. Contact GSI administrator');", true);
            }
            
    }//btnSave_Click
    
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            memoNum = txtMemoNum.Text.Trim();
            poNum = txtPONum.Text.Trim();
            skuName = txtSku.Text.Trim();
            totalQty = txtQuantity.Text.Trim();
            //bagQty = txtBagQty.Text.Trim();
            retailer = txtRetailer.Text.Trim();
            style = txtStyle.Text.Trim();
            bagQty = null;
            int orderCount = IsOrderExist(memoNum);
            if (orderCount == 1)//order number exists
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Order already exists. User another memo number');", true);
            }
            else if (orderCount == -1)//problem with saving
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Problem saving. Try again.');", true);
            else if (orderCount == 0)//subject for update
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo does not exist. Press ADD NEW');", true);
            else if (orderCount == 2)//save record
            {
                string status = GSIAppQueryUtils.SaveSyntheticCustomerData(memoNum, poNum, skuName, totalQty, style, retailer, this, 1, loginName, null);
                if (status == "failed")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo " + memoNum + " not saved.');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Memo saved successfully.');", true);
                    string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                    bool printed = true;// GSIAppQueryUtils.PrintCustomerLabel(memoNum, poNum, skuName, totalQty, bagQty, retailer, custCode, this);
                    if (printed)
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Label generated successfully.');", true);
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error generating label.');", true);
                    //ClearText();
                }
            }
        }//btnUpdate_Click
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            memoNum = txtMemoNum.Text.Trim();
            loginName = (string)HttpContext.Current.Session["LoginName"];
            int orderCount = IsOrderExist(memoNum);
            if (orderCount > 0)
            {
                string custCode = GSIAppQueryUtils.GetSyntheticCustomer(loginName, this);
                DataSet ds = new DataSet();
                bool found = false;
                ds = GSIAppQueryUtils.GetSyntheticRecordByMemo(memoNum, this);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    DataRow dr;
                    dr = ds.Tables[0].Rows[0];
                    txtPONum.Text = dr["PONum"].ToString();
                    txtSku.Text = dr["SKUName"].ToString();
                    txtQuantity.Text = dr["TotalQty"].ToString();
                    //txtBagQty.Text = dr["BagQty"].ToString();
                    txtRetailer.Text = dr["Retailer"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Record found.');", true);
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error finding customer data.');", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No order found.');", true);

        }//search
        */
        protected void specialInstructionsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRetailer.Text = specialInstructionsListBox.SelectedItem.Text;
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //PrintExternalLabel();
            PrintTakeInLabel();
        }//btnPrint_Click
        protected void btnPrint1_Click(object sender, EventArgs e)
        {
            //PrintTwoLabels();
            PrintExternalLabel();
            //PrintTakeInLabel();
        }//btnPrint_Click
         /* alex
         ClearText();
         txtCreateDateSearch.Value = string.Empty;
         txtToCreateDateSearch.Value = string.Empty;
         gsiOrderFilter.Value = string.Empty;
         grdScreening.PageIndex = 0;
         grdScreening.SelectedIndex = -1;
         BindGrid(ddlDestinationSearch.SelectedItem.Value.Trim());
         gvTitle.InnerText = "Screening Result - For orders having destination " + ddlDestinationSearch.SelectedItem.Value;
         alex */
        private void PrintTwoLabels()
        {
            //PrintExternalLabel();
            //PrintTakeInLabel();
            //return;
            if (txtGSIOrder.Text == "" || txtGSIOrder.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Text == "" || txtMemoNum.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Text == "" || txtQuantity.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode.Text == "" || txtCustomerCode.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string groupId = null;
            var gsiOrder = txtGSIOrder.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_TwoLabels_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;

            var totalQ = txtQuantity.Text;
            var memo = txtMemoNum.Text;
            var custCode = txtCustomerCode.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            DataSet dsGroupId = new DataSet();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            dsGroupId = GSIAppQueryUtils.GetGroupId(gsiOrder, this);
            if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
            {
                groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            DataSet dsGroup = new DataSet();
            dsGroup = GSIAppQueryUtils.GetGroupWithCustomer(groupId, authorId, authorOfficeId, groupOfficeId, this);
            DataRow rGroup = null;
            if (dsGroup != null && dsGroup.Tables[0].Rows.Count != 0)
                rGroup = dsGroup.Tables[0].Rows[0];
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer');", true);
                return;
            }
            string officeCode = rGroup["OfficeCode"].ToString();
            string customerName = rGroup["CustomerName"].ToString();
            string notInspectedQuantity = txtQuantity.Text;
            string address = rGroup["Address1"].ToString() + " " + rGroup["Address2"].ToString();
            string city = rGroup["City"].ToString();
            string zip = FillToFiveChars(rGroup["Zip1"].ToString());
            string stateName = rGroup["USStateName"].ToString();
            string country = rGroup["Country"].ToString();
            string phone = rGroup["CountryPhoneCode"].ToString() + " " + rGroup["Phone"].ToString();
            string fax = rGroup["CountryFaxCode"].ToString() + " " + rGroup["Fax"].ToString();
            DataSet dsAuthor = GSIAppQueryUtils.GetAuthorData(groupId, authorId, groupOfficeId, authorOfficeId, this);
            if (dsAuthor == null || dsAuthor.Tables[0].Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Author');", true);
                return;
            }
            string name = dsAuthor.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsAuthor.Tables[0].Rows[0]["LastName"];
            string sPersonID = dsGroup.Tables[0].Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsGroup.Tables[0].Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsGroup.Tables[0].Rows[0]["PersonCustomerOfficeID"].ToString();
            string messenger = messengerListBox.SelectedItem.Text;
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("labels");
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");//6
                writer.WriteElementString("customerName", customerName);//7
                writer.WriteElementString("notInspectedQuantity", notInspectedQuantity);//8
                writer.WriteElementString("address", address);//9
                writer.WriteElementString("city", city);//10
                writer.WriteElementString("zip", zip);//11
                writer.WriteElementString("stateName", stateName);//12
                writer.WriteElementString("country", country);//13
                writer.WriteElementString("phone", phone);//14
                writer.WriteElementString("fax", fax);//15
                writer.WriteElementString("messenger", messenger);//16
                writer.WriteElementString("name", name);//17

                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                
                writer.WriteStartElement("label1");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");
                if (Textarea1.Value != null && Textarea1.Value != "")
                {
                    string[] batchMemos = null;
                    string memoStr = Textarea1.Value.Replace("\r", "");
                    batchMemos = memoStr.Split('\n');
                    string batchInfo = null;
                    foreach (string batch in batchMemos)
                    {
                        string[] batchElements = batch.Split('/');
                        string batchItemsNumber = batchElements[1];
                        batchInfo += batchItemsNumber + ",";
                    }
                    writer.WriteElementString("batches", batchInfo);//6
                }
                else
                    writer.WriteElementString("batches", "no batches");//6
                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            DownloadExcelFile(filename, this);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
        }//PrintTwoLabels()
        private void PrintExternalLabel()
        {
            if (txtGSIOrder.Text == "" || txtGSIOrder.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Text == "" || txtMemoNum.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Text == "" || txtQuantity.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode.Text == "" || txtCustomerCode.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            string groupId = null;
            var gsiOrder = txtGSIOrder.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_External_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;
            
            var totalQ = txtQuantity.Text;
            var memo = txtMemoNum.Text;
            var custCode = txtCustomerCode.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            DataSet dsGroupId = new DataSet();
            int authorId = Convert.ToInt32(this.Session["ID"]);
            int authorOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            int groupOfficeId = Convert.ToInt32(Session["AuthorOfficeID"]);
            dsGroupId = GSIAppQueryUtils.GetGroupId(gsiOrder, this);
            if (dsGroupId != null && dsGroupId.Tables[0].Rows.Count != 0)
            {
                groupId = dsGroupId.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            DataSet dsGroup = new DataSet();
            dsGroup = GSIAppQueryUtils.GetGroupWithCustomer(groupId, authorId, authorOfficeId, groupOfficeId, this);
            DataRow rGroup = null;
            if (dsGroup != null && dsGroup.Tables[0].Rows.Count != 0)
                rGroup = dsGroup.Tables[0].Rows[0];
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer');", true);
                return;
            }
            string officeCode = rGroup["OfficeCode"].ToString();
            string customerName = rGroup["CustomerName"].ToString();
            string notInspectedQuantity = txtQuantity.Text;
            string address = rGroup["Address1"].ToString() + " " + rGroup["Address2"].ToString();
            string city = rGroup["City"].ToString();
            string zip = FillToFiveChars(rGroup["Zip1"].ToString());
            string stateName = rGroup["USStateName"].ToString();
            string country = rGroup["Country"].ToString();
            string phone = rGroup["CountryPhoneCode"].ToString() + " " + rGroup["Phone"].ToString();
            string fax = rGroup["CountryFaxCode"].ToString() + " " + rGroup["Fax"].ToString();
            DataSet dsAuthor = GSIAppQueryUtils.GetAuthorData(groupId, authorId, groupOfficeId, authorOfficeId, this);
            if (dsAuthor == null || dsAuthor.Tables[0].Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Author');", true);
                return;
            }
            string name = dsAuthor.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsAuthor.Tables[0].Rows[0]["LastName"];
            string sPersonID = dsGroup.Tables[0].Rows[0]["PersonID"].ToString();
            string sPersonCustomerID = dsGroup.Tables[0].Rows[0]["PersonCustomerID"].ToString();
            string sPersonCustomerOfficeID = dsGroup.Tables[0].Rows[0]["PersonCustomerOfficeID"].ToString();
            string messenger = messengerListBox.SelectedItem.Text;
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", gsiOrder);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");//6
                writer.WriteElementString("customerName", customerName);//7
                writer.WriteElementString("notInspectedQuantity", notInspectedQuantity);//8
                writer.WriteElementString("address", address);//9
                writer.WriteElementString("city", city);//10
                writer.WriteElementString("zip", zip);//11
                writer.WriteElementString("stateName", stateName);//12
                writer.WriteElementString("country", country);//13
                writer.WriteElementString("phone", phone);//14
                writer.WriteElementString("fax", fax);//15
                writer.WriteElementString("messenger", messenger);//16
                writer.WriteElementString("name", name);//17

                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Flush();
                writer.Close();
                
            }
            
            DownloadExcelFile1(filename, this);
        }//PrintExternalLabel
        private void PrintTakeInLabel()
        {
            if (txtGSIOrder.Text == "" || txtGSIOrder.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for GSI Order');", true);
                return;
            }
            if (txtMemoNum.Text == "" || txtMemoNum.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Memo');", true);
                return;
            }
            if (txtQuantity.Text == "" || txtQuantity.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Total Quantity');", true);
                return;
            }
            if (txtCustomerCode.Text == "" || txtCustomerCode.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Customer Code');", true);
                return;
            }
            if (specialInstructionsListBox.SelectedItem.Text == "" || specialInstructionsListBox.SelectedItem.Text == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for Retailer');", true);
                return;
            }
            var gsiOrder = txtGSIOrder.Text;
            //var dir = GetExportDirectory(this);
            string filename = @"label_Front_Take_In_" + gsiOrder + ".xml";
            var retailer = specialInstructionsListBox.SelectedItem.Text;
            var groupCode = txtGSIOrder.Text;
            var totalQ = txtQuantity.Text;
            var memo = txtMemoNum.Text;
            var custCode = txtCustomerCode.Text;
            string dateTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            var stream = new MemoryStream();
            //using (XmlWriter writer = XmlWriter.Create(dir + filename))
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("label");
                writer.WriteElementString("memo", memo); //0
                writer.WriteElementString("groupCode", groupCode);//1
                writer.WriteElementString("customerCode", custCode); //2
                writer.WriteElementString("totalQ", totalQ);//3

                writer.WriteElementString("retailer", retailer);//4
                writer.WriteElementString("dateStamp", dateTime);//5
                writer.WriteElementString("serviceType", "Screening");
                if (Textarea1.Value != null && Textarea1.Value != "")
                {
                    string[] batchMemos = null;
                    string memoStr = Textarea1.Value.Replace("\r", "");
                    batchMemos = memoStr.Split('\n');
                    string batchInfo = null;
                    foreach (string batch in batchMemos)
                    { 
                        batchInfo += batch + ";";
                    }
                    writer.WriteElementString("batchInfo", batchInfo.Substring(0, batchInfo.Length-1));//6
                }
                else
                    writer.WriteElementString("batchInfo", "no batches");//6
                //writer.WriteElementString("bagQ", bagQ);

                //writer.WriteElementString("screening", screen);//7
                //writer.WriteElementString("test", test);//8
                writer.WriteEndElement();
                writer.WriteEndDocument();
                
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            //var Memory = new MemoryStream();
            //Memory.Position = 0;
            //writer.Write(Memory);
            //p.Session["Memory"] = Memory;
            //DownloadExcelFile(filename, this);
            
        }//PrintTakeInLabel

        public void DownloadExcelFile1(String filename, Page p)
        {
            
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            //p.Response.AddHeader("Refresh", "0.1");
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.End();
        }
        public void DownloadExcelFile(String filename, Page p)
        {
            
            //p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
            //string redirect = "<script>location.assign(window.location);</script>";
            //p.Response.Write(redirect);
            p.Response.Clear();
            //p.Response.Buffer = true; //alex
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=" + filename));
            //p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
            //p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.OutputStream.Flush();//alex
            //p.Response.OutputStream.Close();//alex
            //p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.Flush(); //alex
            //p.Response.Redirect("ScreeningFrontEntries.aspx");
            try
            {
                //SyntheticScreeningModel data = new SyntheticScreeningModel();
                //data.ScreeningID = 123;
                //data.SKUName = txtSku.Text;
                //data.CustomerName = txtCustomerName.Text;
                //Session["ScreeningData"] = data;
                // Context.Items.Add("abc", "xyz");
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("ScreeningFrontEntries.aspx", true);
                //Response.Redirect(Request.RawUrl);
                //Context.Items.Add(txtGSIOrder, "abc");
                //Server.Transfer("ScreeningFrontEntries.aspx", false);
                //string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
                //Response.Redirect(navigate);
                p.Response.End();
            }
            catch (Exception ex)
            {
               // string abc = ex.Message;
               // txtGSIOrder.Text = "abc";
                
            }
        }

        public static string GetExportDirectory(Page p)
        {
            var dir = p.Session["TempDir"] + p.Session.SessionID + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        protected void txtGSIOrder_TextChanged(object sender, EventArgs e)
        {
            ClearText("order");
            string groupCode = txtGSIOrder.Text;
            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }
        protected void OnOrderClick(object sender, EventArgs e)
        {
            ClearText("order");
            string groupCode = txtGSIOrder.Text;
            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }

        protected void onOrderUpdate()
        {
            string groupCode = txtGSIOrder.Text;
            
            if (groupCode == null || groupCode == null)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Enter order properly');", true);
            DataSet ds = new DataSet();
            ds = GSIAppQueryUtils.GetSyntheticEntriesByOrder(groupCode, this);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error retrieving order info');", true);
            else if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "no entries")
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No entries for this order');", true);
            else
                PopulateFrontScreen(ds);
            btnPrint.Enabled = true;
            btnPrint1.Enabled = true;
            return;
        }//onOrderUpdate
        private void PopulateFrontScreen(DataSet ds)
        {
            string batch = null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr.Table.Columns.Contains("Batch"))
                    batch = dr["Batch"].ToString();
                if (batch == null || batch == "")
                {
                    if (!dr.IsNull("Memo"))
                        txtMemoNum.Text = dr["Memo"].ToString();
                    if (!dr.IsNull("PONum"))
                        txtPONum.Text = dr["PONum"].ToString();
                    if (!dr.IsNull("TotalQty"))
                        txtQuantity.Text = dr["TotalQty"].ToString();
                    //if (!dr.IsNull("BagQty"))
                    //    txtBagQty.Text = dr["BagQty"].ToString();
                    if (!dr.IsNull("SKUName"))
                        txtSku.Text = dr["SKUName"].ToString();
                    if (!dr.IsNull("StyleName"))
                        txtStyle.Text = dr["StyleName"].ToString();

                    if (!dr.IsNull("Destination"))
                    {
                        string retailer = dr["Destination"].ToString();
                        for (int i = 0; i <= specialInstructionsListBox.Items.Count - 1; i++)
                        {
                            if (specialInstructionsListBox.Items[i].Value == retailer)
                            {
                                specialInstructionsListBox.SelectedIndex = i;
                                break;
                            }

                        }
                    }
                    //specialInstructionsListBox.SelectedItem.Text = dr["Destination"].ToString();
                    //specialInstructionsListBox.Items.Add(dr["Destination"].ToString());
                    //specialInstructionsListBox.SelectedItem.Value = specialInstructionsListBox.Items[0].Value;
                    if (!dr.IsNull("GSIOrder"))
                        txtGSIOrder.Text = dr["GSIOrder"].ToString();
                    //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                    if (!dr.IsNull("VendorNum"))
                    {
                        txtCustomerCode.Text = dr["VendorNum"].ToString();
                        DataSet custDs = new DataSet();
                        custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Text, this);
                        if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                        {
                            DataRow custDr = custDs.Tables[0].Rows[0];
                            if (!custDr.IsNull("CompanyName"))
                                txtCustomerName.Text = custDr["CompanyName"].ToString();
                            if (!custDr.IsNull("CustomerID"))
                                txtCustomerID.Value = custDr["CustomerID"].ToString();
                        }
                        
                        SetMessengersList();
                        if (ds.Tables.Count > 1)
                        {
                            string messenger = ds.Tables[1].Rows[0].ItemArray[0].ToString();
                            for (int j= 0; j<= messengerListBox.Items.Count - 1; j++)
                            {
                                if (messengerListBox.Items[j].Text == messenger)
                                    messengerListBox.SelectedIndex = j;
                            }
                        }
                    }
                    /*
                    if (!dr.IsNull("Destination"))
                    {
                        foreach (var specInstr in specialInstructionsListBox.Items)
                        {
                            if (specInstr.ToString() == dr["Retailer"].ToString())
                            {
                                specialInstructionsListBox.SelectedItem.Text = specInstr.ToString();
                                break;
                            }
                        }
                    }
                    */
                }
                else
                {
                    //string batchNum = dr["Batch"].ToString();
                    //string batchItemsNum = dr["TotalQty"].ToString();
                    //Textarea1.Value += batchNum + @"/" + batchItemsNum + "\n";
                }

                /*
                if (!dr.IsNull("CustomerCode"))
                    txtCustomerCode.Text = dr["CustomerCode"].ToString();
                DataSet custDs = new DataSet();
                custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Text, this);
                if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                {
                    DataRow custDr = custDs.Tables[0].Rows[0];
                    if (!custDr.IsNull("CompanyName"))
                        txtCustomerName.Text = custDr["CompanyName"].ToString();
                    if (!custDr.IsNull("CustomerID"))
                        txtCustomerID.Value = custDr["CustomerID"].ToString();
                }
                */
            }//foreach
            //SetMessengersList();
            /*
            DataSet messengerDs = new DataSet();
            messengerDs = GSIAppQueryUtils.GetMessengersList(txtCustomerCode.Text, this);
            if (messengerDs != null && messengerDs.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow messengerDr in messengerDs.Tables[0].Rows)
                {
                    string firstName = messengerDr["FirstName"].ToString();
                    string lastName = messengerDr["LastName"].ToString();
                    messengerListBox.Items.Add(firstName + " " + lastName);
                }
            }
            */
        }//PopulateFrontScreen

        private void PopulateFrontScreenRequestID(DataSet ds)
        {
            string batch = null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                    if (!dr.IsNull("MemoNum"))
                        txtMemoNum.Text = dr["MemoNum"].ToString();
                    if (!dr.IsNull("PONum"))
                        txtPONum.Text = dr["PONum"].ToString();
                    if (!dr.IsNull("TotalQty"))
                        txtQuantity.Text = dr["TotalQty"].ToString();
                    //if (!dr.IsNull("BagQty"))
                    //    txtBagQty.Text = dr["BagQty"].ToString();
                    if (!dr.IsNull("SKUName"))
                        txtSku.Text = dr["SKUName"].ToString();
                    if (!dr.IsNull("StyleName"))
                        txtStyle.Text = dr["StyleName"].ToString();

                if (!dr.IsNull("Retailer"))
                {
                    string retailer = dr["Retailer"].ToString();
                    for(int i=0; i<= specialInstructionsListBox.Items.Count-1; i++)
                    {
                        if (specialInstructionsListBox.Items[i].Value.ToLower() == retailer.ToLower())
                        {
                            specialInstructionsListBox.SelectedIndex = i;
                            break;
                        }
                          
                    }
                    //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                    //specialInstructionsListBox.Items.Add(dr["Retailer"].ToString());
                    //specialInstructionsListBox.SelectedItem.Value = specialInstructionsListBox.Items[0].Value;
                }
                    if (!dr.IsNull("GSIOrder"))
                        txtGSIOrder.Text = dr["GSIOrder"].ToString();
                    //specialInstructionsListBox.SelectedItem.Text = dr["Retailer"].ToString();
                    if (!dr.IsNull("CustomerCode"))
                    {
                        txtCustomerCode.Text = dr["CustomerCode"].ToString();
                        DataSet custDs = new DataSet();
                        custDs = GSIAppQueryUtils.GetCustomerName(txtCustomerCode.Text, this);
                        if (custDs != null && custDs.Tables[0].Rows.Count != 0)
                        {
                            DataRow custDr = custDs.Tables[0].Rows[0];
                            if (!custDr.IsNull("CompanyName"))
                                txtCustomerName.Text = custDr["CompanyName"].ToString();
                            if (!custDr.IsNull("CustomerID"))
                                txtCustomerID.Value = custDr["CustomerID"].ToString();
                        }
                    }
                SetMessengersList();
                if (!dr.IsNull("Messenger"))
                {
                    string messenger = dr["Messenger"].ToString();
                    for (int i = 0; i <= messengerListBox.Items.Count - 1; i++)
                    {
                        if (messengerListBox.Items[i].Value.ToLower() == messenger.ToLower())
                        {
                            messengerListBox.SelectedIndex = i;
                            break;
                        }

                    }
                }
            }//foreach

                
        }//PopulateFrontScreenRequestID
        private void SetMessengersList()
        {
            DataSet messengerDs = new DataSet();
            messengerDs = GSIAppQueryUtils.GetMessengersList(txtCustomerCode.Text, this);
            if (messengerDs != null && messengerDs.Tables[0].Rows.Count != 0)
            {
                messengerListBox.Items.Add("");
                foreach (DataRow messengerDr in messengerDs.Tables[0].Rows)
                {
                    string firstName = null, lastName = null;
                    if (!messengerDr.IsNull("FirstName"))
                        firstName = messengerDr["FirstName"].ToString();
                    if (!messengerDr.IsNull("LastName"))
                        lastName = messengerDr["LastName"].ToString();
                    messengerListBox.Items.Add(firstName + " " + lastName);
                }
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No messengers found.');", true);
        }
        private int IsOrderExist(string memoNum)
        {
            return GSIAppQueryUtils.GetScreeningByMemo(memoNum, this);
        }

        private void ClearText(string allSet)
        {
            txtMemoNum.Text = string.Empty;
            txtPONum.Text = string.Empty;
            //txtBagQty.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtSku.Text = string.Empty;
            txtStyle.Text = string.Empty;
            txtCustomerCode.Text = string.Empty;
            txtCustomerName.Text = string.Empty;
            txtCustomerID.Value = string.Empty;
            if (allSet != "requestId")
                RequestIDBox.Text = string.Empty;
            Textarea1.Value = string.Empty;
            if (allSet != "order")
                txtGSIOrder.Text = string.Empty;
            specialInstructionsListBox.SelectedIndex = -1;
            messengerListBox.Items.Clear();
        }

        private static string FillToFiveChars(string sNumber)
        {
            while (sNumber.Length < 5)
                sNumber = "0" + sNumber;
            return sNumber;
        }
    }
}