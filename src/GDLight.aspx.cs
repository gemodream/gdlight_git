﻿using System;
using System.Data;
using System.Data.SqlClient;
using Corpt.Utilities;

namespace Corpt
{
    public partial class GDLight : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");

            //-- Load Measures enum
            var conn = new SqlConnection("" + Session["MyIP_ConnectionString"]);
            var command = new SqlCommand("spGetMeasureValues") { Connection = conn, CommandType = CommandType.StoredProcedure };
            command.Parameters.AddWithValue("@AuthorID", "" + Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + Session["AuthorOfficeID"]);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            Session["GDLight_EnumMeasures"] = dt.Copy();

            //-- State Buttons
            var userId = int.Parse("" + Session["ID"]);
            cmdReAss.Enabled = EnableReAss(userId);
            cmdBulkUpdate.Enabled = EnableBulk(userId);
            cmdAutoMeasure.Enabled = EnableAutoMeasure(userId);
        }
        private bool EnableReAss(int userId)
        {
            var errMsg = "";
            return
                AccessUtils.HasAccessToPage("" + userId, AccessUtils.PrgReassembly, this, out errMsg);

        }
        private bool EnableAutoMeasure(int userId)
        {
            return true;// EnableBulk(userId);
        }
        
        //-- BulkUpdate
        private bool EnableBulk(int userId)
        {
            var errMsg = "";
            return
                AccessUtils.HasAccessToPage("" + userId, AccessUtils.PrgBulkUpdate, this, out errMsg);
        }

        protected void NewOrderClick(object sender, EventArgs e)
        {
            Response.Redirect("ScreeningFront.aspx");
        }

        protected void ItemizeClick(object sender, EventArgs e)
        {
            Response.Redirect("Itemize1.aspx");
        }
        protected void ReAssemblyClick(object sender, EventArgs e)
        {
            Response.Redirect("Reassembly.aspx");
        }

        #region Grade
        protected void ColorClick(object sender, EventArgs e)
        {
            Response.Redirect("Color2.aspx");
        }
        protected void MeasureClick(object sender, EventArgs e)
        {
            Response.Redirect("Measure2.aspx");
        }
        protected void ClarityClick(object sender, EventArgs e)
        {
            Response.Redirect("Clarity2.aspx");
        }
        protected void Grade2Click(object sender, EventArgs e)
        {
            Response.Redirect("Grade2.aspx");
        }

        #endregion

        #region Print Labels
        protected void PrintItemizedClick(object sender, EventArgs e)
        {
            Response.Redirect("PrintLabels.aspx?Mode=Itemized");
        }

        protected void PrintCertifiedClick(object sender, EventArgs e)
        {
            Response.Redirect("PrintLabels.aspx?Mode=Certified");

        }
        #endregion


        protected void NumberGeneratorClick(object sender, EventArgs e)
        {
            Response.Redirect("NumberGenerator.aspx");
        }

        protected void BulkUpdateClick(object sender, EventArgs e)
        {
            Response.Redirect("BulkUpdateAsyncNew.aspx");
        }

        protected void AutoMeasureClick(object sender, EventArgs e)
        {
            Response.Redirect("RMeas.aspx");
        }

        protected void EndSessionClick(object sender, EventArgs e)
        {
#if DEBUG
            Response.Redirect("ReItemizn.aspx?Mode=End");
#endif
        }

        protected void HomeClick(object sender, EventArgs e)
        {

            Response.Redirect("Login.aspx");
        }

        protected void OnReItemiznClick(object sender, EventArgs e)
        {
            Response.Redirect("ReItemizn.aspx");
        }

        protected void OnUpdateOrderClick(object sender, EventArgs e)
        {
            Response.Redirect("UpdateOrder.aspx");
        }

        protected void BillingClick(object sender, EventArgs e)
        {
            Response.Redirect("BillingInvoice.aspx");

        }

        protected void btnAccountRepresentative_Click(object sender, EventArgs e)
        {
            Response.Redirect("AccountRep.aspx");
        }
    }
}