﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="TrackingBatchHistory.aspx.cs" Inherits="Corpt.TrackingBatchHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div class="demoarea">
        <div class="demoheading" style="font-size: medium">
            <asp:Label runat="server" ID="filterLabel"></asp:Label>
        </div>
        <div>
            <asp:Label runat="server" ID="CarrierInfoLabel"></asp:Label>
        </div>
        <div id="gridContainer" style="padding-top: 10px;font-size: small">
            <asp:DataGrid ID="grdBatches" runat="server" AllowSorting="true" CellPadding="5"
                OnSortCommand="OnSortCommand" AutoGenerateColumns="True">
                <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>
