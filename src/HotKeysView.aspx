﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="HotKeysView.aspx.cs" Inherits="Corpt.HotKeysView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">
    <div class="demoarea">
        <div class="demoheading">Hot Keys</div>
        <div class="navbar nav-tabs form-inline">
            <asp:ImageButton ID="ExcelButton" ToolTip="Export to Excel" runat="server" ImageUrl="~/Images/ajaxImages/excel.jpg"
                OnClick="OnExcelClick" Visible="False" />
        </div>
        <table>
            <tr>
                <td style="width: 520px">
                    <asp:GridView runat="server" ID="GridView" AutoGenerateColumns="False" OnSorting="OnSortingEvent"
                        Style="margin-left: 50px" OnDataBound="OnDataBound" EnableViewState="False" CssClass="table table-condensed"
                        DataKeyNames="Access,Measure,MeasureValue,HotKey" RowStyle-Height="6px" Width="500px" 
                        ForeColor="Black">
                        <Columns>
                            <asp:BoundField DataField="Access" HeaderText="Access" SortExpression="Access" />
                            <asp:BoundField DataField="Measure" HeaderText="Measure" SortExpression="Measure" />
                            <asp:BoundField DataField="MeasureValue" HeaderText="Value Title" SortExpression="EnumValue" />
                            <asp:BoundField DataField="HotKey" HeaderText="Hot Key" SortExpression="HotKey" />
                            <asp:BoundField DataField="MeasureId" HeaderText="MeasureId" Visible="False" />
                            <asp:BoundField DataField="MeasureValueId" HeaderText="MeasureValueId" Visible="False" />
                        </Columns>
                        <HeaderStyle BackColor="#5377A9" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
                    </asp:GridView>
                </td>
                <td style="vertical-align: top; padding-left: 40px;">
                    <asp:Label runat="server" ID="DuplicateLabel" ForeColor="DarkRed" Width="300px"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
