﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class Tracking : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Tracking";
            if (!IsPostBack)
            {
                InitForm();
                CustomerLike.Focus();
            }
        }
        private void InitForm()
        {
            LoadCustomerList();
            CalendarExtenderFrom.SelectedDate = DateTime.Now.Date.AddDays(-7);
            CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
        }
        private void LoadCustomerList()
        {
            lstCustomerList.Items.Clear();
            var customers = QueryUtils.GetCustomers(this, false);
            SetViewState(customers, SessionConstants.CustomersList);
            lstCustomerList.DataSource = customers;
            lstCustomerList.DataBind();
        }

        protected void OnCustomerSearchClick(object sender, ImageClickEventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var filterText = CustomerLike.Text.Trim().ToLower();
            var filtered = string.IsNullOrEmpty(filterText) ? customers :
                customers.FindAll(m => m.CustomerName.ToLower().IndexOf(filterText, StringComparison.Ordinal) != -1);
            lstCustomerList.DataSource = filtered;
            lstCustomerList.DataBind();
        }

        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
        }

        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
        }

        protected void OnLoadClick(object sender, EventArgs e)
        {
            var customers = GetViewState(SessionConstants.CustomersList) as List<CustomerModel>;
            if (customers == null) return;
            var customer = customers.Find(m => m.CustomerId == lstCustomerList.SelectedValue);
            if (customer == null) return;
            var filterModel = new TrackingFilterModel
            {
                CustomerId = Int32.Parse(customer.CustomerId),
                CustomerOfficeId = Int32.Parse(customer.OfficeId),
                DateFrom = CalendarExtenderFrom.SelectedDate,
                DateTo = CalendarExtenderTo.SelectedDate
            };
            var allItems = QueryUtils.GetTrackings(filterModel, this);
            SetViewState(allItems, SessionConstants.TrackingAll);
            ApplyCurrentSort();
            ShowMemoNumbers();
            ShowOrders();
            ShowCps();
        }

        protected void OnSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var prevSort = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            var nowColName = e.SortExpression;
            var newSort = SortingModel.CreateRevert(prevSort, nowColName);
            SetViewState(newSort, SessionConstants.CurrentSortModel);
            ApplyCurrentSort();
        }
        void ApplyCurrentSort()
        {
            var trackings = GetViewState(SessionConstants.TrackingAll) as List<TrackingExModel> ??
                          new List<TrackingExModel>();
            var table = new DataTable("Trackings");
            table.Columns.Add(new DataColumn { ColumnName = "MemoNumber", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "CustomerProgram", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "CpOfficeId", DataType = typeof(int) });
            table.Columns.Add(new DataColumn { ColumnName = "CpId", DataType = typeof(int) });
            table.Columns.Add(new DataColumn { ColumnName = "OrderCode", DataType = typeof(string) });
            table.Columns.Add(new DataColumn { ColumnName = "GroupId", DataType = typeof(int) });
            table.Columns.Add(new DataColumn { ColumnName = "GroupOfficeId", DataType = typeof(int) });
            table.Columns.Add(new DataColumn { ColumnName = "StateId", DataType = typeof(int) });
            //	StateID
            foreach (var trackingModel in trackings)
            {
                table.Rows.Add(new object[]
                {
                    trackingModel.MemoNumber, 
                    trackingModel.CpName,
                    trackingModel.CpOfficeId,
                    trackingModel.CpId,
                    trackingModel.OrderCode,
                    trackingModel.GroupId,
                    trackingModel.GroupOfficeId,
                    trackingModel.StateId
                });
            }
            var dv = new DataView { Table = table };

            var sortModel = GetViewState(SessionConstants.CurrentSortModel) as SortingModel;
            if (sortModel != null)
            {
                dv.Sort = sortModel.ColumnName;
                if (!sortModel.Direction) dv.Sort += " DESC";
            }
            else
            {
                dv.Sort = "MemoNumber";
                SetViewState(new SortingModel { ColumnName = "MemoNumber", Direction = true }, SessionConstants.CurrentSortModel);
            }

            // Rebind data 
            grdBatches.DataSource = dv;
            grdBatches.DataBind();
        }
        private void ShowMemoNumbers()
        {
            var all = GetViewState(SessionConstants.TrackingAll) as List<TrackingExModel>;
            if (all == null) return;
            var memos = new SortedList();
            foreach (var tracking in all)
            {
                if (!memos.Contains(tracking.MemoNumber))
                {
                    memos.Add(tracking.MemoNumber, tracking.MemoDisplay);
                }
            }
            MemoLabel.Text = "";
            foreach (DictionaryEntry memo in memos)
            {
                MemoLabel.Text += memo.Value;
            }
            MemoPanel.Visible = memos.Count > 0;
        }
        private void ShowOrders()
        {
            var all = GetViewState(SessionConstants.TrackingAll) as List<TrackingExModel>;
            if (all == null) return;
            var orders = new SortedList();
            foreach (var tracking in all)
            {
                if (!orders.Contains(tracking.OrderCode))
                {
                    orders.Add(tracking.OrderCode, tracking.OrderDisplay);
                }
            }
            OrderLabel.Text = "";
            foreach (DictionaryEntry order in orders)
            {
                OrderLabel.Text += order.Value;
            }
            OrderPanel.Visible = orders.Count > 0;
        }
        private void ShowCps()
        {
            var all = GetViewState(SessionConstants.TrackingAll) as List<TrackingExModel>;
            if (all == null) return;
            var cps = new SortedList();
            foreach (var tracking in all)
            {
                if (!cps.Contains(tracking.CpName))
                {
                    cps.Add(tracking.CpName, tracking.CpNameDisplay);
                }
            }
            CpLabel.Text = "";
            foreach (DictionaryEntry cp in cps)
            {
                CpLabel.Text += cp.Value;
            }
            CpPanel.Visible = cps.Count > 0;
        }
    }

}