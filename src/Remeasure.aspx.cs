﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Remeas;
using Corpt.Utilities;

namespace Corpt
{
    public partial class Remeasure : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Remeasure";
            if (!IsPostBack)
            {
				//-- Enum measures
				LoadEnumMeasures();
                InitViewData();
				InputItemFld.Focus();
            }
        }
        #region Enum Measures
        private void LoadEnumMeasures()
        {
            SetViewState(QueryUtils.GetEnumMeasureUnion(this), SessionConstants.CpCommonEnumMeasures);
        }
        private List<EnumMeasureModel> GetEnumMeasure()
        {
            var measures = GetViewState(SessionConstants.CpCommonEnumMeasures) as List<EnumMeasureModel> ??
                           new List<EnumMeasureModel>();
            return measures;
        }
        #endregion


        #region View State

        private const string ViewMeasures = "viewMeasures";
        private static string ViewBatchData = "viewBatchData";
        private void InitViewData()
        {
            SetViewState(new List<ViewMeasures>(), ViewMeasures);
            SetViewState(new List<ViewBatchData>(), ViewBatchData);

        } 
        private IEnumerable<MeasureModel> GetMeasuresFromView(int itemTypeId)
        {
            var measures = GetViewState(ViewMeasures) as List<ViewMeasures> ?? new List<ViewMeasures>();
            var byItemType = measures.Find(m => m.ItemTypeId == itemTypeId);
            if (byItemType == null)
            {
                byItemType = new ViewMeasures { ItemTypeId = itemTypeId, Measures = QueryUtils.GetMeasures(new BatchModel { ItemTypeId = itemTypeId }, false, this) };
                measures.Add(byItemType);
            }
            return byItemType.Measures;
        }
        private ViewBatchData GetBatchData(string batchNumber)
        {
            var batchesData = GetViewState(ViewBatchData) as List<ViewBatchData> ?? new List<ViewBatchData>();
            var byBatchNumber = batchesData.Find(m => m.BatchNumber == batchNumber);
            if (byBatchNumber == null)
            {
                byBatchNumber = new ViewBatchData{BatchNumber = batchNumber};
                byBatchNumber.Items = QueryUtils.GetItemsCp(batchNumber, this);
                byBatchNumber.Cp = QueryCpUtils.GetCustomerProgramByBatchNumber(batchNumber, this);
                batchesData.Add(byBatchNumber);
            }
            return byBatchNumber;
        }
        private void RemoveBatchData(string batchNumber)
        {
            var batchesData = GetViewState(ViewBatchData) as List<ViewBatchData> ?? new List<ViewBatchData>();
            var byBatchNumber = batchesData.Find(m => m.BatchNumber == batchNumber);
            if (byBatchNumber != null)
            {
                batchesData.Remove(byBatchNumber);
            }
        }
        #endregion

        #region Migrated Items
        private void LoadMigratedItems(string order)
        {
            var migrated = QueryUtils.GetMigratedItems(order, this);
            MigratedGrid.DataSource = migrated;
            MigratedGrid.DataBind();
            MigratedLabel.Text = "Order # " + order + ": Migrated Items";
        }
        #endregion

        #region Information Dialog
        protected void OnCloseInfoDialogClick(object sender, EventArgs e)
        {
            
        }
        private void PopupInfoDialog(string msg, bool isErr, bool onCloseEditUp = false)
        {
            EditDialogUp.Value = onCloseEditUp ? "Y" : "N";
            MessageDiv.InnerText = msg;
            InfoImage.ImageUrl = string.Format("Images/ajaxImages/{0}24.png", isErr ? "error" : "information");
            InfoPopupExtender.Show();
        }

        #endregion

        #region Change Measures Set
        protected void OnSetModeChanged(object sender, EventArgs e)
        {
            ModeLabel.Text = (IsFullMode() ? "(Full Set)" : "(Set By CP)");
        }
        bool IsFullMode()
        {
            return FullSetModeFld.Checked;
        }
        #endregion

        #region Load Data For Selected Item
        void ShowHideItemDetails(bool show)
        {
            MeasurementPanel.Visible = show;
        }
        #endregion

        #region Grid For Item Editing
        private const string ValueStringFld = "ValueStringFld";
        private const string ValueNumericFld = "ValueNumericFld";
        private const string ValueEnumFld = "ValueEnumFld";

        protected void OnEditItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //-- Load data on controls
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;
            var editModel = e.Item.DataItem as ItemValueEditModel;
            if (editModel == null) return;
            var stringFld = e.Item.FindControl(ValueStringFld) as TextBox;
            var numericFld = e.Item.FindControl(ValueNumericFld) as TextBox;
            var enumFld = e.Item.FindControl(ValueEnumFld) as DropDownList;

            if (stringFld == null || numericFld == null || enumFld == null) return;
            if (editModel.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                /*IvanB 15/03 start*/
                if (editModel.MeasureName.Equals("Shape (cut)") || editModel.MeasureName.Equals("Color diamond color"))
                {
                    enumFld.Attributes.Add("class", "filtered-select");
                }
                /*IvanB 15/03 end*/
                enumFld.DataSource = editModel.EnumSource;
                enumFld.DataBind();
                if (string.IsNullOrEmpty(editModel.Value))
                {
                    enumFld.SelectedIndex = -1;
                }
                else
                {
                    enumFld.SelectedValue = editModel.Value;
                }
                enumFld.Visible = true;
                stringFld.Visible = false;
                numericFld.Visible = false;
                return;
            }
            if (editModel.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                numericFld.Text = editModel.Value;
                enumFld.Visible = false;
                stringFld.Visible = false;
                numericFld.Visible = true;
                return;
            }
            stringFld.Text = editModel.Value;
            enumFld.Visible = false;
            stringFld.Visible = true;
            numericFld.Visible = false;
        }
        private List<ItemValueEditModel> GetMeasuresForEdit(string itemNumber)
        {
            var itemNum = itemNumber.Replace(".", "");
            var batchNumber = itemNum.Substring(0, itemNum.Length - 2);
            var batchData = GetBatchData(batchNumber);
            var itemModel = batchData.Items.Find(m => m.FullItemNumber == itemNum);
            var enumsMeasure = GetEnumMeasure();
            var parts = QueryUtils.GetMeasureParts(itemModel.ItemTypeId, this);
            var measureValues = QueryUtils.GetMeasureValues(itemModel, "", this);
            var measures = GetMeasuresFromView(itemModel.ItemTypeId);
            var result = new List<ItemValueEditModel>();
            foreach (var measure in measures)
            {
                if (measure.MeasureClass > 3) continue;
                var partModel = parts.Find(m => m.PartId == measure.PartId);
                if (partModel == null) continue;
                var valueModel =
                        measureValues.Find(m => m.PartId == partModel.PartId && m.MeasureId == "" + measure.MeasureId);
                var enums = (measure.MeasureClass == MeasureModel.MeasureClassEnum
                        ? enumsMeasure.FindAll(m => m.MeasureValueMeasureId == measure.MeasureId) : null);
                result.Add(new ItemValueEditModel(partModel, new MeasureValueCpModel(measure), valueModel, enums, itemModel, false));
            }
            List<RemeasurePartsModel> partsList = new List<RemeasurePartsModel>();
            
            foreach (var part in parts)
            {
                RemeasurePartsModel partItem = new RemeasurePartsModel();
                string partId = part.PartId.ToString();
                string partName = part.PartName;
                partItem.PartId = partId;
                partItem.PartName = partName;
                partsList.Add(partItem);
            }
            partsList.Add(new RemeasurePartsModel("0","All parts"));

            PartsList.DataSource = partsList;
            PartsList.DataBind();
            result.Sort(new ItemValueEditComparer().Compare);
            //IvanB 11/03 start
            var blockShapesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Shapes, Page);
            var shapes = result.FindAll(m => m.MeasureName.Equals("Shape (cut)"));
            if (shapes != null && shapes.Count > 0)
            {
                foreach (var shapeEnum in shapes)
                {
                    var selectedShape = shapeEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(shapeEnum.Value)).ValueTitle;
                    shapeEnum.EnumSource = shapeEnum.EnumSource.Where(x => !blockShapesList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedShape.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    shapeEnum.EnumSource = shapeEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
            var blockColorGroupList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ColorGroup, Page);
            var colorGroups = result.FindAll(m => m.MeasureName.Equals("Color Group"));
            if (colorGroups != null && colorGroups.Count > 0)
            {
                foreach (var colorGroupEnum in colorGroups)
                {
                    var selectedColorGroup = colorGroupEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(colorGroupEnum.Value)).ValueTitle;
                    colorGroupEnum.EnumSource = colorGroupEnum.EnumSource.Where(x => !blockColorGroupList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedColorGroup.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    colorGroupEnum.EnumSource = colorGroupEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
            var blockKaratageList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Karatage, Page);
            var karatageGroups = result.FindAll(m => m.MeasureName.Equals("Karatage"));
            if (karatageGroups != null && karatageGroups.Count > 0)
            {
                foreach (var karatageEnum in karatageGroups)
                {
                    var selectedKaratageGroup = karatageEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(karatageEnum.Value)).ValueTitle;
                    karatageEnum.EnumSource = karatageEnum.EnumSource.Where(x => !blockKaratageList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedKaratageGroup.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    karatageEnum.EnumSource = karatageEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
            //alex 04152027
            var blockItemNameList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ItemName, Page);
            var itemNameGroups = result.FindAll(m => m.MeasureName.Equals("Item Name"));
            if (itemNameGroups != null && itemNameGroups.Count > 0)
            {
                foreach (var itemNameEnum in itemNameGroups)
                {
                    var selectedItemNameGroup = itemNameEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(itemNameEnum.Value)).ValueTitle;
                    itemNameEnum.EnumSource = itemNameEnum.EnumSource.Where(x => !blockKaratageList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedItemNameGroup.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    itemNameEnum.EnumSource = itemNameEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
            var blockColorDiamondColorsList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.ColorDiamondColors, Page);
            var colorDiamondColorsGroups = result.FindAll(m => m.MeasureName.ToLower().Equals("color diamond color"));
            if (colorDiamondColorsGroups != null && colorDiamondColorsGroups.Count > 0)
            {
                foreach (var colorDiamondColorsEnum in colorDiamondColorsGroups)
                {
                    var selectedColorDiamondColorsGroup = colorDiamondColorsEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(colorDiamondColorsEnum.Value)).ValueTitle;
                    colorDiamondColorsEnum.EnumSource = colorDiamondColorsEnum.EnumSource.Where(x => !blockColorDiamondColorsList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedColorDiamondColorsGroup.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    colorDiamondColorsEnum.EnumSource = colorDiamondColorsEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                }
            }
            //alex 04152027
            var blockVarietyList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Variety, Page);
            var varieties = result.FindAll(m => m.MeasureName.Equals("Variety"));
            //if (varieties != null && varieties.Count > 0)
            //{
            //    foreach (var varietyEnum in varieties)
            //    {
            //        var selectedVariety = varietyEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(varietyEnum.Value)).ValueTitle;
            //        var selectedVariety1 = varietyEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(varietyEnum.Value));
            //        varietyEnum.EnumSource = varietyEnum.EnumSource.Where(x => !blockVarietyList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedVariety.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
            //        //Remove duplicates
            //        varietyEnum.EnumSource = varietyEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
            //    }
            //}
            var blockSpeciesList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Species, Page);
            var species = result.FindAll(m => m.MeasureName.Equals("Species"));
            if (species != null && species.Count > 0)
            {
                foreach (var speciesEnum in species)
                {
                    var selectedSpecies = speciesEnum.EnumSource.Find(m => m.MeasureValueId.ToString().Equals(speciesEnum.Value)).ValueTitle;
                    speciesEnum.EnumSource = speciesEnum.EnumSource.Where(x => !blockSpeciesList.Exists(y => (x.ValueTitle.Equals(y.BlockedDisplayName)) && !selectedSpecies.Equals(y.BlockedDisplayName))).OrderBy(x => x.ValueTitle).ToList();
                    //Remove duplicates
                    speciesEnum.EnumSource = speciesEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                    if (selectedSpecies != null && selectedSpecies != "" && varieties != null && varieties.Count > 0)
                    {
                        List<EnumMeasureModel> speciesVarieties = QueryUtils.GetVarietyBySpecies(selectedSpecies, this);
                        foreach (var varietyEnum in varieties)
                        {
                            if (varietyEnum.PartId != speciesEnum.PartId)
                                continue;
                            var selectedVariety = varietyEnum.EnumSource.Find(m => (m.MeasureValueId.ToString().Equals(varietyEnum.Value) && varietyEnum.PartId==speciesEnum.PartId)).ValueTitle;
                            varietyEnum.EnumSource = varietyEnum.EnumSource.Where(x => speciesVarieties.Exists(y => (x.ValueTitle.Equals(y.MeasureValueName)))).OrderBy(x => x.ValueTitle).ToList();
                            //Remove duplicates
                            varietyEnum.EnumSource = varietyEnum.EnumSource.GroupBy(x => x.ValueTitle).Select(x => x.FirstOrDefault()).ToList();
                        }
                    }
                    
                }
            }
            //IvanB 11/03 end
            return result;
        }
        #endregion

        #region Display Picture & Customer Program
        private void ShowPicture(string dbPicture, CustomerProgramModel cpModel, string batchNumber)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            else
            {
                itemPicture.Visible = false;
            }
            //-- Path to picture
            CpPathToPicture.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture,
                                                 string.IsNullOrEmpty(dbPicture) ? "" : "Picture");
            if (errMsg.Contains("does not exist"))
            {
                errMsg = "File does not exists";
            }
            if (string.IsNullOrEmpty(dbPicture) || dbPicture.ToLower().Contains("default"))
            {
                errMsg = "";
            }
            ErrPictureField.Text = errMsg;
            if (cpModel != null)
            {
                CpDescDiv.InnerHtml = "<strong>Description: </strong>" + cpModel.CommentDisplay;
                CpCommDiv.InnerHtml = "<strong>Comment: </strong>" + cpModel.DescriptionDisplay;
                CpRef.InnerHtml = "<strong>Customer Program: </strong>" + cpModel.CustomerProgramName;
                CpRef.HRef = "~/CustomerProgram.aspx?BatchNumber=" + batchNumber;
                CpRef.Visible = true;
                
            } else
            {
                CpDescDiv.InnerHtml = "";
                CpCommDiv.InnerHtml = "";
                CpRef.Visible = false;
            }

        }

        #endregion

        #region Save button
        protected void OnSaveButtonClick(object sender, EventArgs e)
        {
            var itemValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ??
                new List<ItemValueEditModel>();
            if (itemValues.Count == 0) return;
            var editingItem = itemValues.ElementAt(0).EditingItem;
            var batchItemModel = new BatchItemModel { BatchId = editingItem.BatchId, ItemCode = Convert.ToInt32(editingItem.ItemCode) };
            var changedValues = new List<ItemValueEditModel>();
            foreach (DataGridItem item in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[item.ItemIndex];
                var partId = key.Split(';')[0];
                var measureId = key.Split(';')[1];
                var itemValue = itemValues.Find(m => m.PartId == partId && "" + m.MeasureId == measureId);
                if (itemValue == null) continue;
                var newValue = "";
                if (itemValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enumFld = item.FindControl(ValueEnumFld) as DropDownList;
                    if (enumFld != null) newValue = enumFld.SelectedValue;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    var numericFld = item.FindControl(ValueNumericFld) as TextBox;
                    if (numericFld != null) newValue = numericFld.Text;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassText)
                {
                    var stringFld = item.FindControl(ValueStringFld) as TextBox;
                    if (stringFld != null) newValue = stringFld.Text;
                }
                if (!itemValue.HasChanged(newValue)) continue;
                var cloneModel = itemValue.Clone();
                cloneModel.Value = newValue;
                changedValues.Add(cloneModel);
            }
            if (changedValues.Count == 0)
            {
                PopupInfoDialog("No changes", true, true);
                return;
            }
            var updateModel = new BulkUpdateMeasureModel();
            foreach (var value in changedValues)
            {
                updateModel.MeasureValues.Add(new MeasureValueModel(value));
            }
            updateModel.ItemTypeId = editingItem.ItemTypeId;
            updateModel.Items.Add(batchItemModel);
            int newBatchId = editingItem.NewBatchId;
            var msg = QueryUtils.SaveRemeasure(updateModel, this, newBatchId);
            if (!string.IsNullOrEmpty(msg))
            {
                PopupInfoDialog(msg, true, true);
                return;
            }

            //-- Refresh item data and rebuild table
            var itemNumber = editingItem.FullItemNumber;
            var batchNumber = itemNumber.Substring(0, itemNumber.Length - 2);
            RemoveBatchData(batchNumber);
            SwithToViewMode();
            PopupInfoDialog("Changes were updated successfully", false);
        }

        #endregion

        #region Undo Button
        protected void OnUndoButtonClick(object sender, EventArgs e)
        {
            SwithToViewMode();
        }
        private void SwithToViewMode()
        {
//            SaveButton.Enabled = false;
//            UndoButton.Enabled = false;
            InputItemFld.Enabled = true;
            FindInputItemBtn.Enabled = true;
            TreeHistory.Enabled = true;
            //FullSetModeFld.Enabled = true;
            OnTreeHistorySelectedChanged(null, null);
            
        }
        #endregion

        #region Input Item Number
        protected void OnFindItemBtnClick(object sender, EventArgs e)
        {
            FindDataByItemNumber(InputItemFld.Text);

        }
        void FindDataByItemNumber(string inputItemNumber)
        {
            InitViewData();
            TabContainer.Visible = false;
            TreeHistory.Nodes.Clear();
			var myResult = Utilities.QueryUtils.GetItemNumberBy7digit(inputItemNumber, this.Page);
			if (myResult.Trim() != "")
			{
				InputItemFld.Text = myResult;
				inputItemNumber = myResult;
			}
            if (!(inputItemNumber.Length == 10 || inputItemNumber.Length == 11))
            {
                PopupInfoDialog("Invalid Item Number " + inputItemNumber, true);
                return;
            }

            //-- Define NewItemNumber and Order
            var itemModel = QueryUtils.GetNewItemNumber(inputItemNumber, this);
            if (itemModel == null)
            {
                PopupInfoDialog("Invalid Item Number " + inputItemNumber, true);
                return;
            }
            NewItemNumberFld.Text = itemModel.NewItemNumberWithDotes;
            
            //-- Show Migrated Items for Order
            LoadMigratedItems(itemModel.NewOrder);
            
            //-- Show Tree Items for Order
            var items = QueryItemiznUtils.GetOrderHistory(itemModel.NewOrder, false, this);
            TreeUtils.ShowTreeHistory(items, TreeHistory, TreeNodeSelectAction.Select);
            
            //-- Show Invalid Items
            var invalidItem = items.FindAll(m => m.IsInvalidItem);
            ItemsInvalidList.Items.Clear();
            foreach(var item in invalidItem)
            {
                ItemsInvalidList.Items.Add(new ListItem(item.Id.Split('_')[1], item.Id));
            }
            ItemsInvalidList.Rows = invalidItem.Count + 1;

            SelectTreeNode(itemModel.NewItemNumberWithDotes);
            TabContainer.Visible = true;
        }

        #endregion

        #region Find Selected Tree Item Data
        private TreeNode FindBatchTreeNode(string batchNumber)
        {
            var orderNode = TreeHistory.Nodes[0].ChildNodes[0];
            foreach(TreeNode batchNode in  orderNode.ChildNodes)
            {
                if (batchNode.Value.Contains(batchNumber)) return batchNode;
            }
            return null;
        }
        private void CollapseBatchTreeItem(string exclideBatchOfItem)
        {
            var batchNumber = exclideBatchOfItem.Substring(0, exclideBatchOfItem.Length - 3);
            var orderNode = TreeHistory.Nodes[0].ChildNodes[0];
            foreach(TreeNode batchNode in  orderNode.ChildNodes)
            {
                if (!batchNode.Value.Contains(batchNumber)) batchNode.Collapse();
            }
        }
        private TreeNode FindItemTreeNode(string itemNumber)
        {
            var batchNumber = itemNumber.Substring(0, itemNumber.Length - 3);
            var batchTreeNode = FindBatchTreeNode(batchNumber);
            if (batchTreeNode == null) return null;
            foreach(TreeNode itemNode in batchTreeNode.ChildNodes)
            {
                if (itemNode.Value.Contains(itemNumber)) return itemNode;
            }
            return null;
        }
        private void SelectTreeNode(string itemNumber)
        {
            var itemTreeNode = FindItemTreeNode(itemNumber);
            if (itemTreeNode != null)
            {
                itemTreeNode.Selected = true;
            }
            CollapseBatchTreeItem(itemNumber);
            OnTreeHistorySelectedChanged(null, null);
        }
        #endregion

        #region Invalid Item
        private bool IsInvalidItem(string itemNumber)
        {
            return ItemsInvalidList.Items.Cast<ListItem>().Any(item => item.Text == itemNumber);
        }

        #endregion
        #region Tree History
        protected void OnTreeHistorySelectedChanged(object sender, EventArgs e)
        {
            var selectedNode = TreeHistory.SelectedNode;
            if (selectedNode == null || selectedNode.Depth < 3)
            {
//                ShowHideItemDetails(false);
//                ShowPicture("", null);
                return;
            }
            var isInvalid = IsInvalidItem(selectedNode.Value.Split('_')[1]);
            var itemNumber = selectedNode.Value.Split('_')[1].Replace(".", "");
            var batchNumber = itemNumber.Substring(0, itemNumber.Length - 2);
            var batchData = GetBatchData(batchNumber);
            var itemModel = batchData.Items.Find(m => m.FullItemNumber == itemNumber);
            
            ShowPicture(itemModel.Path2Picture, batchData.Cp, batchNumber);
            
            var itemValues = GetMeasuresForEdit(itemNumber);
            SetViewState(itemValues, SessionConstants.ShortReportExtMeasuresForEdit);
            ItemValuesGrid.DataSource = isInvalid ? null : itemValues;
            ItemValuesGrid.DataBind();
            DataForLabel.Text = "Data For # " +selectedNode.Value.Split('_')[1];
            InvalidLabel.Text = isInvalid ? "Invalid Item" : "";
            ShowHideItemDetails(true);

			//Change Input Text Field at Top on Selecting Item from Tree
			InputItemFld.Text = itemNumber;
			var findItemModel = QueryUtils.GetNewItemNumber(itemNumber, this);
			NewItemNumberFld.Text = findItemModel.NewItemNumberWithDotes;
        }
        #endregion

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["NewItemsValues"] = null;
            var serviceFld = (DropDownList)sender;
            //var speciesItem = serviceFld.NamingContainer as DataGridItem;
            var item = serviceFld.NamingContainer as DataGridItem;
            var rownum = "" + ItemValuesGrid.DataKeys[item.ItemIndex];
            var uniqueKey = ItemValuesGrid.DataKeys[item.ItemIndex].ToString();
            var keys = uniqueKey.Split(';');
            var partId = keys[0];
            var measureId = keys[1];
            if (measureId != "53")
                return;
            
            var itemValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel> ??
                new List<ItemValueEditModel>();
            if (itemValues.Count == 0) return;
            List<ItemValueEditModel> updatedValues = QueryUtils.UpdateMeasureList(ItemValuesGrid, itemValues, item, measureId, this);
            ItemValuesGrid.DataSource = updatedValues;
            ItemValuesGrid.DataBind();
            return;
            //var editingItem = itemValues.ElementAt(0).EditingItem;
            //var batchItemModel = new BatchItemModel { BatchId = editingItem.BatchId, ItemCode = Convert.ToInt32(editingItem.ItemCode) };
            var changedValues = new List<ItemValueEditModel>();
            foreach (DataGridItem dgItem in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[dgItem.ItemIndex];
                var itemPartId = key.Split(';')[0];
                var itemMeasureId = key.Split(';')[1];
                var itemValue = itemValues.Find(m => m.PartId == itemPartId && "" + m.MeasureId == itemMeasureId);
                if (itemValue == null) continue;
                var newValue = "";
                if (itemValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enumFld = dgItem.FindControl(ValueEnumFld) as DropDownList;
                    if (enumFld != null) newValue = enumFld.SelectedValue;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    var numericFld = dgItem.FindControl(ValueNumericFld) as TextBox;
                    if (numericFld != null) newValue = numericFld.Text;
                }
                if (itemValue.MeasureClass == MeasureModel.MeasureClassText)
                {
                    var stringFld = dgItem.FindControl(ValueStringFld) as TextBox;
                    if (stringFld != null) newValue = stringFld.Text;
                }
                if (!itemValue.HasChanged(newValue)) continue;
                var cloneModel = itemValue.Clone();
                cloneModel.Value = newValue;
                changedValues.Add(cloneModel);
            }
            
            int idx = item.ItemIndex;
            var enumFldMin = item.FindControl(ValueEnumFld) as DropDownList;
            string speciesName = enumFldMin.SelectedItem.ToString();
            //var itemValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel>;
            var newItemsValues = itemValues.ToList();
            
            if (changedValues.Count != 0)
            {
                foreach (ItemValueEditModel changedValue in changedValues)
                {
                    foreach (ItemValueEditModel itemValue in itemValues)
                    {
                        if (itemValue.MeasureId == changedValue.MeasureId)
                        {
                            itemValue.Value = changedValue.Value;
                            break;
                        }
                    }
                }
            }
            List<ItemValueEditModel> newItemValues = itemValues.ToList();
            int varietyMeasureId = 54;
            List<EnumMeasureModel> varieties = QueryUtils.GetVarietyBySpecies(speciesName, this);
            if (varieties == null || varieties.Count == 0)
                return;
            foreach (ItemValueEditModel itemvalue in newItemsValues)
            {
                if (itemvalue.MeasureId == varietyMeasureId)//Variety
                {
                    itemvalue.EnumSource.Clear();
                    foreach (EnumMeasureModel variety in varieties)
                    {
                        string valueTitle = variety.ValueTitle;
                        string valueName = variety.MeasureValueName;
                        int measureValueId = Convert.ToInt32(variety.MeasureValueId);
                        var newEntry = new EnumMeasureModel
                        {
                            ValueTitle = valueTitle,
                            MeasureValueName = valueName,
                            MeasureValueId = measureValueId,
                            MeasureClass = MeasureModel.MeasureClassEnum,
                            MeasureValueMeasureId = 54
                        };
                        itemvalue.EnumSource.Add(newEntry);
                    }
                    //break;
                }
            }
            //DataTable dt = QueryUtils.getSpeciesVariety(speciesName, this);
            //if (dt == null || dt.Rows.Count == 0)
            //    return;
            //foreach (ItemValueEditModel itemvalue in newItemsValues)
            //{
            //    if (itemvalue.MeasureId == varietyMeasureId)//Variety
            //    {
            //        itemvalue.EnumSource.Clear();
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            string valueTitle = dr["VarietyTitle"].ToString();
            //            string valueName = dr["VarietyName"].ToString();
            //            int measureValueId = Convert.ToInt32(dr["VarietyID"].ToString());
            //            var newEntry = new EnumMeasureModel
            //            {
            //                ValueTitle = valueTitle,
            //                MeasureValueName = valueName,
            //                MeasureValueId = measureValueId,
            //                MeasureClass = MeasureModel.MeasureClassEnum,
            //                MeasureValueMeasureId = 54
            //            };
            //            itemvalue.EnumSource.Add(newEntry);
            //        }
            //        break;
            //    }
            //}
            ItemValuesGrid.DataSource = newItemsValues;
            Session["NewItemsValues"] = newItemsValues;
            ItemValuesGrid.DataBind();
        }

        protected void OnPartsListChanged(object sender, EventArgs e)
        {
            var allValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel>;
            if (PartsList.SelectedValue == "0")
            {
                ItemValuesGrid.DataSource = allValues;
                ItemValuesGrid.DataBind();
            }
            else
            {
                var partValues = allValues.FindAll(m => m.PartId == PartsList.SelectedValue);
                PartsList.SelectedItem.Attributes.CssStyle.Add("color", "green");
                ItemValuesGrid.DataSource = partValues;
                ItemValuesGrid.DataBind();
            }
        }
    }
}