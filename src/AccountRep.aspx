﻿<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master"
    CodeBehind="AccountRep.aspx.cs" Inherits="Corpt.AccountRep"
    EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true"
    Title="GSI: Account Representative" %>

<%@ Import Namespace="Corpt" %>
<%@ Import Namespace="Corpt.Utilities" %>
<%@ Import Namespace="Corpt.Constants" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="corptControl" TagName="ServiceTypeCheckBoxesDropDown" Src="~/ServiceTypeCheckBoxesDropDown.ascx" %>
<%@ Register TagPrefix="corptControl" Namespace="Corpt" Assembly="Corpt" %>

<asp:Content ContentPlaceHolderID="PageHead" ID="HeadId" runat="server">
    <%-- you can turn on unminified versions, otherwise they should be deleted from project --%>
    <script type="text/javascript" src="Style/select2.min.js"></script>
    <link href="Style/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 35px;
        }

        .ajax__calendar .ajax__calendar_invalid .ajax__calendar_day {
            color: lightgray !important;
        }

        .ajax__calendar .ajax__calendar_invalid .ajax__calendar_month {
            color: lightgray !important;
        }

        .ajax__calendar .ajax__calendar_invalid .ajax__calendar_year {
            color: lightgray !important;
        }

        /* Layout */

        .panel-caption {
            padding-bottom: 5px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.5em;
        }

        .panel-sub-caption {
            padding-bottom: 5px;
            color: #5377A9;
            font-family: Arial, Sans-Serif;
            font-weight: bold;
            font-size: 1.2em;
        }

        .first-column {
            width: 580px;
            max-width: 580px;
            min-width: 580px;
        }

        .second-column {
            width: 284px;
            padding-left: 12px;
        }

        .third-column {
            width: 438px;
            padding-left: 12px;
        }

        .forth-column {
            width: 206px;
            padding-left: 12px;
        }

        .invoice-column {
            width: 928px; /* 284 + 438 + 206 */
            padding-left: 12px;
        }

        .invoice-grid {
            width: 956px; /* 284 + 438 + 206  -  12*/
        }

        .first-row-content-tab {
            height: 214px;
        }

        .second-row-content-tab {
            height: 198px;
        }

        div#OrdersTreeViewDiv {
            overflow: auto;
        }

        .scrolableTreeView {
            overflow-y: scroll;
            overflow-x: auto;
            scrollbar-base-color: #ffeaff;
            border: 1px black solid;
            vertical-align: top;
        }

        .scrolableDataGrid {
            overflow-y: auto;
            scrollbar-base-color: #ffeaff;
            border: 1px black solid;
            vertical-align: top;
        }

        .grid-item {
            font-size: small;
            color: black;
            font-family: cambria;
            background-color: white;
            height: 25px;
        }

        tbody .grid-item td {
            cursor: pointer;
        }

        .grid-alternating-item {
            font-size: small;
            color: black;
            font-family: cambria;
            background-color: #eeeeee;
            height: 25px;
        }

        .grid {
            font-family: cambria;
            font-size: small;
        }

        .grid-header th {
            background: #5377A9;
            font-family: cambria;
            font-weight: normal;
            font-size: small;
            color: white;
            border: 1px black solid;
        }

        .grid-freezed-header {
            position: sticky;
            top: 0;
        }

        .grid-header > th {
            border-left: 0;
        }

        .grid-header th {
            border-Top: 0;
        }

        .grid-item > td {
            border-left: 0;
        }

        .grid-selected-item {
            font-weight: bold;
            font-style: italic;
            color: white;
            background: blue;
        }

        .grid-edit-item {
            font-weight: bold;
        }
        /* End DataGrid styles*/

        .radio-horizontal-style input[type="radio"], input[type="checkbox"] {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-left: 30px !important;
            vertical-align: top;
            display: inline-block;
        }

        .radio-horizontal-style label {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-left: 5px !important;
            vertical-align: middle;
            display: inline-block;
        }

        .radio-horizontal-style-left label {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-left: 0px !important;
            vertical-align: middle;
            display: inline-block;
        }

        /* Image styles */

        .image-a {
            display: table;
            height: 198px;
            text-align: center;
        }

        .image-panel {
            word-wrap: break-word;
            max-width: 400px;
            max-height: 198px;
            min-width: 400px;
            min-height: 198px;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

        .image-caption-style {
            font-size: small;
            max-width: 400px;
            max-height: 20px;
        }

        .image-style {
            font-family: cambria;
            font-weight: normal;
            font-size: large;
            text-align: center;
            max-width: 400px;
            max-height: 198px;
        }

        .CurrencyColumn {
            white-space: nowrap;
            padding-right: 20px !important;
            text-align: right !important;
            vertical-align: middle;
        }

        .order-search-section {
            padding-left: 150px;
        }

        .singledropdown {
            width: 150px;
            height: 28px;
        }
    </style>

</asp:Content>

<asp:Content ID="AccountRepContent" ContentPlaceHolderID="SampleContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1"></ajaxToolkit:ToolkitScriptManager>

    <script type="text/javascript">

        /* init search dropdown in case ToolkitScriptManager is on a page
         otherwise other methods should be used */
        function pageLoad() {
            $("#<%= CustomerDropDownList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $("#<%= InvoicesDropDownList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $("#<%= OrdersInvoiceDropDownList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $("#<%= MemoDropDownList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $("#<%= PoDropDownList.ClientID %>").select2({
                placeholder: {
                    id: '', // the value of the option
                    text: ''
                }
            });

            $(".filtered-select").select2({
                scrollAfterSelect: true
            });
        }

        function Tree_scrollIntoView_WithTimeout() {
            setTimeout(Tree_scrollIntoView, 1000);
        }

        function Tree_scrollIntoView() {
            var data = <%= OrdersTreeView.ClientID %>_Data;
            if (!data) {
                return false;
            }
            if ((typeof (data.selectedNodeID) != "undefined") && (data.selectedNodeID != null)) {
                var id = data.selectedNodeID.value;
                if (id.length > 0) {
                    var selectedNode = document.getElementById(id);
                    if ((typeof (selectedNode) != "undefined") && (selectedNode != null)) {
                        selectedNode.scrollIntoView(true);
                    }
                }
            }
            return false;
        }

        //Getting ToolkitScriptManager instance
        var yPos;
        var senderControl = null;
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        //var scrollTop_OrdersTreeViewDiv;
        var scrollTop_AdditionalServiceDataGridDiv;
        var scrollTop_MigratedItemDataGridDiv;
        var scrollTop_CurrentBatchItemsDataGridDiv;
        var scrollTop_MeasureDataGridDiv;
        var scrollTop_InvoiceItemsGridDiv;

        //Handler to get Y position in Rules table when request begin
        function BeginRequestHandler(sender, args) {
            //Get The Div Scroll Position
            //var m = document.getElementById('OrdersTreeViewDiv');
            //scrollTop_OrdersTreeViewDiv = m.scrollTop;

            var m = document.getElementById('AdditionalServiceDataGridDiv');
            scrollTop_AdditionalServiceDataGridDiv = m.scrollTop;

            m = document.getElementById('MigratedItemDataGridDiv');
            scrollTop_MigratedItemDataGridDiv = m.scrollTop;

            m = document.getElementById('CurrentBatchItemsDataGridDiv');
            scrollTop_CurrentBatchItemsDataGridDiv = m.scrollTop;

            m = document.getElementById('MeasureDataGridDiv');
            if ( m != null )
            {
                scrollTop_MeasureDataGridDiv = m.scrollTop;
            }
            m = document.getElementById('InvoiceItemsGridDiv');
            scrollTop_InvoiceItemsGridDiv = m.scrollTop;

            var rules = $('#<%=OrdersTreeViewDiv.ClientID%>');
            if (rules.length > 0) {
                // Get Y positions of scrollbar before the partial postback
                if (rules.find(args._postBackElement).length > 0) {
                    senderControl = args._postBackElement;
                    yPos = rules.scrollTop();
                }
            }

            m = document.getElementById('<%= TimeZoneOffsetHidden.ClientID %>');
            m.value = (new Date()).getTimezoneOffset();

        }

        //Handler to set Y
        // ReSharper disable UnusedParameter
        function EndRequestHandler(sender, args) {

            //Set The Div Scroll Position for 

            //var m = document.getElementById('OrdersTreeViewDiv');
            //m.scrollTop = scrollTop_OrdersTreeViewDiv;

            var m = document.getElementById('AdditionalServiceDataGridDiv');
            m.scrollTop = scrollTop_AdditionalServiceDataGridDiv;

            m = document.getElementById('MigratedItemDataGridDiv');
            m.scrollTop = scrollTop_MigratedItemDataGridDiv;

            m = document.getElementById('CurrentBatchItemsDataGridDiv');
            m.scrollTop = scrollTop_CurrentBatchItemsDataGridDiv;

            m = document.getElementById('MeasureDataGridDiv');
            if ( m != null )
            {
                m.scrollTop = scrollTop_MeasureDataGridDiv;
            }

            m = document.getElementById('InvoiceItemsGridDiv');
            m.scrollTop = scrollTop_InvoiceItemsGridDiv;

            // ReSharper restore UnusedParameter
            if (senderControl == null) {
                return;
            }

            var rules = $('#<%=OrdersTreeViewDiv.ClientID%>');
            var cntrIdToFocus = senderControl.id;
            var isCntrInsideRules = rules.find('#' + cntrIdToFocus).length > 0;

            //check if rules table exists and control we need to scroll to is inside that table
            if (rules.length > 0 && isCntrInsideRules) {
                var targetControl;
                // ReSharper disable once UseOfImplicitGlobalInFunctionScope
                if (__nonMSDOMBrowser) {
                    targetControl = document.getElementById(cntrIdToFocus);
                }
                else {
                    targetControl = document.all[cntrIdToFocus];
                }
                var focused = targetControl;
                // ReSharper disable once UseOfImplicitGlobalInFunctionScope
                if (targetControl && (!WebForm_CanFocus(targetControl))) {
                    // ReSharper disable once UseOfImplicitGlobalInFunctionScope
                    focused = WebForm_FindFirstFocusableChild(targetControl);
                }
                if (focused) {
                    try {
                        focused.focus();
                        // ReSharper disable once UseOfImplicitGlobalInFunctionScope
                        if (__nonMSDOMBrowser) {
                            // Set Y positions back to the scrollbar
                            // after partial postback
                            rules.scrollTop(yPos);
                        }
                        if (window.__smartNav) {
                            window.__smartNav.ae = focused.id;
                        }
                    }
                    catch (e) {
                    }
                }

                //remove _controlIDToFocus so ToolkitScriptManager does not do wrong scroll
                // ReSharper disable once UseOfImplicitGlobalInFunctionScope
                Sys.WebForms.PageRequestManager.getInstance()._controlIDToFocus = null;
                senderControl = null;

            }
        }

        //adding handlers to begin and end request event
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);

        // Check/Uncheck All on OrdersTreeView
        function OrdersTreeViewCheckAllNodes(evt) {
            OrdersTreeViewCheckUncheckAllNodes(true);
        }

        function OrdersTreeViewUnCheckAllNodes(evt) {
            OrdersTreeViewCheckUncheckAllNodes(false);
        }

        function OrdersTreeViewCheckUncheckAllNodes(isChecked) {
            var inputs = $("#<%= OrdersTreeView.ClientID %> input[type=checkbox]");
            for (var i = 0; i < inputs.length; i++) {
                if (!inputs[i].disabled) {
                    inputs[i].checked = isChecked;
                }
            }
            __doPostBack('<%=OrdersTreeView.ClientID %>', "TreeNodeChecked");
        }

        function getScrollValueUpTo(rowNumber) {
            var value = 0;
            $("#<%=InvoiceItemsGridView.ClientID%> tr:has(td)").each(function (index) {
                if (index < rowNumber - 1) {
                    value = value + $(this).height();
                } else {
                    return false;
                }
            });
            return value;
        }

    </script>

    <div class="demoarea">
        <input id="TimeZoneOffsetHidden" runat="server" type="hidden" />
        <div>
            <table>
                <tr>
                    <td style="height: 20px; text-align: center" class="panel-caption">
                        <asp:Label runat="server" Text="Account Representative" />
                    </td>
                    <td style="padding-left: 5px; text-align: center">
                        <div>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                                <ProgressTemplate>
                                    <%-- ReSharper disable once Asp.Image --%>
                                    <img alt="" src="Images/ajaxImages/loader.gif" width="20px" />
                                    <b>Please, wait....</b>
                                    &nbsp;
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <div style="margin-top: 5px">
                <asp:UpdatePanel runat="server" ID="RequestAttributesPanel">
                    <Triggers>
                    </Triggers>
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="MainPanel" CssClass="form-inline"
                                        DefaultButton="SearchByOrderButton">
                                        <table style="color: Black; margin-left: 5px;">
                                            <tr>
                                                <td class="auto-style1" style="padding-top: 0; width: 100px;">
                                                    <asp:Label runat="server">Customer:</asp:Label>
                                                </td>
                                                <td colspan="2" class="auto-style1">
                                                    <asp:DropDownList
                                                        ID="CustomerDropDownList" runat="server" DataTextField="CustomerName"
                                                        AutoPostBack="True"
                                                        DataValueField="CustomerId"
                                                        CssClass=""
                                                        OnSelectedIndexChanged="CustomerDropDownList_OnSelectedIndexChanged"
                                                        ToolTip="Customers List" Width="285px" />
                                                </td>
                                                <td colspan="2" class="auto-style1" style="padding-left: 10px">
                                                    <asp:TextBox runat="server" ID="CustomerCodeTextBox" Width="75px"
                                                        AutoPostBack="True"
                                                        MaxLength="5"
                                                        OnTextChanged="CustomerCodeTextBox_OnTextChanged" />
                                                </td>
                                                <td style="width: 75px; text-align: center; padding-top: 10px"
                                                    class="auto-style1">
                                                    <asp:ImageButton ID="SearchByCustomerButton" runat="server"
                                                        ToolTip="Search orders by 'customer' and 'service type' and 'creation date range'"
                                                        ImageUrl="~/Images/ajaxImages/search.png"
                                                        OnClick="OnSearchByCustomerButtonClick" />
                                                </td>
                                                <td style="padding-left: 30px; padding-top: 0" class="auto-style1">
                                                    <asp:Label runat="server">Memo:</asp:Label>
                                                </td>
                                                <td style="padding-left: 10px; padding-top: 0" class="auto-style1">
                                                    <asp:TextBox runat="server" ID="MemoTextBox"
                                                        Width="75px"
                                                        OnTextChanged="MemoTextBox_TextChanged"
                                                        ToolTip="Search Orders with Memo strictly equal the value"
                                                        Visible="False" />
                                                </td>
                                                <td style="padding-left: 10px; padding-top: 0" class="auto-style1">
                                                    <asp:DropDownList
                                                        ID="MemoDropDownList" runat="server"
                                                        AutoPostBack="True"
                                                        DataTextField="DisplayText"
                                                        DataValueField="Value"
                                                        CssClass=""
                                                        OnSelectedIndexChanged="MemoDropDownList_OnSelectedIndexChanged"
                                                        ToolTip="Available Memo" Width="200px"
                                                        Enabled="False" />
                                                </td>

                                                <td class="auto-style1 order-search-section" style="padding-top: 0">
                                                    <asp:Label runat="server" ID="OrderSearchFieldNameLabel" />
                                                </td>
                                                <td class="auto-style1" style="padding-left: 5px; padding-top: 0">
                                                    <asp:TextBox runat="server" ID="OrderCodeTextBox" Width="95px"
                                                        onfocus="javascript:this.focus();this.select();" />
                                                </td>
                                                <td class="auto-style1" style="padding-left: 10px; padding-top: 10px">
                                                    <asp:ImageButton ID="SearchByOrderButton" runat="server"
                                                        ToolTip="Search order by 'order code'[.'batch code'[.'item code']]"
                                                        ImageUrl="~/Images/ajaxImages/search.png" OnClick="OnSearchByOrderButtonClick" />
                                                </td>
                                                <td colspan="2" class="auto-style1" style="padding-left: 20px;">
                                                    <asp:Button runat="server" ID="ResetButton"
                                                        Text="Reset" OnClick="ResetButton_OnClick"
                                                        Width="200px" />
                                                </td>
                                                <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Label ID="ErrPrintedOrders" runat="server" ForeColor="Red" Width="300px" runat="server"></asp:Label>
                                                                                            </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label runat="server">Services:</asp:Label>
                                                </td>
                                                <td class="auto-style1">
                                                    <asp:DropDownList
                                                        ID="ServiceTypesCategoryDropDown" runat="server"
                                                        CssClass="singledropdown"
                                                        AutoPostBack="True"
                                                        OnSelectedIndexChanged="ServiceTypeCategoryDropDown_OnSelectedIndexChanged"
                                                        ToolTip="Service Types Categories"
                                                        Width="180px">
                                                    </asp:DropDownList>

                                                </td>

                                                <td colspan="10" class="auto-style1 order-search-section">&nbsp;
                                                </td>
                                                <td class="auto-style1" style="padding-left: 20px">
                                                    <asp:Label runat="server" ID="MachineNameLabel">Machine Name:</asp:Label>
                                                </td>
                                                <td class="auto-style1" style="text-align: right">
                                                    <asp:Label runat="server" ID="MachineName">
                                                    </asp:Label>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label runat="server">Service Types:</asp:Label>
                                                </td>
                                                <td class="auto-style1">
                                                    <corptControl:ServiceTypeCheckBoxesDropDown
                                                        ID="ServiceTypesServiceTypeCheckBoxList" runat="server"
                                                        DataSourceID="ServiceTypesDataSource"
                                                        Width="180px"
                                                        ScreeningDefaultSelection="7, 12, 13"
                                                        OnSelectedIndexChanged="ServiceTypesCheckBoxList_OnSelectedIndexChanged" />
                                                    <asp:ObjectDataSource ID="ServiceTypesDataSource" runat="server"
                                                        TypeName="Corpt.AccountRep"
                                                        SelectMethod="GetServiceTypes" />
                                                </td>
                                                <td style="padding-left: 30px; padding-top: 0" class="auto-style1">
                                                    <asp:Label runat="server">Create Date:</asp:Label>
                                                </td>
                                                <td style="padding-left: 10px; padding-top: 0" class="auto-style1">
                                                    <asp:TextBox runat="server"
                                                        ID="BeginCreateDate"
                                                        AutoPostBack="True" Width="75px" />
                                                    <ajaxToolkit:CalendarExtender runat="server"
                                                        ID="BeginCalendarExtender"
                                                        ClearTime="True"
                                                        TargetControlID="BeginCreateDate"
                                                        OnClientDateSelectionChanged="dateSelectionChanged" />
                                                </td>
                                                <td style="padding-left: 0; padding-right: 0; padding-top: 0; text-align: center" class="auto-style1">-
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server"
                                                        ID="EndCreateDate"
                                                        AutoPostBack="True" Width="75px" />
                                                    <ajaxToolkit:CalendarExtender runat="server"
                                                        ID="EndCalendarExtender"
                                                        ClearTime="True"
                                                        TargetControlID="EndCreateDate" />
                                                </td>
                                                <script type="text/javascript">
                                                    function dateSelectionChanged(sender, args) {
                                                        __doPostBack('<%=btnReload.ClientID %>', "dateSelectionChanged");
                                                    }
                                                </script>
                                                <td style="padding-left: 30px; padding-top: 0" class="auto-style1">
                                                    <asp:Label runat="server">PO:</asp:Label>
                                                </td>
                                                <td style="padding-left: 10px; padding-top: 0" class="auto-style1">
                                                    <asp:TextBox runat="server" ID="POTextBox"
                                                        Width="75px"
                                                        OnTextChanged="POTextBox_TextChanged"
                                                        ToolTip="Search Orders with PO strictly equal the value"
                                                        Visible="False" />
                                                </td>
                                                <td style="padding-left: 10px; padding-top: 0" class="auto-style1">
                                                    <asp:DropDownList
                                                        ID="PoDropDownList" runat="server"
                                                        AutoPostBack="True"
                                                        DataTextField="DisplayText"
                                                        DataValueField="Value"
                                                        CssClass=""
                                                        OnSelectedIndexChanged="PoDropDownList_OnSelectedIndexChanged"
                                                        ToolTip="Available PO" Width="200px"
                                                        Enabled="False" />
                                                </td>

                                                <td colspan="3" class="order-search-section" style="padding-top: 0">
                                                    <asp:Button ID="ExtendedOrderSearchButton" runat="server"
                                                        Text="Extended Order Search  (0)"
                                                        ToolTip="Search by current customer order list"
                                                        CommandName="ExtendedOrderSearchCommand"
                                                        OnCommand="ExtendedOrderSearchButton_OnCommand"
                                                        Visible="True" />
                                                </td>
                                                <td colspan="2" class="auto-style1" style="padding-left: 20px;">
                                                    <asp:DropDownList
                                                        ID="TimeZoneModeDropDownList" runat="server"
                                                        AutoPostBack="True"
                                                        CssClass=""
                                                        OnSelectedIndexChanged="TimeZoneDropDownList_OnSelectedIndexChanged"
                                                        ToolTip="Time zone type for display date time data"
                                                        Width="200px" />
                                                </td>

                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Button ID="btnReload" runat="server" Style="display: none;" />
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="padding-left: 5px; padding-top: 10px;">
                                                <ajaxToolkit:TabContainer
                                                    ID="TabContainer" runat="server"
                                                    OnDemand="False"
                                                    ActiveTabIndex="0"
                                                    AutoPostBack="True"
                                                    Height="630px">
                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Open Orders, Stones" ID="OrdersTab"
                                                        OnDemandMode="Once" Height="620px">
                                                        <ContentTemplate>
                                                            <table>
                                                                <tr>
                                                                    <!-- First Row on "Open Orders, Stones" tab -->
                                                                    <td class="first-column">
                                                                        <!-- First Column: Order Tree -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Order Tree Caption -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="font-size: medium; width: 120px; vertical-align: middle;">
                                                                                                <asp:Label runat="server" Text="Order Tree" />
                                                                                            </td>
                                                                                            <td style="width: 460px; vertical-align: middle; text-align: end;" class="auto-style1">
                                                                                                <asp:Label runat="server" ID="OrdersTreeViewStatusBar" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Order Tree Toolbar -->
                                                                                <td style="height: 20px; text-align: end;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="ExpandButton2" runat="server"
                                                                                                                ImageUrl="~/Images/ajaxImages/expand_blue.jpg"
                                                                                                                ToolTip="Expand All tree nodes"
                                                                                                                OnClick="ExpandButtonClick" />
                                                                                                        </td>
                                                                                                        <td style="padding-left: 10px;">
                                                                                                            <asp:ImageButton ID="CollapseButton2" runat="server"
                                                                                                                ImageUrl="~/Images/ajaxImages/collapse_blue.jpg"
                                                                                                                ToolTip="Collapse All tree nodes"
                                                                                                                OnClick="CollapseButtonClick" />
                                                                                                        </td>
                                                                                                        <td style="padding-left: 10px;">
                                                                                                            <asp:ImageButton ID="EnsureSelectionVisibleButton" runat="server"
                                                                                                                ImageUrl="~/Images/ajaxImages/rightArrowBl.png"
                                                                                                                ToolTip="Ensure selected node visible"
                                                                                                                OnClientClick="javascript: return Tree_scrollIntoView();" />
                                                                                                        </td>
                                                                                                        <td style="padding-left: 10px;">
                                                                                                            <asp:ImageButton ID="CheckAllTreeNodesButton" runat="server"
                                                                                                                ImageUrl="~/Images/ajaxImages/checkbox.png"
                                                                                                                ToolTip="Check All Nodes"
                                                                                                                OnClientClick="OrdersTreeViewCheckAllNodes(event);" />
                                                                                                        </td>
                                                                                                        <td style="padding-left: 10px;">
                                                                                                            <asp:ImageButton ID="UnCheckAllTreeNodesButton" runat="server"
                                                                                                                ImageUrl="~/Images/ajaxImages/unCheckbox.png"
                                                                                                                ToolTip="Uncheck All Nodes"
                                                                                                                OnClientClick="OrdersTreeViewUnCheckAllNodes(event);" />
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="width: 500px; vertical-align: middle; text-align: end;">
                                                                                                <asp:Label runat="server" ID="OrdersTreeViewChecked" Height="20px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Order Tree View -->
                                                                                <td>
                                                                                    <div class="first-row-content-tab scrolableTreeView" id="OrdersTreeViewDiv" runat="server">
                                                                                        <asp:TreeView
                                                                                            ID="OrdersTreeView" runat="server"
                                                                                            AutoPostBack="true"
                                                                                            ShowCheckBoxes="All"
                                                                                            ExpandDepth="0"
                                                                                            ShowLines="True"
                                                                                            OnSelectedNodeChanged="OrdersTreeView_OnSelectedNodeChanged"
                                                                                            AfterClientCheck="CheckChildNodes();"
                                                                                            onclick="OnTreeClickWrapper(event)">

                                                                                            <HoverNodeStyle Font-Bold="True" Font-Italic="True" ForeColor="Black" BackColor="LightBlue" />
                                                                                            <NodeStyle Font-Names="Cambria" Font-Size="Small" />
                                                                                            <SelectedNodeStyle Font-Bold="True" Font-Italic="True" ForeColor="White" BackColor="Blue" />
                                                                                        </asp:TreeView>
                                                                                        <script type="text/javascript">
                                                                                            function OnTreeClickWrapper(evt) {
                                                                                                var src = window.event !== window.undefined ? window.event.srcElement : evt.target;
                                                                                                var isChkBoxClick = (src.tagName.toLowerCase() === "input" && src.type === "checkbox");
                                                                                                if (isChkBoxClick) {
                                                                                                    OnTreeClick(evt);
                                                                                                    __doPostBack('<%=OrdersTreeView.ClientID %>', "TreeNodeChecked");
                                                                                                }
                                                                                            }
                                                                                        </script>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Order Tree Command -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <asp:Button ID="EndSessionButton" runat="server"
                                                                                                    Text="End Session"
                                                                                                    ToolTip="End Session"
                                                                                                    CommandName="EndSessionCommand"
                                                                                                    OnCommand="EndSessionButton_OnCommand"
                                                                                                    Visible="True" />
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <asp:Button ID="PrintLabelButton" runat="server"
                                                                                                    Text="Print Label"
                                                                                                    ToolTip="Print Label"
                                                                                                    CommandName="PrintLabelCommand"
                                                                                                    OnCommand="PrintLabelButton_OnCommand"
                                                                                                    Visible="False" />
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <asp:Button ID="BillingButton" runat="server"
                                                                                                    Text="Billing"
                                                                                                    ToolTip="Billing"
                                                                                                    CommandName="BillingCommand"
                                                                                                    OnCommand="BillingButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 14px">
                                                                                                <asp:CheckBox runat="server" ID="BulkBillingCheckBox"
                                                                                                    CssClass="radio-horizontal-style"
                                                                                                    TextAlign="Right"
                                                                                                    Text="Bulk Billing"
                                                                                                    OnCheckedChanged="BulkBillingCheckBox_OnCheckedChanged"
                                                                                                    onclick="OnBulkBillingClick(event)"
                                                                                                    Checked="True" />
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <asp:Button ID="BatchLabelPropertiesButton" runat="server"
                                                                                                    Text="Batch Label"
                                                                                                    ToolTip="Edit Batch Label Properties"
                                                                                                    CommandName="BatchLabelPropertiesCommand"
                                                                                                    OnCommand="BatchLabelProperties_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="UpdateOrderButton" runat="server"
                                                                                                    Text="Update Order"
                                                                                                    ToolTip="UpdateOrder"
                                                                                                    CommandName="UpdateOrderCommand"
                                                                                                    OnCommand="UpdateOrderButton_OnCommand"
                                                                                                    OnClick="UpdateOrderButton_Click"/>
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <asp:Button ID="FinalLabelBtn" runat="server"
                                                                                                    Text="Print Labels" ToolTip="Print Labels"
                                                                                                    CommandName="PrintLabelsCommand"
                                                                                                    OnCommand="PrintLabels_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 1px; padding-top: 10px">
                                                                                                <div class="form-group" style="margin-left:1px;border:5px solid brown;" id="SkuUploadDiv" runat="server">
                                                                                                    <asp:Button ID="FinalLabelLEBtn" runat="server"  Style="font-weight:bold"
                                                                                                        Text="Print LE Labels" ToolTip="Print LE Labels"
                                                                                                        CommandName="PrintLELabelsCommand"
                                                                                                        OnCommand="PrintLELabels_OnCommand" />
                                                                                                    <asp:Label Text="LECount" runat="server" ID="LECountLbl" Style="font-weight:bold"></asp:Label>
                                                                                                    <asp:TextBox runat="server" ID="LELabelsCount" MaxLength="3" Width="20px" style="text-align:center;font-weight:bold;"></asp:TextBox>
                                                                                                </div>
                                                                                            </td>
                                                                                            <script type="text/javascript">
                                                                                                function OnBulkBillingClick(evt) {
                                                                                                    __doPostBack('<%=btnReload.ClientID %>', "BulkBillingChecked");
                                                                                                }
                                                                                            </script>

                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="second-column">
                                                                        <!-- Second Column: Additional Service -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Additional Service DataGrid Caption -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="font-size: medium; width: 198px; vertical-align: middle;">
                                                                                                <asp:Label runat="server" Text="Additional Services" />
                                                                                            </td>
                                                                                            <td class="auto-style1" style="width: 80px; vertical-align: middle; text-align: end;">
                                                                                                <asp:Label runat="server" ID="AdditionalServiceDataGridStatusBar" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Additional Service Toolbar -->
                                                                                <td style="height: 20px; text-align: end;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px;">
                                                                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                                                                    Visible="False"
                                                                                                    ImageUrl="~/Images/ajaxImages/expand.jpg"
                                                                                                    ToolTip="Expand All tree nodes"
                                                                                                    OnClick="ExpandButtonClick" />
                                                                                            </td>
                                                                                            <td>(for checked in Order Tree nodes)</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Additional Service DataGrid -->
                                                                                <td>
                                                                                    <div class="first-row-content-tab scrolableDataGrid" id="AdditionalServiceDataGridDiv">
                                                                                        <asp:GridView runat="server"
                                                                                            ID="AdditionalServicesDataGrid"
                                                                                            CssClass="grid"
                                                                                            AutoGenerateColumns="False"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            OnRowDataBound="AdditionalServicesDataGrid_OnRowDataBound"
                                                                                            OnSelectedIndexChanged="AdditionalServicesDataGrid_OnSelectedIndexChanged">
                                                                                            <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <HeaderTemplate>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox runat="server" ID="AdditionalServicesCheckedCheckBox"
                                                                                                            Text="" AutoPostBack="True" OnCheckedChanged="AdditionalServicesCheckedCheckBox_OnCheckedChanged"
                                                                                                            Checked='<%# DataBinder.Eval(Container.DataItem, "Checked") %>' />

                                                                                                    </ItemTemplate>

                                                                                                    <HeaderStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />

                                                                                                    <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="Name" HeaderText="Service">
                                                                                                    <HeaderStyle Width="140px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="140px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="OrderCode" HeaderText="Order">
                                                                                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="70px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="BatchCode" HeaderText="Batch">
                                                                                                    <HeaderStyle Width="40px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="40px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                            </Columns>

                                                                                            <EditRowStyle CssClass="grid-edit-item" />

                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" />

                                                                                            <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />

                                                                                            <RowStyle CssClass="grid-item" Wrap="False" />

                                                                                            <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Additional Service Command -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="AdditionalServiceClearListButton" runat="server"
                                                                                                    Text="Clear List"
                                                                                                    ToolTip="Clear Additional Services List"
                                                                                                    CommandName="AdditionalServiceClearListCommand"
                                                                                                    OnCommand="AdditionalServiceClearListButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="AdditionalServiceRefreshListButton" runat="server"
                                                                                                    Text="Refresh List"
                                                                                                    ToolTip="Refresh Additional Services List"
                                                                                                    CommandName="AdditionalServiceRefreshListCommand"
                                                                                                    OnCommand="AdditionalServiceRefreshListButton_OnCommand" />
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="third-column">
                                                                        <!-- Second Column: Migrated Items -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Migrated Items DataGrid Caption -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="font-size: medium; width: 350px; vertical-align: middle;">
                                                                                                <asp:Label runat="server" ID="MigratedItemDataGridCaption" Text="Migrated Items" />
                                                                                            </td>
                                                                                            <td class="auto-style1" style="width: 100px; vertical-align: middle; text-align: end;">
                                                                                                <asp:Label runat="server" ID="MigratedItemDataGridStatusBar" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Migrated Items Toolbar -->
                                                                                <td style="height: 20px; text-align: end;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px;">
                                                                                                <asp:ImageButton ID="ImageButton2" runat="server"
                                                                                                    Visible="False"
                                                                                                    ImageUrl="~/Images/ajaxImages/expand.jpg"
                                                                                                    ToolTip="Expand All tree nodes"
                                                                                                    OnClick="ExpandButtonClick" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Migrated Items DataGrid -->
                                                                                <td>
                                                                                    <div class="first-row-content-tab scrolableDataGrid" id="MigratedItemDataGridDiv">
                                                                                        <asp:GridView runat="server"
                                                                                            ID="MigratedItemsDataGrid"
                                                                                            CssClass="grid"
                                                                                            AutoGenerateColumns="False"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            OnRowDataBound="MigratedItemsDataGrid_OnRowDataBound"
                                                                                            OnSelectedIndexChanged="MigratedItemsDataGrid_OnSelectedIndexChanged">
                                                                                            <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="PrevItemNumber" HeaderText="From Old #">
                                                                                                    <HeaderStyle Width="150px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="150px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="CurrentItemNumber" HeaderText="Current #">
                                                                                                    <HeaderStyle Width="150px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="150px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="NewItemNumber" HeaderText="To New #">
                                                                                                    <HeaderStyle Width="150px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="150px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                            </Columns>

                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" />
                                                                                            <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />
                                                                                            <RowStyle CssClass="grid-item" Wrap="False" />
                                                                                            <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Migrated Items Command -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="RedirectToNewItemButton" runat="server"
                                                                                                    Text="Open"
                                                                                                    ToolTip="Open the page for 'To New #' item of selected row"
                                                                                                    CommandName="RedirectToNewItemCommand"
                                                                                                    OnCommand="RedirectToNewItemButton_OnCommand" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="forth-column" style="vertical-align: top !important;">
                                                                        <!-- Forth Column: Current Batch Items -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Current Batch Items DataGrid Caption -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="font-size: medium; width: 198px; vertical-align: middle;">
                                                                                                <asp:Label runat="server" Text="Items" />
                                                                                            </td>
                                                                                            <td class="auto-style1" style="width: 80px; vertical-align: middle; text-align: end;">
                                                                                                <asp:Label runat="server" ID="Label1" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Current Batch Items Toolbar -->
                                                                                <td style="height: 20px; text-align: end;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px;">
                                                                                                <asp:ImageButton ID="ImageButton3" runat="server"
                                                                                                    Visible="False"
                                                                                                    ImageUrl="~/Images/ajaxImages/expand.jpg"
                                                                                                    ToolTip="Expand All tree nodes"
                                                                                                    OnClick="ExpandButtonClick" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Current Batch Items DataGrid -->
                                                                                <td>
                                                                                    <div class="first-row-content-tab scrolableDataGrid" id="CurrentBatchItemsDataGridDiv">
                                                                                        <asp:GridView runat="server"
                                                                                            ID="CurrentBatchItemsDataGrid"
                                                                                            CssClass="grid"
                                                                                            AutoGenerateColumns="False"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            OnRowDataBound="CurrentBatchItemsDataGrid_OnRowDataBound"
                                                                                            OnSelectedIndexChanged="CurrentBatchItemsDataGrid_OnSelectedIndexChanged">
                                                                                            <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="FullItemNumber" HeaderText="Full Item Code">
                                                                                                    <HeaderStyle Width="206px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="206px" HorizontalAlign="Center" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                            </Columns>

                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" />

                                                                                            <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />

                                                                                            <RowStyle CssClass="grid-item" Wrap="False" />

                                                                                            <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Current Batch Items Command -->
                                                                                <td style="max-height: 26px; min-height: 26px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <!-- Second Row on "Open Orders, Stones" tab -->
                                                                    <td>
                                                                        <!-- Measure DataGrid -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Measure DataGrid Caption -->
                                                                                <td style="height: 20px; padding-top: 20px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="width: 435px; vertical-align: middle; font-size: small">
                                                                                                <asp:Label runat="server" ID="MeasuresDataGridCaption" />
                                                                                            </td>
                                                                                            <td style="width: 200px; vertical-align: middle; text-align: end;" class="auto-style1">
                                                                                                <asp:Label runat="server" ID="MeasureDataGridStatusBar" />
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Measure DataGrid -->
                                                                                <td>
                                                                                    <div id="MeasureDataGridDiv" class="second-row-content-tab scrolableDataGrid">
                                                                                        <asp:GridView runat="server"
                                                                                            ID="ItemMeasuresDataGrid"
                                                                                            CssClass="grid"
                                                                                            AutoGenerateColumns="False"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            OnRowDataBound="ItemMeasuresDataGrid_OnRowDataBound"
                                                                                            OnSelectedIndexChanged="ItemMeasuresDataGrid_OnSelectedIndexChanged">
                                                                                            <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="PartName" HeaderText="Part Name">
                                                                                                    <HeaderStyle Width="154px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="154px" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="MeasureName" HeaderText="Grade">
                                                                                                    <HeaderStyle Width="122px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="122px" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="ResultValue" HeaderText="Value">
                                                                                                    <HeaderStyle Width="170px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="170px" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="PrevResultValue" HeaderText="Previous Value">
                                                                                                    <HeaderStyle Width="126px" HorizontalAlign="Center" />

                                                                                                    <ItemStyle Width="126px" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                            </Columns>

                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" />

                                                                                            <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />

                                                                                            <RowStyle CssClass="grid-item" Wrap="False" />

                                                                                            <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Measure Commands -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px; padding-top: 10px; visibility: hidden">
                                                                                                <asp:Button ID="Button1" runat="server"
                                                                                                    Text="Refresh"
                                                                                                    ToolTip="Refresh"
                                                                                                    CommandName="Refresh" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    </td>
                                                                    <!--
                                                                    <td style="<%= BulkBillingHideStyle %>" colspan="3">
                                                                        <table style="width: 100%; height: 100%;">
                                                                            <tr>
                                                                                <td style="padding-left: 40px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="text-align: center; height: 20px; padding-top: 20px">
                                                                                                <a runat="server" id="PictureLink"
                                                                                                    class="panel-caption image-caption-style"
                                                                                                    target="_blank" rel="noreferrer noopener">Picture
                                                                                                </a>

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <a class="image-a">
                                                                                                    <asp:Panel runat="server" ID="PicturePanel" CssClass="second-row-content-tab form-horizontal image-panel">
                                                                                                        <asp:Image runat="server" class="image-style" ID="PictureImage"
                                                                                                            AlternateText="No Image" />
                                                                                                    </asp:Panel>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td style="padding-left: 40px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="text-align: center; height: 20px; padding-top: 20px">
                                                                                                <a runat="server" id="ShapeLink"
                                                                                                    class="panel-caption image-caption-style"
                                                                                                    target="_blank" rel="noreferrer noopener">Shape
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <a class="image-a">
                                                                                                    <asp:Panel runat="server" ID="ShapePanel" CssClass="form-horizontal image-panel">
                                                                                                        <asp:Image runat="server" class="image-style" ID="ShapeImage"
                                                                                                            AlternateText="No Image" />
                                                                                                    </asp:Panel>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
-->
                                                                    <td class="invoice-column" colspan="3">
                                                                        <!-- Invoices DataGrid -->
                                                                        <table>
                                                                            <tr>
                                                                                <!-- Invoices DataGrid Caption -->
                                                                                <td style="height: 20px; padding-top: 20px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="panel-caption" style="width: 50px; vertical-align: middle; font-size: small;">
                                                                                                <asp:Label runat="server" ID="InvoicesDataGridCaption"
                                                                                                    Text="Invoice:" />
                                                                                            </td>
                                                                                            <td class="panel-caption" style="padding-left: 5px; vertical-align: middle; font-size: small;">
                                                                                                <asp:DropDownList
                                                                                                    ID="InvoicesDropDownList" runat="server"
                                                                                                    AutoPostBack="True"
                                                                                                    CssClass=""
                                                                                                    OnSelectedIndexChanged="InvoicesDropDownList_OnSelectedIndexChanged"
                                                                                                    Width="440px" />
                                                                                            </td>

                                                                                            <td class="panel-caption" style="padding-left: 15px; width: 30px; vertical-align: middle; font-size: small;">
                                                                                                <asp:Label runat="server" ID="OrdersInvoice"
                                                                                                    Text="Order:" />
                                                                                            </td>
                                                                                            <td class="panel-caption" style="padding-left: 5px; vertical-align: middle; font-size: small;">
                                                                                                <asp:DropDownList
                                                                                                    ID="OrdersInvoiceDropDownList" runat="server"
                                                                                                    AutoPostBack="True"
                                                                                                    DataTextField="GroupCode"
                                                                                                    DataValueField="InvoiceId"
                                                                                                    CssClass=""
                                                                                                    OnSelectedIndexChanged="OrdersInvoiceDropDownList_OnSelectedIndexChanged"
                                                                                                    ToolTip="<OrderCode> <InvoiceId>" Width="90px" />
                                                                                            </td>

                                                                                            <td style="padding-left: 15px; padding-top: 0" class="auto-style1">
                                                                                                <asp:Label runat="server" ID="InvoiceAuthorLabel">Author:</asp:Label>
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 0; font-weight: bold" class="auto-style1">
                                                                                                <asp:Label runat="server" ID="InvoiceAuthorValue"></asp:Label>
                                                                                            </td>

                                                                                            <td style="width: 0px; vertical-align: middle; text-align: end; display: none;" class="auto-style1">
                                                                                                <asp:Label runat="server" ID="InvoicesDataGridStatusBar" />
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Item Invoices DataGrid -->
                                                                                <td>
                                                                                    <div id="InvoiceItemsGridDiv" class="second-row-content-tab scrolableDataGrid invoice-grid">
                                                                                        <asp:GridView runat="server"
                                                                                            ID="InvoiceItemsGridView"
                                                                                            CssClass="grid"
                                                                                            AutoGenerateColumns="False"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            OnRowDataBound="InvoicesGridView_OnRowDataBound"
                                                                                            OnSelectedIndexChanged="InvoiceItemsGridView_OnSelectedIndexChanged">
                                                                                            <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="Status" HeaderText="Status">
                                                                                                    <HeaderStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Create Date">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateDateLabel" runat="server"
                                                                                                            Text="<%# ApplyTimeZoneOffset(Container.DataItem) %>" />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle Width="250px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="250px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField>
                                                                                                    <HeaderStyle Width="0px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="0px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="FullItemCode" HeaderText="Item">
                                                                                                    <HeaderStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="Description" HeaderText="Description">
                                                                                                    <HeaderStyle Width="500px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="500px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Cost">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateDateLabel" runat="server"
                                                                                                            CssClass="CurrencyColumn"
                                                                                                            Text='<%# string.Format("{0:C}", Eval("Price")) %>' />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <ItemStyle Width="150px" HorizontalAlign="Right" VerticalAlign="Middle" Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>

                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" />
                                                                                            <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />
                                                                                            <RowStyle CssClass="grid-item" Wrap="False" />
                                                                                            <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <!-- Invoices Commands -->
                                                                                <td style="height: 20px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="InvoiceRefreshButton" runat="server"
                                                                                                    Text="Load Invoices"
                                                                                                    ToolTip="Load/Refresh customer's Invoices"
                                                                                                    CommandName="RefreshInvoices"
                                                                                                    OnCommand="InvoiceRefreshButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="OpenInvoiceButton" runat="server"
                                                                                                    Text="Open QB Invoice"
                                                                                                    ToolTip="Open Current Invoice from QuickBooks(PDF)"
                                                                                                    CommandName="OpenQbInvoice"
                                                                                                    OnCommand="OpenQbInvoicesButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="PrintInvoiceButton" runat="server"
                                                                                                    Text="Print QB Invoice"
                                                                                                    ToolTip="Print Current Invoice from QuickBooks(PDF)"
                                                                                                    CommandName="PrintQbInvoice"
                                                                                                    OnCommand="PrintQbInvoicesButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="Button2" runat="server"
                                                                                                    Text="Print All Completed"
                                                                                                    ToolTip="Print All Completed QuickBooks Invoices"
                                                                                                    CommandName="PrintInvoices"
                                                                                                    OnCommand="PrintAllCompletedInvoicesButton_OnCommand" />
                                                                                            </td>
                                                                                            <td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Button ID="Button3" runat="server"
                                                                                                    Text="Set As Printed"
                                                                                                    ToolTip="Set Orders As Printed"
                                                                                                    OnCommand="SetOrdersPrinted_Click" />
                                                                                            </td>
                                                                                            <%--<td style="padding-left: 5px; padding-top: 10px">
                                                                                                <asp:Label ID="ErrPrintedOrders" runat="server" ForeColor="Red" Width="300px" runat="server"></asp:Label>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </ContentTemplate>


                                                    </ajaxToolkit:TabPanel>

                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Customer" ID="CustomerTab"
                                                        OnDemandMode="None" Visible="False">
                                                    </ajaxToolkit:TabPanel>

                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Update Order" ID="UpdateOrderTab"
                                                        OnDemandMode="None" Visible="False">
                                                    </ajaxToolkit:TabPanel>

                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Reports" ID="ReportsTab"
                                                        OnDemandMode="None" Visible="False">
                                                    </ajaxToolkit:TabPanel>

                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Orders for Delivery" ID="OrdersForDeliveryTab"
                                                        OnDemandMode="None" Visible="False">
                                                    </ajaxToolkit:TabPanel>

                                                    <ajaxToolkit:TabPanel runat="server" HeaderText="Blocked Parts" ID="BlockedPartsTab"
                                                        OnDemandMode="None" Visible="False">
                                                    </ajaxToolkit:TabPanel>

                                                </ajaxToolkit:TabContainer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <%-- Information Dialog --%>
                        <asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 560px; display: none">
                            <asp:Panel runat="server" ID="InfoPanelDragHandle"
                                Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <div>
                                    <asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
                                    <b><span runat="server" id="InfoPanelTitle">Information</span></b>
                                </div>
                            </asp:Panel>
                            <div runat="server" id="MessageDiv" style="overflow: auto; max-width: 500px; max-height: 300px; margin-top: 10px" />
                            <ul runat="server" id="MessageList" style="max-height: 200px; overflow-y: auto;" />
                            <div style="padding-top: 10px">
                                <p style="text-align: center;">
                                    <asp:Button ID="InfoCloseButton" runat="server" Text="Ok" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="InfoPopupExtender"
                            TargetControlID="PopupInfoButton"
                            PopupControlID="InfoPanel"
                            PopupDragHandleControlID="InfoPanelDragHandle"
                            OkControlID="InfoCloseButton">
                        </ajaxToolkit:ModalPopupExtender>

                        <%-- Question Dialog --%>
                        <asp:Panel runat="server" ID="QuestionDialog" CssClass="modalPopup" Style="width: 430px; display: none">
                            <asp:Panel runat="server" ID="SaveQDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <div>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                                    <b runat="server" id="QuestionTitle" />
                                </div>
                            </asp:Panel>
                            <div runat="server" id="FirstParagraph" style="max-width: 400px; max-height: 300px; margin-top: 20px" />
                            <ul runat="server" id="SecondListParagraph" style="max-width: 400px; max-height: 200px; overflow-y: auto; margin-top: 10px;"></ul>
                            <div runat="server" id="ThirdParagraph" style="max-width: 400px; padding-top: 10px"></div>
                            <div style="padding-top: 10px">
                                <p style="text-align: center;">
                                    <asp:Button ID="QuesSaveYesBtn" runat="server" Text="Yes" OnClick="OnQuesSaveYesClick" />
                                    <asp:Button ID="QuesSaveNoBtn" runat="server" Text="No" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="PopupQuestBtn" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server"
                            ID="SaveQPopupExtender"
                            TargetControlID="PopupQuestBtn"
                            PopupControlID="QuestionDialog"
                            PopupDragHandleControlID="SaveQDragHandle"
                            OkControlID="QuesSaveNoBtn">
                        </ajaxToolkit:ModalPopupExtender>

                        <%-- Billing Options Dialog --%>
                        <style type="text/css">
                            .billing-option {
                                width: 1300px !important;
                            }

                            .billing-option-min-max {
                                max-width: 1300px;
                                min-width: 1300px
                            }

                            .billing-option-caption {
                                vertical-align: middle;
                            }

                            .billing-option-caption-counts {
                                vertical-align: middle;
                            }

                            .hidden-column {
                                visibility: hidden;
                                min-width: 0;
                                max-width: 0;
                            }
                        </style>
                        <script type="text/javascript">

                            function OnClickRegularBilling(evt) {
                                var regularCheckBox = document.getElementById('<%= RegularCheckBox.ClientID %>');
                                var addSkuCheckBox = document.getElementById('<%= AddSKUCheckBox.ClientID %>');
                                var addLotNoCheckBox = document.getElementById('<%= AddLotNoCheckBox.ClientID %>');

                                addSkuCheckBox.checked = false;
                                addLotNoCheckBox.checked = false;
                                if (!regularCheckBox.checked) {
                                    regularCheckBox.checked = true;
                                }
                            }

                            function OnClickAddSKUBilling(evt) {
                                var regularCheckBox = document.getElementById('<%= RegularCheckBox.ClientID %>');
                                var addSkuCheckBox = document.getElementById('<%= AddSKUCheckBox.ClientID %>');
                                var addLotNoCheckBox = document.getElementById('<%= AddLotNoCheckBox.ClientID %>');

                                if (addSkuCheckBox.checked) {
                                    regularCheckBox.checked = false;
                                } else {
                                    if (!addLotNoCheckBox.checked) {
                                        regularCheckBox.checked = true;
                                    }
                                }
                            }

                            function OnClickAddLotNoBilling(evt) {
                                var regularCheckBox = document.getElementById('<%= RegularCheckBox.ClientID %>');
                                var addSkuCheckBox = document.getElementById('<%= AddSKUCheckBox.ClientID %>');
                                var addLotNoCheckBox = document.getElementById('<%= AddLotNoCheckBox.ClientID %>');

                                if (addLotNoCheckBox.checked) {
                                    regularCheckBox.checked = false;
                                } else {
                                    if (!addSkuCheckBox.checked) {
                                        regularCheckBox.checked = true;
                                    }
                                }
                            }

                            function BulkBillingPricesGridViewClick(evt) {
                                BulkBillingPricesGridViewClickElement(evt.srcElement);
                                evt.stopPropagation();
                            }

                            function BulkBillingPricesGridViewClickElement(srcElement) {
                                var rowElement = srcElement.parentElement;
                                while (!(rowElement.tagName.toLowerCase() === "tr" || rowElement.tagName.toLowerCase() === "th")) {
                                    rowElement = rowElement.parentElement;
                                }

                                var rowIndex = rowElement.sectionRowIndex;

                                var tableElement = srcElement.parentElement;
                                while (!(tableElement.tagName.toLowerCase() === "table")) {
                                    tableElement = tableElement.parentElement;
                                }

                                const row = tableElement.rows[rowIndex];
                                const headerRow = tableElement.rows[0];

                                const conditionCheckBox = getRowCellElementByHeaderText(headerRow, row, "AnswerImage");
                                const pricesLabel = getRowCellElementByHeaderText(headerRow, row, "Prices");
                                const defaultPricesLabel = getRowCellElementByHeaderText(headerRow, row, "DefaultPrices");
                                const specialPricesLabel = getRowCellElementByHeaderText(headerRow, row, "SpecialPrices");
                                const quantityLabel = getRowCellElementByHeaderText(headerRow, row, "Quantities");
                                const costsLabel = getRowCellElementByHeaderText(headerRow, row, "Costs");

                                if (conditionCheckBox.checked) {
                                    pricesLabel.textContent = specialPricesLabel.textContent;
                                } else {
                                    pricesLabel.textContent = defaultPricesLabel.textContent;
                                }

                                if (quantityLabel.textContent) {
                                    costsLabel.textContent = getCosts(quantityLabel.textContent, pricesLabel.textContent);
                                }
                            }

                            function getRowCellElementByHeaderText(headerRow, row, headerText) {
                                const m = headerRow.cells.length;
                                for (let i = 0; i < m; i++) {
                                    const text = headerRow.cells[i].textContent.trim();
                                    if (text === headerText) {
                                        return row.cells[i].firstElementChild;
                                    }
                                }
                                console.log(`Cannot find '${headerText}' in header row`);
                                return null;
                            }

                            function getCosts(quantitySet, priceSet) {
                                var quantities = quantitySet.split('/');
                                var prices = priceSet.split('/');

                                var formatter = new Intl.NumberFormat('en-US', {
                                    style: 'currency',
                                    currency: 'USD'
                                });

                                var passCost = formatter.format((ConvertToInt(quantities[0]) * ConvertToNumber(prices[0])));
                                var rejectCost = formatter.format(0.00);
                                if (quantities.length > 1) {
                                    rejectCost = formatter.format((ConvertToInt(quantities[1]) * ConvertToNumber(prices[1])));
                                }
                                var costs = passCost + ' / ' + rejectCost;
                                //if (prices.length > 2) {
                                //    costs = costs + ' / ' + formatter.format(ConvertToNumber(prices[2]));
                                //}
                                return costs;
                            }

                            function ConvertToInt(number) {
                                return Number(number.trim());
                            }

                            function ConvertToNumber(currency) {
                                return Number(currency.replace(/[^0-9.-]+/g, ""));
                            }

                            function BulkBillingPricesCheckAllRows(e) {
                                BulkBillingPricesCheckUncheckAllRows(true);
                                e.stopPropagation();
                            }

                            function BulkBillingPricesUnCheckAllRows(e) {
                                BulkBillingPricesCheckUncheckAllRows(false);
                                e.stopPropagation();
                            }

                            function BulkBillingPricesCheckUncheckAllRows(isChecked) {
                                var inputs = $("#<%= BulkBillingPricesGridView.ClientID %> input[type=checkbox]");
                                for (var i = 0; i < inputs.length; i++) {
                                    if (!inputs[i].disabled) {
                                        inputs[i].checked = isChecked;
                                        BulkBillingPricesGridViewClickElement(inputs[i]);
                                    }
                                }
                            }

                        </script>
                        <asp:Panel runat="server" ID="BillingOptionsDialog" CssClass="modalPopup billing-option" Style="display: none">
                            <asp:Panel runat="server" ID="BillingOptionsDragHandle"
                                Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <div>
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                                    <asp:Label runat="server" ID="BillingOptionsLabel"
                                        CssClass="panel-caption billing-option-caption" />
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="BulkBillingPricesPanel"
                                CssClass="billing-option-min-max"
                                Style="text-align: left; margin-top: 20px;">
                                <table>
                                    <tr>
                                        <!-- Bulk Billing Prices GridView Caption -->
                                        <td>
                                            <table>
                                                <tr>
                                                    <td style="text-align: left; width: 344px;">
                                                        <asp:Label runat="server" Text="Orders Pricing Settings"
                                                            CssClass="panel-sub-caption billing-option-caption" />
                                                    </td>
                                                    <td style="text-align: right; width: 761px;">
                                                        <asp:Label runat="server" ID="BulkBillingPricesGridViewStatusBar"
                                                            CssClass="billing-option-caption-counts" />

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Bulk Billing Prices GridView Toolbar -->
                                        <td style="height: 20px; text-align: end;">
                                            <table>
                                                <tr>
                                                    <td style="padding-left: 10px;">
                                                        <asp:ImageButton ID="BulkBillingPricesCheckAllRowsButton" runat="server"
                                                            ImageUrl="~/Images/ajaxImages/checkbox.png"
                                                            ToolTip="Answer 'Yes' for all enabled questions (check all)"
                                                            OnClientClick="BulkBillingPricesCheckAllRows(event); return false;" />
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <asp:ImageButton ID="BulkBillingPricesUnCheckAllRows" runat="server"
                                                            ImageUrl="~/Images/ajaxImages/unCheckbox.png"
                                                            ToolTip="Answer 'No' for all enabled questions (uncheck all)"
                                                            OnClientClick="BulkBillingPricesUnCheckAllRows(event); return false;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Bulk Billing Prices GridView -->
                                        <td>
                                            <style type="text/css">
                                                .order-column {
                                                    padding-left: 20px;
                                                }
                                            </style>

                                            <div class="scrolableDataGrid billing-option" id="BulkBillingPricesGridViewDiv"
                                                style="min-height: 244px; max-height: 244px;">
                                                <asp:GridView runat="server"
                                                    ID="BulkBillingPricesGridView"
                                                    CssClass="grid"
                                                    AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    onclick="BulkBillingPricesGridViewClick(event)"
                                                    OnRowDataBound="BulkBillingPricesGridView_OnRowDataBound">
                                                    <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Billing Status">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="OrderStatusHeaderLabel" runat="server" Text="Availability"
                                                                    ToolTip="Ready for Bulk Billing or not"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Image ID="OrderStatusImage" runat="server"
                                                                    ImageUrl="~\Images\green_circle.jpg"
                                                                    ToolTip="Ready for BulkBilling" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="100px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="OrderCode" HeaderText="Order">
                                                            <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="70px" HorizontalAlign="Left" Wrap="False" CssClass="order-column" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CreateDate" HeaderText="Create Date">
                                                            <HeaderStyle Width="190px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="190px" HorizontalAlign="Left" Wrap="False" CssClass="order-column" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SynthServiceTypeName" HeaderText="Synth Service Type">
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ServiceTypeName" HeaderText="GD Service Type">
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="RetailerName" HeaderText="Retailer">
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="QbCustomerName" HeaderText="QB Customer Name">
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Question">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="QuestionTextHeaderLabel" runat="server" Text="Question"
                                                                    ToolTip="Additional question for price customizing"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="QuestionTextLabel" runat="server" Text='<%# Bind("QuestionText") %>'
                                                                    ToolTip="Additional question for price customizing"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <span style="display: none">AnswerImage</span>
                                                                <asp:Image runat="server" ID="AnswerImageID"
                                                                    ImageUrl="~/Images/ajaxImages/checkboxWhite.png"
                                                                    ToolTip="Answer" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox runat="server" ID="AnswerCheckBox"
                                                                    Text=""
                                                                    AutoPostBack="False"
                                                                    Checked='<%# DataBinder.Eval(Container.DataItem, "IsSatisfyACondition") %>'
                                                                    OnCheckedChanged="BulkBillingPricesAnswerCheckBox_OnCheckedChanged" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Prices">
                                                            <ItemTemplate>
                                                                <asp:Label ID="PricesLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="110px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="110px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="DefaultPrices">
                                                            <ItemTemplate>
                                                                <asp:Label ID="DefaultPricesLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="hidden-column" />
                                                            <ItemStyle CssClass="hidden-column" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SpecialPrices">
                                                            <ItemTemplate>
                                                                <asp:Label ID="SpecialPricesLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="hidden-column" />
                                                            <ItemStyle CssClass="hidden-column" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Quantities">
                                                            <ItemTemplate>
                                                                <asp:Label ID="QuantitiesLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Costs">
                                                            <ItemTemplate>
                                                                <asp:Label ID="CostsLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Invoice Costs">
                                                            <ItemTemplate>
                                                                <asp:Label ID="CostsInvoiceLabel" runat="server" Text=" "></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                    </Columns>

                                                    <EditRowStyle CssClass="grid-edit-item" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />
                                                    <RowStyle CssClass="grid-item" Wrap="False" />
                                                    <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <div style="padding-top: 20px">
                                <div runat="server" id="BillingTypeDiv"
                                    style="text-align: center; padding: 10px 0 0 0; margin: 0; height: 25px;">
                                    <asp:CheckBox runat="server" ID="RegularCheckBox" Text="Regular" CssClass="radio-horizontal-style"
                                        onclick="OnClickRegularBilling(event)"
                                        Style="padding-left: 30px !important;" />
                                    <asp:CheckBox runat="server" ID="AddSKUCheckBox" Text="Add SKU" CssClass="radio-horizontal-style"
                                        onclick="OnClickAddSKUBilling(event)"
                                        Style="padding-left: 30px !important;" />
                                    <asp:CheckBox runat="server" ID="AddLotNoCheckBox" Text="Add Lot #" CssClass="radio-horizontal-style"
                                        onclick="OnClickAddLotNoBilling(event)"
                                        Style="padding-left: 30px !important;" />
                                </div>
                                <div style="text-align: center; padding: 10px 0 0 0; margin: 0; height: 25px;">
                                    <asp:RadioButton runat="server" ID="QBPrimaryRadioButton" Text="QB Primary" Checked="False"
                                        GroupName="BillToAccountRadioGroup" CssClass="radio-horizontal-style"
                                        Style="padding-left: 30px !important;" />
                                    <asp:RadioButton runat="server" ID="QBCorptRadioButton" Text="QB Corpt" Checked="False"
                                        GroupName="BillToAccountRadioGroup" CssClass="radio-horizontal-style"
                                        Style="padding-left: 30px !important;" />
                                    <asp:RadioButton runat="server" ID="TallyRadioButton" Text="Tally" Checked="False"
                                        GroupName="BillToAccountRadioGroup" CssClass="radio-horizontal-style"
                                        Style="padding-left: 30px !important;" />
                                </div>
                                <div style="text-align: center; padding: 10px 0 0 0; margin: 0; height: 25px;">
                                    <asp:RadioButton runat="server" ID="AllOrdersToOneInvoiceRadioButton"
                                        Text="All orders in one invoice" Checked="True" GroupName="HowToGroupOrdersToInvoicesRadioGroup"
                                        CssClass="radio-horizontal-style"
                                        Style="padding-left: 30px !important;" />
                                    <asp:RadioButton runat="server" ID="EachOrderToSeparateInvoiceRadioButton"
                                        Text="Each order in the separate invoice" Checked="False" GroupName="HowToGroupOrdersToInvoicesRadioGroup"
                                        CssClass="radio-horizontal-style"
                                        Style="padding-left: 30px !important;" />
                                </div>
                                <p style="text-align: center;">
                                    <asp:Button ID="BillingOptionsOkButton" runat="server" Text="OK" OnClick="BillingOptionsOkButton_OnClick" />
                                    <asp:Button ID="BillingOptionsCancelButton" runat="server" Text="Cancel" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="BillingOptionsTargetButton" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server"
                            ID="BillingOptionsModalPopupExtender"
                            TargetControlID="BillingOptionsTargetButton"
                            PopupControlID="BillingOptionsDialog"
                            PopupDragHandleControlID="BillingOptionsDragHandle">
                        </ajaxToolkit:ModalPopupExtender>

                        <%-- SearchByOrderList Dialog --%>
                        <asp:Panel runat="server" ID="SearchByOrderListDialogPanel" CssClass="modalPopup form-inline"
                            Style="width: 282px; display: none"
                            DefaultButton="OrderListDialogAddButton"
                            enctype="multipart/form-data" method="post">

                            <div style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                                <b>Search By Order List</b>
                            </div>
                            <table>
                                <tr>
                                    <td class="auto-style1" style="padding-left: 0px; padding-top: 15px; width: 80px;">
                                        <asp:Label runat="server" Text="Order Code:" />
                                    </td>
                                    <td class="auto-style1" style="padding-left: 5px; padding-top: 15px;">
                                        <asp:TextBox runat="server" ID="OrderListDialogOrderCodeTextBox" Width="80px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style1" style="padding-left: 0px; padding-top: 15px;">
                                        <asp:Button runat="server" ID="OrderListDialogAddButton"
                                            Text="Add" OnClientClick="orderListDialogAddOrder(event); return false;" />
                                    </td>
                                    <script type="text/javascript">
                                        function orderListDialogAddOrder(evt) {
                                            var orderList = document.getElementById('<%=OrderListDialogListTextBox.ClientID %>');
                                            var orderCode = document.getElementById('<%=OrderListDialogOrderCodeTextBox.ClientID %>');
                                            var isListEmpty = (orderList.value.length === 0);
                                            var endLine = isListEmpty ? "" : "\n";
                                            orderList.value = orderList.value + endLine + orderCode.value;
                                            orderCode.value = "";
                                            orderCode.focus();
                                        }
                                    </script>

                                    <td style="text-align: end; padding-top: 15px;">
                                        <asp:Button runat="server" ID="SearchByOrderListDialogVerifyButton"
                                            Text="Verify" OnClick="SearchByOrderListDialogVerifyButton_OnClick" />
                                    </td>

                                    <td style="text-align: end; padding-top: 15px;">
                                        <asp:Button runat="server" ID="SearchByOrderListDialogClearButton"
                                            Text="Clear" OnClientClick="orderListDialogClearList(event); return false;" />
                                    </td>
                                    <script type="text/javascript">
                                        function orderListDialogClearList(evt) {
                                            var orderList = document.getElementById('<%=OrderListDialogListTextBox.ClientID %>');
                                            var orderCode = document.getElementById('<%=OrderListDialogOrderCodeTextBox.ClientID %>');
                                            orderList.value = "";
                                            orderCode.focus();
                                        }
                                    </script>

                                </tr>
                                <tr>
                                    <td colspan="3" class="auto-style1" style="padding-left: 0px; padding-top: 15px;">
                                        <div style="overflow-y: scroll; overflow-x: auto;">
                                            <asp:TextBox runat="server" ID="OrderListDialogListTextBox"
                                                Width="248px" Height="200px"
                                                TextMode="MultiLine" Columns="10" Rows="10"
                                                Style="resize: none" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-top: 10px">
                                <p style="text-align: center;">
                                    <asp:LinkButton ID="SearchByOrderListDialogOKLinkButton" runat="server"
                                        Text="Load" OnClick="SearchByOrderListDialogOKButton_OnClick"
                                        Style="text-decoration: none; color: #505050; border: 1px;">
                                                    <asp:Image runat="server" ImageUrl="~/Images/ajaxImages/search.png" style="margin-left:5px;" />
                                    </asp:LinkButton>
                                    <asp:Button ID="SearchByOrderListDialogCancelButton" runat="server"
                                        Text="Cancel" OnClick="SearchByOrderListDialogCancelButton_OnClick"
                                        Style="margin-left: 20px" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="SearchByOrderListDialogHiddenButton" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server"
                            ID="SearchByOrderListDialogModalPopupExtender"
                            TargetControlID="SearchByOrderListDialogHiddenButton"
                            PopupControlID="SearchByOrderListDialogPanel">
                        </ajaxToolkit:ModalPopupExtender>

                        <%-- Batch Label Edit Dialog --%>
                        <script type="text/javascript">
                            function noEnter(e) {
                                if (e.keyCode == 13) {
                                    e.preventDefault();
                                } 
                                return false;
                            }
                        </script>
                        <style type="text/css">

                            .batch-label-caption {
                                vertical-align: middle;
                                font-size: 1.2em;
                            }
                            .checkbox-label {
                                padding-left: 5px;
                            }
                            .textbox-in-grid-row
                            {
                                padding-top: 10px;
                                padding-right: 5px;
                            }
                            .grid-row-item{
                                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
                                font-size: 14px !important;
                            }
                        
                        </style>

                        <asp:Panel runat="server" ID="BatchLabelEditDialog" CssClass="modalPopup" Style="display: none; width: 555px;">
                            <asp:Panel runat="server" ID="BatchLabelEditDragHandle"
                                Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
                                <div>
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/ajaxImages/question24.png" />
                                    <asp:Label runat="server" ID="BatchLabelEditLabel"
                                        CssClass="panel-caption" />
                                </div>
                            </asp:Panel>
                            
                            <div style="padding-top: 20px">
                                <p style="text-align: center;">
                                    <asp:Label runat="server" ID="BatchLabelEditBatchNumberLabel"
                                               Text="7106544.01"
                                               CssClass="panel-caption batch-label-caption" />
                                </p>
                            </div>

                            <asp:Panel runat="server" ID="BatchLabelEditPanel"
                                Style="text-align: left; margin-top: 20px; width: 555px;">
                                <table>
                                    <tr>
                                        <!-- Batch Label Edit GridView -->
                                        <td>
                                            <div class="scrolableDataGrid" id="BatchLabelEditGridViewDiv"
                                                style="min-height: 275px; max-height: 455; width: 555px;">
                                                <asp:GridView runat="server"
                                                    ID="BatchLabelEditGridView"
                                                    CssClass="grid"
                                                    AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    Width="100%">
                                                    <AlternatingRowStyle CssClass="grid-item grid-alternating-item" />
                                                    <Columns>

                                                        <asp:BoundField DataField="PartName" HeaderText="Part">
                                                            <HeaderStyle Width="40%" HorizontalAlign="Center" />
                                                            <ItemStyle Width="40%" HorizontalAlign="Left" Wrap="False" CssClass="checkbox-label grid-row-item"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="MeasureName" HeaderText="Measure">
                                                            <HeaderStyle Width="40%" HorizontalAlign="Center" />
                                                            <ItemStyle Width="40%" HorizontalAlign="Left" Wrap="False" CssClass="checkbox-label grid-row-item"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ResultValue" HeaderText="Value">
                                                            <HeaderStyle Width="20%" HorizontalAlign="Center" />
                                                            <ItemStyle Width="20%" HorizontalAlign="Left" Wrap="False" CssClass="checkbox-label grid-row-item"/>
                                                        </asp:BoundField>

<%--                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <span style="display: none">Checked</span>
                                                                <asp:Image runat="server" ID="CheckBoxImageID"
                                                                    ImageUrl="~/Images/ajaxImages/checkboxWhite.png"
                                                                    ToolTip="Batch Label Property Value" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox runat="server" ID="BatchLabelCheckedCheckBox"
                                                                    Text=''
                                                                    AutoPostBack="False"
                                                                    Checked='<%# DataBinder.Eval(Container.DataItem, "PropertyChecked") %>'
                                                                    OnCheckedChanged="BatchLabelValueCheckBox_OnCheckedChanged" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Comment">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="BatchLabelCommentTextBox" runat="server" 
                                                                             Text='<%# DataBinder.Eval(Container.DataItem, "PropertyComment") %>'
                                                                             OnTextChanged="BatchLabelCommentTextBox_OnTextChanged"
                                                                             Width="94%"
                                                                             onkeypress="noEnter(event);"
                                                                             AutoPostBack="False">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="55" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemStyle Width="55%" HorizontalAlign="Left" Wrap="False" CssClass="textbox-in-grid-row"/>
                                                        </asp:TemplateField>--%>
                                                    </Columns>

                                                    <EditRowStyle CssClass="grid-edit-item" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="grid-item grid-header grid-freezed-header" />
                                                    <RowStyle CssClass="grid-item" Wrap="False" />
                                                    <SelectedRowStyle CssClass="grid-item grid-selected-item" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                            <div style="padding-top: 20px">
                                <p style="text-align: center;">
                                    <asp:Button ID="BatchLabelEditCloseButton" runat="server" Text="Close" OnClick="BatchLabelEditCloseButton_OnClick" />
                                </p>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="BatchLabelEditTargetButton" Style="display: none"/>
                        <ajaxToolkit:ModalPopupExtender runat="server"
                            ID="BatchLabelEditModalPopupExtender"
                            TargetControlID="BatchLabelEditTargetButton"
                            PopupControlID="BatchLabelEditDialog"
                            PopupDragHandleControlID="BatchLabelEditDragHandle">
                        </ajaxToolkit:ModalPopupExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
