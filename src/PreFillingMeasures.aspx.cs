﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.TreeModel;
using Corpt.Utilities;

namespace Corpt
{
    public partial class PreFillingMeasures : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
			Page.Title = "GSI: Pre-Filling Measures";
            if (IsPostBack) return;
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
        }
        #region Load Button
        protected void OnLoadClick(object sender, ImageClickEventArgs e)
        {
            //-- Clear
            ClearAll();
            if (BatchNumberFld.Text.Trim().Length == 0) return;

            //-- Load Similar Batches
            var FullBatchNumber = BatchNumberFld.Text.Trim();
            var order = "" + Utils.ParseOrderCode(FullBatchNumber).ToString();
            var batchNumber = Utils.ParseBatchCode(FullBatchNumber).ToString();
            order = Utils.FillToFiveChars(order);
            OrderField.Value = order;
            BatchField.Value = batchNumber;
            var batchList = QueryUtils.GetSimilarBatches(order, batchNumber, this);
            if (batchList.Count == 0)
            {
                LoadInfoLabel.Text = "Batch number " + Utils.FillToFiveChars(order) + Utils.FillToThreeChars(batchNumber, order) + " not found";
                return;
            }
            SetViewState(batchList, SessionConstants.BulkBatches);
            BuildTreeBatches(batchList);

            //-- Load Pre-Filling values
            var preFillings = QueryUtils.GetPreFillingData(Convert.ToInt32(order), this);
            foreach (var valueModel in preFillings)
            {
                var batch = batchList.Find(m => m.BatchId == "" + valueModel.BatchId);
                if (batch == null) continue;
                valueModel.BatchModel = batch;
            }
            SetViewState(preFillings, SessionConstants.PreFillingValues);
            BuildTreeMeasures();
        }
        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }

        #endregion

        private List<PreFillingValueModel> GetPreValues(string batchId)
        {
            var values = GetViewState(SessionConstants.PreFillingValues) as List<PreFillingValueModel> ??
                         new List<PreFillingValueModel>();
            var selValues = string.IsNullOrEmpty(batchId) ?
                values.FindAll(m => m.BatchModel != null) : 
                values.FindAll(m => m.BatchId == Convert.ToInt32(batchId) && m.BatchModel != null);

            selValues.Sort(new PreFillingComparer());
            return selValues;
        }

        #region Batches Tree
        private List<BatchModel> GetCheckedBatches()
        {
            var checkedBatches = new List<BatchModel>();
            foreach (TreeNode node in treeBatches.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0 || node.Parent == null) continue;
                var chkBatch = new BatchModel
                {
                    BatchId = node.Value,
                };
                checkedBatches.Add(chkBatch);
            }
            return checkedBatches;
        }

        protected void TreeBatchCheckChanged(object sender, TreeNodeEventArgs e)
        {
            if (treeBatches.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in treeBatches.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        private void BuildTreeBatches(List<BatchModel> batchList)
        {
            treeBatches.Nodes.Clear();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Batches" } };
            foreach (var batch in batchList)
            {
                var batchTvModel = new TreeViewModel
                {
                    ParentId = "0",
                    Id = batch.BatchId,
                    DisplayName = batch.FullBatchNumber
                };
                data.Add(batchTvModel);
            }

            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            treeBatches.Nodes.Add(rootNode);
        }
        protected void OnTreeBatchSelectedChanged(object sender, EventArgs e)
        {
            if (treeBatches.SelectedNode == null || treeBatches.SelectedNode.ChildNodes.Count > 0 ||
                treeBatches.SelectedNode.Parent == null)
            {
                dgMeasures.DataSource = null;
                dgMeasures.DataBind();
                return;
            }
            //-- Get Measure Values
            var batchId = treeBatches.SelectedNode.Value;
            dgMeasures.DataSource = GetPreValues(batchId);
            dgMeasures.DataBind();
        }
        #endregion
        
        #region Measures Tree
        private List<MeasureModel> GetCheckedMeasures()
        {
            var chkMeasures = new List<MeasureModel>();
            foreach (TreeNode node in treeMeasures.CheckedNodes)
            {
                if (node.ChildNodes.Count > 0 || node.Parent == null) continue;
                var chkMeasure = new MeasureModel
                {
                    PartId = Convert.ToInt32(node.Parent.Value),
                    MeasureId = Convert.ToInt32(node.Value.Split(';')[1])
                };
                chkMeasures.Add(chkMeasure);
            }
            return chkMeasures;
        }
        protected void TreeMeasureCheckChanged(object sender, TreeNodeEventArgs e)
        {
            if (treeMeasures.CheckedNodes.Count > 0)
            {
                // the selected nodes.
                foreach (TreeNode node in treeMeasures.CheckedNodes)
                {
                    if (node.ChildNodes.Count <= 0) continue;
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = true;
                    }
                }
            }
        }
        private void BuildTreeMeasures()
        {
            treeMeasures.Nodes.Clear();
            var values = GetPreValues("");
            var partGroups = values.GroupBy(m => m.PartId + ";" + m.PartName).ToList();
            var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "Measures" } };// 
            foreach (var part in partGroups)
            {
                var partId = Convert.ToInt32(part.Key.Split(';')[0]);
                var partName = part.Key.Split(';')[1];
                data.Add(new TreeViewModel{ParentId = "0", Id = "" + partId, DisplayName = partName});
                var measures = values.FindAll(m => m.PartId == partId).GroupBy(m => m.MeasureId + ";" + m.MeasureTitle);
                foreach (var measure in measures)
                {
                    var measureId = measure.Key.Split(';')[0];
                    var measureTitle = measure.Key.Split(';')[1];
                    data.Add(new TreeViewModel{ParentId = "" + partId, Id=partId + ";" + measureId, DisplayName = measureTitle});
                }
            }
            var root = TreeUtils.GetRootTreeModel(data);
            var rootNode = new TreeNode(root.DisplayName, root.Id);
            TreeUtils.FillNode(rootNode, root);
            treeMeasures.Nodes.Add(rootNode);
        }
        #endregion

        protected void OnSaveClick(object sender, EventArgs e)
        {
            
            var batches = GetCheckedBatches();
            if (batches.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Batches not found!" + "\")</SCRIPT>");
                return;
            }
            var measures = GetCheckedMeasures();
            if (measures.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Checked Measures not found!" + "\")</SCRIPT>");
                return;
            }
            var itemList = QueryUtils.GetItemsByOrderCode(OrderField.Value.Trim(), this);
            var result = new List<MeasureValueModel>();
            foreach(var batch in batches)
            {
                var values = GetPreValues(batch.BatchId);
                var items = itemList.FindAll(m => m.BatchId == batch.BatchId);
                foreach(var measure in measures)
                {
                    var preValue = values.Find(m => m.PartId == measure.PartId && m.MeasureId == measure.MeasureId);
                    if (preValue == null) continue;
                    result.AddRange(items.Select(item => MeasureValueModel.Create(preValue, batch.BatchId, item.ItemCode)));
                }
            }
            var msg = QueryUtils.SavePreFilling(result, this);
            if (string.IsNullOrEmpty(msg))
            {
                msg = "Done (" + result.Count + " rows)";
            }
            System.Web.HttpContext.Current.Response.Write(
                    "<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");

        }
        private void ClearAll()
        {
            treeBatches.Nodes.Clear();
            treeMeasures.Nodes.Clear();
            dgMeasures.DataSource = null;
            dgMeasures.DataBind();
            LoadInfoLabel.Text = "";
        }

        protected void OnTreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            ;
        }

        protected void treeBatches_DataBinding(object sender, EventArgs e)
        {

        }
    }
}