﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/DefaultMaster.Master" Title="Screening User View" CodeBehind="ScreeningUserView.aspx.cs" Inherits="Corpt.ScreeningUserView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SampleContent" runat="server">

	<script src="Style/jquery-1.12.4.js" type="text/javascript"></script>
	<script src="Style/jquery-ui.js" type="text/javascript"></script>
	<script src="Style/bootstrap2.0/js/bootstrap.js" type="text/javascript"></script>

	<link rel="icon" type="image/png" href="logo@2x.png" />
	<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
	<link href="Style/bootstrap2.0/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="Style/jquery-ui.css" rel="stylesheet" type="text/css" />
	<style>
		.hdnColumn {
			display: none;
		}

		.cssTextbox {
			width: 150px;
		}
		.cssTextboxint {
			width: 50px;
			text-align:right;
		}
		.cssLabel {
			padding: 0px 5px 5px 10px;
		}
	</style>
	<%--<script type="text/javascript">

        $(document).ready(function () {

            $('#<%=txtScreenerFailItems.ClientID%>, #<%=txtScreenerPassItems.ClientID%>, #<%=txtTesterPassItems.ClientID%>, #<%=txtTesterSuspectItems.ClientID%>, #<%=txtTesterSyntheticItems.ClientID%>').keypress(function (event) {
                return isOnlyNumber(event, this);
            });

        });

		function isOnlyNumber(evt, element) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode < 48 || charCode > 57)
				return false;
			return true;
		}
		</script>--%>
	<div class="demoarea" style="text-align: left;">
		<ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" EnablePartialRendering="True"></ajaxToolkit:ToolkitScriptManager>
		<asp:UpdatePanel ID="UpdatePanel1" runat="server">
			<ContentTemplate>
		<table>
					<tr>
						<td>
		<div class="demoheading">User View </div>
							</td><td>
				 <div style="font-size:smaller;">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <img alt="" src="Images/ajaxImages/loader.gif" width="25px" />
                    <b>Please, wait....</b>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
</td>
					</tr>
				</table>
		<table>
			<tr>
				<td>
					<br />
					<table style="width: 500px;" id="tblAssignmentDetail" runat="server">
						<tr>
							<td>
								<fieldset style="border: 1px groove #ddd !important; padding: 10px 10px 10px 10px; border-radius: 8px; width: max-content;">
									<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
										class="label">User Detail</legend>
									<table>
										<tr>

											<td>
												<asp:Label ID="Label1" runat="server" Text="Order Code" Style="float: left; width: 80px;" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtOrderCode" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>
											<td>
												<asp:Label ID="Label7" runat="server" Text="Assignment Id" Style="float: left; width: 100px;" CssClass="cssLabel" Visible="false"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtAssignmentId" runat="server" Enabled="false" CssClass="cssTextbox" Visible="false"></asp:TextBox>
											</td>
											
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label2" runat="server" Text="Style Name" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtStyleName" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>
											<td>
												<asp:Label ID="Label3" runat="server" Text="Style Qty" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtStyleQty" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>

										</tr>
										<tr>
											<td>
												<asp:Label ID="Label10" runat="server" Text="BatchCode" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtBatchCode" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>
											<td>
												<asp:Label ID="Label11" runat="server" Text="Assigned Qty" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtAssignedQty" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label5" runat="server" Text="Assign Date" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtAssignDate" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>
											<td>
												<asp:Label ID="Label4" runat="server" Text="Assigned By" Style="float: left; width: 85px;" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtAssignedBy" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>

										</tr>
										<tr>

											<td style="vertical-align: top; padding-top: 5px;">
												<asp:Label ID="Label8" runat="server" Text="Current Status" Style="float: left;" CssClass="cssLabel"></asp:Label>
											</td>
											<td style="vertical-align: top;">
												<asp:TextBox ID="txtStatus" runat="server" Enabled="false" CssClass="cssTextbox"></asp:TextBox>
											</td>

											<td style="vertical-align: top; padding-top: 5px;">
												<asp:Label ID="Label6" runat="server" Text="Comment" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td rowspan="0">
												<asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Height="50px" CssClass="cssTextbox" Enabled="false"></asp:TextBox>
											</td>

										</tr>
										<tr>
											<td style="vertical-align: top; padding-top: 5px;">
												<asp:Label ID="Label9" runat="server" Text="Next Status" Style="float: left;" CssClass="cssLabel"></asp:Label>
											</td>
											<td style="vertical-align: top;">
												<asp:DropDownList ID="ddlStatus" runat="server" Enabled="false" Width="160px"></asp:DropDownList>
												<asp:HiddenField ID="hdnStatusID" runat="server" />
											</td>
										</tr>

									</table>
								</fieldset>
							</td>
							<td style="vertical-align: top;padding-left:10px;" >
								<fieldset id="fsScreener" runat="server" visible="false" style="border: 1px groove #ddd !important; padding: 10px 10px 10px 10px; border-radius: 8px; width: max-content;">
									<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
										class="label">Screener Detail</legend>
									<table>
										<tr>
											<td>
												<asp:Label ID="Label12" runat="server" Text="Pass Items" Style="float: left; width: 80px;" CssClass="cssLabel"></asp:Label>

											</td>
											<td>
												<asp:TextBox ID="txtScreenerPassItems" runat="server" Enabled="true" CssClass="cssTextboxint" Text="0" MaxLength="4" onkeypress='return (event.charCode >= 48 && event.charCode <= 57);'
 ></asp:TextBox>

												
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label13" runat="server" Text="Fail Items" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtScreenerFailItems" runat="server" Enabled="true" CssClass="cssTextboxint" Text="0" MaxLength="4" onkeypress='return (event.charCode >= 48 && event.charCode <= 57);' ></asp:TextBox>
											</td>

										</tr>
									</table>
								</fieldset>
								<fieldset id="fsTester" runat="server" visible="false" style="border: 1px groove #ddd !important; padding: 10px 10px 10px 10px; border-radius: 8px; width: max-content;">
									<legend style="width: max-content; padding-left: 7px; padding-right: 5px; margin-bottom: 0px; border-bottom: 0px !important; font-family: verdana,tahoma,helvetica;"
										class="label">Tester Detail</legend>
									<table>
										<tr>
											<td>
												<asp:Label ID="Label14" runat="server" Text=" Synthetic Items" Style="float: left" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtTesterSyntheticItems" runat="server" Enabled="true" CssClass="cssTextboxint" Text="0" MaxLength="4" onkeypress='return (event.charCode >= 48 && event.charCode <= 57);'></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label15" runat="server" Text="Suspect Items" Style="float: left; width: 85px;" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtTesterSuspectItems" runat="server" Enabled="true" CssClass="cssTextboxint" Text="0" MaxLength="4" onkeypress='return (event.charCode >= 48 && event.charCode <= 57);'></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label16" runat="server" Text="Pass Items" Style="float: left; width: 85px;" CssClass="cssLabel"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="txtTesterPassItems" runat="server" Enabled="true" CssClass="cssTextboxint" Text="0" MaxLength="4" onkeypress='return (event.charCode >= 48 && event.charCode <= 57);'></asp:TextBox>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>

						</tr>
						<tr>
							<td>
								<br />
								<asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Blue"></asp:Label>
								<asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-info" Style="margin-right: 5px;float:right;" OnClick="btnUpdate_Click" Enabled="false" />
								<asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-info" Style="margin-right: 5px;float:right;" OnClick="btnClear_Click" Enabled="false" />
							</td>
							<td colspan="0" style="text-align: right;padding-top:10px;"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					<div class="demoheading" id="dvAllocationHeader" runat="server">User Task</div>
					<asp:GridView ID="dtgListAcceptQty" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnSelectedIndexChanged="dtgListSelectQty_SelectedIndexChanged">
						<Columns>
							<asp:CommandField ShowSelectButton="True" ButtonType="Button" HeaderText="Select" ControlStyle-CssClass="btn btn-info" />
							<asp:BoundField DataField="AssingmentId" HeaderText="Assignment Id" Visible="false"></asp:BoundField>
							<asp:BoundField DataField="OrderCode" HeaderText="Order Code"></asp:BoundField>
							<asp:BoundField DataField="BatchCode" HeaderText="Batch Code"></asp:BoundField>

							<asp:BoundField DataField="ItemQty" HeaderText="Assigned Qty"></asp:BoundField>
							<asp:BoundField DataField="AssignedBy" HeaderText="Assigned By"></asp:BoundField>
							<asp:BoundField DataField="StartDate" HeaderText="Assigned Date" DataFormatString="{0:dd-MMMM-yyyy HH:mm}"></asp:BoundField>
							<asp:BoundField DataField="StatusName" HeaderText="Status"></asp:BoundField>

							<asp:BoundField DataField="StatusID" HeaderText="StatusID" ItemStyle-CssClass="hdnColumn" HeaderStyle-CssClass="hdnColumn"></asp:BoundField>
							<asp:BoundField DataField="StyleName" HeaderText="StyleName" ItemStyle-CssClass="hdnColumn" HeaderStyle-CssClass="hdnColumn"></asp:BoundField>
							<asp:BoundField DataField="TotalQty" HeaderText="TotalQty" ItemStyle-CssClass="hdnColumn" HeaderStyle-CssClass="hdnColumn"></asp:BoundField>
							<asp:BoundField DataField="UserName" HeaderText="UserName" ItemStyle-CssClass="hdnColumn" HeaderStyle-CssClass="hdnColumn"></asp:BoundField>
						</Columns>
						<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
						<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Names="Cambria" ForeColor="White" />
						<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
					</asp:GridView>
					<div id="dvEmptyRecord" runat="server" style="border: 1px solid black; padding-left: 10%; width: 700px; padding: 10px; text-align: center;">
						No Task Assigned
					</div>

				</td>
			</tr>

		</table>
		<%-- Information Dialog --%>
				<asp:Panel runat="server" ID="InfoPanel" CssClass="modalPopup" Style="width: 210px; display: none; border: solid 2px Gray;margin-top:25%;">
					<asp:Panel runat="server" ID="InfoPanelDragHandle" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Silver; color: black; text-align: left">
						<div>
							<asp:Image ID="InfoImage" runat="server" ImageUrl="~/Images/ajaxImages/information24.png" />
							<b>Information</b>
						</div>
					</asp:Panel>
					<div style="overflow: auto; max-width: 200px; max-height: 200px; margin-top: 10px; color: black; text-align: center"
						id="MessageDiv" runat="server">
					</div>
					<div style="padding-top: 10px">
						<p style="text-align: center; font-family: sans-serif;">
							<asp:Button ID="InfoCloseButton" runat="server" Text="Ok" class="btn btn-info btn-small" OnClick="OnInfoCloseButtonClick" />
						</p>
					</div>
				</asp:Panel>
				<asp:Button runat="server" ID="PopupInfoButton" Style="display: none" />
				<ajaxToolkit:ModalPopupExtender runat="server" TargetControlID="PopupInfoButton" Y="0"
					PopupControlID="InfoPanel" ID="InfoPopupExtender" PopupDragHandleControlID="InfoPanelDragHandle">
				</ajaxToolkit:ModalPopupExtender>
				</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
