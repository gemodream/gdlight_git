﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;

namespace Corpt
{
    public partial class StNumberReport : CommonPage
    {
        const string DisplayField = "BatchNumbers";
        const string ValueField = "StructGrp";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            var id = int.Parse("" + Session["ID"]);
            if (id != 10 && id != 12 && id != 17 && id != 19 && id != 24)
            {
                Response.Redirect("Middle.aspx");
            }

            if (!IsPostBack)
            {
                LoadCustomers();
                CalendarExtenderTo.SelectedDate = DateTime.Now.Date;
            }

        }

        #region Events
        protected void GrdShortReportSortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var strSortBy = "" + GetViewState(SessionConstants.ShortReportSortExpression);
            var strSortAscending = "" + GetViewState(SessionConstants.ShortReportSortExpressionDir);

            SetViewState(e.SortExpression, SessionConstants.ShortReportSortExpression);

            // If you click on the sorted column, the order reverses
            if (e.SortExpression == strSortBy)
            {
                SetViewState((strSortAscending == "yes" ? "no" : "yes") , SessionConstants.ShortReportSortExpressionDir);
            }
            else
            {
                // Defaults to ascending order
                SetViewState("yes", SessionConstants.ShortReportSortExpressionDir);
            }

            UpdateDataView();
        }

        protected void OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    if (cell.Text.IndexOf("text_highlitedyellow", StringComparison.Ordinal) != -1)
                    {
                        cell.BackColor = Color.FromArgb(252, 248, 227);
                    }
                }
            }
        }

        protected void OnCustomerSelectedChanged(object sender, EventArgs e)
        {
            LoadMemoNumbers();
        }
        protected void OnMemoSelectedChanged(object sender, EventArgs e)
        {
            LoadBatchNumbers();
        }

        protected void OnChangedDateTo(object sender, EventArgs e)
        {
            
            DateTime? date = null;
            if (calTo.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calTo.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderTo.SelectedDate = date;
            LoadMemoNumbers();
        }
        protected void OnChangedDateFrom(object sender, EventArgs e)
        {
            DateTime? date = null;
            if (calFrom.Text != "")
            {
                try
                {
                    date = DateTime.Parse(calFrom.Text, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception x)
                {
                    Console.Out.WriteLine(x.Message);
                }
            }
            CalendarExtenderFrom.SelectedDate = date;
            LoadMemoNumbers();
        }
        protected void OnChangedBatchGroup(object sender, EventArgs e)
        {
            var grp = Int32.Parse(cblBatches.SelectedValue);
            var table = GetReportViewByGroup(grp);
            SetViewState(table, SessionConstants.ShortReportActive);
            
            if (table != null)
            {
                UpdateDataView();
                cmdSaveExcel.Enabled = true;
                //                cmdAnimateCutgrade.Enabled = true;
                LblRowCounts.Text = string.Format("{0} rows", table.Rows.Count);
                AccordionPane.Visible = true;
            }
            //            lblMovedItems.Visible = SaveMovedItemsList(grp);

            //-- TODO the adapter to the old version ShortReportIICutgradeizer.aspx
            //            SaveActiveBatchList(grp);

        }
        protected void OnExcelClick(object sender, ImageClickEventArgs e)
        {
            ExportToExcel();
        }

        #endregion

        #region Utils
        private void ExportToExcel()
        {
            //-- 'Get Excel File' button
            var dataTable = GetViewState(SessionConstants.ShortReportActive) as DataTable;
            if (dataTable == null) return;
            var table = dataTable.Copy();
            var dv = new DataView { Table = table };

            // Apply sort information to the view
            var sortCol = "" + GetViewState(SessionConstants.ShortReportSortExpression);
            if (dv.Table.Columns.Contains(sortCol))
            {
                dv.Sort = sortCol;
                if (GetViewState(SessionConstants.ShortReportSortExpressionDir) != null &&
                    GetViewState(SessionConstants.ShortReportSortExpressionDir).ToString() == "no")
                {
                   dv.Sort += " DESC";
                }
            }

            var result = table.Copy();
            result.Rows.Clear();
            result.AcceptChanges();
            for (var i = 0; i < dv.Count; i++)
            {
                result.Rows.Add(dv[i].Row.ItemArray);
                result.AcceptChanges();
            }

            var fname = lstMemos.SelectedItem.Text.Replace("/", "");
            ExcelUtils.ExportThrouPoi(result, fname, this);
        }


        private void LoadCustomers()
        {
            lstCustomerList.Items.Clear();
            lstCustomerList.DataSource = QueryUtils.GetCustomers(this);
            lstCustomerList.DataBind();

        }
        private void ClearMemos()
        {
            lstMemos.SelectedValue = null;
            cblBatches.Items.Clear();
            cblBatches.ToolTip = "Batch numbers";

        }
        private void ClearBatches()
        {
            cblBatches.Items.Clear();
            cblBatches.ToolTip = "Batch numbers";
            grdShortReport.DataSource = null;
            grdShortReport.DataBind();
            AccordionPane.Visible = false;
            cmdSaveExcel.Enabled = false;
        }

        private void LoadBatchNumbers()
        {
            ClearBatches();
            
            var batchModelList = QueryUtils.GetBatchesByCustomerByDateRangeByMemoNumber(
                lstCustomerList.SelectedValue,
                CalendarExtenderFrom.SelectedDate,
                CalendarExtenderTo.SelectedDate,
                lstMemos.SelectedValue, this);

            //-- Get documentId and DocStructure
            var shortReportList = new List<ShortReportModel>();
            foreach (var batchModel in batchModelList)
            {
                var shortReportModel = Utlities.CreateShortReportModelWithDocStruct(batchModel, this);
                if (shortReportModel != null) shortReportList.Add(shortReportModel);
            }
            if (shortReportList.Count == 0) return;

            //-- Structure Group
            var structGrp = 0;
            foreach (var shortReportModel in shortReportList)
            {
                if (shortReportModel.DocStructGrp == 0)
                {
                    structGrp++;
                    shortReportModel.DocStructGrp = structGrp;
                }
                for (var j = 1; j < shortReportList.Count; j++)
                {
                    var otherModel = shortReportList[j];
                    if (otherModel.DocStructGrp != 0) continue;
                    if (DocStructModel.CompareStruct(shortReportModel.DocStructure, otherModel.DocStructure))
                    {
                        otherModel.DocStructGrp = shortReportModel.DocStructGrp;
                    }
                }
            }
            SetViewState(shortReportList, SessionConstants.ShortReportModelList);

            //-- Load table for Radio List
            var table = new DataTable();
            table.Columns.Add(new DataColumn(DisplayField));
            table.Columns.Add(new DataColumn(ValueField));
            for (var i = 0; i < structGrp; i++)
            {
                var sameGrp = shortReportList.FindAll(model => model.DocStructGrp == i + 1);
                var batchNumbers = sameGrp.Aggregate("", (current, reportModel) => current + ((current == "" ? "" : " ") + reportModel.BatchModel.FullBatchNumber));
                var dataRow = table.NewRow();
                dataRow[ValueField] = i + 1;
                dataRow[DisplayField] = batchNumbers + " " + DocStructModel.GetAbbr(sameGrp[0].DocStructure, (i + 1).ToString(CultureInfo.InvariantCulture));
                table.Rows.Add(dataRow);
            }

            cblBatches.DataTextField = DisplayField;
            cblBatches.DataValueField = ValueField;
            cblBatches.DataSource = table;
            cblBatches.DataBind();

        }
        private void LoadMemoNumbers()
        {
            ClearMemos();
            //-- Get Memos
            var memoList = QueryUtils.GetMemoNumbersByCustomerByDateRange(
                lstCustomerList.SelectedValue,
                CalendarExtenderFrom.SelectedDate,
                CalendarExtenderTo.SelectedDate, this);


            lstMemos.DataSource = memoList;
            lstMemos.DataBind();
            if (lstMemos.SelectedValue != null)
            {
                OnMemoSelectedChanged(null, null);
            }
        }

        private DataTable GetReportViewByGroup(int grp)
        {
            var shortReportList = GetViewState(SessionConstants.ShortReportModelList) as List<ShortReportModel>;
            if (shortReportList == null || shortReportList.Count == 0) return null;
            var grpShortReportList = shortReportList.FindAll(model => model.DocStructGrp == grp);

            //-- Data Collection
            foreach (var shortReportModel in grpShortReportList)
            {
                if (shortReportModel.ReportView != null) continue;
                //-- Failed Items
                shortReportModel.ItemsFailed = QueryUtils.GetItemsFailed(shortReportModel.BatchModel, this);

                //-- Items and MovedItems
                QueryUtils.GetItemsAndMovedItems(shortReportModel, this);

                //-- Values Items
                shortReportModel.ItemValues = QueryUtils.GetItemValues(shortReportModel.BatchModel, this);

                //--  Report View
                Utlities.CreateMergedItemStructValueTable(shortReportModel);
            }
            var table = grpShortReportList[0].ReportView.Copy();
            table.TableName = "ShortReport";
            for (var i = 1; i < grpShortReportList.Count; i++)
            {
                var addTbl = grpShortReportList[i].ReportView.Copy();
                foreach (DataRow row in addTbl.Rows)
                {
                    table.Rows.Add(row.ItemArray);

                }
            }
            table.AcceptChanges();
            return table;
        }
        void UpdateDataView()
        {
            var table = GetViewState(SessionConstants.ShortReportActive) as DataTable;
            if (table == null) return;
            var dv = new DataView { Table = table};

            // Apply sort information to the view
            var sortCol = "" + GetViewState(SessionConstants.ShortReportSortExpression);
            if (dv.Table.Columns.Contains(sortCol))
            {
                dv.Sort = sortCol;
                if (GetViewState(SessionConstants.ShortReportSortExpressionDir) != null &&
                    GetViewState(SessionConstants.ShortReportSortExpressionDir).ToString() == "no")
                {
                    dv.Sort += " DESC";
                }
            }

            // Rebind data 
            grdShortReport.DataSource = dv;
            grdShortReport.DataBind();

        }

        #endregion

    }
}