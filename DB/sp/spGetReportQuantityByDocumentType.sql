USE [GemoDream16]
GO
-- logic from spGetPrintingHistory.sql
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportQuantityByDocumentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportQuantityByDocumentType]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetReportQuantityByDocumentType]
(
	@DateFrom as datetime = null,
	@DateTo as datetime = null,
	@DocumentType int = null
)
AS
	declare @dateBeg date = ISNULL(@DateFrom, (select min(PrintDate) from tblPrintingQueue))
	declare @dateEnd date = ISNULL(@DateTo, GetDate())

SELECT /*COUNT(tblPrintingQueue.ItemOperationID) as count, i.GroupCode, i.BatchCode, i.ItemCode, i.BatchID,*/ i.NewBatchID, i.NewItemCode, doc.DocumentTypeCode  
INTO #v1
FROM 
	hstItemOperation h, 
	tblItemOperation tio, 
	tblDocument doc, 
	tblDocumentType dt, 
	tblPrintingQueue, 
	v0ItemNoExpDate i, 
	v0ItemNoExpDate i1

WHERE 
	tblPrintingQueue.PrintDate between @dateBeg and @dateEnd
	and tio.BatchID = i.BatchID
	AND tio.ItemCode = i.ItemCode
	and h.ItemOperationHistoryID = tio.ItemOperationHistoryID
	and doc.OperationTypeID = tio.OperationTypeID
	and doc.DocumentTypeCode not in (8,10,19,20)
	and dt.DocumentTypeCode = doc.DocumentTypeCode
	and tblPrintingQueue.ItemOperationID = h.ItemOperationID
	and i.BatchID = i1.BatchID
	and i.BatchID = i1.NewBatchID
	and i.ItemCode = i1.NewItemCode
	and tblPrintingQueue.State in ('Y', 'C')
GROUP BY doc.DocumentTypeCode, i.NewBatchID, i.NewItemCode

		
select count(v.NewItemCode) as quantity, dt.DocumentTypeName 
	from #v1 v, tblDocumentType dt
	where v.DocumentTypeCode = dt.DocumentTypeCode
	group by dt.DocumentTypeName

drop table #v1

GO

