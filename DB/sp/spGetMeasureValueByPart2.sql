--USE [GemoDream16]
--GO
/****** Object:  StoredProcedure [dbo].[spGetMeasureValueByPart2]    Script Date: 01/15/2015 21:32:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGetMeasureValueByPart2] (
      @BatchID int, 
      @ItemCode int, 
      @PartName varchar(50), 
      @MeasureTitle varchar(50), 
      @CutGrade int, 
      @AuthorID int, 
      @AuthorOfficeID int )

WITH EXEC AS CALLER
AS

Select @BatchID = NewBatchID, @ItemCode = NewItemCode
from v0Item
where BatchID = @BatchID and @ItemCode = ItemCode

Declare @MeasureID int
Declare @MeasureCode int
Declare @Value varchar(50)
Declare @cnt int
Declare @CPDocID int
Declare @PartID int
set @cnt = 0

declare @NewBatchID int, @NewItemCode int
Select @NewBatchID = NewBatchID, @NewItemCode = NewItemCode 
from v0Item where BatchID = @BatchID and ItemCode = @ItemCode
set @BatchID = @NewBatchID
set @ItemCode = @NewItemCode

SELECT    top 1
@Value = case MeasureClass
when  '1' then v0PartValue.ValueTitle
when '2' then v0PartValue.StringValue
when '3' then cast(cast(v0PartValue.MeasureValue as decimal (10, 2))as varchar (50))
when '4' then v0PartValue.StringValue
end , @PartID = v0PartValue.PartID, @MeasureID=v0PartValue.MeasureID, @MeasureCode = v0PartValue.MeasureCode
FROM         v0PartValue INNER JOIN
                      v0Part ON v0PartValue.PartID = v0Part.PartID
WHERE     (v0PartValue.BatchID = @BatchID) AND (v0PartValue.ItemCode = @ItemCode)and MeasureTitle =
@MeasureTitle
                    and PartName = @PartName
order by RecheckSessionID desc
print @MeasureCode
if @Value is null
begin
        Declare @MID int
        Declare @Rep int

        SELECT    @MeasureID= MeasureID
        FROM         tblMeasure
        WHERE     (MeasureTitle = @MeasureTitle)

        if @MeasureID is null
        begin
                select @Value = case MeasureID
                when '-1' then (select 'L' + Substring(cast((100000 + OrderCode) as Varchar(6)),2,5) +
			Substring(cast((100000 + OrderCode) as Varchar(6)),2,5)
			+ Substring(cast((1000 + BatchCode) as Varchar(4)),2,3) +
			Substring(cast((100 + @ItemCode) as Varchar(3)),2,2)
			from v0Batch where BatchID = @BatchID)
		
                when '-2' then (select top 1  LongReportName from GetPartValueByMeasureID(@BatchID, @ItemCode, null ,null, null, null, null, 8 /* MeasureID */ )
                                where MeasureTitle = 'Shape (cut)' and PartID = (select top 1 v0Part.PartID
				FROM  v0PartValue INNER JOIN
				v0Part ON v0PartValue.PartID = v0Part.PartID
				WHERE (v0PartValue.BatchID = @BatchID) AND (v0PartValue.ItemCode = @ItemCode)
				and PartName = @PartName
				order by RecheckSessionID desc)
                                order by RecheckSessionID desc)

                when '-3' then (select CustomerProgramName from v0CustomerProgram, v0Batch
                                where v0CustomerProgram.CPOfficeID = v0Batch.CPOfficeID and
				v0CustomerProgram.CPID = v0Batch.CPID and BatchID = @BatchID)

                when '-4' then (select cast(CPPropertyCustomerID as varchar(20))from v0CustomerProgram, v0Batch
                                where v0CustomerProgram.CPOfficeID = v0Batch.CPOfficeID and
				v0CustomerProgram.CPID = v0Batch.CPID and BatchID = @BatchID)
		when '-5' then  (select Substring(cast((100000 + OrderCode) as Varchar(6)),2,5) +
				 Substring(cast((1000 + BatchCode) as Varchar(4)),2,3)
		                                from v0Batch where BatchID = @BatchID)
		when '-6' then  (select  Substring(cast((100000 + OrderCode) as Varchar(6)),2,5) +
		                           Substring(cast((1000 + BatchCode) as Varchar(4)),2,3) +
				Substring(cast((100 + @ItemCode) as Varchar(3)),2,2)
				                                from v0Batch where BatchID = @BatchID)
		when '-7' then (select dbo.GetFullGirdle(0,@BatchID, @ItemCode, @PartName, @AuthorID, @AuthorOfficeID))
		when '-8' then (select dbo.GetFullGirdle(1,@BatchID, @ItemCode, @PartName, @AuthorID, @AuthorOfficeID))

		when '-9' then (select top 1  ShortReportName from GetPartValueByMeasureID(@BatchID, @ItemCode, null ,null, null, null, null, 8 /* MeasureID */ )
                                where MeasureTitle = 'Shape (cut)' and PartID = (select top 1 v0Part.PartID
				FROM  v0PartValue INNER JOIN
				v0Part ON v0PartValue.PartID = v0Part.PartID
				WHERE (v0PartValue.BatchID = @BatchID) AND (v0PartValue.ItemCode = @ItemCode)
				and PartName = @PartName
				order by RecheckSessionID desc)
                                order by RecheckSessionID desc)

	        end , @MeasureCode = MeasureID
                from tblMeasureAdditional where MeasureTitle = @MeasureTitle
        end
        else
        begin
                  select @MID = MeasureID, @Rep = ReplaceMeasureID from tblMeasureReplacements where
			MeasureID = @MeasureID

                  if @MID is null
                  begin                        set @Value = 'exception'
                  end

                  else if @Rep is null
                  begin
                        set @Value = ' '
                  end
                  else
                  begin
                      SELECT    top 1
                      @Value = case MeasureClass
                      when  '1' then v0PartValue.ValueTitle
                      when '2' then v0PartValue.StringValue
                      when '3' then cast(cast(v0PartValue.MeasureValue as decimal (10, 2))as varchar (50))
                      when '4' then v0PartValue.StringValue
                      end , @PartID = v0PartValue.PartID, @MeasureID=v0PartValue.MeasureID, @MeasureCode=v0PartValue.MeasureCode
                      FROM         v0PartValue INNER JOIN
                                            v0Part ON v0PartValue.PartID = v0Part.PartID
                      WHERE     (v0PartValue.BatchID = @BatchID) AND (v0PartValue.ItemCode = @ItemCode)and
				MeasureID = @Rep  and PartName = @PartName
                      order by RecheckSessionID desc
                      if @Value is null
                      begin
                                set @Value = 'exception'
                      end
                      else print @Value
                 end
        end
end
else print @Value

--by 3ter on 2006.04.25 - bug. during sp don't check whether the Measure exists
print 'PartID = ' + cast(@PartID as varchar(10))
	IF @MeasureCode IS NULL
	BEGIN
		SELECT @MeasureCode = m.MeasureCode
		FROM tblPartType_Measure ptm, tblMeasure m
		WHERE ptm.MeasureID = m.MeasureID
		  AND ptm.PartTypeID = (SELECT TOP 1 PartTypeID FROM v0Part WHERE PartName = @PartName)
		  AND m.MeasureTitle = @MeasureTitle
	END

declare @count int

-- for test only  --> SELECT  @BatchID BatchID, @ItemCode ItemCode, @PartID PartID, @MeasureID MeasureID

EXEC @count = [spGetCpRule] @BatchID, @ItemCode, @PartID, @MeasureID
if @CutGrade = 1 and @MeasureID <> 25
begin
	declare @count1 int
	exec @count1 =  [spGetCutGradeRule] @BatchID, @ItemCode, @PartID, @MeasureID
	if @count1 = 0
	begin
		set @count = @count1
	end
end

if @Value is null  set @Value = 'exception'
select @Value as Value, @count as CP, @MeasureCode as MeasureCode