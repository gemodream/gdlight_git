USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFailedParamsByBatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFailedParamsByBatch]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetFailedParamsByBatch]
(
	@BatchId int,
	@MeasureIds [dbo].[intTableType] READONLY
)
AS

	declare @CPID int = 
	(
		SELECT TOP 1 CPID FROM v0Batch WHERE BatchID = @BatchID
	)

	declare @ItemTypeID int = 
	(
		select ItemTypeID from v0CustomerProgram where CPID = @CPID
	)

	declare @CPDocID int  = 
	(
		SELECT MAX(hdtCPDoc.CPDocID) 
			FROM 	v0CustomerProgram, hdtCPDoc, hdtCPDoc_Operation 
			WHERE 	v0CustomerProgram.CPID = @CPID
				and v0CustomerProgram.CustomerProgramHistoryID = hdtCPDoc.CustomerProgramHistoryID
				AND  hdtCPDoc.CPDocID = hdtCPDoc_Operation.CPDocID 
	)

	create table #res
	(
		PartID int,
		MeasureID int,
		MeasureName varchar(20),
		BatchID int,
		ItemCode int,
		StateMsg varchar(100)
	)
	declare @item int
	
	DECLARE ItemsCursor CURSOR FOR
		SELECT ItemCode from v0Item
			WHERE 1 = 1
				and NewBatchID = BatchID 
				and NewBatchID = @BatchId
	
	OPEN ItemsCursor
	FETCH NEXT FROM ItemsCursor INTO @item
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #res(PartID, MeasureID, MeasureName, BatchID, ItemCode, StateMsg)
		SELECT	
			v0Part.PartID, tblMeasure.MeasureID, tblMeasure.MeasureName, @BatchID, @item,
			dbo.GetFailedMeasure(@BatchID, @item,v0Part.PartID, tblMeasure.MeasureID)
		FROM 	v0Part, tblMeasure, @MeasureIds pm, hdtCPDocRule
			left join tblMeasureValue minM on hdtCPDocRule.MinMeasure = minM.MeasureValueID 
			and hdtCPDocRule.MeasureID = minM.MeasureValueMeasureID
			left join tblMeasureValue maxM on hdtCPDocRule.MaxMeasure = maxM.MeasureValueID 
			and hdtCPDocRule.MeasureID = maxM.MeasureValueMeasureID
		where 	hdtCPDocRule.CPDocID = @CPDocID
			and MinMeasure IS NOT NULL 
			and MaxMeasure IS NOT NULL /*and (NotVisibleInCCM = 1 )*/ 
			and hdtCPDocRule.MeasureID = tblMeasure.MeasureID
			and hdtCPDocRule.PartID = v0Part.PartID
			and tblMeasure.MeasureID = pm.ID
		FETCH NEXT FROM ItemsCursor INTO  @item
	END
	CLOSE ItemsCursor
	DEALLOCATE ItemsCursor
	
	select * from #res where StateMsg IS NOT NULL order by ItemCode
	drop table #res

GO

