USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStoneQuantitiesByCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStoneQuantitiesByCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetStoneQuantitiesByCustomer]
(
	@DateFrom date = null,
	@DateTo date = null,
	@CustomerId int = null,
	@OrderStateCode int = null
)
AS
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item))
	declare @dateEnd date = ISNULL(@DateTo, GetDate())
	
	SELECT count(*) as cnt, i.CustomerID into #stones
		FROM v0Item as i 
		WHERE 1=1
			and (@CustomerId IS NULL or @CustomerID = i.CustomerID)
			and (cast(i.OrderDate as DATE) between @dateBeg and @dateEnd) 
			and i.BatchID = i.NewBatchID
			and (@OrderStateCode IS NULL OR @OrderStateCode = i.OrderStateCode)
			and i.CustomerOfficeID <> 3
		GROUP BY i.CustomerID

	SELECT s.cnt, s.CustomerID, c.CustomerName, c.CompanyName 
		FROM #stones As s, v0CustomerNoExpDate As c
		WHERE 
			s.CustomerID = c.CustomerID
	
	drop table #stones	
	
GO

