--USE [GemoDream16]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx]
GO

/****** Object:  StoredProcedure [dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx]    Script Date: 05/08/2015 02:19:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx](
@BatchItemList [dbo].[BatchItemModelTableType]  READONLY,
@PartID int,
@MeasureID int,
@StartValue decimal(6,4),
@StopValue decimal(6,4),
@AuthorID int,
@AuthorOfficeID int,
@CurrentOfficeID int
)

as

select * from v0Item

Declare @NewItemCode int
Declare @NewBatchID  int
Declare @ItemTypeID int
Declare @rID varchar(255)

Declare @MeasureCode int

select @MeasureCode = MeasureCode from tblMeasure where MeasureID = @MeasureID

select v0Item.* into #ItemList from v0Item, @BatchItemList l where v0Item.BatchID = l.BatchId and v0Item.ItemCode = l.ItemCode

Declare items_cursor Cursor
for
	select NewBatchID, NewItemCode, ItemTypeID from #ItemList 
	--where #ItemList.BatchID = @BatchID 
	order by newBatchID, newItemCode

open items_cursor

	fetch next from items_cursor into @NewBatchID, @NewItemCode, @ItemTypeID 

	WHILE @@FETCH_STATUS = 0 
	begin
		
		declare @RandomMeasureValue decimal(6,4)

		exec ppGetRandomDecimal
			@Result = 	@RandomMeasureValue output,
			@ItemCode = 	@NewItemCode,
			@BatchID = 	@NewBatchID,
			@ItemTypeID = 	@ItemTypeID,
			@StartValue = 	@StartValue,
			@EndValue = 	@StopValue

     		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@NewItemCode,
			@BatchID=@NewBatchID, 
			@PartID=@PartID, 
			@MeasureCode= @MeasureCode,
			@MeasureValue= @RandomMeasureValue,
			@MeasureValueID= null,
			@StringValue= null,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, @AuthorOfficeID=@AuthorOfficeID 


		fetch next from items_cursor into @NewBatchID, @NewItemCode, @ItemTypeID 
	end


GO

