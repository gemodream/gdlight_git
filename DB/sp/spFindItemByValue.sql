USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[spFindItemByValue]    Script Date: 11/22/2017 12:00:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spFindItemByValue] --112, '500843%', 1
(
@MeasureID dnTinyID, 
@StringValue dsName, 
@rows int
)
AS

declare @sql nvarchar(max)
declare @7Digit BINARY
declare @IntPrefix INT
-- For Test Only
--declare @MeasureID dnTinyID, @StringValue dsName, @rows int
--SET @MeasureID = 112
--SET @StringValue = '500843%'
--SET @rows = 1
-- End of Test

IF @rows = 1 SET @StringValue = REPLACE(@StringValue, '%', '')
SET @7Digit = 0

CREATE TABLE #MyRechecks (RecheckSessionID INT, ItemCode INT,  
						  RecheckNumber INT, BatchID INT, MeasureGroupID INT)

select * into #PartValue
from v0PartValue
where 1 = 2

SELECT * INTO #AdditionalNumbers
FROM tblItemAdditionalInfo
WHERE 1 = 2

select @sql = N'
	INSERT	INTO  #PartValue
	select  * from v0PartValue where PartValueHistoryID in 
(select PartValueHistoryID from hstPartValue where StringValue like @StringValue and MeasureID = @MeasureID) order by BatchID, ItemCode'

exec sp_executesql @sql,N'@MeasureID dnTinyID, @StringValue dsName', @MeasureID, @StringValue--, @rows

SET @IntPrefix =
(select 
    CASE
        WHEN ISNUMERIC(@StringValue)=1 THEN  CAST(@StringValue as INT)  
        ELSE 0
    END)

IF LEN(RTRIM(LTRIM(@StringValue))) = 7 AND @rows = 1 AND @MeasureID = 112 AND @IntPrefix > 0
BEGIN
	INSERT INTO #AdditionalNumbers
	SELECT TOP 1 * FROM tblItemAdditionalInfo
	WHERE StringValue = @StringValue
	IF @@ROWCOUNT <> 0 SET @7Digit = 1
	UPDATE #AdditionalNumbers
	SET BatchID = i.NewBatchID, ItemCode = i.NewItemCode
	FROM  #AdditionalNumbers ad, v0Item i
	WHERE ad.BatchID = i.BatchID AND ad.ItemCode = i.ItemCode
	
	IF @7Digit = 1
	BEGIN
	--print 'select line 50'
		INSERT INTO	#MyRechecks  
		SELECT rs.RecheckSessionID, rs.ItemCode, 
		rs.RecheckNumber, rs.BatchID, MeasureGroupID
		FROM	tblRecheckSession rs, /* #PartValue i, */ #AdditionalNumbers ad  
		WHERE	rs.BatchID = ad.BatchID
        AND rs.ItemCode = ad.ItemCode
		AND MeasureGroupID in (select m.MeasureGroupID from tblMeasure m where m.MeasureID = @MeasureID)
		--select * from #MyRechecks
	END
END
ELSE
BEGIN
--print 'select line 64'
		INSERT INTO	#MyRechecks  
		SELECT rs.RecheckSessionID, rs.ItemCode, 
		--(CASE WHEN (rs.RecheckSessionID >= 500000000) THEN (DATEADD(minute, - 570, StartDate)) WHEN (rs.RecheckSessionID < 500000000) 
		--	THEN StartDate END) AS StartDate,
		rs.RecheckNumber, rs.BatchID, MeasureGroupID
		FROM	tblRecheckSession rs, #PartValue i  
		WHERE	rs.BatchID = i.BatchID
        AND rs.ItemCode = i.ItemCode
		and MeasureGroupID in (select m.MeasureGroupID from tblMeasure m where m.MeasureID = @MeasureID)
END
select top (@rows) * from #PartValue v
where v.RecheckNumber in
		(
			select max(RecheckNumber) from #MyRechecks rs 
			where	rs.BatchID = v.BatchID
					AND rs.ItemCode = v.ItemCode
					AND rs.MeasureGroupID = (select m.MeasureGroupID from tblMeasure m where m.MeasureID=@MeasureID)
					--order by StartDate DESC
		)
--drop table #PartValue, #MyRechecks,  #AdditionalNumbers

