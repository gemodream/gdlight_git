USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[wspvvGetItemsByBatchExistingNumbersOnly]    Script Date: 01/14/2015 23:36:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER    PROCEDURE [dbo].[wspvvGetItemsByBatchExistingNumbersOnly] 
(
	@BatchID integer)
AS
BEGIN
select i.ItemCode,it.ItemTypeName, b.MemoNumber,i.LotNumber,b.BatchCode, i.orderCode, b.GroupID, i.BatchID, i.NewBatchID, i.NewItemCode, i.prevordercode, i.prevbatchcode, i.previtemcode
    into #item
	from v0Item i 
	inner join v0Batch b on i.BatchID=b.BatchID 
	inner join v0ItemType it on it.ItemTypeID=b.ItemTypeID 
	where @BatchID = b.BatchID
	  and @BatchID = i.BatchID
--  and i.batchid=i.NewBatchID

--order by i.prevordercode, i.prevbatchcode, i.previtemcode

select i.ItemCode,i.ItemTypeName, i.MemoNumber,i.LotNumber,i.BatchCode, i.orderCode, i.GroupID from #item i where i.BatchID = i.NewBatchID
order by i.prevordercode, i.prevbatchcode, i.previtemcode

select i.ItemCode,i.ItemTypeName, i.MemoNumber,i.LotNumber,i.BatchCode, i.orderCode, i.GroupID,
       i2.GroupCode newGroupCode, i2.BatchCode newBatchCode, i2.ItemCode newItemCode
	from #item i
  left outer join v0Item i2 on
    i.NewBatchID = i2.BatchID and i.NewItemCode = i2.ItemCode
	where i.batchid!=i.NewBatchID

order by i.prevordercode, i.prevbatchcode, i.previtemcode

END