USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[spRulesTracking24FullOrder]    Script Date: 12/7/2017 5:10:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[spRulesTracking24FullOrder] --18357900101, 0
(
@BatchFullNumber bigint,
@ShowRules bit
)
AS

declare @CPDocID dnID
declare @GroupCode int
declare @BatchCode dnID
declare @ItemCode dnID
declare @ResultValue VARCHAR(1000)
declare @ItemNumber VARCHAR(20)
declare @NewItemNumber VARCHAR(20)
declare @OldItemNumber VARCHAR(20)
declare @ItemStatus VARCHAR(20)
declare @FullNumber bigint
declare @cnt1 bigint
declare @cnt2 bigint
declare @RulesNumber bigint
declare	@RuleName VARCHAR(40)
declare @CustomerProgramName VARCHAR(40)
declare @Status VARCHAR(40)

--Please set these variables for Test:---
--DECLARE
--@BatchFullNumber bigint,
--@ShowRules bit
--SET	@BatchFullNumber = 49660900100--455800300--417700800 --0411600100--, 0
--SET	@ShowRules = 1
-------------------------------------

set		@FullNumber = @BatchFullNumber
set 	@GroupCode =  @FullNumber /100000
set 	@BatchCode =  (@FullNumber % 100000)/100
set 	@ItemCode  =  (@FullNumber % 100000)%100 --if 0 , procedure works for whole batch
-------------------------------------
SELECT  v.ItemCode, v.NewBatchID, v.NewItemCode,
	dbo.GetFullGroupCode(v.GroupCode) + '.' + 
	--REPLICATE('0', 5 - LEN(CAST(v.GroupCode AS VARCHAR(5)))) + CAST(v.GroupCode AS VARCHAR(5)) + '.' 
	RIGHT('000' + CAST(v.BatchCode AS VARCHAR(3)), 3) + '.' + 
	RIGHT('00' + CAST(v.ItemCode AS VARCHAR(2)), 2)  as ItemNumber,
	dbo.GetFullGroupCode(v1.GroupCode) + '.' + 
	--REPLICATE('0', 5 - LEN(CAST(v1.GroupCode AS VARCHAR(5)))) + CAST(v1.GroupCode AS VARCHAR(5)) + '.' 
	RIGHT('000' + CAST(v1.BatchCode AS VARCHAR(3)), 3) + '.' + 
	RIGHT('00' + CAST(v1.ItemCode AS VARCHAR(2)), 2)  as NewItemNumber,
	dbo.GetFullGroupCode(v1.PrevGroupCode) + '.' + 
	--REPLICATE('0', 5 - LEN(CAST(v1.PrevGroupCode AS VARCHAR(5)))) + CAST(v1.PrevGroupCode AS VARCHAR(5)) + '.' 
	RIGHT('000' + CAST(v1.PrevBatchCode AS VARCHAR(3)), 3) + '.' + 
	RIGHT('00' + CAST(v1.PrevItemCode AS VARCHAR(2)), 2)  	as OldItemNumber,
	b.CPID, (CASE 	WHEN 	v.BatchID = v.NewBatchID THEN 'Active'
			WHEN	v.BatchID != v.NewBatchID THEN 
			('New # ' + dbo.GetFullGroupCode(v1.GroupCode) + '.' + 
						--REPLICATE('0', 5 - LEN(CAST(v1.GroupCode AS VARCHAR(5)))) + CAST(v1.GroupCode AS VARCHAR(5)) + '.' +
							RIGHT('000' + CAST(v1.BatchCode AS VARCHAR(3)), 3) + '.' + 
							RIGHT('00' + CAST(v1.ItemCode AS VARCHAR(2)), 2)  	)
			END) as ItemStatus, b.ItemTypeID
INTO 	#items
FROM 	v0Item v, v0Item v1, v0Batch b
WHERE	v.GroupCode = @GroupCode and
	(@BatchCode = 0 or v.BatchCode = @BatchCode) and
(@ItemCode = 0 or v.ItemCode = @ItemCode) and
    v1.BatchID = v.NewBatchID and
	v1.ItemCode = v.NewItemCode and
	b.BatchID = v.NewBatchID
--select * from #items

--declare @Nlines as int
SELECT	DISTINCT hdtCPDoc.CPDocID as CPDocID, b.CPID, b.ItemTypeID, CAST(@RulesNumber as VARCHAR(2)) as RulesNumber, 
	v0CustomerProgram.CustomerProgramName as CPName, b.NewBatchID as BatchID, @RuleName as [Rule #]
INTO 	#DocID
FROM 	 v0CustomerProgram, hdtCPDoc, hdtCPDoc_Operation, #items b
WHERE 	b.NewBatchID  in (SELECT DISTINCT NewBatchID FROM #items /*where ItemStatus = 'Active'*/)
	 and b.CPID = v0CustomerProgram.CPID
--	and b.CPOfficeID = v0CustomerProgram.CPOfficeID
	and v0CustomerProgram.CustomerProgramHistoryID = hdtCPDoc.CustomerProgramHistoryID
	AND  hdtCPDoc.CPDocID = hdtCPDoc_Operation.CPDocID 

BEGIN
	DELETE #DocID
	FROM #DocID d1
	WHERE CPDocID in (SELECT MIN(CPDocID) FROM #DocID d2 where d1.BatchID = d2.BatchID)
END
--select 'TableDocID' as TableName, * from #DocID
--SET	@RulesNumber = 0

update #DocID 
   set RulesNumber = rn, [Rule #] = 'Rule #' + CAST(rn as varchar(2))
  from (select ROW_NUMBER() over (partition by CPDocID order by batchid) rn, * from #DocID)  d

--select 'TableDocID' as TableName, * from #DocID

SELECT 	#DocID.CPName, #DocID.[Rule #], PartName, v0Part.PartID, tblMeasure.MeasureName, 
	tblMeasure.MeasureCode, MinMeasure, 
	--minM.MeasureValueName MinName, 
	minM.ValueTitle as Min_Value,
	--(select nOrder from vwMeasureValue where MeasureValueID = hdtCPDocRule.MinMeasure) MinOrder,
	MaxMeasure, 
	--maxM.MeasureValueName MaxName, 
	maxM.ValueTitle as Max_Value,
	--(select nOrder from vwMeasureValue where MeasureValueID = hdtCPDocRule.MaxMeasure) MaxOrder, 
	@ResultValue as RealValue, -- @Fail as Status
	tblMeasure.MeasureID,
	tblMeasure.MeasureGroupID,
	NotVisibleInCCM, --hdtCPDocRule.CPDocID   --,
	RulesNumber, #DocID.CPDocID, #DocID.BatchID 
	
INTO 	#partsMeasures
FROM 	tblMeasure,  #DocID, v0Part, hdtCPDocRule
	left 	join tblMeasureValue minM on hdtCPDocRule.MinMeasure = minM.MeasureValueID 
		and hdtCPDocRule.MeasureID = minM.MeasureValueMeasureID
	left 	join tblMeasureValue maxM on hdtCPDocRule.MaxMeasure = maxM.MeasureValueID 
		and hdtCPDocRule.MeasureID = maxM.MeasureValueMeasureID
where 	hdtCPDocRule.CPDocID = #DocID.CPDocID
	and not (((MinMeasure is Null) and (MaxMeasure is Null)) or ((MinMeasure = 0) and (MaxMeasure = 0)))
	and NotVisibleInCCM != 1 
	and hdtCPDocRule.MeasureID = tblMeasure.MeasureID
	and hdtCPDocRule.PartID = v0Part.PartID
	and v0Part.ItemTypeID in (select distinct ItemTypeID from #items)
	and tblMeasure.MeasureID not in (9,26)
--select 'Table_Measures', * from #partsMeasures
;
with v as (select v.*, i.NewItemNumber ItemNumber from V0PartValue2 v, #items i where v.BatchID = i.NewBatchID and v.ItemCode = i.NewItemCode and i.ItemStatus = 'Active' and v.MeasureID not in (9,26))
select *
into #V0PartValue
FROM v
	WHERE RecheckNumber = (select MAX(RecheckNumber) from v v2 where v.BatchID = v2.BatchID and v.ItemCode = v2.ItemCode and v.MeasureID = v2.MeasureID and v.PartID = v2.PartID)

select 	ItemNumber, pv.MeasureValue, pv.MeasureValueOrder, cpdr.MeasureID, 
	cpdr.MeasureCode, cpdr.PartID, pv.MeasureName, cpdr.CPDocID,  pv.ItemCode, pv.BatchID
into 	#b
from 	#v0PartValue pv, #partsMeasures cpdr, #DocID doc
where 	cpdr.PartID = pv.PartID 
	 and pv.MeasureID = cpdr.MeasureID
	and	cpdr.CPDocID = doc.CPDocID
	and doc.BatchID = pv.BatchID
	
-------------------
and 
(
( (cpdr.MinMeasure is Null) or 
		(
			( (pv.ValueCode is null) ) and (MeasureValue >= cpdr.MinMeasure) ) 
			or
			( (pv.ValueCode is not null) and (pv.MeasureValueOrder >= (select nOrder from vwMeasureValue where MeasureValueID = cpdr.MinMeasure) ) 
		)
 )
and
( (cpdr.MaxMeasure is Null) or 
		(
			( (pv.ValueCode is null) ) and (MeasureValue <= cpdr.MaxMeasure) ) 
			or
			( (pv.ValueCode is not null) and (pv.MeasureValueOrder <= (select nOrder from vwMeasureValue where MeasureValueID = cpdr.MaxMeasure ) ) 
		))

)
and not ( (cpdr.MinMeasure is Null) and (cpdr.MaxMeasure is Null) and (cpdr.NotVisibleInCCM = 1) ) 
	-------------------------------------

SELECT 	null as Cnt1, null as Cnt2, null as CPDocID, @NewItemNumber as NewItemNumber, @ItemNumber as ItemNumber, 
	@OldItemNumber as OldItemNumber, @ItemStatus as ItemStatus, ' ' as RulesNumber, 
	@CustomerProgramName as CPName, @RuleName as RuleName, @Status as Status, CAST(null as bigint) BatchID
INTO 	#Cnt
WHERE 	1=2	

--select distinct * from #b
insert into #Cnt
select distinct cnt1.cnt, cnt2.cnt, d.CPDOcID, i.NewItemNumber, i.ItemNumber, I.OldItemNumber, i.ItemStatus, d.RulesNumber, d.CPName, d.[Rule #],
case when i.ItemStatus = 'Active' then case when cnt1.cnt = cnt2.cnt then 'Ok to Print' else 'Failed' end 
     else 'Migrated to New#'
     end, i.NewBatchID
from #items i inner join #DocID d on i.CPID = d.CPID
left outer join (select COUNT(*) cnt, CPDOCID from #PartsMeasures group by CPDocID) cnt1 on cnt1.CPDocID = d.CPDocID
left outer join (select COUNT(*) cnt, CPDOCID, ItemCode, BatchID from #b group by CPDocID, ItemCode, BatchID) cnt2 on cnt2.CPDocID = d.CPDocID and cnt2.ItemCode = i.NewItemCode and cnt2.BatchID = i.NewBatchID

SELECT * FROM #Cnt ORDER BY ItemNumber, ItemStatus, CPDocID

	SELECT 	(CASE 	WHEN v.MeasureClass = 1 THEN v.ValueTitle
			WHEN (v.MeasureClass = 2 OR v.MeasureClass = 4) THEN v.StringValue
			WHEN (v.MeasureClass = 3) 			THEN CAST(v.MeasureValue as VARCHAR(1000))
			END) as RealValue,-- m.MeasureCode, 
			CAST (null as varchar(1000)) RealValue2, 
			v.ItemNumber, v.PartID, v.MeasureID, i.OldItemNumber, d.CPDocID, i.NewBatchID BatchID
	INTO 	#c
	FROM 	#V0PartValue v, #items i, #DocID d
	WHERE v.ItemCode = i.NewItemCode
	  AND v.BatchID = i.NewBatchID
	  AND i.CPID = d.CPID
	  AND d.BatchID = i.NewBatchID
	  and i.ItemStatus = 'Active'

	

	DELETE 	#items
	WHERE 	#items.ItemNumber in (SELECT DISTINCT ItemNumber FROM #Cnt WHERE Cnt1 = Cnt2)
	
	update #c
	set RealValue2 = RealValue
	FROM #c c
	WHERE NOT EXISTS (select * from #b b 
WHERE b.MeasureID  = c.MeasureID 
            			AND b.PartID = c.PartID
            			AND b.ItemNumber = c.ItemNumber
				AND b.CPDocID = c.CPDocID)

insert into #c  (RealValue, RealValue2, ItemNumber, PartID, MeasureID, OldItemNumber, CPDocID, BatchID)
select distinct null, 'No Measure', c.ItemNumber, p.PartID, p.MeasureID, c.OldItemNumber, p.CPDocID, p.BatchID
FROM #c c inner join #partsMeasures p on p.CPDocID = c.CPDocID and p.BatchID = c.BatchID
and not exists (select * from #c c2 where c2.PartID = p.PartID and c2.MeasureID = p.MeasureID and c2.BatchID = c.BatchID and c2.ItemNumber = c.ItemNumber)


SELECT * FROM
(
SELECT 	ItemNumber, OldItemNumber, PartName, MeasureName, 
	ISNULL(Min_Value,p.MinMeasure) as Min_Value, 
	ISNULL(Max_Value,p.MaxMeasure) as Max_Value, 
	CASE	WHEN	LTRIM(RTRIM(c.RealValue2))= '' THEN 'Fail'
		WHEN	LTRIM(RTRIM(c.RealValue2))!='' THEN c.RealValue2
		WHEN c.PartID IS NULL THEN 'No Measure'
	END as RealValue, p.CPDocID, p.RulesNumber, p.CPName, c.BatchID
FROM 	#c c inner join #partsMeasures p
ON 	c.RealValue2 is not null --!= ''
  AND c.PartID = p.PartID
  AND c.MeasureID = p.MeasureID
  AND c.CPDOCID = p.CPDOCID
) d
WHERE RealValue IS NOT NULL
ORDER BY ItemNumber, CPDocID, PartName, MeasureName

IF @ShowRules = 1
	BEGIN
	SELECT * FROM #partsMeasures
	ORDER BY RulesNumber
	END


--DROP TABLE #Cnt
--DROP TABLE #c
--DROP TABLE #b
--DROP TABLE #V0PartValue

--DROP TABLE #DocID
--DROP TABLE #partsMeasures
--DROP TABLE #items


