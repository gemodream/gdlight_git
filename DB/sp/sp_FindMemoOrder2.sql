USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[sp_FindMemoOrder2]    Script Date: 02/14/2015 15:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FindMemoOrder2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FindMemoOrder2]
GO

USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[sp_FindMemoOrder2]    Script Date: 02/14/2015 15:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[sp_FindMemoOrder2]

   @MemoNumber varchar(255)

AS

SELECT REPLICATE('0', 5 - LEN(CAST(v0Group.GroupCode AS VARCHAR(5)))) + CAST(v0Group.GroupCode AS VARCHAR(5)) as [Order Number], 
hstMemoNumber.Name as [Memo Number]
, v0Group.CustomerCode as [Client Number], v0Group.CreateDate as [Date]
	FROM hstMemoNumber INNER JOIN
        tblMemoNumber ON hstMemoNumber.MemoNumberID = tblMemoNumber.MemoNumberID AND 
        hstMemoNumber.MNHID = tblMemoNumber.MNHID INNER JOIN
        v0Group ON tblMemoNumber.GroupID = v0Group.GroupID
	WHERE     (hstMemoNumber.Name LIKE @MemoNumber)
	ORDER BY v0Group.GroupCode

SELECT REPLICATE('0', 5 - LEN(CAST(v0Group.GroupCode AS VARCHAR(5)))) + CAST(v0Group.GroupCode AS VARCHAR(5)) as [Order Number], 
	v0Group.Memo as [Memo Number]
	, v0Group.CustomerCode as [Client Number], v0Group.CreateDate as [Date]
from v0Group
where 
	v0Group.Memo is not null
and	v0Group.Memo like @MemoNumber



GO

