USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spCountryOffices]    Script Date: 09/25/2016 00:26:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCountryOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCountryOffices]
GO

USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spCountryOffices]    Script Date: 09/25/2016 00:26:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spCountryOffices]
as
	--  Countries
	select ParentOfficeID, Country from tblOfficeNewNumbers group by ParentOfficeID, Country
	
	-- Offices
	select OfficeID, ParentOfficeID, OfficeCode, OfficeAddress from tblOfficeNewNumbers  where OfficeID <> 3

GO

