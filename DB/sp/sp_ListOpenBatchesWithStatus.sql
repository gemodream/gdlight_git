--USE [GemoDream16]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ListOpenBatchesWithStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ListOpenBatchesWithStatus]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_ListOpenBatchesWithStatus]
AS
create table #openBatch(
 OrderCode int,
 BatchCode int,
 NumberOfItems int,
 CustomerName varchar(314),
 CustomerID numeric(8,0),
 --State varchar(250),
 OrderCreationDate datetime,
 BatchID numeric(15,0),
 Status varchar(100),
 ifExists int
)

--select distinct BatchID into #t from v0Item where BatchID = NewBatchID and ItemCode = NewItemCode

insert into #openBatch(OrderCode, BatchCode, NumberOfItems, CustomerName, CustomerID, OrderCreationDate, BatchID, ifExists)
SELECT
 v0Batch.GroupCode,
 v0Batch.BatchCode,
 v0Batch.ItemsQuantity AS NumberOfItems, 
    v0Customer.CustomerName AS CustomerName, 
    v0Customer.CustomerID, 
    v0Group.CreateDate AS OrderCreationDate,
 v0Batch.BatchID as BatchID, 
    0 as ifExists
FROM    v0Batch 
 INNER JOIN
        v0Customer ON 
   v0Batch.CustomerID = v0Customer.CustomerID 
  INNER JOIN v0Group ON 
   v0Batch.GroupID = v0Group.GroupID
--     inner join #t on 
--   v0Batch.BatchID = #t.BatchID
   
WHERE
 (NOT (v0Customer.CustomerCode IN (1141, 1183))) 
AND  (NOT (v0Batch.StateName IN ('invalid', 'closed')))
--and (v0Customer.CustomerID = @CustomerID or (ISNULL(@CustomerID,0) = 0))


-- sp_getInvoiceStatusByBatchID 
update #openBatch set Status = tblinvoice2.status,
             ifExists = 1
from 
    #openBatch b
    inner join  hstItem h  on  (h.NewBatchID = h.BatchID and h.NewItemCode = h.ItemCode and h.BatchID = b.BatchID)
    inner join tblItem t on t.ItemHistoryID=h.ItemHistoryID and  t.ExpireDate is null
 left outer join tblinvoiceitem on tblinvoiceitem.batchid=h.batchid and  tblinvoiceitem.itemcode=h.itemcode
 left outer join tblinvoice2 on tblinvoice2.invoiceid=tblinvoiceitem.invoiceid
 
select OrderCode, BatchCode, NumberOfItems, CustomerName, CustomerID, OrderCreationDate, BatchID, Status from #openBatch b
--where ( (ISNULL(@Status,'') = '') or status = @Status )
where ifExists = 1
order by CustomerName, OrderCode, BatchCode

GO


