--USE [GemoDream16]
--GO
/* 82497 */
-- 2236, 19, 1
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetOrderHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetOrderHistory]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spGetOrderHistory] 
(
	@GroupCode as int,
	@AuthorID dnSmallID,
	@AuthorOfficeID dnTinyID
)  AS

Declare @CustomerId int
set @CustomerId = 
(SELECT TOP 1 vg.CustomerID from v0Group vg where 
	(
		@GroupCode = vg.GroupCode AND
		vg.GroupOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
	)
)
-- 1. Customer
Select c.CustomerName, c.CustomerOfficeID_CustomerID,c.CustomerCode,c.IconIndex,c.StateCode,c.StateTargetCode,c.StateName,c.CompanyName , c.ShortName
	From dbo.v0Customer c where c.CustomerID = @CustomerId
	
-- 2. Order
EXEC	[dbo].[spGetGroupByCode]
		@CustomerCode = NULL,
		@BGroupState = NULL,
		@EGroupState = NULL,
		@BState = NULL,
		@EState = NULL,
		@BDate = NULL,
		@EDate = NULL,
		@GroupCode = @GroupCode, 
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID
		
-- 3. Batches
EXEC [dbo].[spGetBatchByCode]
		@CustomerCode = NULL,
		@BGroupState = NULL,
		@EGroupState = NULL,
		@BState = NULL,
		@EState = NULL,
		@BDate = NULL,
		@EDate = NULL,
		@GroupCode = @GroupCode,
		@BatchCode = NULL,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID

-- 4. Items
EXEC	[dbo].[spGetItemByCode]
		@CustomerCode = NULL,
		@BGroupState = NULL,
		@EGroupState = NULL,
		@BState = NULL,
		@EState = NULL,
		@BDate = NULL,
		@EDate = NULL,
		@GroupCode = @GroupCode,
		@BatchCode = NULL,
		@ItemCode = NULL,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID,
		@IsNew = NULL	
		
-- 5. ItemDocs
EXEC	[dbo].[spGetItemDocByCode]
		@CustomerCode = NULL,
		@BGroupState = NULL,
		@EGroupState = NULL,
		@BState = NULL,
		@EState = NULL,
		@BDate = NULL,
		@EDate = NULL,
		@GroupCode = @GroupCode,
		@BatchCode = NULL,
		@ItemCode = NULL,
		@OperationChar = NULL,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID
			
GO

