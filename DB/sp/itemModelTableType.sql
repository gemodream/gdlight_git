--USE [GemoDream16]
--GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeoOrderReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeoOrderReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EnqueueLeoList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EnqueueLeoList]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'itemModelTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[itemModelTableType]
GO

/****** Object:  UserDefinedTableType [dbo].[intTableType]    Script Date: 03/09/2015 00:15:57 ******/
CREATE TYPE [dbo].[itemModelTableType] AS TABLE(
	[Item] varchar(11) NOT NULL,
	[GroupCode] varchar(10) NOT NULL,
	[BatchCode] varchar(10) NOT NULL,
	[ItemCode] varchar(10) NOT NULL
)
GO

