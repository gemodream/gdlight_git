--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[trkSpSetDeliveryLogEx]    Script Date: 05/18/2015 20:56:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trkSpSetDeliveryLogEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[trkSpSetDeliveryLogEx]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'LogItemTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[LogItemTableType]
GO


/****** Object:  UserDefinedTableType [dbo].[intTableType]    Script Date: 03/09/2015 00:15:57 ******/
CREATE TYPE [dbo].[LogItemTableType] AS TABLE(
	[ItemNumber] varchar(11) NOT NULL,
	[ItemCode] int NOT NULL
)
GO



/****** Object:  StoredProcedure [dbo].[trkSpSetDeliveryLogEx]    Script Date: 05/18/2015 20:56:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[trkSpSetDeliveryLogEx]
(
	@ItemNumbers [dbo].[LogItemTableType]  READONLY, 
	@BatchNumber int, 
	@AuthorID int, 
	@AuthorOfficeID int, 
	@trkEventID int, @BatchID int
)
as


declare @NewBatchHistoryID int
declare @AffectedItems int
declare @NumberOfitemsInBatch int
select @NumberOfitemsInBatch = (Select COUNT(*) from V0Item where BatchID = @BatchID and BatchID = newBatchID)

/*
*  Prepare Table With items
*
*/
CREATE TABLE #t
	(
		ItemNumber varchar(11),
		ItemCode int		
	)  
  

insert  into #t (ItemNumber, ItemCode) SELECT  i.ItemNumber, i.ItemCode 

FROM       @ItemNumbers i


/*
*  Get Number of Affected Items
*
*/

select @AffectedItems = COUNT(*) from #t 

--select * from #t

if @AffectedItems > 0

begin

	begin transaction
		insert trkBatchHistory (
					FormID, 
					UserID, 
					RecordTimeStamp,
					EventID, 
					BatchID, 
					NumberOfItemsAffected, 
					NumberOfItemsInBatch)		
				values
				(
				201,
				@AuthorID,
				GETDATE(),
				@trkEventID,
				@BatchID,
				@AffectedItems,
				@NumberOfitemsInBatch
				)
	
	select @NewBatchHistoryID = @@IDENTITY

	
	insert trkItemHistory (
				FormID,
				UserID,
				RecordTimeStamp,
				EventID,
				BatchID,
				BatchTrackingHistoryID,
				FullItemNumber,
				ItemCode
				)
				select 
				201,
				@AuthorID,
				GETDATE(),
				@trkEventID,
				@BatchID,
				@NewBatchHistoryID,
				ItemNumber,
				ItemCode
				from 
				#t
	
	

	commit transaction

end 

GO

