USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportQuantityByCustomer1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportQuantityByCustomer1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetReportQuantityByCustomer1]
(
	@DateFrom as datetime = null,
	@DateTo as datetime = null,
	@CustomerId int = null
)
AS
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item)) -- '2013-01-01' -- 
	declare @dateEnd date = ISNULL(@DateTo, GetDate())	-- '2014-01-01' -- 
	--declare @CustomerID int = 152--, CustomerCode = 1259
	select i.CustomerID, i.BatchID, i.ItemCode into #items
		from 
			v0Item As i
		where 1=1
			and ((CAST(i.OrderDate as Date) between @dateBeg and @dateEnd))
			and (@CustomerID IS NULL or @CustomerID = i.CustomerID)
			and i.BatchID = i.NewBatchID
			and i.CustomerOfficeID <> 3	
			and i.OrderStateCode = 1
	
	select  i.CustomerID, i.BatchID, i.ItemCode, doc.DocumentTypeCode into #items2  
		from 
			#items As i,
			tblItemOperation As tio,
			hstItemOperation As h,
			tblDocument As doc,
			tblPrintingQueue As q
		where 1=1
			
			and i.BatchID = tio.BatchID 
			and i.ItemCode = tio.ItemCode
			
			and tio.ItemOperationHistoryID = h.ItemOperationHistoryID
			
			and tio.OperationTypeID = doc.OperationTypeID
			and doc.DocumentTypeCode not in (8,10,19,20)
			
			and q.ItemOperationID = h.ItemOperationID
			and q.State in ('Y', 'C')
			-- and CAST(q.PrintDate as date) between @dateBeg and @dateEnd
		group by i.CustomerID, i.BatchID, i.ItemCode, doc.DocumentTypeCode
	select i.CustomerID, dt.DocumentTypeName, count(i.ItemCode) as cnt INTO #byDocType 
		from #items2 As i, tblDocumentType AS dt	
		where i.DocumentTypeCode = dt.DocumentTypeCode
		group by i.CustomerID, dt.DocumentTypeName
	
	SELECT CustomerID,
		STUFF((
		SELECT '; ' + CAST([cnt] AS VARCHAR(MAX)) + ' ' + [DocumentTypeName]
		FROM #byDocType 
		WHERE (CustomerID = Results.CustomerID) 
		FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
		,1,2,'') AS details into #byDocType1
	FROM #byDocType Results
	GROUP BY CustomerID	
	select  i.CustomerID, count(i.CustomerID) as quantity, c.CustomerName, d.details  
		from 
			#items2 As i,
			v0CustomerNoExpDate As c,
			#byDocType1 As d
		where 
			i.CustomerID = c.CustomerID
			and i.CustomerID = d.CustomerID
		group by i.CustomerID , c.CustomerName, d.details
		
	drop table #items	
	drop table #items2
	drop table #byDocType
	drop table #byDocType1

GO

