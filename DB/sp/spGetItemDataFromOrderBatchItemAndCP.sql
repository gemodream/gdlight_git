--USE [GemoDream16]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetItemDataFromOrderBatchItemAndCP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetItemDataFromOrderBatchItemAndCP]
GO

/****** Object:  StoredProcedure [dbo].[spGetItemDataFromOrderBatchItemAndCP]    Script Date: 01/25/2015 18:43:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetItemDataFromOrderBatchItemAndCP]
(
@GroupCode int,
@BatchCode int,
@ItemCode int
)
as

-- Test for GroupCode=128693, BatchCode = 12, ItemCode = 5
-- PC = 0 for partName = White Diamond Stone Set and (Color Grade or Clarity Grade)
create table #result1 (
	NewItemNumber varchar(20),
	OldItemNumber varchar(20),
	MeasureCode int,
	MeasureName varchar(100),
	ValueCode varchar(100),
	BatchID int,
	ResultValue varchar(2000),
	PartID int,
	PartName varchar(100),
	NewBatchID int,
	NewItemCode int,
	MeasureID int default 0,
	CP int default 0
)
insert into #result1 (NewItemNumber, OldItemNumber, MeasureCode, MeasureName, ValueCode, BatchID, ResultValue, PartID, PartName, NewBatchID, NewItemCode)
exec [dbo].[sp_GetItemDataFromOrderBatchItem] @GroupCode, @BatchCode, @ItemCode
--, @AuthorID = 19, @AuthorOfficeID = 1

--select * from v0CPDoc where CPDocID in (2189415, 2189416)
SELECT v0CPDoc.CPDocID, tblBatch.BatchID
into #cpDoc
FROM v0CpDoc INNER JOIN
		tblBatch INNER JOIN
        tblCustomerProgram ON tblBatch.CPOfficeID = tblCustomerProgram.CPOfficeID AND
		tblBatch.CPID = tblCustomerProgram.CPID ON
        v0CPDoc.CustomerProgramHistoryID = tblCustomerProgram.CustomerProgramHistoryID
WHERE (tblBatch.BatchID in (select NewBatchID from #result1)) and nDocRule > 0

/*
Select @BatchID = NewBatchID, @ItemCode = NewItemCode
from v0Item2
where BatchID = @BatchID and @ItemCode = ItemCode
*/
--select * from tblMeasure
update #result1 set MeasureID = m.MeasureID
from #result1 res, tblMeasure m
where m.MeasureCode = res.MeasureCode

update #result1 set CP = 1
from #result1 r where not exists (select 1 from v0CPDocRule c
where CPDocID in (select CPDocID from #cpDoc cp where cp.BatchID = r.NewBatchID) and PartID = r.PartID and c.MeasureID = r.MeasureID
and not MinMeasure is null and not MaxMeasure is null)


select v.* into #pv from v0PartValue2 v, (select distinct NewBatchID, NewItemCode from #result1) res where v.BatchID = res.NewBatchID
and v.ItemCode = res.NewItemCode

update #result1 set CP = (
select count(*) from #cpDoc c, hdtCPDocRule cpdr, #pv pv
	where 
		cpdr.MeasureID=pv.MeasureID and pv.MeasureGroupID=pv.rMeasureGroupID 
		and cpdr.CPDocID = c.CPDocID and c.BatchID = pv.BatchID 
        and cpdr.PartID = res.PartID and cpdr.PartID=pv.PartID 
		and cpdr.MeasureID=pv.MeasureID 
		and pv.RecheckNumber = 
			(select max(RecheckNumber)
				from tblRecheckSession rs
					where 
						rs.BatchID=pv.BatchID and rs.ItemCode=pv.ItemCode and
						rs.MeasureGroupID = pv.rMeasureGroupID
			)
		and 
		(
			( (cpdr.MinMeasure is Null) or
                  (( (pv.ValueCode is null) ) and (MeasureValue >= cpdr.MinMeasure) ) or
				  ( (pv.ValueCode is not null) and (pv.MeasureValueOrder >= (select nOrder from vwMeasureValue where MeasureValueID = cpdr.MinMeasure) ) )
            )
        and
            (	(cpdr.MaxMeasure is Null) or
				( ( (pv.ValueCode is null) ) and (MeasureValue <= cpdr.MaxMeasure) ) or
                ( ( pv.ValueCode is not null) and (pv.MeasureValueOrder <= (select nOrder from vwMeasureValue where MeasureValueID = cpdr.MaxMeasure ) ) )
			)
                
        )
        and not ( (cpdr.MinMeasure is Null) and (cpdr.MaxMeasure is Null) and (cpdr.NotVisibleInCCM = 1) )
		and pv.MeasureID = res.MeasureID and pv.PartID = res.PartID
) from #result1 res  where CP = 0

select * from #result1
DROP TABLE #result1
DROP TABLE #pv 



