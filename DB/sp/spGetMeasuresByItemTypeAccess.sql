USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spGetMeasuresByItemTypeAccess]    Script Date: 06/25/2015 23:09:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetMeasuresByItemTypeAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetMeasuresByItemTypeAccess]
GO

/****** Object:  StoredProcedure [dbo].[spGetMeasuresByItemTypeAccess]    Script Date: 06/25/2015 23:09:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE  Procedure [dbo].[spGetMeasuresByItemTypeAccess] 
(
@ItemTypeID dnSmallID,
@ViewAccessCode int
) as

SELECT     v.MeasureID, v.MeasureCode, v.MeasureClass, v.MeasureTitle, v.MeasureName, p.PartID, p.PartName, am.IsEdit
FROM       vwPartType_Measure AS v INNER JOIN
           v0Part AS p ON p.PartTypeID = v.PartTypeID INNER JOIN
           tblAccess_Measure AS am ON v.MeasureID = am.MeasureID INNER JOIN
           tblViewAccess as va ON am.ViewAccessID = va.ViewAccessID
WHERE     
	(am.IsEdit = 1)  and  
	(@ViewAccessCode IS NULL OR va.ViewAccessCode = @ViewAccessCode) and
	(p.ItemTypeID = @ItemTypeID)
	order by p.PartID



GO

