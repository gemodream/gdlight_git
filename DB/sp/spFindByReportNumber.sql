USE [GDVirtualVault]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFindByReportNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFindByReportNumber]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE procedure [dbo].[spFindByReportNumber](
	@ReportNumber varchar(11)
)	
as

SELECT     vc.ID, vc.FirstName, vc.MI, vc.LastName, vc.Address, 
           vc.City, vc.Zip, vc.State, vc.DayPhone, vc.OtherPhone, 
           vc.EmailAddress, vc.Password, vc.CreatedDate, vr.ReportNumber, 
           vr.VirtualVaultNumber
FROM       virtualVaultCustomer vc INNER JOIN VirtualVaultReportList vr ON vc.ID = vr.VVC_ID
WHERE     (vr.ReportNumber LIKE '%' + @ReportNumber + '%')

GO

