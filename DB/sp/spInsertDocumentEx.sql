USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsertDocumentEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsertDocumentEx]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[spInsertDocumentEx] (
 @DocumentID dnID OUTPUT,
 @ItemTypeID  dnSmallID,
 @DocumentTypeCode dnTinyCode,
 @DocumentName  varchar(250),
 @BarCodeFixedText  varchar(2000),
 @UseDate  dnBool,
 @UseVirtualVaultNumber  dnBool,
 @CorelFile varchar(1000),
 @AuthorID dnSmallID,
 @AuthorOfficeID dnTinyID,
 @CurrentOfficeID dnTinyID,
 @ExportTypeID int,
 @ImportTypeID int,
 @FormatTypeID int,
 @DocumentValues [dbo].[DocumentValueTableType] READONLY
 )
AS
begin transaction;

begin
-- 1. spSetDocument

DECLARE	@rId varchar(150)

EXEC	[dbo].[spSetDocument]
		@rId = @rId OUTPUT,
		@DocumentName = @DocumentName,
		@BarCodeFixedText = @BarCodeFixedText,
		@UseDate = @UseDate,
		@UseVirtualVaultNumber = @UseVirtualVaultNumber,
		@ItemTypeID = @ItemTypeID,
		@OperationTypeName = NULL,
		@OperationChar = NULL,
		@DocumentTypeCode = @DocumentTypeCode,
		@CorelFile = @CorelFile,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID,
		@CurrentOfficeID = @CurrentOfficeID,
		@ExportTypeID = @ExportTypeID,
		@ImportTypeID = @ImportTypeID,
		@FormatTypeID = @FormatTypeID
set @DocumentID = convert(numeric(15,0), substring(@rId, 1, charindex('_', @rId)-1))

-- 2. spSetDocumentValue
insert into tblDocumentValue
(
 DocumentID,
 Title,
 Value,
 Unit
)
select
 @DocumentID,
 Title,
 Value,
 Unit
	from @DocumentValues
end;	
commit transaction;

GO

