USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRejectionRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRejectionRates]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetRejectionRates]
(
	@DateFrom date = NULL,
	@DateTo date = NULL,
	@CustomerID int = NULL
)
AS
create table #items
(
	CustomerID int,
	Quantity int default 0,
	PrintedQuantity int default 0
)
--insert into #items(Quantity, CustomerCode)

declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item)) -- '2013-01-01' 
declare @dateEnd date = ISNULL(@DateTo, GetDate())	-- '2014-01-01' 
select v2.CustomerID , v2.NewBatchID, v2.NewItemCode into #stoneItems
	from v0ItemNoExpDate as v1, v0ItemNoExpDate as v2
	where  1=1
		and cast(v1.CreateDate as DATE) between @dateBeg and @dateEnd
		and v1.BatchID = v2.BatchID
		and v1.BatchID = v2.NewBatchID
		and v1.ItemCode = v2.NewItemCode
		and (@CustomerID IS NULL or @CustomerID = v1.CustomerID)
		--and v2.OrderCode = 2236
	group by v2.CustomerID, v2.NewBatchID, v2.NewItemCode 

insert into #items(Quantity, CustomerID)
	select COUNT(*), CustomerID 
		from #stoneItems
		group by CustomerID

	
select v2.CustomerID, v2.NewBatchID, v2.NewItemCode into #prItems
	from 
		hstItemOperation h, 
		tblItemOperation tio, 
		tblDocument doc, 
		tblDocumentType dt, 
		tblPrintingQueue,
		v0ItemNoExpDate as v1, v0ItemNoExpDate as v2
	where  1=1
		and v1.CreateDate between @dateBeg and @dateEnd
		and v1.BatchID = v2.BatchID
		and v1.BatchID = v2.NewBatchID
		and v1.ItemCode = v2.NewItemCode
		--and v2.OrderCode = 2236
		and tio.BatchID = v1.BatchID
		AND tio.ItemCode = v1.ItemCode
		and h.ItemOperationHistoryID = tio.ItemOperationHistoryID
		and doc.OperationTypeID = tio.OperationTypeID
		and doc.DocumentTypeCode not in (8,10,19,20)
		and dt.DocumentTypeCode = doc.DocumentTypeCode
		and tblPrintingQueue.ItemOperationID = h.ItemOperationID
		and tblPrintingQueue.State in ('Y', 'C')
		and (@CustomerID IS NULL or @CustomerID = v1.CustomerID)

	group by v2.CustomerID, v2.NewBatchID, v2.NewItemCode
insert into #items(PrintedQuantity, CustomerID)
	select count(*), CustomerID 
		from #prItems
		group by CustomerID

select SUM(i.Quantity) as quantity, SUM(i.PrintedQuantity) as printed, i.CustomerID, c.CustomerName from #items As i, v0CustomerNoExpDate as c
	where i.CustomerID = c.CustomerID
	group by i.CustomerID, c.CustomerName
	order by c.CustomerName

drop table #stoneItems	
drop table #prItems
drop table #items

GO

