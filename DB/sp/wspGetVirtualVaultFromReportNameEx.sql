USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[wspGetVirtualVaultFromReportNameEx]    Script Date: 08/06/2015 12:29:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wspGetVirtualVaultFromReportNameEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wspGetVirtualVaultFromReportNameEx]
GO


/****** Object:  StoredProcedure [dbo].[wspGetVirtualVaultFromReportNameEx]    Script Date: 08/06/2015 12:29:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[wspGetVirtualVaultFromReportNameEx] ---94, 06285, 1, 15, 'h'
(
	@MeasureId	    int,
	@OrderCode	    int,
	@BatchCode	    int,
	@ItemCode	    int
)
AS

DECLARE 
@BatchID INT,
@PrevItemCode INT,
@ItemTypeID INT,
@PartID INT

--Test
--DECLARE
--	@MeasureId	    int,
--	@OrderCode	    int,
--	@BatchCode	    int,
--	@ItemCode	    int,
--	@OperationChar	    varchar(1)	
--SET @MeasureId = 94
--SET	@OrderCode = 05013
--SET @BatchCode = 1
--SET	@ItemCode = 1

SELECT	@BatchID = BatchID, @PrevItemCode = PrevItemCode, @PartID = PartID
FROM	v0Item i, v0Part p
WHERE	i.OrderCode = @OrderCode
and		i.BatchCode = @BatchCode
and		i.ItemCode  = @ItemCode
and		p.PartTypeID = 15
and		p.ItemTypeID = i.ItemTypeID

SELECT ISNULL(LTRIM(RTRIM(dbo.GetPartMeasureByMeasureID(@BatchID, @PrevItemCode, @PartID, @MeasureId))),'')
GO

