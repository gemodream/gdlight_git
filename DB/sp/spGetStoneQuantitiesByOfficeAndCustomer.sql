USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStoneQuantitiesByOfficeAndCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStoneQuantitiesByOfficeAndCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetStoneQuantitiesByOfficeAndCustomer]
(
	@DateFrom date = null,
	@DateTo date = null,
	@OfficeId int,
	@OrderStateCode int = null
)
AS -- 5103
	-- Acron Customer IDs = 430,448,570, Customer Codes = 1410, 1412, 1433
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item))
	declare @dateEnd date = ISNULL(@DateTo, GetDate())
	create table #stones(cnt int, CustomerId int)
	IF (@OfficeId = -1)
	BEGIN
		INSERT INTO #stones(cnt, CustomerId)
		SELECT count(*) as cnt, i.CustomerID
		--into #stones
		from v0Item As i
		where 1=1
			and (cast(i.OrderDate as DATE) between @dateBeg and @dateEnd) 
			and i.BatchID = i.NewBatchID
			and i.CustomerID in (430,448,570)
			and (@OrderStateCode IS NULL OR @OrderStateCode = i.OrderStateCode)
		group by i.CustomerID
	END
	ELSE
	BEGIN
		INSERT INTO #stones(cnt, CustomerId)
		SELECT count(*) as cnt, i.CustomerID
		--into #stones
		from v0Item As i
		where 1=1
			and (cast(i.OrderDate as DATE) between @dateBeg and @dateEnd) 
			and i.BatchID = i.NewBatchID
			and i.CustomerID not in (430,448,570)
			and i.CustomerOfficeID = @OfficeId
			and (@OrderStateCode IS NULL OR @OrderStateCode = i.OrderStateCode)
		group by i.CustomerID
	END
	select q.*, c.CustomerName, c.CompanyName from #stones as q, v0Customer as c
		where q.CustomerID = c.CustomerId
		order by c.CustomerName
	--select sum(cnt) as totals from #stones
	drop table #stones
GO

