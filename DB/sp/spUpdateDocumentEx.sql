--USE [GemoDream16]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateDocumentEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateDocumentEx]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsertDocumentEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsertDocumentEx]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'DocumentValueTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[DocumentValueTableType]
GO
CREATE TYPE [dbo].[DocumentValueTableType] AS TABLE(
	[DocumentId] [dbo].[dnID] NOT NULL,
	[Title] nvarchar(1000) NOT NULL,
	[Value] nvarchar(2000) NOT NULL,
	[Unit] nvarchar(200) NULL
)
GO


SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[spUpdateDocumentEx] (
 @DocumentID dnID,
 @DocumentName  varchar(250),
 @BarCodeFixedText  varchar(2000),
 @UseDate  dnBool,
 @UseVirtualVaultNumber  dnBool,
 @CorelFile varchar(1000),
 @AuthorID dnSmallID,
 @AuthorOfficeID dnTinyID,
 @ExportTypeID int,
 @ImportTypeID int,
 @FormatTypeID int,
 @DocumentValues [dbo].[DocumentValueTableType] READONLY
 )
AS
begin transaction;

begin
-- 1. spUpdateDocument
update tblDocument
set
	BarCodeFixedText = @BarCodeFixedText,
	UseDate = @UseDate,
	UseVirtualVaultNumber = @UseVirtualVaultNumber,
	CorelFile = @CorelFile,
	AuthorID = @AuthorID,
	AuthorOfficeID = @AuthorOfficeID,
 	ExportTypeID = @ExportTypeID,
 	ImportTypeID = @ImportTypeID,
 	FormatTypeID = @FormatTypeID
where
DocumentID=@DocumentID

-- 2. spDeleteDocumentValue
delete from tblDocumentValue
	where @DocumentID = DocumentID
	
-- 3. spSetDocumentValue
insert into tblDocumentValue
(
 DocumentID,
 Title,
 Value,
 Unit
)
select
 DocumentID,
 Title,
 Value,
 Unit
from @DocumentValues
end;	
commit transaction;
GO

