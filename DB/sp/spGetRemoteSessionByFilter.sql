USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRemoteSessionByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRemoteSessionByFilter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetRemoteSessionByFilter]
(
	@DateFrom date = null,
	@DateTo date = null,
	@UserId int = null,
	@PageNo int = 1,
	@PageSize int = 100
)
AS
	DECLARE @Start INT = (@PageNo - 1) * @PageSize + 1;
	DECLARE @End INT = @PageNo * @PageSize;
	DECLARE @dateBeg date = ISNULL(@DateFrom, (select min(LoginDate) from tblRemoteSession))
	DECLARE @dateEnd date = ISNULL(@DateTo, GetDate())

	create table #ids(id int)
	insert into #ids(id)
		select id FROM
		(
			SELECT s.id, ROW_NUMBER() OVER (ORDER BY s.LoginDate desc) AS RowNumber
				FROM tblRemoteSession s
				WHERE cast(s.LoginDate as DATE) between @dateBeg and @dateEnd
					and (@UserId IS NULL OR s.UserId = @UserId)
		) AS PostQuery
		where RowNumber between @Start and @End	
	
	SELECT s.*, v0User.FirstName, v0User.LastName, v0User.Login
		FROM #ids As i INNER JOIN  tblRemoteSession As s ON i.id = s.Id
			INNER JOIN v0User ON s.UserId = v0User.UserID
		ORDER BY s.LoginDate desc
	declare @rowsCount int = 
	(
		SELECT COUNT(Id) FROM tblRemoteSession 
			WHERE cast(LoginDate as DATE) between @dateBeg and @dateEnd
				and (@UserId IS NULL OR UserId = @UserId)
	)
	select ceiling(@rowsCount/@PageSize) as pagesCount, @rowsCount as rowsCount
	drop table #ids

GO

