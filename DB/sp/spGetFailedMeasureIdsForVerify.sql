USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFailedMeasureIdsForVerify]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFailedMeasureIdsForVerify]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetFailedMeasureIdsForVerify]
AS
select UPPER(LEFT(MeasureName, 1)) as grp, MeasureID, MeasureName from tblMeasure 
	where MeasureClass in (1, 3) 
		and MeasureID <> 65
	order by MeasureName
GO

