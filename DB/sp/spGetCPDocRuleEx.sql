USE [GemoDream16]
GO
-- spGetCustomerProgramByBatchID, spGetCPDocs, spGetCPDocRule

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCPDocRuleEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCPDocRuleEx]
GO
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spGetCPDocRuleEx](
@GroupCode int
) AS

select v.OrderCode as NewOrderCode,v1.BatchID,v1.NewBatchID
into #Items
from 	v0Item v,v0Item v1 where 1 = 2

insert into #Items
select 
v.OrderCode as NewOrderCode, v1.BatchID, v1.NewBatchID
from 	v0Item v,v0Item v1
where @GroupCode = v1.GroupCode 
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode

select 	b.* into #Batch from v0Batch b
WHERE 
  b.BatchID in (select BatchID from #Items where BatchID = NewBatchID and (@GroupCode = NewOrderCode  or (@GroupCode is null))) 


select b.BatchID, r.PartID, r.CPDocID, r.MeasureID,  r.MaxMeasure as MeasureValue, mp.PartName, m.MeasureTitle, m.MeasureCode, m.MeasureClass
from 
	v0CustomerProgram p, v0Batch vb, #Batch b,
	v0CPDoc d, v0CPDocRule r, v0Part mp, tblMeasure m 
where 
	vb.BatchID = b.BatchID and
	p.CPID = vb.CPID and 
	p.CPOfficeID = vb.CPOfficeID and 
	
	d.CustomerProgramHistoryID = p.CustomerProgramHistoryID and
	r.CPDocID = d.CPDocID and
	r.IsDefaultMeasureValue = 1 and
	mp.PartID = r.PartID and
	m.MeasureID = r.MeasureID
GO

