USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[spGetShapes]    Script Date: 01/14/2015 18:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[spGetShapes]
@AuthorID dnSmallID,@AuthorOfficeID dnTinyID
AS
BEGIN
/*
SELECT
      t.ShapeID as ShapeID,
      t.ShapeGroupID as ShapeGroupID,
      h.Path2Drawing as Path2Drawing,
      t.ShapeCode as ShapeCode
FROM tblShape t, hstShape h
WHERE t.ShapeHistoryID=h.ShapeHistoryID and t.ExpireDate is null
*/
select t.ShapeID,
	   coalesce(t.FixShapeGroupID, t.ShapeGroupID) ShapeGroupID, 
	   t.Path2Drawing,
	   t.ShapeCode from v0ShapeHierarchy t
	   order by 2, 1
END
