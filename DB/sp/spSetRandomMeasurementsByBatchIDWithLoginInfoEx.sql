--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfo]    Script Date: 05/08/2015 00:11:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfoEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfoEx]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfoEx](
	@BatchItemList [dbo].[BatchItemModelTableType]  READONLY,
	@PartID int,
	@StartValueMax decimal(6,4),
	@EndValueMax decimal(6,4),
	@StartValueMin decimal(6,4),
	@EndValueMin decimal(6,4),
	@StartValueH_x decimal(6,4),
	@EndValueH_x decimal(6,4),
	@AuthorID int,
	@AuthorOfficeID int,
	@CurrentOfficeID int
)

as

select GetDate()

Declare @NewItemCode int
Declare @NewBatchID  int
Declare @ItemTypeID int
Declare @rID varchar(255)

select v0Item.* into #ItemList from v0Item, @BatchItemList l where v0Item.BatchID = l.BatchId and v0Item.ItemCode = l.ItemCode 


Declare items_cursor Cursor
for
	select NewBatchID, NewItemCode, ItemTypeID from #ItemList 
	--where #ItemList.BatchID = @BatchID 
	order by newBatchID, newItemCode

open items_cursor

	fetch next from items_cursor into @NewBatchID, @NewItemCode, @ItemTypeID 

	WHILE @@FETCH_STATUS = 0 
	begin
		
		declare @Max decimal(6,4)
		declare @Min decimal(6,4)
		declare @H_x decimal(6,4)

-- @Max
		exec ppGetRandomDecimal
			@Result = 	@Max output,
			@ItemCode = 	@NewItemCode,
			@BatchID = 	@NewBatchID,
			@ItemTypeID = 	@ItemTypeID,
			@StartValue = 	@StartValueMax,
			@EndValue = 	@EndValueMax
-- @Min	
		exec ppGetRandomDecimal
			@Result = 	@Min output,
			@ItemCode = 	@NewItemCode,
			@BatchID = 	@NewBatchID,
			@ItemTypeID = 	@ItemTypeID,
			@StartValue = 	@StartValueMin,
			@EndValue = 	@EndValueMin

		while(@Min>@Max)
		begin
			exec ppGetRandomDecimal
				@Result = 	@Min output,
				@ItemCode = 	@NewItemCode,
				@BatchID = 	@NewBatchID,
				@ItemTypeID = 	@ItemTypeID,
				@StartValue = 	@StartValueMin,
				@EndValue = 	@EndValueMin
		end

-- @H_x 	
		exec ppGetRandomDecimal
			@Result = 	@H_x output,
			@ItemCode = 	@NewItemCode,
			@BatchID = 	@NewBatchID,
			@ItemTypeID = 	@ItemTypeID,
			@StartValue = 	@StartValueH_x,
			@EndValue = 	@EndValueH_x


/*
		INSERT INTO  #MeasureData
		SELECT	@Max, 11, 'Max'

		INSERT INTO  #MeasureData
		SELECT	@Min, 12, 'Min'

		INSERT INTO  #MeasureData
		SELECT	@H_x, 13, 'H_x'
*/
--@set @Max 11, 'Max'
     		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@NewItemCode,
			@BatchID=@NewBatchID, 
			@PartID=@PartID, 
			@MeasureCode= 11, --Max
			@MeasureValue= @Max,
			@MeasureValueID= null,
			@StringValue= null,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, @AuthorOfficeID=@AuthorOfficeID 

--@set @Min, 12, 'Min'
     		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@NewItemCode,
			@BatchID=@NewBatchID, 
			@PartID=@PartID, 
			@MeasureCode= 12, --Max
			@MeasureValue= @Min,
			@MeasureValueID= null,
			@StringValue= null,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, @AuthorOfficeID=@AuthorOfficeID 

--@st @H_x, 13, 'H_x'
     		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@NewItemCode,
			@BatchID=@NewBatchID, 
			@PartID=@PartID, 
			@MeasureCode= 13, --Max
			@MeasureValue= @H_x,
			@MeasureValueID= null,
			@StringValue= null,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, @AuthorOfficeID=@AuthorOfficeID 

		fetch next from items_cursor into @NewBatchID, @NewItemCode, @ItemTypeID 
	end



close items_cursor
deallocate items_cursor








GO

