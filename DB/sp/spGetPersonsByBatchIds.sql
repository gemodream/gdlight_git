USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPersonsByBatchIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPersonsByBatchIds]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetPersonsByBatchIds]
(
	@BatchIds [dbo].[intTableType] READONLY
)
as
	SELECT p.FirstName, p.LastName, p.Email, p.PersonID 
		FROM dbo.v0Person as p, 
		(
			SELECT v.CustomerID, v.CustomerOfficeID
			FROM dbo.v0Batch as v, @BatchIds as p 
			WHERE v.BatchID=p.ID
		
		) as  c
		WHERE 
			p.CustomerID = c.CustomerID 
			and p.CustomerOfficeID = c.CustomerOfficeID
		GROUP BY p.FirstName, p.LastName, p.Email, p.PersonID

GO

