--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_EnqueueLeoList]    Script Date: 03/19/2015 07:36:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EnqueueLeoList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EnqueueLeoList]
GO

/****** Object:  StoredProcedure [dbo].[sp_EnqueueLeoList]    Script Date: 03/19/2015 07:36:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_EnqueueLeoList]
(
@ItemNumbers [dbo].[itemModelTableType]  READONLY
)
AS
BEGIN
--number generator
Declare @CurrDate DateTime
set @CurrDate = GetDate()
Declare @SetID varchar(255)
select @SetID = cast(cast(CAST(@CurrDate AS BINARY(8)) as bigint) as varchar(255))
insert tblLeoQueue (ItemNumber, Status, SetID)
select i.Item, 'Q', @SetID 
from @ItemNumbers i

END

GO

