USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReportTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReportTotals]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetStatsReportTotals]
(
	@ReportId int
)
As
	-- Report TOTALS
create table #totals (stone_qty int default 0, pass_qty int default 0, fail_qty int default 0, miss_qty int default 0)
-- Items Quantity
INSERT INTO #totals (stone_qty)
	SELECT count(i.item_id)
		FROM statsReportItem i
		WHERE i.report_id = @ReportId

SELECT i.item_id, d.rejection_type into #custItemType 
	FROM statsReportItem i, statsReportItemDetails d
	WHERE i.item_id = d.item_id
		AND i.report_id = @ReportId
	GROUP BY i.item_id, d.rejection_type
		
	-- fail
update #totals set fail_qty = b.fail_qty
	from #totals t, 
	(	
		select count(a.item_id) as fail_qty 
			from #custItemType as a
			where a.rejection_type = 2 
	) b 

	-- missing
update #totals set miss_qty = b.miss_qty
	from #totals t, 
	(	
		select count(a.item_id) as miss_qty 
			from #custItemType as a
			where a.rejection_type = 3 
				
	) b 
	-- pass
update #totals set pass_qty = b.pass_qty
	from #totals t,
	(	
		select count(a.item_id) as pass_qty from 
		(
			select item_id, 
				sum(CASE rejection_type WHEN 2 THEN 1 ELSE 0 END) as fail_cnt,
				sum(CASE rejection_type WHEN 3 THEN 1 ELSE 0 END) as miss_cnt
				from #custItemType
				group by item_id
		)a 
		where a.fail_cnt = 0 and a.miss_cnt = 0 
		
	) b 
select * from #totals
	
drop table #totals
drop table #custItemType
	
	
	-- Failed details
	select a.reason_id, count(*) as fail_qty, r.ReasonName from 
	(
		select d.reason_id, min(i.item_id) as item_id 
			from statsReportItem i, statsReportItemDetails d
			where 1=1
				and i.report_id = @ReportId
				and i.item_id = d.item_id
				and d.rejection_type = 2
			group by d.reason_id, i.item_id
	) a, tblRejectReason r
	where a.reason_id = r.ReasonID	
	group by a.reason_id, r.ReasonName

	-- Missing details
	select a.reason_id, count(*) as miss_qty, r.ReasonName from 
	(
		select d.reason_id, min(i.item_id) as item_id 
			from statsReportItem i, statsReportItemDetails d
			where 1=1
				and i.report_id = @ReportId
				and i.item_id = d.item_id
				and d.rejection_type = 3
			group by d.reason_id, i.item_id
	) a, tblRejectReason r
	where a.reason_id = r.ReasonID	
	group by a.reason_id, r.ReasonName
GO

