USE [GemoDream16]
GO
-- logic from spGetPrintingHistory.sql
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportQuantityByDocumentType1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportQuantityByDocumentType1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetReportQuantityByDocumentType1]
(
	@DateFrom as datetime = null,
	@DateTo as datetime = null,
	@DocumentType int = null
)
AS
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item))
	declare @dateEnd date = ISNULL(@DateTo, GetDate())

select i.BatchID, i.ItemCode into #items
		from 
			v0Item As i
		where 1=1
			and ((CAST(i.OrderDate as Date) between @dateBeg and @dateEnd))
			and i.BatchID = i.NewBatchID
			and i.CustomerOfficeID <> 3	
			and i.OrderStateCode = 1
	
	select  i.BatchID, i.ItemCode, doc.DocumentTypeCode into #items2  
		from 
			#items As i,
			tblItemOperation As tio,
			hstItemOperation As h,
			tblDocument As doc,
			tblPrintingQueue As q
		where 1=1
			
			and i.BatchID = tio.BatchID 
			and i.ItemCode = tio.ItemCode
			
			and tio.ItemOperationHistoryID = h.ItemOperationHistoryID
			
			and tio.OperationTypeID = doc.OperationTypeID
			and doc.DocumentTypeCode not in (8,10,19,20)
			
			and q.ItemOperationID = h.ItemOperationID
			and q.State in ('Y', 'C')
			-- and CAST(q.PrintDate as date) between @dateBeg and @dateEnd
		group by i.BatchID, i.ItemCode, doc.DocumentTypeCode		
select count(v.ItemCode) as quantity, dt.DocumentTypeName 
	from #items2 v, tblDocumentType dt
	where v.DocumentTypeCode = dt.DocumentTypeCode
	group by dt.DocumentTypeName

drop table #items
drop table #items2

GO

