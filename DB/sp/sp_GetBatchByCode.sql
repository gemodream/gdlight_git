--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_GetBatchByCode]    Script Date: 04/22/2015 19:24:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBatchByCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBatchByCode]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetBatchByCode]    Script Date: 04/22/2015 19:24:10 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE     PROCEDURE [dbo].[sp_GetBatchByCode]
(
--declare
 @CustomerCode dnCode,
 @BGroupState dnTinyCode,
 @EGroupState dnTinyCode,
 @BState dnTinyCode,
 @EState dnTinyCode,
 @BDate ddStartDate,
 @EDate ddStartDate,
 @GroupCode int, -- update for new numbers
 @BatchCode dnBatchCode,
 @AuthorID dnSmallID,
 @AuthorOfficeID dnTinyID

)

AS

DECLARE @vsql nvarchar(max)
/*
--Set block below for test only
SET	@CustomerCode = 1141
SET	@BGroupState = 2
SET	@EGroupState = null
SET	@BState = null
SET	@EState = null
SET	@BDate = null
SET	@EDate = null
SET	@GroupCode = null
SET	@BatchCode = null
SET	@AuthorID = 19
SET	@AuthorOfficeID = 1
*/
select
v.OrderCode as NewOrderCode,v1.BatchID,v1.NewBatchID
into #Items
from 	v0Item v,v0Item v1
where 1 = 2
/*
select @vsql  = N'
insert into #Items
select 
v.CreateDate,v.LastModifiedDate,v.LotNumber,v.Weight,v.CustomerItemWeight,v.WeightUnitID,
v.CustomerItemWeightUnitID,v.StateID,v.StateTargetID,v.OrderCode as NewOrderCode,v.BatchCode as NewBatchCode,
v.ItemComment,v.ItemTypeID,v.StateCode,v.StateName,v.IconIndex,v.StateTargetCode,v.ItemHistoryID,
v.OrderDate,v.OrderStateCode,
v1.PrevItemCode,v1.PrevBatchCode,v1.PrevGroupCode,v1.PrevOrderCode,v1.BatchID_ItemCode,v1.BatchID,v1.ItemCode,
v1.GroupCode,v1.OrderCode,v1.BatchCode,v1.CustomerOfficeID,v1.CustomerID,v1.CustomerCode,v1.NewBatchID,
v1.NewItemCode,
c.Color, c.Clarity, c.Shape, c.KM, c.LD, mu.MeasureUnitName as  WeightUnitName
from 	v0Item v,v0Item v1, (select Color, Clarity, Shape, KM, LD, BatchID, ItemCode from tblItem)  c, tblMeasureUnit mu
where 1 = 1 ' 
+ case when @GroupCode is not null then ' and @GroupCode = v1.GroupCode ' else '' end
+ case when @BatchCode is not null then ' and @BatchCode = v1.BatchCode ' else '' end
+ case when @CustomerCode is not null then ' and @CustomerCode = v1.CustomerCode ' else '' end
+ case when @BDate is not null then ' and v1.OrderDate >= @BDate ' else '' end
+ case when @EDate is not null then ' and v1.OrderDate <= @EDate ' else '' end
+ case when @BGroupState is not null then ' and v1.OrderState >= @BGroupState ' else '' end
+ case when @EGroupState is not null then ' and v1.OrderState <= @EGroupState ' else '' end
+ case when @BState is not null then ' and v1.StateCode >= @BState ' else '' end
+ case when @EState is not null then ' and v1.StateCode <= @EState ' else '' end
+ ' and v.ItemCode=c.ItemCode and  v.BatchID=c.BatchID
 and (mu.MeasureUnitID = v.WeightUnitID or mu.MeasureUnitID = v1.WeightUnitID)
 and v1.CustomerOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode '
*/
/*
(
@GroupCode=v1.GroupCode or (@GroupCode is null))
 and (v1.BatchCode=@BatchCode or (@BatchCode is null))
 --and (v1.ItemCode=@ItemCode  or (@ItemCode is null))
 and ((@CustomerCode=v1.CustomerCode  or (@CustomerCode is null))
 and (v1.OrderDate>=@BDate or (@BDate is null))
 and (v1.OrderDate<=@EDate or (@EDate is null))
 and (v1.OrderStateCode>=@BGroupState or (@BGroupState is null))
 and (v1.OrderStateCode<=@EGroupState or (@EGroupState is null))
 and (v1.StateCode>=@BState or (@BState is null))
 and (v1.StateCode<=@EState or (@EState is null))
 and v.ItemCode=c.ItemCode and  v.BatchID=c.BatchID
 and (mu.MeasureUnitID = v.WeightUnitID or mu.MeasureUnitID = v1.WeightUnitID)
 and v1.CustomerOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode
-- v1.BatchID = v1.NewBatchID
-- and @GroupCode = v.OrderCode
)
*/

select @vsql  = N'
insert into #Items
select 
v.OrderCode as NewOrderCode, v1.BatchID, v1.NewBatchID
from 	v0Item v,v0Item v1
where 1 = 1 ' 
+ case when @GroupCode is not null then ' and @GroupCode = v1.GroupCode ' else '' end
+ case when @BatchCode is not null then ' and @BatchCode = v1.BatchCode ' else '' end
+ case when @CustomerCode is not null then ' and @CustomerCode = v1.CustomerCode ' else '' end
+ case when @BDate is not null then ' and v1.OrderDate >= @BDate ' else '' end
+ case when @EDate is not null then ' and v1.OrderDate <= @EDate ' else '' end
+ case when @BGroupState is not null then ' and v1.OrderState >= @BGroupState ' else '' end
+ case when @EGroupState is not null then ' and v1.OrderState <= @EGroupState ' else '' end
+ case when @BState is not null then ' and v1.StateCode >= @BState ' else '' end
+ case when @EState is not null then ' and v1.StateCode <= @EState ' else '' end
+ ' and v1.CustomerOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode '

/*
(
@GroupCode=v1.GroupCode or (@GroupCode is null))
 and (v1.BatchCode=@BatchCode or (@BatchCode is null))
 --and (v1.ItemCode=@ItemCode  or (@ItemCode is null))
 and ((@CustomerCode=v1.CustomerCode  or (@CustomerCode is null))
 and (v1.OrderDate>=@BDate or (@BDate is null))
 and (v1.OrderDate<=@EDate or (@EDate is null))
 and (v1.OrderStateCode>=@BGroupState or (@BGroupState is null))
 and (v1.OrderStateCode<=@EGroupState or (@EGroupState is null))
 and (v1.StateCode>=@BState or (@BState is null))
 and (v1.StateCode<=@EState or (@EState is null))
 and v.ItemCode=c.ItemCode and  v.BatchID=c.BatchID
 and (mu.MeasureUnitID = v.WeightUnitID or mu.MeasureUnitID = v1.WeightUnitID)
 and v1.CustomerOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode
-- v1.BatchID = v1.NewBatchID
-- and @GroupCode = v.OrderCode
)
*/

execute sp_executeSQL @vsql,  N'@CustomerCode dnCode,
 @BGroupState dnTinyCode,
 @EGroupState dnTinyCode,
 @BState dnTinyCode,
 @EState dnTinyCode,
 @BDate ddStartDate,
 @EDate ddStartDate,
 @GroupCode int,   -- update for new numbers
 @BatchCode dnBatchCode,
 @AuthorID dnSmallID,
 @AuthorOfficeID dnTinyID',
 @CustomerCode = @CustomerCode,
 @BGroupState = @BGroupState,
 @EGroupState = @EGroupState,
 @BState = @BState,
 @EState = @EState,
 @BDate = @BDate,
 @EDate = @EDate,
 @GroupCode = @GroupCode,
 @BatchCode = @BatchCode,
 @AuthorID = @AuthorID,
 @AuthorOfficeID = @AuthorOfficeID

/*
select distinct Batchid
into 	#Batch
from 	#Items
where 	BatchID = NewBatchID 
and	(@GroupCode = NewOrderCode  or (@GroupCode is null))
*/

select 	b.* from v0Batch b

where	b.Batchid in (select BatchID from #Items where BatchID = NewBatchID and (@GroupCode = NewOrderCode  or (@GroupCode is null)))
order 	by b.BatchID


--drop table #Batch, #Items

GO

