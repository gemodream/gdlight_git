USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[sp_AddItemsBatchFromList]    Script Date: 03/12/2015 10:02:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--drop table #LotNumbers
ALTER     procedure [dbo].[sp_AddItemsBatchFromList](
	@OrderNumber int,
	@CustomerProgramName varchar(250),
	@ItemNumber int, 
	@MemoNumber varchar(500),
	@MemoNumberID int,
	@AuthorId int, 
	@AuthorOfficeId int, 
	@CurrentOfficeId int,
	@GroupOfficeID int,
	@CPOfficeID int,
	@LotNumberList nText
)
as 

-- begin transaction

DECLARE @rId varchar(150)
DECLARE @GroupID int
DECLARE @InspectedQuantity int
DECLARE @InspectedTotalWeight numeric(9,4)
DECLARE @InspectedWeightUnitID numeric(4,0)
DECLARE @VendorOfficeID numeric(4,0)
DECLARE @VendorID numeric(8,0)


DECLARE @BatchCode int
DECLARE @ItemTypeID int
DECLARE @ItemsQuantity int
DECLARE @ItemsWeight numeric(9,4)
DECLARE @CPID int
DECLARE @BatchID int
DECLARE @CreateDate datetime
DECLARE @StoredItemsQuantity numeric(2,0)
DECLARE @StateID numeric(4,0)
DECLARE @StateTargetID numeric(4,0)

DECLARE @LotNumber varchar(250)
DECLARE @Weight numeric(9,4)
DECLARE @WeightUnitID numeric(4,0)
DECLARE @CustomerItemWeight numeric(9,4)
DECLARE @CustomerItemWeightUnitID numeric(4,0)
DECLARE @PrevItemCode numeric(2,0)
DECLARE @PrevBatchCode numeric(3,0)
DECLARE @PrevGroupCode int
DECLARE @PrevOrderCode int
DECLARE @ItemComment varchar(2000)
DECLARE @ItemCode numeric(2,0)

DECLARE @Price decimal(19,4)
DECLARE @ViewAccessCode numeric(4,0)

CREATE TABLE #Batches
	(
	BatchID int NULL
	)

SET @WeightUnitID=2
SET @CustomerItemWeightUnitID=2
	
SELECT @InspectedQuantity = ItemsQuantity, 
	@GroupID = GroupID FROM tblGroup
WHERE groupcode = @OrderNumber

--declare @hDoc int

--exec sp_xml_preparedocument @hDoc OUTPUT, @LotNumberList

CREATE TABLE #LotNumbers
	(
		LID int,
		LotNumber varchar(255)
	)  

--exec sp_xml_preparedocument @hDoc OUTPUT, @LotNumberList
declare @xml xml

select @xml = convert(xml,@LotNumberList)

insert into #LotNumbers (LotNumber, LID)

select xml_col.col.value('@LotNumber','varchar(255)') as Name
,xml_Col.col.value('@LID','int') as Value
from @xml.nodes('/LotNumberSet/LotNumberSetTable') as xml_col(col)


--insert into #LotNumbers
--select LID, LotNumber from OPENXML(@hDoc, '/LotNumberSet/LotNumberSetTable', 1)
--			with(LID int, LotNumber varchar(255))

--exec sp_xml_removedocument @hDoc

-- select * from #LotNumbers

select @ItemNumber=count(*) from #LotNumbers

IF @InspectedQuantity IS null
	SET @InspectedQuantity = @ItemNumber
ELSE
	SET @InspectedQuantity = @InspectedQuantity + @ItemNumber

select @VendorOfficeID = VendorOfficeID, @VendorID = VendorID, 
	@InspectedTotalWeight = InspectedTotalWeight, @InspectedWeightUnitID = InspectedWeightUnitID
	 from hstGroup where GroupID = @GroupID and GroupHistoryID=(select GroupHistoryID from tblGroup where GroupID = @GroupID)

EXEC spItemizingUpdateGroup @rId OUTPUT , @GroupOfficeID, @GroupID, @InspectedQuantity, @InspectedTotalWeight, @InspectedWeightUnitID, @VendorOfficeID, @VendorID, @CurrentOfficeID, @AuthorID, @AuthorOfficeID

SELECT TOP 1 @CPID = CPID, @ItemTypeID = ItemTypeID 
	FROM v1CustomerProgram 
	WHERE 
		(NOT(IsCopy=1) or IsCopy IS NULL) 
		AND CustomerProgramName=@CustomerProgramName
		and customerid = (select top 1 customerid from v0group where ordercode = @OrderNumber)

WHILE @ItemNumber > 0
BEGIN
	IF @ItemNumber >= 25
	BEGIN
		SET @StoredItemsQuantity = 25
		SET @ItemNumber = @ItemNumber - 25
	END
	ELSE
	BEGIN
		SET @StoredItemsQuantity = @ItemNumber
		SET @ItemNumber = 0
	END
	
	SET @BatchID = null
	EXEC spAddBatch @rId OUTPUT , @GroupOfficeID, @GroupID, @BatchCode, @ItemTypeID, @ItemsQuantity, @ItemsWeight, @CPOfficeID, @CPID, @BatchID, @CreateDate, @AuthorID, @AuthorOfficeID, @StoredItemsQuantity, @StateID, @StateTargetID, @CurrentOfficeID, @MemoNumber, @MemoNumberID
	SET @BatchID = @rId

	insert into #Batches 
		(BatchID) values (@BatchID)
	
	WHILE @StoredItemsQuantity > 0
	BEGIN
--		SET @ItemCode = @StoredItemsQuantity 
		SET @ItemCode = NULL
		SET @StoredItemsQuantity = @StoredItemsQuantity - 1
		SET @Price = -2
		SET @ViewAccessCode = 2
		Declare @LotNumberID int;
		SET @LotNumberID = (Select min(LID) from #LotNumbers)
		Select @LotNumber = LotNumber from #LotNumbers where LID = @LotNumberID
		delete from #LotNumbers where LID = @LotNumberID
		Declare @ParNo dsName = null -- new parameter for the spAddItem 
		EXEC spAddItem @rId OUTPUT , 
				@CreateDate, 
				@AuthorID, 
				@AuthorOfficeID, 
				@LotNumber, 
				@ParNo,
				@Weight, 
				@WeightUnitID, 
				@CustomerItemWeight, 
				@CustomerItemWeightUnitID, 
				@StateID, 
				@StateTargetID, 
				@PrevItemCode, 
				@PrevBatchCode, 
				@PrevGroupCode, 
				@PrevOrderCode, 
				@ItemComment, 
				@BatchID, 
				@ItemCode, 
				@CurrentOfficeID
		SET @ItemCode= SUBSTRING(@rID,CHARINDEX('_',@rID)+1,LEN(@rID)-CHARINDEX('_',@rID))
		EXEC spAddInvoice @rId OUTPUT , @Price, @ViewAccessCode, @GroupOfficeID, @GroupID, @BatchID, @ItemCode, @CurrentOfficeID, @AuthorID, @AuthorOfficeID
	END
	EXEC spSetPrefilledMeasuresFromCP @rId OUTPUT , @AuthorID, @AuthorOfficeID, @CurrentOfficeID, @BatchID	
END


-- commit transaction

select 	vi.GroupCode,
	vi.BatchCode,
	vi.ItemCode,
	vi.LotNumber 
from 
	v0item vi,
	#Batches
where 
	#Batches.BatchID = vi.BatchID
order by
	vi.GroupCode,
	vi.BatchCode,
	vi.ItemCode
