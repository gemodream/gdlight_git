USE [GemoDream16]
GO

/****** Object:  UserDefinedTableType [dbo].[intTableType]    Script Date: 03/09/2015 00:15:57 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'intTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[intTableType]
GO

USE [GemoDream16]
GO

/****** Object:  UserDefinedTableType [dbo].[intTableType]    Script Date: 03/09/2015 00:15:57 ******/
CREATE TYPE [dbo].[intTableType] AS TABLE(
	[ID] [dbo].[dnID] NOT NULL
)
GO

