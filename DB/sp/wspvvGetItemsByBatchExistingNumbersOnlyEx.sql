--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[wspvvGetItemsByBatchExistingNumbersOnly]    Script Date: 04/22/2015 21:35:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wspvvGetItemsByBatchExistingNumbersOnlyEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wspvvGetItemsByBatchExistingNumbersOnlyEx]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE    PROCEDURE [dbo].[wspvvGetItemsByBatchExistingNumbersOnlyEx] 
(
	@BatchID integer)
AS
BEGIN
select i.ItemCode,it.ItemTypeName, b.MemoNumber,i.LotNumber,b.BatchCode, i.orderCode, b.GroupID, i.BatchID, i.NewBatchID, i.NewItemCode, i.prevordercode, i.prevbatchcode, i.previtemcode
    into #item
	from v0Item i 
	inner join v0Batch b on i.BatchID=b.BatchID 
	inner join v0ItemType it on it.ItemTypeID=b.ItemTypeID 
	where @BatchID = b.BatchID
	  and @BatchID = i.BatchID
--  and i.batchid=i.NewBatchID

--order by i.prevordercode, i.prevbatchcode, i.previtemcode

select i.ItemCode,i.ItemTypeName, i.MemoNumber,i.LotNumber,i.BatchCode, i.orderCode, i.GroupID from #item i where i.BatchID = i.NewBatchID
order by i.prevordercode, i.prevbatchcode, i.previtemcode

select i.ItemCode,i.ItemTypeName, i.MemoNumber,i.LotNumber,i.BatchCode, i.orderCode, i.GroupID,
       i2.GroupCode newGroupCode, i2.BatchCode newBatchCode, i2.ItemCode newItemCode
	from #item i
  left outer join v0Item i2 on
    i.NewBatchID = i2.BatchID and i.NewItemCode = i2.ItemCode
	where i.batchid!=i.NewBatchID

order by i.prevordercode, i.prevbatchcode, i.previtemcode

END
GO

