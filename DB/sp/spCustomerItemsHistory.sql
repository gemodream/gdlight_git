USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCustomerItemsHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCustomerItemsHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE    PROCEDURE [dbo].[spCustomerItemsHistory]
(
 @CustomerId int,
 @OrderState int = null,
 @DateFrom date = null,
 @DateTo date = null,
 @GroupCode as int = null,
 @BatchCode as int = null,
 @ItemCode int = null,
 @IsNew int = 0
)  

AS
declare @startDate date, @endDate date
set @startDate = ISNULL(@DateFrom, GetDate())
set @endDate = ISNULL(@DateTo, GetDate())
select
	v.CreateDate, v.LastModifiedDate, v.LotNumber, v.Weight, v.CustomerItemWeight, v.WeightUnitID,
	v.CustomerItemWeightUnitID, v.StateID, v.StateTargetID, v.OrderCode as NewOrderCode, v.BatchCode as NewBatchCode,
	v.ItemComment, v.ItemTypeID, v.StateCode, v.StateName, v.IconIndex, v.StateTargetCode, v.ItemHistoryID,
	v.OrderDate, v.OrderStateCode,
	v1.PrevItemCode, v1.PrevBatchCode, v1.PrevGroupCode, v1.PrevOrderCode, v1.BatchID_ItemCode, v1.BatchID,v1.ItemCode,
	v1.GroupCode, v1.OrderCode, v1.BatchCode, v1.CustomerOfficeID, v1.CustomerID, v1.CustomerCode, v1.NewBatchID,
	v1.NewItemCode,
	c.Color, c.Clarity, c.Shape, c.KM, c.LD, mu.MeasureUnitName as  WeightUnitName

into 	#Items
from 	v0Item v,v0Item v1, (select Color, Clarity, Shape, KM, LD, BatchID, ItemCode from tblItem)  c, tblMeasureUnit mu
where 1=1
 and (@GroupCode IS NULL OR	@GroupCode = v1.GroupCode)
 and (@BatchCode  IS NULL OR v1.BatchCode = @BatchCode)
 and (@ItemCode IS NULL OR v1.ItemCode = @ItemCode)
 and (@CustomerID=v1.CustomerID)
 and (CAST(v1.OrderDate as date) between @startDate and @endDate)
 and (@OrderState IS NULL OR v1.OrderStateCode = @OrderState)
 and v.ItemCode = c.ItemCode and  v.BatchID = c.BatchID
 and (mu.MeasureUnitID = v.WeightUnitID or mu.MeasureUnitID = v1.WeightUnitID)
 --and v1.CustomerOfficeID in (Select OfficeID From tblUser_Office Where UserID = @AuthorID and UserOfficeID = @AuthorOfficeID)
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode
-- v1.BatchID = v1.NewBatchID
-- and @GroupCode = v.OrderCode


if ISNULL(@IsNew,0) <> 0
BEGIN
	select * from #Items
	order by NewBatchID, NewItemCode
END
ELSE
BEGIN
	select * from #Items
	where 	BatchID = NewBatchID and ItemCode = NewItemCode
	and	(@GroupCode = NewOrderCode  or (@GroupCode is null))
	order by NewBatchID, NewItemCode
END

GO

