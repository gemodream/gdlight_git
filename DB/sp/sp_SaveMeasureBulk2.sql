--USE [GemoDream16]
--GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSaveMeasureBulkEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSaveMeasureBulkEx]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SaveMeasureBulk2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SaveMeasureBulk2]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfoEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetRandomMeasurementsByBatchIDWithLoginInfoEx]
GO


IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'BatchItemModelTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[BatchItemModelTableType]
GO
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'UpdateItemModelTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[UpdateItemModelTableType]
GO

CREATE TYPE [dbo].[BatchItemModelTableType] AS TABLE(
	[BatchId] [dbo].[dnID] NOT NULL,
	[ItemCode] int NOT NULL
)
GO

CREATE TYPE [dbo].[UpdateItemModelTableType] AS TABLE(
	[SetPartId] int,
	[SetMeasureID] int,
	[SetMeasureValueID] int DEFAULT null,		-- enum value or
	[SetMeasureValue] float DEFAULT null,		-- numeric value or
	[SetStringValue] varchar(100) DEFAULT null	-- string value	
)
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[sp_SaveMeasureBulk2](

@AuthorID int,
@AuthorOfficeID int,
@CurrentOfficeID int,

@ItemTypeID int,
@PartId int,
@SetMeasureID int,
@SetMeasureValueID int = null,		-- enum value or
@SetMeasureValue float = null,		-- numeric value or
@SetStringValue varchar(100) = null,-- string value	

@BatchItemList [dbo].[BatchItemModelTableType]  READONLY
)

as 
	Declare @MeasureCode int
	Select @MeasureCode = MeasureCode 
	from
		tblMeasure 
	where
		MeasureID = @SetMeasureID

	CREATE TABLE #BatchIDTable
	(
		BatchID int,
		ItemCode int		
	)  

	insert into #BatchIDTable (BatchID, ItemCode) select BatchId, ItemCode from @BatchItemList
	BEGIN
		Declare myCursor Cursor
		for
		Select NewItemCode, NewBatchID 
		from
			v0Item vi,
			#BatchIDTable bt
		where
			vi.batchid = bt.batchid
			and vi.ItemCode = bt.ItemCode
			order by vi.groupcode, vi.batchcode, vi.itemcode

		Declare @ItemCode int
		Declare @BatchID int

		open myCursor
	
		Fetch Next From myCursor into @ItemCode, @BatchID
		While(@@FETCH_STATUS=0)
		BEGIN
			Declare @rID varchar(255)
      		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@ItemCode,
			@BatchID=@BatchID, 
			@PartID=@PartID, 
			@MeasureCode=@MeasureCode,
			@MeasureValue=@SetMeasureValue,
			@MeasureValueID=@SetMeasureValueID,
			@StringValue=@SetStringValue,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, @AuthorOfficeID=@AuthorOfficeID
			FETCH NEXT FROM myCursor INTO @ItemCode, @BatchID
		END
		CLOSE myCursor 
		DEALLOCATE myCursor 

	END

GO
CREATE  procedure [dbo].[spSaveMeasureBulkEx](

@AuthorID int,
@AuthorOfficeID int,
@CurrentOfficeID int,
@ItemTypeID int,
@BatchItemList [dbo].[BatchItemModelTableType]  READONLY,
@MeasureList [dbo].[UpdateItemModelTableType] READONLY
)

as 
BEGIN TRANSACTION;

	Declare @SetPartId int
	Declare @SetMeasureID int
	Declare @SetMeasureValueID int = null		-- enum value or
	Declare @SetMeasureValue float = null		-- numeric value or
	Declare @SetStringValue varchar(100) = null	-- string value	

BEGIN
	Declare myCursorEx Cursor for
		Select SetPartId, SetMeasureID, SetMeasureValueID, SetMeasureValue, SetStringValue
			from @MeasureList

	open myCursorEx
	
	Fetch Next from myCursorEx into @SetPartId, @SetMeasureID, @SetMeasureValueID, @SetMeasureValue, @SetStringValue
	While(@@FETCH_STATUS=0)
	BEGIN
		exec sp_SaveMeasureBulk2
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID,
		@CurrentOfficeID = @CurrentOfficeID,
		@ItemTypeID = @ItemTypeID,
		@PartId = @SetPartId,
		@SetMeasureID = @SetMeasureID,
		@SetMeasureValueID = @SetMeasureValueID,
		@SetMeasureValue = @SetMeasureValue,
		@SetStringValue = @SetStringValue,
		@BatchItemList = @BatchItemList
		Fetch Next from myCursorEx into @SetPartId, @SetMeasureID, @SetMeasureValueID, @SetMeasureValue, @SetStringValue
	END
	CLOSE myCursorEx 
	DEALLOCATE myCursorEx 

END

COMMIT TRANSACTION;
GO



