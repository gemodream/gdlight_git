USE [GemoDream16]
GO

-- DELETE spUpdateOrderEx
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateOrderEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateOrderEx]
GO

-- DELETE OrderUpdateTableType
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'OrderUpdateTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[OrderUpdateTableType]
GO

-- CREATE OrderUpdateTableType
CREATE TYPE [dbo].[OrderUpdateTableType] AS TABLE(
	[BatchId] [dbo].[dnID] NOT NULL,
	[ItemCode] int NOT NULL,
	[OperationTypeOfficeID] dnTinyID, 
	[OperationTypeID] dnSmallID
)
GO

-- DELETE BatchEventTableType
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'BatchEventTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[BatchEventTableType]
GO

-- CREATE BatchEventTableType
CREATE TYPE [dbo].[BatchEventTableType] AS TABLE(
	[BatchId] [dbo].[dnID] NOT NULL,
	[NumberOfItemsAffected] int NOT NULL,
	[NumberOfItemsInBatch] int NOT NULL, 
	[FormCode] int NOT NULL,
	[EventID] int NOT NULL
)
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

-- CREATE spUpdateOrderEx
CREATE   PROCEDURE [dbo].[spUpdateOrderEx]
(
	@AuthorID dnSmallID,
	@AuthorOfficeID dnTinyID,
	@UpdateItems [dbo].[OrderUpdateTableType] READONLY,
	@BatchEvents [dbo].[BatchEventTableType] READONLY
)  AS

declare @rId varchar(150)
declare @OperationTypeOfficeID dnTinyID 
declare @OperationTypeID dnSmallID
declare @BatchID dnID
declare @ItemCode dnItemCode
declare @Closed integer
declare @NumberOfItemsAffected int
declare @NumberOfItemsInBatch int
declare @FormCode int
declare @EventID int

begin transaction;
begin
	Declare updateItemsCursor Cursor for
		Select BatchId, ItemCode, OperationTypeOfficeID, OperationTypeID from @UpdateItems

	open updateItemsCursor
	Fetch Next from updateItemsCursor into @BatchID, @ItemCode, @OperationTypeOfficeID, @OperationTypeID
	While(@@FETCH_STATUS=0)
	BEGIN
		-- 1. spAddItemOperation
		EXEC [dbo].[spAddItemOperation]
			@rId  = @rId OUTPUT,
			@OperationTypeOfficeID = @OperationTypeOfficeID,
			@OperationTypeID = @OperationTypeID,
			@BatchID = @BatchID,
			@ItemCode = @ItemCode,
			@CurrentOfficeID = NULL,
			@AuthorID = @AuthorID,
			@AuthorOfficeID = @AuthorOfficeID
	
		-- 2. spGetCheckClossedRecheckSessionForItem
		EXEC @Closed = [dbo].[spGetCheckClossedRecheckSessionForItem]	
			@BatchID = @BatchID,
			@ItemCode = @ItemCode,
			@AuthorID = @AuthorID,
			@AuthorOfficeID = @AuthorOfficeID
		
		-- 3. spEndRecheckSession1	
		if @Closed = 0	
			EXEC [dbo].[spEndRecheckSession1]
			@rId  = @rId OUTPUT,
			@BatchID = @BatchID,
			@ItemCode = @ItemCode,
			@CurrentOfficeID = NULL,
			@AuthorID = @AuthorID,
			@AuthorOfficeID = @AuthorOfficeID
		
		Fetch Next from updateItemsCursor into @BatchID, @ItemCode, @OperationTypeOfficeID, @OperationTypeID
	END

	CLOSE updateItemsCursor 
	DEALLOCATE updateItemsCursor 

	-- 4. spSetBatchEvents
	Declare batchEventsCursor Cursor for
		Select BatchId, NumberOfItemsAffected, NumberOfItemsInBatch, FormCode, EventID from @BatchEvents
	open batchEventsCursor
	Fetch Next from batchEventsCursor into 
		@BatchID, @NumberOfItemsAffected, @NumberOfItemsInBatch, @FormCode, @EventID
	While(@@FETCH_STATUS=0)
	BEGIN
		EXEC [dbo].[spSetBatchEvents]
			@rId  = @rId OUTPUT,
			@FormCode = @FormCode,
			@EventID = @EventID,
			@BatchID = @BatchID,
			@NumberOfItemsAffected = @NumberOfItemsAffected,
			@NumberOfItemsInBatch = @NumberOfItemsInBatch,
			@CurrentOfficeID = NULL,
			@AuthorID = @AuthorID,
			@AuthorOfficeID = @AuthorOfficeID
		Fetch Next from batchEventsCursor into 
			@BatchID, @NumberOfItemsAffected, @NumberOfItemsInBatch, @FormCode, @EventID
	END	
	CLOSE batchEventsCursor 
	DEALLOCATE batchEventsCursor 
end;
commit transaction;	

GO

