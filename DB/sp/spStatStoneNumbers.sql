USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spStatStoneNumbers]    Script Date: 09/24/2016 00:16:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatStoneNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatStoneNumbers]
GO

USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spStatStoneNumbers]    Script Date: 09/24/2016 00:16:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spStatStoneNumbers]
(
	@DateFrom date,
	@DateTo date,
	@ParentOfficeId int = null,
	@CustomerId int = null,
	@PageNo int = 1,
	@PageSize int = 100
)
AS
	DECLARE @Start INT = (@PageNo - 1) * @PageSize;
	DECLARE @End INT = @PageNo * @PageSize;
	
	select * FROM
	(
		SELECT 
			i.OrderCode, i.BatchCode, i.ItemCode, i.CreateDate, c.CustomerName, i.CustomerID, o.ParentOfficeID,
			ROW_NUMBER() OVER (ORDER BY i.CreateDate) AS RowNumber
		FROM v0Item i, tblOfficeNewNumbers o, v0Customer c 
		WHERE
			i.CreateDate between @DateFrom and @DateTo
			and i.CustomerOfficeID = o.OfficeID
			and i.CustomerID = c.CustomerID
			and i.CustomerOfficeID = c.CustomerOfficeID
			and (@CustomerId is null or @CustomerId = i.CustomerID)
			and (@ParentOfficeId is null or o.ParentOfficeID = @ParentOfficeId)
	) AS PostQuery
		where RowNumber between @Start and @End	

	select COUNT(*) as cnt FROM v0Item i, tblOfficeNewNumbers o, v0Customer c 
		WHERE
			i.CreateDate between @DateFrom and @DateTo
			and i.CustomerOfficeID = o.OfficeID
			and i.CustomerID = c.CustomerID
			and i.CustomerOfficeID = c.CustomerOfficeID
			and (@CustomerId is null or @CustomerId = i.CustomerID)
			and (@ParentOfficeId is null or o.ParentOfficeID = @ParentOfficeId)
	
GO

