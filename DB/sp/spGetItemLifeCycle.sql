USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetItemLifeCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetItemLifeCycle]
GO

USE [GemoDream16]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
Examples:
1) 09146.001.23 -> 09146.002.23 -> 09955.001.01
0914600123
*/
create procedure [dbo].[spGetItemLifeCycle]
(
	@OrderCode int,
	@BatchCode int,
	@ItemCode int
)
As
create table #links
(
	BatchID int,
	ItemCode int
)	
insert into #links(BatchId, ItemCode)
	select i.BatchId, i.ItemCode 
	from v0ItemNoExpDate i
	where i.OrderCode = @OrderCode
			and i.BatchCode = @BatchCode
			and i.ItemCode = @ItemCode 

-- Search Forward
declare @batch int, @newBatch int, @newItemCode int
select 
	@batch = i.batchId, 
	@newBatch = i.NewBatchID,
	@newItemCode = i.NewItemCode 
	
	from v0ItemNoExpDate i
	where i.OrderCode = @OrderCode
		and i.BatchCode = @BatchCode
		and i.ItemCode = @ItemCode 


while (@batch <> @newBatch)
begin
	insert into #links(BatchId, ItemCode)
		select BatchId, ItemCode 
			from v0ItemNoExpDate
			where BatchId = @newBatch and ItemCode = @newItemCode
	select 
		@batch = i.batchId, 
		@newBatch = i.NewBatchID,
		@newItemCode = i.NewItemCode 
	
		from v0ItemNoExpDate i
		where i.BatchId = @newBatch
		and i.ItemCode = @newItemCode
end

-- Reverse Search 
-- start parameters
declare @sBatchID int, @sItemCode int
select @sBatchID = i.BatchID, @sItemCode = i.ItemCode
	from v0ItemNoExpDate As i
	where i.OrderCode = @OrderCode
		and i.BatchCode = @BatchCode
		and i.ItemCode = @ItemCode 

declare @prevBatchId int, @prevItemCode int
select @prevBatchId = BatchID, @prevItemCode = ItemCode 
	from v0ItemNoExpDate
	where 
		NewBatchID = @sBatchID and 
		NewItemCode = @sItemCode and 
		NewBatchID <> BatchID

while(@sBatchID <> @prevBatchId)
begin
	insert into #links(BatchId, ItemCode)
		select BatchId, ItemCode 
			from v0ItemNoExpDate
			where BatchId = @prevBatchId and ItemCode = @prevItemCode
	set @sBatchID = @prevBatchId
	set @sItemCode = @prevItemCode
	select @prevBatchId = BatchID, @prevItemCode = ItemCode 
		from v0ItemNoExpDate
		where 
		NewBatchID = @sBatchID and 
		NewItemCode = @sItemCode and
		NewBatchID <> BatchID

end
	
select i.CreateDate, i.OrderCode, i.BatchCode, i.ItemCode, i.BatchId 
	from v0ItemNoExpDate As i, #links As l
	where 
		i.BatchID = l.BatchID
		and i.ItemCode = l.ItemCode
	order by CreateDate
	
drop table #links

GO

