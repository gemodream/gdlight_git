USE [GemoDream16]
GO
/****** Object:  StoredProcedure [dbo].[spGetShapeGroups]    Script Date: 01/14/2015 18:03:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetShapeGroups]
@AuthorID dnSmallID,
@AuthorOfficeID dnTinyID
AS
BEGIN

;
with ts as (
select ShapeGroupID, ShapeGroupName, ParentShapeGroupID 
  from v0ShapeGroupHierarchy
 where ShapeGroupID in (select FixShapeGroupID from v0ShapeHierarchy) 
 union all
 select s.ShapeGroupID, s.ShapeGroupName, s.ParentShapeGroupID from v0ShapeGroupHierarchy s, ts where s.ShapeGroupID = ts.ParentShapeGroupID )
 select distinct * from ts
    order by ShapeGroupID
/*
SELECT t.ShapeGroupID as ShapeGroupID,
       h.ShapeGroupName as ShapeGroupName,
       t.ParentShapeGroupID as ParentShapeGroupID
FROM tblShapeGroup t, hstShapeGroup h
WHERE t.ShapeGroupHistoryID=h.ShapeGroupHistoryID and t.ExpireDate is null
*/

END
