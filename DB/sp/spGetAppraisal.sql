USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spGetAppraisal]    Script Date: 12/28/2017 16:18:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[spGetAppraisal]
(@rId varchar(150) output,
@BatchID dnID,
@ItemCode dnItemCode,
@CurrentOfficeID dnTinyID, 
@AuthorID dnSmallID,@AuthorOfficeID dnTinyID) 

AS

declare @RapaportName VARCHAR(2)
declare @Price dnMoney
DECLARE @Color VARCHAR(10)
DECLARE @Clarity VARCHAR(100)
--DECLARE @Rows INT
--DECLARE @Messages VARCHAR(200)
DECLARE @ItemTypeID INT
DECLARE @Weight DECIMAL(10,3)
DECLARE @Markup DECIMAL(10,2)
DECLARE @PartID INT
DECLARE @ItemNumber VARCHAR(11)
DECLARE @numberOfStones DECIMAL(10,2)
--For test only 10077536, 5
--DECLARE
--@rId money,
--@BatchID dnID,
--@ItemCode int
-- 7493500701
--209367	1
--SET @BatchID = 209367  --*/341881	
--SET @ItemCode = 5 --5
-- end of test part

SET		@ItemTypeID = (SELECT ItemTypeID FROM v0Batch WHERE BatchID = @BatchID)
SELECT	PartID, ItemTypeID, PartTypeID 
INTO	#Parts
FROM	v0Part
WHERE	ItemTypeID = @ItemTypeID AND PartTypeID in (1, 10)

SELECT	@Weight as [Weight], @Color as Color, @Clarity as Clarity, null as ShapeCode,
		@PartID as PartID,
		@ItemTypeID as ItemTypeID,
		@RapaportName as RapaportName, 
		@Color as UpperColor,
		@Clarity as UpperClarity,
		@Color as LowerColor,
		@Clarity as LowerClarity,
		@Price as UpperPrice,
		@Price as LowerPrice,
		@Price as AveragePrice,
		@Markup as MarkUp,
		@Price as Price,
		@numberOfStones as numberOfStones,
		@ItemNumber as ItemNumber
INTO	#Data
WHERE 1 = 2

--SET	@Rows = @@ROWCOUNT
--SET @Messages = (CASE
--					WHEN  @@ROWCOUNT = 0 THEN 'No Data to Calculate'
--					WHEN  @@ROWCOUNT > 1 THEN 'Multi Line Result'
--					WHEN  @@ROWCOUNT = 1 THEN ''
--				END)
--IF @Messages = ''
BEGIN
DECLARE price_cursor CURSOR FOR
	SELECT PartID, ItemTypeID FROM #Parts
	ORDER by PartID
	OPEN price_cursor
	FETCH NEXT FROM price_CURSOR
	INTO @PartID,  @ItemTypeID
	WHILE @@FETCH_STATUS = 0  
BEGIN 
INSERT INTO #Data
SELECT 	
  	ISNULL(CONVERT(decimal(10,3), CAST(dbo.GetPartMeasureByMeasureID(b.NewBatchID,b.NewItemCode,p.PartID,4)  as VARCHAR(10))),0), 
	ISNULL(LTRIM(RTRIM(dbo.GetPartMeasureByMeasureID(b.NewBatchID,b.NewItemCode,p.PartID,27))),''), 
	ISNULL(LTRIM(RTRIM(dbo.GetPartMeasureByMeasureID(b.NewBatchID,b.NewItemCode,p.PartID,29))),''),
	[dbo].[GetShapeCode](b.NewBatchID,b.NewItemCode,p.PartID),
	p.PartID,
	p.ItemTypeID,
	@RapaportName,
	@Color,
	@Clarity,
	@Color,
	@Clarity,
	@Price,
	@Price,
	@Price,
	@Markup,
	@Price,
	ISNULL(CONVERT(decimal(10,3),  CAST(dbo.GetPartMeasureByMeasureID(b.NewBatchID,b.NewItemCode,p.PartID,65)  as VARCHAR(10))),1),		
	dbo.GetFullGroupCode(b.PrevGroupCode) +
	REPLICATE('0', 3 - LEN(CAST(b.PrevBatchCode AS VARCHAR(3)))) + CAST(b.PrevBatchCode AS VARCHAR(3)) + 
	REPLICATE('0', 2 - LEN(CAST(b.PrevItemCode AS VARCHAR(2)))) 	+ CAST(b.PrevItemCode AS VARCHAR(2))

	from v0Item b, #Parts p  
	where b.BatchID = @BatchID 
	and (b.ItemCode = @ItemCode)
	and p.PartID = @PartID
	and p.ItemTypeID = @ItemTypeID

	SET  @Color = (SELECT Color FROM #Data WHERE PartID = @PartID and ItemTypeID = @ItemTypeID)
	SET  @Clarity = (SELECT Clarity FROM #Data WHERE PartID = @PartID and ItemTypeID = @ItemTypeID)
	SET @Weight = (SELECT [Weight] FROM #Data WHERE PartID = @PartID and ItemTypeID = @ItemTypeID)
	
	update #Data
	SET RapaportName = (SELECT  m.RapaportName FROM tblMeasureValue m /*, #Data d */ WHERE m.ValueCode = d.ShapeCode), -- and d.PartID = @PartID),
	UpperColor = 	(SELECT  UpperGrade FROM [dbo].[GetGradesForAppraisal] (@Color,@PartID)),-- #Data d WHERE d.PartID =@PartID AND d.Color = @Color),
	UpperClarity = 	(SELECT  UpperGrade FROM [dbo].[GetGradesForAppraisal] (@Clarity,@PartID)),-- #Data d WHERE d.PartID =@PartID AND d.Clarity = @Clarity),
	LowerColor = 	(SELECT  LowerGrade FROM [dbo].[GetGradesForAppraisal] (@Color,@PartID)),-- #Data d WHERE d.PartID =@PartID AND d.Color = @Color),
	LowerClarity = 	(SELECT  LowerGrade FROM [dbo].[GetGradesForAppraisal] (@Clarity,@PartID))-- #Data d WHERE d.PartID =@PartID AND d.Clarity = @Clarity)
	FROM #Data d where PartID = @PartID and ItemTypeID = @ItemTypeID

	update #Data
	SET
	UpperPrice = 	(SELECT ap.Price FROM tblRapaportAppraisal ap--, #Data d 
					WHERE	d.Weight between ap.WeightFrom	and ap.WeightTo and
							d.UpperClarity = ap.Clarity		and
							d.UpperColor = ap.Color			and
							d.RapaportName = ap.Shape), --		and d.PartID = @PartID),
	LowerPrice =	(SELECT ap.Price FROM tblRapaportAppraisal ap--, #Data d 
					WHERE	d.Weight between ap.WeightFrom	and ap.WeightTo and
							d.LowerClarity = ap.Clarity		and
							d.LowerColor = ap.Color			and
							d.RapaportName = ap.Shape) --		and d.PartID = @PartID)
						FROM #Data d where PartID = @PartID and ItemTypeID = @ItemTypeID
	--select * from #Data
	Update #Data 
	SET AveragePrice = (CAST(UpperPrice as money) + CAST(LowerPrice as money))/2 FROM #Data WHERE PartID = @PartID and ItemTypeID = @ItemTypeID
	Update #Data
	SET MarkUp = (SELECT mp.Markup FROM tblMarkupPrice mp, #Data WHERE (#Data.AveragePrice  BETWEEN PriceFrom and PriceTo) AND PartID = @PartID and ItemTypeID = @ItemTypeID)
	
	Update #Data
	SET Price = AveragePrice * MarkUp FROM #Data WHERE PartID = @PartID and ItemTypeID = @ItemTypeID				
	FETCH NEXT FROM price_CURSOR
	INTO @PartID,  @ItemTypeID
END
CLOSE price_cursor
DEALLOCATE price_cursor
DELETE #Data
WHERE Weight = 0

select d.*, s.ShortReportName from #Data d , v0Shape s where d.ShapeCode = s.ShapeCode
SET @rID = (SELECT SUM(Price) FROM #Data)
SELECT @rID 
	
END
--ELSE
--BEGIN
--select @rID = null
--END
--RETURN @@ROWCOUNT
drop table #Data, #Parts



--declare @rIdtest  varchar(10)
--exec spGetAppraisal
--@rId = @rIdtest output,
--@BatchID = 209367,	
--@ItemCode = 1,
--@CurrentOfficeID = 1,
--@AuthorID = 1,@AuthorOfficeID  = 1
--select @rIdtest


GO

