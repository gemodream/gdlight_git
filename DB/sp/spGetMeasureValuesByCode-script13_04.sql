CREATE PROCEDURE [dbo].spGetMeasureValuesByCode
(
@MeasureCode int
) as
SELECT * from vwMeasureValue where MeasureCode=@MeasureCode
ORDER BY MeasureValueName