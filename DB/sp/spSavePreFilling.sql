USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSavePreFilling]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSavePreFilling]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spSavePreFilling]
(
	@ErrMsg nvarchar(1000) OUTPUT,
	@AuthorID int,
	@AuthorOfficeID int,
	@CurrentOfficeID int,
	@PreFillingList [dbo].[PreFillingModelTableType] READONLY
)

as 

Declare @BatchID int
Declare @ItemCode int
Declare @PartId int
Declare @MeasureCode int
Declare @MeasureValueID int = null		-- enum value or
Declare @MeasureValue float = null		-- numeric value or
Declare @StringValue varchar(100) = null	-- string value	

BEGIN TRANSACTION;

BEGIN TRY
	Declare preFillingCursor Cursor for
		Select BatchID, ItemCode, PartId, MeasureCode, MeasureValueID, MeasureValue, StringValue
			from @PreFillingList

	open preFillingCursor
	
	Fetch Next from preFillingCursor into @BatchID, @ItemCode, @PartId, @MeasureCode, @MeasureValueID, @MeasureValue, @StringValue
	While(@@FETCH_STATUS=0)
	BEGIN
		Declare @rID varchar(255)
		exec spSetPartValue
			@rId=@rId output,
			@ItemCode=@ItemCode,
			@BatchID=@BatchID, 
			@PartID=@PartID, 
			@MeasureCode=@MeasureCode,
			@MeasureValue=@MeasureValue,
			@MeasureValueID=@MeasureValueID,
			@StringValue=@StringValue,
			@UseClosedRecheckSession=NULL,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, 
			@AuthorOfficeID=@AuthorOfficeID
		Fetch Next from preFillingCursor into @BatchID, @ItemCode, @PartId, @MeasureCode, @MeasureValueID, @MeasureValue, @StringValue
	END
	CLOSE preFillingCursor 
	DEALLOCATE preFillingCursor 

END TRY
BEGIN CATCH
	SET @ErrMsg = ERROR_MESSAGE()
	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH
IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;	

GO

