USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spStatsCalc]    Script Date: 02/01/2017 03:48:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsCalc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsCalc]
GO

USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spStatsCalc]    Script Date: 02/01/2017 03:48:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spStatsCalc]
(
	@ErrMsg nvarchar(1000) OUTPUT,
	@DateFrom date = null,
	@DateTo date = null,
	@MeasureIds [dbo].[intTableType] READONLY,
	@CustomerIds [dbo].[intTableType] READONLY,
	@Comments varchar(1000) = NULL,
	@UserId int
)
AS
	DECLARE @begDate date = ISNULL(@DateFrom, (select min(OrderDate) from v0Item ))
	DECLARE @endDate date = ISNULL(@DateTo, (select max(OrderDate) from v0Item))	

	declare @customers varchar(8000) = ''
	declare @customerQty int = (select count(*) from @CustomerIds)
	DECLARE @batch int

	create table #customers(ID int)

	-- Customers
	IF (@customerQty = 0)
		-- ALL
		INSERT INTO #customers(ID)
			SELECT i.CustomerID AS ID 
				FROM v0Item as i 
				WHERE 1=1
					and (cast(i.OrderDate as DATE) between @begDate and @endDate) 
					and i.BatchID = i.NewBatchID
					and i.CustomerOfficeID <> 3
				GROUP BY i.CustomerID
	ELSE
		-- SELECTED
		INSERT INTO #customers (ID) SELECT ID from @CustomerIds 
		SELECT @customers = COALESCE(@customers + ';', '') + ltrim(str(id)) from #customers


	-- Measures
	SELECT rm.ReasonID, rm.MeasureID into #rules
		FROM tblRejectReason_Measure As rm, @MeasureIds As sm
		WHERE rm.MeasureID = sm.ID
			
	-- Create Report record
	DECLARE @myReport int
	INSERT INTO statsReport(calc_date, date_from, date_to, user_id, customerIds, comments) 
		values 
			(GETDATE(), @begDate, @endDate, @UserId, @customers, @Comments)
	SET @myReport = @@IDENTITY
	
	-- Add ReportRules
	INSERT INTO statsReportRule (report_id, reason_id, measure_id)
		SELECT @myReport, ReasonID, MeasureID FROM #rules
 
	
	declare @customerId int	
	DECLARE CustomerCursor CURSOR FOR
		select ID from #customers
	OPEN CustomerCursor
	FETCH NEXT FROM CustomerCursor INTO @customerId
	
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		exec spStatsCalcByCustomer 
			@ErrMsg = @ErrMsg, 
			@ReportId = @myReport, 
			@CustomerId = @customerId, 
			@DateFrom = @begDate, 
			@DateTo = @endDate, 
			@MeasureIds = @MeasureIds
		FETCH NEXT FROM CustomerCursor INTO @customerId
	END
	CLOSE CustomerCursor
	DEALLOCATE CustomerCursor
	drop table #customers
	drop table #rules

GO

