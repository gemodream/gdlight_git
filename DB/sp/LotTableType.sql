USE [GemoDream16]
GO

/****** Object:  UserDefinedTableType [dbo].[LotTableType]    Script Date: 03/09/2015 04:41:27 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'LotTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[LotTableType]
GO

USE [GemoDream16]
GO

/****** Object:  UserDefinedTableType [dbo].[LotTableType]    Script Date: 03/09/2015 04:41:27 ******/
CREATE TYPE [dbo].[LotTableType] AS TABLE(
	[LID] [int] NULL,
	[LotNumber] [nvarchar](250) NULL
)
GO

