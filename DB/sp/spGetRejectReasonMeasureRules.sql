USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRejectReasonMeasureRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRejectReasonMeasureRules]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure  [dbo].[spGetRejectReasonMeasureRules]
(
	@PartTypeId int
)
AS
SELECT pm.MeasureID, pm.PartTypeID into #pm
FROM tblPartType_Measure AS pm LEFT OUTER JOIN
     tblRejectReason_Measure AS rm ON pm.MeasureID = rm.MeasureID AND pm.PartTypeID = rm.PartTypeID
WHERE  pm.PartTypeID = @PartTypeId and   (rm.ID IS NULL)

SELECT r.ID, 
	r.PartTypeID, p.PartTypeName, 
	r.MeasureID, m.MeasureName, 
	r.ReasonID, rl.ReasonName 
	FROM tblPartType AS p INNER JOIN
		tblMeasure AS m INNER JOIN
		tblRejectReason_Measure AS r INNER JOIN
		tblRejectReason AS rl ON r.ReasonID = rl.ReasonID ON m.MeasureID = r.MeasureID ON p.PartTypeID = r.PartTypeID
	where r.PartTypeID = @PartTypeId
UNION
SELECT	null as ID, 
	pm.PartTypeID, p.PartTypeName, 
	pm.MeasureID, m.MeasureName,
	null as ReasonID, '' as ReasonName
	FROM tblPartType AS p INNER JOIN
             #pm AS pm ON p.PartTypeID = pm.PartTypeID INNER JOIN
             tblMeasure AS m ON pm.MeasureID = m.MeasureID
drop table #pm		            
GO

