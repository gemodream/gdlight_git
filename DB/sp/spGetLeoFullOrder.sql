--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[spGetLeoFullOrder]    Script Date: 03/19/2015 04:23:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLeoFullOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLeoFullOrder]
GO

/****** Object:  StoredProcedure [dbo].[spGetLeoFullOrder]    Script Date: 03/19/2015 04:23:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetLeoFullOrder]
( @GroupCode int)
AS
BEGIN
	exec [dbo].[spGetFullOrder] @GroupCode
	exec [dbo].[sasha_SelectPrintingQueue3] 
 		@GroupCode,
 		@BatchCode = 0,
 		@ItemCode = 0,
		@ReportStatus = N'A',
		@ShowLabel = 0,
		@ShowLaserGold = 0,
		@ShowGMXorderFiles = 1,
		@ShowOldNumbers = 0
	EXEC	[dbo].[spGetPrintingHistory]
		@GroupCode,
		@BatchCode = 0,
		@ItemCode = 0,
		@Status = N'Y',
		@FullHistory = 0,
		@OperationChar = ''
	
		
END

GO

