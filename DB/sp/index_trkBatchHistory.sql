--USE [GemoDream16]
--GO

/****** Object:  Index [IX2_trkBatchHistory]    Script Date: 02/20/2015 12:45:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[trkBatchHistory]') AND name = N'IX2_trkBatchHistory')
DROP INDEX [IX2_trkBatchHistory] ON [dbo].[trkBatchHistory] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX2_trkBatchHistory]    Script Date: 02/20/2015 12:45:36 ******/
CREATE NONCLUSTERED INDEX [IX2_trkBatchHistory] ON [dbo].[trkBatchHistory] 
(
	[RecordTimeStamp] ASC
)
INCLUDE ( [EventID],
[BatchID],
[FormID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO

