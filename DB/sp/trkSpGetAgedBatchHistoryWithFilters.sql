USE [GemoDreamTest]
GO
/****** Object:  StoredProcedure [dbo].[trkSpGetAgedBatchHistoryWithFilters]    Script Date: 02/19/2015 09:25:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [dbo].[trkSpGetAgedBatchHistoryWithFilters](
@DateFrom DateTime, 
@DateTo DateTime, 
@CustomerID int = null, 
@ShowCheckedOut int = Null,
@ViewAccessID int = null,
@EventID int = null
)

as

--select * from refCalendarHourly

--delete refCalendarHourly

--drop table #t
/*
select distinct vb.* 
into #v
	from 
		v0Batch vb
		inner join v0Item vi on vb.BatchID = vi.BatchID

where 
	vi.NewBatchID = vi.BatchID
	
*/

declare @vsql nvarchar(max)

select 
		trb.RecordTimeStamp,  
		tre.EventName,
		vb.GroupCode,
		vb.BatchCode,
		vc.CompanyName,
		tva.VewAccessName,
		vb.BatchID
into #t
from 
	trkBatchHistory trb
	left outer join v0Batch vb on
		trb.Batchid = vb.batchID
	left outer join v0Customer vc on
		vc.CustomerID = vb.CustomerID
	left outer  join trkEvent tre on
		trb.EventID = tre.EventID
	left outer join tblViewAccess tva on
		trb.FormID = tva.ViewAccessID
WHERE 1 = 2

select @vsql = N'
insert into #t
select 
		trb.RecordTimeStamp,  
		tre.EventName,
		vb.GroupCode,
		vb.BatchCode,
		vc.CompanyName,
		tva.VewAccessName,
		vb.BatchID
from 
	trkBatchHistory trb
	inner join v0Batch vb on
		trb.Batchid = vb.batchID ' + case when @CustomerID is not null then ' and vb.CustomerID = @CustomerID ' else '' end + '
	left outer join v0Customer vc on
		vc.CustomerID = vb.CustomerID
	left outer  join trkEvent tre on
		trb.EventID = tre.EventID
	left outer join tblViewAccess tva on
		trb.FormID = tva.ViewAccessID
	inner join v0Group vg on
		vb.GroupID = vg.GroupID 		
		
where trb.RecordTimeStamp between @DateFrom and @DateTo ' + 
		case when @ShowCheckedOut is null then ' and (
												tre.EventID != 8
											and	tre.EventID != 9
											and vb.StateCode not in (1)
											and vg.StateCode not in (1)
											) ' else '' end + 
		 case when @ViewAccessID is not null then ' and trb.FormID = @ViewAccessID ' else '' end + 
		 case when @EventID is not null then ' and trb.EventID = @EventID ' else '' end + ' option (force order, maxdop 1 )'

execute sp_executesql @vSql, N'@DateFrom DateTime, @DateTo DateTime, @CustomerID int, @ViewAccessID int, @EventID int',
@DateFrom = @DateFrom,
@DateTo = @DateTo,
@CustomerID = @CustomerID,
@ViewAccessID = @ViewAccessID,
@EventID = @EventID

declare @Now DateTime 
select @Now = GETDATE()	

select dbo.GetAgeInWorkHours(RecordTimeStamp, @Now) ageInHrs,   * from #t t
where BatchID  in (select BatchID from v0Item vi where vi.NewBatchID = t.BatchID)
order by RecordTimeStamp




--select COUNT(*) from v0Group