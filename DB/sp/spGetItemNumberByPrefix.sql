USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetItemNumberByPrefix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetItemNumberByPrefix]
GO

USE [GemoDream16]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetItemNumberByPrefix]
(
	@Prefix varchar(7)
)
As
--declare @Prefix varchar(7) = '0024024'
select vi.GroupCode, vi.BatchCode, vi.ItemCode
	from 	v0item vi
	inner join v0partvalue pv on 
		vi.newitemcode 	= pv.itemcode	and
		vi.newbatchid 	= pv.batchid	and
		pv.measureid = 112
		inner join v0Part vp on
			pv.PartID = vp.PartID 	
			--and vp.PartTypeID = 1
where pv.stringvalue = @Prefix
	and vi.NewBatchID = vi.BatchID
	group by vi.GroupCode, vi.BatchCode, vi.ItemCode
-- 0024018
-- 0023882
-- 0003169

GO

