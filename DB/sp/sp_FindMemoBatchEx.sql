--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_FindMemoBatchEx]    Script Date: 02/14/2015 15:29:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FindMemoBatchEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FindMemoBatchEx]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[sp_FindMemoBatchEx] --'fghy456fg'

   @MemoNumber varchar(255)

AS

CREATE TABLE #t (
OrderCode VARCHAR(255) , 
BatchCode VARCHAR(255) ,
MemoNumber VARCHAR(255),
CustomerCode VARCHAR(255) ,
CreateDate VARCHAR(255) 
 )

INSERT  #t
SELECT       v0Batch.GroupCode as OrderCode, v0Batch.BatchCode as BatchCode, hstMemoNumber.Name as MemoNumber
, v0Batch.CustomerCode as CustomerCode , v0Batch.CreateDate as CreateDate
FROM         tblMemoNumber INNER JOIN
                      hstMemoNumber ON tblMemoNumber.MemoNumberID = hstMemoNumber.MemoNumberID AND 
                      tblMemoNumber.MNHID = hstMemoNumber.MNHID INNER JOIN
                      v0Batch ON tblMemoNumber.MemoNumberID = v0Batch.MemoNumberID
WHERE     hstMemoNumber.Name LIKE @MemoNumber
ORDER BY v0Batch.GroupCode, v0Batch.BatchCode

select	dbo.GetFullGroupCode(OrderCode) + 
		--REPLICATE('0', 5 - LEN(CAST(OrderCode AS VARCHAR(5)))) + CAST(OrderCode AS VARCHAR(5)) + 
		REPLICATE('0', 3 - LEN(CAST(BatchCode AS VARCHAR(3)))) + CAST(BatchCode  AS VARCHAR(3)) as [Batch Number], 
		memonumber as [Memo Number]
		, CustomerCode as [Client Number], CreateDate as [Date]
		 from #t

GO

