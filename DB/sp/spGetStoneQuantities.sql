USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStoneQuantities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStoneQuantities]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetStoneQuantities]
(
	@DateFrom date = null,
	@DateTo date = null,
	@OfficeIds [dbo].[intTableType] READONLY,
	@OrderStateCode int = null
)
AS -- 5103
	-- Acron Customer IDs = 430,448,570, Customer Codes = 1410, 1412, 1433
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0Item))
	declare @dateEnd date = ISNULL(@DateTo, GetDate())

	select count(*) as cnt, 
		(CASE WHEN i.CustomerID IN (430,448,570) THEN -1 ELSE i.CustomerOfficeID END) as officeId
	into #stones
		from v0Item As i, @OfficeIds As o
		where 1=1
			and (cast(i.OrderDate as DATE) between @dateBeg and @dateEnd) 
			and i.BatchID = i.NewBatchID
			and i.CustomerOfficeID = o.ID
			and i.CustomerOfficeID <> 3
			and (@OrderStateCode IS NULL OR @OrderStateCode = i.OrderStateCode)
		group by (CASE WHEN i.CustomerID IN (430,448,570) THEN -1 ELSE i.CustomerOfficeID END)
	
	select s.*, ISNULL(o.OfficeCode, 'Akron') as OfficeCode, ISNULL(o.Country,'USA') as Country from #stones s LEFT JOIN  tblOfficeNewNumbers o
		ON s.OfficeID = o.OfficeID 
		order by o.Country, o.OfficeCode

	drop table #stones
GO

