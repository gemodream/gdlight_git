USE [GemoDream16]
GO

/****** Object:  StoredProcedure [dbo].[spAddCustomerProgram]    Script Date: 11/08/2015 10:39:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSaveCustomerProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSaveCustomerProgram]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PricePartsTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[PricePartsTableType]
GO
CREATE TYPE [dbo].[PricePartsTableType] AS TABLE(
	[PartId] [dbo].[dnSmallID] NOT NULL,
	[MeasureCode] [dbo].[dnCode] NOT NULL,
	[HomogeneousClassID] [dbo].[dnTinyID] NOT NULL,
	[PartNameMeasureName] [dbo].[dsName] NOT NULL
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PriceRangeTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[PriceRangeTableType]
GO
CREATE TYPE [dbo].[PriceRangeTableType] AS TABLE(
	[HomogeneousClassID] [dbo].[dnTinyID] ,
	[FromValue] [dbo].[dnMeasureExt],
	[ToValue] [dbo].[dnMeasureExt],
	[Price] [dbo].[dnMeasureExt]
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PriceAddServTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[PriceAddServTableType]
GO
CREATE TYPE [dbo].[PriceAddServTableType] AS TABLE(
	[AdditionalServiceID] [dbo].[dnTinyID],
	[Price] [dbo].[dnMeasureExt]
)
GO


IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CpOperationTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[CpOperationTableType]
GO
CREATE TYPE [dbo].[CpOperationTableType] AS TABLE(
	[OperationTypeOfficeId] [dbo].[dnTinyID] NOT NULL,
	[OperationTypeID] [dbo].[dnID] NOT NULL,
	[Checked] bit 
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CpDocTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[CpDocTableType]
GO
CREATE TYPE [dbo].[CpDocTableType] AS TABLE(
	[CpDocId] varchar(20) NOT NULL,
	[Description] nvarchar(2000) NULL,
	[IsReturn] smallInt NULL
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CpDocRuleTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[CpDocRuleTableType]
GO
CREATE TYPE [dbo].[CpDocRuleTableType] AS TABLE(
	[CpDocId] dnID NOT NULL,
	[MeasureId] dnTinyID NOT NULL,
	[MinMeasure] dnMeasureExt,
	[MaxMeasure] dnMeasureExt,
	[PartID] dnSmallID,
	[NotVisibleInCCM] dnBool,
	[IsDefaultMeasureValue] dnBool
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CpDocGroupTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[CpDocGroupTableType]
GO
CREATE TYPE [dbo].[CpDocGroupTableType] AS TABLE(
	[CpDocId] dnID NOT NULL,
	[MeasureGroupID] dnSmallID NOT NULL,
	[NoRecheck] dnInteger
)
GO
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CpDocOperTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[CpDocOperTableType]
GO
CREATE TYPE [dbo].[CpDocOperTableType] AS TABLE(
	[CpDocId] [dbo].[dnID] NOT NULL,
	[OperationTypeOfficeId] [dbo].[dnTinyID] NOT NULL,
	[OperationTypeID] [dbo].[dnID] NOT NULL
)
GO

/****** Object:  StoredProcedure [dbo].[spAddCustomerProgram]    Script Date: 11/08/2015 10:39:58 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE  PROCEDURE [dbo].[spSaveCustomerProgram](
@rId varchar(150) output,
@CPOfficeID dnTinyID, 
@CPID dnID, 
@CustomerProgramName dsName, 
@ItemTypeID dnSmallID, 
@Comment dsComment, 
@Path2Picture dsComment,
@CustomerOfficeID dnSmallID, 
@CustomerID dnID, 
@VendorOfficeID dnSmallID, 
@VendorID dnID,
@ItemTypeGroupID dnSmallID,
@CustomerStyle dsName,
@CPPropertyCustomerID dsName,
@SRP dsName,
@Description dsComment,
@IsFixed dnTinyID, 
@FixedPrice dnMeasureExt, 
@DeltaFix dnMeasureExt, 
@Discount dnMeasureExt, 
@FailFixed dnMeasureExt,
@FailDiscount dnMeasureExt,
@CopyAllCp dnTinyID, 
@PriceParts [dbo].[PricePartsTableType] READONLY,
@PriceRange [dbo].[PriceRangeTableType] READONLY,
@PriceAddServs [dbo].[PriceAddServTableType] READONLY,
@CpOperations [dbo].[CpOperationTableType] READONLY,
@CpDocs [dbo].[CpDocTableType] READONLY,
@CpDocRules [dbo].[CpDocRuleTableType] READONLY,
@CpDocGroups [dbo].[CpDocGroupTableType] READONLY,
@CpDocOpers [dbo].[CpDocOperTableType] READONLY,
@AuthorID dnSmallID,
@AuthorOfficeID dnTinyID,
@CPOfficeIDFrom dnTinyID, 
@CPIDFrom dnID
) AS

declare @PriceIdNew numeric(15,0)
declare @rIdNew varchar(150)

begin transaction;
begin

-- 1 spSetPrices
	INSERT INTO tblPrice(AuthorID, AuthorOfficeID, StartDate)
		VALUES(@AuthorID, @AuthorOfficeID, GetDate())

	set @PriceIdNew=@@identity

-- 2 spSetPricePartsMeasures
	INSERT INTO tblPricePartsMeasures(PriceID, PartID , MeasureCode ,  HomogeneousClassID, PartNameMeasureName)
		select @PriceIdNew, PartID, MeasureCode, HomogeneousClassID, PartNameMeasureName from @PriceParts

-- 3 spSetPriceRange
	INSERT INTO tblPriceRange(PriceID, HomogeneousClassID, ValueFrom, ValueTo, Price)
		select @PriceIdNew, HomogeneousClassID, FromValue, ToValue, Price from @PriceRange

-- 4 spSetAdditionalServicePrice
	INSERT INTO tblAdditionalServicePrice(PriceID, ASID, Price)
		select @PriceIdNew, AdditionalServiceID, Price from @PriceAddServs

-- 5 spAddCustomerProgram

	EXEC [dbo].[spAddCustomerProgram]
	@rId  = @rIdNew OUTPUT,
	@CPOfficeID = @CPOfficeID, 
	@CPID = @CPID, 
	@CreateDate = NULL, 
	@LastModifiedDate = NULL, 
	@CustomerProgramName = @CustomerProgramName, 
	@CustomerProgramHistoryID = NULL, 
	@ItemTypeID = @ItemTypeID, 
	@Comment = @Comment, 
	@IsCopy = NULL, 
	@Path2Picture = @Path2Picture,
	@CurrentOfficeID = @CustomerOfficeID, --NULL,
	@CustomerOfficeID = @CustomerOfficeID, 
	@CustomerID = @CustomerID, 
	@VendorOfficeID = @VendorOfficeID, 
	@VendorID = @VendorID,
	@ItemTypeGroupID = @ItemTypeGroupID,
	@CustomerStyle = @CustomerStyle,
	@CPPropertyCustomerID = @CPPropertyCustomerID,
	@SRP = @SRP,
	@Description = @Description,
	@AuthorID = @AuthorID,
	@AuthorOfficeID = @AuthorOfficeID,
	@isFixed = @IsFixed, 
	@FixedPrice = @FixedPrice, 
	@DeltaFix = @DeltaFix, 
	@Discount = @Discount, 
	@FailFixed = @FailFixed,
	@FailDiscount = @FailDiscount, 
	@PriceID = @PriceIdNew
	set @rId = @rIdNew
	
declare @myCpID numeric(15,0)
declare @myCpOfficeId numeric(4,0)
set @myCpOfficeId = convert(numeric(15,0), substring(@rIdNew, 1, charindex('_', @rIdNew) - 1))
set @myCpId = convert(numeric(15,0), substring(@rIdNew, charindex('_', @rIdNew) + 1, len(@rIdNew) - charindex('_', @rIdNew)))


-- 6. spSetCPOperationWithCheck
declare @OperationTypeId int
declare @OperationTypeOfficeId int
declare @Checked bit

	Declare cpOperationsCursor Cursor for
		Select OperationTypeId, OperationTypeOfficeId, Checked
			from @CpOperations

	open cpOperationsCursor
	
	Fetch Next from cpOperationsCursor into @OperationTypeId, @OperationTypeOfficeId, @Checked
	While(@@FETCH_STATUS=0)
	BEGIN
		EXEC [dbo].[spSetCPOperationWithCheck]	
		@rId  = @rId OUTPUT,
		@CPOfficeID = @myCpOfficeId,
		@CPID = @myCpId,
		@OperationTypeOfficeID = @OperationTypeOfficeId	,
		@OperationTypeID = @OperationTypeId	,
		@CustomerProgramHistoryID = NULL,
		@CurrentOfficeID = NULL,
		@Checked = @Checked,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID
		
		Fetch Next from cpOperationsCursor into @OperationTypeId, @OperationTypeOfficeId, @Checked
	END
	CLOSE cpOperationsCursor 
	DEALLOCATE cpOperationsCursor 
	
-- 7. spSetCPDoc

declare @cpDocIdOld varchar(20)
declare @cpDocIdNew varchar(20)
declare @cpDescrip varchar(2000)
declare @isReturn smallInt
	Declare cpDocsCursor Cursor for
		Select CpDocId, [Description], IsReturn
			from @CpDocs

	open cpDocsCursor
	
	Fetch Next from cpDocsCursor into @cpDocIdOld, @cpDescrip, @isReturn
	While(@@FETCH_STATUS=0)
	BEGIN
		EXEC [dbo].[spSetCPDoc]
		@rId  = @cpDocIdNew OUTPUT,
		@CPOfficeID = @myCpOfficeId,
		@CPID = @myCpId,
		@Description = @cpDescrip,
		@IsReturn = @isReturn,
		@CustomerProgramHistoryID = NULL,
		@CurrentOfficeID = NULL,
		@AuthorID = @AuthorID,
		@AuthorOfficeID = @AuthorOfficeID
		
-- 8. spSetCPDocRule
		insert into hdtCPDocRule
			(CPDocID, MeasureID, MinMeasure, MaxMeasure, PartID, NotVisibleInCCM, IsDefaultMeasureValue )
		select @cpDocIdNew, MeasureID, MinMeasure, MaxMeasure, PartID, NotVisibleInCCM, IsDefaultMeasureValue 
			from @CpDocRules r where r.CpDocId = cast(@cpDocIdOld as int)

		-- 4. spSetCPDoc_MeasureGroup
		insert into hdtCPDoc_MeasureGroup (CPDocID, MeasureGroupID, NoRecheck )
		select @cpDocIdNew, MeasureGroupID, NoRecheck 
			from @CpDocGroups g where g.CpDocId = cast(@cpDocIdOld as int)
			
		-- 5. spSetCPDoc_Operation
		insert into hdtCPDoc_Operation (CPDocID, OperationTypeOfficeID, OperationTypeID )
		select @cpDocIdNew, OperationTypeOfficeID, OperationTypeID 
			from @CpDocOpers o where o.CpDocId = cast(@cpDocIdOld as int)
			
		Fetch Next from cpDocsCursor into @cpDocIdOld, @cpDescrip, @isReturn		
	END
	CLOSE cpDocsCursor 
	DEALLOCATE cpDocsCursor 
	
-- 9. If SaveAS, spCopyDefDocs
	if @CPIDFrom is not null
	begin
		INSERT INTO tblDocument_CP (DocumentID, CPOfficeID, CPID) 
			SELECT DocumentID, @myCpOfficeId, @myCpId FROM tblDocument_CP WHERE CPOfficeID=@CPOfficeIDFrom AND CPID=@CPIDFrom
	end;
		
-- 9. spCopyAllCP_New
if @CopyAllCp = 1
	EXEC [dbo].[spCopyAllCP_New]
	@rId = @rId output,
	@AuthorID = @AuthorID,
	@AuthorOfficeID = @AuthorOfficeID,
	@CurrentOfficeID = NULL,
	@CPOfficeID = @myCpOfficeId,
	@CPID = @myCpId,
	@CustomerID = @CustomerID,
	@CustomerOfficeID = @CustomerOfficeID

end;
commit transaction;	
set @rId = cast(@myCpOfficeId as varchar(5)) + '_' + cast(@myCpId as varchar(20))

GO

