--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_LeoOrderReports]    Script Date: 03/20/2015 02:52:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeoOrderReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeoOrderReports]
GO

/****** Object:  StoredProcedure [dbo].[sp_LeoOrderReports]    Script Date: 03/20/2015 02:52:52 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO
-- 1. Original spGetCPDocsFromItemList
-- 2. Original sp_SetItemsDocsFromList
-- 3. Original sp_EnqueueLeo

CREATE procedure [dbo].[sp_LeoOrderReports](

	@Author dnSmallID,
	@AuthorOffice dnTinyID,
	@ItemNumbers [dbo].[itemModelTableType]  READONLY
)

AS

create table #Items
(
	Item varchar(11),
	GroupCode  varchar(10),
	BatchCode  varchar(10),
	ItemCode varchar(10)
)
insert into #Items select * from @ItemNumbers

select 	
	#Items.Item,
	vi.GroupCode, 
	vi.BatchCode, 
	vi.ItemCode, 
	vi.NewBatchID, 
	vi.NewItemCode,
	vb.CPID 
into 
	#SelectedItems
from
	#Items inner join v0Item vi on
	#Items.GroupCode = vi.GroupCode 
and	#Items.BatchCode = vi.BatchCode
and	#Items.ItemCode = vi.ItemCode,
	v0Batch vb
where 
	vb.BatchID = vi.NewBatchID
	

select 	
	@Author as AuthorId, 
	@AuthorOffice as AuthorOfficeID,
	co.OperationTypeOfficeID as CurrentOfficeId,
	co.OperationTypeOfficeID,
	co.OperationTypeID, 
	si.NewBatchID as BatchId, 
	si.NewItemCode as ItemCode 
into #Documents
from 
	hdtCPDoc d, 
	tblCustomerProgram cp,
	hdtCPDoc_Operation co,
	tblDocument td,
	#SelectedItems si
where 	
	d.CustomerProgramHistoryID=cp.CustomerProgramHistoryID 
and	co.cpdocid = d.cpdocid
and	co.OperationTypeID = td.OperationTypeID
and	(td.DocumentTypeCode not in (8,10))
and	cp.cpid = si.cpid

order by 
	si.groupcode, si.batchcode, si.itemcode, td.DocumentTypeCode

--2. Original sp_SetItemsDocsFromList
DECLARE	
@AuthorId INT,
@AuthorOfficeId INT, 
@CurrentOfficeId INT, 
@OperationTypeOfficeID INT, 
@OperationTypeID INT, 
@BatchID INT, 
@ItemCode INT,
@rId varchar(150),
@idoc int,
@Count int
BEGIN TRANSACTION;

BEGIN TRY

SET	@Count = (SELECT Count(*) FROM #Documents)

IF	@Count > 0 
	BEGIN
		DECLARE tnames_cursor CURSOR
		FOR
   		SELECT AuthorId, AuthorOfficeId, CurrentOfficeId, OperationTypeOfficeID, OperationTypeID, BatchID, ItemCode
   		FROM #Documents
		OPEN tnames_cursor
		FETCH NEXT FROM tnames_cursor 
		INTO	@AuthorId, @AuthorOfficeId, @CurrentOfficeId, @OperationTypeOfficeID, @OperationTypeID, @BatchID, @ItemCode
		WHILE (@@FETCH_STATUS <> -1)
		BEGIN
   		IF (@@FETCH_STATUS <> -2)
   		BEGIN 
	 
      		exec dbo.spAddItemOperation
			@rId=@rId output,
			@ItemCode=@ItemCode,
			@BatchID=@BatchID, 
			@OperationTypeOfficeID=@OperationTypeOfficeID, 
			@OperationTypeID=@OperationTypeID,
			@CurrentOfficeID=@CurrentOfficeID,
			@AuthorID=@AuthorID, 
			@AuthorOfficeID=@AuthorOfficeID

		exec dbo.spGetCheckClossedRecheckSessionForItem
			@ItemCode=@ItemCode,
			@BatchID=@BatchID, 
			@AuthorID=@AuthorID, 
			@AuthorOfficeID=@AuthorOfficeID			

   		END
   		FETCH NEXT FROM tnames_cursor 
		INTO	@AuthorId, @AuthorOfficeId, @CurrentOfficeId, @OperationTypeOfficeID, @OperationTypeID, @BatchID, @ItemCode
		END
		CLOSE tnames_cursor
		DEALLOCATE tnames_cursor	

	END
-- 3. Original sp_EnqueueLeo
	exec [dbo].[sp_EnqueueLeoList] @ItemNumbers
	
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
	
GO

