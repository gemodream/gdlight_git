--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_GetItemDataFromOrderBatchItem]    Script Date: 04/22/2015 18:55:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetItemDataFromOrderBatchItem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetItemDataFromOrderBatchItem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetItemDataFromOrderBatchItem] --128345,1,0,112--410442,1,19 --87557,001,8,429
(
@GroupCode int,
@BatchCode int,
@ItemCode int,
@MeasureCode int = 0,
@PartID int = 0,
@OldNumbers int = 0,
@AuthorID dnSmallID = 1,
@AuthorOfficeID dnTinyID = 1
)
as

declare	
@FullShapeName VARCHAR(50),
@SarinGroup VARCHAR(50),
@GirdleFrom VARCHAR(50),
@GirdleTo VARCHAR(50),
@Girdle VARCHAR(50),
@FullGirdle VARCHAR(50),
@GirdleType VARCHAR(50),
@ShortReportName VARCHAR(50),
@PartTypeID int,
@AddMeasureCode INT,
@AddMeasureName VARCHAR(50),
@ValueCode INT,
@Temp VARCHAR(20),
@TempValueCode int,
@Max1 VARCHAR(10),
@Min1 VARCHAR(10),
@H_x1 VARCHAR(10),
@Measurements VARCHAR(50),
@MesSign VARCHAR,
@TotalLooseDiamondWeight decimal(10,4),
@TotalItemWeight decimal(10,4),
@myMeasureCode int,
@SplitDiamondColor VARCHAR(20),
@SplitDiamondClarity VARCHAR(20),
@myResult VARCHAR(20)

 /* Test set */

--DECLARE
--@GroupCode int,
--@BatchCode int,
--@ItemCode int,
--@MeasureCode int,
--@PartID int,
--@OldNumbers int
 
-----------------------------------------44740,1,1
--set @GroupCode = 417719--,1,1 87557 --106584--87557--,001,02--00103--00106--00101--107784--00101  82273 --44740--,001,01-- 76047--,002,04
--set @BatchCode = 1
--set @ItemCode  = 1
--set	@MeasureCode = 429
-- CONVERT(nvarchar(30), GETDATE(),107) AS UsingConvert
-------------------------------------
SET	@SplitDiamondColor = ''
SET	@SplitDiamondClarity = ''

SELECT	null as NewBatchID, null as NewItemCode, null as GroupCode, null as BatchCode,
		null as ItemCode, null as PrevGroupCode, null as PrevBatchCode, null as PrevItemCode, null as ItemTypeID, CONVERT(nvarchar(30), GETDATE(),107) as CreateDate,
		 CONVERT(nvarchar(30), GETDATE(),107) as LastModDate
INTO 	#Items0
WHERE	1 = 2

	INSERT	INTO 	#Items0
	SELECT i.NewBatchID, i.NewItemCode, i.GroupCode, i.BatchCode, i.ItemCode, 
		i1.PrevGroupCode, i1.PrevBatchCode, i1.PrevItemCode, i.ItemTypeID, CONVERT(nvarchar(30), i.CreateDate,107) as CreateDate,
		CONVERT(nvarchar(30), i.LastModifiedDate,107) as LastModDate
	FROM	v0Item i, v0item i1 
	WHERE	i1.GroupCode  =	@GroupCode
		AND	i.BatchID = i1.NewBatchID
		AND	i.ItemCode = i1.NewItemCode
		and (i1.BatchCode = @BatchCode or (ISNULL(@BatchCode,0) = 0))
		and (i1.ItemCode = @ItemCode  or (ISNULL(@ItemCode,0) = 0))

SELECT	DISTINCT NewBatchID, NewItemCode,
		dbo.GetFullGroupCode(GroupCode) + 
		REPLICATE('0', 3 - LEN(CAST(BatchCode AS VARCHAR(3))))		+ CAST(BatchCode AS VARCHAR(3)) + 
		REPLICATE('0', 2 - LEN(CAST(ItemCode AS VARCHAR(2))))		+ CAST(ItemCode AS VARCHAR(2)) as NewItemNumber,
		dbo.GetFullGroupCode(GroupCode) +  '.' + 
		REPLICATE('0', 3 - LEN(CAST(BatchCode AS VARCHAR(3)))) 		+ CAST(BatchCode AS VARCHAR(3)) + '.' + 
		REPLICATE('0', 2 - LEN(CAST(ItemCode AS VARCHAR(2)))) 		+ CAST(ItemCode AS VARCHAR(2)) as DotNewItemNumber,
		dbo.GetFullGroupCode(GroupCode) +  '.' + 
		REPLICATE('0', 3 - LEN(CAST(BatchCode AS VARCHAR(3))))		+ CAST(BatchCode AS VARCHAR(3)) as DotNewBatchNumber,
		dbo.GetFullGroupCode(PrevGroupCode) + 
		REPLICATE('0', 3 - LEN(CAST(PrevBatchCode AS VARCHAR(3))))	+ CAST(PrevBatchCode AS VARCHAR(3)) + 
		REPLICATE('0', 2 - LEN(CAST(PrevItemCode AS VARCHAR(2))))	+ CAST(PrevItemCode AS VARCHAR(2)) as OldItemNumber,
		dbo.GetFullGroupCode(PrevGroupCode) +  '.' + 
		REPLICATE('0', 3 - LEN(CAST(PrevBatchCode AS VARCHAR(3))))	+ CAST(PrevBatchCode AS VARCHAR(3)) + '.' + 
		REPLICATE('0', 2 - LEN(CAST(PrevItemCode AS VARCHAR(2))))	+ CAST(PrevItemCode AS VARCHAR(2)) as DotOldItemNumber,
		ItemTypeID, CONVERT(nvarchar(30), CreateDate,107) as CreateDate, CONVERT(nvarchar(30), LastModDate,107) as LastModDate
INTO	#Items
FROM	#Items0	
	
SELECT	DISTINCT p.*
INTO	#Parts
FROM	V0Part p, #Items
WHERE	p.ItemTypeID in (SELECT DISTINCT ItemTypeID FROM #Items) AND
		p.PartTypeID in (1, 2, 10, 11) 
--select * from #Parts
SELECT	RecheckNumber, rs.BatchID, rs.ItemCode, rs.MeasureGroupID
INTO	#Rechecks  
FROM	tblRecheckSession rs, #Items i  
WHERE	rs.BatchID = i.NewBatchID
        AND rs.ItemCode = i.NewItemCode

SELECT	v.* 
INTO	#PartValue
FROM	 V0PartValue v,#Items i
WHERE	v.BatchID = i.NewBatchID
		AND v.ItemCode =i.NewItemCode

SELECT DISTINCT i.NewBatchID,i.NewItemCode, i.NewItemNumber, i.OldItemNumber, 
		m.MeasureCode, CAST(m.MeasureName as VARCHAR(30)) MeasureName, v.ValueCode, v.BatchID,
		(CASE 	WHEN v.MeasureClass = 1											THEN v.ValueTitle
				WHEN (v.MeasureClass = 2 OR v.MeasureClass = 4)					THEN v.StringValue
/*update */		WHEN (v.MeasureClass = 3 AND m.MeasureCode not in (2, 4, 5, 160, 162, 168))	THEN   /*CAST(v.MeasureValue as VARCHAR(2000))*/ LTRIM(Str(v.MeasureValue,12,2))
				WHEN (v.MeasureClass = 3 AND m.MeasureCode in (160, 162))		THEN LTRIM(Str(v.MeasureValue,12,3))
/*update */		WHEN (v.MeasureClass = 3 AND m.MeasureCode in (2, 4, 5, 168))		THEN CAST(v.MeasureValue as VARCHAR(2000))
				
		END) ResultValue, p.PartID, p.PartName, @FullShapeName as FullShapeName, @ShortReportName as ShortShapeName, p.PartTypeID, @SarinGroup as SarinGroup
INTO 	#V1
FROM 	v0Part p, #PartValue v, tblMeasure m, #Items i
WHERE 	p.ItemTypeID = 	i.ItemTypeID AND 
		v.PartID = 	p.PartID
		AND v.BatchID = i.NewBatchID
		AND v.ItemCode =i.NewItemCode
--	AND i.BatchID = @BatchID
--	AND i.ItemCode =@NewItemCode
		AND v.RecheckNumber =
		(
			select max(RecheckNumber) from #Rechecks rs 
			where	rs.BatchID = i.NewBatchID
					AND rs.ItemCode = i.NewItemCode
					AND rs.MeasureGroupID = (select m.MeasureGroupID from tblMeasure m where m.MeasureID=v.MeasureID)
		)
		AND m.MeasureId = v.MeasureId
		AND m.MeasureCode != 106
ORDER by NewItemNumber, p.PartID
--select * from #V1 where measurecode = 4
update #V1
SET ResultValue = LTRIM(Str(ResultValue, 10,0)) where measurecode = 65

/*update for not rounded weight */	
update #V1
SET ResultValue =CAST([dbo].[CalcCaratWeight](ResultValue) as numeric(10,2)) where measurecode in (2, 4, 5, 168)

--SET ResultValue = dbo.CalcCaratWeight(ResultValue) where measurecode in (2, 4, 5, 168)

UPDATE	#V1
SET		FullShapeName =  s.LongReportName, ShortShapeName = s.ShortReportName, SarinGroup = s.SarinGroup
FROM	#v1 , V0Shape s
WHERE 	#v1.ValueCode = s.ShapeCode AND #v1.MeasureCode = 8

	SET	@Girdle = ''
	SET	@FullGirdle = ''
	SET	@GirdleFrom = ''
	SET	@GirdleTo = ''
	SET	@GirdleType = ''

	SET NOCOUNT OFF
	
	SELECT	#V1.*, @GirdleFrom as GirdleFrom, @GirdleType as GirdleType, 
			@GirdleTo as GirdleTo, @Girdle as Girdle, @FullGirdle as FullGirdle
	INTO	#Girdle1
	FROM	#V1 WHERE #V1.MeasureCode = 93 --IN (21, 93, 94)
	ORDER BY NewBatchID, NewItemCode, MeasureCode
	
IF @@ROWCOUNT <> 0
BEGIN
	UPDATE	#Girdle1
	SET		GirdleFrom = ISNULL(#V1.ResultValue,'')
	FROM	#Girdle1, #V1 
	WHERE	#V1.NewBatchID = #Girdle1.NewBatchID AND 
			#V1.NewItemCode =  #Girdle1.NewItemCode AND 
			#V1.MeasureCode = 93	and 
			#V1.PartID = #Girdle1.PartID

	UPDATE	#Girdle1
	SET		GirdleTo = ISNULL(#V1.ResultValue,'')
	FROM	#Girdle1, #V1 
	WHERE	#V1.NewBatchID = #Girdle1.NewBatchID AND 
			#Girdle1.NewItemCode =  #V1.NewItemCode AND 
			#V1.MeasureCode = 94	and 
			#V1.PartID = #Girdle1.PartID
	
	UPDATE	#Girdle1
	SET		GirdleType = ISNULL(#V1.ResultValue,'')
	FROM	#Girdle1, #V1 
	WHERE	#V1.NewBatchID = #Girdle1.NewBatchID AND 
			#Girdle1.NewItemCode =  #V1.NewItemCode AND 
			#V1.MeasureCode = 21	and 
			#V1.PartID = #Girdle1.PartID

	UPDATE #Girdle1
	SET Girdle = (CASE	WHEN ((GirdleFrom = GirdleTo) OR (GirdleFrom <> '' and GirdleTo = ''))	Then	GirdleFrom
						WHEN (GirdleFrom <> '' and GirdleTo <> '')	Then	GirdleFrom + ' to ' + GirdleTo
				 END)

	UPDATE #Girdle1
	SET FullGirdle = (CASE	WHEN LTRIM(RTRIM(Girdle)) <> '' and LTRIM(RTRIM(GirdleType)) <> ''	Then Girdle + ',' + GirdleType
							WHEN LTRIM(RTRIM(Girdle)) <> ''	and LTRIM(RTRIM(GirdleType)) = ''	Then Girdle
						END)

	SET		@AddMeasureCode = 600
	SET		@AddMeasureName = 'Girdle'
	SET		@ValueCode = null
	
	INSERT #V1
	SELECT	NewBatchID, NewItemCode, NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, 
			null, BatchID, Girdle, PartID, PartName, FullShapeName, ShortShapeName, PartTypeID, SarinGroup
	FROM	#Girdle1
	
	SET		@AddMeasureCode = 601
	SET		@AddMeasureName = 'FullGirdle'
	SET		@ValueCode = null	
	
	INSERT #V1
	SELECT	NewBatchID, NewItemCode, NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, 
			null, BatchID, FullGirdle, PartID, PartName, FullShapeName, ShortShapeName, PartTypeID, SarinGroup
	FROM	#Girdle1
			
END
	SET		@Max1 = ''
	SET		@Min1 = ''
	SET		@H_x1 = ''
	SET		@Measurements = ''
	SET		@MesSign = 'x'
	
	SET NOCOUNT OFF
	
	SELECT	#V1.*, @Max1 as Max1, @Min1 as Min1, 
			@H_x1 as H_x1, @Measurements as Measurements, @MesSign as MesSign
	INTO	#Measurements
	FROM	#V1 WHERE #V1.MeasureCode = 11 --IN (21, 93, 94)
	ORDER BY NewBatchID, NewItemCode, MeasureCode

IF @@ROWCOUNT <> 0
BEGIN
	--DELETE	#V1
	--FROM #V1
	--WHERE	MeasureCode = 106

	UPDATE	#Measurements
	SET		SarinGroup = ISNULL(#V1.SarinGroup,'')
	FROM	#Measurements, #V1 
	WHERE	#V1.NewBatchID = #Measurements.NewBatchID AND 
			#V1.NewItemCode =  #Measurements.NewItemCode AND 
			#V1.MeasureCode = 8	and 
			#V1.PartID = #Measurements.PartID

	UPDATE	#Measurements
	SET		Max1 = ISNULL(#V1.ResultValue,0)
	FROM	#Measurements, #V1 
	WHERE	#V1.NewBatchID = #Measurements.NewBatchID AND 
			#V1.NewItemCode =  #Measurements.NewItemCode AND 
			#V1.MeasureCode = 11	and 
			#V1.PartID = #Measurements.PartID
			
	UPDATE	#Measurements
	SET		Min1 = ISNULL(#V1.ResultValue,0)
	FROM	#Measurements, #V1 
	WHERE	#V1.NewBatchID = #Measurements.NewBatchID AND 
			#V1.NewItemCode =  #Measurements.NewItemCode AND 
			#V1.MeasureCode = 12	and 
			#V1.PartID = #Measurements.PartID
			
	UPDATE	#Measurements
	SET		H_x1 = ISNULL(#V1.ResultValue,0)
	FROM	#Measurements, #V1 
	WHERE	#V1.NewBatchID = #Measurements.NewBatchID AND 
			#V1.NewItemCode =  #Measurements.NewItemCode AND 
			#V1.MeasureCode = 13	and 
			#V1.PartID = #Measurements.PartID
			
	UPDATE	#Measurements
	SET		MesSign = '-'
	WHERE	#Measurements.SarinGroup  like '%round%' or #Measurements.SarinGroup like '%RdBrill%'
	
	UPDATE	#Measurements 
	SET		Measurements = Max1 + ' ' + MesSign + ' ' + Min1 + ' x ' + H_x1 -- CAST(H_x1 as VARCHAR(10)) --LTRIM(Str(H_x1,10,2))	
	WHERE	ISNULL(Max1,'') <> '' and ISNULL(Min1,'') <> '' and ISNULL(H_x1,'') <> ''
	
	SET		@AddMeasureCode = 106
	SET		@AddMeasureName = 'Measurements'
	SET		@ValueCode = null
	
	INSERT #V1
	SELECT	NewBatchID, NewItemCode, NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, 
			null, BatchID, Measurements, PartID, PartName, FullShapeName, ShortShapeName, PartTypeID, SarinGroup
	FROM	#Measurements	
							
--select * from 	#V1	
END	
	-----------------
	UPDATE #V1
	SET ResultValue = UPPER(LTRIM(RTRIM(ResultValue)))
	WHERE MeasureCode in (18, 19, 25)
	
	SELECT	NewItemNumber, OldItemNumber, MeasureCode, MeasureName, ValueCode, BatchID,	ResultValue, PartID, PartName, NewBatchID,  NewItemCode 
	INTO	#RESULT	
	FROM	#V1
	WHERE	1 = 2
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, MeasureCode, MeasureName, ValueCode, BatchID,	ResultValue, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#V1
	WHERE 	MeasureCode != 8 --AND
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, MeasureCode, MeasureName, ValueCode, BatchID, 
			ResultValue, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#V1
	WHERE 	MeasureCode = 8 AND
			(not FullShapeName is null and LTRIM(RTRIM(FullShapeName)) <> '')	AND
			(not ShortShapeName is null and LTRIM(RTRIM(ShortShapeName)) <> '')

	SET		@AddMeasureCode = 501
	SET		@AddMeasureName = 'FullShapeName'
	SET		@ValueCode = null
	
	INSERT	INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, @ValueCode, BatchID, 
			FullShapeName, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#V1
	WHERE 	MeasureCode = 8 AND
			(not FullShapeName is null and LTRIM(RTRIM(FullShapeName)) <> '')	AND
			(not ShortShapeName is null and LTRIM(RTRIM(ShortShapeName)) <> '')
	
	SET		@AddMeasureCode = 502
	SET		@AddMeasureName = 'ShortShapeName'
	SET		@ValueCode = null
		
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, @ValueCode, BatchID, 
			ShortShapeName, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#V1
	WHERE 	MeasureCode = 8 AND
	(not FullShapeName is null and LTRIM(RTRIM(FullShapeName)) <> '')	AND
	(not ShortShapeName is null and LTRIM(RTRIM(ShortShapeName)) <> '')
	
	SET		@AddMeasureCode = 503
	SET		@AddMeasureName = 'TolkShapeName'
	SET		@ValueCode = null
		
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, @AddMeasureCode, @AddMeasureName, @ValueCode, BatchID, 
			FullShapeName, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#V1
	WHERE 	MeasureCode = 8 AND
	(not FullShapeName is null and LTRIM(RTRIM(FullShapeName)) <> '')	
	
	UPDATE #RESULT
	SET		ResultValue = 'TOLKOWSKY IDEAL CUT-RBC' 
	WHERE	MeasureCode = 503 AND ResultValue like 'round bril%'
	
	UPDATE #RESULT
	SET		ResultValue = 'TOLKOWSKY IDEAL CUT-PR' 
	WHERE	MeasureCode = 503 AND ResultValue like 'princess%'
	
	SELECT	v.NewItemNumber, v.OldItemNumber, MeasureCode, MeasureName, ValueCode, 
			v.BatchID, ResultValue, PartID, PartName, v.NewBatchID,  v.NewItemCode,
			i.DotNewItemNumber, i.DotOldItemNumber, i.DotNewBatchNumber, i.CreateDate, i.LastModDate
	INTO	#Additional  
	FROM	#V1 v, #Items i
	WHERE 	v.NewBatchID = i.NewBatchID		AND
			v.NewItemCode = i.NewItemCode	AND
			MeasureCode = 92

	UPDATE #Additional
	SET CreateDate = DATENAME(month,CreateDate) + ' ' + CAST(DATEPART(day, CreateDate) as VARCHAR(2)) + ', ' + CAST(DATEPART(year, CreateDate) as VARCHAR(4))
	
	UPDATE #Additional
	SET LastModDate = CAST(DATEPART(month, LastModDate) as VARCHAR(2)) + '/' + CAST(DATEPART(day, LastModDate) as VARCHAR(2)) + '/' + CAST(DATEPART(year, LastModDate) as VARCHAR(4))
	
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 900, 'Item #', null, BatchID, OldItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 901, 'Batch #', null, BatchID, Substring(OldItemNumber,1, LEN(OldItemNumber) - 2) , PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional	
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 902, 'Report Number', null, BatchID, OldItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 903, 'DotNewItemNumber', null, BatchID, DotNewItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 904, 'DotOldItemNumber', null, BatchID, DotOldItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 910, 'NewItemNumber', null, BatchID, NewItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 911, 'OldItemNumber', null, BatchID, OldItemNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 912, 'DotNewBatchNumber', null, BatchID, DotNewBatchNumber, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 920, 'ItemCode', null, BatchID, SUBSTRING(OldItemNumber, LEN(OldItemNumber) - 1, 2), PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 999, 'ConvertedDate', null, BatchID, CreateDate, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional	
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 998, 'LastModDate', null, BatchID, LastModDate, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional	
	
	SELECT	cp.CustomerProgramName, cp.CPPropertyCustomerID, CP.CustomerStyle, b.BatchID, CP.Description as CPComment, CP.Comment as CPDescription, c.CompanyName
	INTO	#CPData
	FROM	v0CustomerProgram cp, v0Batch b, v0Customer c
	WHERE	cp.CPID = b.CPID AND
			b.BatchID IN (SELECT DISTINCT NewBatchID FROM #RESULT) AND
			c.CustomerID = b.CustomerID
----select * from #CPData

	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 905, 'CPSKU', null, ad.BatchID, cp.CustomerProgramName, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 906, 'Customer Style', null, ad.BatchID, CP.CustomerStyle, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 907, 'CPCustomerID', null, ad.BatchID, cp.CPPropertyCustomerID, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 908, 'CPDescription', null, ad.BatchID, CPComment, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 909, 'CPComment', null, ad.BatchID, CPDescription, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 930, 'CalcItemTotalWeight', null, ad.BatchID, null, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 404, 'TotalLooseDiamondWeight', null, ad.BatchID, null, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID	

	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 427, 'SplitDiamondColor', null, ad.BatchID, @SplitDiamondColor, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID	
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 429, 'SplitDiamondClarity', null, ad.BatchID, @SplitDiamondClarity, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID	
	
	INSERT INTO #RESULT
	SELECT	NewItemNumber, OldItemNumber, 500, 'CompanyName', null, ad.BatchID, CompanyName, PartID, PartName, NewBatchID,  NewItemCode  
	FROM	#Additional ad, #CPData cp
	WHERE	ad.BatchID = cp.BatchID			
	
	DECLARE 
	@NewItemNumber as VARCHAR(20),
	@OldItemNumber as VARCHAR(20),
	@spfMeasureCode as VARCHAR(20),
	@spMeasureName as VARCHAR(50),
	@spValueCode as VARCHAR(20),
	@spBatchID as VARCHAR(20),
	@ValueTitle as VARCHAR(50),
	@spPartID as VARCHAR(20),
	@PartName as VARCHAR(50),
	@spNewBatchID as VARCHAR(20),
	@spNewItemCode as VARCHAR(20)	
	
	SET NOCOUNT OFF
	
	SELECT	* 
	INTO	#SpecialMeasure
	FROM	vSpecialMeasure
	
	INSERT	INTO #SpecialMeasure
	SELECT	null, '', 9, 9, 'External Comment', 9
	
	INSERT	INTO #SpecialMeasure
	SELECT	null, '', 26, 26, 'Internal Comment', 26	
	
	SELECT	ad.NewItemNumber, ad.OldItemNumber, spf.MeasureCode,  spf.MeasureName, 
			spf.ValueCode, ad.BatchID, spf.ValueTitle as ResultValue, p.PartID, p.PartName, ad.NewBatchID, ad.NewItemCode
	INTO	#SpField
	FROM	#Additional ad, #SpecialMeasure spf, #Parts p

	INSERT INTO #SpField
	SELECT	ad.NewItemNumber, ad.OldItemNumber, spf.MeasureCode,  spf.MeasureName, 
			null as ValueCode, ad.BatchID, null as ResultValue, p.PartID, p.PartName, ad.NewBatchID, ad.NewItemCode
	FROM	#Additional ad, tblMeasure spf, #Parts p
			WHERE spf.MeasureCode in (4,5)
--select * from  #SpField
IF @@ROWCOUNT <> 0
BEGIN
	DECLARE MyCalculation CURSOR FOR
	SELECT	NewItemNumber, OldItemNumber, MeasureCode, MeasureName, 
			ValueCode, BatchID, ResultValue, PartID, PartName, NewBatchID, NewItemCode
	FROM 	#SpField
	ORDER BY PartID

	OPEN 	MyCalculation
	
	FETCH NEXT FROM MyCalculation
	INTO	@NewItemNumber, @OldItemNumber, @spfMeasureCode, @spMeasureName, 
			@spValueCode,	@spBatchID,	@ValueTitle, @spPartID, @PartName, @spNewBatchID, @spNewItemCode

	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		IF NOT EXISTS (SELECT * FROM #V1 
						WHERE	#V1.NewBatchID =	@spNewBatchID AND
								#V1.NewItemCode =	@spNewItemCode AND
								#V1.PartID =		@spPartID AND
								#V1.MeasureCode =	@spfMeasureCode)
		BEGIN
			SET @myResult = null
			
			INSERT INTO #RESULT
			SELECT	@NewItemNumber, @OldItemNumber, @spfMeasureCode, @spMeasureName, @spValueCode,
					@spBatchID,	@ValueTitle, @spPartID, @PartName, @spNewBatchID, @spNewItemCode
			
			IF @spfMeasureCode = 5
			BEGIN
				SET @myResult = (SELECT TOP 1 ResultValue FROM  #RESULT
								WHERE MeasureCode = 4 AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode) 
				UPDATE #RESULT
				SET ResultValue = LTRIM(Str(@myResult,12,2))
				WHERE MeasureCode = @spfMeasureCode AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode
		
			END
			
			IF @spfMeasureCode = 4
			BEGIN
				SET @myResult = (SELECT TOP 1 ResultValue FROM  #RESULT
								WHERE MeasureCode = 5 AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode) 				
				UPDATE #RESULT
				SET ResultValue = LTRIM(Str(@myResult,12,2))
				WHERE MeasureCode = @spfMeasureCode AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode
			END
					
		END
		
		FETCH NEXT FROM MyCalculation
		INTO	@NewItemNumber, @OldItemNumber, @spfMeasureCode, @spMeasureName, @spValueCode,
				@spBatchID,	@ValueTitle, @spPartID, @PartName, @spNewBatchID, @spNewItemCode	
	END
	
	CLOSE MyCalculation
	DEALLOCATE MyCalculation
	
END

DELETE #RESULT
WHERE MeasureCode = 4 AND ResultValue IS NULL

DELETE #RESULT
WHERE MeasureCode = 5 AND ResultValue IS NULL

SELECT * 
INTO	#WEIGHTS
FROM	#RESULT
WHERE	MeasureCode IN (92, 4, 27, 29)

	SET @myResult = null
	SET @SplitDiamondColor = ''
	SET	@SplitDiamondClarity = ''
	SET	@TotalLooseDiamondWeight = null

	DECLARE @myBatchID_myItemNumber VARCHAR(20)
	DECLARE @myOldWeightBatchID_myItemNumber VARCHAR(20)
	DECLARE @myOldColorBatchID_myItemNumber VARCHAR(20)
	DECLARE @myOldClarityBatchID_myItemNumber VARCHAR(20)
	
	DECLARE @ItemContainerPartID int
	DECLARE @myItemContainerPartName VARCHAR(50)
	
	SET @myOldWeightBatchID_myItemNumber = ''
	SET @myOldColorBatchID_myItemNumber = ''
	SET @myOldClarityBatchID_myItemNumber = ''
	SET	@myBatchID_myItemNumber = ''

	DECLARE MyTotalWeightCalculation CURSOR FOR
	SELECT	NewItemNumber, OldItemNumber, MeasureCode, MeasureName, 
			ValueCode, BatchID, ResultValue, PartID, PartName, NewBatchID, NewItemCode
	FROM 	#WEIGHTS
	ORDER BY NewBatchID, NewItemCode, PartID

	OPEN 	MyTotalWeightCalculation
	
	FETCH NEXT FROM MyTotalWeightCalculation
	INTO	@NewItemNumber, @OldItemNumber, @spfMeasureCode, @spMeasureName, 
			@spValueCode,	@spBatchID,	@ValueTitle, @spPartID, @PartName, @spNewBatchID, @spNewItemCode
			
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		IF  @spfMeasureCode = 92	
		BEGIN
		SET @ItemContainerPartID = @spPartID
		SET	@myItemContainerPartName = @PartName
		END
			
		SET	@myBatchID_myItemNumber = CAST(@spNewBatchID as VARCHAR(10)) + '_' + CAST(@spNewItemCode as VARCHAR(2))
		IF (@PartName like 'Diamond%') and (@spfMeasureCode = 4) and (@ItemContainerPartID is not null)
		BEGIN
			IF ISNULL(LTRIM(RTRIM(@myBatchID_myItemNumber)),'') != ISNULL(LTRIM(RTRIM(@myOldWeightBatchID_myItemNumber)),'')
			BEGIN
				SET	@myMeasureCode = 404
				SET	@TotalLooseDiamondWeight = (SELECT SUM(CAST(ResultValue as decimal(10,4))) 
												FROM	#WEIGHTS
												WHERE	MeasureCode = 4 AND 
														PartID in (SELECT PartID FROM #Parts WHERE PartTypeID = 1) AND	
														NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode) 
			
				SET	@myOldWeightBatchID_myItemNumber = @myBatchID_myItemNumber
			
				IF NOT EXISTS (SELECT * FROM	#RESULT 
										WHERE	#RESULT.NewBatchID =	@spNewBatchID AND
												#RESULT.NewItemCode =	@spNewItemCode AND
												#RESULT.PartID =		@ItemContainerPartID AND
												#RESULT.MeasureCode =	@myMeasureCode)
					BEGIN
					
						INSERT INTO #RESULT
						SELECT	@NewItemNumber, @OldItemNumber, @myMeasureCode, 'TotalLooseDiamondWeight', null, 
							@spBatchID, LTRIM(Str(@TotalLooseDiamondWeight,12,2)), @ItemContainerPartID, @myItemContainerPartName, @spNewBatchID, @spNewItemCode
					END				
				ELSE
					BEGIN
					
						UPDATE #RESULT
						SET ResultValue = LTRIM(Str(@TotalLooseDiamondWeight,12,2))
						WHERE MeasureCode = @myMeasureCode AND PartID = @ItemContainerPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode
					END	
			END		
		END
		IF (@PartName like 'Diamond%') and (@spfMeasureCode = 27) and (@ItemContainerPartID is not null)
		BEGIN
			SET @myResult = null
			SET	@myMeasureCode = 427
			SET @myResult = (SELECT TOP 1 ResultValue FROM  #WEIGHTS
								WHERE MeasureCode = 27 AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode) 
			IF ISNULL(LTRIM(RTRIM(@myBatchID_myItemNumber)),'') != ISNULL(LTRIM(RTRIM(@myOldColorBatchID_myItemNumber)),'')
			BEGIN
				SET	@SplitDiamondColor = ISNULL(LTRIM(RTRIM(@myResult)),'')
				SET	@myOldColorBatchID_myItemNumber = @myBatchID_myItemNumber
			END
			ELSE
			BEGIN 
				IF	UPPER(@SplitDiamondColor) != UPPER(ISNULL(LTRIM(RTRIM(@myResult)),''))
				SET @SplitDiamondColor = ISNULL(LTRIM(RTRIM(@SplitDiamondColor)),'') + '-' +  ISNULL(LTRIM(RTRIM(@myResult)),'')
				
			END
		
			IF NOT EXISTS (SELECT * FROM #RESULT 
								WHERE	#RESULT.NewBatchID =	@spNewBatchID AND
										#RESULT.NewItemCode =	@spNewItemCode AND
										#RESULT.PartID =		@ItemContainerPartID AND
										#RESULT.MeasureCode =	@myMeasureCode)	
					BEGIN
						INSERT INTO #RESULT
						SELECT	@NewItemNumber, @OldItemNumber, @myMeasureCode, 'SplitDiamondColor', null, 
								@spBatchID, @SplitDiamondColor, @ItemContainerPartID, @myItemContainerPartName, @spNewBatchID, @spNewItemCode
							
					END				
					ELSE
					BEGIN
						UPDATE #RESULT
						SET ResultValue = @SplitDiamondColor
						WHERE MeasureCode = @myMeasureCode AND PartID = @ItemContainerPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode
					END														
		END	
		
		IF (@PartName like 'Diamond%') and (@spfMeasureCode = 29) and (@ItemContainerPartID is not null)
		BEGIN
			SET @myResult = null
			SET	@myMeasureCode = 429
			SET @myResult = (SELECT TOP 1 ResultValue FROM  #WEIGHTS
								WHERE MeasureCode = 29 AND PartID = @spPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode) 
			IF ISNULL(LTRIM(RTRIM(@myBatchID_myItemNumber)),'') != ISNULL(LTRIM(RTRIM(@myOldClarityBatchID_myItemNumber)),'')
			BEGIN
				SET	@SplitDiamondClarity = ISNULL(LTRIM(RTRIM(@myResult)),'')
				SET	@myOldClarityBatchID_myItemNumber = @myBatchID_myItemNumber
			END
			ELSE
			BEGIN 
				IF	UPPER(@SplitDiamondClarity) != UPPER(ISNULL(LTRIM(RTRIM(@myResult)),''))
				SET @SplitDiamondClarity = ISNULL(LTRIM(RTRIM(@SplitDiamondClarity)),'') + '-' +  ISNULL(LTRIM(RTRIM(@myResult)),'')
				
			END
		
			IF NOT EXISTS (SELECT * FROM #RESULT 
								WHERE	#RESULT.NewBatchID =	@spNewBatchID AND
										#RESULT.NewItemCode =	@spNewItemCode AND
										#RESULT.PartID =		@ItemContainerPartID AND
										#RESULT.MeasureCode =	@myMeasureCode)	
					BEGIN
						INSERT INTO #RESULT
						SELECT	@NewItemNumber, @OldItemNumber, @myMeasureCode, 'SplitDiamondClarity', null, 
								@spBatchID, @SplitDiamondClarity, @ItemContainerPartID, @myItemContainerPartName, @spNewBatchID, @spNewItemCode
							
					END				
					ELSE
					BEGIN
						UPDATE #RESULT
						SET ResultValue = @SplitDiamondClarity
						WHERE MeasureCode = @myMeasureCode AND PartID = @ItemContainerPartID AND NewBatchID = @spNewBatchID AND NewItemCode = @spNewItemCode
					END														
		END	
			
		FETCH NEXT FROM MyTotalWeightCalculation
		INTO	@NewItemNumber, @OldItemNumber, @spfMeasureCode, @spMeasureName, @spValueCode,
				@spBatchID,	@ValueTitle, @spPartID, @PartName, @spNewBatchID, @spNewItemCode
	END			
		CLOSE MyTotalWeightCalculation
		DEALLOCATE MyTotalWeightCalculation		
	

IF ISNULL(@OldNumbers,0) = 1
BEGIN
	DELETE 	#RESULT
	FROM 	#RESULT
	WHERE 	NewItemNumber = OldItemNumber
END

IF ISNULL(@PartID,0) = 0
BEGIN	
IF (@MeasureCode is null) or (@MeasureCode = 0)
	BEGIN
	SELECT DISTINCT * /*, @GroupCode as GroupCode, ISNULL(@BatchCode,0) as BatchCode, ISNULL(@ItemCode,0) as ItemCode */ FROM #RESULT
	ORDER BY NewItemNumber, PartID, MeasureCode
	END
ELSE
	BEGIN
	SELECT DISTINCT * /*, @GroupCode as GroupCode, ISNULL(@BatchCode,0) as BatchCode, ISNULL(@ItemCode,0) as ItemCode */ FROM #RESULT
	WHERE	MeasureCode = @MeasureCode
	ORDER BY NewItemNumber, PartID, MeasureCode
	END
END
ELSE
BEGIN	
IF (@MeasureCode is null) or (@MeasureCode = 0)
	BEGIN
	SELECT DISTINCT * /*, @GroupCode as GroupCode, ISNULL(@BatchCode,0) as BatchCode, ISNULL(@ItemCode,0) as ItemCode */ FROM #RESULT
	WHERE PartID = @PartID
	ORDER BY NewItemNumber, PartID, MeasureCode
	END
ELSE
	BEGIN
	SELECT DISTINCT */*, @GroupCode as GroupCode, ISNULL(@BatchCode,0) as BatchCode, ISNULL(@ItemCode,0) as ItemCode */ FROM #RESULT
	WHERE PartID = @PartID
	AND	MeasureCode = @MeasureCode
	ORDER BY NewItemNumber, PartID, MeasureCode
	END
END

DROP TABLE #Items0
DROP TABLE #Items 
DROP TABLE #V1
DROP TABLE #RESULT
DROP TABLE #Additional
DROP TABLE #CPData
DROP TABLE #Rechecks
DROP TABLE #PartValue
DROP TABLE #Girdle1
DROP TABLE #SpField
DROP TABLE #Parts
DROP TABLE #Measurements
DROP TABLE #SpecialMeasure
DROP TABLE #WEIGHTS

GO

