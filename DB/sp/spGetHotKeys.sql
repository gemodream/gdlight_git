--Use GemoDream16
--go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHotKeys]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHotKeys]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[spGetHotKeys] 

AS
SELECT     va.ViewAccessID, va.VewAccessName, va.ViewAccessCode, m.MeasureID, m.MeasureName, mv.MeasureValueID, mv.ValueTitle, mvk.HotKeys
FROM         tblViewAccess AS va INNER JOIN
                      tblAccess_Measure AS ma ON va.ViewAccessID = ma.ViewAccessID INNER JOIN
                      tblMeasure AS m ON ma.MeasureID = m.MeasureID INNER JOIN
                      tblMeasureValue AS mv ON m.MeasureID = mv.MeasureValueMeasureID INNER JOIN
                      tblMeasureValueKeys AS mvk ON mv.MeasureValueID = mvk.MeasureValueID
WHERE     (LEN(mvk.HotKeys) > 0)
go		