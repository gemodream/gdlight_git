USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPrintedStonesByCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPrintedStonesByCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetPrintedStonesByCustomer]
(
	@DateFrom as datetime = null,
	@DateTo as datetime = null,
	@CustomerId int = null
)
AS
	declare @dateBeg date = ISNULL(@DateFrom, (select min(CreateDate) from v0ItemNoExpDate)) -- '2013-01-01' -- 
	declare @dateEnd date = ISNULL(@DateTo, GetDate())	-- '2014-01-01' -- 
	--declare @CustomerID int = NULL --152, CustomerCode = 1259
	select i.CustomerID, i.BatchID, i.ItemCode into #items
		from 
			v0ItemNoExpDate As i
		where 1=1
			and ((CAST(i.CreateDate as Date) between @dateBeg and @dateEnd))
			and (@CustomerID IS NULL or @CustomerID = i.CustomerID)
			and i.BatchID = i.NewBatchID	
	
	select  i.CustomerID, i.BatchID, i.ItemCode into #items2  
		from 
			#items As i,
			tblItemOperation As tio,
			hstItemOperation As h,
			tblDocument As doc,
			tblPrintingQueue As q
		where 1=1
			
			and i.BatchID = tio.BatchID 
			and i.ItemCode = tio.ItemCode
			
			and tio.ItemOperationHistoryID = h.ItemOperationHistoryID
			
			and tio.OperationTypeID = doc.OperationTypeID
			and doc.DocumentTypeCode not in (8,10,19,20)
			
			and q.ItemOperationID = h.ItemOperationID
			and q.State in ('Y', 'C')
			-- and CAST(q.PrintDate as date) between @dateBeg and @dateEnd
		group by i.CustomerID, i.BatchID, i.ItemCode--, doc.DocumentTypeCode
		
	select  i.CustomerID, count(i.CustomerID) as quantity  
		from 
			#items2 As i
		group by i.CustomerID
		
	drop table #items	
	drop table #items2


GO

