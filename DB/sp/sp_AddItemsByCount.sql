--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[sp_AddItemsByCount]    Script Date: 03/10/2015 01:25:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddItemsByCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AddItemsByCount]
GO

/****** Object:  StoredProcedure [dbo].[sp_AddItemsByCount]    Script Date: 03/10/2015 01:25:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- 1. sp_AddItemsByCPandOrderCode
CREATE  procedure [dbo].[sp_AddItemsByCount](
	@OrderNumber int,
	@CustomerProgramName varchar(250),
	@ItemNumber int, 
	@MemoNumber varchar(500),
	@MemoNumberID int,
	@AuthorId int, 
	@AuthorOfficeId int, 
	@CurrentOfficeId int,
	@GroupOfficeID int,
	@CPOfficeID int,
	@ParNo varchar(255) = null)
as 

DECLARE @rId varchar(150)
DECLARE @GroupID int
DECLARE @InspectedQuantity int
DECLARE @InspectedTotalWeight numeric(9,4)
DECLARE @InspectedWeightUnitID numeric(4,0)
DECLARE @VendorOfficeID numeric(4,0)
DECLARE @VendorID numeric(8,0)


DECLARE @BatchCode numeric(3,0)
DECLARE @ItemTypeID numeric(8,0)
DECLARE @ItemsQuantity int
DECLARE @ItemsWeight numeric(9,4)
DECLARE @CPID numeric(15,0)
DECLARE @BatchID numeric(15,0)
DECLARE @CreateDate datetime
DECLARE @StoredItemsQuantity numeric(2,0)
DECLARE @StateID numeric(4,0)
DECLARE @StateTargetID numeric(4,0)

DECLARE @LotNumber varchar(250)
DECLARE @Weight numeric(9,4)
DECLARE @WeightUnitID numeric(4,0)
DECLARE @CustomerItemWeight numeric(9,4)
DECLARE @CustomerItemWeightUnitID numeric(4,0)
DECLARE @PrevItemCode int
DECLARE @PrevBatchCode int
DECLARE @PrevGroupCode int
DECLARE @PrevOrderCode int
DECLARE @ItemComment varchar(2000)
DECLARE @ItemCode numeric(2,0)

DECLARE @Price decimal(19,4)
DECLARE @ViewAccessCode numeric(4,0)

-- begin transaction
CREATE TABLE #Batches
	(
	BatchID int NULL
	)

SET @WeightUnitID=2
SET @CustomerItemWeightUnitID=2
	
SELECT @InspectedQuantity = ItemsQuantity, 
	@GroupID = GroupID FROM tblGroup
WHERE groupcode = @OrderNumber

IF @InspectedQuantity IS null
	SET @InspectedQuantity = @ItemNumber
ELSE
	SET @InspectedQuantity = @InspectedQuantity + @ItemNumber

select @VendorOfficeID = VendorOfficeID, @VendorID = VendorID, 
	@InspectedTotalWeight = InspectedTotalWeight, @InspectedWeightUnitID = InspectedWeightUnitID
	 from hstGroup where GroupID = @GroupID and GroupHistoryID=(select GroupHistoryID from tblGroup where GroupID = @GroupID)


EXEC spItemizingUpdateGroup @rId OUTPUT , @GroupOfficeID, @GroupID, @InspectedQuantity, @InspectedTotalWeight, @InspectedWeightUnitID, @VendorOfficeID, @VendorID, @CurrentOfficeID, @AuthorID, @AuthorOfficeID


SELECT TOP 1 @CPID = CPID, @ItemTypeID = ItemTypeID 
	FROM v1CustomerProgram 
	WHERE 
		(NOT(IsCopy=1) or IsCopy IS NULL) 
		AND CustomerProgramName=@CustomerProgramName
		and customerid = (select top 1 customerid from v0group where ordercode = @OrderNumber)
	

WHILE @ItemNumber > 0
BEGIN
	IF @ItemNumber >= 25
	BEGIN
		SET @StoredItemsQuantity = 25
		SET @ItemNumber = @ItemNumber - 25
	END
	ELSE
	BEGIN
		SET @StoredItemsQuantity = @ItemNumber
		SET @ItemNumber = 0
	END
	
	SET @BatchID = null
	EXEC spAddBatch @rId OUTPUT , @GroupOfficeID, @GroupID, @BatchCode, @ItemTypeID, @ItemsQuantity, @ItemsWeight, @CPOfficeID, @CPID, @BatchID, @CreateDate, @AuthorID, @AuthorOfficeID, @StoredItemsQuantity, @StateID, @StateTargetID, @CurrentOfficeID, @MemoNumber, @MemoNumberID
	SET @BatchID = @rId

	insert into #Batches 
		(BatchID) values (@BatchID)
	
	WHILE @StoredItemsQuantity > 0
	BEGIN
		SET @ItemCode = @StoredItemsQuantity 
		SET @StoredItemsQuantity = @StoredItemsQuantity - 1
		SET @Price = -2
		SET @ViewAccessCode = 2
		EXEC spAddItem @rId OUTPUT , @CreateDate, @AuthorID, @AuthorOfficeID, @LotNumber, @ParNo, @Weight, @WeightUnitID, @CustomerItemWeight, @CustomerItemWeightUnitID, @StateID, @StateTargetID, @PrevItemCode, @PrevBatchCode, @PrevGroupCode, @PrevOrderCode, @ItemComment, @BatchID, @ItemCode, @CurrentOfficeID
		EXEC spAddInvoice @rId OUTPUT , @Price, @ViewAccessCode, @GroupOfficeID, @GroupID, @BatchID, @ItemCode, @CurrentOfficeID, @AuthorID, @AuthorOfficeID
	END
	EXEC spSetPrefilledMeasuresFromCP @rId OUTPUT , @AuthorID, @AuthorOfficeID, @CurrentOfficeID, @BatchID	
END

-- 2. sp_GetBatchInfoFromList

CREATE TABLE #BatchInfo
(
	BatchID int,
	OrderCode int,
	BatchCode int,
	ItemsQuantity int,
	StoredItemsQuantity int,
	CustomerProgramName varchar(250)
)
insert into #BatchInfo (BatchID, OrderCode, BatchCode, ItemsQuantity, StoredItemsQuantity, CustomerProgramName)	
select 	v0Batch.BatchID,
	v0Batch.OrderCode,
	v0Batch.BatchCode,
	v0Batch.ItemsQuantity,
	v0Batch.StoredItemsQuantity,
	v0CustomerProgram.CustomerProgramName
from 
	v0Batch,
	v0CustomerProgram
where v0Batch.BatchID in (select BatchID from #Batches)
		and	v0Batch.cpid=v0CustomerProgram.cpid
order by v0Batch.OrderCode,
	v0Batch.BatchCode
	
-- 3. wspSetBatchHistory
declare @RecordTimeStamp DateTime
select @RecordTimeStamp = GETDATE()

insert trkBatchHistory (FormID, UserID, EventID, BatchID, NumberOfItemsAffected, NumberOfItemsInBatch, RecordTimeStamp)
select 19 as FormID, @AuthorId as UserID, 5 as EventID, BatchID, 
	StoredItemsQuantity, StoredItemsQuantity,  
	@RecordTimeStamp as RecordTimeStamp  
from #BatchInfo

-- result
SELECT * FROM #BatchInfo



GO

