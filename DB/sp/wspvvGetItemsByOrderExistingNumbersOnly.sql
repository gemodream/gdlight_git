--USE [GemoDream16]
--GO
/****** Object:  StoredProcedure [dbo].[wspvvGetItemsByOrderExistingNumbersOnly]    Script Date: 03/12/2015 11:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[wspvvGetItemsByOrderExistingNumbersOnly] 
(
	@OrderCode integer)
AS
BEGIN
select i.ItemCode,it.ItemTypeName, b.MemoNumber,i.LotNumber,b.BatchCode, i.orderCode, b.GroupID, b.BatchID
	from v0Item i 
	inner join v0Batch b on i.BatchID=b.BatchID 
	inner join v0ItemType it on it.ItemTypeID=b.ItemTypeID 
	where @OrderCode = i.OrderCode
  and i.batchid=i.NewBatchID

order by i.prevordercode, i.prevbatchcode, i.previtemcode




select i.ItemCode,it.ItemTypeName, b.MemoNumber,i.LotNumber,b.BatchCode, i.orderCode, b.GroupID, i.BatchID,
       i2.GroupCode newGroupCode, i2.BatchCode newBatchCode, i2.ItemCode newItemCode, i2.BatchID newBatchID
	from v0Item i 
	inner join v0Batch b on i.BatchID=b.BatchID 
	inner join v0ItemType it on it.ItemTypeID=b.ItemTypeID 
  left outer join v0Item i2 on
    i.NewBatchID = i2.BatchID and i.NewItemCode = i2.ItemCode
	where @OrderCode = i.OrderCode
  and i.batchid!=i.NewBatchID

order by i.prevordercode, i.prevbatchcode, i.previtemcode

END

