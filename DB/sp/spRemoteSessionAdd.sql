USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoteSessionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoteSessionAdd]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spRemoteSessionAdd]
(
	@Ip varchar(20),
	@CountryCode varchar(20),
	@ZipCode varchar(20) = NULL,
	@UserId int
)
as
	insert into tblRemoteSession(Ip, CountryCode, ZipCode, UserId)
	values (@Ip, @CountryCode, @ZipCode, @UserId)

GO

