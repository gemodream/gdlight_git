--USE [GemoDream16]
--GO

/****** Object:  StoredProcedure [dbo].[spGetFullOrder]    Script Date: 03/09/2015 06:06:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
exec spGetFullOrder 64823
exec sp_RulesTracking24 6482300000, 0
exec spRulesTracking24FullOrder 6482300000, 0

exec [wspvvGetItemsByOrderExistingNumbersOnly] 64823
exec [wspvvGetItemsByBatchExistingNumbersOnly] 75207
*/
ALTER     PROCEDURE [dbo].[spGetFullOrder]
(
 @GroupCode int
)

AS

select
v.OrderCode as NewOrderCode,v1.BatchID,v1.NewBatchID
into #Items
from 	v0Item v,v0Item v1
where 1 = 2

insert into #Items
select 
v.OrderCode as NewOrderCode, v1.BatchID, v1.NewBatchID
from 	v0Item v,v0Item v1
where @GroupCode = v1.GroupCode 
 and v.BatchID = v1.NewBatchID and v.ItemCode=v1.NewItemCode
select 	b.* into #Batch from v0Batch b
WHERE 
  b.BatchID in (select BatchID from #Items where BatchID = NewBatchID and (@GroupCode = NewOrderCode  or (@GroupCode is null))) 

 
-- 1. spGetBatchByCode
------ 0. BatchList ----------
select * from #Batch order by BatchID
------ 1. PathToPicture -----------
select cp.Path2Picture, v0Batch.BatchID from v0CustomerProgram cp, v0Batch, #Batch bl 
where 
	cp.CPOfficeID = v0Batch.CPOfficeID and 
	cp.CPID = v0Batch.CPID and v0Batch.BatchID = bl.BatchID

-- LABEL Documents
SELECT  doc.*, b.BatchId
FROM tblCustomerProgram cp,
     hdtCPDoc cpd,
     hdtCPDoc_Operation cpdo,
     tblDocument doc,
     tblBatch b,
     #Batch bb    
WHERE cp.CustomerProgramHistoryID = cpd.CustomerProgramHistoryID
  AND cp.CPID = b.CPID
  AND cp.CPOfficeID = b.CPOfficeID
  AND cpd.CPDocID = cpdo.CPDocID
  AND doc.OperationTypeID = cpdo.OperationTypeID
  AND b.BatchID = bb.BatchID
  AND doc.DocumentTypeCode = 8
  
-- 2. spGetDocumentValue (Document structure)

SELECT  doc.DocumentID, doc.DocumentName, bb.BatchId into #Docs
FROM tblCustomerProgram cp,
     hdtCPDoc cpd,
     hdtCPDoc_Operation cpdo,
     tblDocument doc,
     tblBatch b,
     #Batch bb    
WHERE cp.CustomerProgramHistoryID = cpd.CustomerProgramHistoryID
  AND cp.CPID = b.CPID
  AND cp.CPOfficeID = b.CPOfficeID
  AND cpd.CPDocID = cpdo.CPDocID
  AND doc.OperationTypeID = cpdo.OperationTypeID
  and DocumentTypeCode = 10
  AND b.BatchID = bb.BatchId

------ 2. Document Structure ----------
SELECT distinct dv.*, d.BatchID, d.DocumentName FROM dbo.tblDocumentValue dv, #Docs d
WHERE dv.DocumentID = d.DocumentID 
ORDER BY d.BatchID, dv.DocumentID

drop table #Items
drop table #Batch
drop table #Docs

-- 3. spRulesTracking24FullOrder
-------- 3. 4. SpRulesTracking24: Rules, ItemFailed -----------
declare @FullOrder bigint
select @FullOrder = cast (@GroupCode as bigint) * 100000
exec spRulesTracking24FullOrder @FullOrder, 0

-------- 5. 6. [wspvvGetItemsByOrderExistingNumbersOnly]: Items, MovedItems -----------
exec [wspvvGetItemsByOrderExistingNumbersOnly] @GroupCode
--exec spRulesTracking24FullOrder 6482300000, 0

-------- 7. Item Values -----------
exec [spGetItemDataFromOrderBatchItemFull] @GroupCode, 0,0



GO

