USE [GemoDream16]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportItemDetails_statsReportItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportItemDetails]'))
ALTER TABLE [dbo].[statsReportItemDetails] DROP CONSTRAINT [FK_statsReportItemDetails_statsReportItem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportItemDetails]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportItemDetails]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[statsReportItemDetails](
	[item_id] [int] NOT NULL,
	[part_id] [int] NOT NULL,
	[measure_id] [int] NOT NULL,
	[reason_id] [int] NOT NULL,
	[currValue] [nvarchar](50) NULL,
	[minValue] [nvarchar](50) NULL,
	[maxValue] [nvarchar](50) NULL,
	[rejection_type] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[statsReportItemDetails]  WITH CHECK ADD  CONSTRAINT [FK_statsReportItemDetails_statsReportItem] FOREIGN KEY([item_id])
REFERENCES [dbo].[statsReportItem] ([item_id])
GO

ALTER TABLE [dbo].[statsReportItemDetails] CHECK CONSTRAINT [FK_statsReportItemDetails_statsReportItem]
GO

