--USE [GemoDream16]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeasureValueKeys]') AND type in (N'U'))
DROP TABLE [dbo].[tblMeasureValueKeys]
GO

CREATE TABLE [dbo].[tblMeasureValueKeys](
	[MeasureValueID] [dbo].[dnSmallID],
	[HotKeys] [nvarchar](10) NULL
) 

GO

insert into tblMeasureValueKeys (MeasureValueID)
	select  MeasureValueID from tblMeasureValue 
go

-- set HotKeys
-- Color Grade (measureId = 27)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'DD'	WHERE MeasureValueId = 12	-- D
UPDATE	tblMeasureValueKeys SET	HotKeys = 'EE'	WHERE MeasureValueId = 13	-- E
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FF'	WHERE MeasureValueId = 14	-- F
UPDATE	tblMeasureValueKeys SET	HotKeys = 'GG'	WHERE MeasureValueId = 15	-- G
UPDATE	tblMeasureValueKeys SET	HotKeys = 'HH'	WHERE MeasureValueId = 16	-- H
UPDATE	tblMeasureValueKeys SET	HotKeys = 'II'	WHERE MeasureValueId = 17	-- I
UPDATE	tblMeasureValueKeys SET	HotKeys = 'JJ'	WHERE MeasureValueId = 18	-- J
UPDATE	tblMeasureValueKeys SET	HotKeys = 'KK'	WHERE MeasureValueId = 19	-- H
UPDATE	tblMeasureValueKeys SET	HotKeys = 'HH'	WHERE MeasureValueId = 20	-- H
UPDATE	tblMeasureValueKeys SET	HotKeys = 'MM'	WHERE MeasureValueId = 21	-- M
UPDATE	tblMeasureValueKeys SET	HotKeys = 'NOO' WHERE MeasureValueId = 22	-- N-O
UPDATE	tblMeasureValueKeys SET	HotKeys = 'OPP' WHERE MeasureValueId = 23	-- O-P
UPDATE	tblMeasureValueKeys SET	HotKeys = 'PQQ' WHERE MeasureValueId = 24	-- P-Q
UPDATE	tblMeasureValueKeys SET	HotKeys = 'QRR' WHERE MeasureValueId = 25	-- Q-R
UPDATE	tblMeasureValueKeys SET	HotKeys = 'RSS' WHERE MeasureValueId = 26	-- R-S
UPDATE	tblMeasureValueKeys SET	HotKeys = 'STT' WHERE MeasureValueId = 27	-- S-T
UPDATE	tblMeasureValueKeys SET	HotKeys = 'TUU' WHERE MeasureValueId = 28	-- T-U
UPDATE	tblMeasureValueKeys SET	HotKeys = 'UVV' WHERE MeasureValueId = 29	-- U-V
UPDATE	tblMeasureValueKeys SET	HotKeys = 'VWW' WHERE MeasureValueId = 30	-- V-W
UPDATE	tblMeasureValueKeys SET	HotKeys = 'WXX' WHERE MeasureValueId = 31	-- W-X
UPDATE	tblMeasureValueKeys SET	HotKeys = 'XYY' WHERE MeasureValueId = 32	-- X-Y
UPDATE	tblMeasureValueKeys SET	HotKeys = 'YZZ' WHERE MeasureValueId = 33	-- Y-Z
UPDATE	tblMeasureValueKeys SET	HotKeys = 'EFF' WHERE MeasureValueId = 34	-- E-F
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FGG' WHERE MeasureValueId = 163	-- F-G
UPDATE	tblMeasureValueKeys SET	HotKeys = 'GHH' WHERE MeasureValueId = 164	-- G-H
UPDATE	tblMeasureValueKeys SET	HotKeys = 'HII' WHERE MeasureValueId = 165	-- H-I
UPDATE	tblMeasureValueKeys SET	HotKeys = 'IJJ' WHERE MeasureValueId = 166	-- I-J
UPDATE	tblMeasureValueKeys SET	HotKeys = 'JKK' WHERE MeasureValueId = 167	-- J-K
UPDATE	tblMeasureValueKeys SET	HotKeys = 'KLL' WHERE MeasureValueId = 168	-- K-L
UPDATE	tblMeasureValueKeys SET	HotKeys = 'LMM' WHERE MeasureValueId = 169	-- L-M
UPDATE	tblMeasureValueKeys SET	HotKeys = 'MNN' WHERE MeasureValueId = 170	-- M-N
UPDATE	tblMeasureValueKeys SET	HotKeys = 'NN'	WHERE MeasureValueId = 206	-- N
UPDATE	tblMeasureValueKeys SET	HotKeys = 'DEE' WHERE MeasureValueId = 5590	-- D-E
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZN'	WHERE MeasureValueId = 5591	-- N/A
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZZ'	WHERE MeasureValueId = 5631	-- Reject
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZT'	WHERE MeasureValueId = 5817	-- Mismatch
 -- Hearts and Arrows (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ha' WHERE MeasureValueId = 5522
UPDATE	tblMeasureValueKeys SET	HotKeys = 'hn' WHERE MeasureValueId = 5521
-- FF (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ff' WHERE MeasureValueId = 50
UPDATE	tblMeasureValueKeys SET	HotKeys = 'fg' WHERE MeasureValueId = 51
-- Broken Stone (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zb1' WHERE MeasureValueId = 5555
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zb2' WHERE MeasureValueId = 5659
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zb3' WHERE MeasureValueId = 5658
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zb4' WHERE MeasureValueId = 5554
-- Indented Natural (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zii' WHERE MeasureValueId = 5552
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zig' WHERE MeasureValueId = 6140
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ziq' WHERE MeasureValueId = 5553
-- Black Pique (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zpp' WHERE MeasureValueId = 5548
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zpq' WHERE MeasureValueId = 5549
UPDATE	tblMeasureValueKeys SET	HotKeys = 'zoo' WHERE MeasureValueId = 5550
UPDATE	tblMeasureValueKeys SET	 HotKeys = 'zoq' WHERE MeasureValueId = 5551
-- Clarity Grade (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = '1' WHERE MeasureValueId = 1
UPDATE	tblMeasureValueKeys SET	HotKeys = '2' WHERE MeasureValueId = 2
UPDATE	tblMeasureValueKeys SET	HotKeys = '3' WHERE MeasureValueId = 3
UPDATE	tblMeasureValueKeys SET	HotKeys = '4' WHERE MeasureValueId = 4
UPDATE	tblMeasureValueKeys SET	HotKeys = '5' WHERE MeasureValueId = 5
UPDATE	tblMeasureValueKeys SET	HotKeys = '6' WHERE MeasureValueId = 6
UPDATE	tblMeasureValueKeys SET	HotKeys = '7' WHERE MeasureValueId = 7
UPDATE	tblMeasureValueKeys SET	HotKeys = '8' WHERE MeasureValueId = 8
UPDATE	tblMeasureValueKeys SET	HotKeys = '9' WHERE MeasureValueId = 9
UPDATE	tblMeasureValueKeys SET	HotKeys = '0' WHERE MeasureValueId = 10
UPDATE	tblMeasureValueKeys SET	HotKeys = 'Q' WHERE MeasureValueId = 11
UPDATE	tblMeasureValueKeys SET	HotKeys = 'W' WHERE MeasureValueId = 185
UPDATE	tblMeasureValueKeys SET	HotKeys = 'E' WHERE MeasureValueId = 186
UPDATE	tblMeasureValueKeys SET	HotKeys = 'R' WHERE MeasureValueId = 187
UPDATE	tblMeasureValueKeys SET	HotKeys = 'T' WHERE MeasureValueId = 188
UPDATE	tblMeasureValueKeys SET	HotKeys = 'Y' WHERE MeasureValueId = 189
UPDATE	tblMeasureValueKeys SET	HotKeys = 'U' WHERE MeasureValueId = 190
UPDATE	tblMeasureValueKeys SET	HotKeys = 'I' WHERE MeasureValueId = 5796
UPDATE	tblMeasureValueKeys SET	HotKeys = 'O' WHERE MeasureValueId = 5765
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZT' WHERE MeasureValueId = 5818
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZZ' WHERE MeasureValueId = 5630
UPDATE	tblMeasureValueKeys SET	HotKeys = 'ZN' WHERE MeasureValueId = 5583
-- Polish (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'pe' WHERE MeasureValueId = 37
UPDATE	tblMeasureValueKeys SET	HotKeys = 'pv' WHERE MeasureValueId = 38
UPDATE	tblMeasureValueKeys SET	HotKeys = 'pg' WHERE MeasureValueId = 193
UPDATE	tblMeasureValueKeys SET	HotKeys = 'pf' WHERE MeasureValueId = 194
UPDATE	tblMeasureValueKeys SET	HotKeys = 'pp' WHERE MeasureValueId = 195
-- Symmetry (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'se' WHERE MeasureValueId = 40
UPDATE	tblMeasureValueKeys SET	HotKeys = 'sv' WHERE MeasureValueId = 41
UPDATE	tblMeasureValueKeys SET	HotKeys = 'sg' WHERE MeasureValueId = 5486
UPDATE	tblMeasureValueKeys SET	HotKeys = 'sf' WHERE MeasureValueId = 5487
UPDATE	tblMeasureValueKeys SET	HotKeys = 'sp' WHERE MeasureValueId = 5488
-- GirdleType (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gtp' WHERE MeasureValueId = 196
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gtf' WHERE MeasureValueId = 197
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gtb' WHERE MeasureValueId = 77
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gtsp' WHERE MeasureValueId = 78
UPDATE	tblMeasureValueKeys SET	HotKeys = 'stsf' WHERE MeasureValueId = 79
--	GirdleFrom (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf1' WHERE MeasureValueId = 5475
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf2' WHERE MeasureValueId = 5476
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf3' WHERE MeasureValueId = 5477
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf4' WHERE MeasureValueId = 5478
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf5' WHERE MeasureValueId = 5479
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf6' WHERE MeasureValueId = 5480
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf7' WHERE MeasureValueId = 5481
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gf8' WHERE MeasureValueId = 5482
-- GirdleTo (Clarity)
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt1' WHERE MeasureValueId = 5511
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt2' WHERE MeasureValueId = 5512
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt3' WHERE MeasureValueId = 5513
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt4' WHERE MeasureValueId = 5514
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt5' WHERE MeasureValueId = 5515
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt6' WHERE MeasureValueId = 5516
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt7' WHERE MeasureValueId = 5517
UPDATE	tblMeasureValueKeys SET	HotKeys = 'gt8' WHERE MeasureValueId = 5518
-- Fluorescence (Color)	
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLB' WHERE MeasureValueId = 5426
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLY' WHERE MeasureValueId = 5427
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLR' WHERE MeasureValueId = 5428
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLO' WHERE MeasureValueId = 5429
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLW' WHERE MeasureValueId = 5430
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLG' WHERE MeasureValueId = 5431
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLZ' WHERE MeasureValueId = 5570
UPDATE	tblMeasureValueKeys SET	HotKeys = 'FLBY' WHERE MeasureValueId = 5921
