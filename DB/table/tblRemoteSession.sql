USE [GemoDream16]
GO

/****** Object:  Table [dbo].[tblRemoteSession]    Script Date: 11/01/2016 11:22:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRemoteSession]') AND type in (N'U'))
DROP TABLE [dbo].[tblRemoteSession]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblRemoteSession](
	[Id] int IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Ip] [varchar](50) NOT NULL,
	[CountryCode] [varchar](10) NULL,
	[ZipCode] [varchar](10) NULL,
	[UserId] [dbo].[dnSmallID] NOT NULL,
	[LoginDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRemoteSession] ADD  DEFAULT (getdate()) FOR [LoginDate]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IndexByLoginDate] ON [dbo].[tblRemoteSession] 
(
	[LoginDate] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IndexByUserId] ON [dbo].[tblRemoteSession] 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


