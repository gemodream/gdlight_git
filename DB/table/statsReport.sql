USE [GemoDream16]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReport]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[statsReport](
		[report_id] [int] IDENTITY(1,1) NOT NULL,
		[calc_date] [datetime] NOT NULL,
		[user_id] [int] NOT NULL,
		[customerIds] [varchar](3000) NULL,
		[date_from] [datetime] NULL,
		[date_to] [datetime] NULL,
		[comments] [varchar] (1000) NULL
	) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [statsReport_PrimaryIndex] ON [dbo].[statsReport] 
(
	[report_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

END
GO


