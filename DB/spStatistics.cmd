rem example: Change_sp.cmd localhost GemoDream16 gemoAdmin16 gemoAdmin16
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spCountryOffices.sql

rem Stone Quantity Task
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStoneQuantities.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStoneQuantitiesByCustomer.sql

rem Report Quantity Task
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByCustomer1.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByDocumentType1.sql

rem Rejected Stone Task
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFailedMeasureIdsForVerify.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFailedParamsByBatch.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFailedParamsByCustomer.sql

rem Old/New numbers Task
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetItemLifeCycle.sql

rem  for magic
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetItemNumberByPrefix.sql
