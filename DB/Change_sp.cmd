rem example: Change_sp.cmd localhost GemoDream16 gemoAdmin16 gemoAdmin16
rem removed hardcoded "gemoDream16"
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetMeasureValueByPart.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetMeasureValueByPart2.sql

rem Find by Memo Number
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_FindMemoBatchEx.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_FindMemoOrderEx.sql

rem GDLight/Itemize
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_AddItemsByLots.sql

sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_AddItemsByCount.sql

rem GDLight/Bulk Update !!!
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_SaveMeasureBulk2.sql

rem Open Batch List
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_ListOpenBatchesWithStatus.sql

rem Item View (Short Report)
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_GetItemDataFromOrderBatchItem.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetItemDataFromOrderBatchItemAndCP.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_GetBatchByCode.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/wspvvGetItemsByBatchExistingNumbersOnlyEx.sql

rem Trackin II, tab History
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/index_trkBatchHistory.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/trkSpGetAgedBatchHistoryWithFilters_Max.sql

rem LeoReport II
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/itemModelTableType.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_EnqueueLeoList.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/sp_LeoOrderReports.sql

rem Full Order
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetItemDataFromOrderBatchItemFull.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spRulesTracking24FullOrder.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/wspvvGetItemsByOrderExistingNumbersOnly.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFullOrder.sql

rem Leo Report
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetLeoFullOrder.sql

rem AutoMeasure !!!
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spSetRandomMeasurementsByBatchIDWithLoginInfoEx.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx.sql

rem DeliveryLog, Add Records
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/trkSpSetDeliveryLogEx.sql

rem Find by Measue Value
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spFindItemByValue.sql

rem HotKeys
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/tblMeasureValueKeys.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetHotKeys.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetMeasuresByItemTypeAccess.sql
rem BulkUpdate, PreFilling
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spSavePreFilling.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetCPDocRuleEx.sql

rem Report Lookup
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/wspGetVirtualVaultFromReportNameEx.sql
rem 02 November 2015
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetItemDataFromOrderBatchItemFull.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spUpdateDocumentEx.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spInsertDocumentEx.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spSaveCustomerProgram.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetOrderHistory.sql
rem 01 December 2015
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spUpdateOrderEx.sql

rem 12 January 2016, Short Report New!
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFullOrder.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetLeoFullOrder.sql
rem 18 January 2016, added label documents
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFullOrder.sql

rem 25 Sep 2016, stats reports
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spCountryOffices.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spStatStoneNumbers.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStoneQuantities.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStoneQuantitiesByCustomer.sql


rem 1 NOV 2016, remote session
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/tblRemoteSession.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spRemoteSessionAdd.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetRemoteSessionByFilter.sql

rem 8 NOV 2016, stats ReportQuantity
rem sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantity.sql
rem sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByCustomer.sql
rem sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByDocumentType.sql


rem 17 NOV 2016
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetPersonsByBatchIds.sql

rem 21 NOV 2016
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByCustomer1.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReportQuantityByDocumentType1.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetPrintedStonesByCustomer.sql

