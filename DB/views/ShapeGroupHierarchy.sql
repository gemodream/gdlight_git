create view v0ShapeGroupHierarchy 
as
with ts as 
(
SELECT t.ShapeGroupID as ShapeGroupID,
       convert(varchar(100), h.ShapeGroupName) as ShapeGroupName,
       t.ParentShapeGroupID as ParentShapeGroupID,
       convert(varchar(100), h.ShapeGroupName) as FullGroupName
FROM tblShapeGroup t inner join hstShapeGroup h
on t.ShapeGroupHistoryID=h.ShapeGroupHistoryID and t.ExpireDate is null
where t.ParentShapeGroupID is null
UNION ALL
SELECT t.ShapeGroupID as ShapeGroupID,
       convert(varchar(100), h.ShapeGroupName) as ShapeGroupName,
       t.ParentShapeGroupID as ParentShapeGroupID,
       convert(varchar(100), ts.ShapeGroupName + '\' + h.ShapeGroupName) as FullGroupName
FROM tblShapeGroup t inner join hstShapeGroup h
on t.ShapeGroupHistoryID=h.ShapeGroupHistoryID and t.ExpireDate is null
inner join ts on ts.ShapeGroupID = t.ParentShapeGroupID) 
select ts.* from ts

create view v0ShapeHierarchy 
as
select ts.ShapeGroupId FixShapeGroupID, ts.FullGroupName,  d.* from 
(select max(ShapeGroupID) ShapeGroupID, FullGroupName from v0ShapeGroupHierarchy group by FullGroupName) ts
right outer join
(
SELECT
      t.ShapeID as ShapeID,
      t.ShapeGroupID as ShapeGroupID,
      h.Path2Drawing as Path2Drawing,
      t.ShapeCode as ShapeCode
FROM tblShape t, hstShape h
WHERE t.ShapeHistoryID=h.ShapeHistoryID and t.ExpireDate is null
) d on Path2Drawing like '%shapes\' + ts.FullGroupName + '\%'
where (ts.ShapeGroupID is null or ts.ShapeGroupID = (select MAX(ShapeGroupID) from v0ShapeGroupHierarchy ts2 where ts2.FullGroupName = ts.FullGroupName or ts2.FullGroupName like ts.FullGroupName + '\%' and d.Path2Drawing like '%shapes\' + ts2.FullGroupName + '\%'))
