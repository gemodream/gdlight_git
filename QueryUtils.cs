﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Xml;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Models.Stats;
using Corpt.Models.Stats.FailStats;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Net.NetworkInformation;
using Corpt.Models.Customer;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Collections;
using System.EnterpriseServices;
using System.Web.SessionState;
using System.Text;
using System.Web.UI.WebControls;

namespace Corpt.Utilities
{
    public class QueryUtils : BaseQueryUtils
    {
        public static string _domain = null;
        #region Customers
        public static CustomerModel GetCustomerName(BatchModel batchModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpGetCustomer,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = Int32.Parse(batchModel.CustomerOfficeId);
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Int32.Parse(batchModel.CustomerId);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            if (cdt.Rows.Count > 0)
            {
                var row = cdt.Rows[0];
                return new CustomerModel 
                {CustomerName = "" + row["CustomerName"], CustomerId = "" + row["CustomerId"], OfficeId = "" + row["CustomerOfficeID"]};
            }
                
            return new CustomerModel();
        }
        public static List<CustomerModel> GetCustomers(Page p)
        {
            return GetCustomers(p, false);
        }
        public static List<CustomerModel> GetCustomers(Page p, bool addAll)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetCustomers,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new CustomerModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            if (addAll)
            {
                result.Insert(0, new CustomerModel{CustomerId = null, CustomerName = "(All)"});
            }
            conn.Close();
            return result;
        }
        public static DataTable GetCustomerCompanyGroup(CustomerModel objCustomerModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerCompanyGroupByCustomerID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerID", objCustomerModel.CustomerId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            conn.Close();
            return cdt;
        }
        /*IvanB start*/
        public static List<CustomerModel> GetAllCustomersForBlockEditing(Page p)
        {
            var result = new List<CustomerModel>();
            using(SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllCustomers",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                var cda = new SqlDataAdapter(command);
                var cdt = new DataTable();
                cda.Fill(cdt);
                result = (from DataRow row in cdt.Rows select new CustomerModel(row)).ToList();
                result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            }

            return result;
        }
        /*IvanB end*/

        public static DataTable GetSkuMemoPerSku(string sku, string customerID, Page p)
        {
            try
            {
                var cdt = new DataTable();
                using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
                {
                    conn.Open();
                    var command = new SqlCommand
                    {
                        CommandText = "spGetSkuMemoPerSkuCustomer",
                        Connection = conn,
                        CommandType = CommandType.StoredProcedure,
                        CommandTimeout = p.Session.Timeout
                    };
                    command.Parameters.AddWithValue("@SkuName", sku);
                    command.Parameters.AddWithValue("@CustomerID", Convert.ToInt32(customerID));
                    var cda = new SqlDataAdapter(command);
                    cda.Fill(cdt);
                }
                return cdt;
            }
            catch
            {
                return null;
            }
        }
        public static List<MemoModel> GetMemoNumbersByCustomerByDateRange(string customerId, DateTime? dateFrom, DateTime? dateTo, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetMemoNumbersByCustomerByDateRange,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@CustomerID", SqlDbType.Int);
            command.Parameters["@CustomerID"].Value = Int32.Parse(customerId);

            if (dateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = dateFrom;
            }

            if (dateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = ((DateTime)dateTo).AddDays(1);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MemoModel(row)).ToList();
            conn.Close();
            return result;

        }
        #endregion
        
        #region Batches
        public static string GetBatchId(string batchNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpBatchExists,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@BatchFullNumber", batchNumber + "00");
            LogUtils.UpdateSpLogWithParameters(command, p);
            var reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                conn.Close();
                return "";
            }

            reader.Read();
            var batchKey = reader.GetSqlValue(reader.GetOrdinal("BatchID")).ToString();

            conn.Close();
            return batchKey;
        }
        public static List<BatchModel> GetBatches(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetBatchByCode,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BDate", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EDate", SqlDbType.Int).Value = DBNull.Value;
            if (String.IsNullOrEmpty(batchCode))
            {
                command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = DBNull.Value;
            } else
            {
                command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = Int32.Parse(batchCode);
            }
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = Int32.Parse(orderCode.Trim());
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtBatchIds = new DataTable();
            da.Fill(dtBatchIds);
            var result = (from DataRow row in dtBatchIds.Rows select new BatchModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        public static List<BatchModel> GetBatchesByBatchNumber(string batchNumber, Page p)
        {
            Utils.DissectItemNumber(batchNumber, out string orderCode, out string batchCode, out string _);
            return GetBatches(orderCode, batchCode, p);
        }

        public static List<BatchModel> GetBatchesByOrderCode(string orderCode, Page p)
        {
            return GetBatches(orderCode, null, p);
        }

        private const int IndexBatch = 0;
        private const int IndexPicture = 1;
        private const int IndexLblDocs = 2;
        private const int IndexDocStructure = 3;
        private const int IndexRules = 4;
        private const int IndexFailedItems = 5;
        private const int IndexItems = 6;
        private const int IndexMovedItems = 7;
        private const int IndexItemValues = 8;
        private const int IndexOrderedItems = 9;
        private const int IndexPrintedItems = 10;
        public const string AccountRepQueueNamePrefix = "accountrep";
        public const string AccountRepPrintInvoiceQueueNamePrefix = "accountrepinvoice";

        public static List<ShortReportModel> GetFullOrder(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetFullOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", order));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dataAdapter = new SqlDataAdapter(command);

            var ds = new DataSet("FullOrder");
            dataAdapter.Fill(ds);
            
            //-- Batches
            var batches = new List<BatchModel>();
            foreach (DataRow batchRow in ds.Tables[IndexBatch].Rows)
            {
                batches.Add(new BatchModel(batchRow));
            }

            var shortReports = new List<ShortReportModel>();
            foreach (var batchModel in batches)
            {
                var shortReport = new ShortReportModel {BatchModel = batchModel};
                //-- Label Documents
                var lblRows = ds.Tables[IndexLblDocs].Select("BatchID = " + batchModel.BatchId );
                foreach(var lblDoc in lblRows)
                {
                    shortReport.LabelDocuments.Add(new DocumentModel(lblDoc));
                }
                //-- Document structure
                var docRows = ds.Tables[IndexDocStructure].Select("BatchID = " + batchModel.BatchId);
                foreach (var docRow in docRows)
                {
                    var structModel = new DocStructModel(docRow);
                    shortReport.DocStructure.Add(structModel);
                    shortReport.DocumentId = "" + docRow["DocumentID"];
                }
                

                //-- Failed Items
                var ruleRows = ds.Tables[IndexFailedItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var ruleRow in ruleRows)
                {
                    var rule = new ItemFailedModel(ruleRow);
                    if (!rule.IsGood)
                    {
                        shortReport.ItemsFailed.Add(rule);
                    }
                }


                //-- Items
                var items = ds.Tables[IndexItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var item in items)
                {
                    shortReport.Items.Add(new ItemModel(item));
                }

                //-- Moved Items
                var movedItems = ds.Tables[IndexMovedItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var movedItem in movedItems)
                {
                    shortReport.MovedItems.Add(new MovedItemModel(movedItem));
                }

                //-- Item Values
                var itemValues = ds.Tables[IndexItemValues].Select("BatchID = " + batchModel.BatchId);
                foreach (var itemValue in itemValues)
                {
                    shortReport.ItemValues.Add(ItemValueModel.Create1(itemValue));
                }
                shortReports.Add(shortReport);

            }

            //-- Moved Items
            var itemsMoved = new List<MovedItemModel>();
            foreach (DataRow movedItem in ds.Tables[IndexMovedItems].Rows)
            {
                itemsMoved.Add(new MovedItemModel(movedItem));
            }

            return shortReports;

        }

        public static BatchModel SpGetBatch(int intBatchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpGetBatch,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };

            command.Parameters.Add(new SqlParameter("@BatchID", intBatchId));
            command.Parameters.Add(new SqlParameter("@AuthorID", p.Session["ID"].ToString()));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString()));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            if (dt.Rows.Count == 1)
            {
                return new BatchModel(dt.Rows[0]);
            }
            var up = new Exception("Can't find full Batch Number by BatchID.");
            throw up;
        }
       
        public static List<BatchModel> GetBatchesByCustomerByDateRangeByMemoNumber(string customerId, DateTime? dateFrom, DateTime? dateTo, string memoNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetBatchesByCustomerByDateRangeByMemoNumber,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@CustomerID", SqlDbType.Int);
            command.Parameters["@CustomerID"].Value = Int32.Parse(customerId);

            command.Parameters.Add("@MemoNumber", SqlDbType.VarChar).Size = 255;
            command.Parameters["@MemoNumber"].Value = memoNumber;

            if (dateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = dateFrom;
            }

            if (dateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = ((DateTime)dateTo).AddDays(1);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new BatchModel(row, true)).ToList();
            conn.Close();
            return result;
            
        }
        #endregion

        #region Document & Structure
        public static string GetDocumentId(BatchModel batchModel, Page p)
        {
            return GetDocumentId(batchModel, "10", p);
        }
        public static string GetDocumentId(BatchModel batchModel, string docType, Page p)
        {

            var documentId = "0";

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
                              {
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandText = StoredProcedures.SpGetDocumentTypeCodeByBatchId,
                                  CommandTimeout = p.Session.Timeout
                              };

            command.Parameters.AddWithValue("@BatchID", batchModel.BatchId);
            command.Parameters.AddWithValue("@AuthorId", p.Session["ID"].ToString());
            command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"].ToString());
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dataAdapter = new SqlDataAdapter(command);

            var myDocList = new DataSet();
            dataAdapter.Fill(myDocList);

            var myDataView = new DataView(myDocList.Tables[0]) { RowFilter = "DocumentTypeCode = " + docType };

            if (myDataView.Count > 0)
            {
                documentId = myDataView[0]["DocumentID"].ToString();
            }
            myDataView.Dispose();
            dataAdapter.Dispose();
            command.Dispose();
            conn.Close();
            return documentId;
        }

        public static void SetItemKmsMessage(ItemModel itemModel, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpGetKmlDffByItemNumber,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };

            command.Parameters.Add(new SqlParameter("@GroupCode", itemModel.OrderCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", itemModel.BatchCode));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemModel.ItemCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var measureItemList = new List<MeasureItemModel>();
            foreach (DataRow drKm in dt.Rows)
            {
                
                var measureItemModel = new MeasureItemModel(drKm, itemModel);
                if (measureItemModel.HasValue)
                {
                    measureItemList.Add(measureItemModel);
                }
            }
            conn.Close();

        }

        public static List<DocStructModel> GetDocStructure(string documentId, Page p)
        {
            var docStructureList = new List<DocStructModel>();
            if (documentId == "")
            {
                return docStructureList;
            }

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpGetDocumentValue,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };
            command.Parameters.AddWithValue("@DocumentID", Int32.Parse(documentId));//--1327
            command.Parameters.AddWithValue("@AuthorID", p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dataAdapter = new SqlDataAdapter(command);

            var myDocStructure = new DataSet("Doc Structure");
            dataAdapter.Fill(myDocStructure);

            docStructureList.AddRange(from DataRow row in myDocStructure.Tables[0].Rows select new DocStructModel(row));
            conn.Close();
            return docStructureList;
        }
        #endregion
       
        #region Document & Values
        public static void GetItemsAndMovedItems(ShortReportModel shortReportModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.WspvvGetItemsByBatchExistingNumbersOnly,
                                  CommandType = CommandType.StoredProcedure,
                                  Connection = conn,
                                  CommandTimeout = p.Session.Timeout
                              };
            command.Parameters.AddWithValue("@BatchID", shortReportModel.BatchModel.BatchId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dataAdapter = new SqlDataAdapter(command) { SelectCommand = { CommandTimeout = p.Session.Timeout } };

            var dsItemsByBatchExistingNumbersOnly = new DataSet();
            dataAdapter.Fill(dsItemsByBatchExistingNumbersOnly);

            //-- Items
            var dtItems = dsItemsByBatchExistingNumbersOnly.Tables[0];
            foreach (DataRow row in dtItems.Rows)
            {
                shortReportModel.Items.Add(new ItemModel(row));
            }

            //-- Moves Items
            var dtMovedItems = dsItemsByBatchExistingNumbersOnly.Tables[1].Copy();
            foreach(DataRow row in dtMovedItems.Rows)
            {
                shortReportModel.MovedItems.Add(new MovedItemModel(row));
            }
            //-- KMS Message
            foreach (var itemModel in shortReportModel.Items)
            {
                SetItemKmsMessage(itemModel, p);
            }
            conn.Close();
        }

        public static List<ItemStructModel> GetItemStructureByBatch(BatchModel batchModel, Page p)
        {

            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpGetItemStructureByBatchId,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };
            command.Parameters.Add(new SqlParameter("@BatchID", batchModel.BatchId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtItemStructure = new DataTable();
            da.Fill(dtItemStructure);

            var result = (from DataRow row in dtItemStructure.Rows select new ItemStructModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<ItemValueModel> GetItemValues(BatchModel batchModel, Page p)
        {
            return GetItemValues(batchModel, "0", p);
        }

        public static List<ItemValueModel> GetItemValues(BatchModel batchModel, string itemCode, Page p)
        {
            return GetItemValues(Convert.ToInt32(batchModel.GroupCodeFiveChars), Convert.ToInt32(batchModel.BatchCodeTreeChars), Convert.ToInt32(itemCode), p);
        }

        public static string GetBatchIdByItemNumber(string itemNumber, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
            {
                //CommandText = "spGetItemDataFromOrderBatchItemFull",//StoredProcedures.SpGetItemDataFromOrderbatchItem,
                CommandText = "spGetItemDataFromOrderBatchItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", Utils.ParseOrderCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@BatchCode", Utils.ParseBatchCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@ItemCode", Utils.ParseItemCode(itemNumber)));

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtItemValueList = new DataTable();
            da.Fill(dtItemValueList);
            var result = (from DataRow row in dtItemValueList.Rows select ItemValueModel.Create1(row)).ToList();
            conn.Close();
            return result[0].BatchId;
        }
        //IvanB 19/05 start
        public static void SetRealValues(string itemNumber, List<ExpressGradingModel> measureList, Page p)
        {

            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemDataFromOrderBatchItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", Utils.ParseOrderCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@BatchCode", Utils.ParseBatchCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@ItemCode", Utils.ParseItemCode(itemNumber)));

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtItemValueList = new DataTable();
            da.Fill(dtItemValueList);
            conn.Close();
            if (dtItemValueList.Rows.Count > 0)
            {
                foreach (ExpressGradingModel measure in measureList)
                {
                    var realValue = "";
                    DataRow dataRow = dtItemValueList.AsEnumerable().FirstOrDefault(r => (Convert.ToInt32(r["MeasureCode"]) == Convert.ToInt32(measure.MeasureCode) && Convert.ToInt32(r["PartID"]) == measure.PartId));
                    if (dataRow != null)
                        realValue = dataRow["ResultValue"].ToString().Trim();
                    measure.MeasureValueReal = realValue;
                }
            }
                      
        }
        //IvanB 19/05 end
        public static List<ItemValueModel> GetItemValues(int groupCode, int batchCode, int itemCode, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
            {
                //CommandText = "spGetItemDataFromOrderBatchItemFull",//StoredProcedures.SpGetItemDataFromOrderbatchItem,
				CommandText = "spGetItemDataFromOrderBatchItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", groupCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchCode));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtItemValueList = new DataTable();
            da.Fill(dtItemValueList);
            var result = (from DataRow row in dtItemValueList.Rows select ItemValueModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<ItemValueModel> GetItemValuesAndCp(BatchModel batchModel, string itemCode, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
                              {
                //CommandText = StoredProcedures.SpGetItemDataFromOrderbatchItemAndCp,
                				  CommandText = "spGetItemDataFromOrderBatchItemAndCP",
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };
            command.Parameters.Add(new SqlParameter("@GroupCode", batchModel.GroupCodeFiveChars));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchModel.BatchCodeTreeChars));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtItemValueList = new DataTable();
            da.Fill(dtItemValueList);
            var result = (from DataRow row in dtItemValueList.Rows select ItemValueModel.Create2(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<ItemRuleModel> GetItemRules(string  batchNumber, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpRulesTracking24,
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@BatchFullNumber", batchNumber + "00"));
            command.Parameters.Add(new SqlParameter("@ShowRules", "0"));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dsCpResult = new DataSet();
            da.Fill(dsCpResult);        //Item Rules
            var dtResult = dsCpResult.Tables[0];
            var itemRulesList = (from DataRow row in dtResult.Rows select new ItemRuleModel(row)).ToList();

            conn.Close();
            return itemRulesList;
        }
        public static List<ItemFailedModel> GetItemsFailed(BatchModel batchModel, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            var command = new SqlCommand
                              {
                                  CommandText = StoredProcedures.SpRulesTracking24,
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };

            command.Parameters.Add(new SqlParameter("@BatchFullNumber", batchModel.FullBatchNumber + "00"));
            command.Parameters.Add(new SqlParameter("@ShowRules", "0"));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dsCpResult = new DataSet();
            da.Fill(dsCpResult);        //Failed items with reasons;
            var dtResult = dsCpResult.Tables[1];
            var itemFailedList = (from DataRow row in dtResult.Rows select new ItemFailedModel(row)).ToList();

            conn.Close();
            return itemFailedList;
        }
        #endregion

        #region GradeII
        public static bool MarkItemInvalid(SingleItemModel singleItemModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spSetItemStateByCode",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar, 255));
            command.Parameters["@rId"].Direction = ParameterDirection.Output;

            command.Parameters.Add(new SqlParameter("@StateCode", DbConstants.ItemInvalidStateCode));

            command.Parameters.Add(new SqlParameter("@GroupCode", singleItemModel.OrderCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", singleItemModel.BatchCode));
            command.Parameters.Add(new SqlParameter("@ItemCode", singleItemModel.ItemCode));

            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return false;
            }
            conn.Close();
            return true;
        }

		//Determines if SetEstimatedValues function should be called (true) or to skip (false)
		public static bool CheckSetEstimatedValues(List<ItemValueEditModel> itemValues)
		{
			if(itemValues != null && itemValues.Exists(m => (m.MeasureId == 25 || m.MeasureId == 101 || m.MeasureId == 181) && !String.Equals(m.Value, "0")))
			{
				return true;
			}
			return false;
		}
        public static bool IsValueIn(string measureId)
        {
            string ids = "11,12,14,15,16,18,19,20,22,24,27,77,80,81,92,93,100,117,179,234";
            char delim = ',';
            var splitIds = ids.Split(delim);
            return splitIds.Any(item => item.Equals(measureId));
        }
        public static bool IsValueInNew(string ids, string measureId)
        {
            //string ids = "11,12,14,15,16,18,19,20,22,24,27,77,80,81,92,93,100,117,179";
            char delim = ',';
            var splitIds = ids.Split(delim);
            return splitIds.Any(item => item.Equals(measureId));
        }
        public static bool IsStone(MeasureValueModel exCom, List<MeasurePartModel> parts)
        {
            string partTypeId = parts.Find(m => m.PartId == exCom.PartId).PartTypeId;
            if (partTypeId == "1" || partTypeId == "2" || partTypeId == "3" || partTypeId == "10" || partTypeId == "11" || partTypeId == "12")
                return true;
            else
                return false;
            

        }
        public static string GetGradeMeasureIds(Page p)
        {
            string result = null;
            var msg = "";
            //run recalc here
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGradeMeasureIds",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (i == 0)
                            result += dr["measureid"].ToString();
                        else
                            result += "," + dr["measureid"].ToString();
                        i++;
                    }
                }
            }
            catch(Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return result;
        }
        public static string SetEstimatedValues(SingleItemModel itemModel, Page p)
        {
            var msg = "";
            //run recalc here
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetEstimatedValues",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemCode", itemModel.NewItemCode);
            command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);

            command.Parameters.Add("@rId", SqlDbType.Int);
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters["@rId"].Value = DBNull.Value;
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            } catch (Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static string SaveNewMeasures(List<MeasureValueModel> measureValueList, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            //Build Input DataTable
            DataTable dataInputs = new DataTable();
            dataInputs.Columns.Add("BatchID", typeof(int));
            dataInputs.Columns.Add("ItemCode", typeof(int));
            dataInputs.Columns.Add("PartID", typeof(int));
            dataInputs.Columns.Add("MeasureID", typeof(int));
            dataInputs.Columns.Add("MeasureValueID", typeof(int));
            dataInputs.Columns.Add("MeasureValue", typeof(double));
            dataInputs.Columns.Add("StringValue", typeof(String));
            DataRow row;
            foreach (MeasureValueModel model in measureValueList)
            {
                row = dataInputs.NewRow();
                row[0] = model.BatchId;
                row[1] = model.ItemCode;
                row[2] = model.PartId;
                row[3] = model.MeasureId;
                row[4] = model.MeasureValueIdSql;
                row[5] = model.MeasureValueSql;
                row[6] = model.StringValueSql;
                dataInputs.Rows.Add(row);
            }

            //Build Call to Stored Procedure
            var command = new SqlCommand
            {
                CommandText = "sp_SetPartValueCCM_MeasureID_Bulk",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@InputTable", dataInputs);
            command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);
            command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return ex.Message;
            }

            /* Old Code from before 1/4/2019
            foreach (MeasureValueModel model in measureValueList)
            {
                var command = new SqlCommand
                {
                    CommandText = "sp_SetPartValueCCM_MeasureID", 
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };

                command.Parameters.Add("@rId", SqlDbType.Int);
                command.Parameters["@rId"].Direction = ParameterDirection.Output;
                command.Parameters["@rId"].Value = DBNull.Value;

                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]);
                command.Parameters.AddWithValue("@ItemCode", model.ItemCode);
                command.Parameters.AddWithValue("@BatchID", model.BatchId);
                command.Parameters.AddWithValue("@PartID", model.PartId);
                command.Parameters.AddWithValue("@MeasureID", model.MeasureId);
                command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);
                command.Parameters.AddWithValue("@MeasureValue", model.MeasureValueSql);
                command.Parameters.AddWithValue("@StringValue", model.StringValueSql);
                command.Parameters.AddWithValue("@MeasureValueID", model.MeasureValueIdSql);
                LogUtils.UpdateSpLogWithParameters(command, p);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                    return ex.Message;
                }
            }
			*/
            conn.Close();
            return errMsg;

        }

        public static string SaveNewMeasuresToStorage(List<MeasureValueModel> measureValueList, string batchId, string estimate, Page p)
        {
            DataTable dataInputs = new DataTable("SaveMeasures");
            dataInputs.Columns.Add("BatchID", typeof(int));
            dataInputs.Columns.Add("ItemCode", typeof(int));
            dataInputs.Columns.Add("PartID", typeof(int));
            dataInputs.Columns.Add("MeasureID", typeof(int));
            dataInputs.Columns.Add("MeasureValueID", typeof(int));
            dataInputs.Columns.Add("MeasureValue", typeof(double));
            dataInputs.Columns.Add("StringValue", typeof(String));
            dataInputs.Columns.Add("AuthorOfficeID", typeof(String));
            dataInputs.Columns.Add("CurrentOfficeID", typeof(String));
            dataInputs.Columns.Add("ID", typeof(String));
            dataInputs.Columns.Add("Estimate", typeof(String));
            string authorOfficeID = p.Session["AuthorOfficeID"].ToString();
            string currentOfficeID = p.Session["AuthorOfficeID"].ToString();
            string authorID = p.Session["ID"].ToString();
            DataRow row;

            foreach (MeasureValueModel model in measureValueList)
            {
                row = dataInputs.NewRow();
                row[0] = model.BatchId;
                row[1] = model.ItemCode;
                row[2] = model.PartId;
                row[3] = model.MeasureId;
                row[4] = model.MeasureValueIdSql;
                row[5] = model.MeasureValueSql;
                row[6] = model.StringValueSql;
                row[7] = authorOfficeID;
                row[8] = currentOfficeID;
                row[9] = authorID;
                row[10] = estimate;

                dataInputs.Rows.Add(row);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                dataInputs.WriteXml(ms);
                bool msgSent = SendMessageToStorage(ms, p);
                if (msgSent)
                    return null;
                else
                    return "problem sending message";
            }
        }//SaveNewMeasuresToStorage

        public static bool SendMessageToStorage(MemoryStream ms, Page p, string dir = "Front")
        {
            var msg = ms.ToArray();
            var cloudQueueMessage = new CloudQueueMessage(msg);
            return SendMessageToStorage(cloudQueueMessage, p, dir);
        }

        // ReSharper disable once StringLiteralTypo
        public static bool SendMessageToStorage(string msg, Page p, string dir)
        {
            var cloudQueueMessage = new CloudQueueMessage(msg);
            return SendMessageToStorage(cloudQueueMessage, p, dir);
        }
        
        private static bool SendMessageToStorage(CloudQueueMessage cloudQueueMessage, Page p, string dir)
        {
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            string queueName = p.Session["AzureQueue"].ToString();
            string machineName = "";
            if (p.Session["MachineName"] != null)
                machineName = p.Session["MachineName"].ToString();
            else if (dir.ToLower() != "moveoldcerts")
            {
                if (dir != AccountRepQueueNamePrefix)
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                                                  "The Machine Name is required!" + "\")</SCRIPT>");
                }
                else
                {
                    var message = $"Error. Machine name is not specified. dir = {dir}";
                    LogUtils.UpdateSpLog(message, p.Session);
                    return false;
                }

                return false;
            }

            //machineName = "gsi-ny-6";
            if (machineName != "" || dir.ToLower() == "moveoldcerts")
            {
                if (dir.ToLower() == "screening")
                    queueName = "screening-" + machineName.ToLower();
                else if (dir.ToLower() == "itemizing")
                    queueName = "itemizing-" + machineName.ToLower();
                else if (dir == "MoveOldCerts")
                    queueName = "MoveOldCerts";
                else if(dir.ToLower() == AccountRepQueueNamePrefix)
                    // ReSharper disable once StringLiteralTypo
                    queueName = AccountRepQueueNamePrefix + "-" + machineName.ToLower();
                else if (dir.ToLower() == AccountRepPrintInvoiceQueueNamePrefix)
                    // ReSharper disable once StringLiteralTypo
                    queueName = AccountRepPrintInvoiceQueueNamePrefix + "-" + machineName.ToLower();
                else
                    queueName = "front-" + machineName.ToLower();
            }

            //string myAccountName = "gdlightstorage";
            //string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
            CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            //string queueName = "testqueue";
            queueName = queueName.ToLower();
            CloudQueue queue = queueClient.GetQueueReference(queueName);
            try
            {
                queue.CreateIfNotExists();
                queue.AddMessage(cloudQueueMessage);
                return true;
            }
            catch (Exception e)
            {
                LogUtils.UpdateSpLog($"Error. queue.Name={queue.Name}", p.Session);
                LogUtils.UpdateSpLog($"Error. cloudQueueMessage={cloudQueueMessage}", p.Session);
                LogUtils.UpdateSpLogWithException(e, p);
                return false;
            }
        }
        //SendMessageToStorage

        public static DataTable GetClientMachineName(string machineName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from FrontToClient where FrontMachineName = '" + machineName + "'",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;
            
        }

        public static DataSet GetMeasure2ShapeList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from tblMeasure2ShapeTypes order by Shape",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cds = new DataSet();
            cda.Fill(cds);
            return cds;

        }
        public static DataTable GetMeasureColorStone(string stoneName,Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = @"Select * from tblColorStonesSG where StoneName like '%" + stoneName + @"%'",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            return cdt;

        }

        public static bool InsertMeasure2ShapeType(string shape, string WCF, string shapeType, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                string sql = null;
                sql = @"insert into  tblMeasure2ShapeTypes(Type, Shape, WCF) values('" +shapeType + @"', '" + shape + @"', '" +  WCF + @"')";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
                return true;
            }
            catch (Exception e)
            {

                //LogUtils.UpdateSpLogWithParameters(command, p);
                conn.Close();
                return false;
            }
        }//SaveSyntheticCustomerData

        public static bool TmpAddTblCount(int cnt, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                string sql = null;
                sql = @"insert into  tmpTblCount(TblCount, DateStamp) values(" + cnt.ToString() + @", GETDATE())";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
                return true;
            }
            catch (Exception e)
            {

                //LogUtils.UpdateSpLogWithParameters(command, p);
                conn.Close();
                return false;
            }
        }//SaveSyntheticCustomerData


        public static bool SendMessengerInfoToStorage(string msg, DataTable dt, string direction, Page p)
        {
            try
            {
                string myAccountName = p.Session["AzureAccountName"].ToString();
                string myAccountKey = p.Session["AzureAccountKey"].ToString();
                string queueName = p.Session["AzureQueue"].ToString();
                //string machineName = "gsi-ny-6";
                string frontMadenName = null, clientMadenName = null;
                if (dt == null || dt.Rows.Count == 0)
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "The Machine Name is required!" + "\")</SCRIPT>");
                    return false;
                }
                frontMadenName = dt.Rows[0]["FrontMadenName"].ToString();
                clientMadenName = dt.Rows[0]["ClientMadenName"].ToString();
                string date = DateTime.Now.ToString("MMddyyhhmmss");
                //machineName = "gsi-ny-6";
                if (direction == "signature")
                    queueName = "front-" + frontMadenName + "-" + clientMadenName + "-signature";
                else
                    queueName = "front-" + frontMadenName + "-" + clientMadenName + "-photo";
                //string myAccountName = "gdlightstorage";
                //string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
                StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                //string queueName = "testqueue";
                queueName = queueName.ToLower();
                CloudQueue queue = queueClient.GetQueueReference(queueName);
                queue.CreateIfNotExists();
                //var msg = ms.ToArray();
                queue.AddMessage(new CloudQueueMessage(msg));

                return true;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
            return false;
        }//SendMessageToStorage
        public static string GetSignatureForValidation(string clientMachineName, string machineName, string date, Page p)
        {
            string found = "false";
            try
            {
                ArrayList msgList = new ArrayList();
                //string myAccountName = "gdlightstorage";
                string myAccountName = p.Session["AzureAccountName"].ToString();
                string myAccountKey = p.Session["AzureAccountKey"].ToString();
                //string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
                StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                string queueName = p.Session["AzureQueue"].ToString();
                queueName = "front-" + machineName.ToLower() + "-" + clientMachineName.ToLower() + "-tofront";
                //queueName = "front-gsi-ny-9";
                queueName = queueName.ToLower();
                CloudQueue queue = queueClient.GetQueueReference(queueName);

                //var msgs = queue.GetMessage();
                //string msg = msgs.AsString;
                
                try
                {
                    var messages = queue.GetMessages(10);
                    foreach (var msgs in messages)
                    {
                        found = "true";
                        break;
                    }//foreach
                    foreach (var msgs in messages)
                        queue.DeleteMessage(msgs);
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("404"))
                    {
                        found = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                found = ex.Message;
            }
            //Thread.Sleep(1000);

            return found;
        }

        public static string GetSignatureForValidation(string id, string date,  Page p)
        {
            var fileList = new List<string>();

            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            //var containerName = ConfigurationManager.AppSettings["AzureContainerName"].ToString();
            string containerName = @"gdlight";
            //var myAccountName = @"gdlightstorage";
            //var myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            string fileName = id + "_" + date + @".png";
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                var directory = cloudBlobContainer.GetDirectoryReference(@"FrontInOut/Signature");
                var jjj = directory.ListBlobs();
                foreach (var item in jjj)
                {
                    string uri = item.Uri.ToString();
                    if (uri.Contains(fileName))
                        return "true";
                }
                
                return "false";
                //CloudBlockBlob blob = cloudBlobContainer.GetBlockBlobReference("77_06302021033934.jpg");
                ////foreach (subdir in directory)
                //var subdirectory = directory.GetDirectoryReference("@TakeInPersonSignature");
                //var abc = subdirectory.ListBlobs();
                //var xyz = abc.OfType<CloudBlockBlob>();
                //foreach (var file in xyz)
                //{
                //    var name = ((CloudBlockBlob)file).Name;
                //}
                ////var folders = subdirectory.ListBlobs();
                ////foreach(var item in folders)
                ////{
                ////    string yyy = item.ToString();
                ////}
                ////var zzz = directory.ListBlobs();
                
                ////foreach (var folder in subdirectory.)
                ////{
                ////    var uri = folder.Uri.ToString();
                ////    if (!uri.Contains("TakeInPersonSignature"))
                ////        continue;
                    
                ////}
                //var items = cloudBlobContainer.ListBlobs();
                //foreach (var dir in items.OfType<CloudBlobDirectory>())
                //{
                //    string uriStr = dir.Uri.ToString();
                //    string dirSearch = @"gdlight/FrontIntOut/TakeInPersonSignature";
                //    if (!uriStr.Contains(dirSearch))
                //        continue;
                //    var dirs = dir.ListBlobs();
                //    //var files = dirs.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(fileType));
                //    var files = dirs.OfType<CloudBlockBlob>();
                //    //var ddd = yyy.OfType<CloudBlockBlob>();
                //    foreach (var cdrfile in files)
                //    {
                //        var cdrname = ((CloudBlockBlob)cdrfile).Name;
                //        //var date = ((CloudBlockBlob)cdrfile).Properties.LastModified;
                //        //var todayD = DateTime.Today;
                //        //var diff = date - todayD;
                //        //if (date. - todayD).totalDays
                //        //    cdrfile.DeleteIfExists();
                //        //cdrname = cdrname.Replace(dirSearch + @"/", "");
                //        string match = id + "-" + date;
                //        if (cdrname.Contains(match))
                //            return "true";
                //        //fileList.Add(cdrname);
                //    }
                //    return "false";
                    //var xxx = yyy.OfType<CloudBlockBlob>.Where(b => Path.GetExtension(b.Name).Equals(".png"));

                   // Console.WriteLine("\t{0}", dir.Uri);

                }
                /*
                var blobs = items.OfType<CloudBlockBlob>().Where(b => Path.GetExtension(b.Name).Equals(".cdr"));
                foreach (var item in blobs)
                {

                    string name = ((CloudBlockBlob)item).Name;
                    fileList.Add(new CorelFileModel(name, true));
                    //CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(name);
                    //string path = (@"C:\Users\mbcrump\Downloads\test\" + name);
                    //blockBlob.DownloadToFile(path, FileMode.OpenOrCreate);
                }
                */
            
            catch (Exception ex)
            {
                var errMsg = String.Format("Can't load corel files.") + ex.Message;
                return "false";
            }
        }

        public static bool GetInitialSignature(string id, Page p, string photo = "true")
        {
            var fileList = new List<string>();

            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            //var containerName = ConfigurationManager.AppSettings["AzureContainerName"].ToString();
            string containerName = @"gdlight";
            //var myAccountName = @"gdlightstorage";
            //var myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            string fileName = "";
            
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                if (photo == "true")
                {
                    fileName = id + @"_Signature.png";
                    var directory = cloudBlobContainer.GetDirectoryReference(@"Person/Signature");
                    var jjj = directory.ListBlobs();
                    foreach (var item in jjj)
                    {
                        string uri = item.Uri.ToString();
                        if (uri.Contains(fileName))
                            return true;
                    }

                    return false;
                }
                else
                {
                    fileName = id + @"_Photo.png";
                    var directory = cloudBlobContainer.GetDirectoryReference(@"Person/Photo");
                    var jjj = directory.ListBlobs();
                    foreach (var item in jjj)
                    {
                        string uri = item.Uri.ToString();
                        if (uri.Contains(fileName))
                            return true;
                    }

                    return false;
                }
            }

            catch (Exception ex)
            {
                var errMsg = String.Format("Error finding signature. ") + ex.Message;
                return false;
            }
        }

        public static string GetPhotoForValidation(string id, Page p, string date = "")
        {
            var fileList = new List<string>();

            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            //var containerName = ConfigurationManager.AppSettings["AzureContainerName"].ToString();
            string containerName = @"gdlight";
            //var myAccountName = @"gdlightstorage";
            //var myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            string fileName = "";
            if (date == "")
                fileName = id + @"_Photo.png";
            else
                fileName = id + "_" + date + @".png";
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                string photoPath = @"Person/Photo";
                if (date == "")
                    photoPath = @"Person/Photo";
                else
                    photoPath = @"FrontInOut/Photo";
                var directory = cloudBlobContainer.GetDirectoryReference(photoPath);
                var jjj = directory.ListBlobs();
                foreach (var item in jjj)
                {
                    string uri = item.Uri.ToString();
                    if (uri.Contains(fileName))
                        return "true";
                }
                return "false";
            }
            catch (Exception ex)
            {
                var errMsg = String.Format("Can't load corel files.") + ex.Message;
                return "false";
            }
        }
        public static bool DeleteScreeningInitialPhoto(string id, Page p)
        {
            string photoName = id + @"_Photo.png";
            string myAccountName = p.Session["AzureAccountName"].ToString();
            string myAccountKey = p.Session["AzureAccountKey"].ToString();
            //var containerName = ConfigurationManager.AppSettings["AzureContainerName"].ToString();
            string containerName = @"gdlight";
            var connectionString = string.Format(@"DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}",
  myAccountName, myAccountKey);
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                string photoPath = @"Person/Photo";
                string dirSearch = @"Person";
                string dirSearch1 = @"Photo";
                string fileName = id + @"_Photo.png";
                //alex
                var items = cloudBlobContainer.ListBlobs();
                foreach (var dir in items.OfType<CloudBlobDirectory>())
                {
                    string uriStr = dir.Uri.ToString();
                    if (!uriStr.Contains(dirSearch))
                        continue;
                    var dirs = dir.ListBlobs();
                    foreach (var dir1 in dirs.OfType<CloudBlobDirectory>())
                    {
                        string uriStr1 = dir1.Uri.ToString();
                        
                        if (!uriStr1.Contains(dirSearch1))
                            continue;
                        var dirs1 = dir1.ListBlobs();
                        var files1 = dirs1.OfType<CloudBlockBlob>();
                        foreach (var cdrfile1 in files1)
                        {
                            var cdrname1 = ((CloudBlockBlob)cdrfile1).Name;
                            if (cdrname1 == photoPath + @"/" + fileName)
                            {
                                cdrfile1.DeleteIfExists();
                                return true;
                            }
                        }
                    }
                }
                    //alex
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public static DataTable SetTakeOutInfo(string orderCode, string messenger, string carrier, string trackNumber, Page p)
        {
            int order = Convert.ToInt32(orderCode);
            int messengerId = 0;
            if (messenger != "")
                messengerId = Convert.ToInt32(messenger);
            int carrierId = Convert.ToInt32(carrier);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetTakeOutInfo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@Order", order);
            if (messenger != "0")
            {
                command.Parameters.AddWithValue("@MessengerID", messengerId);
                command.Parameters.AddWithValue("@CarrierID", DBNull.Value);
                command.Parameters.AddWithValue("CarrierTrackingNumber", DBNull.Value);
                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            }

            else if (carrier != "")
            {
                command.Parameters.AddWithValue("@MessengerID", DBNull.Value);
                command.Parameters.AddWithValue("@CarrierID", carrierId);
                command.Parameters.AddWithValue("CarrierTrackingNumber", trackNumber);
                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return null;
            }
        }
        public static string UpdateMessengerSignaturePhoto(string id, string direction, Page p)
        {
            var msg = "success";
            string path = "";
            if (direction == "photo")
                path = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Photo/" + id + @"_Photo.png";
            else
                path = @"https://gdlightstorage.blob.core.windows.net/gdlight/Person/Signature/" + id + @"_Signature.png";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetMessengerSignaturePhoto",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            
            command.Parameters.AddWithValue("@ImagePath", path);
            command.Parameters.AddWithValue("@ID", id);
            command.Parameters.AddWithValue("@Direction", direction);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            
            return msg;
        }
        //public static string GetPhotoForValidation(string clientMachineName, string machineName, Page p)
        //{
        //    string found = "false";
        //    try
        //    {
        //        ArrayList msgList = new ArrayList();
        //        //string myAccountName = "gdlightstorage";
        //        string myAccountName = p.Session["AzureAccountName"].ToString();
        //        string myAccountKey = p.Session["AzureAccountKey"].ToString();
        //        //string myAccountKey = @"/z9dcUF//wPoI61IjO0nLJdYP6aEOePsSS4k1lZK5mGbc5d0hos82XxPp2/xXqhBZrjrbdvRYL3su4i2Ecuw5Q==";
        //        StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
        //        CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
        //        CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
        //        string queueName = p.Session["AzureQueue"].ToString();
        //        queueName = "front-" + machineName.ToLower() + "-" + clientMachineName.ToLower() + "-tofront";
        //        //queueName = "front-gsi-ny-9";
        //        CloudQueue queue = queueClient.GetQueueReference(queueName);

        //        //var msgs = queue.GetMessage();
        //        //string msg = msgs.AsString;

        //        try
        //        {
        //            var messages = queue.GetMessages(10);
        //            foreach (var msgs in messages)
        //            {
        //                found = "true";
        //                break;
        //            }//foreach
        //            foreach (var msgs in messages)
        //                queue.DeleteMessage(msgs);
        //        }
        //        catch (Exception ex)
        //        {
        //            if (!ex.Message.Contains("404"))
        //            {
        //                found = ex.Message;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        found = ex.Message;
        //    }
        //    //Thread.Sleep(1000);

        //    return found;
        //}

        public static bool AddMessageToLog(string mLog, string fileName, Page p)
        {
            return AddMessageToLog(mLog, fileName, p.Session);
        }

        public static bool AddMessageToLog(string mLog, string fileName, HttpSessionState session)
        {
            var logPath = LogUtils.GetLogPath(session);
            using (var fileStream = File.Open(logPath, FileMode.Append))
            {
                byte[] bytes = Encoding.ASCII.GetBytes("\n" + mLog);
                fileStream.Write(bytes, 0, bytes.Length);
                fileStream.Flush();
                fileStream.Close();
            }

            return true;

            try
            {
                string pathName = @"gdlight/gdlight_logs";
                string myAccountName = session["AzureAccountName"].ToString();
                string myAccountKey = session["AzureAccountKey"].ToString();
                StorageCredentials storageCredentials = new StorageCredentials(myAccountName, myAccountKey);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                CloudBlobContainer container = blobClient.GetContainerReference(pathName);
                var blockBlob = container.GetBlockBlobReference(fileName);
                byte[] bytes = Encoding.ASCII.GetBytes(mLog);
                var inputStream = new MemoryStream(bytes);

                blockBlob.UploadFromStream(inputStream);
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
        public static string GetLogFromAzure(string url)
        {
            //string url = p.Session["WebPictureShapeRoot"] + @"gdlight_logs/" + fileName;
            byte[] result = null;
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    result = webClient.DownloadData(url);
                }
                var str = System.Text.Encoding.Default.GetString(result);
                byte[] bytes = Encoding.ASCII.GetBytes(str);
                return str;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return "";
            }
        }
        public static List<EnumMeasureModel> GetEnumMeasure(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasureValues",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EnumMeasureModel(row)).ToList();
            conn.Close();
            return result;
        }
        //IvanB start 25/04
        public static List<EnumMeasureModel> GetEnumMeasureByCode(Page p, int measureCode)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetMeasureValueListByMeasureCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@MeasureCode", measureCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EnumMeasureModel(row, 1)).ToList();
            conn.Close();
            return result;
        }
        //IvanB end 25/04
        //IvanB start 14/04

        public static List<ExpressGradingModel> GetSkuRules(long item, Page p)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetSkuRulesDataPerItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Item", item);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            //var result = (from DataRow row in dt.Rows select new ExpressGradingModel(row)).ToList();
            conn.Close();
            return (from DataRow row in dt.Rows select new ExpressGradingModel(row)).ToList();
        }
        //IvanB end 14/04
        public static List<EnumMeasureModel> GetEnumMeasureUnion(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasureValuesUnion",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            //var dt = new DataTable();
            //var da = new SqlDataAdapter(command);
            //da.Fill(dt);
            //var result = (from DataRow row in dt.Rows select new EnumMeasureModel(row)).ToList();
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            var result1 = (from DataRow row in ds.Tables[0].Rows select new EnumMeasureModel(row)).ToList();
            var result2 = (from DataRow row in ds.Tables[1].Rows select new EnumMeasureModel(row)).ToList();
            var result = result1.Concat(result2).ToList();
            conn.Close();
            return result;
        }
        public static List<MeasureValueModel> GetMeasureValues(SingleItemModel itemModel, string accessCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPartValue",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);
            command.Parameters.AddWithValue("@ItemCode", itemModel.NewItemCode);
            command.Parameters.AddWithValue("@PartID", DBNull.Value);
            command.Parameters.AddWithValue("@RecheckNumber", "-1");
            if (!string.IsNullOrEmpty(accessCode))
            {
                command.Parameters.AddWithValue("@ViewAccessCode", accessCode);
            } else
            {
                command.Parameters.AddWithValue("@ViewAccessCode", DBNull.Value);    
            }
            LogUtils.UpdateSpLogWithParameters(command, p);

            var ds = new DataSet("ItemStuff");
            var da = new SqlDataAdapter(command);
            da.Fill(ds);

            var dt  = ds.Tables[0].Copy();
            conn.Close();

            return (from DataRow row in dt.Rows select new MeasureValueModel(row, itemModel)).ToList(); 
            
        }//getMeasureValues

        public static List<MeasureValueModel> GetMeasureValuesBatch(SingleItemModel itemModel, string accessCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPartValueFullBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@BatchID", itemModel.NewBatchId);
            command.Parameters.AddWithValue("@PartID", DBNull.Value);
            command.Parameters.AddWithValue("@RecheckNumber", "-1");
            if (!string.IsNullOrEmpty(accessCode))
            {
                command.Parameters.AddWithValue("@ViewAccessCode", accessCode);
            }
            else
            {
                command.Parameters.AddWithValue("@ViewAccessCode", DBNull.Value);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);

            var ds = new DataSet("ItemStuff");
            var da = new SqlDataAdapter(command);
            da.Fill(ds);

            var dt = ds.Tables[0].Copy();
            conn.Close();

            return (from DataRow row in dt.Rows select new MeasureValueModel(row, itemModel)).ToList();

        }//GetMeasureValuesBatch
        public static List<MeasureValueCpModel> GetMeasureValuesCp(SingleItemModel singleItemModel, Page p)
        {
            //Measure List
            //clarity
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemMeasuresByCP_New",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@BatchID", singleItemModel.BatchId);
            command.Parameters.AddWithValue("@ItemTypeID", singleItemModel.ItemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var pvdsds = new DataSet("ItemStuff2");
            var pvda = new SqlDataAdapter(command);
            pvda.Fill(pvdsds);
            var dt = pvdsds.Tables[0].Copy();
            conn.Close();
            return (from DataRow row in dt.Rows select new MeasureValueCpModel(row, singleItemModel)).ToList(); 
        }

		public static List<MeasureValueCpModel> GetMeasureValuesCp(int batchId, int itemTypeId, Page p)
		{
			//For Measure New            
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetItemMeasuresByCP_New",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
			command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
			command.Parameters.AddWithValue("@BatchID", batchId);
			command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
			LogUtils.UpdateSpLogWithParameters(command, p);
			var pvdsds = new DataSet("ItemStuff2");
			var pvda = new SqlDataAdapter(command);
			pvda.Fill(pvdsds);
			var dt = pvdsds.Tables[0].Copy();
			conn.Close();

			List<MeasureValueCpModel> lstMeasureValueCpModel = new List<MeasureValueCpModel>();
			foreach (DataRow row in dt.Rows)
			{
				lstMeasureValueCpModel.Add(MeasureValueCpModel.Create(row));
			}
			return lstMeasureValueCpModel;
		}

		public static List<MeasurePartModel> GetMeasureParts(int itemTypeId, Page p)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetPartsByItemType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasurePartModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<MeasurePartModel> GetMeasurePartsShort(int itemTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetPartsByItemTypeEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ItemTypeID", itemTypeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasurePartModel.Create(row)).ToList();
            conn.Close();
            return result;
        }
        public static string AddPassedReports(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            //Build Call to Stored Procedure
            var command = new SqlCommand
            {
                CommandText = "spAddReportBulkGrading",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@FullItemCode", itemNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return ex.Message;
            }
            conn.Close();
            return errMsg;

        }
        public static string VoidItemReport(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var errMsg = "";

            //Build Call to Stored Procedure
            var command = new SqlCommand
            {
                CommandText = "spVoidItemReport",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@FullItemCode", itemNumber);
            //var dt = new DataTable();
            //var da = new SqlDataAdapter(command);
            try
            {
                //da.Fill(dt);
                LogUtils.UpdateSpLogWithParameters(command, p);
            
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return ex.Message;
            }
            conn.Close();
            return errMsg;

        }
        #endregion

        #region CutGrade Details
        public static List<CutGradeModel> GetCutGradeDetails(string order, string batch, string item, string partId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetCutGrade_New",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@OrderCode", SqlDbType.Int);
            command.Parameters["@OrderCode"].Value = Int32.Parse(order);

            command.Parameters.Add("@BatchCode", SqlDbType.Int);
            command.Parameters["@BatchCode"].Value = Int32.Parse(batch);

            command.Parameters.Add("@ItemCode", SqlDbType.Int);
            command.Parameters["@ItemCode"].Value = Int32.Parse(item);

            command.Parameters.Add("@PartID", SqlDbType.Int);
            command.Parameters["@PartID"].Value = Int32.Parse(partId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtCutGrade = new DataTable();
            da.Fill(dtCutGrade);
            var result = (from DataRow row in dtCutGrade.Rows select new CutGradeModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion
        
        #region Item Number
        public static string GetOldItemNumber(string batchId, string itemCode, Page p)
        {
            var conn = new SqlConnection(p.Session["MyIP_ConnectionString"].ToString());
            conn.Open();
            //-- Get Old Number
            var command = new SqlCommand
            {
                CommandText = StoredProcedures.SpGetOldNumberByBatchIdItemCode,
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@ItemCode", itemCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var reader = command.ExecuteReader();

            var myVal = "<strong>No measure</strong>";
            if (reader.HasRows)
            {
                reader.Read();
                myVal = reader.GetSqlValue(reader.GetOrdinal("Value")).ToString();
            }
            conn.Close();
            return myVal;

        }


        public  static void  GetAppraisalBatchdataset(int bn, DataSet ds, Page p )
        {
            
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            DAL.AccesstoData ad = new DAL.AccesstoData(conn);
            //    int bn = System.Convert.ToInt32(batchNumber);
            var runNewAppraisal = p.Session["NewAppraisal"].ToString();
            if (runNewAppraisal == "0")
                ad.getBatchAppraisal(ds, "spGetBatchItemAppraisal_testCD", bn);
            //ad.getBatchAppraisal(ds, "spGetBatchItemAppraisal_test", bn);
            else
                ad.getBatchAppraisal(ds, "AppraisalVY", bn);
        }


        public static List<SingleItemModel> GetItemsCp(string batchNumber, Page p)
        {
            string item = batchNumber;
   	        batchNumber = GetBatchNumberBy7digit(batchNumber, p);//sasha
            string strItemNumber = null, itemCode = null;
            if (item.Length > 9 && item.Length < 12)
            {
                strItemNumber = item;
                itemCode = item.Substring(item.Length - 2);
            }
            else
                strItemNumber = batchNumber;
            //var strItemNumber = batchNumber + "00";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "spGetItemCPPictureByCode1Test",
                CommandTimeout = p.Session.Timeout
            };
            //if (itemCode == null)
            //    command.CommandText = "spGetItemCPPictureByCode";
            command.Parameters.AddWithValue("@AuthorId","" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            
            var strGroupCode = Utils.FillToFiveChars("" + Utils.ParseOrderCode(strItemNumber));
            var strBatchCode = Utils.FillToThreeChars("" + Utils.ParseBatchCode(strItemNumber), strGroupCode);

            command.Parameters.AddWithValue("@GroupCode", strGroupCode);
            command.Parameters.AddWithValue("@BatchCode", strBatchCode);
            if (itemCode == null)
                command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
            else
                command.Parameters.AddWithValue("@ItemCode", itemCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new SingleItemModel(row)).ToList();
            conn.Close();
            return result;
        }
        
        public static DataTable GetItemsCpOldNew(string batchNumber, Page p)
        {
            string item = batchNumber;
            batchNumber = GetBatchNumberBy7digit(batchNumber, p);//sasha
            string strItemNumber = null, itemCode = null;
            if (item.Length > 9 && item.Length < 12)
            {
                strItemNumber = item;
                itemCode = item.Substring(item.Length - 2);
            }
            else
                strItemNumber = batchNumber;
            //var strItemNumber = batchNumber + "00";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "spGetItemCPPictureByCode1Test",
                CommandTimeout = p.Session.Timeout
            };
            //if (itemCode == null)
            //    command.CommandText = "spGetItemCPPictureByCode";
            command.Parameters.AddWithValue("@AuthorId", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);

            var strGroupCode = Utils.FillToFiveChars("" + Utils.ParseOrderCode(strItemNumber));
            var strBatchCode = Utils.FillToThreeChars("" + Utils.ParseBatchCode(strItemNumber), strGroupCode);

            command.Parameters.AddWithValue("@GroupCode", strGroupCode);
            command.Parameters.AddWithValue("@BatchCode", strBatchCode);
            if (itemCode == null)
                command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
            else
                command.Parameters.AddWithValue("@ItemCode", itemCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            //var result = (from DataRow row in dt.Rows select new SingleItemModel(row)).ToList();
            conn.Close();
            return dt;
        }
        #endregion

        #region Printed/Not
        public static List<ReportModel> GetPrintedNot(string orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetReportsList1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@FullNumber", orderCode + Utils.FillToThreeChars("0", orderCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ReportModel(row)).ToList();
            conn.Close();
            //-- Create footer rows
            var groups = result.GroupBy(m => m.BatchNumber).ToList();
            foreach (var group in groups)
            {
                result.Add(ReportModel.CreateFooter(result.FindAll(m => m.BatchNumber == @group.Key && !m.IsFooter)));
            }
            result.Sort((m1, m2) => String.Compare(m1.SortExpr, m2.SortExpr, StringComparison.Ordinal));
            return result;
        }
        #endregion
        
        #region Open Orders
        public static List<OpenOrderModel> GetOpenOrders(DateTime? dateFrom, DateTime? dateTo, int customerId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetOpenOrders",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (dateFrom != null)
            {
                command.Parameters.Add(new SqlParameter("@dateFrom", dateFrom));
            }
            if (dateTo != null)
            {
                command.Parameters.Add(new SqlParameter("@DateTo", ((DateTime)dateTo).AddDays(1)));
            }

            if (customerId > 0)
            {
                command.Parameters.Add(new SqlParameter("@CustomerID", customerId));
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new OpenOrderModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region CP Overview
        public static List<CpRuleLookupModel> GetCpRulesBySku(string sku, Page p, string itemTypeID = "All Types")
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetSkuListBySKU3",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SKU", "%" + sku.Trim() + "%");
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            List<CpRuleLookupModel> result = new List<CpRuleLookupModel>();
            if (itemTypeID == "All Types" || itemTypeID== "")
            {
                result = (from DataRow row in dt.Rows select CpRuleLookupModel.CreateByStyleOrSku(row, true)).ToList();
            }
            else
            {
                int iItemTypeID = Convert.ToUInt16(itemTypeID);
                DataRow[] rows = dt.Select("ItemTypeID=" + itemTypeID);
                result = (from DataRow row in rows select CpRuleLookupModel.CreateByStyleOrSku(row, true)).ToList();
            }
            conn.Close();
            return result;
        }

        public static List<CpRuleLookupModel> GetCpRulesByStyle(string style, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetSkuListByStyle2",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerStyle", "%" + style.Trim() + "%");
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select CpRuleLookupModel.CreateByStyleOrSku(row, false)).ToList();
            conn.Close();
            return result;
        }
        public static List<DocModel> GetDocumentList(CustomerModel customerModel, string cpName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "oleg_sp_GetDocList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerID", customerModel.CustomerId);
            command.Parameters.AddWithValue("@CustomerOfficeID", customerModel.OfficeId);
            command.Parameters.AddWithValue("@CustomerProgramName", cpName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static CpRuleLookupModel  GetCpRulesByBatch(string batchNumber, Page p)
        {
            int orderCode = Utils.ParseOrderCode(batchNumber);
            int batchCode = Utils.ParseBatchCode(batchNumber);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetCPRules",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);  
            command.Parameters.AddWithValue("@BatchCode", batchCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                conn.Close();
                return null;
            }
            var result = new CpRuleLookupModel(dt.Rows[0]);
            foreach (DataRow row in dt.Rows)
            {
                result.AddCpRule(row);
            }
            conn.Close();

            //-- Get Documents List
            var docs = GetDocumentList(result.Customer, result.Cp.CustomerProgramName, p);
            foreach (var docModel in docs)
            {
                result.DocList.Add(docModel);
            }
            return result;
        }
        public static CpRuleLookupModel GetCpRulesByStyle(CpRuleLookupModel model, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            if (!model.Cp.ExtData)
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetCustomerProgramInstanceByCPID",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@CPID", model.Cp.CpId);
                command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    CustomerProgramModel.SetExtData(dt.Rows[0], model.Cp);    
                }
            }
            return GetCpRulesByCustomerAndCp(model.Customer, model.Cp, p);
        }
        public static CpRuleLookupModel GetCpRulesByCustomerAndCp(CustomerModel customer, CustomerProgramModel cp, Page p)
        {
            var result = new CpRuleLookupModel {Customer = customer, Cp = cp};
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            //-- Get Cp Rules
            var command = new SqlCommand
            {
                CommandText = "sp_GetCPRulesByCPOfficeID_CPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID_CPID", cp.CpOfficeIdAndCpId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                result.CpRuleList.Add(new CpRuleModel(row));    
            }
            conn.Close();

            //-- Get Documents
            var docs = GetDocumentList(result.Customer, result.Cp.CustomerProgramName, p);
            foreach (var docModel in docs)
            {
                result.DocList.Add(docModel);
            }
            return result;
        }
        public static List<CpRuleModel> GetCpRulesByCp(string cpId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetCPRulesByCPOfficeID_CPID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CPOfficeID_CPID", cpId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CpRuleModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<CustomerProgramModel> GetCustomerPrograms(CustomerModel customerModel, CustomerModel vendorModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramsPerCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerOfficeID", customerModel.OfficeId);
            command.Parameters.AddWithValue("@CustomerID", customerModel.CustomerId);

            //alex temporary removed
            //if (vendorModel != null)
            //{
            //    command.Parameters.AddWithValue("@VendorOfficeID", vendorModel.OfficeId);
            //    command.Parameters.AddWithValue("@VendorID", vendorModel.CustomerId);
                
            //} else
            //{
                command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
                command.Parameters.AddWithValue("@VendorID",DBNull.Value);
            //}
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerProgramModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<CustomerProgramModel> GetCustomerPrograms(CustomerModel customerModel, Page p)
        {
            return GetCustomerPrograms(customerModel, customerModel, p);
        }
        public static List<CustomerProgramModel> GetOldOrderCp(string oldOrder, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var result = new List<CustomerProgramModel>();
            oldOrder = oldOrder.Replace(".", "");
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetCustomerProgramsPerOldItem",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@FullNumber", oldOrder);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CustomerProgramModel(row)).ToList();
            }
            catch
            {
                result = null;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        /*IvanB start*/
        public static List<CustomerProgramModel> GetAllCustomerPrograms(Page p)
        {
            var result = new List<CustomerProgramModel>();
            using(SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetAllCustomerPrograms",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = (from DataRow row in dt.Rows select new CustomerProgramModel(row, true)).ToList();
            }

            return result;
        }
        public static string AddStyleToPrintedItems(string style, string cpname, Page p)
        {
            string result = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spAddStyleBySkuToPrintedItems",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@CustomerStyle", style);
                command.Parameters.AddWithValue("@CustomerProgramName", cpname);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                result = dt.Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        /*IvanB end*/
        #endregion

        #region Report Lookup
        public static ReportFindModel GetItemNumberByHexGnumber(ReportFindModel findModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemNumberByGNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            findModel.IntGNumber = int.Parse(findModel.HexGnumber, System.Globalization.NumberStyles.AllowHexSpecifier);
            command.Parameters.Add(new SqlParameter("@Gnumber", findModel.IntGNumber));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                var row = dt.Rows[0];
                findModel.VirtualVaultNumber = "" + row["VirtualVaultNumber"];
                findModel.ItemNumber = "" + row["OldItemNumber"];
            }
            conn.Close();
            return findModel;
        }
        public static ReportFindModel GetVirtualVaultByItemNumber(ReportFindModel findModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspGetVirtualVaultFromReportNameEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var sMyReport = Utils.GetItemNumberWithDots(findModel.ItemNumber).Split('.');

            command.Parameters.AddWithValue("@MeasureId", Int32.Parse("" + p.Session["VirtualVaultID"]));
            command.Parameters.AddWithValue("@OrderCode", int.Parse(sMyReport[0]));
            command.Parameters.AddWithValue("@BatchCode", int.Parse(sMyReport[1]));
            command.Parameters.AddWithValue("@ItemCode", int.Parse(sMyReport[2]));
            //command.Parameters.AddWithValue("@OperationChar", DBNull.Value);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                findModel.VirtualVaultNumber = "" + dt.Rows[0][0];
            }
            conn.Close();
            return findModel;
        }
        public static DataTable GetDeletedItems(string fullItemNumber, Page p)
        {
            string groupCode = null, batchCode = null, itemCode = null, sqlText = null;
            if (fullItemNumber.Length == 5 || fullItemNumber.Length == 6 || fullItemNumber.Length == 7)//order only
                groupCode = fullItemNumber;
            //    sqlText = @"Select GroupCode, null as BatchCode, null as ItemCode, ExpireDate from v0GroupNoExpDate where GroupCode = " + fullItemNumber + @" and ExpireDate is not null";
            else if (fullItemNumber.Length == 8 || fullItemNumber.Length == 9)//order + batch
            {
                if (fullItemNumber.Length == 8)//5 digits order
                {
                    groupCode = fullItemNumber.Substring(0, 5);
                    batchCode = fullItemNumber.Substring(5, 3);
                }
                else
                {
                    if (fullItemNumber[6] == '0')//6 digits  order
                    {
                        groupCode = fullItemNumber.Substring(0, 6);
                        batchCode = fullItemNumber.Substring(6, 3);
                    }
                    else
                    {
                        groupCode = fullItemNumber.Substring(0, 7);
                        batchCode = fullItemNumber.Substring(7, 2);
                    }
                    //sqlText = @"Select GroupCode, BatchCode, null as ItemCode, ExpireDate from v0BatchNoExpDate where GroupCode = " + groupCode + @" and Batchcode = " + batchCode +  @" and ExpireDate is not null";
                }
            }
            else if (fullItemNumber.Length == 10 || fullItemNumber.Length == 11) //order + batch + item
            {
                if (fullItemNumber.Length == 10)
                {
                    groupCode = fullItemNumber.Substring(0, 5);
                    batchCode = fullItemNumber.Substring(5, 3);
                    itemCode = fullItemNumber.Substring(8, 2);
                }
                else
                {
                    if (fullItemNumber[6] == '0')//6 digits order
                    {
                        groupCode = fullItemNumber.Substring(0, 6);
                        batchCode = fullItemNumber.Substring(6, 3);
                        itemCode = fullItemNumber.Substring(9, 2);
                    }
                    else
                    {
                        groupCode = fullItemNumber.Substring(0, 7);
                        batchCode = fullItemNumber.Substring(7, 2);
                        itemCode = fullItemNumber.Substring(9, 2);
                    }
                }
                //sqlText = @"Select GroupCode, BatchCode, ItemCode, ExpireDate from v0ItemNoExpDate where GroupCode = " + groupCode + @" and Batchcode = " + batchCode + 
                //    @" and ItemCode = " + itemCode + @" and ExpireDate is not null";
            }
            DataTable dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spGetDeletedItems",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@GroupCode", int.Parse(groupCode));
                if (batchCode == null)
                    command.Parameters.AddWithValue("@BatchCode", DBNull.Value);
                else
                    command.Parameters.AddWithValue("@BatchCode", int.Parse(batchCode));
                if (itemCode == null)
                    command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
                else
                    command.Parameters.AddWithValue("@ItemCode", int.Parse(itemCode));
                //command.Parameters.AddWithValue("@OperationChar", DBNull.Value);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                //var dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
            //var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            //conn.Open();
            //var command = new SqlCommand
            //{
            //    CommandText = sqlText,
            //    Connection = conn,
            //    CommandType = CommandType.Text,
            //    CommandTimeout = p.Session.Timeout
            //};
            //LogUtils.UpdateSpLogWithParameters(command, p);
            //var cda = new SqlDataAdapter(command);
            //var cdt = new DataTable();
            //cda.Fill(cdt);

            //conn.Close();
            //return cdt;
        }
        #endregion
       
        #region Get Invoice Numbers
        public static List<InvoiceModel> GetInvoiceNumbers(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetInvoiceNumberByOrderNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderNumber", "" + order);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new InvoiceModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Tracking II
        public static List<EventModel> GetEventsList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpGetTrackingActions",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EventModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.EventName, m2.EventName, StringComparison.Ordinal));
            result.Insert(0, new EventModel{EventId = 0, EventName = "All"});
            conn.Close();
            return result;
        }
        public static List<ViewAccessModel> GetFormsList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpGetFormList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ViewAccessModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.ViewAccessName, m2.ViewAccessName, StringComparison.Ordinal));
            result.Insert(0, new ViewAccessModel{ ViewAccessId = 0, ViewAccessName = "All" });
            conn.Close();
            return result;
        }
        public static List<TrackingModel> GetAgedBatches(TrackingFilterModel filterModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "trkSpGetAgedBatches",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(filterModel.DateFrom != null
                                       ? new SqlParameter("@DateFrom", filterModel.DateFrom)
                                       : new SqlParameter("@DateFrom", DBNull.Value));
            command.Parameters.Add(filterModel.DateTo != null
                                       ? new SqlParameter("@DateTo", filterModel.DateTo)
                                       : new SqlParameter("@DateTo", DBNull.Value));

            command.Parameters.Add(filterModel.CustomerId > 0
                                       ? new SqlParameter("@CustomerID", filterModel.CustomerId)
                                       : new SqlParameter("@CustomerID", DBNull.Value));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new TrackingModel(row)).ToList();
            if (filterModel.AgeCurrent > 0)
            {
                result = result.FindAll(m => m.AgeInHrs > filterModel.AgeCurrent);
            }
            conn.Close();
            return result;
        }
        public static List<TrackingHistoryModel> GetAgesHistoryBatches(TrackingFilterModel filterModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkSpGetAgedBatchHistoryWithFilters_Max",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(filterModel.DateFrom != null
                                       ? new SqlParameter("@DateFrom", filterModel.DateFrom)
                                       : new SqlParameter("@DateFrom", DBNull.Value));

            command.Parameters.Add(filterModel.DateTo != null
                                       ? new SqlParameter("@DateTo", filterModel.DateTo)
                                       : new SqlParameter("@DateTo", DBNull.Value));

            command.Parameters.Add(filterModel.CustomerId > 0
                                       ? new SqlParameter("@CustomerID", filterModel.CustomerId)
                                       : new SqlParameter("@CustomerID", DBNull.Value));

            command.Parameters.Add(filterModel.EventId != 0
                                       ? new SqlParameter("@EventID", filterModel.EventId)
                                       : new SqlParameter("@EventID", DBNull.Value));

            command.Parameters.Add(filterModel.ViewAccessId != 0
                                       ? new SqlParameter("@ViewAccessID", filterModel.ViewAccessId)
                                       : new SqlParameter("@ViewAccessID", DBNull.Value));

            command.Parameters.Add(filterModel.ShowCheckedOut
                                       ? new SqlParameter("@ShowCheckedOut", 1)
                                       : new SqlParameter("@ShowCheckedOut", DBNull.Value));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            try
            {
                da.Fill(dt);
            } catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return new List<TrackingHistoryModel>();
            }
            conn.Close();
            var result = (from DataRow row in dt.Rows select new TrackingHistoryModel(row)).ToList();
            var groups = result.GroupBy(x => new { x.FullBatchNumber }).Select(group => new { FullOrder = @group.Key, TimeStamp = @group.Max(x => x.CreateDate) });
            var resGrp = new List<TrackingHistoryModel>();
            
            foreach (var @group in groups)
            {
                var grp = result.Find(m => m.FullBatchNumber != null && (m.FullBatchNumber.Equals(@group.FullOrder.FullBatchNumber) && m.CreateDate == @group.TimeStamp));
                if (grp != null) resGrp.Add(grp);
            }
            return resGrp;// result;
        }
        public static List<BatchHistoryModel> GetBatchHistory(BatchModel batchModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkSpGetBatchHistoryDetails",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchModel.BatchId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new BatchHistoryModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static bool HasWebAccess21(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetWebPermissions",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@UserID", "" + p.Session["ID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var res = dt.Select("LinkID = 21").Length > 0;
            conn.Close();
            return res;
        }
        #endregion

        #region Tracking
        public static List<TrackingExModel> GetTrackings(TrackingFilterModel filterModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspvvGetMemoDateRange",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(filterModel.CustomerId > 0
                ? new SqlParameter("@CustomerID", filterModel.CustomerId)
                : new SqlParameter("@CustomerID", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@CustomerOfficeID", filterModel.CustomerOfficeId));

            command.Parameters.Add(filterModel.DateFrom != null
                ? new SqlParameter("@DateFrom", filterModel.DateFrom)
                : new SqlParameter("@DateFrom", DBNull.Value));
            command.Parameters.Add(filterModel.DateTo != null
                ? new SqlParameter("@DateTo", filterModel.DateTo)
                : new SqlParameter("@DateTo", DBNull.Value));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new TrackingExModel(row, filterModel)).ToList();
            conn.Close();
            return result;
        }
        public static CarrierModel GetCarrierInfo(int orderCode, Page p )
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetTrackingInfo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@OrderCode", orderCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            var result = new CarrierModel(dt.Rows[0]);
            conn.Close();
            return result;
        }
        public static List<TrackingDetModel> GetTrackingDetails(TrackDetailsFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspvvGetBatchList2WithDateRangeWithOrderMemo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@CustomerID", filter.CustomerId));
            command.Parameters.Add(new SqlParameter("@CustomerOfficeID", filter.OfficeId));
            command.Parameters.Add(new SqlParameter("@VendorID", DBNull.Value));
            command.Parameters.Add(new SqlParameter("@VendorOfficeID", DBNull.Value));
            
            if (filter.IsMemoMode)
            {
                command.Parameters.Add(new SqlParameter("@MemoNumber", filter.Value));
                command.Parameters.Add(new SqlParameter("@DateFrom", DateTime.Parse(filter.DateFrom)));
                command.Parameters.Add(new SqlParameter("@DateTo", DateTime.Parse(filter.DateTo).AddDays(1)));
            }
            if (filter.IsOrderMode)
            {
                command.Parameters.Add(new SqlParameter("@OrderCode", int.Parse(filter.Value)));
            }
            if (filter.IsCpMode)
            {
                command.Parameters.Add(new SqlParameter("@CustomerProgramName", filter.Value));
                command.Parameters.Add(new SqlParameter("@DateFrom", DateTime.Parse(filter.DateFrom)));
                command.Parameters.Add(new SqlParameter("@DateTo", DateTime.Parse(filter.DateTo).AddDays(1)));
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new TrackingDetModel(row, filter)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Tracking Batch History
        public static List<TrackingBatchHistoryModel> GetTrackingBatchHistory(string batchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspvvGetHistory2",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new TrackingBatchHistoryModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static CarrierModel GetCarrierInfoByBatch(string batchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_getTrackingInfoByBatchId",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", batchId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            var result = new CarrierModel(dt.Rows[0]);
            conn.Close();
            return result;
        }

        #endregion

        #region New Order
        public static bool CreateOrder(NewOrderModel orderModel, Page p, out string msg)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spsetEntryBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;

            command.Parameters["@rID"].Size = 1024;

            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@IsIQInspected", SqlDbType.Int).Value = 0;
            command.Parameters.Add("@IsTWInspected", SqlDbType.Int).Value = 0;
            if (orderModel.NumberOfItems < 1)
                command.Parameters.Add("@ItemsQuantity", SqlDbType.Int).Value = DBNull.Value;
            else
                command.Parameters.Add("@ItemsQuantity", SqlDbType.Int).Value = orderModel.NumberOfItems;

            command.Parameters.Add("@TotalWeight", SqlDbType.Float).Value = DBNull.Value;
            command.Parameters.Add("@MeasureUnitID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@ServiceTypeID", SqlDbType.Int).Value = 1;
            command.Parameters.Add("@Memo", SqlDbType.VarChar).Value = orderModel.OrderMemo;
            if ((orderModel.MemoNumbers != null) && (orderModel.MemoNumbers.Count > 0))
                command.Parameters.Add("@IsMemo", SqlDbType.Int).Value = orderModel.MemoNumbers.Count;
            else
                command.Parameters.Add("@IsMemo", SqlDbType.Int).Value = 0;
            
            if (string.IsNullOrEmpty(orderModel.SpecialInstructions))
                command.Parameters.Add("@SpecialInstruction", SqlDbType.VarChar).Value = DBNull.Value;
            else
                command.Parameters.Add("@SpecialInstruction", SqlDbType.VarChar).Value = orderModel.SpecialInstructions;

            command.Parameters.Add("@CarrierTrackingNumber", SqlDbType.VarChar).Value = DBNull.Value;
            command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@PersonCustomerOfficeID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@PersonCustomerID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@PersonID", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = orderModel.Customer.OfficeId;
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = orderModel.Customer.CustomerId;
            command.Parameters.AddWithValue("@VendorOfficeID", DbType.VarNumeric).Value = DBNull.Value;
            command.Parameters.Add("@VendorID", SqlDbType.Int).Value = DBNull.Value;
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            try
            {
                var res = command.Parameters["@rId"].Value.ToString();
                orderModel.GroupOfficeId = Int32.Parse(res.Split('_')[0]);
                orderModel.GroupId = Int32.Parse(res.Split('_')[1]);

                command = new SqlCommand
                              {
                                  CommandText = "spGetGroup",
                                  Connection = conn,
                                  CommandType = CommandType.StoredProcedure,
                                  CommandTimeout = p.Session.Timeout
                              };

                command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
                command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value =
                    Int32.Parse("" + p.Session["AuthorOfficeID"]);
                command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = orderModel.GroupOfficeId;
                command.Parameters.Add("@GroupID", SqlDbType.Int).Value = orderModel.GroupId;
                LogUtils.UpdateSpLogWithParameters(command, p);
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    orderModel.OrderNumber = Utils.FillToFiveChars("" + dt.Rows[0]["OrderCode"]);
                    orderModel.Customer.CustomerName = "" + dt.Rows[0]["CustomerName"];
                }
                if ((dt.Rows.Count > 0) && (orderModel.MemoNumbers != null) && (orderModel.MemoNumbers.Count > 0))
                {
                    foreach (var memo in orderModel.MemoNumbers)
                    {
                        command = new SqlCommand
                                      {
                                          CommandText = "spSetGroupMemoNumber",
                                          Connection = conn,
                                          CommandType = CommandType.StoredProcedure,
                                          CommandTimeout = p.Session.Timeout
                                      };

                        command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
                        command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value =
                            Int32.Parse("" + p.Session["AuthorOfficeID"]);
                        command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = orderModel.GroupOfficeId;
                        command.Parameters.Add("@rId", SqlDbType.VarChar).Direction = ParameterDirection.Output;
                        command.Parameters["@rId"].Size = 1024;
                        command.Parameters.Add("@GroupID", SqlDbType.Int).Value = orderModel.GroupId;
                        command.Parameters.Add("@Name", SqlDbType.VarChar);
                        command.Parameters["@Name"].Size = 2048;
                        command.Parameters["@Name"].Value = memo;
                        LogUtils.UpdateSpLogWithParameters(command, p);
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            msg = ex.Message;
                            LogUtils.UpdateSpLogWithException(msg, p);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return false;
            }
            conn.Close();
            msg = "";
            return true;

        }
        #endregion

        #region Itemize
        public static OrderModel GetOrderInfo(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGroupByCode2",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            
            command.Parameters.Add("@CustomerCode", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@BGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.Add("@EGroupState", SqlDbType.Int).Value = DBNull.Value;
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);

            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = int.Parse(order);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count == 0 ) return null;
            var result = new OrderModel(dt.Rows[0], order);
            conn.Close();
            return result;
        }
        public static List<MemoModel> GetOrderMemos(OrderModel orderModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGroupMemoNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@GroupID", SqlDbType.Int).Value = orderModel.GroupId;
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MemoModel.Create1(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.MemoNumber, m2.MemoNumber, StringComparison.Ordinal));
            
            conn.Close();
            return result;
        }
        public static List<CustomerProgramModel>  GetOrderCp(OrderModel orderModel, Page p)
        {
            return GetCustomerPrograms(orderModel.Customer, p);
        }

        public static List<ItemizeNewBatchLotModel>  ItemizeAddItemsByLots(ItemizeGoModel model, Page p)
        {
            //-- Create Lot NUmbers table
            var dt = new DataTable("LotNumberSetTable");
            dt.Columns.Add("LID");
            dt.Columns.Add("LotNumber");
            var lid = 0;
            foreach (var lotNumber in model.LotNumbers)
            {
                dt.Rows.Add(new object[] { lid, lotNumber});
                lid++;
            }
            dt.AcceptChanges();

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_AddItemsByLots",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add("@OrderNumber", SqlDbType.Int);
            command.Parameters["@OrderNumber"].Value = model.Order.GroupCode;

            command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
            command.Parameters["@CustomerProgramName"].Size = 250;
            command.Parameters["@CustomerProgramName"].Value = model.Cp.CustomerProgramName;
            

            command.Parameters.Add("@ItemNumber", SqlDbType.Int);
            command.Parameters["@ItemNumber"].Value = DBNull.Value;

            command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
            command.Parameters["@MemoNumber"].Size = 500;
            command.Parameters["@MemoNumber"].Value = model.Memo.MemoNumber;

            command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
            command.Parameters["@MemoNumberID"].Value = model.Memo.MemoId;

            command.Parameters.Add("@AuthorId", SqlDbType.Int);
            command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);

            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
            command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);

            command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = model.Order.GroupOfficeId;
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = model.Order.GroupOfficeId;

            command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(model.Cp.CpOfficeIdAndCpId.Split('_')[0]);

            var param = new SqlParameter
            {
                ParameterName = "LotNumberList",
                SqlDbType = SqlDbType.Structured,
                Value = dt,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dtr = new DataTable();
            command.CommandTimeout = 0;
            try
            {
                da.Fill(dtr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogUtils.UpdateSpLogWithException(ex.Message, p);
            }
            var result = (from DataRow row in dtr.Rows select new ItemizeNewBatchLotModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataTable GetUpdatedItems(string order, Page p)
        {
            DataTable cdt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "Select * from v0group where groupcode= " + order,
                    Connection = conn,
                    CommandType = CommandType.Text,
                    CommandTimeout = p.Session.Timeout
                };
                LogUtils.UpdateSpLogWithParameters(command, p);
                var cda = new SqlDataAdapter(command);
                cda.Fill(cdt);
                conn.Close();
                return cdt;
            }
            catch (Exception ex)
            {
                conn.Close();
                string msg = ex.Message;
                return null;
            }

        }
        public static List<BatchNewModel> ItemizeAddItemsByCount(ItemizeGoModel model, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_AddItemsByCount", //"sp_AddItemsByCPandOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@OrderNumber", SqlDbType.Int);
            command.Parameters["@OrderNumber"].Value = model.Order.GroupCode;
            
            command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
            command.Parameters["@CustomerProgramName"].Size = 250;
            command.Parameters["@CustomerProgramName"].Value = model.Cp.CustomerProgramName;
            
            command.Parameters.Add("@ItemNumber", SqlDbType.Int);
            command.Parameters["@ItemNumber"].Value = model.NumberOfItems;
            
            command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
            command.Parameters["@MemoNumber"].Size = 500;
            command.Parameters["@MemoNumber"].Value = model.Memo.MemoNumber;

            command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
            command.Parameters["@MemoNumberID"].Value = model.Memo.MemoId;

            command.Parameters.Add("@AuthorId", SqlDbType.Int);
            command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);
            
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
            command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            
            command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = model.Order.GroupOfficeId;
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = model.Order.GroupOfficeId;

            command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(model.Cp.CpOfficeIdAndCpId.Split('_')[0]);
            
            command.Parameters.Add("@ParNo", SqlDbType.VarChar).Size = 255;
            command.Parameters["@ParNo"].Value = DBNull.Value;
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            //var dt = new DataTable();
            //da.Fill(dt);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            var result = (from DataRow row in dt.Rows select new BatchNewModel(row)).ToList();

            conn.Close();
            return result;

        }
        public static DataTable ReprintBatchItemLabel(string order, string batch, string item, bool allItems, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spReprintBatchItemLabel", //"sp_AddItemsByCPandOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", Convert.ToInt32(order));
            command.Parameters.AddWithValue("@BatchCode", Convert.ToInt16(batch));
            if (item == null)
                command.Parameters.AddWithValue("@ItemCode", 0);
            else
                command.Parameters.AddWithValue("@ItemCode", Convert.ToInt16(item));
            if (allItems)
                command.Parameters.AddWithValue("@AllItems", "YES");
            else
                command.Parameters.AddWithValue("@AllItems", "NO");
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        //public static DataTable ItemizeAddItemsByCount1(ItemizeGoModel model, Page p)
        //{
        //    var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
        //    conn.Open();
        //    var command = new SqlCommand
        //    {
        //        CommandText = "sp_AddItemsByCount", //"sp_AddItemsByCPandOrderCode",
        //        Connection = conn,
        //        CommandType = CommandType.StoredProcedure,
        //        CommandTimeout = p.Session.Timeout
        //    };
        //    command.Parameters.Add("@OrderNumber", SqlDbType.Int);
        //    command.Parameters["@OrderNumber"].Value = model.Order.GroupCode;

        //    command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
        //    command.Parameters["@CustomerProgramName"].Size = 250;
        //    command.Parameters["@CustomerProgramName"].Value = model.Cp.CustomerProgramName;

        //    command.Parameters.Add("@ItemNumber", SqlDbType.Int);
        //    command.Parameters["@ItemNumber"].Value = model.NumberOfItems;

        //    command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
        //    command.Parameters["@MemoNumber"].Size = 500;
        //    command.Parameters["@MemoNumber"].Value = model.Memo.MemoNumber;

        //    command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
        //    command.Parameters["@MemoNumberID"].Value = model.Memo.MemoId;

        //    command.Parameters.Add("@AuthorId", SqlDbType.Int);
        //    command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);

        //    command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
        //    command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);

        //    command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = model.Order.GroupOfficeId;
        //    command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = model.Order.GroupOfficeId;

        //    command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(model.Cp.CpOfficeIdAndCpId.Split('_')[0]);

        //    command.Parameters.Add("@ParNo", SqlDbType.VarChar).Size = 255;
        //    command.Parameters["@ParNo"].Value = DBNull.Value;
        //    LogUtils.UpdateSpLogWithParameters(command, p);
        //    var da = new SqlDataAdapter(command);
        //    var dt = new DataTable();
        //    da.Fill(dt);
        //    var result = (from DataRow row in dt.Rows select new BatchNewModel(row)).ToList();

        //    conn.Close();
        //    return result;

        //}
        public static List<BatchNewModel> ItemizeAddOldNewItemsByCount(ItemizeGoModel model, Page p)
        {
            var dtr = new DataTable("PrevItemList");
            dtr.Columns.Add("PrevGroupCode");
            dtr.Columns.Add("PrevBatchCode");
            dtr.Columns.Add("PrevItemCode");
            foreach (var items in model.LotNumbers)
            {
                string[] part = items.Split('.');
                dtr.Rows.Add(new object[] { part[0], part[1], part[2] });
            }
            dtr.AcceptChanges();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_AddOldNewItemsByCount", //"sp_AddItemsByCPandOrderCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@OrderNumber", SqlDbType.Int);
            command.Parameters["@OrderNumber"].Value = model.Order.GroupCode;

            command.Parameters.Add("@CustomerProgramName", SqlDbType.VarChar);
            command.Parameters["@CustomerProgramName"].Size = 250;
            command.Parameters["@CustomerProgramName"].Value = model.Cp.CustomerProgramName;

            command.Parameters.Add("@ItemNumber", SqlDbType.Int);
            command.Parameters["@ItemNumber"].Value = model.NumberOfItems;

            command.Parameters.Add("@MemoNumber", SqlDbType.VarChar);
            command.Parameters["@MemoNumber"].Size = 500;
            command.Parameters["@MemoNumber"].Value = model.Memo.MemoNumber;

            command.Parameters.Add("@MemoNumberID", SqlDbType.Int);
            command.Parameters["@MemoNumberID"].Value = model.Memo.MemoId;

            command.Parameters.Add("@AuthorId", SqlDbType.Int);
            command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);

            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
            command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);

            command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = model.Order.GroupOfficeId;
            command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = model.Order.GroupOfficeId;

            command.Parameters.Add("@CPOfficeID", SqlDbType.Int).Value = int.Parse(model.Cp.CpOfficeIdAndCpId.Split('_')[0]);

            command.Parameters.Add("@ParNo", SqlDbType.VarChar).Size = 255;
            command.Parameters["@ParNo"].Value = DBNull.Value;
            var param = new SqlParameter
            {
                ParameterName = "PrevItemList",
                SqlDbType = SqlDbType.Structured,
                Value = dtr,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);
            //LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new BatchNewModel(row)).ToList();

            conn.Close();
            return result;

        }

        public static bool UpdateInspectedItems(OrderModel Order, int ItemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            bool updated = false;
            try
            {
                var command = new SqlCommand
                {
                    CommandText = "spUpdateInspectedItems", //"sp_AddItemsByCPandOrderCode",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add("@OrderNumber", SqlDbType.Int);
                command.Parameters["@OrderNumber"].Value = Order.GroupCode;
                command.Parameters.Add("@ItemNumber", SqlDbType.Int);
                command.Parameters["@ItemNumber"].Value = ItemNumber;
                command.Parameters.Add("@AuthorId", SqlDbType.Int);
                command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);
                command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
                command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);
                command.Parameters.Add("@CurrentOfficeId", SqlDbType.Int).Value = Order.GroupOfficeId;
                command.Parameters.Add("@GroupOfficeID", SqlDbType.Int).Value = Order.GroupOfficeId;
                LogUtils.UpdateSpLogWithParameters(command, p);
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                    updated = true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            conn.Close();
            return updated;
        }

        public static string GetItemsQuantity(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select ItemsQuantity from v0group where groupcode = " + Convert.ToInt32(order),
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            string result = null;
            try
            {
                cda.Fill(cdt);

                if (cdt.Rows.Count > 0)
                    result = cdt.Rows[0].ItemArray[0].ToString();
                else
                    result = null;
            }
            catch (Exception ex)
            {
                result = null;
            }
            conn.Close();
            return result;
        }
        #endregion

        #region Report Lookup
        #endregion

        #region BulkUpdate II
        public static List<ItemModel> GetItemsByOrderCode(string orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            command.Parameters.AddWithValue("@IsNew", DBNull.Value);
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = Convert.ToInt32(orderCode);
            command.Parameters.AddWithValue("@BatchCode", DBNull.Value);
            command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemModel.Create2(row)).ToList();
            conn.Close();
            return result;
        }

        public static List<PartModel> GetPartsMeasure(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "SELECT v0Part.PartID, v0Part.PartName FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID " +
                "WHERE (v0Batch.GroupCode = '" + orderCode + "') AND (v0Batch.BatchCode = '" + batchCode + "')",
                CommandType = CommandType.Text,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PartModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<PartModelMulti> GetPartsMeasureMulti(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "SELECT v0Part.PartID, v0Part.PartName, v0Part.PartTypeId FROM v0Part INNER JOIN v0Batch ON v0Part.ItemTypeID = v0Batch.ItemTypeID " +
                "WHERE (v0Batch.GroupCode = '" + orderCode + "') AND (v0Batch.BatchCode = '" + batchCode + "')",
                CommandType = CommandType.Text,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PartModelMulti(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<MeasureModel> GetMeasures(BatchModel batchModel, bool byCp, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = byCp ? "spGetItemMeasuresByCP" : "spGetMeasuresByItemTypePartID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@ItemTypeID", batchModel.ItemTypeId));
            if (byCp)
            {
                command.Parameters.Add(new SqlParameter("@BatchID", batchModel.BatchId));
            }
            command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureModel(row)).ToList();
            conn.Close();
            return result;
        }
        
        public static List<BatchModel> GetSimilarBatches(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_getSimialrBatches",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", orderCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", batchCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select BatchModel.CreateSimilar(row)).ToList();
            conn.Close();
            return result;
        }
        public static DataTable CreateBulkValuesTable(IEnumerable<MeasureValueModel> values)
        {
            var table = new DataTable("Values");
            
            table.Columns.Add("SetPartId");
            table.Columns.Add("SetMeasureID");
            
            table.Columns.Add("SetMeasureValueID");
            table.Columns.Add("SetMeasureValue");
            table.Columns.Add("SetStringValue");
            table.Columns.Add("SetMaxMeasureValueID");
            table.Columns.Add("SetMaxMeasureValue");
            foreach (var value in values)
            {
                object numVal = null;
                object enumVal = null;
                object textVal = null;
                object numValMax = null;
                object enumValMax = null;
                if (value.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    enumVal = value.MeasureValueIdSql;
                    numVal = DBNull.Value;
                    textVal = DBNull.Value;
                    enumValMax = value.MeasureValueIdMaxSql;
                }
                if (value.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    enumVal = DBNull.Value;
                    numVal = value.MeasureValueSql;
                    textVal = DBNull.Value;
                    numValMax = value.MeasureValueMaxSql;
                }
                if (value.MeasureClass == MeasureModel.MeasureClassText)
                {
                    enumVal = DBNull.Value;
                    numVal = DBNull.Value;
                    textVal = value.StringValueSql;
                }
                table.Rows.Add(new object[]
                {
                    value.PartId, 
                    value.MeasureId, 
                    enumVal, //value.MeasureValueIdSql, 
                    numVal, //value.MeasureValueSql,
                    textVal, //value.StringValueSql
                    enumValMax,
                    numValMax
                });
            }
            table.AcceptChanges();
            return table;
        }
        public static string GetConnection(Page p)
        {
            return "" + p.Session["MyIP_ConnectionString"];
        }
        #region Remeasure
        public static string SaveRemeasure(BulkUpdateMeasureModel updateModel, Page p, int newBatchId = 0)
        {
            var errMsg = SaveMeasureValues(updateModel, p);
            if (!string.IsNullOrEmpty((errMsg))) return errMsg;
            var itemModel = new SingleItemModel
                {BatchId = updateModel.Items[0].BatchId, NewItemCode = "" + updateModel.Items[0].ItemCode};
            if (newBatchId != 0)
                itemModel.NewBatchId = newBatchId;
            errMsg = SetEstimatedValues(itemModel, p);
            return errMsg;
        }
        private const string ValueStringFld = "ValueStringFld";
        private const string ValueNumericFld = "ValueNumericFld";
        private const string ValueEnumFld = "ValueEnumFld";
        public static List<ItemValueEditModel> UpdateMeasureList(DataGrid ItemValuesGrid, List<ItemValueEditModel> itemValues, DataGridItem item, string measureId, Page p)
        {
            if (itemValues.Count == 0) return null;
            //var editingItem = itemValues.ElementAt(0).EditingItem;
            //var batchItemModel = new BatchItemModel { BatchId = editingItem.BatchId, ItemCode = Convert.ToInt32(editingItem.ItemCode) };
            var changedValues = new List<ItemValueEditModel>();
            foreach (DataGridItem dgItem in ItemValuesGrid.Items)
            {
                var key = "" + ItemValuesGrid.DataKeys[dgItem.ItemIndex];
                var itemPartId = key.Split(';')[0];
                var itemMeasureId = key.Split(';')[1];
                var itemValue = itemValues.Find(m => m.PartId == itemPartId && "" + m.MeasureId == itemMeasureId);
                if (itemValue == null) continue;
                var newValue = "";
                if (itemValue.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    var enumFld = dgItem.FindControl(ValueEnumFld) as DropDownList;
                    if (enumFld != null) newValue = enumFld.SelectedValue;
                }
                
                if (!itemValue.HasChanged(newValue)) continue;
                var cloneModel = itemValue.Clone();
                cloneModel.Value = newValue;
                changedValues.Add(cloneModel);
            }

            int idx = item.ItemIndex;
            var enumFldMin = item.FindControl(ValueEnumFld) as DropDownList;
            string speciesName = enumFldMin.SelectedItem.ToString();
            //var itemValues = GetViewState(SessionConstants.ShortReportExtMeasuresForEdit) as List<ItemValueEditModel>;
            var newItemsValues = itemValues.ToList();
            var blockVarietyList = QueryDropDownBlock.GetDropDownBlockList((int)EnumDropDownBlock.Variety, p);
            string speciesPartId = "";
            if (changedValues.Count != 0)
            {
                foreach (ItemValueEditModel changedValue in changedValues)
                {
                    foreach (ItemValueEditModel itemValue in itemValues)
                    {
                        if (itemValue.MeasureId == changedValue.MeasureId && itemValue.PartId==changedValue.PartId)
                        {
                            itemValue.Value = changedValue.Value;
                            if (itemValue.MeasureId == 53)//species
                                speciesPartId = itemValue.PartId;
                            break;
                        }
                    }
                }
            }
            List<ItemValueEditModel> newItemValues = itemValues.ToList();
            int varietyMeasureId = 54;
            List < EnumMeasureModel > varieties = GetVarietyBySpecies(speciesName, p);
            if (varieties == null || varieties.Count == 0)
                return newItemsValues;
            foreach (ItemValueEditModel itemvalue in newItemsValues)
            {
                if (itemvalue.MeasureId == varietyMeasureId && itemvalue.PartId == speciesPartId)//Variety
                {
                    itemvalue.EnumSource.Clear();
                    foreach (EnumMeasureModel variety in varieties)
                    {
                        string valueTitle = variety.ValueTitle;
                        var blockedValue = blockVarietyList.Find(m => m.BlockedDisplayName == valueTitle);
                        if (blockedValue != null)
                            continue;
                        string valueName = variety.MeasureValueName;
                        int measureValueId = Convert.ToInt32(variety.MeasureValueId);
                        var newEntry = new EnumMeasureModel
                        {
                            ValueTitle = valueTitle,
                            MeasureValueName = valueName,
                            MeasureValueId = measureValueId,
                            MeasureClass = MeasureModel.MeasureClassEnum,
                            MeasureValueMeasureId = 54
                        };
                        itemvalue.EnumSource.Add(newEntry);
                    }
                    break;
                }
            }
            //DataTable dt = QueryUtils.getSpeciesVariety(speciesName, p);
            //if (dt == null || dt.Rows.Count == 0)
            //    return null;
            //foreach (ItemValueEditModel itemvalue in newItemsValues)
            //{
            //    if (itemvalue.MeasureId == varietyMeasureId)//Variety
            //    {
            //        itemvalue.EnumSource.Clear();
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            string valueTitle = dr["VarietyTitle"].ToString();
            //            string valueName = dr["VarietyName"].ToString();
            //            int measureValueId = Convert.ToInt32(dr["VarietyID"].ToString());
            //            var newEntry = new EnumMeasureModel
            //            {
            //                ValueTitle = valueTitle,
            //                MeasureValueName = valueName,
            //                MeasureValueId = measureValueId,
            //                MeasureClass = MeasureModel.MeasureClassEnum,
            //                MeasureValueMeasureId = 54
            //            };
            //            itemvalue.EnumSource.Add(newEntry);
            //        }
            //        break;
            //    }
            //}
            return newItemsValues;
        }

        
        #endregion
        public static string SaveMeasureValues_New(BulkUpdateMeasureModel updateModel, Page p)
        {
            var msg = "";
            foreach (var batch in updateModel.Items)
            {
                using (var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
                {
                    conn.Open();

                    var command = new SqlCommand
                    {
                        CommandText = "spSaveMeasureBulkEx_VY_A",
                        Connection = conn,
                        CommandType = CommandType.StoredProcedure,
                        CommandTimeout = p.Session.Timeout
                    };
                    command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
                    command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
                    command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
                    command.Parameters.Add(new SqlParameter("@ItemTypeID", updateModel.ItemTypeId));

                    //-- Make Batches table

                    //var msg = "";
                    //foreach (var batch in updateModel.Items)
                    //{
                    var table = new DataTable("Batches");
                    table.Columns.Add("BatchId");
                    table.Columns.Add("ItemCode");
                    table.Rows.Add(new object[] { batch.BatchId, batch.ItemCode });
                    table.AcceptChanges();

                    var param = new SqlParameter
                    {
                        ParameterName = "BatchItemList",
                        SqlDbType = SqlDbType.Structured,
                        Value = table,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(param);

                    //-- Make Measure Values table
                    var tableValues = CreateBulkValuesTable(updateModel.MeasureValues);
                    var paramValues = new SqlParameter()
                    {
                        ParameterName = "MeasureList",
                        SqlDbType = SqlDbType.Structured,
                        Value = tableValues,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(paramValues);
                    LogUtils.UpdateSpLogWithParameters(command, p);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        msg = msg + "\n\r" + ex.Message;
                        LogUtils.UpdateSpLogWithException(msg, p);
                    }
                }
                //command.Dispose();
                //conn.Dispose();
                //conn.Close();
            }
            return msg;
        }
        public static string SaveMeasureValues(BulkUpdateMeasureModel updateModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spSaveMeasureBulkExBulkFor",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
                command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
                command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
                command.Parameters.Add(new SqlParameter("@ItemTypeID", updateModel.ItemTypeId));

                //-- Make Batches table
                var table = new DataTable("Batches");
                table.Columns.Add("BatchId");
                table.Columns.Add("ItemCode");
                foreach (var batch in updateModel.Items)
                {
                    table.Rows.Add(new object[] {batch.BatchId, batch.ItemCode});
                }

                table.AcceptChanges();

                var param = new SqlParameter
                {
                    ParameterName = "BatchItemList",
                    SqlDbType = SqlDbType.Structured,
                    Value = table,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(param);

                //-- Make Measure Values table
                var tableValues = CreateBulkValuesTable(updateModel.MeasureValues);
                var paramValues = new SqlParameter()
                {
                    ParameterName = "MeasureList",
                    SqlDbType = SqlDbType.Structured,
                    Value = tableValues,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(paramValues);
                LogUtils.UpdateSpLogWithParameters(command, p);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            finally
            {
                conn.Close();
            }
            return msg;
        }

//        public static string AddFeedBack(string sender, string email, string subject, string text, Page p)
//        {
//            var errMsg = "";
//            var conn = new SqlConnection(GetConnection(p));
//            conn.Open();
//            var command = new SqlCommand
//            {
//                CommandText = "addFeedback",
//                Connection = conn,
//                CommandType = CommandType.StoredProcedure,
//                CommandTimeout = p.Session.Timeout
//            };
//            command.Parameters.Add(new SqlParameter("@Sender", sender));
//            command.Parameters.Add(new SqlParameter("@Email", email));
//            command.Parameters.Add(new SqlParameter("@Subject", subject));
//            command.Parameters.Add(new SqlParameter("@Text", text));
//            try
//            {
//                command.ExecuteNonQuery();
//            }
//
//            catch (Exception ex)
//            {
//                errMsg = ex.Message;
//                
//            }
//            conn.Close();
//            return errMsg;
//        }
        #endregion

        #region Leo Report
        public static List<ShortReportModel> GetLeoFullOrder(string order, out string  msg, Page p, out List<MovedItemModel> orderMovedItems)
        {
            msg = "";
            orderMovedItems = new List<MovedItemModel>();

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetLeoFullOrder",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", order));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dataAdapter = new SqlDataAdapter(command);

            var ds = new DataSet("FullOrder");
            try
            {
                dataAdapter.Fill(ds);
            } catch (Exception ex)
            {
                msg = ex.Message;
                return null;
            }


            #region Batches
            var batches = new List<BatchModel>();
            foreach (DataRow batchRow in ds.Tables[IndexBatch].Rows)
            {
                var batchModel = new BatchModel(batchRow);
                batchModel.ItemTypeId = Convert.ToInt32(batchRow["ItemTypeID"]);
                var photos = ds.Tables[IndexPicture].Select("BatchID = " + batchModel.BatchId);
                if (photos.Length > 0)
                {
                    batchModel.PathToPicture = "" + photos[0]["Path2Picture"];
                }
                batches.Add(batchModel);
            }
            #endregion

            var shortReports = new List<ShortReportModel>();
            foreach (var batchModel in batches)
            {
                var shortReport = new ShortReportModel { BatchModel = batchModel };
                //-- Label Documents
                var lblRows = ds.Tables[IndexLblDocs].Select("BatchID = " + batchModel.BatchId);
                foreach (var lblDoc in lblRows)
                {
                    shortReport.LabelDocuments.Add(new DocumentModel(lblDoc));
                }

                //-- Document structure
                var docRows = ds.Tables[IndexDocStructure].Select("BatchID = " + batchModel.BatchId);
                //if (docRows.Length == 0) continue;  //-- No attached SRT document, skip
                foreach (var docRow in docRows)
                {
                    var structModel = DocStructModel.Create1(docRow);
                    shortReport.DocStructure.Add(structModel);
                    shortReport.DocumentId = "" + docRow["DocumentID"];
                    shortReport.DocumentName = "" + docRow["DocumentName"];
                }
                
                //-- Rules Items


                //-- Failed Items
                var ruleRows = ds.Tables[IndexFailedItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var ruleRow in ruleRows)
                {
                    var rule = new ItemFailedModel(ruleRow);
                    if (!rule.IsGood)
                    {
                        shortReport.ItemsFailed.Add(rule);    
                    }
                    
                }


                //-- Moved Items
                var movedItems = ds.Tables[IndexMovedItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var movedItem in movedItems)
                {
                    shortReport.MovedItems.Add(new MovedItemModel(movedItem));
                }

                //-- Item Values
                var itemValues = ds.Tables[IndexItemValues].Select("BatchID = " + batchModel.BatchId);
                foreach (var itemValue in itemValues)
                {
                    shortReport.ItemValues.Add(ItemValueModel.Create1(itemValue));
                }

                //-- Items
                var items = ds.Tables[IndexItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var item in items)
                {
                    var itemModel = new ItemModel(item);
                    var virtualModel =
                        shortReport.ItemValues.Find(
                            m =>
                            m.NewItemNumber == itemModel.FullItemNumber &&
                            m.MeasureCode == DbConstants.MeasureCodeVirtualNumber);
                    if (virtualModel != null)
                    {
                        itemModel.PdfFile = PrintUtils.FindPdfDocument(virtualModel.OldItemNumber, virtualModel.ResultValue, p);
                    }
                    shortReport.Items.Add(itemModel);
                }
                
                shortReports.Add(shortReport);

                //-- Ordered Items
                var itemQueues = ds.Tables[IndexOrderedItems].Select("BatchID = " + batchModel.BatchId);
                foreach (var itemQueue in itemQueues)
                {
                    shortReport.ItemsQueue.Add(new QueueModel(itemQueue));    
                }
                //-- Set Ordered state
                foreach(var item in shortReport.Items)
                {
                    var ordered = shortReport.ItemsQueue.Find(m => m.NewItem == item.FullItemNumber);
                    if (ordered != null) item.LeoOrdered = "Y";
                }

                //-- Printed Items
                var itemsPrinted =
                    ds.Tables[IndexPrintedItems].Select("BatchCode ='" + batchModel.FullBatchNumber + "'");
                foreach(var itemPrinted in itemsPrinted)
                {
                    shortReport.ItemsPrinted.Add(new PrintedModel(itemPrinted, Convert.ToInt32(shortReport.BatchModel.BatchId)));
                }
                //-- Set Printed state
                foreach (var item in shortReport.Items)
                {
                    var printed = shortReport.ItemsPrinted.Find(m => m.FullItemNumber == item.FullItemNumber && m.IsPrinted);
                    if (printed != null) item.LeoPrinted = "Y";
                }

            }
            foreach (DataRow row in ds.Tables[IndexMovedItems].Rows)
            {
                orderMovedItems.Add(new MovedItemModel(row));
            }
            return shortReports;

        }
        public static string SetLeoOrderReports(List<string> itemNumbers, Page p)
        {
            var table = new DataTable("ItemNumbers");
            table.Columns.Add("Item");
            table.Columns.Add("GroupCode");
            table.Columns.Add("BatchCode");
            table.Columns.Add("ItemCode");

            foreach(var itemNumber in itemNumbers)
            {
                var parts = itemNumber.Split('.');
                var item = parts[0] + parts[1] + parts[2];
                table.Rows.Add(new object[] {item, parts[0], parts[1], parts[2]});
            }
            table.AcceptChanges();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "sp_LeoOrderReports",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@Author", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOffice", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            
            var param = new SqlParameter
            {
                ParameterName = "ItemNumbers",
                SqlDbType = SqlDbType.Structured,
                Value = table,
                Direction = ParameterDirection.Input
            };

            command.Parameters.Add(param);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            } catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static ItemModel GetNewItemNumberFromOld(ItemModel oldModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetNewItemNumberFromOldItemNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@GroupCode", oldModel.OrderCode));
            command.Parameters.Add(new SqlParameter("@BatchCode", oldModel.BatchCode));
            command.Parameters.Add(new SqlParameter("@ItemCode", oldModel.ItemCode));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            ItemModel result = null;
            if (dt.Rows.Count > 0)
            {
                result = ItemModel.Create1(dt.Rows[0]);
            }
            conn.Close();
            return result;

        }
        #endregion

        #region Reassembly (CopyData)
        public static ItemCopyModel GetItemByCode(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            command.Parameters.Add("@IsNew", SqlDbType.Int).Value = 1;
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = Utils.ParseOrderCode(itemNumber);
            command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = Utils.ParseBatchCode(itemNumber);
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = Utils.ParseItemCode(itemNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count != 1)
            {
                conn.Close();
                return null;
            }
            var itemCopyModel = new ItemCopyModel(dt.Rows[0]);
            var batchModel = new BatchModel
            {
                CustomerId = "" + dt.Rows[0]["CustomerID"],
                CustomerOfficeId = "" + dt.Rows[0]["CustomerOfficeID"]
            };
            itemCopyModel.CustomerName = GetCustomerName(batchModel, p).CustomerName;
            conn.Close();
            itemCopyModel.Measures = GetMeasures(itemCopyModel, p);
            return itemCopyModel;
        }
        public static List<ItemValueModel> GetMeasures(ItemCopyModel itemCopyModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPartValueByBatchIDItemCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@BatchID", SqlDbType.Int).Value = itemCopyModel.BatchId;
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCopyModel.ItemCode;
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemValueModel.Create5(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.SortExpr, m2.SortExpr, StringComparison.Ordinal));
            conn.Close();
            return result;
        }
        public static string CopyItemPart(ItemCopyModel fromModel, ItemCopyModel toModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_CopyItemPart2ItemPartByBatchIDItemCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@BatchID_From", SqlDbType.Int).Value = fromModel.BatchId;
            command.Parameters.Add("@ItemCodeFrom", SqlDbType.Int).Value = fromModel.ItemCode;
            command.Parameters.Add("@PartID_From", SqlDbType.Int).Value = fromModel.CopyPartId;

            command.Parameters.Add("@BatchID_To", SqlDbType.Int).Value = toModel.BatchId;
            command.Parameters.Add("@ItemCodeTo", SqlDbType.Int).Value = toModel.ItemCode;
            command.Parameters.Add("@PartID_To", SqlDbType.Int).Value = toModel.CopyPartId;

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = fromModel.OfficeId;
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            } catch(Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }

public static string CopyDiffItemPart(ItemCopyModel fromModel, ItemCopyModel toModel, int srcPartType, int trgPartType,Page p)
        {

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_CopyItemPart2ItemPartByBatchIDItemCodeDiff",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@BatchID_From", SqlDbType.Int).Value = fromModel.BatchId;
            command.Parameters.Add("@ItemCodeFrom", SqlDbType.Int).Value = fromModel.ItemCode;
            command.Parameters.Add("@PartID_From", SqlDbType.Int).Value = fromModel.CopyPartId;

            command.Parameters.Add("@BatchID_To", SqlDbType.Int).Value = toModel.BatchId;
            command.Parameters.Add("@ItemCodeTo", SqlDbType.Int).Value = toModel.ItemCode;
            command.Parameters.Add("@PartID_To", SqlDbType.Int).Value = toModel.CopyPartId;

            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = fromModel.OfficeId;
            command.Parameters.Add("@SrcPartTypeID", SqlDbType.Int).Value = srcPartType;
            command.Parameters.Add("@TrgPartTypeID", SqlDbType.Int).Value = trgPartType;
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        #endregion
        #region ItemizeOldNew
        public static DataRow GetNewCustomerProgramInstanceByBatchCode(int ordercode, int batchcode, int itemcode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetNewCustomerProgramInstanceByBatchCode", //"spGetNewCustomerProgramInstanceByBatchCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@GroupCode", SqlDbType.Int);
            command.Parameters["@GroupCode"].Value = ordercode;
            command.Parameters.Add("@BatchCode", SqlDbType.Int);
            command.Parameters["@BatchCode"].Value = batchcode;
            command.Parameters.Add("@ItemCode", SqlDbType.Int);
            command.Parameters["@ItemCode"].Value = itemcode;
            command.Parameters.Add("@AuthorId", SqlDbType.Int);
            command.Parameters["@AuthorID"].Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int);
            command.Parameters["@AuthorOfficeID"].Value = int.Parse("" + p.Session["AuthorOfficeID"]);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                conn.Close();
                return null;
            }
            var result = dt.Rows[0]; //(from DataRow row in dt.Rows select new BatchNewModel(row)).ToList();

            conn.Close();
            return result;
        }
        #endregion
        #region Remote Session
        public static void RemoteSessionAdd(LoginModel loginModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spRemoteSessionAdd",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Ip", loginModel.UserIp.Ip);
            command.Parameters.AddWithValue("@CountryCode", loginModel.UserIp.CountryCode);
            command.Parameters.AddWithValue("@ZipCode", loginModel.UserIp.RegionCode);
            command.Parameters.AddWithValue("@UserId", loginModel.User.UserId);

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
            }
            conn.Close();
        }
        public static List<RemoteSessionModel> GetRemoteSessions(StatsFilterModel filterModel, out int pageCount, out int rowsCount, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetRemoteSessionByFilter",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filterModel.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filterModel.DateFrom;
            }
            if (filterModel.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filterModel.DateTo;
            }
            if (!string.IsNullOrEmpty(filterModel.UserId) && filterModel.UserId != "0")
            {
                command.Parameters.AddWithValue("@UserId", filterModel.UserId);
            }
            command.Parameters.AddWithValue("@PageNo", filterModel.PageNo);
            command.Parameters.AddWithValue("@PageSize", PaginatorUtils.SessionCountOnPage);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var result = (from DataRow row in ds.Tables[0].Rows select new RemoteSessionModel(row)).ToList();
            pageCount = Convert.ToInt32(ds.Tables[1].Rows[0]["pagesCount"]);
            rowsCount = Convert.ToInt32(ds.Tables[1].Rows[0]["rowsCount"]);
            conn.Close();
            return result;
        }

        public static List<UserModel> GetAppUsers(Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "SELECT UserID, FirstName, LastName FROM v0User",
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new UserModel(row)).ToList();
            conn.Close();
            result.Sort((m1, m2) => string.Compare(m1.DisplayName, m2.DisplayName, StringComparison.Ordinal));
            result.Insert(0, new UserModel { FirstName = "All" });
            return result;
        }

        public static DataSet GetBatchWithCustomer(int batchid, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetBatchWithCustomer_New",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@BatchID", batchid);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }

        public static DataSet GetGroupWithCustomer(string groupOfficeId, string groupId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGroupWithCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupID", Convert.ToInt32(groupId));
            command.Parameters.AddWithValue(@"GroupOfficeID", Convert.ToInt16(groupOfficeId));
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }

        public static DataSet GetItemByCode(string groupCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", Convert.ToInt32(groupCode));
            command.Parameters.AddWithValue(@"BatchCode", Convert.ToInt16(batchCode));
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }
        public static DataTable GetItemByCode(long groupCode, int batchCode, int itemCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", groupCode);
            command.Parameters.AddWithValue("@BatchCode", batchCode);
            command.Parameters.AddWithValue("@ItemCode", itemCode);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@IsNew", 1);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var dt = new DataTable();
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }
        public static DataSet GetCarriers(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCarriers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }

        public static DataSet GetCheckedOperations(int batchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetNameCheckedOperationByBatchID",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }

        public static DataSet GetCIDInfo(string groupCode, string batchCode, string itemCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCIDInfo",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", groupCode);
            command.Parameters.AddWithValue("@Batch", batchCode);
            command.Parameters.AddWithValue("@Item", itemCode);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }
        public static DataSet GetItem_New_1(int batchId, Page p)
        {
            int itemCode = 0;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItem_New_1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@ItemCode", itemCode);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }
        public static DataSet GetServiceType(string serviceTypeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetServiceType",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ServiceTypeID", Convert.ToInt16(serviceTypeId));
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var ds = new DataSet();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                conn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return null;
            }
        }
        #endregion

        #region Connenction & Login
        public static string GetDbName(string connection)
        {
            var conn = new SqlConnection("" + connection);
            return conn.Database + ": " + conn.DataSource;
        }
        
        public static int SetNewPassword(LoginModel loginModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_ChangePassword",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@Result", SqlDbType.Int);
            command.Parameters["@Result"].Direction = ParameterDirection.Output;

            command.Parameters.Add("@UserID", SqlDbType.Int);
            command.Parameters["@UserID"].Value = loginModel.User.UserId;

            command.Parameters.Add("@OldPassword", SqlDbType.VarChar, 255);
            command.Parameters["@OldPassword"].Value = loginModel.Password;

            command.Parameters.Add("@NewPassword", SqlDbType.VarChar, 255);
            command.Parameters["@NewPassword"].Value = loginModel.NewPassword;
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return 0;
            }

            var result =  Int32.Parse(command.Parameters["@Result"].Value.ToString());
            conn.Close();
            return result;
        }
        
        private static void SetSessionParameters(LoginModel loginModel, Page p)
        {
            //-- For Old Version
            p.Session["ID"] = loginModel.User.UserId;
            p.Session["FirstName"] = loginModel.User.FirstName;
            p.Session["LastName"] = loginModel.User.LastName;
            p.Session["AuthorOfficeID"] = loginModel.AuthorOfficeId;

            p.Session["LoginName"] = loginModel.LoginName;
            p.Session["PasswordString"] = loginModel.Password;
            p.Session["MyIP_ConnectionString"] = p.Session["ConnectionString"].ToString();
            switch (loginModel.AuthorOfficeId.ToString())
            {
                case "1":
                case "4":
                case "5":
                    p.Session["OfficeIPGroup"] = "NY";
                    break;
                case "2":
                case "3":
                case "6":
                    p.Session["OfficeIPGroup"] = "NY";
                    break;
                default:
                    p.Session["OfficeIPGroup"] = "NY";
                    break;
            }
        }
        public static IpModel GetUserIp(Page p)
        {
            var ipModel = new IpModel();
            ipModel.Ip = LocationUtils.GetIPAddress(p.Request);
            
            if (!string.IsNullOrEmpty(ipModel.Ip))
            {
                ipModel = LocationUtils.GetIpParameters(ipModel);
            }
            return ipModel;
        }
       
        public static LoginModel GetLogin(LoginModel loginModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            { 
                CommandText = "SELECT Login, Password, UserID, FirstName, LastName, OfficeID, RoleID FROM v0User WHERE Login = @Login AND Password = @PassWord",
                Connection = conn,
                CommandTimeout = p.Session.Timeout 
            };

            command.Parameters.AddWithValue("@Login", loginModel.LoginName);
            command.Parameters.AddWithValue("@Password", loginModel.Password);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                
                var row = dt.Rows[0];
                loginModel.User.UserId = Convert.ToInt32(row["UserID"]);
                loginModel.User.FirstName = "" + row["FirstName"];
                loginModel.User.LastName = "" + row["LastName"];
                loginModel.AuthorOfficeId = Convert.ToInt32(row["OfficeID"]);
                HttpContext.Current.Session["RoleID"] = row["RoleID"].ToString();
                //-- For Old Version
                SetSessionParameters(loginModel, p);
                
                try
                {
                    //Try to check GeoIP of user
                    loginModel.UserIp = GetUserIp(p);
                    //var tmpIp = loginModel.UserIp.Ip; for testing
                    RemoteSessionAdd(loginModel, p);

                    var errMsg = "";
                    var hasRemoteAccess = AccessUtils.HasAccessToPage("" + loginModel.User.UserId, AccessUtils.PrgRemoteSession, p, out errMsg);

                    if (hasRemoteAccess)
                    {
                        conn.Close();
                        return loginModel;
                    }
                    //var fromOfficeIp = AccessUtils.GetOfficeIps(p, out errMsg).Contains(loginModel.UserIp.Ip);
                    var userip = p.Request.UserHostAddress;
                    var fromOfficeIp = AccessUtils.GetOfficeIps(p, out errMsg).Contains(userip);
                    //var fromOfficeHost = AccessUtils.GetOfficeIpsByHost(p, out errMsg).Contains(loginModel.UserIp.Ip);
                    var fromOfficeHost = AccessUtils.GetOfficeIpsByHost(p, out errMsg).Contains(userip);
                    var fromOffice = fromOfficeIp || fromOfficeHost;
                    if (!fromOffice)
                    {
                        loginModel.Error = true;
                        //loginModel.ErrorText = "You don't have permission to use NewCorpt application out of office! IP= " + userip + ", " + tmpIp; for testing
                        loginModel.ErrorText = "You don't have permission to use NewCorpt application out of office!";
                    }
                    
                    /*alex
                    //string _domain = IPGlobalProperties.GetIPGlobalProperties().DomainName;
                    
                    var localDomain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
                    _domain = localDomain;
                    //alex
                    bool fromOfficeHost = false;
                    if (_domain != "gsi.local")
                    {
                        string externalip = new WebClient().DownloadString("https://api.ipify.org/");
                        fromOfficeHost = AccessUtils.GetOfficeIpsByHost(p, out errMsg).Contains(externalip);
                        //if (!hasRemoteAccess && !fromOfficeHost)
                        if (!fromOfficeHost)
                        {
                            LogUtils.UpdateSpLogWithException(@"domain=" + _domain + @", externalip=" + externalip + @", ip=" + loginModel.UserIp.Ip, p);
                            loginModel.Error = true;
                            loginModel.ErrorText = "You don't have permission to use NewCorpt application out of office! Domain: " + _domain;
                        }
                    }
                    alex */
                }
                catch (Exception e)
                {
                    try
                    {
                        MailMessage myEmail;
                        //myEmail = new MailMessage("program.update@gemscience.net", "sasha@gemscience.net");
                        //myEmail.From = new MailAddress("program.update@gemscience.net");
                        myEmail = new MailMessage();
                        myEmail.From = new MailAddress("app@gemscience.net", null);
                        myEmail.Subject = "Login Error";
                        myEmail.Body = @"Error to login to GDLight: " + e.Message;//error; // "Uploaded " + i.ToString() + " JPG files on the leo FTP.";
                                                                                                   //myEmail.From = new MailAddress("info@gemscience.net");
                        myEmail.To.Add("Andrew.Deegan@GemScience.net,alexander.remennik@gemscience.net");
                        SmtpClient client = new SmtpClient("smtp.office365.com");
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential("app@gemscience.net", "Testing123");
                        client.Port = 587;
                        client.EnableSsl = true;
                        client.Send(myEmail);
                        /*
                        //Bypass GeoIP check and send email
                        MailMessage mail = new MailMessage("Web.Alert@gemscience net", "SergeyG@gemscience.net");
                        SmtpClient client = new SmtpClient("gemscience-net.mail.protection.outlook.com");
                        client.Port = 25;
                        client.UseDefaultCredentials = false;
                        mail.Subject = "GeoIP Failure in NewCorpt";
                        mail.Body = "NewCorpt has failed to find a user's GeoIP and has generated the following error: " + e.Message;
                        client.Send(mail);
                        */
                    }
                    catch
                    {
                        //Email failed to send
                    }
                }
                /*
                if (hasRemoteAccess && !fromOffice)
                {
                    RemoteSessionAdd(loginModel, p);
                }
                */
            } else
            {
                var userip = p.Request.UserHostAddress;
                if (p.Request.UserHostAddress != null)
                {
                    Int64 macinfo = new Int64();
                    string macSrc = macinfo.ToString("X");
                    if (macSrc == "0")
                    {
                        if (userip == "127.0.0.1")
                        {
                            p.Response.Write("visited Localhost!");
                        }
                        else
                        {
                            //string abc = userip;
                        }
                    }
                }
                //loginModel.UserIp = GetUserIp(p);
                //string tmpIp = loginModel.UserIp.Ip;
                loginModel.Error = true;
                //loginModel.ErrorText = "Incorrect user name/password combination. Please try again. Domain: " + userip + ", " + tmpIp; for testing
                loginModel.ErrorText = "Incorrect user name/password combination. Please try again.";
            }
            conn.Close();
            return loginModel;
        }
		public static DataTable GetOfficeList(string IPAddress,string City)
		{
            string con = HttpContext.Current.Session["MyIP_ConnectionString"].ToString();
            var conn = new SqlConnection(con);
            conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetOfficeList",
				CommandType = CommandType.StoredProcedure,
				Connection = conn,
				CommandTimeout = HttpContext.Current.Session.Timeout
			};
            command.Parameters.AddWithValue("@IPAddress", IPAddress);
            command.Parameters.AddWithValue("@City", City);
            //LogUtils.UpdateSpLogWithParameters(command, p);
			var da = new SqlDataAdapter(command);
			var dt = new DataTable();
			da.Fill(dt);
			return dt;
		}
        public static DataTable GetAllOfficeList()
        {
            string con = HttpContext.Current.Session["MyIP_ConnectionString"].ToString();
            var conn = new SqlConnection(con);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetAllOfficeList",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = HttpContext.Current.Session.Timeout
            };
            //LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        #endregion

        #region Send Mail
        public static List<PersonModel> GetPersonsByBatchIds(List<ShortReportModel> reports, Page p)
        {
            var pt = new DataTable();
            pt.Columns.Add("ID");
            foreach (var report in reports)
            {
                pt.Rows.Add(new object[] { report.BatchModel.BatchId });
            }
            pt.AcceptChanges();
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPersonsByBatchIds",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var param = new SqlParameter
            {
                ParameterName = "BatchIds",
                SqlDbType = SqlDbType.Structured,
                Value = pt,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PersonModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<PersonModel> GetPersons(string customerId, string customerOfficeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetPersonsByCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerOfficeID", customerOfficeId);
            command.Parameters.AddWithValue("@CustomerID", customerId);
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new PersonModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Billing Info
        public static List<BillingInfoModel>  GetBillingInfo(string orderCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_getInvoiceForBatch",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new BillingInfoModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region PhotoSpex
        public static List<PsxNormDataModel>  GetScanData(PsxFilterModel filterModel, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "psxFindScansNORMOITemNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filterModel.DateFrom != null)
            {
                command.Parameters.AddWithValue("@DateFrom", filterModel.DateFrom);
            }
            if (filterModel.DateTo != null)
            {
                command.Parameters.AddWithValue("@DateTo", ((DateTime)filterModel.DateTo).AddDays(1));
            }
            if (filterModel.HasScan)
            {
                command.Parameters.Add("@ScanID", SqlDbType.VarChar).Size = 50;
                command.Parameters["@ScanID"].Value = "%" + filterModel.ScanLike + "%";

            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);

            var result = dt.Rows.Cast<DataRow>().Select(row => new PsxNormDataModel(row)).Where(data => data.CompareFactor(filterModel.Xfactor)).ToList();
            conn.Close();
            return result;

        }
        #endregion

        #region Open Batch List

        public static Tuple<List<OpenBatchModel>, DataTable> GetOpenBatchList(string CustomerID, string OfficeCode, string Status, DateTime? FromDate, DateTime? ToDate, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_ListOpenBatchesWithFilterCriteria",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@CustomerID", CustomerID);
            command.Parameters.AddWithValue("@OfficeCode", OfficeCode);            
            command.Parameters.AddWithValue("@Status", Status);

            if (FromDate != null)
            {
                command.Parameters.Add("@FromDate", SqlDbType.DateTime);
                command.Parameters["@FromDate"].Value = FromDate;
            }
            if (ToDate != null)
            {
                command.Parameters.Add("@ToDate", SqlDbType.DateTime);
                command.Parameters["@ToDate"].Value = ToDate;
            }

            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var ds = new DataSet();
            da.Fill(ds);
            var result = (from DataRow row in ds.Tables[0].Rows select new OpenBatchModel(row)).ToList();
            var result2 = ds.Tables[1];
            conn.Close();
            return Tuple.Create(result, result2);
        }

        public static int GetOpenBatchSum(Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_ListOpenBatchesSUM",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0) return 0;
            return Convert.ToInt32(dt.Rows[0]["TotalNumberOfItems"]);

        }
        #endregion

        #region Customer Name by ItemNumber
        public static string GetCustomerByItemNumber(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetCustomerNameByItemNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", Utils.ParseOrderCode(itemNumber));
            command.Parameters.AddWithValue("@BatchCode", Utils.ParseBatchCode(itemNumber));
            command.Parameters.AddWithValue("@ItemCode", Utils.ParseItemCode(itemNumber));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var name = dt.Rows.Count == 0 ? "" : Convert.ToString(dt.Rows[0]["CustomerName"]);
            conn.Close();
            return name;
        }
        #endregion

        #region Customer, Vendor, CP
        public static List<CustomerModel> GetAllCustomers(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspvvGetCustomerList",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select CustomerModel.Create1(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            result.Insert(0, new CustomerModel { CustomerId = null, CustomerName = "(All)" });
            conn.Close();
            return result;
        }
        public static List<CustomerProgramModel>  GetVendorPrograms(string vendor, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspvvGetCustomerPrograms",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@CustomerID", SqlDbType.Int).Value = int.Parse(vendor);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select CustomerProgramModel.Create1(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerProgramName, m2.CustomerProgramName, StringComparison.Ordinal));
            conn.Close();
            return result;

        }
        public static List<CustomerVendorCpModel>  GetCustomerVendorCps(List<CustomerModel> customers, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select * from tblCustomerVendorCP",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new CustomerVendorCpModel(row, customers)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            conn.Close();
            return result;
        }

        public static DataTable GetCustomerCodeByItem(string gsiNumber, Page p)
        {
            Utils.DissectItemNumber(gsiNumber, out string order, out string _, out string _);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "Select customercode, servicetypeid from v0group where groupcode=" + order,
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            
            conn.Close();
            return cdt;
        }
        #endregion

        #region Sorting Stat
        public static List<SortingStatModel> GetSortingStats(DateTime? dateFrom, DateTime? dateTo, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_SortingStat",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (dateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = dateFrom;
            }

            if (dateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = ((DateTime)dateTo).AddDays(1);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new SortingStatModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Leslie Stat
        public static List<string> GetLesliePrograms(string cpName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select distinct customerprogramname from v0customerprogram where customerprogramname like '%" + cpName + "%'",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select row["CustomerProgramName"].ToString()).ToList();
            conn.Close();
            return result;
            
        }
        public static List<ColorClarityStatModel> GetLeslieList(string cp, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_StatColorClarity2",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@cpName", cp);
            command.Parameters.AddWithValue("@monthly", 1);
            command.Parameters.AddWithValue("@companyName", 1);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new ColorClarityStatModel(row)).ToList();
            conn.Close();
            return result;
            
        }
        #endregion

        #region Mark Laser
        public static string InsertRecordLaserLog(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spRecordLaserLog",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@RecordText", itemNumber));
            command.Parameters.Add(new SqlParameter("@UserID", p.Session["ID"].ToString()));
            command.Parameters.Add(new SqlParameter("@OperationType", 1));
            command.Parameters.Add(new SqlParameter("@GroupCode", Utils.ParseOrderCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@BatchCode", Utils.ParseBatchCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@ItemCode", Utils.ParseItemCode(itemNumber)));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery(); 
            }
            
            catch(Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static string SetLaserValue(string itemNumber, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemDataFromOrderBatchItem",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@GroupCode", Utils.ParseOrderCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@BatchCode", Utils.ParseBatchCode(itemNumber)));
            command.Parameters.Add(new SqlParameter("@ItemCode", Utils.ParseItemCode(itemNumber)));
            
            command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            if (cdt.Rows.Count == 0)
            {
                conn.Close();
                return string.Format("The Item Number {0} is not found", itemNumber);

            }
            var rows = cdt.Select("MeasureCode=147");
            if (rows.Length == 0)
            {
                conn.Close();
                return string.Format("The 'LI type' measure is not found");
            }
            foreach (var row in rows)
            {
                command = new SqlCommand
                {
                    CommandText = "spSetPartValue",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add(new SqlParameter("@rId", SqlDbType.VarChar));
                command.Parameters["@rId"].Size = 150;
                command.Parameters["@rId"].Direction = ParameterDirection.Output;
                var newItemCode = row["NewItemNumber"].ToString().Trim().Substring(row["NewItemNumber"].ToString().Trim().Length - 2);
                command.Parameters.Add(new SqlParameter("@ItemCode", newItemCode));
                command.Parameters.Add(new SqlParameter("@BatchID", row["BatchID"].ToString()));
                command.Parameters.Add(new SqlParameter("@PartID", row["PartID"].ToString()));
                command.Parameters.Add(new SqlParameter("@MeasureCode", 67));
                command.Parameters.Add(new SqlParameter("@MeasureValue", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@MeasureValueID", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@StringValue", itemNumber));
                command.Parameters.Add(new SqlParameter("@UseClosedRecheckSession", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
                command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
                command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
                LogUtils.UpdateSpLogWithParameters(command, p);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch(Exception x)
                {
                    msg = "UPATE FAILED!!!!: " + x.Message;
                    LogUtils.UpdateSpLogWithException(msg, p);
                    break;
                }

            }
            conn.Close();
            return msg;

        }
        #endregion

        #region End Session

        public static string SetEndSession(SingleItemModel item, Page p)
        {
            return SetEndSession(item.NewBatchId, Convert.ToInt32(item.NewItemCode), p);
        }

        public static string SetEndSession(int batchId, int itemCode, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spEndRecheckSession",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.VarChar);
            command.Parameters["@rId"].Size = 150;
            command.Parameters["@rID"].Direction = ParameterDirection.Output;
            command.Parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = itemCode;
            command.Parameters.Add("@CurrentOfficeID", SqlDbType.Int).Value = 3;
            command.Parameters.AddWithValue("@AuthorId", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return "Error: " + msg;
            }

            conn.Close();
            return command.Parameters["@rId"].Value.ToString();
        }
        #endregion

        #region AutoMeasure
        public static string SetWeightRandomMeasure(MeasRandomModel measModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetRandomDecimalMeasureByBatchIDWithLoginInfoEx", // original spSetRandomDecimalMeasureByBatchIDWithLoginInfo
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //-- Make Batches table
            var table = new DataTable("Batches");
            table.Columns.Add("BatchId");
            table.Columns.Add("ItemCode");
            foreach (var batch in measModel.BatchItems)
            {
                table.Rows.Add(new object[] { batch.BatchId, batch.ItemCode });
            }
            table.AcceptChanges();

            var param = new SqlParameter
            {
                ParameterName = "BatchItemList",
                SqlDbType = SqlDbType.Structured,
                Value = table,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);

            command.Parameters.Add(new SqlParameter("@PartID", measModel.PartId));
            command.Parameters.Add(new SqlParameter("@MeasureID", measModel.MeasureId));
            command.Parameters.Add(new SqlParameter("@StartValue", measModel.WeightFrom));
            command.Parameters.Add(new SqlParameter("@StopValue", measModel.WeightTo));
            command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }

            command.Connection.Close();
            return msg;
        }
        public static string SetRandomMeasurements(MeasRandomModel measModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetRandomMeasurementsByBatchIDWithLoginInfoEx", // original spSetRandomMeasurementsByBatchIDWithLoginInfo
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //-- Make Batches table
            var table = new DataTable("Batches");
            table.Columns.Add("BatchId");
            table.Columns.Add("ItemCode");
            foreach (var batch in measModel.BatchItems)
            {
                table.Rows.Add(new object[] { batch.BatchId, batch.ItemCode });
            }
            table.AcceptChanges();

            var param = new SqlParameter
            {
                ParameterName = "BatchItemList",
                SqlDbType = SqlDbType.Structured,
                Value = table,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);

            command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@PartID", measModel.PartId));
            command.Parameters.Add(new SqlParameter("@StartValueMax", measModel.MaxFrom));
            command.Parameters.Add(new SqlParameter("@EndValueMax", measModel.MaxTo));
            command.Parameters.Add(new SqlParameter("@StartValueMin", measModel.MinFrom));
            command.Parameters.Add(new SqlParameter("@EndValueMin", measModel.MinTo));
            command.Parameters.Add(new SqlParameter("@StartValueH_x", measModel.HxFrom));
            command.Parameters.Add(new SqlParameter("@EndValueH_x", measModel.HxTo));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();    
            } catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            
            command.Connection.Close();
            return msg;
        }
        #endregion

        #region AutoMeasure (New)
        public static string AutoMeasureSave(List<MeasureValueModel> values, Page p)
        {
            return SavePreFilling(values, p);
        }
        #endregion

        #region Number Generator
        public static List<LabelModel> NumberGenerator(string prefix, int numberOfLabels, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "GetNumbers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@NumberOfLabels", numberOfLabels);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new LabelModel(row, prefix)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Itemized/Certified Labels
        public static List<LabelModel> GetItemizedLabels(string orderCode, string batchCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "sp_GetBatchForLabels",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@OrderCode", orderCode);
            command.Parameters.AddWithValue("@BatchCode", batchCode);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select LabelModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<BatchNumberModel> GetItemsByBatchRealNumbers(string batchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            //-- Batch Items
            var command = new SqlCommand
            {
                CommandText = "wspvvGetItemsByBatchRealNumbers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@BatchID", batchId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var items = (from DataRow row in cdt.Rows select new BatchNumberModel(row)).ToList();
            conn.Close();
            return items;
        }
        public static List<LabelModel> GetCertifiedLabels(string batchId, string docId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var items = GetItemsByBatchRealNumbers(batchId, p);

            //-- Document Structure
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentValue",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@DocumentID", Int32.Parse(docId));
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var docStruct = (from DataRow row in cdt.Rows select new DocStructModel(row)).ToList();
            if (docStruct.Count > 4)
            {
                docStruct.RemoveAt(0);  //-- ?
            }

            conn.Close();
            return null;
        }

        public static DataTable GetGroupByCode(string newOrder, Page p)
        {//spGetGroupByCode3
            //command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetGroupByCode3",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(newOrder)));
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            //command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(parts[0])));
            //command.Parameters.Add(new SqlParameter("@BatchCode", Convert.ToInt32(parts[1])));
            //command.Parameters.Add(new SqlParameter("@ItemCode", Convert.ToInt32(parts[2])));
            try
            {
                var cda = new SqlDataAdapter(command);
                var dt = new DataTable();
                cda.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                conn.Close();
                return null;
            }
        }

        public static DataTable GetCPPerCustomer(string newCustomerId, Page p)
        {//spGetCustomerProgramsPerCustomer
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCustomerProgramsPerCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.Add("@CustomerOfficeID", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.Add(new SqlParameter("@CustomerID", Convert.ToInt32(newCustomerId)));
            command.Parameters.AddWithValue("@VendorOfficeID", DBNull.Value);
            command.Parameters.AddWithValue("@VendorID", DBNull.Value);
            try
            {
                var cda = new SqlDataAdapter(command);
                var dt = new DataTable();
                cda.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                conn.Close();
                return null;
            }
        }

        public static DataTable GetOldCustomerProgramByBatchCode(string oldItem, Page p)
        {
            string[] parts = oldItem.Split('.');
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetNewCustomerProgramInstanceByBatchCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.Add(new SqlParameter("@GroupCode", Convert.ToInt32(parts[0])));
            command.Parameters.Add(new SqlParameter("@BatchCode", Convert.ToInt32(parts[1])));
            command.Parameters.Add(new SqlParameter("@ItemCode", Convert.ToInt32(parts[2])));
            try
            {
                var cda = new SqlDataAdapter(command);
                var dt = new DataTable();
                cda.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                conn.Close();
                return null;
            }
        }
        #endregion

        #region Delivery Log
        public static List<DeliveryLogModel> GetDeliveryLog(Page p, string outEventId)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkGetItemsOut",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@StageOut", outEventId));  //-- 'Sent to Avi Polish' or 'Sent to Photoscribe Engraving'
            command.Parameters.Add(new SqlParameter("@StageIn", outEventId == "14" ? "15" : "17"));   //-- 'Received from Avi Polish' or 'Received to Photoscribe Engraving'
            LogUtils.UpdateSpLogWithParameters(command, p);
            var cda = new SqlDataAdapter(command);
            var cdt = new DataTable();
            cda.Fill(cdt);
            var result = (from DataRow row in cdt.Rows select new DeliveryLogModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<DeliveryLogModel> GetBatchDeliveryLog(string batchId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkSpGetDeliveryLog",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", Convert.ToInt32(batchId)));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select DeliveryLogModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<EventModel> GetTrkEventsList(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select * from trkEvent;",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EventModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<ItemModel> GetItemsByBatchNumber(string batchNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorId", SqlDbType.Int).Value = int.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeId", SqlDbType.Int).Value = int.Parse("" + p.Session["AuthorOfficeId"]);
            command.Parameters.AddWithValue("@CustomerCode", DBNull.Value);
            command.Parameters.AddWithValue("@BGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@EGroupState", DBNull.Value);
            command.Parameters.AddWithValue("@BState", DBNull.Value);
            command.Parameters.AddWithValue("@EState", DBNull.Value);
            command.Parameters.AddWithValue("@BDate", DBNull.Value);
            command.Parameters.AddWithValue("@EDate", DBNull.Value);
            command.Parameters.AddWithValue("@IsNew", DBNull.Value);
            command.Parameters.Add("@GroupCode", SqlDbType.Int).Value = Utils.ParseOrderCode(batchNumber);
            command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = Utils.ParseBatchCode(batchNumber);
            command.Parameters.AddWithValue("@ItemCode", DBNull.Value);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemModel.Create2(row)).ToList();
            conn.Close();
            return result;
        }

        public static string AddRecordsDeliveryLog(List<ItemModel> items, string batchNumber, string eventId, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "trkSpSetDeliveryLogEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            //-- Make Items table
            var table = new DataTable("Items");
            table.Columns.Add("ItemNumber",typeof(string));
            table.Columns.Add("ItemCode", typeof(int));
            foreach (var item in items)
            {
                table.Rows.Add(new object[] { item.OldFullIemNumber, Convert.ToInt32(item.ItemCode) });
            }
            table.AcceptChanges();

            var param = new SqlParameter
            {
                ParameterName = "ItemNumbers",
                SqlDbType = SqlDbType.Structured,
                Value = table,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(param);
            command.Parameters.Add(new SqlParameter("@BatchNumber",batchNumber));
            command.Parameters.AddWithValue("@AuthorId", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(new SqlParameter("@trkEventID", eventId));
            command.Parameters.Add(new SqlParameter("@BatchID", items.ElementAt(0).NewBatchId));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();

            return msg;
        }
        public static void AddLogToDB(string message, HttpSessionState session, string url="Exception")
        {
            string userId = "";
            string sessionId = session.SessionID;
            var conn = new SqlConnection("" + session["MyIP_ConnectionString"]);
            if (session["ID"] != null)
                userId = session["ID"].ToString();
            else
                return;
            message = message.Replace("'", "");
            string sql = null;
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                //string sql = null;
                if (message.Length > 8000)
                    message = message.Substring(0, 7900);
                sql = @"insert into  tblGDLightLog(Message, ID, Session, Page) values('" + message + @"', '" + userId + @"', '" + sessionId + @"', '" + url + @"')";
                
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
            }
            catch (Exception e)
            {
                var msg = e.Message;
                //LogUtils.UpdateSpLogWithParameters(command, p);
                conn.Close();
            }
        }
        #endregion

        #region Grade
        public static string UpdateMeasureValue(UpdateMeasureModel updateModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spSetPartValueCCM",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@rId", SqlDbType.Int);
            command.Parameters["@rId"].Direction = ParameterDirection.Output;
            command.Parameters["@rId"].Value = DBNull.Value;

            command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
            command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));

            command.Parameters.AddWithValue("@ItemCode", updateModel.Item.NewItemCode);
            command.Parameters.AddWithValue("@BatchID",updateModel.Item.NewBatchId);
            command.Parameters.AddWithValue("@PartID", updateModel.MeasureValue.PartId);
            command.Parameters.AddWithValue("@MeasureCode", updateModel.MeasureValue.MeasureCode);
            command.Parameters.AddWithValue("@UseClosedRecheckSession", DBNull.Value);

            if (updateModel.MeasureValue.MeasureClass == MeasureModel.MeasureClassEnum)
            {
                command.Parameters.Add("@MeasureValueID", SqlDbType.Int).Value = updateModel.MeasureValue.MeasureValueIdSql;
                command.Parameters.Add("@MeasureValue", SqlDbType.Float).Value = DBNull.Value;
                command.Parameters.Add("@StringValue", SqlDbType.VarChar).Value = DBNull.Value;
            }
            if (updateModel.MeasureValue.MeasureClass == MeasureModel.MeasureClassNumeric)
            {
                command.Parameters.Add("@MeasureValueID", SqlDbType.Int).Value = DBNull.Value;
                command.Parameters.Add("@MeasureValue", SqlDbType.Float).Value = updateModel.MeasureValue.MeasureValueSql;
                command.Parameters.Add("@StringValue", SqlDbType.VarChar).Value = DBNull.Value;
            }
            if (updateModel.MeasureValue.MeasureClass == MeasureModel.MeasureClassText)
            {
                command.Parameters.Add("@MeasureValueID", SqlDbType.Int).Value = DBNull.Value;
                command.Parameters.Add("@MeasureValue", SqlDbType.Float).Value = DBNull.Value;
                command.Parameters.Add("@StringValue", SqlDbType.VarChar).Size = 100;
                command.Parameters["@StringValue"].Value = updateModel.MeasureValue.StringValueSql;
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            } catch(Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            if (!string.IsNullOrEmpty(msg)) return msg;
            msg = SetEstimatedValues(updateModel.Item, p);
            return msg;
        }
        public static string UpdateTracking(TrackingUpdateModel trackModel, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspSetBatchHistory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@BatchID", trackModel.BatchId));
            command.Parameters.Add(new SqlParameter("@FormID", trackModel.ViewAccess));
            command.Parameters.Add(new SqlParameter("@UserID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@EventID", trackModel.EventId)); 
            command.Parameters.Add(new SqlParameter("@NumberOfItemsAffected", trackModel.ItemsAffected));
            command.Parameters.Add(new SqlParameter("@NumberOfItemsInBatch", trackModel.ItemsInBatch));
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            } catch (Exception x)
            {
                msg = x.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        public static List<MeasureValueCpModel> GetMeasureListByAcces(SingleItemModel itemModel, string accessCode, bool ignoreCp, Page p)
        {
            var listByCp = GetMeasureListByMode(itemModel, accessCode, p);
            if (!ignoreCp) return listByCp;
            var listByMode = GetMeasureListByModeIgnoreCp(itemModel, accessCode, p);
            foreach(var byMode in listByMode)
            {
                if (listByCp.Find(m=> m.PartId == byMode.PartId && m.MeasureId == byMode.MeasureId ) == null)
                {
                    byMode.ByCp = false;
                    listByCp.Add(byMode);
                }
            }
            return listByCp;
        }
        private static IEnumerable<MeasureValueCpModel> GetMeasureListByModeIgnoreCp(SingleItemModel itemModel, string accessCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMeasuresByItemTypeAccess",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ItemTypeID", itemModel.ItemTypeId);
            if (string.IsNullOrEmpty(accessCode))
            {
                command.Parameters.Add("@ViewAccessCode", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
                command.Parameters.AddWithValue("@ViewAccessCode", accessCode);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureValueCpModel(row, itemModel)).ToList();
            conn.Close();
            result.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));
            return result;

        }
        public static List<MeasureValueCpModel>  GetMeasureListByMode(SingleItemModel itemModel, string accessCode, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemMeasuresByViewAccessAndCP",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@AuthorID", "" + p.Session["ID"]);
            command.Parameters.AddWithValue("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(new SqlParameter("@BatchID", itemModel.BatchId));
            command.Parameters.AddWithValue("@ItemTypeID", itemModel.ItemTypeId);
            if (string.IsNullOrEmpty(accessCode))
            {
                command.Parameters.AddWithValue("@ViewAccessCode", DBNull.Value);
            } else
            {
                command.Parameters.AddWithValue("@ViewAccessCode", accessCode);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureValueCpModel(row, itemModel)).ToList();
            conn.Close();
            result.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));
            return result;

        }
        #endregion

        #region Find by Measure Value
        public static List<MeasureModel> GetStringMeasures(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "select * from tblMeasure where MeasureClass=2 order by MeasureName",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select MeasureModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }
        public static List<JumpPageModel> FindItemsByMeasureValue(string measureId, string value, int maxRows, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spFindItemByValue",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@MeasureID", measureId);
            command.Parameters.AddWithValue("@StringValue", value + "%");
            command.Parameters.AddWithValue("@rows", maxRows);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select ItemValueModel.Create4(row)).ToList();
            if (result.Count == 0) return new List<JumpPageModel>();

            //-- Find Order Code, Batch Code
            var groups = result.GroupBy(m => m.BatchId).ToList();
            var criteria = "";
            foreach (var group in groups)
            {
                criteria += string.IsNullOrEmpty(criteria) ? "" : ", ";
                criteria += @group.Key;
            }
            var orderCommand = new SqlCommand()
            {
                CommandText = "select * from v0Batch where BatchID in (" + criteria + ")",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var oda = new SqlDataAdapter(orderCommand);
            var tbl = new DataTable();
            oda.Fill(tbl);
            var batches = (from DataRow row in tbl.Rows select new BatchModel(row)).ToList();
            var jumps = result.Select(res => JumpPageModel.Create(batches, res)).ToList();
            conn.Close();
            return jumps.FindAll(m => !string.IsNullOrEmpty(m.OrderCode));
        }
        #endregion

        #region Measure HotKeys
        public static List<MeasureHotKeyModel>  GetMeasureHotKeys(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetHotKeys",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MeasureHotKeyModel(row)).ToList();
            return result;
        }
        #endregion

        #region Pre-Filling Measures
        public static List<PreFillingValueModel> GetPreFillingData(int order, Page p)
        {
            var enumItems = QueryUtils.GetEnumMeasure(p);

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetCPDocRuleEx",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", order);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }

            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            var values = (from DataRow row in dt.Rows select new PreFillingValueModel(row, enumItems)).ToList();
            return values;
        }
        public static string SavePreFilling(List<MeasureValueModel> values, Page p )
        {

            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            try
            {
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spSavePreFilling",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add(new SqlParameter("@ErrMsg", SqlDbType.NVarChar));
                command.Parameters["@ErrMsg"].Direction = ParameterDirection.Output;
                command.Parameters["@ErrMsg"].Size = 1000;
                command.Parameters.Add(new SqlParameter("@AuthorId", "" + p.Session["ID"]));
                command.Parameters.Add(new SqlParameter("@AuthorOfficeId", "" + p.Session["AuthorOfficeID"]));
                command.Parameters.Add(new SqlParameter("@CurrentOfficeID", "" + p.Session["AuthorOfficeID"]));

                //-- Make Measure Values table
                var tableValues = CreatePreFillingTable(values);
                var paramValues = new SqlParameter()
                {
                    ParameterName = "PreFillingList",
                    SqlDbType = SqlDbType.Structured,
                    Value = tableValues,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(paramValues);
                LogUtils.UpdateSpLogWithParameters(command, p);

                command.ExecuteNonQuery();
                msg = command.Parameters["@ErrMsg"].Value.ToString();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            finally
            {
                conn.Close();
            }
            return msg;
        }
        private static DataTable CreateIdTable(List<string> ids)
        {
            var table = new DataTable("Ids");
            table.Columns.Add("ID");
            foreach (var id in ids)
            {
                table.Rows.Add(new object[]
                {
                    id
                });
            }
            return table;
        }
        private static DataTable CreatePreFillingTable(IEnumerable<MeasureValueModel> values)
        {
            var table = new DataTable("Values");

            table.Columns.Add("BatchID");
            table.Columns.Add("ItemCode");
            table.Columns.Add("PartId");
            table.Columns.Add("MeasureCode");

            table.Columns.Add("MeasureValueID");
            table.Columns.Add("MeasureValue");
            table.Columns.Add("StringValue");
            foreach (var value in values)
            {
                object numVal = null;
                object enumVal = null;
                object textVal = null;
                if (value.MeasureClass == MeasureModel.MeasureClassEnum)
                {
                    enumVal = value.MeasureValueIdSql;
                    numVal = DBNull.Value;
                    textVal = DBNull.Value;
                }
                if (value.MeasureClass == MeasureModel.MeasureClassNumeric)
                {
                    enumVal = DBNull.Value;
                    numVal = value.MeasureValueSql;
                    textVal = DBNull.Value;
                }
                if (value.MeasureClass == MeasureModel.MeasureClassText)
                {
                    enumVal = DBNull.Value;
                    numVal = DBNull.Value;
                    textVal = value.StringValueSql;
                }
                table.Rows.Add(new object[]
                {
                    value.BatchId,
                    value.ItemCode,
                    value.PartId, 
                    value.MeasureCode, 
                    enumVal, //value.MeasureValueIdSql, 
                    numVal, //value.MeasureValueSql,
                    textVal //value.StringValueSql
                });
            }
            table.AcceptChanges();
            return table;
        }
        public static DataTable GetLaserInscrFromLiType(int batchId, int itemCode, int liTypeValueId, Page p)
        {
            var msg = "";
            var dt = new DataTable();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetLaserInscrFromLITYPe",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@BatchID", batchId);
            command.Parameters.AddWithValue("@Itemcode", itemCode);
            command.Parameters.AddWithValue("@MeasureValueID", liTypeValueId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                var da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch (Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                dt = null;
            }
            conn.Close();
            return dt;
        }
        #endregion

        #region Item History
        public static List<ItemHistoryModel> GetItemHistory(ItemHistoryFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "spGetItemHistory",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add(new SqlParameter("@filtrGroupCode", filter.Order));
            command.Parameters.Add(string.IsNullOrEmpty(filter.Batch) ? 
                new SqlParameter("@filtrBatchCode", DBNull.Value) : 
                new SqlParameter("@filtrBatchCode", filter.Batch));
            command.Parameters.Add(string.IsNullOrEmpty(filter.Item) ?
                new SqlParameter("@filtrItemCode", DBNull.Value) :
                new SqlParameter("@filtrItemCode", filter.Item));
            command.Parameters.Add(filter.DateFrom == null ?
                new SqlParameter("@filtrDateFrom", DBNull.Value) :
                new SqlParameter("@filtrDateFrom", filter.DateFrom));
            command.Parameters.Add(filter.DateTo == null ?
                new SqlParameter("@filtrDateTo", DBNull.Value) :
                new SqlParameter("@filtrDateTo", filter.DateTo));
            command.Parameters.Add(string.IsNullOrEmpty(filter.AuthorId) ?
                new SqlParameter("@filtrAuthorID", DBNull.Value) :
                new SqlParameter("@filtrAuthorID", filter.AuthorId));
            command.Parameters.Add(string.IsNullOrEmpty(filter.AuthorOfficeId) ?
                new SqlParameter("@filtrAuthorOfficeID", DBNull.Value) :
                new SqlParameter("@filtrAuthorOfficeID", filter.AuthorOfficeId));
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            command.Parameters.Add(new SqlParameter("@rand", 10000));
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet("ItemHistory");
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            p.Session["HistoryData"] = ds;
            var result = (from DataRow row in ds.Tables[1].Rows select new ItemHistoryModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region VV Admin
        public static bool IsExistsReport(string reportNumber, string virtualVault, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                CommandText = "wspVerifyVirtualVault2",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", Utils.ParseOrderCode(reportNumber));
            command.Parameters.AddWithValue("@BatchCode", Utils.ParseBatchCode(reportNumber));
            command.Parameters.AddWithValue("@ItemCode", Utils.ParseItemCode(reportNumber));
            command.Parameters.AddWithValue("@VirtualVaultNumber", virtualVault);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (dt.Rows.Count > 0 && dt.Rows[0][0].ToString() == "1");
            conn.Close();
            return result;
        }
        public static List<CustomerInfoModel> FindUserByEmail(string email, Page p)
        {
            var conn = new SqlConnection("" + p.Session["VVConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spFindUser",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@Email", email);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new CustomerInfoModel(row)).ToList();
            conn.Close();
            return result;
        }
        public static string UpdateVvPassword(string userId, string oldPwd, string newPwd, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["VVConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "wspUpdateCustomerPassword",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            //@Result
            command.Parameters.Add(new SqlParameter("@Result", SqlDbType.Int));
            command.Parameters["@Result"].Direction = ParameterDirection.Output;

            command.Parameters.AddWithValue("@ID", userId);
            command.Parameters.AddWithValue("@OldPassword", oldPwd);
            command.Parameters.AddWithValue("@NewPassword", newPwd);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                msg = x.Message;
            }
            conn.Close();
            return msg;
        }
        public static List<CustomerInfoModel> SearchUsersByReportNumber(string reportNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["VVConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spFindByReportNumber",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ReportNumber", reportNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select CustomerInfoModel.CreateModel(row)).ToList();
            result.Sort((m1, m2) => string.CompareOrdinal(m1.ReportNumber.ToUpper(), m2.ReportNumber.ToUpper()));
            conn.Close();
            return result;
        }
        public static string NewUserActivation(CustomerInfoModel info, Page p)
        {
            //-- If exists
            var users = FindUserByEmail(info.Email, p);
            var conn = new SqlConnection("" + p.Session["VVConnectionString"]);
            conn.Open();
            var result = "";
            if (users.Count == 1)
            {
                var command = new SqlCommand
                {
                    CommandText = "wspInsertVaultInfo",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@ID", users.ElementAt(0).Id);
                command.Parameters.Add("@ReportNumber", SqlDbType.VarChar);
                command.Parameters["@ReportNumber"].Size = 20;
                command.Parameters["@ReportNumber"].Value = info.ReportNumber;

                command.Parameters.Add("@VirtualVaultNumber", SqlDbType.VarChar);
                command.Parameters["@VirtualVaultNumber"].Size = 20;
                command.Parameters["@VirtualVaultNumber"].Value = info.VirtualVaultNumber;

                command.Parameters.Add("@CCResponse", SqlDbType.VarChar);
                command.Parameters["@CCResponse"].Size = 2000;
                command.Parameters["@CCResponse"].Value = "manual creation";
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception x)
                {
                    result = x.Message;
                }
            } 
            else
            {
                var command = new SqlCommand
                {
                    CommandText = "wspInsertCustomerInfo",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.Add("@FirstName", SqlDbType.VarChar);
                command.Parameters["@FirstName"].Size = 50;
                command.Parameters["@FirstName"].Value = info.FirstName;

                command.Parameters.Add("@MI", SqlDbType.VarChar);
                command.Parameters["@MI"].Size = 1;
                command.Parameters["@MI"].Value = info.MiddleName;

                command.Parameters.Add("@LastName", SqlDbType.VarChar);
                command.Parameters["@LastName"].Size = 50;
                command.Parameters["@LastName"].Value = info.LastName;

                command.Parameters.Add("@Address", SqlDbType.VarChar);
                command.Parameters["@Address"].Size = 100;
                command.Parameters["@Address"].Value = info.Address;

                command.Parameters.Add("@City", SqlDbType.VarChar);
                command.Parameters["@City"].Size = 50;
                command.Parameters["@City"].Value = info.City;

                command.Parameters.Add("@Zip", SqlDbType.VarChar);
                command.Parameters["@Zip"].Size = 10;
                command.Parameters["@Zip"].Value = info.Zip;

                command.Parameters.Add("@State", SqlDbType.VarChar);
                command.Parameters["@State"].Size = 2;
                command.Parameters["@State"].Value = info.State;

                command.Parameters.Add("@DayPhone", SqlDbType.VarChar);
                command.Parameters["@DayPhone"].Size = 20;
                command.Parameters["@DayPhone"].Value = info.DayPhone;

                command.Parameters.Add("@OtherPhone", SqlDbType.VarChar);
                command.Parameters["@OtherPhone"].Size = 20;
                command.Parameters["@OtherPhone"].Value = info.OtherPhone;

                command.Parameters.Add("@Email", SqlDbType.VarChar);
                command.Parameters["@Email"].Size = 100;
                command.Parameters["@Email"].Value = info.Email;

                command.Parameters.Add("@Password", SqlDbType.VarChar);
                command.Parameters["@Password"].Size = 12;
                command.Parameters["@Password"].Value = info.Password;

                command.Parameters.Add("@CCResponse", SqlDbType.VarChar);
                command.Parameters["@CCResponse"].Size = 2000;
                command.Parameters["@CCResponse"].Value = "manual creation";

                command.Parameters.Add("@ReportNumber", SqlDbType.VarChar);
                command.Parameters["@ReportNumber"].Size = 20;
                command.Parameters["@ReportNumber"].Value = info.ReportNumber;

                command.Parameters.Add("@VirtualVaultNumber", SqlDbType.VarChar);
                command.Parameters["@VirtualVaultNumber"].Size = 20;
                command.Parameters["@VirtualVaultNumber"].Value = info.VirtualVaultNumber;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception x)
                {
                    result = x.Message;
                }
            }
            conn.Close();
            return result;
        }
        #endregion

        #region Migrated Items
        public static List<MigratedItemModel>  GetMigratedItems(string order, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetMigratedItemByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", order);
            command.Parameters.AddWithValue("@BatchCode", 0);
            command.Parameters.AddWithValue("@ItemCode", 0);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new MigratedItemModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Remeasure
        public static RemeasParamModel GetNewItemNumber(string itemNumber, Page p)
        {
            var order = Utils.ParseOrderCode(itemNumber);
            var batch = Utils.ParseBatchCode(itemNumber);
            var item = Utils.ParseItemCode(itemNumber);
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetNewItemCustomerCodeByCode",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@GroupCode", order);
            command.Parameters.AddWithValue("@OrderCode", order);
            command.Parameters.AddWithValue("@BatchCode", batch);
            command.Parameters.AddWithValue("@ItemCode", item);
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = new RemeasParamModel(itemNumber);
            if (dt.Rows.Count > 0)
            {
                result = new RemeasParamModel(dt.Rows[0], itemNumber);
            }
            conn.Close();
            return result;
        }
        #endregion
        
        #region Stats Stone Numbers
        public static List<StoneCustomerModel> GetStoneQuantityByCustomer(StatsFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetStoneQuantitiesByCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            if (!string.IsNullOrEmpty(filter.CustomerId))
            {
                command.Parameters.AddWithValue("@CustomerId", filter.CustomerId);
            }
            if (!string.IsNullOrEmpty(filter.OrderStateCode))
            {
                command.Parameters.AddWithValue("@OrderStateCode", filter.OrderStateCode);
            }

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new StoneCustomerModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            conn.Close();
            return result;
        }
        //-- By OfficeId with totals per customer
        public static List<StoneCustomerModel> GetStoneQuantityByLocationPerCustomer(StatsFilterModel filter, string officeId, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetStoneQuantitiesByOfficeAndCustomer",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            if (!string.IsNullOrEmpty(filter.OrderStateCode))
            {
                command.Parameters.AddWithValue("@OrderStateCode", filter.OrderStateCode);
            }
            command.Parameters.AddWithValue("@OfficeId", officeId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new StoneCustomerModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            conn.Close();
            return result;
        }

        
        public static List<StoneLocationModel> GetStoneQuantityByLocation(StatsFilterModel filter, Page p)
        {
            if (filter.OfficesIds.Count == 0) return new List<StoneLocationModel>();
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetStoneQuantities",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            if (!string.IsNullOrEmpty(filter.OrderStateCode)) {
                command.Parameters.AddWithValue("@OrderStateCode", filter.OrderStateCode);
            }
            var ids = CreateIdTable(filter.OfficesIds);
            var paramValues = new SqlParameter()
            {
                ParameterName = "OfficeIds",
                SqlDbType = SqlDbType.Structured,
                Value = ids,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new StoneLocationModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.Country, m2.Country, StringComparison.Ordinal));
            conn.Close();
            return result;
        }
        public static List<StoneNumberModel> GetStatsStoneNumber(StatsFilterModel model, out int cnt, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spStatStoneNumbers",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
            command.Parameters["@DateFrom"].Value = model.DateFrom;

            command.Parameters.Add("@DateTo", SqlDbType.DateTime);
            command.Parameters["@DateTo"].Value = model.DateTo;

            command.Parameters.AddWithValue("@PageNo", model.PageNo);
            command.Parameters.AddWithValue("@PageSize", PaginatorUtils.StoneNumbersCountOnPage);

            if (!string.IsNullOrEmpty(model.CustomerId))
            {
                command.Parameters.AddWithValue("@CustomerId", model.CustomerId);
            }

            if (!string.IsNullOrEmpty(model.OfficeId))
            {
                command.Parameters.AddWithValue("@ParentOfficeId", model.OfficeId);
            }
            
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var result = (from DataRow row in ds.Tables[0].Rows select new StoneNumberModel(row)).ToList();
            cnt = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
            conn.Close();
            return result;
        }
        public static List<CountryOfficeModel> GetCountryOffices(Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spCountryOffices",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var countries = (from DataRow row in ds.Tables[0].Rows select new CountryOfficeModel(row)).ToList();
            foreach (var country in countries)
            {
                var officeRows = ds.Tables[1].Select("ParentOfficeID=" + country.ParentOfficeId);
                var offices = (from DataRow row in officeRows select new OfficeModel(row)).ToList();
                offices.Sort((m1, m2) => string.Compare(m1.OfficeCode, m2.OfficeCode, StringComparison.Ordinal));
                country.Offices.AddRange(offices);
            }
            //result.Insert(0, new CountryOfficeModel());
            conn.Close();
            return countries;
        }
        #endregion

        #region Stats Report Numbers
        public static List<DocumentTypeModel> GetDocumentTypes(Page p)
        {
            //spGetDocumentTypes
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetDocumentTypes",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
            command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new DocumentTypeModel(row)).ToList();

            conn.Close();
            return result;
        }

        public static List<StatReportModel> GetReportQuantityByCustomer(StatsFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetReportQuantityByCustomer1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            if (!string.IsNullOrEmpty(filter.CustomerId))
            {
                command.Parameters.AddWithValue("@CustomerId", filter.CustomerId);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new StatReportModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            conn.Close();
            return result;

        }
        public static List<StatReportModel> GetReportQuantityByDocumentType(StatsFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetReportQuantityByDocumentType1",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select StatReportModel.Create(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.DocumentTypeName, m2.DocumentTypeName, StringComparison.Ordinal));
            conn.Close();
            return result;

        }

        public static string GetReportQuantity(StatsFilterModel filter, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetReportQuantity",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = "" + dt.Rows[0][0];
            conn.Close();
            return result;
        }
        #endregion

        #region Stats Old/New Number
        public static List<ItemCycleModel> GetItemLifeCicle(string itemNumber, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetItemLifeCycle",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.Add("@OrderCode", SqlDbType.Int).Value = Utils.ParseOrderCode(itemNumber);
            command.Parameters.Add("@BatchCode", SqlDbType.Int).Value = Utils.ParseBatchCode(itemNumber);
            command.Parameters.Add("@ItemCode", SqlDbType.Int).Value = Utils.ParseItemCode(itemNumber);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new ItemCycleModel(row)).ToList();
            conn.Close();
            return result;
        }
        #endregion

        #region Rejection Stats Report
       
        #region Reasons + Measures Rules
        //-- Get Reasons
        public static List<RejectionReasonModel> GetRejectionReasons(Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                CommandText = "spGetRejectReason",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new RejectionReasonModel(row)).ToList();
            conn.Close();
            return result;
        }
        //-- Add Reason
        public static void AddRejectReason(string reasonName, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                CommandText = "spAddRejectReason",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@ReasonName", reasonName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogUtils.UpdateSpLogWithException(ex.Message, p);
            }
            conn.Close();
        }
        //-- : Add Measures to Reason
        public static string SetReasonMeasureRule(List<string> measureIds, string reasonId, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                CommandText = "spSetReasonMeasureRule",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            var ids = CreateIdTable(measureIds);
            var paramValues = new SqlParameter()
            {
                ParameterName = "MeasureIds",
                SqlDbType = SqlDbType.Structured,
                Value = ids,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramValues);
            if (!string.IsNullOrEmpty(reasonId))
            {
                command.Parameters.AddWithValue("@ReasonId", reasonId);
            }
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return ex.Message;
            }
            conn.Close();
            return "";
        }

        //-- Get Reason + Measures Rules
        public static List<ReasonMeasureModel> GetReasonMeasureRules(out List<MeasureModel> measures, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                CommandText = "spGetReasonMeasureRules",
                CommandType = CommandType.StoredProcedure,
                Connection = conn,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var da = new SqlDataAdapter(command);
            var ds = new DataSet();
            da.Fill(ds);

            //-- Attached Measures
            var result = (from DataRow row in ds.Tables[0].Rows select new ReasonMeasureModel(row)).ToList();
            result.Sort((m1, m2) => String.Compare(m1.MeasureName, m2.MeasureName, StringComparison.Ordinal));
            
            //-- UnAttached Measures
            measures = (from DataRow row in ds.Tables[1].Rows select MeasureModel.Create1(row)).ToList();
            conn.Close();
            return result;
        }

        #endregion
        
        #region Rejected Stone, New Report

        //-- Order State Codes
        public static List<OrderStateModel> GetOrderStateCodes(Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetOrderStateCodes",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);

            da.Fill(dt);
            var states = (from DataRow row in dt.Rows select new OrderStateModel(row)).ToList();
            states.Insert(0, new OrderStateModel()); 
            conn.Close();
            return states;
        }
        //-- Create Report
        public static string StatsCalcReportRun(StatsFilterModel filter, Page p)
        {
            if (filter.MeasureIds.Count == 0) return "Not checked Measures!";
            var conn = OpenConnectionStats(p);

            var command = new SqlCommand
            {
                CommandText = "spStatsCalc",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout * 3
            };

            //-- Measure Ids
            var ids = CreateIdTable(filter.MeasureIds);
            var paramMeasures = new SqlParameter()
            {
                ParameterName = "MeasureIds",
                SqlDbType = SqlDbType.Structured,
                Value = ids,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramMeasures);

            //-- Customer Ids
            var customerIds = CreateIdTable(filter.CustomerIds);
            var paramCustomers = new SqlParameter()
            {
                ParameterName = "CustomerIds",
                SqlDbType = SqlDbType.Structured,
                Value = customerIds,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramCustomers);

            //-- Date From, Date To
            if (filter.DateFrom != null)
            {
                command.Parameters.Add("@DateFrom", SqlDbType.DateTime);
                command.Parameters["@DateFrom"].Value = filter.DateFrom;
            }
            if (filter.DateTo != null)
            {
                command.Parameters.Add("@DateTo", SqlDbType.DateTime);
                command.Parameters["@DateTo"].Value = filter.DateTo;
            }
            command.Parameters.Add(new SqlParameter("@ErrMsg", SqlDbType.NVarChar));
            command.Parameters["@ErrMsg"].Direction = ParameterDirection.Output;
            command.Parameters["@ErrMsg"].Size = 4000;

            //-- UserId
            command.Parameters.AddWithValue("@UserId", "" + p.Session["ID"]);

            //-- OrderStateCode
            if (!string.IsNullOrEmpty(filter.OrderStateCode))
            {
                command.Parameters.AddWithValue("@OrderStateCode", filter.OrderStateCode);
            }

            LogUtils.UpdateSpLogWithParameters(command, p);
            var msg = "";
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                msg = ex.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
                return msg;
            }
            conn.Close();
            msg = command.Parameters["@ErrMsg"].Value.ToString();
            return msg;
        }

        //-- Delete Report
        public static string StatsCalcReportRemove(string reportId, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spStatsReportRemove",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                LogUtils.UpdateSpLogWithException(ex.Message, p);
                return ex.Message;
            }
            conn.Close();
            return "";
        }

        #endregion

        #region Rejected Stones, Reports
        //-- Report List
        public static List<StatsReportModel> GetStatsReports(Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                CommandText = "spGetStatsReports",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var reports = (from DataRow row in ds.Tables[0].Rows select new StatsReportModel(row)).ToList();
            foreach (var report in reports)
            {

                var rows = ds.Tables[1].Select("report_id=" + report.ReportId);
                var customers = (from DataRow row in rows select row["CustomerName"].ToString()).ToList();
                report.ReportDetails.Customers = customers;

                var ruleRows = ds.Tables[2].Select("report_id=" + report.ReportId);
                var rules = (from DataRow row in ruleRows select ReasonMeasureModel.CreateShort(row)).ToList();
                report.ReportDetails.Rules = rules;
            }
            conn.Close();
            return reports;
        }

        //-- Report Totals by all customers
        public static List<CalcStatsTotalModel> GetStatsReportTotals(string reportId, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetStatsReportTotals",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var totals = (from DataRow row in ds.Tables[0].Rows select CalcStatsTotalModel.Create(row)).ToList();

            var failed = (from DataRow row in ds.Tables[1].Rows select CalcStatsTotalReasonModel.CreateFailTotal(row)).ToList();
            var missing = (from DataRow row in ds.Tables[2].Rows select CalcStatsTotalReasonModel.CreateMissTotal(row)).ToList();
            foreach (var total in totals)
            {
                total.TotalFail = failed;
                total.TotalMiss = missing;
            }
            conn.Close();
            return totals;
        }

        //-- Report Totals Per Customer
        public static List<CalcStatsTotalModel> GetStatsReportTotalsPerCustomer(string reportId, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetStatsItemTotals",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);

            da.Fill(ds);
            var totals = (from DataRow row in ds.Tables[0].Rows select new CalcStatsTotalModel(row)).ToList();
            var failed = (from DataRow row in ds.Tables[1].Rows select CalcStatsTotalReasonModel.CreateFail(row)).ToList();
            var missing = (from DataRow row in ds.Tables[2].Rows select CalcStatsTotalReasonModel.CreateMiss(row)).ToList();
            foreach (var total in totals)
            {
                total.TotalFail = failed.FindAll(m => m.CustomerId == total.CustomerId);
                total.TotalMiss = missing.FindAll(m => m.CustomerId == total.CustomerId);
            }
            totals.Sort((m1, m2) => String.Compare(m1.CustomerName, m2.CustomerName, StringComparison.Ordinal));
            conn.Close();
            return totals;
        }


        //-- Get Report Details For Export to Excel
        public static int ReportDetailsForExcelPageSize = 500;
        public static List<CalcStatsItemModel> GetStatsReportDetailsForExcel(string reportId, string customerId, string pageNo, out int pageCount, Page p)
        {
            var conn = OpenConnectionStats(p);

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetCustomerStatsItems",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            command.Parameters.AddWithValue("@CustomerId", customerId);
            command.Parameters.AddWithValue("@PageNo", pageNo);
            command.Parameters.AddWithValue("@PageSize", ReportDetailsForExcelPageSize);
            var reasonTable = CreateReasonFilterTable(new List<ReportFilterReasonModel>());
            var paramReasons = new SqlParameter()
            {
                ParameterName = "ReasonFilter",
                SqlDbType = SqlDbType.Structured,
                Value = reasonTable,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramReasons);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            //-- Items
            var items = (from DataRow row in ds.Tables[0].Rows select new CalcStatsItemModel(row)).ToList();
            //-- Item Values
            var itemValues = (from DataRow row in ds.Tables[1].Rows select new CalcStatsItemValueModel(row)).ToList();

            //-- Pagination Info
            var pagination = new PaginationModel(ds.Tables[2].Rows[0]);
            pageCount = Convert.ToInt32(pagination.PageCount);

            //-- Merge Items with his Values
            foreach (var item in items)
            {
                item.ItemValues.AddRange(itemValues.FindAll(m => m.ItemId == item.ItemId));
            }
            conn.Close();
            return items;
        }

        #endregion
       
        #region Rejected Stones, Report Details

        //-- Data For Filter Panel on Report Details Tab
        public static StatsReportFilterSourceModel GetStatsReportFilterSource(string reportId, string customerId, Page p)
        {
            var result = new StatsReportFilterSourceModel();
            var conn = OpenConnectionStats(p);

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetStatsReportFilter",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            command.Parameters.AddWithValue("@CustomerId", customerId);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);

            //-- Customer Programs
            var cps = (from DataRow row in ds.Tables[0].Rows select row["cp_name"].ToString()).ToList();
            result.CustomerPrograms.AddRange(cps);

            var reasons = (from DataRow row in ds.Tables[1].Rows select new ReportFilterReasonModel(row)).ToList();
            foreach (var reason in reasons)
            {
                var valueRows = ds.Tables[2].Select("reason_id = " + reason.ReasonId);
                var values = (from DataRow row in valueRows select row["currValue"].ToString()).ToList();
                reason.Values.AddRange(values);
            }
            //-- Items Count
            var itemsCount = Convert.ToInt32(ds.Tables[3].Rows[0][0]);
            result.Reasons = reasons;
            conn.Close();
            return result;
        }

        //-- Report Details Data
        public static ReportPageModel GetStatsReportDetails(string reportId, string customerId, string pageNo, int pageSize, StatsReportFilterSourceModel filter, Page p)
        {
            var conn = OpenConnectionStats(p);
            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spGetCustomerStatsItems",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.AddWithValue("@ReportId", reportId);
            command.Parameters.AddWithValue("@CustomerId", customerId);
            command.Parameters.AddWithValue("@PageNo", pageNo);
            command.Parameters.AddWithValue("@PageSize", pageSize);
            command.Parameters.AddWithValue("@ItemCriteria", filter.ItemCriteria);
            if (!string.IsNullOrEmpty(filter.RejectionTypeSelected))
            {
                command.Parameters.AddWithValue("@RejectionType", filter.RejectionTypeSelected);
            }

            var reasonTable = CreateReasonFilterTable(filter.Reasons);

            var paramReasons = new SqlParameter()
            {
                ParameterName = "ReasonFilter",
                SqlDbType = SqlDbType.Structured,
                Value = reasonTable,
                Direction = ParameterDirection.Input
            };
            command.Parameters.Add(paramReasons);

            LogUtils.UpdateSpLogWithParameters(command, p);
            var ds = new DataSet();
            var da = new SqlDataAdapter(command);
            da.Fill(ds);
            //-- Items
            var items = (from DataRow row in ds.Tables[0].Rows select new CalcStatsItemModel(row)).ToList();
            //-- Item Values
            var itemValues = (from DataRow row in ds.Tables[1].Rows select new CalcStatsItemValueModel(row)).ToList();

            //-- Pagination Info
            var pagination = new PaginationModel(ds.Tables[2].Rows[0]);

            //-- Dynamic columns
            var additional = StatsUtils.GetReasonColNames(itemValues);

            //-- Merge Items with his Values
            foreach (var item in items)
            {
                item.ItemValues.AddRange(itemValues.FindAll(m => m.ItemId == item.ItemId));
            }

            //-- Create Display DataTable
            DataTable pageData = StatsUtils.CreateTableStruct(additional, items);
            var result = new ReportPageModel { ReportPageTable = pageData, Pagination = pagination };
            conn.Close();
            return result;
        }

        #endregion
        
        #region Rejected Stones, Utils
        private static DataTable CreateReasonFilterTable(List<ReportFilterReasonModel> reasons)
        {
            var table = new DataTable("Ids");
            table.Columns.Add("ReasonID");
            table.Columns.Add("CurrValue");
            foreach (var reason in reasons)
            {
                table.Rows.Add(new object[]
                {
                    reason.ReasonId, reason.ValueSelected
                });
            }
            return table;
        }

        #endregion
        
        #endregion

        #region BatchNumber By 7 digit
        public static string GetBatchNumberBy7digit(string batchNumber, Page p) //sasha
		{
            if (Regex.IsMatch(batchNumber, @"^\d{10,12}$"))
            { 
                long x = Convert.ToInt64(batchNumber)/100;
                bool prefixZero = false;
                if (batchNumber.Length == 10)
                    prefixZero = true;
                batchNumber = x.ToString();
                if (prefixZero)
                    batchNumber = "0" + batchNumber;
                
            }
   
            string rexBracket = (@"^\d{7}"); 
			var mm = new Regex(rexBracket);
			if (!Regex.IsMatch(batchNumber, @"^\d{8,9}$"))
			{
				foreach (Match myMatch in mm.Matches(batchNumber))
				{
					if (myMatch.ToString().Length == 7)// break;
					{
						batchNumber = myMatch.ToString();
						break;
					}
				}
				var measureId = "112";
				var maxRows = 1;

				var myResult = Utilities.QueryUtils.FindItemsByMeasureValue(measureId, batchNumber, maxRows, p);

				if (myResult.Count > 0)
				{
					if (myResult[0].FullBatchNumber.ToString().Trim() != "") batchNumber = myResult[0].FullBatchNumber.ToString().Trim();
				}
			}
			return batchNumber;
		}

		public static string GetItemNumberBy7digit(string ItemNumber, Page p) //sasha
		{
			string rexBracket = (@"^\d{7}");
			var mm = new Regex(rexBracket);
			if (!Regex.IsMatch(ItemNumber, @"^\d{10,12}$") && !Regex.IsMatch(ItemNumber, @"^\d{8,9}$"))
			{
				foreach (Match myMatch in mm.Matches(ItemNumber))
				{
					if (myMatch.ToString().Length == 7)
					{
						ItemNumber = myMatch.ToString();
						break;
					}
				}
				var measureId = "112";
				var maxRows = 1;

				var myResult = Utilities.QueryUtils.FindItemsByMeasureValue(measureId, ItemNumber, maxRows, p);

				if (myResult.Count > 0)
				{
					if (myResult[0].FullItemNumber.ToString().Trim() != "") ItemNumber = myResult[0].FullItemNumber.ToString().Trim();
				}
			}
			return ItemNumber;
		}

        #endregion

        #region Calculate Markup
        //Calculate Markup written by Sergey
        //Calculates the appropriate markup given the input price
        public static decimal CalculateMarkup(decimal price, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "SELECT TOP 1 Markup FROM tblMarkupPrice WHERE @price >= PriceFrom AND @price <= PriceTo",
                Connection = conn,
                CommandType = CommandType.Text,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@price", Decimal.Floor(price));//Mimics what is done in procedure to decimal prices
            //LogUtils.UpdateSpLogWithParameters(command, p);//Not necessary, not stored procedure?
            decimal markup = (decimal)command.ExecuteScalar();
            conn.Close();
            return markup;
        }
        #endregion
        
		#region KYC
		public static KYCModel GetKYC(string CustomerID, Page p)
		{
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetCustomerKYC",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@CustomerID", CustomerID);
			//command.Parameters.Add("@AuthorID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["ID"]);
			//command.Parameters.Add("@AuthorOfficeID", SqlDbType.Int).Value = Int32.Parse("" + p.Session["AuthorOfficeID"]);

			LogUtils.UpdateSpLogWithParameters(command, p);
			var dt = new DataTable();
			var da = new SqlDataAdapter(command);
			da.Fill(dt);
			//var result = (from DataRow row in dt.Rows select new KYCModel(row)).ToList();
			var result = (dt.Rows.Count == 0 ? null : new KYCModel(dt.Rows[0]));
			conn.Close();
			return result;
		}

		public static string SetKYC(KYCModel itemModel, Page p)
		{
			var msg = "";
			//run recalc here
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spSetCustomerKYC",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@KYCDate", itemModel.KYCDate);
			command.Parameters.AddWithValue("@YearofEstd", itemModel.YearofEstd);
			command.Parameters.AddWithValue("@CustomerID", itemModel.CustomerID);
			command.Parameters.AddWithValue("@IsDomestic", itemModel.IsDomestic);
			command.Parameters.AddWithValue("@IsExistingCustomer", itemModel.IsExistingCustomer);

			command.Parameters.AddWithValue("@AddressType", itemModel.AddressType);
			command.Parameters.AddWithValue("@CompanyName", itemModel.CompanyName);
			command.Parameters.AddWithValue("@OwnerName", itemModel.OwnerName);
			command.Parameters.AddWithValue("@Address1", itemModel.Address1);
			command.Parameters.AddWithValue("@Address2", itemModel.Address2);

			command.Parameters.AddWithValue("@City", itemModel.City);
			command.Parameters.AddWithValue("@Zip", itemModel.Zip);
			command.Parameters.AddWithValue("@State", itemModel.State);
			command.Parameters.AddWithValue("@CityCode", itemModel.CityCode);
			command.Parameters.AddWithValue("@Tel1", itemModel.Tel1);

			command.Parameters.AddWithValue("@Tel2", itemModel.Tel2);
			command.Parameters.AddWithValue("@Mobile", itemModel.Mobile);
			command.Parameters.AddWithValue("@FAX", itemModel.FAX);
			command.Parameters.AddWithValue("@Email", itemModel.Email);
			command.Parameters.AddWithValue("@Website", itemModel.Website);

			command.Parameters.AddWithValue("@Representative", itemModel.Representative);
			command.Parameters.AddWithValue("@Designation", itemModel.Designation);
			command.Parameters.AddWithValue("@Memberof", itemModel.Memberof);
			command.Parameters.AddWithValue("@Ref1", itemModel.Ref1);
			command.Parameters.AddWithValue("@RefContact1", itemModel.RefContact1);

			command.Parameters.AddWithValue("@Ref2", itemModel.Ref2);
			command.Parameters.AddWithValue("@RefContact2", itemModel.RefContact2);
			command.Parameters.AddWithValue("@PAN", itemModel.PAN);
			command.Parameters.AddWithValue("@GSTNo", itemModel.GSTNo);
			command.Parameters.AddWithValue("@GSTwef", itemModel.GSTwef);

			command.Parameters.AddWithValue("@PhotoIDProof", itemModel.PhotoIDProof);
			command.Parameters.AddWithValue("@CompanyTypeID", itemModel.CompanyTypeID);
			command.Parameters.AddWithValue("@BusinessTypeID", itemModel.BusinessTypeID);
			command.Parameters.AddWithValue("@DistributionID", itemModel.DistributionID);
			command.Parameters.AddWithValue("@CompanyBrief", itemModel.CompanyBrief);
			command.Parameters.AddWithValue("@CustomerImg", itemModel.CustomerImg);
			

			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				msg = e.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
			}
			conn.Close();
			return msg;
		}

		public static DataTable GetPersonAuthForm(PersonExModel itemModel, Page p)
		{
			var msg = "";
			var dt = new DataTable();
			var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
			conn.Open();
			var command = new SqlCommand
			{
				CommandText = "spGetPersonauthForm",
				Connection = conn,
				CommandType = CommandType.StoredProcedure,
				CommandTimeout = p.Session.Timeout
			};
			command.Parameters.AddWithValue("@PersonID", itemModel.PersonId);
			LogUtils.UpdateSpLogWithParameters(command, p);
			try
			{
				var da = new SqlDataAdapter(command);
				da.Fill(dt);
			}
			catch (Exception e)
			{
				msg = e.Message;
				LogUtils.UpdateSpLogWithException(msg, p);
				dt = null;
			}
			conn.Close();
			return dt;
		}

        public static string SetPersonAuthForm(string personID, string authFormImgPath, Page p)
        {
            var msg = "";
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spSetPersonAuthForm",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@PersonID", personID);
            command.Parameters.AddWithValue("@AuthFormImg", authFormImgPath);

            LogUtils.UpdateSpLogWithParameters(command, p);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        #endregion

        /*IvanB start*/
        #region SKU report type

        public static int GetReportAssigned(string sku, Page p)
        {
            var result = 0;
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetReportTypeAssigned",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@SKU", sku);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var cda = new SqlDataAdapter(command);
                var cdt = new DataTable();
                cda.Fill(cdt);

                if(cdt.Rows.Count > 0)
                {
                    result = (int)cdt.Rows[0].Field<decimal>("ReportType");
                }
            }
            return result;
        }

        public static void AssignReportType(int reportType, string sku, Page p)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spAssignReportType",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@ReportType", reportType);
                command.Parameters.AddWithValue("@SKU", sku);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                }
            }
        }

        public static void DeleteReportTypeAssignment(string sku, Page p)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spDeleteReportTypeAssignment",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@SKU", sku);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                }
            }
        }


        #endregion
        /*IvanB end*/

        #region IsBigBatchCode

        private static readonly object BigBatchCodesLock = new object();
        private static HashSet<long> BigBatchCodes;

        /// <summary>
        /// Determines whether the specified group batch code is in dbo.tblBigBatchCode table.
        /// </summary>
        /// <param name="groupBatchCode">The group + batch codes: [Group Code] * 10^3 + [Batch Code].</param>
        public static bool IsBigBatchCode(long groupBatchCode)
        {
            if (BigBatchCodes != null)
            {
                return BigBatchCodes.Contains(groupBatchCode);
            }
            return IsBigBatchCodeWithLoad(groupBatchCode);
        }

        private static bool IsBigBatchCodeWithLoad(long groupBatchCode)
        {
            lock (BigBatchCodesLock)
            {
                if (BigBatchCodes == null)
                {
                    var session = HttpContext.Current.Session;
                    var connectionString = session["ConnectionString"] as string;
                    BigBatchCodes = LoadBigBatchCodes(session, connectionString);
                }
            }
            return BigBatchCodes.Contains(groupBatchCode);
        }

        private static HashSet<long> LoadBigBatchCodes(HttpSessionState session, string connectionString)
        {
            var resultSet = new HashSet<long>();
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                var sqlCommand = new SqlCommand
                {
                    CommandText = "SELECT GroupBatchCode FROM dbo.tblBigBatchCode",
                    Connection = sqlConnection,
                    CommandType = CommandType.Text,
                    CommandTimeout = session.Timeout
                };

                using (var command = sqlCommand)
                {
                    //LogUtils.UpdateSpLogWithParameters(command, session);
                    try
                    {
                        var result = command.ExecuteReader();
                        while (result.Read())
                        {
                            resultSet.Add(result.GetInt32(0));
                        }
                    }
                    catch (Exception e)
                    {
                        LogUtils.UpdateSpLogWithException(e.Message, session);
                    }
                }
            }
            return resultSet;
        }
        #endregion

        #region Color Stones
        public static List<EnumMeasureModel> GetVarietyBySpecies(string speciesName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetVarietyBySpecies",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@SpeciesName", speciesName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EnumMeasureModel(row, true)).ToList();
            conn.Close();
            return result;
        }

        public static List<EnumMeasureModel> GetTreatmentGradeByVariety(string varietyName, Page p)
        {
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetTreatmentGradeByVariety",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@VarietyName", varietyName);
            LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);
            var result = (from DataRow row in dt.Rows select new EnumMeasureModel(row, false)).ToList();
            conn.Close();
            return result;
        }

        #endregion

        #region Extended Description

        public static DataTable GetExtendedDescription(int partID, int batchID, int itemCode,Page p)
        {
            string extendedDescription = string.Empty;

            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();
            var command = new SqlCommand
            {
                CommandText = "spGetExtDescription",
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };
            command.Parameters.AddWithValue("@PartID", partID);
            command.Parameters.AddWithValue("@BatchID", batchID);
            command.Parameters.AddWithValue("@ItemCode", itemCode);

			LogUtils.UpdateSpLogWithParameters(command, p);
            var dt = new DataTable();
            var da = new SqlDataAdapter(command);
            da.Fill(dt);

            //if (dt.Rows.Count > 0) extendedDescription = dt.Rows[0][0].ToString();
            return dt;
        }

        public static string SetExtendedDescription(string ExtDescription, string ExtExternalComment, int partID, int batchID, int itemCode, Page p)
        {
            string msg = string.Empty;
            var conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]);
            conn.Open();

            var command = new SqlCommand
            {
                Connection = conn,
                CommandText = "spSetExtDescription",
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = p.Session.Timeout
            };

            command.Parameters.Add(new SqlParameter("@ExtDescription", ExtDescription));
			command.Parameters.Add(new SqlParameter("@ExtExternalComment", ExtExternalComment));
			command.Parameters.Add(new SqlParameter("@PartID", partID));
            command.Parameters.Add(new SqlParameter("@BatchID", batchID));
            command.Parameters.Add(new SqlParameter("@ItemCode", itemCode));
			command.Parameters.Add(new SqlParameter("@AuthorID", "" + p.Session["ID"]));
            command.Parameters.Add(new SqlParameter("@AuthorOfficeID", "" + p.Session["AuthorOfficeID"]));
            LogUtils.UpdateSpLogWithParameters(command, p);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                msg = e.Message;
                LogUtils.UpdateSpLogWithException(msg, p);
            }
            conn.Close();
            return msg;
        }
        #endregion
        #region AccRep
        public static void SetDocumentPrintedByBatch(int groupCode, int batchCode, Page p)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spSetDocumentWasPrintedByBatch",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@OrderCode", groupCode);
                command.Parameters.AddWithValue("@BatchCode", batchCode);
                command.Parameters.AddWithValue("@AuthorID", p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"]);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogUtils.UpdateSpLogWithException(ex.Message, p);
                }
            }
        }
        public static string GetSkuByBatch(int order, int batch, Page p)
        {
            using (SqlConnection conn = new SqlConnection("" + p.Session["MyIP_ConnectionString"]))
            {
                conn.Open();
                var command = new SqlCommand
                {
                    CommandText = "spGetCustomerProgramInstanceByBatchCode",
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = p.Session.Timeout
                };
                command.Parameters.AddWithValue("@GroupCode", order);
                command.Parameters.AddWithValue("@BatchCode", batch);
                command.Parameters.AddWithValue("@AuthorID", p.Session["ID"]);
                command.Parameters.AddWithValue("@AuthorOfficeID", p.Session["AuthorOfficeID"]);
                LogUtils.UpdateSpLogWithParameters(command, p);
                var cda = new SqlDataAdapter(command);
                var cdt = new DataTable();
                try
                {
                    cda.Fill(cdt);
                    if (cdt.Rows.Count > 0)
                    {
                        conn.Close();
                        return cdt.Rows[0]["CustomerProgramName"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    conn.Close();
                }
            }
            return null;
        }
        #endregion
    }
}