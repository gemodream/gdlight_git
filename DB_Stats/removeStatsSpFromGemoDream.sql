use GemoDream16
go
--==  Remove stored procedures

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddRejectReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddRejectReason]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRejectReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRejectReason]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReasonMeasureRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReasonMeasureRules]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetReasonMeasureRule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetReasonMeasureRule]
GO

-- Reports
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReports]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReportFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReportFilter]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReportTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReportTotals]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsItemTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsItemTotals]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCustomerStatsItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCustomerStatsItems]
GO

-- Calculate
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsCalc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsCalc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsCalcByCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsCalcByCustomer]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFailedParamsByBatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFailedParamsByBatch]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsReportRemove]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsReportRemove]
GO

--== Types

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'StatsFilterTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[StatsFilterTableType]
GO

--== Functions
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StringSplit]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[StringSplit]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFailedMeasure]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFailedMeasure]
GO

--== Tables
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRejectReason_Measure]') AND type in (N'U'))
DROP TABLE [dbo].[tblRejectReason_Measure]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRejectReason]') AND type in (N'U'))
DROP TABLE [dbo].[tblRejectReason]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportRule_statsReport]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportRule]'))
ALTER TABLE [dbo].[statsReportRule] DROP CONSTRAINT [FK_statsReportRule_statsReport]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportRule]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportRule]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportItemDetails_statsReportItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportItemDetails]'))
ALTER TABLE [dbo].[statsReportItemDetails] DROP CONSTRAINT [FK_statsReportItemDetails_statsReportItem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportItemDetails]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportItemDetails]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportItem_statsReport]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportItem]'))
ALTER TABLE [dbo].[statsReportItem] DROP CONSTRAINT [FK_statsReportItem_statsReport]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportItem]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportItem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReport]') AND type in (N'U'))
DROP TABLE [dbo].[statsReport]
GO















