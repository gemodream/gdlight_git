rem example: Change_sp.cmd localhost GemoDream16Stats gemoAdmin16 gemoAdmin16

rem tables
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/tblRejectReason.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/tblRejectReason_Measure.sql

sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/statsReport.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/statsReportItem.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/statsReportItemDetails.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i table/statsReportRule.sql

rem types
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/intTableType.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/StatsFilterTableType.sql

rem functions
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/StringSplit.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/GetFailedMeasure.sql

rem procedures
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetOrderStateCodes.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spAddRejectReason.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetRejectReason.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetReasonMeasureRules.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spSetReasonMeasureRule.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStatsReports.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStatsReportFilter.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStatsReportTotals.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetStatsItemTotals.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetCustomerStatsItems.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spGetFailedParamsByBatch.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spStatsCalcByCustomer.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spStatsCalc.sql
sqlcmd -S %1 -d %2 -U %3 -P %4 -b -m 1 -i sp/spStatsReportRemove.sql
