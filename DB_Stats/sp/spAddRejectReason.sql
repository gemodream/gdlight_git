USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddRejectReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddRejectReason]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spAddRejectReason]
(
	@ReasonName varchar(50)
)
AS
insert into tblRejectReason(ReasonName) values (@ReasonName)

GO

