USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReportFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReportFilter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[spGetStatsReportFilter]
(
	@CustomerId int,
	@ReportId int
)
AS
-- Customer Programs
select cp_name from statsReportItem
	where report_id = @ReportId
		and customer_id = @CustomerId
	group by cp_name
	order by cp_name

	
-- Reasons
select d.reason_id, r.ReasonName
	from statsReportItem i, statsReportItemDetails d, tblRejectReason r
	where i.report_id = @ReportId
		and i.customer_Id = @CustomerId
		and i.item_id = d.item_id
		and d.reason_id = r.ReasonID
	group by d.reason_id, r.ReasonName

-- Reason Values
select d.reason_id, d.currValue as currValue
	from statsReportItem i, statsReportItemDetails d
	where i.report_id = @ReportId
		and i.customer_Id = @CustomerId
		and i.item_id = d.item_id
	group by d.reason_id, d.currValue
	order by d.reason_id, d.currValue

-- Items Count
select count(*) as cnt from statsReportItem
	where report_id = @ReportId
		and customer_id = @CustomerId
	group by cp_name
	order by cp_name
GO

