USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'intTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[intTableType]
GO

CREATE TYPE [dbo].[intTableType] AS TABLE(
	[ID] int NOT NULL
)
GO

