USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsCalcByCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsCalcByCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spStatsCalcByCustomer]
(
	@ErrMsg nvarchar(1000) OUTPUT,
	@ReportId int,
	@CustomerId int,
	@DateFrom date = null,
	@DateTo date = null,
	@MeasureIds [dbo].[intTableType] READONLY,
	@OrderStateCode int = NULL
)
AS
	DECLARE @begDate date = ISNULL(@DateFrom, (select min(OrderDate) from GemoDream16.dbo.v0Item where CustomerID = @CustomerID))
	DECLARE @endDate date = ISNULL(@DateTo, (select max(OrderDate) from GemoDream16.dbo.v0Item where CustomerID = @CustomerID))	
	DECLARE @batch int

	create table #params
	(
		PartID int,
		MeasureID int,
		MeasureName varchar(20),
		BatchID int,
		ItemCode int,
		StateMsg varchar(100)
	)
	DECLARE BatchesCursor CURSOR FOR	
		SELECT BatchID 
			from GemoDream16.dbo.v0Item
			where 1 = 1
				and CustomerID = @CustomerID
				and cast(OrderDate as date) between @begDate and @endDate
				and BatchID = NewBatchID
				and (@OrderStateCode IS NULL OR OrderStateCode = @OrderStateCode)
			group by BatchID
	OPEN BatchesCursor
	FETCH NEXT FROM BatchesCursor INTO @batch
	
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #params(PartID, MeasureID, MeasureName, BatchID, ItemCode, StateMsg)
			exec spGetFailedParamsByBatch @BatchId = @batch, @MeasureIds = @MeasureIds
		FETCH NEXT FROM BatchesCursor INTO @batch
	END
	CLOSE BatchesCursor
	DEALLOCATE BatchesCursor
	
	create table #customerParams
	(
		OrderCode int,
		BatchCode int,
		ItemCode int,
		BatchID int,
		MeasureID int,
		PartID int,
		MeasureName varchar(20),
		StateMsg varchar(100),
		OrderDate datetime,
		CpID int,
		CpName varchar(250)
	)
	INSERT INTO #customerParams 
		(OrderCode, BatchCode, ItemCode, BatchID, MeasureID, PartID, MeasureName, StateMsg, OrderDate, CpID)
	SELECT 
		i.OrderCode, i.BatchCode, r.ItemCode, r.BatchID, 
		r.MeasureID, r.PartID, r.MeasureName, r.StateMsg, i.OrderDate, 
		(SELECT TOP 1 CPID FROM GemoDream16.dbo.v0Batch  WHERE BatchID = r.BatchID) as CpID
		FROM #params As r, GemoDream16.dbo.v0Item As i
		WHERE
			r.BatchID = i.BatchID
			and r.ItemCode = i.ItemCode
			and i.BatchID = i.NewBatchID
	UPDATE #customerParams SET CpName = cp.CustomerProgramName
		FROM GemoDream16.dbo.v0CustomerProgram As cp, #customerParams As p
		WHERE cp.CPID = p.CpID
	drop table #params

BEGIN TRANSACTION
BEGIN TRY
	-- Items
	INSERT INTO statsReportItem(report_id, order_code, batch_code, item_code, customer_id, item_createDate, cp_name)
		SELECT @ReportId, OrderCode, BatchCode, ItemCode, @CustomerId,  OrderDate, CpName 
			FROM #customerParams 
			GROUP BY OrderCode, BatchCode, ItemCode, OrderDate, CpName
	
	-- 	Details
	INSERT INTO statsReportItemDetails
		(item_id, part_id, measure_id, reason_id, currValue, minValue, maxValue, rejection_type)
	SELECT 
		s.item_id, r.PartID, r.MeasureID, 
		CAST((select Data from dbo.StringSplit(r.StateMsg,';') where Id = 1 ) AS INT)as reasonId,
		(select Data from dbo.StringSplit(r.StateMsg,';') where Id = 4 ) as currValue,
		(select Data from dbo.StringSplit(r.StateMsg,';') where Id = 5 ) as minValue,
		(select Data from dbo.StringSplit(r.StateMsg,';') where Id = 6 ) as maxValue,
		CAST((select Data from dbo.StringSplit(r.StateMsg,';') where Id = 7 ) as int) as rejectionType

		FROM statsReportItem as s, #customerParams as r
		where s.order_code = r.OrderCode
			and s.batch_code = r.BatchCode
			and s.item_code = r.ItemCode
			and s.report_id = @ReportId
END TRY
	
BEGIN CATCH
	SET @ErrMsg = ERROR_MESSAGE()
	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH
IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;	
   
GO

