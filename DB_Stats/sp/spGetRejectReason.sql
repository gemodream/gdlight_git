USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRejectReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRejectReason]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetRejectReason]
AS
SELECT * from tblRejectReason order by ReasonName

GO

