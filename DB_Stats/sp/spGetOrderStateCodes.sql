USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetOrderStateCodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetOrderStateCodes]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetOrderStateCodes]
AS
SELECT StateCode, StateName
  FROM [GemoDream16].[dbo].[tblState] where stateTargetId=2

GO

