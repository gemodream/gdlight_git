USE [GemoDream16Stats]
GO
-- Original GetFailedParameter.sql
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFailedMeasure]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFailedMeasure]
GO
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[GetFailedMeasure] --NewBAtchID = 34317, ItemCode = 1, MeasureID = 27(Color Grade), order = 6215, ItemTypeId=13, PartId = 55
(
@NewBatchID int,
@NewItemCode int,
@PartID INT,
@MeasureID int
)

RETURNS varchar(100) AS

begin

declare @CPDocID int
declare @ResultValue VARCHAR(100)
declare @Status VARCHAR(200)
declare @MeasureName VARCHAR(50)
declare @MeasureClass int
declare @CPID int
declare @CPName VARCHAR(250)
DECLARE @PartName VARCHAR(50)
DECLARE @PartNameOrig VARCHAR(50)

-- MeasureName, MeasureClass (1 - enum, 3 - numeric)
SELECT 
	@MeasureName = MeasureName,
	@MeasureClass = MeasureClass
FROM GemoDream16.dbo.tblMeasure WHERE MeasureID = @MeasureID

-- CustomerProgram
SET @CPID = (SELECT TOP 1 CPID FROM GemoDream16.dbo.v0Batch WHERE BatchID = @NewBatchID)
SET @CPName = (SELECT TOP 1 CustomerProgramName FROM GemoDream16.dbo.v0CustomerProgram WHERE CPID = @CPID)

-- Cp Document
SET @CPDocID = 
(
	SELECT MAX(d.CPDocID) 
		FROM GemoDream16.dbo.v0CustomerProgram c, GemoDream16.dbo.hdtCPDoc d, GemoDream16.dbo.hdtCPDoc_Operation o 
		WHERE c.CPID = @CPID
			and c.CustomerProgramHistoryID = d.CustomerProgramHistoryID
			and  d.CPDocID = o.CPDocID 
)

-- Min/Max For MeasureClass = 3 (numeric)
DECLARE	@MinMeasure numeric(14,4)
DECLARE @MaxMeasure numeric(14,4) 

-- Min/Max For MeasureClass = 1 (enum)
DECLARE	@Min_Value VARCHAR(20)
DECLARE	@Max_Value VARCHAR(20)

DECLARE @MeasureGroupID int

SELECT	@MinMeasure = MinMeasure,
		@MaxMeasure = MaxMeasure,

		@Min_Value = minM.ValueTitle,
		@Max_Value = maxM.ValueTitle,
		
		@MeasureGroupID = m.MeasureGroupID,
		@PartName = p.PartName
FROM 	GemoDream16.dbo.v0Part As p, GemoDream16.dbo.tblMeasure As m, GemoDream16.dbo.hdtCPDocRule As dr
		LEFT JOIN GemoDream16.dbo.tblMeasureValue minM ON 
			dr.MinMeasure = minM.MeasureValueID AND dr.MeasureID = minM.MeasureValueMeasureID
		LEFT JOIN GemoDream16.dbo.tblMeasureValue maxM ON 
			dr.MaxMeasure = maxM.MeasureValueID AND dr.MeasureID = maxM.MeasureValueMeasureID
WHERE 	dr.CPDocID = @CPDocID
	and not ( (MinMeasure is Null) and (MaxMeasure is Null) )
	and dr.MeasureID = m.MeasureID
	and dr.PartID = @PartID 
	and	dr.MeasureID = @MeasureID
	and dr.PartID = @PartID
	and dr.PartID = p.PartID
	
DECLARE @ReasonId nvarchar(50) = 
(
	Select top 1 ltrim(str(r.ReasonID ))
		from tblRejectReason_Measure as rm, tblRejectReason As r 
		where rm.ReasonID = r.ReasonID 
			-- and rm.PartTypeID = @PartTypeID
			and rm.MeasureID = @MeasureID
)	
IF @ReasonID IS NULL
	SET @ReasonID = ''		 
SET @PartNameOrig = @PartName
IF LTRIM(RTRIM(ISNULL(@PartName, ''))) <> ''
BEGIN
	IF @PartName LIKE '%STONE SET%' SET @PartName = 'Set'
END	


IF LTRIM(RTRIM(ISNULL(@MeasureName, ''))) <> ''
BEGIN
	DECLARE @RecheckNumber INT

	SET @RecheckNumber = 
	(	
		select 	max(RecheckNumber)
			from GemoDream16.dbo.tblRecheckSession rs, GemoDream16.dbo.tblMeasure m
			where rs.BatchID = @NewBatchID
				AND rs.ItemCode = @NewItemCode 
				AND rs.MeasureGroupID = m.MeasureGroupId
				AND m.MeasureID = @MeasureID
	)

	DECLARE @ValueCode INT
	DECLARE @ValueTitle VARCHAR(100)
	DECLARE @Norder int
	DECLARE @MeasureValue NUMERIC(14,4) -- Value numeric
	IF @MeasureClass = 1 -- enum
		SELECT 	
			@ValueTitle = v.ValueTitle,
			@ValueCode = v.ValueCode,
			@Norder = v.nOrder--,
			--@MeasureValue = h.MeasureValue
		FROM GemoDream16.dbo.tblPartValue t 
			INNER JOIN GemoDream16.dbo.hstPartValue h ON t.PartValueHistoryID = h.PartValueHistoryID 
			INNER JOIN GemoDream16.dbo.tblRecheckSession r ON t.RecheckSessionID = r.RecheckSessionID 
			LEFT OUTER JOIN GemoDream16.dbo.tblMeasureValue v ON 
				v.MeasureValueID = h.MeasureValueID AND v.MeasureValueMeasureID = h.MeasureValueMeasureID
		WHERE   (t.ExpireDate IS NULL)
			AND r.ItemCode = @NewItemCode
			AND r.BatchID = @NewBatchID
			and t.MeasureID = @MeasureID
			and t.PartID = @PartID
			and r.RecheckNumber = @RecheckNumber
			and v.MeasureValueMeasureID = @MeasureID

	ELSE -- numeric
		SELECT 	
			--@ValueTitle = v.ValueTitle,
			--@ValueCode = v.ValueCode,
			--@Norder = v.nOrder,
			@MeasureValue = h.MeasureValue
		FROM GemoDream16.dbo.tblPartValue t 
			INNER JOIN GemoDream16.dbo.hstPartValue h ON t.PartValueHistoryID = h.PartValueHistoryID 
			INNER JOIN GemoDream16.dbo.tblRecheckSession r ON t.RecheckSessionID = r.RecheckSessionID 
		WHERE   (t.ExpireDate IS NULL)
			AND r.ItemCode = @NewItemCode
			AND r.BatchID = @NewBatchID
			and t.MeasureID = @MeasureID
			and t.PartID = @PartID
			and r.RecheckNumber = @RecheckNumber
	
	
	
	DECLARE @ValueTitleMin VARCHAR(100), @ValueTitleMax VARCHAR(100), @NorderMin int, @NorderMax int
	DECLARE @ValueMinDisplay VARCHAR(100),@ValueMaxDisplay VARCHAR(100), @ValueCurrent VARCHAR(100)

	SELECT TOP 1 @NorderMin = nOrder, @ValueTitleMin = ValueTitle FROM GemoDream16.dbo.vwMeasureValue WHERE MeasureValueID = @MinMeasure
	SELECT TOP 1 @NorderMax = nOrder, @ValueTitleMax = ValueTitle FROM GemoDream16.dbo.vwMeasureValue WHERE MeasureValueID = @MaxMeasure
	
	IF @MeasureClass = 1 -- enum
	BEGIN
		set @ValueMinDisplay = @ValueTitleMin
		set @ValueMaxDisplay = @ValueTitleMax
		set @ValueCurrent = @ValueTitle
	END
	ELSE
	BEGIN
		set @ValueMinDisplay =  CONVERT(decimal(14,2), CONVERT(varbinary(20), @MinMeasure)) -- LTRIM(STR(@MinMeasure))
		set @ValueMaxDisplay =  CONVERT(decimal(14,2), CONVERT(varbinary(20), @MaxMeasure)) -- LTRIM(STR(@MaxMeasure))
		set @ValueCurrent  =	CONVERT(decimal(14,2), CONVERT(varbinary(20), @MeasureValue)) -- LTRIM(STR(@MeasureValue))
	END	
	IF
	(
		( 
			@MinMeasure IS NULL 
			OR 
			(
				@ValueCode IS NULL AND @MeasureValue >= @MinMeasure -- numeric
			) 
			OR
			( 
				@ValueCode IS NOT NULL and @Norder >= @NorderMin -- enum
			)
			OR
			( 
				@ValueCode IS NULL and @NorderMin = @NorderMax and @ValueTitleMin='' -- enum
			)
		)
		AND
		( 
			@MaxMeasure IS NULL 
			OR 
			(
				@ValueCode IS NULL and @MeasureValue <= @MaxMeasure  -- numeric
			) 
			OR
			(
				@ValueCode IS NOT NULL and @Norder <= @NorderMax -- enum
			)
			OR
			( 
				@ValueCode IS NULL and @NorderMin = @NorderMax and @ValueTitleMin='' -- enum
			)
		)
	)
	AND NOT 
	( 
		@MinMeasure IS NULL AND @MaxMeasure IS NULL 
	)	
	BEGIN
		SET @ResultValue = 	@MeasureName
	END

	-- Result: ReasonId;PartName;MeasureName;CurrentValue;MinValue;MaxValue;TypeRejection(Pass=1/Fail=2/Missing=3);CPName
	IF  RTRIM(LTRIM(ISNULL(@ResultValue, ''))) <> ''  
	-- Pass
	BEGIN
		SET @Status = 
			@ReasonId + ';' + 
			@PartNameOrig + ';' + @MeasureName + ';' + 
			@ValueCurrent + ';' +  
			@ValueMinDisplay + ';' +  
			@ValueMaxDisplay + ';' +  
			'1' + ';' + @CPName	
	END
	
	IF	RTRIM(LTRIM(ISNULL(@ResultValue, ''))) = '' AND RTRIM(LTRIM(ISNULL(@ValueTitle, ''))) <> '' 
	-- Fails
	BEGIN
		SET @Status =  
			@ReasonId + ';' + 
			@PartNameOrig + ';' + @MeasureName + ';' + 
			@ValueCurrent + ';' +  
			@ValueMinDisplay + ';' +  
			@ValueMaxDisplay + ';' +  
			'2' + ';' + @CPName
			
	END
	
	IF	RTRIM(LTRIM(ISNULL(@ResultValue, ''))) = '' AND RTRIM(LTRIM(ISNULL(@ValueTitle, ''))) = '' 
	-- Missing
	BEGIN
		SET @Status = 
			@ReasonId + ';' + 
			@PartNameOrig + ';' + @MeasureName + ';' + 
			'' + ';' +  
			@ValueMinDisplay + ';' +  
			@ValueMaxDisplay + ';' +  
			'3'  + ';' + @CPName
	END
END
ELSE 
BEGIN
	SET @Status = null
END

RETURN @Status
end

GO

