USE [GemoDream16Stats]
GO
--select top 100 * from [GemoDream16].[dbo].v0User
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetStatsReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetStatsReports]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetStatsReports]
AS
	SELECT r.report_id, r.calc_date, r.date_from, r.date_to, r.comments, r.calc_end,
		(u.FirstName + ' ' + u.LastName) as user_name, r.order_state_code 
		FROM statsReport as r, [GemoDream16].[dbo].v0User As u
		WHERE r.user_id = u.UserID
		ORDER BY r.calc_date desc
	
	-- Customers
	CREATE TABLE #Customers
	(
		report_id int,
		customer_id int
	)
	DECLARE @reportId int
	DECLARE @customers varchar(3000)

	DECLARE rptCursor CURSOR FOR
		SELECT report_id, customerIds FROM statsReport
	OPEN rptCursor
	FETCH NEXT FROM rptCursor INTO @reportId, @customers
	WHILE @@FETCH_STATUS = 0
	BEGIN

		INSERT INTO #Customers(report_id, customer_id)
		SELECT @reportId, CAST(Data as int) as CustomerID
			from  dbo.StringSplit(@customers, ';') OPTION (MAXRECURSION 0) --where len(Data) > 0
		FETCH NEXT FROM rptCursor INTO @reportId, @customers
	END
	CLOSE rptCursor
	DEALLOCATE rptCursor		

	SELECT sc.report_id, c.CustomerID, c.CustomerName 
		FROM #customers sc, GemoDream16.dbo.v0Customer c
		WHERE sc.customer_id = c.CustomerID
		ORDER BY c.CustomerName
	
	DROP TABLE #customers		
	
	-- Rules
	SELECT rl.report_id, r.ReasonName, m.MeasureName
		FROM statsReportRule AS rl INNER JOIN
             tblRejectReason AS r ON rl.reason_id = r.ReasonID INNER JOIN
             GemoDream16.dbo.tblMeasure AS m ON rl.measure_id = m.MeasureID
        ORDER BY rl.report_id
GO

