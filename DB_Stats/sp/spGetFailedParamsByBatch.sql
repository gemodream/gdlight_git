USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFailedParamsByBatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFailedParamsByBatch]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spGetFailedParamsByBatch]
(
	@BatchId int,
	@MeasureIds [dbo].[intTableType] READONLY
)
AS

	declare @CPID int = 
	(
		SELECT TOP 1 CPID FROM GemoDream16.dbo.v0Batch WHERE BatchID = @BatchID
	)

	declare @ItemTypeID int = 
	(
		select ItemTypeID from GemoDream16.dbo.v0CustomerProgram where CPID = @CPID
	)

	declare @CPDocID int  = 
	(
		SELECT MAX(d.CPDocID) 
			FROM 	GemoDream16.dbo.v0CustomerProgram cp, GemoDream16.dbo.hdtCPDoc d, GemoDream16.dbo.hdtCPDoc_Operation o
			WHERE 	cp.CPID = @CPID
				and cp.CustomerProgramHistoryID = d.CustomerProgramHistoryID
				AND  d.CPDocID = o.CPDocID 
	)

	create table #res
	(
		PartID int,
		MeasureID int,
		MeasureName varchar(20),
		BatchID int,
		ItemCode int,
		StateMsg varchar(100)
	)
	declare @item int
	
	DECLARE ItemsCursor CURSOR FOR
		SELECT ItemCode from GemoDream16.dbo.v0Item
			WHERE 1 = 1
				and NewBatchID = BatchID 
				and NewBatchID = @BatchId
	
	OPEN ItemsCursor
	FETCH NEXT FROM ItemsCursor INTO @item
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #res(PartID, MeasureID, MeasureName, BatchID, ItemCode, StateMsg)
		SELECT	
			p.PartID, m.MeasureID, m.MeasureName, @BatchID, @item,
			dbo.GetFailedMeasure(@BatchID, @item,p.PartID, m.MeasureID)
		FROM 	
			GemoDream16.dbo.v0Part p, 
			GemoDream16.dbo.tblMeasure m, 
			@MeasureIds pm, 
			GemoDream16.dbo.hdtCPDocRule dr
			left join GemoDream16.dbo.tblMeasureValue minM on dr.MinMeasure = minM.MeasureValueID 
			and dr.MeasureID = minM.MeasureValueMeasureID
			left join GemoDream16.dbo.tblMeasureValue maxM on dr.MaxMeasure = maxM.MeasureValueID 
			and dr.MeasureID = maxM.MeasureValueMeasureID
		where 	dr.CPDocID = @CPDocID
			and MinMeasure IS NOT NULL 
			and MaxMeasure IS NOT NULL /*and (NotVisibleInCCM = 1 )*/ 
			and dr.MeasureID = m.MeasureID
			and dr.PartID = p.PartID
			and m.MeasureID = pm.ID
		FETCH NEXT FROM ItemsCursor INTO  @item
	END
	CLOSE ItemsCursor
	DEALLOCATE ItemsCursor
	
	select * from #res where StateMsg IS NOT NULL order by ItemCode
	drop table #res

GO

