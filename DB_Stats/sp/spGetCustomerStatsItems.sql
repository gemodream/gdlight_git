USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCustomerStatsItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCustomerStatsItems]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetCustomerStatsItems]
(
	@ReportId int,
	@CustomerId int,
	@PageNo int = 1,
	@PageSize int = 500,
	@RejectionType int = NULL,
	@ItemCriteria nvarchar(1000) = '',
	@ReasonFilter [dbo].[StatsFilterTableType] READONLY
)
AS
-- 1) Items by Item Criteria
CREATE TABLE #ItemsByItemType
(
	item_id int,
	rejection_type int
)

declare @ItemSql nvarchar(1000) =
N'SELECT i.item_id, d.rejection_type
	FROM statsReportItem i, statsReportItemDetails d
	WHERE i.report_id = ' + RTRIM(cast(@ReportId as char)) + '
		and i.customer_id = ' + RTRIM(cast(@CustomerId as char)) + '
		and i.item_id = d.item_id ' +
		@ItemCriteria + '
	GROUP BY i.item_id, d.rejection_type
'

INSERT INTO #ItemsByItemType (item_id, rejection_type)
EXEC sp_executesql @ItemSql

CREATE TABLE #ItemsByItem(item_id int, pass_cnt int default 0)
INSERT INTO #ItemsByItem(item_id)
	SELECT item_id FROM #ItemsByItemType
		WHERE @RejectionType IS NULL OR rejection_type = @RejectionType
		GROUP BY item_id
drop table #ItemsByItemType	

-- 1) Items by Value Criteria
CREATE TABLE #Items(item_id int)	
DECLARE @paramCount int = (select count(*) from @ReasonFilter)
IF @paramCount = 0
	INSERT INTO #Items (item_id)
		SELECT item_id from #ItemsByItem
ELSE
BEGIN
	declare @reasonId int, @currValue nvarchar(50)
	declare paramCursor Cursor for select ReasonID, CurrValue from @ReasonFilter
	open paramCursor
	Fetch Next from paramCursor into @reasonId, @currValue
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		update #ItemsByItem set pass_cnt = pass_cnt + 1
		from #ItemsByItem ii, 
		(
			select i.item_id from #ItemsByItem i, statsReportItemDetails d
			where i.item_id = d.item_id
				and d.reason_id = @reasonId
				and d.currValue = @currValue
			group by i.item_id
		) a
		where ii.item_id = a.item_id
		
		Fetch Next from paramCursor into @reasonId, @currValue
	END
	CLOSE paramCursor 
	DEALLOCATE paramCursor
	INSERT INTO #Items(item_id)
		SELECT item_id FROM #ItemsByItem
			WHERE pass_cnt = @paramCount
END
drop table #ItemsByItem	
	
DECLARE @rowCountByCriteria float = (select count(*) from #Items)
DECLARE @PageCountByCriteria int = CEILING(@rowCountByCriteria / @PageSize)
IF @PageNo > @PageCountByCriteria
	set @PageNo = 1

-- 4) Items by PageNo
CREATE TABLE #ItemsByPage(item_id int)

DECLARE @startRow int = (@PageNo - 1) * @PageSize + 1
DECLARE @endRow int = @startRow + @PageSize -1 

--select @startRow as startRow, @endRow as endRow

;WITH PostQuest AS    
(	
	SELECT	i.item_id, ROW_NUMBER() OVER (ORDER BY i.item_id) AS RowNumber
	FROM    #Items AS i
)
INSERT INTO #ItemsByPage (item_id)
	SELECT item_id FROM PostQuest WHERE RowNumber between @startRow AND @endRow
drop table #Items

-- 5) Result Items
SELECT
		i.item_id, i.order_code, i.batch_code, i.item_code, 
		i.item_createDate, i.cp_name, i.customer_id
		from statsReportItem i , #ItemsByPage ii
		where 
			i.item_id = ii.item_id
		order by i.item_CreateDate

-- 6) Result Values
SELECT d.*, m.MeasureName, p.PartName, r.ReasonName
		from #ItemsByPage i, statsReportItemDetails d, GemoDream16.dbo.tblMeasure m, GemoDream16.dbo.v0Part p, tblRejectReason r
		where 1=1
			and i.item_id = d.item_id
			and d.measure_id = m.MeasureID
			and d.part_id = p.PartID
			and d.reason_id = r.ReasonId
		order by d.item_id, d.reason_id

-- 7) Report Properties by Criteria
select @PageCountByCriteria as PageCountByCriteria, @rowCountByCriteria as RowCountByCriteria, @PageNo as PageNo, @PageSize as PageSize

drop table #ItemsByPage
GO

