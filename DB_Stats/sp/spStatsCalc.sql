USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsCalc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsCalc]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spStatsCalc]
(
	@ErrMsg nvarchar(max) OUTPUT,
	@DateFrom date = null,
	@DateTo date = null,
	@MeasureIds [dbo].[intTableType] READONLY,
	@CustomerIds [dbo].[intTableType] READONLY,
	@Comments varchar(1000) = NULL,
	@UserId int,
	@OrderStateCode int = NULL
)
AS
	DECLARE @begDate date = ISNULL(@DateFrom, (select min(OrderDate) from GemoDream16.dbo.v0Item ))
	DECLARE @endDate date = ISNULL(@DateTo, (select max(OrderDate) from GemoDream16.dbo.v0Item))	

	declare @customers varchar(8000) = ''
	declare @customerQty int = (select count(*) from @CustomerIds)
	DECLARE @batch int

	create table #customers(ID int)

	-- Customers
	IF (@customerQty = 0)
		-- ALL
		INSERT INTO #customers(ID)
			SELECT i.CustomerID AS ID 
				FROM GemoDream16.dbo.v0Item as i 
				WHERE 1=1
					and (cast(i.OrderDate as DATE) between @begDate and @endDate) 
					and i.BatchID = i.NewBatchID
					and (@OrderStateCode IS NULL OR i.OrderStateCode = @OrderStateCode)
					and i.CustomerOfficeID <> 3
				GROUP BY i.CustomerID
	ELSE
		-- SELECTED
		INSERT INTO #customers (ID) SELECT ID from @CustomerIds 
	SELECT @customers = COALESCE(@customers + ';', '') + ltrim(str(id)) from #customers


	-- Measures
	SELECT rm.ReasonID, rm.MeasureID into #rules
		FROM tblRejectReason_Measure As rm, @MeasureIds As sm
		WHERE rm.MeasureID = sm.ID
			
	-- Create Report record
	DECLARE @myReport int
	INSERT INTO statsReport(calc_date, date_from, date_to, user_id, customerIds, comments, order_state_code) 
		values 
			(GETDATE(), @begDate, @endDate, @UserId, @customers, @Comments, @OrderStateCode)
	SET @myReport = @@IDENTITY
	
	-- Add ReportRules
	INSERT INTO statsReportRule (report_id, reason_id, measure_id)
		SELECT @myReport, ReasonID, MeasureID FROM #rules
 
	declare @Errors nvarchar(max)=''
	declare @customerId int	
	DECLARE CustomerCursor CURSOR FOR
		select ID from #customers
	OPEN CustomerCursor
	FETCH NEXT FROM CustomerCursor INTO @customerId
	
	WHILE 	@@FETCH_STATUS = 0
	BEGIN
		exec spStatsCalcByCustomer 
			@ErrMsg = @ErrMsg, 
			@ReportId = @myReport, 
			@CustomerId = @customerId, 
			@DateFrom = @begDate, 
			@DateTo = @endDate, 
			@MeasureIds = @MeasureIds,
			@OrderStateCode = @OrderStateCode
		IF LEN(@ErrMsg) > 0
			set @Errors = @Errors + ' ' + @ErrMsg
		FETCH NEXT FROM CustomerCursor INTO @customerId
	END
	CLOSE CustomerCursor
	DEALLOCATE CustomerCursor
	drop table #customers
	drop table #rules
	
	declare @addedItems int = (select count(*) from statsReportItem where report_id = @myReport)
	IF @addedItems = 0
		BEGIN
			exec spStatsReportRemove @ReportId = @myReport
			SET @ErrMsg = 'Data for the query parameters are not found!'
		END
	ELSE
		BEGIN
			update statsReport set calc_end = GetDate()
				from statsReport where report_id = @myReport
			SET @ErrMsg = @Errors
		END
GO

