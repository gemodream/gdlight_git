USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'StatsFilterTableType' AND ss.name = N'dbo')
DROP TYPE [dbo].[StatsFilterTableType]
GO

CREATE TYPE [dbo].[StatsFilterTableType] AS TABLE(
	[ReasonID] int NOT NULL,
	[CurrValue] nvarchar(50) 
)
GO

