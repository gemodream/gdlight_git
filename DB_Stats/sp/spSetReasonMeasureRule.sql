USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetReasonMeasureRule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetReasonMeasureRule]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSetReasonMeasureRule]
(
	@MeasureIds [dbo].[intTableType] READONLY,
	@ReasonId int = NULL -- null if delete
)
AS
	DELETE FROM tblRejectReason_Measure 
		WHERE MeasureID IN (SELECT ID FROM @MeasureIds)

	IF @ReasonId IS NOT NULL
		INSERT INTO tblRejectReason_Measure (MeasureID, ReasonID)
			SELECT ID, @ReasonId from @MeasureIds
GO

