USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStatsReportRemove]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStatsReportRemove]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spStatsReportRemove]
(
	@ReportId int
)
AS
DELETE FROM statsReportItemDetails
	FROM statsReportItemDetails d INNER JOIN
		statsReportItem i ON d.item_id = i.item_id
	WHERE i.report_id = @ReportId
	
	DELETE FROM statsReportItem where report_id = @ReportId
	DELETE FROM statsReportRule where report_id = @ReportId
	DELETE FROM statsReport where report_id = @ReportId

GO

