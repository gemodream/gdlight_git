USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReasonMeasureRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReasonMeasureRules]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[spGetReasonMeasureRules]

AS
SELECT r.ReasonID, r.ReasonName, rm.ID, m.MeasureID, m.MeasureName
	FROM GemoDream16.dbo.tblMeasure AS m INNER JOIN
         tblRejectReason_Measure AS rm ON m.MeasureID = rm.MeasureID INNER JOIN
         tblRejectReason AS r ON rm.ReasonID = r.ReasonID
ORDER BY r.ReasonName, m.MeasureName

SELECT MeasureID, MeasureName 
	FROM GemoDream16.dbo.tblMeasure 	
	WHERE MeasureClass IN (1, 3) 
		and MeasureID not in (select MeasureID from tblRejectReason_Measure)
	ORDER BY MeasureName
GO

