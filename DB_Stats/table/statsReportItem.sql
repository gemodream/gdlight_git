USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportItem_statsReport]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportItem]'))
ALTER TABLE [dbo].[statsReportItem] DROP CONSTRAINT [FK_statsReportItem_statsReport]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportItem]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportItem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[statsReportItem](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [int] NOT NULL,
	[order_code] [int] NOT NULL,
	[batch_code] [int] NOT NULL,
	[item_code] [int] NOT NULL,
	[customer_id] [int] NOT NULL,
	[item_createDate] [datetime] NOT NULL,
	[cp_name] varchar(250) NULL
) ON [PRIMARY]

GO
-- Unique
CREATE UNIQUE NONCLUSTERED INDEX [statsReportItem_Index_Primary] ON [dbo].[statsReportItem] 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


-- FK
ALTER TABLE [dbo].[statsReportItem]  WITH CHECK ADD  CONSTRAINT [FK_statsReportItem_statsReport] FOREIGN KEY([report_id])
REFERENCES [dbo].[statsReport] ([report_id])
GO

ALTER TABLE [dbo].[statsReportItem] CHECK CONSTRAINT [FK_statsReportItem_statsReport]
GO

-- ReportId + CustomerId
CREATE NONCLUSTERED INDEX [statsReportItem_CustomerId] ON [dbo].[statsReportItem] 
(
	[report_id] ASC,
	[customer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


-- ReportId + CpName
CREATE NONCLUSTERED INDEX [statsReportItem_ReportId_CpName] ON [dbo].[statsReportItem] 
(
	[report_id] ASC,
	[cp_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

-- ReportId + OrderCode + BatchCode + ItemCode
CREATE NONCLUSTERED INDEX [statsReportItem_itemNumber] ON [dbo].[statsReportItem] 
(
	[report_id] ASC,
	[order_code] ASC,
	[batch_code] ASC,
	[item_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO



