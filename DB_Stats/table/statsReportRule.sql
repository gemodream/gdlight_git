USE [GemoDream16Stats]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_statsReportRule_statsReport]') AND parent_object_id = OBJECT_ID(N'[dbo].[statsReportRule]'))
ALTER TABLE [dbo].[statsReportRule] DROP CONSTRAINT [FK_statsReportRule_statsReport]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReportRule]') AND type in (N'U'))
DROP TABLE [dbo].[statsReportRule]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[statsReportRule](
	[report_id] [int] NOT NULL,
	[reason_id] [int] NOT NULL,
	[measure_id] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[statsReportRule]  WITH CHECK ADD  CONSTRAINT [FK_statsReportRule_statsReport] FOREIGN KEY([report_id])
REFERENCES [dbo].[statsReport] ([report_id])
GO

ALTER TABLE [dbo].[statsReportRule] CHECK CONSTRAINT [FK_statsReportRule_statsReport]
GO

