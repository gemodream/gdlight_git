USE [GemoDream16Stats]
GO

/****** Object:  Table [dbo].[tblRejectReason]    Script Date: 01/06/2017 12:48:10 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRejectReason]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblRejectReason](
		[ReasonID] [int] IDENTITY(1,1) NOT NULL,
		[ReasonName] [varchar](50) NOT NULL
	) ON [PRIMARY]

END
GO

delete from tblRejectReason
SET IDENTITY_INSERT tblRejectReason ON
insert into tblRejectReason(ReasonID, ReasonName)
	values 
		(1, 'Carat weight'), 
		(2, 'Gram weight'),
		(3, 'Color distribution'),
		(4, 'Clarity grade'), 
		(5, 'Color grade')
SET IDENTITY_INSERT tblRejectReason OFF
GO
