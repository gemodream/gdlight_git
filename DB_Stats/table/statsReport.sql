USE [GemoDream16Stats]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[statsReport]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[statsReport](
		[report_id] [int] IDENTITY(1,1) NOT NULL,
		[calc_date] [datetime] NOT NULL,
		[calc_end] [datetime] NULL,
		[user_id] [int] NOT NULL,
		[customerIds] [varchar](3000) NULL,
		[date_from] [datetime] NULL,
		[date_to] [datetime] NULL,
		[comments] [varchar] (1000) NULL,
		[order_state_code] int null
	) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [statsReport_PrimaryIndex] ON [dbo].[statsReport] 
(
	[report_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [statsReport_UserId] ON [dbo].[statsReport] 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [statsReport_CalcDate] ON [dbo].[statsReport] 
(
	[calc_date] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

END

GO



