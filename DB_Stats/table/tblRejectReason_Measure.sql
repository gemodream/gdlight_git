USE [GemoDream16Stats]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[tblRejectReason_Measure]    Script Date: 01/06/2017 12:12:00 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRejectReason_Measure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRejectReason_Measure](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MeasureID] [int] NOT NULL,
	[ReasonID] [int] NOT NULL
) ON [PRIMARY]
END
GO
