﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Corpt.Constants;
using Corpt.Models;
using Corpt.Utilities;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using System.Xml;
using System.Linq;
using Corpt.TreeModel;
//using System.Diagnostics;

namespace Corpt
{
    public partial class Itemize1 : CommonPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID"] == null) Response.Redirect("Login.aspx");
            Page.Title = "GSI: Itemize";
            if (IsPostBack) return;
            itemizedItems.Enabled = false;
            NotInspectedItems.Enabled = false;
            SetViewState(new List<string>(), SessionConstants.ItemizeLotList);
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            GridNewBatches.DataSource = null;
            GridNewBatches.DataBind();
            OldNewErrorMsg.Text = "";
            Session["BatchData"] = null;
            Session["BatchDataLot"] = null;
            Session["PrintBatch"] = null;
            Session["PrintBatchLot"] = null;
            TabContainer.ActiveTabIndex = 2;
            ChangeCPPanel.Enabled = false;
            OldNewPanel.Enabled = false;
            OrderField.Focus();
            SetViewState(new List<string>(), SessionConstants.ItemizeDragedItems);
            SetViewState(new List<string>(), "NewTreeNodes");
        }

        #region Save Button
        protected void OnSaveClick(object sender, EventArgs e)
        {
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            var isLot = IsLotMode();
            List<ItemizedModel> batchList = new List<ItemizedModel>();
            List<ItemizedLotModel> batchListLot = new List<ItemizedLotModel>();
            List<BatchNewModel> fullBatchList = new List<BatchNewModel>();
            List<ItemizeNewBatchLotModel> fullBatchLotList = new List<ItemizeNewBatchLotModel>();
            //ItemizedModel item = new ItemizedModel();
            if (isLot)
            {
                if (Session["BatchDataLot"] != null)
                    batchListLot = (List<ItemizedLotModel>)Session["BatchDataLot"];
                else if (LotList.Items.Count > 0)
                {
                    ItemizedLotModel batchLot = new ItemizedLotModel();
                    batchLot.Memo = MemoListField.SelectedItem.Text;
                    batchLot.Sku = CpListField.SelectedItem.Text;
                    batchLot.Items = LotList.Items.Count.ToString();
                    /*alex
                    foreach (var lotItem in LotList.Items)
                    {
                        ItemizedLotModel batchLot = new ItemizedLotModel();
                        batchLot.Lot = lotItem.ToString();
                        batchLot.Memo = MemoListField.SelectedItem.Text;
                        batchLot.Items = "1";
                        batchLot.Sku = CpListField.SelectedItem.Text;
                        int setNumber = batchListLot.Count;
                        if (setNumber == 0)
                            batchLot.ItemsSet = "1";
                        else
                            batchLot.ItemsSet = (setNumber + 1).ToString();
                        batchListLot.Add(batchLot);
                    }
                    alex */
                    batchListLot.Add(batchLot);
                }
            }
            if (Session["BatchData"] != null)
                batchList = (List<ItemizedModel>)Session["BatchData"];
            if (!isLot)
            {
                foreach (var item in batchList)
                {
                    //item.
                    //-- Customer Program is required
                    //ItemizedModel item = new ItemizedModel();//alex temp
                    //var cp = CpListField.SelectedValue;
                    var cp = item.Sku;
                    if (string.IsNullOrEmpty(cp))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Customer Program is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var cps = GetViewState(SessionConstants.ItemizeCpList) as List<CustomerProgramModel>;
                    if (cps == null) return;
                    //var cpModel = cps.Find(m => m.CpOfficeIdAndCpId == cp);
                    var cpModel = cps.Find(m => m.CustomerProgramName == cp);
                    if (cpModel == null) return;


                    //-- Memo Number is required
                    //var memo = MemoListField.SelectedValue;
                    var memo = item.Memo;
                    if (string.IsNullOrEmpty(memo))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var memos = GetViewState(SessionConstants.ItemizeMemoList) as List<MemoModel>;
                    if (memos == null) return;

                    //var memoModel = memos.Find(m => m.MemoId == memo);
                    var memoModel = memos.Find(m => m.MemoNumber == memo);
                    if (memoModel == null) return;

                    //-- if mode == LOT, Lot Numbers size  > 0
                    var lots = GetViewState(SessionConstants.ItemizeLotList) as List<string>;
                    if (isLot)
                    {
                        if (lots == null || lots.Count == 0)
                        {
                            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Please, Set Lot Numbers!" + "\")</SCRIPT>");
                            return;
                        }
                    }
                    var model = new ItemizeGoModel
                    {
                        Cp = cpModel,
                        Memo = memoModel,
                        //NumberOfItems = GetNumberItems(),
                        NumberOfItems = Convert.ToInt16(item.Items),
                        Order = GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel,
                        LotNumbers = isLot ? lots : new List<string>()
                    };

                    var newBatches = QueryUtils.ItemizeAddItemsByCount(model, this);
                    foreach (var batch in newBatches)
                        fullBatchList.Add(batch);
                    Session["SavedBatch"] = newBatches;
                    Session["BatchData"] = null;
                    
                }//foreach
                GridNewBatches.DataSource = fullBatchList;
                GridNewBatches.DataBind();
            }//!IsLot
            else
            {
                foreach (var item in batchListLot)
                {
                    //item.
                    //-- Customer Program is required
                    //ItemizedModel item = new ItemizedModel();//alex temp
                    //var cp = CpListField.SelectedValue;
                    var cp = item.Sku;
                    if (string.IsNullOrEmpty(cp))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Customer Program is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var cps = GetViewState(SessionConstants.ItemizeCpList) as List<CustomerProgramModel>;
                    if (cps == null) return;
                    //var cpModel = cps.Find(m => m.CpOfficeIdAndCpId == cp);
                    var cpModel = cps.Find(m => m.CustomerProgramName == cp);
                    if (cpModel == null) return;


                    //-- Memo Number is required
                    //var memo = MemoListField.SelectedValue;
                    var memo = item.Memo;
                    if (string.IsNullOrEmpty(memo))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var memos = GetViewState(SessionConstants.ItemizeMemoList) as List<MemoModel>;
                    if (memos == null) return;

                    //var memoModel = memos.Find(m => m.MemoId == memo);
                    var memoModel = memos.Find(m => m.MemoNumber == memo);
                    if (memoModel == null) return;

                    //-- if mode == LOT, Lot Numbers size  > 0
                    //var lots = GetViewState(SessionConstants.ItemizeLotList) as List<string>;
                    //var lots = item.Lot;
                    List<string> lots = new List<string>();
                    if (LotList.Items.Count == 0)
                        lots.Add(item.Lot);
                    else
                    {
                        foreach (var lotItem in LotList.Items)
                            lots.Add(lotItem.ToString());
                    }

                    if (lots == null)
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Please, Set Lot Numbers!" + "\")</SCRIPT>");
                        return;
                    }
                    var model = new ItemizeGoModel
                    {
                        Cp = cpModel,
                        Memo = memoModel,
                        //NumberOfItems = GetNumberItems(),
                        NumberOfItems = Convert.ToInt16(item.Items),
                        Order = GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel,
                        LotNumbers = lots
                        //alex LotNumbers = isLot ? lots : new List<string>()


                    };
                    var newBatches = QueryUtils.ItemizeAddItemsByLots(model, this);
                    foreach (var batch in newBatches)
                        fullBatchLotList.Add(batch);
                    //GridNewBatches.DataSource = newBatches;
                    //GridNewBatches.DataBind();
                    Session["SavedBatchLot"] = newBatches;
                    Session["BatchDataLot"] = null;

                }//foreach
                GridNewBatches.DataSource = fullBatchLotList;
                GridNewBatches.DataBind();
            }//IsLot
            //GridNewBatches.DataSource = fullBatchList;
            //GridNewBatches.DataBind();
            PrintButton.Enabled = true;//GridNewBatches.Items.Count > 0;
            //PrintLabelsBtn.Enabled = GridNewBatches.Items.Count > 0;
            if (!isLot)
            {
                if (PrintBox.Checked)
                {
                    bool printed = PrintLabels(fullBatchList);
                    if (!printed)
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                    "Error printing labels!" + "\")</SCRIPT>");
                }
                else
                    Session["SavePrintBatch"] = fullBatchList;
            }
            else
            {
                if (PrintBox.Checked)
                {
                    bool printed = PrintLabels(fullBatchLotList);
                    if (!printed)
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                    "Error printing labels!" + "\")</SCRIPT>");
                }
                else
                    Session["SavePrintBatchLot"] = fullBatchLotList;
            }
            DataTable dt = QueryUtils.GetUpdatedItems(OrderField.Text, this);
            if (dt != null && dt.Rows.Count > 0)
            {
                string notInspectedItems = dt.Rows[0]["NotInspectedQuantity"].ToString();
                string inspectedItems = dt.Rows[0]["InspectedQuantity"].ToString();
                int intinspectedItems = Convert.ToInt32(inspectedItems);
                string itemizeditems = dt.Rows[0]["ItemsQuantity"].ToString();
                int intitemizeditems = Convert.ToInt32(itemizeditems);
                int countItems = intinspectedItems - intitemizeditems;
                NotInspectedItems.Text = notInspectedItems;
                InspectedItems.Text = inspectedItems;
                itemizedItems.Text = itemizeditems;
                CountItemsField.Text = countItems.ToString();
            }
        }
        private int GetNumberItems()
        {
            if (IsLotMode()) return 0;
            return string.IsNullOrEmpty(CountItemsField.Text) ? 1 : Int32.Parse(CountItemsField.Text);
        }

        protected void OnSaveInspectedClick(object sender, EventArgs e)
        {
            var Order = GetViewState(SessionConstants.ItemizeOrderInfo) as OrderModel;
            int itemNumber = Convert.ToInt16(InspectedItems.Text);
            bool updated = QueryUtils.UpdateInspectedItems(Order, itemNumber, this);
            if (!updated)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                        "Please, Set Lot Numbers!" + "\")</SCRIPT>");
        }

        protected void OnBatchAddClick(object sender, EventArgs e)
        {
            //CountItemsField.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            if (CountItemsField.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Enter the number of items in the division \")</SCRIPT>");
                return;
            }
            int inspected = Convert.ToInt16(InspectedItems.Text);
            int itemized = Convert.ToInt16(itemizedItems.Text);
            int newItems = Convert.ToInt16(CountItemsField.Text);
            int availableItems = inspected - itemized;
            if (string.IsNullOrEmpty(MemoListField.SelectedItem.Text))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                return;
            }
            if (availableItems > 0)
            {
                if (availableItems < newItems)
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Cannot itemize. Available items: " + availableItems.ToString() + "\")</SCRIPT>");
            }
            else
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Cannot itemize. Inspected items less than itemized." + "\")</SCRIPT>"); ;
            }
            if (!IsLotMode())
            {
                List<ItemizedModel> batchList = new List<ItemizedModel>();
                ItemizedModel item = new ItemizedModel();
                if (Session["BatchData"] != null)
                    batchList = (List<ItemizedModel>)Session["BatchData"];
                item.Items = CountItemsField.Text;
                item.Memo = MemoListField.SelectedItem.Text;
                item.Sku = CpListField.SelectedItem.Text;
                int setNumber = batchList.Count;
                if (setNumber == 0)
                {
                    item.ItemsSet = "1";
                    batchList.Add(item);
                    GridNewBatches.DataSource = batchList;
                    GridNewBatches.DataBind();
                    Session["BatchData"] = batchList;
                }
                else
                {
                    item.ItemsSet = (setNumber + 1).ToString();
                    //batchList = (List<ItemizedModel>)GridNewBatches.DataSource;
                    batchList.Add(item);
                    GridNewBatches.DataSource = batchList;
                    GridNewBatches.DataBind();
                    Session["BatchData"] = batchList;
                }
            }
            else
            {
                List<ItemizedLotModel> batchList = new List<ItemizedLotModel>();
                ItemizedLotModel item = new ItemizedLotModel();
                if (Session["BatchDataLot"] != null)
                    batchList = (List<ItemizedLotModel>)Session["BatchDataLot"];
                item.Items = CountItemsField.Text;
                item.Memo = MemoListField.SelectedItem.Text;
                item.Sku = CpListField.SelectedItem.Text;
                item.Lot = LotNumberField.Text;
                int setNumber = batchList.Count;
                if (setNumber == 0)
                {
                    item.ItemsSet = "1";
                    batchList.Add(item);
                    GridNewBatches.DataSource = batchList;
                    GridNewBatches.DataBind();
                    Session["BatchDataLot"] = batchList;
                }
                else
                {
                    item.ItemsSet = (setNumber + 1).ToString();
                    //batchList = (List<ItemizedModel>)GridNewBatches.DataSource;
                    batchList.Add(item);
                    GridNewBatches.DataSource = batchList;
                    GridNewBatches.DataBind();
                    Session["BatchDataLot"] = batchList;
                }
            }
            CountItemsField.Text = "";
        }//OnBatchAddClick

        #endregion
        #region Refresh Button
        protected void OnRefreshClick(object sender, EventArgs e)
        {
            List<ItemizedModel> batchList = new List<ItemizedModel>();
            List<BatchNewModel> savebatchList = new List<BatchNewModel>();
            List<ItemizeNewBatchLotModel> savebatchListLot = new List<ItemizeNewBatchLotModel>();
            List<ItemizedLotModel> batchListLot = new List<ItemizedLotModel>();
            bool isLot = IsLotMode();
            if (isLot)
            {
                if (Session["BatchDataLot"] != null)
                    batchListLot = (List<ItemizedLotModel>)Session["BatchDataLot"];
                else if (Session["SavedBatchLot"] != null)
                    savebatchListLot = (List<ItemizeNewBatchLotModel>)Session["SavedBatchLot"];
            }
            else
            { 
                if (Session["BatchData"] != null)
                {
                    batchList = (List<ItemizedModel>) Session["BatchData"];
                }
                else if (Session["SavedBatch"] != null)
                    savebatchList = (List<BatchNewModel>)Session["SavedBatch"];
            }
            if (batchList.Count > 0)
            {
                GridNewBatches.DataSource = batchList;
                GridNewBatches.DataBind();
            }
            else if (batchListLot.Count > 0)
            {
                GridNewBatches.DataSource = batchListLot;
                GridNewBatches.DataBind();
            }
            else if (savebatchList.Count > 0)
            {
                GridNewBatches.DataSource = savebatchList;
                GridNewBatches.DataBind();
            }
            else if (savebatchListLot.Count > 0)
            {
                GridNewBatches.DataSource = savebatchListLot;
                GridNewBatches.DataBind();
            }
            else
            {
                GridNewBatches.DataSource = null;
                GridNewBatches.DataBind();
            }
            Session["SavedBatch"] = null;
            Session["SavedBatchLot"] = null;
            Session["BatchData"] = null;
            Session["BatchDataLot"] = null;
            string itemized = QueryUtils.GetItemsQuantity(OrderField.Text, this);
            if (itemized != null)
                itemizedItems.Text = itemized;
            else
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Error getting inspected items \")</SCRIPT>");


        }
        #endregion
        #region OldNumber
        protected void OnOldNumberClick(object sender, EventArgs e)
        {
            if (CountItemsField.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Enter inspected items \")</SCRIPT>");
            }
            else
            {
                string maxItems = (Convert.ToInt16(InspectedItems.Text) - Convert.ToInt16(itemizedItems.Text)).ToString();
                //Response.Redirect("ItemizeOldNew.aspx?OrderCode=" + OrderField.Text + @"&MaxItems=" + maxItems);
                ItemsAvailable.Value = maxItems;
                TabContainer.ActiveTabIndex = 1;
                ChangeCPPanel.Enabled = false;
                OldNewPanel.Enabled = true;
                ItemsAvailableBox.Text = ItemsAvailable.Value; ;
                OnOldNewLoadClick(null, null);
            }
        }
        #endregion
        #region Clear Button
        protected void OnClearClick(object sender, EventArgs e)
        {
            OrderField.Text = "";
            CpListField.Items.Clear();
            MemoListField.Items.Clear();
            itemizedItems.Text = "";
            InspectedItems.Text = "";
            NotInspectedItems.Text = "";
            //-- Picture section
            itemPicture.Visible = false;
            itemPicture.ImageUrl = "";
            ErrorPictureField.Text = "";
            Picture2PathField.Text = "";

            //-- GridData section
            GridNewBatches.DataSource = null;
            GridNewBatches.DataBind();
            //PrintButton.Enabled = false; alex
            PrintButton.Enabled = true;
            ReprintButton.Enabled = true;
            ReprintPanel.Visible = false;
            //PrintLabelsBtn.Enabled = false; alex
            //PrintLabelsBtn.Enabled = true;
            CountItemsField.Text = "";
            OldNewErrorMsg.Text = "";
            ErrPrintLbl.Text = "";
            OnClearButtonClick(null, null);
            //-- Lot section
            ClearLotFields();
            SetViewState(new List<string>(), SessionConstants.ItemizeDragedItems);
        }
        private void ClearLotFields()
        {
            LotNumberField.Text = "";
            LotList.Items.Clear();
            SetViewState(new List<string>(), SessionConstants.ItemizeLotList);
        }
        #endregion

        #region Print
        private bool PrintLabels(List<ItemizeNewBatchLotModel> newbatches)
        {
            string sReportPath = null;
            string filename = @"itemizing_batch_label_on_" +  DateTime.Now.ToString() + ".xml";
            string sReportsDir = @"\\mars\reports";
            foreach (var batch in newbatches)
            {
                DataSet dsBatch = QueryUtils.GetBatchWithCustomer(Convert.ToInt32(batch.BatchID), this);
                if (dsBatch == null || dsBatch.Tables[0].Rows.Count == 0)
                {
                    return false;
                }
                DataSet dsItems = QueryUtils.GetItemByCode(dsBatch.Tables[0].Rows[0]["GroupCode"].ToString(),
                dsBatch.Tables[0].Rows[0]["BatchCode"].ToString(), this);
                DataSet dsGroup = QueryUtils.GetGroupWithCustomer(dsBatch.Tables[0].Rows[0]["GroupOfficeID"].ToString(), dsBatch.Tables[0].Rows[0]["GroupID"].ToString(), this);
                DataRow rGroup = dsGroup.Tables[0].Rows[0];
                DataSet dsServiceType = QueryUtils.GetServiceType(rGroup["ServiceTypeID"].ToString(), this);
                StringBuilder sbOldNumbers = new StringBuilder("");
                if (dsItems.Tables[0].Rows[0]["PrevGroupCode"].ToString() != "")
                {
                    sReportPath = sReportsDir + @"batch_label_on.xls";

                    foreach (DataRow drItem in dsItems.Tables[0].Rows)
                    {
                        if (sbOldNumbers.Length != 0) sbOldNumbers.Append(" ");
                        var prevGroupCode = Utils.FillToFiveChars(drItem["PrevGroupCode"].ToString());
                        var prevBatchCode = Utils.FillToThreeChars(drItem["PrevBatchCode"].ToString(), prevGroupCode);
                        var fillToTwoChars = Utils.FillToTwoChars(drItem["PrevItemCode"].ToString());
                        sbOldNumbers.Append(prevGroupCode + "." + prevBatchCode + "." + fillToTwoChars + @",");
                    }
                }
                else
                    sReportPath = sReportsDir + @"Labal_Batch.xls";
                DataRow rBatch = dsBatch.Tables[0].Rows[0];
                string sDeliveryMethod = null, sAccount = null, sCarrier = null;
                if (rBatch["WeShipCarry"].ToString() == "1")
                {
                    sDeliveryMethod = "We Ship Carry";
                    try
                    {
                        sAccount = rBatch["Account"].ToString();
                        //DataSet dsCustomerTypeEx = gemoDream.Service.GetCustomerTypeEx();
                        DataSet tblCarriers = QueryUtils.GetCarriers(this);
                        DataRow[] rCarrier = tblCarriers.Tables[0].Select("CarrierID=" + rBatch["CarrierID"]);
                        sCarrier = rCarrier[0]["CarrierName"].ToString();
                        sDeliveryMethod = "We Use Their Account To Ship";
                    }
                    catch
                    {
                        sAccount = "";
                        sCarrier = "";
                    }
                }
                string gsiOrder = Utils.FillToFiveChars(rBatch["GroupCode"].ToString());
                string gsiBatch = Utils.FillToThreeChars(rBatch["BatchCode"].ToString(), gsiOrder);
                
                string sOrderDate = rGroup["CreateDate"].ToString();
                System.DateTime dOrderDate = System.DateTime.Parse(sOrderDate);
                var stream = new MemoryStream();
                using (XmlWriter writer = XmlWriter.Create(stream))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Labels");
                    writer.WriteStartElement("Batch");
                    writer.WriteElementString("GsiBatch", gsiBatch); //0
                    writer.WriteElementString("GroupCode", gsiOrder);//1
                    writer.WriteElementString("CustomerName", rBatch["CustomerName"].ToString()); //2
                    writer.WriteElementString("ItemsQuantity", rBatch["ItemsQuantity"].ToString());//3
                    if (sbOldNumbers.Length > 0)
                        writer.WriteElementString("OldNumbers", sbOldNumbers.ToString());//4
                    else
                        writer.WriteElementString("OldNumbers", "");
                    writer.WriteElementString("DeliveryMethod", sDeliveryMethod);//5
                    writer.WriteElementString("Carrier", sCarrier);//6
                    writer.WriteElementString("Account", sAccount);//8
                    writer.WriteElementString("Sku", rBatch["CP_Origin"].ToString());
                    writer.WriteElementString("MemoNumber", rBatch["MemoNumber"].ToString());//9
                    if (rBatch["ItemsWeight"].ToString() != "")
                        writer.WriteElementString("ItemWeight", rBatch["ItemsWeight"].ToString() + " ct.");//10
                    else
                        writer.WriteElementString("ItemWeight", "");
                    writer.WriteElementString("DdDate", dsBatch.Tables[0].Rows[0]["CreateDate"].ToString());//11
                    writer.WriteElementString("OrderDate", dOrderDate.Date.ToShortDateString());//12
                    writer.WriteElementString("TimeOfDay", dOrderDate.TimeOfDay.ToString());//13
                    writer.WriteElementString("ExtPhone", rBatch["ExtPhone"].ToString());//14
                    if (!(sbOldNumbers.Length > 0))
                    {
                        DataSet dsCheckedOperations = QueryUtils.GetCheckedOperations(Convert.ToInt32(batch.BatchID), this);
                        if (dsCheckedOperations.Tables[0].Rows.Count > 0)
                        {
                            string operationTypeName = null;
                            for (int i = 0; i < System.Math.Min(dsCheckedOperations.Tables[0].Rows.Count, 10); i++)
                            {
                                if (i == 0)
                                {
                                    operationTypeName = dsCheckedOperations.Tables[0].Rows[i][0].ToString();
                                }
                                else //if(i>0 && i<10)
                                {
                                    operationTypeName += dsCheckedOperations.Tables[0].Rows[i][0].ToString() + @",";
                                }
                            }
                            writer.WriteElementString("OperationTypeName", operationTypeName);
                        }
                    }
                    if (dsServiceType.Tables[0].Rows.Count > 0)
                        writer.WriteElementString("ServiceType", dsServiceType.Tables[0].Rows[0]["ServiceTypeName"].ToString());
                    else
                        writer.WriteElementString("ServiceType", "");
                    writer.WriteEndElement();
                    DataSet dsRecvSet = QueryUtils.GetItem_New_1(Convert.ToInt32(batch.BatchID), this);
                    int itemCount = dsRecvSet.Tables[0].Rows.Count;
                    if (dsRecvSet != null && itemCount > 0)
                    {
                        DataRow sarinRow = dsRecvSet.Tables[0].Rows[0];
                        string sarinLRID = sarinRow["SarinLRID"].ToString();
                        if (sarinLRID == "2000")
                        {
                            /*
                            DataSet dsResults = QueryUtils.GetCIDInfo()
                            dsResults.Tables.Add("CIDInfo");
                            dsResults.Tables[0].Columns.Add("GroupCode");
                            dsResults.Tables[0].Columns.Add("Batch");
                            dsResults.Tables[0].Columns.Add("Item");
                            dsResults.Tables[0].Rows.Add(dsResults.Tables[0].NewRow());

                            dsResults.Tables[0].Rows[0]["GroupCode"] = sGroupCode;
                            dsResults.Tables[0].Rows[0]["Batch"] = sBatchID;
                            dsResults.Tables[0].Rows[0]["Item"] = sItemCode;

                            dsResults = Service.ProxyGenericGet(dsResults);
                            */
                        }
                        int i = 1;
                        foreach (DataRow row in dsRecvSet.Tables[0].Rows)
                        {
                            writer.WriteStartElement("Lot" + i.ToString());
                            i++;
                            writer.WriteElementString("GroupCode", Utils.FillToFiveChars(row["GroupCode"].ToString()));
                            writer.WriteElementString("BatchCode", Utils.FillToThreeChars(row["BatchCode"].ToString(), row["GroupCode"].ToString()));
                            writer.WriteElementString("ItemCode", Utils.FillToTwoChars(row["ItemCode"].ToString()));
                            int itemCode = Convert.ToInt16(row["ItemCode"].ToString());
                            if (itemCode == itemCount)
                                writer.WriteElementString("EndOfBatch", "Y");
                            else
                                writer.WriteElementString("EndOfBatch", "N");
                            writer.WriteElementString("PrevGroupCode", Utils.FillToFiveChars(row["PrevGroupCode"].ToString()));
                            writer.WriteElementString("PrevBatchCode", Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), Utils.FillToFiveChars(row["PrevGroupCode"].ToString())));
                            writer.WriteElementString("PrevItemCode", Utils.FillToTwoChars(row["PrevItemCode"].ToString()));
                            writer.WriteElementString("Sku", row["CP_Origin"].ToString());
                            writer.WriteElementString("Memo", row["MemoNumber"].ToString());
                            writer.WriteElementString("Prefix", row["Prefix"].ToString());
                            writer.WriteElementString("Weight", row["Weight"].ToString());
                            writer.WriteElementString("SarinLRID", row["SarinLRID"].ToString());
                            writer.WriteElementString("SarinID", row["SarinID"].ToString());
                            writer.WriteEndElement();
                        }
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    this.Session["Memory"] = stream;
                    writer.Close();
                    writer.Flush();
                }

                
            }
            DownloadExcelFile(filename, this);
            return true;
        }//PrintLabels by lot

        public bool PrintLabels(List<BatchNewModel> newbatches)
        {
            string sReportPath = null;
            string sReportsDir = @"\\mars\reports";
            string filename = null;// @"itemizing_batch_label_on_" +DateTime.Now.ToString() + ".xml";
            
            var stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Labels");
                foreach (var batch in newbatches)
            {
                DataSet dsBatch = QueryUtils.GetBatchWithCustomer(batch.BatchId, this);
                if (dsBatch == null || dsBatch.Tables[0].Rows.Count == 0)
                {
                    return false;
                }
                DataSet dsItems = QueryUtils.GetItemByCode(dsBatch.Tables[0].Rows[0]["GroupCode"].ToString(),
                dsBatch.Tables[0].Rows[0]["BatchCode"].ToString(), this);
                DataSet dsGroup = QueryUtils.GetGroupWithCustomer(dsBatch.Tables[0].Rows[0]["GroupOfficeID"].ToString(), dsBatch.Tables[0].Rows[0]["GroupID"].ToString(), this);
                DataRow rGroup = dsGroup.Tables[0].Rows[0];
                DataSet dsServiceType = QueryUtils.GetServiceType(rGroup["ServiceTypeID"].ToString(), this);
                StringBuilder sbOldNumbers = new StringBuilder("");
                if (dsItems.Tables[0].Rows[0]["PrevGroupCode"].ToString() != "")
                {
                    sReportPath = sReportsDir + @"batch_label_on.xls";

                    foreach (DataRow drItem in dsItems.Tables[0].Rows)
                    {
                        if (sbOldNumbers.Length != 0) sbOldNumbers.Append(" ");
                        var prevGroupCode = Utils.FillToFiveChars(drItem["PrevGroupCode"].ToString());
                        var prevBatchCode = Utils.FillToThreeChars(drItem["PrevBatchCode"].ToString(), prevGroupCode);
                        var fillToTwoChars = Utils.FillToTwoChars(drItem["PrevItemCode"].ToString());
                        sbOldNumbers.Append(prevGroupCode + "." + prevBatchCode + "." + fillToTwoChars + @",");
                    }
                }
                else
                    sReportPath = sReportsDir + @"Labal_Batch.xls";
                DataRow rBatch = dsBatch.Tables[0].Rows[0];
                string sDeliveryMethod = null, sAccount = null, sCarrier = null;
                if (rBatch["WeShipCarry"].ToString() == "1")
                {
                    sDeliveryMethod = "We Ship Carry";
                    try
                    {
                        sAccount = rBatch["Account"].ToString();
                        //DataSet dsCustomerTypeEx = gemoDream.Service.GetCustomerTypeEx();
                        DataSet tblCarriers = QueryUtils.GetCarriers(this);
                        DataRow[] rCarrier = tblCarriers.Tables[0].Select("CarrierID=" + rBatch["CarrierID"]);
                        sCarrier = rCarrier[0]["CarrierName"].ToString();
                        sDeliveryMethod = "We Use Their Account To Ship";
                    }
                    catch
                    {
                        sAccount = "";
                        sCarrier = "";
                    }
                }
                string gsiOrder = Utils.FillToFiveChars(rBatch["GroupCode"].ToString());
                string gsiBatch = Utils.FillToThreeChars(rBatch["BatchCode"].ToString(), Utils.FillToFiveChars(rBatch["GroupCode"].ToString()));
                filename = @"itemizing_batch_label_on_" + gsiOrder + @"." + gsiBatch + @"_" + DateTime.Now.ToString() + ".xml";
                string sOrderDate = rBatch["CreateDate"].ToString();
                System.DateTime dOrderDate = System.DateTime.Parse(sOrderDate);
                    writer.WriteStartElement("Batch");
                    writer.WriteElementString("GsiBatch", gsiBatch); //0
                    writer.WriteElementString("GroupCode", gsiOrder);//1
                    writer.WriteElementString("CustomerName", rBatch["CustomerName"].ToString()); //2
                    writer.WriteElementString("ItemsQuantity", rBatch["ItemsQuantity"].ToString());//3
                    if (sbOldNumbers.Length > 0)
                        writer.WriteElementString("OldNumbers", sbOldNumbers.ToString());//4
                    else
                        writer.WriteElementString("OldNumbers", "");
                    writer.WriteElementString("DeliveryMethod", sDeliveryMethod);//5
                    writer.WriteElementString("Carrier", sCarrier);//6
                    writer.WriteElementString("Account", sAccount);//8
                    //writer.WriteElementString("Sku", rBatch["CustomerProgramName"].ToString());
                    writer.WriteElementString("Sku", rBatch["CP_Origin"].ToString());
                    writer.WriteElementString("MemoNumber", rBatch["MemoNumber"].ToString());//9
                    if (rBatch["ItemsWeight"].ToString() != "")
                        writer.WriteElementString("ItemWeight", rBatch["ItemsWeight"].ToString() + " ct.");//10
                    else
                        writer.WriteElementString("ItemWeight", "");
                    writer.WriteElementString("DdDate", dsBatch.Tables[0].Rows[0]["CreateDate"].ToString());//11
                    writer.WriteElementString("OrderDate", dOrderDate.Date.ToShortDateString());//12
                    writer.WriteElementString("TimeOfDay", dOrderDate.TimeOfDay.ToString());//13
                    writer.WriteElementString("ExtPhone", rBatch["ExtPhone"].ToString());//14
                    if (!(sbOldNumbers.Length > 0))
                    {
                        DataSet dsCheckedOperations = QueryUtils.GetCheckedOperations(batch.BatchId, this);
                        if (dsCheckedOperations.Tables[0].Rows.Count > 0)
                        {
                            string operationTypeName = null;
                            for (int i = 0; i < System.Math.Min(dsCheckedOperations.Tables[0].Rows.Count, 10); i++)
                            {
                                if (i == 0)
                                {
                                    operationTypeName = dsCheckedOperations.Tables[0].Rows[i][0].ToString();
                                }
                                else //if(i>0 && i<10)
                                {
                                    operationTypeName += dsCheckedOperations.Tables[0].Rows[i][0].ToString() + @",";
                                }
                            }
                            writer.WriteElementString("OperationTypeName", operationTypeName);
                        }
                    }
                    if (dsServiceType.Tables[0].Rows.Count > 0)
                        writer.WriteElementString("ServiceType", dsServiceType.Tables[0].Rows[0]["ServiceTypeName"].ToString());
                    else
                        writer.WriteElementString("ServiceType", "");
                    writer.WriteEndElement();
                    DataSet dsRecvSet = QueryUtils.GetItem_New_1(batch.BatchId, this);
                    int itemCount = dsRecvSet.Tables[0].Rows.Count;
                    if (dsRecvSet != null && itemCount > 0)
                    {
                        DataRow sarinRow = dsRecvSet.Tables[0].Rows[0];
                        string sarinLRID = sarinRow["SarinLRID"].ToString();
                        if (sarinLRID == "2000")
                        {
                            /*
                            DataSet dsResults = QueryUtils.GetCIDInfo()
                            dsResults.Tables.Add("CIDInfo");
                            dsResults.Tables[0].Columns.Add("GroupCode");
                            dsResults.Tables[0].Columns.Add("Batch");
                            dsResults.Tables[0].Columns.Add("Item");
                            dsResults.Tables[0].Rows.Add(dsResults.Tables[0].NewRow());

                            dsResults.Tables[0].Rows[0]["GroupCode"] = sGroupCode;
                            dsResults.Tables[0].Rows[0]["Batch"] = sBatchID;
                            dsResults.Tables[0].Rows[0]["Item"] = sItemCode;

                            dsResults = Service.ProxyGenericGet(dsResults);
                            */
                        }
                        int i = 1;
                        foreach (DataRow row in dsRecvSet.Tables[0].Rows)
                        {
                            if (sarinLRID == "2000")//CID
                            {
                                writer.WriteStartElement("CID");
                                DataSet dsresults = QueryUtils.GetCIDInfo(
                                    Utils.FillToFiveChars(row["GroupCode"].ToString()),
                                    Utils.FillToThreeChars(row["BatchCode"].ToString(), Utils.FillToFiveChars(row["GroupCode"].ToString())),
                                    Utils.FillToTwoChars(row["ItemCode"].ToString()), this);
                                if (dsresults != null && dsresults.Tables[0].Rows.Count != 0)
                                {
                                    var cidRow = dsresults.Tables[0].Rows[0];
                                    i++;
                                    writer.WriteElementString("GroupCode", Utils.FillToFiveChars(row["GroupCode"].ToString()));
                                    writer.WriteElementString("BatchCode", Utils.FillToThreeChars(row["BatchCode"].ToString(), Utils.FillToFiveChars(row["GroupCode"].ToString())));
                                    writer.WriteElementString("ItemCode", Utils.FillToTwoChars(row["ItemCode"].ToString()));
                                    int itemCode = Convert.ToInt16(row["ItemCode"].ToString());
                                    if (itemCode == itemCount)
                                        writer.WriteElementString("EndOfBatch", "Y");
                                    else
                                        writer.WriteElementString("EndOfBatch", "N");
                                    writer.WriteElementString("PrevGroupCode", Utils.FillToFiveChars(row["PrevGroupCode"].ToString()));
                                    writer.WriteElementString("PrevBatchCode", Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), Utils.FillToFiveChars(row["PrevGroupCode"].ToString())));
                                    writer.WriteElementString("PrevItemCode", Utils.FillToTwoChars(row["PrevItemCode"].ToString()));
                                    writer.WriteElementString("Prefix", cidRow["Prefix"].ToString());
                                    writer.WriteElementString("Weight", row["Weight"].ToString());
                                    writer.WriteElementString("SarinLRID", row["SarinLRID"].ToString());
                                    writer.WriteElementString("SarinID", row["SarinID"].ToString());
                                    writer.WriteEndElement();
                                }
                            }
                            else
                            {
                                writer.WriteStartElement("Item");
                                i++;
                                writer.WriteElementString("GroupCode", Utils.FillToFiveChars(row["GroupCode"].ToString()));
                                writer.WriteElementString("BatchCode", Utils.FillToThreeChars(row["BatchCode"].ToString(), Utils.FillToFiveChars(row["GroupCode"].ToString())));
                                writer.WriteElementString("ItemCode", Utils.FillToTwoChars(row["ItemCode"].ToString()));
                                int itemCode = Convert.ToInt16(row["ItemCode"].ToString());
                                if (itemCode == itemCount)
                                    writer.WriteElementString("EndOfBatch", "Y");
                                else
                                    writer.WriteElementString("EndOfBatch", "N");
                                writer.WriteElementString("PrevGroupCode", Utils.FillToFiveChars(row["PrevGroupCode"].ToString()));
                                writer.WriteElementString("PrevBatchCode", Utils.FillToThreeChars(row["PrevBatchCode"].ToString(), Utils.FillToFiveChars(row["PrevGroupCode"].ToString())));
                                writer.WriteElementString("PrevItemCode", Utils.FillToTwoChars(row["PrevItemCode"].ToString()));
                                writer.WriteElementString("Sku", rBatch["CP_Origin"].ToString());
                                writer.WriteElementString("Memo", row["MemoNumber"].ToString());
                                writer.WriteElementString("Prefix", row["Prefix"].ToString());
                                writer.WriteElementString("Weight", row["Weight"].ToString());
                                writer.WriteElementString("SarinLRID", row["SarinLRID"].ToString());
                                writer.WriteElementString("SarinID", row["SarinID"].ToString());
                                writer.WriteEndElement();
                            }
                        }
                    }
                    

                
            }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                this.Session["Memory"] = stream;
                writer.Close();
                writer.Flush();
            }
            bool sent = QueryUtils.SendMessageToStorage(stream, this, "itemizing");
            //DownloadExcelFile(filename, this);
            return true;
        }
        public void DownloadExcelFile(String filename, Page p)
        {

            //p.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true); alex
            //string redirect = "<script>location.assign(window.location);</script>";
            //p.Response.Write(redirect);
            p.Response.Clear();
            //p.Response.Buffer = true; //alex
            MemoryStream msMemory = (MemoryStream)p.Session["Memory"];
            //var dir = GetExportDirectory(p);
            //-- Download
            p.Response.ContentType = "text/plain";
            p.Response.AddHeader("Refresh", "12;URL=ScreeningFrontEntries.aspx"); //alex
            p.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            p.Response.BinaryWrite(msMemory.ToArray());
            //p.Response.OutputStream.Flush();//alex
            //p.Response.OutputStream.Close();//alex
            //p.Response.TransmitFile(filename);
            //p.Response.TransmitFile(filename);
            //p.Response.Flush(); //alex
            //p.Response.Redirect("ScreeningFrontEntries.aspx");
            try
            {
                //SyntheticScreeningModel data = new SyntheticScreeningModel();
                //data.ScreeningID = 123;
                //data.SKUName = txtSku.Text;
                //data.CustomerName = txtCustomerName.Text;
                //Session["ScreeningData"] = data;
                // Context.Items.Add("abc", "xyz");
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("ScreeningFrontEntries.aspx", true);
                //Response.Redirect(Request.RawUrl);
                //Context.Items.Add(txtGSIOrder, "abc");
                //Server.Transfer("ScreeningFrontEntries.aspx", false);
                //string navigate = "<script>window.open('ScreeningFrontEntries.aspx');</script>";
                //Response.Redirect(navigate);
                p.Response.End();
            }
            catch (Exception ex)
            {
                // string abc = ex.Message;
                // txtGSIOrder.Text = "abc";

            }
        }

        #endregion
        #region Order search, Cp changes, Picture show
        protected void OnOrderSearchClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("OrderGroup");
            if (!Page.IsValid) return;
            Session["BatchData"] = null;
            Session["BatchDataLot"] = null;
            MemoListField.Items.Clear();
            NotInspectedItems.Text = "";
            InspectedItems.Text = "";
            itemizedItems.Text = "";
            CountItemsField.Text = "";
            GridNewBatches.DataSource = null;
            GridNewBatches.DataBind();
            var orderModel = QueryUtils.GetOrderInfo(OrderField.Text.Trim(), this);

            if (orderModel == null)
            {
                var msg = string.Format("Order '{0}' not found.", OrderField.Text.Trim());
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + msg + "\")</SCRIPT>");
                return;
            }
            SetViewState(orderModel, SessionConstants.ItemizeOrderInfo);

            CustomerNameField.Text = orderModel.Customer.CustomerName;
            var memoList = QueryUtils.GetOrderMemos(orderModel, this);
            SetViewState(memoList, SessionConstants.ItemizeMemoList);

            var cpList = QueryUtils.GetOrderCp(orderModel, this);
            SetViewState(cpList, SessionConstants.ItemizeCpList);
            AddBatchButton.Enabled = true;
            //AddBatchButton.Visible = false;
            
            MemoListField.DataTextField = "MemoNumber";
            MemoListField.DataValueField = "MemoId";

            MemoListField.DataSource = memoList;
            MemoListField.DataBind();

            CpListField.DataTextField = "CustomerProgramName";
            CpListField.DataValueField = "CpOfficeIdAndCpId";

            CpListField.DataSource = cpList;
            CpListField.DataBind();

            if (!string.IsNullOrEmpty(CpListField.SelectedValue))
            {
                OnCpChanged(null, null);
            }
            NotInspectedItems.Text = orderModel.notInspected.ToString();
            InspectedItems.Text = orderModel.inspected.ToString();
            itemizedItems.Text = orderModel.itemized.ToString();
            CountItemsField.Text = (Convert.ToInt16(InspectedItems.Text) - Convert.ToInt16(itemizedItems.Text)).ToString();
            OldNewPanel.Enabled = false;
            ChangeCPPanel.Enabled = false;
        }
        protected void OnCpChanged(object sender, EventArgs e)
        {
            Picture2PathField.Text = "";
            itemPicture.Visible = false;

            var cp = CpListField.SelectedValue;
            if (string.IsNullOrEmpty(cp)) return;
            var cpList = GetViewState(SessionConstants.ItemizeCpList) as List<CustomerProgramModel>;
            if (cpList == null) return;
            var cpModel = cpList.Find(m => m.CpOfficeIdAndCpId == cp);
            if (cpModel == null) return;
            ShowPicture(cpModel.Path2Picture);
        }
        private void ShowPicture(string dbPicture)
        {
            //string imgPath;
            string errMsg;
			var ms = new MemoryStream();
			var fileType = "";
			var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
			//var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
				var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
				itemPicture.ImageUrl = ImageUrl;
                itemPicture.Visible = true;
            }
            //-- Path to picture
            Picture2PathField.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture"); 
            ErrorPictureField.Text = errMsg;
        }
        #endregion

        #region LotNumbers
        protected void OnLotAddClick(object sender, ImageClickEventArgs e)
        {
            var lot = LotNumberField.Text.Trim();
            if (lot.Length == 0) return;
            var lotList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            if (lotList.Contains(lot)) return;

            lotList.Add(lot);
            SetViewState(lotList, SessionConstants.ItemizeLotList);

            LotList.DataSource = lotList;
            LotList.DataBind();
            LotList.SelectedValue = lot;
            LotNumberField.Text = "";
            LotNumberField.Focus();
        }

        protected void OnLotDelClick(object sender, ImageClickEventArgs e)
        {
            var lot = LotList.SelectedValue;
            if (string.IsNullOrEmpty(lot)) return;
            var lotList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            var idxNext = LotList.SelectedIndex;
            lotList.Remove(lot);
            SetViewState(lotList, SessionConstants.ItemizeLotList);
            LotList.DataSource = lotList;
            LotList.DataBind();
            if (idxNext >= lotList.Count && lotList.Count > 0)
            {
                LotList.SelectedIndex = 0;
            }
            if (idxNext < lotList.Count)
            {
                LotList.SelectedIndex = idxNext;
            }

        }
        #endregion

        #region Mode section
        private bool IsLotMode()
        {
            return modeSelector.SelectedValue == "1";
        }
        protected void OnModeChanged(object sender, EventArgs e)
        {
            var isLot = IsLotMode();
            LotPanel.Visible = isLot;
            Panel2.Visible = isLot;
            Label2.Visible = isLot;
            //LotPanel.Enabled = isLot;
            //LotNumberField.Enabled = isLot;
            //AddLotButton.Enabled = isLot;
            //CountItemsField.Enabled = !isLot; alex
            //CountItemsField.Enabled = true;
            if (!isLot)
            {
                ClearLotFields();
            }

        }
        #endregion

        #region Print labels
        protected void OnPrintClick(object sender, EventArgs e)
        {
            if (Session["SavePrintBatch"] != null)
            {
                List<BatchNewModel> savePrintBatchList = new List<BatchNewModel>();
                savePrintBatchList = (List<BatchNewModel>)Session["SavePrintBatch"];
                bool printed = PrintLabels(savePrintBatchList);
                if (!printed)
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Error printing labels!" + "\")</SCRIPT>");
            }
            else if (Session["SavePrintBatchLot"] != null)
            {   
                List<ItemizeNewBatchLotModel> savePrintBatchListLot = new List<ItemizeNewBatchLotModel>();
                savePrintBatchListLot = (List<ItemizeNewBatchLotModel>)Session["SavePrintBatchLot"];
                bool printed = PrintLabels(savePrintBatchListLot);
                if (!printed)
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Error printing labels!" + "\")</SCRIPT>");
            }
        }
        protected void OnPreviewLabelsClick(object sender, EventArgs e)
        {
            /* alex
             * <td><asp:Button ID="PrintButton" runat="server" Text="Print" CssClass="btn btn-info" OnClientClick="DataPrint()" Enabled="False" ToolTip="Print new batch numbers"/></td>
             * <td><asp:Button ID="PrintLabelsBtn" runat="server" Text="Preview Labels" CssClass="btn btn-info" OnClick="OnPreviewLabelsClick"  Enabled="False"/></td>
             */
            var labels = new List<PrintLabelModel>();
            foreach (DataGridItem item in GridNewBatches.Items)
            {
                var order = item.Cells[1].Text;
                var batch = item.Cells[2].Text;
                var count = Convert.ToInt32( item.Cells[4].Text);
                labels.AddRange(PrintUtils.CreateLabelItemize(order, batch, count));
            }
            if (labels.Count == 0)
            {
                // alert
                return;
            }
            /*
            if (LetterBox.Checked)
            {
                LoadLabels2Grid(labels);
            }
            else {
                LoadLabelsGrid(labels);
            }
            */
        }

        protected void OnPreviewLabelDataBound(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
            var labelModel = e.Item.DataItem as PrintLabelModel;
            if (labelModel == null) return;
            foreach (var val in labelModel.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName) as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
        }
        protected void OnPreviewLabelDataBound2(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) return;
            var label2Model = e.Item.DataItem as PrintLabel2Model;
            if (label2Model == null) return;
            foreach (var val in label2Model.LabelOne.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName + "_l") as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
            foreach (var val in label2Model.LabelTwo.PaintValues)
            {
                var fld = e.Item.FindControl(val.CellName + "_r") as Label;
                if (fld != null) fld.Text = val.CellValue;
            }
        }

        protected void OnLabelOldPrintClick(object sender, EventArgs e)
        {
            LabelOldPopupExtender.Show();
        }
        protected void OnLabelOld2PrintClick(object sender, EventArgs e)
        {
            LabelOld2PopupExtender.Show();
        }
        private void LoadLabels2Grid(List<PrintLabelModel> labelModels)
        {
            var labels2 = PrintUtils.Convert2ColumnsFormat(labelModels);
            LabelOld2Grid.DataSource = labels2;
            LabelOld2Grid.DataBind();
            LabelOld2PopupExtender.Show();
        }
        private void LoadLabelsGrid(List<PrintLabelModel> labelModels)
        {
            LabelOldGrid.DataSource = labelModels;
            LabelOldGrid.DataBind();
            LabelOldPopupExtender.Show();
        }


        #endregion

        protected void OnReprintBatchItemClick(object sender, EventArgs e)
        {
            ErrPrintLbl.Text = "";
            List<BatchNewModel> savePrintBatchList = new List<BatchNewModel>();
            string order = null, batch = null, item = null;
            if (ItemTextBox.Text.Length == 9)
            {
                if (ItemTextBox.Text[6] == '0')//6 digits  order
                {
                    order = ItemTextBox.Text.Substring(0, 6);
                    batch = ItemTextBox.Text.Substring(6, 3);
                }
                else
                {
                    order = ItemTextBox.Text.Substring(0, 7);
                    batch = ItemTextBox.Text.Substring(7, 2);
                }
            }
            else if (ItemTextBox.Text.Length == 10 || ItemTextBox.Text.Length == 8)//5 digit order with items
            {
                order = ItemTextBox.Text.Substring(0, 6);
                batch = ItemTextBox.Text.Substring(6, 3);
            }
            else if (ItemTextBox.Text.Length == 11)//order/batch/item
            {
                if (ItemTextBox.Text[6] == '0')//6 digits  order
                {
                    order = ItemTextBox.Text.Substring(0, 6);
                    batch = ItemTextBox.Text.Substring(6, 3);
                }
                else
                {
                    order = ItemTextBox.Text.Substring(0, 7);
                    batch = ItemTextBox.Text.Substring(7, 2);
                }
            }
            else
            {
                ErrPrintLbl.Text = "Enter right number";
                return;
            }
            DataTable dtReprintBatchItems = QueryUtils.ReprintBatchItemLabel(order, batch, item, true, this);
            BatchNewModel newBatch = new BatchNewModel();
            if (dtReprintBatchItems == null || dtReprintBatchItems.Rows.Count == 0)
                return;
            newBatch.BatchId = Convert.ToInt32(dtReprintBatchItems.Rows[0]["BatchID"].ToString());
            savePrintBatchList.Add(newBatch);
            bool printed = PrintLabels(savePrintBatchList);
            if (!printed)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Error printing labels!" + "\")</SCRIPT>");
        }

        protected void OnReprintItemClick(object sender, EventArgs e)
        {
            ErrPrintLbl.Text = "";
            if (ItemTextBox.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Enter Item Number" + "\")</SCRIPT>");
                return;
            }
            string order = null, batch = null, item = null;
            if (ItemTextBox.Text.Length == 11)
            {
                if (ItemTextBox.Text[6] == '0')//6 digits  order
                {
                    order = ItemTextBox.Text.Substring(0, 6);
                    batch = ItemTextBox.Text.Substring(6, 3);
                }
                else
                {
                    order = ItemTextBox.Text.Substring(0, 7);
                    batch = ItemTextBox.Text.Substring(7, 2);
                }
                item = ItemTextBox.Text.Substring(9);
            }
            else if (ItemTextBox.Text.Length == 10)
            {
                order = ItemTextBox.Text.Substring(0, 5);
                batch = ItemTextBox.Text.Substring(5, 3);
                item = ItemTextBox.Text.Substring(8);
            }
            else
            {
                ErrPrintLbl.Text = "Enter right number";
                return;
            }
            DataTable dtReprintItem = QueryUtils.ReprintBatchItemLabel(order, batch, item, false, this);
            if (dtReprintItem == null || dtReprintItem.Rows.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Item Not Found!" + "\")</SCRIPT>");
                return;
            }
            bool printed = ReprintBatchItemLabels(dtReprintItem, "items", order, batch, item);
            if (!printed)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Error printing labels!" + "\")</SCRIPT>");
        }

        protected void OnReprintBatchClick(object sender, EventArgs e)
        {
            ErrPrintLbl.Text = "";
            if (ItemTextBox.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Enter Batch Number" + "\")</SCRIPT>");
                return;
            }
            string order = null, batch = null, item = null;
            if (ItemTextBox.Text.Length == 9)
            {
                if (ItemTextBox.Text[6] == '0')//6 digits  order
                {
                    order = ItemTextBox.Text.Substring(0, 6);
                    batch = ItemTextBox.Text.Substring(6, 3);
                }
                else
                {
                    order = ItemTextBox.Text.Substring(0, 7);
                    batch = ItemTextBox.Text.Substring(7, 2);
                }
            }
            else if (ItemTextBox.Text.Length == 8)
            {
                order = ItemTextBox.Text.Substring(0, 5);
                batch = ItemTextBox.Text.Substring(5, 3);
            }
            else
            {
                ErrPrintLbl.Text = "Enter right number";
                return;
            }
                DataTable dtReprintBatch = QueryUtils.ReprintBatchItemLabel(order, batch, item, false, this);
            if (dtReprintBatch == null || dtReprintBatch.Rows.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Item Not Found!" + "\")</SCRIPT>");
                return;
            }
            bool printed = ReprintBatchItemLabels(dtReprintBatch, "batch", null, null, null);
            if (!printed)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Error printing labels!" + "\")</SCRIPT>");
        }

        protected bool ReprintBatchItemLabels(DataTable dt, string batchOrItem, string order, string batch, string item)
        {
            bool result = true;
            string sReportPath = null;
            string sReportsDir = @"\\mars\reports";
            string filename = null;// @"itemizing_batch_label_on_" +DateTime.Now.ToString() + ".xml";
            string memo = "", sku = "", gsiOrder = "", gsiBatch = "", serviceTypeName = "";
            int batchId = Convert.ToInt32( dt.Rows[0]["BatchID"].ToString());
            var stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                if (batchOrItem == "batch")
                //foreach (var batch in newbatches)
                {
                    DataRow dr = dt.Rows[0];
                    order = dr["GroupCode"].ToString();
                    batch = dr["BatchCode"].ToString();
                    batchId = Convert.ToInt32(dr["BatchId"].ToString());
                    memo = dr["MemoNumber"].ToString();
                    sku = dr["CP_Origin"].ToString();
                    DataSet dsBatch = QueryUtils.GetBatchWithCustomer(batchId, this);
                    if (dsBatch == null || dsBatch.Tables[0].Rows.Count == 0)
                    {
                        return false;
                    }
                    DataSet dsItems = QueryUtils.GetItemByCode(dsBatch.Tables[0].Rows[0]["GroupCode"].ToString(),
                    dsBatch.Tables[0].Rows[0]["BatchCode"].ToString(), this);
                    DataSet dsGroup = QueryUtils.GetGroupWithCustomer(dsBatch.Tables[0].Rows[0]["GroupOfficeID"].ToString(), dsBatch.Tables[0].Rows[0]["GroupID"].ToString(), this);
                    DataRow rGroup = dsGroup.Tables[0].Rows[0];
                    DataSet dsServiceType = QueryUtils.GetServiceType(rGroup["ServiceTypeID"].ToString(), this);
                    if (dsServiceType != null && dsServiceType.Tables[0].Rows.Count > 0)
                        serviceTypeName = dsServiceType.Tables[0].Rows[0].ItemArray[1].ToString();
                    StringBuilder sbOldNumbers = new StringBuilder("");
                    if (dsItems.Tables[0].Rows[0]["PrevGroupCode"].ToString() != "")
                    {

                        foreach (DataRow drItem in dsItems.Tables[0].Rows)
                        {
                            if (sbOldNumbers.Length != 0) sbOldNumbers.Append(" ");
                            var prevGroupCode = Utils.FillToFiveChars(drItem["PrevGroupCode"].ToString());
                            var prevBatchCode = Utils.FillToThreeChars(drItem["PrevBatchCode"].ToString(), prevGroupCode);
                            var fillToTwoChars = Utils.FillToTwoChars(drItem["PrevItemCode"].ToString());
                            sbOldNumbers.Append(prevGroupCode + "." + prevBatchCode + "." + fillToTwoChars + @",");
                        }
                    }
                    else
                        sReportPath = sReportsDir + @"Labal_Batch.xls";
                    DataRow rBatch = dsBatch.Tables[0].Rows[0];
                    string sDeliveryMethod = null, sAccount = null, sCarrier = null, operationTypeName = null;
                    if (rBatch["WeShipCarry"].ToString() == "1")
                    {
                        sDeliveryMethod = "We Ship Carry";
                        try
                        {
                            sAccount = rBatch["Account"].ToString();
                            //DataSet dsCustomerTypeEx = gemoDream.Service.GetCustomerTypeEx();
                            DataSet tblCarriers = QueryUtils.GetCarriers(this);
                            DataRow[] rCarrier = tblCarriers.Tables[0].Select("CarrierID=" + rBatch["CarrierID"]);
                            sCarrier = rCarrier[0]["CarrierName"].ToString();
                            sDeliveryMethod = "We Use Their Account To Ship";
                        }
                        catch
                        {
                            sAccount = "";
                            sCarrier = "";
                        }
                    }
                    gsiOrder = Utils.FillToFiveChars(rBatch["GroupCode"].ToString());
                    gsiBatch = Utils.FillToThreeChars(rBatch["BatchCode"].ToString(), Utils.FillToFiveChars(rBatch["GroupCode"].ToString()));
                    filename = @"itemizing_batch_label_on_" + gsiOrder + @"." + gsiBatch + @"_" + DateTime.Now.ToString() + ".xml";
                    string sOrderDate = rBatch["CreateDate"].ToString();
                    System.DateTime dOrderDate = System.DateTime.Parse(sOrderDate);

                    if (!(sbOldNumbers.Length > 0))
                    {
                        DataSet dsCheckedOperations = QueryUtils.GetCheckedOperations(batchId, this);
                        if (dsCheckedOperations.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < System.Math.Min(dsCheckedOperations.Tables[0].Rows.Count, 10); i++)
                            {
                                if (i == 0)
                                {
                                    operationTypeName = dsCheckedOperations.Tables[0].Rows[i][0].ToString();
                                }
                                else //if(i>0 && i<10)
                                {
                                    operationTypeName += dsCheckedOperations.Tables[0].Rows[i][0].ToString() + @",";
                                }
                            }
                            //writer.WriteElementString("OperationTypeName", operationTypeName);
                        }
                    }
                    DataTable dataInputs = new DataTable("Batch");
                    dataInputs.Columns.Add("GsiBatch", typeof(String));
                    dataInputs.Columns.Add("GroupCode", typeof(String));
                    dataInputs.Columns.Add("CustomerName", typeof(String));
                    dataInputs.Columns.Add("ItemsQuantity", typeof(String));
                    dataInputs.Columns.Add("OldNumbers", typeof(String));
                    dataInputs.Columns.Add("DeliveryMethod", typeof(String));
                    dataInputs.Columns.Add("Carrier", typeof(String));
                    dataInputs.Columns.Add("Account", typeof(String));
                    dataInputs.Columns.Add("Sku", typeof(String));
                    dataInputs.Columns.Add("MemoNumber", typeof(String));
                    dataInputs.Columns.Add("ItemsWeight", typeof(String));
                    dataInputs.Columns.Add("DdDate", typeof(String));
                    dataInputs.Columns.Add("OrderDate", typeof(String));
                    dataInputs.Columns.Add("TimeOfDay", typeof(String));
                    dataInputs.Columns.Add("ExtPhone", typeof(String));
                    dataInputs.Columns.Add("OperationTypeName", typeof(String));
                    dataInputs.Columns.Add("ServiceType", typeof(String));

                    DataRow row;
                    row = dataInputs.NewRow();
                    row[0] = gsiBatch;
                    row[1] = gsiOrder;
                    row[2] = rBatch["CustomerName"].ToString();
                    row[3] = rBatch["ItemsQuantity"].ToString();
                    if (sbOldNumbers.Length > 0)
                        row[4] = sbOldNumbers.ToString();
                    else
                        row[4] = "";
                    row[5] = sDeliveryMethod;
                    row[6] = sCarrier;
                    row[7] = sAccount;
                    row[8] = rBatch["CP_Origin"].ToString();
                    row[9] = rBatch["MemoNumber"].ToString();
                    if (rBatch["ItemsWeight"].ToString() != "")
                        row[10] = rBatch["ItemsWeight"].ToString() + " ct.";//10
                    else
                        row[10] = "";
                    row[11] = dsBatch.Tables[0].Rows[0]["CreateDate"].ToString();
                    row[12] = dOrderDate.Date.ToShortDateString();
                    row[13] = dOrderDate.TimeOfDay.ToString();
                    row[14] = rBatch["ExtPhone"].ToString();
                    if (operationTypeName != null)
                        row[15] = operationTypeName;
                    row[16] = serviceTypeName;

                    dataInputs.Rows.Add(row);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        dataInputs.WriteXml(ms);
                        bool sent = QueryUtils.SendMessageToStorage(ms, this, "itemizing");
                        if (!sent)
                            result = false;
                    }
                    dataInputs.Clear();

                }//batch
                else if (batchOrItem == "items")
                {
                    DataRow dr = dt.Rows[0];
                    DataTable dataInputs = new DataTable("Item");
                    dataInputs.Columns.Add("GroupCode", typeof(String));
                    dataInputs.Columns.Add("BatchCode", typeof(String));
                    dataInputs.Columns.Add("ItemCode", typeof(String));
                    dataInputs.Columns.Add("Sku", typeof(String));
                    dataInputs.Columns.Add("Memo", typeof(String));
                    DataRow row;
                    row = dataInputs.NewRow();
                    row[0] = order;
                    row[1] = batch;
                    row[2] = item;
                    row[3] = dr["CP_Origin"].ToString();
                    row[4] = dr["MemoNumber"].ToString(); ;
                    dataInputs.Rows.Add(row);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        dataInputs.WriteXml(ms);
                        bool sent = QueryUtils.SendMessageToStorage(ms, this, "itemizing");
                        if (!sent)
                            result = false;
                    }
                    dataInputs.Clear();
                }
            }
            return true;
        }
        

        protected void OnReprintClick(object sender, EventArgs e)
        {
            ReprintPanel.Visible = true;
        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            string abc = "abc";
            var zzz = TabContainer.Tabs[TabContainer.ActiveTabIndex];
            if (TabContainer.ActiveTabIndex == 0)//Change CP
            {
                string checkedItem = GetCheckedItems();
                if (checkedItem == null)
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items found for \")</SCRIPT>");
                    return;
                }
                string[] items = checkedItem.Split(',');
                string item = items[0];
                string oldSku = items[1];
                int idx1 = item.IndexOf('(');
                int idx2 = item.IndexOf(')');
                item = item.Substring(idx1 + 1, idx2 - idx1 - 1);
                string newOrder = null;
                string oldItem = null;
                if (OldNewNewOrder.Text != "")
                {
                    newOrder = OldNewNewOrder.Text;
                    ChangeCPNewOrder.Value = newOrder;
                }
                if (item != "")
                {
                    oldItem = (string)Request.QueryString["Item"];
                    ChangeCPOldItem.Value = item;
                }

                LoadItemizingScreen(newOrder, oldItem);
            }
            else if (TabContainer.ActiveTabIndex == 1)//OldNew
            {
                OldNewNewOrder.Text = OrderField.Text;
                ItemsAvailable.Value = itemizedItems.Text;
                OldNewNewOrder.Enabled = false;
                ItemsToAdd.Text = "";
                var orderModel = QueryUtils.GetOrderInfo(OldNewNewOrder.Text.Trim(), this);
                Session["OrderModel"] = orderModel;
                var memoList = QueryUtils.GetOrderMemos(orderModel, this);
                SetViewState(memoList, SessionConstants.ItemizeMemoList);
                MemoListField.DataTextField = "MemoNumber";
                MemoListField.DataValueField = "MemoId";
                MemoListField.DataSource = memoList;
                MemoListField.DataBind();
                OldItemsTree.Visible = false;
                OldItemsTreePanel.Visible = false;
                ItemField.Enabled = false;
                Session["SaveBatchList"] = null;
                lstMovedItems.Items.Clear();
                lstMovedItems.Visible = false;
                DragAndDropButtons.Visible = false;
                DragedItems.Items.Clear();
                DragedItems.Visible = false;
                ItemField.Focus();
            }
        }
        #region ChangeCP
        protected void OnCPChangedLoad(object sender, EventArgs e)
        {
            var tmp = Request.UrlReferrer.ToString();
            Session["Redirect"] = tmp;
            string newOrder = null;
            string oldItem = null;
            if (Request.QueryString["OrderCode"] != null)
            {
                ChangeCPNewOrder.Value = OldNewNewOrder.Text;
            }
            if (Request.QueryString["Item"] != null)
            {
                oldItem = (string)Request.QueryString["Item"];
                ChangeCPOldItem.Value = oldItem;
            }

            LoadItemizingScreen(newOrder, oldItem);
        }
        protected void OnCPCopyClick(object sender, EventArgs e)
        {
            ItemizedChangedCPModel itemChangedCP = new ItemizedChangedCPModel();
            itemChangedCP.newOrder = ChangeCPNewOrder.Value;
            itemChangedCP.oldItem = ChangeCPOldItem.Value;
            itemChangedCP.newSku = NewCP.Text;
            itemChangedCP.oldSku = OldCP.Text.Substring(8);
            itemChangedCP.itemTypeId = ChangeCPItemTypeId.Value;
            Session["BackToItemizing"] = itemChangedCP;
            ItemizedChangedCPModel changeResults = new ItemizedChangedCPModel();
            changeResults = (ItemizedChangedCPModel)Session["BackToItemizing"];
            OldNewPanel.Enabled = true;
            ChangeCPPanel.Enabled = false;
            bool reloaded = ReloadTree(changeResults);
            TabContainer.ActiveTabIndex = 1;
            
        }

        protected void OnCPClearClick(object sender, EventArgs e)
        {
            ClearCPChangedData();
            TabContainer.ActiveTabIndex = 1;
            //ResetSourceData();
            //ResetTargetData();
        }
        protected void ClearCPChangedData()
        {
            ChangeCPListField.Items.Clear();
            OldCP.Text = "";
            NewCP.Text = "";
            OldCustomerIDBox.Text = "";
            OldCustomerStyleBox.Text = "";
            OldSRPBox.Text = "";
            OldDescBox.Text = "";
            OldCommentBox.Text = "";
            OldItemPicture.ImageUrl = "";
            NewCustomerIDBox.Text = "";
            NewCustomerStyleBox.Text = "";
            NewSRPBox.Text = "";
            NewDescBox.Text = "";
            NewCommentBox.Text = "";
            NewItemPicture.ImageUrl = "";
        }
        protected void LoadItemizingScreen(string newOrder, string oldItem)
        {
            string oldCPName = null, oldCustomerId = null, oldCustomerStyle = null, oldDescription = null, oldComment = null, oldPicturePath = null, itemTypeID = null;
            string newItemTypeId = null, newCustomerId = null;
            DataTable dtOldBatch = QueryUtils.GetOldCustomerProgramByBatchCode(oldItem, this);
            if (dtOldBatch != null && dtOldBatch.Rows.Count != 0)
            {
                DataRow drOldBatch = dtOldBatch.Rows[0];
                oldCPName = drOldBatch["CustomerProgramName"].ToString();
                oldCustomerId = drOldBatch["CustomerID"].ToString();
                oldCustomerStyle = drOldBatch["CustomerStyle"].ToString();
                oldDescription = drOldBatch["Description"].ToString();
                oldComment = drOldBatch["Comment"].ToString();
                oldPicturePath = drOldBatch["Path2Picture"].ToString();
                itemTypeID = drOldBatch["ItemTypeID"].ToString();
                OldCP.Text = "Old CP: " + oldCPName;
                OldCustomerIDBox.Text = oldCustomerId;
                OldCustomerStyleBox.Text = oldCustomerStyle;
                OldDescBox.Text = oldDescription;
                OldCommentBox.Text = oldComment;
                ShowPicture(oldPicturePath, true);
                OldCP.Enabled = false;
                OldCustomerIDBox.Enabled = false;
                OldCustomerStyleBox.Enabled = false;
                OldDescBox.Enabled = false;
                OldCommentBox.Enabled = false;
                OldSRPBox.Enabled = false;
                NewCP.Enabled = false;
                NewCustomerIDBox.Enabled = false;
                NewCustomerStyleBox.Enabled = false;
                NewDescBox.Enabled = false;
                NewCommentBox.Enabled = false;
                NewSRPBox.Enabled = false;
                TabContainer.ActiveTabIndex = 1;
                ChangeCPPanel.Enabled = false;
            }
            else
                return;
            DataTable dtNewOrder = QueryUtils.GetGroupByCode(newOrder, this);
            if (dtNewOrder != null && dtNewOrder.Rows.Count != 0)
            {
                DataRow drNewOrder = dtNewOrder.Rows[0];
                newItemTypeId = itemTypeID;
                newCustomerId = drNewOrder["CustomerID"].ToString();
                Session["CustomerID"] = newCustomerId;
                DataTable dtNewCP = QueryUtils.GetCPPerCustomer(newCustomerId, this);
                ChangeCPListField.Items.Clear();
                ChangeCPItemTypeId.Value = newItemTypeId;
                if (dtNewCP != null && dtNewCP.Rows.Count != 0)
                {
                    foreach (DataRow dr in dtNewCP.Rows)
                    {
                        if (dr["ItemTypeID"].ToString() == newItemTypeId)
                            ChangeCPListField.Items.Add(dr["CustomerProgramName"].ToString());
                    }
                    ChangeCPListField.SelectedIndex = -1;
                    var cpList = (from DataRow row in dtNewCP.Rows select row).Where(m => m["ItemTypeID"].ToString() == newItemTypeId);
                    Session["NewCPs"] = cpList;
                }
            }
        }
        protected void ShowPicture(string dbPicture, bool old)
        {
            string errMsg;
            var ms = new MemoryStream();
            var fileType = "";
            var result = Utlities.GetPictureImageUrl(dbPicture, out ms, out errMsg, out fileType, this);
            //var result = Utlities.GetPictureImageUrl(dbPicture, out imgPath, out errMsg, this);
            if (result)
            {
                var ImageUrl = "data:image/" + fileType + ";base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                if (old)
                {
                    OldItemPicture.ImageUrl = ImageUrl;
                    OldItemPicture.Visible = true;
                }
                else
                {
                    NewItemPicture.ImageUrl = ImageUrl;
                    NewItemPicture.Visible = true;
                }
            }
            //-- Path to picture
            //Picture2PathField.Text = string.Format("<abbr title=\"{0}\">{1}</abbr>", dbPicture, "Picture");
            //ErrorPictureField.Text = errMsg;
        }
        //protected void OnCpChanged(object sender, EventArgs e)
        //{
        //    string newCP = CpListField.SelectedItem.Text;
        //    string customerId = (string)Session["CustomerID"];
        //    IEnumerable<DataRow> cps = (IEnumerable<DataRow>)Session["NewCPs"];
        //    foreach (var cp in cps)
        //    {
        //        if (cp["CustomerProgramName"].ToString() == CpListField.SelectedItem.Text)
        //        {
        //            NewCP.Text = "New CP: " + cp["CustomerProgramName"].ToString();
        //            NewCustomerIDBox.Text = cp["CustomerID"].ToString();
        //            NewCustomerStyleBox.Text = cp["CustomerStyle"].ToString();
        //            NewDescBox.Text = cp["Description"].ToString();
        //            NewCommentBox.Text = cp["Comment"].ToString();
        //            ShowPicture(cp["Path2Picture"].ToString(), false);
        //            break;
        //        }
        //    }
        //}
        //protected void OnCPCopyClick(object sender, EventArgs e)
        //{
        //    ItemizedChangedCPModel itemChangedCP = new ItemizedChangedCPModel();
        //    itemChangedCP.newOrder = NewOrder.Value;
        //    itemChangedCP.oldItem = OldItem.Value;
        //    itemChangedCP.newSku = NewCP.Text.Substring(8);
        //    itemChangedCP.oldSku = OldCP.Text.Substring(8);
        //    Session["BackToItemizing"] = itemChangedCP;
        //    ClientScript.RegisterStartupScript(this.GetType(), "Close", "window.close();", true);

        //}
        #endregion
        #region OldNew
        protected void OnOldNewLoadClick(object sender, EventArgs e)
        {
            var tmp = Request.UrlReferrer;
            OldNewNewOrder.Text = OrderField.Text;
                //ItemsAvailable.Value = (string)Request.QueryString["MaxItems"];
            OldNewNewOrder.Enabled = false;
            ItemsToAdd.Text = "";
            var orderModel = QueryUtils.GetOrderInfo(OldNewNewOrder.Text.Trim(), this);
            Session["OrderModel"] = orderModel;
            var memoList = QueryUtils.GetOrderMemos(orderModel, this);
            SetViewState(memoList, SessionConstants.ItemizeMemoList);
            MemoListField.DataTextField = "MemoNumber";
            MemoListField.DataValueField = "MemoId";
            MemoListField.DataSource = memoList;
            MemoListField.DataBind();
            OldItemsTree.Visible = false;
            OldItemsTreePanel.Visible = false;
            DragAndDropButtons.Visible = false;
            ItemField.Enabled = false;
            Session["SaveBatchList"] = null;
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = false;
            DragedItems.Items.Clear();
            DragedItems.Visible = false;
            ItemField.Focus();
        }
        protected void OnOldNewSaveClick(object sender, EventArgs e)
        {
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            if (OldItemsTree.CheckedNodes.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items Found!" + "\")</SCRIPT>");
                return;
            }
            if (MemoListField.SelectedItem.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Memo not found!" + "\")</SCRIPT>");
                return;
            }
            List<BatchNewModel> saveBatchList = new List<BatchNewModel>();
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = true;
            DragedItems.Items.Clear();
            DragedItems.Visible = false;
            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                List<string> oldItemsList = new List<string>();
                List<ItemizedModel> batchList = new List<ItemizedModel>();
                if (node.Text.Contains("Batch"))
                {
                    int i = 0;
                    string nodeId = node.Text;
                    int idx1 = nodeId.IndexOf('(');
                    int idx2 = nodeId.LastIndexOf(')');
                    nodeId = nodeId.Substring(idx1 + 1, idx2 - idx1 - 1);
                    string sku = nodeId;

                    foreach (TreeNode child in OldItemsTree.CheckedNodes)
                    {
                        if (child.Text.Contains("Item") && child.Parent.Text == node.Text)
                        {
                            int idx3 = child.Text.IndexOf('(');
                            int idx4 = child.Text.IndexOf(')');
                            string oldItemNew = child.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList.Add(oldItemNew);
                            i++;
                        }
                    }
                    ItemizedModel batch = new ItemizedModel();
                    batch.Items = i.ToString();
                    batch.Memo = MemoListField.SelectedItem.Text;
                    batch.Sku = sku;
                    batch.ItemsSet = "1";//j.ToString();
                    batchList.Add(batch);
                    OrderModel orderModel = null;
                    if (Session["OrderModel"] != null)
                        orderModel = (OrderModel)Session["OrderModel"];
                    else
                        orderModel = QueryUtils.GetOrderInfo(OldNewNewOrder.Text.Trim(), this);
                    var cpList = QueryUtils.GetOrderCp(orderModel, this);
                    var memoList = QueryUtils.GetOrderMemos(orderModel, this);
                    var cp = batch.Sku;
                    if (string.IsNullOrEmpty(cp))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Customer Program is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var cpModel = cpList.Find(m => m.CustomerProgramName == cp);
                    if (cpModel == null)
                    {
                        var cpListOld = QueryUtils.GetOldOrderCp(oldItemsList[0], this);
                        cpModel = cpListOld.Find(m => m.CustomerProgramName == cp);
                        if (cpModel == null)
                            return;
                    }
                    var memo = batch.Memo;
                    if (string.IsNullOrEmpty(memo))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var memoModel = memoList.Find(m => m.MemoNumber == memo);
                    if (memoModel == null) return;
                    List<string> oldItem = new List<string>();
                    var model = new ItemizeGoModel
                    {
                        Cp = cpModel,
                        Memo = memoModel,
                        NumberOfItems = Convert.ToInt16(batch.Items),
                        Order = orderModel,
                        LotNumbers = oldItemsList //new List<string>()
                    };
                    var newBatches = QueryUtils.ItemizeAddOldNewItemsByCount(model, this);
                    for (int y = 0; y <= newBatches.Count - 1; y++)
                    {
                        var batch1 = newBatches[y];
                        saveBatchList.Add(batch1);
                        int x = (y * 25);
                        int batchItems = Convert.ToInt16(batch1.ItemsAffected);
                        for (int z = 0; z <= batchItems - 1; z++)
                        {
                            var lot = model.LotNumbers[z + x];
                            foreach (TreeNode node1 in OldItemsTree.CheckedNodes)
                            {
                                if (node1.Text.Contains("Batch"))
                                {
                                    foreach (TreeNode childnode in node1.ChildNodes)
                                    {
                                        if (childnode.Text.Contains(lot))
                                        {
                                            string newItem = batch1.OrderCode + @"." + batch1.BatchCode + @"." + Utils.FillToTwoChars((z + 1).ToString());
                                            childnode.Text += @"(" + newItem + @")";
                                            foreach (string old in oldItemsList)
                                            {
                                                if (childnode.Text.Contains(old))
                                                {
                                                    lstMovedItems.Items.Add(old + @" -> " + newItem);
                                                    break;
                                                }
                                            }

                                            break;
                                        }
                                    }
                                }
                            }//node1
                        }//batchItems
                    }//newBatches
                }//node=Batch
            }//for each node
            Session["SaveBatchListOldNew"] = saveBatchList;
            Itemize1 itemize1 = new Itemize1();
            //bool printed = itemize1.PrintLabels(saveBatchList);
        }
        protected void OnOldNewSaveClickOld(object sender, EventArgs e)
        {
            SaveButton.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');");
            if (OldItemsTree.CheckedNodes.Count == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items Found!" + "\")</SCRIPT>");
                return;
            }
            if (MemoListField.SelectedItem.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "Memo not found!" + "\")</SCRIPT>");
                return;
            }
            List<BatchNewModel> saveBatchList = new List<BatchNewModel>();
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = true;
            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                List<string> oldItemsList = new List<string>();
                List<ItemizedModel> batchList = new List<ItemizedModel>();
                if (node.Text.Contains("Batch"))
                {
                    int i = 0;
                    string nodeId = node.Text;
                    int idx1 = nodeId.IndexOf('(');
                    int idx2 = nodeId.LastIndexOf(')');
                    nodeId = nodeId.Substring(idx1 + 1, idx2 - idx1 - 1);
                    string sku = nodeId;

                    foreach (TreeNode child in OldItemsTree.CheckedNodes)
                    {
                        if (child.Text.Contains("Item") && child.Parent.Text == node.Text)
                        {
                            int idx3 = child.Text.IndexOf('(');
                            int idx4 = child.Text.IndexOf(')');
                            string oldItemNew = child.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList.Add(oldItemNew);
                            i++;
                        }
                    }
                    ItemizedModel batch = new ItemizedModel();
                    batch.Items = i.ToString();
                    batch.Memo = MemoListField.SelectedItem.Text;
                    batch.Sku = sku;
                    batch.ItemsSet = "1";//j.ToString();
                    batchList.Add(batch);
                    OrderModel orderModel = null;
                    if (Session["OrderModel"] != null)
                        orderModel = (OrderModel)Session["OrderModel"];
                    else
                        orderModel = QueryUtils.GetOrderInfo(OldNewNewOrder.Text.Trim(), this);
                    var cpList = QueryUtils.GetOrderCp(orderModel, this);
                    var memoList = QueryUtils.GetOrderMemos(orderModel, this);
                    var cp = batch.Sku;
                    if (string.IsNullOrEmpty(cp))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Customer Program is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var cpModel = cpList.Find(m => m.CustomerProgramName == cp);
                    if (cpModel == null) return;
                    var memo = batch.Memo;
                    if (string.IsNullOrEmpty(memo))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "A Memo Number is required!" + "\")</SCRIPT>");
                        return;
                    }
                    var memoModel = memoList.Find(m => m.MemoNumber == memo);
                    if (memoModel == null) return;
                    List<string> oldItem = new List<string>();
                    var model = new ItemizeGoModel
                    {
                        Cp = cpModel,
                        Memo = memoModel,
                        NumberOfItems = Convert.ToInt16(batch.Items),
                        Order = orderModel,
                        LotNumbers = oldItemsList //new List<string>()
                    };
                    var newBatches = QueryUtils.ItemizeAddOldNewItemsByCount(model, this);
                    for (int y = 0; y <= newBatches.Count - 1; y++)
                    {
                        var batch1 = newBatches[y];
                        saveBatchList.Add(batch1);
                        int x = (y * 25);
                        int batchItems = Convert.ToInt16(batch1.ItemsAffected);
                        for (int z = 0; z <= batchItems - 1; z++)
                        {
                            var lot = model.LotNumbers[z + x];
                            foreach (TreeNode node1 in OldItemsTree.CheckedNodes)
                            {
                                if (node1.Text.Contains("Batch"))
                                {
                                    foreach (TreeNode childnode in node1.ChildNodes)
                                    {
                                        if (childnode.Text.Contains(lot))
                                        {
                                            string newItem = batch1.OrderCode + @"." + batch1.BatchCode + @"." + Utils.FillToTwoChars((z + 1).ToString());
                                            childnode.Text += @"(" + newItem + @")";
                                            foreach (string old in oldItemsList)
                                            {
                                                if (childnode.Text.Contains(old))
                                                {
                                                    lstMovedItems.Items.Add(old + @" -> " + newItem);
                                                    break;
                                                }
                                            }

                                            break;
                                        }
                                    }
                                }
                            }//node1
                        }//batchItems
                    }//newBatches
                }//node=Batch
            }//for each node
            Session["SaveBatchListOldNew"] = saveBatchList;
            Itemize1 itemize1 = new Itemize1();
            //bool printed = itemize1.PrintLabels(saveBatchList);
        }
        protected void OnItemCountClick(object sender, EventArgs e)
        {
            if (ItemsToAdd.Text == "")
            {
                ItemField.Enabled = false;
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Enter items number to add. " + "\")</SCRIPT>");
                return;
            }
            int added = Convert.ToInt16(ItemsToAdd.Text);
            int avail = Convert.ToInt16(ItemsAvailable.Value);
            if (added > avail)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                                "Available items: " + ItemsAvailable.Value + "\")</SCRIPT>");
                ItemsToAdd.Text = "";
            }
            ItemField.Enabled = true;
            ChangeCPPanel.Enabled = false;
        }
        protected void OnItemsCopyClick(object sender, EventArgs e)
        {
            OnItemsCopy();
        }
        protected void OnItemsCopy()
        {
            // LoadOfficesTree();
            //LoadOfficesTreeOld();
            //return;
            if (OrderListBox.Items.Count == 0)
                return;
            OldItemsTree.Visible = true;
            OldItemsTreePanel.Visible = true;
            DragAndDropButtons.Visible = true;
            OldItemsTree.Nodes.Clear();
            try
            {
                int i = 1;
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "" } };
                var list = OrderListBox.Items.Cast<ListItem>().OrderBy(item => item.Text).ToList();
                OrderListBox.Items.Clear();
                foreach (ListItem listItem in list)
                {
                    OrderListBox.Items.Add(listItem);
                }
                string prevordercode = null, prevbatchcode = null, previtemcode = null;
                string sku = null;
                foreach (var item in OrderListBox.Items)
                {
                    Utils.DissectItemNumber(item.ToString(), out string ordercode, out var batchcode, out var itemcode);

                    bool newTree = false, newBatch = false;
                    if (prevordercode == null)
                    {
                        prevordercode = ordercode;
                        prevbatchcode = batchcode;
                        previtemcode = itemcode;
                        newTree = true;
                    }
                    if ((ordercode != prevordercode) || (ordercode == prevordercode && prevbatchcode != batchcode))
                    {
                        newBatch = true;
                        prevordercode = ordercode;
                        prevbatchcode = batchcode;
                        previtemcode = itemcode;
                    }
                    if (newTree || newBatch)
                    {
                        DataRow dr = QueryUtils.GetNewCustomerProgramInstanceByBatchCode(Convert.ToInt32(ordercode), Convert.ToInt16(batchcode), Convert.ToInt16(itemcode), this);
                        if (dr != null)
                        {
                            sku = dr["CustomerProgramName"].ToString();
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + item.ToString() + "\")</SCRIPT>");
                            return;
                        }
                    }
                    if (newTree)
                    {
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                    if (newBatch)
                    {
                        i++;
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                    int orderNumber = OrderListBox.Items.Count;
                    data.Add(new TreeViewModel { ParentId = i.ToString(), Id = (orderNumber + i).ToString(), DisplayName = @"Item(" + Utils.FillToFiveChars(ordercode) + @"." + Utils.FillToThreeChars(batchcode, ordercode) + @"." + Utils.FillToTwoChars(itemcode) + @")" });
                }
                //data.Add(new TreeViewModel { ParentId = "1", Id = "2", DisplayName = itemcode });
                //string order = 
                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);
                OldItemsTree.Nodes.Add(rootNode);

                rootNode.ExpandAll();
                OldItemsTree.Enabled = true;
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + msg + "\")</SCRIPT>");
                return;
            }

        }

        private string GetCheckedItems()
        {
            //int i = 0;
            string item = null;
            string oldSku = null;
            //return null;

            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                if (node.Depth == 1)
                {
                    int skuStart = node.Text.IndexOf("(");
                    int skuStop = node.Text.IndexOf(")");
                    oldSku = node.Text.Substring(skuStart + 1, node.Text.Length - skuStart - 2);
                    continue;//-- 0 - all, 1 - country, 2 - office
                }
                else if (node.Depth == 2)
                {
                    item = node.Text;
                    return item + @"," + oldSku;
                    //i++;
                }
            }
            return null;
        }
        private List<string> GetListCheckedItems()
        {
            //int i = 0;
            List<string> result = new List<string>();
            string item = null;
            string oldSku = null;
            //return null;

            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                if (node.Depth == 1)
                {
                    int skuStart = node.Text.IndexOf("(");
                    int skuStop = node.Text.IndexOf(")");
                    oldSku = node.Text.Substring(skuStart + 1, node.Text.Length - skuStart - 2);
                    continue;//-- 0 - all, 1 - country, 2 - office
                }
                else if (node.Depth == 2)
                {
                    item = node.Text;
                    result.Add(item + @"," + oldSku);
                    //i++;
                }
            }
            return result;
        }

        protected void OnChangeCPButtonClick(object sender, EventArgs e)
        {
            string checkedItem = GetCheckedItems();
            if (checkedItem == null)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items found for \")</SCRIPT>");
                return;
            }
            string[] items = checkedItem.Split(',');
            string item = items[0];
            string oldSku = items[1];
            int idx1 = item.IndexOf('(');
            int idx2 = item.IndexOf(')');
            item = item.Substring(idx1 + 1, idx2 - idx1 - 1);
            ChangeCPOldItem.Value = item;
            //Response.Redirect("ItemizingChangeCP.aspx?Item=" + item + "&OrderCode=" + OldNewNewOrder.Text);
            LoadItemizingScreen(OldNewNewOrder.Text, item);
            TabContainer.ActiveTabIndex = 0;
            OldNewPanel.Enabled = false;
            ChangeCPPanel.Enabled = true;
            Session["OldTreeNodes"] = OldItemsTree.CheckedNodes;
        }//OnChangeCPButtonClick

        protected void OnPreloadButtonClick(object sender, EventArgs e)
        {
            if (Session["BackToItemizing"] == null)
                return;
            ItemizedChangedCPModel changeResults = new ItemizedChangedCPModel();
            changeResults = (ItemizedChangedCPModel)Session["BackToItemizing"];

            bool reloaded = ReloadTree(changeResults);

        }
        protected void OnPrintButtonClick(object sender, EventArgs e)
        {
            List<BatchNewModel> saveBatchList = new List<BatchNewModel>();
            saveBatchList = (List<BatchNewModel>)Session["SaveBatchListOldNew"];
            bool printed = PrintLabels(saveBatchList);
            if (!printed)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" +
                            "Error printing labels!" + "\")</SCRIPT>");
        }
        private bool ReloadTree(ItemizedChangedCPModel changeResults)
        {
            //string newOrder = changeResults.newOrder;
            //string oldItem = changeResults.oldItem;
            //string oldSku = changeResults.oldSku;
            //string newSku = changeResults.newSku;
            string newOrder = OldNewNewOrder.Text;
            //string oldItem = 
            //string oldSku = OldCP.Text;

            try
            {
                //OldItemsTree.Nodes.Add(new TreeNode(newSku));

                int count = 0, itemCount = 0;
                TreeNodeCollection oldTreeNodes = Session["OldTreeNodes"] as TreeNodeCollection;
                foreach (TreeNode node in OldItemsTree.Nodes)//find the number of batches
                {
                    count = node.ChildNodes.Count;
                }
                foreach (TreeNode node in oldTreeNodes)//find the number of items in selected batch
                {
                    if (node.Text.Contains("Batch") && !node.Text.Contains("New"))
                    {
                        itemCount = node.ChildNodes.Count;
                        break;
                    }
                    else if (node.Text.Contains("Item"))
                    {
                        itemCount++;
                    }
                }
                int z = 0;
                string[] oldItemsList = new string[itemCount];
                foreach (TreeNode node in oldTreeNodes)
                {
                    if (node.Text.Contains("Batch"))
                    {
                        foreach (TreeNode childnode in node.ChildNodes)
                        {
                            int idx3 = childnode.Text.IndexOf('(');
                            int idx4 = childnode.Text.IndexOf(')');
                            string item = childnode.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList[z] = item;
                            z++;
                        }
                    }
                    else if (node.Text.Contains("Item"))
                    {
                        int idx3 = node.Text.IndexOf('(');
                        int idx4 = node.Text.IndexOf(')');
                        string item = node.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                        if (!oldItemsList.Contains(item))
                            oldItemsList[z] = item;
                        z++;
                    }
                }
                List<string> newNodes = GetViewState("NewTreeNodes") as List<string>;
                int newBatchNumber = count + 1;
                if (newNodes.Count > 0)
                {
                    string last = newNodes.LastOrDefault<string>();
                    int idx3 = last.IndexOf('(');
                    string batchName = last.Substring(0, idx3);
                    var batchArr = batchName.Split(' ');
                    string lastBatchNumber = batchArr[batchArr.Length - 1];
                    newBatchNumber = Convert.ToInt32(lastBatchNumber) + 1;
                }
                TreeNode tn = new TreeNode("New Batch " + newBatchNumber.ToString() + @"(" + NewCP.Text + @")_" + changeResults.itemTypeId);
                tn.Text.Replace(@"color=""Black""", @"color=""Red""");
                
                tn.Expand();
                                
                newNodes.Add(tn.Text);
                SetViewState(newNodes, "NewTreeNodes");
                OldItemsTree.Nodes[0].ChildNodes.Add(tn);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
            return true;
        }
        private bool ReloadTreeOld(ItemizedChangedCPModel changeResults)
        {
            //string newOrder = changeResults.newOrder;
            //string oldItem = changeResults.oldItem;
            //string oldSku = changeResults.oldSku;
            //string newSku = changeResults.newSku;
            string newOrder = OldNewNewOrder.Text;
            //string oldItem = 
            //string oldSku = OldCP.Text;

            try
            {
                //OldItemsTree.Nodes.Add(new TreeNode(newSku));

                int count = 0, itemCount = 0;
                foreach (TreeNode node in OldItemsTree.Nodes)//find the number of batches
                {
                    count = node.ChildNodes.Count;
                }
                foreach (TreeNode node in OldItemsTree.CheckedNodes)//find the number of items in selected batch
                {
                    if (node.Text.Contains("Batch") && !node.Text.Contains("New"))
                    {
                        itemCount = node.ChildNodes.Count;
                        break;
                    }
                    else if (node.Text.Contains("Item"))
                    {
                        itemCount++;
                    }
                }
                int z = 0;
                string[] oldItemsList = new string[itemCount];
                foreach (TreeNode node in OldItemsTree.CheckedNodes)
                {
                    if (node.Text.Contains("Batch"))
                    {
                        foreach (TreeNode childnode in node.ChildNodes)
                        {
                            int idx3 = childnode.Text.IndexOf('(');
                            int idx4 = childnode.Text.IndexOf(')');
                            string item = childnode.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                            oldItemsList[z] = item;
                            z++;
                        }
                    }
                    else if (node.Text.Contains("Item"))
                    {
                        int idx3 = node.Text.IndexOf('(');
                        int idx4 = node.Text.IndexOf(')');
                        string item = node.Text.Substring(idx3 + 1, idx4 - idx3 - 1);
                        if (!oldItemsList.Contains(item))
                            oldItemsList[z] = item;
                        z++;
                    }
                }
                RemoveCheckedNodesOld();

                TreeNode tn = new TreeNode("New Batch " + (count + 1).ToString() + @"(" + NewCP.Text + @")");
                tn.Checked = true;
                tn.Text.Replace(@"color=""Black""", @"color=""Red""");
                for (int i = 1; i <= itemCount; i++)
                {
                    TreeNode child = new TreeNode(@"Item_" + i.ToString() + @"(" + oldItemsList[i - 1] + @")");
                    child.Checked = true;
                    tn.ChildNodes.Add(child);
                }
                tn.Expand();
                OldItemsTree.Nodes.Add(tn);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
            return true;

            /*
            if (node.Depth == 1)
                {
                    int skuStart = node.Text.IndexOf("(");
                    int skuStop = node.Text.IndexOf(")");
                    oldSkuTree = node.Text.Substring(skuStart + 1, node.Text.Length - skuStart - 2);
                    continue;//-- 0 - all, 1 - country, 2 - office
                }
                else if (node.Depth == 2)
                {
                    item = node.Text;
                    //return item + @"," + oldSku;
                    //i++;
                }
            }
            return true;
            */
        }
        private void RemoveCheckedNodes(TreeNodeCollection checkedNodes)
        {
            List<TreeNode> checkedItems = new List<TreeNode>();
            List<string> parentBatchList = new List<string>();
            //String parentBatch = "";
            for (int i = 0; i < checkedNodes.Count; i++)
            {
                TreeNode checkedNode = checkedNodes[i];
                if (checkedNode.Text.Contains("Batch"))
                {
                    //parentBatch = checkedNode.Text;
                    parentBatchList.Add(checkedNode.Text);
                }
                if (checkedNode.Text.Contains("Item"))
                {
                    checkedItems.Add(checkedNode);
                }
            }
            if (checkedItems.Count == 0)
                return;
            try
            {
                TreeNode rootNode = OldItemsTree.Nodes[0];
                if (rootNode == null)
                    return;
                foreach (TreeNode node in rootNode.ChildNodes)
                {
                    var match = parentBatchList.FirstOrDefault(x => x.Contains(node.Text));
                    if (match != null)
                    {
                        var childNodes = node.ChildNodes;
                        foreach (TreeNode checkedItem in checkedItems)
                        {
                            childNodes.Remove(checkedItem);
                        }
                        if (childNodes.Count == 0)
                            node.Checked = false;
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                
            }
            catch (Exception ex)
            {
                return;
            }

        }
        private void RemoveCheckedNodesOld()
        {
            foreach (TreeNode node in OldItemsTree.CheckedNodes)//uncheck old number
            {
                if (node != null && node.Text.Contains("Item"))
                {
                    var parent = node.Parent;
                    foreach (TreeNode child in parent.ChildNodes)
                    {
                        if (child.Text == node.Text)
                            parent.ChildNodes.Remove(node);
                        break;
                    }
                    node.Checked = false;
                }
                else
                    return;
                RemoveCheckedNodesOld();
                int count = OldItemsTree.CheckedNodes.Count;
                if (count == 0)
                    return;
                //node.SelectAction = TreeNodeSelectAction.None;
            }
        }
        protected void OnDeleteBatchButtonClick(object sender, EventArgs e)
        {
            for (int j = 2; j >= 0; j--)
            {
                var checkedNodes = OldItemsTree.CheckedNodes;
                int count = checkedNodes.Count;
                if (count == 0)
                    break;
                var abc = checkedNodes[0];
                for (int i = 0; i < count; i++)
                {
                    if (checkedNodes.Count == 0)
                        continue;
                    var node = checkedNodes[i];
                    if (node.Depth == j)
                    {
                        if (node.Parent != null)
                            node.Parent.ChildNodes.Remove(node);
                        else
                        {
                            checkedNodes.Clear();
                        }
                    }

                }
                count = checkedNodes.Count;
                if (count == 0)
                {
                    OldItemsTreePanel.Visible = false;
                    DragAndDropButtons.Visible = false;
                }
            }
        }//OnDeleteBatchButtonClick

        protected void OnItemAddClick(object sender, EventArgs e)
        {
            OldNewErrorMsg.Text = "";
            if (ItemsToAdd.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No items to add \")</SCRIPT>");
                return;
            }
            int itemAvailable = Convert.ToInt16(ItemsAvailable.Value);
            int itemsNew = Convert.ToInt16(ItemsToAdd.Text);
            if (itemAvailable == 0)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No available items \")</SCRIPT>");
                return;
            }
            var item = ItemField.Text.Trim();
            if (item.Length == 0)
                return;

            DataTable dt = QueryUtils.GetItemByCode(Utils.ParseOrderCode(item), Utils.ParseBatchCode(item), Utils.ParseItemCode(item), this);
            if (dt.Rows.Count == 0)
            {
                OldNewErrorMsg.Text = "No data found for " + item;
                return;
            }
            DataRow dr = dt.Rows[0];
            var prevGroupCode = Utils.FillToFiveChars(dr["PrevGroupCode"].ToString());
            var prevBatchCode = Utils.FillToThreeChars(dr["PrevBatchCode"].ToString(), prevGroupCode);
            var prevItemCode = Utils.FillToTwoChars(dr["PrevItemCode"].ToString());
            string oldItem = prevGroupCode + prevBatchCode + prevItemCode;
            //var itemList = new List<string>();
            //if (itemList.Contains(item)) return;
            var itemList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            if (itemList.Contains(oldItem)) return;
            itemList.Add(oldItem);
            SetViewState(itemList, SessionConstants.ItemizeLotList);

            OrderListBox.DataSource = itemList;
            OrderListBox.DataBind();
            OrderListBox.SelectedValue = oldItem;
            ItemField.Text = "";
            ItemField.Focus();
            ItemsAvailable.Value = (itemAvailable - 1).ToString();
            ItemsToAdd.Text = (itemsNew - 1).ToString();
            if ((itemsNew - 1) == 0)
                OnItemsCopy();
        }//OnItemAddClick

        protected void OnClearButtonClick(object sender, EventArgs e)
        {
            //ItemField.Text = "";
            ItemsToAdd.Text = "";
            ItemsAvailableBox.Text = "";
            ItemField.Text = "";
            ItemsAvailable.Value = "";
            OrderListBox.Items.Clear();
            OldItemsTree.Nodes.Clear();
            OldItemsTreePanel.Visible = false;
            DragAndDropButtons.Visible = false;
            ItemField.Enabled = false;
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = false;
            OldNewPanel.Enabled = false;
            ChangeCPPanel.Enabled = false;
            TabContainer.ActiveTabIndex = 2;
            OldNewErrorMsg.Text = "";
            PrintBox.Checked = false;
            SetViewState(new List<string>(), SessionConstants.ItemizeLotList);
            SetViewState(new List<string>(), SessionConstants.ItemizeDragedItems);
            SetViewState(new List<string>(), "NewTreeNodes");
        }
        protected void OnItemDelClick(object sender, EventArgs e)
        {
            var item = OrderListBox.SelectedValue;
            if (string.IsNullOrEmpty(item)) return;
            var itemList = GetViewState(SessionConstants.ItemizeLotList) as List<string> ?? new List<string>();
            var idxNext = OrderListBox.SelectedIndex;

            itemList.Remove(item);
            ItemsAvailable.Value = (Convert.ToInt16(ItemsAvailable.Value) + 1).ToString();
            ItemsToAdd.Text = (Convert.ToInt16(ItemsToAdd.Text) + 1).ToString();
            SetViewState(itemList, SessionConstants.ItemizeLotList);
            OrderListBox.DataSource = itemList;
            OrderListBox.DataBind();
            if (idxNext >= itemList.Count && itemList.Count > 0)
            {
                OrderListBox.SelectedIndex = 0;
            }
            if (idxNext < itemList.Count)
            {
                OrderListBox.SelectedIndex = idxNext;
            }
            if (OrderListBox.Items.Count == 0)
            {
                OldItemsTree.Visible = false;
                OldItemsTreePanel.Visible = false;
                DragAndDropButtons.Visible = false;
            }
            else
            {
                bool removed = RemoveTreeNode(OldItemsTree.Nodes, item);
                /*
                foreach (TreeNode node in OldItemsTree.Nodes)
                {
                    string nodeid = getNodeValue(node.Text);
                    if (nodeid == item)
                    {
                        OldItemsTree.Nodes.Remove(node);
                        break;
                    }
                }
                */
            }

        }

        private string getNodeValue(string nodeText)
        {
            try
            {
                if (nodeText.Length == 0)
                    return null;
                int idx1 = nodeText.IndexOf('(');
                int idx2 = nodeText.IndexOf(')');
                return nodeText.Substring(idx1 + 1, idx2 - idx1 - 1);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        private bool RemoveTreeNode(TreeNodeCollection itemsTreeNodes, string item)
        {
            foreach (TreeNode node in itemsTreeNodes)
            {
                string nodeid = getNodeValue(node.Text);
                if (nodeid != null)
                    nodeid = nodeid.Replace(".", "");

                if (nodeid == item)
                {
                    itemsTreeNodes.Remove(node);
                    return true;
                }
                else
                {
                    if (node.ChildNodes.Count > 0)
                    {
                        bool removed = RemoveTreeNode(node.ChildNodes, item);
                        if (removed && node.ChildNodes.Count > 0)
                            return true;
                        else//remove batch
                        {
                            itemsTreeNodes.Remove(node);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        #endregion

        protected void OnChangeCpCpChanged(object sender, EventArgs e)
        {
            string newCP = ChangeCPListField.SelectedItem.Text;
            string customerId = (string)Session["CustomerID"];
            IEnumerable<DataRow> cps = (IEnumerable<DataRow>)Session["NewCPs"];
            foreach (var cp in cps)
            {
                if (cp["CustomerProgramName"].ToString() == newCP)
                {
                    NewCP.Text = cp["CustomerProgramName"].ToString();
                    NewCustomerIDBox.Text = cp["CustomerID"].ToString();
                    NewCustomerStyleBox.Text = cp["CustomerStyle"].ToString();
                    NewDescBox.Text = cp["Description"].ToString();
                    NewCommentBox.Text = cp["Comment"].ToString();
                    ShowPicture(cp["Path2Picture"].ToString(), false);
                    break;
                }
            }
        }

        protected void OnItemAddClick(object sender, ImageClickEventArgs e)
        {
            string abc = "abc";
        }

        protected void OnItemsCopyClick(object sender, ImageClickEventArgs e)
        {
            OnItemsCopy();
        }
        protected void OnDragButtonClick(object sender, EventArgs e)
        {
            List<string> checkedItems = GetListCheckedItems();
            List<string> dragedItems = new List<string>();
            if (checkedItems == null)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No Checked Items found for \")</SCRIPT>");
                return;
            }
            if (checkedItems!=null && checkedItems.Count >0)
            {
                foreach (string chItem in checkedItems)
                {
                    string[] items = chItem.Split(',');
                    string item = items[0];
                    string oldSku = items[1];
                    int idx1 = item.IndexOf('(');
                    int idx2 = item.IndexOf(')');
                    item = item.Substring(idx1 + 1, idx2 - idx1 - 1);
                    dragedItems.Add(item);                    
                }  
            }
            SetViewState(dragedItems, SessionConstants.ItemizeDragedItems);
            RemoveCheckedNodes(OldItemsTree.CheckedNodes);
            Session["OldTreeNodes"] = OldItemsTree.CheckedNodes;
            //BuildTreeWithoutDraged(dragedItems);
            DragedItems.Items.Clear();
            DragedItems.Visible = true;
            DragedItems.DataSource = dragedItems;
            DragedItems.DataBind();

        }
        protected void OnDropButtonClick(object sender, EventArgs e)
        {
            List<string> dragedItems = GetViewState(SessionConstants.ItemizeDragedItems) as List<string>;

            TreeNode dropTo = null;
            foreach (TreeNode node in OldItemsTree.CheckedNodes)
            {
                if (node.Text.Contains("Batch"))
                {
                    dropTo = node;
                }
            }
            
            if (!string.IsNullOrEmpty(dropTo.Text))
            {
                string itemTypeIdNew = dropTo.Text.Split('_').LastOrDefault();
                foreach (string item in dragedItems)
                {
                    string itemTypeID = null;
                    DataTable dtOldBatch = QueryUtils.GetOldCustomerProgramByBatchCode(item, this);
                    if (dtOldBatch != null && dtOldBatch.Rows.Count != 0)
                    {
                        DataRow drOldBatch = dtOldBatch.Rows[0];
                        itemTypeID = drOldBatch["ItemTypeID"].ToString();
                    }
                    if (!itemTypeIdNew.Equals(itemTypeID))
                    {
                        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "You can not drop dragged items to " + dropTo.Text + "\")</SCRIPT>");
                        return;
                    }
                }
                BuildTreeWithDroped(dragedItems, dropTo);
                dropTo.Checked = false;
                DragedItems.Items.Clear();
                DragedItems.Visible = false;
            }
        }
        protected void OnRollbackButtonClick(object sender, EventArgs e)
        {
            SetViewState(new List<string>(), "NewTreeNodes");
            SetViewState(new List<string>(), SessionConstants.ItemizeDragedItems);
            BuildTreeWithoutDraged(new List<string>());
            DragedItems.Items.Clear();
            DragedItems.Visible = false;
            lstMovedItems.Items.Clear();
            lstMovedItems.Visible = false;
        }

        protected void BuildTreeWithoutDraged(List<String> dragedItems)
        {
            OldItemsTree.Nodes.Clear();
            try
            {
                int i = 1;
                var data = new List<TreeViewModel> { new TreeViewModel { ParentId = "", Id = "0", DisplayName = "" } };
                var list = OrderListBox.Items.Cast<ListItem>().OrderBy(item => item.Text).ToList();
                OrderListBox.Items.Clear();
                foreach (ListItem listItem in list)
                {
                    OrderListBox.Items.Add(listItem);
                }
                string prevordercode = null, prevbatchcode = null, previtemcode = null;
                string sku = null;
                foreach (var item in OrderListBox.Items)
                {
                    Utils.DissectItemNumber(item.ToString(), out string ordercode, out var batchcode, out var itemcode);

                    bool newTree = false, newBatch = false;
                    if (prevordercode == null)
                    {
                        prevordercode = ordercode;
                        prevbatchcode = batchcode;
                        previtemcode = itemcode;
                        newTree = true;
                    }
                    if ((ordercode != prevordercode) || (ordercode == prevordercode && prevbatchcode != batchcode))
                    {
                        newBatch = true;
                        prevordercode = ordercode;
                        prevbatchcode = batchcode;
                        previtemcode = itemcode;
                    }
                    if (newTree || newBatch)
                    {
                        DataRow dr = QueryUtils.GetNewCustomerProgramInstanceByBatchCode(Convert.ToInt32(ordercode), Convert.ToInt16(batchcode), Convert.ToInt16(itemcode), this);
                        if (dr != null)
                        {
                            sku = dr["CustomerProgramName"].ToString();
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + item.ToString() + "\")</SCRIPT>");
                            return;
                        }
                    }
                    if (newTree)
                    {
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                    if (newBatch)
                    {
                        i++;
                        data.Add(new TreeViewModel { ParentId = "0", Id = i.ToString(), DisplayName = "Batch" + Utils.FillToThreeChars(i.ToString(), ordercode) + @"(" + sku + @")" });
                    }
                    int orderNumber = OrderListBox.Items.Count;
                    string DisplayItemNumber = Utils.FillToFiveChars(ordercode) + @"." + Utils.FillToThreeChars(batchcode, ordercode) + @"." + Utils.FillToTwoChars(itemcode);
                    if (dragedItems.IndexOf(DisplayItemNumber) == -1)
                        data.Add(new TreeViewModel { ParentId = i.ToString(), Id = (orderNumber + i).ToString(), DisplayName = @"Item(" + DisplayItemNumber + @")" });
                }

                var root = TreeUtils.GetRootTreeModel(data);
                var rootNode = new TreeNode(root.DisplayName, root.Id);

                TreeUtils.FillNode(rootNode, root, false, TreeNodeSelectAction.Select);

                List<string> newNodes = GetViewState("NewTreeNodes") as List<string>;
                if (newNodes.Count > 0)
                {
                    foreach (string newBatch in newNodes)
                    {
                        TreeNode tn = new TreeNode(newBatch);
                        rootNode.ChildNodes.Add(tn);
                    }
                }
                OldItemsTree.Nodes.Add(rootNode);
                rootNode.ExpandAll();
                OldItemsTree.Enabled = true;
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + msg + "\")</SCRIPT>");
                return;
            }

        }
        protected void BuildTreeWithDroped(List<String> dragedItems, TreeNode dropTo)
        {
            try
            {
                int i = 0;                
                var idx = 1;
                int orderNumber = OrderListBox.Items.Count;
                List<string> newNodes = GetViewState("NewTreeNodes") as List<string>;
                int j = 1;
                if (newNodes.Count > 0)
                {
                    foreach (string newBatch in newNodes)
                    {
                        if (newBatch.Equals(dropTo.Text))
                            idx = orderNumber + j;
                        j++;
                    }
                }
                foreach (string dragedItem in dragedItems)
                {
                    TreeNode child = new TreeNode(@"Item_" + (idx+i).ToString() + @"(" + dragedItem + @")");
                    dropTo.ChildNodes.Add(child);
                    i++;
                }
                
                OldItemsTree.Enabled = true;
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"" + "No SKU found for " + msg + "\")</SCRIPT>");
                return;
            }

        }
    }
}