﻿using System;
using System.IO;
using Corpt.Models.AccountRepresentative.UI;
using Corpt.Utilities.BulkBilling;
using Corpt.Utilities.BulkBilling.CostManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace CorptTests.Tests
{

    [TestClass]
    public class InvoiceItemDescriptionTests
    {
        PricesGridViewItemModel _pricesGridViewItemModel;
        string _customerName;
        string _serviceTypeName;
        int _groupCode;

        [TestMethod]
        public void DefaultCase_Screening()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = 38,
                RetailerName = "Amazon",
                IsSatisfyACondition = false,
                YesAnswerText = null,
                NoAnswerText = null,
                SynthServiceTypeName = null,
                ServiceTypeId = 7
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "Screening";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, Screening, 12 pieces", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces - 5 stones", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, Screening, 12 pieces", description.GetDescription());
        }

        [TestMethod]
        public void DefaultCase_QC()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = 38,
                RetailerName = "Amazon",
                IsSatisfyACondition = false,
                YesAnswerText = null,
                NoAnswerText = null,
                SynthServiceTypeName = null,
                ServiceTypeId = 12
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "QC";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, QC, 12 pieces", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, QC, 2 pieces", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, QC, 2 pieces", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, QC, 12 pieces", description.GetDescription());
        }

        [TestMethod]
        public void Helzberg()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = InvoiceItemDescription.HelzbergRetailerId,
                RetailerName = "Helzberg",
                IsSatisfyACondition = false,
                YesAnswerText = null,
                NoAnswerText = null,
                SynthServiceTypeName = null,
                ServiceTypeId = 7
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "Screening";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, Screening, 12 pieces, HDS Gemstone and Diamond", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces - 5 stones, HDS Gemstone and Diamond", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces, HDS Gemstone and Diamond", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, Screening, 12 pieces, HDS Gemstone and Diamond", description.GetDescription());
        }

        [TestMethod]
        public void BlueNileColor()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = InvoiceItemDescription.BlueNileRetailerId,
                RetailerName = "Blue Nile",
                IsSatisfyACondition = true,
                YesAnswerText = "Color",
                NoAnswerText = "White",
                SynthServiceTypeName = null,
                ServiceTypeId = 7
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "Screening";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, Screening, 12 pieces, BLUE NILE COLOR", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces - 5 stones, BLUE NILE COLOR", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces, BLUE NILE COLOR", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, Screening, 12 pieces, BLUE NILE COLOR", description.GetDescription());

        }


        [TestMethod]
        public void BlueNileWhite()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = InvoiceItemDescription.BlueNileRetailerId,
                RetailerName = "Blue Nile",
                IsSatisfyACondition = false,
                YesAnswerText = "Color",
                NoAnswerText = "White",
                SynthServiceTypeName = null,
                ServiceTypeId = 7
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "Screening";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, Screening, 12 pieces, BLUE NILE WHITE", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces - 5 stones, BLUE NILE WHITE", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces, BLUE NILE WHITE", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, Screening, 12 pieces, BLUE NILE WHITE", description.GetDescription());

        }

        [TestMethod]
        public void MarksJewelers()
        {
            _pricesGridViewItemModel = new PricesGridViewItemModel()
            {
                RetailerId = InvoiceItemDescription.MarksJewelersRetailerId,
                RetailerName = "Marks Jewelers",
                IsSatisfyACondition = false,
                YesAnswerText = null,
                NoAnswerText = null,
                SynthServiceTypeName = null,
                ServiceTypeId = 7
            };
            _customerName = "NY Customer Name";
            _serviceTypeName = "Screening";
            _groupCode = 3012712;

            // Pass
            var quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);

            var description = new InvoiceItemDescription("Pass", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);
            Assert.AreEqual("Order 3012712, Screening, 12 pieces, NY Customer Name", description.GetDescription());

            //Reject with Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 5, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces - 5 stones, NY Customer Name", description.GetDescription());

            //Reject with no Stones
            quantitiesModel = new RegularQuantitiesModel(10, 2, 10, 2, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Reject", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Rejection, Order 3012712, Screening, 2 pieces, NY Customer Name", description.GetDescription());

            //Repack
            _pricesGridViewItemModel.SynthServiceTypeName = "Re Seal";
            quantitiesModel = new RegularQuantitiesModel(12, 0, 12, 0, 0, 12, QuantitySource.Gd2K);
            description = new InvoiceItemDescription("Repack", _pricesGridViewItemModel,
                _pricesGridViewItemModel.ServiceTypeId, _serviceTypeName, _groupCode, _customerName, quantitiesModel);

            Assert.AreEqual("Re Seal, Order 3012712, Screening, 12 pieces, NY Customer Name", description.GetDescription());

        }

    }
}