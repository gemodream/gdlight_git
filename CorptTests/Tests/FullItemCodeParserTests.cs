﻿using System;
using Corpt;
using Corpt.FullItemCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CorptTests.Tests
{

    [TestClass]
    public class FullItemCodeParserTests
    {
        /// <summary>
        /// Parses the order code (full or not).
        /// With assumption that OrderCode LE 10,000,000
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="version">version</param>
        /// <param name="isBigBatchCode"></param>
        /// <param name="itemNumber">The item number.</param>
        /// <param name="expectedOrderCode">expected OrderCode</param>
        /// <param name="expectedBatchCode">expected BatchCode</param>
        /// <param name="expectedItemCode">expected ItemCode</param>
        [DataTestMethod]
        [DataRow(101, "old", false, "12345", 12345, 0, 0)]
        [DataRow(102, "old", false, "123456", 123456, 0, 0)]
        [DataRow(103, "new", false, "1234567", 1234567, 0, 0)]
        [DataRow(104, "old", true,  "12345001", 12345, 001, 0)] //12345678 = 12345 678 - old
        [DataRow(105, "old", false, "12345100", 12345, 100, 0)] //12345100 = 12345 100, batch code 100 > 99 - when 12345 is NOT order code with big batch code, but to keep compatibility
        [DataRow(106, "old", true,  "35598100", 35598, 100, 0)] //35598100 = 35598 100 - when 35598 is order code with big batch code
        [DataRow(107, "old", false, "123456089", 123456, 089, 0)] //123456089 = 123456 089 - old
        [DataRow(108, "new", false, "123456789", 1234567, 89, 0)] //123456789 = 1234567 89 - new
        [DataRow(109, "new", true, "158472187", 158472, 187, 0)] //158472187 = 158472 187 - old  (35688 is order code with big batch code) 
        [DataRow(110, "new", false, "1234567890", 12345, 678, 90)] //1234567890 = 12345 678 90 - old, expected=0 because batch code 678 > 99 - so this can be violation of a convention, ok let it be so
                                                                   //1234567890 = 123456 789 0 - old, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 89 0 - new, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 890 - new, expected=0 because batch code should has only 2 symbols for "new" (7-chars) code.
        [DataRow(111, "old", false, "1234507890", 12345, 078, 90)] //1234507890 = 12345 078 90 - old - so, 10-chars returns first 5 chars only if 6th char = 0, otherwise - 0.
        [DataRow(112, "old", true, "3568814301", 35688, 143, 01)] //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
        [DataRow(113, "new", false, "12345678901", 1234567, 89, 01)] //12345678901 = 1234567 89 01 - new
        [DataRow(114, "old", false, "12345608901", 123456, 089, 01)] //12345608901 = 123456 089 01 - old
        [DataRow(115, "old", true, "15847211101", 158472, 111, 01)] //15847211101 = 158472 111 01 - old (158472 is order code with big batch code) 
        [DataRow(116, "old", false, "1234", 0, 0, 0)]
        
        public void FullItemCodeParser(int id, string version, bool isBigBatchCode,
            string itemNumber, 
            int expectedOrderCode,
            int expectedBatchCode = 0,
            int expectedItemCode = 0)
        {

            var parser = new FullItemCodeParser(itemNumber, s => isBigBatchCode).Parse();

            Assert.AreEqual(expectedOrderCode, parser.OrderCode);
            Assert.AreEqual(expectedBatchCode, parser.BatchCode);
            Assert.AreEqual(expectedItemCode, parser.ItemCode);
        }

        [DataTestMethod]
        [DataRow(101, "old", false, "12345", "", false)]
        [DataRow(102, "old", false, "123456", "", false)]
        [DataRow(103, "new", false, "1234567", "", false)]
        [DataRow(104, "old", true, "12345001", "", false)] //12345678 = 12345 678 - old
        [DataRow(105, "old", false, "12345100", "", false)] //12345100 = 12345 100, batch code 100 > 99 - when 12345 is NOT order code with big batch code, but to keep compatibility
        [DataRow(106, "old", true, "35598100", "", false)] //35598100 = 35598 100 - when 35598 is order code with big batch code
        [DataRow(107, "old", false, "123456089", "", false)] //123456089 = 123456 089 - old
        [DataRow(108, "new", false, "123456789", "", false)] //123456789 = 1234567 89 - new
        [DataRow(109, "new", true, "158472187", "", false)] //158472187 = 158472 187 - old  (35688 is order code with big batch code) 
        [DataRow(110, "new", false, "1234567890", "12345.678.90", false)] //1234567890 = 12345 678 90 - old, expected=0 because batch code 678 > 99
                                                              //1234567890 = 123456 789 0 - old, expected=0 because item code has only one symbol 
                                                              //1234567890 = 1234567 89 0 - new, expected=0 because item code has only one symbol 
                                                              //1234567890 = 1234567 890 - new, expected=0 because batch code should has only 2 symbols for "new" (7-chars) code.
        [DataRow(111, "old", false, "1234507890", "12345.078.90", false)] //1234507890 = 12345 078 90 - old - so, 10-chars returns first 5 chars only if 6th char = 0, otherwise - 0.
        [DataRow(112, "old", true, "3568814301", "35688.143.01", false)] //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
        [DataRow(113, "new", false, "12345678901", "1234567.89.01", false)] //12345678901 = 1234567 89 01 - new
        [DataRow(114, "old", false, "12345608901", "123456.089.01", false)] //12345608901 = 123456 089 01 - old
        [DataRow(115, "old", true, "15847211101", "158472.111.01", false)] //15847211101 = 158472 111 01 - old (158472 is order code with big batch code) 
        [DataRow(116, "old", false, "1234", "", false)]
        [DataRow(117, "old", true, "158xxx11101", "?", true)] 
        public void GetItemNumberWithDots(int id, string version, bool isBigBatchCode,
                                                      string itemNumber,
                                                      string itemNumberWithDotExpected,
                                                      bool isException = false)
        {
            void Action() => Assert.AreEqual(itemNumberWithDotExpected, Utils.GetItemNumberWithDots(itemNumber, s => isBigBatchCode));

            if (isException)
            {
                Assert.ThrowsException<FormatException>((Action)Action);
            }
            else
            {
                Action();
            }
        }

        [DataTestMethod]
        [DataRow(101, "old", false, "12345", "1234500000", false)]
        [DataRow(102, "old", false, "123456", "12345600000", false)]
        [DataRow(103, "new", false, "1234567", "12345670000", false)]
        [DataRow(104, "old", true, "12345001", "1234500100", false)] //12345678 = 12345 678 - old
        [DataRow(105, "old", false, "12345100", "1234510000", false)] //12345100 = 12345 100, batch code 100 > 99 - when 12345 is NOT order code with big batch code, but to keep compatibility
        [DataRow(106, "old", true, "35598100", "3559810000", false)] //35598100 = 35598 100 - when 35598 is order code with big batch code
        [DataRow(107, "old", false, "123456089", "12345608900", false)] //123456089 = 123456 089 - old
        [DataRow(108, "new", false, "123456789", "12345678900", false)] //123456789 = 1234567 89 - new
        [DataRow(109, "new", true, "158472187", "15847218700", false)] //158472187 = 158472 187 - old  (35688 is order code with big batch code) 
        [DataRow(110, "new", false, "1234567890", "1234567890", false)] //1234567890 = 12345 678 90 - old, expected=0 because batch code 678 > 99
                                                                   //1234567890 = 123456 789 0 - old, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 89 0 - new, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 890 - new, expected=0 because batch code should has only 2 symbols for "new" (7-chars) code.
        [DataRow(111, "old", false, "1234507890", "1234507890", false)] //1234507890 = 12345 078 90 - old - so, 10-chars returns first 5 chars only if 6th char = 0, otherwise - 0.
        [DataRow(112, "old", true, "3568814301", "3568814301", false)] //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
        [DataRow(113, "new", false, "12345678901", "12345678901", false)] //12345678901 = 1234567 89 01 - new
        [DataRow(114, "old", false, "12345608901", "12345608901", false)] //12345608901 = 123456 089 01 - old
        [DataRow(115, "old", true, "15847211101", "15847211101", false)] //15847211101 = 158472 111 01 - old (158472 is order code with big batch code) 
        [DataRow(116, "old", false, "1234", "0000000000", false)]
        [DataRow(117, "old", false, "158xxx11101", "?", true)]
        public void GetItemNumberWithNoDots(int id, string version, bool isBigBatchCode,
                                                      string itemNumber,
                                                      string itemNumberWithDotExpected,
                                                      bool isException = false )
        {
            void Action() => Assert.AreEqual(itemNumberWithDotExpected, Utils.GetItemNumberWithNoDots(itemNumber, s => isBigBatchCode));

            if (isException)
            {
                Assert.ThrowsException<FormatException>((Action)Action);
            }
            else
            {
                Action();
            }

        }

        [DataTestMethod]
        [DataRow(101, "old", false, "12345", 12345, 0, 0, true)]
        [DataRow(102, "old", false, "123456", 123456, 0, 0, true)]
        [DataRow(103, "new", false, "1234567", 1234567, 0, 0, true)]
        [DataRow(104, "old", true, "12345001", 12345, 001, 0, true)] //12345678 = 12345 678 - old
        [DataRow(105, "old", false, "12345100", 12345, 100, 0, true)] //12345100 = 12345 100, batch code 100 > 99 - when 12345 is NOT order code with big batch code, but to keep compatibility
        [DataRow(106, "old", true, "35598100", 35598, 100, 0, true)] //35598100 = 35598 100 - when 35598 is order code with big batch code
        [DataRow(107, "old", false, "123456089", 123456, 089, 0, true)] //123456089 = 123456 089 - old
        [DataRow(108, "new", false, "123456789", 1234567, 89, 0, true)] //123456789 = 1234567 89 - new
        [DataRow(109, "new", true, "158472187", 158472, 187, 0, true)] //158472187 = 158472 187 - old  (35688 is order code with big batch code) 
        [DataRow(110, "new", false, "1234567890", 12345, 678, 90, true)] //1234567890 = 12345 678 90 - old, expected=0 because batch code 678 > 99 - so this can be violation of a convention, ok let it be so
                                                                   //1234567890 = 123456 789 0 - old, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 89 0 - new, expected=0 because item code has only one symbol 
                                                                   //1234567890 = 1234567 890 - new, expected=0 because batch code should has only 2 symbols for "new" (7-chars) code.
        [DataRow(111, "old", false, "1234507890", 12345, 078, 90, true)] //1234507890 = 12345 078 90 - old - so, 10-chars returns first 5 chars only if 6th char = 0, otherwise - 0.
        [DataRow(112, "old", true, "3568814301", 35688, 143, 01, true)] //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
        [DataRow(113, "new", false, "12345678901", 1234567, 89, 01, true)] //12345678901 = 1234567 89 01 - new
        [DataRow(114, "old", false, "12345608901", 123456, 089, 01, true)] //12345608901 = 123456 089 01 - old
        [DataRow(115, "old", true, "15847211101", 158472, 111, 01, true)] //15847211101 = 158472 111 01 - old (158472 is order code with big batch code) 
        [DataRow(116, "old", false, "1234", 0, 0, 0, false)]
        [DataRow(117, "err", false, @"ERROR", 0, 0, 0, false)]

        public void DissectItemNumber(int id, string version, bool isBigBatchCode,
            string itemNumber,
            int expectedOrderCode,
            int expectedBatchCode = 0,
            int expectedItemCode = 0,
            bool isSuccessExpected = true)
        {
            Assert.AreEqual(isSuccessExpected, 
                Utils.DissectItemNumber(
                    itemNumber, 
                    out int orderCode, 
                    out int batchCode, 
                    out int itemCode,
                    s => isBigBatchCode));

            Assert.AreEqual(expectedOrderCode, orderCode);
            Assert.AreEqual(expectedBatchCode, batchCode);
            Assert.AreEqual(expectedItemCode,  itemCode);
        }

        [DataTestMethod]
        [DataRow(101, "old", false, "12345", "12345", "000", "00", true)]
        [DataRow(102, "old", false, "123456", "123456", "000", "00", true)]
        [DataRow(103, "new", false, "1234567", "1234567", "00", "00", true)]
        [DataRow(104, "old", true, "12345001", "12345", "001", "00", true)] //12345678 = 12345 678 - old
        [DataRow(105, "old", false, "12345100", "12345", "100", "00", true)] //12345100 = 12345 100, batch code 100 > 99 - when 12345 is NOT order code with big batch code, but to keep compatibility
        [DataRow(106, "old", true, "35598100", "35598", "100", "00", true)] //35598100 = 35598 100 - when 35598 is order code with big batch code
        [DataRow(107, "old", false, "123456089", "123456", "089", "00", true)] //123456089 = 123456 089 - old
        [DataRow(108, "new", false, "123456789", "1234567", "89", "00", true)] //123456789 = 1234567 89 - new
        [DataRow(109, "new", true, "158472187", "158472", "187", "00", true)] //158472187 = 158472 187 - old  (35688 is order code with big batch code) 
        [DataRow(110, "new", false, "1234567890", "12345", "678", "90", true)] //1234567890 = 12345 678 90 - old, expected=0 because batch code 678 > 99 - so this can be violation of a convention, ok let it be so
        //1234567890 = 123456 789 0 - old, expected=0 because item code has only one symbol 
        //1234567890 = 1234567 89 0 - new, expected=0 because item code has only one symbol 
        //1234567890 = 1234567 890 - new, expected=0 because batch code should has only 2 symbols for "new" (7-chars) code.
        [DataRow(111, "old", false, "1234507890", "12345", "078", "90", true)] //1234507890 = 12345 078 90 - old - so, 10-chars returns first 5 chars only if 6th char = 0, otherwise - 0.
        [DataRow(112, "old", true, "3568814301", "35688", "143", "01", true)] //3568867890 = 35688 678 90 - old (35688 is order code with big batch code) 
        [DataRow(113, "new", false, "12345678901", "1234567", "89", "01", true)] //12345678901 = 1234567 89 01 - new
        [DataRow(114, "old", false, "12345608901", "123456", "089", "01", true)] //12345608901 = 123456 089 01 - old
        [DataRow(115, "old", true, "15847211101", "158472", "111", "01", true)] //15847211101 = 158472 111 01 - old (158472 is order code with big batch code) 
        [DataRow(116, "old", false, "1234", "00000", "000", "00", false)]
        [DataRow(117, "err", false, @"ERROR", "00000", "000", "00", false)]

        public void DissectItemNumber_ToString(
            int id, 
            string version, 
            bool isBigBatchCode,
            string itemNumber,
            string expectedOrderCode,
            string expectedBatchCode,
            string expectedItemCode = "00",
            bool isSuccessExpected = true)
        {
            Assert.AreEqual(isSuccessExpected,
                Utils.DissectItemNumber(
                    itemNumber,
                    out string orderCode,
                    out string batchCode,
                    out string itemCode,
                    s => isBigBatchCode));

            Assert.AreEqual(expectedOrderCode, orderCode);
            Assert.AreEqual(expectedBatchCode, batchCode);
            Assert.AreEqual(expectedItemCode, itemCode);
        }

        [DataTestMethod]
        [DataRow(101, "1", "00001")]
        [DataRow(102, "12", "00012")]
        [DataRow(103, "123", "00123")]
        [DataRow(104, "1234", "01234")]
        [DataRow(105, "12345", "12345")]
        [DataRow(106, "123456", "123456")]
        [DataRow(107, "1234567", "1234567")]
        [DataRow(108, "12345678", "12345678")]
        [DataRow(109, "xxx", "00xxx")]
        public void FillToFiveChars(int id, string source, string expected)
        {
            Assert.AreEqual(expected, Utils.FillToFiveChars(source));
        }

        [DataTestMethod]
        [DataRow(101, "1", "1", "001")]
        [DataRow(102, "12", "1", "001")]
        [DataRow(103, "123", "1", "001")]
        [DataRow(104, "1234", "1", "001")]
        [DataRow(105, "12345", "1", "001")]
        [DataRow(106, "123456", "1", "001")]
        [DataRow(107, "1234567", "1", "01")]
        // ReSharper disable once StringLiteralTypo
        [DataRow(108, "XXXXXXX", "1", "01")]
        // ReSharper disable once StringLiteralTypo
        [DataRow(108, "XXXXXXX", "Y", "0Y")]
        public void FillToThreeChars(int id, string order, string batch, string expected)
        {
            Assert.AreEqual(expected, Utils.FillToThreeChars(batch, order));
        }

        [DataTestMethod]
        [DataRow(101, "1", "01")]
        [DataRow(102, "12", "12")]
        [DataRow(103, "123", "123")]
        [DataRow(104, "X", "0X")]
        public void FillToTwoChars(int id, string item, string expected)
        {
            Assert.AreEqual(expected, Utils.FillToTwoChars(item));
        }

        [DataTestMethod]
        [DataRow(101, "12345", "1", "1", "1234500101")]
        [DataRow(101, "123456", "1", "1", "12345600101")]
        [DataRow(101, "1234567", "1", "1", "12345670101")]
        [DataRow(101, "12xxx67", "1", "1", "-1")]
        public void FullItemNumber(int id, string batchCode, string orderCode, string itemCode, string expected)
        {
            Assert.AreEqual(expected, Utils.FullItemNumber(batchCode, orderCode, itemCode));
        }
        //public static String FullBatchNumber(String groupCode, String batchCode)

        [DataTestMethod]
        [DataRow(101, "1", "12345", "12345001")]
        [DataRow(102, "1", "123456",  "123456001")]
        [DataRow(103, "1", "1234567", "123456701")]
        [DataRow(104, "1", "12xxx67", "-1")]
        public void FullBatchNumber(int id, string batchCode, string orderCode, string expected)
        {
            Assert.AreEqual(expected, Utils.FullBatchNumber(orderCode, batchCode));
        }

        [DataTestMethod]
        [DataRow(101, "1234500101", 12345, false)]
        [DataRow(102, "12345600101", 123456, false)]
        [DataRow(103, "123456700101", 0, false)]
        [DataRow(104, "12345670101", 1234567, false)]
        [DataRow(105, "123xxx70101", 0, true)]
        public void ParseOrderCode(int id, string fullItemCode, int expected, bool isException = false)
        {
            void Action() => Assert.AreEqual(expected, Utils.ParseOrderCode(fullItemCode, s => false));

            if (isException)
            {
                Assert.ThrowsException<FormatException>((Action)Action);
            }
            else
            {
                Action();
            }
        }

        [DataTestMethod]
        [DataRow(101, "1234500101", 1, false)]
        [DataRow(102, "12345600101", 1, false)]
        [DataRow(103, "123456700101", 0, false)]
        [DataRow(104, "12345670101", 1, false)]
        [DataRow(105, "123xxx70101", 0, true)]
        public void ParseBatchCode(int id, string fullItemCode, int expected, bool isException = false)
        {
            void Action() => Assert.AreEqual(expected, Utils.ParseBatchCode(fullItemCode, s => false));

            if (isException)
            {
                Assert.ThrowsException<FormatException>((Action)Action);
            }
            else
            {
                Action();
            }
        }

        [DataTestMethod]
        [DataRow(101, "1234500101", 1, false)]
        [DataRow(102, "12345600101", 1, false)]
        [DataRow(103, "123456700101", 0, false)]
        [DataRow(104, "12345670101", 1, false)]
        [DataRow(105, "123xxx70101", 0, true)]
        public void ParseItemCode(int id, string fullItemCode, int expected, bool isException = false)
        {
            void Action() => Assert.AreEqual(expected, Utils.ParseItemCode(fullItemCode, s => false));

            if (isException)
            {
                Assert.ThrowsException<FormatException>((Action)Action);
            }
            else
            {
                Action();
            }
        }

    }
}