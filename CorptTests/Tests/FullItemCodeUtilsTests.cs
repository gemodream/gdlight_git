﻿using Corpt.FullItemCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CorptTests.Tests
{
    [TestClass]
    public class FullItemCodeUtilsTests
    {

        [DataTestMethod]
        [DataRow(101, 895119, false)]
        [DataRow(102, 895118, true)]
        [DataRow(103, 123456, false)]
        [DataRow(104, 5314, true)]
        [DataRow(105, 66212, false)]
        [DataRow(106, 895118, true)]
        [DataRow(107, 895119, false)]
        [DataRow(108, 5313, false)]
        public void IsBigBatchCode(int Id, long groupCode, bool expected)
        {
            Assert.AreEqual(expected, FullItemCodeUtils.IsGroupHasBigBatchCodes(groupCode));
        }
    }
}