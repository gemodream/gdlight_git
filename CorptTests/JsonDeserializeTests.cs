﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace CorptTests
{
    [TestClass]
    public class JsonDeserializeTests
    {
        #region Procedures

        [TestMethod]
        public void DeserializeTest()
        {
            var customer = new Customer()
            {
                Name = "John Doe",
                Addresses = new List<Address>() {
                    new Address() {
                        Street = "Somestreet",
                        House = "10",
                        Postalcode = "1234"
                    },
                    new Address() {
                        Street = "OtherStreet",
                        House = "20",
                        Postalcode = "4567"
                    }
                }
            };

            //
            // Convert the customer to Json
            string json = JsonConvert.SerializeObject(customer);

            //
            // Create a new customer form the Json string
            var newCustomer = JsonConvert.DeserializeObject<Customer>(json);

            Assert.AreEqual(customer, newCustomer);

        }

        #endregion

        #region Nested type: Address

        public class Address
        {
            public string Street { get; set; }

            public string House { get; set; }

            public string Postalcode { get; set; }

            protected bool Equals(Address other)
            {
                return Street == other.Street && House == other.House && Postalcode == other.Postalcode;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Address)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = (Street != null ? Street.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (House != null ? House.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Postalcode != null ? Postalcode.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        #endregion

        #region Nested type: Customer

        public class Customer
        {
            public string Name { get; set; }

            public List<Address> Addresses { get; set; }

            protected bool Equals(Customer other)
            {
                return Name == other.Name && EqualsLists(Addresses, other.Addresses);
            }

            private bool EqualsLists(IReadOnlyList<Address> addresses, IReadOnlyList<Address> otherAddresses)
            {
                if (ReferenceEquals(null, otherAddresses)) return false;
                if (ReferenceEquals(addresses, null)) return false;
                if (ReferenceEquals(addresses, otherAddresses)) return true;
                if (addresses.Count != otherAddresses.Count) return false;
                for (int i = 0; i < addresses.Count; i++)
                {
                    if (!addresses[i].Equals(otherAddresses[i]))
                    {
                        return false;
                    }
                }
                return true;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Customer)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Addresses != null ? Addresses.GetHashCode() : 0);
                }
            }
        }

        #endregion

        [TestMethod]
        public void PrintRequestDeserializeTest()
        {

        }
    }
    }
